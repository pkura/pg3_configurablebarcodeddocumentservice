package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;

/**
 * @author Marcin Szymaniuk
 * @info
 * Slownik cz這nk闚 dla dokumentow cz這nkowskich
 */ 
public class DCMember extends AbstractDocumentKindDictionary {

	private static final Logger log = LoggerFactory.getLogger(DicInvoice.class);
	
	//pola slownika
	private Long id;
	private String name;
	private String surname;
    private String pesel; 
    private Long accountNumber;
   
    public DCMember(){
    	
    }
    public String getDictionaryDescription()
    {
        return pesel + " " +name + " " + surname;
    }
    public String dictionaryAction()
    {
        return "/office/common/dcdict.action";
    }
	
    //po zaakceptowaniu funkcja js do wykonania, plik dockind.js [dockind_NUMER_RACHUNKU_BANKOWEGO w invoice wypelnic z tego]
    public String dictionaryAccept()
    {
        return "";              
    }
	
    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        	//map.put("numerKontrahenta","Numer Kontrahenta");
        	//map.put("name", "Nazwa instytucji/kontrahenta");
        	//map.put("nip","NIP");
        	
        return map;
    }
    
    /**
     * Zapisuje cz這nka w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania cz這nka. "+e.getMessage());
            throw new EdmException("Blad dodania cz這nka. "+e.getMessage());
        }
    }
    
    /**
     * Usuwa cz這nka
     * @throws EdmException
     */
    public void delete() throws EdmException 
    {
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia cz這nka "+e.getMessage());
            throw new EdmException("Blad usuniecia cz這nka "+e.getMessage());
        }
    }
    
    
    /**
     * Wyszukuje cz這nka
     * @param pesel
     * @return
     * @throws EdmException
     */
    public DCMember find(Long pesel) throws EdmException
    {
        DCMember inst = DSApi.getObject(DCMember.class, pesel);
        if (inst == null) 
            throw new EdmException("Nie znaleziono cz這nka o nr " + pesel);
        else 
            return inst;
    }
    
    public static List<DCMember> find(Map<String, Object> parametry) throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(DCMember.class);
        if(c == null) 
            throw new EdmException("Nie mozna okreslic kryterium wyszukiwania cz這nka");
        if(parametry != null) 
        {
        	if(parametry.get("pesel") != null) 
                c.add(Expression.like("pesel",  parametry.get("pesel")));
        	if(parametry.get("surname") != null) 
                c.add(Expression.like("surname",  "%"+parametry.get("surname")+"%"));
            if(parametry.get("name") != null) 
                c.add(Expression.like("name",  "%"+parametry.get("name")+"%"));
            if(parametry.get("accountNumber")!=null)
            	c.add(Expression.eq("accountNumber", parametry.get("accountNumber")));
        }
        try 
        {
            log.info("pobranie listy cz這nk闚 (dc)");
            return (List<DCMember>)c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Znajduje obiekty DCMember na podstawie formularza wyszukiwania.
     * @param form
     * @return
     * @throws EdmException
     */
    public static SearchResults<? extends DCMember> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DCMember.class);

            if(form.hasProperty("name")) 
                c.add(Expression.like("name",  "%"+form.getProperty("name")+"%"));
        	if(form.hasProperty("surname")) 
                c.add(Expression.like("surname",  "%"+form.getProperty("surname")+"%"));
            if (form.hasProperty("pesel"))
                c.add(Expression.eq("pesel",form.getProperty("pesel")));
            if(form.hasProperty("accountNumber"))
            	c.add(Expression.eq("accountNumber",form.getProperty("accountNumber")));

            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }

            List<DCMember> list = (List<DCMember>) c.list();

            if (list.size() == 0)
                return SearchResultsAdapter.emptyResults(DCMember.class);
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                int toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();
                return new SearchResultsAdapter<DCMember>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), DCMember.class);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	@Override
	public String toString() {
		return "DCMember [accountNumber=" + accountNumber + ", id=" + id + ", name=" + name + ", pesel=" + pesel + ", surname=" + surname
				+ "]";
	}
	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}