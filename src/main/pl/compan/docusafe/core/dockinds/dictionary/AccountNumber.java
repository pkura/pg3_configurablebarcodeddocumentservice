package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.dockinds.field.EnumItem;

/**
 * Numer konta (u�ywane w {@link CentrumKosztowDlaFaktury}).
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class AccountNumber implements EnumItem
{
    //Rozr�niacze dla IC
	private final static int SAD_DISCRIMINATOR = 1;
	private final static int FIXED_ASSETS_DISCRIMINATOR = 2;
	private final static int FIXED_ASSETS_AND_SAD_DISCRIMINATOR = 3;

	private Integer id;
    private String account_number;
    private String name;
    private Boolean needdescription;
    private boolean visibleInSystem = true;
	
	//warto��, kt�ra pozwala rozr�ni� rodzaje kont
	private Integer discriminator;
    //kolejny rozr�niacz
    private boolean flag1 = false;

	/**************************
	 *
	 *		PRZY LISTOWANIU KONT NALE�Y SPRAWDZA� "visibleInSystem"
	 *
	 *************************/

	/**
	 * Zwraca list� kont dla �rodk�w trwa�ych
	 */
	public static List<AccountNumber> fixedAssetsList() throws EdmException{
		return DSApi.context().session().createCriteria(AccountNumber.class)
				.add(Restrictions.or(Restrictions.eq("discriminator", FIXED_ASSETS_DISCRIMINATOR),
					Restrictions.eq("discriminator", FIXED_ASSETS_AND_SAD_DISCRIMINATOR)))
				.add(Restrictions.eq("visibleInSystem", true))
				.addOrder(Order.desc("account_number"))
				.list();
	}

	public static List<AccountNumber> icList() throws EdmException{
		return DSApi.context().session().createCriteria(AccountNumber.class)
				.add(Restrictions.or(Restrictions.eq("discriminator", SAD_DISCRIMINATOR),
					Restrictions.eq("discriminator", FIXED_ASSETS_AND_SAD_DISCRIMINATOR)))
				.add(Restrictions.eq("visibleInSystem", true))
				.addOrder(Order.desc("account_number"))
				.list();
	}

	/**
	 * Wy�wietla wszystkie widoczne konta
	 * @return
	 * @throws pl.compan.docusafe.core.EdmException
	 */
    public static List<AccountNumber> list() throws EdmException
    {
		return DSApi.context().session().createCriteria(AccountNumber.class)
			.add(Restrictions.eq("visibleInSystem", true))
			.addOrder(Order.desc("account_number"))
			.list();
    }

	/**
	 * Wy�wietla wszystkie konta zapisane w bazie danych
	 * @return
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	public static List<AccountNumber> listAll() throws EdmException
    {
		return DSApi.context().session().createCriteria(AccountNumber.class)
			//.addOrder(Order.desc("account_number"))
			.addOrder(Order.asc("account_number"))
			.list();
    }
    
    //add
    public static AccountNumber find(Integer id) throws EdmException {
    	return Finder.find(AccountNumber.class, id);
    }

    
    public static AccountNumber findByNumber(String number) throws EdmException {
    	List<AccountNumber> accountNumbers = DSApi.context().session().createCriteria(AccountNumber.class)
    	        .add(Restrictions.eq("account_number", number))
		        .list();
    	
    	return accountNumbers.size() < 1 ? null : accountNumbers.get(0);
    }
    
    public String getRefValue()
    {
    	return "";
    }
    
    /**
     * Zwraca stringa zawieraj�cego nazwe funkcji (najlepiej umieszczonej w pliku dockind.js)
     *  kt�ra wykona si� w funkcji accept_cn
     * 
     */
    public static String dictionaryAccept()
    {
        return "";              
    }
    
    
    public String getNumber()
    {
        return account_number;
    }

    public void setNumber(String account_number)
    {
        this.account_number = account_number;
    }        
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    
    public Integer getId() {
		return this.id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
    public String getAccountFullName()
	{
		return name == null ? account_number : account_number + " (" + name + ")";  
	}
    
    public String getTitle() {
		return this.name;
	}
	
	public String getCn() {		
		return this.account_number;
	}
	
	public String getArg(int position) {
		return null;
	}
	
	public Object getTmp() {
		return null;
	}
	
	public void setTmp(Object tmp) {
		//nil
	}
	
	public boolean isForceRemark() {
		return false;
	}

	public Integer getCentrum() {
		// TODO Auto-generated method stub
		return null;
	}

	public Boolean getNeeddescription() {
		return needdescription;
	}

	public void setNeeddescription(Boolean needdescription) {
		this.needdescription = needdescription;
	}

	/**
	 * @return the discriminator
	 */
	public Integer getDiscriminator() {
		return discriminator;
	}

	/**
	 * @param discriminator the discriminator to set
	 */
	public void setDiscriminator(Integer discriminator) {
		this.discriminator = discriminator;
	}

	public String getFieldCn() {
		// TODO HELLOO
		return null;
	}

	public boolean isVisibleInSystem() {
		return visibleInSystem;
	}

	public void setVisibleInSystem(boolean visibleInSystem) {
		this.visibleInSystem = visibleInSystem;
	}

    public boolean isAvailable() {
        return visibleInSystem;
    }

    /**
     * Zwraca informacje czy dane konto kosztowe posiada flag� umo�liwiaj�c� omijanie akceptacji dyrektora finansowego
     * Zg�oszenie 3098 w bugzilli
     * @return true jest posiada tak� flag�
     */
    public boolean isIc3098Flag(){
        return flag1;
    }

    public void setIc3098Flag(boolean flag){
        this.flag1 = flag;
    }
}
