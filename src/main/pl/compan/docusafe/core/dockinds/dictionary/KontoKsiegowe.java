package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.dockinds.field.EnumItem;

public class KontoKsiegowe implements EnumItem 
{
	private Integer id;
	//private Integer noumber;
	private String account_number;
	private String name;
	
	
	/*
	public KontoKsiegowe(Integer noumber, String name) {
		this.noumber = noumber;
		this.name = name;
	}
	*/
	
	
	public static KontoKsiegowe find(Integer id) throws EdmException
    {
        return Finder.find(KontoKsiegowe.class, id);
    }
	
	
	public static List<KontoKsiegowe> list() throws EdmException {
		return Finder.list(KontoKsiegowe.class, "id");
	}
	
	/*
	public Integer getNoumber() {
		return this.noumber;
	}
	
	public void setNoumber(Integer noumber) {
		this.noumber = noumber;
	}
	*/
	
	public String getAccountFullName()
	{
		return name == null ? account_number : account_number + " (" + name + ")";  
	}
	public String getAccount_number() {
		return this.account_number;
	}
	
	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}
	
	public String getName() {
		return this.name;
	}
	
	
	//Metody interface EnumItem
	public Integer getId() {
		return this.id;
	}
	
	/** TODO - nieimplementowany
	 * 
	 */
	public String getRefValue()
	{
		return "";
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getTitle() {
		return this.name;
	}
	
	public String getCn() {
		//return this.noumber.toString();
		return this.account_number;
	}
	
	public String getArg(int position) {
		return null;
	}
	
	public Object getTmp() {
		return null;
	}
	
	public void setTmp(Object tmp) {
		//nil
	}
	
	public boolean isForceRemark() {
		return false;
	}

	public Integer getCentrum() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getFieldCn() {
		// TODO TODO HELLOO
		return null;
	}

    public boolean isAvailable() {
        return true;
    }
}