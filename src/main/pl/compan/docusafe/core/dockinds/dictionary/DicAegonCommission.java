package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.crm.ContractorWorker;
import pl.compan.docusafe.util.QueryForm;


public class DicAegonCommission extends AbstractDocumentKindDictionary
{
	private static final Logger log = LoggerFactory.getLogger(DicAegonCommission.class);
	
	private Long id;
	private String numer_agenta;
	private String nip;
	private String nazwa_posrednik_agencja;
	private static DicAegonCommission instance;
	public static synchronized DicAegonCommission getInstance()
	{
		if (instance == null)
			instance = new DicAegonCommission();
		return instance;
	}
	public DicAegonCommission() 
	{
		
	}
	
	public String dictionaryAction()
    {
        return "/office/common/dicaegoncommission.action"; 
    }
	
    public String dictionaryAccept()
    {
        return "";              
    }
    
    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        	map.put("numer_agenta","Numer agenta");
        	map.put("nip","Nip");
        	map.put("nazwa_posrednik_agencja","Nazwa posrednika/agencji");
        return map;
    }
	
    public String getDictionaryDescription()
    {
        return numer_agenta + " " + nip + " " + nazwa_posrednik_agencja;
    }
    
    public String dictionarySortColumn()
    {
        return "numer_agenta";
    }
    
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania wpisu do s�ownika. "+e.getMessage());
            throw new EdmException("Blad dodania wpisu do slownika. "+e.getMessage());
        }
    }
    
    public void delete() throws EdmException 
    {
        try 
        {
        	DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia wpisu ze slownika");
            throw new EdmException("Blad usuniecia wpisu ze slownika. "+e.getMessage());
        }
    }
    
    public DicAegonCommission find(Long id) throws EdmException
    {
    	DicAegonCommission inst = DSApi.getObject(DicAegonCommission.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono wpisu o nr " + id);
        else 
            return inst;
    }
    
    /**
     * Metoda pobiera wartosci ze slownika
     * @param parametry
     * @return
     * @throws EdmException
     */
    public static List<DicAegonCommission> find(Map<String, Object> parametry) throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(DicAegonCommission.class);
        if(c == null) 
            throw new EdmException("Nie mozna okreslic kryterium wyszukiwania.");
        if(parametry != null) 
        {
        	if(parametry.get("numer_agenta") != null) 
                c.add(Expression.like("numer_agenta",  "%"+parametry.get("numer_agenta")+"%"));
        	if(parametry.get("nip") != null) 
                c.add(Expression.like("nip",  "%"+parametry.get("nip")+"%"));
            if(parametry.get("nazwa_posrednik_agencja") != null) 
                c.add(Expression.like("nazwa_posrednik_agencja", "%" + parametry.get("nazwa_posrednik_agencja") + "%"));            
            c.setMaxResults(200);
        }
        try 
        {
            log.info("pobranie listy (DicAegonCommission)");
            return (List<DicAegonCommission>)c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }

    public static SearchResults<? extends DicAegonCommission> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DicAegonCommission.class);

            if(form.hasProperty("numer_agenta")) 
                c.add(Expression.like("numer_agenta",  "%"+form.getProperty("numer_agenta")+"%"));
        	if(form.hasProperty("nip")) 
                c.add(Expression.like("nip",  "%"+form.getProperty("nip")+"%"));
            if (form.hasProperty("nazwa_posrednik_agencja"))
                c.add(Expression.like("nazwa_posrednik_agencja", "%"+form.getProperty("nazwa_posrednik_agencja")+"%"));            
                     
            
            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }
            c.setMaxResults(100);
            List<DicAegonCommission> list = (List<DicAegonCommission>) c.list();

            if (list.size() == 0)
                return SearchResultsAdapter.emptyResults(DicAegonCommission.class);
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                int toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();
                 
                return new SearchResultsAdapter<DicAegonCommission>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), DicAegonCommission.class);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumer_agenta() {
		return numer_agenta;
	}

	public void setNumer_agenta(String numer_agenta) {
		this.numer_agenta = numer_agenta;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getNazwa_posrednik_agencja() {
		return nazwa_posrednik_agencja;
	}

	public void setNazwa_posrednik_agencja(String nazwa_posrednik_agencja) {
		this.nazwa_posrednik_agencja = nazwa_posrednik_agencja;
	}
	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
}
