package pl.compan.docusafe.core.dockinds.dictionary;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * S�ownik s�ownik�w. Zawiera spis s�ownik�w mo�liwych do edycji poprzez link w administracji.
 * @author Micha� Sankowski
 */
public final class DictionaryDictionary implements Dictionary<EditableDictionary>{

	private final static Logger log = LoggerFactory.getLogger(DictionaryDictionary.class);
	public final static DictionaryDictionary INSTANCE = new DictionaryDictionary();

	private final Map<String, EditableDictionary> dictionaries = new ConcurrentHashMap<String, EditableDictionary>();

	/**
	 * List� s�ownik�w inicjalizowanych podczas startu najlepiej umie�ci� tutaj. Na razie tylko dla IC.
	 */
	private DictionaryDictionary(){
		try {
            if(DocumentKind.isAvailable("invoice_ic")){
                this.add(CustomsAgencyDictionary.INSTANCE);
                this.add(VatRateDictionary.INSTANCE);
            }
		} catch (EdmException ex) {
			log.error(ex.getLocalizedMessage(), ex);
		}
	}

	public EditableDictionary get(Serializable id) throws EdmException {
        checkNotNull(id, "id cannot be null");
		return dictionaries.get(TextUtils.toString(id));
	}

	public void add(EditableDictionary entry) throws EdmException {
        log.info("added dictionary '{}'", entry.getCn());
		dictionaries.put(entry.getCn(), entry);
	}

	public void remove(EditableDictionary entry) throws EdmException {
		dictionaries.remove(entry.getCn());
	}

	public List<EditableDictionary> getAll() throws EdmException {
		return new ArrayList(dictionaries.values());
	}

    @Override
    public void update(EditableDictionary entry) throws EdmException {
        this.add(entry);
    }
}
