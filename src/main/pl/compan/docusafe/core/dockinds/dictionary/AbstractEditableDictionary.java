package pl.compan.docusafe.core.dockinds.dictionary;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;

/**
 * Bazowa implementacja EditableDictionary bazuj�ca na tabelce
 * @author Micha� Sankowski
 */
public abstract class AbstractEditableDictionary<T> extends AbstractDictionary<T> implements EditableDictionary<T> {
	private final static Logger LOG = LoggerFactory.getLogger(AbstractEditableDictionary.class);

	public final String getSelectionURL() {
		return "/office/dictionaries/editable-dictionary.action?dictionaryCn=" + getCn();
	}

    @Override
    public final String getSelectionURL(Serializable id) {
        return "/office/dictionaries/editable-dictionary.action?dictionaryCn=" + getCn()
                + "&id="+id;
    }

    @Override
    public void add(T entry) throws EdmException {
        DSApi.context().session().save(entry);
    }

    @Override
    public void remove(T entry) throws EdmException {
        DSApi.context().session().delete(entry);
    }

    @Override
    public void update(T entry) throws EdmException {
        DSApi.context().session().update(entry);
    }

    public abstract String[] getHeaders();

    public abstract Object[] getObjectsForEntry(T entry);

    public abstract String getEditJspPage();

    /**
     * Zwraca now� instancj� obiektu s�ownikowego do dodatnia / modyfikacji
     * @return nowa, pusta instancja
     */
    @Override
    public abstract T get();
}
