package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.DpLogic;
import pl.compan.docusafe.core.dockinds.logic.DpPteLogic;
import pl.compan.docusafe.parametrization.prosika.DicProSIKA;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Piotr Komisarski mailto:komisarskip@student.mini.pw.edu.pl
 * 
 * Jest to slownik instytucji/kontrahent�w dla archiwum dokument�w prawnych
 * Klasa DfInst te� jest slownikiem instytucji/kontrahent�w, ale dla archiwum dokument�w finansowych
 * R�nice s� bardzo drobne. W pryszlosci jest mozliwe ujednolicenie ich. W DpInst trzebaby dodac dwa pola.
 * NIP oraz REGON
 */
public class DpInst extends AbstractDocumentKindDictionary
{

    private static final Logger log = LoggerFactory.getLogger(DpInst.class);
    //@IMPORT/EXPORT
    private Long id;
    private String imie;
    private String nazwisko;
    private String name;
    private String oldname;
    private String ulica;
    private String kod;
    private String miejscowosc;
    private String telefon;
    private String faks;
    private String email;
    private String kraj;
    private String nip;
    private String numerKontrahenta;
    private static DpInst instance;

    public static synchronized DpInst getInstance()
    {
        if( instance == null )
            instance = new DpInst();
        return instance;
    }
    static StringManager sm =
            GlobalPreferences.loadPropertiesFile(DpInst.class.getPackage().getName(), null);

    public DpInst()
    {
    }

    public String dictionaryAction()
    {
        return "/office/common/inst.action";
    }

    /**
     * Zwraca stringa zawieraj�cego nazwe funkcji (najlepiej umieszczonej w pliku dockind.js)
     *  kt�ra wykona si� w funkcji accept_cn
     * 
     */
    public String dictionaryAccept()
    {
        return "";
    }

    public Map<String, String> dictionaryAttributes()
    {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("numerKontrahenta", String.valueOf("Nr Kontrahenta "));
        map.put("name", String.valueOf("Nazwa kontrahenta"));
        map.put("nip", String.valueOf("NIP"));
        return map;
    }

    public String getDictionaryDescription()
    {
        return name;
    }

    public String dictionarySortColumn()
    {
        return "name";
    }

    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch ( HibernateException e )
        {
            log.error(sm.getString(
                    "BladDodaniaInstytucjiKontrahentaDokumentuPrawnego") + e.getMessage());
            throw new EdmException(
                    sm.getString("BladDodaniaInstytucjiKontrahentaDokumentuPrawnego") + e.getMessage());
        }
    }

    /**
     * Usuwa strone
     * @throws EdmException
     */
    public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        }
        catch ( HibernateException e )
        {
            log.error(sm.getString(
                    "BladUsunieciaInstytucjiKontrahentaDokumentuPrawnego"));
            throw new EdmException(sm.getString(
                    "BladUsunieciaInstytucjiKontrahentaDokumentuPrawnego") + e.getMessage());
        }
    }

    /**
     * Wyszukuje konkretna instytucje/kontrahenta dokumentu prawnego
     * @param id
     * @return
     * @throws EdmException
     */
    public DpInst find(Long id) throws EdmException
    {
        DpInst inst = DSApi.getObject(DpInst.class, id);
        if( inst == null )
            throw new EdmException(sm.getString("NieZnalezionoInstytucjiKontrahentaONR") + id);
        else
            return inst;
    }

    @SuppressWarnings(value =
    {
        "unchecked"
    })
    public static List<DpInst> find(Map<String, Object> parametry) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(DpInst.class);
        if( c == null )
            throw new EdmException(sm.getString("NieMoznaOkreslicKryteriumWyszukiwaniaInstytucjiKontrahentaDokumentuPrawnego"));
        if( parametry != null )
        {
            if( parametry.get("numerKontrahenta") != null )
                c.add(Expression.like("numerKontrahenta", "%" + parametry.get("numerKontrahenta") + "%"));
            if( parametry.get("imie") != null )
                c.add(Expression.like("imie", "%" + parametry.get("imie") + "%"));
            if( parametry.get("nazwisko") != null )
                c.add(Expression.like("nazwisko", "%" + parametry.get("nazwisko") + "%"));
            if( parametry.get("name") != null )
                c.add(Expression.like("name", "%" + parametry.get("name") + "%"));
            if( parametry.get("oldname") != null )
                c.add(Expression.like("oldname", "%" + parametry.get("oldname") + "%"));
            if( parametry.get("ulica") != null )
                c.add(Expression.like("ulica", "%" + parametry.get("ulica") + "%"));
            if( parametry.get("kod") != null )
                c.add(Expression.like("kod", "%" + parametry.get("kod") + "%"));
            if( parametry.get("miejscowosc") != null )
                c.add(Expression.like("miejscowosc", "%" + parametry.get("miejscowosc") + "%"));
            if( parametry.get("telefon") != null )
                c.add(Expression.like("telefon", "%" + parametry.get("telefon") + "%"));
            if( parametry.get("faks") != null )
                c.add(Expression.like("faks", "%" + parametry.get("faks") + "%"));
            if( parametry.get("email") != null )
                c.add(Expression.like("email", "%" + parametry.get("email") + "%"));
            if( parametry.get("kraj") != null )
                c.add(Expression.like("kraj", "%" + parametry.get("kraj") + "%"));
        }
        try
        {
            log.info("pobranie listy instytucji/kontrahent�w");
            return (List<DpInst>) c.list();
        }
        catch ( HibernateException e )
        {
            throw new EdmHibernateException(e);
        }
    }

    @SuppressWarnings(value =
    {
        "unchecked"
    })
    /**
     * Znajduje obiekty DpInst na podstawie formularza wyszukiwania.
     * @param form
     * @return
     * @throws EdmException
     */
    public static SearchResults<? extends DpInst> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DpInst.class);

            if( form.hasProperty("numerKontrahenta") )
                c.add(Restrictions.like("numerKontrahenta", "%" + form.getProperty("numerKontrahenta") + "%"));
            if( form.hasProperty("imie") )
                c.add(Restrictions.like("imie", "%" + form.getProperty("imie") + "%"));
            if( form.hasProperty("nazwisko") )
                c.add(Restrictions.like("nazwisko", "%" + form.getProperty("nazwisko") + "%"));
            if( form.hasProperty("name") )
                c.add(Restrictions.like("name", "%" + form.getProperty("name") + "%"));
            if( form.hasProperty("oldname") )
                c.add(Restrictions.like("oldname", "%" + form.getProperty("oldname") + "%"));
            if( form.hasProperty("ulica") )
                c.add(Restrictions.like("ulica", "%" + form.getProperty("ulica") + "%"));
            if( form.hasProperty("kod") )
                c.add(Restrictions.like("kod", "%" + form.getProperty("kod") + "%"));
            if( form.hasProperty("miejscowosc") )
                c.add(Restrictions.like("miejscowosc", "%" + form.getProperty("miejscowosc") + "%"));
            if( form.hasProperty("telefon") )
                c.add(Restrictions.like("telefon", "%" + form.getProperty("telefon") + "%"));
            if( form.hasProperty("faks") )
                c.add(Restrictions.like("faks", "%" + form.getProperty("faks") + "%"));
            if( form.hasProperty("email") )
                c.add(Restrictions.like("email", "%" + form.getProperty("email") + "%"));
            if( form.hasProperty("kraj") )
                c.add(Restrictions.like("kraj", "%" + form.getProperty("kraj") + "%"));
            if( form.hasProperty("nip") )
                c.add(Restrictions.eq("nip", cleanNip("" + form.getProperty("nip"))));

            for( Iterator iter = form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if( field.isAscending() )
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }

            List<DpInst> list = (List<DpInst>) c.list();

            if( list.size() == 0 )
                return SearchResultsAdapter.emptyResults(DpInst.class);
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size() - 1;
                int toIndex = (form.getOffset() + form.getLimit() <= list.size()) ? form.getOffset() + form.getLimit() : list.size();

                return new SearchResultsAdapter<DpInst>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), DpInst.class);
            }
        }
        catch ( HibernateException e )
        {
            throw new EdmHibernateException(e);
        }
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFaks()
    {
        return faks;
    }

    public void setFaks(String faks)
    {
        this.faks = faks;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getImie()
    {
        return imie;
    }

    public void setImie(String imie)
    {
        this.imie = imie;
    }

    public String getKod()
    {
        return kod;
    }

    public void setKod(String kod)
    {
        this.kod = kod;
    }

    public String getKraj()
    {
        return kraj;
    }

    public void setKraj(String kraj)
    {
        this.kraj = kraj;
    }

    public String getMiejscowosc()
    {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc)
    {
        this.miejscowosc = miejscowosc;
    }

    public String getNazwisko()
    {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko)
    {
        this.nazwisko = nazwisko;
    }

    public String getName()
    {
        return name;
    }

    public void cleanFolders() throws EdmException
    {

        Set<Folder> folders = new HashSet<Folder>();

        if( DocumentKind.isAvailable(DocumentLogicLoader.DP_PTE_KIND) )
        {
            DockindQuery query = new DockindQuery(0, 0);
            DocumentKind dc = DocumentKind.findByCn(DocumentLogicLoader.DP_PTE_KIND);
            query.setDocumentKind(dc);
            query.setCheckPermissions(false);
            query.field(dc.getFieldByCn(DpPteLogic.INSTYTUCJA_KONTRAHENT_FIELD_CN), getId());
            Iterator<Document> it = DocumentKindsManager.search(query);
            while( it.hasNext() )
            {
                Document doc = it.next();
                folders.add(doc.getFolder());
                dc.logic().archiveActions(doc, -500);
            }
        }

        if( DocumentKind.isAvailable(DocumentLogicLoader.DP_KIND) )
        {
            DockindQuery query = new DockindQuery(0, 0);
            DocumentKind dc = DocumentKind.findByCn(DocumentLogicLoader.DP_KIND);
            query.setDocumentKind(dc);
            query.setCheckPermissions(false);
            query.field(dc.getFieldByCn(DpLogic.INST_FIELD_CN), getId());
            Iterator<Document> it = DocumentKindsManager.search(query);
            while( it.hasNext() )
            {
                Document doc = it.next();
                folders.add(doc.getFolder());
                dc.logic().archiveActions(doc, -500);
            }
        }

        DSApi.context().session().flush();

        try
        {
            pl.compan.docusafe.core.base.DocumentHelper dh = new pl.compan.docusafe.core.base.DocumentHelper();
            for( Folder fl : folders )
            {
                dh.safeDeleteFolder(fl, false);
            }
        }
        catch ( Exception e )
        {
            throw new EdmException(e);
        }

    }

    public void setName(String name)
    {
        this.name = name;

        /*if (name != null && !name.equals(this.name) && id != null)
        {
        this.name = name;
        try
        {
        this.cleanFolders();
        }
        catch (EdmException e) 
        {
        log.debug(e.getMessage());
        }
        }
        else
        {
        this.name = name;
        }*/
    }

    public String getOldname()
    {
        return oldname;
    }

    public void setOldname(String oldname)
    {
        this.oldname = oldname;
    }

    public String getTelefon()
    {
        return telefon;
    }

    public void setTelefon(String telefon)
    {
        this.telefon = telefon;
    }

    public String getUlica()
    {
        return ulica;
    }

    public void setUlica(String ulica)
    {
        this.ulica = ulica;
    }

    @Override
    public String toString()
    {
        return getName();
    }

    public String getNip()
    {
        return nip;
    }

    public void setNip(String nip)
    {

        this.nip = cleanNip(nip);
    }

    public static String cleanNip(String nip)
    {
        if( nip != null )
            return nip.replaceAll("-", "").replaceAll(" ", "");
        else
            return nip;
    }

    public static String prettyNip(String nip)
    {
        try
        {
            StringBuffer sb = new StringBuffer(nip);
            for( int i = 3; i <= 9; i = i + 3 )
            {
                sb.insert(i, "-");
            }

            return sb.toString();
        }
        catch ( Exception e )
        {
            return nip;
        }
    }

    public String getPrettyNip()
    {
        return prettyNip(getNip());
    }

    public String getNumerKontrahenta()
    {
        return numerKontrahenta;
    }

    public void setNumerKontrahenta(String numerKontrahenta)
    {
        this.numerKontrahenta = numerKontrahenta;
    }

    @Override
    public Map<String, String> popUpDictionaryAttributes()
    {
        // TODO Auto-generated method stub
        return null;
    }
}
