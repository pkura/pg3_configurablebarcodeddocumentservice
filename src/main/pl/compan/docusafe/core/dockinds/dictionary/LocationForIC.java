package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.io.IOUtils;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Zmapowana lokalizacja dla IC, nie powinna by� tworzona tylko wyszukiwana
 */
public class LocationForIC {
    private final static Logger LOG = LoggerFactory.getLogger(LocationForIC.class);

    public final static Set<String> LOCATION_CNS_WITH_ANALYTICS = ImmutableSet.of("DAB",
            "CZO",
            "HUB",
            "HWR",
            "KAT",
            "KOM");

	private Integer id;
	private String cn;
	private String title;
	private Integer centrum;
	private String refValue;
	private boolean available;
	
	LocationForIC(){
	}

//	public LocationForIC(String cn, String title, Integer centrum,
//			String refValue, boolean available) {
//		this.cn = cn;
//		this.title = title;
//		this.centrum = centrum;
//		this.refValue = refValue;
//		this.available = available;
//	}

	public static List<LocationForIC> list() throws EdmException{
		return DSApi.context().session()
                .createCriteria(LocationForIC.class)
                .add(Restrictions.eq("available", true))
                .addOrder(Order.asc("cn"))
                .list();
	}

	public static LocationForIC find(int id) throws EdmException{
		return (LocationForIC) DSApi.context().session().get(LocationForIC.class, id);
	}

    /**
     * Zwraca lokalizacj� o podanym cn
     * @param cn
     * @return
     * @throws EdmException
     */
    public static LocationForIC findByCn(String cn) throws EdmException {
        List<LocationForIC> list = DSApi.context().session().createCriteria(LocationForIC.class)
                .add(Restrictions.eq("cn",cn))
                .setMaxResults(1)
                .list();
        return list.isEmpty() ? null : list.get(0);
    }

	/**
	 * @return the cn
	 */
	public String getCn() {
		return cn;
	}

	/**
	 * @param cn the cn to set
	 */
	public void setCn(String cn) {
		this.cn = cn;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the centrum
	 */
	public Integer getCentrum() {
        
                
		return centrum;
	}

	/**
	 * @param centrum the centrum to set
	 */
	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	/**
	 * @return the refValue
	 */
	public String getRefValue() {
		return refValue;
	}

	/**
	 * @param refValue the refValue to set
	 */
	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	/**
	 * @return the available
	 */
	public boolean isAvailable() {
		return available;
	}

	/**
	 * @param available the available to set
	 */
	public void setAvailable(boolean available) {
		this.available = available;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LocationForIC [id=" + id + ", cn=" + cn + ", title=" + title
				+ ", centrum=" + centrum + ", refValue=" + refValue
				+ ", available=" + available + "]";
	}

}
