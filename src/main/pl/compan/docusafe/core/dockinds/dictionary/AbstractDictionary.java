package pl.compan.docusafe.core.dockinds.dictionary;
import com.google.common.base.Supplier;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * 
 * @author Micha� Sankowski
 */
public abstract class AbstractDictionary<T> implements Dictionary<T>, Supplier<T> {
	private final static Logger LOG = LoggerFactory.getLogger(AbstractDictionary.class);

}
