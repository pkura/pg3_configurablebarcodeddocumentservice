package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindQuery;

public interface DocumentKindDictionary
{
	/***
	 * Zwraca opis sownika
	 * @return
	 */
	public String getDictionaryDescription();

	/**
	 * Zapisuje strone w sesji hibernate
	 * 
	 * @throws EdmException
	 */
	public void create() throws EdmException;

	public void delete() throws EdmException;

    public Long getId();
	
	/***
	 * Metoda wczytuje objekt, wywoywane z FieldsManagera
	 * @param key
	 * @return
	 * @throws EdmException
	 */
	public DocumentKindDictionary find(Long key) throws EdmException;
	/***
	 * Funkcja potrzebna do obsugi Ajaxa, wyszukuje instancje sownika na podstawie porzekazanych danych
	 * Wynik jest w postaci Listy map w celu atwiejszej konwersjii na JSON.
	 * Kazda mapa jest pojedynczym obiektem do wyswietlenia na wyniku wyszukania
	 * args s� w takiej kolejno�ci w jakiej s� wyswietlone dictionaryAttributes
	 * mapa jako pierwszy atrybut musi miec zmienna dicId zawierajaca id instancjii slownika ktora posluzy do zapisania obiektu w dokumencie
	 * @param args
	 * @return
	 * @throws EdmException
	 */
	public List<Map<String,String>> search(Object[] args) throws EdmException; 
	
	public List<String> additionalSearchParam();
	
	/**
	 * Zwraca stringa zawieraj�cego kod javascript kt�ry wykona si� w funkcji
	 * accept_cn
	 * 
	 */
	public String dictionaryAccept();

	/***
	 * Mapa warto�ci widocznych na formatce,
	 * <nazwa zmiennej,nazwa pola dla uzytkownika>
	 * @return
	 */
	public Map<String, String> dictionaryAttributes();
	
	public boolean isAjaxAvailable();
	
	public boolean isBoxDescriptionAvailable();
	
	public String getBoxDescription();
	
	public Integer getHeight();
	
	public Integer getWidth();
	
	/**
	 * Sprawdza czy slownik nadpisuje utworzenie zaptania dla tego pola
	 */
    public boolean isOverloadQuery(String property);
    
    
    /**
     * Dodaje warunek do zapytania 
     */
    public void addOverloadQuery(String property, String value, DockindQuery query);
    
    public String dictionarySortColumn();
    
    public String dictionaryAction();
}
