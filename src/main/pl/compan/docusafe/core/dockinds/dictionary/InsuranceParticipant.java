package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Proxy;
import org.hibernate.validator.constraints.Length;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
@Entity
@Proxy(lazy=false)
@Table(name = "dsg_insurance_participant")
public class InsuranceParticipant extends AbstractDocumentKindDictionary{

	private final static Logger LOG = LoggerFactory.getLogger(InsuranceParticipant.class);

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@NotNull
	@Length(max=40)
	private String name;

	@NotNull
	@Length(max=40)
	private String surname;

//	@Email
	@Length(max=40)
	private String email;

	@Length(max=40)
	private String pesel;

	@Length(max=40)
	private String ac;

	@Length(max=40)
	private String oc;

	public Map<String, String> dictionaryAttributes() {
		
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("name", "Imi�");
		map.put("surname", "Nazwisko");
		return map;
	}

	public String dictionaryAction() {
		return "/office/common/insurance-participant.action";
	}

	public String dictionarySortColumn() {
		return "surname";
	}

	public String getDictionaryDescription(){
		return "Zg�aszaj�cy";
	}

	public String dictionaryAccept() {
		return "";
	}

	public Long getId() {
		return id;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	/**
	 * @return the ac
	 */
	public String getAc() {
		return ac;
	}

	/**
	 * @param ac the ac to set
	 */
	public void setAc(String ac) {
		this.ac = ac;
	}

	/**
	 * @return the oc
	 */
	public String getOc() {
		return oc;
	}

	/**
	 * @param oc the oc to set
	 */
	public void setOc(String oc) {
		this.oc = oc;
	}

	@Override
	public void create() throws EdmException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete() throws EdmException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DocumentKindDictionary find(Long key) throws EdmException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

}
