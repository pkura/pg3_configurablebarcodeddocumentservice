package pl.compan.docusafe.core.dockinds.dictionary;

import com.google.common.base.Supplier;

import java.io.Serializable;

/**
 * S�ownik, kt�ry posiada odpowiedni interfejs webowy, kt�ry umo�liwia edycj�
 * @author Micha� Sankowski
 */
public interface EditableDictionary<T> extends Dictionary<T> {

	/**
	 * Zwraca odno�nik do strony, na kt�rej mo�na wybra�/wyszuka� wpis.
	 * @param id identyfikator wpisu, kt�ry ma by� wyr�niony, zaznaczony
	 * @return url do strony pozwalaj�cej na wyszukiwanie
	 */
	String getSelectionURL(Serializable id);

	/**
	 * Zwraca odno�nik do strony, na kt�rej mo�na wybra�/wyszuka� wpis.
	 * @return url do strony pozwalaj�cej na wyszukiwanie
	 */
	String getSelectionURL();

	/**
	 * @return Unikalny w ca�ym systemie kod s�ownika.
	 */
	String getCn();

	/**
	 * @return Opis dla u�ytkownika.
	 */
	String getTitle();


}
