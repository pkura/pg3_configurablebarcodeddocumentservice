package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.Collections;
import java.util.Map;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.EdmException;

public class UserDivision extends AbstractDocumentKindDictionary{

    public Long getId() { return null; }

	public String dictionaryAction() {
		// TODO Auto-generated method stub
		return null;
	}


	public void create() throws EdmException {
		// TODO Auto-generated method stub
		
	}


	public void delete() throws EdmException {
		// TODO Auto-generated method stub
		
	}


	public String getDictionaryDescription() {
		// TODO Auto-generated method stub
		return null;
	}


	public DocumentKindDictionary find(Long key) throws EdmException {
		// TODO Auto-generated method stub
		return null;
	}


	public String dictionaryAccept() {
		// TODO Auto-generated method stub
		return null;
	}

	private final static Map<String, String> dictionaryAttributes;
    static {
        Map<String, String> map = Maps.newLinkedHashMap();
        map.put("firstname", "Imi�");
        map.put("lastname", "Nazwisko");
        map.put("division", "Dzia�");
        dictionaryAttributes = Collections.unmodifiableMap(map);
    }

    private final static Map<String, String> popUpDictionaryAttributes;
    static {
        Map<String, String> map = Maps.newLinkedHashMap();
        map.put("firstname", "Imi�");
        map.put("lastname", "Nazwisko");
        map.put("division", "Dzia�");    
        popUpDictionaryAttributes = Collections.unmodifiableMap(map);
    }
    
    private final static Map<String, String> promptDictionaryAttributes;
    static {
        Map<String, String> map = Maps.newLinkedHashMap();
        map.put("firstname", "Imi�");
        map.put("lastname", "Nazwisko");
        map.put("division", "Dzia�");    
        promptDictionaryAttributes = Collections.unmodifiableMap(map);
    }
	
    public Map<String, String> promptDictionaryAttributes() {
        return promptDictionaryAttributes;
    }
    
    @Override
    public Map<String, String> dictionaryAttributes() {
        return dictionaryAttributes;
    }

    @Override
    public Map<String, String> popUpDictionaryAttributes() {
        return popUpDictionaryAttributes;
    }

}
