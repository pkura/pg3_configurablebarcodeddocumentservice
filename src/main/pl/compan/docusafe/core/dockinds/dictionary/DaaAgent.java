package pl.compan.docusafe.core.dockinds.dictionary;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.QueryForm;
import org.hibernate.HibernateException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * @author Bartlomiej Spychalski mailto:bsp@spychalski.eu
 */
public class DaaAgent extends DaaDictionary implements DocumentKindDictionary
{
    private static final Logger log = LoggerFactory.getLogger(DaaAgent.class);

    private Long id;
    private String imie;
    private String nazwisko;
    private String numer;
    private Boolean setOfDocuments;
    private DaaAgencja agencja;

    public DaaAgent()
    {

    }

    public String dictionaryAction()
    {
        return "/office/common/agent.action";
    }
    
    /**
     * Zwraca stringa zawieraj�cego nazwe funkcji (najlepiej umieszczonej w pliku dockind.js)
     *  kt�ra wykona si� w funkcji accept_cn
     * 
     */
    public String dictionaryAccept()
    {
        return "";              
    }

    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        map.put("imie", "Imi�");
        map.put("nazwisko", "Nazwisko");
        map.put("numer", "Numer");
        map.put("setOfDocuments", "Komplet dokument�w");

        return map;
    }
    
    public String getDictionaryDescription()
    {
        return imie+" "+nazwisko;
    }
    
    public String dictionarySortColumn()
    {
        return "nazwisko";
    }

    /**
     * Pobiera liste wszystkich agencji modulu archiwum agenta
     * @return
     * @throws EdmException
     */
    public static List<DaaAgent> findAll() throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(DaaAgent.class);

        if (c == null) 
            throw new EdmException("Nie mozna ustalic kryterium wyszukiwania agencji archiwum agenta");
        try 
        {
            return c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List<DaaAgent> find(Map<String, Object> parametry) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(DaaAgent.class);
        if (c == null) 
            throw new EdmException("Nie mo�na ustalic kryterium wyszukiwania agencji AA");

        if (parametry != null)
        {
            if (parametry.get("imie") != null) c.add(Expression.like("imie", "%"+parametry.get("imie")+"%"));
            if (parametry.get("nazwisko") != null) c.add(Expression.like("nazwisko", "%"+parametry.get("nazwisko")+"%"));
            if (parametry.get("agencja") != null) c.add(Expression.eq("agencja", parametry.get("agencja")));
            if (parametry.get("numer") != null) c.add(Expression.eq("numer", parametry.get("numer")));
            if (parametry.get("setOfDocuments") != null) c.add(Expression.eq("setOfDocuments", parametry.get("setOfDocuments")));
        }
        try 
        {
            return c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Znajduje obiekty DaaAgent na podstawie formularza wyszukiwania.
     * @param form
     * @return
     * @throws EdmException
     */
    public static SearchResults<? extends DaaAgent> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DaaAgent.class);

            if (form.hasProperty("imie"))
                c.add(Expression.like("imie", "%"+form.getProperty("imie")+"%"));
            if (form.hasProperty("nazwisko"))
                c.add(Expression.like("nazwisko", "%"+form.getProperty("nazwisko")+"%"));
            if (form.hasProperty("numer"))
                c.add(Expression.eq("numer", form.getProperty("numer")));
            if (form.hasProperty("agencja"))
                c.add(Expression.eq("agencja", form.getProperty("agencja")));
            System.out.println("SetOfDocuments: " + form.getProperty("setOfDocuments"));
            if (form.hasProperty("setOfDocuments"))
            	c.add(Expression.eq("setOfDocuments", form.getProperty("setOfDocuments")));

            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }

            List<DaaAgent> list = (List<DaaAgent>) c.list();

            if (list.size() == 0)
            {
                return SearchResultsAdapter.emptyResults(DaaAgent.class);
            }
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                int toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();

                return new SearchResultsAdapter<DaaAgent>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), DaaAgent.class);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Wyszukuje agenta po identyfikatorze
     * @param id
     * @return
     * @throws EdmException
     */
    public DaaAgent find(Long id) throws EdmException {

        DaaAgent daaAgent = DSApi.getObject(DaaAgent.class, id);
        if (daaAgent == null) throw new EdmException("Nie znaleziono agenta o nr " + id); else return daaAgent;
    }

    public static List<DaaAgent> findByAgencja(DaaAgencja agencja) throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(DaaAgent.class);

        try
        {
            c.createCriteria("agencja").add(org.hibernate.criterion.Expression.eq("id",agencja.getId()));
            return c.list();

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zapisuje dokument w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad dodania typu dokumentu archiwum agenta. "+e.getMessage());
        }
    }

    public void delete() throws EdmException 
    {
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e)
        {
            log.error("Blad usuniecia typu dokumentu archiwum agenta. "+e.getMessage());
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getImie()
    {
        return imie;
    }

    public void setImie(String imie) 
    {
        this.imie = imie;
    }

    public String getNazwisko()
    {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko)
    {
        this.nazwisko = nazwisko;
    }

    public DaaAgencja getAgencja()
    {
        return agencja;
    }

    public void setAgencja(DaaAgencja agencja)
    {
        this.agencja = agencja;
    }

    public String getNumer() 
    {
        return numer;
    }

    public void setNumer(String numer)
    {
        this.numer = numer;
    }
    
    public Boolean getSetOfDocuments()
	{
		return setOfDocuments;
	}
    
    public void setSetOfDocuments(Boolean setOfDocuments)
	{
		this.setOfDocuments = setOfDocuments;
	}

	public List<String> additionalSearchParam() {
		return null;
	}

	public String getBoxDescription() {
		return null;
	}

	public boolean isAjaxAvailable() {
		return false;
	}

	public boolean isBoxDescriptionAvailable() {
		return false;
	}

	public List<Map<String, String>> search(Object[] args) throws EdmException {
		return null;
	}
}
