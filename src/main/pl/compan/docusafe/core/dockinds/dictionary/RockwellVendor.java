package pl.compan.docusafe.core.dockinds.dictionary;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.parametrization.presale.ItemOrderDictionary;
import pl.compan.docusafe.util.QueryForm;
import org.hibernate.HibernateException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.*;

/**
 * @author Piotr Komisarski mailto:komisarskip@student.mini.pw.edu.pl
 * 
 * Jest to slownik instytucji/kontrahent�w dla archiwum dokument�w finansowych
 * Klasa DpInst te� jest slownikiem instytucji/kontrahent�w, ale dla archiwum dokument�w prawnych
 * R�nice s� bardzo drobne. W przyszlosci jest mozliwe ujednolicenie ich. W DpInst trzebaby dodac dwa pola.
 */
public class RockwellVendor extends AbstractDocumentKindDictionary
{
    private static final Logger log = LoggerFactory.getLogger(RockwellVendor.class);
    
    //@IMPORT/EXPORT
    private Long id;
    private String vendorId;
    private String setId;
    private String shortName;
    private String name;
    private String location;
    private String bankAccountNumber;
    private String bankId;
    private String swiftId;
    private String iban;
    private String address;
    private String city;
    private Integer paymentTerms;
	private static RockwellVendor instance;
	public static synchronized RockwellVendor getInstance()
	{
		if (instance == null)
			instance = new RockwellVendor();
		return instance;
	}
    public RockwellVendor()
    {
        
    }
    
    public String dictionaryAction()
    {
        return "/office/common/rockwellVendor.action";
    }
    
    /**
     * Zwraca stringa zawieraj�cego nazwe funkcji (najlepiej umieszczonej w pliku dockind.js)
     *  kt�ra wykona si� w funkcji accept_cn
     * 
     */
    public String dictionaryAccept()
    {
        return "";              
    }

    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        map.put("name", "Name");
        map.put("vendorId","Vendor ID");
        map.put("setId","Set ID");
        map.put("bankAccountNumber","Bank Account Number");
        return map;
    }
    
    public static String getMultiSearchKolumn()
    {
    	return "vendorId";
    }
    
    public static String getMultiSearchName()
    {
    	return "Vendor ID";
    }
    
    public String getDictionaryDescription()
    {
        return name + " " + vendorId;
    }    

    public String dictionarySortColumn()
    {
        return "name";
    }  
    
    /**
     * Zapisuje kontrahenta w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania instytucji/kontrahenta dokumentu finansowego. "+e.getMessage());
            throw new EdmException("Blad dodania instytucji/kontrahenta dokumentu finansowego. "+e.getMessage());
        }
    }
    
    /**
     * Usuwa kontrahenta
     * @throws EdmException
     */
    public void delete() throws EdmException 
    {
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia instytucji/kontrahenta dokumentu prawnego");
            throw new EdmException("Blad usuniecia instytucji/kontrahenta dokumentu prawnego. "+e.getMessage());
        }
    }

    /**
     * Wyszukuje konkretna instytucje/kontrahenta dokumentu finansowego
     * @param id
     * @return
     * @throws EdmException
     */
    public RockwellVendor find(Long id) throws EdmException
    {
        RockwellVendor inst = DSApi.getObject(RockwellVendor.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono instytucji/kontrahenta o nr " + id);
        else 
            return inst;
    }

    @SuppressWarnings(value={"unchecked"})
    public static List<RockwellVendor> find(Map<String, Object> parametry) throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(RockwellVendor.class);
        if(c == null) 
            throw new EdmException("Nie mozna okreslic kryterium wyszukiwania instytucji/kontrahenta dokumentu finansowego");
        if(parametry != null) 
        {
            if(parametry.get("imie") != null) 
                c.add(Expression.like("imie",  "%"+parametry.get("imie")+"%"));
            if(parametry.get("nazwisko") != null) 
                c.add(Expression.like("nazwisko", "%" + parametry.get("nazwisko") + "%"));
            if(parametry.get("name") != null) 
                c.add(Expression.like("name", "%"+parametry.get("name")+"%"));
            if(parametry.get("oldname") != null) 
                c.add(Expression.like("oldname", "%"+parametry.get("oldname")+"%"));
            if(parametry.get("nip") != null) 
                c.add(Expression.like("nip", "%"+parametry.get("nip")+"%"));
            if(parametry.get("regon") != null) 
                c.add(Expression.like("regon", "%"+parametry.get("regon")+"%"));
            if(parametry.get("ulica") != null) 
                c.add(Expression.like("ulica", "%" + parametry.get("ulica") + "%"));
            if(parametry.get("kod") != null) 
                c.add(Expression.like("kod", "%" + parametry.get("kod") + "%"));
            if(parametry.get("miejscowosc") != null) 
                c.add(Expression.like("miejscowosc", "%" + parametry.get("miejscowosc") + "%"));
            if(parametry.get("telefon") != null) 
                c.add(Expression.like("telefon", "%" + parametry.get("telefon") + "%"));
            if(parametry.get("faks") != null) 
                c.add(Expression.like("faks", "%" + parametry.get("faks") + "%"));
            if(parametry.get("email") != null) 
                c.add(Expression.like("email", "%" + parametry.get("email") + "%"));
            if(parametry.get("kraj") != null) 
                c.add(Expression.like("kraj", "%" + parametry.get("kraj") + "%"));
            if(parametry.get("paymentTerms") != null) 
                c.add(Expression.eq("paymentTerms",parametry.get("paymentTerms")));
        }
        try 
        {
            return (List<RockwellVendor>)c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }    
    
    @SuppressWarnings(value={"unchecked"})
    /**
     * Znajduje obiekty DfInst na podstawie formularza wyszukiwania.
     * @param form
     * @return
     * @throws EdmException
     */
    public static SearchResults<? extends RockwellVendor> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(RockwellVendor.class);            
            
            if(form.hasProperty("vendorId"))
            	c.add(Expression.like("vendorId",form.getProperty("vendorId")));                               
            if(form.hasProperty("setId")) 
            	c.add(Expression.like("setId", "%"+form.getProperty("setId")+"%"));
            if(form.hasProperty("shortName")) 
            	c.add(Expression.like("shortName", "%"+form.getProperty("shortName")+"%"));
            if(form.hasProperty("name")) 
            	c.add(Expression.like("name", "%"+form.getProperty("name")+"%"));
            if(form.hasProperty("location")) 
            	c.add(Expression.like("location", "%"+form.getProperty("location")+"%"));
            if(form.hasProperty("bankAccountNumber")) 
            	c.add(Expression.like("bankAccountNumber", "%"+form.getProperty("bankAccountNumber")+"%"));
            if(form.hasProperty("bankId")) 
            	c.add(Expression.like("bankId", "%"+form.getProperty("bankId")+"%"));
            if(form.hasProperty("swiftId")) 
            	c.add(Expression.like("swiftId", "%"+form.getProperty("swiftId")+"%"));
            if(form.hasProperty("iban")) 
            	c.add(Expression.like("iban", "%"+form.getProperty("iban")+"%"));
            if(form.hasProperty("address")) 
            	c.add(Expression.like("address", "%"+form.getProperty("address")+"%"));
            if(form.hasProperty("city")) 
            	c.add(Expression.like("city", "%"+form.getProperty("city")+"%"));
            if(form.hasProperty("paymentTerms")) 
                c.add(Expression.eq("paymentTerms",form.getProperty("paymentTerms")));
            
            
            

            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }

            List<RockwellVendor> list = (List<RockwellVendor>) c.list();

            if (list.size() == 0)
                return SearchResultsAdapter.emptyResults(RockwellVendor.class);
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                int toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();
                 
                return new SearchResultsAdapter<RockwellVendor>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), RockwellVendor.class);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getSetId() {
		return setId;
	}

	public void setSetId(String setId) {
		this.setId = setId;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getSwiftId() {
		return swiftId;
	}

	public void setSwiftId(String swiftId) {
		this.swiftId = swiftId;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

}
