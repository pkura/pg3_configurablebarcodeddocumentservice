package pl.compan.docusafe.core.dockinds.dictionary;
import java.io.Serializable;
import java.util.List;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 *
 * @author Micha� Sankowski
 */
public class CustomsAgencyDictionary extends AbstractEditableDictionary<CustomsAgency>{

	//TODO - doda� cacheowanie
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(CustomsAgencyDictionary.class.getPackage().getName(),null);
	private final static Logger log = LoggerFactory.getLogger(CustomsAgencyDictionary.class);
	private final static String CN = "customs-agency";

	public final static CustomsAgencyDictionary INSTANCE = new CustomsAgencyDictionary();

	private CustomsAgencyDictionary(){
	}

    @Override
    public CustomsAgency get() {
        return new CustomsAgency();
    }

    @Override
    public String getEditJspPage() {
        return "/office/dictionaries/edit-custom-agency.jsp";
    }

	public CustomsAgency get(Serializable id) throws EdmException {
		return CustomsAgency.find((Integer)id);
	}

	public List<CustomsAgency> getAll() throws EdmException {
		return CustomsAgency.list();
	}

    @Override
    public String[] getHeaders() {
        return new String[]{"ID","Nazwa"};
    }

    @Override
    public Object[] getObjectsForEntry(CustomsAgency entry) {
        return new Object[]{entry.getId(), entry.getTitle()};
    }

    public String getCn() {
		return CN;
	}

	public String getTitle() {
		return sm.getString("customsAgencyDictionary");
	}
}
