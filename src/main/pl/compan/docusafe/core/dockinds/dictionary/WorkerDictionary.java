package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.EmployeeDocumentLogic;
import pl.compan.docusafe.parametrization.prosika.DicProSIKA;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Mariusz Kilja�czyk
 *
 */

public class WorkerDictionary extends AbstractDocumentKindDictionary
{
    private static final Logger log = LoggerFactory.getLogger(WorkerDictionary.class);

    private Long   id;
    private String idPracownika;
    private String nazwisko;
    private String imie;
    private Boolean pracuje;
    private Integer instytucja;

    public static Integer LIFE = 1;
    public static Integer PTE = 2;
	private static WorkerDictionary instance;
	public static synchronized WorkerDictionary getInstance()
	{
		if (instance == null)
			instance = new WorkerDictionary();
		return instance;
	}
    StringManager sm =
        GlobalPreferences.loadPropertiesFile(WorkerDictionary.class.getPackage().getName(),null);

    public WorkerDictionary()
    {

    }

    public String dictionaryAction()
    {
        return "/office/common/workerDictionary.action";
    }

    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        map.put("imie","Imi�");
        map.put("nazwisko", "Nazwisko");
        map.put("idPracownika", "Nr. ewidencyjny");
        return map;
    }

    /**
     * Zwraca stringa zawieraj�cego nazwe funkcji (najlepiej umieszczonej w pliku dockind.js)
     *  kt�ra wykona si� w funkcji accept_cn
     *
     */
    public String dictionaryAccept()
    {
        return "";
    }


    public String getDictionaryDescription()
    {
        return imie +" "+nazwisko;
    }

    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            log.error("Blad dodania pracownika. "+e.getMessage());
            throw new EdmException("B��d dodania pracownika");
        }
    }

    public static List<WorkerDictionary> findWorkerByImieNazwisko(String imie,String nazwisko) throws EdmException
    {
    	Criteria criteria = DSApi.context().session().createCriteria(WorkerDictionary.class,"id");
    	criteria.add(Restrictions.eq("imie", imie).ignoreCase());
    	criteria.add(Restrictions.eq("nazwisko", nazwisko).ignoreCase());
    	return criteria.list();
    }

    public static WorkerDictionary getWorkerByIDpracownika(String idPracownika) throws EdmException
    {
    	Criteria criteria = DSApi.context().session().createCriteria(WorkerDictionary.class,"id");
    	criteria.add(Restrictions.eq("idPracownika", idPracownika).ignoreCase());
    	List<WorkerDictionary> lista = criteria.list();
    	if(lista != null && lista.size() > 0)
    		return lista.get(0);
    	else
    		return null;
    }

    public WorkerDictionary find(Long id) throws EdmException
    {
        WorkerDictionary inst = DSApi.getObject(WorkerDictionary.class, id);
        if (inst == null)
            throw new EdmException("Nie znaleziono pracownika "+id);
        else
            return inst;
    }

    public void delete() throws EdmException
    {
        try
        {
        	DockindQuery dockindQuery = new DockindQuery(0, 10);
        	dockindQuery.setDocumentKind(DocumentKind.findByCn(DocumentLogicLoader.EMPLOYEE_KIND));
        	dockindQuery.field(DocumentKind.findByCn(DocumentLogicLoader.EMPLOYEE_KIND).getFieldByCn(EmployeeDocumentLogic.PRACOWNIK_FIELD_CN), id);
        	SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
        	if(searchResults.count() > 0 )
        		throw new EdmException("Nie mo�na usun�� pracownika zwi�zanego z dokumentem.");

            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            log.error("Blad usuniecia pracownika");
            throw new EdmException("B��d usuni�cia pracownika");
        }
    }

    public void save() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            log.error("Blad zapisu pracownika");
            throw new EdmException("B��d zapisu pracownika");
        }

    }

    @SuppressWarnings("unchecked")
    public static SearchResults<? extends WorkerDictionary> search(QueryForm form)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(WorkerDictionary.class);

            if (form.hasProperty("imie"))
            {
                c.add(Restrictions.like("imie",form.getProperty("imie") + "%").ignoreCase());
            }
            if (form.hasProperty("nazwisko"))
            {
                c.add(Restrictions.like("nazwisko",form.getProperty("nazwisko") + "%").ignoreCase());
            }
            if (form.hasProperty("idPracownika"))
            {
                c.add(Restrictions.eq("idPracownika",form.getProperty("idPracownika")).ignoreCase());
            }
            if (form.hasProperty("pracuje"))
            {
                c.add(Restrictions.eq("pracuje",form.getProperty("pracuje")));
            }
            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }

            List<WorkerDictionary> list = (List<WorkerDictionary>) c.list();

            if (list.size() == 0)
                return SearchResultsAdapter.emptyResults(WorkerDictionary.class);
            else
            {
                int toIndex;
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                if(form.getLimit()== 0)
                    toIndex = list.size();
                else
                    toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();

                return new SearchResultsAdapter<WorkerDictionary>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), WorkerDictionary.class);
            }
        }
        catch (HibernateException e)
        {
            try
            {
                throw new EdmHibernateException(e);
            }
            catch (EdmHibernateException e1)
            {
            	LogFactory.getLog("eprint").error("", e1);
            }
        }
        catch (EdmException e)
        {
        	LogFactory.getLog("eprint").error("", e);
        }
        return null;
    }

    public String getNazwiskoImie()
    {
    	return (this.nazwisko != null ? this.nazwisko : "")+" "+(this.imie != null ? this.imie : "");
    }

	public String getNazwiskoImieWithBackets()
    {
    	return "(" + getNazwiskoImie() + ")";
    }


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getIdPracownika() {
		return idPracownika;
	}

	public void setIdPracownika(String idPracownika) {

		this.idPracownika = idPracownika;
	}

	public Boolean getPracuje() {
		return pracuje;
	}

	public void setPracuje(Boolean pracuje) throws EdmException{
		if (!pracuje.equals(this.pracuje) && id != null) {
			this.pracuje = pracuje;
			DockindQuery query = new DockindQuery(0, 0);
			DocumentKind dc = DocumentKind.findByCn(DocumentLogicLoader.EMPLOYEE_KIND);
			query.setDocumentKind(dc);
			query.field(dc.getFieldByCn(EmployeeDocumentLogic.PRACOWNIK_FIELD_CN), getId());
			Iterator<Document> it = DocumentKindsManager.search(query);
			while (it.hasNext()) {
				Document doc = it.next();
				dc.logic().archiveActions(doc, -500);
			}
		} else {
			this.pracuje = pracuje;
		}
	}

	public Integer getInstytucja() {
		return instytucja;
	}

	public void setInstytucja(Integer instytucja) throws EdmException
	{
		if (instytucja != null && !instytucja.equals(this.instytucja) && id != null)
		{
			this.instytucja = instytucja;
			DockindQuery query = new DockindQuery(0, 0);
			DocumentKind dc = DocumentKind.findByCn(DocumentLogicLoader.EMPLOYEE_KIND);
			query.setDocumentKind(dc);
			query.field(dc.getFieldByCn(EmployeeDocumentLogic.PRACOWNIK_FIELD_CN), getId());
			Iterator<Document> it = DocumentKindsManager.search(query);
			while (it.hasNext())
			{
				Document doc = it.next();
				dc.logic().archiveActions(doc, -500);
			}
		}
		else
		{
			this.instytucja = instytucja;
		}
	}

	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
}
