package pl.compan.docusafe.core.dockinds.dictionary;

import com.google.common.collect.*;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceModeRule;
import pl.compan.docusafe.core.dockinds.acceptances.IntercarsAcceptanceMode;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.ic.IcUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.*;

import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;

/**
 * Centrum kosztowe wybrane dla faktury.
 * Klasa podobna do klas s�ownikowych (jak np. DaaAgent, DfInst), ale u�ywana troch� w inny spos�b, w 
 * zwi�zku z tym wi�kszo�� 'metod s�ownikowych' nie jest tu implementowana.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class CentrumKosztowDlaFaktury extends AbstractDocumentKindDictionary
{
	private final static Logger log = LoggerFactory.getLogger(CentrumKosztowDlaFaktury.class);

	private final static ThreadLocal<NumberFormat> NUMBER_FORMAT;//not thread safe without ThreadLocal

	static {
		NUMBER_FORMAT = new ThreadLocal<NumberFormat>(){
			@Override
			protected NumberFormat initialValue() {
				DecimalFormat nf = (DecimalFormat)DecimalFormat.getInstance(Locale.UK);
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				DecimalFormatSymbols dfs = nf.getDecimalFormatSymbols();
				dfs.setGroupingSeparator('\u00A0');//twarda spacja
				nf.setDecimalFormatSymbols(dfs);
				return nf;
			}
		};
	}

	private static CentrumKosztowDlaFaktury instance;
    private String fullAccountNumber;


    private Long id;
    private Long documentId;
    
    private Integer itemNo;
    private Integer additionalId;
    private Integer inventoryId;
    private Integer groupId;
    private Integer productId;
    private Integer orderId;
    /** id centrum */
    private Integer centrumId;
    
    /** kod centrum (MPK) */
    private String centrumCode;

	/** id centrum (dzia�u), kt�re dokonuje akceptacji zwykle == centrumId */
	private Integer acceptingCentrumId;

    /** numer konta */
    private String accountNumber;
    
    /** kwota cz�stkowa */
    private BigDecimal amount;
    
    /** kwota cz�stkowa wykorzystana */
    private BigDecimal amountUsed;
    
    /** specjalny description dla textarea **/
    private String textdesc;
    
    /**opis konta**/
    private String descriptionAmount;

	//INTERCARS
    /** tryb akceptacji danego dokumentu dla danego mpka, dla nieintercarsowych = null */
    private Integer acceptanceMode;
    /** inicjalizowane metod� initAcceptancesModes(), tylko dla intercarsa */
    private transient Set<IntercarsAcceptanceMode> possibleAcceptanceModes;
	//id FixedAssetsSubgroup
	private Integer subgroupId;
	private Integer customsAgencyId;
	private String storagePlace;
    private Integer locationId;

    /** Id komentarza do przypisanego centrum kosztowego */
    private Integer remarkId;
    private Integer vatTypeId;
    private Integer classTypeId;
    private Integer connectedTypeId;
    private Integer vatTaxTypeId;
    private Integer analyticsId;

    transient private AccountNumber account;

    public static synchronized CentrumKosztowDlaFaktury getInstance() {
        if (instance == null)
            instance = new CentrumKosztowDlaFaktury();
        return instance;
    }

    public static Map<Integer, String> getClassTypes() {
        Map<Integer, String> ret = Maps.newLinkedHashMap();
        ret.put(10, "KUP (S)");
        ret.put( 0, "NKUP (N)");
        return ret;
    }

    public static Map<Integer, String> getClassTypesCns() {
        Map<Integer, String> ret = Maps.newLinkedHashMap();
        ret.put(10, "S");
        ret.put( 0, "N");
        return ret;
    }

    /** czy ostatnia faktura */
    private static final Map<Integer,String> CONNECTED_TYPES = ImmutableMap.<Integer, String>builder()
            .put( 0, "Powi�zane (PO)")
            .put(10, "Niepowi�zane (NP)")
            .build();
    public static Map<Integer, String> getConnectedTypes(){
        return CONNECTED_TYPES;
    }

    private static final Map<Integer,String> CONNECTED_TYPES_CNS = ImmutableMap.<Integer, String>builder()
            .put( 0, "PO")
            .put(10, "NP")
            .build();
    public static Map<Integer, String> getConnectedTypesCns(){
        return CONNECTED_TYPES_CNS;
    }

    public static Map<Integer, String> getVatTypes(){
        Map<Integer, String> ret = Maps.newLinkedHashMap();
        ret.put(20, "100% ca�kowita warto�� VAT do odliczenia");
        ret.put(10, "0% ca�kowita warto�� VAT w koszty");
        ret.put(30, "odliczenie wg. struktury sprzeda�y");
        return ret;
    }

    public static Map<Integer, String> getVatTaxTypes(){
        Map<Integer, String> ret = Maps.newLinkedHashMap();
        ret.put(10, "Bez podatku VAT");
        ret.put(20, "Z naliczeniem podatku VAT (faktura wewn�trzna)");
        return ret;
    }

    private static Map<Integer, String> ANALYTICS = ImmutableMap.of(
            1, "Sprzeda� (01)",
            2, "Magazyn (02)"
    );

    public static Map<Integer,String> getAnalytics() {
        return ANALYTICS;
    }

    private static final Map<Integer, String> ANALYTICS_CS = ImmutableMap.of(
            1, "01",
            2, "02"
    );

    public static Map<Integer,String> getAnalyticCns() {
        return ANALYTICS_CS;
    }

    public String getRefValue() {
    	return "";
    }
    
    /** Opis obiektu na potrzeby operacji s�ownikowej z mechanizmu 'dockind'. */
    public String getDictionaryDescription()
    {
        return getDescription();
    }

    /** zwraca centrum kosztow dla faktury */
    public CentrumKosztowDlaFaktury find(Long id) throws EdmException
    {
        return Finder.find(CentrumKosztowDlaFaktury.class, id);
    }
    
    /** zwraca liste pozycji budzetowych przypisanych do faktury zwiazanych z danym centrum */
    public static List<CentrumKosztowDlaFaktury> find(Long documentId, Integer centrumId) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(CentrumKosztowDlaFaktury.class);
    	
    	crit.add(Restrictions.eq("documentId", documentId));
    	crit.add(Restrictions.eq("centrumId", centrumId));
    	
    	return (List<CentrumKosztowDlaFaktury>) crit.list();
    }
    
    /** sprawdza, czy dana faktura ma przypisane jakiekolwiek pozycje budzetowe.
     * Porownaj z hasCentrum */
    public static boolean existsForDocument(long documentId) throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(CentrumKosztowDlaFaktury.class);
        criteria.add(Expression.eq("documentId", documentId));
        criteria.setMaxResults(1);
        return !criteria.list().isEmpty();
    }
    
    /** zwraca liste wszystkich pozycji budzetowych przypisanych do faktury, sortowane */
    @SuppressWarnings("unchecked")
    public static List<CentrumKosztowDlaFaktury> findByDocumentId(Long documentId) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(CentrumKosztowDlaFaktury.class);
        criteria.add(Expression.eq("documentId", documentId));
        criteria.addOrder(Order.asc("centrumId"));
        return (List<CentrumKosztowDlaFaktury>) criteria.list();
    }

    /** sprawdza, czy dana faktura ma przypisane jakiekolwiek pozycje budzetowe.
     * Porownaj z existsForDocument */
	public static boolean hasCentrum(Long documentId) throws EdmException{
		Criteria criteria = DSApi.context().session().createCriteria(CentrumKosztowDlaFaktury.class);
        criteria.add(Restrictions.eq("documentId", documentId));
		criteria.setMaxResults(1);
		return !criteria.list().isEmpty();
	}

	/** sumuje kolumne 'amount' dla wszystkich pozycji budzetowych faktury */ 
    public static BigDecimal amountSumForDocument(long documentId) throws EdmException {
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            statement = DSApi.context().prepareStatement("SELECT SUM(amount) FROM DSG_CENTRUM_KOSZTOW_FAKTURY WHERE documentId = ?");
            statement.setLong(1, documentId);
            rs = statement.executeQuery();
            rs.next();
            return rs.getBigDecimal(1);
        } catch (SQLException ex) {
            throw new EdmSQLException(ex);
        } finally {
            DSApi.context().closeStatement(statement);
            DbUtils.closeQuietly(rs);
        }
    }
    
    public String getAccountNumber()
    {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber)
    {
        this.accountNumber = accountNumber;
        this.account = null;
    }

    public AccountNumber getAccount() throws EdmException {
        if(account != null){
            return account;
        }
        if(accountNumber != null){
            account = AccountNumber.findByNumber(accountNumber);
        }
        return  account;
    }

	/** @deprecated nale�y korzysta� z getRealAmount() */
	@Deprecated
    public Double getAmount()
    {
        return amount.doubleValue();
    }

	public BigDecimal getRealAmount(){
		return amount;
	}

	/** @deprecated nale�y korzysta� z wersji z BigDecimal */
	@Deprecated
    public void setAmount(Double amount)
    {
        this.amount = new BigDecimal(amount);
		this.amount.setScale(2, RoundingMode.HALF_UP);
    }
	
	public BigDecimal getAmountUsed(){
		return amountUsed;
	}

	public void setAmount(BigDecimal amount){
		this.amount = amount;
	}

	public void setRealAmount(BigDecimal amount){
		this.amount = amount;
	}
	
	public void setAmountUsed(BigDecimal amount){
		this.amountUsed = amount;
	}

    public String getCentrumCode()
    {
        return centrumCode;
    }

    public void setCentrumCode(String centrumCode)
    {
        this.centrumCode = centrumCode;
    }

    public Integer getCentrumId()
    {
        return centrumId;
    }

    public void setCentrumId(Integer centrumId)
    {
        this.centrumId = centrumId;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }        
    
    public Integer getItemNo() {
		return itemNo;
	}

	public void setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
	}

	public String getDescription() {
        try {
            return centrumCode + ((accountNumber!=null)?" (" + accountNumber + ") ":" (brak konta) ") + NUMBER_FORMAT.get().format(amount) + getLocationString(" ");
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return "(b��d danych)";
        }
    }

    public int getDescriptionRows(){
        return (vatTypeId == null ? 1 : 3)
                + (remarkId == null ? 0 : 1)
                + (vatTaxTypeId == null ? 0 : 1);
    }

    private void appendIcString(StringBuilder sb) throws EdmException {
        sb.append(getLocationString(" // "))
                .append(getConnectedTypeString())
                .append(getClassTypeString())
                .append(getVatTypeString())
                .append(getVatTaxTypeString())
                .append(getRemarkString());
    }

    private String getClassTypeString() {
        return classTypeId == null ? "" : " // " + getClassTypesCns().get(classTypeId);
    }

    private String getConnectedTypeString() {
        return connectedTypeId == null ? "" : " // " + getConnectedTypesCns().get(connectedTypeId);
    }

    private String getVatTypeString(){
        return vatTypeId == null ?  "" : " // " + getVatTypes().get(vatTypeId);
    }

    private String getVatTaxTypeString(){
        return vatTaxTypeId == null ?  "" : " // " + getVatTaxTypes().get(vatTaxTypeId);
    }

    private String getRemarkString() throws EdmException {
        return remarkId == null ?  "" : " // " + AccountNumberComment.find(remarkId).getName();
    }

    public String getTextdesc() throws EdmException {
    	String ret = "";
    	try {
    		DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
//    		DSApi.context().begin();
    		
    		CentrumKosztow ck = CentrumKosztow.find(this.centrumId);    		    		
    		ret += ck.getSymbol()+" "+ck.getName()+"\n";
    		
    		AccountNumber an = AccountNumber.findByNumber(this.accountNumber);
			
    		if(an != null){
    			ret += an.getNumber()+" ";
    			ret += ( an.getName() != null ? an.getName() : "");
			} else {
    			ret += "(brak konta)";
			}

    		if(descriptionAmount != null){
    			ret += "("+descriptionAmount+")";
    		}
    		ret += "\n" + NUMBER_FORMAT.get().format(amount);

//    		DSApi.context().commit();
    	} catch(Exception e) {
			ret = ""+e;
			log.error(e.getMessage(), e);
		} finally {
			DSApi.close();
		}
    		
    	return ret;
    }
    
    public String getText() throws EdmException
    {
    	StringBuilder ret = new StringBuilder(32);
    	CentrumKosztow ck = CentrumKosztow.find(this.centrumId);    		    		
		ret.append(ck.getSymbol()).append(" ").append(ck.getName()).append(" // ");
		
		AccountNumber an = AccountNumber.findByNumber(this.accountNumber);
		if(an != null){
			ret.append(an.getNumber()).append(" ").append(an.getName()).append(" // ");
		} else {
			ret.append(" (brak konta) // ");
		}

		if(!StringUtils.isEmpty(storagePlace)){
			ret.append(storagePlace).append(" // ");
		}

		if(descriptionAmount != null){
			ret.append(" (").append(descriptionAmount).append(") ").append(" // ");
		}
        //log.info("IC account = {}", IcUtils.buildAccountNumberCode(this));
		ret.append(NUMBER_FORMAT.get().format(amount));
        appendIcString(ret);
		return ret.toString();
    }
    
	public String getTextToJsp() throws EdmException
	{

		String ret = "";
		try
		{
			DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
			ret = getText();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.close();
		}
		return ret;
	}

    public void setTextdesc(String textdesc)
    {
    	this.textdesc = textdesc;
    }

	public String getDescriptionAmount() {
		return descriptionAmount;
	}

	public void setDescriptionAmount(String descriptionAmount) {
		this.descriptionAmount = descriptionAmount;
	}
	public void setAcceptanceMode(Integer acceptanceMode) {
		this.acceptanceMode = acceptanceMode;
	}
	public Integer getAcceptanceMode() {
		return acceptanceMode;
	}
	
	IntercarsAcceptanceMode mode = null;
	
	public IntercarsAcceptanceMode getAcceptanceModeInfo(){
		if (mode == null && acceptanceMode != null) {
			mode = IntercarsAcceptanceMode.values()[acceptanceMode];
		}
		return mode;
	}

	/** sprawdza, czy konto ma flage Ic3098 */
    private boolean isIC3098(){
        try {
            return getAccount() != null && getAccount().isIc3098Flag();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return false;
        }
    }

    /**
     * Zwraca wymagane cn akceptacji (zar�wno wykonane jak i nie) dla tego centrum koszt�w dla faktury
     * @return zbi�r string�w nigdy null
     */
    public Set<String> getAcceptanceCnsNeeded(){
        if(isIC3098()){
            return Sets.difference(getAcceptanceModeInfo().getCNs(), Collections.singleton("dyrektora_fin"));
        } else {
            return getAcceptanceModeInfo().getCNs();
        }
    }

    /**
     * Zwraca list� akceptacji w kolejno�ci wykonywania dla tego centrum kosztowego
     * @return
     */
    public List<String> getAcceptanceCnsList(){
        if(isIC3098()){
            return Lists.newArrayList(
                    Collections2.filter(getAcceptanceModeInfo().getCnList(),
                                 not(equalTo("dyrektora_fin"))));
        } else {
            return getAcceptanceModeInfo().getCnList();
        }
    }
	
	public void initAcceptancesModes() throws EdmException {
		this.possibleAcceptanceModes = EnumSet.copyOf(CentrumKosztow.find(this.getAcceptingCentrumId()).getAvailableAcceptanceModes());
        for(IntercarsAcceptanceMode mode : possibleAcceptanceModes){
            AcceptanceModeRule rule = AcceptanceModeRule.get(this.centrumId, mode.getId());
            if(rule != null && !rule.isModeAvailable(this)){
                possibleAcceptanceModes.remove(mode);
            }
        }
	}

	public Collection<IntercarsAcceptanceMode> getPossibleAcceptanceModes() {
		List<IntercarsAcceptanceMode> ret = new ArrayList<IntercarsAcceptanceMode>(possibleAcceptanceModes);
		Collections.reverse(ret);
		return ret;
	}

	/**
	 * Centrum, kt�re dokonuje akceptacji dla tego ckdf
	 * @return the acceptingCentrumId
	 */
	public Integer getAcceptingCentrumId() {
		if(acceptingCentrumId!= null){
			return acceptingCentrumId;
		} else {
			return centrumId;
		}
	}

	public Integer getRealAcceptingCentrumId() {
		return acceptingCentrumId;
	}
	

	public void setAcceptingCentrumId(Integer acceptingCentrumId) {
		this.acceptingCentrumId = acceptingCentrumId;
	}


	public Integer getSubgroupId() {
		return subgroupId;
	}


	public void setSubgroupId(Integer subgroupId) {
		this.subgroupId = subgroupId;
	}


	public String getStoragePlace() {
		return storagePlace;
	}


	public void setStoragePlace(String storagePlace) {
		this.storagePlace = storagePlace;
	}


	public Integer getCustomsAgencyId() {
		return customsAgencyId;
	}


	public void setCustomsAgencyId(Integer customsAgencyId) {
		this.customsAgencyId = customsAgencyId;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	@Override
	public void create() throws EdmException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void delete() throws EdmException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String dictionaryAccept() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Map<String, String> dictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
	public String dictionaryAction() {
		// TODO Auto-generated method stub
		return null;
	}


    /**
     * Zwraca CN lokalizacji je�li jest ona wybrana, w przeciwnym przypadku zwraca ""
     */
    public String getLocationString(String prefix){
        if(this.locationId != null){
            try{
                return prefix + DataBaseEnumField.getEnumItemForTable("dsg_enum2",locationId).getCn();
            } catch (Exception ex) {
                log.warn(ex.getMessage() ,ex);
                return "";
            }
        } else {
            return "";
        }
    }

    public Map<String, String> popUpDictionaryAttributes()
    {
    	return null;
    }

    public Integer getClassTypeId() {
        return classTypeId;
    }

    public void setClassTypeId(Integer classTypeId) {
        this.classTypeId = classTypeId;
    }

    public Integer getConnectedTypeId() {
        return connectedTypeId;
    }

    public void setConnectedTypeId(Integer connectedTypeId) {
        this.connectedTypeId = connectedTypeId;
    }

    public Integer getRemarkId() {
        return remarkId;
    }

    public void setRemarkId(Integer remarkId) {
        this.remarkId = remarkId;
    }

    public Integer getVatTypeId() {
        return vatTypeId;
    }

    public void setVatTypeId(Integer vatTypeId) {
        this.vatTypeId = vatTypeId;
    }

    public Integer getVatTaxTypeId() {
        return vatTaxTypeId;
    }

    public void setVatTaxTypeId(Integer vatTaxTypeId) {
        this.vatTaxTypeId = vatTaxTypeId;
    }

    public void setAnalyticsId(Integer analyticsId) {
        this.analyticsId = analyticsId;
    }

    public Integer getAnalyticsId() {
        return analyticsId;
    }

    /**  Metoda zwraca zbudowany numer konta kosztowego, mo�e go zbudowa� gdy b�dzie to konieczne  */
    public String getFullAccountNumber() throws EdmException{
        if(fullAccountNumber == null){
            fullAccountNumber = IcUtils.buildAccountNumberCode(this);
        }
        return fullAccountNumber;
    }
    
    /** kopiuje do siebie wartosci z podanego centrum */
    public void copyValueFrom (CentrumKosztowDlaFaktury source) {
    	//this.setAccountNumber(source.getFullAccountNumber());
		this.setDocumentId(source.getDocumentId());
		this.setCentrumId(source.getCentrumId());
		this.setCentrumCode(source.getCentrumCode());
		this.setAcceptingCentrumId(source.getRealAcceptingCentrumId());
		this.setAccountNumber(source.getAccountNumber());
		this.setAmount(source.getRealAmount());
		this.setAmountUsed(source.getAmountUsed());
		//this.setTextdesc(source.getTextdesc());
		this.setDescriptionAmount(source.getDescriptionAmount());
		this.setAcceptanceMode(source.getAcceptanceMode());
		this.setSubgroupId(source.getSubgroupId());
		this.setCustomsAgencyId(source.getCustomsAgencyId());
		this.setStoragePlace(source.getStoragePlace());
		this.setLocationId(source.getLocationId());
		this.setRemarkId(source.getRemarkId());
		this.setVatTypeId(source.getVatTypeId());
		this.setClassTypeId(source.getClassTypeId());
		this.setConnectedTypeId(source.getConnectedTypeId());
		this.setVatTypeId(source.getVatTaxTypeId());
		this.setAnalyticsId(source.getAnalyticsId());
    }

	public Integer getAdditionalId() {
		return additionalId;
	}

	public void setAdditionalId(Integer additionalId) {
		this.additionalId = additionalId;
	}

	public Integer getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

}
