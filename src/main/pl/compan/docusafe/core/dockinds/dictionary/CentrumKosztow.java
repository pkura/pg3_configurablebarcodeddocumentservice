package pl.compan.docusafe.core.dockinds.dictionary;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;


import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.IntercarsAcceptanceMode;

/**
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class CentrumKosztow implements EnumItem
{
	public final static String SPECIAL_INVOICE_CODE = "K/CN";

    private Integer id;
    private String name;
    private String symbol;
    /** w przypadku IC kod safo */
    private String symbol2;

    private boolean available = true;

    private boolean autoAdded = false;
    private Integer defaultAccountNumber;
    private Integer defaultLocationId;

    /** (IC) czy faktury z dzia�em dla danego centrum musz� mie� wype�nion� nazw� umowy */
    private boolean contractNeeded = false;
    
    private Boolean defaultSimpleAcceptance;
    //private Boolean needdescription;
    
    /** mozliwe tryby akceptacji dla danego centrum, null w przypadku nie intercarsowych*/
    private Long acceptanceModes;

	/** pisma z tego centrum mog� mie� dodane empeki wszystkich innych centr�w ale id� �cie�kami tego centrum (nie ja to wymy�li�em)*/
	private Boolean intercarsPowered;

    /** czy domyslnie ustawiona jest akceptacja uproszczona */
    public boolean getDefaultSimpleAcceptance() {
		return defaultSimpleAcceptance!=null && defaultSimpleAcceptance;
	}
    
	public void setDefaultSimpleAcceptance(Boolean defaultSimpleAcceptance) {
		this.defaultSimpleAcceptance = defaultSimpleAcceptance;
	}
	
	public String getRefValue()
    {
    	return "";
    }
    public static CentrumKosztow find(Integer id) throws EdmException
    {
        return Finder.find(CentrumKosztow.class, id);
    }

    /**
     * Zwraca list� zawieraj�c� wszystkie widoczne (nie ukryte) centra, lista jest posortowana wg nazw.
     * @return list zawieraj�ca wszystkie widoczne centra, nigdy nie zwraca null'a
     * @throws EdmException
     */
    public static List<CentrumKosztow> list() throws EdmException
    {
        return DSApi.context().session().createCriteria(CentrumKosztow.class)
			.add(Restrictions.eq("available", true))
            .addOrder(Order.asc("name"))
			.list();
    }

    /**
     * Zwraca list� zawieraj�c� wszystkie centra, lista jest posortowana wg nazw.
     * @return list zawieraj�ca wszystkie centra, nigdy nie zwraca null'a
     * @throws EdmException
     */
    public static List<CentrumKosztow> listAll() throws EdmException
    {
        return DSApi.context().session().createCriteria(CentrumKosztow.class)
            .addOrder(Order.asc("name"))
			.list();
    }

    /**
     * Zwraca list� zawieraj�c� wszystkie centra, lista jest posortowana wg warto�ci o przekazanej nazwie.
     * @return list zawieraj�ca wszystkie centra, nigdy nie zwraca null'a
     * @throws EdmException
     */
    public static List<CentrumKosztow> listBy(String option) throws EdmException
    {
         return DSApi.context().session().createCriteria(CentrumKosztow.class)
            .add(Restrictions.eq("available", true))
            .addOrder(Order.asc(option))
			.list();
    }
    
    public static List<CentrumKosztow> listAllBy(String option) throws EdmException
    {
         return DSApi.context().session().createCriteria(CentrumKosztow.class)
            //.add(Restrictions.eq("available", true))
            .addOrder(Order.asc(option))
			.list();
    }
    
    /**Zwraca liste centrow jaka moze akcepotowac zalogowany uzytkownik*/
    public static List<CentrumKosztow> findCentrum() throws Exception, SQLException, EdmException
    {
    	List<Integer> accList = AcceptanceCondition.findByUserGuids(DSApi.context().getPrincipalName());
    	List<CentrumKosztow> centra = new ArrayList<CentrumKosztow>();
    	for (Integer id : accList){
    		 centra.add(CentrumKosztow.find(id));
		}
		return centra;
    }
    
    public static CentrumKosztow findBySymbol(String symbol) throws EdmException
    {
        List<CentrumKosztow> list = DSApi.context().session().createCriteria(CentrumKosztow.class)
            .add(Restrictions.eq("symbol",symbol))
			.list();
        if(list.isEmpty()){
            throw new IllegalArgumentException("Nie znaleziono centrum '" + symbol + "'");
        }
        return list.get(0);
    }

    public static CentrumKosztow findBySymbolInAll(String symbol) throws EdmException
    {
    	CentrumKosztow tmp = null;
    	for(int i=0;i<CentrumKosztow.listAll().size();i++) {
    		if(CentrumKosztow.listAll().get(i).getSymbol().equalsIgnoreCase(symbol))
    			tmp = CentrumKosztow.listAll().get(i);
    	} 
    	return tmp;
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSymbol()
    {
        return symbol;
    }

    public void setSymbol(String symbol)
    {
        this.symbol = symbol;
    }         

    public void setId(Integer id)
    {
        this.id = id;
    }
    
    // METODY Z INTERFEJSU EnumItem    
    public Integer getId()
    {
        return id;
    }
    
    public String getCn()
    {
        return symbol;
    }
    
    public String getTitle()
    {
        return name;
    }
    
    public String getSearchTitle()
    {
    	return symbol+" - "+name;
    }
    
    public String getArg(int position)
    {
        return null;
    }
    
    public Object getTmp()
    {
        return null;
    }
    
    public void setTmp(Object tmp)
    {  
        
    }
    
    public boolean isForceRemark()
    {
        return false;
    }

	public Integer getCentrum() {
		return null;
	}
	
	public Boolean isEmpty() throws EdmException 
	{
		DockindQuery q;
		List<DocumentKind> dockinds = DocumentKind.list(true); //DocumentKind.list();
		
		for (DocumentKind dc : dockinds) 
		{
			dc.initialize();
			if (dc.getAcceptancesDefinition() != null)
			{
				q = new DockindQuery(1,1);
				q.setDocumentKind(dc);
				q.centrumKosztowField(null, this.id);
				SearchResults<Document> results = DocumentKindsManager.search(q);
				if (results.count() > 0) 
				{
					return false;
				}
			}
		}
		return true;
	}

	void setAcceptanceModes(Long acceptanceModes) {
		this.acceptanceModes = acceptanceModes;
	}

	Long getAcceptanceModes() {
		return acceptanceModes;
	}
	/***************************************************************************
	 * Wyliczanie dost�pnego zbioru tryb�w akceptacji (intercars)
	 **************************************************************************/
	
	Set<IntercarsAcceptanceMode> getAvailableAcceptanceModesCache;
	public Set<IntercarsAcceptanceMode> getAvailableAcceptanceModes(){
		if(getAvailableAcceptanceModesCache == null){
			getAvailableAcceptanceModesCache = createModesSet();
		}
		return getAvailableAcceptanceModesCache;		
	}
	
	private Set<IntercarsAcceptanceMode> createModesSet(){
		if(acceptanceModes == null){
			return Collections.emptySet();
		}
		Set<IntercarsAcceptanceMode> ret = EnumSet.noneOf(IntercarsAcceptanceMode.class);
		long temp = acceptanceModes;
		for(int i = 0; i < IntercarsAcceptanceMode.values().length; ++i){
			if(temp % 2 == 1){
				ret.add(IntercarsAcceptanceMode.values()[i]);
			}
			temp >>= 1;
		}
		
		return ret;
	}
	
	public void setAvailableAcceptanceModes(Set<IntercarsAcceptanceMode> modes){
		long newModes = 0;
		for(IntercarsAcceptanceMode mode: modes){
			newModes += (1 << mode.ordinal());
		}
		acceptanceModes = newModes;
		getAvailableAcceptanceModesCache = null;
	}

	public boolean isIntercarsPowered() {
		return intercarsPowered == Boolean.TRUE;
	}

	public void setIntercarsPowered(boolean intercarsPower) {
		this.intercarsPowered = intercarsPower;
	}

	public String getFieldCn() {
		// TODO HELLOO
		return null;
	}

    public Integer getDefaultAccountNumber() {
        return defaultAccountNumber;
    }

    public void setDefaultAccountNumber(Integer defaultAccountNumber) {
        this.defaultAccountNumber = defaultAccountNumber;
    }

    public boolean isAutoAdded() {
        return autoAdded;
    }

    public void setAutoAdded(boolean autoAdded) {
        this.autoAdded = autoAdded;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

	/**
	 * @return the defaultLocationId
	 */
	public Integer getDefaultLocationId() {
		return defaultLocationId;
	}

	/**
	 * @param defaultLocationId the defaultLocationId to set
	 */
	public void setDefaultLocationId(Integer defaultLocationId) {
		this.defaultLocationId = defaultLocationId;
	}

    public boolean isContractNeeded() {
        return contractNeeded;
    }

    public void setContractNeeded(boolean contractNeeded){
        this.contractNeeded = contractNeeded;
    }

    public String getSymbol2() {
        return symbol2;
    }

    public void setSymbol2(String symbol2) {
        this.symbol2 = symbol2;
    }
}
