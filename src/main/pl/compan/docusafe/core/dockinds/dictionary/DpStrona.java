package pl.compan.docusafe.core.dockinds.dictionary;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.parametrization.presale.DJPersonDictionary;
import pl.compan.docusafe.util.QueryForm;
import org.hibernate.HibernateException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.util.*;

/**
 * S�ownik stron dla dokument�w prawnych.
 * 
 * @author Piotr Komisarski mailto:komisarskip@student.mini.pw.edu.pl
 */
public class DpStrona extends AbstractDocumentKindDictionary
{
    private static final Logger log = LoggerFactory.getLogger(DpStrona.class);
    
    //@IMPORT/EXPORT
    private Long id;
    private String imie;
    private String nazwisko;
    private String sygnatura;
    private String ulica;
    private String kod;
    private String miejscowosc;
    private String telefon;
    private String faks;
    private String email;
    private String kraj;
	private static DpStrona instance;
	public static synchronized DpStrona getInstance()
	{
		if (instance == null)
			instance = new DpStrona();
		return instance;
	}
    public DpStrona()
    {
        
    }
    
    public String dictionaryAction()
    {
        return "/office/common/strona.action";
    }
    
    /**
     * Zwraca stringa zawieraj�cego nazwe funkcji (najlepiej umieszczonej w pliku dockind.js)
     *  kt�ra wykona si� w funkcji accept_cn
     * 
     */
    public String dictionaryAccept()
    {
        return "";              
    }

    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        map.put("imie", "Imi�");
        map.put("nazwisko", "Nazwisko");
        map.put("sygnatura", "Sygnatura");

        return map;
    }
    
    public String getDictionaryDescription()
    {
        return imie + " " + nazwisko + " " + sygnatura;
    }    

    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania strony dokumentu prawnego. "+e.getMessage());
            throw new EdmException("Blad dodania strony dokumentu prawnego. "+e.getMessage());
        }
    }
    
    /**
     * Usuwa strone
     * @throws EdmException
     */
    public void delete() throws EdmException 
    {
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia strony dokumentu prawnego");
            throw new EdmException("Blad usuniecia strony dokumentu prawnego. "+e.getMessage());
        }
    }

    /**
     * Wyszukuje konkretna strone dokumentu prawnego
     * @param id
     * @return
     * @throws EdmException
     */
    public DpStrona find(Long id) throws EdmException
    {
        DpStrona strona = DSApi.getObject(DpStrona.class, id);
        if (strona == null) 
            throw new EdmException("Nie znaleziono strony o nr " + id);
        else 
            return strona;
    }

    @SuppressWarnings(value={"unchecked"})
    /**
     * Wyszukuje liste dostepnych agencji archiwum agenta
     * @return
     * @throws EdmException
     */
    public static List<DaaAgencja> findAll() throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(DaaAgencja.class);
        if(c == null) throw new EdmException("Nie mozna okreslic kryterium wyszukiwania agencji archiwum agenta");
        try {
            log.info("pobranie listy agencji");
            return (List<DaaAgencja>)c.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
     
    @SuppressWarnings(value={"unchecked"})
    public static List<DpStrona> find(Map<String, Object> parametry) throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(DpStrona.class);
        if(c == null) 
            throw new EdmException("Nie mozna okreslic kryterium wyszukiwania strony dokumentu prawnego");
        if(parametry != null) 
        {
            if(parametry.get("imie") != null) 
                c.add(Expression.like("imie",  "%"+parametry.get("imie")+"%"));
            if(parametry.get("nazwisko") != null) 
                c.add(Expression.like("nazwisko", "%" + parametry.get("nazwisko") + "%"));
            if(parametry.get("sygnatura") != null) 
                c.add(Expression.like("sygnatura", "%"+parametry.get("sygnatura")+"%"));
            if(parametry.get("ulica") != null) 
                c.add(Expression.like("ulica", "%" + parametry.get("ulica") + "%"));
            if(parametry.get("kod") != null) 
                c.add(Expression.like("kod", "%" + parametry.get("kod") + "%"));
            if(parametry.get("miejscowosc") != null) 
                c.add(Expression.like("miejscowosc", "%" + parametry.get("miejscowosc") + "%"));
            if(parametry.get("telefon") != null) 
                c.add(Expression.like("telefon", "%" + parametry.get("telefon") + "%"));
            if(parametry.get("faks") != null) 
                c.add(Expression.like("faks", "%" + parametry.get("faks") + "%"));
            if(parametry.get("email") != null) 
                c.add(Expression.like("email", "%" + parametry.get("email") + "%"));
            if(parametry.get("kraj") != null) 
                c.add(Expression.like("kraj", "%" + parametry.get("kraj") + "%"));
        }
        try 
        {
            log.info("pobranie listy stron");
            return (List<DpStrona>)c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }    
    
    @SuppressWarnings(value={"unchecked"})
    /**
     * Znajduje obiekty DpStrona na podstawie formularza wyszukiwania.
     * @param form
     * @return
     * @throws EdmException
     */
    public static SearchResults<? extends DpStrona> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DpStrona.class);

            if (form.hasProperty("imie"))
                c.add(Expression.like("imie", "%"+form.getProperty("imie")+"%"));
            if (form.hasProperty("nazwisko"))
                c.add(Expression.like("nazwisko", "%"+form.getProperty("nazwisko")+"%"));
            if (form.hasProperty("sygnatura"))
                c.add(Expression.like("sygnatura", "%" + form.getProperty("sygnatura") + "%"));
            if (form.hasProperty("ulica"))
                c.add(Expression.like("ulica", "%" + form.getProperty("ulica") + "%"));
            if (form.hasProperty("kod"))
                c.add(Expression.like("kod", "%" + form.getProperty("kod") + "%"));
            if (form.hasProperty("miejscowosc"))
                c.add(Expression.like("miejscowosc", "%" + form.getProperty("miejscowosc") + "%"));
            if (form.hasProperty("telefon"))
                c.add(Expression.like("telefon", "%" + form.getProperty("telefon") + "%"));
            if (form.hasProperty("faks"))
                c.add(Expression.like("faks", "%" + form.getProperty("faks") + "%"));
            if (form.hasProperty("email"))
                c.add(Expression.like("email", "%" + form.getProperty("email") + "%"));
            if (form.hasProperty("kraj"))
                c.add(Expression.like("kraj", "%" + form.getProperty("kraj") + "%"));

            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }

            List<DpStrona> list = (List<DpStrona>) c.list();

            if (list.size() == 0)
                return SearchResultsAdapter.emptyResults(DpStrona.class);
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                int toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();
                 
                return new SearchResultsAdapter<DpStrona>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), DpStrona.class);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFaks()
    {
        return faks;
    }

    public void setFaks(String faks)
    {
        this.faks = faks;
    }

    public Long getId() 
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getImie()
    {
        return imie;
    }

    public void setImie(String imie) 
    {
        this.imie = imie;
    }

    public String getKod()
    {
        return kod;
    }

    public void setKod(String kod)
    {
        this.kod = kod;
    }

    public String getKraj()
    {
        return kraj;
    }

    public void setKraj(String kraj)
    {
        this.kraj = kraj;
    }

    public String getMiejscowosc()
    {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc)
    {
        this.miejscowosc = miejscowosc;
    }

    public String getNazwisko()
    {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko)
    {
        this.nazwisko = nazwisko;
    }

    public String getSygnatura()
    {
        return sygnatura;
    }

    public void setSygnatura(String sygnatura) 
    {
        this.sygnatura = sygnatura;
    }

    public String getTelefon()
    {
        return telefon;
    }

    public void setTelefon(String telefon) 
    {
        this.telefon = telefon;
    }

    public String getUlica() 
    {
        return ulica;
    }

    public void setUlica(String ulica) 
    {
        this.ulica = ulica;
    }
	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
}
