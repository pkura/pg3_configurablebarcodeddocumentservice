package pl.compan.docusafe.core.dockinds.dictionary;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindQuery;

public abstract class AbstractDocumentKindDictionary implements DocumentKindDictionary, Serializable
{
	public abstract void create() throws EdmException;

	public abstract void delete() throws EdmException ; 

	public abstract String getDictionaryDescription();
	
	public abstract DocumentKindDictionary find(Long key) throws EdmException;

//	public abstract List<Map<String,String>> search(Object[] args) throws EdmException; 
	
//	public abstract List<String> additionalSearchParam();
	
	public abstract String dictionaryAccept();

	public abstract Map<String, String> dictionaryAttributes();
	
	public abstract Map<String, String> popUpDictionaryAttributes();
	
	public List<String> additionalSearchParam() {
		return null;
	}
	
	public List<Map<String, String>> search(Object[] args) throws EdmException {
		return null;
	}
	
	public Map<String, String> promptDictionaryAttributes() {
        return null;
    }
	
	public boolean isBoxDescriptionAvailable()
	{
		return false;
	}
	
	public String getBoxDescription()
	{
		return getDictionaryDescription();
	}
	
	public boolean isAjaxAvailable() {
		return false;
	}
	
	public Integer getHeight()
	{
		return 700;
	}
	
	public Integer getWidth()
	{
		return 650;
	}
	
	/**
	 * Sprawdza czy slownik nadpisuje utworzenie zaptania dla tego pola
	 */
    public boolean isOverloadQuery(String property)
    {
    	return false;
    }
    
    
    /**
     * Dodaje warunek do zapytania 
     */
    public void addOverloadQuery(String property, String value, DockindQuery query)
    {
    	
    }
    
    public String dictionarySortColumn()
    {
        return "brak";
    }

}
