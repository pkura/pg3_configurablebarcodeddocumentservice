package pl.compan.docusafe.core.dockinds.dictionary;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;

/**
 * @author Piotr Komisarski mailto:komisarskip@student.mini.pw.edu.pl
 * 
 * Jest to slownik instytucji/kontrahent�w dla archiwum dokument�w finansowych
 * Klasa DpInst te� jest slownikiem instytucji/kontrahent�w, ale dla archiwum dokument�w prawnych
 * R�nice s� bardzo drobne. W przyszlosci jest mozliwe ujednolicenie ich. W DpInst trzebaby dodac dwa pola.
 */
public class DfInst extends AbstractDocumentKindDictionary
{
    private static final Logger log = LoggerFactory.getLogger(DfInst.class);
    
    //@IMPORT/EXPORT
    private Long id;
    private String imie;
    private String nazwisko;
    private String name;
    private String oldname;
    private String nip;
    private String regon;
    private String ulica;
    private String kod;
    private String miejscowosc;
    private String telefon;
    private String faks;
    private String email;
    private String kraj;

    public DfInst()
    {
        
    }
    
    public String dictionaryAction()
    {
        return "/office/common/dfinst.action";
    }
    
    /**
     * Zwraca stringa zawieraj�cego nazwe funkcji (najlepiej umieszczonej w pliku dockind.js)
     *  kt�ra wykona si� w funkcji accept_cn
     * 
     */
    public String dictionaryAccept()
    {
        return "";              
    }

    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        try
        {
        	if(AvailabilityManager.isAvailable("BiskDfInst"))
        	{
        		map.put("name", "Nazwa");
		        map.put("imie","Imie");
		        map.put("nazwisko","Nazwisko");
        	}
        	else if(AvailabilityManager.isAvailable("dfinst.kancelaria"))
	        {
	        	map.put("name", " ");
	        }
	        else
	        {
		        map.put("name", "Nazwa instytucji/kontrahenta");
		        map.put("nip","NIP");
	        }
        }
        catch (Exception e) 
        {
        	map.clear();
        	map.put("name", "Nazwa instytucji/kontrahenta");
	        map.put("nip","NIP");
		}
        return map;
    }
    
    public String getDictionaryDescription()
    {
        return name + " " + nip;
    }    

    public String dictionarySortColumn()
    {
        return "name";
    }  
    
    /**
     * Zapisuje kontrahenta w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania instytucji/kontrahenta dokumentu finansowego. "+e.getMessage());
            throw new EdmException("Blad dodania instytucji/kontrahenta dokumentu finansowego. "+e.getMessage());
        }
    }
    
    /**
     * Usuwa kontrahenta
     * @throws EdmException
     */
    public void delete() throws EdmException 
    {
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia instytucji/kontrahenta dokumentu prawnego");
            throw new EdmException("Blad usuniecia instytucji/kontrahenta dokumentu prawnego. "+e.getMessage());
        }
    }

    /**
     * Wyszukuje konkretna instytucje/kontrahenta dokumentu finansowego
     * @param id
     * @return
     * @throws EdmException
     */
    public DfInst find(Long id) throws EdmException
    {
        DfInst inst = DSApi.getObject(DfInst.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono instytucji/kontrahenta o nr " + id);
        else 
            return inst;
    }

    @SuppressWarnings(value={"unchecked"})
    public static List<DfInst> find(Map<String, Object> parametry) throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(DfInst.class);
        if(c == null) 
            throw new EdmException("Nie mozna okreslic kryterium wyszukiwania instytucji/kontrahenta dokumentu finansowego");
        if(parametry != null) 
        {
            if(parametry.get("imie") != null) 
                c.add(Restrictions.like("imie",  "%"+parametry.get("imie")+"%"));
            if(parametry.get("nazwisko") != null) 
                c.add(Restrictions.like("nazwisko", "%" + parametry.get("nazwisko") + "%"));
            if(parametry.get("name") != null) 
                c.add(Restrictions.like("name", "%"+parametry.get("name")+"%"));
            if(parametry.get("oldname") != null) 
                c.add(Restrictions.like("oldname", "%"+parametry.get("oldname")+"%"));
            if(parametry.get("nip") != null) 
                c.add(Restrictions.like("nip", "%"+parametry.get("nip")+"%"));
            if(parametry.get("regon") != null) 
                c.add(Restrictions.like("regon", "%"+parametry.get("regon")+"%"));
            if(parametry.get("ulica") != null) 
                c.add(Restrictions.like("ulica", "%" + parametry.get("ulica") + "%"));
            if(parametry.get("kod") != null) 
                c.add(Restrictions.like("kod", "%" + parametry.get("kod") + "%"));
            if(parametry.get("miejscowosc") != null) 
                c.add(Restrictions.like("miejscowosc", "%" + parametry.get("miejscowosc") + "%"));
            if(parametry.get("telefon") != null) 
                c.add(Restrictions.like("telefon", "%" + parametry.get("telefon") + "%"));
            if(parametry.get("faks") != null) 
                c.add(Restrictions.like("faks", "%" + parametry.get("faks") + "%"));
            if(parametry.get("email") != null) 
                c.add(Restrictions.like("email", "%" + parametry.get("email") + "%"));
            if(parametry.get("kraj") != null) 
                c.add(Restrictions.like("kraj", "%" + parametry.get("kraj") + "%"));
            c.setMaxResults(200);
        }
        try 
        {
            log.info("pobranie listy instytucji/kontrahent�w (df)");
            return (List<DfInst>)c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }    
    
    @SuppressWarnings(value={"unchecked"})
    /**
     * Znajduje obiekty DfInst na podstawie formularza wyszukiwania.
     * @param form
     * @return
     * @throws EdmException
     */
    public static SearchResults<? extends DfInst> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DfInst.class);

            if (form.hasProperty("imie"))
                c.add(Restrictions.like("imie", "%"+form.getProperty("imie")+"%"));
            if (form.hasProperty("nazwisko"))
                c.add(Restrictions.like("nazwisko", "%"+form.getProperty("nazwisko")+"%"));
            if (form.hasProperty("name"))
                c.add(Restrictions.like("name", "%" + form.getProperty("name") + "%"));
            if (form.hasProperty("oldname"))
                c.add(Restrictions.like("oldname", "%" + form.getProperty("oldname") + "%"));
            if (form.hasProperty("nip"))
                c.add(Restrictions.like("nip", "%" +  form.getProperty("nip")+ "%"));
            if (form.hasProperty("regfon"))
                c.add(Restrictions.like("regon", "%" +  form.getProperty("regon")+ "%"));
            if (form.hasProperty("ulica"))
                c.add(Restrictions.like("ulica", "%" + form.getProperty("ulica") + "%"));
            if (form.hasProperty("kod"))
                c.add(Restrictions.like("kod", "%" + form.getProperty("kod") + "%"));
            if (form.hasProperty("miejscowosc"))
                c.add(Restrictions.like("miejscowosc", "%" + form.getProperty("miejscowosc")+ "%"));
            if (form.hasProperty("telefon"))
                c.add(Restrictions.like("telefon", "%" + form.getProperty("telefon") + "%"));
            if (form.hasProperty("faks"))
                c.add(Restrictions.like("faks", "%" + form.getProperty("faks") + "%"));
            if (form.hasProperty("email"))
                c.add(Restrictions.like("email","%" +  form.getProperty("email") + "%"));
            if (form.hasProperty("kraj"))
                c.add(Restrictions.like("kraj", "%" + form.getProperty("kraj")+ "%"));

            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }

            List<DfInst> list = (List<DfInst>) c.list();

            if (list.size() == 0)
                return SearchResultsAdapter.emptyResults(DfInst.class);
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                int toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();
                 
                return new SearchResultsAdapter<DfInst>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), DfInst.class);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getFaks()
    {
        return faks;
    }

    public void setFaks(String faks) 
    {
        this.faks = faks;
    }

    public Long getId() 
    {
        return id;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public String getImie() 
    {
        return imie;
    }

    public void setImie(String imie) 
    {
        this.imie = imie;
    }

    public String getKod()
    {
        return kod;
    }

    public void setKod(String kod)
    {
        this.kod = kod;
    }

    public String getKraj()
    {
        return kraj;
    }

    public void setKraj(String kraj)
    {
        this.kraj = kraj;
    }

    public String getMiejscowosc() 
    {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc)
    {
        this.miejscowosc = miejscowosc;
    }

    public String getNazwisko() 
    {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) 
    {
        this.nazwisko = nazwisko;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getOldname() 
    {
        return oldname;
    }

    public void setOldname(String oldname)
    {
        this.oldname = oldname;
    }

    public String getTelefon()
    {
        return telefon;
    }

    public void setTelefon(String telefon) 
    {
        this.telefon = telefon;
    }

    public String getUlica()
    {
        return ulica;
    }

    public void setUlica(String ulica)
    {
        this.ulica = ulica;
    }

    public String getNip() 
    {
        return nip;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public String getRegon()
    {
        return regon;
    }

    public void setRegon(String regon)
    {
        this.regon = regon;
    }
    

	public List<Map<String, String>> search(Object[] args) throws EdmException
	{
		if(args == null || args.length < 1)
			return null;
		
		List<Map<String, String>> result = new ArrayList<Map<String,String>>();
		Map<String, String> mapT = new LinkedHashMap<String, String>();
		mapT.put("KONTRAHENT", "ID");
		mapT.put("KONTRAHENTname", "Nazwa");
		mapT.put("KONTRAHENTimie", "Imie");
		mapT.put("KONTRAHENTnazwisko", "Nazwisko");
		
		result.add(mapT);
		String imie = null;
		String nazwisko = null;
		StringBuilder sb = new StringBuilder("select top 10 id,name,imie,nazwisko from  df_inst where ");
		String nazwa = args[0].toString();
		if(args.length > 1)
			imie = args[1].toString();
		if(args.length > 2)
			nazwisko = args[2].toString();
		if(StringUtils.isNotEmpty(nazwa))
		{
			sb.append(" name like ? and ");
		}
		if(StringUtils.isNotEmpty(imie))
		{
			sb.append(" imie like ? and ");
		}
		if(StringUtils.isNotEmpty(nazwisko))
		{
			sb.append(" nazwisko like ? and ");
		}
		sb.delete(sb.length()-5, sb.length());

		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{	
			ps =DSApi.context().prepareStatement(sb.toString());
			int i = 1;
			if(StringUtils.isNotEmpty(nazwa))
			{
				ps.setString(i, nazwa+"%");
				i++;
			}
			if(StringUtils.isNotEmpty(imie))
			{
				ps.setString(i, imie+"%");
				i++;
			}
			if(StringUtils.isNotEmpty(nazwisko))
			{
				ps.setString(i, nazwisko+"%");
				i++;
			}
			rs = ps.executeQuery();
			while(rs.next())
			{
				Map<String, String> map = new LinkedHashMap<String, String>();
				map.put("KONTRAHENT", getStringNotNull(rs.getString(1)));
				map.put("KONTRAHENTname", getStringNotNull(rs.getString(2)));
				map.put("KONTRAHENTimie", getStringNotNull(rs.getString(3)));
				map.put("KONTRAHENTnazwisko", getStringNotNull(rs.getString(4)));
				result.add(map);
			}
		}
		catch (Exception e) 
		{
			log.error(e.getMessage(),e);
			throw new EdmException(e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return result; 
	}
	
	private String getStringNotNull(String value)
	{
		if(value == null || value.equalsIgnoreCase("NULL"))
		{
			return "";
		}
		return value;
	}

	public boolean isAjaxAvailable() 
	{
		return true;
	}

	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
}
