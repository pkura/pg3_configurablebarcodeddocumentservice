package pl.compan.docusafe.core.dockinds.dictionary;
import java.util.Collections;
import java.util.List;

import org.hibernate.validator.constraints.Length;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Finder;

/**
 *
 * @author Micha� Sankowski
 */
@Entity
@Table(name = "ic_fixed_assets_group")
public class FixedAssetsGroup {
	private final static Logger log = LoggerFactory.getLogger(FixedAssetsGroup.class);

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)//ic = sql server
	private Integer id;

	@NotNull
	@Length(max=255)
	private String title;

	@OneToMany(fetch=FetchType.LAZY)
	@JoinColumn(name="group_id")
	private List<FixedAssetsSubgroup> subgroups;

	FixedAssetsGroup(){
		
	}

	public Integer getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the subgroups
	 */
	public List<FixedAssetsSubgroup> getSubgroups() {
		return Collections.unmodifiableList(subgroups);
	}

	public static List<FixedAssetsGroup> getAll() throws EdmException {
		return DSApi.context().session().createCriteria(FixedAssetsGroup.class).list();
	}

	public static FixedAssetsGroup find(Integer id) throws EdmException{
		return Finder.find(FixedAssetsGroup.class, id);
	}
}
