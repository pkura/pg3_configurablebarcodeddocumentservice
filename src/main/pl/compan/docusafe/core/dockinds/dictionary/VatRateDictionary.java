package pl.compan.docusafe.core.dockinds.dictionary;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;
import java.util.List;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class VatRateDictionary extends AbstractEditableDictionary<VatRate> implements Dictionary<VatRate>{
    private final static Logger LOG = LoggerFactory.getLogger(VatRateDictionary.class);

    public final static VatRateDictionary INSTANCE = new VatRateDictionary();

    @Override
    public String getCn() {
        return "vat-rate";
    }

    @Override
    public String getTitle() {
        return "Stawki VAT";
    }

    @Override
    public String getEditJspPage() {
        LOG.info("edit jsp page = {}", "/office/dictionaries/edit-vat-rate.jsp");
        return "/office/dictionaries/edit-vat-rate.jsp";
    }

    @Override
    public VatRate get(Serializable id) throws EdmException {
        return VatRate.find((Integer)id);
    }

    @Override
    public List<VatRate> getAll() throws EdmException {
        return VatRate.list();
    }

    @Override
    public String[] getHeaders() {
        return new String[]{"ID", "Symbol", "Stawka [%]", "Od", "Do"};
    }

    @Override
    public Object[] getObjectsForEntry(VatRate entry) {
        return new Object[]{entry.getId(), entry.getSymbol(), entry.getValue(),
                entry.getFromJsString(),entry.getToJsString()};
    }

    @Override
    public VatRate get() {
        return new VatRate();
    }
}