package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;

/**
 * @author Bartlomiej Spychalski mailto:bsp@spychalski.eu
 */
public class DaaAgencja extends DaaDictionary
{
    private static final Logger log = LoggerFactory.getLogger(DaaAgencja.class);
    private static final String BIURO_SIEC_WEWN = "Biuro w sieci wewnetrznej";
    private static final String AGENCJA_SIEC_ZEWN = "Agencja w sieci zewnetrznej";
    private static final String BANK = "Bank";

    //rodzaj agencji (agencja, bank, biuro)
    public static final Map<Integer, String> agencjaTypMap = new HashMap<Integer, String>();
    static {
        agencjaTypMap.put(1, BIURO_SIEC_WEWN);
        agencjaTypMap.put(2, AGENCJA_SIEC_ZEWN);
        agencjaTypMap.put(3, BANK);
    }

    private Long id;
    private String nazwa;
    private String nip;
    private String numer;

    private String ulica;
    private String kod;
    private String miejscowosc;
    private String email;
    private String faks;
    private String telefon;

    private Integer rodzaj_sieci;

    public  DaaAgencja() 
    {

    }

    public String dictionaryAction()
    {
        return "/office/common/agencja.action";
    }
    
    /**
     * Zwraca stringa zawieraj�cego nazwe funkcji (najlepiej umieszczonej w pliku dockind.js)
     *  kt�ra wykona si� w funkcji accept_cn
     * 
     */
    public String dictionaryAccept()
    {
        return "";              
    }

    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        map.put("nazwa", "Nazwa");
        map.put("numer", "Numer");
        map.put("nip", "NIP");

        return map;
    }

    public String getDictionaryDescription()
    {
        return ""+nazwa;
    }

    public String dictionarySortColumn()
    {
        return "nazwa";
    }    
    
    /**
     * Zapisuje agencje w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException{
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad dodania agencji archiwum agenta. "+e.getMessage());
        }

    }

    /**
     * Usuwa agencje 
     * @throws EdmException
     */
    public void delete() throws EdmException {
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia agencji archiwum agenta");
        }
    }

    /**
     * Wyszukuje konkretna agencje archiwum agenta
     * @param id
     * @return
     * @throws EdmException
     */
    public DaaAgencja find(Long id) throws EdmException
    {
        DaaAgencja daaAgencja = DSApi.getObject(DaaAgencja.class, id);
        if (daaAgencja == null) 
            throw new EdmException("Nie znaleziono agencji o nr " + id);
        else 
            return daaAgencja;
    }

    /**
     * Wyszukuje liste dostepnych agencji archiwum agenta
     * @return
     * @throws EdmException
     */
    public static List<DaaAgencja> findAll() throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(DaaAgencja.class);
        if (c == null) 
            throw new EdmException("Nie mozna okreslic kryterium wyszukiwania agencji archiwum agenta");
        try 
        {
            log.info("pobranie listy agencji");
            return (List<DaaAgencja>)c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List<DaaAgencja> find(Map<String, Object> parametry) throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(DaaAgencja.class);
        if (c == null) 
            throw new EdmException("Nie mozna okreslic kryterium wyszukiwania agencji archiwum agenta");
        if (parametry != null) 
        {
            if (parametry.get("nazwa") != null) c.add(Expression.like("nazwa",  "%"+parametry.get("nazwa")+"%"));
            if (parametry.get("nip") != null) c.add(Expression.eq("nip", parametry.get("nip")));
            if (parametry.get("numer") != null) c.add(Expression.like("numer", "%"+parametry.get("numer")+"%"));
            if (parametry.get("ulica") != null) c.add(Expression.eq("ulica", parametry.get("ulica")));
            if (parametry.get("kod") != null) c.add(Expression.eq("kod", parametry.get("kod")));
            if (parametry.get("miejscowosc") != null) c.add(Expression.eq("miejscowosc", parametry.get("miejscowosc")));
            if (parametry.get("email") != null) c.add(Expression.eq("email", parametry.get("email")));
            if (parametry.get("faks") != null) c.add(Expression.eq("faks", parametry.get("faks")));
            if (parametry.get("telefon") != null) c.add(Expression.eq("telefon", parametry.get("telefon")));
            if (parametry.get("rodzaj_sieci") != null) c.add(Expression.eq("rodzaj_sieci", parametry.get("rodzaj_sieci")));
        }
        try 
        {
            log.info("pobranie listy agencji");
            return (List<DaaAgencja>)c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }


    /**
     * Znajduje obiekty DaaAgencja na podstawie formularza wyszukiwania.
     * @param form
     * @return
     * @throws EdmException
     */
    public static SearchResults<? extends DaaAgencja> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DaaAgencja.class);

            if (form.hasProperty("nazwa"))
                c.add(Expression.like("nazwa", "%"+form.getProperty("nazwa")+"%"));
            if (form.hasProperty("numer"))
                c.add(Expression.like("numer", "%"+form.getProperty("numer")+"%"));
            if (form.hasProperty("nip"))
                c.add(Expression.eq("nip", form.getProperty("nip")));
            if (form.hasProperty("telefon"))
                c.add(Expression.eq("telefon", form.getProperty("telefon")));
            if (form.hasProperty("faks"))
                c.add(Expression.eq("faks", form.getProperty("faks")));
            if (form.hasProperty("email"))
                c.add(Expression.eq("email", form.getProperty("email")));
            if (form.hasProperty("kod"))
                c.add(Expression.eq("kod", form.getProperty("kod")));
            if (form.hasProperty("rodzaj_sieci"))
                c.add(Expression.eq("rodzaj_sieci", form.getProperty("rodzaj_sieci")));
            if (form.hasProperty("miejscowosc"))
                c.add(Expression.eq("miejscowosc", form.getProperty("miejscowosc")));
            if (form.hasProperty("ulica"))
                c.add(Expression.eq("ulica", form.getProperty("ulica")));

            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }

            List<DaaAgencja> list = (List<DaaAgencja>) c.list();

            if (list.size() == 0)
            {
                return SearchResultsAdapter.emptyResults(DaaAgencja.class);
            }
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                int toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();

                return new SearchResultsAdapter<DaaAgencja>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), DaaAgencja.class);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public Long getId() 
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNazwa()
    {
        return nazwa;
    }

    public void setNazwa(String nazwa) 
    {
        this.nazwa = nazwa;
    }

    public String getNip()
    {
        return nip;
    }

    public void setNip(String nip) 
    {
        this.nip = nip;
    }

    public Integer getRodzaj_sieci()
    {
        return rodzaj_sieci;
    }

    public void setRodzaj_sieci(Integer rodzaj_sieci)
    {
        this.rodzaj_sieci = rodzaj_sieci;
    }

    public String getUlica()
    {
        return ulica;
    }

    public void setUlica(String ulica) 
    {
        this.ulica = ulica;
    }

    public String getKod() 
    {
        return kod;
    }

    public void setKod(String kod) 
    {
        this.kod = kod;
    }

    public String getMiejscowosc() 
    {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc) 
    {
        this.miejscowosc = miejscowosc;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFaks()
    {
        return faks;
    }

    public void setFaks(String faks)
    {
        this.faks = faks;
    }

    public String getTelefon()
    {
        return telefon;
    }

    public void setTelefon(String telefon)
    {
        this.telefon = telefon;
    }

    public String getNumer()
    {
        return numer;
    }

    public void setNumer(String numer)
    {
        this.numer = numer;
    }

	public String getBoxDescription() {
		return null;
	}

	public boolean isAjaxAvailable() {
		return false;
	}

	public boolean isBoxDescriptionAvailable() {
		return false;
	}

	public List<String> additionalSearchParam() {
		return null;
	}

	public List<Map<String, String>> search(Object[] args) throws EdmException {
		return null;
	}

}
