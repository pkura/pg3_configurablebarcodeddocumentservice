package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentHelper;
import pl.compan.docusafe.core.base.Folder;

public abstract class DaaDictionary extends AbstractDocumentKindDictionary
{
	
	public static Logger folderClearLog = LoggerFactory.getLogger("folderClearLog");
	
	private JSONArray folders;

	/**
	 * Metoda przeznaczona tylko dla hibernate
	 * @return
	 */
	protected String getFolders()
	{
		if(folders == null)
		{
			return null;
		}
		return folders.toString();
	}

	
	/**
	 * Metoda przeznaczona tylko dla hibernate
	 * @param folders
	 */
	protected void setFolders(String folders)
	{		
		try
		{
			if(folders == null)
				throw new EdmException("null string folder");
				
			this.folders = new JSONArray(folders);
		}
		catch (Exception e) 
		{
			folderClearLog.trace(e.getMessage());
			this.folders = new JSONArray();
		}
		
	}
	
	public void addFolder(Long folderId)
	{
		this.folders.put(folderId);
	}
	
	public void changeFolders() throws EdmException
	{
		LoggerFactory.getLogger("tomekl").debug("changeFolders");
		Set<Folder> folders = new LinkedHashSet<Folder>();
		Set<Document> documents = new LinkedHashSet<Document>();
		
		if (this.folders == null)
			throw new EdmException("Zmienna this.folders jest null!");
		
		for(int i=0; i<this.folders.length();i++)
		{
			folderClearLog.trace("fid {}",this.folders.get(i));		
			folders.add(Folder.find(Long.parseLong(String.valueOf(this.folders.get(i)))));
		}
		
		pl.compan.docusafe.core.base.DocumentHelper dh = new pl.compan.docusafe.core.base.DocumentHelper();
		
		for(Folder folder : folders)
		{
			documents.addAll(archiveDocumentsInFolder(folder, dh));
		}
		
		for(Document doc : documents)
		{
			folderClearLog.trace("doc {}",doc.getId());
			doc.setPermissionsOn(false);
			doc.getDocumentKind().logic().archiveActions(doc, -500);
		}
		
		for(Folder fl : folders)
		{
			try
			{
				dh.safeDeleteFolder(fl, false);
			}
			catch (Exception e) 
			{
				folderClearLog.trace(e.getMessage());
				//TU BEDA LECIALY EXCEPTIONY ALE TO NIE SZKODZI
			}
		}
		
	}
	
	private Set<Document> archiveDocumentsInFolder(Folder folder, DocumentHelper dh) throws EdmException
	{
		Set<Document> documents = new LinkedHashSet<Document>();
		
		if(folder.hasChildFolders())
		{
			for(Folder fl : folder.getChildFolders())
				documents.addAll(archiveDocumentsInFolder(fl, dh));
		}
		
		documents.addAll(Arrays.asList(dh.searchDocuments(0, 100, folder, true, "id", true).results()));
		
		return documents;
	}

	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
}
