package pl.compan.docusafe.core.dockinds.dictionary;

import com.google.common.base.Preconditions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jfree.util.Log;

/**
 * Zbi�r narz�dzi przydatnych przy s�ownikach
 */
public class DictionaryUtils {
    private static Pattern fieldCnPattern = Pattern.compile("_\\d+$");

    /**
     * Dodaje (SQL insert) wpis do tabeli s�ownika wielowarto�ciowego. Wstawia tylko odwo�anie, bez dodawania elementu do s�ownika. <br />
     * <b>NIE otwiera kontekstu ani transakcji</b>
     * @param doc Dokument, z kt�rego zostanie pobrana tabel multiple s�ownika
     * @param dictionaryName Nazwa s�ownika (czyli cn pola s�ownika)
     * @param dictionaryValue Warto�� wpisu
     * @throws EdmException
     */
    public static void addToMultipleTable(Document doc, String dictionaryName, String dictionaryValue) throws EdmException {
        String multipleTable = doc.getDocumentKind().getMultipleTableName();
        StringBuilder sql = new StringBuilder("insert into ");
        sql.append(multipleTable).append(" (DOCUMENT_ID, FIELD_CN, FIELD_VAL) values (?, ?, ?)");
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(sql.toString());
            ps.setLong(1, doc.getId());
            ps.setString(2, dictionaryName);
            ps.setString(3, dictionaryValue);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new EdmException(e.getLocalizedMessage(), e);
        }finally {
            if(DSApi.isContextOpen()){
                DSApi.context().closeStatement(ps);
            }
        }
    }

    /**
     * Zwraca nazw� pola w s�owniku bez suffiksu, tzn bez <code>_1</code>
     * @param fieldCn Pole bez suffiksu
     * @return
     */
    public static String getFieldCnWithoutSuffix(String fieldCn) {
        Preconditions.checkNotNull(fieldCn);
        return fieldCnPattern.matcher(fieldCn).replaceFirst("");
    }

    /**
     * Zwraca nazw� pola z przedrostkiem s�ownika
     * @param dictionaryName
     * @param fieldCn
     * @return
     */
    public static String getFieldCnWithDictionaryPrefix(String dictionaryName, String fieldCn) {
        Preconditions.checkNotNull(dictionaryName);
        Preconditions.checkNotNull(fieldCn);
        String dicPrefix = dictionaryName + "_";
        if(fieldCn.startsWith(dicPrefix))
            return fieldCn;

        return dicPrefix + fieldCn;
    }

    /**
     * Zwraca nazw� pola bez przedrostka s�ownika
     * @param dictionaryName
     * @param fieldCn
     * @return
     */
    public static String getFieldCnWithoutDictionaryPrefix(String dictionaryName, String fieldCn) {
        Preconditions.checkNotNull(dictionaryName);
        Preconditions.checkNotNull(fieldCn);
        String dicPrefix = dictionaryName + "_";
        if(fieldCn.startsWith(dicPrefix)) {
            return fieldCn.replaceFirst(dicPrefix, "");
        }

        return fieldCn;
    }
    
    /**Metoda sortuje s�ownik wielowarto�ciowy wed�ug pola sort (DictionaryName + FieldName)
     * @param sort nazwa pola s�ownikowego (DictionaryName + FieldName) bez _1
     * @param multipleDictionaryName nazwa s�ownika wielowarto�ciowego
     * @param fm FieldsManager
     * @throws EdmException
     */
    public static void sortMultipleDictionary(final String sort, final String multipleDictionaryName, FieldsManager fm) throws EdmException{
    	Object multipleDictionaryObject = fm.getFieldValues().get(multipleDictionaryName);
    	if(multipleDictionaryObject != null) {
    		List<Long> multipleDictionary = (List<Long>) multipleDictionaryObject;
    		List<Map<String, Object>> multiValues = new LinkedList<Map<String, Object>>();
        	
        	for (Long id : multipleDictionary) {
    			Map<String, Object> dicValues = DwrDictionaryFacade.getDictionary(fm.getField(multipleDictionaryName), null).getValues(id.toString());
    			multiValues.add(dicValues);
    		}
        	
        	if (sort != null) {
    			Collections.sort(multiValues, new Comparator<Map<String, Object>>() {
    				@Override
    				public int compare(Map<String, Object> arg0, Map<String, Object> arg1) {
    					Object ob0 = arg0.get(sort);
    					Object ob1 = arg1.get(sort);
    					if (ob0 != null && ob1 != null && ob0 instanceof Comparable) {
    						return ((Comparable) ob0).compareTo((Comparable) ob1);
    					} else if (ob0 != null && ob1 != null) {
    						return ob0.toString().compareTo(ob1.toString());
    					} else if (ob0 == null){
    						return 1;
    					} else if (ob1 == null) {
    						return -1;
    					} else {
    						return 0;
    					}
    				}
    			});
    			if (multiValues.size() > 0 && multiValues.get(0).get("id") != null) {
    				ArrayList array = new ArrayList<Object>();
    				for (Map<String, Object> value : multiValues)
    					array.add(value.get("id"));
    				
    				if(multipleDictionary.size() > 0){
    					multipleDictionary.clear();
    					multipleDictionary.addAll(array);
    				}
    			}
    		}
    	}
    }
    
    /**Metoda zwraca pole EnumValues zawarte w s�owniku (multiple="false")
     * @param dictionaryName - nazwa s�ownika
     * @param enumFieldName - nazwa pola typu enum na s�owniku
     * @param fm - FieldsManager z dokumentu
     * @return
     */
    public static EnumValues getEnumValuesFromDictionary(String dictionaryName, String enumFieldName, FieldsManager fm) {
    	try {
    		Map<String,Object> dictionary = (Map<String, Object>) fm.getValue(dictionaryName);
        	EnumValues enumField;
    		StringBuilder enumValuesName = new StringBuilder(dictionaryName).append("_").append(enumFieldName);
    		enumField = (EnumValues) dictionary.get(enumValuesName.toString());
    		return enumField;
    	} catch (Exception e) {
    		Log.error(e.getMessage(), e);
    		return null;
    	}
    }
    
    /**Metoda zwraca pola EnumValues zawarte w s�owniku (multiple="true")
     * @param dictionaryName - nazwa s�ownika
     * @param enumFieldName - nazwa pola typu enum na s�owniku
     * @param fm - FieldsManager z dokumentu
     * @return
     */
    public static List<EnumValues> getEnumValuesFromMultipleDictionary(String dictionaryName, String enumFieldName, FieldsManager fm) {
    	try {
    		List<Map<String,Object>> dictionary = (List<Map<String,Object>>)fm.getValue(dictionaryName);
        	List<EnumValues> enumFields = new ArrayList<EnumValues>();
        	StringBuilder enumValuesName = new StringBuilder(dictionaryName).append("_").append(enumFieldName);
        	for (Map<String,Object> item : dictionary) {
        		enumFields.add((EnumValues)item.get(enumValuesName.toString()));
			}
    		
    		if (enumFields.size() > 0) {
    			return enumFields;
    		} else {
    			return null;
    		}
    		
    	} catch (Exception e) {
    		Log.error(e.getMessage(), e);
    		return null;
    	}
    }
    
    /**Metoda zwraca pole DataBaseEnumField zawarte w s�owniku (multiple="false")
     * @param dictionaryName - nazwa s�ownika
     * @param enumFieldName - nazwa pola typu enum na s�owniku
     * @param fm - FieldsManager z dokumentu
     * @return
     */
    public static EnumItem getEnumItemFromDictionary(String tableName, String dictionaryName, String enumFieldName, FieldsManager fm) {
    	try {
    		EnumValues enumValues = getEnumValuesFromDictionary(dictionaryName, enumFieldName, fm);
    		Integer selectedEnumId = Integer.valueOf(enumValues.getSelectedId());
    		StringBuilder enumItemName = new StringBuilder(dictionaryName).append("_").append(enumFieldName);
    		EnumItem enumItem = DataBaseEnumField.getEnumItemForTable(tableName, selectedEnumId);
    		return enumItem;
    	} catch (Exception e) {
    		Log.error(e.getMessage(), e);
    		return null;
    	}
    }
    
    /**Metoda zwraca pola DataBaseEnumField zawarte w s�owniku (multiple="true")
     * @param dictionaryName - nazwa s�ownika
     * @param enumFieldName - nazwa pola typu enum na s�owniku
     * @param fm - FieldsManager z dokumentu
     * @return
     */
    public static List<EnumItem> getEnumItemFromMultipleDictionary(String tableName, String dictionaryName, String enumFieldName, FieldsManager fm) {
    	try {
    		List<EnumValues> enumValues = getEnumValuesFromMultipleDictionary(dictionaryName, enumFieldName, fm);
    		StringBuilder enumItemName = new StringBuilder(dictionaryName).append("_").append(enumFieldName);
    		List<EnumItem> enumItems = new ArrayList<EnumItem>();
    		for (EnumValues item : enumValues) {
    			enumItems.add(DataBaseEnumField.getEnumItemForTable(tableName, Integer.valueOf(item.getSelectedId())));
			}
    		
    		if (enumItems.size() > 0) {
    			return enumItems;
    		} else {
    			return null;
    		}
    		
    	} catch (Exception e) {
    		Log.error(e.getMessage(), e);
    		return null;
    	}
    }

}
