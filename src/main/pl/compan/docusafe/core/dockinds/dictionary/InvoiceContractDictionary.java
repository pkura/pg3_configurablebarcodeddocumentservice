package pl.compan.docusafe.core.dockinds.dictionary;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class InvoiceContractDictionary extends AbstractDocumentKindDictionary 
{
	private static final Logger log = LoggerFactory.getLogger(InvoiceContractDictionary.class);
	
	private Long documentId;
	private String numerUmowy;

    @Override
    public Long getId() {
        return documentId;
    }

    @Override
	public void create() throws EdmException 
	{
	}

	@Override
	public void delete() throws EdmException {
	}

	@Override
	public String getDictionaryDescription() 
	{
		return ""+numerUmowy;
	}
	
	public String dictionaryAction()
	{
		return "/office/common/invoice-contract.action?popup=true";
	}
	
	public  String dictionaryAccept()
	{
		return "";
	}
	
	public List<String> additionalSearchParam()
	{
		List<String> list = new ArrayList<String>();
		list.add("document.getElementById('dockind_"+InvoiceLogic.DOSTAWCA_FIELD_CN+"prettyNip').value");
		list.add("document.getElementById('dockind_"+InvoiceLogic.DOSTAWCA_FIELD_CN+"').value");
		return list;
	}
	

	public Map<String, String> dictionaryAttributes()
	{
		StringManager smL = GlobalPreferences.loadPropertiesFile(InvoiceContractDictionary.class.getPackage().getName(), null);
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("numerUmowy", smL.getString("NumerUmowy"));
		return map;
	}

	@Override
	public DocumentKindDictionary find(Long key) throws EdmException 
	{
		if(key == null)
			return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		documentId = key;
		try
		{
			ps =DSApi.context().prepareStatement("select TITLE from DS_document where id = ? ");
			ps.setLong(1, key);
			rs = ps.executeQuery();
			if(rs.next())
				numerUmowy = rs.getString(1);
		}
		catch (Exception e) 
		{
			log.error(e.getMessage(),e);
			throw new EdmException(e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return this;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public String getNumerUmowy() {
		return numerUmowy;
	}

	public void setNumerUmowy(String numerUmowy) {
		this.numerUmowy = numerUmowy;
	}

	public List<Map<String, String>> search(Object[] args) throws EdmException
	{
		if(args == null || args.length < 1)
			return null;
		
		List<Map<String, String>> result = new ArrayList<Map<String,String>>();
		Map<String, String> mapT = new LinkedHashMap<String, String>();
		mapT.put("UMOWA", "ID");
		mapT.put("UMOWAnumerUmowy", "Nazwa");
		mapT.put("dataStart", "Data");
		mapT.put("dataStop", "Data");
		result.add(mapT);
		String nazwaUmowy = args[0].toString();
		String kontrahentNIP = null;
		String kontrahentId = null;
		if(args.length > 1)
		{
			kontrahentNIP = args[1].toString();
			kontrahentNIP = kontrahentNIP.replaceAll("-", "");
			kontrahentId = String.valueOf(args[1]);
		}
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if(DocumentKind.isAvailable(DocumentLogicLoader.DP_KIND))
			{
				ps =DSApi.context().prepareStatement(
                                        "SELECT top 30 doc.id,doc.title,dp.data_poczatkowa,dp.data_koncowa,dp.data_umowy,dp.data_aneksu" +
					" FROM DS_document doc, dsg_prawny dp "+(kontrahentNIP != null ? ", dp_inst inst" : "")+
                                        " WITH (NOLOCK) WHERE doc.id = dp.document_id AND doc.title like ? "+
                                        (kontrahentNIP != null ? " AND dp.inst = inst.id and inst.nip = ? " : "")+
                                          " AND (dp.nieaktywny <> 1 OR dp.nieaktywny IS NULL)");
				ps.setString(1, nazwaUmowy+"%");
				if(kontrahentNIP != null)
					ps.setString(2, kontrahentNIP);
			}
			else if(DocumentKind.isAvailable(DocumentLogicLoader.DP_PTE_KIND))
			{
				ps =DSApi.context().prepareStatement("select doc.id,doc.title, dp.okres_od, dp.okres_do, dp.data180, dp.data190 from DS_document doc WITH (NOLOCK) join dsg_prawny_pte dp on dp.document_id = doc.id where doc.title like ? and dp.instytucja30 = ? and doc.folder_Id <> 2");
				ps.setString(1, nazwaUmowy+"%");
				ps.setLong(2, Long.valueOf(kontrahentId));
			}
			
			//ps =DSApi.context().prepareStatement("select top 10 doc.id,doc.title,dp.data_poczatkowa,dp.data_koncowa,dp.data_umowy,dp.data_aneksu" +
			//		" from DS_document doc, dsg_prawny dp "+(kontrahentNIP != null ? ", dp_inst inst" : "")+" WITH (NOLOCK) where doc.id = dp.document_id and doc.title like ? "+(kontrahentNIP != null ? " and dp.inst = inst.id and inst.nip = ? " : ""));
			
			/*String tmp = "select top 10 doc.id,doc.title,dp.data_poczatkowa,dp.data_koncowa,dp.data_umowy,dp.data_aneksu" +
			" from DS_document doc, dsg_prawny dp "+(kontrahentNIP != null ? ", dp_inst inst" : "")+" WITH (NOLOCK) where doc.id = dp.document_id and doc.title like ? "+(kontrahentNIP != null ? " and dp.inst = inst.id and inst.nip = ? " : "");*/
			
			/*ps.setString(1, nazwaUmowy+"%");
			if(kontrahentNIP != null)
				ps.setString(2, kontrahentNIP);*/
			rs = ps.executeQuery();
			while(rs.next())
			{
				Map<String, String> map = new LinkedHashMap<String, String>();
				map.put("UMOWA", getStringNotNull(rs.getString(1)));
				map.put("UMOWAnumerUmowy", getStringNotNull(rs.getString(2)));
				if(rs.getString(2) != null && rs.getString(2).equalsIgnoreCase("NULL"))
					map.put("dataStart", getStringNotNull(rs.getString(3)));
				else
					map.put("dataStart", getStringNotNull(rs.getString(5)));
				if(rs.getString(3) != null && rs.getString(3).equalsIgnoreCase("NULL"))
					map.put("dataStop", getStringNotNull(rs.getString(4)));
				else
					map.put("dataStop", getStringNotNull(rs.getString(6)));
				result.add(map);
			}
			
			/**Jesli nie znalazl i byla nazwa kontrahenta to szuka tylko po tytule*/
			/*if(result.size() < 2 && kontrahentNIP != null && !searchNext)
			{
				LoggerFactory.getLogger("tomekl").debug("bez nipu");
				return search(new Object[]{args[0]});
			}*/									
		}
		catch (Exception e) 
		{
			log.error(e.getMessage(),e);
			throw new EdmException(e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return result;
	}
	
	private String getStringNotNull(String value)
	{
		if(value == null || value.equalsIgnoreCase("NULL"))
		{
			return "";
		}
		return value;
	}

	public boolean isAjaxAvailable() 
	{
		return true;
	}

	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
}
