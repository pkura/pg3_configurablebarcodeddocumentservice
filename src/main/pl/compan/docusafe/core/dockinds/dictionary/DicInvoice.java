package pl.compan.docusafe.core.dockinds.dictionary;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;

/**
 * @author Tomasz Lipka
 * @info
 * Slownik instytucji/kontrahentow dla dokumentow invoice, zawiera dodatkowo pole numerKontaBankowego i numerKontrahenta
 */
public class DicInvoice extends AbstractDocumentKindDictionary{
	public static final int MAX_SEARCH_RESULTS = 1000;

	private static final Logger log = LoggerFactory.getLogger(DicInvoice.class);

    private static final String NUMBER_DUPLICATE_QUERY = "select count(*) from " + DicInvoice.class.getName() + " as dic where dic.numerKontrahenta = :number";
	private static DicInvoice instance;
	public static synchronized DicInvoice getInstance()
	{
		if (instance == null)
			instance = new DicInvoice();
		return instance;
	}
	//pola slownika
	private long id;
	private String numerKontrahenta;
	private String numerKontaBankowego;
	private String imie;
    private String nazwisko;
    private String name;
    private String oldname;
    private String nip;
    private String regon;
    private String ulica;
    private String kod;
    private String miejscowosc;
    private String telefon;
    private String faks;
    private String email;
    private String senderCountry;
    private String numerDomu;


    public DicInvoice() {}


    public static boolean isNumberDuplicate(String numerKontrahenta) throws EdmException {
        return Long.valueOf(DSApi.context().session()
                .createQuery(NUMBER_DUPLICATE_QUERY)
                .setString("number", numerKontrahenta)
                .uniqueResult() + "") > 0;
    }

	public static void checkNumberExists(String numerKontrahenta) throws EdmException, HibernateException {
		if (AvailabilityManager.isAvailable("dicinvoice.verify.numerKontrahenta")
                && isNumberDuplicate(numerKontrahenta)) {
            throw new EdmException("Kontrahent o podanym numerze ju� istnieje");
		}
	}
    
    public String getDicDescription()
    {
    	return name + " \n"+ (ulica != null ? ulica : "")+"\n"+(kod != null ? kod : "")+"  "+(miejscowosc != null ? miejscowosc : "")+"\n NIP:"+nip;
    }
    
    public  String dictionaryAction()
    {
        return "/office/common/dicinvoice.action"; // /office/common/dicinvoice.action 
    }
	
    //po zaakceptowaniu funkcja js do wykonania, plik dockind.js [dockind_NUMER_RACHUNKU_BANKOWEGO w invoice wypelnic z tego]
    public String dictionaryAccept()
    {
        return "";              
    }
	
    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        if(AvailabilityManager.isAvailable("DicInvoice.sender.short")){
            map.put("numerKontrahenta","Nr nadawcy");
            map.put("name", "Nazwa nadawcy");
        } else {
        	map.put("numerKontrahenta","Nr Kontrahenta");
        	map.put("name", "Nazwa kontrahenta");
        	map.put("prettyNip","NIP");
        	if(AvailabilityManager.isAvailable("DicInvoice.show.telefon"))
        	{
        		map.put("telefon","Telefon");
        	}
        }
        if(AvailabilityManager.isAvailable("DicInvoice.eservice"))
        {
        	map.put("name","Nazwa klienta");
        	map.put("prettyNip", "NIP klienta");
        	map.put("numerKontrahenta","Numer klienta");
//        	map.put("ulica","Ulica");
//        	map.put("numerDomu","Numer");
//        	map.put("kod","Kod Pocztowy");
//        	map.put("miejscowosc","Miasto");
        }
        return map;

    }
    
    public String getDictionaryDescription()
    {
        return numerKontrahenta + " " +name + " " + getPrettyNip();
    }
    
    public String dictionarySortColumn()
    {
        return "numerKontrahenta";
    }
    
    /**
     * Zapisuje kontrahenta w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
			checkNumberExists(this.numerKontrahenta);
            DSApi.context().session().saveOrUpdate(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania instytucji/kontrahenta dokumentu finansowego. "+e.getMessage(),e);
            throw new EdmException("Blad dodania instytucji/kontrahenta dokumentu finansowego. "+e.getMessage());
        }
    }
    
    /**
     * Usuwa kontrahenta
     * @throws EdmException
     */
    public void delete() throws EdmException 
    {
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia instytucji/kontrahenta dokumentu prawnego");
            throw new EdmException("Blad usuniecia instytucji/kontrahenta dokumentu prawnego. "+e.getMessage());
        }
    }
    
    
    /**
     * Wyszukuje konkretna instytucje/kontrahenta dokumentu finansowego
     * @param id
     * @return
     * @throws EdmException
     */
    public DicInvoice find(Long id) throws EdmException
    {
        DicInvoice inst = DSApi.getObject(DicInvoice.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono instytucji/kontrahenta o nr " + id);
        else 
            return inst;
    }

    public static DicInvoice get(long id) throws EdmException {
        return DSApi.getObject(DicInvoice.class, id);
    }
    
    public static List<DicInvoice> find(Map<String, Object> parametry) throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(DicInvoice.class);
        if(c == null) 
            throw new EdmException("Nie mozna okreslic kryterium wyszukiwania instytucji/kontrahenta dokumentu finansowego");
        if(parametry != null) 
        {
        	if(parametry.get("numerKontrahenta") != null) 
        		c.add(Restrictions.like("numerKontrahenta",  "%"+parametry.get("numerKontrahenta")+"%"));    
        	if(parametry.get("numerKontaBankowego") != null) 
                c.add(Restrictions.like("numerKontaBankowego",  "%"+parametry.get("numerKontaBankowego")+"%"));
            if(parametry.get("imie") != null) 
                c.add(Restrictions.like("imie",  "%"+parametry.get("imie")+"%"));
            if(parametry.get("nazwisko") != null) 
                c.add(Restrictions.like("nazwisko", "%" + parametry.get("nazwisko") + "%"));
            if(parametry.get("name") != null) 
                c.add(Restrictions.like("name", "%"+parametry.get("name")+"%"));
            if(parametry.get("oldname") != null) 
                c.add(Restrictions.like("oldname", "%"+parametry.get("oldname")+"%"));
            if(parametry.get("nip") != null) 
                c.add(Restrictions.like("nip", "%"+parametry.get("nip")+"%"));
            if(parametry.get("regon") != null) 
                c.add(Restrictions.like("regon", "%"+parametry.get("regon")+"%"));
            if(parametry.get("ulica") != null) 
                c.add(Restrictions.like("ulica", "%" + parametry.get("ulica") + "%"));
            if(parametry.get("kod") != null) 
                c.add(Restrictions.like("kod", "%" + parametry.get("kod") + "%"));
            if(parametry.get("miejscowosc") != null) 
                c.add(Restrictions.like("miejscowosc", "%" + parametry.get("miejscowosc") + "%"));
            if(parametry.get("telefon") != null) 
                c.add(Restrictions.like("telefon", "%" + parametry.get("telefon") + "%"));
            if(parametry.get("faks") != null) 
                c.add(Restrictions.like("faks", "%" + parametry.get("faks") + "%"));
            if(parametry.get("email") != null) 
                c.add(Restrictions.like("email", "%" + parametry.get("email") + "%"));
            if(parametry.get("senderCountry") != null) 
                c.add(Restrictions.like("senderCountry", "%" + parametry.get("senderCountry") + "%"));
            if(parametry.get("numerDomu") != null) 
                c.add(Restrictions.like("numerDomu", "%" + parametry.get("numerDomu") + "%"));
        }
        try 
        {
            log.info("pobranie listy instytucji/kontrahent�w (df)");
            return (List<DicInvoice>)c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Znajduje obiekty DicInvoice na podstawie formularza wyszukiwania.
     * @param form
     * @return
     * @throws EdmException
     */
    public static SearchResults<? extends DicInvoice> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DicInvoice.class);

            if(form.hasProperty("numerKontrahenta")) 
                c.add(Restrictions.like("numerKontrahenta",  "%"+form.getProperty("numerKontrahenta")+"%"));
        	if(form.hasProperty("numerKontaBankowego")) 
                c.add(Restrictions.like("numerKontaBankowego",  "%"+form.getProperty("numerKontaBankowego")+"%"));
            if (form.hasProperty("imie"))
                c.add(Restrictions.like("imie", "%"+form.getProperty("imie")+"%"));
            if (form.hasProperty("nazwisko"))
                c.add(Restrictions.like("nazwisko", "%"+form.getProperty("nazwisko")+"%"));
            if (form.hasProperty("name"))
                c.add(Restrictions.like("name", "%" + form.getProperty("name") + "%"));
            if (form.hasProperty("oldname"))
                c.add(Restrictions.like("oldname", "%" + form.getProperty("oldname") + "%"));
            if (form.hasProperty("nip"))
                c.add(Restrictions.like("nip", "%" +  (DicInvoice.cleanNip((String) form.getProperty("nip")))+ "%"));
            if (form.hasProperty("regfon"))
                c.add(Restrictions.like("regon", "%" +  form.getProperty("regon")+ "%"));
            if (form.hasProperty("ulica"))
                c.add(Restrictions.like("ulica", "%" + form.getProperty("ulica") + "%"));
            if (form.hasProperty("kod"))
                c.add(Restrictions.like("kod", "%" + form.getProperty("kod") + "%"));
            if (form.hasProperty("miejscowosc"))
                c.add(Restrictions.like("miejscowosc", "%" + form.getProperty("miejscowosc")+ "%"));
            if (form.hasProperty("telefon"))
                c.add(Restrictions.like("telefon", "%" + form.getProperty("telefon") + "%"));
            if (form.hasProperty("faks"))
                c.add(Restrictions.like("faks", "%" + form.getProperty("faks") + "%"));
            if (form.hasProperty("email"))
                c.add(Restrictions.like("email","%" +  form.getProperty("email") + "%"));
            if (form.hasProperty("senderCountry"))
                c.add(Restrictions.like("senderCountry", "%" + form.getProperty("senderCountry")+ "%"));
            if (form.hasProperty("numerDomu"))
                c.add(Restrictions.like("numerDomu", "%" + form.getProperty("numerDomu")+ "%"));

            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }
			c.setMaxResults(MAX_SEARCH_RESULTS);
            List<DicInvoice> list = (List<DicInvoice>) c.list();

            if (list.size() == 0)
                return SearchResultsAdapter.emptyResults(DicInvoice.class);
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                int toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();
                 
                return new SearchResultsAdapter<DicInvoice>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), DicInvoice.class);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static String cleanNip(String nip)
    {
    	if(nip != null)
    		return nip.replaceAll("-", "").replaceAll(" ", "");
    	else
    		return nip;
    }
    
    public static String prettyNip(String nip) {
		try {
			StringBuffer sb = new StringBuffer(nip);
			for (int i = 3; i <= 9; i = i + 3) {
				sb.insert(i, "-");
			}

			return sb.toString();
		} catch (Exception e) {
			return nip;
		}
	}
    
    public String toString()
    {
    	return (name != null ? name : "");
    }
    
    /* Getters & Setters */
	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumerKontrahenta() {
		return numerKontrahenta;
	}

	public void setNumerKontrahenta(String numerKontrahenta) {
		this.numerKontrahenta = numerKontrahenta;
	}

	public String getNumerKontaBankowego() {
		return numerKontaBankowego;
	}

	public void setNumerKontaBankowego(String numerKontaBankowego) {
		this.numerKontaBankowego = numerKontaBankowego;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
        if(name != null && name.length() > 255) throw new IllegalArgumentException("Nazwa zbyt d�uga : " + name);
		this.name = name;
	}

	public String getOldname() {
		return oldname;
	}

	public void setOldname(String oldname) {
		this.oldname = oldname;
	}

	public String getNip() 
	{		
		return nip;
	}

	public String getPrettyNip(){
		return prettyNip(getNip());
	}

	public void setNip(String nip) 
	{
		if(nip != null)
			this.nip = nip.replaceAll("-", "").replaceAll(" ", "");
		else
			this.nip = nip;
	}

	public String getRegon() {
		return regon;
	}

	public void setRegon(String regon) {
		this.regon = regon;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	public String getMiejscowosc() {
		return miejscowosc;
	}

	public void setMiejscowosc(String miejscowosc) {
		this.miejscowosc = miejscowosc;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getFaks() {
		return faks;
	}

	public void setFaks(String faks) {
		this.faks = faks;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getsenderCountry() {
		return senderCountry;
	}

	public void setsenderCountry(String senderCountry) {
		this.senderCountry = senderCountry;
	}


	public String getNumerDomu() {
		return numerDomu;
	}


	public void setNumerDomu(String numerDomu) {
		this.numerDomu = numerDomu;
	}


	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}



}