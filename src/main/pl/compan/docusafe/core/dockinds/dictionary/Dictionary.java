package pl.compan.docusafe.core.dockinds.dictionary;

import java.io.Serializable;
import java.util.List;
import pl.compan.docusafe.core.EdmException;

/**
 * DAO dla wpis�w w pewnym s�owniku.
 * @author Micha� Sankowski
 */
public interface Dictionary<T> {

	/**
	 * Wyszukuje rekord o danym id lub zwraca null
	 * @param id identyfikator wpisu s�ownikowego
	 * @return rekord w s�owniku lub null
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	T get(Serializable id) throws EdmException;

	/**
	 * Dodaje nowy wpis w s�owniku
	 * @param entry nowy wpis, kt�ry zostany dodany do s�ownika
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	void add(T entry) throws EdmException;

	/**
	 * Usuwa wpis w s�owniku
	 * @param entry wpis, kt�ry - je�li istnieje - zostanie usuni�ty z tego s�ownika
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	void remove(T entry) throws EdmException;

    /**
     * Aktualizuje wpis w bazie danych / cache'u
     * @param entry istniej�cy wpis
     * @throws EdmException
     */
    void update(T entry) throws EdmException;

	/**
	 * Zwraca wszystkie wpisy w s�owniku
	 * @return lista zawieraj�ca wszystkie wpisy w s�owniku
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	List<T> getAll() throws EdmException;
}
