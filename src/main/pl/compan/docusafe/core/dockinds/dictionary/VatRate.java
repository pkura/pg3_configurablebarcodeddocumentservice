package pl.compan.docusafe.core.dockinds.dictionary;

import org.joda.time.DateTime;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

/**
 * Słownik stawek VAT
 * @author Michał Sankowski <michal.sankowski@docusafe.pl>
 */
@Entity
@Table(name="dsg_vat_rate")
public class VatRate {
    private final static Logger LOG = LoggerFactory.getLogger(VatRate.class);

    @Id
    @SequenceGenerator(name = "dsg_vat_rate_seq", sequenceName = "dsg_vat_rate_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dsg_vat_rate_seq")
    Integer id;

    /**
     * Wartość w procentach stawki
     */
    @Column(name = "value", nullable = false)
    int value;

    /**
     * Symbol stawki
     */
    @Column(name = "symbol", length = 5, nullable = false)
    private String symbol;

    /**
     * Data obowiązywania od
     */
    @Column(name = "date_from", nullable = true)
    private Date from;

    /**
     * Data obowiązywania do
     */
    @Column(name = "date_to", nullable = true)
    private Date to;

    public static VatRate find(int id) throws EdmException{
		return (VatRate) DSApi.context().session().get(VatRate.class, id);
	}

    public static List<VatRate> list() throws EdmException {
		return DSApi.context().session().createCriteria(VatRate.class).list();
	}

    /**
     * Zwraca VatRate na podstawie sklejonych wartości w konwencji csv.
     * Przykładowe argumenty:
     * "7;B;1900-01-01;2010-12-31"
     * "8;B;2011-01-01;"
     * @param entry sklejone poprzez ';' stawka vat, symbol vat, data obowiązywania od, data obowiązywania do
     * @return VatRate jeśli jest dostępne, null w przeciwnym przypadku
     */
    public static VatRate fromCsvEntry(String entry) throws EdmException {
        List<VatRate> rates = VatRateDictionary.INSTANCE.getAll();        
        for(VatRate rate : rates) {
            if(rate.toCsvEntry().equals(entry)){
                return rate;
            }
        }
        return null;
    }

    /**
     * @return sklejone poprzez ';' stawka vat, symbol vat, data obowiązywania od, data obowiązywania do
     */
    public String toCsvEntry() {
        DateFormat df = (DateFormat) DateUtils.sqlDateFormat.clone();
        return value + ";" + symbol + ";" + df.format(from) + ";" + (to == null ? "" : df.format(to));
    }

    public String getNiceString(){
        return MessageFormat.format("{0}({1}%)", symbol, value);
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Date getTo() {
        return to;
    }

    public String getFromJsString() {
        return DateUtils.formatJsDateOrEmptyString(from);
    }

    public void setFromJsString(String jsString){
        from = DateUtils.parseJsDate(jsString);
    }

    public String getToJsString() {
        return DateUtils.formatJsDateOrEmptyString(to);
    }

    public void setToJsString(String jsString){
        to = DateUtils.parseJsDate(jsString);
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}