package pl.compan.docusafe.core.dockinds.dictionary;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
//import org.hibernate.validator.Length;
//import org.hibernate.validator.NotNull;
import org.hibernate.validator.constraints.Length;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
@Entity
@Table(name = "ic_customs_agency")
public class CustomsAgency {

	private final static Logger LOG = LoggerFactory.getLogger(CustomsAgency.class);

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)//ic = sql server
	private Integer id;

	@NotNull
	@Length(max=255)
	private String title;

	CustomsAgency(){
	}

	/**
	 * Pozycje nale�y tworzy� poprzez s�ownik
	 * @param title
	 */
	public CustomsAgency(String title){
		this.title = title;
	}

	public static List<CustomsAgency> list() throws EdmException{
		return DSApi.context().session().createCriteria(CustomsAgency.class).list();
	}

	public static CustomsAgency find(int id) throws EdmException{
		return (CustomsAgency) DSApi.context().session().get(CustomsAgency.class, id);
	}

	public Integer getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return super.toString() + "  " + getId();
	}


}
