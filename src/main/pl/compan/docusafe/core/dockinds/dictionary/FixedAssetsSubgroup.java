package pl.compan.docusafe.core.dockinds.dictionary;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.core.Finder;

/**
 *
 * @author Micha� Sankowski
 */
@Entity
@Table(name = "ic_fixed_assets_subgroup")
public class FixedAssetsSubgroup {

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY) //ic = sql server
	private Integer id;

	@NotNull
	@Length(max=255)
	private String title;

	@ManyToOne
	@JoinColumn(name="group_id")
	private FixedAssetsGroup group;

	private final static Logger log = LoggerFactory.getLogger(FixedAssetsSubgroup.class);

	FixedAssetsSubgroup(){
		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getId() {
		return id;
	}

	public FixedAssetsGroup getGroup() {
		return group;
	}

	public static FixedAssetsSubgroup find(Integer id) throws EdmException{
		return Finder.find(FixedAssetsSubgroup.class, id);
	}
}
