package pl.compan.docusafe.core.dockinds.dictionary;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "dsg_account_number_comment")
public class AccountNumberComment {
    @Id
    @SequenceGenerator(name = "dsg_account_number_comment_seq", sequenceName = "dsg_account_number_comment_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dsg_account_number_comment_seq")
    Integer id;

    @Column(name = "account_number")
    String accountNumber;

    @Column(name = "name")
    String name;
    
    @Column(name = "visible")
    boolean visible=true;

    public static AccountNumberComment find(int id) throws EdmException {
        return (AccountNumberComment) DSApi.context().session().get(AccountNumberComment.class, id);
    }

    public static Collection<AccountNumberComment> findForAccount(String accountNumber) throws EdmException {
        return DSApi.context().session()
                .createCriteria(AccountNumberComment.class)
                .add(Restrictions.eq("accountNumber", accountNumber))
                .addOrder(Order.asc("name"))
                .list();
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

    @Override
    public String toString() {
        return '"' + name + '"';
    }
}
