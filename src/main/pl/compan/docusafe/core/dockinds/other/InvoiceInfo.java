package pl.compan.docusafe.core.dockinds.other;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;

public class InvoiceInfo {
	
	
	public static Integer INVOICE_TYPE_KOSZT = 1;
	public static Integer INVOICE_TYPE_ZAKUP = 2;
	
	public static Integer STATUS_OK = 1;
    public static Integer STATUS_NOT_FOUND = 0;    
    public static Integer STATUS_NOT_EVERYTHING_MATCH = 3;
    public static Integer STATUS_TOO_MANY_FOUND = 4;
    public static Integer STATUS_PARSE_ERROR = 5; 
    public static Integer STATUS_WRONG_NIP = 6;
    public static Integer STATUS_WRONG_DATE = 7;
    public static Integer STATUS_WRONG_AMOUNT = 8;
    
    public static Integer PROCESS_STATUS_NEW = 1;
    public static Integer PROCESS_STATUS_DONE = 2;
    
    private Long id;
    private Integer lineNumber;
    private String invoiceNumber;
    private String nip;
    private Double amountBrutto;
    private Double origAmountBrutto;
    private Double vat;
    private Float amountNetto;
    private Float amountVat;
    private Date ctime;
    private Date invDate;
    private String creatingUser;
    private Integer status;
    private String errorInfo;
    private Long documentId;
    private Integer processStatus;
    private String numerKontrahenta;
    private Integer invoiceType;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Double getAmountBrutto() {
		return amountBrutto;
	}
	public void setAmountBrutto(Double amountBrutto) {
		this.amountBrutto = amountBrutto;
	}
	public Float getAmountNetto() {
		return amountNetto;
	}
	public void setAmountNetto(Float amountNetto) {
		this.amountNetto = amountNetto;
	}
	public Float getAmountVat() {
		return amountVat;
	}
	public void setAmountVat(Float amountVat) {
		this.amountVat = amountVat;
	}
	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	public String getCreatingUser() {
		return creatingUser;
	}
	public void setCreatingUser(String creatingUser) {
		this.creatingUser = creatingUser;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getErrorInfo() {
		return errorInfo;
	}
	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = DicInvoice.cleanNip(nip);
	}
	public Date getInvDate() {
		return invDate;
	}
	public void setInvDate(Date invDate) {
		this.invDate = invDate;
	}
	public Double getOrigAmountBrutto() {
		return origAmountBrutto;
	}
	public void setOrigAmountBrutto(Double origAmountBrutto) {
		this.origAmountBrutto = origAmountBrutto;
	}
	public Double getVat() {
		return vat;
	}
	public void setVat(Double vat) {
		this.vat = vat;
	}
	public Integer getProcessStatus() {
		return processStatus;
	}
	public void setProcessStatus(Integer processStatus) {
		this.processStatus = processStatus;
	}
	
	public String getNumerKontrahenta() {
		return numerKontrahenta;
	}
	public void setNumerKontrahenta(String numerKontrahenta) {
		this.numerKontrahenta = numerKontrahenta;
	}
	public Integer getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(Integer invoiceType) {
		this.invoiceType = invoiceType;
	}
	public String getStatusDescription()
    {
        if (STATUS_OK.equals(status))
            return "OK";
        else if (STATUS_NOT_FOUND.equals(status))
            return "nie znaleziono dokumentu";
        else if (STATUS_TOO_MANY_FOUND.equals(status))
            return "znaleziono wi�cej ni� 1 dokument";
        else if (STATUS_NOT_EVERYTHING_MATCH.equals(status))
            return "niezgodno�� kwoty";
        else if (STATUS_WRONG_DATE.equals(status))
            return "niezgodno�� daty wystawienia faktury";
        else if (STATUS_WRONG_NIP.equals(status))
            return "niezgodno�� nipu dostawcy";
        else if (STATUS_PARSE_ERROR.equals(status))
            return "b��d w pliku - "+errorInfo;
        else
            return null;
    }
	
	@SuppressWarnings("unchecked")
    public static List<InvoiceInfo> find(String username, String sortField, boolean asc) throws EdmException
    {        
        Criteria c = DSApi.context().session().createCriteria(InvoiceInfo.class);
        c.add(Restrictions.eq("creatingUser", username));
        if (sortField == null)
        {
            sortField = "lineNumber";
            asc = true;
        }        
        c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
        return (List<InvoiceInfo>) c.list();
    }
	
	public static List<InvoiceInfo> find(String username, String sortField, Integer type, boolean asc) throws EdmException
	{        
        Criteria c = DSApi.context().session().createCriteria(InvoiceInfo.class);
        c.add(Restrictions.eq("creatingUser", username));
        c.add(Restrictions.eq("invoiceType", type));
		if (sortField == null)
		{
			sortField = "lineNumber";
	        asc = true;
	    }        
	    c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
	    return (List<InvoiceInfo>) c.list();
	}

    
    public static InvoiceInfo find(Long id) throws EdmException
    {
        return Finder.find(InvoiceInfo.class, id);
    }
    

}
