package pl.compan.docusafe.core.dockinds;

import org.antlr.stringtemplate.AttributeRenderer;
import org.antlr.stringtemplate.StringTemplate;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class StringPatternResolver implements FolderResolver{
    private final static Logger LOG = LoggerFactory.getLogger(StringPatternResolver.class);

    private final String pattern;
    private final String splitter;

    public StringPatternResolver(String pattern, String splitter) {
        this.pattern = pattern;
        this.splitter = splitter;
    }

    public static void resolve(String pattern, String splitter, long documentId, Map<String, ?> additionalParams) throws EdmException {
        StringTemplate st = new StringTemplate(pattern);
        DocFacade doc = Documents.document(documentId);
        for(Map.Entry<String,?> entry : additionalParams.entrySet()){
            st.setAttribute(entry.getKey(), entry.getValue());
        }
        st.setAttribute("fields", doc.getFields());
        String result = st.toString();
        LOG.info("resolver result : '{}'", result);
        FolderInserter.root()
            .subfolders(result.split(Pattern.quote(splitter)))
            .insert(doc.getDocument());
    }

    public void insert(long documentId, Map<String, ?> additionalParams) throws EdmException {
        resolve(pattern, splitter, documentId, additionalParams);
    }
}
