package pl.compan.docusafe.core.dockinds.paa;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;


public class PlanPracyMulti {
	
	private Long ID;
    private Long DOCUMENT_ID;
    private String FIELD_CN;
    private String FIELD_VAL;
    
    
    
    
	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public Long getDOCUMENT_ID() {
		return DOCUMENT_ID;
	}

	public void setDOCUMENT_ID(Long dOCUMENT_ID) {
		DOCUMENT_ID = dOCUMENT_ID;
	}

	public String getFIELD_CN() {
		return FIELD_CN;
	}

	public void setFIELD_CN(String fIELD_CN) {
		FIELD_CN = fIELD_CN;
	}

	public String getFIELD_VAL() {
		return FIELD_VAL;
	}

	public void setFIELD_VAL(String fIELD_VAL) {
		FIELD_VAL = fIELD_VAL;
	}

	@SuppressWarnings("unchecked")
    public static List<PlanPracyMulti> findPodzadania(Long id) throws EdmException
    {        
		try{
	        Criteria c = DSApi.context().session().createCriteria(PlanPracyMulti.class);
	        c.add(Restrictions.eq("DOCUMENT_ID", id));
	        
	//        c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
	        return (List<PlanPracyMulti>) c.list();
		} catch (EdmException e){
			return null;
		}
    }
	
	@SuppressWarnings("unchecked")
	public static List<PlanPracyMulti> findPodzadania() throws EdmException
	{        
        Criteria c = DSApi.context().session().createCriteria(PlanPracyMulti.class);
        c.add(Restrictions.eq("TYP_OBIEKTU", 702));
                
	    //c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
	    return (List<PlanPracyMulti>) c.list();
	}

    
//    public static PlanPracyZad find(Long id) throws EdmException
//    {
//        return Finder.find(PlanPracyZad.class, id);
//    }
//    

}
