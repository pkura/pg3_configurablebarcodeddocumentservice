package pl.compan.docusafe.core.dockinds.paa;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;


public class PlanPracyPodzad {
	
    private Long DOCUMENT_ID;
    private Long MASTER_DOC;
    private Integer TYP_ZADANIA;
    private String OPIS;
    private Date DATAOD;
    private Date DATADO;
    private String STAN;
    private short PROCENT;
    private Long WYKONAWCA;
    private Long KOMORKA_WIODACA;


    
    public Long getDOCUMENT_ID() {
		return DOCUMENT_ID;
	}

	public void setDOCUMENT_ID(Long dOCUMENT_ID) {
		DOCUMENT_ID = dOCUMENT_ID;
	}

	public Long getMASTER_DOC() {
		return MASTER_DOC;
	}

	public void setMASTER_DOC(Long mASTER_DOC) {
		MASTER_DOC = mASTER_DOC;
	}

	public short getPROCENT() {
		return PROCENT;
	}

	public void setPROCENT(short pROCENT) {
		PROCENT = pROCENT;
	}

	public Integer getTYP_ZADANIA() {
		return TYP_ZADANIA;
	}

	public void setTYP_ZADANIA(Integer tYP_ZADANIA) {
		TYP_ZADANIA = tYP_ZADANIA;
	}

	public String getOPIS() {
		return OPIS;
	}

	public void setOPIS(String oPIS) {
		OPIS = oPIS;
	}

	public Date getDATAOD() {
		return DATAOD;
	}

	public void setDATAOD(Date dATAOD) {
		DATAOD = dATAOD;
	}

	public Date getDATADO() {
		return DATADO;
	}

	public void setDATADO(Date dATADO) {
		DATADO = dATADO;
	}

	public String getSTAN() {
		return STAN;
	}

	public void setSTAN(String sTAN) {
		STAN = sTAN;
	}

	public Long getWYKONAWCA() {
		return WYKONAWCA;
	}

	public void setWYKONAWCA(Long wYKONAWCA) {
		WYKONAWCA = wYKONAWCA;
	}

	public Long getKOMORKA_WIODACA() {
		return KOMORKA_WIODACA;
	}

	public void setKOMORKA_WIODACA(Long kOMORKA_WIODACA) {
		KOMORKA_WIODACA = kOMORKA_WIODACA;
	}


    public static PlanPracyPodzad getPodzadanie(Long id)
    {        
		try {
	        Criteria c = DSApi.context().session().createCriteria(PlanPracyPodzad.class);
	        c.add(Restrictions.eq("DOCUMENT_ID", id));
	        
	        return (PlanPracyPodzad) c.list().get(0);
		} catch (EdmException e)
		{
			return null;
		}
    }
	
	@SuppressWarnings("unchecked")
	public static List<PlanPracyPodzad> findPodzadania() throws EdmException
	{        
        Criteria c = DSApi.context().session().createCriteria(PlanPracyPodzad.class);
        c.add(Restrictions.eq("TYP_OBIEKTU", 702));
                
	    //c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
	    return (List<PlanPracyPodzad>) c.list();
	}
  
	@SuppressWarnings("unchecked")
	public static List<PlanPracyPodzad> list() throws EdmException
	{        
        Criteria c = DSApi.context().session().createCriteria(PlanPracyPodzad.class);
        //c.add(Restrictions.eq("TYP_OBIEKTU", 702));
                
	    //c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
	    return (List<PlanPracyPodzad>) c.list();
	}
  

}
