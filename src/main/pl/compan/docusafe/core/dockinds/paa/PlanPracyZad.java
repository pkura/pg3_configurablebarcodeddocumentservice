package pl.compan.docusafe.core.dockinds.paa;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;


public class PlanPracyZad {
	
    private Long DOCUMENT_ID;
    private Long MASTER_DOC;
    private Integer TYP_OBIEKTU;
    private Integer TYP_ZADANIA;
    private String OPIS;
    private Date DATAOD;
    private Date DATADO;
    private String STAN;
    private Long WYKONAWCA;
    private Long KOMORKA_WIODACA;


    
    public Long getDOCUMENT_ID() {
		return DOCUMENT_ID;
	}

	public void setDOCUMENT_ID(Long dOCUMENT_ID) {
		DOCUMENT_ID = dOCUMENT_ID;
	}

	public Long getMASTER_DOC() {
		return MASTER_DOC;
	}

	public void setMASTER_DOC(Long mASTER_DOC) {
		MASTER_DOC = mASTER_DOC;
	}

	public Integer getTYP_OBIEKTU() {
		return TYP_OBIEKTU;
	}

	public void setTYP_OBIEKTU(Integer tYP_OBIEKTU) {
		TYP_OBIEKTU = tYP_OBIEKTU;
	}

	public Integer getTYP_ZADANIA() {
		return TYP_ZADANIA;
	}

	public void setTYP_ZADANIA(Integer tYP_ZADANIA) {
		TYP_ZADANIA = tYP_ZADANIA;
	}

	public String getOPIS() {
		return OPIS;
	}

	public void setOPIS(String oPIS) {
		OPIS = oPIS;
	}

	public Date getDATAOD() {
		return DATAOD;
	}

	public void setDATAOD(Date dATAOD) {
		DATAOD = dATAOD;
	}

	public Date getDATADO() {
		return DATADO;
	}

	public void setDATADO(Date dATADO) {
		DATADO = dATADO;
	}

	public String getSTAN() {
		return STAN;
	}

	public void setSTAN(String sTAN) {
		STAN = sTAN;
	}

	public Long getWYKONAWCA() {
		return WYKONAWCA;
	}

	public void setWYKONAWCA(Long wYKONAWCA) {
		WYKONAWCA = wYKONAWCA;
	}

	public Long getKOMORKA_WIODACA() {
		return KOMORKA_WIODACA;
	}

	public void setKOMORKA_WIODACA(Long kOMORKA_WIODACA) {
		KOMORKA_WIODACA = kOMORKA_WIODACA;
	}

	//	public String getStatusDescription()
//    {
//        if (STATUS_OK.equals(status))
//            return "OK";
//        else if (STATUS_NOT_FOUND.equals(status))
//            return "nie znaleziono dokumentu";
//        else if (STATUS_TOO_MANY_FOUND.equals(status))
//            return "znaleziono wi�cej ni� 1 dokument";
//        else if (STATUS_NOT_EVERYTHING_MATCH.equals(status))
//            return "niezgodno�� kwoty";
//        else if (STATUS_WRONG_DATE.equals(status))
//            return "niezgodno�� daty wystawienia faktury";
//        else if (STATUS_WRONG_NIP.equals(status))
//            return "niezgodno�� nipu dostawcy";
//        else if (STATUS_PARSE_ERROR.equals(status))
//            return "b��d w pliku - "+errorInfo;
//        else
//            return null;
//    }
//	
	@SuppressWarnings("unchecked")
    public static List<PlanPracyZad> findGlowne() throws EdmException
    {        
        Criteria c = DSApi.context().session().createCriteria(PlanPracyZad.class);
        c.add(Restrictions.eq("TYP_OBIEKTU", 701));
        
//        c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
        return (List<PlanPracyZad>) c.list();
    }
	
	@SuppressWarnings("unchecked")
	public static List<PlanPracyZad> findPodzadania() throws EdmException
	{        
        Criteria c = DSApi.context().session().createCriteria(PlanPracyZad.class);
        c.add(Restrictions.eq("TYP_OBIEKTU", 702));
                
	    //c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
	    return (List<PlanPracyZad>) c.list();
	}

    
	public static  PlanPracyZad getZadanie(Long id){
		try {
			Criteria c = DSApi.context().session().createCriteria(PlanPracyZad.class);
	        c.add(Restrictions.eq("DOCUMENT_ID", id));
	        
	        return  (PlanPracyZad) c.list().get(0);
		} catch (EdmException e) {
			return null;
		}
		
	}
	
//    public static PlanPracyZad find(Long id) throws EdmException
//    {
//        return Finder.find(PlanPracyZad.class, id);
//    }
//    

}
