package pl.compan.docusafe.core.dockinds;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.Database.Column;
import pl.compan.docusafe.core.dockinds.Database.Table;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.ManualSortedNonColumnField;
import pl.compan.docusafe.core.dockinds.field.OrderPredicateProvider;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.parametrization.adm.AdmDwrPermissionFactory;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

/* User: Administrator, Date: 2007-03-07 11:12:17 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class DocumentKindsManager
{
    private static final Logger log = LoggerFactory.getLogger(DocumentKindsManager.class);
	private static  boolean searchByPermisionClass =  AvailabilityManager.isAvailable("search.dokind.document.action.ByPermisiionClass");
    private static boolean searchbyOfficeRole = AvailabilityManager.isAvailable("search.byOfficeRole");
    private static boolean searchWithoutPermissionTable= AvailabilityManager.isAvailable("search.withoutPermissionTable");
    /**
     * Wyszukiwanie dokument�w wed�ug zapytania {@link DockindQuery}, kt�re okre�la kryteria wyszukiwania.
     * <br/> <br/>
     * <b>UWAGA</b> W przypadku sortowania po polu typu {@link OrderPredicateProvider}, dla kt�rego metoda isManualSorted() zwraca true,
     * do zapytania nie jest doklejany cz�on orderBy dla tego pola oraz zwracane s� wszystkie wyniki wyszukania (bez
     * limitu).
     *
     * @throws EdmException
     */
    public static SearchResults<Document> search(DockindQuery query) throws EdmException
    {

    	//query.getDocumentKind()
        DocumentKind documentKind = query.getDocumentKind();
        
        
        
        		
        		
        		
        // ustalanie tabel, kt�re b�dziemy u�ywali
        FromClause from = new FromClause();
        Map<String,TableAlias> tables = new HashMap<String,TableAlias>();

        TableAlias  docTable = from.createTable("ds_document",true);
        tables.put("ds_document", docTable);
        
        TableAlias dockindTable = from.createJoinedTable(documentKind.getTablename(), true, docTable, from.INNER_JOIN, "$l.id = $r.document_id");
        tables.put(documentKind.getTablename(), dockindTable);



        //Jezeli ma byc opcja sortowania po senderze to musimy doda� join z dso_person
        TableAlias personTable = null;
//        if(AvailabilityManager.isAvailable("searchDockindDocuments.order.person"))
//        {


        //sortowanie po polach typu "document_*"
        List<OrderPredicateProvider.OrderColumn> orderColumns = new ArrayList<OrderPredicateProvider.OrderColumn>();
        boolean manualSorting = false;
        try {
            String cn = query.getDocumentKind().getCn();
            Table docKindTable = null;

            // todo - czy WSZYSTKIE pola typu "document_*" s� w KA�DEJ z tych tabel ?? ?? ?? ?? ?? ?? ?? ?? ?? -> nie -> doda� do OrderPredicateProvider sprawdzenie czy dopuszcza tak� tabel�
            // todo - czy pola typu "document_*" mog� byc JESZCZE w jakiej� tabeli ?? ?? ?? ?? ?? ?? ?? ?? ??

            if(query.getDocumentKind().getDockindInfo().getDocumentTypes().contains(DocumentType.INCOMING))
            {
            	docKindTable = new Table("dso_in_document");
            }
            else
            {
            	docKindTable = new Table("dso_out_document");
            }

            TableAlias middleDocTable = null;
            if (docKindTable != null) {
                FormQuery.OrderBy[] orderBy = query.getOrder();
                for (int i = 0; i < orderBy.length; i++) {
                    if (orderBy[i].getAttribute().startsWith("dockind_")) {
                        String fieldCn = orderBy[i].getAttribute().substring("dockind_".length());
                        Field field = documentKind.getFieldByCn(fieldCn);


                        if (ManualSortedNonColumnField.isManualSorted(field)) {
                            manualSorting = true;

                        } else if (field instanceof OrderPredicateProvider) {
                            OrderPredicateProvider orderProvider = (OrderPredicateProvider) field;

                            if (!orderProvider.isManualSorted()) {
                                OrderPredicateProvider.Order order = orderProvider.getOrder(docKindTable, fieldCn);

                                if (order != null) {

                                    TableAlias alias = docTable;
                                    if (!order.whetherPlaceInThis(new Table(docTable.getTable()))) {
                                        OrderPredicateProvider.LinkedTable[] linkedTables = order.getLinkedTablesIfStartFrom(new Table(docTable.getTable()));
                                        for (OrderPredicateProvider.LinkedTable lt : linkedTables) {
                                            alias = from.createJoinedTable(lt.getNextTable().getName(), true, alias, from.INNER_JOIN, "$l." + lt.getLinkPrevToNext() + " = $r." + lt.getLinkNextToPrev());
                                            tables.put(docKindTable.getName(), alias);
                                        }
                                    }

                                    boolean ascending = Boolean.valueOf(orderBy[i].isAscending());
                                    Column orderColumn = order.getOrderColumn();
                                    orderColumns.add(new OrderPredicateProvider.OrderColumn(orderColumn, alias, ascending));
                                }
                            }

                        } else {
                            //TODO: POPRZEDNIA WERSJA - POZOSTAWIONA W RAZIE WYKORZYSTANIA PRZEZ STARSZ� LOGIK�
                            Column orderColumn = null;
                            if (orderBy[i].getAttribute().equals("dockind_SENDER")) orderColumn = new Column("sender");
                            else if (orderBy[i].getAttribute().equals("dockind_RECIPIENT") || orderBy[i].getAttribute().equals("dockind_RECIPIENT_HERE"))
                                orderColumn = new Column("recipientUser");
                            if (orderColumn != null) {
                                if (middleDocTable == null) {
                                    middleDocTable = from.createJoinedTable(docKindTable.getName(), true, docTable, from.INNER_JOIN, "$l.id = $r.id");
                                    tables.put(docKindTable.getName(), middleDocTable);
                                }
                                personTable = from.createJoinedTable("dso_person", true, middleDocTable, from.INNER_JOIN, "$l." + orderColumn.getName() + " = $r.id");
                                tables.put("dso_person", personTable);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
//        }



        /**
        TableAlias dockindTable = from.createTable(documentKind.getTablename(), true);
        tables.put(documentKind.getTablename(), dockindTable);
        
        TableAlias docTable = from.createJoinedTable("ds_document", true, dockindTable,from.JOIN, "$l.document_id = $r.id");
        tables.put("ds_document", docTable);
        */
        TableAlias perm = null;
        if (query.isCheckPermissions() && !((DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD) ||
        	DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE)) &&searchbyOfficeRole ) && !searchWithoutPermissionTable)
        {
        	
            perm = from.createTable(DSApi.getTableName(DocumentPermission.class), true);
            tables.put(DSApi.getTableName(DocumentPermission.class), perm);
        }

        Map<String,DockindQuery.JoinInfo> map = query.getTables();
        for (String key : map.keySet())
        {
            DockindQuery.JoinInfo joinInfo = map.get(key);
            TableAlias alias;
            String table = joinInfo.isMultiple() ? key.split("#")[0] : key;
            if (joinInfo.isOuterJoin()) {
                alias = from.createJoinedTable(table, true, docTable, FromClause.LEFT_OUTER, "$l." + joinInfo.getJoinTableColumn() + "=$r." + joinInfo.getTableKeyColumn());
            } else {
                alias = from.createTable(table, true);
            }
            tables.put(key, alias);
        }

        WhereClause where = new WhereClause();

        // dodawanie warunk�w do zapytania, aby poprawnie z��czy� tabele
        //robie joina wiec to nie potrzebe
        //where.add(Expression.eqAttribute(docTable.attribute("id"), dockindTable.attribute("document_id")));
        //if (!DSApi.context().isAdmin() && query.isCheckPermissions())
        if (query.isCheckPermissions() && !((DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD) || DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE)
	        )&& searchbyOfficeRole))
        {
        	if (!searchWithoutPermissionTable)
            where.add(Expression.eqAttribute(docTable.attribute("id"), perm.attribute("document_id")));
        }


        
        
        //zapobieganie wyszukiwaniu dokumentow ktorym zostala cofnieta archwizacja
        //deleteDataOnDockindChange
        //if(!Boolean.parseBoolean(Docusafe.getAdditionProperty("deleteDataOnDockindChange")))
        //{
	        where.add(Expression.eq(docTable.attribute("dockind_id"), documentKind.getId()));
	        for (String key : map.keySet())
	        {
	            DockindQuery.JoinInfo joinInfo = map.get(key);
                if (!joinInfo.isOuterJoin()) {
                    if (joinInfo.isMultiple()) {
                        String[] parts = key.split("#");
                        where.add(Expression.eqAttribute(tables.get(key).attribute("document_id"),
                                docTable.attribute("id")));
                        where.add(Expression.eq(tables.get(key).attribute("field_cn"),
                                parts[1]));
                    } else
                        where.add(Expression.eqAttribute(tables.get(key).attribute(joinInfo.getTableKeyColumn()),
                                tables.get(joinInfo.getJoinTableName()).attribute(joinInfo.getJoinTableColumn())));
                }
            }

        //}
        
        // tworzenie fragmentu zapytania WHERE na podstawie warto�ci z formularza
        // wyszukiwania wype�nionego przez u�ytkownika
        query.visitQuery(new DockindQueryVisitor(tables, where));

        for(Expression ex : query.getExpressionList()){
            where.add(ex);
        }

        // tworzenie pozosta�ego fragmentu zapytania WHERE niezwi�zanego z polami formularza
        if (!(DSApi.context().isAdmin() && AvailabilityManager.isAvailable("search.show.deleted")))
        {
            where.add(Expression.eq(docTable.attribute("deleted"), Boolean.FALSE));
        }        
        //if (!DSApi.context().isAdmin() && query.isCheckPermissions())
        if (query.isCheckPermissions())
        {
        	DocumentLogic documentLogic = documentKind.logic();
        	DSDivision[] divisions = null;
        	if (searchbyOfficeRole && searchWithoutPermissionTable && AvailabilityManager.isAvailable("AdmDwrPermissionFactory")){
        				AdmDwrPermissionFactory.getPerrmissionsQuery(tables, from ,where,query,documentKind );
        }
        else if(searchbyOfficeRole)
        	{
        	if(searchByPermisionClass){
    			getPermissionQueryFromPerrmissionFactoryClass(tables, from ,where,query,documentKind ,perm);
        	}
        	else
        		{

    		 	if (!DSApi.context().isAdmin() && !(DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD) ||
             	    DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE)))
         	   	{	
    		 		
	    	        	where.add(Expression.eq(perm.attribute("name"), ObjectPermission.READ));	
	    	        	//pytanie o uprawnienie ANY zostanie - musi byc chociaz jeden warunek
	    	            Junction OR = Expression.disjunction().
	    	                add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.ANY));
	    	           
	    	            if (documentLogic.isPersonalRightsAllowed())
	    	        	{
	    	            	log.trace("dockind przewiduje szukania po uprawnieniach przypisanych osobie");
	    	            	OR.add(Expression.conjunction().
	    	                    add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.USER)).
	    	                    add(Expression.eq(perm.attribute("subject"), DSApi.context().getPrincipalName())));
	    	        	}
	    	            if(DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD))
                    	{
	    	            	List<DSDivision> divs = UserFactory.getInstance().getAllCanAssignDivisionsDivisionType();
		    	            divisions = divs.toArray(new DSDivision[divs.size()]);
	 	                     for (DSDivision division : divisions)
	 	                     {
	 	                    	 
	 	                    		 String guid = division.getGuid();
	     	                         if (documentLogic.isReadPermissionCode(guid))
	     	                         {
	     	                         	log.trace("grupa {} jest uprawnieniem dla dockindu",guid);
	     	                         	OR.add(Expression.conjunction().
	     	                         			add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.GROUP)).
	     	                         			add(Expression.eq(perm.attribute("subject"), guid)));
	     	                         }
	     	                         else
	     	                         {
	     	                         	log.trace("grupa {} nie jest uprawnieniem dla dockindu",guid);
	     	                         }
	 	                        
	 	                     }
                    	}
	    	            
	    	           where.add(OR);
         	        } 
    		 	
        		}
        	}
        		else
	        	{
	        		divisions =  DSApi.context().getDSUser().getDivisions();
	        		where.add(Expression.eq(perm.attribute("name"), ObjectPermission.READ));     
	            	//negative -- pozbedziemy sie tego -- nie uzywamy uprawnien negative
	            	//where.add(Expression.eq(perm.attribute("negative"), Boolean.FALSE));
	
	            	//pytanie o uprawnienie ANY zostanie - musi byc chociaz jeden warunek
	                Junction OR = Expression.disjunction().
	                    add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.ANY));
	                
	                if (documentLogic.isPersonalRightsAllowed())
	            	{
	                	log.trace("dockind przewiduje szukania po uprawnieniach przypisanych osobie");
	                	OR.add(Expression.conjunction().
	                        add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.USER)).
	                        add(Expression.eq(perm.attribute("subject"), DSApi.context().getPrincipalName())));
	            	}
	               
	                for (DSDivision division : divisions)
	                {
	                    // tylko grupy mog� mie� uprawnienia
	                    if (!division.isGroup())
	                        continue;
	                    String guid = division.getGuid();
	                    if (documentLogic.isReadPermissionCode(guid))
	                    {
	                    	log.trace("grupa {} jest uprawnieniem dla dockindu",guid);
	                    	OR.add(Expression.conjunction().
	                    			add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.GROUP)).
	                    			add(Expression.eq(perm.attribute("subject"), guid)));
	                    }
	                    else
	                    {
	                    	log.trace("grupa {} nie jest uprawnieniem dla dockindu",guid);
	                    }
	                }
	
	                where.add(OR);
        	}

        	
        }


		// tworzenie cz�� ORDER BY zapytania
        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(dockindTable, "document_id");

        OrderClause order = new OrderClause();

        FormQuery.OrderBy[] orderBy = query.getOrder();
        for (int i=0; i < orderBy.length; i++)
        {
            if ("id".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("id"), orderBy[i].isAscending());
                selectId.add(docTable, "id");
            }
            else if ("title".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("title"), orderBy[i].isAscending());
                selectId.add(docTable, "title");
            }
            else if ("ctime".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("ctime"), orderBy[i].isAscending());
                selectId.add(docTable, "ctime");
            }
            else if ("mtime".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("mtime"), orderBy[i].isAscending());
                selectId.add(docTable, "mtime");
            }
            else if (orderBy[i].getAttribute().startsWith("dockind_"))
            {
                try
                {
                    String fieldCn = orderBy[i].getAttribute().substring("dockind_".length());
                    Field field = documentKind.getFieldByCn(fieldCn);

                    if (!field.isMultiple()){ // UWAGA! dla typ�w z�o�onych sortowanie jest "r�czne"

                        if (Field.CLASS.equals(field.getType()))
                        {
                            try
                            {
                                Class c = Class.forName(field.getClassname());
                                String tableName = DSApi.getTableName(c);
                                String column = field.getDictionarySortColumn();
                                order.add(tables.get(tableName).attribute(column), orderBy[i].isAscending());
                                selectId.add(tables.get(tableName), column);
                            }
                            catch (ClassNotFoundException e)
                            {
                                throw new EdmException("Nie znaleziono klasy: "+field.getClassname());
                            }
                        }

                        if (manualSorting || field instanceof OrderPredicateProvider){
                            //obsluga tego typu p�l p�niej - wed�ug listy sortowanych p�l tworzonej przy dodawaniu join-�w.
                        }
                        else if ((fieldCn.equals("SENDER") || fieldCn.equals("RECIPIENT") || fieldCn.equals("RECIPIENT_HERE") || fieldCn.equals("SENDER_HERE")) && personTable != null) {
                            //TODO: POPRZEDNIA WERSJA - POZOSTAWIONA W RAZIE WYKORZYSTANIA PRZEZ STARSZ� LOGIK�
                            order.add(personTable.attribute("organization"), Boolean.valueOf(orderBy[i].isAscending()));
                            selectId.add(personTable, "organization");
                        } else {
                            String column = field.getColumn();
                            order.add(dockindTable.attribute(column), Boolean.valueOf(orderBy[i].isAscending()));
                            selectId.add(dockindTable, column);
                        }

                    }
                }
                catch (Exception e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }

        for(OrderPredicateProvider.OrderColumn orderColumn : orderColumns){
            order.add(orderColumn.getColumnAttribute(), orderColumn.isAscending);
            selectId.add(orderColumn.tableAlias, orderColumn.column.getName());
        }



        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(distinct "+docTable.getAlias()+".id)");

        SelectQuery selectQuery;

        int totalCount;
        try
        {
        	long start = System.currentTimeMillis();
            selectQuery = new SelectQuery(selectCount, from, where, null);


            ResultSet rs = selectQuery.resultSet();
            Date d = new Date();
            
            
            if (!rs.next())
            {
            	rs.close();
            	DSApi.context().closeStatement(rs.getStatement());
                throw new EdmException("Nie mo�na pobra� liczby dokument�w.");
            }

            totalCount = rs.getInt(1);

            rs.close(); 
            DSApi.context().closeStatement(rs.getStatement());
            long stepA = System.currentTimeMillis();
            long durationA = stepA - start;
            dumpBenchmark(durationA,selectQuery);
            List<Document> results =  new ArrayList<Document>(manualSorting || query.getLimit() > totalCount ? totalCount : query.getLimit());
            if (totalCount==0)
            {
                log.trace("zapytanie nie zwrocilo wynikow");
            	return new SearchResultsAdapter<Document>(results, query.getOffset(), totalCount,
                        Document.class);
            }
            selectQuery = new SelectQuery(selectId, from, where, order);
            if (!manualSorting && query.getLimit() > 0) {
                selectQuery.setMaxResults(query.getLimit());
                selectQuery.setFirstResult(query.getOffset());
            }            
            
            rs = selectQuery.resultSet();
            long stepB = System.currentTimeMillis();
            long durationB = stepB - stepA;
            dumpBenchmark(durationB,selectQuery);

            long rsStartTime = System.currentTimeMillis();
            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(DSApi.context().load(Document.class, id));
                long duration = System.currentTimeMillis()-rsStartTime;
                rsStartTime = System.currentTimeMillis();
                log.trace("Dokument {} - wyciagany {} [ms]",id,duration);
            }
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());
            
            int test1 = results.size();

            return new SearchResultsAdapter<Document>(results, query.getOffset(), totalCount,
                Document.class);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }                
    }
    
    public static List<Long> searchIdForUser(final DockindQuery query, String username) throws EdmException
    {
    	DocumentKind documentKind = query.getDocumentKind();

        FromClause from = new FromClause();
        Map<String,TableAlias> tables = new HashMap<String,TableAlias>();

        TableAlias  docTable = from.createTable("ds_document",true);
        tables.put("ds_document", docTable);
        
        TableAlias dockindTable = from.createJoinedTable(documentKind.getTablename(), true, docTable, from.INNER_JOIN, "$l.id = $r.document_id");
        tables.put(documentKind.getTablename(), dockindTable);

        TableAlias perm = null;
        if (!DSUser.findByUsername(username).isAdmin() && query.isCheckPermissions())
        {
            perm = from.createTable(DSApi.getTableName(DocumentPermission.class), true);
            tables.put(DSApi.getTableName(DocumentPermission.class), perm);
        }

        Map<String,DockindQuery.JoinInfo> map = query.getTables();
        for (String key : map.keySet())
        {
            DockindQuery.JoinInfo joinInfo = map.get(key);
            TableAlias alias;
            if (joinInfo.isMultiple())
            {
                String[] parts = key.split("#");
                alias = from.createTable(parts[0], true);
            }
            else
                alias = from.createTable(key, true);            
            tables.put(key, alias);
        }

        WhereClause where = new WhereClause();

        if (!DSUser.findByUsername(username).isAdmin() && query.isCheckPermissions())
        {
            where.add(Expression.eqAttribute(docTable.attribute("id"), perm.attribute("document_id")));
        }
        
        
       
	        where.add(Expression.eq(docTable.attribute("dockind_id"), documentKind.getId()));
	        for (String key : map.keySet())
	        {
	            DockindQuery.JoinInfo joinInfo = map.get(key);
	            if (joinInfo.isMultiple())
	            {                
	                String[] parts = key.split("#");
	                where.add(Expression.eqAttribute(tables.get(key).attribute("document_id"),
	                    docTable.attribute("id")));
	                where.add(Expression.eq(tables.get(key).attribute("field_cn"),
	                    parts[1]));
	            }
	            else
	                where.add(Expression.eqAttribute(tables.get(key).attribute(joinInfo.getTableKeyColumn()),
	                    tables.get(joinInfo.getJoinTableName()).attribute(joinInfo.getJoinTableColumn())));
	        }
        query.visitQuery(new DockindQueryVisitor(tables, where));
        
        if(!(DSUser.findByUsername(username).isAdmin() && AvailabilityManager.isAvailable("search.show.deleted")))
        	where.add(Expression.eq(docTable.attribute("deleted"), Boolean.FALSE));
        
        if (!DSUser.findByUsername(username).isAdmin() && query.isCheckPermissions())
        {
            where.add(Expression.eq(perm.attribute("name"), ObjectPermission.READ));
            where.add(Expression.eq(perm.attribute("negative"), Boolean.FALSE));
            
            Junction OR = Expression.disjunction().
                add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.ANY)).
                add(Expression.conjunction().
                    add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.USER)).
                    add(Expression.eq(perm.attribute("subject"), username)));

            DSDivision[] divisions = DSUser.findByUsername(username).getDivisions();

            for (DSDivision division : divisions)
            {
                if (!division.isGroup())
                    continue;
                OR.add(Expression.conjunction().
                    add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.GROUP)).
                    add(Expression.eq(perm.attribute("subject"), division.getGuid())));
            }

            where.add(OR);
        }


        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(dockindTable, "document_id");

        OrderClause order = new OrderClause();

        FormQuery.OrderBy[] orderBy = query.getOrder();
        for (int i=0; i < orderBy.length; i++)
        {
            if ("id".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("id"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(docTable, "id");
            }
            else if ("title".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("title"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(docTable, "title");
            }
            else if ("ctime".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("ctime"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(docTable, "ctime");
            }
            else if ("mtime".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("mtime"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(docTable, "mtime");
            }
            else if (orderBy[i].getAttribute().startsWith("dockind_"))
            {
                try
                {
                    String fieldCn = orderBy[i].getAttribute().substring("dockind_".length());
                    Field field = documentKind.getFieldByCn(fieldCn);
                    if (Field.CLASS.equals(field.getType()))
                    {
                        try
                        {
                            Class c = Class.forName(field.getClassname());       
                            String tableName = DSApi.getTableName(c);
                            String column = field.getDictionarySortColumn();
                            order.add(tables.get(tableName).attribute(column), Boolean.valueOf(orderBy[i].isAscending()));
                            selectId.add(tables.get(tableName), column);
                        }
                        catch (ClassNotFoundException e)
                        {
                            throw new EdmException("Nie znaleziono klasy: "+field.getClassname());
                        }
                    }
                    else
                    {
                        String column = field.getColumn();
                        order.add(dockindTable.attribute(column), Boolean.valueOf(orderBy[i].isAscending()));
                        selectId.add(dockindTable, column);
                    }
                }
                catch (Exception e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }


        SelectClause selectCount = new SelectClause();

        SelectQuery selectQuery;

        try
        {
        	selectQuery = new SelectQuery(selectCount, from, where, null);

            ResultSet rs = null;          
            selectQuery = new SelectQuery(selectId, from, where, order);
            /*if (query.getLimit() > 0) {
                selectQuery.setMaxResults(query.getLimit());
                selectQuery.setFirstResult(query.getOffset());
            } */           
            
            rs = selectQuery.resultSet();
            List<Long> results = new ArrayList<Long>(query.getLimit());

            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(id);
            }
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            return results;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }    	
    }
    
    
    /**
     * Metoda zapisuje informacje w razie bledu
     * @param duration
     * @param selectQuery
     */
    protected static void dumpBenchmark(long duration,SelectQuery selectQuery)
    {
    	if (duration > 500l)
        {
        	log.error("czas wykonania pytania = {}",duration);
        	log.error("Zapytanie {}",selectQuery.getSql()); 
        	if (selectQuery.getTraceParameters()!=null)
        	{
        		int licznik = 0;
        		for (Object param : selectQuery.getTraceParameters())
        		{
        			licznik++;
        			log.error("param {}={}",licznik, param.toString());
        		}
        	}
        	//selectQuery.get
        }
    }
    /**
     * Zwraca sta�� reprezentuj�c� typ dokumentu - u�ywane w {@link DocumentLogic}.
     * @param document
     */
    public static int getType(Document document)
    {
        if (document instanceof OfficeDocument)
        {            
            if (document.getType() == DocumentType.INCOMING)
                return DocumentLogic.TYPE_IN_OFFICE;
            else if (document.getType() == DocumentType.OUTGOING)
                return DocumentLogic.TYPE_OUT_OFFICE;
            else
                return DocumentLogic.TYPE_INTERNAL_OFFICE;   
        }
        else
            return DocumentLogic.TYPE_ARCHIVE;
    }
    private static void getPermissionQueryFromPerrmissionFactoryClass(Map<String, TableAlias> tables, FromClause from, WhereClause where, DockindQuery query,
			DocumentKind documentKind, TableAlias perm)
	{
    	PermissionManager  pm = PermissionManager.getInstance();
    	pm.getSearchPermisionFactory().getPerrmissionsQuery(tables, from, where, query, documentKind,perm);
    	
	}
    /**
     * Zwraca wszystkie pola (w postaci nazw kodowych) danego rodzaju w postaci napisu - opr�cz
     * p�l hidden i searchHidden 
     */
    public static String getAllDockindColumnsString(DocumentKind documentKind) throws EdmException
    {
        return new JSONArray(getAllDockindColumns(documentKind)).toString();
    }
    
    /**
     * Zwraca wszystkie pola (w postaci nazw kodowych) danego rodzaju w postaci listy - opr�cz
     * p�l hidden i searchHidden.
     */
    public static List<String> getAllDockindColumns(DocumentKind documentKind) throws EdmException
    {
        List<String> list = new ArrayList<String>();
        for (Field field : documentKind.getFields())
            if ((!field.isHidden() && !field.isSearchHidden()) || field.isSearchShow())
                list.add(field.getCn());
        return list;
    }
    
    /**
     * Dla wybranych nazw kodowych p�l danego rodzaju, zwraca map� (nazwa_kodowa,nazwa_pe�na) opisuj�ca te pola.
     */
    public static Map<String,String> getDockindColumns(DocumentKind documentKind, List<String> fieldCns) throws EdmException
    {
        Map<String,String> map = new LinkedHashMap<String, String>();
        for (String fieldCn : fieldCns)
        {
        	if(fieldCn.startsWith("#complex#"))
        		map.put(fieldCn, fieldCn.substring("#complex#".length()));
        	else
        	{
        		if(documentKind.getFieldByCn(fieldCn) != null)
        			map.put(fieldCn, documentKind.getFieldByCn(fieldCn).getName());
        	}
        }
        return map;
    }
    
    /**
     * Zwraca list� nazw kodowych p�l (wybranych przez aktualnie zalogowanego u�ytkownika w ustawieniach),
     * kt�re maj� by� wy�wietlane w wyszukiwarce.
     */
    public static List<String> getUserDockindColumns(DocumentKind documentKind) throws EdmException
    {
    	String tmp = DSApi.context().systemPreferences().node("search-documents").get("columns_"+documentKind.getCn(),getAllDockindColumnsString(documentKind));
        String columnsString = DSApi.context().userPreferences().node("search-documents").get("columns_"+documentKind.getCn(), tmp);
        List<String> list = new ArrayList<String>();

        if (!StringUtils.isEmpty(columnsString))
        {
            try
            {
                JSONArray array = new JSONArray(columnsString);
                for (int i=0; i < array.length(); i++)
                {
                    list.add(array.getString(i));
                }
            }
            catch (ParseException e)
            {
                list.clear();
            }
        }
        return list;
    }
    
}
