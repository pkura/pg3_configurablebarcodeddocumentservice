package pl.compan.docusafe.core.dockinds;

import pl.compan.docusafe.core.EdmException;

/**
 * Obiekt odpowiedzialny za wygenerowanie Schemy dla danej instancji FieldsManagera
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface SchemeResolver {
    SchemeResolver NULL = new NullSchemeResolver();

    DockindScheme provideSheme(FieldsManager fm) throws EdmException;

	DockindScheme provideSheme(FieldsManager fm, String activity) throws EdmException;
}

class NullSchemeResolver implements SchemeResolver {

    @Override
    public DockindScheme provideSheme(FieldsManager fm) throws EdmException {
        return null;
    }

	@Override
	public DockindScheme provideSheme(FieldsManager fm, String activity) {
		return null;
	}
}
