package pl.compan.docusafe.core.dockinds;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;
import java.util.Set;

/**
 * @author Mariusz Kilja�czyk
 */
public final class Condition implements Serializable
{
    private final Logger log = LoggerFactory.getLogger(Condition.class);
	public static final String OPERATOR_EQ = "equals";
	public static final String OPERATOR_GREATER = "greater";
	public static final String OPERATOR_LESS = "less";
	public static final String OPERATOR_GREATER_EQ = "greater_eq";
	public static final String OPERATOR_LESS_EQ = "less_eq";
	public static final String OPERATOR_IS_NULL = "is_null";
	public static final String OPERATOR_NOT_NULL = "not_null";
    public static final String OPERATOR_IN = "in";
		
	private String field;
	private String operator;
	private String value;

    //lazy init
    private Predicate<FieldsManager> fieldsManagerPredicate;
	
	public Condition()
	{
		
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Integer getOperatorAsInteger()
	{
		if(operator.equals(OPERATOR_EQ))return 0;
		if(operator.equals(OPERATOR_GREATER))return 1;
		if(operator.equals(OPERATOR_LESS))return -1;
		return 0;
	}
	
	public static Integer getOperatorAsInteger(String operator)
	{
		if(operator.equals(OPERATOR_EQ))return 0;
		if(operator.equals(OPERATOR_GREATER))return 1;
		if(operator.equals(OPERATOR_LESS))return -1;
		return 0;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public String toString()
	{
		return "::"+field+"::"+operator+"::"+value+"\n";
	}
	
	public String getPrettyOperator() {
		if (operator.equals(OPERATOR_EQ)) {
			return "=";
		}
		if (operator.equals(OPERATOR_GREATER)) {
			return ">";
		}
		if (operator.equals(OPERATOR_GREATER_EQ)) {
			return ">=";
		}
		if (operator.equals(OPERATOR_IS_NULL)) {
			return "pusty";
		}
		if (operator.equals(OPERATOR_LESS)) {
			return "<";
		}
		if (operator.equals(OPERATOR_LESS_EQ)) {
			return "<=";
		}
		if (operator.equals(OPERATOR_NOT_NULL)) {
			return "dowolny";
		}
		return operator;
	}


    public Predicate<FieldsManager> toFieldsManagerPredicate(){
        if(fieldsManagerPredicate == null){
            fieldsManagerPredicate = initFieldsManagerPredicate();
        }
        return fieldsManagerPredicate;
    }

    private abstract class UnsafePredicate implements Predicate<FieldsManager> {

        @Override
        public final boolean apply(FieldsManager fieldsManager) {
            try {
                return unsafeApply(fieldsManager);
            } catch (EdmException ex) {
                throw new RuntimeException(ex);
            }
        }

        public abstract boolean unsafeApply(FieldsManager fieldsManager) throws EdmException;
    }

    private Predicate<FieldsManager> initFieldsManagerPredicate() {
        if(getField() == null) {
            log.warn("Scheme : null field attribute, {}, returning always false", getField());
            return Predicates.alwaysFalse();
        } else if (getOperator().equals(Condition.OPERATOR_IS_NULL)) {
            return new UnsafePredicate() {
                @Override
                public boolean unsafeApply(FieldsManager fm) throws EdmException {
                    return fm.getKey(getField()) == null;
                }
            };
        } else if (getOperator().equals(Condition.OPERATOR_NOT_NULL)) {
            return new UnsafePredicate() {
                @Override
                public boolean unsafeApply(FieldsManager fm) throws EdmException {
                    return fm.getKey(getField()) != null;
                }
            };
        } else if (getOperator().equals(Condition.OPERATOR_IN)) {
            return new UnsafePredicate() {
                Set<String> in = ImmutableSet.copyOf(getValue().split(","));
                @Override
                public boolean unsafeApply(FieldsManager fm) throws EdmException {
                    return fm.getKey(getField()) != null 
                            && in.contains(fm.getKey(getField()).toString());
                }
            };
        } else {
            return new UnsafePredicate() {
                @Override
                public boolean unsafeApply(FieldsManager fm) throws EdmException {
                    return fm.getKey(getField()) != null
                            && fm.getKey(getField()).toString().compareTo(getValue()) == getOperatorAsInteger();
                }
            };
        }
    }
}
