package pl.compan.docusafe.core.dockinds;

import java.util.*;


import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.dockinds.field.ArchiveListener;
import pl.compan.docusafe.core.dockinds.field.CustomPersistence;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.ValidationRule;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.filter.UserFilter;
import pl.compan.docusafe.util.filter.UserFilters;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Klasa zawierajaca info o dockindzie, zgromadzimy tu takie informacje jak 
 * definicje pol, etykiet, etc,
 * @author wkutyla
 */
public class DockindInfoBean {
    private final static Logger LOG = LoggerFactory.getLogger(DockindInfoBean.class);

	private String cn;
	private Long dockindId;
	private org.dom4j.Document xml = null;
	private volatile List<Field> dockindFields = null;
	private volatile Map<String,Field> dockindFieldsMap = null;
    private volatile List<CustomPersistence> customPersistedFields = null;
	private String multipleTableName;
    /** Rodzaje dokument�w dost�pne dla danego dockindu - domy�lnie wszystkie */
    private Set<DocumentType> documentTypes = EnumSet.allOf(DocumentType.class);

	/*
	 * Statyczne kana�y dockindu (zdefiniowane w xml'u)
	 */
	private List<ProcessChannel> processChannels = Collections.emptyList();
    private List<ChannelProvider> channelProviders = Collections.emptyList();

	private List<LabelDefinition> labelsDefs = Collections.emptyList();
	private List<BoxLine> boxesLines = new ArrayList<BoxLine>();

    private SchemeResolver schemeResolver;
    private FolderResolver folderResolver;
    private Function<DocFacade, String> titleProvider;
    private Function<DocFacade, String> descriptionProvider;
    
    private List<Reminder> reminders = Lists.newArrayList();
    
    private final List<ArchiveListener> archiveListeners = Lists.newArrayList();
//    private List<DockindScheme> dockindScheme = new ArrayList<DockindScheme>();

	/**
     * cn g��wnego pola w tym typie (czyli pola, kt�re mo�e miec wp�yw na wygl�d formularza np.
     * ukrywanie pewnych p�l lub obligatoryjno�� pewnych p�l.
     * Powinno by� to pole o typie Field.ENUM
     */
    private String mainFieldCn;
    /**
     * okre�la czy obowi�zkowe jest dodawanie za��cznika podczas przyjmowania pisma
     */
    private boolean attachmentRequired;
    
    /**
     * lista aspektow dla dockind'u
     */
    //private List<Aspect> aspects = null;
    private Map<String, Aspect> aspectsMap = null;
    
	/**
	 *  Manager wszystkich procesow
	 */
	private ProcessManager processesDeclarations = ProcessManager.empty();

    /**
     * regu�y walidacji
     */
    private List<ValidationRule> validationRules;
    
    /** opis dost�pnych akceptacji dokumentu */
    
    private AcceptancesDefinition acceptancesDefinition;
    
    /**
     * dodatkowe w�a�ciwo�ci dla danego rodzaju dokumentu
     */
    private Map<String,String> properties = Collections.emptyMap();
    
    /** raporty dost�pne dla tego rodzaju dokumentu */
    private Map<String,Report> reports = Collections.emptyMap();
    
    /** Definicje dla icrcuttera */
    private Boolean isIcrCutterOn = null;
    private String  icrCutterFolder = null;
    private List<String> icrCutterCut = null;

	private UserFilter visibilityFilter = UserFilters.ALL_PASS;

    private UserFilter creatingFilter = UserFilters.ALL_PASS;

    /**
	 * szablony wydruk�w dla danego dokumentu
	 */
	private Map<String, String> templates = Collections.emptyMap();
	
    public DockindInfoBean(String CN){
    	cn = CN;
    }

	public org.dom4j.Document getXml() {
		return xml;
	}

	public void setXml(org.dom4j.Document xml) {
		this.xml = xml;
	}

	public List<Field> getDockindFields() {
		return dockindFields;
	}

	public void setDockindFields(List<Field> dockindFields) {
        Preconditions.checkNotNull(dockindFields);
		this.dockindFields = Collections.unmodifiableList(dockindFields);
        List<CustomPersistence> cpfields = Lists.newArrayList();
        for(Field field: this.dockindFields){
            if(field instanceof CustomPersistence){
                cpfields.add((CustomPersistence) field);
            }
        }
        this.customPersistedFields = cpfields;
	}

	public String getMultipleTableName() {
		return multipleTableName;
	}

	public void setMultipleTableName(String multipleTableName) {
		this.multipleTableName = multipleTableName;
	}

	public List<ProcessChannel> getStaticProcessChannels() {
		return processChannels;
	}

	public void setStaticProcessChannels(List<ProcessChannel> processChannels) {
		this.processChannels = processChannels;
	}

	public List<LabelDefinition> getLabelsDefs() {
		return labelsDefs;
	}

	public void setLabelsDefs(List<LabelDefinition> labelsDefs) {
		this.labelsDefs = labelsDefs;
	}

	public String getMainFieldCn() {
		return mainFieldCn;
	}

	public void setMainFieldCn(String mainFieldCn) {
		this.mainFieldCn = mainFieldCn;
	}

	public boolean isAttachmentRequired() {
		return attachmentRequired;
	}

	public void setAttachmentRequired(boolean attachmentRequired) {
		this.attachmentRequired = attachmentRequired;
	}

	public List<ValidationRule> getValidationRules() {
		return validationRules;
	}

	public void setValidationRules(List<ValidationRule> validationRules) {
		this.validationRules = validationRules;
	}

	public AcceptancesDefinition getAcceptancesDefinition() {
		return acceptancesDefinition;
	}

	public void setAcceptancesDefinition(AcceptancesDefinition acceptancesDefinition) {
		this.acceptancesDefinition = acceptancesDefinition;
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	public Map<String, Report> getReports() {
		return reports;
	}

	public void setReports(Map<String, Report> reports) {
		this.reports = reports;
	}

	public Boolean getIsIcrCutterOn() {
		return isIcrCutterOn;
	}

	public void setIsIcrCutterOn(Boolean isIcrCutterOn) {
		this.isIcrCutterOn = isIcrCutterOn;
	}

	public String getIcrCutterFolder() {
		return icrCutterFolder;
	}

	public void setIcrCutterFolder(String icrCutterFolder) {
		this.icrCutterFolder = icrCutterFolder;
	}

	public List<String> getIcrCutterCut() {
		return icrCutterCut;
	}

	public void setIcrCutterCut(List<String> icrCutterCut) {
		this.icrCutterCut = icrCutterCut;
	}

	public List<Aspect> getAspects() {
		if(aspectsMap == null)
			return null;
		
		return new ArrayList<Aspect>(aspectsMap.values());
	}

	public Map<String, Aspect> getAspectsMap() {
		return aspectsMap;
	}

	public void setAspectsMap(Map<String, Aspect> aspectsMap) {
		this.aspectsMap = aspectsMap;
	}

	/**
	 * @return the processesDeclarations
	 */
	public ProcessManager getProcessesDeclarations() {
		return processesDeclarations;
	}

	/**
	 * @param processesDeclarations the processesDeclarations to set
	 */
	public void setProcessesDeclarations(ProcessManager processesDeclarations) {
		this.processesDeclarations = processesDeclarations;
	}

	public void setDockindFieldsMap(Map<String,Field> dockindFieldsMap) {
        Preconditions.checkNotNull(dockindFieldsMap);
		this.dockindFieldsMap = dockindFieldsMap;
	}


	public Map<String,Field> getDockindFieldsMap() {
		return dockindFieldsMap;
	}

	public UserFilter getVisibilityFilter() {
		return visibilityFilter;
	}

	void setVisibilityFilter(UserFilter visibilityFilter) {
		this.visibilityFilter = visibilityFilter;
	}

    public UserFilter getCreatingFilter() {
        return creatingFilter;
    }

    public void setCreatingFilter(UserFilter creatingFilter) {
        this.creatingFilter = creatingFilter;
    }

    public Long getDockindId() {
		return dockindId;
	}

	public void setDockindId(Long dockindId) {
		this.dockindId = dockindId;
	}

	public List<BoxLine> getBoxesLines() {
		return boxesLines;
	}

	public void setBoxesLines(List<BoxLine> boxesLines) {
		this.boxesLines = boxesLines;
	}

    SchemeResolver getSchemeResolver() {
        return schemeResolver;
    }

    void setSchemeResolver(SchemeResolver schemeResolver) {
        this.schemeResolver = schemeResolver;
    }

    public List<ChannelProvider> getChannelProviders() {
        return channelProviders;
    }

    public void setChannelProviders(List<ChannelProvider> channelProviders) {
        this.channelProviders = channelProviders;
    }

    public List<CustomPersistence> getCustomPersistedFields() {
        return customPersistedFields;
    }

    public Set<DocumentType> getDocumentTypes() {
        return documentTypes;
    }

    public void setDocumentTypes(Set<DocumentType> documentTypes) {
        Preconditions.checkNotNull(documentTypes, "documentTypes cannot be null");
        this.documentTypes = documentTypes;
    }

    public boolean isForType(DocumentType type){
        return documentTypes.contains(type);
    }

    public FolderResolver getFolderResolver() {
        return folderResolver;
    }

    public void setFolderResolver(FolderResolver folderResolver) {
        this.folderResolver = folderResolver;
    }

    public Function<DocFacade, String> getDescriptionProvider() {
        return descriptionProvider;
    }

    public void setDescriptionProvider(Function<DocFacade, String> descriptionProvider) {
        LOG.info("setDescriptionProvider {}", descriptionProvider);
        this.descriptionProvider = descriptionProvider;
    }

    public Function<DocFacade, String> getTitleProvider() {
        return titleProvider;
    }

    public void setTitleProvider(Function<DocFacade, String> titleProvider) {
        LOG.info("setTitleProvider {}", titleProvider);
        this.titleProvider = titleProvider;
    }

    public synchronized void addArchiveListener(ArchiveListener al){
        checkNotNull(al);
        archiveListeners.add(al);
    }

    public List<ArchiveListener> getArchiveListeners() {
        return archiveListeners;
    }

	public List<Reminder> getReminders() {
		return reminders;
	}
	
	public List<Reminder> getReminders(String period) {
		List<Reminder> specifiedReminders = Lists.newArrayList();
		
		for (Reminder reminder : reminders) {
			if (reminder.getPeriod().equals(period)) {
				specifiedReminders.add(reminder);
			}
		}
		
		return specifiedReminders;
	}

	public void setReminders(List<Reminder> reminders) {
		this.reminders = reminders;
	}
	
	public void addReminder(Reminder reminder) {
		this.reminders.add(reminder);
	}

	public Map<String, String> getTemplates() {
		return templates;
	}

	public void setTemplates(Map<String, String> templates) {
		this.templates = templates;
	}

    public String getCn() {
        return cn;
    }
}
