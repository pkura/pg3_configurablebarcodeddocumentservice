package pl.compan.docusafe.core.dockinds;

public class PdfLayerParams {
	
	/**Od lewej*/
	private float llx = 250;
	/**Od g�ry*/
	private float lly = 100;
	private float llrotation = 20;
	/**Nazwa warstwy*/
	private String llname = "Znak";
	/**Wielko�c znak�w*/
	private float llSize = 28;
	/**Tekst*/
	private String lltext = "";
	
	
	public PdfLayerParams() {
		super();
	}
	
	
	public PdfLayerParams(float llx, float lly, float llrotation,
			String llname, float llSize, String lltext) {
		super();
		this.llx = llx;
		this.lly = lly;
		this.llrotation = llrotation;
		this.llname = llname;
		this.llSize = llSize;
		this.lltext = lltext;
	}
	
	public float getLlx() {
		return llx;
	}
	public void setLlx(float llx) {
		this.llx = llx;
	}
	public float getLly() {
		return lly;
	}
	public void setLly(float lly) {
		this.lly = lly;
	}
	public float getLlrotation() {
		return llrotation;
	}
	public void setLlrotation(float llrotation) {
		this.llrotation = llrotation;
	}
	public String getLlname() {
		return llname;
	}
	public void setLlname(String llname) {
		this.llname = llname;
	}
	public float getLlSize() {
		return llSize;
	}
	public void setLlSize(float llSize) {
		this.llSize = llSize;
	}
	public String getLltext() {
		return lltext;
	}
	public void setLltext(String lltext) {
		this.lltext = lltext;
	}
	
	

}
