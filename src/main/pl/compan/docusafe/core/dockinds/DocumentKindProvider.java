package pl.compan.docusafe.core.dockinds;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Klasa s�u��ca do wyszukiwania DocumentKind
 *
 * Obiekt zwracany przez get() s�u�y do budowania listy Dockind�w spe�niaj�cych odpowiednie warunki
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DocumentKindProvider {

    private final LinkedList<DocumentKind> list;

    private DocumentKindProvider() throws EdmException {
        list = new LinkedList<DocumentKind>(DocumentKind.list(true));
    }

    public static DocumentKindProvider get() throws EdmException{
        return new DocumentKindProvider();
    }

    /**
     * Pozostawia DK widoczne dla danego u�ytkownika
     * @return this
     */
    public DocumentKindProvider visible(){
        for(ListIterator<DocumentKind> it = list.listIterator(); it.hasNext();){
            DocumentKind kind = it.next();
            if(!kind.getDockindInfo().getVisibilityFilter().acceptCurrentUser()){
                it.remove();
            }
        }
        return this;
    }

    /**
     * Pozostawia DK widoczne dla danego u�ytkownik przy tworzeniu.
     * @return this
     */
    public DocumentKindProvider canCreate(String currentDocumentKindCn, boolean newDocument){
        for(ListIterator<DocumentKind> it = list.listIterator(); it.hasNext();){
            DocumentKind kind = it.next();
            if (newDocument){
                if(!kind.getDockindInfo().getCreatingFilter().acceptCurrentUser()){
                    it.remove();
                }
            }else{
                if(!kind.getDockindInfo().getCreatingFilter().acceptCurrentUser()
                        && !kind.getCn().equalsIgnoreCase(currentDocumentKindCn)){
                    it.remove();
                }
            }
        }
        return this;
    }

    /**
     * Sprawdza czy dany uzytkownik ma mo�liwo�� utworzenia danego rodzaju dokumentu
     * @return this
     */
    public static boolean ifCurrentUserCanCreate(String dockindCn) throws EdmException {
        DocumentKind kind = DocumentKind.findByCn(dockindCn);
        return kind.getDockindInfo().getCreatingFilter().acceptCurrentUser();
    }

    /**
     * Pozostawia DK dost�pne dla danego typu dokumentu (in, out etc)
     * @return this
     */
    public DocumentKindProvider type(DocumentType type){
        for(ListIterator<DocumentKind> it = list.listIterator(); it.hasNext();){
            DocumentKind kind = it.next();
            if(!kind.getDockindInfo().isForType(type)){
                it.remove();
            }
        }
        return this;
    }

    /**
     * Pozostawia DK dost�pne dla r�cznego tworzenia dokumentu
     * @return this
     */
    public DocumentKindProvider forManualCreate() throws EdmException {
        for(ListIterator<DocumentKind> it = list.listIterator(); it.hasNext();){
            DocumentKind kind = it.next();
            DocumentLogic logic = kind.logic();
            if(logic == null){
                throw new NullPointerException("null logic for '" + kind.getCn()+ "'");
            }
            if(!logic.canCreateManually()
                    || !logic.canReadDocumentDictionaries()){
                it.remove();
            }
        }
        return this;
    }

    public DocumentKindProvider forEdit() throws EdmException {
        for(ListIterator<DocumentKind> it = list.listIterator(); it.hasNext();){
            DocumentKind kind = it.next();
            if(!kind.logic().canCreateManually()){
                it.remove();
            }
        }
        return this;
    }

    /**
     * Zwraca przechowywan� list�
     * @return this
     */
    public List<DocumentKind> list(){
	   Collections.sort(list, new DocumentKind.DocumentKindComparatorByName());
        return Collections.unmodifiableList(list);
    }
}
