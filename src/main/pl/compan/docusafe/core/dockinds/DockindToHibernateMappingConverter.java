package pl.compan.docusafe.core.dockinds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.mvel2.optimizers.impl.refl.nodes.ArrayLength;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DockindToHibernateMappingConverter {

	private static Logger log = LoggerFactory.getLogger(DockindToHibernateMappingConverter.class);
	
	public static final String HB_ID = "id";
	public static final String HB_NAME = "name";
	public static final String HB_TYPE = "type";

	public static final String HB_ACCESS = "access";
	public static final String HB_FIELD = "field";
	public static final String HB_PROPERTY = "property";
	public static final String HB_CLASS_NAME = "ClassName";
	
	public static final String HB_UNSAVED_VALUE = "unsaved-value";
	public static final String HB_NULL = "null";
	public static final String HB_ANY = "any";
	public static final String HB_NONE = "none";
	public static final String HB_UNDEFINED = "undefined";
	public static final String HB_ID_VALUE = "id_value";
	
	public static final String HB_NODE = "node";
	public static final String HB_ELEMENT_NAME = "element-name";
	public static final String HB_ATTRIBUTE_NAME = "@attribute-name";
	public static final String HB_ELEMENT_ATTRIBUTE = "element/@attribute";
	
	public static final String HB_GENERATOR = "generator";
	public static final String HB_CLASS = "class";
	public static final String HB_NATIVE = "native";
			
	public static final String HB_PARAM = "param";
	public static final String HB_SEQUENCE = "sequence";
	
	public static final String HB_COLUMN = "column";
	public static final String HB_UPDATE = "update";
	public static final String HB_INSERT = "insert";
	public static final String HB_FORMULA = "formula";
	public static final String HB_LAZY = "lazy";
	public static final String HB_UNIQUE = "unique";
	public static final String HB_NOT_NULL = "not-null";
	public static final String HB_OPTIMISTIC_LOCK = "optimistic-lock";
	public static final String HB_GENERATED = "generated";
	public static final String HB_INDEX = "index";
	public static final String HB_UNIQUE_KEY = "unique-key";
	public static final String HB_LENGTH = "length";
	public static final String HB_PRECISION = "precision";
	public static final String HB_SCALE = "scale";
	
	public static final String FALSE = "false";
	public static final String TRUE = "true";
	
	public static final String HB_HIBERNATE_MAPPING = "hibernate-mapping";
	public static final String HB_DEFAULT_LAZY = "default-lazy";
	public static final String HB_TABLE = "table";
	public static final String DOCKIND_DEFAULT_TABLE_PREFIX = "dsg";
	public static final String DOCKIND_DEFAULT_CLASS_TYPE = "java.lang.String";
	public static final String TABLE_DEFAULT_ID = "document_id";
	public static final String HB_DEFAULT_ID_TYPE = "long";
		
	/** 
	 	Mapowanie aktualnych [30-11-2012] typow pol dockindow do typow Hibernate.
		W przypadku nie uwzglednienia danego typu pola dockinda konwersja plikow zostaje przerwana.
		Je�li dany typ pola dockinda nie ma zosta� uwzgledniony podczas konwersji trzeba doda� go do mapy z warto�cia pustego Stringa
	 */
	private static Map<String, String> dataTypeMapping;
	static
	{
		dataTypeMapping = new HashMap<String, String>();
//		current [30-11-2012] dockinds types specified in Field.class
//		SQL DATE
		dataTypeMapping.put(Field.DATE,"date");
//		SQL TIMESTAMP
		dataTypeMapping.put(Field.TIMESTAMP,"timestamp");
//		SQL VARCHAR
		dataTypeMapping.put(Field.STRING,"string");
//		SQL VARCHAR
		dataTypeMapping.put(Field.BOOL,"boolean");
//		SQL NUMERIC
		dataTypeMapping.put(Field.ENUM,"integer");
//		SQL NUMERIC
		dataTypeMapping.put(Field.ENUM_REF,"integer");
//		SQL FLOAT
		dataTypeMapping.put(Field.FLOAT,"float");
//		SQL DOUBLE
		dataTypeMapping.put(Field.DOUBLE,"double");
//		SQL INTEGER
		dataTypeMapping.put(Field.INTEGER,"integer");
//		SQL LONG
		dataTypeMapping.put(Field.LONG,"long");
//		SQL INTEGER
		dataTypeMapping.put(Field.CLASS,"integer");
//		SQL INTEGER
		dataTypeMapping.put(Field.DATA_BASE,"integer");
//		SQL NUMERIC
		dataTypeMapping.put(Field.MONEY,"big_decimal");
		dataTypeMapping.put(Field.DSDIVISION,"integer");
		dataTypeMapping.put(Field.DSUSER,"integer");
		dataTypeMapping.put(Field.ATTACHMENT,"big_integer");
//		SQL INTEGER
		dataTypeMapping.put(Field.DICTIONARY,"integer");
//		currently not mapped
//		dataTypeMapping.put(Field.DOCUMENT_PERSON,"");		
//		Some other Hibernate data type mappings -> more info on http://docs.jboss.org/hibernate/core/3.6/reference/en-US/html_single/#types-value
//		SQL appropriate types
		dataTypeMapping.put("int","integer");
		dataTypeMapping.put("Long","long");
		dataTypeMapping.put("Double","double");
		dataTypeMapping.put("char","character");
		dataTypeMapping.put("Character","character");
		dataTypeMapping.put("boolean","boolean");
		dataTypeMapping.put("Boolean","boolean");
//		SQL LONGVARCHAR or TEXT
		dataTypeMapping.put("String","text");
//		SQL NUMERIC
		dataTypeMapping.put("BigDecimal","big_decimal");
		dataTypeMapping.put("BigInteger","big_integer");
//		SQL VARCHAR
		dataTypeMapping.put("String","string");
		dataTypeMapping.put("Locale","locale");
		dataTypeMapping.put("TimeZone","timezone");
		dataTypeMapping.put("Currency","currency");
		dataTypeMapping.put("Class","class");
//		SQL CLOB or BLOB
		dataTypeMapping.put("java.sql.Clob","clob");
		dataTypeMapping.put("java.sql.Blob","blob");
	}
	
	private final static List<String> classes;
	static
	{
		classes = new ArrayList<String>();
		classes.add("java.lang.Long");
		classes.add("java.lang.Boolean");
		classes.add("java.lang.Byte");
		classes.add("java.lang.Double");
		classes.add("java.lang.Float");
		classes.add("java.lang.Integer");
		classes.add("java.lang.Number");
	}
	
	private final static String xPathNormalFields 	= "/*[name()='doctype']/*[name()='fields']/*[name()='field'][@column!='']";
	
	private Document hbmDocument;
			
	public Document convertToHBMThrowsException(Document dockindDocument, String dockindTableName, String xPath, int clazzCt) throws EdmException
	{
		try
		{
			this.createDicHBMDocumentCore(dockindTableName, clazzCt);
			this.processDockindDocument(dockindDocument,xPath);
			return getHbmDocument();
		}
		catch(Exception e) { throw new EdmException(e.getMessage(),e); }
	}
	
	public Document convertToHBMThrowsException(Document dockindDocument, String dockindTableName) throws EdmException
	{
		try
		{
			this.createHBMDocumentCore(dockindTableName);
			this.processDockindDocument(dockindDocument,xPathNormalFields);
			return getHbmDocument();
		}
		catch(Exception e) { throw new EdmException(e.getMessage(),e); }
	}
	
	private void createHBMDocumentCore(String dockindTableName){
		hbmDocument = DocumentHelper.createDocument();
		hbmDocument.addDocType(HB_HIBERNATE_MAPPING, "-//Hibernate/Hibernate Mapping DTD 3.0//EN", "http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd");
		hbmDocument.addElement(HB_HIBERNATE_MAPPING)
			.addAttribute(HB_DEFAULT_LAZY, FALSE)
			.addElement(HB_CLASS)
			.addAttribute(HB_NAME, DOCKIND_DEFAULT_CLASS_TYPE)
			.addAttribute(HB_TABLE, dockindTableName)
			.addElement(HB_ID)
			.addAttribute(HB_ACCESS, HB_FIELD)
			.addAttribute(HB_NAME, TABLE_DEFAULT_ID)
			.addAttribute(HB_TYPE, HB_DEFAULT_ID_TYPE);
			//.addElement(HB_GENERATOR)
			//.addAttribute(HB_CLASS, HB_NATIVE)
			//.addElement(HB_PARAM)
			//.addAttribute(HB_NAME, HB_SEQUENCE)
			//.addText(dockindTableName+"_"+TABLE_DEFAULT_ID);
	}
	
	private void createDicHBMDocumentCore(String dockindTableName, int clazz){
		hbmDocument = DocumentHelper.createDocument();
		hbmDocument.addDocType(HB_HIBERNATE_MAPPING, "-//Hibernate/Hibernate Mapping DTD 3.0//EN", "http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd");
		hbmDocument.addElement(HB_HIBERNATE_MAPPING)
			.addAttribute(HB_DEFAULT_LAZY, FALSE)
			.addElement(HB_CLASS)
			.addAttribute(HB_NAME, classes.get(clazz))
			.addAttribute(HB_TABLE, dockindTableName)
			.addElement(HB_ID)
			.addAttribute(HB_ACCESS, HB_FIELD)
			.addAttribute(HB_NAME, "ID")
			.addAttribute(HB_TYPE, HB_DEFAULT_ID_TYPE)
			.addElement(HB_GENERATOR)
			.addAttribute(HB_CLASS, HB_NATIVE)
			.addElement(HB_PARAM)
			.addAttribute(HB_NAME, HB_SEQUENCE)
			.addText(dockindTableName+"_"+TABLE_DEFAULT_ID);
	}
	
	private void processDockindDocument(Document dockindDocument, String xPath) throws EdmException
	{
		for(Object o1 : dockindDocument.selectNodes(xPath))
		{
			createHBMProperty((Element) o1);
		}		
	}
	
	
	private void createHBMProperty(Element dockindField) throws EdmException{
		Element dockindFieldType = dockindField.element("type");
		String dockindFieldTypeValue = dockindFieldType.attributeValue("name");
		String hbmDataTypeValue = dataTypeMapping.get(dockindFieldTypeValue);
		if(hbmDataTypeValue != null && (dockindField.attributeValue("column") != null || dockindField.attributeValue("column").length() > 0))
		{
			if(hbmDataTypeValue.equals(""))
			{
				log.info("INFO: dla field CN='" + dockindField.attributeValue("cn") + "' i type name='" + dockindFieldTypeValue + "' nie przewidziano odpowiadajacej mu konwersji do pliku mapowania Hibernate");
			}
			else if("ID".equalsIgnoreCase(dockindField.attribute("cn").getText()))
			{
				log.info("INFO: id ze slownika");
			}
			else
			{
				Element hbmProperty = hbmDocument.getRootElement().element(HB_CLASS).addElement(HB_PROPERTY);
				hbmProperty.addAttribute(HB_ACCESS, HB_FIELD);
				hbmProperty.addAttribute(HB_NOT_NULL, FALSE);
				hbmProperty.addAttribute(HB_TYPE, hbmDataTypeValue);
				hbmProperty.addAttribute(HB_COLUMN, dockindField.attributeValue("column"));
				hbmProperty.addAttribute(HB_NAME, dockindField.attributeValue("column"));
				if("string".equalsIgnoreCase(hbmDataTypeValue))
				{
					hbmProperty.addAttribute("length",dockindField.element("type") != null && dockindField.element("type").attribute("length") != null ? dockindField.element("type").attribute("length").getText() : "50");
				}
			}
			
			if(dockindFieldTypeValue.equals(Field.DICTIONARY))
			{
				//TODO: odpalic create dla slownika
			}
		}		
	}

	public Document getHbmDocument() {
		return hbmDocument;
	}

	public void setHbmDocument(Document hbmDocument) {
		this.hbmDocument = hbmDocument;
	}

}
