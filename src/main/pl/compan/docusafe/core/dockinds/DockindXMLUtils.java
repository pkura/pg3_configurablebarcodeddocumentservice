package pl.compan.docusafe.core.dockinds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;

public class DockindXMLUtils 
{
	
	private static final Map<String,String> handlers = new HashMap<String,String>();
	static
	{
		handlers.put("pl.compan.docusafe.core.jbpm4.BooleanDecisionHandler", pl.compan.docusafe.core.dockinds.field.Field.BOOL);
		handlers.put("pl.compan.docusafe.core.jbpm4.MoneyDecisionHandler", pl.compan.docusafe.core.dockinds.field.Field.MONEY);
		handlers.put("pl.compan.docusafe.core.jbpm4.EnumCnDecisionHandler", pl.compan.docusafe.core.dockinds.field.Field.ENUM);
	}
	
	public static Map<String,Map<String,Set<String>>>  getRequiredFieldsValuesFromProcess(Document xml)
	{
		return getRequiredFieldsValuesFromProcess(xml, handlers.keySet()); 
	}
	
	private static Map<String,Map<String,Set<String>>>  getRequiredFieldsValuesFromProcess(Document xml, Set<String> classes)
	{
		Map<String,Map<String,Set<String>>> retMap = new HashMap<String,Map<String,Set<String>>>();
		String xPath;
		Attribute at = null;
		Element el = null;
		for(String clazz : classes)
		{
			xPath = "/*[name()='process']/*[name()='decision']/*[name()='handler'][@class='"+clazz+"']/*[name()='field'][@name='field']/*[name()='string']/@value";
			for(Object atr : xml.selectNodes(xPath))
			{
				at = (Attribute) atr;
				retMap.put(at.getText(), new HashMap<String,Set<String>>());
				if(retMap.get(at.getText()).get("TYP") == null)
					retMap.get(at.getText()).put("TYP", new HashSet<String>());
				retMap.get(at.getText()).get("TYP").add(handlers.get(at.getParent().getParent().getParent().attributeValue("class")));
				if(retMap.get(at.getText()).get("VALUE") == null)
					retMap.get(at.getText()).put("VALUE", new HashSet<String>());
				for(Object trans : at.getParent().getParent().getParent().getParent().elements("transition"))
				{
					el = (Element) trans;
					retMap.get(at.getText()).get("VALUE").add(el.attribute("name").getText());
				}
				
			}
		}
		
		return retMap;
	}
	
	@Deprecated
	public static Set<String> getRequiredFieldsFromProcess(Document xml, Set<String> classes)
	{
		Set<String> retSet = new HashSet<String>();
		String xPath;
		for(String clazz : classes)
		{
			xPath = "/*[name()='process']/*[name()='decision']/*[name()='handler'][@class='"+clazz+"']/*[name()='field'][@name='field']/*[name()='string']/@value";
			for(Object obj : xml.selectNodes(xPath))
				retSet.add(((Attribute) obj).getText());
		}
		return retSet;
	}
}
