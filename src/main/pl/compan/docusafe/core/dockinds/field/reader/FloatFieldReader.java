package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.FloatColumnField;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.core.dockinds.field.FloatNonColumnField;
import pl.compan.docusafe.core.dockinds.field.NonColumnField;

public class FloatFieldReader implements FieldReader
{

    @Override
    public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {
        String column = el.attributeValue("column");

        if (column == null) {
            NonColumnField field;
            field = new FloatNonColumnField((el.attributeValue("id")), el.attributeValue("cn"), names, required, requiredWhen, availableWhen);
            return field;

        } else {
            Field field;
            field = new FloatColumnField((el.attributeValue("id")), el.attributeValue("cn"), el.attributeValue("column"), names, required, requiredWhen, availableWhen);
            return field;
        }
    }

}
