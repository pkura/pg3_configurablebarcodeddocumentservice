package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static pl.compan.docusafe.util.I18nUtils.string;

public class DocumentAutorDivisionField  extends NonColumnField implements CustomPersistence, EnumNonColumnField, SearchPredicateProvider
{

	private static final Logger log = LoggerFactory.getLogger(DocumentAutorDivisionField.class);
	
	private List<EnumItem> enumItems = new ArrayList<EnumItem>();

	
	public DocumentAutorDivisionField(String id, String type, String cn, String logic, Map<String, String> names, boolean required,
			List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, names, Field.DOCUMENT_AUTOR_DIVISION, null, null, required, requiredWhen, availableWhen);



		for (DSDivision division : DSDivision.getAllDivisionsDesc())
		{
			Map<String, String> titles = new HashMap<String, String>();
			if (division.isDivision())
			{
				titles.put("pl", division.getName());
				if (division.getId() > (long) Integer.MAX_VALUE)
				{
					throw new RuntimeException("utrata dokładności - niemożliwe stworzenie pola");
				}
				boolean available = !division.isHidden();
				EnumItem ei = new EnumItemImpl(division.getId().intValue(), division.getGuid(), titles, new String[1], false, getCn(), available);
				if (available)
				{
					enumItems.add(ei);
				}
			}
		}
	}

	@Override
	public void persist(Document doc, Object key) throws EdmException
	{
		Long divId = null;
		if(key == null) return;
        	if (key instanceof String)
        		divId = Long.valueOf((String) key);
        	else
        		divId = (Long) key;
        	
            if (divId != null)
      doc.setAutorDivision(DSDivision.findById(divId).getGuid());
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId,false);
	
		if (doc instanceof OfficeDocument && doc.getAutorDivision() != null ){
			DSDivision div = DSDivision.find(doc.getAutorDivision());
			return new FieldValue(this, div.getId().toString(),  div.getName());
		}
		else 
			return new FieldValue(this, null, null);

	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value == null || (value.toString().equals("")))
			return null;
		if (value instanceof Integer)
			return value;
		else if (value instanceof String)
		try
		{
			return Long.parseLong(value.toString());
		}
		catch (NumberFormatException e)
		{
			throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci") + " " + value + " " + string("DoLiczbyCalkowitej"));
		}
		return value;
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
	}
	
	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException
	{

		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		Long userDivision = 1l;
		DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();

		for (int i = 0; i < divs.length; i++)
		{
			if (divs[i].isPosition() && !(divs[i].isHidden()))
			{

				DSDivision parent = divs[i].getParent();
				if (parent != null)
				{
					userDivision = parent.getId();
					enumValues.put(parent.getId().toString(), parent.getName());

				} else
				{
					userDivision = divs[i].getId();
					enumValues.put(divs[i].getId().toString(), divs[i].getName());
				}
			} else if (divs[i].isDivision() && !(divs[i].isHidden()))
			{
				userDivision = divs[i].getId();
				enumValues.put(divs[i].getId().toString(), divs[i].getName());
			}
		}


		DSDivision d = DSDivision.find(DSDivision.ROOT_GUID);
		if (userDivision != null)
			d = DSDivision.findById(userDivision);
		
		List<String> enumSelected = new ArrayList<String>();
		DSDivision dsel;
		if (fieldsValues.get(this.getCn()) != null)
		{
			Object o = fieldsValues.get(this.getCn());
			if (o instanceof String)
			{
				dsel = DSDivision.findById((Long.valueOf((String) fieldsValues.get(this.getCn()))));
				enumValues.put(dsel.getId().toString(), dsel.getName());
				enumSelected.add(dsel.getId().toString());
			} else if (o instanceof Long)
			{
				dsel = DSDivision.findById((Long) fieldsValues.get(this.getCn()));
				enumValues.put(dsel.getId().toString(), dsel.getName());
				enumSelected.add(dsel.getId().toString());
			}

		} else
		{
			enumSelected.add(d.getId().toString());
			enumValues.put(d.getId().toString(), d.getName());
		}
		return new EnumValues(enumValues, enumSelected);
	}

	public final List<EnumItem>  getAllEnumItemsByAspect(String documentAspectCn,DocumentKind kind)
	{
		
		return this.enumItems;
	}

	
	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) 
	{
		/*String  valS = (String) values.get(this.getCn());
		Integer valI = valS != null && valS.length() > 0 ? Integer.parseInt(valS) : null;
		LoggerFactory.getLogger("tomekl").debug(" {} {} ",valS,valI);
		dockindQuery.officeDocumentClerk(this.type, valI);	*/	
	}
	
}
