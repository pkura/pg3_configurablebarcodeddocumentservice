package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.Database;
import pl.compan.docusafe.core.dockinds.Database.Column;
import pl.compan.docusafe.core.dockinds.Database.Table;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.paa.NormalLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static pl.compan.docusafe.util.I18nUtils.string;

public class DocumentJournalField extends OrderedNonColumnField implements CustomPersistence, EnumNonColumnField
{

	private static final Logger log = LoggerFactory.getLogger(DocumentJournalField.class);

	public static final String CN = "JOURNAL";
	public static final String PREFIX_TO_JOURNAL_PERMISSION_GUID = "PERMISSION_TO_JOURNAL_";

	private final String type;

	public DocumentJournalField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String type) throws EdmException
	{
		super(getOrders(), id, CN, names, Field.DOCUMENT_JOURNAL, null, null, required, requiredWhen, availableWhen);
		this.type = type;
	}

    public static OrderPredicateProvider.Order[] getOrders() {
        return new Order[]{
                createOrder("dso_in_document", "JOURNAL", "dso_journal", "journal_id", "description"),
                createOrder("dso_out_document", "JOURNAL", "dso_journal", "journal_id", "description")
        };
    }

    public static Order createOrder(String documentKindName, String fieldCn, String ordTable, String linkToOrdTable, String orderedColumnName) {
        Table documentsKind = new Table(documentKindName);

        LinkedTable docLT = OrdersFactory.createDocumentTable(new Table(documentKindName));
        LinkedTable ordLT = OrdersFactory.createNextTable(docLT, linkToOrdTable, "id", new Table(ordTable));

        LinkedTable[] linkedTables = new LinkedTable[]{docLT, ordLT};
        Database.Column orderedColumn = new Column(orderedColumnName);
        return new Order(fieldCn, documentsKind, linkedTables, orderedColumn);
    }

	@Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
    {
        OfficeDocument doc = OfficeDocument.find(documentId);
        Journal journalDoc = doc.getJournal();
        if (journalDoc != null )
        {

        	if (AvailabilityManager.isAvailable("dontShowMainJournalOnDockind")){
           Journal journalByType = Journal.getMainJournal(type);
           if (journalByType.equals(journalDoc))
               return new FieldValue(this, null, null);
        	}

            return new FieldValue(this, journalDoc.getId().toString(), journalDoc.getDescription());
        }
        else
        {
            return new FieldValue(this, null, null);
        }
    }

    public FieldValue provideFieldRealValue(long documentId, ResultSet rs) throws SQLException, EdmException
    {
        OfficeDocument doc = OfficeDocument.find(documentId);
        Journal journalDoc = doc.getJournal();
        if (journalDoc != null )
            return new FieldValue(this, journalDoc.getId().toString(), journalDoc.getDescription());
        else
            return new FieldValue(this, null, null);
    }

	@Override
	public Long simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value == null || value.equals(""))
			return null;
		if (value instanceof Journal)
		{
			return ((Journal) value).getId();
		}
		try
		{
			return Long.parseLong(value.toString());
		}
		catch (NumberFormatException e)
		{
			throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci") + " " + value + " " + string("DoLiczbyCalkowitej"));
		}
	}

	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException
	{

		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("", "--wybierz--");

		if (fieldsValues != null && fieldsValues.get(getCn()) != null)
		{
			enumSelected.add(fieldsValues.get(getCn()).toString());
		}
		else
		{
			enumSelected.add("");
		}

		// dla PAA wyszukiwanie dzinnik�w z departamentu i dzia��w podleg�ych 
		List<String> listaDzialow = new ArrayList<String>();
		if(AvailabilityManager.isAvailable("PAA"))
		{
			NormalLogic.getDepartamentUzytkownika(DSApi.context().getDSUser(), listaDzialow);
			for (DSUser subs : DSApi.context().getDSUser().getSubstitutedUsers())
			{
				NormalLogic.getDepartamentUzytkownika(subs, listaDzialow);
			}
		}
		
		
		for (Journal journal : Journal.findByType(type))
		{
			if (!Journal.getMainJournal(type).equals(journal))
			{
                if (AvailabilityManager.isAvailable("field.documentJournal.out.showAll") && type.equals(Journal.OUTGOING))
                {
                    enumValues.put(journal.getId().toString(), journal.getDescription());
                }
                else
				if (AvailabilityManager.isAvailable("journals.Permissions")) {
					if (DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)) {
						enumValues.put(journal.getId().toString(), journal.getDescription());
					} else {
						if (hasPermissionsToJournal(journal)) {
							enumValues.put(journal.getId().toString(), journal.getDescription());
						}
					}
					// w przypadku gdy dziennik zostal juz wybrany to pokaz go nawet dla osoby ktora nie ma do niego uprawnien
					if (enumSelected.contains(journal.getId().toString())) {
						enumValues.put(journal.getId().toString(), journal.getDescription());
					}
				} else if(DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD) ||
				   AvailabilityManager.isAvailable("field.documentJournal.showAll") ||
						(	DSApi.context().hasPermission(DSPermission.DZIENNIK_KOMORKA_PODGLAD) &&
                		     DSApi.context().getDSUser().inDivision(DSDivision.find(journal.getOwnerGuid())	, true)
						)
                    )
					enumValues.put(journal.getId().toString(), journal.getDescription());
				
				// dla PAA dodawanie wszystkich dziennik�w z departamentu u�ytkownika do listy 
				else if(AvailabilityManager.isAvailable("PAA")){
				 if (listaDzialow.contains(journal.getOwnerGuid()))
					 enumValues.put(journal.getId().toString(), journal.getDescription());
				}
			}
		}

		return new EnumValues(enumValues, enumSelected);
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
	}

	public void persist(Document doc, Object key) throws EdmException
	{
		if (key == null && doc instanceof OfficeDocument && ((OfficeDocument) doc).getJournal() == null)
		{
			 ((OfficeDocument) doc).setJournal(null);
			 return;
		}
		else if(key==null)return;
		else 
		{
            Long journalId = null;
            if (key instanceof String)
                journalId = Long.valueOf((String) key);
            else
                journalId = (Long) key;

            if (doc instanceof OfficeDocument && journalId!=null)
                ((OfficeDocument) doc).setJournal(Journal.find(journalId));
            String formattedOfficeNumber = ((OfficeDocument) doc).getFormattedOfficeNumber();
            if (AvailabilityManager.isAvailable("BinToJournalWhenSelected")&&journalId!=null){
            	Journal.TX_newEntry2(journalId, doc.getId(), GlobalPreferences.getCurrentDay());
            } else if (journalId!=null && (doc instanceof InOfficeDocument) || (formattedOfficeNumber != null &&journalId!=null && !formattedOfficeNumber.equalsIgnoreCase("brak")))
            	Journal.TX_newEntry2(journalId, doc.getId(), GlobalPreferences.getCurrentDay());
		}
	}

	/**
	 * Typ dziennik�w kt�ry mo�na wybra�
	 * 
	 * @return
	 */
	public String getJournalType()
	{
		return type;
	}
	
    private boolean hasPermissionsToJournal(Journal journal) {
    	boolean hasPermissions;
    	try {
			DSDivision division = DSDivision.find(PREFIX_TO_JOURNAL_PERMISSION_GUID + journal.getId().toString());
			if (DSApi.context().getDSUser().inDivision(division, true)) {
				hasPermissions = true;
			} else {
				hasPermissions = false;
			}
		} catch (DivisionNotFoundException e) {
			hasPermissions = false;
		} catch (UserNotFoundException e) {
			hasPermissions = false;
		} catch (EdmException e) {
			hasPermissions = false;
		}
    	return hasPermissions;
    }
}