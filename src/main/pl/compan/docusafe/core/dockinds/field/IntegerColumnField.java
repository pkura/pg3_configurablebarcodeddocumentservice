package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Klasa reprezentuj�ca pole, w kt�rym zapisywana jest warto�� ca�kowita
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class IntegerColumnField extends Field implements IntegerField{
    /**
     * @param id            id z xml'a, musi by� unikalne
     * @param cn            cn z xml'a, musi b� unikalne
     * @param column        nazwa kolumny, nie mo�e by� null (niestety)
     * @param names         nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param required      czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen  warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws pl.compan.docusafe.core.EdmException
     *
     */
    public IntegerColumnField(String id, String cn, String column, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, column, names, Field.INTEGER, null, null, required, requiredWhen, availableWhen);
    }

    @Override
    public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
        if(value == null){
            ps.setObject(index, null);
        } else {
            ps.setInt(index, simpleCoerce(value));
        }
    }

    @Override
    public Integer simpleCoerce(Object value) throws FieldValueCoerceException {
        if(value == null){
            return null;
        }
        if ((value instanceof String && String.valueOf(value).length() == 0)) return null;
        if(value instanceof Integer) {
            return (Integer) value;
        }

        if(value instanceof CharSequence){
            try {
                return Integer.valueOf(value.toString().replaceAll(" ",""));
            } catch (NumberFormatException ex) {
                throw new FieldValueCoerceException(value.toString().replaceAll(" ","") + " cannot be converted to Integer by IntegerField");
            }
        }

        throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + " cannot be converted to Integer by IntegerField");
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        int integer = rs.getInt(getColumn());
        Integer value = rs.wasNull() ? null : integer;
        return new FieldValue(this, value, value);
    }
    
    public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() 
    {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER);
	}
    
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getCn() == null) ? 0 : getCn().hashCode());
		return result;
	}

}
