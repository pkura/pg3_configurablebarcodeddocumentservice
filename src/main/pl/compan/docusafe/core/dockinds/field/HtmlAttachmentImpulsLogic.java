package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.web.viewserver.ViewServer;

public class HtmlAttachmentImpulsLogic implements HTMLFieldLogic {
	

	@Override
	public String getValue(String docId) throws EdmException  {
		Long attachmentId = null;
		String baseUrl = Docusafe.pageContext;
        Attachment attach = null;
        StringBuilder sb = new StringBuilder();

        NativeCriteria nc = DSApi.context().createNativeCriteria("DS_ATTACHMENT", "d");
        nc.setProjection(NativeExps.projection().addProjection("d.id")).add(NativeExps.eq("d.document_id", docId));
           

        CriteriaResult cr = nc.criteriaResult();
        while (cr.next())
        {
            attachmentId = cr.getLong(0, null);
            attach = Attachment.find(attachmentId,false);
            break;
        }
		
        if(attachmentId != null && attach.getMostRecentRevision()!= null)
        {
        	// nowa wersja
            sb.append("<a href=\"#\" onclick=\"openToolWindow('/"+baseUrl+"/office/common/add-attachment-from-pages.action?");
            sb.append("binderId=' + $j('#documentId').val() + '&attachmentId="+attachmentId+"&documentId="+docId+"','vs',800,600);\">");
            sb.append("<img src=\"/"+baseUrl+"/img/add_icon.gif\" width=\"16\" height=\"16\"    height=\"17\" title=\"Dodaj nowa wersje\"/></a>");
            sb.append("&nbsp;&nbsp;");
            if(ViewServer.mimeAcceptable(attach.getMostRecentRevision().getMime()))
            {
            	// pdf
                sb.append("<a href=\"/"+baseUrl+"/repository/view-attachment-revision.do?asPdf=true&attachmentId=" + attachmentId + "\">");
                sb.append("<img src=\"/"+baseUrl+"/img/pdf.gif\" width=\"16\" height=\"16\"    height=\"17\" title=\"Pobierz za��cznik\"/></a>&nbsp;&nbsp;");

                // https://docusafe.impuls-leasing.pl/docusafe/viewserver/viewer.action?id=683076&fax=false&width=1000&height=750&binderId=748957
                // javascript:openToolWindow('/docusafe/viewserver/viewer.action?id=569060&height=750&width=1000',%20'nwPdf',%201000,%20750)
                
                // viewserver
                sb.append("<a href=\"#\" onclick=\"openToolWindow('/"+baseUrl+"/viewserver/viewer.action?");
        		sb.append("binderId=' + $j('#documentId').val() + '&attachmentId="+attachmentId+"&documentId="+docId+"','vs',800,600);\">");
        		sb.append("<img src=\"/"+baseUrl+"/img/wyswietl.gif\" width=\"16\" height=\"16\"  height=\"17\" title=\"Wyswietl zalacznik w przegladarce\"/></a>&nbsp;&nbsp;");
             }
        }
        else
        {
        	sb.append("<a href=\"#\" onclick=\"openToolWindow('/"+baseUrl+"/office/common/add-attachment-from-pages.action?");
        	sb.append("binderId=' + $j('#documentId').val() + '&documentId="+docId+"','vs',800,600);\">");
        	sb.append("<img src=\"/"+baseUrl+"/img/add_icon.gif\" width=\"16\" height=\"16\"    height=\"17\" title=\"Dodaj nowa wersje\"/></a>");
        	
        }
		
		return sb.toString();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

}

