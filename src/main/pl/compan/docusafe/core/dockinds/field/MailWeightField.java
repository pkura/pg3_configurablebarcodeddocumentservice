package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.Database;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.office.Gauge;
import pl.compan.docusafe.core.office.MailWeightKind;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

import static pl.compan.docusafe.util.I18nUtils.string;

/**
 * MailWeightField
 *
 * @version $Id$
 */
public class MailWeightField extends OrderedNonColumnField implements CustomPersistence, EnumNonColumnField {

	public MailWeightField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {

		super(id, cn, names, Field.MAIL_WEIGHT, null, null, required, requiredWhen, availableWhen);

		this.setSearchShow(false);
	}

	public MailWeightField(Database.Column orderedColumn, String id, String cn, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
		super(orderedColumn, id, cn, names, type, refStyle, classname, required, requiredWhen, availableWhen);
	}

	@Override
	public void persist(Document doc, Object key) throws EdmException {

        Integer weightId = null;

        if (key instanceof String) {
            weightId = Integer.valueOf((String) key);
        } else {
            weightId = (Integer) key;
        }

        if (weightId != null && doc instanceof OutOfficeDocument) {
            ((OutOfficeDocument) doc).setMailWeight(MailWeightKind.find(weightId).getId());
        }
	}

	@Override
	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException {

		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("", "--wybierz--");

		if (fieldsValues != null && fieldsValues.get(getCn()) != null) {
			enumSelected.add(fieldsValues.get(getCn()).toString());
		} else {
			enumSelected.add("");
		}

		for (MailWeightKind kind : MailWeightKind.list()) {
			enumValues.put(kind.getId().toString(), kind.getName());
		}

		return new EnumValues(enumValues, enumSelected);
	}

	@Override
    public EnumValues getEnumItemsForDwrByScheme(Map<String, Object> fieldsValues, Map<String, SchemeField> schemeFields) throws EdmException {
        EnumValues enumValues = getEnumItemsForDwr(fieldsValues);
        EnumValues filteredEnumValues = new EnumValues();

        if(schemeFields != null && schemeFields.containsKey(this.getCn())) {
            SchemeField schemeField = schemeFields.get(this.getCn());

            if(schemeField.getEnumValuesId() != null) {
                List<Map<String, String>> filteredOptions = Lists.newArrayList();

                for(Map<String, String> singleOption : enumValues.getAllOptions()) {
                    for(String id : schemeField.getEnumValuesId()) {
                        if(singleOption.containsKey(id)) {
                            filteredOptions.add(singleOption);
                            break;
                        }
                    }
                }

                filteredEnumValues.setAllOptions(filteredOptions);
                filteredEnumValues.setSelectedOptions(enumValues.getSelectedOptions());

                return filteredEnumValues;
            }
        }

        return enumValues;
    }
	
	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        OfficeDocument doc = OfficeDocument.find(documentId);

        //narazie jest tylko dla OutOfficeDocument, pozniej mozna zrobic takze dla InOfficeDocument
        if (doc instanceof OutOfficeDocument && ((OutOfficeDocument)doc).getMailWeight() != null ) {
            return new FieldValue(this, ((OutOfficeDocument)doc).getMailWeight().toString(), ((OutOfficeDocument)doc).getMailWeight().toString());
        } else {
            return new FieldValue(this, null, null);
        }
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {

		if (value == null || (value.toString().equals(""))) {
			return null;
		}
        if (value instanceof MailWeightKind) {
            return ((MailWeightKind) value).getId();
        }
		try {
			return Integer.parseInt(value.toString());
		} catch (NumberFormatException e) {
			throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci") + " " + value + " " + string("DoLiczbyCalkowitej"));
		}
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
	}
}
