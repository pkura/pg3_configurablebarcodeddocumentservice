package pl.compan.docusafe.core.dockinds.field;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.StringManager;

public class DocumentPostalRegNr extends NonColumnField implements CustomPersistence
{

	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);

	public DocumentPostalRegNr(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, names, Field.DOCUMENT_POSTAL_REG_NR, null, null, required, requiredWhen, availableWhen);
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		OfficeDocument doc = OfficeDocument.find(documentId);
		return new FieldValue(this, doc.getPostalRegNumber());
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value == null)
			return null;
		String postalRegNr = null;
		if (value instanceof String)
			postalRegNr = (String) value;
		else
		{
			throw new FieldValueCoerceException(this.getName() + ":" + sm.getString("NieMoznaSkonwertowacWartosci") + " " + value + " " + "do Stringa");
		}
		return postalRegNr;
	}

	public void persist(Document doc, Object key) throws EdmException
	{
		((OfficeDocument) doc).setPostalRegNumber((String) key);
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING);
	}
}
