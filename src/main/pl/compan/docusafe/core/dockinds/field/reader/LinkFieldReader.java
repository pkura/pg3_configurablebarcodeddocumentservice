package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.LinkField;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class LinkFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		String type = el.element("type").attributeValue("type");
		String fieldLabel = elType.attributeValue("field-label");
		String labelColumn = null;
		if (fieldLabel.equals("column"))
			labelColumn = elType.attributeValue("column");
		LinkField linkField = new LinkField(el.attributeValue("id"), el.attributeValue("logic"), el.attributeValue("cn"), names, required, requiredWhen, availableWhen, type, fieldLabel, elType.attributeValue("link-target"), labelColumn);
		return linkField;
	}
}
