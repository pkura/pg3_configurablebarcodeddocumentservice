package pl.compan.docusafe.core.dockinds.field;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class ListValueDbEnumField extends NonColumnField
{
	private static final Logger LOG = LoggerFactory.getLogger(ListValueDbEnumField.class);
	
	private final StringManager sm = StringManager.getManager(ListValueDbEnumField.class.getPackage().getName());
	
	private final String DEFAULT_ID_COLUMN = "ID";
	
	private final String DEFAULT_CN_COLUMN = "CN";
	
	private final String DEFAULT_TITLE_COLUMN = "TITLE";
	
	private final String DEFAULT_AVAILABLE_COLUMN = "AVAILABLE";
	
	private String tableName;
	
	private String idColumnName;
	
	private String cnColumnName;
	
	private String titleColumnName;
	
	private String availableColumnName;
	
	private Map<Integer, EnumItem> enumItems;
	
    private Set<EnumItem> availableItems;
    
    private Set<EnumItem> allItems;
    
  //pole zawieraj�ce wszystkie informacje o s�ownikach przypisanych do tej samej tabeli
    private static final Multimap<String, ListValueDbEnumField> tableToField
        = Multimaps.synchronizedListMultimap(ArrayListMultimap.<String, ListValueDbEnumField>create());
	
	public ListValueDbEnumField(String id, String cn, String column,
			Map<String, String> names, String refStyle,
			 boolean required, List<Condition> requiredWhen,
			List<Condition> availableWhen, String tableName, String idColumnName,
			String cnColumnName, String titleColumnName, String availableColumnName) throws EdmException 
	{
		super(id, cn, names, LIST_VALUE, null, null, required, requiredWhen,
				availableWhen);
		
		// ustawienie nazwy tabeli
		if (StringUtils.isNotEmpty(tableName))
			this.tableName = tableName;
		else
		{
			String message = "Nie okreslono nazwy tabeli dla pola: " + cn; 
			LOG.error(message);
			throw new EdmException(message);
		}
		
		// nazwa kolumny parametru ID
		if (StringUtils.isNotEmpty(idColumnName))
			this.idColumnName = idColumnName;
		else
			this.idColumnName = DEFAULT_ID_COLUMN;
		
		// nazwa kolumny parametru CN
		if (StringUtils.isNotEmpty(cnColumnName))
			this.cnColumnName = cnColumnName;
		else
			this.cnColumnName = DEFAULT_CN_COLUMN;
		
		// nazwa kolumny parametru TITLE
		if (StringUtils.isNotEmpty(titleColumnName))
			this.titleColumnName = titleColumnName;
		else
			this.titleColumnName = DEFAULT_TITLE_COLUMN;
		
		// nazwa kolumny parametru AVAILABLE
		if (StringUtils.isNotEmpty(availableColumnName))
			this.availableColumnName = availableColumnName;
		else 
			this.availableColumnName = DEFAULT_AVAILABLE_COLUMN;
		
		// inicjalizacja
		initialize();
		tableToField.put(tableName, this);
	}
	
	public static void reloadForTable(String tablename) throws EdmException 
	{
        EdmException ex = null;
        for(ListValueDbEnumField field : tableToField.get(tablename)){
            try {
                field.singleReload();
            } catch (EdmException e) {
                if(ex == null){
                    ex = e;
                } else {
                    LOG.error(e.getMessage(), e);
                }
            }
        }
        if(ex != null) {
            throw ex;
        }
    }

    public void reload() throws EdmException 
    {
        reloadForTable(tableName);
    }
    
    private void singleReload() throws EdmException
    {
        initialize();
    }
	
	private void initialize() throws EdmException
	{
		ResultSet rs = null;
    	PreparedStatement ps = null;
    	try
    	{
            Map<Integer, EnumItem> items = new LinkedHashMap<Integer, EnumItem>();
            SortedSet<EnumItem> available = new TreeSet<EnumItem>(EnumItem.COMPARATOR_BY_CN);
            SortedSet<EnumItem> all = new TreeSet<EnumItem>(EnumItem.COMPARATOR_BY_CN);
            
            StringBuilder sql = new StringBuilder();
            sql.append("select ")
               .append(idColumnName).append(", ")
               .append(cnColumnName).append(", ")
               .append(titleColumnName).append(", ")
               .append(availableColumnName)
               .append(" from ").append(tableName)
               .append(" order by ").append(cnColumnName);
            
    		ps = DSApi.context().prepareStatement(sql.toString());
    		rs = ps.executeQuery();
    		while(rs.next())
    		{
    			Map<String,String> title =  new TreeMap<String,String>();
    			title.put("pl", rs.getString(cnColumnName) + " - " + rs.getString(titleColumnName));
    			EnumItemImpl ei = new EnumItemImpl(
    					rs.getInt(idColumnName),
                        rs.getString(cnColumnName),
                        title,
                        new String[0],
                        false, 
                        getCn());
    			
                ei.setAvailable(rs.getBoolean(availableColumnName));

                if(ei.isAvailable())
                    available.add(ei);

                all.add(ei);

    			items.put(rs.getInt(1), ei);
    		}
            
            enumItems = items;
            availableItems = available;
            allItems = all;
    		
    		rs.close();
    	}
    	catch (Exception e) 
		{
    		throw new EdmException("B��d podczas wczytania s�ownika z bazy" , e);
		}
    	finally
    	{
    		if(rs != null)
    			try
    			{
    				rs.close();
				} catch(SQLException e)
				{
					LOG.debug("", e);
				}
    		DSApi.context().closeStatement(ps);
    	}
	}

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        return new FieldValue(this, null);
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        if (value == null){
            return null;
        } else {
            return value.toString();
        }
    }

    public List<EnumItem> getEnumItems()
    {
        return enumItems != null ? new ArrayList<EnumItem>(enumItems.values()) : Collections.EMPTY_LIST;
    }
    
    public void setEnumItems(List<EnumItem> enumItems)
    {
        this.enumItems = new LinkedHashMap<Integer, EnumItem>();
        for (EnumItem enumItem : enumItems)
            this.enumItems.put(enumItem.getId(), enumItem);
    }

    public Collection<EnumItem> getAvailableItems(){
        return availableItems;
    }
    
    public Collection<EnumItem> getSortedEnumItems()
    {
        List<EnumItem> list = getEnumItems();
        Collections.sort(list, EnumItem.COMPARATOR_BY_CN);
        return list;
    }

    public EnumItem getEnumItem(Integer id) throws EntityNotFoundException
    {
        EnumItem item = enumItems != null ? (EnumItem) enumItems.get(id) : null;
        if (item == null)
            throw new EntityNotFoundException(
            		sm.getString("NieZnalezionoWartosciWyliczeniowejOidentyfikatorze") + " = " + id + " " + sm.getString("DlaPola") + " " + getCn());
        return item;
    }
    
    public EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException
    {
        if (enumItems != null)
        {
            for (EnumItem i : enumItems.values())
            {
                if (cn.equals(i.getCn()))
                    return i;
            }
        }
        throw new EntityNotFoundException(
        		sm.getString("NieZnalezionoWartosciWyliczeniowejOcn") + " = " + cn + " " + sm.getString("DlaPola")+" " + getCn());
    }

    public EnumItem getEnumItemByTitle(String title) throws EntityNotFoundException
    {
        if (enumItems != null)
        {
            for (EnumItem i : enumItems.values())
            {
                if (title.equals(i.getTitle()))
                    return i;
            }
        }
        throw new EntityNotFoundException(
        		sm.getString("NieZnalezionoWartosciWyliczeniowejOtitle") + " = " + title + " " + sm.getString("DlaPola") + " " + getCn());
    }
    
    public String getTableName() 
    {
		return tableName;
	}

	public void setTableName(String tableName) 
	{
		this.tableName = tableName;
	}

	public String getIdColumnName() 
	{
		return idColumnName;
	}

	public void setIdColumnName(String idColumnName) 
	{
		this.idColumnName = idColumnName;
	}

	public String getCnColumnName() 
	{
		return cnColumnName;
	}

	public void setCnColumnName(String cnColumnName) 
	{
		this.cnColumnName = cnColumnName;
	}

	public String getTitleColumnName() 
	{
		return titleColumnName;
	}

	public void setTitleColumnName(String titleColumnName) 
	{
		this.titleColumnName = titleColumnName;
	}

	public String getAvailableColumnName() 
	{
		return availableColumnName;
	}

	public void setAvailableColumnName(String availableColumnName) 
	{
		this.availableColumnName = availableColumnName;
	}

	public Set<EnumItem> getAllItems() 
	{
		return allItems;
	}

	public void setAllItems(Set<EnumItem> allItems) 
	{
		this.allItems = allItems;
	}

	public void setAvailableItems(Set<EnumItem> availableItems) 
	{
		this.availableItems = availableItems;
	}
	
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.LIST_VALUE);
	}
	
	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EntityNotFoundException 
    {
    	Map<String, String> enumValues = new HashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("","--wybierz--");

		if (fieldsValues != null && fieldsValues.get(getCn()) != null)
			enumSelected.add(getEnumItem((Integer)fieldsValues.get(getCn())).getTitle());
		else 
			enumSelected.add("");
		
		// pobranie wartosci enumFielda
		for (EnumItem item : getEnumItems())
			enumValues.put(item.getId().toString(), item.getTitle());
		return new EnumValues(enumValues, enumSelected);		
	}
}
