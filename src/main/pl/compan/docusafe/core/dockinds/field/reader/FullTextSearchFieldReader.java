package pl.compan.docusafe.core.dockinds.field.reader;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.dom4j.Element;

import com.google.common.base.CaseFormat;
import com.google.common.base.Function;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.FullTextSearchField;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.core.dockinds.field.FullTextSearchField.Syntax;
import pl.compan.docusafe.lucene.IndexStatus;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FullTextSearchFieldReader implements FieldReader
{
	private final static Logger LOG = LoggerFactory.getLogger(FullTextSearchFieldReader.class);

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		FullTextSearchField ret = new FullTextSearchField(el.attributeValue("id"), el.attributeValue("cn"), el.attributeValue("column"), names);

		Element elQuery = elType.element("query");
		checkNotNull(elQuery, "'type' element must contain 'query' element in 'full-text-search'");
		String syntax = elQuery.attributeValue("syntax");
		checkNotNull(syntax, "'query' element must have 'syntax' attribute in 'full-text-search'");
		ret.setSyntax(Syntax.valueOf(CaseFormat.LOWER_HYPHEN.to(CaseFormat.UPPER_UNDERSCORE, syntax)));

		Element elIndex = elType.element("index");
		checkNotNull(elQuery, "'type' element must contain 'index' element in 'full-text-search'");
		List<Element> elTextProviders = elIndex.elements();
		checkArgument(!elTextProviders.isEmpty(), "'index' element cannot be empty in 'full-text-search'");
		for (Element elTp : elTextProviders)
		{
			String name = elTp.getName();
			if (name.equals("all-attachments"))
			{
				ret.getTextProviders().add(new Function<Document, CharSequence>() {
					@Override
					public CharSequence apply(Document document)
					{
						try
						{
							return collectTextFromAllAttachments(document);
						}
						catch (Exception ex)
						{
							LOG.error(ex.getMessage(), ex);
							return "";
						}
					}
				});
			}
			else if (name.equals("all-attachments-from-ocr"))
			{
				ret.getTextProviders().add(new Function<Document, CharSequence>() {
					@Override
					public CharSequence apply(Document document)
					{
						try
						{
							return collectTextFromAllAttachmentsFromOcr(document);
						}
						catch (Exception ex)
						{
							LOG.error(ex.getMessage(), ex);
							return "";
						}
					}
				});
			}
			else
			{
				throw new IllegalArgumentException("unknown element '" + name + "' inside element 'index' in 'full-text-search'");
			}
		}

		return ret;
	}

	/**
	 * Metoda nic nie robi poniewa� za ca�o�� jest odpowiedzilany serwis
	 * Index.java
	 * 
	 */
	private static CharSequence collectTextFromAllAttachmentsFromOcr(Document doc) throws EdmException
	{
		for (Attachment att : doc.getAttachments())
		{
			doc.getDocumentKind().logic().getOcrSupport().putToOcr(att.getMostRecentRevision());
		}
		return "";

	}

	private static CharSequence collectTextFromAllAttachments(Document doc) throws EdmException
	{
		LOG.info("collectTextFromAllAttachments");
		StringBuilder sb = new StringBuilder();
		Tika tika = new Tika();
		attachments: for (Attachment att : doc.getAttachments())
		{
			AttachmentRevision ar = att.getMostRecentRevision();
			if (ar.getIndexStatus() == IndexStatus.INDEXING_NOT_SUPPORTED)
			{
				continue attachments;
			}
			try
			{
				sb.append(IOUtils.toString(tika.parse(ar.getBinaryStream())));
				ar.setIndexStatus(IndexStatus.INDEXED);
			}
			catch (IOException ex)
			{
				LOG.error(ex.getMessage(), ex);
				ar.setIndexStatus(IndexStatus.INDEXING_NOT_SUPPORTED);
			}
		}
		return sb;
	}
}
