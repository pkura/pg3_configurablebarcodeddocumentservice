package pl.compan.docusafe.core.dockinds.field;

import org.apache.commons.fileupload.FileItem;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;

/**
 * Logika dla pola typu załącznik
 */
public interface AttachmentFieldLogic {
    /**
     * Tworzy i zapisuje załączniki - przy zapisywaniu dokumentu
     * @see  pl.compan.docusafe.core.dockinds.dwr.attachments.DwrAttachmentStorage
     * @see  CustomPersistence
     * @param doc  Zapisywany dokument
     * @param field  Pole załącznika
     * @param fileItems Pliki do zapisania
     * @throws EdmException
     * @throws IOException
     */
	public void persist(Document doc, Field field, List<FileItem> fileItems) throws EdmException, IOException;

    public Attachment provideFieldValue(Field field, FieldValue fieldValue, long documentId, ResultSet rs) throws EdmException;
}
