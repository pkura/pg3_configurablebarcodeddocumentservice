package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class DocumentOutZPODate extends NonColumnField implements CustomPersistence, SearchPredicateProvider
{
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(DocumentOutZPODate.class.getPackage().getName(), null);
	
	public DocumentOutZPODate(String id, String type, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, names, pl.compan.docusafe.core.dockinds.field.Field.DOCUMENT_OUT_ZPODATE, null, null, required, requiredWhen, availableWhen);
	}
	
	public void persist(Document doc, Object key) throws EdmException 
	{
		if (key == null)
		{
			((OutOfficeDocument) doc).setZpoDate(null);
		}
		else if( key instanceof Date)
		{
			((OutOfficeDocument) doc).setZpoDate((Date) key);
		}
		else 
		{
			throw new EdmException("DocumentOutZPODate persist key class: "+key.getClass());
		}
		
	}

	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException 
	{
		OfficeDocument doc = OfficeDocument.find(documentId);
		
		if (doc instanceof InOfficeDocument)
			throw new EdmException("DocumentOutZPODate provideFieldValue doc class: "+doc.getClass());
		else if (doc instanceof OutOfficeDocument && ((OutOfficeDocument)doc).getZpoDate() != null )
			return new FieldValue(this, ((OutOfficeDocument)doc).getZpoDate(), ((OutOfficeDocument)doc).getZpoDate());
		else
			return new FieldValue(this, null, null);
	}

	public Object simpleCoerce(Object value) throws FieldValueCoerceException 
	{
		if (value == null )
            return null;
        Date date;
        if ((value instanceof String && String.valueOf(value).length() == 0)) return null;
        if (!(value instanceof Date)) {
            try {
                date = DateUtils.parseJsDate(String.valueOf(value));
                if (!DSApi.isLegalDbDate(date))
                    throw new FieldValueCoerceException(sm.getString("PrzekazanaDataNieZnajdujeSie") + " " +
                            sm.getString("WzakresieWymaganymPrzezBazeDanych") + ": " + DateUtils.formatJsDate(date));
            } catch (FieldValueCoerceException e) {
                throw e;
            } catch (Exception e) {
                throw new FieldValueCoerceException(sm.getString("NieMoznaSkonwertowacWartosci") + " '" + value + "' " + sm.getString("DoDaty"));
            }
        } else {
            date = (Date) value;
            if (!DSApi.isLegalDbDate(date))
                throw new FieldValueCoerceException(sm.getString("PrzekazanaDataNieZnajdujeSie") + " " +
                        sm.getString("WzakresieWymaganymPrzezBazeDanych") + ": " + DateUtils.formatJsDate(date));
        }
        return date;
	}

	public Field getDwrField() throws EdmException 
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.DATE);
	}

	
	public void addSearchPredicate(DockindQuery dockindQuery,Map<String, Object> values, Map<String, Object> fieldValues) 
	{
		String fromK = this.getCn()+"_from";
		String toK   = this.getCn()+"_to";
		
		String from = (String) values.get(fromK);
		String to   = (String) values.get(toK);
		
		dockindQuery.outOfficeZpoDate(from != null ? DateUtils.parseJsDate(from) : null, to != null ? DateUtils.parseJsDate(to) : null);
	}
}
