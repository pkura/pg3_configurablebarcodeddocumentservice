package pl.compan.docusafe.core.dockinds.field;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.util.NumericUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.core.dockinds.dwr.Popup;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Klasa reprezentuj�ca jedno pole(atrybut) z danego rodzaju dokumentu. Bazowa klasa dla wszystkich p�l.
 * Obiekt tego typu musi by� thread-safe - b�dzie wykorzystywane r�wnolegle przez w�tki obs�uguj�ce akcje.
 *
 * W tej klasie i jej implementacjach korzysta si� z 3 poj�� dotycz�cych warto�ci pola:
 * klucz(key) - warto�� zapisywana w bazie; np w przypadku warto�ci s�ownikowych
 *              jest to id czyli int lub long
 * obiekt(object) - obiekt kt�ry reprezentuje warto�� danego pola,
 *                  w niekt�rych przypadkach jest taki sam jak klucz (np w typie money),
 *                  w innych (s�owniki, typ class) jest "pe�nym" obiektem
 * warto��/string - tekstowa reprezentacja obiektu do odczytu/zapisu z/do formatki
 *
 * Implementowanie nowych p�l:
 *
 * Nowe typy p�l powinny w�drowa� do paczki pl.compan.docusafe.core.dockinds.field
 * i dziedziczy� po tej klasie.
 *
 * Dodatkowo powinny doda� sta�� statyczn� w klasie field z unikalnym stringiem oznaczaj�cym typ.
 * T� zmienn� nale�y umie�ci� w tabeli TYPES_AVAILABLE.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @author <a href="mailto:michal.sankowski@docusafe.pl">Micha� Sankowski</a>
 * @version $Id$
 */
public abstract class Field implements Serializable {
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(),null);
	private static final Logger log = LoggerFactory.getLogger(Field.class);
    public static final String MULTI_FIELD_SEPARATOR = "_";
    /*******************************************************************
     *  TYPY P�L - warto�ci @Deprecated nie powinny by� u�ywane w kodzie
     *  .java, z wyj�tkiem parsowania xml'a dockindowego
     *  u�ycie stringa powinno si� ogranicza� do plik�w jsp
     *******************************************************************/
    @Deprecated /** u�ywa� instanceof DateField */
    public static final String DATE = "date";
    @Deprecated /** u�ywa� instanceof TimestampField */
    public static final String TIMESTAMP = "timestamp";
    @Deprecated /** u�ywa� instanceof StringField */
    public static final String STRING = "string";
    @Deprecated //u�ywa� instanceof BooleanField
    public static final String BOOL = "bool";
    @Deprecated //u�ywa� instanceof XmlEnumColumnField
    public static final String ENUM = "enum";
    @Deprecated //u�ywa� instanceof FloatField je�li konieczne
    public static final String FLOAT = "float";
    @Deprecated //u�ywa� instanceof DoubleField je�li konieczne
    public static final String DOUBLE = "double";
    @Deprecated //u�ywa� instanceof IntegerField
    public static final String INTEGER = "integer";
    @Deprecated //u�ywa� instanceof LongField
    public static final String LONG = "long";
    @Deprecated //u�ywa� instanceof ClassField
    public static final String CLASS = "class";
    @Deprecated //u�ywa� instanceof EnumRefField
    public static final String ENUM_REF = "enum-ref";
    @Deprecated //u�ywa� instanceof CostCenterField
    public static final String CENTRUM_KOSZT = "centrum-koszt";
    @Deprecated //u�ywa� instanceof DataBaseEnumField
    public static final String DATA_BASE = "dataBase";
    @Deprecated //u�ywa� instanceof MoneyField
    public static final String MONEY = "money";
    public static final String DSUSER = "dsuser";
    public static final String DSDIVISION = "dsdivision";
    public static final String CACHEABLE_SOURCE_ENUM = "cacheable-source-enum";
    public static final String DOCLIST = "doclist";
    public static final String DOCKIND_BUTTON = "dockindButton";
    public static final String DOCUMENT = "document";
    public static final String DROOLS_BUTTON = "drools-button";
    public static final String LIST_VALUE = "list-value";
    public static final String DICTIONARY = "dictionary";
    public static final String DOCUMENT_PERSON = "document-person";
    public static final String DOCUMENT_POSTAL_REG_NR = "document-postal-reg-nr";
    public static final String DOCUMENT_BARCODE = "document-barcode";
    public static final String DOCUMENT_USER_DIVISION = "document-user-division";
    public static final String DOCUMENT_USER_DIVISION_SECOND ="document-user-division-second";
    public static final String DOCUMENT_OFFICENUMBER = "document-officenumber";
    public static final String DOCUMENT_DATE = "document-date";
    public static final String DOCUMENT_DESCRIPTION = "document-description";
    public static final String DOCUMENT_JOURNAL = "document-journal";
    public static final String DOCUMENT_FIELD = "document-field";
    public static final String DOCUMENT_PARAMS = "document-params";
    public static final String DOCUMENT_OFFICE_DELIVERY = "document-office-delivery";
    public static final String DOCUMENT_IN_OFFICE_KIND = "document-in-office-kind";
    public static final String DOCUMENT_IN_INVOICE_KIND = "document-in-invoice-kind";
	public static final String DOCUMENT_IN_OFFICE_STATUS = "document-in-office-status";
	public static final String DOCUMENT_OFFICE_CASE_SYMBOL = "document-office-case-symbol";
	public static final String DOCUMENT_IN_STAMP_DATE = "document-in-stamp-date";
	public static final String DOCUMENT_REFERENCE_ID = "document-reference-id";
	public static final String DOCUMENT_IN_SUBMIT_TO_BIP = "document-in-submit-to-bip";
	public static final String DOCUMENT_IN_LOCATION = "document-in-location";
	public static final String DOCUMENT_IN_ORIGINAL = "document-in-original";
	public static final String DOCUMENT_OTHERREMARKS = "document-otherremarks";
	public static final String DOCUMENT_PACKAGEREMARKS = "document-packageremarks";
	public static final String DOCUMENT_OFFICE_CASE_PRIORITY = "document-office-case-priority";
	public static final String DOCUMENT_OUT_OFFICE_PREPSTATE = "document-out-office-prepstate";
	public static final String DOCUMENT_OUT_PRIORITY ="document-out-priority";
	public static final String DOCUMENT_OFFICE_CLERK = "document-office-clerk";
	public static final String DOCUMENT_IN_TRACKING_NUMBER = "document-in-tracking-number";
	public static final String DOCUMENT_IN_TRACKING_PASSWORD = "document-in-tracking-password";
	public static final String USER_DIVISION = "user-division";
	public static final String ACCESS_TO_DOCUMENT = "access-to-document";
	public static final String DOCUMENT_AUTOR = "document-autor";
	
	public static final String MAIL_WEIGHT = "mail-weight";

    public static final String LINK = "link";
    public static final String ATTACHMENT = "attachment";
    public static final String HTML = "html";
    public static final String HTML_EDITOR = "html-editor";
    public static final String BUTTON = "button";
    public static final String DOCUMENT_OUT_ZPODATE = "document-out-zpo-date";
    public static final String DOCUMENT_OUT_ZPO = "document-out-zpo";
    public static final String DOCUMENT_OUT_ADDITIONAL_ZPO = "document-out-additional-zpo";
    public static final String EMAIL_MESSAGE = "email-message";
    public static final String FULL_TEXT_SEARCH = "full-text-search";
    public static final String DOCUMENT_OUT_GAUGE = "document-out-gauge";
    public static final String CURRENCY = "currency";
    public static final String DOCUMENT_ABSTRAKT_DESCRIPTION = "document-abstract-description";
	public static final String DOCUMENT_AUTOR_DIVISION = "document-autor-division";
    public static final String SIMPLE_PROCESS_VARIABLE = "simple-process-variable";
    public static final String SIMPLE_PROCESS_ACTIVITY_NAME = "simple-process-activity-name";

    /**
     * generator id, kt�re w momencie pisania nie by�o nigdzie u�ywane
     * thread-safe
     */
    public static final AtomicInteger UNIQUE_ID = new AtomicInteger(1);

    public static final Set<String> TYPES_AVAILABLE = Collections.unmodifiableSet(Sets.newHashSet(Arrays.asList(
            DATE,
            TIMESTAMP,
            STRING,
            BOOL,
            ENUM,
            FLOAT,
            DOUBLE,
            INTEGER,
            LONG,
            CLASS,
            ENUM_REF,
            CENTRUM_KOSZT,
            DATA_BASE,
            MONEY,
            DSUSER,
            DSDIVISION,
            DOCLIST,
            DOCKIND_BUTTON,
            DOCUMENT,
            DROOLS_BUTTON,
            LIST_VALUE,
            DICTIONARY,
            DOCUMENT_PERSON,
            DOCUMENT_USER_DIVISION,
            DOCUMENT_USER_DIVISION_SECOND,
            DOCUMENT_POSTAL_REG_NR,
            DOCUMENT_BARCODE,
            DOCUMENT_OFFICENUMBER,
            DOCUMENT_DATE,
            DOCUMENT_DESCRIPTION,
            DOCUMENT_JOURNAL,
            DOCUMENT_PARAMS,
            DOCUMENT_OFFICE_DELIVERY,
            DOCUMENT_IN_OFFICE_KIND,
            DOCUMENT_IN_INVOICE_KIND,
            DOCUMENT_IN_OFFICE_STATUS,
            DOCUMENT_OFFICE_CASE_SYMBOL,
            DOCUMENT_IN_STAMP_DATE,
            DOCUMENT_REFERENCE_ID,
            DOCUMENT_IN_SUBMIT_TO_BIP,
            DOCUMENT_IN_LOCATION,
            DOCUMENT_IN_ORIGINAL,
            DOCUMENT_OTHERREMARKS,
            DOCUMENT_PACKAGEREMARKS,
            DOCUMENT_OUT_OFFICE_PREPSTATE,
            DOCUMENT_OUT_PRIORITY,
            DOCUMENT_OFFICE_CLERK,
            DOCUMENT_IN_TRACKING_NUMBER,
            DOCUMENT_IN_TRACKING_PASSWORD,
            DOCUMENT_AUTOR,
            DOCUMENT_AUTOR_DIVISION,
            DOCUMENT_ABSTRAKT_DESCRIPTION,
			MAIL_WEIGHT,
            LINK,
            ATTACHMENT,
            HTML,
            HTML_EDITOR,
            DOCUMENT_PARAMS,
            DOCUMENT_FIELD,
            FULL_TEXT_SEARCH,
            DOCUMENT_OUT_ZPODATE,
            DOCUMENT_OUT_ZPO,
            DOCUMENT_OUT_ADDITIONAL_ZPO,
            EMAIL_MESSAGE,
            BUTTON,
            DOCUMENT_OUT_GAUGE,
            USER_DIVISION,
            ACCESS_TO_DOCUMENT,
            CACHEABLE_SOURCE_ENUM,
            SIMPLE_PROCESS_VARIABLE,
            SIMPLE_PROCESS_ACTIVITY_NAME
    )));

    public static final String STYLE_ALL_VISIBLE = "all-visible";
    public static final String STYLE_NOT_VISIBLE = "not-visible";
    public static final String STYLE_CHOSEN_VISIBLE = "chosen-visible";
    public static final String STYLE_NORMAL = "normal";
    public static final String STYLE_CN = "cn";
    public static final String STYLE_CN_COLON = "cn-by-colon";
    public static final String STYLE_CN_ONLY = "cn-only";

    public static final String MATCH_EXACT = "exact";
    public static final String MATCH_CONTEXT = "context";
    /** start context - wyszukiwanie jak LIKE je�li w zapytaniu s� gwiazdki lub procenty np 123* i 123% wyszuka LIKE "123%" */
    public static final String MATCH_STAR_CONTEXT = "star-context";
    public static final String MATCH_CONTEXT_CASE_SENSITIVE = "context-case-sensitive";

	
    /** ======================== INSTANCE FIELDS ============================ **/

    private final int id;
    private final String cn;
    private final String column;
    private final String type;
    
    private String dockindCn;

    /** Domy�lna warto�� pola - jest przekazywana do metody simpleCoerce */
    private String defaultValue;

	private Map<String,String> names;
    /** Okre�la czy to pole mo�e mie� wiele warto�ci */
    private boolean multiple;
    /** Okresla nazwe tabeli dla wielowartosciowego enuma zagniezdzonego w slowniku */
    private String multipleDictionParam;
    private Boolean correction;
    /** okre�la spos�b w jaki ma by� przedstawione pola "referencyjne" (ENUM_REF)*/
    private String refStyle;   
    protected String classname;
    private String match;
    private String infoBox;
    private boolean hidden;
    private boolean saveHidden = false;
    private boolean readOnly;
    private boolean autofocus;
    private boolean submit = true;
    private boolean disabled;
    private String onchange;
    protected String logic;
    protected FieldLogic fieldLogic;
    private boolean disableSubmitDictionary = false;
    private boolean refreshMultipleDictionary = false;
    private String coords;
    private boolean autohideMultipleDictionary;
    private String jsPopupFunction;
    private Map<String, Popup> popups = new HashMap<String, Popup>();
    /** okre�la czy pole mo�e przyjmowa� warto�� r�wn� null */
    private boolean allowNull = false;
	/**
	 * wymu� w dwr, hidden na true, je�li pole jest puste oraz disabled=true
	 */
	private boolean hiddenOnDisabledEmpty = false;

    /**
     * ladowanie enumow dla pola w slowniku odbywa sie po kliknieciu na select
     */
    private boolean lazyEnumLoad = false;


    /**
     * Parametry filda (uzywane przez dwr)
     */
    private Map<String, Object> params;
    /**Pole enum typu eutocomplete*/
    private boolean auto;
    /**
     * Wyra�enie regularne dopasowuj�ce podpowiedzi dla pola z ustawionym podpowiadaniem (autocomplete)
     */
    private String autocompleteRegExp;
    /** Okre�la czy pole ma by� brane pod uwage przy wyszukiwaniu duplikat�w */
    private boolean keyMember;
    /** Okre�la czy to pole jest obowi�zkowe. */
    private boolean required;
    /**
     * Okre�la, przy kt�rych warto�ciach g��wnego pola to pole ma by� obowi�zkowe. Gdy ten atrybut jest
     * 'pusty' to b�dzie obowi�zkowe zawsze niezale�nie od warto�ci g��wnego pola.
     */
    private List<Condition> requiredWhen = Collections.emptyList();
    /** Okre�la przy, kt�rych warto�ciach g��wnego pola ma by� widoczne na formatce to pole */
    private List<Condition> availableWhen = Collections.emptyList();

    /**
     * Je�li true to znaczy, �e dane pole wymaga specyficznej obs�ugi po stronie JSP, kt�ra nie zosta�a
     * uog�lniona. W�wczas to pole b�dzie obs�u�one w wyj�tkowy spos�b - wymaga zaimplementowania obs�ugi
     * w "/common/dockind-specific-fields.jsp".
     */
    private boolean specific;
    /**
     * Okre?la czy wyszukiwanie normalne czy po zakresie - ma sens tylko dla p?l liczbowych i tekstowych.
     * Zatem dla p?l innych ni? LONG, INTEGER, FLOAT, STRING nie powinno si? tej flagi ustawia?.
     */
    private boolean searchByRange;
    /** 
     * true <=> to pole jest wy??czone z wyszukiwarki - nie mo?na po nim wyszukiwa? i nie pojawia si?
     * na wynikach wyszukiwanie
     */
    private boolean searchHidden;
    /** 
     * true <=> to pole Hidden jest w��czone w wyszukiwarce ma sens tylko dla pol Hidden
     */
    private boolean searchShow;
    /** 
     * true <=> to pole Hidden jest w??czone w wyszukiwarce ma sens tylko dla pol Hidden
     */
    private boolean newDocumentShow;
    /**
     * Uprawnienia edycji tego pola w postaci listy "guid-?w" grup/dzia??w maj?cych te uprawnienia.
     */
    private List<String> editPermissions;
    /**
     * okre�la czy aktualnie zalogowany u�ytkownik ma prawo edytowa� warto�� tego pola - wyliczane
     * na podstawie editPermissions
     */
    private boolean canEdit;
    /**
     * Okre?la czy pole u?ywane przez jak?? dokonan? akceptacj? - je?li tak to nie mo?na tego pola edytowa?.
     * Zatem dane pole mo?na edytowa? gdy (canEdit && !usedByAcceptance).
     */
    private boolean usedByAcceptance;
    
    /**Okre�la czy pole ma byc wy�wietlone jako input/czy inne pole czy jako normalny tekst*/
    private boolean asText;
    /**Je�li pole jest multiple okre�la czy ma by� wy�wietlone jako tabela */
    private boolean asTable;
    /**Object s�ownika*/
    private AbstractDocumentKindDictionary dicObject;

    private FieldFormatting formatting;

    private String dwrFieldset = "default";
    private int dwrColumn = 0;
	private String cssClass = "";
	private String format;
	private Boolean showDictionaryPopUp;
	private boolean cantUpdate = false;
	private boolean onlyPopUpCreate;
	/** wiadomosc pokazujaca sie uzytkownikowi w momencie doanai nowego wpisu z formatki glwnoej dokumentu */
	private String newItemMessage = sm.getString("NowyWpis");
	
	/** w�a�ciwo�� wskazuj�ca czy pole powinno wywo�ywa� listenery wskazane w atrybucie listenersList */
	private Boolean submitForm;
	/** lista z nazwami listener�w, kt�re powinny zosta� wywo�ane w przypadku ustawienia parametru submitForm na true */
	private List<String> listenersList;

	/**
	 * Czy pole typu "string" ma być przeszukiwane poprzez full-text-search
	 * Tworzenie indeksów pod full-text search: /sql/updates/example_full_text_search.sql
	 */
	private boolean isFullTextSearch;
    /**
     * Uruchamianie walidacji
     */
    private boolean validate;
    /**
     * Sortowanie pól w słowniku wielowartościowym
     */
    private Sortable sortable;
    
    /**
     * wylacznie dla multislownika. Pole oznacza czy zapisane wartosci maja uwzgledniac kolejnosc wprowadzania na formatce
     * Bez ustawienia wartosci saveInTheOrder zapis slownika do bazy jest w kolejnosci losowej
     */
	private boolean saveInTheOrder;
    private boolean autocleanEmptyItems;

    public static enum Sortable {
        ASCENDING("ascending"), DESCENDING("descending");
        private String value;

        private Sortable(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public static Sortable fromString(String value) {
            for(Sortable sortable : Sortable.values()) {
                if(sortable.getValue().equalsIgnoreCase(value)) {
                    return sortable;
                }
            }

            return null;
        }
    }
	
    /**
     * Nieudokumentowany wcze�niej konstruktor klasy Field, poni�ej moje badania (strza�y)
     * @param id id z xml'a, musi by� unikalne, mo�e by� null
     * @param cn cn z xml'a, musi b� unikalne
     * @param column nazwa kolumny, nie mo�e by� null (niestety)
     * @param names nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param type typ fielda, musi znajdowa� si� w Field.TYPES_AVAILABLE
     * @param refStyle styl wy�wietlania powi�zanych warto�ci, mo�e by� null, niekt�re fieldy tego potrzebuj�, ale wszystkie maj� takie pole
     * @param classname niekt�re fieldy tego potrzebuj�, ale wszystkie maj� takie pole
     * @param required czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws EdmException
     */
    public Field(String id, String cn, String column, Map<String, String> names, String type, String refStyle,
                 String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen)
            throws EdmException {

        if (column == null && !DSUSER.equals(type))
            throw new NullPointerException("no column for field: " + cn);

        Preconditions.checkNotNull(names, "names cannot be null");
        
        if(!TYPES_AVAILABLE.contains(type)){ //sprawdza tak�e czy typ jest null
            throw new EdmException(sm.getString("NieznanyTypPola")+": "+type);
        }

        if ((CLASS.equals(type) || CENTRUM_KOSZT.equals(type)) && classname == null)
            throw new EdmException(sm.getString("PoleTypuClassMusiMiecPodanaNazweKlasy"));

        if (cn.contains(" ")){
        	throw new EdmException("CN field nie mo�e zawiera� spacji!" + cn);
        }

        if(id != null){
            this.id = Integer.valueOf(id);
        } else {
            this.id = UNIQUE_ID.getAndIncrement();
            log.info("generated id = {} for cn = '{}'", this.id, cn);
        }
        this.cn = cn;
        this.column = column;
        this.names = names;
        this.type = type;
        this.refStyle = refStyle;
        this.classname = classname;
        this.availableWhen = ((availableWhen == null) ? Collections.<Condition>emptyList(): availableWhen);
        this.required = required;
        this.requiredWhen = ((requiredWhen == null ) ?  Collections.<Condition>emptyList() : requiredWhen);
        
    	if(dicObject == null && classname != null)
    	{
	    	try 
			{
				Class c = Class.forName(getClassname());
				dicObject = (AbstractDocumentKindDictionary) c.getConstructor().newInstance();
				
			} 
			catch (Exception e)
			{
				log.error(e.getMessage(),e);
			}		
    	}
    }

    
    /**
     * Tworzy FieldValue na podstawie documentId i resultseta, w przypadku p�l nie zapisywanych w kolumnie
     * metoda wci�� jest wywo�ywana
     * @param documentId
     * @param rs
     * @return
     * @throws SQLException
     * @throws EdmException
     */
    public abstract FieldValue provideFieldValue(long documentId, ResultSet rs)  throws SQLException, EdmException;

    /**
     * Konwertuje przekazany klucz do obiektu (klucz, obiekt -> patrz opis Field).
     * W wielu przypadkach wystarczy zwr�ci� przekazan� warto�� (np MoneyField, StringField itp).
     *
     * @param key, mo�e by� null
     * @return obiekt, mo�e by� null
     * @throws IllegalArgumentException je�li przekazany obiekt nie jest kluczem
     */
    public Object asObject(Object key) throws IllegalArgumentException, EdmException{
        return key;
    }

    /**
     * Czy dane pole jest zapisywane w kolumnie w tabeli dla danego dockindu
     * (og�lnie, je�eli zwraca false to dane pole nigdy nie jest
     * zapisywane w pojedynczej kolumnie i korzysta z innego mechanizmu)
     *
     * Dana instancja Field powina zawsze zwraca� t� sam� warto�� (powinna
     * by� na sztywno wpisana w definicj� klasy lub pobierana z finalnego pola)
     *
     * @return true je�li pole korzysta ze standardowego mechanizmu zapisu,
     *          false, w przeciwnym przypadku
     */
    public boolean isPersistedInColumn(){
        return true;
    }

    /**
     * Funkcja uzupe�nia parametr o podanym indeksie w prepared statemencie,
     * musi by� przeci��ona w klasach implementuj�cych kt�re zwracaj� true w isPersistedInColumn,
     * implementacje powinny korzysta� z simpleCoerce
     * (w takim przypadku nie mo�na wo�a� super.set)
     *
     * Funkcja jest wywo�ywana tylko w przypadku gdy isPersistedInColumn = true
     *
     * @param ps
     * @param index indeks parametru 
     * @param value warto��, mo�e by� null (trzeba uwa�a� w przypadku unboxingu)
     * @throws FieldValueCoerceException
     * @throws SQLException
     */
    public abstract void set(PreparedStatement ps, int index, Object value)
                    throws FieldValueCoerceException, SQLException;

    /**
     * Zwraca obiekt, kt�rego metoda .toString zwraca "czyteln�" reprezentacj�
     * danej warto�ci, wykorzystanie tej metody zale�y od zawarto�ci
     * dockind-fields.jsp
     *
     * @param value dany obiekt, mo�e by� null
     * @return
     */
    public Object provideDescription(Object value){
        return value;
    }

    /**
     * Konwertuje przekazany obiekt do klucza reprezentowanego przez
     * to pole.  Je�eli obiekt jest ju� ��danej klasy, nie s� w nim
     * dokonywane �adne zmiany z wyj�tkiem sytuacji, gdy jest to
     * napis przekraczaj�cy d�ugo�� pola - w�wczas warto�� jest skracana.
     * <p>
     * Dla pola typu ENUM lub ENUM_REF klas� jest java.lang.Integer, a dla typu CLASS java.lang.Long.
     * <p>
     * Je�eli warto�� nie mo�e by� bezb��dnie skonwertowana (np.
     * niepoprawna tekstowa posta� daty), rzucany jest wyj�tek
     * FieldValueCoerceException.
     * @param value tablica obiekt�w, Iterable (w przypadku multiple) lub pojedyncza warto��
     * @return lista kluczy (w przypadku multiple) lub pojedynczy klucz
     * @throws FieldValueCoerceException w przypadku gdy nie da si� danego obiektu
     * konwertowa� do klucza
     */
    public final Object coerce(Object value) throws FieldValueCoerceException {
        if (isMultiple()) {
        	if (value==null)
        		return null;
            List<Object> list = new ArrayList<Object>();
            if (value instanceof Object[]) {
                for (Object object : (Object[]) value) {
                    Object o = simpleCoerce(object);
                    if (o != null) {
                        list.add(o);
                    }
                }
            } else if (value instanceof Iterable) {
                for (Object object : (Iterable) value) {
                    Object o = simpleCoerce(object);
                    if (o != null) {
                        list.add(o);
                    }
                }
            } else {
                list.add(simpleCoerce(value));
            }
            return list;
        } else {
            return simpleCoerce(value);
        }
    }  
    
    /**
     * Konwertuje przekazany pojedynczy obiekt (w sensie javowym) do pojedynczego klucza. Metoda pr�buje na podstwie typu
     * przekazanego pr�buje uzyska� prawid�owy klucz. Zwracana warto�� musi zawsze byc takiego samego typu.
     * 
     * Dzia�a nie zwa�aj�c na pola wielokrotne, dlatego te�
     * powinno si� zawsze wywo�ywa� funkcj� {@link #coerce}, a ta funkcja powinna by� wo�ana tylko
     * wewn�trz {@link #coerce} lub w�wczas, gdy �wiadomie chcemy przekonwertowa� pojedyncz� warto��.
     *
     * @param value najcz�ciej String ale mo�e tak�e by� typ docelowy albo klucz
     * @return klucz odpowiadaj�cy danej warto�ci
     */
    public abstract Object simpleCoerce(Object value) throws FieldValueCoerceException;

    /**
     * Wywo?anie metody remoteMethod na klasie zwi?zanej z tym polem. Ma sens tylko dla p?l
     * typu CLASS.
     * @param thisMethod nazwa metody lokalnej, kt?ra zosta?a wywo?ana
     * @param remoteMethod nazwa metody zdalnej
     * @param object obiekt, na kt?rym wywo?a? metod? - gdy null wo?ana b?dzie metoda statyczna
     *przemek mowi ze to jest 10 razy wolniejsze 
     */
    @Deprecated 
    public Object invokeMethod(String thisMethod, String remoteMethod, Object object){
        return invokeMethod(thisMethod, remoteMethod, object, null, null);
    }    
    
    /**
     * Wywo?anie metody remoteMethod na klasie zwi?zanej z tym polem. Ma sens tylko dla p?l
     * typu CLASS.
     * @param thisMethod nazwa metody lokalnej, kt?ra zosta?a wywo?ana
     * @param remoteMethod nazwa metody zdalnej
     * @param object obiekt, na kt?rym wywo?a? metod? - gdy null wo?ana b?dzie metoda statyczna
     * @param params parametry, kt?ry b?dzie przekazany wo?anej metodzie
     * @param paramClass klasa parametru, kt?ry b?dzie przekazany wo?anej metodzie
     *przemek mowi ze to jest 10 razy wolniejsze 
     */
    @Deprecated 
    Object invokeMethod(String thisMethod, String remoteMethod, Object object, Object[] params, Class[] paramClass)
    {
        if (!CLASS.equals(type) && !CENTRUM_KOSZT.equals(type))
            throw new IllegalStateException("Metoda '"+thisMethod+"' mo?e by? wo?ana tylko dla typu CLASS i CENTRUM_KOSZT");
        try
        {
            Class c = Class.forName(getClassname());
            Method method = c.getDeclaredMethod(remoteMethod, paramClass == null ? new Class[]{} : paramClass);
            return method.invoke(object, params == null ? new Object[]{} : params );
        }
        catch (ClassNotFoundException e)
        {
            throw new IllegalStateException(sm.getString("PoleMaOkreslonsNierozpoznanaKlase")+": "+getClassname(), e);
        }
        catch (NoSuchMethodException e)
        {
            throw new IllegalStateException(sm.getString("NieIstniejeMetoda")+" '"+remoteMethod+"' "+sm.getString("wKlasie")+": "+getClassname(), e);
        }
        catch (Exception e) 
        {
            throw new IllegalStateException(sm.getString("BladPodczasWolaniaMetody")+" '"+remoteMethod+"' "+sm.getString("wKlasie")+": "+getClassname(), e);
        }
    }
    
    /**
     * Sprawdza czy dla tego pola istnieje przeciazenie tworzenia query
     * typu CLASS.
     */
    public boolean isOverloadQuery(String property){
    	return dicObject.isOverloadQuery(property);
    }
    
    
    /**
     * Sprawdza czy dla tego pola istnieje przeciazenie tworzenia query
     * typu CLASS.
     * @param query 
     * @param value 
     */
    public void addOverloadQuery(String property, String value, DockindQuery query){
    	dicObject.addOverloadQuery(property, value, query);
    }

    /**
     * Zwraca 'akcj? s?ownikow?' dla klasy, z kt?r? zwi?zane jest to pole. Ma sens tylko dla p?l
     * typu CLASS.
     */
    public String getDictionaryAction()
    {
    	return dicObject.dictionaryAction();
    }
    
    /**
     * Zwraca 'akcj? s?ownikow?' dla klasy, z kt?r? zwi?zane jest to pole. Ma sens tylko dla p?l
     * typu CLASS.
     */
    public String getDictionaryAccept()
    {
    	return dicObject.dictionaryAccept();
    }
    
	public boolean isAjaxAvailable() 
	{
		return dicObject.isAjaxAvailable();
	}
	
	public boolean isBoxDescriptionAvailable()
	{
		return dicObject.isBoxDescriptionAvailable();
    }
	
	public  List<String> additionalSearchParam()
	{
		return dicObject.additionalSearchParam();
	}

	
	public Integer getHeight()
	{ 
		return dicObject.getHeight();
	}
	
	public abstract pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException;
	
	public pl.compan.docusafe.core.dockinds.dwr.Field getFieldForDwr(Map<String, SchemeField> schemeFields) throws EdmException 
	{
		SchemeField schemeField = null;
		if (schemeFields != null && schemeFields.containsKey(this.getCn()))
			schemeField = schemeFields.get(this.getCn());
		
		return getFieldForDwr(schemeField);
	}
	
	public pl.compan.docusafe.core.dockinds.dwr.Field getFieldForDwr(SchemeField scheme) throws EdmException 
	{
		pl.compan.docusafe.core.dockinds.dwr.Field field = null;
		try
		{
			field = getDwrField();
			field.setHidden(isHidden());
			field.setHiddenOnDisabledEmpty(isHiddenOnDisabledEmpty());
            field.setLazyEnumLoad(isLazyEnumLoad());
            field.setAsText(isAsText());
			field.setDisabled(isDisabled());
			field.setRequired(isRequired());
            field.setAutofocus(isAutofocus());
			field.setReadonly(isReadOnly());
			field.setColumn(getDwrColumn());
			field.setFieldset(getDwrFieldset());
			field.setSubmit(isSubmit());
			field.setCssClass(getCssClass());
			field.setFormat(getFormat());
			field.setMultiple(isMultiple());
            field.setPopups(getPopups());
            field.setValidate(isValidate());
			
			if (scheme != null)
			{
				field.setDisabled(scheme.getDisabled());
				
				if (scheme.isReadonly() == null && scheme.isRequired() == null && scheme.isHidden() == null && !scheme.getDisabled())
					field.setHidden(scheme.isVisible());
				
				if (scheme.isRequired() != null)
					field.setRequired(scheme.isRequired());
				
				if (scheme.isReadonly() != null)
					field.setReadonly(scheme.isReadonly());
				
				if (scheme.isHidden() != null)
					field.setHidden(scheme.isHidden());
				
				if (scheme.getCssClass() != null)
					field.setCssClass(scheme.getCssClass());
			}
			checkPermissions();
            if (scheme == null || (scheme != null && scheme.isReadonly() == null))
			    field.setReadonly(!isCanEdit());
				
			field.setCoords(coords);
		}
		catch (Exception e)
		{
			throw new EdmException("Brak filda dwr dla filda o cn: " + getCn() + " typu: " + getType());
		}
	   
	    return field;
	}
	
	public Integer getWidth()
	{
		return dicObject.getWidth();
	}
	
    /**
     * Zwraca map� atrybut�w klasy zwi�zanej z tym polem, kt�re maj� by� wy�wietlane na formatce
     * obok przycisku wo�aj�cego 'akcj� s�ownikow�'. Ma sens tylko dla p�l typu CLASS.
     */
    public Map<String,String> getDictionaryAttributes()
    {
    	return dicObject.dictionaryAttributes();
    }

    public Map<String,String> getPopUpDictionaryAttributes() 
    {
    	return dicObject.popUpDictionaryAttributes();
    }
    
    public Map<String,String> getPromptDictionaryAttributes() 
    {
    	return dicObject.promptDictionaryAttributes();
    }
    /**
     * Zwraca nazw� kolumny w bazie, po kt�rej maj� by� sortowane wyniki wyszukiwania rodzaj�w dokument�w,
     * gdy u�ytkownik wybierze sortowanie po obiektach tej klasy. Ma sens tylko dla p�l typu CLASS.
     */
    public String getDictionarySortColumn()
    {
    	return dicObject.dictionarySortColumn();
    }
    
    /**
     * Na podstawie listy 'editPermissions' ustala czy aktualnie zalogowany u?ytkownik ma prawo edytowa? 
     * to pole. Wynik zapisywany na zmienna 'canEdit'.
     * WCALE TAK NIE ROBI :)
     */
    public void checkPermissions() throws EdmException
    {   
        if (readOnly)
        {
            canEdit = false;
            return;
        }
        if (DSApi.context().isAdmin())
        {
            canEdit = true;
            return;
        }
        else
            canEdit = false;
        if (editPermissions != null && editPermissions.size() > 0)
        {
        	//DSDivision[] divisions = DSApi.context().getDSUser().getDivisions();
        	DSDivision[] divisions = DSUser.findByUsername(DSApi.context().getPrincipalName()).getDivisions();
        	
            for (DSDivision division : divisions)
            {
                if (editPermissions.contains(division.getGuid()))
                {
                    canEdit = true;
                    break;
                }
            }
        }
        else
        {
            canEdit = true;
        }
    }
    
    /**
     * Metoda zwraca uprzednio zainicjalizowan� b�d� now� instancj� obiektu typu FieldLogic utworzon� na podstawie zadeklarowanej warto�ci atrybutu logic z typu pola dockinda 
     * @return obiekt reprezentuj�cy logik� danego pola b�d� null w przypadku b��du b�d� braku deklaracji logiki
     */
    public FieldLogic prepareFieldLogic(){
    	if (logic != null){
            try{
                if(fieldLogic != null && fieldLogic.getClass().getCanonicalName().equals(logic)){
                	return fieldLogic;
                }else{
                	Class c = Class.forName(logic);
	                fieldLogic = (FieldLogic) c.getConstructor().newInstance();
	                fieldLogic.setField(this);
	                return fieldLogic;
                }
            }
            catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    	return null;
    }

	/**
	 * Sprawdza czy na kolumnie w tabeli włączone jest "full-text-search".
	 *
	 * Tworzenie indeksów pod full text search : /sql/updates/example_full_text_search.sql
	 *
	 * @param tableName
	 * @param field
	 * @return "true" jeśli jest na tej kolumnie taka opcja
	 * @return "false" jeśli nie ma na kolumnie nie ma takiej opcji
	 * @throws EdmException
	 */
	public static boolean fullTextSearchEnabledOnColumn(String tableName, Field field) throws EdmException
	{
		if (tableName == null)
			throw new EdmException("Brak nazwy tablicy dla sprawdzenia full-text-search przy polu: " + field.getCn());
		if (field == null || field.getColumn() == null)
			throw new EdmException("Brak kolumny dla sprawdzenia full-text-search przy polu: " + field.getCn());
		PreparedStatement pst;
		try
		{
			if (DSApi.isSqlServer())
			{
				pst = DSApi.context().prepareStatement("SELECT COLUMNPROPERTY(OBJECT_ID(?), ?, ?)" );
				pst.setString(1, tableName);
				pst.setString(2, field.getColumn());
				pst.setString(3, "IsFulltextIndexed");

				ResultSet rs = pst.executeQuery();
				if (!rs.next())
					return false;

				int textSearchEnabled = rs.getInt(1);

				if (textSearchEnabled == 1)
					return true;
			}
			else if (DSApi.isOracleServer())
			{
				pst = DSApi.context().prepareStatement("SELECT all_ind_columns.column_name, all_ind_columns.table_name FROM all_indexes " +
						"INNER JOIN all_ind_columns ON all_ind_columns.index_name = all_indexes.index_name " +
						"WHERE all_indexes.table_name = ? " +
						"AND all_ind_columns.column_name = ? " +
						"AND all_indexes.ITYP_OWNER = ? " +
						"AND all_indexes.ITYP_NAME = ? ");
				pst.setString(1, tableName.toUpperCase());
				pst.setString(2, field.getColumn().toUpperCase());
				pst.setString(3, "CTXSYS");
				pst.setString(4, "CONTEXT");
				ResultSet rs = pst.executeQuery();
				if (rs.next())
					return true;
			}
			else
			{
				throw new EdmException("Nie potrafie zdefiniowac rodzaju bazy danych");
			}
		} catch (SQLException e)
		{
			log.error(e.getMessage(), e);
		} catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		return false;
	}
    
    public void setUsedByAcceptance(boolean usedByAcceptance)
    {
        this.usedByAcceptance = usedByAcceptance;
    }
    
    public boolean isCanEdit()
    {
    	return canEdit && !usedByAcceptance;
    }
    
    public int getId() {
        return id;
    }

    public String getCn()
    {
        return this.cn;
    }

    /**
     * Konwertuje cny po� s�ownik�w. je�li jest wielowarto�ciowy uscina ostatni� cyfr� pola maja zazwyczaj format
     * S�OWNIK_POLE_X gdzie X to cyfra np. SENDER_COUNTRY_1.
     *
     * jesli nie jest wielowartosciowy zwraca normalny cn.
     * @param isMultiple czy s�ownik ( rodzic pola ) jest wielowartosciwoy
     * @return
     */
    public String getCnWithoutLastUnderscore(boolean isMultiple)
    {
        int index = this.cn.indexOf(MULTI_FIELD_SEPARATOR);
        if(isMultiple && index > 0 && isLastNumber())
            return StringUtils.substringBeforeLast(this.cn, MULTI_FIELD_SEPARATOR);
        else
            return this.cn;
    }

    /**
     * Czy ostatnie wyst�pienie textu po ostatnim podkre�lniku jest cyfr�.
     * @return
     */
    private boolean isLastNumber(){
        String c = StringUtils.substringAfterLast(this.cn, MULTI_FIELD_SEPARATOR);
        return StringUtils.isNumeric(c);
    }
    /**
     * Zwraca FieldData u�ywane w obiektach dwr
     * @param value
     * @return
     */
    public FieldData getFieldData(Object value){
       return new FieldData(getType(), value);
    }

    public String getColumn()
    {
        return column;
    }

    public String getName()
    {        
        return Docusafe.getLanguageValue(names);       
    }

    public final String getType() {
        return type;
    }

    public String getRefStyle()
    {
        return refStyle;
    }

    public void setRefStyle(String refStyle)
    {
        this.refStyle = refStyle;
    }

    public String getClassname()
    {
        return classname;
    }

    public void setClassname(String classname)
    {
        this.classname = classname;
    }
    
//    public List<EnumItem> getEnumItemsByAspect(String aspectCn,  FieldsManager fm) throws EdmException
//    {
//    	throw new IllegalStateException("Wywo?anie getEnumItems() ma sens tylko dla pola typu "+ENUM+" lub "+ENUM_REF+" lub "+DATA_BASE);
//    }
    
    public void initField()
    {
    	
    }

    public List<EnumItem> getEnumItemsByAspect(String aspectCn, DocumentKind documentKind) throws EdmException {
        throw new UnsupportedOperationException();
    }

    public List<EnumItem> getEnumItems() {
        throw new UnsupportedOperationException(cn);
    }

    public Collection<EnumItem> getSortedEnumItems() {
        throw new UnsupportedOperationException(cn);
    }

    public List<EnumItem> getEnumItems(Object discriminator) {
        throw new UnsupportedOperationException(cn);
    }

    public EnumItem getEnumItem(Integer id) throws EntityNotFoundException {
        throw new UnsupportedOperationException(cn);
    }

    public EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException {
        throw new UnsupportedOperationException(cn);
    }

    public EnumItem getEnumItemByTitle(String cn) throws EntityNotFoundException {
        throw new UnsupportedOperationException(cn);
    }
    
    public String getRefField() throws EntityNotFoundException {
        throw new UnsupportedOperationException(cn);
    }
    
    public SortedSet<EnumItem> getRefEnumItems(String id)  throws EntityNotFoundException {
    	 throw new IllegalStateException("Wywo?anie getRefEnumItems(id) ma sens tylko dla pola typu "+DATA_BASE);
	}
    
    public boolean isShowAttachment() {
        throw new UnsupportedOperationException(cn);
	}
    
    public String getMatch()
    {
        return match;
    }

    public void setMatch(String match)
    {
        this.match = match;
    }

    public boolean isHidden()
    {
        return hidden;
    }

    public void setHidden(boolean hidden)
    {
        this.hidden = hidden;
    }
    
    public boolean isSaveHidden()
    {
        return saveHidden;
    }

    public void setSaveHidden(boolean saveHidden)
    {
        this.saveHidden = saveHidden;
    }

    public boolean isHiddenOnDisabledEmpty() {
		return hiddenOnDisabledEmpty;
	}

	public void setHiddenOnDisabledEmpty(boolean hiddenOnDisabledEmpty) {
		this.hiddenOnDisabledEmpty = hiddenOnDisabledEmpty;
	}

    public boolean isLazyEnumLoad() {
        return lazyEnumLoad;
    }

    public void setLazyEnumLoad(boolean lazyEnumLoad) {
        this.lazyEnumLoad = lazyEnumLoad;
    }

    public boolean isReadOnly()
    {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly)
    {
        this.readOnly = readOnly;
    }

    public boolean isMultiple()
    {
        return multiple;
    }

    public void setMultiple(boolean multiple)
    {
        this.multiple = multiple;
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }

    public List<Condition> getRequiredWhen()
    {
        return requiredWhen;
    }

    public void setRequiredWhen(List<Condition> requiredWhen)
    {
        this.requiredWhen = requiredWhen;
    }

    public List<Condition> getAvailableWhen()
    {
        return availableWhen;
    }

    public void setAvailableWhen(List<Condition> availableWhen)
    {
        this.availableWhen = availableWhen;
    }

    public boolean isSpecific()
    {
        return specific;
    }

    public void setSpecific(boolean specific)
    {
        this.specific = specific;
    }         

    public List<String> getEditPermissions()
    {
        return editPermissions;
    }

    public void setEditPermissions(List<String> editPermissions)
    {
        this.editPermissions = editPermissions;
    }

    public boolean isSearchByRange()
    {
        return searchByRange;
    }

    public void setSearchByRange(boolean searchByRange)
    {
        this.searchByRange = searchByRange;
    }
    
    public boolean isSearchHidden()
    {
        return searchHidden;
    }

    public void setSearchHidden(boolean searchHidden)
    {
        this.searchHidden = searchHidden;
    }

    public static FieldValue readMultiple(Field field, long documentId, String tableName) throws EdmException {
        StringBuilder sql = new StringBuilder("select field_val from ").append(tableName).append(
                " where field_cn=? and document_id=?");
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement(sql.toString());
            ps.setString(1, field.getCn());
            ps.setLong(2, documentId);
            ResultSet vrs = ps.executeQuery();
            List<Object> values = null;
            while (vrs.next())
            {
                String v = vrs.getString(1);
                Object value = field.simpleCoerce(v);

                if (value != null)
                {
                	if (values==null)
                		values = new ArrayList<Object>();
                	values.add(value);
                }
            }
            return new FieldValue(field, values);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
        }
    }

    public static class Condition implements Serializable
    {
        public static String TYPE_MAIN = "main";
        // TODO: obecnie typ 'other' dziala tylko dla pola o typie enum-ref
        public static String TYPE_OTHER = "other"; 
        
        private String type;        
        private String fieldCn;        
        private Long fieldValue;

        public String getFieldCn()
        {
            return fieldCn;
        }

        public void setFieldCn(String fieldCn)
        {
            this.fieldCn = fieldCn;
        }

        public Long getFieldValue()
        {
            return fieldValue;
        }

        public void setFieldValue(Long fieldValue)
        {
            this.fieldValue = fieldValue;
        }

        public String getType()
        {
            return type;
        }

        public void setType(String type)
        {
            this.type = type;
        }                
    }
    
    public static class ValidationRule
    {
        private String fieldCn;
        private String type;        
        private String value;

        public String getFieldCn()
        {
            return fieldCn;
        }

        public void setFieldCn(String fieldCn)
        {
            this.fieldCn = fieldCn;
        }

        public String getType()
        {
            return type;
        }

        public void setType(String type)
        {
            this.type = type;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }              
    }

    public boolean isKeyMember()
    {
        return keyMember;
    }

    public void setKeyMember(boolean keyMember)
    {
        this.keyMember = keyMember;
    }

	public boolean isSearchShow() {
		return searchShow;
	}

	public void setSearchShow(boolean searchShow) {
		this.searchShow = searchShow;
	}

	public void setInfoBox(String infoBox) {
		this.infoBox = infoBox;
	}

	public String getInfoBox() {
		return infoBox;
	}

        @Override
	public String toString()
	{
		return getName() + " " + getType()+ " " +getColumn();
	}

	public void setAsText(boolean asText)
	{
		this.asText = asText;
	}

	public boolean isAsText()
	{
		return asText;
	}

	public void setAsTable(boolean asTable)
	{
		this.asTable = asTable;
	}

	public boolean isAsTable()
	{
		return asTable;
	}

	public boolean isNewDocumentShow() {
		return newDocumentShow;
	}

	public void setNewDocumentShow(boolean newDocumentShow) {
		this.newDocumentShow = newDocumentShow;
	}

    public FieldFormatting getFormatting() {
        return formatting;
    }

    public void setFormatting(FieldFormatting formatting) {
        this.formatting = formatting;
    }

	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException {
        throw new UnsupportedOperationException(cn);
	}

    public EnumValues getEnumItemsForDwrByScheme(Map<String, Object> fieldsValues, Map<String, SchemeField> schemeFields)
            throws EdmException {
        return getEnumItemsForDwr(fieldsValues);
    }

    public int getDwrColumn() {
        return dwrColumn;
    }

    public void setDwrColumn(int dwrColumn) {
        this.dwrColumn = dwrColumn;
    }

    public String getDwrFieldset() {
        return dwrFieldset;
    }

    public void setDwrFieldset(String dwrFieldset) {
        this.dwrFieldset = dwrFieldset;
    }
    
    public List<String> getPopUpFields() {
		return null;
	}

	public void setPopUpFields(List<String> popUpFields) {
		
	}

	public List<String> getPromptFields() {
		return null;
	}

	public void setPromptFields(List<String> promptFields) {
		
	}

	public List<String> getSearchFields() {
		return null;
	}

	public void setSearchFields(List<String> searchFields) {
		
	}
	/**
	 * Zwraca List starych (nie DWR) fieldow dockindu
	 */
	public List<Field> getFields() {
		return null;
	}

	public void setFields(List<Field> fields) {

	}
	
	public List<String> getVisibleFields() {
		return null;
	}

	public void setVisibleFields(List<String> visibleFields) {
		
	}
	
	/**
	 * Zwraca list przyciskow na popapie slownikowym
	 */
	public List<String> getPopUpButtons() {
		return null;
	}
	
	public LinkValue getLinks(Object object) throws EdmException {
		throw new UnsupportedOperationException(cn);
	}

	public String getMultipleDictionParam() {
		return multipleDictionParam;
	}

	public void setMultipleDictionParam(String multipleDictionParam) {
		this.multipleDictionParam = multipleDictionParam;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public boolean isAuto() {
		return auto;
	}

	public void setAuto(boolean auto) {
		this.auto = auto;
	}

	public String getLogic() {
		return logic;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
		
	}

	public String getCssClass() {
		return cssClass;
	}

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

	public Boolean getShowDictionaryPopUp() {
		return showDictionaryPopUp = showDictionaryPopUp==null? true: showDictionaryPopUp;
	}

	public void setShowDictionaryPopUp(Boolean showDictionaryPopUp) {
		this.showDictionaryPopUp = showDictionaryPopUp;
	}

	public boolean isSubmit() {
		return submit;
	}

	public void setSubmit(boolean submit) {
		this.submit = submit;
	}

	public Boolean getCorrection() {
		return correction!=null ? correction : false;
	}

	public void setCorrection(Boolean correction) {
		this.correction = correction;
	}

	public boolean isDisableSubmitDictionary()
	{
		return disableSubmitDictionary;
	}

	public void setDisableSubmitDictionary(boolean disableSubmitDictionary)
	{
		this.disableSubmitDictionary = disableSubmitDictionary;
	}

	public Map<String, Object> getParams()
	{
		return params;
	}

	public void setParams(Map<String, Object> params)
	{
		this.params = params;
	}
	
	public void addParam(String key, Object obj)
	{
		if (this.params == null)
			this.params = new HashMap<String, Object>();
		this.params.put(key, obj);
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getFormat() {
		return format;
	}

	public void setCantUpdate(boolean booleanValue)
	{
		this.cantUpdate = booleanValue;
		
	}

	public boolean isCantUpdate()
	{
		return cantUpdate;
	}
	
	public void setOnlyPopUpCreate(boolean onlyPopUpCreate) {
		this.onlyPopUpCreate = onlyPopUpCreate;
		
	}

	public boolean isOnlyPopUpCreate() {
		return onlyPopUpCreate;
	}

	public void setNewItemMessage(String newItemMessage)
	{
		this.newItemMessage = newItemMessage;
	}

	public String getNewItemMessage()
	{
		return newItemMessage;
	}
	
	public void setCoords(String coords) {
		this.coords = coords;
	}

	public String getCoords() {
		return coords;
	}


	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cn == null) ? 0 : cn.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Field other = (Field) obj;
		if (cn == null)
		{
			if (other.cn != null)
				return false;
		}
		else if (!cn.equalsIgnoreCase(other.cn))
			return false;
		return true;
	}

	public void setRefreshMultipleDictionary(boolean refreshMultipleDictionary) {
		this.refreshMultipleDictionary = refreshMultipleDictionary;
	}

	public boolean isRefreshMultipleDictionary() {
		return refreshMultipleDictionary;
	}

	public void setAutocompleteRegExp(String autocompleteRegExp) {
		this.autocompleteRegExp = autocompleteRegExp;
	}

	public String getAutocompleteRegExp() {
		return autocompleteRegExp;
	}

	public void setJsPopupFunction(String jsPopupFunction) {
		this.jsPopupFunction = jsPopupFunction;
	}

	public String getJsPopupFunction() {
		return jsPopupFunction;
	}

	public void setAutohideMultipleDictionary(boolean autohideMultipleDictionary) {
		this.autohideMultipleDictionary = autohideMultipleDictionary;
	}

	public boolean isAutohideMultipleDictionary() {
		return autohideMultipleDictionary;
	}

	public List<String> getDicButtons() {
		return null;
	}

	public String getDockindCn() {
		return dockindCn;
	}

	public void setDockindCn(String dockindCn) {
		this.dockindCn = dockindCn;
	}

	public boolean isAllowNull() {
		return allowNull;
	}

	public void setAllowNull(boolean allowNull) {
		this.allowNull = allowNull;
	}

	public Boolean getSubmitForm() {
		return submitForm;
	}

	public void setSubmitForm(Boolean submitForm) {
		this.submitForm = submitForm;
	}
	
	public List<String> getListenersList() {
		return listenersList;
	}

	public void setListenersList(List<String> listenersList) {
		this.listenersList = listenersList;
	}

    public boolean isAutofocus()
    {
        return autofocus;
    }

    public void setAutofocus(boolean autofocus)
    {
        this.autofocus = autofocus;
    }

    public Map<String, Popup> getPopups() {
        return popups;
    }

    public void setPopups(Map<String, Popup> popups) {
        this.popups = popups;
    }

	public boolean isFullTextSearch()
	{
		return isFullTextSearch;
	}

	public void setFullTextSearch(boolean fullTextSearch)
	{
		isFullTextSearch = fullTextSearch;
	}

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }

    public Sortable getSortable() {
        return sortable;
    }

    public void setSortable(Sortable sortable) {
        this.sortable = sortable;
    }

	public boolean isSaveInTheOrder()
	{
		return saveInTheOrder;
	}

	public void setSaveInTheOrder(boolean saveInTheOrder)
	{
		this.saveInTheOrder= saveInTheOrder;
	}

    public void setAutocleanEmptyItems(boolean autocleanEmptyItems) {
        this.autocleanEmptyItems = autocleanEmptyItems;
    }

    public boolean isAutocleanEmptyItems() {
        return autocleanEmptyItems;
    }
}
