package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.core.users.DSUser;

public class DSUserFieldReader implements FieldReader
{
	private boolean dsuserAsLtNFstNExtName = AvailabilityManager.isAvailable("dsuserFieldAsLastNameFirstNameExternalName");
	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		Field field;
		DSUserEnumField enumField = new DSUserEnumField(refStyle, el.attributeValue("id"), el.attributeValue("cn"), el.attributeValue("column"), names, required, requiredWhen, availableWhen, "true".equals(el.attributeValue("sort")), elType.attributeValue("ref-field"),
				elType.attributeValue("ref-guid"));

		boolean acceptanceSubordinates = Boolean.valueOf(elType.attributeValue("acceptance-subordinates"));

		List<DSUser> users;
		if (acceptanceSubordinates == false)
			users = DSUser.listWithDeleted(DSUser.SORT_LASTNAME_FIRSTNAME);
		else
			users = DSUser.listAcceptanceSubordinates();

		for (DSUser user : users)
		{
			boolean available = !(user.isDeleted());
			Map<String, String> titles = new HashMap<String, String>();
			if (dsuserAsLtNFstNExtName)
				titles.put("pl", user.getLastnameFirstnameWithOptionalIdentifier());
			else
				titles.put("pl", user.asLastnameFirstname());
		
			enumField.addEnum(new Integer(user.getId().toString()), user.getName(), titles, new String[1], false, enumField.getCn(), available);
		}
		field = enumField;
		return field;
	}

}
