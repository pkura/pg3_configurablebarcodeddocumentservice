package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.DocumentOfficeCaseSymbolField;
import pl.compan.docusafe.core.dockinds.field.DocumentOfficeNumberField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class DocumentOfficeCaseSymbolFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String cn, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		return new DocumentOfficeCaseSymbolField((el.attributeValue("id")), el.attributeValue("cn"), cn, names, required, requiredWhen, availableWhen);
	}

}
