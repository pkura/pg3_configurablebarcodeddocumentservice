package pl.compan.docusafe.core.dockinds.field;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class CurrencyEnumField extends DataBaseEnumField {

    private static final Logger log = LoggerFactory.getLogger(CurrencyEnumField.class);
    StringManager sm = GlobalPreferences.loadPropertiesFile(CurrencyEnumField.class.getPackage().getName(), null);

    /**
     * Tworzy i inicjalizuje pole typu dataBase
     *
     * @param id
     * @param cn
     * @param column
     * @param names
     * @param type
     * @param refStyle
     * @param required
     * @param requiredWhen
     * @param availableWhen
     * @param tableName
     * @param refField
     * @param assoField
     * @throws pl.compan.docusafe.core.EdmException
     *
     */
    public CurrencyEnumField(String id, String cn, String column, Map<String, String> names, String type, String refStyle, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String tableName, String refField, String assoField, String correction, boolean auto, boolean emptyOnStart, boolean allowNotApplicableItem, boolean reloadAtEachDwrGet, boolean repoField) throws EdmException {
        super(id, cn, column, names, type, refStyle, required, requiredWhen, availableWhen, tableName, refField, assoField, correction, auto, emptyOnStart, allowNotApplicableItem, reloadAtEachDwrGet, repoField);
    }


    @Override
    public Integer simpleCoerce(Object value) throws FieldValueCoerceException {

        if (value instanceof String){
            Integer defaultId = getEnumIdForCnOrTitle((String)value);
            if (defaultId != null)
                return defaultId;
        }

        return FieldUtils.intCoerce(this, value);
    }

//    @Override
//    public String getDefaultValue() {
//        String defaultValue =  super.getDefaultValue();
//        Integer defaultId = getEnumIdForCnOrTitle(defaultValue);
//        return defaultId != null ? defaultId.toString() : defaultValue;
//    }

    private Integer getEnumIdForCnOrTitle (String value){
        if (StringUtils.isEmpty(value))
            return null;

        Pattern pattern = Pattern.compile("^\\d+$");
        if (!pattern.matcher(value).find()){
            for (EnumItem item : this.getEnumItems()){
                if (item.getCn().equalsIgnoreCase(value) || item.getTitle().equalsIgnoreCase(value))
                    return item.getId();
            }
        }

        return null;
    }
}
