package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class LinkField extends NonColumnField implements LinkFieldLogic
{
    private static final Logger log = LoggerFactory.getLogger(LinkField.class);

    public enum LinkType
    {
        DOCUMENT, NORMAL
    }

    private final LinkType linkType;
    private final String fieldLabel;
    private final String labelColumn;
    private final String linkTarget;
    private LinkFieldLogic logic = null;

    public LinkField(String id, String logic, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String type, String fieldLabel, String linkTarget, String labelColumn) throws EdmException
    {
        super(id, cn, names, Field.LINK, null, null, required, requiredWhen, availableWhen);
        this.linkType = LinkType.valueOf(type.toUpperCase());
        this.fieldLabel = fieldLabel;
        this.linkTarget = linkTarget;
        this.labelColumn = labelColumn;
        if (logic != null)
        {
            try
            {
                Class c = Class.forName(logic);
                this.logic = (LinkFieldLogic) c.getConstructor().newInstance();
            }
            catch (Exception ex)
            {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public LinkFieldLogic getLogicField()
    {
        if (logic != null)
            return logic;
        else
            return this; 
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
    {
        String id = rs.getString(getColumn());
        String value = rs.wasNull() ? null : createLink(id);
        return new FieldValue(this, value, value);
    }

    @Override
    public String simpleCoerce(Object value) throws FieldValueCoerceException
    {
        if (value == null)
            return null;

        if (value instanceof CharSequence)
        {
            return getId((String) value);
        }
        if (value instanceof Long)
        {
            return ((Long) value).toString();
        }
        throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + " cannot be converted to String by DocumentLinkField");
    }

    private static String getId(String link)
    {
        int index = link.lastIndexOf("=");
        return link.substring(++index);
    }

    /** Tworzy URL na podstawie podanego ID dokumentu.
     *  Byloby to przydatniejsze jako metoda statyczna */
    public String createLink(String id) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
    {

        Long docId = null;
        try
        {
            docId = Long.parseLong(id);
        }
        catch (NumberFormatException e)
        {
            docId = Long.parseLong(getId(id));            
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
        }
        Document doc = Document.find(docId,false);
        StringBuilder b = new StringBuilder();
        log.info("PageContext: {}", Docusafe.getPageContext());
        String pageContext = Docusafe.getPageContext();
        if (doc instanceof InOfficeDocument)
        {
            if(pageContext.length() > 0)
                b.append("/");
            b.append(Docusafe.getPageContext() + "/office/incoming/document-archive.action?documentId=").append(docId);
        }
        else if (doc instanceof OutOfficeDocument)
        {
            if (((OutOfficeDocument) doc).isInternal())
            {
                if(pageContext.length() > 0)
                    b.append("/");
                b.append(Docusafe.getPageContext() + "/office/internal/document-archive.action?documentId=").append(docId);
            }
            else
            {
                if(pageContext.length() > 0)
                    b.append("/");
                b.append(Docusafe.getPageContext() + "/office/outgoing/document-archive.action?documentId=").append(docId);
            }
        }
        else
        {
            if(pageContext.length() > 0)
                b.append("/");
            b.append(Docusafe.getPageContext() + "/repository/edit-dockind-document.action?id=").append(docId);
        }
        log.info("Link: {}", b.toString());
        return b.toString();
    }

    public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
    {
    	pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.LINK);
    	field.setLinkTarget(this.getLinkTarget());

    	return field;
    }

    private LinkValue getLink(Object value) throws EdmException
    {
        String label = null;
        Document doc = Document.find(Long.parseLong((String) value));
        if (getLinkLabel().equals("document-title"))
            label = doc.getTitle();
        else if (getLinkLabel().equals("document-description"))
            label = doc.getDescription();
        else 
            label = (String) doc.getFieldsManager().getValue(getLinkLabel());
        String link = createLink((String) value);
        return new LinkValue(label, link);
    }

    public LinkType getLinkType()
    {
        return linkType;
    }

    public String getLinkLabel()
    {
        return fieldLabel;
    }

	public String getLinkTarget() {
		return linkTarget;
	}

	public String getFieldLabel()
	{
		return fieldLabel;
	}

	public String getLabelColumn()
	{
		return labelColumn;
	}

}