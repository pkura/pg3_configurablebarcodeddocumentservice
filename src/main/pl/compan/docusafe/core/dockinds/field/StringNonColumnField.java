package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.*;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.util.TextUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;


public class StringNonColumnField extends NonColumnField implements StringField {

    protected String value;

    private final static String DATEFORMAT_REGEX = "datefromat_regex:";
    /** d�ugo�� stringa */
    private final int length;
    /** wielko�� pola na formatce */
    private final Integer size;
    /**  Wyra�enie regularne do walidacji pola przez javascript */
    private final String validatorRegExp;

    public StringNonColumnField(String id, String cn, Map<String, String> names, Integer size, int length, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String validatorRegExp) throws EdmException {
        super(id, cn, names, pl.compan.docusafe.core.dockinds.field.Field.STRING, null, null, required, requiredWhen, availableWhen);
        this.length = length;
        this.size = size;
        if (validatorRegExp != null)
            this.validatorRegExp = validatorRegExp;
        else
        {
            this.validatorRegExp = "^.{0," + length + "}$";
        }
    }


    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        return new FieldValue(this, value, value);
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        if (value == null){
            return null;
        }
        String s = TextUtils.trimmedStringOrNull(value.toString());
        if (s == null){
            return null;
        }

        if (s.length() > length){
            s = s.substring(0, length);
        }

        /** Je�li chcemy aby domyslna wartosc by�o konretnej daty bierz�cej */
        if(s.startsWith(DATEFORMAT_REGEX))
        {
            String format = s.substring(DATEFORMAT_REGEX.length(), s.length());
            DateFormat df = new SimpleDateFormat(format);

            return df.format(Calendar.getInstance().getTime());
        }
        return s;
    }

    @Override
    public Field getDwrField() throws EdmException {
        pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(),
                this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING);
        field.setAutocompleteRegExp(getAutocompleteRegExp());
        if (getLength() > 250)
            field.setType(pl.compan.docusafe.core.dockinds.dwr.Field.Type.TEXTAREA);
        else
        {
            field.setValidatorRegExp(validatorRegExp);
        }

        return field;

    }

    public int getLength() {
        return length;
    }

    public Integer getSize() {
        return size;
    }
}
