package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static pl.compan.docusafe.util.I18nUtils.string;

public class DocumentOfficeDeliveryField  extends NonColumnField implements CustomPersistence, EnumNonColumnField, SearchPredicateProvider
{

	private static final Logger log = LoggerFactory.getLogger(DocumentOfficeDeliveryField.class);
	private String type = "in";
	private List<EnumItem> enumItems = new ArrayList<EnumItem>();
	
	public DocumentOfficeDeliveryField(String id, String type, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, names, Field.DOCUMENT_OFFICE_DELIVERY, null, null, required, requiredWhen, availableWhen);
		if (type != null)
		{
			this.type = type;
		}

		Map<String,String> titles = new HashMap<String,String>();
		if (type.equals("in"))
		{
			for (InOfficeDocumentDelivery delivery : InOfficeDocumentDelivery.list())
			{
				titles = new HashMap<String,String>();
				titles.put("pl", delivery.getName());
				enumItems.add(new EnumItemImpl(delivery.getId(), "CN_"+delivery.getId(), titles, new String[3], false,this.getCn()));
			}
		}
		else if (type.equals("out"))
		{
			for (OutOfficeDocumentDelivery delivery : OutOfficeDocumentDelivery.list())
			{
				
				titles = new HashMap<String,String>();
				titles.put("pl", delivery.getName());
				enumItems.add(new EnumItemImpl(delivery.getId(), "CN_"+delivery.getId(), titles, new String[3], false,this.getCn()));
			}
		}
	}

	@Override
	public void persist(Document doc, Object key) throws EdmException
	{
		Integer deliverId = null;
		if(key == null) return;
        	if (key instanceof String)
        		deliverId = Integer.valueOf((String) key);
        	else
        		deliverId = (Integer) key;
            if (deliverId != null)
            {
        	    if (doc instanceof InOfficeDocument)
        	        ((InOfficeDocument) doc).setDelivery(InOfficeDocumentDelivery.find(deliverId));
        	    else if (doc instanceof OutOfficeDocument)
        		    ((OutOfficeDocument) doc).setDelivery(OutOfficeDocumentDelivery.find(deliverId));
            }
		
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		OfficeDocument doc = OfficeDocument.find(documentId);

		if (doc instanceof InOfficeDocument && ((InOfficeDocument)doc).getDelivery() != null )
			return new FieldValue(this, ((InOfficeDocument)doc).getDelivery().getId().toString(), ((InOfficeDocument)doc).getDelivery().getName());
		else if (doc instanceof OutOfficeDocument && ((OutOfficeDocument)doc).getDelivery() != null )
			return new FieldValue(this, ((OutOfficeDocument)doc).getDelivery().getId().toString(), ((OutOfficeDocument)doc).getDelivery().getName());
		else
			return new FieldValue(this, null, null);

	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value == null || (value.toString().equals("")))
			return null;
		if (value instanceof InOfficeDocumentDelivery)
			return ((InOfficeDocumentDelivery) value).getId();
		else if (value instanceof OutOfficeDocumentDelivery)
			return ((OutOfficeDocumentDelivery) value).getId();
		try
		{
			return Integer.parseInt(value.toString());
		}
		catch (NumberFormatException e)
		{
			throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci") + " " + value + " " + string("DoLiczbyCalkowitej"));
		}
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
	}
	
	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException
	{
		
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("", "--wybierz--");

		if (fieldsValues != null && fieldsValues.get(getCn()) != null)
			enumSelected.add(fieldsValues.get(getCn()).toString());
		else
			enumSelected.add("");

		if (type.equals("in"))
		{
			for (InOfficeDocumentDelivery delivery : InOfficeDocumentDelivery.list())
				enumValues.put(delivery.getId().toString(), delivery.getName());
		}
		else if (type.equals("out"))
		{
			for (OutOfficeDocumentDelivery delivery : OutOfficeDocumentDelivery.list())
				enumValues.put(delivery.getId().toString(), delivery.getName());
		}
		return new EnumValues(enumValues, enumSelected);
	}

	public final List<EnumItem>  getAllEnumItemsByAspect(String documentAspectCn,DocumentKind kind)
	{
		
		return this.enumItems;
	}

	
	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) 
	{
		String  valS = (String) values.get(this.getCn());
		Integer valI = valS != null && valS.length() > 0 ? Integer.parseInt(valS) : null;
		LoggerFactory.getLogger("tomekl").debug(" {} {} ",valS,valI);
		dockindQuery.officeDelivery(this.type, valI);		
	}
	
}
