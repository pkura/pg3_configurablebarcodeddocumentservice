package pl.compan.docusafe.core.dockinds.field;

import static pl.compan.docusafe.util.I18nUtils.*;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Pole typu Double, nie powinno by� u�ywane do zapisywania kwoty
 * od tego jest MoneyField
 *
 * klucz, obiekt - Double reprezentuj�ce warto��
 * 
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DoubleColumnField extends Field implements DoubleField{

    /**
     * Kontruktor DoubleColumnField
     *
     * @param id            id z xml'a, musi by� unikalne
     * @param cn            cn z xml'a, musi b� unikalne
     * @param column        nazwa kolumny, nie mo�e by� null (niestety)
     * @param names         nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param required      czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen  warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws pl.compan.docusafe.core.EdmException
     */
    public DoubleColumnField(String id, String cn, String column, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, column, names, Field.DOUBLE, null, null, required, requiredWhen, availableWhen);
    }

    @Override
    public Double simpleCoerce(Object value) throws FieldValueCoerceException {
        if (value == null){
            return null;
        }

        if (value instanceof Double){
            return (Double)value;
        }

        try {
            String s = value.toString().replace(",", ".");
            if (s.length() == 0) return null;
            return Double.parseDouble(s);
        } catch (NumberFormatException e) {
            throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci")+" "+value+" "+string("DoLiczbyRzeczywistej"));
        }
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        double d = rs.getDouble(getColumn());
        Double value = null;
        if (!rs.wasNull()){
            value = d;
        }
        return new FieldValue(this, value, value);
    }

    @Override
    public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
        if(value == null){
            ps.setObject(index, null);
        } else {
            ps.setDouble(index, simpleCoerce(value));
        }
    }

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
			throws EdmException {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY);
	}
}
