package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static pl.compan.docusafe.util.I18nUtils.string;

public class DocumentAutorField extends NonColumnField implements CustomPersistence, EnumNonColumnField, SearchPredicateProvider
{

	private static final Logger log = LoggerFactory.getLogger(DocumentAutorField.class);

	private List<EnumItem> enumItems = new ArrayList<EnumItem>();
	private boolean dsuserAsLtNFstNExtName = AvailabilityManager.isAvailable("dsuserFieldAsLastNameFirstNameExternalName");

	public DocumentAutorField(String id, String type, String cn, String logic, Map<String, String> names, boolean required, List<Condition> requiredWhen,
			List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, names, Field.DOCUMENT_AUTOR, null, null, required, requiredWhen, availableWhen);

		List<DSUser> users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
		for (DSUser user : users)
		{
			boolean aviable = !(user.isDeleted() /*|| user.isLoginDisabled()*/);
			Map<String, String> titles = new HashMap<String, String>();
			if (dsuserAsLtNFstNExtName)
				titles.put("pl", user.getLastnameFirstnameWithOptionalIdentifier());
			else
				titles.put("pl", user.asLastnameFirstname());
			if (user.getId() > (long) Integer.MAX_VALUE)
			{
				throw new RuntimeException("utrata dokładności - niemożliwe stworzenie pola");
			}
			EnumItem ei = new EnumItemImpl(user.getId().intValue(), user.getName(), titles, new String[1], false, getCn(), aviable);
			enumItems.add(ei);
		}


	}

	@Override
	public void persist(Document doc, Object key) throws EdmException
	{
		Long userId = null;
		if (key == null)
			return;
		if (key instanceof String)
			userId = Long.valueOf((String) key);
		else
			userId = (Long) key;

		if (userId != null)
			doc.setAuthor(DSUser.findById(userId).getName());
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId,false);

		if (doc instanceof OfficeDocument && doc.getAuthor() != null)
		{
			DSUser user = DSUser.findByUsername(doc.getAuthor());
			if (dsuserAsLtNFstNExtName)
			return new FieldValue(this, user.getId().toString(), user.getLastnameFirstnameWithOptionalIdentifier());
			else
			return new FieldValue(this, user.getId().toString(), user.asLastnameFirstname());
		} else
			return new FieldValue(this, null, null);

	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value == null || (value.toString().equals("")))
			return null;
		if (value instanceof Long)
			return ((DSUser) value).getId();
		else if (value instanceof String)
			try
			{
				return Long.parseLong(value.toString());
			} catch (NumberFormatException e)
			{
				throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci") + " " + value + " " + string("DoLiczbyCalkowitej"));
			}
		return value;
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
	}

	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException
	{
		List<DSUser> users = new ArrayList<DSUser>();
		List<String> enumSelected = new ArrayList<String>();
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		DSUser usel;
		if (fieldsValues.get(this.getCn()) != null)
		{
			Object o = fieldsValues.get(this.getCn());
			if (o instanceof String)
			{
				usel = DSUser.findById((Long.valueOf((String) fieldsValues.get(this.getCn()))));
				if(dsuserAsLtNFstNExtName)
				enumValues.put(usel.getId().toString(), usel.getLastnameFirstnameWithOptionalIdentifier());
				else 
				enumValues.put(usel.getId().toString(), usel.asLastnameFirstname());
				
				enumSelected.add(usel.getId().toString());
			} else if (o instanceof Long)
			{
				usel = DSUser.findById((Long) fieldsValues.get(this.getCn()));
				if(dsuserAsLtNFstNExtName)
					enumValues.put(usel.getId().toString(), usel.getLastnameFirstnameWithOptionalIdentifier());
					else 
					enumValues.put(usel.getId().toString(), usel.asLastnameFirstname());
				
				enumSelected.add(usel.getId().toString());
			} 
		} else
		{
			enumSelected.add(DSApi.context().getDSUser().getId().toString());
			if(dsuserAsLtNFstNExtName)
				enumValues.put(DSApi.context().getDSUser().getId().toString(), DSApi.context().getDSUser().getLastnameFirstnameWithOptionalIdentifier());
			else 
				enumValues.put(DSApi.context().getDSUser().getId().toString(), DSApi.context().getDSUser().asLastnameFirstname());
			
		}

		return new EnumValues(enumValues, enumSelected);
	}

	public final List<EnumItem> getAllEnumItemsByAspect(String documentAspectCn, DocumentKind kind)
	{

		return this.enumItems;
	}


	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues)
	{
		/*String  valS = (String) values.get(this.getCn());
		Integer valI = valS != null && valS.length() > 0 ? Integer.parseInt(valS) : null;
		LoggerFactory.getLogger("tomekl").debug(" {} {} ",valS,valI);
		dockindQuery.officeDocumentClerk(this.type, valI);	*/
	}

}
