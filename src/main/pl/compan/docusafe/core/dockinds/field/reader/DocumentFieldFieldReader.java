package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.DocumentField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class DocumentFieldFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		DocumentField documentField = new DocumentField(el.attributeValue("id"), el.attributeValue("cn"),  el.attributeValue("column") != null ? el.attributeValue("column") : "*none*", elType.attributeValue("logic"), names, required, requiredWhen, availableWhen, elType.attributeValue("tableName"), elType.attributeValue("resourceName"), DocumentKind.find(di.getDockindId()));
		documentField.readParameters(el, elType);
		return documentField;
	}

}
