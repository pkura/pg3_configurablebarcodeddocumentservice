package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.AttachmentValue;

public interface AttachmentField {
	public AttachmentValue getAttachmentForDwr(Long value) throws EdmException;
}
