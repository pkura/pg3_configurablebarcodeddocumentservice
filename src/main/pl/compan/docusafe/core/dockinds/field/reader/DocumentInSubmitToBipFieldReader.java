package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.DocumentInSubmitToBipField;
import pl.compan.docusafe.core.dockinds.field.DocumentOutZPODate;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class DocumentInSubmitToBipFieldReader implements FieldReader
{

	public Field read(DockindInfoBean di, Element el, Element elType,String refStyle, boolean required, List<Condition> availableWhen,List<Condition> requiredWhen, Map<String, String> names,boolean disabled) throws EdmException 
	{
		String cn = el.attributeValue("cn");
		String type = elType.attributeValue("type");
		return new DocumentInSubmitToBipField(el.attributeValue("id"), type, cn, names, required, requiredWhen, availableWhen);
	}

	
}
