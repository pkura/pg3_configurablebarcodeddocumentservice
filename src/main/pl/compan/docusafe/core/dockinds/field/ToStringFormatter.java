package pl.compan.docusafe.core.dockinds.field;

import java.io.IOException;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
class ToStringFormatter implements FieldFormatter<Object> {
    public void format(Object fieldValue, Appendable app) throws IOException {
        app.append(fieldValue + "");
    }
}
