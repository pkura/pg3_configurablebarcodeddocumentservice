package pl.compan.docusafe.core.dockinds.field;

import java.text.Collator;
import java.util.Comparator;
/* User: Administrator, Date: 2007-02-16 11:48:24 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public interface EnumItem
{


    public Integer getId();

    public String getCn();

    public String getTitle();

    public Object getTmp();

    public void setTmp(Object tmp);

    public String getRefValue();

    public Integer getCentrum();

    public boolean isForceRemark();

    public String getFieldCn();

    /**
     * Informacja czy dany wpis powinien by� mo�liwy do wybrania dla u�ytkownik�w.
     * @return true je�li dany rekord jest dost�pny, false w przeciwnym przypadku
     */
    public boolean isAvailable();

    /**
     * Zwraca warto�� argumentu zwi�zanego z warto�ci�.
     * @param position Pozycja argumentu liczona od 1.
     * @return Warto�� argumentu lub null.
     */
    public String getArg(int position);


    public static final Comparator<EnumItem> COMPARATOR_BY_CN = new EnumItemComparatorByCn();
    public static final Comparator<EnumItem> COMPARATOR_BY_TITLE = new EnumItemComparator();
    public static final Comparator<EnumItem> COMPARATOR_BY_ID = new EnumItemComparatorByID();

    public static class EnumItemComparator implements Comparator<EnumItem>
    {
        private static final Collator collator = Collator.getInstance();

        public int compare(EnumItem e1, EnumItem e2)
        {
            collator.setStrength(Collator.SECONDARY);
            int compare = collator.compare(e1.getTitle(), e2.getTitle());
            return compare == 0 ? e1.getId().compareTo(e2.getId()) : compare;
        }
    }

    public static class EnumItemCnComparator implements Comparator<EnumItem>
    {
        private static final Collator collator = Collator.getInstance();

        public int compare(EnumItem e1, EnumItem e2)
        {
            collator.setStrength(Collator.SECONDARY);
            int compare = collator.compare(e1.getCn(), e2.getCn());
            return compare == 0 ? e1.getId().compareTo(e2.getId()) : compare;
        }
    }

    public static class EnumItemComparatorByCn implements Comparator<EnumItem>
    {
        public int compare(EnumItem e1, EnumItem e2)
        {
            int compare = e1.getCn().compareTo(e2.getCn());
            return compare == 0 ? e1.getId().compareTo(e2.getId()) : compare;
        }
    }

    public static class EnumItemComparatorByID implements Comparator<EnumItem>
    {
        public int compare(EnumItem e1, EnumItem e2)
        {
            return e1.getId().compareTo(e2.getId());
        }
    }
}
