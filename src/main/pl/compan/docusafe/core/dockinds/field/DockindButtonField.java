package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.Field;

public class DockindButtonField extends NonColumnField
{
	public static final String EVERYWHERE = "everywhere";
	public static final String DOCUMENT_ARCHIVE = "documentArchive";
	public static final String EDIT_DOCKIND_DOCUMENT = "editDockindDocument";
	public static final String TASKLIST = "tasklist";

	private String onclick;
	private Map<String,String> actions;
	private Set<String> places;
	public DockindButtonField(String id, String cn,Map<String, String> names, String type,
			List<Condition> requiredWhen, List<Condition> availableWhen, String onclick,String places)throws EdmException
	{
		super(id, cn, names, DOCKIND_BUTTON, null, null, false,
				requiredWhen, availableWhen);
		this.onclick = onclick;
		if(places == null)
		{
			this.places = new HashSet<String>();
			this.places.add(DOCUMENT_ARCHIVE);
		}
		else
		{
			this.places = new HashSet<String>(Arrays.asList(places.split(";")));
		}
	}

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        return new FieldValue(this, null);
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        return value;
    }

    public void setOnclick(String onclick) {
		this.onclick = onclick;
	}
	public String getOnclick() {
		return onclick;
	}

	public void setActions(Map<String,String> actions) {
		this.actions = actions;
	}

	public Map<String,String> getActions() {
		return actions;
	}

    public boolean isButtonAvailable(String place){
		return (places == null ? false : places.contains(place));
	}

	@Override
	public Field getDwrField() throws EdmException {
		// TODO Auto-generated method stub
		return null;
	}
}
