package pl.compan.docusafe.core.dockinds.field;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class DocumentInStampDateField extends NonColumnField implements CustomPersistence {

	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);
	private static final Logger log = LoggerFactory.getLogger(DocumentInStampDateField.class);
	public DocumentInStampDateField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
		super(id, cn, names, Field.DOCUMENT_IN_STAMP_DATE, null, null, required, requiredWhen, availableWhen);
	}
	
	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId,false);
		if (doc instanceof OutOfficeDocument) 
			 throw new  EdmException("Data stempla pocztowego przewidzaina tylko dla dokumentów przychodzacych");
			else if (doc instanceof InOfficeDocument) 
			return new FieldValue(this, ((InOfficeDocument) doc).getStampDate());
		else return null;
		
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
		 if (value == null )
	            return null;
	        Date date;
	        if ((value instanceof String && String.valueOf(value).length() == 0)) return null;
	        if (!(value instanceof Date)) {
	            try {
	                date = DateUtils.parseJsDate(String.valueOf(value));
	                if (!DSApi.isLegalDbDate(date))
	                    throw new FieldValueCoerceException(sm.getString("PrzekazanaDataNieZnajdujeSie") + " " +
	                            sm.getString("WzakresieWymaganymPrzezBazeDanych") + ": " + DateUtils.formatJsDate(date));
	            } catch (FieldValueCoerceException e) {
	                throw e;
	            } catch (Exception e) {
	                //log.error(e.getMessage(), e);
	                throw new FieldValueCoerceException(sm.getString("NieMoznaSkonwertowacWartosci") + " '" + value + "' " + sm.getString("DoDaty"));
	            }
	        } else {
	            date = (Date) value;
	            if (!DSApi.isLegalDbDate(date))
	                throw new FieldValueCoerceException(sm.getString("PrzekazanaDataNieZnajdujeSie") + " " +
	                        sm.getString("WzakresieWymaganymPrzezBazeDanych") + ": " + DateUtils.formatJsDate(date));
	        }
	        return date;
	}

	public void persist(Document doc, Object key) throws EdmException {
		Date date = null; 
		
		 if (doc instanceof InOfficeDocument) 
		{
			if (key instanceof String)
				date = DateUtils.parseJsDate((String)key);
			else 
				date = (Date)key;
			((InOfficeDocument) doc).setStampDate(date);
		}
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
		return  new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.DATE);
	}
	/*public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) 
	{
		Date  valS = (Date) values.get(this.getCn());
		if (valS!=null){
		log.debug(" {} {} ",valS);
		dockindQuery.inDocumentStampDate(valS);
		}
		
	}*/
}
