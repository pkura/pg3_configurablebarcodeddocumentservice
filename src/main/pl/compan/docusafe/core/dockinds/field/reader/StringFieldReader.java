package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;
import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class StringFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
        String column = el.attributeValue("column");

        Integer length = null;
        if (elType.attribute("length") != null)
        {
            length = new Integer(elType.attributeValue("length"));
        }
        Integer size = null;
        if (elType.attribute("size") != null)
        {
            size = new Integer(elType.attributeValue("size"));
        }
        String validatorRegExp = null;
        if (elType.attribute("reg-exp") != null)
        {
            validatorRegExp = elType.attributeValue("reg-exp");
        }

        if(column == null){

            NonColumnField field = new StringNonColumnField(el.attributeValue("id"), el.attributeValue("cn"), names, size, length, required, requiredWhen, availableWhen, validatorRegExp);

            boolean reset = Boolean.parseBoolean(elType.attributeValue("reset"));
            field.setResettable(reset);

            return field;

        } else {

		StringColumnField field = new StringColumnField(el.attributeValue("id"), el.attributeValue("cn"), el.attributeValue("column"), names, size, length, required, requiredWhen, availableWhen, validatorRegExp);
		return field;
        }

}
}
