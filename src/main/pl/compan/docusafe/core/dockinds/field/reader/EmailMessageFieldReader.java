package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.DocumentField;
import pl.compan.docusafe.core.dockinds.field.DocumentParamsField;
import pl.compan.docusafe.core.dockinds.field.EmailMessageField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class EmailMessageFieldReader implements FieldReader {
	
	
	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		return new EmailMessageField((el.attributeValue("id")), el.attributeValue("cn"), names, elType, elType.attributeValue("content-type"), required, requiredWhen, availableWhen);
	}
	
}