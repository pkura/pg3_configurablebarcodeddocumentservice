package pl.compan.docusafe.core.dockinds.field;


public class EditField
{
	private Field field;
	private Object value;
	private Boolean canEdit;
	
	public void setField(Field field) {
		this.field = field;
	}
	public Field getField() {
		return field;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public Object getValue() {
		return value;
	}
	public Boolean getCanEdit() {
		return canEdit;
	}
	public void setCanEdit(Boolean canEdit) {
		this.canEdit = canEdit;
	}
}
