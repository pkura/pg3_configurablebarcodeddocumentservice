package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;

import java.util.Collection;
import java.util.List;

/**
 * Interfejs p�l z warto�ciami wyliczeniowymi, w przysz�o�ci poni�sze metody zostan� wywalone z klasy Field
 * (w kt�rej i tak nie zosta�y zaimplementowane poprawnie)
 *
 * Na razie interfejs b�dzie g��wnie s�u�y� do oznaczania p�l, kt�re implementuj� poni�sze metody
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface EnumerationField {
    List<EnumItem> getEnumItemsByAspect(String aspectCn, DocumentKind documentKind) throws EdmException;

    List<EnumItem> getEnumItems();

    Collection<EnumItem> getSortedEnumItems();

    List<EnumItem> getEnumItems(Object discriminator);

    EnumItem getEnumItem(Integer id) throws EntityNotFoundException;

    EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException;

    EnumItem getEnumItemByTitle(String cn) throws EntityNotFoundException;
}
