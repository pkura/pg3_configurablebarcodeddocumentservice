package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.StringManager;

public class DocumentInOriginalField extends NonColumnField implements CustomPersistence, SearchPredicateProvider
{
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(DocumentInOriginalField.class.getPackage().getName(), null);
	
	public DocumentInOriginalField(String id, String type, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, names, pl.compan.docusafe.core.dockinds.field.Field.DOCUMENT_IN_ORIGINAL, null, null, required, requiredWhen, availableWhen);
	}
	
	public void persist(Document doc, Object key) throws EdmException 
	{
		System.out.println("KEY "+ key);
		if (key == null)
		{
			((InOfficeDocument) doc).setOriginal(true);
		}
		else if( key instanceof Boolean)
		{
			((InOfficeDocument) doc).setOriginal((Boolean) key);
		}
		else 
		{
			throw new EdmException("DocumentInOriginalField persist key class: "+key.getClass());
		}
	}

	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException 
	{
		OfficeDocument doc = OfficeDocument.find(documentId);
		
		if (doc instanceof InOfficeDocument)
			return new FieldValue(this, ((InOfficeDocument)doc).getOriginal() , ((InOfficeDocument)doc).getOriginal());
		else
			throw new EdmException("DocumentInOriginalField provideFieldValue doc class: "+doc.getClass());
	}

	public Object simpleCoerce(Object value) throws FieldValueCoerceException 
	{
		if (value == null )
            return null;
        Boolean original;
        if ((value instanceof String && String.valueOf(value).length() == 0)) return null;
        if (!(value instanceof Boolean)) {
            try {
            	if("on".equalsIgnoreCase((String) value))
            		original = true;
            	else
            		original = Boolean.parseBoolean(String.valueOf(value));
            } catch (Exception e) {
                throw new FieldValueCoerceException(sm.getString("NieMoznaSkonwertowacWartosci") + " '" + value + "' " + sm.getString("DoBoleana"));
            }
        } else {
        	original = (Boolean) value;
            
        }
        return original;
	}

	public Field getDwrField() throws EdmException 
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
	}

	
	public void addSearchPredicate(DockindQuery dockindQuery,Map<String, Object> values, Map<String, Object> fieldValues) 
	{		
		String bipString = (String) values.get(this.getCn());
		
		Boolean original = null;
		if(StringUtils.isNotBlank(bipString))
			original = Boolean.parseBoolean(bipString);
		dockindQuery.isOriginal(original);
	}
}
