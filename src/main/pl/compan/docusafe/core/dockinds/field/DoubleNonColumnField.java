package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.Field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static pl.compan.docusafe.util.I18nUtils.string;

/**
 *
 * Date: 11.07.13
 * Time: 09:34
 * Gosia
 */
public class DoubleNonColumnField extends NonColumnField implements DoubleField {

    public DoubleNonColumnField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, names, pl.compan.docusafe.core.dockinds.field.Field.DOUBLE, null, null, required, requiredWhen, availableWhen);
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        Double value = null;
        return new FieldValue(this, value, value);
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        if (value == null){
            return null;
        }

        if (value instanceof Double){
            return (Double)value;
        }

        try {
            String s = value.toString().replace(",", ".");
            if (s.length() == 0) return null;
            return Double.parseDouble(s);
        } catch (NumberFormatException e) {
            throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci")+" "+value+" "+string("DoLiczbyRzeczywistej"));
        }    }

    @Override
    public Field getDwrField() throws EdmException {
            return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY);
        }
}

