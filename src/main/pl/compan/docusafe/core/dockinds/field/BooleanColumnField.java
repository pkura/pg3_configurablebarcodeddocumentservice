package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Pole tak/nie wy�wietlane w formie checkboxa
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class BooleanColumnField extends Field {
	private CommonBooleanLogic item;

    /**
     * Konstruktor BooleanField, wymaga podania podstawowych danych dla ka�dego Fielda
     *
     * @param id            id z xml'a, musi by� unikalne
     * @param cn            cn z xml'a, musi b� unikalne
     * @param column        nazwa kolumny, nie mo�e by� null (niestety)
     * @param names         nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param required      czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen  warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws pl.compan.docusafe.core.EdmException
     */
    public BooleanColumnField(String id, String cn, String column, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
    	super(id, cn, column, names, Field.BOOL, null, null, required, requiredWhen, availableWhen);
        item = new CommonBooleanLogic(this);
    }

	public Boolean simpleCoerce(Object value) throws FieldValueCoerceException {
		return item.simpleCoerce(value);
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
		return item.getDwrField();
	}

	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		return item.provideColumnFieldValue(documentId, rs);
	}

	public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
		item.set(ps, index, value);
	}
}
