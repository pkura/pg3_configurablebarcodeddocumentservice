package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.UserDivisionDictionary;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class DocumentUserDivisionSecondField extends ManualSortedNonColumnField implements CustomPersistence
{

	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);
	private static final Logger LOG = LoggerFactory.getLogger(DocumentUserDivisionSecondField.class);

	private final String logic;
	private List<Field> fields;
	private List<String> popUpFields;
	private List<String> promptFields;
	private List<String> searchFields;
	private List<String> visibleFields;
	private List<String> dicButtons;
	private List<String> popUpButtons;
	
	public enum PersonType
	{
		SENDER, RECIPIENT
	}

	private PersonType type = PersonType.RECIPIENT;

	public DocumentUserDivisionSecondField(String id, String cn, String logic, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String person) throws EdmException
	{
		super(id, cn, names, Field.DOCUMENT_USER_DIVISION_SECOND, null, null, required, requiredWhen, availableWhen);
		this.logic = logic != null ? logic : UserDivisionDictionary.class.getName();
		this.type = PersonType.valueOf(person.toUpperCase());
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		OfficeDocument doc = OfficeDocument.find(documentId) ;
		switch (type)
		{
			case SENDER:
				if (doc.getSender() == null)
				{
					return new FieldValue(this, "", "");
				}
				else
				{
					return new FieldValue(this, doc.getSender().getId(), doc.getSender().getShortSummary());
				}
			case RECIPIENT:
				if (!doc.getRecipients().isEmpty())
				{
                    if (isMultiple())
                    {
                        List<String> ids = new LinkedList<String>();

                        // zakaldam, ze na dokumencie znajduje sie dodatkowy slownik z odbiorca wiod�cym, dlatego nie dodoajmy go do slownika multiple
                        //tnowak zwroci w tym momencie ostatnio dodanego odbiorce jako pierwsza wartosc
                        if(AvailabilityManager.isAvailable("dokument.Formularz.zwrocOstanioDodanegoOdbiorce")){
                        	for (int i = doc.getRecipients().size()-1; i>0; i--)
                        	{
                        		ids.add(doc.getRecipients().get(i).getId().toString());
                        	}
                        }
                        else{
                        	for (int i = 0; i < doc.getRecipients().size(); i++)
                        	{
                        		ids.add(doc.getRecipients().get(i).getId().toString());
                        	}
                        }
                        return new FieldValue(this, ids, null);
                    }
                    else{
                    	if(AvailabilityManager.isAvailable("dokument.Formularz.zwrocOstanioDodanegoOdbiorce")){
                    		return new FieldValue(this, doc.getRecipients().get(doc.getRecipients().size()-1).getId(), doc.getRecipients().get(doc.getRecipients().size()-1).getShortSummary());
                    	}
                    	return new FieldValue(this, doc.getRecipients().get(0).getId(), doc.getRecipients().get(0).getShortSummary());
                    }
				}
		}
		return null;
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value == null)
			return null;
		try
		{
			if (value instanceof CharSequence)
			{
				return (String) value;
			}
			else if (value instanceof Long)
				return ((Long) value).toString();
			else if (value instanceof ArrayList && ((ArrayList)value).size()>0)
			{
		     	if (((ArrayList)value).get(0) instanceof CharSequence)
		     		return (String)((ArrayList)value).get(0);
		     	else 
		     		return ((Long)((ArrayList)value).get(0)).toString();
			}
			else
				throw new FieldValueCoerceException(this.getName() + " : cannot coerce " + value);
		}
		catch (EdmException e)
		{
			LOG.error(e.getMessage(), e);
			throw new FieldValueCoerceException(e.getMessage());
		}
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getFieldForDwr(Map<String, SchemeField> fieldsScheme) throws EdmException
	{
		pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + getCn(), getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY);
		
		SchemeField scheme = null;
		if (fieldsScheme != null)
			scheme = fieldsScheme.get(this.getCn());
		
		new DwrDictionaryBase(this, null);
		DwrDictionaryBase dict = DwrDictionaryFacade.getDwrDictionary(this.getDockindCn(), this.getCn());
		field.setDictionary(dict);
		field.setHidden(isHidden());
		field.setDisabled(isDisabled());
		field.setRequired(isRequired());
		field.setReadonly(isReadOnly());
		field.setColumn(getDwrColumn());
		field.setFieldset(getDwrFieldset());
		field.setSubmit(isSubmit());
		field.setCssClass(getCssClass());
		field.setNewItemMessage(getNewItemMessage());
		field.setCoords(getCoords());
        field.setPopups(getPopups());
		if (scheme != null)
		{
			field.setDisabled(scheme.getDisabled());

			if (scheme.isReadonly() == null && scheme.isRequired() == null && scheme.isHidden() == null && !scheme.getDisabled())
				field.setHidden(scheme.isVisible());

			if (scheme.isRequired() != null)
				field.setRequired(scheme.isRequired());

			if (scheme.isReadonly() != null)
				field.setReadonly(scheme.isReadonly());

			if (scheme.isHidden() != null)
				field.setHidden(scheme.isHidden());
		}
		if (field.getDisabled() || field.getReadonly())
		{
			for (pl.compan.docusafe.core.dockinds.dwr.Field f:  field.getDictionary().getFields())
			{
				if ((field.getDictionary().isMultiple() && !f.getCn().toUpperCase().contains("_ID_")) || (!f.getCn().toUpperCase().equals("ID"))) {
                    if(field.getDisabled())
                        f.setDisabled(field.getDisabled());
                    if(field.getReadonly())
                        f.setReadonly(field.getReadonly());
                }
			}
		}
		if (!field.getDictionary().isMultiple() && !field.getDisabled()) {
			for (pl.compan.docusafe.core.dockinds.dwr.Field f : field.getDictionary().getFields()) {
				if (f.getCn().toUpperCase(Locale.ENGLISH).equals("ID")) {
					f.setRequired(true);
					break;
				}
			}
		}
		
		return field;
	}

	public void persist(Document dc, Object key) throws EdmException
	{
		LOG.info("DocumentUserDivision.persist: cn - {}, type - {}, dc - {}, key - {}", this.getCn(), type, dc, key);

		OfficeDocument doc = ((OfficeDocument) dc);

        if (isMultiple())
        {
            if (key instanceof ArrayList)
            {
                for (String id : (List<String>)key)
                {
                    persistUserDivision(id, doc, false);
                }
            }
        }
        else
        {
            persistUserDivision(key, doc, true);
        }
	}

    private void persistUserDivision(Object key, OfficeDocument doc, boolean addToDocument) throws EdmException
    {
        String selectedUserName = null, selectedDivisionGUID = null;
		boolean jestJuzNaLiscie = false;
        if (((String) key).contains("u:") || ((String) key).contains("d:"))
        {
            String[] ids = ((String) key).split(";");
            if (ids[0].contains("u:"))
                selectedUserName = ids[0].split(":")[1];
            if (ids[1].contains("d:"))
                selectedDivisionGUID = ids[1].split(":")[1];
            switch (type)
            {
                case SENDER:
                {
                    if (key == null)
                    {
                        doc.setSenderId(null);
                    }
                    else
                    {
                    	// przy edycji dokumentu usuwa starego nadawce z bazy
    					if (AvailabilityManager
    							.isAvailable("dokument.EdycjaZapisanieZmianNadawcaOdbiorca")) {
    						String sql = "delete from dso_person where discriminator ='sender' and document_id="
    								+ doc.getId().toString();
    						PreparedStatement ps = null;
    						try {
    							ps = DSApi.context().prepareStatement(sql);
    							ps.executeUpdate();
    						} catch (SQLException e) {
    							// TODO Auto-generated catch block
    							DSApi.context().rollback();
    							e.printStackTrace();
    						}
    					}
                        if (selectedUserName != null)
                        {
                            String personFirstName = DSUser.findByUsername(selectedUserName).getFirstname();
                            String personLastName = DSUser.findByUsername(selectedUserName).getLastname();
                            Sender sender = new Sender();
                            sender.setFirstname(personFirstName);
                            sender.setLastname(personLastName);
                            sender.setEmail(DSUser.findByUsername(selectedUserName).getEmail()!=null ? DSUser.findByUsername(selectedUserName).getEmail() : "" );
                            sender.setOrganization(selectedDivisionGUID != null ? DSDivision.find(selectedDivisionGUID).getName() : DSDivision.find(DSDivision.ROOT_GUID).getName());
                            sender.setOrganizationDivision(selectedDivisionGUID != null ? DSDivision.find(selectedDivisionGUID).getName() : DSDivision.find(DSDivision.ROOT_GUID).getName());
                            sender.setDictionaryType(Person.DICTIONARY_SENDER);
                            sender.setLparam(key.toString());
                            sender.create();
                            doc.setSender(sender);
                        }
                        else if (selectedDivisionGUID != null)
                        {
                            Map address = GlobalPreferences.getAddress();
                            Sender sender = new Sender();
                            sender.setOrganizationDivision(DSDivision.find(selectedDivisionGUID).getName());
                            sender.setOrganization(DSDivision.find(selectedDivisionGUID).getName());
                            sender.setStreet((String) address.get(GlobalPreferences.STREET));
                            sender.setLocation((String) address.get(GlobalPreferences.LOCATION));
                            sender.setZip((String) address.get(GlobalPreferences.ZIP));
                            sender.setDictionaryType(Person.DICTIONARY_SENDER);
                            sender.setLparam(key.toString());
                            sender.create();
                            doc.setSender(sender);
                        }
                        else
                        {
                            Map address = GlobalPreferences.getAddress();
                            Sender sender = new Sender();
                            sender.setOrganizationDivision((String) address.get(GlobalPreferences.ORGANIZATION));
                            sender.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
                            sender.setStreet((String) address.get(GlobalPreferences.STREET));
                            sender.setLocation((String) address.get(GlobalPreferences.LOCATION));
                            sender.setZip((String) address.get(GlobalPreferences.ZIP));
                            sender.setDictionaryType(Person.DICTIONARY_SENDER);
                            sender.setLparam(key.toString());
                            sender.create();
                            doc.setSender(sender);
                        }
                    }
                    return;
                }
                case RECIPIENT:
                {
                    if (key == null)
                    {
                        doc.setRecipients();
                    }
                    else
                    {
                        if (selectedUserName != null)
                        {	
                            String personFirstName = DSUser.findByUsername(selectedUserName).getFirstname();
                            String personLastName = DSUser.findByUsername(selectedUserName).getLastname();
                        	jestJuzNaLiscie = false;
                        	if (AvailabilityManager.isAvailable("documentUserDivisionSecond.RemoveOldRecipients")) {
                        		removeOldRecipients(doc);
                        	}
    						if (AvailabilityManager
    								.isAvailable("dokument.EdycjaZapisanieZmianNadawcaOdbiorca")) {
    							// przy edycji dodaje nowego recipienta, choc juz
    							// jest
    							// na liscie
    							// sprawdzenie czy ju� ma takiego persona
    							// recipienta
    							List<Recipient> lista = doc.getRecipients();
    							Iterator it = lista.iterator();
    							while (it.hasNext()) {
    								Recipient r = (Recipient) it.next();
    								if (selectedUserName != null) {
    									if (personFirstName
    											.equals(r.getFirstname())
    											&& personLastName.equals(r
    													.getLastname()))
    										jestJuzNaLiscie = true;
    								}
    							}
    						}

    						if (!jestJuzNaLiscie) {
                            Recipient recipient = new Recipient();
                            recipient.setFirstname(personFirstName);
                            recipient.setLastname(personLastName);
                            recipient.setEmail(DSUser.findByUsername(selectedUserName).getEmail()!=null ? DSUser.findByUsername(selectedUserName).getEmail() : "" );
                            recipient.setOrganization(selectedDivisionGUID != null ? DSDivision.find(selectedDivisionGUID).getName() : DSDivision.find(DSDivision.ROOT_GUID).getName());
                            recipient.setOrganizationDivision(selectedDivisionGUID != null ? DSDivision.find(selectedDivisionGUID).getName() : DSDivision.find(DSDivision.ROOT_GUID).getName());
                            recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
                            recipient.setLparam(key.toString());
                            recipient.create();
                            if (!this.isMultiple())
                                doc.getRecipients().clear();
                            doc.addRecipient(recipient);
    						}
                        }
                        else if (selectedDivisionGUID != null)
                        {	
                        	if (AvailabilityManager.isAvailable("documentUserDivisionSecond.RemoveOldRecipients")) {
                        		removeOldRecipients(doc);
                        	}
                        	if (AvailabilityManager
    								.isAvailable("dokument.EdycjaZapisanieZmianNadawcaOdbiorca")) {
    							// przy edycji dodaje nowego recipienta, choc juz
    							// jest
    							// na liscie
    							// sprawdzenie czy ju� ma takiej organizacji
    							// recipienta
    							List<Recipient> lista = doc.getRecipients();
    							Iterator it = lista.iterator();
    							String tmp = DSDivision.find(selectedDivisionGUID)
    									.getName();
    							while (it.hasNext()) {
    								Recipient r = (Recipient) it.next();
    								if (r.getFirstname() != null
    										&& r.getLastname() != null) {
    								} else {
    									if (tmp.equals(r.getOrganizationDivision()))
    										jestJuzNaLiscie = true;
    								}
    							}
    						}

    						if (!jestJuzNaLiscie) {
                            Map address = GlobalPreferences.getAddress();
                            Recipient recipient = new Recipient();
                            recipient.setOrganization(DSDivision.find(selectedDivisionGUID).getName());
                            recipient.setOrganizationDivision(DSDivision.find(selectedDivisionGUID).getName());
//                            recipient.setEmail(DSUser.findByUsername(selectedUserName).getEmail()!=null ?DSUser.findByUsername(selectedUserName).getEmail() : "" );
                            recipient.setStreet((String) address.get(GlobalPreferences.STREET));
                            recipient.setLocation((String) address.get(GlobalPreferences.LOCATION));
                            recipient.setZip((String) address.get(GlobalPreferences.ZIP));
                            recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
                            recipient.setLparam(key.toString());
                            recipient.create();
                            if (!this.isMultiple())
                                doc.getRecipients().clear();
                            doc.addRecipient(recipient);
    						}
                        }
                        else
                        {
                            Map address = GlobalPreferences.getAddress();
                            Recipient recipient = new Recipient();
                            recipient.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
                            recipient.setOrganizationDivision((String) address.get(GlobalPreferences.ORGANIZATION));
                            recipient.setStreet((String) address.get(GlobalPreferences.STREET));
                            recipient.setLocation((String) address.get(GlobalPreferences.LOCATION));
                            recipient.setZip((String) address.get(GlobalPreferences.ZIP));
                            recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
                            recipient.setLparam(key.toString());
                            recipient.create();
                            if (!this.isMultiple())
                                doc.getRecipients().clear();
                            doc.addRecipient(recipient);
                        }
                        return;
                    }
                }
            }
        }
    }
    
    private void removeOldRecipients(OfficeDocument document) throws EdmException {
		String sql = "delete from dso_person where discriminator ='recipient' and document_id="
				+ document.getId().toString();
		PreparedStatement ps = null;
		try {
			ps = DSApi.context().prepareStatement(sql);
			ps.executeUpdate();
		} catch (SQLException e) {
			DSApi.context().rollback();
			e.printStackTrace();
		}
    }

    public List<Field> getFields()
	{
		return fields;
	}

	public void setFields(List<Field> fields)
	{
		this.fields = fields;
	}

	public List<String> getPopUpFields()
	{
		return popUpFields;
	}

	public void setPopUpFields(List<String> popUpFields)
	{
		this.popUpFields = popUpFields;
	}

	public List<String> getPromptFields()
	{
		return promptFields;
	}

	public void setPromptFields(List<String> promptFields)
	{
		this.promptFields = promptFields;
	}

	public List<String> getSearchFields()
	{
		return searchFields;
	}

	public void setSearchFields(List<String> searchFields)
	{
		this.searchFields = searchFields;
	}

	public List<String> getVisibleFields()
	{
		return visibleFields;
	}

	public void setVisibleFields(List<String> visibleFields)
	{
		this.visibleFields = visibleFields;
	}

	public boolean isSenderField()
	{
		return type == PersonType.SENDER;
	}

	public boolean isRecipientField()
	{
		return type == PersonType.RECIPIENT;
	}

	public String getLogic()
	{
		return logic;
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
	{
		return null;
	}

	public List<String> getDicButtons()
	{
		return dicButtons;
	}

	public void setDicButtons(List<String> dicButtons)
	{
		this.dicButtons = dicButtons;
	}

	public List<String> getPopUpButtons() {
		return popUpButtons;
	}

	public void setPopUpButtons(List<String> popUpButtons) {
		this.popUpButtons = popUpButtons;
	}

}
