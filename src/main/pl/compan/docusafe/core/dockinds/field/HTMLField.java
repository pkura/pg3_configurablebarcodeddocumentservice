package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.HtmlUtils;

public class HTMLField extends NonColumnField implements HTMLFieldLogic
{
	private final static Logger log = LoggerFactory.getLogger(HTMLField.class);
	private HTMLFieldLogic logic;
	/** wartosc pola */
	private String value;
	
	private String tagFieldValue;
	
	/** 
	 * Tag jaki zostanie stworzony
	 */
	private String tag;
	
	public HTMLField(String id, String logic, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String value) throws EdmException {
		super(id, cn, names, Field.HTML, null, null, required, requiredWhen, availableWhen);
		this.value = value;
		if (logic != null)
        {
            try
            {
                Class c = Class.forName(logic);
                this.logic = (HTMLFieldLogic) c.getConstructor().newInstance();
                
            }
            catch (Exception ex)
            {
                log.error(ex.getMessage(), ex);
            }
        }
	}

	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {

		FieldValue returnFieldValue = new FieldValue(this, getValue(), getValue());
		
		if(tagFieldValue != null && tag != null)
		{
				if(tag.equals("a"))
				{
					try
					{
						String href = rs.getString(tagFieldValue);
						
						if(href == null)
							return returnFieldValue;
						
						if(!href.trim().startsWith("http://"))
							href = "http://" + href.trim();
						
						String url = HtmlUtils.toHtmlAnchor(href, value);
						
						returnFieldValue = new  FieldValue(this, url, value);
					}catch (Exception e) {
						log.error("blad podczas tworzenia url'a", e);
					}
				}
		}

		return returnFieldValue;
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
		
		Object returnValue = getValue();
		
		if(tagFieldValue != null && tag != null && !getValue().equals(value))
		{
			if(!value.toString().startsWith("http://"))
				returnValue=value.toString();
			else
				returnValue = HtmlUtils.toHtmlAnchor(value.toString(), getValue().toString());
		}
		
		return returnValue;
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field  getDwrField() throws EdmException {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.HTML);
	}

	public String getFieldValue()
	{
		return tagFieldValue;
	}

	public void setFieldValue(String fieldLabel)
	{
		this.tagFieldValue = fieldLabel;
	}

	public String getTag()
	{
		return tag;
	}

	public void setTag(String tag)
	{
		this.tag = tag;
	}

    @Override
    public String getValue() {
        if (logic != null) {
            return logic.getValue();
        } else if (value == null && getDefaultValue() != null) {
            return getDefaultValue();
        } else {
            return value;
        }
    }

	@Override
	public String getValue(String docId) throws EdmException {
		return logic != null ? logic.getValue(docId) : value;
	}

	@Override
	public String reset() {
		this.value = "";
		return this.value;
	}
	
	
}
