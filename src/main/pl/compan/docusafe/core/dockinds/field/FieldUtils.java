package pl.compan.docusafe.core.dockinds.field;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * Klasa narz�dziowa dla Field�w, podobne metody, kt�re u�ywane s�
 * w implementacjach klasy Field najlepiej umieszcza� tutaj
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
class FieldUtils {
    private final static Logger LOG = LoggerFactory.getLogger(FieldUtils.class);
    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);

    private FieldUtils(){}

    /**
     * Implementacja simpleCoerce dla klucza b�d�cego Integerem
     * @param field
     * @param value
     * @return
     * @throws FieldValueCoerceException
     */
    public static Integer intCoerce(Field field, Object value) throws FieldValueCoerceException{
        if (value == null){
            return null;
        }

        if (value instanceof String && StringUtils.isBlank(value.toString())){
            return null;
        }

        if (value instanceof Integer){
            return (Integer) value;
        }

        try {
            return Integer.parseInt(value.toString());
        } catch (NumberFormatException e) {
            throw new FieldValueCoerceException(field.getName()
                    + ":" + sm.getString("NieMoznaSkonwertowacWartosci")
                    + " " + value + " " + sm.getString("DoLiczbyCalkowitej"));
        }
    }

     public static Long longCoerce(Field field, Object value) throws FieldValueCoerceException{
        if (value == null){
            return null;
        }

        if (value instanceof String &&
                StringUtils.isBlank(value.toString())){
            return null;
        }

        if (value instanceof Long){
            return (Long) value;
        }

        try {
            return Long.parseLong(value.toString());
        } catch (NumberFormatException e) {
            throw new FieldValueCoerceException(field.getName() + ", " + field.getCn()
                    + ":" + sm.getString("NieMoznaSkonwertowacWartosci")
                    + " " + value.toString() + " " + sm.getString("DoLiczbyCalkowitej"));
        }
    }

}