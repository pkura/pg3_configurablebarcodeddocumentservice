package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.DockindButtonField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class DockindButtonFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		 Field field;
	        String onclick = el.attributeValue("onclick");
	        DockindButtonField dockindButtonField
	            = new DockindButtonField((el.attributeValue("id")),
	            el.attributeValue("cn"),
	            names,
	            Field.DOCKIND_BUTTON,
	            requiredWhen,
	            availableWhen,
	            onclick,
	            el.attributeValue("place"));
	        dockindButtonField.setOnclick(onclick);

	        Map<String,String> actions = new LinkedHashMap<String, String>();
	        Element ectionsEL = el.element("actions");
	        if(ectionsEL != null)
	        {
	            List<Element> actionEls = ectionsEL.elements("action");
	            for (Element element : actionEls)
	            {
	                actions.put(element.attributeValue("cn"),element.attributeValue("name"));
	            }
	        }
	        dockindButtonField.setActions(actions);
	        field = dockindButtonField;
	        return field;
	}

}
