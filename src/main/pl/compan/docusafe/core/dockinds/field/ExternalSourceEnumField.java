package pl.compan.docusafe.core.dockinds.field;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.enums.EnumDataSourceLogic;
import pl.compan.docusafe.core.dockinds.field.enums.EnumSourceException;
import pl.compan.docusafe.core.dockinds.field.enums.ExternalSourceEnumItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by kk on 18.02.14.
 */
public class ExternalSourceEnumField extends Field implements EnumerationField, EnumDataSourceLogic {

    private StringManager sm = GlobalPreferences.loadPropertiesFile(ExternalSourceEnumField.class.getPackage().getName(), null);
    private static final Logger log = LoggerFactory.getLogger(ExternalSourceEnumField.class);

    private final EnumDataSourceLogic sourceLogic;
    private String refField;

    public ExternalSourceEnumField(String id, String cn, String column, Map<String, String> names, String type, boolean auto, String logic, String refField, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, column, names, type, refStyle, classname, required, requiredWhen, availableWhen);
        this.logic = logic;
        this.refField = refField;
        this.setAuto(auto);

        Preconditions.checkNotNull(logic, "Nie zdefiniowano logiki �rod�a pola");

        try {
            sourceLogic = (EnumDataSourceLogic) Class.forName(logic).newInstance();
        } catch (Exception e) {
            throw new EdmException(e.getMessage(), e);
        }
    }

    public EnumDataSourceLogic getEnumSourceLogic() {
        return sourceLogic;
    }

    @Override
    public List<EnumItem> getEnumItems() {
        try {
            return sourceLogic.getEnumItemsFromSource();
        } catch (EnumSourceException e) {
            log.error(e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    @Override
    public List<EnumItem> getEnumItemsFromSource(String refFieldId) throws EnumSourceException {
        return sourceLogic.getEnumItemsFromSource(refFieldId);
    }

    @Override
    public List<EnumItem> getEnumItemsFromSource() throws EnumSourceException {
        return sourceLogic.getEnumItemsFromSource();
    }

    @Override
    public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
        if (value == null) {
            ps.setObject(index, null);
        } else {
            ps.setString(index, ExternalSourceEnumItem.checkValidate(value));
        }
    }

    @Override
    public EnumItem getEnumItemByCn(String key) throws EntityNotFoundException {
        if (Strings.isNullOrEmpty(key)) {
            throw new EntityNotFoundException(getDockindCn() + ":" + getCn());
        }
        return ExternalSourceEnumItem.makeEnumItem(key);
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        String rawData = rs.getString(getColumn());
        return new FieldValue(this, rawData);
    }

    @Override
    public Object asObject(Object key) throws IllegalArgumentException, EdmException {
        throw new UnsupportedOperationException();
    }

    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EntityNotFoundException {
        Map<String, String> enumValues = new LinkedHashMap<String, String>();
        List<String> enumSelected = new ArrayList<String>();

        String selectedId = getSelectedEnumId(fieldsValues, getCn());

        boolean hasSelected;
        EnumItem selectedEnum;
        if (StringUtils.isBlank(selectedId)) {
            hasSelected = false;
            selectedEnum = ExternalSourceEnumItem.makeEnumItem("");
        } else {
            hasSelected = true;
            selectedEnum = ExternalSourceEnumItem.makeEnumItem(selectedId);
        }

        boolean wasAvailable = false;
        try {
            List<EnumItem> enumItems;
            if (getRefField() == null) {
                enumItems = getEnumItemsFromSource();
            } else {
                String refFieldId = getSelectedEnumId(fieldsValues, getRefField());
                enumItems = getEnumItemsFromSource(refFieldId);
            }

            Preconditions.checkNotNull(enumItems);
            for (EnumItem item : enumItems) {
                Preconditions.checkNotNull(item);

                if (hasSelected && checkIfEquals(selectedEnum, item)) {
                    selectedEnum = item;
                    wasAvailable = true;
                    enumSelected.add(ExternalSourceEnumItem.makeRawData(selectedEnum));
                } else {
                    if (!item.isAvailable()) {
                        continue;
                    }
                }

                enumValues.put(ExternalSourceEnumItem.makeRawData(item), makeEnumLabel(item));
            }

            enumValues.put("", sm.getString("select.wybierz"));

        } catch (EnumSourceException e) {
            log.error(e.getMessage(), e);

            enumValues.put("", sm.getString("select.wybierz"));
            enumValues.put(" ", "B��d w pobieraniu pozycji z zewn�trznego �r�d�a");
        } finally {
            if (hasSelected && !wasAvailable) {
                enumSelected.add(ExternalSourceEnumItem.makeRawData(selectedEnum));
                enumValues.put(ExternalSourceEnumItem.makeRawData(selectedEnum), makeEnumLabel(selectedEnum));
            } else {
                enumSelected.add("");
            }
        }


        if (isSort()) {
            Map<String, String> sortedEnumValues = Maps.newTreeMap(newComparator(enumValues));
            sortedEnumValues.putAll(enumValues);
            enumValues = sortedEnumValues;
        }

        if (enumSelected.isEmpty()) {
            enumSelected.add("");
        }

        return new EnumValues(enumValues, enumSelected);
    }

    private String getSelectedEnumId(Map<String, Object> fieldsValues, String cn) {
        return fieldsValues != null ? ObjectUtils.toString(fieldsValues.get(cn), "") : "";
    }

    private Comparator<String> newComparator(final Map<String, String> enumValues) {
        return new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return ObjectUtils.toString(enumValues.get(o1), "").compareTo(ObjectUtils.toString(enumValues.get(o2), ""));
            }
        };
    }

    private boolean isSort() {
        return true;
    }

    private String makeEnumLabel(EnumItem item) {
        String label;

        if (Field.STYLE_CN.equals(getRefStyle()))
            label = item.getCn() + " - " + item.getTitle();
        else if (Field.STYLE_CN_COLON.equals(getRefStyle()))
            label = item.getCn() + ": " + item.getTitle();
        else if (Field.STYLE_CN_ONLY.equals(getRefStyle()))
            label = item.getCn();
        else
            label = item.getTitle();

        return label;
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        return value;
    }

    public static boolean checkIfEquals(EnumItem selected, EnumItem compared) {
        if (selected == null || compared == null) {
            return false;
        }

        return Objects.equal(selected.getId(), compared.getId());
    }

    @Override
    public String getRefField() {
        return refField;
    }

    @Override
    public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
        pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);

        if (isAuto())
            field.setAutocomplete(true);
        field.setAutocompleteRegExp(getAutocompleteRegExp());

        return field;
    }
}