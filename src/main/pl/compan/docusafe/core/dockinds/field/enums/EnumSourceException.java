package pl.compan.docusafe.core.dockinds.field.enums;

import pl.compan.docusafe.core.EdmException;

/**
 * Created by kk on 21.02.14.
 */
public class EnumSourceException extends EdmException {
    public EnumSourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public EnumSourceException(String message) {
        super(message);
    }

    public EnumSourceException(Throwable cause) {
        super(cause);
    }
}
