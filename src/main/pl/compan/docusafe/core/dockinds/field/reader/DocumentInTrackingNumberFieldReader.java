package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;

import pl.compan.docusafe.core.dockinds.field.DocumentInTrackingNumberField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class DocumentInTrackingNumberFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		String type = elType.attributeValue("type");
		return new DocumentInTrackingNumberField((el.attributeValue("id")), el.attributeValue("cn"), type, names, required, requiredWhen, availableWhen);
	}

}
