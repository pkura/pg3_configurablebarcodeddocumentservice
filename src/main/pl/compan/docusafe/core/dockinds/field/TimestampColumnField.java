package pl.compan.docusafe.core.dockinds.field;

import org.dom4j.Element;
import org.drools.util.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class TimestampColumnField extends Field implements TimestampField {
    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);

    /**
     * @param elType
     * @param id            id z xml'a, musi by� unikalne
     * @param cn            cn z xml'a, musi b� unikalne
     * @param column        nazwa kolumny, nie mo�e by� null (niestety)
     * @param names         nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param required      czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen  warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws pl.compan.docusafe.core.EdmException
     *
     */
    public TimestampColumnField(Element elType, String id, String cn, String column, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, column, names, Field.TIMESTAMP, null, null, required, requiredWhen, availableWhen);

        addParam("hours", true);
        addParam("minutes", true);

        boolean secs = true;

        if (elType.attributeValue("seconds") != null) {
            if ("false".equals(elType.attributeValue("seconds"))) {
                secs = false;
            }
        }
        addParam("seconds", secs);

        if (elType.attributeValue("allowNull") != null && elType.attributeValue("allowNull").equals("true")) {
            setAllowNull(true);
        }
    }

    @Override
    public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
        if (value == null) {
            ps.setObject(index, null);
        } else {
            ps.setTimestamp(index, new java.sql.Timestamp((simpleCoerce(value)).getTime()));
        }
    }

    @Override
    public Date simpleCoerce(Object value) throws FieldValueCoerceException {
        if (value == null)
            return null;

        Date date;
        if (!(value instanceof Date)) {
            if (StringUtils.isEmpty(value.toString())) {
                return null;
            }
            try {
                date = DateUtils.parseJsDateWithOptionalTime(String.valueOf(value));
                if (!DSApi.isLegalDbDate(date))
                    throw new FieldValueCoerceException(sm.getString("PrzekazanaDataNieZnajdujeSie") + " " +
                            sm.getString("WzakresieWymaganymPrzezBazeDanych") + ": " + DateUtils.formatJsDate(date));
            } catch (FieldValueCoerceException e) {
                throw e;
            } catch (Exception e) {
                //log.error(e.getMessage(), e);
                throw new FieldValueCoerceException(sm.getString("NieMoznaSkonwertowacWartosci") + " '" + value + "' " + sm.getString("DoDaty"));
            }
        } else {
            date = (Date) value;
            if (!DSApi.isLegalDbDate(date))
                throw new FieldValueCoerceException(sm.getString("PrzekazanaDataNieZnajdujeSie") + " " +
                        sm.getString("WzakresieWymaganymPrzezBazeDanych") + ": " + DateUtils.formatJsDate(date));
        }
        return date;
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        java.sql.Timestamp date = rs.getTimestamp(getColumn());
        Date value = date != null ? new Date(date.getTime()) : null;
        return new FieldValue(this, value, value);
    }

    @Override
    public Object provideDescription(Object value) {
        if (value == null) {
            return value;
        } else {
            return DateUtils.formatJsDateTimeWithSeconds((Date) value);
        }
    }

    public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
        pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.DATE);
        field.setParams(getParams());
        return field;
    }
}
