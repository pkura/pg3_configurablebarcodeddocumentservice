/**
 * 
 */
package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.office.Gauge;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class DocumentOutGauge extends NonColumnField implements CustomPersistence, EnumNonColumnField, SearchPredicateProvider {

	private static final StringManager sm = StringManager.getManager(Constants.Package);
	
	public DocumentOutGauge(String id, String type, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen)
			throws EdmException {
		super(id, cn, names, Field.DOCUMENT_OUT_GAUGE, null, null, required, requiredWhen, availableWhen);
	}
	
	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) {
		String valS = (String)values.get(this.getCn());
		Integer valI = valS != null && valS.length() > 0 ? Integer.parseInt(valS) : null;
		dockindQuery.outOfficeGauge(valI);
	}

	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException {
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("", "--wybierz--");
		
		if (fieldsValues != null && fieldsValues.get(getCn()) != null) {
			enumSelected.add(fieldsValues.get(getCn()).toString());
		} 
		else {
			enumSelected.add("");
		}

		for (Gauge gauge : Gauge.list()) {
			enumValues.put(gauge.getId().toString(), gauge.getTitle());
		}
		
		return new EnumValues(enumValues, enumSelected);
	}

	public void persist(Document doc, Object key) throws EdmException {
		Integer gaugeId = null;
		
		if (key instanceof String) {
			gaugeId = Integer.valueOf((String) key);
		} else {
			gaugeId = (Integer) key;
		}
		
		if (gaugeId != null && doc instanceof OutOfficeDocument) {
			((OutOfficeDocument) doc).setGauge(Gauge.find(gaugeId).getId());
		}
	}

	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		OfficeDocument doc = OfficeDocument.find(documentId);
		
		//narazie jest tylko dla OutOfficeDocument, pozniej mozna zrobic takze dla InOfficeDocument 
		if (doc instanceof OutOfficeDocument && ((OutOfficeDocument)doc).getGauge() != null ) {
			return new FieldValue(this, ((OutOfficeDocument)doc).getGauge().toString(), ((OutOfficeDocument)doc).getGauge().toString());
		} else {
			return new FieldValue(this, null, null);
		}
	}

	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
		if (value == null || value.toString().equals("")) {
			return null;
		}
		
		if (value instanceof Gauge) {
			return ((Gauge) value).getId();
		}
		
		try {
			return Integer.parseInt(value.toString());
		} catch (NumberFormatException e) {
			throw new FieldValueCoerceException(sm.getString("DocumentOutGaugeConversionError", value));
		}
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
	}

}
