package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.service.tasklist.Task;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Klasa reprezentuj�ca pole typu long czyli du�a liczba ca�kowita
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class LongColumnField  extends Field implements LongField {

    /**
     * @param id            id z xml'a, musi by� unikalne
     * @param cn            cn z xml'a, musi b� unikalne
     * @param column        nazwa kolumny, nie mo�e by� null (niestety)
     * @param names         nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param required      czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen  warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws pl.compan.docusafe.core.EdmException
     *
     */
    public LongColumnField(String id, String cn, String column, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, column, names, Field.LONG, null, null, required, requiredWhen, availableWhen);
    }

    @Override
    public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
        if(value == null){
            ps.setObject(index, null);
        } else {
            ps.setLong(index, simpleCoerce(value));
        }
    }

    @Override
    public Long simpleCoerce(Object value) throws FieldValueCoerceException {
        if(value == null || value.toString().isEmpty()){
            return null;
        }

        if(value instanceof Long) {
            return (Long) value;
        }

        if(value instanceof CharSequence){
            try {
                return Long.valueOf(value.toString().replaceAll(" ",""));
            } catch (NumberFormatException ex) {
                throw new FieldValueCoerceException(value.toString().replaceAll(" ","") + " cannot be converted to Long by LongColumnField");
            }
        }

        throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + " cannot be converted to Long by LongColumnField");
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        long l = rs.getLong(getColumn());
        Long value = rs.wasNull() ? null : new Long(l);
        return new FieldValue(this, value, value);
    }
    
    public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.LONG);
	}
    
    // potrzebowa�e zeby wrzuca tutaj object
    public static class LongComparator implements Comparator<Object>
    {
        public int compare(Object e1, Object e2)
        {
        	if(e1 instanceof Long && e2 instanceof Long && e1 != null && e2 != null)
        		return ((Long)e1).compareTo((Long)e1);
        	else
        		return 1;
        }
    }
}
