package pl.compan.docusafe.core.dockinds.field;


import org.dom4j.Element;
import org.drools.builder.*;
import org.drools.definition.KnowledgePackage;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.drools.DroolsUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;

import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Przycisk, kt�ry powoduje odpalenie regu�y droolsowej
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DroolsButtonField extends NonColumnField {
    private static final Logger log = LoggerFactory.getLogger(Field.class);

    /**
     * Zbi�r regu� powi�zana z danym przyciskiem. Wszystkie s� odpalane w momencie wci�ni�cia.
     */
    private final KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();

    /**
     * Obiekt zwracany w provideFieldValue - tak by nie tworzy� ich za wiele
     */
    private final FieldValue fieldValue = new FieldValue(this, null);

    public DroolsButtonField(String id, String cn, Map<String, String> names, List<Condition> availableWhen) throws EdmException {
        
        super(id, cn, names,
                Field.DROOLS_BUTTON, null, null, false, null, availableWhen);
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        return value;
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        return fieldValue;
    }

    @Override
    public void setMultiple(boolean multiple) {
        if(multiple){
            throw new IllegalArgumentException("Drools button cannot be multiple");
        }
    }

    public void apply(DocFacade doc) throws EdmException {
        StatelessKnowledgeSession ksession = getKbase().newStatelessKnowledgeSession();
        ksession.execute(Arrays.asList(doc, doc.getFields()));
    }

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
			throws EdmException {
		// TODO Auto-generated method stub
		return null;
	}

	public KnowledgeBase getKbase()
	{
		return kbase;
	}
}
