package pl.compan.docusafe.core.dockinds.field;

import java.io.Serializable;

import org.dom4j.Element;

/**
 * Formatowanie danego pola
 */
public class FieldFormatting implements Serializable{

    //TODO klasa mo�liwa do rozbudowy o nie-domy�lne formattery - wystarczy zmieni� metod� parse
    private final FieldFormatter defaultFormatter;

    public FieldFormatting(FieldFormatter defaultFormatter) {
        this.defaultFormatter = defaultFormatter;
    }

    public static FieldFormatting parse(Element el){
        if(el == null){
            return null;
        } else {
            return new FieldFormatting(FormattingParsers
                    .valueOf(el.attributeValue("type").toUpperCase().replace("-","_"))
                    .parse(el));
        }
    }

    public FieldFormatter getDefaultFormatter() {
        return defaultFormatter;
    }

    enum FormattingParsers {
        CODE_AND_NAME{
            @Override
            FieldFormatter parse(Element el) {
                return FieldFormatter.CODE_AND_TITLE;
            }},
        TO_STRING{
            @Override
            FieldFormatter parse(Element el) {
                return FieldFormatter.TO_STRING;
            }
        };

        abstract FieldFormatter parse(Element el);
    }
}
