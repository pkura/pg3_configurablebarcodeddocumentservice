package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.HTMLField;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class HTMLFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{

		String value = (String) elType.attributeValue("value");

		HTMLField htmlField = new HTMLField(el.attributeValue("id"), el.attributeValue("logic"), el.attributeValue("cn"), names, required, requiredWhen, availableWhen, value);

		if (elType.attributeValue("tag") != null)
			htmlField.setTag(elType.attributeValue("tag"));

		if (elType.attribute("field-value") != null)
			htmlField.setFieldValue(elType.attributeValue("field-value"));
		
		boolean reset = Boolean.parseBoolean(elType.attributeValue("reset"));
		htmlField.setResettable(reset);

		return htmlField;
	}
}
