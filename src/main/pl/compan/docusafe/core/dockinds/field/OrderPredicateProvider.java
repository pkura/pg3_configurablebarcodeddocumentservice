package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.dockinds.Database.Column;
import pl.compan.docusafe.core.dockinds.Database.Table;
import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.TableAlias;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public interface OrderPredicateProvider {

    /**
     * @return Czy pole jest sortwane wed�ug widocznych warto�ci pola. Warto�ci takie mog� by� wynikiem z��czenia wielu kolumn.
     */
    public boolean isManualSorted();

    /**
     * @return Je�eli nie ma pasuj�cego obiektu lub gdy pasuj�cych obiekt�w jest wi�cej ni� 1, zwaracny jest null
     */
    public Order getOrder(Table documentTable, String fieldCn);

    public static class OrderColumn {
        public final Column column;
        public final TableAlias tableAlias;
        public final boolean isAscending;

        public OrderColumn(Column column, TableAlias tableAlias, boolean ascending) {
            this.column = column;
            this.tableAlias = tableAlias;
            isAscending = ascending;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            OrderColumn that = (OrderColumn) o;
            return !(column != null ? !column.equals(that.column) : that.column != null) && !(tableAlias != null ? !tableAlias.equals(that.tableAlias) : that.tableAlias != null);
        }

        public Attribute getColumnAttribute() {
            return tableAlias.attribute(column.getName());
        }
    }

    /**
     * Reprezentuje po��czenie tabel: prevTable.linkPrevToNext = nextTable.linkNextToPrev
     */
    public static class LinkedTable {
        private final Table prevTable;
        private final Table nextTable;
        private String linkPrevToNext;
        private String linkNextToPrev;

        public LinkedTable(Table prevTable, String linkPrevToNext, String linkNextToPrev, Table nextTable) {
            this.prevTable = prevTable;
            this.linkPrevToNext = linkPrevToNext;
            this.linkNextToPrev = linkNextToPrev;
            this.nextTable = nextTable;
        }

        public Table getPrevTable() {
            return prevTable;
        }

        public String getLinkNextToPrev() {
            return linkNextToPrev;
        }

        public String getLinkPrevToNext() {
            return linkPrevToNext;
        }

        public Table getNextTable() {
            return nextTable;
        }
    }

    public static class Order {
        private final String fieldCn;
        private final Table documentKind;
        private final LinkedTable[] linkedTables;
        private final Column orderedColumn;

        /**
         * @param fieldCn       Je�eli fieldCns r�wne null, to Order jest zgodny ze wszystkimi fieldCn
         * @param documentKind  Rodzaje dokument�w, kt�rych dotycz� tworzeone zapytanie na podstawie tego obiektu
         * @param linkedTables  Po�aczenia kolejnych tabel. Pierwsz� tabel� powinna by� "ds_document", drug� tabela
         *                      dla danego dokumentu... ostatnia tabela musi zawiera� kolumn�, po kt�rej b�d� sortowane
         *                      wyniki
         * @param orderedColumn kolumna, po kt�rej b�d� sortowane wyniki
         */
        public Order(String fieldCn, Table documentKind, LinkedTable[] linkedTables, Column orderedColumn) {
            this.fieldCn = fieldCn;
            this.orderedColumn = orderedColumn;
            this.documentKind = documentKind;
            this.linkedTables = linkedTables;
        }

        /**
         * @return Pusty lub null fieldCns odpowiada ka�demu fieldCn
         */
        public boolean isMaching(Table documentKind, String fieldCn) {
            return isMachingDocumentKind(documentKind) && isMachingFieldCn(fieldCn);
        }

        /**
         * @return Je�eli fieldCns puste lub null, zwracany rezultat jest pozytywny
         */
        public boolean isMachingFieldCn(String fieldCn) {
            return this.fieldCn == null || this.fieldCn.equalsIgnoreCase(fieldCn);
        }

        public boolean isMachingDocumentKind(Table documentKind) {
            return this.documentKind == null || this.documentKind.getName().equalsIgnoreCase(documentKind.getName());
        }

        public LinkedTable[] getLinkedTablesIfStartFrom(Table table) {
            if (linkedTables != null && linkedTables.length > 0)
                if (linkedTables[0].getPrevTable().equals(table))
                    return linkedTables;
            return new LinkedTable[0];
        }

        public boolean whetherPlaceInThis(Table table) {
            return linkedTables != null && linkedTables.length > 0 && linkedTables[0].getPrevTable().equals(table) && linkedTables[0].getNextTable() == null;
        }

        public Column getOrderColumn() {
            return this.orderedColumn;
        }
    }

    public static class OrdersFactory {

        @SuppressWarnings("unused")
        public static LinkedTable createNextTable(LinkedTable prevLinkedTable, String linkPrevToNext, String linkNextToPrev, Table nextTable) {
            return new LinkedTable(prevLinkedTable.getNextTable(), linkPrevToNext, linkNextToPrev, nextTable);
        }

        /**
         * @param orderBy       nazwa kolumny, po kt�rej bedzie sortowanie
         * @param documentKinds tabele po��czone z ds_document poprzez id-id, w przypadku braku argument�w dokumentem,
         *                      w kt�rym znajduje si� kolumna bedzie tabela ds_document
         */
        public static Order[] createOrders(Column orderBy, Table... documentKinds) {
            if (documentKinds == null || documentKinds.length == 0)
                return new Order[]{createOrder(orderBy)};
            Order[] orders = new Order[documentKinds.length];
            for (int i = 0; i < documentKinds.length; ++i)
                orders[i] = createOrder(orderBy, documentKinds[i]);
            return orders;
        }

        public static Order createOrder(Column orderBy) {
            String fieldCn = null;
            Table documentsKind = null;
            OrderPredicateProvider.LinkedTable[] linkedTables = new LinkedTable[]{OrdersFactory.createDocumentTable()};
            Column orderedColumn = orderBy;
            return new OrderPredicateProvider.Order(fieldCn, documentsKind, linkedTables, orderedColumn);
        }

        /**
         * Tworzy jednostronn� relacj�: DS_DOCUMENT. Wykorzystywane, gdy pole znajduje si� bezpo�rednio w DS_DOCUMENT
         */
        public static LinkedTable createDocumentTable() {
            return new LinkedTable(new Table("ds_document"), null, null, null);
        }

        public static Order createOrder(Column orderBy, Table documentKind) {
            String fieldCn = null;
            Table documentsKind = documentKind;
            LinkedTable[] linkedTables = new LinkedTable[]{OrdersFactory.createDocumentTable(documentKind)};
            Column orderedColumn = orderBy;
            return new Order(fieldCn, documentsKind, linkedTables, orderedColumn);
        }

        /**
         * Tworzy po��czenie DS_DOCUMENT.id - documentKind.id
         */
        public static LinkedTable createDocumentTable(Table documentKindTable) {
            return new LinkedTable(new Table("ds_document"), "id", "id", documentKindTable);
        }


    }
}
