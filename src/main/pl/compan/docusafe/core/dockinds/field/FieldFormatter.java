package pl.compan.docusafe.core.dockinds.field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 */
public interface FieldFormatter<T> {

    void format(T fieldValue, Appendable app) throws IOException;

    FieldFormatter<Object> TO_STRING = new ToStringFormatter();
    FieldFormatter<EnumItem> CODE_AND_TITLE = new CodeAndTitleFormatter();
    FieldFormatter<EnumItem> TITLE = new TitleFormatter();
}

class CodeAndTitleFormatter implements FieldFormatter<EnumItem> {
    final static Logger LOG =  LoggerFactory.getLogger(FieldFormatter.class);

    public void format(EnumItem fieldValue, Appendable app) throws IOException{
        app.append(fieldValue.getCn())
           .append(" - ")
           .append(fieldValue.getTitle());
    }
}