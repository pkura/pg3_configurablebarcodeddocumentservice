package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.primitives.Ints;

public class DocumentInLocationField extends NonColumnField implements CustomPersistence {

	protected static Logger log = LoggerFactory.getLogger(DocumentInLocationField.class);
	private String type = "in";
	public DocumentInLocationField(String id, String type ,String cn, String refStyle, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
		super(id, cn, names, Field.DOCUMENT_IN_LOCATION, null, null, required, requiredWhen, availableWhen);
		//this.setSearchShow(false);
		
		if (type != null)
		{
			this.type = type;
		}
		
	}
	
	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		
		OfficeDocument doc = OfficeDocument.find(documentId);
		if (doc instanceof OutOfficeDocument)
			throw new EdmException("DocumentInLocationField provideFieldValue doc class: "+doc.getClass());
		else if (doc instanceof  InOfficeDocument)
			return new FieldValue(this, ((InOfficeDocument) doc).getLocation());
		
		else
			return null;
		
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        if(value == null){
            return null;
        }

        if(value instanceof String)
        	return  value;
        else
        {
        	throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + "b�ad kowertowania  przez DocumentReferenceIdField");
        }
		
	}

	public void persist(Document doc, Object key) throws EdmException {
		
	   	if (key instanceof String)
        {
    	    if (doc instanceof InOfficeDocument)
    	        ((InOfficeDocument) doc).setReferenceId((String) key);
    	    else if (doc instanceof OutOfficeDocument)
            		throw new EdmException("STATUS -- LokalizacjeDokumentuPrzychodzacegoMoznaDodacTylkoWpismiePrzychodzacym");
        } else
		log.error("B�ad przy dodawaniu do dokumentu Lokalizacji");
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
		return  new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING);
	}
	
	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) 
	{
		String  valS = (String) values.get(this.getCn());
		log.debug(" {} {} ",valS);
		dockindQuery.likeInOfficeLocation(valS);	
	}
}
