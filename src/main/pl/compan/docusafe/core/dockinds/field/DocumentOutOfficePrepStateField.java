package pl.compan.docusafe.core.dockinds.field;

import static pl.compan.docusafe.util.I18nUtils.string;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DocumentOutOfficePrepStateField  extends NonColumnField implements CustomPersistence, EnumNonColumnField, SearchPredicateProvider
{

	private Map<Integer,String>  clearOrDraft = new HashMap<Integer, String>();
	private Map<String,String> values =new HashMap<String, String>();
	
	private static final Logger log = LoggerFactory.getLogger(DocumentOutOfficePrepStateField.class);
	private String type = "in";
	private List<EnumItem> enumItems = new ArrayList<EnumItem>();
	
	public DocumentOutOfficePrepStateField(String id, String type, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, names, Field.DOCUMENT_OUT_OFFICE_PREPSTATE, null, null, required, requiredWhen, availableWhen);
		if (type != null)
		{
			this.type = type;
		}
		clearOrDraft.put(1, OutOfficeDocument.PREP_STATE_FAIR_COPY);
		clearOrDraft.put(2, OutOfficeDocument.PREP_STATE_DRAFT);
		values.put("1","Czystopis" );
		values.put("2","Brudnopis");
		Map<String,String> titles = new HashMap<String,String>();
		
		{
			titles = new HashMap<String,String>();
			titles.put("Czystopis", OutOfficeDocument.PREP_STATE_DRAFT);
			enumItems.add(new EnumItemImpl(1, "CN_"+1, titles, new String[3], false,this.getCn()));
			titles = new HashMap<String,String>();
			titles.put("Brudnopis",OutOfficeDocument.PREP_STATE_FAIR_COPY);
			enumItems.add(new EnumItemImpl(2, "CN_"+2, titles, new String[3], false,this.getCn()));
		}
	}

	@Override
	public void persist(Document doc, Object key) throws EdmException
	{
		Integer statusId = null;
        	if (key instanceof String)
        		statusId = Integer.valueOf((String) key);
        	else
        		statusId = (Integer) key;
        	if (statusId != null)
            {
        	if (doc instanceof OutOfficeDocument) {
        	    ((OutOfficeDocument) doc).setPrepState(clearOrDraft.get((statusId)));
        	     if (statusId==1)
        	    ((OutOfficeDocument) doc).setBlocked(true);
        	    else ((OutOfficeDocument) doc).setBlocked(false);
        	}
        	else
        		throw new EdmException("STATUS -- RodzajDokumentuPrzychodzacegoMoznaDodacTylkoWpismiePrzychodzacym");
            }
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		OfficeDocument doc = OfficeDocument.find(documentId);
		Object key=null;
		Object value=null;
		if (doc instanceof OutOfficeDocument && ((OutOfficeDocument)doc).getPrepState() != null ){
			for ( Integer i   : clearOrDraft.keySet()){
				if (clearOrDraft.get(i).equals(((OutOfficeDocument)doc).getPrepState()))
					key=i ; 
				value=((OutOfficeDocument)doc).getPrepState();
			}
		
			return new FieldValue(this,key, value);
		}else
			return new FieldValue(this, null, null);

	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value == null || (value.toString().equals("")))
			return null;
		if (value instanceof OutOfficeDocument)
			return ((OutOfficeDocument) value).getId();
		try
		{
			return Integer.parseInt(value.toString());
		}
		catch (NumberFormatException e)
		{
			throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci") + " " + value + " " + string("DoLiczbyCalkowitej"));
		}
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
	}
	
	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException
	{
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("", "--wybierz--");

		if (fieldsValues != null && fieldsValues.get(getCn()) != null)
			enumSelected.add(fieldsValues.get(getCn()).toString());
		else
			enumSelected.add("");

			enumValues.putAll(values);
		
		return new EnumValues(enumValues, enumSelected);
	}

	public final List<EnumItem>  getAllEnumItemsByAspect(String documentAspectCn,DocumentKind kind)
	{
		
		return this.enumItems;
	}

	
	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) 
	{
		
	}
	
}
