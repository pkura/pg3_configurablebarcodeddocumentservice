package pl.compan.docusafe.core.dockinds.field.reader;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DictionaryFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		String notNull = elType.attributeValue("not-nullable-fields");
		
		DictionaryField dicField = new DictionaryField(el.attributeValue("id"), el.attributeValue("cn"), el.attributeValue("column") != null ? el.attributeValue("column") : "*none*", elType.attributeValue("logic"), names, Field.DICTIONARY, refStyle, required, requiredWhen, availableWhen,
				elType.attributeValue("tableName"), elType.attributeValue("resourceName"), elType.attributeValue("filteringDictionaryCn"), DocumentKind.find(di.getDockindId()));
		dicField.readParameters(el, elType);
		dicField.setNotNullableFields(StringUtils.isNotBlank(notNull)?Arrays.asList(notNull.split(",")):null);

        if(AvailabilityManager.isAvailable("initDictionaryDocumentKind")){
            if(di != null && di.getDockindId() != null && DocumentKind.find(di.getDockindId()) != null)
                dicField.setDockindCn(DocumentKind.find(di.getDockindId()).getCn());
        }
        new DwrDictionaryBase(dicField, null);

		return dicField;
	}
}
