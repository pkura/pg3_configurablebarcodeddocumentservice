package pl.compan.docusafe.core.dockinds.field;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;

public class CommonBooleanLogic {
	protected Field field;
	protected Boolean value;
	
	public CommonBooleanLogic(Field field) {
		this.field = field;
	}

	/**
	 * Pr�buje skonwerowa� warto�� na Boolean: null -> zwraca null Boolean ->
	 * zwraca value CharSequence -> zwraca Boolean.valueOf(value.toString()) w
	 * innym przypadku -> wyrzuca FieldValueCoerceException
	 * 
	 * @param value
	 *            najcz�ciej String ale mo�e tak�e by� typ docelowy
	 * @return
	 * @throws FieldValueCoerceException
	 *             gdy value jest nieobs�ugiwanego typu
	 */
	public Boolean simpleCoerce(Object value) throws FieldValueCoerceException {
		if (value == null) {
			this.value = null;
			return null;
		}
		Boolean ret;

		if (value instanceof Boolean) {
			ret = (Boolean) value;
		} else if (value instanceof CharSequence) {
			if (value.equals("on"))
				ret = true;
			else
				ret = Boolean.valueOf(value.toString());
		} else {
			throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + " cannot be converted to Boolean by BooleanField");
		}

		this.value = ret;
		return ret;
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
        pl.compan.docusafe.core.dockinds.dwr.Field f = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.field.getCn(), this.field.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        f.setOnchange(this.field.getOnchange());
        return f;
	}

	public FieldValue provideColumnFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        try{
		    boolean bool = rs.getBoolean(this.field.getColumn());
		    Boolean value = rs.wasNull() ? null : bool;
		    return new FieldValue(this.field, value, (value == null || !value) ? "nie" : "tak");
        }catch(SQLException e){
            throw new SQLException(e.getMessage() + ":" + this.field.getColumn());
        }
	}
	
	public FieldValue provideNonColumnFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		return new FieldValue(this.field, value, (value == null || !value) ? "nie" : "tak");
	}

	public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
		if (value == null) {
			ps.setObject(index, null);
		} else {
			ps.setBoolean(index, simpleCoerce(value));
		}
	}
	
	public Boolean reset() {
		this.value = false;
		return this.value;
	}
}