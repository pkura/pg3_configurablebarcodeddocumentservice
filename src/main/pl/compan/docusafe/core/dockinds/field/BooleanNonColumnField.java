package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;

public class BooleanNonColumnField extends NonColumnField {
	protected CommonBooleanLogic item;
	protected Boolean value;
	
	public BooleanNonColumnField(String id, String cn, Map<String, String> names, boolean required,
			List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
		super(id, cn, names, Field.BOOL, null, null, required, requiredWhen, availableWhen);
		item = new CommonBooleanLogic(this);
	}

	public Boolean simpleCoerce(Object value) throws FieldValueCoerceException {
		this.value = item.simpleCoerce(value);
		return this.value;
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
		return item.getDwrField();
	}

	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		return item.provideNonColumnFieldValue(documentId, rs);
	}

	@Override
	public Boolean reset() {
		return item.reset();
	}
}
