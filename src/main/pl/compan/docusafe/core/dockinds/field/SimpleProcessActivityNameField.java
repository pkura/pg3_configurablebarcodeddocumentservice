package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.Field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * User: Tomasz
 * Date: 16.04.14
 * Time: 12:09
 */
public class SimpleProcessActivityNameField extends NonColumnField implements ProcessVariableField, CustomPersistence {

    public SimpleProcessActivityNameField(String id, String type, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, names, pl.compan.docusafe.core.dockinds.field.Field.SIMPLE_PROCESS_ACTIVITY_NAME, null, null, required, requiredWhen, availableWhen);
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        throw new UnsupportedOperationException("Field " + getType() + " is not persisted in db");
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        if(value == null) {
            return null;
        }
        else if(value instanceof String) {
            return value;
        }
        else {
            return String.valueOf(value);
        }
    }

    @Override
    public Field getDwrField() throws EdmException {
        return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), Field.Type.STRING);
    }

    @Override
    public FieldValue provideFieldValue(long documentId, String activityId) throws EdmException {
        if(activityId == null || activityId.length() < 1) {
            return new FieldValue(this, null, null);
        }
        activityId = activityId.contains(",") ? activityId.split(",")[1] : activityId;

        //TODO: wstawic na value wartosc pola
        String value = String.valueOf("");
        return new FieldValue(this,value,value);
    }

    @Override
    public void persist(Document doc, Object key) throws EdmException {}

}
