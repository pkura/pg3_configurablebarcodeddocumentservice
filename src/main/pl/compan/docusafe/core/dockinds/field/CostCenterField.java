package pl.compan.docusafe.core.dockinds.field;

import static pl.compan.docusafe.util.I18nUtils.string;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;

/**
 * pole Centrum Koszt�w - wyspecjalizowany s�ownik dla faktur
 *
 * klucz - id (Long) Centrum Kosztowego
 * obiekt - na razie tylko obiekt typu CentrumKosztowDlaFaktury
 *          typ obiektu jest zapisany w parametrze klass
 *
 * Centra koszt�w przypiasne do dokumentu s� zapisywane w oddzielnej tabeli
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class CostCenterField extends NonColumnField {

    /**
     * Konstruktor pola Centrum Koszt�w
     *
     * @param id            id z xml'a, musi by� unikalne
     * @param cn            cn z xml'a, musi b� unikalne
     * @param classname     nazwa klasy zwykle CentrumKosztowDlaFaktury.class.getName()
     * @param names         nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param required      czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen  warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws pl.compan.docusafe.core.EdmException
     */
    public CostCenterField(String id, String cn, String classname,  Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, names, Field.CENTRUM_KOSZT, null, classname, required, requiredWhen, availableWhen);
    }

    @Override
    public Long simpleCoerce(Object value) throws FieldValueCoerceException {
        if (value == null){
           return null;
        }

        if (value instanceof Long) {
            return (Long)value;
        }

        if(value instanceof CentrumKosztowDlaFaktury){
            return ((CentrumKosztowDlaFaktury)value).getId();
        }

        try {
            return Long.parseLong(value.toString());
        } catch (NumberFormatException e) {
            throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci")+" "+value+" "+string("DoLiczbyCalkowitej"));
        }
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        Object object = invokeMethod("findByDocumentId", "findByDocumentId", null, new Object[]{documentId}, new Class[]{Long.class});
        List<Object> values;
        if (object instanceof List)
            values = (List) object;
        else
            throw new IllegalStateException("Wynik funkcji 'findByDocumentId' jest innego typu ni� 'List'");
        List<Long> keys = new ArrayList<Long>();
        for (Object o : values)
        {
            Object id = invokeMethod("getId", "getId", o);
            if (id instanceof Long)
                keys.add((Long) id);
            else
                throw new IllegalStateException("Wynik funkcji 'getId' jest innego typu ni� 'Long'");
        }
        return new FieldValue(this, keys, values);
    }
    
    @Override
    public Object provideDescription(Object value)
    {
    	if (value != null)
    	{
    		CentrumKosztowDlaFaktury ckf = (CentrumKosztowDlaFaktury) value;
			return ckf.getDescription();
    	}
    	
    	return "";
    }

    /**
     * Zwraca odpowiedni obiekt typu CentrumKosztowDlaFaktury
     * @param key - Long identyfikator powy�szego obiektu
     * @return CentrumKosztowDlaFaktury
     * @throws EdmException
     */
    public CentrumKosztowDlaFaktury asObject(Object key) throws EdmException{
        return CentrumKosztowDlaFaktury.getInstance().find((Long) key);
    }

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
			throws EdmException {
		// TODO Auto-generated method stub
		return null;
	}
}
