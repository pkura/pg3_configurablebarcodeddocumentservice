package pl.compan.docusafe.core.dockinds.field.reader;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.SimpleProcessVariableField;

import java.util.List;
import java.util.Map;

/**
 * User: Tomasz
 * Date: 03.04.14
 * Time: 14:09
 */
public class SimpleProcessVariableReader implements FieldReader {

    @Override
    public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Field.Condition> availableWhen, List<Field.Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {
        return new SimpleProcessVariableField(el.attributeValue("id"),
                elType.attributeValue("type"),
                el.attributeValue("cn"),
                names,
                required,
                requiredWhen,
                availableWhen,
                elType.attributeValue("variableName"));
    }
}
