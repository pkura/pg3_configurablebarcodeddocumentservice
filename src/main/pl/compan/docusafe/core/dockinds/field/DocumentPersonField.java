package pl.compan.docusafe.core.dockinds.field;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.directwebremoting.WebContextFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * (tylko dla DWR) Klasa zapisuj�ca obiekt klasy Person w odpowiednim polu klasy
 * Document (nie w tabeli dockindowej)
 * 
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DocumentPersonField extends ManualSortedNonColumnField implements CustomPersistence, SearchPredicateProvider,DictionarableField
{
    private static final Logger LOG = LoggerFactory.getLogger(DocumentPersonField.class);
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);
    static final String SHOW_POPUP = "showPopup";

    //private final String logic;
	private List<Field> fields;
	private List<String> popUpFields;
	private List<String> promptFields;
	private List<String> searchFields;
	private List<String> visibleFields;
	private List<String> popUpButtons;
	private List<String> dicButtons;
	//wy�wietlanie tych pozycji s�ownika ds_person, kt�rych pola okre�lone parametrem not-nullable-fields nie s� null'ami
	private List<String> notNullableFields;

	public enum PersonType
	{
		SENDER, RECIPIENT
	}

	private final PersonType type;

    public DocumentPersonField(String id, String cn, String logic, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String person) throws EdmException {
        super(id, cn, names, Field.DOCUMENT_PERSON, null, null, required, requiredWhen, availableWhen);
        this.logic = logic;
        this.type = PersonType.valueOf(person.toUpperCase());
    }

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		OfficeDocument doc = OfficeDocument.find(documentId);
		switch (type)
		{
			case SENDER:
				if (doc.getSender() != null)
				{
					return new FieldValue(this, doc.getSender().getId(), doc.getSender().getShortSummary());
				}
				break;
			case RECIPIENT:
				if (!doc.getRecipients().isEmpty())
				{
					return new FieldValue(this, doc.getRecipients().get(doc.getRecipients().size() - 1).getId(), doc.getRecipients().get(doc.getRecipients().size() - 1).getShortSummary());
				}
				break;
		}
		return null;
	}

	public void persist(Document dc, Object key) throws EdmException
	{
		LOG.info("DocumentPersonField.persist: cn - {}, type - {}, dc - {}, key - {}", this.getCn(), type, dc, key);
		OfficeDocument doc = ((OfficeDocument) dc);
		switch (type)
		{
			case SENDER:
			{
				if (doc.getSenderId() == null && key == null)
				{
					doc.setSenderId(null);
				}
				else if (doc.getSenderId() != null && key == null)
					return;
				else if (doc.getSenderId() != null) {
					if (key instanceof String) {
						if (doc.getSenderId().equals(Long.valueOf((String) key)))
							return;
					} else if (doc.getSenderId().equals((Long) key))
						return;
					else if (doc.getSenderId() != null
							&& key != null
							&& AvailabilityManager
									.isAvailable("dokument.EdycjaZapisanieZmianNadawcaOdbiorca")) {
						// zmiana przy edycji dokumentu,
						// usuniecie starego sendera
						String sql = "delete from dso_person where discriminator ='sender' and document_id="
								+ dc.getId().toString();
						PreparedStatement ps = null;
						try {
							ps = DSApi.context().prepareStatement(sql);
							ps.executeUpdate();

							LOG.debug("EditPerson");

							long tmp;
							if (key instanceof String)
								tmp = Long.parseLong((String) key);
							else
								tmp = (Long) key;
							Person person = Person.find(tmp);
							Sender sender = new Sender();
							sender.fromMap(person.toMap());
							sender.setDictionaryGuid("rootdivision");
							sender.setDictionaryType(Person.DICTIONARY_SENDER);
							sender.setBasePersonId(person.getId());
							sender.create();
							doc.setSender(sender);
							doc.setSenderId(sender.getId());
							LOG.debug("Edited sender");

						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						return;

					}
				} else {
					LOG.debug("CReatePerson");
					// DSApi.context().begin();
					long tmp;
					if (key instanceof String)
						tmp = Long.parseLong((String) key);
					else
						tmp = (Long) key;
					Person person = Person.find(tmp);
					Sender sender = new Sender();
					sender.fromMap(person.toMap());
					sender.setDictionaryGuid("rootdivision");
					sender.setDictionaryType(Person.DICTIONARY_SENDER);
					sender.setBasePersonId(person.getId());
					sender.create();
					doc.setSender(sender);
					doc.setSenderId(sender.getId());
					// DSApi.context().commit();
					LOG.debug("Created sender");
				}
				return;
			}
			case RECIPIENT:
			{
				if (key == null)
				{
					doc.setRecipients();
				}
				else if (!doc.getRecipients().isEmpty()) {
					if (key instanceof String) {
						if (doc.getRecipients().get(doc.getRecipients().size() - 1)
								.getId().equals(Long.valueOf((String) key)))
							return;
					} else if (doc.getRecipients()
							.get(doc.getRecipients().size() - 1).getId()
							.equals((Long) key))
						return;
					else if (doc.getRecipients() != null
							&& key != null
							&& AvailabilityManager
									.isAvailable("dokument.EdycjaZapisanieZmianNadawcaOdbiorca")) {
						// zmiana przy edycji dokumentu,
						//dodanie nowego recipienta
							LOG.debug("EditPerson");
							long tmp;
							if (key instanceof String)
								tmp = Long.parseLong((String) key);
							else
								tmp = (Long) key;
							Person person = Person.find(tmp);
							Recipient recipient = new Recipient();
							recipient.fromMap(person.toMap());
							recipient.setDictionaryGuid("rootdivision");
							recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
							recipient.setBasePersonId(person.getId());
							recipient.create();
							doc.addRecipient(recipient);
							LOG.debug("Edited(added) recipient");

						return;

					}

				} else {
					LOG.info("Create new Recipient");
					// DSApi.context().begin();
					long tmp;
					if (key instanceof String)
						tmp = Long.parseLong((String) key);
					else
						tmp = (Long) key;
					Person person = Person.find(tmp);
					Recipient recipient = new Recipient();
					recipient.fromMap(person.toMap());
					recipient.setDictionaryGuid("rootdivision");
					recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
					recipient.setBasePersonId(person.getId());
					recipient.create();
					doc.addRecipient(recipient);
					// DSApi.context().commit();
				}
				return;
			}
		}
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException
	{
		long id;
		if (value == null)
		{
			return null;
		}
		if (value instanceof Long)
		{
			id = (Long) value;
		}
		else if (value instanceof Person)
		{
			id = ((Person) value).getId();
		}
		else if (value instanceof ArrayList && ((ArrayList)value).size()>0)
     	   return FieldUtils.longCoerce(this, ((ArrayList)value).get(0));
		else if (value instanceof CharSequence)
		{
			if (!value.toString().equals(""))
				id = Long.valueOf(value.toString());
			else return null;
		}
		else
		{
			throw new FieldValueCoerceException(this.getName() + ":" + sm.getString("NieMoznaSkonwertowacWartosci") + " " + value + " " + sm.getString("DoLiczbyCalkowitej"));
		}
		return id;
	}
	 
	public pl.compan.docusafe.core.dockinds.dwr.Field getFieldForDwr(Map<String, SchemeField> fieldsScheme) throws EdmException
	{
		pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + getCn(), getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY);
		
		SchemeField scheme = null;
		if (fieldsScheme != null)
			scheme = fieldsScheme.get(this.getCn());
		
		DSUser loggedUser = DSApi.context().getDSUser();
		boolean unlockFields = false;
		if(WebContextFactory.get() != null &&
           WebContextFactory.get().getCurrentPage() != null &&
           WebContextFactory.get().getCurrentPage().endsWith("edit-dockind-document.action") &&
           loggedUser.isAdmin())
            unlockFields = true;
		
		new DwrDictionaryBase(this, fieldsScheme);
		DwrDictionaryBase dic = DwrDictionaryFacade.getDwrDictionary(this.getDockindCn(), this.getCn());
		field.setDictionary(dic);
		field.setHidden(isHidden());
		field.setDisabled(isDisabled());
		field.setRequired(isRequired());
		field.setReadonly(isReadOnly());
		field.setColumn(getDwrColumn());
		field.setFieldset(getDwrFieldset());
		field.setSubmit(isSubmit());
		field.setCssClass(getCssClass());
		field.setNewItemMessage(getNewItemMessage());
		field.setCoords(getCoords());
		if (scheme != null)
		{
			field.setDisabled(scheme.getDisabled());
            if (scheme.getDicButtons()!= null && scheme.getDicButtons().contains(SHOW_POPUP))
                field.getDictionary().setShowDictionaryPopup(true);
			if (scheme.isReadonly() == null && scheme.isRequired() == null && scheme.isHidden() == null && !scheme.getDisabled())
				field.setHidden(scheme.isVisible());

			if (scheme.isRequired() != null)
				field.setRequired(scheme.isRequired());

			if (scheme.isReadonly() != null)
				field.setReadonly(scheme.isReadonly());

			if (scheme.isHidden() != null)
				field.setHidden(scheme.isHidden());
			
			//jezeli jestes adminem i przegladasz archiwum to mozna edytowac wszystkie pola
			if(unlockFields)
			{
				field.setHidden(false);
				field.setDisabled(false);
				field.setRequired(false);
				field.setReadonly(false);
			}
		}
		if (field.getDisabled())
		{
			for (pl.compan.docusafe.core.dockinds.dwr.Field f:  field.getDictionary().getFields())
			{
				if ((field.getDictionary().isMultiple() && !f.getCn().toUpperCase().contains("_ID_")) || (!f.getCn().toUpperCase().equals("ID")))
					f.setDisabled(field.getDisabled());

			}
		}
		for(pl.compan.docusafe.core.dockinds.dwr.Field f : field.getDictionary().getFields())
		{
			//jezeli jestes adminem i przegladasz archiwum to mozna edytowac wszystkie pola s�ownika
			if(unlockFields)
			{
				f.setHidden(false);
				f.setDisabled(false);
				f.setRequired(false);
				f.setReadonly(false);
			}
		}
		return field;
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
	{
		return null;
	}
	
	public List<Field> getFields()
	{
		return fields;
	}

	public void setFields(List<Field> fields)
	{
		this.fields = fields;
	}

	public List<String> getPopUpFields()
	{
		return popUpFields;
	}

	public void setPopUpFields(List<String> popUpFields)
	{
		this.popUpFields = popUpFields;
	}

	public List<String> getPromptFields()
	{
		return promptFields;
	}

	public void setPromptFields(List<String> promptFields)
	{
		this.promptFields = promptFields;
	}

	public List<String> getSearchFields()
	{
		return searchFields;
	}

	public void setSearchFields(List<String> searchFields)
	{
		this.searchFields = searchFields;
	}

	public List<String> getVisibleFields()
	{
		return visibleFields;
	}

	public void setVisibleFields(List<String> visibleFields)
	{
		this.visibleFields = visibleFields;
	}

	public List<String> getNotNullableFields() {
		return notNullableFields;
	}

	public void setNotNullableFields(List<String> notNullableFields) {
		this.notNullableFields = notNullableFields;
	}

	public boolean isSenderField()
	{
		return type == PersonType.SENDER;
	}

	public boolean isRecipientField()
	{
		return type == PersonType.RECIPIENT;
	}

	public String getLogic()
	{
		return logic;
	}

	public List<String> getPopUpButtons()
	{
		return popUpButtons;
	}

	public void setPopUpButtons(List<String> popUpButtons)
	{
		this.popUpButtons = popUpButtons;
	}

	public List<String> getDicButtons()
	{
		return dicButtons;
	}

	public void setDicButtons(List<String> dicButtons)
	{
		this.dicButtons = dicButtons;
	}

	@Override
	public void addSearchPredicate(DockindQuery dockindQuery,
			Map<String, Object> values, Map<String, Object> fieldValues) {
		// TODO Auto-generated method stub
		
	}

}
