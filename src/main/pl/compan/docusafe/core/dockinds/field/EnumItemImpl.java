package pl.compan.docusafe.core.dockinds.field;

import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;

public class EnumItemImpl implements EnumItem
{
    private Integer id;
    private String cn;
    private Map<String,String> titles;
    private String[] args;
    /**
     * pomocniczy atrybut (niewykorzystywany podczas wczytywania danych z XMLa) - u�ywany w pewnych
     * sytuacjach, mo�e by� wykorzystany w dowolny spos�b przez u�ytkownika
     */
    private Object tmp;
    private boolean forceRemark;
    /**Pole dla DataBaseEnumField oznaczaj�ce przy jakiej warto�ci pola od kt�rego zale�y enum bedzie wy�wietlana */
    private String refValue;
    /**Pole wi���ce enuma z centrum*/
    private Integer centrum;
    
    private String fieldCn;

    private boolean available = true;

	public EnumItemImpl(Integer id, String cn, Map<String,String> titles, String[] args, boolean forceRemark,String refValue,Integer centrum,String fieldCn)
    {
    	this.id = id;
        this.cn = cn;
        this.titles = titles;
        this.args = args;
        this.forceRemark = forceRemark;
        this.refValue = refValue;
        this.centrum = centrum;
        this.fieldCn = fieldCn;
    }

    public EnumItemImpl(Integer id, String cn, Map<String,String> titles, String[] args, boolean forceRemark,String fieldCn)
    {
        this.id = id;
        this.cn = cn;
        this.titles = titles;
        this.args = args;
        this.forceRemark = forceRemark;
        this.fieldCn = fieldCn;
    }
    
    public EnumItemImpl(Integer id, String cn, Map<String,String> titles, String[] args, boolean forceRemark,String fieldCn,boolean available)
    {
        this.id = id;
        this.cn = cn;
        this.titles = titles;
        this.args = args;
        this.forceRemark = forceRemark;
        this.fieldCn = fieldCn;
        this.available = available;
    }

    public boolean isAvailable() {
        return available;
    }

    public Integer getId()
    {
        return id;
    }
    
    public String getCn()
    {
        return cn;
    }

    public String getTitle()
    {
        return Docusafe.getLanguageValue(titles);  
    }

    public boolean isForceRemark()
    {
        return forceRemark;
    }

    public Object getTmp()
    {
        return tmp;
    }

    public void setTmp(Object tmp)
    {
        this.tmp = tmp;
    }

    /**
     * Zwraca warto�� argumentu zwi�zanego z warto�ci�.
     * @param position Pozycja argumentu liczona od 1.
     * @return Warto�� argumentu lub null.
     */
    public String getArg(int position)
    {
        if (position > 0 && (position-1) < args.length)
            return args[position-1];
        return null;
    }

	public String getRefValue() {
		return refValue;
	}

	public Integer getCentrum() {
		return centrum;
	}
    
    public String getFieldCn() {
		return fieldCn;
	}

	public void setFieldCn(String fieldCn) {
		this.fieldCn = fieldCn;
	}

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
