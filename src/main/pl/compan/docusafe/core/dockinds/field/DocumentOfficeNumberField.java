package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.primitives.Ints;

public class DocumentOfficeNumberField extends NonColumnField implements CustomPersistence {

	protected static Logger log = LoggerFactory.getLogger(DocumentOfficeNumberField.class);
	
	public DocumentOfficeNumberField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
		super(id, cn, names, Field.DOCUMENT_OFFICENUMBER, null, null, required, requiredWhen, availableWhen);
		this.setSearchShow(false);
		this.setDisabled(true);
	}
	
	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId,false);
		if (doc instanceof OutOfficeDocument) 
			return new FieldValue(this, ((OutOfficeDocument) doc).getOfficeNumber());
		else if (doc instanceof InOfficeDocument) 
			return new FieldValue(this, ((InOfficeDocument) doc).getOfficeNumber());
		else return null;
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        if(value == null || value.toString().isEmpty()){
            return null;
        }

        if(value instanceof Integer) {
            return (Integer) value;
        }
		
		if (Ints.tryParse(value.toString()) != null) {
	        return Ints.tryParse(value.toString());
		}
		
		throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + " cannot be converted to Integer by DocumentOfficeNumberField");
	}

	public void persist(Document doc, Object key) throws EdmException {
		//throw new EdmException("Nie mo�na zmieni� nr KO");
		log.error("Cant change office number. This field should be disabled");
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
		return  new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER);
	}
}
