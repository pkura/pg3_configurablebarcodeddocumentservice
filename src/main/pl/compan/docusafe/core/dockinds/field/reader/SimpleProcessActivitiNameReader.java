package pl.compan.docusafe.core.dockinds.field.reader;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.SimpleProcessActivityNameField;

import java.util.List;
import java.util.Map;

/**
 * User: Tomasz
 * Date: 16.04.14
 * Time: 12:14
 */
public class SimpleProcessActivitiNameReader implements FieldReader {

    @Override
    public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Field.Condition> availableWhen, List<Field.Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {
        return new SimpleProcessActivityNameField(el.attributeValue("id"),
                elType.attributeValue("type"),
                el.attributeValue("cn"),
                names,
                required,
                requiredWhen,
                availableWhen);
    }
}
