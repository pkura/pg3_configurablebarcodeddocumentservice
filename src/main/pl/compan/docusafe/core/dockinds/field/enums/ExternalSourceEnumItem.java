package pl.compan.docusafe.core.dockinds.field.enums;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by kk on 21.02.14.
 */
public class ExternalSourceEnumItem implements EnumItem {
    private static final Logger log = LoggerFactory.getLogger(ExternalSourceEnumItem.class);

    public static final char SPLIT_CHAR = '#';

    public static final Set<Character> DENIED_CHARS = ImmutableSet.of(SPLIT_CHAR, '\"');

    private Integer id;
    private String cn;
    private String title;
    private List<String> args;

    public static EnumItem makeEnumItem(String rawData) {
        if (Strings.isNullOrEmpty(rawData)) {
            return new ExternalSourceEnumItem(null, null, null);
        } else {
            List<String> params = new ArrayList<String>(Splitter.on(SPLIT_CHAR).splitToList(rawData)) {
                @Override
                public String get(int index) {
                    return this.size() > index ? super.get(index) : null;
                }
            };
            Preconditions.checkState(params.size() >= ParamsInRawData.NUMBER_OF_OBLIGATORY_PARAMS);
            return new ExternalSourceEnumItem(Ints.tryParse(params.get(ParamsInRawData.EID.getIndex())),
                    params.get(ParamsInRawData.ECN.getIndex()),
                    params.get(ParamsInRawData.TITLE.getIndex()))
                    .addArg(params.get(ParamsInRawData.ARG1.getIndex()));
        }
    }

    public static String makeRawData(EnumItem item) {
        List<Object> raw = Lists.newArrayList();

        for (ParamsInRawData param : ParamsInRawData.values()) {
            raw.add(param.getIndex(), param.getParamValue(item));
        }

        return Joiner.on(SPLIT_CHAR).useForNull("").join(raw);
    }

    public static String checkValidate(Object value) throws FieldValueCoerceException {
        Preconditions.checkNotNull(value);
        Preconditions.checkArgument(value.toString().split(""+SPLIT_CHAR).length >= ParamsInRawData.NUMBER_OF_OBLIGATORY_PARAMS);
        return value.toString();
    }

    public ExternalSourceEnumItem(Integer id, String cn, String title) {
        this.id = id;
        this.cn = validateString(cn);
        this.title = validateString(title);
    }

    private String validateString(String key) {
        if (key == null) {
            return null;
        }

        for (Character deniedChar : DENIED_CHARS) {
            if (key.indexOf(deniedChar) != -1) {
                log.error("ExternalSourceEnumItem data contains denied char {} on: {}, id: {}", deniedChar, key, this.id);
                key = key.replace(deniedChar, '*');
            }
        }

        return key;
    }

    public ExternalSourceEnumItem addArg(String arg) {
        if (args == null) {
            args = Lists.newArrayListWithExpectedSize(1);
        }
        args.add(validateString(arg));
        return this;
    }

    public Integer getId() {
        return id;
    }

    public String getCn() {
        return cn;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public Object getTmp() {
        return null;
    }

    @Override
    public void setTmp(Object tmp) {

    }

    @Override
    public String getRefValue() {
        return null;
    }

    @Override
    public Integer getCentrum() {
        return null;
    }

    @Override
    public boolean isForceRemark() {
        return false;
    }

    @Override
    public String getFieldCn() {
        return null;
    }

    @Override
    public boolean isAvailable() {
        return true;
    }

    @Override
    public String getArg(int position) {
        if (args != null && position > 0)
            return args.get(position - 1);
        return null;
    }

    enum ParamsInRawData {

        EID(0), ECN(1), TITLE(2), ARG1(3);

        public static final int NUMBER_OF_OBLIGATORY_PARAMS = 3;
        int index;

        ParamsInRawData(int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }

        public Object getParamValue(EnumItem item) {
            switch (this) {
                case EID: return item.getId();
                case ECN: return item.getCn();
                case TITLE: return item.getTitle();
                case ARG1: return item.getArg(1);
            }
            throw new UnsupportedOperationException();
        }

    }
}