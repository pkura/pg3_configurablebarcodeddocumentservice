package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.Database;
import pl.compan.docusafe.core.dockinds.Database.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class OrderedNonColumnField extends NonColumnField implements OrderPredicateProvider {

    private final Order[] orders;
    private final boolean isManualSorted;

    /**
     * @param orders Uwaga - dwa obiekty nie mog� zawiera� takiego samego elementu w fieldCns i documentKinds
     * @param isManualSorted Okre�la czy pole jest sortwane wed�ug widocznych warto�ci pola. Warto�ci takie mog� by� wynikiem z��czenia wielu kolumn.
     */
    private OrderedNonColumnField(Order[] orders, boolean isManualSorted, String id, String cn, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, names, type, refStyle, classname, required, requiredWhen, availableWhen);
        this.isManualSorted = isManualSorted;
        this.orders = orders;
    }

    /**
     * Sortwanie wed�ug warto�ci okre�lonej kolumny. Kolumna znajduje si� w tabeli po��czonej (bez/-po�rednio) z tabel� ds_document, co okre�lone
     * jest w <i>orders</i>
     * @param orders Uwaga - dwa obiekty nie mog� zawiera� takiego samego elementu w fieldCns i documentKinds
     */
    public OrderedNonColumnField(Order[] orders, String id, String cn, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        this(orders, false, id, cn, names, type, refStyle, classname, required, requiredWhen, availableWhen);
    }

    /**
     * Sortwanie wed�ug widocznych warto�ci pola. Warto�ci takie mog� by� wynikiem z��czenia wielu kolumn.
     */
    public OrderedNonColumnField(String id, String cn, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        this(null, true, id, cn, names, type, refStyle, classname, required, requiredWhen, availableWhen);
    }

    /**
     * Sortwanie wed�ug warto�ci okre�lonej kolumny. Kolumna znajduje si� bezpo�rednio w tabeli danego rodzaju dokumentu.
     */
    public OrderedNonColumnField(Database.Column orderedColumn, String id, String cn, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(orderedColumn, id, cn, names, type, refStyle, classname, required, requiredWhen, availableWhen);
        orders = null;
        isManualSorted = false;
    }

    @Override
    public boolean isManualSorted() {
        return this.isManualSorted;
    }

    @Override
    public Order getOrder(Table documentTable, String fieldCn) {
        if (this.orders == null)
            return null;
        List<Order> machingOrders = new ArrayList<Order>();
        for (Order o : this.orders)
            if (o.isMaching(documentTable, fieldCn))
                machingOrders.add(o);
        if (machingOrders.size() == 1)
            return machingOrders.get(0);
        return null;
    }
}
