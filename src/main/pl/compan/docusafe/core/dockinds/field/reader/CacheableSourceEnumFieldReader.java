package pl.compan.docusafe.core.dockinds.field.reader;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.CacheableSourceEnumField;
import pl.compan.docusafe.core.dockinds.field.Field;

import java.util.List;
import java.util.Map;

/**
 * Created by kk on 21.02.14.
 */
public class CacheableSourceEnumFieldReader implements FieldReader {

    @Override
    public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Field.Condition> availableWhen, List<Field.Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {
        return new CacheableSourceEnumField(el.attributeValue("id"), el.attributeValue("cn"), el.attributeValue("column"), names, "true".equalsIgnoreCase(elType.attributeValue("auto")), Field.CACHEABLE_SOURCE_ENUM, elType.attributeValue("logic"), elType.attributeValue("ref-field"), "", null, required,  requiredWhen, availableWhen);
    }
}
