package pl.compan.docusafe.core.dockinds.field.reader;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.MailWeightField;

import java.util.List;
import java.util.Map;

/**
 * MailWeightReader
 *
 * @version $Id$
 */
public class MailWeightReader implements FieldReader {

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Field.Condition> availableWhen, List<Field.Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {
		String cn = el.attributeValue("cn");
		return new MailWeightField(el.attributeValue("id"), cn, names, required, requiredWhen, availableWhen);
	}
}
