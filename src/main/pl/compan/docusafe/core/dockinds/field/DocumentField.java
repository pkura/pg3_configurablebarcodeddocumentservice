package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.map.LinkedMap;
import org.dom4j.Element;

import com.google.common.collect.Maps;

public class DocumentField extends AbstractDictionaryField implements SearchPredicateProvider
{
	private final static Logger log = LoggerFactory.getLogger(DocumentField.class);

	public DocumentField(String id, String cn, String column, String logic, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String tableName, String resourceName, DocumentKind documentKind) throws EdmException
	{
		super(id, cn, column, logic, names, DOCUMENT_FIELD, null, null, required, requiredWhen, availableWhen, tableName, resourceName, null, documentKind);
	}

	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues)
	{
		log.info("addSearchPredicate map = '{}', value = '{}'", values, values.get(getCn()));
		Map<String, Object> dictionaryFieldsValues = Maps.newHashMap();
		for (Field field : getDockindSearchFields())
		{
			String key = getCn() + "_" + field.getCn();
			if (values.get(key) != null)
			{
				Object a = values.get(key);
				dictionaryFieldsValues.put(field.getCn(), values.get(key));
			}
		}
		if (!dictionaryFieldsValues.isEmpty())
			dockindQuery.documentField(this, dictionaryFieldsValues);
	}

}
