package pl.compan.docusafe.core.dockinds.field;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Popup;
import pl.compan.docusafe.core.dockinds.field.reader.*;
import pl.compan.docusafe.util.*;

import java.util.*;

/**
 * Klasa narz�dziowa do wczytywania p�l z xml dockindowego
 * 
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class FieldsXmlLoader
{
	private static final Logger log = LoggerFactory.getLogger(FieldsXmlLoader.class);
    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);
	/**
	 * strategie do wczytywania poszczegolnych pol z xml
	 */
	private static final Map<String, FieldReader> fieldReaderHandlers = new HashMap<String, FieldReader>();
	static
	{
		fieldReaderHandlers.put(Field.ENUM, new EnumFieldReader());
		fieldReaderHandlers.put(Field.BOOL, new BooleanFieldReader());
		fieldReaderHandlers.put(Field.MONEY, new MoneyFieldReader());
		fieldReaderHandlers.put(Field.LONG, new LongFieldReader());
		fieldReaderHandlers.put(Field.STRING, new StringFieldReader());
		fieldReaderHandlers.put(Field.LINK, new LinkFieldReader());
		fieldReaderHandlers.put(Field.ATTACHMENT, new AttachmentFieldReader());
		fieldReaderHandlers.put(Field.HTML, new HTMLFieldReader());
		fieldReaderHandlers.put(Field.HTML_EDITOR, new HtmlEditorFieldReader());
		fieldReaderHandlers.put(Field.BUTTON, new ButtonFieldReader());
		fieldReaderHandlers.put(Field.CENTRUM_KOSZT, new CostCenterFieldReader());
		fieldReaderHandlers.put(Field.DOUBLE, new DoubleFieldReader());
		fieldReaderHandlers.put(Field.FLOAT, new FloatFieldReader());
		fieldReaderHandlers.put(Field.DSUSER, new DSUserFieldReader());
		fieldReaderHandlers.put(Field.DSDIVISION, new DSDivisionFieldReader());
		fieldReaderHandlers.put(Field.ENUM_REF, new EnumRefFieldReader());
		fieldReaderHandlers.put(Field.DATA_BASE, new DataBaseFieldReader());
		fieldReaderHandlers.put(Field.LIST_VALUE, new ListValueFieldReader());
		fieldReaderHandlers.put(Field.DOCLIST, new DocListFieldReader());
		fieldReaderHandlers.put(Field.DOCKIND_BUTTON, new DockindButtonFieldReader());
		fieldReaderHandlers.put(Field.DROOLS_BUTTON, new DroolsButtonFieldReader());
		fieldReaderHandlers.put(Field.INTEGER, new IntegerFieldReader());
		fieldReaderHandlers.put(Field.DATE, new DateFieldReader());
		fieldReaderHandlers.put(Field.TIMESTAMP, new TimestampFieldReader());
		fieldReaderHandlers.put(Field.DICTIONARY, new DictionaryFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_FIELD, new DocumentFieldFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_PERSON, new DocumentPersonFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_USER_DIVISION, new DocumentUserDivisionFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_USER_DIVISION_SECOND, new DocumentUserDivisionSecondFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_POSTAL_REG_NR, new DocumentPostalRegNrFieldReader());
        fieldReaderHandlers.put(Field.DOCUMENT_BARCODE, new DocumentBarCodeFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_OFFICENUMBER, new DocumentOfficeNumberFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_DATE, new DocumentDateFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_DESCRIPTION, new DocumentDescriptionFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_JOURNAL, new DocumentJournalFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_OFFICE_DELIVERY, new DocumentOfficeDeliveryFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_IN_OFFICE_KIND, new DocumentInOfficeKindFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_IN_INVOICE_KIND, new DocumentInvoicesKindFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_IN_OFFICE_STATUS, new DocumentInOfficeStatusFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_OFFICE_CASE_SYMBOL, new DocumentOfficeCaseSymbolFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_IN_STAMP_DATE, new DocumentInStampDateFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_REFERENCE_ID, new DocumentReferenceIdFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_IN_SUBMIT_TO_BIP, new DocumentInSubmitToBipFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_IN_LOCATION, new DocumentInLocationFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_IN_ORIGINAL, new DocumentInOriginalFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_OUT_PRIORITY, new DocumentOutPriorityFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_OTHERREMARKS, new DocumentOtherRemarksFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_PACKAGEREMARKS, new DocumentPackageRemarksFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_OUT_OFFICE_PREPSTATE, new DocumentOutOfficePrepStateFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_OFFICE_CLERK, new DocumentOfficeClerkFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_AUTOR, new DocumentAutorFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_AUTOR_DIVISION, new DocumentAutorDivisionFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_ABSTRAKT_DESCRIPTION, new DocumentAbstractDescriptionFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_IN_TRACKING_NUMBER, new DocumentInTrackingNumberFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_IN_TRACKING_PASSWORD, new DocumentInTrackingPasswordFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_PARAMS, new DocumentParamsFieldReader());
		fieldReaderHandlers.put(Field.CLASS, new ClassFieldReader());
		fieldReaderHandlers.put(Field.FULL_TEXT_SEARCH, new FullTextSearchFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_OUT_ZPODATE, new DocumentOutZPODateReader());
		fieldReaderHandlers.put(Field.DOCUMENT_OUT_ADDITIONAL_ZPO, new DocumentOutAdditionalZPOReader());
		fieldReaderHandlers.put(Field.DOCUMENT_OUT_ZPO, new DocumentOutZPOReader());
		fieldReaderHandlers.put(Field.EMAIL_MESSAGE, new EmailMessageFieldReader());
		fieldReaderHandlers.put(Field.DOCUMENT_OUT_GAUGE, new DocumentOutGaugeReader());
		fieldReaderHandlers.put(Field.USER_DIVISION, new UserDivisionReader());
		fieldReaderHandlers.put(Field.ACCESS_TO_DOCUMENT, new AccessToDocumentFieldReader());
		fieldReaderHandlers.put(Field.MAIL_WEIGHT, new MailWeightReader());
		fieldReaderHandlers.put(Field.CURRENCY, new CurrencyReader());
		fieldReaderHandlers.put(Field.CACHEABLE_SOURCE_ENUM, new CacheableSourceEnumFieldReader());
        fieldReaderHandlers.put(Field.SIMPLE_PROCESS_VARIABLE, new SimpleProcessVariableReader());
        fieldReaderHandlers.put(Field.SIMPLE_PROCESS_ACTIVITY_NAME, new SimpleProcessActivitiNameReader());
	}

	private FieldsXmlLoader()
	{
	}

	public static void readFields(Element elFields, DockindInfoBean di) throws EdmException, NumberFormatException {
		readFields(elFields, di, null);
	}

	public static void readFields(Element elFields, DockindInfoBean di, String dockindCn) throws EdmException, NumberFormatException
	{
		if (elFields != null)
		{
			log.info(elFields.asXML());
			Element elMainField = elFields.element("main-field");
			if (elMainField != null)
			{
				di.setMainFieldCn(elMainField.attributeValue("cn"));
			}
			List fieldTags = elFields.elements("field");

			if (fieldTags != null)
			{
				List<Field> fields = new ArrayList<Field>(fieldTags.size());
				Map<String, Field> fieldsMap = new HashMap<String, Field>();
                boolean autofocusAlredySet = false;
				for (Iterator<Element> iter = fieldTags.iterator(); iter.hasNext();)
				{
					Element el = iter.next();
					// Czy pole jest dostepne
					boolean isEnabled = true;
					/**
					 * TODO Trzeba to wyrzuci�, gdy� pole ma ju� w�asno��
					 * DISABLED
					 */
					if (el.attributeValue("enabled") != null)
					{
						isEnabled = "true".equals(el.attributeValue("enabled"));
					}
					
					Element elType = el.element("type");
					String type = elType.attributeValue("name");
					System.out.println("WCZYTUJE POLE = "+ el.attributeValue("name")+" typu "+elType.attributeValue("name"));
					String refStyle = elType.attributeValue("ref-style");
                    if (refStyle == null)
                    {
                        refStyle = Field.STYLE_NORMAL;
                    }
					boolean required = "true".equals(el.attributeValue("required"));
					Element elAvailable = el.element("available");
					List<Field.Condition> availableWhen = elAvailable != null ? readWhenAvailable(elAvailable) : null;
					Element elRequired = el.element("required");
					List<Field.Condition> requiredWhen = elRequired != null ? readWhenRequired(elRequired) : null;
					// wczytywanie nazwy pola w r�znych j�zykach
					Map<String, String> names = new LinkedHashMap<String, String>();
					for (Locale locale : LocaleUtil.AVAILABLE_LOCALES)
					{
						names.put(locale.getLanguage(), "pl".equals(locale.getLanguage()) ? el.attributeValue("name") : el.attributeValue("name_" + locale.getLanguage()));
					}
					FieldFormatting formatting = FieldFormatting.parse(el.element("formatting"));
					Field field = null;

					if (fieldReaderHandlers.containsKey(type))
					{
						log.trace("Wczytuje +"+el.attributeValue("cn"));
						field = fieldReaderHandlers.get(type).read(di, el, elType, refStyle, required, availableWhen, requiredWhen, names, !isEnabled);
						if (Field.DATA_BASE.equals(type))
						{
							if (formatting == null)
							{
								formatting = new FieldFormatting(FieldFormatter.TITLE);
							}
						}
					}
					else
					{
						throw new RuntimeException("Nieznany typ pola:" + type);
					}

					field.setDockindCn(dockindCn);

                    setBooleanParameters(el, elType, field);

                    // domy�lne formatowanie
                    if (formatting == null)
                    {
                        formatting = new FieldFormatting(FieldFormatter.TO_STRING);
                    }
                    field.setFormatting(formatting);
                    if (el.element("default-value") != null)
                    {
                        field.setDefaultValue(el.element("default-value").getTextTrim());
                    }
                    if (elType.attributeValue("dwr-fieldset") != null)
                    {
                        field.setDwrFieldset(elType.attributeValue("dwr-fieldset"));
                    }
                    if (elType.attributeValue("dwr-column") != null)
                    {
                        field.setDwrColumn(Integer.valueOf(elType.attributeValue("dwr-column")));
                    }
					if (elType.attributeValue("autocompleteRegExp") != null)
					{
						field.setAutocompleteRegExp(elType.attributeValue("autocompleteRegExp"));
					}
					if (elType.attribute("match") != null && (field instanceof StringField || Field.CLASS.equals(type)))
					{
						field.setMatch(elType.attributeValue("match"));
					}
					if (el.attribute("newItemMessage") != null)
					{
						field.setNewItemMessage(el.attributeValue("newItemMessage"));
					}
					if (el.attribute("cssClass") != null)
					{
						field.setCssClass(el.attributeValue("cssClass"));
					}
					if (el.attribute("format") != null)
					{
						field.setFormat(el.attributeValue("format"));
					}
					if (el.attributeValue("coords") != null)
					{
						field.setCoords(el.attributeValue("coords").toLowerCase());
					}
					if (elType.attribute("infoBox") != null)
					{
						field.setInfoBox(elType.attributeValue("infoBox"));
					}
					if (el.attribute("onchange") != null)
					{
						field.setOnchange(el.attributeValue("onchange"));
					}
					if (el.attributeValue("newDocumentShow") != null)
                    {
						field.setNewDocumentShow("true".equals(el.attributeValue("newDocumentShow")));
                    }
					else
                    {
						field.setNewDocumentShow(true);
                    }

					if (field instanceof ArchiveListener)
					{
						di.addArchiveListener((ArchiveListener) field);
					}
					if (el.attribute("listenersList") != null)
					{
						field.setListenersList(Arrays.asList(StringUtils.stripAll(StringUtils.split(el.attributeValue("listenersList"), ","))));
					}
                    if(el.attributeValue("sortable") != null) {
                        Field.Sortable sortable = Field.Sortable.fromString(el.attributeValue("sortable").trim());
                        field.setSortable(sortable);
                    }
					
					if (elType.attribute("full-text-search") != null && "true".equals(elType.attribute("full-text-search").getText()) && type.equals("string"))
					{
						String tableName = null;
						if (field.getDockindCn() != null)
						{
							DocumentKind dockind = DocumentKind.findByCn(field.getDockindCn());
							tableName = dockind.getTablename();
						}
						else
						{
							//TODO s�abe rozwi�zanie, ale dzia�a. Mozna poprawic, je�li kto� ma inny pomys�� jak ze s�ownika wyci�� nazwe tabeli
							tableName = StringUtils.substringBetween(elFields.asXML(),"tableName=\"", "\"");
						}
						if (Field.fullTextSearchEnabledOnColumn(tableName, field))
							field.setFullTextSearch("true".equals(elType.attribute("full-text-search").getText()));
						else
							throw new EdmException("Pole " + field.getName() + " nie posiada wyszukiwania poprzez full-text-search");
					}

                    field.setAllowNull(Boolean.valueOf(ObjectUtils.toString(elType.attributeValue("allowNull"), "")));

                    // uprawnienia do odczytu
					Element elEditPerm = el.element("can-edit");
					List<String> editPermissions = elEditPerm != null ? readEditPermissions(elEditPerm) : null;
					field.setEditPermissions(editPermissions);
					field.checkPermissions();
					field.initField();
					fields.add(field);
					fieldsMap.put(field.getCn(), field);

                    if (field.isAutofocus())
                    {
                        if (autofocusAlredySet)
                            throw new EdmException(sm.getString("TylkoJednoPoleMozeMiecUstawionyAutofoxusTrue"));
                        else
                            autofocusAlredySet = true;
                    }
				}
				di.setDockindFields(fields);
				di.setDockindFieldsMap(fieldsMap);
			}
		}
	}

    private static void setBooleanParameters(Element el, Element elType, Field field)
    {
        field.setKeyMember("true".equals(el.attributeValue("keyMember")));
        field.setAutofocus("true".equals(el.attributeValue("autofocus")));
        field.setAuto("true".equals(elType.attributeValue("auto")));
        field.setHidden("true".equals(el.attributeValue("hidden")));
        field.setHiddenOnDisabledEmpty("true".equals(el.attributeValue("hiddenOnDisabledEmpty")));
        field.setLazyEnumLoad("true".equals(el.attributeValue("lazyEnumLoad")));
        field.setSaveHidden("true".equals(el.attributeValue("saveHidden")));
        field.setDisabled("true".equals(el.attributeValue("disabled")));
        field.setCantUpdate("true".equals(el.attributeValue("cantUpdate")));
        field.setReadOnly("true".equals(el.attributeValue("readonly")));
        field.setOnlyPopUpCreate("true".equals(el.attributeValue("onlyPopUpCreate")));
        field.setAsText("true".equals(el.attributeValue("asText")));
        field.setMultiple("true".equals(elType.attributeValue("multiple")));
        field.setAsTable("true".equals(elType.attributeValue("asTable")));
        field.setSearchByRange("true".equals(elType.attributeValue("searchByRange")));
        field.setSearchShow("true".equals(el.attributeValue("searchShow")));
        field.setSearchHidden("true".equals(el.attributeValue("search-hidden")));
        field.setSaveInTheOrder("true".equals(elType.attributeValue("saveInTheOrder")));
        if (el.attributeValue("submit") != null)
        {
            field.setSubmit("true".equals(el.attributeValue("submit")));
        }
        field.setSpecific("true".equals(el.attributeValue("specific")));
        field.setSubmitForm("true".equals(el.attributeValue("submitForm")));
        field.setValidate("true".equals(el.attributeValue("validate")));
    }

    /**
	 * Czyta z dokumentu DOM uprawnienia do edycji danego pola. Zak�adam, �e
	 * element != null.
	 * 
	 * @param element
	 *             w�ze� zawieraj�cy dane o widoczno��i
	 */
	public static List<String> readEditPermissions(Element element)
	{
		List permList = element.elements("division");
		List<String> editPermissions = new ArrayList<String>();
		for (Iterator permIter = permList.iterator(); permIter.hasNext();)
		{
			Element elPerm = (Element) permIter.next();
			String guid = TextUtils.trimmedStringOrNull(elPerm.attributeValue("guid"));
			if (guid != null)
				editPermissions.add(guid);
		}
		return editPermissions;
	}

	/**
	 * Czyta z dokumentu DOM dane o widoczno�ci aktualnie przetwarzanego pola.
	 * Zak�adam, �e elAvailable != null - optymista ;-).
	 * 
	 * @param elAvailable
	 *             element zawieraj�cy dane o widoczno�ci
	 */
	public static List<Field.Condition> readWhenAvailable(Element elAvailable)
	{
		List whenList = elAvailable.elements("when");
		List<Field.Condition> availableWhen = new ArrayList<Field.Condition>();
		for (Iterator whenIter = whenList.iterator(); whenIter.hasNext();)
		{
			Element elWhen = (Element) whenIter.next();
			Field.Condition condition = new Field.Condition();
			String main = TextUtils.trimmedStringOrNull(elWhen.attributeValue("main-field"));
			if (main != null)
			{
				condition.setType(Field.Condition.TYPE_MAIN);
				condition.setFieldValue(new Long(main));
			}
			else
			{
				String fieldCn = TextUtils.trimmedStringOrNull(elWhen.attributeValue("cn"));
				String fieldValue = TextUtils.trimmedStringOrNull(elWhen.attributeValue("value"));
				if (fieldCn == null || fieldValue == null)
					// jak niepe�ne dane to omijam ten warunek
					continue;
				condition.setType(Field.Condition.TYPE_OTHER);
				condition.setFieldCn(fieldCn);
				condition.setFieldValue(new Long(fieldValue));
			}
			availableWhen.add(condition);
		}
		return availableWhen;
	}

	/**
	 * Czyta z dokumentu DOM dane o obligatoryjno�ci aktualnie przetwarzanego
	 * pola. Zak�adam, �e elRequired != null.
	 * 
	 * @param elRequired
	 *             element zawieraj�cy dane o obligatoryjno�ci
	 */
	public static List<Field.Condition> readWhenRequired(Element elRequired)
	{
		List whenList = elRequired.elements("when");
		List<Field.Condition> requiredWhen = new ArrayList<Field.Condition>();
		for (Iterator whenIter = whenList.iterator(); whenIter.hasNext();)
		{
			Element elWhen = (Element) whenIter.next();
			Field.Condition condition = new Field.Condition();
			String main = TextUtils.trimmedStringOrNull(elWhen.attributeValue("main-field"));
			if (main != null)
			{
				condition.setType(Field.Condition.TYPE_MAIN);
				condition.setFieldValue(new Long(main));
			}
			else
			{
				String fieldCn = TextUtils.trimmedStringOrNull(elWhen.attributeValue("cn"));
				String fieldValue = TextUtils.trimmedStringOrNull(elWhen.attributeValue("value"));
				if (fieldCn == null || fieldValue == null)
					// jak niepe�ne dane to omijam ten warunek
					continue;
				condition.setType(Field.Condition.TYPE_OTHER);
				condition.setFieldCn(fieldCn);
				condition.setFieldValue(new Long(fieldValue));
			}
			requiredWhen.add(condition);
		}
		return requiredWhen;
	}

    /**
     * Wczytuje popupy
     * @param popups
     */
    public static void readPopups(Element popups, DockindInfoBean di) {
        if(popups != null) {
            Map<String, Field> fields = di.getDockindFieldsMap();
            List<Element> popupElements = popups.elements("popup");
            for (Element popup : popupElements) {
                String popupCn = popup.attributeValue("cn");
                String popupCaption = popup.attributeValue("caption");
                boolean popupCloseable = Boolean.parseBoolean(popup.attributeValue("closeable"));
                String popupCloserefcn = popup.attributeValue("closerefcn");
                Element fieldsRef = popup.element("fieldsref");
                List<Element> refs = fieldsRef.elements("fieldref");
                for(Element ref : refs) {
                    String fieldCn = ref.attributeValue("cn");
                    Field field = fields.get(fieldCn);

                    Popup pop;
                    if(field.getPopups().containsKey(popupCn)) {
                        pop = field.getPopups().get(popupCn);
                    } else {
                        pop = new Popup();
                        field.getPopups().put(popupCn, pop);
                    }
                    pop.setPopupcn(popupCn);
                    pop.setCaption(popupCaption);
                    pop.setCloseable(popupCloseable);
                    if(popupCloserefcn != null)
                        pop.setCloserefcn(DwrUtils.getCnWithPrefix(popupCloserefcn));
                }
            }
        }
    }
}
