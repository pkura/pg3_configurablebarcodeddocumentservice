package pl.compan.docusafe.core.dockinds.field;

import static pl.compan.docusafe.util.I18nUtils.string;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.core.office.AccessToDocument;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AccessToDocumentField extends NonColumnField implements CustomPersistence, EnumNonColumnField, SearchPredicateProvider
{

	private static final Logger log = LoggerFactory.getLogger(AccessToDocumentField.class);
	private String type = "in";
	private List<EnumItem> enumItems = new ArrayList<EnumItem>();
	
	public AccessToDocumentField(String id, String type, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, names, Field.ACCESS_TO_DOCUMENT, null, null, required, requiredWhen, availableWhen);
		if (type != null)
		{
			this.type = type;
		}

		Map<String,String> titles = new HashMap<String,String>();
		
		for (AccessToDocument access : AccessToDocument.list())
		{
			
			titles = new HashMap<String,String>();
			titles.put("pl", access.getName());
			enumItems.add(new EnumItemImpl(access.getId(), "CN_"+access.getId(), titles, new String[3], false,this.getCn()));
		}
	}

	@Override
	public void persist(Document doc, Object key) throws EdmException
	{
		Integer accessId = null;
		if(key == null) return;
        	if (key instanceof String)
        		accessId = Integer.valueOf((String) key);
        	else
        		accessId = (Integer) key;
            if (accessId != null)
            {
        	   doc.setAccessToDocument(Long.valueOf(accessId));
            }
		
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		OfficeDocument doc = OfficeDocument.find(documentId);
		

		if (doc.getAccessToDocument() != null){
			AccessToDocument access = AccessToDocument.findById(doc.getAccessToDocument().intValue());
			return new FieldValue(this, access.getId(), access.getName());
		}else{
			return new FieldValue(this, null, null);
		}
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value == null || (value.toString().equals("")))
			return null;

		try
		{
			return Integer.parseInt(value.toString());
		}
		catch (NumberFormatException e)
		{
			throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci") + " " + value + " " + string("DoLiczbyCalkowitej"));
		}
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
	}
	
	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException
	{
		
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("", "--wybierz--");

		if (fieldsValues != null && fieldsValues.get(getCn()) != null)
			enumSelected.add(fieldsValues.get(getCn()).toString());
		else
			enumSelected.add("");

			for (AccessToDocument access : AccessToDocument.list())
				enumValues.put(access.getId().toString(), access.getName());
		
		return new EnumValues(enumValues, enumSelected);
	}

	public final List<EnumItem>  getAllEnumItemsByAspect(String documentAspectCn,DocumentKind kind)
	{
		
		return this.enumItems;
	}

	
	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) 
	{
				
	}
}
