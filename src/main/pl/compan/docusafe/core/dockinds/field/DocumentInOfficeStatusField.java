package pl.compan.docusafe.core.dockinds.field;

import static pl.compan.docusafe.util.I18nUtils.string;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DocumentInOfficeStatusField  extends NonColumnField implements CustomPersistence, EnumNonColumnField, SearchPredicateProvider
{

	private static final Logger log = LoggerFactory.getLogger(DocumentInOfficeStatusField.class);
	private String type = "in";
	private List<EnumItem> enumItems = new ArrayList<EnumItem>();
	
	public DocumentInOfficeStatusField(String id, String type, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, names, Field.DOCUMENT_IN_OFFICE_STATUS, null, null, required, requiredWhen, availableWhen);
		if (type != null)
		{
			this.type = type;
		}

		Map<String,String> titles = new HashMap<String,String>();
		for (InOfficeDocumentStatus kind : InOfficeDocumentStatus.list())
		{
			titles = new HashMap<String,String>();
			titles.put("pl", kind.getName());
			enumItems.add(new EnumItemImpl(kind.getId(), "CN_"+kind.getId(), titles, new String[3], false,this.getCn()));
		}
	}

	@Override
	public void persist(Document doc, Object key) throws EdmException
	{
		Integer statusId = null;
        	if (key instanceof String)
        		statusId = Integer.valueOf((String) key);
        	else
        		statusId = (Integer) key;
        	if (statusId != null)
            {
        	if (doc instanceof InOfficeDocument) 
        	    ((InOfficeDocument) doc).setStatus(InOfficeDocumentStatus.find(statusId));
        	else
        		throw new EdmException("STATUS -- RodzajDokumentuPrzychodzacegoMoznaDodacTylkoWpismiePrzychodzacym");
            }
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId,false);

		if (doc instanceof InOfficeDocument && ((InOfficeDocument)doc).getStatus() != null )
			return new FieldValue(this, ((InOfficeDocument)doc).getStatus().getId().toString(), ((InOfficeDocument)doc).getStatus().getName());
		else
			return new FieldValue(this, null, null);

	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value == null || (value.toString().equals("")))
			return null;
		if (value instanceof InOfficeDocumentKind)
			return ((InOfficeDocumentKind) value).getId();
		try
		{
			return Integer.parseInt(value.toString());
		}
		catch (NumberFormatException e)
		{
			throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci") + " " + value + " " + string("DoLiczbyCalkowitej"));
		}
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
	}
	
	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException
	{
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("", "--wybierz--");

		if (fieldsValues != null && fieldsValues.get(getCn()) != null)
			enumSelected.add(fieldsValues.get(getCn()).toString());
		else
			enumSelected.add("");

		for (InOfficeDocumentStatus status : InOfficeDocumentStatus.list())
			enumValues.put(status.getId().toString(), status.getName());
		
		return new EnumValues(enumValues, enumSelected);
	}

	public final List<EnumItem>  getAllEnumItemsByAspect(String documentAspectCn,DocumentKind kind)
	{
		
		return this.enumItems;
	}

	
	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) 
	{
		String  valS = (String) values.get(this.getCn());
		Integer valI = valS != null && valS.length() > 0 ? Integer.parseInt(valS) : null;
		log.debug(" {} {} ",valS,valI);
		dockindQuery.inOfficeStatus(valI);	
	}
	
}
