package pl.compan.docusafe.core.dockinds.field;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.AttachmentValue;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.MimetypesFileTypeMap;
import pl.compan.docusafe.web.viewserver.ViewServer;
import pl.compan.docusafe.webwork.FormFile;

/**
 * Pole reprezentuje za��cznik zapisywany w kolumnie dokumentu. <br />
 * 
 * @see AttachmentNonColumnField
 * @author Kamil
 * 
 */
public class AttachmentColumnField extends Field implements AttachmentField, CustomPersistence {
//	private Attachment attachment = null;
//	private AttachmentRevision mostRecentRevision = null;

	public AttachmentColumnField(String id, String cn, String column, Map<String, String> names, boolean required,
			List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
		super(id, cn, column, names, Field.ATTACHMENT, null, null, required, requiredWhen, availableWhen);
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		Object objAttId = rs.getObject(this.getColumn());
		FieldValue fieldValue = new FieldValue(this, null, null);
		
		// dokument ma za��cznik
		if(objAttId != null) {
			Long attId = ((BigDecimal) objAttId).longValue();
			fieldValue.setKey(attId);
			Attachment attachment = DSApi.getObject(Attachment.class, attId);
			fieldValue.setValue(attachment);
			attachment.getMostRecentRevision();
		}
		
		return fieldValue;
	}

	@Override
	public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
		Object coercedValue = this.simpleCoerce(value);
		if(coercedValue == null)
			ps.setObject(index, null);
		else if(coercedValue instanceof Long)
			ps.setLong(index, (Long)coercedValue);
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
		// Long lub String oznacza �e dostali�my z formatki attachmentId, 
		// czyli nie wgrywamy nowego za��cznika
		if (value instanceof Long) {
			return (Long)value;
		} else if (value instanceof String && !"".equalsIgnoreCase(value.toString())) {
			try {
				Long val = Long.parseLong((String)value);
				return val;
			} catch(NumberFormatException e) {
				throw new FieldValueCoerceException(e.getLocalizedMessage());
			}
		} else if (value instanceof File) {
			File file = (File) value;
			FormFile formFile = new FormFile(file, file.getName(), MimetypesFileTypeMap.getInstance().getContentType(file));
			return formFile;
		} else if (value instanceof AttachmentValue) {
			return ((AttachmentValue)value).getAttachmentId();
		} 
		return null;
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(),
				pl.compan.docusafe.core.dockinds.dwr.Field.Type.ATTACHMENT);
	}

	@Override
	public AttachmentValue getAttachmentForDwr(Long attachmentId) throws EdmException {
		AttachmentValue attVal = null;
        if(attachmentId == null)
            return attVal;
        Attachment value = DSApi.getObject(Attachment.class, attachmentId);
        AttachmentRevision mostRecentRevision = value.getMostRecentRevision();
        attVal = new AttachmentValue();
        attVal.setAttachmentId(value.getId());
        attVal.setTitle(value.getTitle());
        attVal.setUserSummary(DSUser.safeToFirstnameLastname(mostRecentRevision.getAuthor()));
        attVal.setCtime(mostRecentRevision.getCtime());
        attVal.setRevisionId(mostRecentRevision.getId());
        attVal.setRevisionMime(mostRecentRevision.getMime());
        attVal.setMimeAcceptable(ViewServer.mimeAcceptable(attVal.getRevisionMime()));
        attVal.setMimeXml(ViewServer.mimeXml(attVal.getRevisionMime()));

//		if(attachment != null && mostRecentRevision != null) {
//			attVal = new AttachmentValue();
//			attVal.setAttachmentId(attachment.getId());
//			attVal.setTitle(attachment.getTitle());
//			attVal.setUserSummary(DSUser.safeToFirstnameLastname(attachment.getAuthor()));
//			attVal.setCtime(attachment.getCtime());
//			attVal.setRevisionId(mostRecentRevision.getId());
//			attVal.setRevisionMime(mostRecentRevision.getMime());
//			attVal.setMimeAcceptable(ViewServer.mimeAcceptable(attVal.getRevisionMime()));
//			attVal.setMimeXml(ViewServer.mimeXml(attVal.getRevisionMime()));
//		}

		return attVal;
	}

	
	@Override
	public void persist(Document doc, Object key) throws EdmException {
		// nie ma za��cznika (lub dostali�my id za��cznika a nie plik) - nic nie robimy
		if(key == null || !(key instanceof AttachmentValue)) {
			return;
		}
		AttachmentValue attVal = (AttachmentValue) key;
		
		doc.setPermissionsOn(false);
		if(attVal.getAttachmentId() == null) {
			// tworzymy nowy za��cznik
			FormFile formFile = attVal.getFormFile();
			Attachment att = new Attachment(formFile.getNameWithoutExtension());
			doc.createAttachment(att);
			att.createRevision(formFile.getFile()).setOriginalFilename(formFile.getName());
			att.setEditable(false);
			
			try {
				StringBuilder sb = new StringBuilder("UPDATE ");
				sb.append(doc.getDocumentKind().getTablename()).append(" SET ")
				.append(this.getColumn()).append(" = ? WHERE document_id = ?");
				
				PreparedStatement ps = DSApi.context().prepareStatement(sb);
				ps.setLong(1, att.getId());
				ps.setLong(2, doc.getId());
				ps.executeUpdate();
			} catch (SQLException e) {
				throw new EdmException(e);
			}
			attVal.setAttachmentId(att.getId());
		} else {
			// tworzymy now� wersj� za��cznika
			doc.getAttachment(attVal.getAttachmentId()).createRevision(attVal.getFormFile().getFile());
		}
	}
}
