package pl.compan.docusafe.core.dockinds.field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.util.TextUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Klasa raprezentuj�ca pole wyliczeniowe wczytywane z xml'a
 *
 * klucz - int
 * object - EnumItem
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public final class XmlEnumColumnField extends AbstractEnumColumnField implements SearchPredicateProvider{
    private static final Logger log = LoggerFactory.getLogger(XmlEnumColumnField.class);
    public static final String REPO_REF_FIELD_PREFIX = "REF_";

    /** Nazwa tabeli w bazie danych z kt�rej zaci�gane s� warto�ci do pola s */
    private String tableName;
    /** Pole oznacz�jce od kt�rego pola zale�y */
    private String refField;
    /** Pole oznacz�jce kt�re pole jest zale�ne */
    private String assoField;
    /** Pole enum typu autocomplete */
    private boolean auto;

    private boolean emptyOnStart;
    private boolean allowNotApplicableItem;
    private boolean reloadAtEachDwrGet;
    private boolean repoField;

    /** po czym mo�na sortowa� - domy�lnie CN */
    public enum Sorted
    {
        TITLE, CN, ID
    }

    private Sorted sort = Sorted.CN;

    public XmlEnumColumnField(String refStyle, String id, String cn, String column, Map<String, String> names,
                              boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, boolean sort) throws EdmException
    {
        super(id, cn, column, names, ENUM, refStyle, null, required, requiredWhen, availableWhen);
    }
    private Map<Integer, EnumItem> enumItems;
    private Map<String, SortedSet<EnumItem>> refEnumItemsMap;
    private Set<EnumItem> availableItems;
    private Set<EnumItem> allItems;
    private Map<Integer, EnumItem> itemsReferencedForAll;

    @Override
    public void set(PreparedStatement ps, int index, Object key) throws FieldValueCoerceException, SQLException {
        if(key == null){
            ps.setObject(index, null);
        } else {
    		Integer integer = simpleCoerce(key);
    		if(integer == null)
                ps.setObject(index, null);
            else
            	ps.setInt(index,integer );
        }
    }

    public void initField() {
    }

    @Override
    public EnumItem asObject(Object key) throws IllegalArgumentException, EdmException {
    	if(key == null){
    		return null;
    	} else {
    		return getEnumItem((Integer)key);
    	}
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        int id = rs.getInt(getColumn());
        if(rs.wasNull()){
            return new FieldValue(this, null, null);
        } else {
            try {
                return new FieldValue(this, id, getEnumItem(id).getTitle());
            } catch (EntityNotFoundException ex){
            //je�li nie znajdziesz warto�ci zwr�� pust�
                log.error(ex.getMessage());
                return new FieldValue(this, null, null);
            }
        }
    }

    @Override
    public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) {
        if (values.get(getCn()) == null){
            return;
        }
        Integer[] enumIds = TextUtils.parseIntoIntegerArray(values.get(getCn()));
        if(!isMultiple()){
            dockindQuery.enumField(this, enumIds);
        } else {
            dockindQuery.InField(this,enumIds);
        }
        fieldValues.put(getCn(),enumIds);
    }

    public List<EnumItem> getSortedEnumItems(boolean onlyAvailable)
    {
        List<EnumItem> list = getEnumItems(onlyAvailable);
        Collections.sort(list, getComparator());
        return list;
    }

    public List<EnumItem> getEnumItems(boolean onlyAvailable)
    {
        if (onlyAvailable) {
            return availableItems != null ? new ArrayList<EnumItem>(availableItems) : Collections.EMPTY_LIST;
        } else {
            return enumItems != null ? new ArrayList<EnumItem>(enumItems.values()) : Collections.EMPTY_LIST;
        }
    }

    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, String id, Sorted sort) throws EntityNotFoundException {
        String temp = sort.toString();
        return enumDefault.getEnumItemsForDwr(fieldsValues, id, temp);
    }

    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EntityNotFoundException {
        return getEnumItemsForDwr(fieldsValues, null, sort);
    }

    @Override
    public void setEnumItems(Map<Integer, EnumItem> enumItems) {
        enumDefault.setEnumItems(enumItems);
    }

    @Override
    public void setRepoField(boolean repoField) {
        enumDefault.setRepoField(repoField);
    }

    @Override
    public void setItemsReferencedForAll(Map<Integer, EnumItem> itemsReferencedForAll) {
        enumDefault.setItemsReferencedForAll(itemsReferencedForAll);
    }

    public List<EnumItem> getSortedEnumItems()
    {
        return getSortedEnumItems(false);
    }

    @Override
    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, String id, String temp) throws EntityNotFoundException {
        return getEnumItemsForDwr(fieldsValues, id, sort);
    }


    private Comparator<EnumItem> getComparator()
    {
        switch (getSort())
        {
            case CN:
                return EnumItem.COMPARATOR_BY_CN;
            case TITLE:
                return EnumItem.COMPARATOR_BY_TITLE;
            case ID:
                return EnumItem.COMPARATOR_BY_ID;
            default:
                throw new IllegalStateException("Nieobs�ugiwany Sort " + getSort());
        }
    }

    @Override
    public void setRefEnumItemsMap(Map<String, SortedSet<EnumItem>> refEnumItemsMap) {
        enumDefault.setRefEnumItemsMap(refEnumItemsMap);
    }

    @Override
    public void setAvailableItems(Set<EnumItem> availableItems) {
        enumDefault.setAvailableItems(availableItems);
    }

    public String getRefField()
    {
        return refField;
    }

    @Override
    public void setAllItems(Set<EnumItem> allItems) {
        enumDefault.setAllItems(allItems);
    }

    public Sorted getSort()
    {
        return sort;
    }

    public void setSort(Sorted sort)
    {
        this.sort = sort;
    }
}
