package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * (tylko dla DWR) Pole umo�liwiaj�ce wyb�r u�ytkownika lub dzia�u w formie
 * listy rozwijanej
 * 
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DocumentUserDivisionField extends ManualSortedNonColumnField implements EnumNonColumnField
{
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);
	private static final Logger LOG = LoggerFactory.getLogger(DocumentUserDivisionField.class);

	public enum PersonType
	{
		SENDER, RECIPIENT
	}

	private PersonType type = PersonType.RECIPIENT;

	public DocumentUserDivisionField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, names, Field.DOCUMENT_USER_DIVISION, null, null, required, requiredWhen, availableWhen);
		LOG.info("created {}", this.getCn());
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		OfficeDocument doc = OfficeDocument.find(documentId);
		switch (type)
		{
			case SENDER:
				return new FieldValue(this, doc.getSender().getId(), doc.getSender());
			case RECIPIENT:
				if (!doc.getRecipients().isEmpty())
				{
					return new FieldValue(this, doc.getRecipients().get(0).getId(), doc.getRecipients().get(0));
				}
		}
		return null;
	}

	/**
	 * Zwraca obiekt, kt�ry zostanie zapisany w Document jako odbiorca
	 * 
	 * @param value
	 *             DSUser, DSDivision, lub String z przedrostkiem 'user_' lub
	 *             'div_' po kt�rym jest login usera lub guid dzia�u
	 * @return DSDivision lub DSUser
	 * @throws FieldValueCoerceException
	 */
	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value == null)
			return null;
		try
		{
			if (value instanceof CharSequence)
			{
				String key = value.toString();
				if (key.startsWith("div_"))
				{
					return DSDivision.find(key.substring(4));
				}
				else if (key.startsWith("user_"))
				{
					return DSUser.findByUsername(key.substring(5));
				}
				else
				{
					throw new FieldValueCoerceException("Nieznany identyfikator : " + key);
				}
			}
			else if (value instanceof DSUser || value instanceof DSDivision)
			{
				return value;
			}
			else
			{
				throw new FieldValueCoerceException(this.getName() + " : cannot coerce " + value);
			}
		}
		catch (EdmException e)
		{
			LOG.error(e.getMessage(), e);
			throw new FieldValueCoerceException(e.getMessage());
		}
	}

	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException
	{
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("", "--wybierz--");

		for (DSUser user : DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME))
		{
			enumValues.put("user_" + user.getName(), user.asLastnameFirstname());
		}

		for (DSDivision div : DSDivision.getAllDivisions())
		{
			enumValues.put("div_" + div.getGuid(), div.getName());
		}

		return new EnumValues(enumValues, enumSelected);
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM_AUTOCOMPLETE);
	}

	public boolean isSenderField()
	{
		return type == PersonType.SENDER;
	}

	public boolean isRecipientField()
	{
		return type == PersonType.RECIPIENT;
	}
}
