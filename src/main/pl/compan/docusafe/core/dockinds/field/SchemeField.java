package pl.compan.docusafe.core.dockinds.field;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public final class SchemeField implements Serializable
{

	private final static Logger LOG = LoggerFactory.getLogger(SchemeField.class);
	private final String cn;

	private boolean visible = true;
	private boolean disabled = false;
	private Boolean readonly;
	private Boolean required;
	private Boolean hidden;
	private String color;
	private String showAs;
	private String cssClass;
	private List<String> dicButtons;
	private List<String> enumValuesId;
    private String label;

	public SchemeField(String cn)
	{
		this.cn = cn;
	}

	public boolean getDisabled()
	{
		LOG.trace("getDisabled for field '{}' = {}", cn, disabled);
		return disabled;
	}

	public void setDisabled(boolean disabled)
	{
		this.disabled = disabled;
	}

	public boolean isVisible()
	{
		LOG.trace("isVisible for field '{}' = {}", cn, visible);
		return visible;
	}

	public void setVisible(boolean visible)
	{
		this.visible = visible;
	}

	public String getColor()
	{
		LOG.trace("getColor for field '{}' = {}", cn, color);
		return color;
	}

	public void setColor(String color)
	{
		// LOG.info("setColor : '{}'", color);
		this.color = color;
	}

	public String getCn()
	{
		return cn;
	}

	public String getShowAs()
	{
		LOG.trace("getShowAs for field '{}' = {}", cn, showAs);
		return showAs;
	}

	public void setShowAs(String showAs)
	{
		this.showAs = showAs;
	}

	public Boolean isReadonly()
	{
		LOG.trace("isReadonly for field '{}' = {}", cn, readonly);
		return readonly;
	}

	public void setReadonly(String readonly)
	{
		this.readonly = Boolean.parseBoolean(readonly);
	}

	public Boolean isHidden()
	{
		return hidden;
	}

	public void setHidden(Boolean hidden)
	{
		this.hidden = hidden;
	}

	public Boolean isRequired()
	{
		return required;
	}

	public void setRequired(String required)
	{
		this.required = Boolean.parseBoolean(required);
	}

	public List<String> getDicButtons() {
		return dicButtons;
	}

	public void setDicButtons(String dicButtons) {
		this.dicButtons = Arrays.asList(dicButtons.split(","));
	}

    public List<String> getEnumValuesId() {
        return enumValuesId;
    }

    public void setEnumValuesId(String strEnumValuesCn) {
        this.enumValuesId = Arrays.asList(strEnumValuesCn.split(","));
    }

    @Override
	public String toString()
	{
		return "SchemeField [cn=" + cn + ", visible=" + visible + ", color=" + color + ", disabled=" + disabled + ", showAs=" + showAs + ", readonly=" + readonly + ", hidden=" + hidden + ", required=" + required + "]";
	}

	public String getCssClass()
	{
		return cssClass;
	}

	public void setCssClass(String cssClass)
	{
		this.cssClass = cssClass;
	}

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
