package pl.compan.docusafe.core.dockinds.field;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Maps;

public class DictionaryField extends AbstractDictionaryField implements SearchPredicateProvider
{
	private static final Logger log = LoggerFactory.getLogger(DictionaryField.class);
	
	public DictionaryField(String id, String cn, String column, String logic, Map<String, String> names, String type, String refStyle, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String tableName, String resourceName, String filterDictionaryCn, DocumentKind documentKind) throws EdmException
	{
		super(id, cn, column, logic, names, DICTIONARY, null, null, required, requiredWhen, availableWhen, tableName, resourceName, filterDictionaryCn, documentKind);
	}

	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues)
	{
		log.info("addSearchPredicate map = '{}', value = '{}'", values, values.get(getCn()));
		Map<String, Object> dictionaryFieldsValues = Maps.newHashMap();
		for (Field field : getDockindSearchFields())
		{
			String key = null;
			String key_from = null;
			String key_to = null;
			if(field.getType().equals(Field.DATE)){
				key_from = getCn() + "_" + field.getCn()+"_from";
				key_to = getCn() + "_" + field.getCn()+"_to";
				if (values.get(key_from) != null && !values.get(key_from).toString().isEmpty()
						&& values.get(key_to) != null && !values.get(key_to).toString().isEmpty())
				{
					List<String> keys = new ArrayList<String>();
					keys.add((String) values.get(key_from));
					keys.add((String) values.get(key_to));
					dictionaryFieldsValues.put(field.getCn(), keys);
				}
			}else{
				key = getCn() + "_" + field.getCn();
				if (values.get(key) != null)
					dictionaryFieldsValues.put(field.getCn(), values.get(key));
			}
			
			
		}
		if (!dictionaryFieldsValues.isEmpty())
			dockindQuery.dictionary(this, dictionaryFieldsValues);
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
	{
		return null;
	}

	public Field getDictionaryFieldByCn(String dicFieldCn)
	{
		for (Field field : getFields())
		{
			if (field.getCn().replace("_1", "").equals(dicFieldCn.replace(getCn() + "_", "")))
				return field;
		}
		return null;
	}
}
