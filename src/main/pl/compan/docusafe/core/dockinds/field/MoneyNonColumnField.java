package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.util.StringManager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 *
 * Date: 11.07.13
 * Time: 15:03
 * Gosia
 */
public class MoneyNonColumnField extends NonColumnField implements  MoneyField {

    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);
    private static final int MONEY_SCALE = 2;

    private final ThreadLocal<DecimalFormat> moneyFormat = new ThreadLocal<DecimalFormat>(){
        @Override
        protected DecimalFormat initialValue() {
            DecimalFormat decimalFormat = new DecimalFormat("###########0.00");
            DecimalFormatSymbols decimalSymbols = new DecimalFormatSymbols();
            decimalSymbols.setDecimalSeparator('.');
            decimalSymbols.setGroupingSeparator(' ');
            decimalFormat.setDecimalFormatSymbols(decimalSymbols);
            decimalFormat.setGroupingSize(3);
            decimalFormat.setGroupingUsed(true);
            return decimalFormat;
        }
    };

    /**
     * @param id            id z xml'a, musi by� unikalne
     * @param cn            cn z xml'a, musi b� unikalne
     * @param names         nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param required      czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen  warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws pl.compan.docusafe.core.EdmException
     */
    public MoneyNonColumnField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, names, Field.MONEY, null, null, required, requiredWhen, availableWhen);
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        String value = null;
        return new FieldValue(this, value, value);
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        if (value == null){
            return null;
        }
        BigDecimal ret;
        if(value instanceof BigDecimal){
            ret = (BigDecimal) value;
            if(ret.scale() != MONEY_SCALE){
                ret = ret.setScale(MONEY_SCALE, RoundingMode.HALF_UP);
            }
        } else if(value instanceof CharSequence){
            String str = value.toString();
            if(isBlank(str)){
                return null;
            }
            try {
                ret = new BigDecimal(str.replaceAll(" ","").replaceAll(",","."))
                        .setScale(MONEY_SCALE,RoundingMode.HALF_UP);
            } catch (NumberFormatException e) {
                throw new FieldValueCoerceException(sm.getString("NieMoznaSkonwertowacWartosci")+" "+value+" "+sm.getString("doKwoty"));
            }
        } else {
            throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + " cannot be converted to BigDecimal by MoneyNonColumnField");
        }

        return ret;    }

    @Override
    public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
        return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY);    }
}
