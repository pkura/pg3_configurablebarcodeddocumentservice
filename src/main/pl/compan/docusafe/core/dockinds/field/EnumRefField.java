package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
/* User: Administrator, Date: 2007-02-16 11:55:59 */
import java.util.ArrayList;

/**
 * Klasa raprezentuj�ca "pole wyliczeniowe z referencj�" z danego rodzaju dokumentu.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class EnumRefField extends Field implements EnumerationField, SearchPredicateProvider
{
	StringManager sm = 
        GlobalPreferences.loadPropertiesFile(EnumRefField.class.getPackage().getName(),null);
    /**
     * Identyfikator 'cn' pola, z kt�rym jest powi�zane to pole.
     */
    private String fieldRefCn;
    private Map<Object, Map<Integer, EnumItem>> enumItems;
    private boolean sort;
    /**
     * Okre�la, czy w select-ach wy�wietlaj�cych naraz wszystkie warto�ci wyliczeniowe, ma by�
     * dodatkowo pokazywany kod kategorii (warto�ci g��wnego pola), z kt�r� ta warto�� jest powi�zana.
     * U�ywane np. w wyszukiwarce zaawansowanej.
     */
    private boolean withRefCodes;

    public EnumRefField(String refStyle, String fieldRefCn, String id, String cn, String column, Map<String,String> names,
                        boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, boolean withRefCodes,boolean sort) throws EdmException
    {
        super(id, cn, column, names, ENUM_REF, refStyle, null, required, requiredWhen, availableWhen);

        this.fieldRefCn = fieldRefCn;
        this.enumItems = new LinkedHashMap<Object, Map<Integer, EnumItem>>();
        this.withRefCodes = withRefCodes;
        this.sort = sort;
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        int l = rs.getInt(getColumn());
        Integer value = rs.wasNull() ? null : new Integer(l);
        return new FieldValue(this, value);
    }

    @Override
    public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
        if(value == null || value.equals(0)){
            ps.setObject(index, null);
        } else {
            ps.setInt(index, (Integer) simpleCoerce(value));
        }
    }

    @Override
    public EnumItem asObject(Object key) throws IllegalArgumentException, EdmException {
	        if(key == null) return null;
        return getEnumItem((Integer)key);
    }

    @Override
    public Integer simpleCoerce(Object value) throws FieldValueCoerceException {
        if(value instanceof  EnumItem){
            return ((EnumItem)value).getId();
        } else {
            return FieldUtils.intCoerce(this,value);
        }
    }

    public void addEnum(Object discriminator, Integer id, String cn, Map<String,String> titles, String[] args, boolean forceRemark,String fieldCn)
    {
        if (id == null)
            throw new NullPointerException("id");
        if (titles == null)
            throw new NullPointerException("title");

        if (enumItems.get(discriminator) == null)
            enumItems.put(discriminator, new LinkedHashMap<Integer, EnumItem>());

        enumItems.get(discriminator).put(id, new EnumItemImpl(id, cn, titles, args, forceRemark,fieldCn));
    }

    public Map<Object, Map<Integer, EnumItem>> getEnumItemsList()
    {
        return enumItems;
    }
    
    public void initField()
    {
    	if(sort)
    	{
    		 for (Object discriminator : enumItems.keySet())
	        {
    			 List<EnumItem> list = new ArrayList<EnumItem>(enumItems.get(discriminator).values());
    			 Collections.sort(list, new EnumItem.EnumItemComparator());
    			 enumItems.get(discriminator).clear();
    			 for (EnumItem item : list) 
		        {
    				 enumItems.get(discriminator).put(item.getId(), item);
				}
	        }
    	}
    }

    public List<EnumItem> getEnumItems()
    {
        List<EnumItem> list = new ArrayList<EnumItem>();
        for (Object discriminator : enumItems.keySet())
        {
            list.addAll(enumItems.get(discriminator).values());
        }
        Map<Integer, EnumItem> map = new HashMap<Integer, EnumItem>();
        for(EnumItem enumitem : list)
            map.put(enumitem.getId(), enumitem);
        list = new ArrayList<EnumItem>();
        for(EnumItem enumitem: map.values())
            list.add(enumitem);
        
        return list;
    }
    
    public List<EnumItem> getSortedEnumItems()
    {
        List<EnumItem> list = new ArrayList<EnumItem>();
        for (Object discriminator : enumItems.keySet())
        {
            for (EnumItem item : enumItems.get(discriminator).values())
            {
                item.setTmp(discriminator);
                list.add(item);
            }
        }
        
        Map<Integer, EnumItem> map = new HashMap<Integer, EnumItem>();
        for(EnumItem enumitem : list)
            map.put(enumitem.getId(), enumitem);
        list = new ArrayList<EnumItem>();
        for(EnumItem enumitem: map.values())
            list.add(enumitem);
        
        Collections.sort(list, new EnumItem.EnumItemComparator());
        return list;
    }

    public List<EnumItem> getEnumItems(Object discriminator)
    {
        return enumItems.get(discriminator) != null ? new ArrayList<EnumItem>(enumItems.get(discriminator).values()) : Collections.EMPTY_LIST;
    }

    public EnumItem getEnumItem(Integer id) throws EntityNotFoundException
    {
        // TODO: moze przerobic zeby byla jakas wspolna lista dla wszystkich enum-ow (albo taka metoda dodac - sie moze przyda tez w innych miejscach)
        for (Map<Integer, EnumItem> map : enumItems.values())
        {
            EnumItem item = map != null ? (EnumItem) map.get(id) : null;
            if (item != null)
                return item;
        }

        throw new EntityNotFoundException(sm.getString("NieZnalezionoWartosciWyliczeniowejOcn")+" = " + id + " "+sm.getString("DlaPola")+" " + getCn());
    }
    
    public EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException
    {
        for (Map<Integer, EnumItem> map : enumItems.values())
        {
            Set<Integer> keys = map.keySet();
            for (Integer key : keys)
            {
				if(cn.equals(map.get(key).getCn()))
					return map.get(key);
			}
        }
        
        throw new EntityNotFoundException("Nie znaleziono warto�ci wyliczeniowej o cn = " + cn + " dla pola " + getCn());
    }    
    
    public String getFieldRefCn()
    {
        return fieldRefCn;
    }

    public boolean isWithRefCodes()
    {
        return withRefCodes;
    }    

    /**
     * Uwaga: je�eli podany tytu� wyst�puje dla kilku discriminator�w to zwracany jest pierwszy element o tym tytule
     */
    public EnumItem getEnumItemByTitle(String title) throws EntityNotFoundException
    {
        if (enumItems != null)
        {
            for (Map<Integer, EnumItem> items : enumItems.values())
            {
                if (items != null)
                {
                    for (EnumItem i : items.values())
                    {
                        if (title.equals(i.getTitle()))
                            return i;
                    }
                }
            }
        }
        throw new EntityNotFoundException(sm.getString("NieZnalezionoWartosciWyliczeniowejOtitle")+" = " + title + " "+sm.getString("DlaPola")+" " + getCn());
    }

    public EnumItem getEnumItemByTitle(String title, Integer discrId) throws EntityNotFoundException
    {
        if (enumItems != null)
        {
            Map<Integer, EnumItem> items = enumItems.get(discrId);
            if (items != null)
            {
                for (EnumItem i : items.values())
                {
                    if (title.equals(i.getTitle()))
                        return i;
                }
            }
        }
        throw new EntityNotFoundException(sm.getString("NieZnalezionoWartosciWyliczeniowejOtitle")+" = " + title + " "+sm.getString("DlaDiscriminatora")+" " + discrId + " "+sm.getString("DlaPola")+" " + getCn());
    }

    /**
     * Zwraca id pola fieldRefCn (pole z kt�rym jest powi�zane) dla kt�rego jest widoczny
     */
    public Object getEnumItemDiscriminator(Integer id) throws EntityNotFoundException
    {
        for (Object i : enumItems.keySet())
        {
            Map<Integer, EnumItem> items = enumItems.get(i);
            if (items.get(id) != null)
            {
                return i;
            }
        }
        return null;
    }
    
    public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
		pl.compan.docusafe.core.dockinds.dwr.Field field;
    	if (isMultiple()) {
    		  field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM_MULTIPLE);
    	}
    	else {
    		field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
    	}
    	return field;
	}
    
    public String getRefField() throws EntityNotFoundException {
        return this.fieldRefCn;
    }

    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EntityNotFoundException 
    {
    	Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("","--wybierz--");

		if (fieldsValues != null && fieldsValues.get(getCn()) != null)
			enumSelected.add(getEnumItem((Integer)fieldsValues.get(getCn())).getTitle());
		else 
			enumSelected.add("");

		if (fieldsValues.get(getRefField())!=null && getEnumItems(fieldsValues.get(getRefField())) != null)
		{
			for (EnumItem item : getEnumItems(fieldsValues.get(getRefField())))
				enumValues.put(item.getId().toString(), item.getTitle());
			return new EnumValues(enumValues, enumSelected);
		}
		else 
			return new EnumValues(enumValues, enumSelected);
    }

    @Override
    public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) {
        if (values.get(getCn()) == null){
            return;
        }
        Integer[] enumIds = TextUtils.parseIntoIntegerArray(values.get(getCn()));
        if(!isMultiple()){
            dockindQuery.enumField(this, enumIds);
        } else {
            dockindQuery.InField(this,enumIds);
        }
        fieldValues.put(getCn(),enumIds);
    }
}
