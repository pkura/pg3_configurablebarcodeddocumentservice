package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.viewserver.ViewServer;

public class DocListItem
{
	private static final Logger log = LoggerFactory.getLogger(DocListBean.class);
	
    private static String baseUrl = Docusafe.pageContext;
	public static Integer BRANCH = 1;
	public static Integer LEAF = 2;
	public static Integer nextID = 0;

	public static Integer getNextId()
	{
		return nextID ++;
	}
	
	private Integer id;
	private String title;
	private DocListItem parent;
	private Map<Integer,DocListItem> children;
	private Integer type;
	private String column;
	private List<DocListBean> doclistbeans;
	private Integer level;
	private Boolean alert;
	private Boolean containsDoc;
	private Long binderId;

	public DocListItem(){}
	
	public DocListItem(EnumItem enumItem, DocListItem docListItem, Integer index, List<String> groupingBys, String lastItem, Map<String, Field> fMap,Long binderId) 
	{
		this.title = enumItem.getTitle();
		this.binderId = binderId;
		this.parent = docListItem;
		index ++ ;		
		String bg = groupingBys.get(index);   
		Field groupField = fMap.get(bg);
		this.setParent(docListItem);
		this.level = new Integer(index);
		this.setId(getNextId()); 
		this.setColumn(groupField.getColumn());
		if(!bg.equals(lastItem)) 
			this.setChildrenFromEnum(groupField.getEnumItems(),index,groupingBys,lastItem,fMap,binderId);
		else
			this.setLastChildrenFromEnum(groupField.getEnumItems(),index,groupingBys,lastItem,fMap,binderId);
	}
	
	public DocListItem(EnumItem enumItem, DocListItem docListItem, Integer index, List<String> groupingBys, String lastItem, Map<String, Field> fMap,boolean qwe,Long binderId) 
	{
		index++;
		this.binderId = binderId;
		this.title = enumItem.getTitle();
		this.parent = docListItem;
		this.level = new Integer(index);
		this.type = LEAF;
		this.setId(getNextId());
	}
	
	public void setChildrenFromEnum(List<EnumItem> groupEnumItems, Integer index, List<String> groupingBys, String lastItem, Map<String, Field> map,Long binderId) 
	{
		if(children == null)
			children = new LinkedHashMap<Integer,DocListItem>();
		for (EnumItem groupEnumItem : groupEnumItems) 
		{
			children.put(groupEnumItem.getId(),new DocListItem(groupEnumItem,this, index,groupingBys,lastItem, map,binderId));			
		}		
	}
	
	public void setLastChildrenFromEnum(List<EnumItem> enumItems, Integer index, List<String> list, String lastItem, Map<String, Field> map,Long binderId) 
	{
		if(children == null)
			children = new LinkedHashMap<Integer,DocListItem>();
		for (EnumItem enumItem : enumItems) 
		{
			children.put(enumItem.getId(),new DocListItem(enumItem,this, index,list,lastItem, map,true,binderId));			
		}		
	}

	public DocListItem getLeaf(ResultSet rs,Boolean isAlert) throws SQLException
	{
		DocListItem child = children.get(new Integer(rs.getInt(column)));
		if(child != null)
		{
			if(isAlert)
				child.setAlert();
			if(LEAF.equals(child.getType()))
				return child;
			else
				return child.getLeaf(rs,isAlert);
		}
		else
			return this;
	}
	
	public void setAlert()
	{
		this.alert = true;
		if(this.parent != null)
		{
			this.parent.setAlert();
		}
	}
	
	public void containsDoc()
	{
		this.containsDoc = true;
		if(this.parent != null)
			this.parent.containsDoc();
	}
	
    public String getDocListHTML(Field field)
    {
    	
    	StringBuilder sb = new StringBuilder();
    	try
    	{
	    	Set<Integer> children = getChildren().keySet();
	    	for (Integer docItmCn : children) 
	    	{
				DocListItem docItm = getChildren().get(docItmCn);
				sb.append("<table><tr><td>"+getShif(docItm)+"</td>\n");
				sb.append("<td><a id=\"ITM_LINK_"+docItm.getId()+"\" href=\"javascript:changeShow('ITM_SPAN_"+docItm.getId()+"','ITM_LINK_"+docItm.getId()+"')\">"+docItm.getTitle()+"</a>\n");
				if(docItm.getContainsDoc()!= null && docItm.getContainsDoc())
					sb.append("<img src=\"/"+baseUrl+"/img/arrow.gif\" width=\"17\" height=\"17\"/>");
				if(docItm.getAlert() != null && docItm.getAlert())
					sb.append("<img src=\"/"+baseUrl+"/img/alert.gif\" width=\"17\" height=\"17\"/>");
				sb.append("</td></tr></table>\n");
				sb.append("<span id=\"ITM_SPAN_"+docItm.getId()+"\" >");//style=\"display:none;\">");
				if(docItm.getDoclistbeans() != null)
				{
					for (DocListBean bean :  docItm.getDoclistbeans()) 
					{
						sb.append("<table><tr class=singleRow>\n");
						sb.append(getOneDocLine(bean,field,docItm)+"\n");
						sb.append("</tr></table>\n");
					}
				}
				sb.append(docItm.getDocListHTML(field));
				sb.append("</span>");
	    	}	
    	}
    	catch (Exception e) 
    	{
    		log.error("",e);
    		sb = new StringBuilder("B��d generowania HTML");
		}
    	return sb.toString();
    }
    
    private String getShif(DocListItem docItm)
    {
    	if(docItm.getLevel() > 1)
    		return "&bull;";
    	else 
    		return "";
    }

    private String getOneDocLine(DocListBean bean, Field field, DocListItem docItm)
    {
    	StringBuilder sb = new StringBuilder();

    	sb.append("<input name=\"values."+field.getCn()+"\" value=\""+bean.getId()+"\" type=\"hidden\">");
    	sb.append("<td>&nbsp;&nbsp;&nbsp;&nbsp;&bull;"+"</td><td width=\"300\"><a href=\"/"+baseUrl+"/repository/edit-dockind-document.action?id="+bean.getId()+"&binderId="+docItm.getBinderId()+"\">"+bean.getTitle()+"</a></td>\n");
    	for (EditField eField: bean.getEditFields())
    	{
			if("enum".equals(eField.getField().getType()))
			{
				sb.append(getEnum(bean, field,eField)+"\n");
			}
			else
			{
				sb.append(getOther(bean, field,eField)+"\n");
			}
		}
    	sb.append(getAttachment(bean, field,docItm)+"</td>\n");
    	sb.append("");
    	
    	return sb.toString();
    }


    private String getEnum(DocListBean bean, Field field, EditField eField)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("<td><select class=\"sel multicolor\" ");
    	boolean canEdit = eField.getCanEdit();
    	if(canEdit)
    	{
    		sb.append(" name=\"values."+field.getCn()+"_EF_"+eField.getField().getCn()+bean.getId()+"\"");
    		sb.append("id=\""+field.getCn()+"_EF_"+eField.getField().getCn()+bean.getId()+"\" ");
    	}
    	else
    	{
    		sb.append(" disabled=\"disabled\" ");
    		sb.append("style=\"");
    		sb.append("background-color:#ffffc4;\"");
    	}
    	sb.append(">");
    	sb.append("<option value=\"\">-- wybierz --</option>");
    	for (EnumItem eITM : eField.getField().getEnumItems()) 
    	{
    		sb.append("<option value=\""+eITM.getId()+"\"");
    		if(eField.getValue() != null && eITM.getId().equals(new Integer((String)eField.getValue())))
    			sb.append("selected=\"selected\"");    		
    		sb.append(">"+eITM.getTitle()+"</option>\n");
		}
    	sb.append("</select>\n");
    	/*if(!canEdit)
    	{
    		sb.append("<input type=\"hidden\" name=\"values."+field.getCn()+"_EF_"+eField.getField().getCn()+bean.getId()+"\" ");
    		sb.append("id=\""+field.getCn()+"_EF_"+eField.getField().getCn()+bean.getId()+"\" ");
    		sb.append("value=\""+(eField.getValue() != null ? eField.getValue() : "")+"\"");
    		sb.append(" />");
    	}*/
    	return sb.toString();
    }
    
    private String getOther(DocListBean bean, Field field, EditField eField)
    {
    	StringBuilder sb = new StringBuilder();   	
    	sb.append("<td><span id=\""+bean.getId()+"_box\"");
    	sb.append("title=\"offsetx=[15] offsety=[15] delay=[300] header=["+eField.getField().getName()+"] body=["+(eField.getValue() != null ? eField.getValue() : "")+"]");
    	sb.append("style=\"vertical-align:middle;font-family:arial;font-size:20px;font-weight:bold;color:#ABABAB;cursor:pointer\">");
    	//sb.append("<img src=\"/"+baseUrl+"/img/podpisz.gif\" width=\"17\" height=\"17\"");
    	sb.append("<input id=\""+bean.getId()+"\"");
    	sb.append(" type=\"text\" name=\"values."+field.getCn()+"_EF_"+eField.getField().getCn()+bean.getId()+"\"");
    	sb.append(" value=\""+(eField.getValue() != null ? eField.getValue() : "")+"\" class=\"txt\" ");
    	boolean canEdit = eField.getCanEdit();
    	if(!canEdit)
    	{
    		sb.append(" style=\""); 
    		sb.append("background-color:#ffffc4;\" ");
    		sb.append(" readonly=\"readonly\"");
    	}
    	sb.append(" size=\"10\"/>");
    	sb.append("</span>");
    	sb.append("&nbsp;&nbsp;");
    	return sb.toString();
    }
    
    private String getAttachment(DocListBean bean, Field field,DocListItem docItm)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("<span id=\""+bean.getId()+"_atta\">");
    	if(bean.getAttachmentRevision() != null)
    	{
    		sb.append("<a name=\"DodajNowaWersje\" href=\"javascript:openToolWindow('/"+baseUrl+"/office/common/add-attachment-from-pages.action?");
    		sb.append("binderId="+docItm.getBinderId()+"&attachmentId="+bean.getAttachmentRevision().getAttachment().getId()+"&documentId="+bean.getId()+"','vs',800,600);\">");
    		sb.append("<img src=\"/"+baseUrl+"/img/add_icon.gif\" width=\"16\" height=\"16\"	height=\"17\" title=\"DodajNowaWersje\"/></a>");
    		sb.append("&nbsp;&nbsp;");
    		if(ViewServer.mimeAcceptable(bean.getAttachmentRevision().getMime()))
    		{
    			
    			sb.append("<a name=\"WyswietlZalacznikWprzegladarce\" " +
    					"href=\"/"+baseUrl+"/repository/view-attachment-revision.do?asPdf=true&id="+
    					bean.getAttachmentRevision().getId()+"\">");
    			sb.append("<img src=\"/"+baseUrl+"/img/pdf.gif\" width=\"16\" height=\"16\"	height=\"17\" title=\"pobierz za��cznik\"/></a>&nbsp;&nbsp;");
    			
    			sb.append("<a name=\"WyswietlZalacznikWprzegladarce\" " +
    					"href=\"/"+baseUrl+"/viewserver/viewer.action?id="+
    					bean.getAttachmentRevision().getId()+"&fax=false&width=1000&height=750&binderId="+docItm.getBinderId()+"\" onclick=\"return !aTarget(this)\">");
    			sb.append("<img src=\"/"+baseUrl+"/img/wyswietl.gif\" width=\"16\" height=\"16\"	height=\"17\" title=\"WyswietlZalacznikWprzegladarce\"/></a>&nbsp;&nbsp;");
    		}
    	}
    	else
    	{
    		sb.append("<a name=\"DodajZalacznik\" href=\"javascript:openToolWindow('/"+baseUrl+"/office/common/add-attachment-from-pages.action?");
    		sb.append("documentId="+bean.getId()+"&binderId="+docItm.getBinderId()+"','vs',800,600);\">");
    		sb.append("<img src=\"/"+baseUrl+"/img/add_icon.gif\" width=\"16\" height=\"16\"	height=\"17\" title=\"DodajNowaWersje\"/></a>");
    	}
    	sb.append("</span>");
    	return sb.toString();
    }
    
    @Deprecated
	public void print()
	{
		LoggerFactory.getLogger("mariusz").debug("ITM" + id +" "+ title);
		LoggerFactory.getLogger("mariusz").debug(getChildren().size());
		LoggerFactory.getLogger("mariusz").debug("");
		if(getDoclistbeans() != null)
		{
			for (DocListBean doc :getDoclistbeans() ) {
				LoggerFactory.getLogger("mariusz").debug("DOC - "+ doc.getId()+"  "+ doc.getTitle());
			}
		}

		for (Integer itm : getChildren().keySet()) 
		{
			getChildren().get(itm).print();
		}
	}
	
    @Deprecated
	public void printParent()
	{
		LoggerFactory.getLogger("mariusz").debug("THIS ID = "+ id +" title = "+title);
		if(parent != null)
		{
			LoggerFactory.getLogger("mariusz").debug("--PARENT ID = "+ parent.getId() +" title = "+parent.getId());
			parent.printParent();
		}
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public DocListItem getParent() {
		return parent;
	}
	public void setParent(DocListItem parent) {
		this.parent = parent;
	}
	public Map<Integer,DocListItem> getChildren() {
		if(children == null)
			children = new LinkedHashMap<Integer, DocListItem>();
		return children;
	}
	public void setChildren(Map<Integer,DocListItem> children) {
		this.children = children;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public void setId(Integer cn) {
		this.id = cn;
	}
	public Integer getId() {
		return id;
	}

	public void setDoclistbeans(List<DocListBean> doclistbeans) {
		this.doclistbeans = doclistbeans;
	}
	public List<DocListBean> getDoclistbeans() {
		if(doclistbeans == null)
			doclistbeans = new ArrayList<DocListBean>();
		return doclistbeans;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getLevel() {
		return level;
	}
	
	public void setColumn(String column) {
		this.column = column;
	}
	public String getColumn() {
		return column;
	}

	public void setAlert(Boolean alert) {
		this.alert = alert;
	}

	public Boolean getAlert() {
		return alert;
	}

	public void setContainsDoc(Boolean containsDoc) {
		this.containsDoc = containsDoc;
	}

	public Boolean getContainsDoc() {
		return containsDoc;
	}

	public void setBinderId(Long binderId)
	{
		this.binderId = binderId;
	}

	public Long getBinderId()
	{
		return binderId;
	}
}
