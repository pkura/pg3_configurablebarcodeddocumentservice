package pl.compan.docusafe.core.dockinds.field;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.StringManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class DocumentOutAdditionalZPOField extends NonColumnField implements CustomPersistence, SearchPredicateProvider
{

    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(DocumentOutAdditionalZPOField.class.getPackage().getName(), null);

    public DocumentOutAdditionalZPOField(String id, String type, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
    {
        super(id, cn, names, pl.compan.docusafe.core.dockinds.field.Field.BOOL, null, null, required, requiredWhen, availableWhen);
    }

    public void persist(Document doc, Object key) throws EdmException
    {
        System.out.println("KEY "+ key);
        if (key == null)
        {
            ((OutOfficeDocument) doc).setAdditionalZpo(false);
        }
        else if( key instanceof Boolean)
        {
            ((OutOfficeDocument) doc).setAdditionalZpo((Boolean) key);
        }
        else
        {
            throw new EdmException("DocumentOutAdditionalZPOField persist key class: "+key.getClass());
        }
    }



    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
    {
        OfficeDocument doc = OfficeDocument.find(documentId);

        if (doc instanceof InOfficeDocument)
            throw new EdmException("DocumentOutAdditionalZPOField provideFieldValue doc class: "+doc.getClass());
        else if (doc instanceof OutOfficeDocument && ((OutOfficeDocument)doc).getAdditionalZpo() != null )
            return new FieldValue(this, ((OutOfficeDocument)doc).getAdditionalZpo(), ((OutOfficeDocument)doc).getAdditionalZpo());
        else
            return new FieldValue(this, null, null);
    }
    public Object simpleCoerce(Object value) throws FieldValueCoerceException
    {
        if (value == null )
            return false;
        Boolean additionalZpo;
        if ((value instanceof String && String.valueOf(value).length() == 0)) return null;
        if (!(value instanceof Boolean)) {
            try {
                if("on".equalsIgnoreCase((String) value))
                    additionalZpo = true;
                else
                    additionalZpo = Boolean.parseBoolean(String.valueOf(value));
            } catch (Exception e) {
                throw new FieldValueCoerceException(sm.getString("NieMoznaSkonwertowacWartosci") + " '" + value + "' " + sm.getString("DoDaty"));
            }
        } else {
            additionalZpo = (Boolean) value;

        }
        return additionalZpo;
    }


    public Field getDwrField() throws EdmException
    {
        return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
    }


    public void addSearchPredicate(DockindQuery dockindQuery,Map<String, Object> values, Map<String, Object> fieldValues)
    {
        String BoleanString = (String) values.get(this.getCn());

        Boolean additionalZpo = null;
        if(StringUtils.isNotBlank(BoleanString))
            additionalZpo = Boolean.parseBoolean(BoleanString);
        dockindQuery.isAdditionalZpo(additionalZpo);
    }
}
