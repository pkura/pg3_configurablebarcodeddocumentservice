package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.Database.Column;
import pl.compan.docusafe.util.StringManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class DocumentDescriptionField extends OrderedNonColumnField implements CustomPersistence  {

    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);
    private static final String VALIDATOR_REG_EXP = "^(.|\\s){0,510}$";

    public DocumentDescriptionField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(OrdersFactory.createOrders(new Column("description")), id, cn, names, Field.DOCUMENT_DESCRIPTION, null, null, required, requiredWhen, availableWhen);
    }

    @Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		Document doc = Document.find(documentId,false);
		return new FieldValue(this, doc.getDescription());
	}


	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
		if (value == null)
			return null;
		String postalRegNr = null;
		if (value instanceof String)
			postalRegNr = (String)value;
		else
		{
			throw new FieldValueCoerceException(this.getName()
                    + ":" + sm.getString("NieMoznaSkonwertowacWartosci")
                    + " " + value + " " + "do Stringa");
		}
		return postalRegNr;
	}

	public void persist(Document doc, Object key) throws EdmException {
        doc.setDescription(key != null ? ((String) key).replaceAll("\r\n", "\n") : (String) key);
    }

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
        pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.TEXTAREA);
        field.setValidatorRegExp(VALIDATOR_REG_EXP);
        return field;
	}
}
