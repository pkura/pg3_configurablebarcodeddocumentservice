package pl.compan.docusafe.core.dockinds.field;

import java.util.List;
import java.util.Map;

/**
 * Interfejs dla po� typu s�ownikowego
 */
public interface DictionarableField {

    List<Field> getFields();

    Map<String, Field> getRequiredFields();
}
