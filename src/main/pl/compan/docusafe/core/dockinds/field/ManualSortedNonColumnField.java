package pl.compan.docusafe.core.dockinds.field;

import com.beust.jcommander.internal.Maps;
import pl.compan.docusafe.core.EdmException;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class ManualSortedNonColumnField extends /*Ordered*/NonColumnField implements DictionarableField {

    public ManualSortedNonColumnField(String id, String cn, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn , names, type, refStyle, classname, required, requiredWhen, availableWhen);
    }

    public static boolean isManualSorted(Field field){
        return field instanceof NonColumnField
                && (!(field instanceof OrderPredicateProvider) || ((OrderPredicateProvider)field).isManualSorted());
    }

    /**
     * Zwraca map� wymaganych p�l na s�owniku
     * @return
     */
    @Override
    public Map<String, Field> getRequiredFields(){
        Map<String, Field> results = Maps.newHashMap();
        for(Field field : getFields()){
            if(field.isRequired()){
                results.put(field.getCn(), field);
            }
        }

        return results;
    }
}
