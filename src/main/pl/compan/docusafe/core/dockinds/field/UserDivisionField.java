package pl.compan.docusafe.core.dockinds.field;

import com.google.common.base.Function;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.primitives.Ints;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class UserDivisionField extends AbstractEnumColumnField
{
	private final String disc;
	private final String refField;

	StringManager sm = GlobalPreferences.loadPropertiesFile(UserDivisionField.class.getPackage().getName(), null);
	protected static Logger log = LoggerFactory.getLogger(UserDivisionField.class);

	private static final Multimap<String, UserDivisionField> divisionToField = Multimaps.synchronizedListMultimap(ArrayListMultimap.<String, UserDivisionField> create());

	public UserDivisionField(String disc, String refStyle, String id, String cn, String column, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, boolean sort, String refField) throws EdmException
	{
		super(id, cn, column, names, DSDIVISION, refStyle, null, required, requiredWhen, availableWhen);
		divisionToField.put(cn, this);
		this.disc = disc;
		this.refField = refField;
		// super(refStyle,id,cn,column,names,required,requiredWhen,availableWhen,false);
	}

	@Override
	public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException
	{
		if (value == null)
		{
			ps.setObject(index, null);
		}
		else
		{
			ps.setInt(index, simpleCoerce(value));
		}
	}

	public static void reloadForAll() throws EdmException
	{
		for (UserDivisionField field : divisionToField.values())
		{
			field.loadEnumItem();
		}
	}

    public void initialize() throws EdmException {
        loadEnumItem();
    }

  
    private synchronized void loadEnumItem() throws EdmException {
        Map<String, SortedSet<EnumItem>> refEnumItemsMap = new LinkedHashMap<String, SortedSet<EnumItem>>();
        //LinkedHashMap<Integer, EnumItem> enumItems = new LinkedHashMap<Integer, EnumItem>();
        SortedSet<EnumItem> availableEnumItems = new TreeSet<EnumItem>(EnumItem.COMPARATOR_BY_TITLE);
        SortedSet<EnumItem> enumItems = new TreeSet<EnumItem>(EnumItem.COMPARATOR_BY_TITLE);

        for (DSDivision division : DSDivision.getAllDivisions()) {
            if (division.isDivision()) {
                if (division.getId() > (long) Integer.MAX_VALUE) {
                    throw new RuntimeException("utrata dokładności - niemożliwe stworzenie pola");
                }

                boolean available = !division.isHidden();
                EnumItem ei = new EnumItemImpl(new Integer(division.getId().toString()), division.getGuid(), ImmutableMap.of("pl", division.getName()), new String[1], false, getCn(), available);
                enumItems.add(ei);
                if (available) {
                    availableEnumItems.add(ei);
                }

                if (getRefField() != null) {
                    DSUser[] users = division.getUsersWithDeleted(true);
                    for (DSUser user : users) {
                        if (!refEnumItemsMap.containsKey(user.getId().toString())) {
                            refEnumItemsMap.put(user.getId().toString(), new TreeSet<EnumItem>(EnumItem.COMPARATOR_BY_TITLE));
                        }
                        refEnumItemsMap.get(user.getId().toString()).add(ei);
                    }
                }
            }
        }

        ImmutableMap<Integer, EnumItem> enumItemsMap = Maps.uniqueIndex(enumItems, new Function<EnumItem, Integer>() {
            @Override
            public Integer apply(EnumItem input) {
                return input.getId();
            }
        });

        setEnumItems(enumItemsMap);
        setAvailableItems(availableEnumItems);
    }

	@Override
	public Object asObject(Object key) throws IllegalArgumentException, EdmException
	{
        if (key == null) {
            return null;
        } else {
            return getEnumItem((Integer) key);
        }
    }

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		int i = rs.getInt(getColumn());
		Integer value = rs.wasNull() ? null : new Integer(i);
		return new FieldValue(this, value);
	}

	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EntityNotFoundException
	{
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("", "--wybierz--");
		DSUser user;
		if (fieldsValues != null && fieldsValues.get(getCn()) != null)
			enumSelected.add(fieldsValues.get(getCn()).toString());
		else
			enumSelected.add("");
			String id = String.valueOf(fieldsValues.get("DSUSER"));
			
			try {
				user = DSUser.findById(Long.valueOf(id));
			
			DSDivision[] div = user.getDivisionsWithoutGroupPosition();
			for(int i=0;i<div.length;i++){
				enumValues.put(String.valueOf(div[i].getId()), div[i].getName());
			}
			} catch (NumberFormatException e) {
				log.error("", e);
			} catch (EdmException e) {
				log.error("", e);
			}
		return new EnumValues(enumValues, enumSelected);
	}

	public String getRefField()
	{
		return refField;
	}

    @Override
    public void setRefEnumItemsMap(Map<String, SortedSet<EnumItem>> refEnumItemsMap) {
        enumDefault.setRefEnumItemsMap(refEnumItemsMap);
    }

    @Override
    public void setAvailableItems(Set<EnumItem> availableItems) {
        enumDefault.setAvailableItems(availableItems);
    }

    @Override
    public void setAllItems(Set<EnumItem> allItems) {
        enumDefault.setAllItems(allItems);
    }

    @Override
    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, String id, String temp) throws EntityNotFoundException {
        return getEnumItemsForDwr(fieldsValues);
    }

    @Override
    public void setEnumItems(Map<Integer, EnumItem> enumItems) {
        enumDefault.setEnumItems(enumItems);
    }

    @Override
    public void setRepoField(boolean repoField) {
    }

    @Override
    public void setItemsReferencedForAll(Map<Integer, EnumItem> itemsReferencedForAll) {

    }

}
