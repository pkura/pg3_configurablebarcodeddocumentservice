package pl.compan.docusafe.core.dockinds.field.reader;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.DSDivisionEnumField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

import java.util.List;
import java.util.Map;

public class DSDivisionFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		DSDivisionEnumField enumField = new DSDivisionEnumField(elType.attributeValue("discrminator"), refStyle, el.attributeValue("id"), el.attributeValue("cn"), el.attributeValue("column"), names, required, requiredWhen, availableWhen, "true".equals(el.attributeValue("sort")),
				elType.attributeValue("ref-field"));

        enumField.initialize();

		return enumField;
	}

}
