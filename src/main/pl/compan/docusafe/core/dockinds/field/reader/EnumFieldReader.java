package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.field.XmlEnumNonColumnField.Sort;
import pl.compan.docusafe.core.dockinds.field.XmlEnumColumnField.Sorted;
import pl.compan.docusafe.core.dockinds.field.XmlEnumNonColumnField;
import pl.compan.docusafe.core.dockinds.field.XmlEnumColumnField;

import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.util.XmlUtils;

public class EnumFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, 
	    		List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {

        String column = el.attributeValue("column");

         if(column != null){
            XmlEnumColumnField enumField = new XmlEnumColumnField(refStyle, el.attributeValue("id"), el.attributeValue("cn"), el.attributeValue("column"), names, required, requiredWhen, availableWhen,"true".equals(el.attributeValue("sort")));
	        if (elType.attributeValue("class") != null) {
	            // warto�ci wyliczeniowe dynamiczne - na podstawie obiekt�w danej klasy
	            enumField.setClassname(elType.attributeValue("class"));
	            try {
	                Class c = Class.forName(enumField.getClassname());
	                List<EnumItem> list = (List<EnumItem>) Finder.list(c, "id");
	                enumField.setEnumItems(list);
	            } catch (ClassNotFoundException e) {
	                throw new EdmException("Nie znaleziono definicji klasy " + enumField.getClassname(), e);
	            }
	        } else {
	            // warto�ci wyliczeniowe sta�e
	            List elItems = elType.elements("enum-item");
	            if (elItems != null && elItems.size() > 0) {
	                for (Iterator itemIter = elItems.iterator(); itemIter.hasNext();) {
	                    Element elItem = (Element) itemIter.next();
	                    // wczytywanie nazwy warto�ci wyliczeniowej w r�znych j�zykach
	                    Map<String, String> titles = XmlUtils.readLanguageValues(elItem, "title");
	                    // wczytywanie dodatkowych argument�w
	                    List<String> args = new ArrayList<String>(6);
	                    for (int i = 1; i <= 5; i++) {
	                        String attrName = "arg" + i;
	                        if (elItem.attributeValue(attrName) != null) {
	                            args.add(elItem.attributeValue(attrName));
	                        } else {
	                            break;
	                        }
	                    }
	                    boolean available = true;
	                    if(elItem.attributeValue("available") != null){
	                        available = "true".equalsIgnoreCase(elItem.attributeValue("available") );
	                    }
	                    enumField.addEnum(new Integer(elItem.attributeValue("id")), elItem.attributeValue("cn"), titles, args.toArray(new String[args.size()]), "true".equals(elItem.attributeValue("force-remark")),enumField.getCn(),available);
	                }
	            }
	        }
             String sort = elType.attributeValue("sort");
             if (sort != null)
             {
                 enumField.setSort(Sorted.valueOf(sort.toUpperCase()));
             }
            Field field;
	        field = enumField;
	        field.setDisabled(disabled);
	        return field;
         } else {
            XmlEnumNonColumnField enumField = new XmlEnumNonColumnField(refStyle, el.attributeValue("id"), el.attributeValue("cn"), names, required, requiredWhen, availableWhen,"true".equals(el.attributeValue("sort")));
            if (elType.attributeValue("class") != null) {
                // warto�ci wyliczeniowe dynamiczne - na podstawie obiekt�w danej klasy
                enumField.setClassname(elType.attributeValue("class"));
                try {
                    Class c = Class.forName(enumField.getClassname());
                    List<EnumItem> list = (List<EnumItem>) Finder.list(c, "id");
                    enumField.setEnumItems(list);
                } catch (ClassNotFoundException e) {
                    throw new EdmException("Nie znaleziono definicji klasy " + enumField.getClassname(), e);
                }
            } else {
                // warto�ci wyliczeniowe sta�e
                List elItems = elType.elements("enum-item");
                if (elItems != null && elItems.size() > 0) {
                    for (Iterator itemIter = elItems.iterator(); itemIter.hasNext();) {
                        Element elItem = (Element) itemIter.next();
                        // wczytywanie nazwy warto�ci wyliczeniowej w r�znych j�zykach
                        Map<String, String> titles = XmlUtils.readLanguageValues(elItem, "title");
                        // wczytywanie dodatkowych argument�w
                        List<String> args = new ArrayList<String>(6);
                        for (int i = 1; i <= 5; i++) {
                            String attrName = "arg" + i;
                            if (elItem.attributeValue(attrName) != null) {
                                args.add(elItem.attributeValue(attrName));
                            } else {
                                break;
                            }
                        }
                        boolean available = true;
                        if(elItem.attributeValue("available") != null){
                            available = "true".equalsIgnoreCase(elItem.attributeValue("available") );
                        }
                        enumField.addEnum(new Integer(elItem.attributeValue("id")), elItem.attributeValue("cn"), titles, args.toArray(new String[args.size()]), "true".equals(elItem.attributeValue("force-remark")),enumField.getCn(),available);
                    }
                }
            }
             String sort = elType.attributeValue("sort");
             if (sort != null)
             {
                 enumField.setSort(Sort.valueOf(sort.toUpperCase()));
             }
            enumField.setDisabled(disabled);
            return enumField;

        }

    }
}

