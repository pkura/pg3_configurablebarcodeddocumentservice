package pl.compan.docusafe.core.dockinds.field;

import java.util.List;


import pl.compan.docusafe.core.base.AttachmentRevision;

public class DocListBean
{
	private Long id;
	private String title;
	private List<EditField> editFields;
	private AttachmentRevision attachmentRevision;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setAttachmentRevision(AttachmentRevision attachmentRevision) {
		this.attachmentRevision = attachmentRevision;
	}

	public AttachmentRevision getAttachmentRevision() {
		return attachmentRevision;
	}

	public void setEditFields(List<EditField> editFields) {
		this.editFields = editFields;
	}

	public List<EditField> getEditFields() {
		return editFields;
	}

}
