package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.core.dockinds.field.HtmlEditorColumnField;
import pl.compan.docusafe.core.dockinds.field.HtmlEditorNonColumnField;

public class HtmlEditorFieldReader implements FieldReader {

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType,
			String refStyle, boolean required, List<Condition> availableWhen,
			List<Condition> requiredWhen, Map<String, String> names,
			boolean disabled) throws EdmException {
	
		if(el.attributeValue("column") != null ){
			HtmlEditorColumnField field = new HtmlEditorColumnField(el.attributeValue("id"), el.attributeValue("column"), elType.attributeValue("logic"), el.attributeValue("cn"), names, required, requiredWhen, availableWhen, null);
			return field;
		}else{
			HtmlEditorNonColumnField field = new HtmlEditorNonColumnField(el.attributeValue("id"), elType.attributeValue("logic"), el.attributeValue("cn"), names, required, requiredWhen, availableWhen, null);
			boolean reset = Boolean.parseBoolean(elType.attributeValue("reset"));
			field.setResettable(reset);
			return field;
		}
	}

}
