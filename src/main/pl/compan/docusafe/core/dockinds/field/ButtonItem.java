package pl.compan.docusafe.core.dockinds.field;

public interface ButtonItem {
	boolean clicked();
	boolean hidden();
}
