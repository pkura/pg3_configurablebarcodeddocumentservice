package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;

/**
 * Klasa nas�uchuj�ca zdarze� archiwizacji dokumentu
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface ArchiveListener {
    void onArchive(Document document, ArchiveSupport as) throws EdmException;
}
