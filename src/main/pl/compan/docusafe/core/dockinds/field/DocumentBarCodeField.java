package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.Database;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jtarasiuk
 * Date: 21.06.13
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
public class DocumentBarCodeField  extends OrderedNonColumnField implements CustomPersistence, SearchPredicateProvider
{
    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(pl.compan.docusafe.core.dockinds.field.DocumentBarCodeField.class.getPackage().getName(), null);
    protected static Logger log = LoggerFactory.getLogger(DocumentReferenceIdField.class);
    public DocumentBarCodeField(String id, String cn, String refStyle,  Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
    {
        super(new Database.Column("barcode"), id, cn, names, pl.compan.docusafe.core.dockinds.field.Field.DOCUMENT_BARCODE, null, null, required, requiredWhen, availableWhen);
    }
    @Override
    public void persist(Document doc, Object key) throws EdmException {
    	 
	   	if (key != null && key  instanceof String)
        {
    	    if (doc instanceof InOfficeDocument)
    	        ((InOfficeDocument) doc).setBarcode((String) key);
    	    else if (doc instanceof OutOfficeDocument)
    		    ((OutOfficeDocument) doc).setBarcode((String) key);
        } 
	}

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
    {
    	
    	OfficeDocument doc = OfficeDocument.find(documentId);
		if (doc instanceof OutOfficeDocument ) 
			return new FieldValue(this, ((OutOfficeDocument) doc).getBarcode(),doc.getBarcode());
		else if (doc instanceof InOfficeDocument)
			return new FieldValue(this, ((InOfficeDocument) doc).getBarcode(),doc.getBarcode());
		else
			  return new FieldValue(this, doc.getBarcode(), doc.getBarcode());
    	/*
        OfficeDocument doc = OfficeDocument.find(documentId);
        return new FieldValue(this, doc.getBarcode(), doc.getBarcode());*/
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException
    {
        if (value == null)
            return null;
        String barcode = null;
        if (value instanceof String)
            barcode = (String) value;
        else
        {
            throw new FieldValueCoerceException(this.getName() + ":" + sm.getString("NieMoznaSkonwertowacWartosci") + " " + value + " " + "do Stringa");
        }
        return barcode;
    }

    
    @Override
    public Field getDwrField() throws EdmException
    {
        pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING);
        field.setDisabled(true);
        return field;
    }

    public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) 
	{
		String  valS = (String) values.get(this.getCn());
		log.debug(" {} {} ",valS);
        if (valS != null) {
            DocumentKind kind = dockindQuery.getDocumentKind();
            dockindQuery.barcode(valS, kind.getDockindInfo().isForType(DocumentType.INCOMING));
        }
	}
}
