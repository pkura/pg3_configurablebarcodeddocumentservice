package pl.compan.docusafe.core.dockinds.field;

import com.beust.jcommander.internal.Maps;
import com.google.common.collect.Lists;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrSearchable;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.util.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class FieldsManagerUtils {

    protected static Logger log = LoggerFactory.getLogger(FieldsManagerUtils.class);


    /**
     * @return data if found object is instance of cls, or null
     */
    public static <T> T getData(Map<String, ?> fieldValues, String fieldCn, Class<T> cls, T defaultValue) {
        T data = getData(fieldValues, fieldCn, cls);
        return data == null ? defaultValue : data;
    }
    /**
     * @return data if found object is instance of cls, or null
     */
    public static <T> T getData(FieldsManager fm, String fieldCn, Class<T> cls) throws EdmException {
        return getData(fm.getFieldValues(), fieldCn, cls);
    }
    /**
     * @return data if found object is instance of cls, or null
     */
    public static <T> T getData(FieldsManager fm, String fieldCn, Class<T> cls, T defaultValue) throws EdmException {
        return getData(fm.getFieldValues(), fieldCn, cls, defaultValue);
    }
    /**
     * @return data if found object is instance of cls, or null
     */
    public static <T> T getData(Map<String, ?> fieldValues, String fieldCn, Class<T> cls) {
        Object o = fieldValues.get(fieldCn);
        if (o != null)
            if (cls.isAssignableFrom(o.getClass()))
                //if (o.getClass().equals(cls))
                return cls.cast(o);
        return null;
    }

    public static String getFieldName (FieldsManager fm, String cn, String defaultVal) throws EdmException {
        Field field = fm.getField(cn);
        if (field == null) return defaultVal;
        return field.getName();
    }
    public static String getFieldName (FieldsManager fm, String cn, boolean cnIsDefaultVal) throws EdmException {
        String name = getFieldName(fm, cn, null);
        return name != null ? name : cnIsDefaultVal ? cn : null;
    }

//
//    Object kontoObj = ((Map)el).get("DEKRETACJA_KSIEG_CONNECTED_TYPE_ID");
//    Object naliczPodatekObj = ((Map)el).get("DEKRETACJA_KSIEG_GROUPID");
//    Object wmObj = ((Map)el).get("DEKRETACJA_KSIEG_GROUPID");
//
//    String konto = null;


    /**
     * Pobranie pola ze s�ownika wielowarto�ciowego w FieldsManager-e
     */
    public static Field getFieldFromMultiple(FieldsManager fm, String multipleDictionaryCn, String subFieldCn, boolean addSuffix_1) throws EdmException {
        if (fm == null) return null;

        Field multipleDictionary = fm.getField(multipleDictionaryCn);
        if (multipleDictionary == null) return null;

        List<Field> subFields = multipleDictionary.getFields();
        if (subFields == null) return null;

        for(Field field : subFields)
            if (field.getCn().equals(subFieldCn + (addSuffix_1 ? "_1" : "")))
                return field;

        return null;
    }

    /**
     * Pobranie etykiety cn (wy�wietlanej warto�ci) enum-a na podstawie id
     *
     * @param selectedValueId wybrana, wy�wietlana warto�c slownika, obiekt typu Integer, Long lub String
     */
    public static String getEnumLabel (Field enumField, Object selectedValueId) {
        EnumItem item = getEnumItem(enumField,selectedValueId);
        if (item != null)
            return item.getCn();
        return null;
    }
    /**
     * Pobranie etykiety (wy�wietlanej warto�ci) enum-a na podstawie id
     *
     * @param selectedValueId wybrana, wy�wietlana warto�c slownika, obiekt typu Integer, Long lub String
     * @param dictCn
     * @param dictFieldCn
     */
    public static String getEnumLabel(FieldsManager fm, String fieldCn, Object selectedValueId) throws EdmException {
        pl.compan.docusafe.core.dockinds.field.Field enumField = fm.getField(fieldCn);
        String value = enumField != null ? FieldsManagerUtils.getEnumLabel(enumField, selectedValueId) : null;
        return value;
    }

    /**
     * Pobranie etykiety (wy�wietlanej warto�ci) enum-a na podstawie id
     *
     * @param selectedValueId wybrana, wy�wietlana warto�c slownika, obiekt typu Integer, Long lub String
     */
    public static String getEnumTitle (Field enumField, Object selectedValueId) {
        EnumItem item = getEnumItem(enumField,selectedValueId);
        if (item != null)
            return item.getTitle();
        return null;
    }
    /**
     * Pobranie etykiety (wy�wietlanej warto�ci) enum-a na podstawie id
     *
     * @param selectedValueId wybrana, wy�wietlana warto�c slownika, obiekt typu Integer, Long lub String
     * @param fieldCn
     */
    public static String getEnumTitle (FieldsManager fm, String fieldCn, Object selectedValueId) throws EdmException {
        pl.compan.docusafe.core.dockinds.field.Field enumField = fm.getField(fieldCn);
        String value = enumField != null ? FieldsManagerUtils.getEnumTitle(enumField, selectedValueId) : null;
        return value;
    }
    /**
     * Pobranie wy�wietlanego elementu enum-a na podstawie id
     *
     * @param selectedValueId wybrana, wy�wietlana warto�c slownika, obiekt typu Integer, Long lub String
     */
    public static EnumItem getEnumItem (Field enumField, Object selectedValueId) {
        if (enumField == null || selectedValueId == null)
            return null;
        if (!(selectedValueId instanceof Integer || selectedValueId instanceof Long || selectedValueId instanceof String))
            return null;

        List<EnumItem> items;
        try {
            items = enumField.getEnumItems();
        } catch (Exception e) {
            log.error("Pola, dla kt�rego za��dano pobrania enumItems nie jest odpowiedniego typu", e);
            return null;
        }

        for (EnumItem item : items) {
            if (item.getId() != null) {
                if ((selectedValueId instanceof String && selectedValueId.equals(Long.toString(item.getId())))
                        //|| (selectedValueId instanceof Integer && selectedValueId.equals(item.getId()))
                        //|| (selectedValueId instanceof Long && selectedValueId.equals((long) item.getId()))
                        || selectedValueId.equals(item.getId()))
                    return item;
            }
        }
        return null;
    }

    public static EnumItem getEnumItem(FieldsManager fm, String cn) throws EdmException {
//        Object enumValue = fm.getValue(cn);
//        if (enumValue!=null){
//            String enumTitle = enumValue.toString();
//            List<EnumItem> items = fm.getField(cn).getEnumItems();
//            for(EnumItem item : items){
//                if(item.getTitle().equals(enumTitle));
//                    return item;
//            }
//        }
//        return null;
        return fm.getEnumItem(cn);
    }

    public static <T> List<T> getDictionaryItems (FieldsManager fm, String dictionaryCn, String fieldCn, boolean joinFieldWithDictCn, Class<T> clazz) throws EdmException {

        if (joinFieldWithDictCn)
            fieldCn = dictionaryCn +"_"+fieldCn;
        List<Map<String, Object>> allItems = getDictionaryItems(fm, dictionaryCn, new FieldProperty(fieldCn, clazz));

        List<T> items = new ArrayList<T>();
        if (allItems.size()<1) return items;

        for(Map<String,Object> hm : allItems){
            Object hmItem = hm.get(fieldCn);
            if (hmItem!=null)
                items.add(clazz.cast(hmItem));
            else
                log.error("FieldsManagerUtils.getDictionaryItems - w mapach wyszukanych musza byc takie elementy: "+fieldCn);
        }

        return items;
    }

    public static <T> List<T> getDictionaryItems (FieldsManager fm, String dictionaryCn, String fieldCn, Class<T> clazz) throws EdmException {
        return getDictionaryItems(fm, dictionaryCn, fieldCn, false, clazz);
    }


    /**
     * Lista warto�ci wybranej kolumny z danego s�ownika
     */
    public static <T> List<T> getSimpleDictionaryItems (FieldsManager fm, String dictionaryCn, String dictfieldCn, Class<T> clazz, boolean joinFieldCnWithDictCn) throws EdmException {
        List<T> simpleValues = new ArrayList<T>();
        dictfieldCn = joinFieldCnWithDictCn ? joinDictionaryWithField(dictionaryCn, dictfieldCn) : dictfieldCn;

        List<Map<String, Object>> values = FieldsManagerUtils.getDictionaryItems(fm, dictionaryCn,
                new FieldsManagerUtils.FieldProperty(dictfieldCn, clazz));

        for (Map o : values)
            if (o.containsKey(dictfieldCn))
                simpleValues.add((T) o.get(dictfieldCn));

        return simpleValues;
    }
    /**
     * Zbi�r warto�ci wybranej kolumny z danego s�ownika wraz z id wpisu
     */
    public static <T> Map<Integer,T> getSimpleDictionaryItemsWithId (FieldsManager fm, String dictionaryCn, String dictfieldCn, Class<T> clazz, boolean joinFieldCnWithDictCn) throws EdmException {
        Map<Integer,T> simpleValues = new HashMap<Integer,T>();
        dictfieldCn = joinFieldCnWithDictCn ? joinDictionaryWithField(dictionaryCn, dictfieldCn) : dictfieldCn;
        String dictfieldIdCn = joinFieldCnWithDictCn ? joinDictionaryWithField(dictionaryCn, "ID") : dictfieldCn;

        List<Map<String, Object>> values = FieldsManagerUtils.getDictionaryItems(fm, dictionaryCn,
                new FieldsManagerUtils.FieldProperty(dictfieldCn, clazz),
                new FieldsManagerUtils.FieldProperty(dictfieldCn, Integer.class));

        for (Map o : values)
            if (o.containsKey(dictfieldCn))
                simpleValues.put( (Integer)o.get(dictfieldIdCn), (T) o.get(dictfieldCn) );

        return simpleValues;
    }

    public static List<Map<String,Object>> getDictionaryItems (FieldsManager fm, String dictionaryCn, String... fieldsCns) throws EdmException {
        return getDictionaryItems(fm,dictionaryCn,toFieldPropertyArray(fieldsCns));
    }

    public static List<Map<String,Object>> getDictionaryItems (FieldsManager fm, String dictionaryCn, FieldProperty... fieldsProperties) throws EdmException {
        if (fieldsProperties==null || fieldsProperties.length==0) return new ArrayList();

        List<Map<String,Object>> items = new ArrayList<Map<String, Object>>();

        Object efekty = fm.getValue(dictionaryCn);
        if (efekty != null && efekty instanceof List) {
            for (Object efekt : (List) efekty) {
                if (efekt instanceof Map) {
                    Map<String,Object> item = new HashMap<String, Object>();
                    for (FieldProperty fp : fieldsProperties){
                        Object i = ((HashMap) efekt).get(fp.cn);
//                        if (i!=null && fp.clazz.equals(i.getClass()))
//                            item.put(fp.cn,fp.clazz.cast(i));
                        if (i!=null && fp.clazz.isAssignableFrom(i.getClass()))
                            item.put(fp.cn,fp.clazz.cast(i));
                    }
                    if (!item.isEmpty())
                        items.add(item);
                }
            }
        }

        return items;
    }

    protected static String joinDictionaryWithField(String dictionaryCn, String fieldCn) {
        return dictionaryCn + '_' + fieldCn;
    }
//    public static List<Map<String,Object>> getMultipleDictionaryValues (FieldsManager fm, String dictionaryCn, boolean cutDictionaryPrefix) throws EdmException {
//
//        List<Map<String,Object>> items = new ArrayList<Map<String, Object>>();
//        String dictionaryPrefix = dictionaryCn+"_";
//        int dictionaryPrefixLength = dictionaryPrefix.length();
//
//        Object multipleValues = fm.getValue(dictionaryCn);
//        if (multipleValues instanceof List){
//            for(Object values : (List)multipleValues){
//                if (values instanceof Map){
//                    Map<String,Object> item = new HashMap<String, Object>();
//
//                    Map<String,Object> valuesKV = (Map<String,Object>)values;
//                    for (Map.Entry<String,Object> kv : valuesKV.entrySet()){
//                        String key = kv.getKey();
//                        Object val = kv.getValue();
//
//                        if(key.startsWith(dictionaryPrefix)){
//                            if (cutDictionaryPrefix)
//                                key = key.substring(dictionaryPrefixLength);
//
//                            item.put(key,val);
//                        }
//                    }
//
//                    if (!item.isEmpty())
//                        items.add(item);
//                }
//            }
//        }
//
//        return items;
//    }

    public static EnumValues getSimpleEnumValues(DSUser user) {
        return getSimpleEnumValues(Long.toString(user.getId()),user.getWholeName());
    }

    public static EnumValues getEmptyEnumValues() {
        StringManager sm = GlobalPreferences.loadPropertiesFile(FieldsManagerUtils.class.getPackage().getName(), null);
        Map<String,String> allOptions = new HashMap<String, String>();
        allOptions.put("", sm.getString("select.wybierz"));
        //Collections.singletonMap(Long.toString(user.getId()),user.getWholeName())
        return new EnumValues(allOptions, new ArrayList<String>());
    }

    public static EnumValues getSimpleEnumValues(String enumId, String enumValue) {
        StringManager sm = GlobalPreferences.loadPropertiesFile(FieldsManagerUtils.class.getPackage().getName(), null);

        Map<String,String> allOptions = new HashMap<String, String>();
        allOptions.put("", sm.getString("select.wybierz"));
        allOptions.put(enumId,enumValue);
        //Collections.singletonMap(Long.toString(user.getId()),user.getWholeName())
        return new EnumValues(allOptions, Collections.singletonList(enumId));
    }

    public static EnumValues getUserEnumValues(Collection<DSUser> usersCollection) {
        StringManager sm = GlobalPreferences.loadPropertiesFile(FieldsManagerUtils.class.getPackage().getName(), null);

        Map<String,String> allOptions = new HashMap<String, String>();
        allOptions.put("", sm.getString("select.wybierz"));
        for (DSUser user : usersCollection)
            allOptions.put(Long.toString(user.getId()),user.getWholeName());

        return new EnumValues(allOptions, Collections.singletonList(""));
    }

    public static EnumValues getEnumValues(Collection<EnumItem> items) {
        StringManager sm = GlobalPreferences.loadPropertiesFile(FieldsManagerUtils.class.getPackage().getName(), null);

        Map<String,String> allOptions = new HashMap<String, String>();
        allOptions.put("", sm.getString("select.wybierz"));
        for(EnumItem item : items)
            allOptions.put(Integer.toString(item.getId()),item.getTitle());
        return new EnumValues(allOptions, new ArrayList<String>());
    }
    public static DSUser getUserFromEnum (FieldsManager fm, String enumFieldCn) {
        try {
            EnumItem enumField = fm.getEnumItem(enumFieldCn);
            if (enumField!=null)
                return DSUser.findById(enumField.getId().longValue());
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return null;
    }
    public static String getUserWholeNameFromEnum(FieldsManager fm, String enumFieldCn, boolean emptyIfNotFound) {
        DSUser user = getUserFromEnum(fm, enumFieldCn);
        return getUserWholeName(user, emptyIfNotFound);

    }
    public static String getUserNameFromAcceptance(FieldsManager fm, String taskCn) {
        try {
            Long docId = fm.getDocumentId();
            List<DocumentAcceptance> accList = DocumentAcceptance.find(docId);
            for (int i = 0; i < accList.size(); ++i){
                DocumentAcceptance acc = accList.get(i);

                if (taskCn.equalsIgnoreCase(acc.getAcceptanceCn())){
                    return acc.getUsername();
                }
            }

            log.error("Document accepteance for task cn \""+taskCn+"\" has not been found");
        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        }
        return null;
    }
    public static DSUser getUserFromAcceptance(FieldsManager fm, String taskCn) {
        String username = getUserNameFromAcceptance(fm, taskCn);
        return findUserSafely(username);
    }
    public static String getUserNameFromLastAcceptance(FieldsManager fm) {
        try {
            Long docId = fm.getDocumentId();
            List<DocumentAcceptance> accList = DocumentAcceptance.find(docId);
            for (int i = 0; i < accList.size(); ++i){
                DocumentAcceptance acc = accList.get(i);
                return acc.getUsername();
            }

            log.error("There is no document accepteance");
        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        }
        return null;
    }
    public static DSUser getUserFromLastAcceptance(FieldsManager fm) {
        String username = getUserNameFromLastAcceptance(fm);
        return findUserSafely(username);
    }
    private static DSUser findUserSafely(String username) {
        if (username != null) {
            try{
                DSUser user = DSUser.findByUsername(username);
                return user;
            } catch (EdmException e) {
                log.error(e.getMessage(),e);
            }
        }
        return null;
    }
    public static String getUserWholeNameFromAcceptance(FieldsManager fm, String taskCn, boolean emptyIfNotFound) {
        DSUser user = getUserFromAcceptance(fm, taskCn);
        return getUserWholeName(user, emptyIfNotFound);
    }

    public static String getUserWholeName(DSUser user, boolean emptyIfNotFound){
        return user!=null ? user.getWholeName() : emptyIfNotFound ? "" : null;
    }

    /**
     * Konwertuje map� String,Object na map� string,Object u�ywan� w validatorach
     * @param values
     * @param fm
     * @return
     */
    public static Map<String, Object> convert(Map<String, Object> values, FieldsManager fm) throws EdmException {
        Map<String,Object> results = Maps.newHashMap();

        for(Map.Entry<String,Object> v : values.entrySet()){
            Field field = fm.getField(v.getKey());
            if(field == null)  // nie ma takiego pola
                continue;
            Object object = field.coerce(v.getValue()); //skonwertowana warto��
            results.put(v.getKey(), object);
        }

        log.info("fielddata maps {}", results);
        return results;
    }

    public static class FieldProperty{
        public final String cn;
        public final Class clazz;

        public FieldProperty(String dictCn, String fieldCn, Class clazz) {
            this.cn = dictCn+"_"+fieldCn;
            this.clazz = clazz;
        }
        public FieldProperty(String fieldCnWithDictCn, Class clazz) {
            this.cn = fieldCnWithDictCn;
            this.clazz = clazz;
        }
        public FieldProperty(String cn) {
            this(cn,Object.class);
        }
    }
    public static FieldProperty[] toFieldPropertyArray(String... fieldsCns){
        if (fieldsCns==null) return null;
        if (fieldsCns.length==0) return new FieldProperty[0];
        FieldProperty[] fpa = new FieldProperty[fieldsCns.length];
        for(int i = 0; i < fieldsCns.length; ++i)
            fpa[i] = new FieldProperty(fieldsCns[i]);
        return fpa;
    }

    public static <T> T getDictionaryField (FieldsManager fm, String dictionaryCn, String dictFieldCn, Class<T> clazz){
        Field field = null;
        try {
            field = fm.getField(dictionaryCn);
            return getDictionaryField(field, dictFieldCn, clazz);
        } catch (EdmException e) {
            return null;
        }
    }
    public static <T> T getDictionaryField (Field field, String dictFieldCn, Class<T> clazz){
        if (field == null || !field.getType().equalsIgnoreCase("dictionary"))
            return null;

        DictionaryField dict = (DictionaryField)field;
        Object dictField = dict.getDictionaryFieldByCn(dictFieldCn);

        if (dictField!=null && clazz.isAssignableFrom(dictField.getClass()))
            return clazz.cast(dictField);

        return null;
    }



    /**
     * Zwraca warto�ci ze s�ownika o przekazanym dictionaryId
     * @param fm
     * @param dictCn
     * @param dictionaryId
     * @param getter
     * @return
     * @throws EdmException
     */
    public static Map<String, Object> getDictionaryExtractedValues(FieldsManager fm, String dictCn, Long dictionaryId ,FieldValuesGetter getter) throws EdmException {

        final Map<String, Object> ret = DwrDictionaryFacade.getDictionary(fm.getField(dictCn), null).getValues(dictionaryId.toString());

        return ret;
    }

    /**
     * Zwraca obiekt slownika DWR, mo�e by� u�ywana w momencie gdy nie mamy dokumentu, a potrzebujemy obiekt s�ownika
     * @param dockind
     * @param dictionaryCn
     * @return
     */
    public static DwrSearchable getDictionaryFromDockind(String dockind, String dictionaryCn) throws EdmException {
        return DwrDictionaryFacade.getDictionary(DocumentKind.findByCn(dockind).getFieldByCn( dictionaryCn), null);
    }

    /**
     * Zwraca obiekt slownika DWR, mo�e by� u�ywana w momencie gdy nie mamy dokumentu, a potrzebujemy obiekt s�ownika
     * @param dockind
     * @param dictionaryCn
     * @return
     */
    public static DwrSearchable getDictionaryFromDockind(DocumentKind dockind, String dictionaryCn) throws EdmException {
        return DwrDictionaryFacade.getDictionary(dockind.getFieldByCn( dictionaryCn), null);
    }


     /**
     * @TODO nie jestem pewien czy metoda dzia�a poprawnie.
     * @param fm
     * @param dictCn
     * @param getter
     * @return
     * @throws EdmException
     */
    public static List<Map<String, Object>> getMultiDictionaryExtractedValues(FieldsManager fm, String dictCn, FieldValuesGetter getter) throws EdmException {
        List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();

        List<Map<String, Object>> dictRows = (List<Map<String, Object>>) fm.getValue(dictCn);
        if (dictRows == null || dictRows.size() == 0)
        {
            Object keys = fm.getKey(dictCn);
            if (keys != null)
            {
                List<Long> ids = (List<Long>) keys;
                dictRows = Lists.newLinkedList();
                for (Long key : ids)
                    dictRows.add(DwrDictionaryFacade.getDictionary(fm.getField(dictCn), null).getValues(key.toString()));
            }
        }

        if (dictRows != null)
        {
            for (Map<String, Object> row : dictRows) {
                Map<String, Object> dictRowValues = new HashMap<String, Object>();

                for (Map.Entry<String, Object> rowVal : row.entrySet()) {
                    Object rowValue = getter.getValue(rowVal.getValue());
                    dictRowValues.put(rowVal.getKey(), rowValue);
                }

                values.add(dictRowValues);
            }
        }

        return values;
    }

    public static class FieldValuesGetter {

        private FieldValuesGetter[] getters;
        private boolean nullForUnsupported;

        public FieldValuesGetter(boolean nullForUnsupported, FieldValuesGetter... getters) {
            this.getters = getters==null ? new FieldValuesGetter[0] : getters;
            this.nullForUnsupported = nullForUnsupported;
        }

        public final Object getValue(Object value) {
            if (value==null)
                return nullForUnsupported ? null : value;

            for(FieldValuesGetter getter : getters){
                Object extractedValue = getter.getExtractedValue(value);
                if (extractedValue != null)
                    return extractedValue;
            }

            return nullForUnsupported ? null : value;
        }

        protected Object getExtractedValue(Object value) {
            return nullForUnsupported ? null : value;
        }
    }

    public static class EnumValuesGetter extends FieldValuesGetter{

        public EnumValuesGetter(boolean nullForUnsupported) {
            super(nullForUnsupported);
        }

        @Override
        public Object getExtractedValue(Object value) {
            if (value instanceof EnumValues)
                return ((EnumValues) value).getSelectedValue();
            return super.getExtractedValue(value);
        }
    }

    public static class DateValuesGetter extends FieldValuesGetter{

        public DateValuesGetter(boolean nullForUnsupported) {
            super(nullForUnsupported);
        }

        @Override
        public Object getExtractedValue(Object value) {
            if (value instanceof Date)
                return DateUtils.formatCommonDate((Date) value);
            return super.getExtractedValue(value);
        }
    }

    public static class DateEnumValuesGetter extends FieldValuesGetter{

        public DateEnumValuesGetter(boolean nullForUnsupported) {
            super(nullForUnsupported, new DateValuesGetter(true), new EnumValuesGetter(true));
        }
    }
}
