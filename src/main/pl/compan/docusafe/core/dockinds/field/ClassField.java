package pl.compan.docusafe.core.dockinds.field;

import com.google.common.collect.Lists;
import org.apache.log4j.pattern.ClassNamePatternConverter;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dictionary.DocumentKindDictionary;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ClassField extends Field{
    private final static Logger LOG = LoggerFactory.getLogger(ClassField.class);

    /**
     * @param id            id z xml'a, musi by� unikalne
     * @param cn            cn z xml'a, musi b� unikalne
     * @param column        nazwa kolumny, nie mo�e by� null (niestety)
     * @param names         nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param refStyle      styl wy�wietlania powi�zanych warto�ci, mo�e by� null, niekt�re fieldy tego potrzebuj�, ale wszystkie maj� takie pole
     * @param classname     niekt�re fieldy tego potrzebuj�, ale wszystkie maj� takie pole
     * @param required      czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen  warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws pl.compan.docusafe.core.EdmException
     */
    public ClassField(String id, String cn, String column, Map<String, String> names, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, column, names, Field.CLASS, refStyle, classname, required, requiredWhen, availableWhen);
    }

    @Override
    public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
        if(value == null){
            ps.setObject(index, null);
        } else {
            ps.setLong(index, simpleCoerce(value));
        }
    }

    @Override
    public Object asObject(Object key) throws IllegalArgumentException, EdmException {
        if(key == null) {
            return null;
        }
        if(key instanceof Collection){
            List ret = Lists.newArrayList();
            for(Object id : (Collection) key){
                ret.add(asObject(id));
            }
            return ret;
        } else {
            return DSApi.context().session().get(classname, (Serializable) key);
        }
    }

    @Override
    public Long simpleCoerce(Object value) throws FieldValueCoerceException {
        if(value instanceof DocumentKindDictionary) {
           return ((DocumentKindDictionary)value).getId();
        } else {
            return FieldUtils.longCoerce(this,value);
        }
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        long l = rs.getLong(getColumn());
        Long value = rs.wasNull() ? null : new Long(l);
        return new FieldValue(this, value);
    }
    
    public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
		pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+getCn(), getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY);
		field.setDictionary(new DwrDictionaryBase(this, null));
		return field;
	}

    @Override
    public Object provideDescription(Object value) {
        if(value == null) {
            return null;
        }

        Object object = invokeMethod("getDescription", "getDictionaryDescription", value);
        if(object == null)
            return "";
        if (object instanceof String) {
        	 return (String) object;
        } else
            throw new IllegalStateException("Wynik funkcji 'getDictionaryDescription' jest innego typu ni� 'String'");
    }


}