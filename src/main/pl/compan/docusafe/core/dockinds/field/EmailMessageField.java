package pl.compan.docusafe.core.dockinds.field;

import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.dom4j.Element;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.HtmlEditorValue;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.EmailUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class EmailMessageField extends NonColumnField {
	
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);
	private static final Logger log = LoggerFactory.getLogger(EmailMessageField.class);
	public static final String MESSAGE_ID_COLUMN = "MESSAGE_ID";
	private final ContentType contentType;
	public enum ContentType
	{
		EMAIL_SUBJECT, EMAIL_SENDER, EMAIL_SENTDATE, EMAIL_BODY, EMAIL_ATTACHMENT
	}
	
	public EmailMessageField(String id, String cn, Map<String, String> names, Element elType, String contentName, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
		super(id, cn, names, Field.EMAIL_MESSAGE, null, null, required, requiredWhen, availableWhen);
		this.contentType = ContentType.valueOf(contentName.toUpperCase());
		
		if(this.contentType == ContentType.EMAIL_SENTDATE){
			if(elType.attributeValue("hours") != null){
	     	   addParam("hours", "true".equals(elType.attributeValue("hours")));
	        }
	        if(elType.attributeValue("minutes") != null){
	     	   addParam("minutes", "true".equals(elType.attributeValue("minutes")));
	        } 
	        if(elType.attributeValue("seconds") != null){
	     	   addParam("seconds", "true".equals(elType.attributeValue("seconds")));
	        }  
		}
	}
	
	
	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs)	throws SQLException, EdmException {
		Long messageId = null; 
		Document doc = Document.find(documentId);
		try {
			if(doc instanceof InOfficeDocument){
				InOfficeDocument inDoc = ((InOfficeDocument)doc);
				messageId = inDoc.getMessageId();
				//	nie znaleziono powiazania
				if(messageId == null || messageId == 0)
					return new FieldValue(this, null);
			
				MailMessage mailMessage = MailMessage.find(messageId);
				
				if(ContentType.EMAIL_SUBJECT == contentType)
				{
					return new FieldValue(this, mailMessage.getSubject());
				}
				else if(ContentType.EMAIL_SENDER == contentType)
				{
					return new FieldValue(this, mailMessage.getFrom());
				}
				else if(ContentType.EMAIL_SENTDATE == contentType)
				{
					return new FieldValue(this, mailMessage.getSentDate());
				}
				else if(ContentType.EMAIL_BODY == contentType)
				{
					return new FieldValue(this, EmailUtils.getEmailContent(mailMessage));
				}
				else if(ContentType.EMAIL_ATTACHMENT == contentType)
				{
					throw new EdmException("Brak implementacji dla pola typu ContentName.ATTACHMENT");
				}
	        }else{
	        	messageId = rs.getLong(MESSAGE_ID_COLUMN);
	        }
		} catch (MessagingException e) {
			log.error(e.getMessage(),e);
			throw new EdmException(e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			throw new EdmException(e.getMessage());
		}
		return new FieldValue(this, null);
	}

	public FieldValue provideFieldValue(Long messageId)
			throws SQLException, EdmException {
		
		//	nie znaleziono powiazania
		if(messageId == null || messageId == 0)
			return new FieldValue(this, null);
		
		MailMessage mailMessage = MailMessage.find(messageId);
		
		try {
			if(ContentType.EMAIL_SUBJECT == contentType)
			{
				return new FieldValue(this, mailMessage.getSubject());
			}
			else if(ContentType.EMAIL_SENDER == contentType)
			{
				return new FieldValue(this, mailMessage.getFrom());
			}
			else if(ContentType.EMAIL_SENTDATE == contentType)
			{
				return new FieldValue(this, mailMessage.getSentDate());
			}
			else if(ContentType.EMAIL_BODY == contentType)
			{
				return new FieldValue(this, EmailUtils.getEmailContent(mailMessage));
			}
			else if(ContentType.EMAIL_ATTACHMENT == contentType)
			{
				throw new EdmException("Brak implementacji dla pola typu ContentName.ATTACHMENT");
			}
		} catch (MessagingException e) {
			log.error(e.getMessage(),e);
			throw new EdmException(e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			throw new EdmException(e.getMessage());
		}
		return new FieldValue(this, null);
	}
	
	private String getEmailAttachment(MailMessage mailMessage) throws EdmException, MessagingException, IOException{
		Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        //	pobranie wartosci z kolumny contentdata
        InputStream input = mailMessage.getBinaryStream();
        MimeMessage message = new MimeMessage(session, input);
        input.close();
            
		ArrayList<Map<String, Object>> attachments = new ArrayList<Map<String,Object>>();
		
        String content = null;
		if (message.getContent() instanceof Multipart)
        {
            Multipart mp = (Multipart) message.getContent();

	            for (int j=0, m = mp.getCount(); j < m; j++) {
	                Part part = mp.getBodyPart(j);

                String disposition = part.getDisposition();
                if ((disposition != null) &&
	                    ((disposition.equals(Part.ATTACHMENT) ||
	                    (disposition.equals(Part.INLINE)))))
	                {
                    Map<String,Object> map = new HashMap<String,Object>();
                    map.put("id", j);
                    map.put("filename", part.getFileName());
                    map.put("filesize", part.getSize());

                    attachments.add(map);
                }

                if (disposition == null && j == 0 && !(part.getContent() instanceof MimeMultipart))
                {
                    if (part.getContent() instanceof String)
                        content = (String) part.getContent();
                }
            }
        }
        else
        {
            if (message.getContent() instanceof String)
                content = (String) message.getContent();
        }
		return content;
	}
	
	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
		if (value == null)
			return null;		
		Object paramValue = null;
		if(ContentType.EMAIL_SUBJECT == contentType || ContentType.EMAIL_SENDER == contentType || ContentType.EMAIL_BODY == contentType)
		{
			if (value instanceof String)
				paramValue = (String)value;
			else if (value instanceof HtmlEditorValue) {
				paramValue = ((HtmlEditorValue)value).getHtml();
			}
			else
			{
				throw new FieldValueCoerceException(this.getName()
	                    + ": " + sm.getString("NieMoznaSkonwertowacWartosci")
	                    + " " + value + " " + "do typu String");
			}		
		}	
		else if(ContentType.EMAIL_SENTDATE == contentType){
			Date date;
			if ((value instanceof String && String.valueOf(value).length() == 0)) 
				return null;
	        if (!(value instanceof Date)) {
	            try {
	                date = DateUtils.parseJsDate(String.valueOf(value));
	                if (!DSApi.isLegalDbDate(date))
	                    throw new FieldValueCoerceException(sm.getString("PrzekazanaDataNieZnajdujeSie") + " " +
	                            sm.getString("WzakresieWymaganymPrzezBazeDanych") + ": " + DateUtils.formatJsDate(date));
	            } catch (FieldValueCoerceException e) {
	                throw e;
	            } catch (Exception e) {
	                throw new FieldValueCoerceException(sm.getString("NieMoznaSkonwertowacWartosci") + " '" + value + "' " + sm.getString("DoDaty"));
	            }
	        } else {
	            date = (Date) value;
	            if (!DSApi.isLegalDbDate(date))
	                throw new FieldValueCoerceException(sm.getString("PrzekazanaDataNieZnajdujeSie") + " " +
	                        sm.getString("WzakresieWymaganymPrzezBazeDanych") + ": " + DateUtils.formatJsDate(date));
	        }
	        return date;
		}
		return paramValue;
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
		if(ContentType.EMAIL_SUBJECT == contentType)
		{
			return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING);
		}
		else if(ContentType.EMAIL_SENDER == contentType)
		{
			return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING);
		}
		else if(ContentType.EMAIL_SENTDATE == contentType)
		{
			pl.compan.docusafe.core.dockinds.dwr.Field field =  new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.DATE);
		    field.setParams(getParams());
		    return field;
		}
		else if(ContentType.EMAIL_BODY == contentType)
		{
			return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.HTML_EDITOR);
		}
		else if(ContentType.EMAIL_ATTACHMENT == contentType)
		{
			return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ATTACHMENT);
		}
		return null;
	}

	
	public ContentType getContentType() {
		return contentType;
	}

}
