package pl.compan.docusafe.core.dockinds.field;

import com.beust.jcommander.internal.Maps;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.directwebremoting.WebContextFactory;
import org.dom4j.Element;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dictionary.DictionaryUtils;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.LocaleUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class AbstractDictionaryField extends Field implements DictionarableField
{
	private final static Logger log = LoggerFactory.getLogger(AbstractDictionaryField.class);
	/**
	 * Xml parameters
	 */
	public static String POP_UP_FIELDS = "pop-up-fields";
	public static String SEARCH_FIELDS = "search-fields";
	public static String DOCKIND_SEARCH_FIELDS = "dockind-search-fields";
	public static String PROMPT_FIELDS = "prompt-fields";
	public static String VISIBLE_FIELDS = "visible-fields";
	public static String POP_UP_BUTTONS = "pop-up-buttons";
	public static String DICTIONARY_BUTTONS = "dictionary-buttons";
	public static String POP_UP_BUTTONS_DEFAULT = "doAdd,doEdit,doRemove,doSelect,doClear";
	public static String DOCUMENT_USER_DIVISION_POP_UP_BUTTONS_DEFAULT = "doSelect,doClear";
	public static String DOCUMENT_USER_DIVISION_DICTIONARY_BUTTONS_DEFAULT = "doClear";
	public static String DICTIONARY_BUTTONS_DEFAULT = "showPopup,doAdd,doRemove,doClear";

	private final String table;
	private final String resource;
	private final String filteringDictionaryCn;
	private final String logic;
	private final DocumentKind documentKind;
	private List<Field> fields;
	private List<String> popUpFields;
	private List<String> promptFields;
	private List<String> searchFields;
	private List<String> visibleFields;
	private List<String> popUpButtons;
	private List<String> dicButtons;
	private List<Field> dockindSearchFields;
	private Set<String> dockindSearchCns;
	//wy�wietlanie tych pozycji s�ownika ds_person, kt�rych pola okre�lone parametrem not-nullable-fields nie s� null'ami
	private List<String> notNullableFields;
	private Map<String, Map<String, String>> fieldsAttributeValue;
	
	public AbstractDictionaryField(String id, String cn, String column, String logic, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String tableName, String resourceName, String filteringDictionaryCn, DocumentKind documentKind) throws EdmException
	{
		super(id, cn, column, names, type, null, null, required, requiredWhen, availableWhen);
		this.logic = logic;
		this.table = tableName;
		this.resource = resourceName;
		this.filteringDictionaryCn = filteringDictionaryCn;
		this.documentKind = documentKind;
	}

	
	public void readParameters(Element el, Element elType) throws EdmException
	{

		setDisableSubmitDictionary(Boolean.valueOf(elType.attributeValue("disabledSubmit")).booleanValue());

		String popUp = elType.attributeValue(POP_UP_FIELDS, "").replace(" ", "");
		String searchCns = elType.attributeValue(SEARCH_FIELDS, "").replace(" ", "");
		String promptCns = elType.attributeValue(PROMPT_FIELDS, "").replace(" ", "");
		String visible = elType.attributeValue(VISIBLE_FIELDS, "").replace(" ", "");
		String popButtons = elType.attributeValue(POP_UP_BUTTONS, POP_UP_BUTTONS_DEFAULT).replace(" ", "");
		if (elType.attribute("pop-up") != null)
			setShowDictionaryPopUp(Boolean.valueOf(elType.attributeValue("pop-up")).booleanValue());
		else
			setShowDictionaryPopUp(true);
		
		String dicButtons = "";
		if (getShowDictionaryPopUp())
		{
			dicButtons = elType.attributeValue(DICTIONARY_BUTTONS, DICTIONARY_BUTTONS_DEFAULT).replace(" ", "");
		}
		else
		{
			dicButtons = elType.attributeValue(DICTIONARY_BUTTONS, DICTIONARY_BUTTONS_DEFAULT).replace(" ", "").replace("showPopup,", "");
		}
		setPopUpButtons(Arrays.asList(popButtons.split(",")));
		setDicButtons(Arrays.asList(dicButtons.split(",")));
		setPopUpFields(Arrays.asList(popUp.split(",")));
		setSearchFields(Arrays.asList(searchCns.split(",")));
		setPromptFields(Arrays.asList(promptCns.split(",")));
		setVisibleFields(Arrays.asList(visible.split(",")));
		
		DockindInfoBean di = new DockindInfoBean(documentKind != null ? documentKind.getCn() : "dictionary");
        FieldsXmlLoader.readFields(elType, di, documentKind != null ? documentKind.getCn() : null); //KM:0801 wypelniamy di danymi z xmla
		fieldsAttributeValue = new HashMap<String, Map<String, String>>();
		readDictionaryFields(elType, "filteringColumn");
		setFields(di.getDockindFields());

		String dockindCnSearch = elType.attributeValue(DOCKIND_SEARCH_FIELDS, "").replace(" ", "");
		dockindSearchCns = Sets.newHashSet(dockindCnSearch.split(","));
		setDockindSearchFields(Lists.newArrayList(Iterables.filter(getFields(), new Predicate<Field>() {
			@Override
			public boolean apply(Field field)
			{
				return dockindSearchCns.contains(field.getCn());
			}
		})));

		if (elType.attribute("refreshMultipleDictionary") != null)
			setRefreshMultipleDictionary(Boolean.valueOf(elType.attributeValue("refreshMultipleDictionary")).booleanValue());
		else
			setRefreshMultipleDictionary(false);
		
		if (elType.attribute("jsPopupFunction") != null)
			setJsPopupFunction(elType.attributeValue("jsPopupFunction"));

		if (elType.attribute("autohideMultipleDictionary") != null)
			setAutohideMultipleDictionary(Boolean.valueOf(elType.attributeValue("autohideMultipleDictionary")).booleanValue());

        if (elType.attribute("autocleanEmptyItems") != null)
            setAutocleanEmptyItems(Boolean.valueOf(elType.attributeValue("autocleanEmptyItems")).booleanValue());

		List<Field> dicFields = new ArrayList<Field>();
		dicFields.addAll(getFields());

		Map<String, String> idNames = new LinkedHashMap<String, String>();
		for (Locale locale : LocaleUtil.AVAILABLE_LOCALES)
			idNames.put(locale.getLanguage(), "ID");

		IntegerColumnField idField = null;
		if ("true".equals(elType.attributeValue("multiple")))
			idField = new IntegerColumnField(String.valueOf(dicFields.get(dicFields.size() - 1).getId() + 10), "ID_1", "ID_1", idNames, false, null, null);
		else
			idField = new IntegerColumnField(String.valueOf(dicFields.get(dicFields.size() - 1).getId() + 10), "ID", "ID", idNames, false, null, null);

		log.debug("{} - {} - !dicFields.contains(idField) {}", getCn(), idField.getCn(), !dicFields.contains(idField));
		if (!dicFields.contains(idField))
		{
			idField.setHidden(true);
			idField.setSubmit(true);
			dicFields.add(0, idField);
		}
		setFields(dicFields);
	}

	/**
	* Metoda przeszukuje elementy field s�ownika w poszukiwaniu warto�ci 
	* zapisanych we wskazanym, unikatowym dla p�l s�ownika, atrybucie
	* i zachowuje je w mapie s�ownika.
	* @param elType
	* @param attributeName
	*/
	private void readDictionaryFields(Element elType, String attributeName){
		Map<String, String> fieldsValue = new HashMap<String, String>();
		String value;
		for(Object object : elType.elements("field")){
			Element dictionaryField = (Element) object;
			if(dictionaryField.attribute(attributeName) != null && !(value = dictionaryField.attributeValue(attributeName)).equals("")){
				fieldsValue.put(dictionaryField.attributeValue("column"), value);
			}
		}
		if(fieldsValue.size() > 0){
			this.fieldsAttributeValue.put(attributeName, fieldsValue);
		}
	}
	
	
	
	public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException
	{
		if (value == null)
			ps.setObject(index, null);
		else
			ps.setLong(index, simpleCoerce(value));
	}

	@Override
	public Long simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value instanceof ArrayList && ((List) value).size() > 0)
			return FieldUtils.longCoerce(this, ((List) value).get(0));
		else
		{
			return FieldUtils.longCoerce(this, value);
		}
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Map<String, Map<String, String>> getFieldsAttributeValue() {
		return fieldsAttributeValue;
	}

	public String getTable()
	{
		return table;
	}

	public List<Field> getFields()
	{
		return fields;
	}

    /**
     * Zwraca map� wymaganych p�l na s�owniku
     * @return
     */
    public Map<String, Field> getRequiredFields(){
        Map<String, Field> results = Maps.newHashMap();
        for(Field field : getFields()){
            if(field.isRequired()){
                results.put(field.getCn(), field);
            }
        }

        return results;
    }

	public String getResource() {
		return resource;
	}

	
	public String getFilteringDictionaryCn() {
		return filteringDictionaryCn;
	}

	public void setFields(List<Field> fields)
	{
		this.fields = fields;
	}

	public List<String> getPopUpFields()
	{
		return popUpFields;
	}

	public void setPopUpFields(List<String> popUpFields)
	{
		this.popUpFields = popUpFields;
	}

	public List<String> getPromptFields()
	{
		return promptFields;
	}

	public void setPromptFields(List<String> promptFields)
	{
		this.promptFields = promptFields;
	}

	public List<String> getSearchFields()
	{
		return searchFields;
	}

	public void setSearchFields(List<String> searchFields)
	{
		this.searchFields = searchFields;
	}

	public List<String> getVisibleFields()
	{
		return visibleFields;
	}

	public void setVisibleFields(List<String> visibleFields)
	{
		this.visibleFields = visibleFields;
	}

	public List<String> getPopUpButtons()
	{
		return popUpButtons;
	}

	public void setPopUpButtons(List<String> popUpButtons)
	{
		this.popUpButtons = popUpButtons;
	}

	public List<String> getDicButtons() {
		return dicButtons;
	}

	public void setDicButtons(List<String> dicButtons) {
		this.dicButtons = dicButtons;
	}

	public List<Field> getDockindSearchFields()
	{
		return dockindSearchFields;
	}

	public void setDockindSearchFields(List<Field> dockindSearchFields)
	{
		this.dockindSearchFields = dockindSearchFields;
	}

	public Set<String> getDockindSearchCns()
	{
		return dockindSearchCns;
	}

	public void setDockindSearchCns(Set<String> dockindSearchCns)
	{
		this.dockindSearchCns = dockindSearchCns;
	}

	public List<String> getNotNullableFields() {
		return notNullableFields;
	}

	public void setNotNullableFields(List<String> notNullableFields) {
		this.notNullableFields = notNullableFields;
	}

	public String getLogic()
	{
		return logic;
	}

	public DocumentKind getDocumentKind()
	{
		return documentKind;
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		if (!isMultiple())
		{
			long l = rs.getLong(getColumn());
			Long key = rs.wasNull() ? null : l;
			if (key == null)
				return new FieldValue(this, key, new HashMap<String, Object>());
			else
			{
				Map<String, Object> dicValues = null;
				try
				{
					dicValues = DwrDictionaryFacade.getDictionary(this, null).getValues(key.toString());
				}
				catch (Exception e)
				{
					log.error(" , key:" + key + " , cn:" + getCn(), e);
				}
				return new FieldValue(this, key, dicValues);
			}
		}
		else
		{
			FieldValue ids = readMultiple(this, documentId, getDocumentKind().getMultipleTableName());
			if (ids.getValue() == null)
				return new FieldValue(this, null, null);
			List<Map<String, Object>> multiValues = new LinkedList<Map<String, Object>>();
			
			Collection<Long> dictionaryIds= null;
			if( isSaveInTheOrder() ){
				dictionaryIds = new ArrayList<Long>();
			}else{
				dictionaryIds = new TreeSet<Long>();
			}
			dictionaryIds.addAll((List<Long>) ids.getKey());
			
			(OfficeDocument.find(documentId, false)).getDocumentKind().logic().revaluateMultipleDictionaryValues(documentId ,this,dictionaryIds);
			
			
			List<Long> linkedIds = new LinkedList<Long>();
			for (Long id : dictionaryIds)
			{
				Map<String, Object> dicValues = DwrDictionaryFacade.getDictionary(this, null).getValues(id.toString());
				multiValues.add(dicValues);
				linkedIds.add(id);
			}
			final String sort = documentKind.getProperties().get(DocumentKind.SORT_BY_FIELD);
			if (sort != null)
			{
				Collections.sort(multiValues, new Comparator<Map<String, Object>>() {
					@Override
					public int compare(Map<String, Object> arg0, Map<String, Object> arg1)
					{
						Object ob0 = arg0.get(sort);
						Object ob1 = arg1.get(sort);
						if (ob0 instanceof Comparable)
							return ((Comparable) ob0).compareTo((Comparable) ob1);
						return ob0.toString().compareTo(ob1.toString());
					}
				});
				if (multiValues.size() > 0 && multiValues.get(0).get("id") != null)
				{
					ArrayList array = new ArrayList<Object>();
					ids.setKey(array);
					for (Map<String, Object> value : multiValues)
						array.add(value.get("id"));
				}

			}

            final List<Field> sortableFields = this.getSortableFields();
            final String dictionaryCn = this.getCn();
            if(!sortableFields.isEmpty()) {
                Collections.sort(multiValues, new Comparator<Map<String, Object>>() {
                    @Override
                    public int compare(Map<String, Object> first, Map<String, Object> second) {
                        int compareResult = 0;
                        for(Field sortField : sortableFields) {
                            String fieldCn = DictionaryUtils.getFieldCnWithoutSuffix(sortField.getCn());
                            fieldCn = DictionaryUtils.getFieldCnWithDictionaryPrefix(dictionaryCn, fieldCn);
                            Comparable firstVal = (Comparable) first.get(fieldCn);
                            Comparable secondVal = (Comparable) second.get(fieldCn);
                            int order = sortField.getSortable() == Sortable.ASCENDING ? 1 : -1;

                            if(firstVal == null || secondVal == null)
                                return 0;
                            compareResult = firstVal.compareTo(secondVal) * order;
                        }

                        return compareResult;
                    }
                });
                linkedIds.clear();
                for(Map<String, Object> item : multiValues) {
                    if(item.get("id") != null)
                        linkedIds.add(Long.parseLong(String.valueOf(item.get("id"))));
                }

            }

			return new FieldValue(this, linkedIds, multiValues);
		}
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getFieldForDwr(Map<String, SchemeField> fieldsScheme) throws EdmException
	{
		pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + getCn(), getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY);
		
		DSUser loggedUser = DSApi.context().getDSUser();
		boolean unlockFields = false;
		if(Docusafe.getAdditionProperty("search_doc_and_edit").equalsIgnoreCase("true") && WebContextFactory.get().getCurrentPage().endsWith("edit-dockind-document.action") && loggedUser.isAdmin()) unlockFields = true;

		SchemeField scheme = null;
		if (fieldsScheme != null)
			scheme = fieldsScheme.get(this.getCn());

		new DwrDictionaryBase(this, fieldsScheme);
		field.setDictionary(DwrDictionaryFacade.getDwrDictionary(this.getDockindCn(), this.getCn()));
		field.setHidden(isHidden());
		field.setDisabled(isDisabled());
		field.setRequired(isRequired());
		field.setReadonly(isReadOnly());
		field.setColumn(getDwrColumn());
		field.setFieldset(getDwrFieldset());
		field.setSubmit(isSubmit());
		field.setCssClass(getCssClass());
		field.setNewItemMessage(getNewItemMessage());
		field.setCoords(getCoords());
        field.setPopups(getPopups());
		if (scheme != null)
		{
			field.setDisabled(scheme.getDisabled());

			if (scheme.isReadonly() == null && scheme.isRequired() == null && scheme.isHidden() == null && !scheme.getDisabled())
				field.setHidden(scheme.isVisible());

			if (scheme.isRequired() != null)
				field.setRequired(scheme.isRequired());

			if (scheme.isReadonly() != null)
				field.setReadonly(scheme.isReadonly());

			if (scheme.isHidden() != null)
				field.setHidden(scheme.isHidden());
			
			//jezeli jestes adminem i przegladasz archiwum to mozna edytowac wszystkie pola
			if(unlockFields)
			{
				field.setHidden(false);
				field.setDisabled(false);
				field.setRequired(false);
				field.setReadonly(false);
			}
			
			if (scheme.getDicButtons() != null)
				field.getDictionary().setDicButtons(scheme.getDicButtons());

            if(scheme.getLabel() != null)
                field.setLabel(scheme.getLabel());
		}
		if (field.getDisabled())
		{
			for (pl.compan.docusafe.core.dockinds.dwr.Field f : field.getDictionary().getFields())
			{
				if ((field.getDictionary().isMultiple() && !f.getCn().toUpperCase().contains("_ID_")) || (!f.getCn().toUpperCase().equals("ID")))
					f.setDisabled(field.getDisabled());
			}
		}
		for(pl.compan.docusafe.core.dockinds.dwr.Field f : field.getDictionary().getFields())
		{
			//jezeli jestes adminem i przegladasz archiwum to mozna edytowac wszystkie pola s�ownika
			if(unlockFields)
			{
				f.setHidden(false);
				f.setDisabled(false);
				f.setRequired(false);
				f.setReadonly(false);
			}
		}
		return field;
	}

	public Object provideDescription(Object value)
	{

		Map<String, String> cnWithName = new HashMap<String, String>();
		StringBuilder sb = new StringBuilder();
		if (value instanceof Map)
		{
			try
			{
				for (Field fff : getFields())
				{
					if (fff.getName() != null)
					{
						cnWithName.put(fff.getCn(), fff.getName());
					}
				}
			}
			catch (Exception a)
			{
				// i co teraz???
			}

			for (String a : ((Map<String, Object>) value).keySet())
			{
				Object b = ((HashMap<String, Object>) value).get(a);

				String temp = a.replaceAll(getCn() + "_", "");
				if (isMultiple())
					temp+="_1";
				if (getDockindSearchCns().contains(temp))
					temp = cnWithName.get(temp);
				else
					temp = null;

				if (b != null && !b.toString().isEmpty() && temp != null)
				{
					if (b instanceof EnumValues)
					{
						if (((EnumValues) b).getSelectedValue() != null && !(((EnumValues) b).getSelectedValue().isEmpty()))
						{
							appendNameWithCommaIfEmpty(sb, temp);
							sb.append(((EnumValues) b).getSelectedValue());
						}
					}
					else if (b instanceof LinkValue)
					{
						appendNameWithCommaIfEmpty(sb, temp);
						((LinkValue) b).getLabel();
					}
					else
					{
						appendNameWithCommaIfEmpty(sb, temp);
						sb.append(b);
					}
				}
			}
		}

		return sb.toString();
	}
	
	private void appendNameWithCommaIfEmpty(StringBuilder sb, String temp)
	{
		if (sb.length() != 0)
			sb.append(", ");
		
		sb.append(temp + ": ");
	}

    public List<Field> getSortableFields() {
        List<Field> sortableFields = Lists.newArrayList();
        for (Field field : this.fields) {
            if (field.getSortable() != null) {
                sortableFields.add(field);
            }
        }

        return sortableFields;
    }
}
