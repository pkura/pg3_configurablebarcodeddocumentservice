package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.DocumentAutorField;
import pl.compan.docusafe.core.dockinds.field.DocumentOfficeClerkField;
import pl.compan.docusafe.core.dockinds.field.DocumentOfficeDeliveryField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class DocumentAutorFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
	
		String cn = el.attributeValue("cn");
		String type = elType.attributeValue("type");
		String logic = null;

	      if(StringUtils.isNotEmpty(elType.attributeValue("logic"))) {
	            logic = elType.attributeValue("logic");
	        }
		return new DocumentAutorField(el.attributeValue("id"), type, cn, logic, names, required, requiredWhen, availableWhen);
	}

}
