package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.DocListField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class DocListFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		Field field;
		DocListField docListField = new DocListField((el.attributeValue("id")), el.attributeValue("cn"), names, Field.DOCLIST, null, required, requiredWhen, availableWhen);

		docListField.setDockind(elType.attributeValue("dockind"));
		docListField.setDiscriminator(elType.attributeValue("discriminator"));
		docListField.setGroupingByEnum("true".equals(elType.attributeValue("groupingByEnum")));
		docListField.setShowAttachment("true".equals(elType.attributeValue("showAttachment")));

		Map<String, String> rfs = new HashMap<String, String>();
		Element elRelatedField = el.element("relatedFields");
		List<Element> elRelatedFields = elRelatedField.elements("relatedField");
		for (Element element : elRelatedFields)
		{
			rfs.put(element.attributeValue("this"), element.attributeValue("mappedBy"));
		}
		docListField.setRelatedFields(rfs);
		List<String> efs = new ArrayList<String>();
		Element elEditField = el.element("editFields");
		if (elEditField != null)
		{
			List<Element> elEditFields = elEditField.elements("editField");
			for (Element element : elEditFields)
			{
				efs.add(element.attributeValue("name"));
			}
		}
		docListField.setEditFields(efs);

		List<String> gbs = new ArrayList<String>();
		Element elGb = el.element("groupingBys");
		if (elGb != null)
		{
			List<Element> elGbs = elGb.elements("groupingBy");
			for (Element element : elGbs)
			{
				gbs.add(element.attributeValue("name"));
			}
		}
		docListField.setGroupingBys(gbs);

		Map<String, String> ndk = new LinkedHashMap<String, String>();
		Element elnewDocumentKind = el.element("newDocumentKinds");
		if (elnewDocumentKind != null)
		{
			List<Element> elnewDocumentKinds = elnewDocumentKind.elements("newDocumentKind");
			for (Element element : elnewDocumentKinds)
			{
				ndk.put(element.attributeValue("cn"), element.attributeValue("name"));
			}
			docListField.setNewDocumentKinds(ndk);
		}

		Element alertEl = el.element("alert");

		if (alertEl != null)
		{
			docListField.setAlertFromField(alertEl.attributeValue("from"));
			docListField.setAlertOperator(alertEl.attributeValue("operator"));
			docListField.setAlertThisField(alertEl.attributeValue("this"));
		}

		field = docListField;
		return field;
	}

}
