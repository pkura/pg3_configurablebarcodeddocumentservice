package pl.compan.docusafe.core.dockinds.field;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import com.google.common.base.Strings;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;

/**
 * Klasa raprezentuj�ca pole wyliczeniowe pobierane z bazy.
 * 
 * @author Mariusz Kilja�czyk
 * @version $Id$
 */
public class DataBaseEnumField extends Field implements EnumerationField
{
	public static final String REPO_REF_FIELD_PREFIX = "REF_";
	StringManager sm = GlobalPreferences.loadPropertiesFile(DataBaseEnumField.class.getPackage().getName(), null);
	protected static Logger log = LoggerFactory.getLogger(DataBaseEnumField.class);
    protected EnumBase enumDefault = new EnumDefaultBase(this);

	// pole zawieraj�ce wszystkie informacje o s�ownikach przypisanych do tej
	// samej tabeli
	
	//zmieni�em na HashMultimap, bo na ArrayListMultimap fieldy powtarza�y si�
	public static final Multimap<String, DataBaseEnumField> tableToField = Multimaps.synchronizedSetMultimap(HashMultimap.<String, DataBaseEnumField> create());
	//public static final Multimap<String, DataBaseEnumField> tableToField = Multimaps.synchronizedListMultimap(ArrayListMultimap.<String, DataBaseEnumField> create());

	private Map<Integer, EnumItem> enumItems;

    public void setRefEnumItemsMap(Map<String, SortedSet<EnumItem>> refEnumItemsMap) {
        enumDefault.setRefEnumItemsMap(refEnumItemsMap);
    }

    public void setAvailableItems(Set<EnumItem> availableItems) {
        enumDefault.setAvailableItems(availableItems);
    }

    public void setAllItems(Set<EnumItem> allItems) {
        enumDefault.setAllItems(allItems);
    }

	private Map<String, SortedSet<EnumItem>> refEnumItemsMap;
	private Set<EnumItem> availableItems;
	private Set<EnumItem> allItems;
	private Map<Integer, EnumItem> itemsReferencedForAll;

	/** Nazwa tabeli w bazie danych z kt�rej zaci�gane s� warto�ci do pola s */
	private String tableName;
	/** Pole oznacz�jce od kt�rego pola zale�y */
	private String refField;
	/** Pole oznacz�jce kt�re pole jest zale�ne */
	private String assoField;
	/** Pole enum typu autocomplete */
	private boolean auto;
	
	private boolean emptyOnStart;
	private boolean allowNotApplicableItem;
	private boolean reloadAtEachDwrGet;
	private boolean repoField;
	
	private static final int NOT_APPLICABLE_ITEM_ID = -1;
	private static final String NOT_APPLICABLE_ITEM_ID_STRING = Integer.toString(NOT_APPLICABLE_ITEM_ID);
	
	//w przypadku wybranej pozycji, kt�rej refValue=0, wymuszamy �adowanie wszystkich pozycji, dla kt�rych refValue=0;
	public static final String REF_ID_FOR_ALL_FIELD = "0";
	
	/** po czym mo�na sortowa� - domy�lnie CN */
	public enum Sort
	{
		TITLE, CN, ID
	}

	private Sort sort = Sort.CN;

	/**
	 * Tworzy i inicjalizuje pole typu dataBase
	 * 
	 * @param id
	 * @param cn
	 * @param column
	 * @param names
	 * @param type
	 * @param refStyle
	 * @param required
	 * @param requiredWhen
	 * @param availableWhen
	 * @param tableName
	 * @param refField
	 * @param assoField
	 * @throws EdmException
	 */
	public DataBaseEnumField(String id, String cn, String column, Map<String, String> names, String type, String refStyle, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String tableName, String refField, String assoField, String correction, boolean auto, boolean emptyOnStart, boolean allowNotApplicableItem, boolean reloadAtEachDwrGet, boolean repoField) throws EdmException
	{
		// super("", refField, id, cn, column, names, required, requiredWhen,
		// availableWhen, false, false);
		super(id, cn, column, names, DATA_BASE, refStyle, null, required, requiredWhen, availableWhen);
		this.tableName = tableName;
		this.refField = refField;
		this.assoField = assoField;
		this.auto = auto;
		this.emptyOnStart = emptyOnStart;
		this.allowNotApplicableItem = allowNotApplicableItem;
		this.reloadAtEachDwrGet = reloadAtEachDwrGet;
		this.repoField = repoField;
		if (correction != null)
		{
			this.setCorrection(Boolean.valueOf(correction).booleanValue());
		}

        enumDefault = new EnumDefaultBase(this);
		
	}

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) ? Objects.equal(getDockindCn(), ((Field)obj).getDockindCn()) : false;
    }

    @Override
	public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException
	{
		if (value == null)
		{
			ps.setObject(index, null);
		}
		else
		{
			ps.setInt(index, simpleCoerce(value));
		}
	}

	@Override
	public Integer simpleCoerce(Object value) throws FieldValueCoerceException
	{
		return FieldUtils.intCoerce(this, value);
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		int id = rs.getInt(getColumn());
		Integer value = rs.wasNull() ? null : id;
		return new FieldValue(this, value);
	}

	@Override
	public EnumItem asObject(Object key) throws IllegalArgumentException, EdmException
	{
		return enumItems.get(key);
	}

	/**
	 * Zwraca dany EnumItem znajduj�cy si� w tabeli, wykorzystuj�c jedno z
	 * dost�pnych p�l
	 * 
	 * @param tablename
	 *             - nazwa tabeli gdzie znajduje si� enum
	 * @param id
	 *             - id enuma
	 * @return enum, null je�li tabela nie jest powi�zana z �adnym enumem
	 * @throws EdmException
	 */
	public static EnumItem getEnumItemForTable(String tablename, int id) throws EdmException
	{
		if (tableToField.containsKey(tablename) && !tableToField.get(tablename).isEmpty())
		{
			log.debug("TABLE NAME: {}", tablename);
			return tableToField.get(tablename).iterator().next().getEnumItem(id);
		}
		else
		{
			return null;
		}
	}
	
	
	/**
	 * Zwraca dany EnumItem znajduj�cy si� w tabeli, wykorzystuj�c jedno z
	 * dost�pnych p�l
	 * 
	 * @param tablename
	 *             - nazwa tabeli gdzie znajduje si� enum
	 * @param id
	 *             - id enuma
	 * @return enum, null je�li tabela nie jest powi�zana z �adnym enumem
	 * @throws EdmException
	 */
	public static DataBaseEnumField getEnumFiledForTable(String tablename) throws EdmException
	{
		if (tableToField.containsKey(tablename) && !tableToField.get(tablename).isEmpty())
		{
			log.debug("TABLE NAME: {}", tablename);
			return tableToField.get(tablename).iterator().next();
		}
		else
		{
			return null;
		}
	}

    public static void reloadForTable(String tablename) throws EdmException {
        DataBaseEnumField dbField = Iterables.getFirst(tableToField.get(tablename), null);
        if (dbField != null) {
            dbField.singleReload();
        } else {
            log.error("Cant reloadForTable {}", tablename);
        }
    }

	public void reload() throws EdmException
	{
		reloadForTable(tableName);
	}

	private void singleReload() throws EdmException
	{
		initialize();
	}

	/**
	 * Inicjalizuje obiekt
	 * 
	 * @throws EdmException
	 */
	public void initialize() throws EdmException
	{
        //zast�puje stary field, nowym
        if (!tableToField.put(tableName, this)){
            tableToField.remove(tableName, this);
            tableToField.put(tableName, this);
        }

		loadEnumItem();
	}

    public Map<Integer, EnumItem> getItemsReferencedForAll() {
        return itemsReferencedForAll;
    }

    public void setItemsReferencedForAll(Map<Integer, EnumItem> itemsReferencedForAll) {
        enumDefault.setItemsReferencedForAll(itemsReferencedForAll);
    }

    public void setEnumItems(Map<Integer, EnumItem> enumItems) {
        enumDefault.setEnumItems(enumItems);
    }

    /**
	 * Metoda buduje tablice wartosci wielojezycznych dla elementow typu
	 * DataBaseEnumField W celu poprawnego uzycia nalezy wypelnic tablice:
	 * title.put("pl","stol"), title.put("en","table") ... Dla kazdej
	 * tlumaczonej wartosci. Metoda obsluguje bazy: MSSQL Server Dla
	 * pozostalych baz (Firebird, Oracle) dzialanie metody pozostaje bez zmian.
	 * W celu uzycia, nalezy tabele slownikowa rozszerzyc o kolumne "language"
	 * A nastepnie dodac wiersze tlumaczenia, np. cn=1, title=stol, language=pl
	 * cn=1, title=table, language=en
	 * 
	 * @throws EdmException
	 */
	private void loadEnumItem() throws EdmException
	{
		ResultSet rs = null;
		PreparedStatement ps = null;
		try
		{
			Map<Integer, EnumItem> items = new LinkedHashMap<Integer, EnumItem>();
			Map<Integer, EnumItem> referencedForAll = new LinkedHashMap<Integer, EnumItem>();
			SortedSet<EnumItem> available = new TreeSet<EnumItem>(getComparator());
			SortedSet<EnumItem> all = new TreeSet<EnumItem>(getComparator());
			Map<String, SortedSet<EnumItem>> refEnumItems = new LinkedHashMap<String, SortedSet<EnumItem>>();

			// Sprawdzenie, czy tabela jest wielojezyczna

			boolean isMultilanguage = IsMultilanguageTable(tableName);

			// Tabela wielojezyczna posiada dodatkowa kolumne "language",
			// ktora jest pobierana
			if (isMultilanguage)
			{
				ps = DSApi.context().prepareStatement("select id,cn,title,refValue,centrum,available,language from " + tableName + " " + "  order by cn");
			}
			else
			{

				ps = DSApi.context().prepareStatement("select id,cn,title,refValue,centrum,available from " + tableName + " order by cn");
			}
			rs = ps.executeQuery();
			// Tabela tymczasowa
			ArrayList<DictionaryObject> slownik = new ArrayList<DictionaryObject>();

			// Wczytanie slownika do tabeli tymczasowej
			while (rs.next())
			{
				DictionaryObject pos;
                String internedStringTitle = rs.getString(3) != null ? rs.getString(3).intern() : null;
                if(StringUtils.isBlank(rs.getString(2)) || StringUtils.isBlank(internedStringTitle))
                {
                    continue;
                }
                if (isMultilanguage)
				{
					pos = new DictionaryObject(rs.getInt(1), rs.getString(2), internedStringTitle, rs.getString(4), rs.getInt(5), rs.getBoolean(6), rs.getString(7));
				}
				else
				{
					pos = new DictionaryObject(rs.getInt(1), rs.getString(2), internedStringTitle, rs.getString(4), rs.getInt(5), rs.getBoolean(6));
				}

				slownik.add(pos);
			}

			// Utworzenie map tlumaczen
			for (DictionaryObject obj : slownik)
			{
				if (obj.isUsed() || StringUtils.isBlank(obj.getCn()) || StringUtils.isBlank(obj.getTitle()))
					continue;

				Map<String, String> title = new TreeMap<String, String>();

				// Uzupe�nienie wszystkich warto�ci
				if (isMultilanguage)
				{
					for (DictionaryObject tmp : slownik)
					{
						if (tmp.getCn().equals(obj.getCn()) && (!tmp.isUsed()))
						{
							title.put(tmp.getLanguage(), tmp.getTitle());
							tmp.setUsed(true);
						}
					}
				}
				else
				{
					title.put("pl", obj.getTitle());
				}
				EnumItemImpl ei = new EnumItemImpl(obj.getId(), obj.getCn(), title, new String[0], false, obj.getRefValue(), obj.getCentrum(), getCn());
				ei.setAvailable(obj.isAvailable());

				if (ei.isAvailable())
				{
					available.add(ei);
				}

				all.add(ei);

				items.put(obj.getId(), ei);

				if (obj.getRefValue() != null)
				{
					if (!refEnumItems.containsKey(obj.getRefValue()))
					{
						refEnumItems.put(obj.getRefValue(), new TreeSet<EnumItem>(getComparator()));
					}
					refEnumItems.get(obj.getRefValue()).add(ei);
					
					if (obj.getRefValue().equals(REF_ID_FOR_ALL_FIELD)) {
						referencedForAll.put(obj.getId(), ei);
					}
				}	
				else if (AvailabilityManager.isAvailable("allowNullRefValues"))
				{
					if (!refEnumItems.containsKey("NULL"))
					{
						refEnumItems.put("NULL", new TreeSet<EnumItem>(getComparator()));
					}
					refEnumItems.get("NULL").add(ei);
				}
			}
			refEnumItemsMap = Collections.unmodifiableMap(refEnumItems);
			enumItems = Collections.unmodifiableMap(items);
			availableItems = Collections.unmodifiableSet(available);
			allItems = Collections.unmodifiableSet(all);
			itemsReferencedForAll = Collections.unmodifiableMap(referencedForAll);
            setRefEnumItemsMap(refEnumItemsMap);
            setItemsReferencedForAll(itemsReferencedForAll);
            setAllItems(allItems);
            setAvailableItems(availableItems);
            setEnumItems(enumItems);

            // obej�cie problemu dublowania si� kolekcji opisuj�cych zawarto�� tej samej tabeli
            // przypisywanie to�samych obiekt�w, oczekiwany gc
            Collection<DataBaseEnumField> dbFields = tableToField.get(getTableName());

            for (DataBaseEnumField dbField : dbFields) {
                dbField.refEnumItemsMap = refEnumItemsMap;
                dbField.enumItems = enumItems;
                dbField.availableItems = availableItems;
                dbField.allItems = allItems;
                dbField.itemsReferencedForAll = itemsReferencedForAll;
                dbField.setRefEnumItemsMap(refEnumItemsMap);
                dbField.setItemsReferencedForAll(itemsReferencedForAll);
                dbField.setAllItems(allItems);
                dbField.setAvailableItems(availableItems);
                dbField.setEnumItems(enumItems);
            }

            rs.close();
		}
		catch (Exception e)
		{
			log.error("", e);
			throw new EdmException("B��d podczas wczytania s�ownika z bazy: " + this.tableName, e);
		}
		finally
		{
			if (rs != null)
				try
				{
					rs.close();
				}
				catch (SQLException e)
				{
					log.debug("", e);
				}
			DSApi.context().closeStatement(ps);
		}
	}

	/**
	 * Metoda sprawdza, czy tabela slownikowa obsluguje wielojezycznosc.
	 * 
	 * @param tableName
	 *             - Nazwa tabeli, ktora sprawdzamy
	 * @return true - obsluguje, false - nie obsluguje TODO: Metoda obs�uguje
	 *         bazy: MSSQL Server Dla bazy Oracle, Firebird domyslnie zwraca
	 *         false
	 */
	private boolean IsMultilanguageTable(String tableName) throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;

		try
		{
			// Utworzenie zapytania dla bazy MS SQL SERVER
			if (DSApi.isSqlServer())
			{
				ps = DSApi.context().prepareStatement("select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tableName + "' and COLUMN_NAME='language'");
			}
			if (ps != null)
			{
				rs = ps.executeQuery();

				while (rs.next())
				{
					rs.close();
					return true;
				}
				return false;
			}
			// Baza inna ni� MSSQL
			return false;
		}
		catch (Exception e)
		{
			throw new EdmException("B��d podczas wczytania s�ownika z bazy", e);
		}
		finally
		{
			if (rs != null)
				try
				{
					rs.close();
				}
				catch (SQLException e)
				{
					log.debug("", e);
				}
			DSApi.context().closeStatement(ps);
		}
	}

	public List<EnumItem> getEnumItems()
	{
		return getEnumItems(false);
	}
	
	public List<EnumItem> getEnumItems(boolean onlyAvailable)
	{
		if (onlyAvailable) {
			return availableItems != null ? new ArrayList<EnumItem>(availableItems) : Collections.EMPTY_LIST; 
		} else {
			return enumItems != null ? new ArrayList<EnumItem>(enumItems.values()) : Collections.EMPTY_LIST;
		}
	}

	public Collection<EnumItem> getAvailableItems()
	{
		return availableItems;
	}
	
	public List<EnumItem> getSortedEnumItems(boolean onlyAvailable)
	{
		List<EnumItem> list = getEnumItems(onlyAvailable);
		Collections.sort(list, getComparator());
		return list;
	}

	@Override
	public List<EnumItem> getSortedEnumItems()
	{
		return getSortedEnumItems(false);
	}

	public EnumItem getEnumItem(String id) throws EntityNotFoundException {
		return getEnumItem(Integer.valueOf(id));
	}
	
	public EnumItem getEnumItem(Integer id) throws EntityNotFoundException
	{
		EnumItem item = enumItems != null ? (EnumItem) enumItems.get(id) : null;
		if (item == null)
			throw new EntityNotFoundException(sm.getString("NieZnalezionoWartosciWyliczeniowejOidentyfikatorze") + " = " + id + " " + sm.getString("DlaPola") + " " + getCn());
		return item;
	}

	public EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException
	{
		for (EnumItem i : allItems)
		{
			if (cn.equals(i.getCn()))
			{
				return i;
			}
		}
		throw new EntityNotFoundException(sm.getString("NieZnalezionoWartosciWyliczeniowejOcn") + " = " + cn + " " + sm.getString("DlaPola") + " " + getCn());
	}

	public EnumItem getEnumItemByTitle(String title) throws EntityNotFoundException
	{
		if (enumItems != null)
		{
			for (EnumItem i : enumItems.values())
			{
				if (title.equals(i.getTitle()))
					return i;
			}
		}
		throw new EntityNotFoundException(sm.getString("NieZnalezionoWartosciWyliczeniowejOtitle") + " = " + title + " " + sm.getString("DlaPola") + " " + getCn());
	}

	public String getTableName()
	{
		return tableName;
	}

	public String getRefField()
	{
		return refField;
	}
	
	public String getRefFieldWithRepo()
	{
		return isRepoField()?REPO_REF_FIELD_PREFIX+getCn():refField;
	}

	public String getAssoField()
	{
		return assoField;
	}

	public void setAssoField(String assoField)
	{
		this.assoField = assoField;
	}

	/*
	 * zwraca liste enumow zaleznch od id
	 */
	public SortedSet<EnumItem> getEnumItems(String id)
	{
		if (refEnumItemsMap.containsKey(id))
		{
			return refEnumItemsMap.get(id);
		}
		else
			return null;
	}

	public Map<String, SortedSet<EnumItem>> getRefEnumItemsMap()
	{
		return refEnumItemsMap;
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
	{
		pl.compan.docusafe.core.dockinds.dwr.Field field;
		if (isMultiple())
		{
			field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM_MULTIPLE);
		}
		else
		{
			field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
		}
		
		if(getOnchange() != null)
			field.setOnchange(getOnchange());
		
		if (isAuto())
			field.setAutocomplete(true);
		field.setAutocompleteRegExp(getAutocompleteRegExp());
		return field;
	}

	private boolean isFieldAndRefFieldNotSelected(Map<String, Object> fieldsValues) {
		return (fieldsValues.get(getCn()) == null || Strings.isNullOrEmpty(fieldsValues.get(getCn()).toString())) 
				&& (fieldsValues.get(getRefFieldWithRepo()) == null || Strings.isNullOrEmpty(fieldsValues.get(getRefFieldWithRepo()).toString()));
	}
	
	private boolean isRefFieldNotSelected(Map<String, Object> fieldsValues) {
		return fieldsValues.get(getRefFieldWithRepo()) == null 
				|| Strings.isNullOrEmpty(fieldsValues.get(getRefFieldWithRepo()).toString());
	}
	
	private boolean isFieldNotSelected(Map<String, Object> fieldsValues) {
		return fieldsValues.get(getCn()) == null 
				|| Strings.isNullOrEmpty(fieldsValues.get(getCn()).toString());
	}
	
	/**
	 * magiczne rzeczy - do poprawki
	 */
	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, String id, Sort sort) throws EntityNotFoundException {

		//nale�y inaczej rozwi�za�
		if (isReloadAtEachDwrGet()) {
			try {
				singleReload();
			} catch (EdmException ex) {
				log.error(ex.getMessage(), ex);
			}
		}

        enumDefault.setRepoField(repoField);
        String temp = sort.toString();
        return enumDefault.getEnumItemsForDwr(fieldsValues, id, temp);
	}
    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EntityNotFoundException {
        return getEnumItemsForDwr(fieldsValues,  null, sort);
    }

    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, String id) throws EntityNotFoundException {
        return getEnumItemsForDwr(fieldsValues, id, sort);
    }

	private boolean isSelected(Map<String, Object> fieldsValues) {
		return fieldsValues != null && fieldsValues.get(getCn()) != null;
	}
	
	/**
	 * magiczne rzeczy - do poprawki
	 */
	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, Sort sort) throws EntityNotFoundException
	{
		return getEnumItemsForDwr(fieldsValues, null, sort);
	}

	private Comparator<EnumItem> getComparator()
	{
		switch (getSort())
		{
			case CN:
				return EnumItem.COMPARATOR_BY_CN;
			case TITLE:
				return EnumItem.COMPARATOR_BY_TITLE;
			case ID:
			    	return EnumItem.COMPARATOR_BY_ID;
			default:
				throw new IllegalStateException("Nieobs�ugiwany Sort " + getSort());
		}
	}

	public EnumValues getEnumItemsForDwr(Long documentId, Map<String, Object> fieldsValues)
	{
		// nale�y inaczej rozwi�za�
		if (isReloadAtEachDwrGet()) {
			try {
				singleReload();
			} catch (EdmException ex) {
				log.error(ex.getMessage(), ex);
			}
		}
		List<EnumItem> sortedList = getEnumItems();
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		Set<String> enumSelectedSet = Sets.newHashSet();

		enumValues.put("", sm.getString("select.wybierz"));
		if (isSelected(fieldsValues))
			enumSelectedSet.add(fieldsValues.get(getCn()).toString());
		else
			enumSelectedSet.add("");
		if (documentId != null)
		{
			for (EnumItem item : sortedList)
			{
				if (item.getRefValue().equals(documentId.toString()))
				{
					String user = null;
					try
					{
						user = DSUser.findByUsername(item.getTitle()).asLastnameFirstname();
					}
					catch (EdmException e)
					{
						log.error(e.getMessage(), e);
					}
					enumValues.put(item.getId().toString(), user != null ? user : item.getTitle());
				}
			}
		}
		return new EnumValues(enumValues, Lists.newArrayList(enumSelectedSet));
	}
	
	// na potrzeby wymuszenia �adowania pozycji, czly empty-on-start="true"
    public EnumValues getEnumValuesForForcedPush(Map<String, FieldData> values, String dictionaryNameSuffix) {
        String fieldSelectedId = values.get(dictionaryNameSuffix + StringUtils.remove(getCn(), "_1")) != null ? values.get(dictionaryNameSuffix + StringUtils.remove(getCn(), "_1")).getEnumValuesData().getSelectedOptions().get(0) : null;
        return getEnumValuesForForcedPush(fieldSelectedId);
    }

    public EnumValues getEnumValuesForForcedPush(String fieldSelectedId) {
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		Set<String> enumSelectedSet = Sets.newHashSet();

		// nale�y inaczej rozwi�za�
		if (isReloadAtEachDwrGet()) {
			try {
				singleReload();
			} catch (EdmException ex) {
				log.error(ex.getMessage(), ex);
			}
		}

		List<EnumItem> sortedList = getEnumItems();
		if (getSort() != null && getSort().equals(Sort.ID))
		{
		    	Collections.sort(sortedList, new EnumItem.EnumItemComparatorByID());
		}
		else if (getRefStyle() != null && (getRefStyle().equals(Field.STYLE_CN) || getRefStyle().equals(Field.STYLE_CN_COLON) || getRefStyle().equals(Field.STYLE_CN_ONLY)))
		{
			Collections.sort(sortedList, new EnumItem.EnumItemCnComparator());
		}
		else
		{
			Collections.sort(sortedList, new EnumItem.EnumItemComparator());
		}

        if (fieldSelectedId != null) {
            enumSelectedSet.add(fieldSelectedId);
        }

        for (EnumItem item : sortedList) {
            String title;
            if (getRefStyle() != null && getRefStyle().equals(Field.STYLE_CN))
                title = item.getCn() + " - " + item.getTitle();
            else if (getRefStyle() != null && getRefStyle().equals(Field.STYLE_CN_COLON))
                title = item.getCn() + ": " + item.getTitle();
            else if (getRefStyle() != null && getRefStyle().equals(Field.STYLE_CN_ONLY))
                title = item.getCn();
            else
                title = item.getTitle();

            enumValues.put("", sm.getString("select.wybierz"));

            if (item.isAvailable() || enumSelectedSet.contains(item.getId().toString())) {
                enumValues.put(item.getId().toString(), title);
            }
        }

        if (fieldSelectedId == null || !enumValues.containsKey(fieldSelectedId)) {
            enumSelectedSet.clear();
            enumSelectedSet.add("");
        }


		return new EnumValues(enumValues, Lists.newArrayList(enumSelectedSet));
	}

	public Sort getSort()
	{
		return sort;
	}

	public void setSort(Sort sort)
	{
		this.sort = sort;
	}

	public boolean isReloadAtEachDwrGet() {
		return reloadAtEachDwrGet;
	}

	public void setReloadAtEachDwrGet(boolean reloadAtEachDwrGet) {
		this.reloadAtEachDwrGet = reloadAtEachDwrGet;
	}
	
	public boolean isRepoField() {
		return repoField;
	}
	
	public void setRepoField(boolean repoField) {
		enumDefault.setRepoField(repoField);
	}

    public boolean isEmptyOnStart() {
        return emptyOnStart;
    }

    public void setEmptyOnStart(boolean emptyOnStart) {
        this.emptyOnStart = emptyOnStart;
    }

    public boolean isAllowNotApplicableItem() {
        return allowNotApplicableItem;
    }

    public void setAllowNotApplicableItem(boolean allowNotApplicableItem) {
        this.allowNotApplicableItem = allowNotApplicableItem;
    }

	public void setRefField(String refField) {
		this.refField = refField;
	}
}

class DictionaryObject
{

	public DictionaryObject()
	{
	};

	public DictionaryObject(int id, String cn, String title, String refValue, Integer centrum, boolean available, String language)
	{
		this.id = id;
		this.cn = cn;
		this.title = title;
		this.refValue = refValue;
		this.centrum = centrum;
		this.available = available;
		this.language = language;
	}

	public DictionaryObject(int id, String cn, String title, String refValue, Integer centrum, boolean available)
	{
		this.id = id;
		this.cn = cn;
		this.title = title;
		this.refValue = refValue;
		this.centrum = centrum;
		this.available = available;

	}

	private int id;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getCn()
	{
		return cn;
	}

	public void setCn(String cn)
	{
		this.cn = cn;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getRefValue()
	{
		return refValue;
	}

	public void setRefValue(String refValue)
	{
		this.refValue = refValue;
	}

	public Integer getCentrum()
	{
		return centrum;
	}

	public void setCentrum(Integer centrum)
	{
		this.centrum = centrum;
	}

	public boolean isAvailable()
	{
		return available;
	}

	public void setAvailable(boolean available)
	{
		this.available = available;
	}

	public String getLanguage()
	{
		return language;
	}

	public void setLanguage(String language)
	{
		this.language = language;
	}

	private String cn;
	private String title;
	private String refValue;
	private int centrum;
	private boolean available;
	private String language;
	private boolean used;

	public boolean isUsed()
	{
		return used;
	}

	public void setUsed(boolean used)
	{
		this.used = used;
	}

}
