package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;

public interface HTMLFieldLogic {
	
	public String getValue();
	
	public String getValue(String docId) throws EdmException;

}
