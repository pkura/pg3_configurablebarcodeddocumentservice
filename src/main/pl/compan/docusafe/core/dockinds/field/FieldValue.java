package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.crm.Contact;
import pl.compan.docusafe.core.dockinds.dictionary.DocumentKindDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Reprezentuje warto�� danego pola. Pole 'key' oznacza warto��(klucz)
 * zapisywan� do bazy, a 'value' warto�� odpowiadaj�c� temu kluczowi,
 * kt�ra b�dzie w jaki� spos�b wy�wietlana na formatce.
 *
 * Dla wielu p�l 'key' == 'value'.
 */
public class FieldValue implements Serializable {
    private final static Logger log = LoggerFactory.getLogger(FieldValue.class);

    private final Field field;

    private Object key;
    private Object value;


    /**
     * Konstruktor.
     *
     * @param field
     * @param key
     * @param value
     */
    public FieldValue(Field field, Object key, Object value)
    {
        this.field = field;
        this.key = key;
        this.value = value;
    }

    /**
     * Konstruktor dla warto�ci typ�w wymagaj�cych dodatkowego wyliczenia
     * warto�ci 'value' na podstawie 'key'.
     *
     * @param field
     * @param key
     * @throws pl.compan.docusafe.core.EdmException
     */
    @SuppressWarnings("unchecked")
    public FieldValue(Field field, Object key) throws EdmException
    {
        this.field = field;
        this.key = key;

        // inicjalizujemy wartosc (o ile 'key' r�ne od null)
        if (this.key != null) {
            if (field.isMultiple())
            {
                // atrybut wielowarto�ciowy
                List<Object> keys = (List<Object>) key;
                List<Object> values = new ArrayList<Object>();
                for (Object o : keys)
                {
                    values.add(calculateValue(o));
                }
                sortKeys(values);
                this.value = values;
            }
            else
            {
                // atrybut jednowarto�ciowy
                this.value = calculateValue(this.key);
            }
        } else if (Field.CLASS.equals(field.getType())) {

            if(field.isMultiple()){
               this.value = Collections.emptyList();
            } else {
                try {
                    Class c = Class.forName(field.getClassname());
                    this.value =  c.getConstructor().newInstance();
                } catch (Exception e) {
                    throw new EdmException(e);
                }
            }
        }
    }

    /** Sortowanie list slownikowych */
    private void sortKeys(List keys)
    {
        try
        {
            if (keys.size() < 1)
                return;
            if (keys.get(0) instanceof Contact)
            {
                Collections.sort(((List<Contact>) keys), new Contact.ContactComparatorById());
            }
        }
        catch (Exception e)
        {
            log.error("B��d sortowania listy s�ownikowej");
        }
    }

    /**
     * na podstawie klucza wylicza pojedy�cz� warto�c dla tego pola
     */
    @SuppressWarnings("unchecked")
    private Object calculateValue(Object key) throws EdmException
    {
        if (Field.CLASS.equals(field.getType()) || Field.CENTRUM_KOSZT.equals(field.getType()))
        {
            try
            {
                Class c = Class.forName(field.getClassname());
                try
                {
                    Object o = c.getConstructor(new Class[0]).newInstance(new Object[0]);
                    if(DocumentKindDictionary.class.isInstance(o))
                    {
                        return ((DocumentKindDictionary)o).find((Long)key);
                    }
                }
                catch (Exception e)
                {
                    throw new EdmException("Nie znaleziono definicji klasy " + field.getClassname(), e);
                }
                return Finder.find(c, (Long) key);
            }
            catch (ClassNotFoundException e)
            {
                throw new EdmException("Nie znaleziono definicji klasy " + field.getClassname(), e);
            }
        }
        else if (Field.ENUM.equals(field.getType()) || Field.ENUM_REF.equals(field.getType()) 
        		|| Field.DATA_BASE.equals(field.getType()) || Field.DSUSER.equals(field.getType())
        		|| Field.DSDIVISION.equals(field.getType()))
        {
            return field.getEnumItem((Integer) key).getTitle();
        }
        else
        {
            // nie potrzeba dodatkowego wyliczenia dla innych typ�w -
            // warto�� taka sama jak klucz
            return key;
        }
    }

    public Object getKey()
    {
        return key;
    }

    public Object getValue()
    {
        return value;
    }

    /**
     * Zwraca warto�� pola w postaci opisu (zamiast rzeczywistego obiektu) -
     * ma sens tylko dla p�l typu CLASS, bo dla pozosta�ych zwracane to samo
     * co przez funkcje {@link this#getValue()}
     *
     * - t.j. w dockind-fields.jsp nie jest wywo�ywana ta metoda tylko getValue.
     */
    @SuppressWarnings("unchecked")
    public Object getDescription()
    {
        if (field.isMultiple() && value!=null && value instanceof List)
        {
            List<Object> values = (List<Object>) value;
            String description = null;
            for (int i = 0; i < values.size(); i++)
            {
                if (i == 0)
                    description = field.provideDescription(values.get(i)).toString();
                else
                    description += ", " + field.provideDescription(values.get(i)).toString();
            }
            return description;
        }
        else
            return field.provideDescription(this.value);
    }

    public Field getField()
    {
        return field;
    }

    public void setKey(Object key)
    {
        this.key = key;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }
}
