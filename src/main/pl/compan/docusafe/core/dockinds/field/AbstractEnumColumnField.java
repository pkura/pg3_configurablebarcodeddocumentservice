package pl.compan.docusafe.core.dockinds.field;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Lists;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.util.StringManager;

/**
 * Klasa bazowa p�l wyliczeniowych, kt�re mieszcz� si� w pami�ci
 * 
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public abstract class AbstractEnumColumnField extends Field implements EnumBase,EnumerationField{
    StringManager sm =
        GlobalPreferences.loadPropertiesFile(XmlEnumColumnField.class.getPackage().getName(), null);

    protected EnumBase enumDefault;


	public AbstractEnumColumnField(String id, String cn, String column, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, column, names, type, refStyle, classname, required, requiredWhen, availableWhen);
        enumDefault = new EnumDefaultBase(this);

    }

    public final void addEnum(Integer id, String cn, Map<String, String> titles, String[] args, boolean forceRemark, String fieldCn, boolean available)
    {
		enumDefault.addEnum(id, cn, titles, args, forceRemark, fieldCn, available);
	}

	/**
	 * Lista dost�pnych do wyboru rekord�w
	 * 
	 * @return
	 */
	public List<EnumItem> getAvailableEnumItems()
	{
		return enumDefault.getAvailableEnumItems();
	}

	public List<EnumItem> getEnumItems()
	{
        return enumDefault.getEnumItems();
	}

    public final List<EnumItem> getAllEnumItemsByAspect(String aspectCn, DocumentKind documentKind) throws EdmException
    {
       return enumDefault.getAllEnumItemsByAspect(aspectCn, documentKind);
    }

    public final List<EnumItem> getEnumItemsByAspect(String aspectCn, DocumentKind documentKind) throws EdmException
    {
        return enumDefault.getEnumItemsByAspect(aspectCn, documentKind);
	}

    public final List<EnumItem> getEnumItemsByAspect(String aspectCn,  FieldsManager fm) throws EdmException
    {
        return enumDefault.getEnumItemsByAspect(aspectCn, fm);
	}

	@Override
	public abstract Object asObject(Object key) throws IllegalArgumentException, EdmException;

	public void setEnumItems(List<EnumItem> enumItems)
	{
		enumDefault.setEnumItems(enumItems);
	}

	public List<EnumItem> getSortedEnumItems()
	{
		return enumDefault.getSortedEnumItems();
	}

	public EnumItem getEnumItem(Integer id) throws EntityNotFoundException
	{
		return enumDefault.getEnumItem(id);
	}

	public EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException
	{
		return enumDefault.getEnumItemByCn(cn);
	}

	public EnumItem getEnumItemByTitle(String title) throws EntityNotFoundException
	{
		return enumDefault.getEnumItemByTitle(title);
	}

    @Override
    public Set<EnumItem> getEnumItemsByRef(String id) {
        return enumDefault.getEnumItemsByRef(id);
    }

    @Override
	public Integer simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value instanceof EnumItem)
		{
			return ((EnumItem) value).getId();
		}
		else
		{
			return FieldUtils.intCoerce(this, value);
		}
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
	{
		pl.compan.docusafe.core.dockinds.dwr.Field field;

		if (isMultiple())
		{
			field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM_MULTIPLE);
		} else {
			field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
		}
		if (isAuto())
			field.setAutocomplete(true);
		field.setAutocompleteRegExp(getAutocompleteRegExp());

		return field;
	}

	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EntityNotFoundException
	{
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();

		if (isMultiple() && fieldsValues.get(getCn()) != null)
		{
			for (Object str : (Iterable) fieldsValues.get(getCn()))
			{
				if (str instanceof Integer)
					enumSelected.add(((Integer) str).toString());
				else if (str instanceof String)
					enumSelected.add((String) str);
			}
		}
		else if (!isMultiple())
		{
			enumValues.put("", sm.getString("select.wybierz"));
			if (fieldsValues != null && fieldsValues.get(getCn()) != null)
			{
				enumSelected.add(fieldsValues.get(getCn()).toString());
			}
			else if (getDefaultValue()!=null)
			{
				enumSelected.add(getDefaultValue());
			}
			else
			{
				enumSelected.add("");
			}
		}

		// pobranie wartosci enumFielda
		for (EnumItem item : getEnumItems())
			enumValues.put(item.getId().toString(), item.getTitle());
		return new EnumValues(enumValues, enumSelected);
	}

    @Override
    public EnumValues getEnumItemsForDwrByScheme(Map<String, Object> fieldsValues, Map<String, SchemeField> schemeFields) throws EdmException {
        EnumValues enumValues = getEnumItemsForDwr(fieldsValues);
        EnumValues filteredEnumValues = new EnumValues();

        if(schemeFields != null && schemeFields.containsKey(this.getCn())) {
            SchemeField schemeField = schemeFields.get(this.getCn());

            if(schemeField.getEnumValuesId() != null) {
                List<Map<String, String>> filteredOptions = Lists.newArrayList();

                for(Map<String, String> singleOption : enumValues.getAllOptions()) {
                    for(String id : schemeField.getEnumValuesId()) {
                        if(singleOption.containsKey(id)) {
                            filteredOptions.add(singleOption);
                            break;
                        }
                    }
                }

                filteredEnumValues.setAllOptions(filteredOptions);
                filteredEnumValues.setSelectedOptions(enumValues.getSelectedOptions());

                return filteredEnumValues;
            }
        }

        return enumValues;
    }
}
