package pl.compan.docusafe.core.dockinds.field;

import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;

public interface EnumNonColumnField
{
	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException;
}
