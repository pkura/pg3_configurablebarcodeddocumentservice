package pl.compan.docusafe.core.dockinds.field;

import org.dom4j.Element;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Date: 12.07.13
 * Time: 10:10
 * Gosia
 * Klasa reprezentuj�ca pole przechowuj�ce dat�, nie potrzebuje kolumny w bazie danych.
 */
public class DateNonColumnField extends NonColumnField implements DateField {

    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);

    /**
     * @param elType
     * @param id            id z xml'a, musi by� unikalne
     * @param cn            cn z xml'a, musi b� unikalne
     * @param names         nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param required      czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen  warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws pl.compan.docusafe.core.EdmException
     *
     */
    public DateNonColumnField(Element elType, String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, names, Field.DATE, null, null, required, requiredWhen, availableWhen);

        if (elType.attributeValue("hours") != null) {
            addParam("hours", "true".equals(elType.attributeValue("hours")));
        }
        if (elType.attributeValue("minutes") != null) {
            addParam("minutes", "true".equals(elType.attributeValue("minutes")));
        }
        if (elType.attributeValue("allowNull") != null && elType.attributeValue("allowNull").equals("true")) {
            setAllowNull(true);
        }
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        String value = null;
        return new FieldValue(this, value, value);
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        if (value == null)
            return null;
        Date date;
        if ((value instanceof String && String.valueOf(value).length() == 0)) return null;
        if (!(value instanceof Date)) {
            try {
                date = DateUtils.parseJsDate(String.valueOf(value));
                if (!DSApi.isLegalDbDate(date))
                    throw new FieldValueCoerceException(sm.getString("PrzekazanaDataNieZnajdujeSie") + " " +
                            sm.getString("WzakresieWymaganymPrzezBazeDanych") + ": " + DateUtils.formatJsDate(date));
            } catch (FieldValueCoerceException e) {
                throw e;
            } catch (Exception e) {
                //log.error(e.getMessage(), e);
                throw new FieldValueCoerceException(sm.getString("NieMoznaSkonwertowacWartosci") + " '" + value + "' " + sm.getString("DoDaty"));
            }
        } else {
            date = (Date) value;
            if (!DSApi.isLegalDbDate(date))
                throw new FieldValueCoerceException(sm.getString("PrzekazanaDataNieZnajdujeSie") + " " +
                        sm.getString("WzakresieWymaganymPrzezBazeDanych") + ": " + DateUtils.formatJsDate(date));
        }
        return date;
    }

    @Override
    public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
        pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.DATE);
        field.setParams(getParams());
        return field;
    }

    public Object provideDescription(Object value) {
        if (value != null && value instanceof Date)
            return DateUtils.formatJsDate((Date) value);

        return null;
    }
}
