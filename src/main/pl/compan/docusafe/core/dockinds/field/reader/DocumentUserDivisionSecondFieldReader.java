package pl.compan.docusafe.core.dockinds.field.reader;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.util.LocaleUtil;

import java.util.*;

public class DocumentUserDivisionSecondFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		String person = el.element("type").attributeValue("person");
		String cn = el.attributeValue("cn");
		Preconditions.checkNotNull(person, cn + ": person attribute must be set");
        String logic = null;

        if(StringUtils.isNotEmpty(elType.attributeValue("logic"))) {
            logic = elType.attributeValue("logic");
        }

		DocumentUserDivisionSecondField userDivisonField = new DocumentUserDivisionSecondField(el.attributeValue("id"), cn, logic, names, required, requiredWhen, availableWhen, person);

		String popUp = elType.attributeValue("pop-up-fields");
		String searchCns = elType.attributeValue("search-fields");
		String promptCns = elType.attributeValue("prompt-fields");
		String visible = elType.attributeValue("visible-fields");
	
		Preconditions.checkNotNull(popUp, cn + ": pop-up-fields attribute must be set");
		Preconditions.checkNotNull(searchCns, cn + ": search-fields attribute must be set");
		Preconditions.checkNotNull(promptCns, cn + ": prompt-fields attribute must be set");
		Preconditions.checkNotNull(visible, cn + ": visible-fields attribute must be set");

		if (elType.attribute("pop-up") != null)
		{
			userDivisonField.setShowDictionaryPopUp(Boolean.valueOf(elType.attributeValue("pop-up")).booleanValue());
		}
		else
			userDivisonField.setShowDictionaryPopUp(false);
		
		userDivisonField.setPopUpFields(Arrays.asList(popUp.split(",")));
		userDivisonField.setSearchFields(Arrays.asList(searchCns.split(",")));
		userDivisonField.setPromptFields(Arrays.asList(promptCns.split(",")));
		userDivisonField.setVisibleFields(Arrays.asList(visible.split(",")));
		
		String dicButtons = elType.attributeValue(AbstractDictionaryField.DICTIONARY_BUTTONS, AbstractDictionaryField.DOCUMENT_USER_DIVISION_DICTIONARY_BUTTONS_DEFAULT).replace(" ", "");
		
		String popButtons = elType.attributeValue(AbstractDictionaryField.POP_UP_BUTTONS, AbstractDictionaryField.DOCUMENT_USER_DIVISION_POP_UP_BUTTONS_DEFAULT).replace(" ", "");
		
		userDivisonField.setPopUpButtons(Arrays.asList(popButtons.split(",")));
		
		userDivisonField.setDicButtons(Arrays.asList(dicButtons.split(",")));
		DockindInfoBean dockindInfo = new DockindInfoBean("dictionary");
		FieldsXmlLoader.readFields(elType, dockindInfo);

        List<Field> dicFields = new ArrayList<Field>();
        dicFields.addAll(dockindInfo.getDockindFields());
        StringColumnField idField = null;
        Map<String, String> idNames = new LinkedHashMap<String, String>();
        for (Locale locale : LocaleUtil.AVAILABLE_LOCALES)
            idNames.put(locale.getLanguage(), "ID");
        if ("true".equals(elType.attributeValue("multiple")))
            idField = new StringColumnField(String.valueOf(dicFields.get(dicFields.size() - 1).getId() + 10), "ID_1", "ID_1", idNames, null,249,false, null, null, null);
        else
            idField = new StringColumnField(String.valueOf(dicFields.get(dicFields.size() - 1).getId() + 10), "ID", "ID", idNames, null,249,false, null, null, null);
        if (!dicFields.contains(idField))
        {
            idField.setHidden(true);
            idField.setSubmit(true);
            dicFields.add(0, idField);
        }

		userDivisonField.setFields(dicFields);
        if ("true".equals(elType.attributeValue("multiple")))
            userDivisonField.setMultiple(true);

		new DwrDictionaryBase(userDivisonField, null);
		return userDivisonField;
	}

}
