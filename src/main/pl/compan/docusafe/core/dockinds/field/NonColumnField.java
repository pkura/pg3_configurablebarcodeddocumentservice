package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.Database;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Klasa bazowa dla p�l nie zapisywanych w kolumnie tabeli dockindowej przy zapisie dokumentu
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public abstract class NonColumnField extends Field{
	protected boolean resettable = false;

    public NonColumnField(String id, String cn, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, "*none*", names, type, refStyle, classname, required, requiredWhen, availableWhen);
    }

    protected NonColumnField(Database.Column column, String id, String cn, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, column.getName(), names, type, refStyle, classname, required, requiredWhen, availableWhen);
    }

//    protected NonColumnField(String id, String cn, String column, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
//        super(id, cn, column, names, type, refStyle, classname, required, requiredWhen, availableWhen);
//    }

    @Override
    public final void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
        throw new UnsupportedOperationException("Field " + getType() + " is not persisted in column");
    }

    @Override
    public final boolean isPersistedInColumn() {
        return false;
    }
    
    public Object reset() {
    	return null;
    }

	public boolean isResettable() {
		return resettable;
	}

	public void setResettable(boolean resettable) {
		this.resettable = resettable;
	}
    
}
