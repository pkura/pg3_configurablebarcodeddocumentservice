package pl.compan.docusafe.core.dockinds.field.reader;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.DocumentBarCodeField;
import pl.compan.docusafe.core.dockinds.field.Field;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jtarasiuk
 * Date: 21.06.13
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 */
public class DocumentBarCodeFieldReader implements FieldReader
{
    @Override
    public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Field.Condition> availableWhen, List<Field.Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
    {
        return new DocumentBarCodeField(el.attributeValue("id"), el.attributeValue("cn"), refStyle, names, required, requiredWhen, availableWhen);
    }
}
