package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.AttachmentColumnField;
import pl.compan.docusafe.core.dockinds.field.AttachmentNonColumnField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class AttachmentFieldReader implements FieldReader {

	/**
	 * Pole za��cznika bez podanej kolumny tworzy obiekty
	 * {@link AttachmentNonColumnField} (czyli <code>cn</code> pola jest
	 * zapisywany w za��czniku {@link Attachment#setFieldCn(String)}). <br />
	 * Je�li podamy kolumn�, to tworzymy {@link AttachmentColumnField} -
	 * zapisujemy za��cznik jako kolumn� dokumentu.
	 */
	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen,
			List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {
		String column = el.attributeValue("column");
		Field field = null;
		boolean multiple = Boolean.parseBoolean(elType.attributeValue("multiple"));

		if (column == null) {
			field = new AttachmentNonColumnField(el.attributeValue("id"), elType.attributeValue("logic"), el.attributeValue("cn"), names, required, requiredWhen,
					availableWhen, multiple);
		} else {
			// kolumna istnieje
			field = new AttachmentColumnField(el.attributeValue("id"), el.attributeValue("cn"), column, names, required, requiredWhen,
					availableWhen);
		}
		if(elType.attributeValue("multiple") != null) {
			field.setMultiple(Boolean.parseBoolean(elType.attributeValue("multiple")));
		}
		return field;
	}

}
