package pl.compan.docusafe.core.dockinds.field;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.HtmlEditorValue;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class HtmlEditorColumnField extends Field implements HtmlEditorField{

	private static final Logger log = LoggerFactory.getLogger(HtmlEditorColumnField.class);
	private Object value;
	
	public HtmlEditorColumnField(String id, String column, String logic, String cn,
			Map<String, String> names, boolean required,
			List<Condition> requiredWhen, List<Condition> availableWhen,
			String value) throws EdmException {
		super(id, cn, column, names, Field.HTML_EDITOR, null, null, required, requiredWhen, availableWhen);
		if(logic != null && !logic.isEmpty()){
			this.logic = logic;
		}
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs)
			throws SQLException, EdmException {
		String value = rs.getString(getColumn());
        return new FieldValue(this, value, value);
	}

	@Override
	public void set(PreparedStatement ps, int index, Object value)
			throws FieldValueCoerceException, SQLException {
		Object val = simpleCoerce(value);
		if(val != null)
			ps.setString(index, (String) val);
		else{
			ps.setObject(index, val);
		}
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {       
        if(value == null) {
			this.value = null;
			return null;
		}
		if(value instanceof String) {
			this.value = (String) value;
		} else if(value instanceof HtmlEditorValue) {
			this.value = ((HtmlEditorValue)value).getHtml();
		} else {
			throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + " cannot be converted to String by HtmlEditorField");
		}
		return this.value;
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
			throws EdmException {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), 
				pl.compan.docusafe.core.dockinds.dwr.Field.Type.HTML_EDITOR);
	}
}
