package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;
import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.MoneyColumnField;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.core.dockinds.field.MoneyNonColumnField;

public class MoneyFieldReader implements FieldReader
{

    @Override
    public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {
        String column = el.attributeValue("column");

        if (column == null) {
            return new MoneyNonColumnField(el.attributeValue("id"), el.attributeValue("cn"), names, required, requiredWhen, availableWhen);

        } else {

            return new MoneyColumnField(el.attributeValue("id"), el.attributeValue("cn"), el.attributeValue("column"), names, required, requiredWhen, availableWhen);
        }
    }

}
