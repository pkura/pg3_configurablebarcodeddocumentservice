package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.viewserver.ViewServer;

public class AttachmentLinkLogic implements LinkFieldLogic
{

	private static final Logger log = LoggerFactory.getLogger(AttachmentLinkLogic.class);

	public String createLink(String docId) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
	{
Long binderId = 545L;
		   Long attachmentId = null;
	        pl.compan.docusafe.core.base.Attachment attach = null;

	        NativeCriteria nc = DSApi.context().createNativeCriteria("DS_ATTACHMENT", "d");
	        nc.setProjection(NativeExps.projection()
	                .addProjection("d.id"))
	            .add(NativeExps.eq("d.document_id", docId));
	           

	        CriteriaResult cr = nc.criteriaResult();
	        while (cr.next())
	        {
	            attachmentId = cr.getLong(0, null);
	            attach = Attachment.find(attachmentId,false);
	            break;
	        }
	       
	        String baseUrl = Docusafe.pageContext;
	        StringBuilder sb = new StringBuilder();
	        if(attachmentId != null && attach.getMostRecentRevision()!= null)
	        {
	            sb.append("<a name=\"DodajNowaWersje\" href=\"javascript:openToolWindow('/"+baseUrl+"/office/common/add-attachment-from-pages.action?");
	            sb.append("binderId="+binderId+"&attachmentId="+attachmentId+"&documentId="+docId+"','vs',800,600);\">");
	            sb.append("<img src=\"/"+baseUrl+"/img/add_icon.gif\" width=\"16\" height=\"16\"    height=\"17\" title=\"DodajNowaWersje\"/></a>");
	            sb.append("&nbsp;&nbsp;");
	            if(ViewServer.mimeAcceptable(attach.getMostRecentRevision().getMime()))
	            {
	              
	                sb.append("<a name=\"WyswietlZalacznikWprzegladarce\" " +
	                        "href=\"/"+baseUrl+"/repository/view-attachment-revision.do?asPdf=true&id="+
	                        attachmentId+"\">");
	                sb.append("<img src=\"/"+baseUrl+"/img/pdf.gif\" width=\"16\" height=\"16\"    height=\"17\" title=\"pobierz za��cznik\"/></a>&nbsp;&nbsp;");

	                
	                sb.append("<a name=\"WyswietlZalacznikWprzegladarce\" " +
	                        "href=\"/"+baseUrl+"/viewserver/viewer.action?id="+
	                        attachmentId+"&fax=false&width=1000&height=750&binderId="+binderId+"\" onclick=\"return !aTarget(this)\">");
	                sb.append("<img src=\"/"+baseUrl+"/img/wyswietl.gif\" width=\"16\" height=\"16\"    height=\"17\" title=\"WyswietlZalacznikWprzegladarce\"/></a>&nbsp;&nbsp;");

	             }
	        }
	        else
	        {
	            sb.append("<a name=\"DodajZalacznik\" href=\"javascript:openToolWindow('/"+baseUrl+"/office/common/add-attachment-from-pages.action?");
	            sb.append("documentId="+docId+"&binderId="+binderId+"','vs',800,600);\">");
	            sb.append("<img src=\"/"+baseUrl+"/img/add_icon.gif\" width=\"16\" height=\"16\"    height=\"17\" title=\"DodajNowaWersje\"/></a>");
	        }

	        return sb.toString();
	}

	@Override
	public String getLinkLabel()
	{
		return "";
	}
}
