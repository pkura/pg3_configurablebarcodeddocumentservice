package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.primitives.Ints;

public class DocumentInTrackingPasswordField extends NonColumnField implements CustomPersistence {

	protected static Logger log = LoggerFactory.getLogger(DocumentInTrackingPasswordField.class);
	private String type = "in";
	public DocumentInTrackingPasswordField(String id ,String cn,String type, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
		super(id, cn, names, Field.DOCUMENT_IN_TRACKING_PASSWORD, null, null, required, requiredWhen, availableWhen);
		//this.setSearchShow(false);
		
		if (type != null)
		{
			this.type = type;
		}
		
	}
	
	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		
		OfficeDocument doc = OfficeDocument.find(documentId);
		
		if (doc instanceof InOfficeDocument)
			return new FieldValue(this, ((InOfficeDocument) doc).getTrackingPassword());
		else return null;
		
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        if( value == null || (value.toString().equals(""))){
            return null;
        }

        if(value instanceof String)
        	return  value;
        else
        {
        	throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + "b�ad kowertowania  przez DocumentReferenceIdField");
        }
		
	}

	public void persist(Document doc, Object key) throws EdmException {
		
	   
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
		return  new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING);
	}
	
	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) 
	{
		
	}
}
