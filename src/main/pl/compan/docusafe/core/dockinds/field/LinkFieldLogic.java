package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.security.AccessDeniedException;

public interface LinkFieldLogic
{
	public String createLink(String id) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException;
	public String getLinkLabel();
}
