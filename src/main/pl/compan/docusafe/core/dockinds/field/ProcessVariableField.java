package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;

/**
 * User: Tomasz
 * Date: 03.04.14
 * Time: 12:02
 */
public interface ProcessVariableField {

    public abstract FieldValue provideFieldValue(long documentId, String activityId) throws EdmException;
}
