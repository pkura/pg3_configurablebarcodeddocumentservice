package pl.compan.docusafe.core.dockinds.field.reader;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.dwr.Popup;
import pl.compan.docusafe.core.dockinds.field.ButtonField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

import java.util.List;
import java.util.Map;

public class ButtonFieldReader implements FieldReader {
    @Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen,
			List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {

		ButtonField field = new ButtonField(el.attributeValue("id"), el.attributeValue("logic"), el.attributeValue("cn"), names, required, requiredWhen,
				availableWhen, elType.attributeValue("value"), elType.attributeValue("field-label"));
		
		boolean reset = Boolean.parseBoolean(elType.attributeValue("reset"));
		field.setResettable(reset);
        boolean validate = Boolean.parseBoolean(elType.attributeValue("validate"));
        field.setValidate(validate);

        Popup popup = new Popup();
        popup.setShowpopupcn(el.attributeValue("showpopupcn"));
        popup.setClosepopupcn(el.attributeValue("closepopupcn"));
        if(popup.isPopupPresent()) {
            String popupCn = popup.getShowpopupcn() != null ? popup.getShowpopupcn() : popup.getClosepopupcn();
            field.getPopups().put(popupCn, popup);
        }

		return field;
	}

}
