package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

/**
 *
 * Date: 01.08.13
 * Time: 14:36
 *
 */
public interface EnumBase {

    public void addEnum(Integer id, String cn, Map<String, String> titles, String[] args, boolean forceRemark, String fieldCn, boolean available);

    public List<EnumItem> getAvailableEnumItems();

    public List<EnumItem> getEnumItems();

    public List<EnumItem> getAllEnumItemsByAspect(String aspectCn, DocumentKind documentKind) throws EdmException;

    public List<EnumItem> getEnumItemsByAspect(String aspectCn, DocumentKind documentKind) throws EdmException;

    public List<EnumItem> getEnumItemsByAspect(String aspectCn,  FieldsManager fm) throws EdmException;

    public void setEnumItems(Map<Integer, EnumItem> enumItems);

    public void setEnumItems(List<EnumItem> enumItems);

    public void setRepoField(boolean repoField);

    public void setItemsReferencedForAll(Map<Integer, EnumItem> itemsReferencedForAll);

    public void setRefEnumItemsMap(Map<String, SortedSet<EnumItem>> refEnumItemsMap);

    public void setAvailableItems(Set<EnumItem> availableItems);

    public void setAllItems(Set<EnumItem> allItems);

    public List<EnumItem> getSortedEnumItems();

    public EnumItem getEnumItem(Integer id) throws EntityNotFoundException;

    public EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException;

    public EnumItem getEnumItemByTitle(String title) throws EntityNotFoundException;

    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, String id, String temp) throws EntityNotFoundException;

    public Set<EnumItem> getEnumItemsByRef(String id);
}
