package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import com.google.common.base.Preconditions;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.field.DocumentPersonField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.FieldsXmlLoader;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class DocumentPersonFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		String person = el.element("type").attributeValue("person");
		String cn = el.attributeValue("cn");
		Preconditions.checkNotNull(person, cn + ": person attribute must be set (xml = " + el.asXML() + ")");
		String logic = PersonDictionary.class.getName();
        if(StringUtils.isNotEmpty(elType.attributeValue("logic"))) {
            logic = elType.attributeValue("logic");
        }
		DocumentPersonField personField = new DocumentPersonField((el.attributeValue("id")), cn, logic, names, required, requiredWhen, availableWhen, person);

		String popUp = elType.attributeValue(AbstractDictionaryField.POP_UP_FIELDS, "").replace(" ", "");
		String searchCns = elType.attributeValue(AbstractDictionaryField.SEARCH_FIELDS, "").replace(" ", "");
		String promptCns = elType.attributeValue(AbstractDictionaryField.PROMPT_FIELDS, "").replace(" ", "");
		String visible = elType.attributeValue(AbstractDictionaryField.VISIBLE_FIELDS, "").replace(" ", "");
		String popButtons = elType.attributeValue(AbstractDictionaryField.POP_UP_BUTTONS, AbstractDictionaryField.POP_UP_BUTTONS_DEFAULT).replace(" ", "");
		
		if (elType.attribute("pop-up") != null)
			personField.setShowDictionaryPopUp(Boolean.valueOf(elType.attributeValue("pop-up")).booleanValue());
		else
			personField.setShowDictionaryPopUp(true);
		
		String dicButtons = "";
		if (personField.getShowDictionaryPopUp())
		{
			dicButtons = elType.attributeValue(AbstractDictionaryField.DICTIONARY_BUTTONS, AbstractDictionaryField.DICTIONARY_BUTTONS_DEFAULT).replace(" ", "");
		}
		else
		{
			dicButtons = elType.attributeValue(AbstractDictionaryField.DICTIONARY_BUTTONS, AbstractDictionaryField.DICTIONARY_BUTTONS_DEFAULT).replace(" ", "").replace("showPopup,", "");
		}
		personField.setPopUpButtons(Arrays.asList(popButtons.split(",")));
		personField.setDicButtons(Arrays.asList(dicButtons.split(",")));
		personField.setPopUpFields(Arrays.asList(popUp.split(",")));
		personField.setSearchFields(Arrays.asList(searchCns.split(",")));
		personField.setPromptFields(Arrays.asList(promptCns.split(",")));
		personField.setVisibleFields(Arrays.asList(visible.split(",")));

		String notNull = elType.attributeValue("not-nullable-fields");
		personField.setNotNullableFields(StringUtils.isNotBlank(notNull)?Arrays.asList(notNull.split(",")):null);

		DockindInfoBean dockInfo = new DockindInfoBean("dictionary");
		FieldsXmlLoader.readFields(elType, dockInfo);
		personField.setFields(dockInfo.getDockindFields());
		
		new DwrDictionaryBase(personField, null);
		return personField;
	}

}
