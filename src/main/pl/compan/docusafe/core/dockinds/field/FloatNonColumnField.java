package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.Field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static pl.compan.docusafe.util.I18nUtils.string;

/**
 *
 * Date: 11.07.13
 * Time: 15:42
 * Gosia
 */
public class FloatNonColumnField extends NonColumnField implements FloatField {

    protected Float value;

    /**
     * Konstruktor FloatNonColumnField
     *
     * @param id            id z xml'a, musi by� unikalne
     * @param cn            cn z xml'a, musi b� unikalne
     * @param names         nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param required      czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen  warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws pl.compan.docusafe.core.EdmException
     *
     */
    public FloatNonColumnField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, names, pl.compan.docusafe.core.dockinds.field.Field.FLOAT, null, null, required, requiredWhen, availableWhen);
    }
    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        return new FieldValue(this, value, value);
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        if (value == null) {
            return null;
        }
        if (value instanceof Float) {
            return (Float) value;
        }
        try {
            String s = value.toString().replace(",", ".");
            return Float.parseFloat(s);
        } catch (NumberFormatException e) {
            throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci") + " " + value + " " + string("DoLiczbyRzeczywistej") + " cn=" + getCn());
        }
    }

    @Override
    public Field getDwrField() throws EdmException {
        return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY);
    }
}
