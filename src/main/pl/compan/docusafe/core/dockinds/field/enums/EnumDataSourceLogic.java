package pl.compan.docusafe.core.dockinds.field.enums;

import pl.compan.docusafe.core.dockinds.field.EnumItem;

import java.util.List;

/**
 * Created by kk on 19.02.14.
 */
public interface EnumDataSourceLogic {

    List<EnumItem> getEnumItemsFromSource(String refFieldId) throws EnumSourceException;
    List<EnumItem> getEnumItemsFromSource() throws EnumSourceException;

}
