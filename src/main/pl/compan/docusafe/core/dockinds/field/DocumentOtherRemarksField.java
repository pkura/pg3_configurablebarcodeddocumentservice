package pl.compan.docusafe.core.dockinds.field;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.StringManager;

public class DocumentOtherRemarksField extends NonColumnField implements CustomPersistence  {

	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);
	
	public DocumentOtherRemarksField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
		super(id, cn, names, Field.DOCUMENT_OTHERREMARKS, null, null, required, requiredWhen, availableWhen);
		this.setSearchShow(false);
		
	}
	
	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		Document doc = Document.find(documentId);
		if (doc instanceof InOfficeDocument)
			return new FieldValue(this, ((InOfficeDocument) doc).getOtherRemarks());
		else if (doc instanceof  OutOfficeDocument)
			throw new EdmException("DocumentOtherRemarksField provideFieldValue doc class: "+doc.getClass());
		
		return null;

	}


	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
		if (value == null || (value.toString().equals("")))
			return null;
		String otherremarks = null;
		if (value instanceof String)
			otherremarks = (String)value;
		else
		{
			throw new FieldValueCoerceException(this.getName()
                    + ":" + sm.getString("NieMoznaSkonwertowacWartosci")
                    + " " + value + " " + "do Stringa");
		}
		return otherremarks;
	}

	public void persist(Document doc, Object key) throws EdmException {
		
		if (key!=null && key instanceof String && doc instanceof InOfficeDocument)
			((InOfficeDocument)doc).setOtherRemarks((String)key);
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.TEXTAREA);
	}
}
