package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.util.TextUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * Date: 08.07.13
 * Time: 13:37
 *
 */
public class StringColumnField extends Field implements StringField{
    private final static String DATEFORMAT_REGEX = "datefromat_regex:";
    /** d�ugo�� stringa */
    private final int length;
    /** wielko�� pola na formatce */
    private final Integer size;
    /**  Wyra�enie regularne do walidacji pola przez javascript */
    private final String validatorRegExp;

    /**
     *
     * @param id id z xml'a, musi by� unikalne
     * @param cn cn z xml'a, musi b� unikalne
     * @param column nazwa kolumny, nie mo�e by� null (niestety)
     * @param names nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param size
     * @param length
     * @param required
     * @param requiredWhen
     * @param availableWhen
     * @throws EdmException
     */
    public StringColumnField(String id, String cn, String column, Map<String, String> names, Integer size, int length, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String validatorRegExp) throws EdmException {
        super(id, cn, column, names, Field.STRING, null, null, required, requiredWhen, availableWhen);
        this.length = length;
        this.size = size;
        if (validatorRegExp != null)
            this.validatorRegExp = validatorRegExp;
        else
        {
            this.validatorRegExp = "^.{0," + length + "}$";
        }
    }

    @Override
    public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
        ps.setString(index, simpleCoerce(value));
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        String value = rs.getString(getColumn());
        return new FieldValue(this, value, value);
    }

    /**
     * Je�li null zwraca null, w innym przypadku zwraca odpowiednio obci�ty toString
     * @param value dowolny obiekt
     * @return null, je�li value == null, w przeciwnym przypadku trimowany toString o length <= length
     */
    @Override
    public String simpleCoerce(Object value) {
        if (value == null){
            return null;
        }
        String s = TextUtils.trimmedStringOrNull(value.toString());
        if (s == null){
            return null;
        }

        if (s.length() > length){
            s = s.substring(0, length);
        }

        /** Je�li chcemy aby domyslna wartosc by�o konretnej daty bierz�cej */
        if(s.startsWith(DATEFORMAT_REGEX))
        {
            String format = s.substring(DATEFORMAT_REGEX.length(), s.length());
            DateFormat df = new SimpleDateFormat(format);

            return df.format(Calendar.getInstance().getTime());
        }
        return s;
    }

    // === gettery dla JSPk�w ===

    public int getLength() {
        return length;
    }

    public Integer getSize() {
        return size;
    }

    public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
        pl.compan.docusafe.core.dockinds.dwr.Field field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(),
                this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING);
        field.setAutocompleteRegExp(getAutocompleteRegExp());
        if (getLength() > 250)
            field.setType(pl.compan.docusafe.core.dockinds.dwr.Field.Type.TEXTAREA);
        else
        {
            field.setValidatorRegExp(validatorRegExp);
        }

        return field;
    }
}
