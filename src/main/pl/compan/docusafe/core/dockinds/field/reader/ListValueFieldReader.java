package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.ListValueDbEnumField;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class ListValueFieldReader implements FieldReader
{

	private static final String TABLE_NAME_ATTR = "tableName";
	
	private static final String ID_COLUMN_NAME_ATTR = "idColumnName";
	
	private static final String CN_COLUMN_NAME_ATTR = "cnColumnName";
	
	private static final String TITLE_COLUMN_NAME_ATTR = "titleColumnName";
	
	private static final String AVAILABLE_COLUMN_NAME_ATTR = "availableColumnName";
	
	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		return new ListValueDbEnumField(
	    			el.attributeValue("id"),
					el.attributeValue("cn"),
	                el.attributeValue("column"),
					names, 
					refStyle,
					required, 
					requiredWhen, 
					availableWhen, 
					elType.attributeValue(TABLE_NAME_ATTR), 
					elType.attributeValue(ID_COLUMN_NAME_ATTR), 
					elType.attributeValue(CN_COLUMN_NAME_ATTR), 
					elType.attributeValue(TITLE_COLUMN_NAME_ATTR), 
					elType.attributeValue(AVAILABLE_COLUMN_NAME_ATTR));
	}

}
