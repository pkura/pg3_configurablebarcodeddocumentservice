package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Pole tak/nie wy�wietlane w formie checkboxa
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class BooleanField extends Field {

    /**
     * Konstruktor BooleanField, wymaga podania podstawowych danych dla ka�dego Fielda
     *
     * @param id            id z xml'a, musi by� unikalne
     * @param cn            cn z xml'a, musi b� unikalne
     * @param column        nazwa kolumny, nie mo�e by� null (niestety)
     * @param names         nazwy w r�nych j�zykach - kod kraju -> nazwa, zwykle tylko 'pl'
     * @param required      czy jest wymagany, uwaga na kombinacje z requiredWhen
     * @param requiredWhen  warunki kiedy jest wymagany, mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @param availableWhen warunki kiedy jest dost�pny (widoczny), mo�e by� null, wtedy zast�powany jest pust� kolekcj�
     * @throws pl.compan.docusafe.core.EdmException
     */
    public BooleanField(String id, String cn, String column, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, column, names, Field.BOOL, null, null, required, requiredWhen, availableWhen);
    }

    @Override
    public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
        if(value == null){
            ps.setObject(index, null);
        } else {
            ps.setBoolean(index, simpleCoerce(value));
        }
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        boolean bool = rs.getBoolean(getColumn());
        Boolean value = rs.wasNull() ? null : bool;
        return new FieldValue(this, value, (value == null || !value) ? "nie" : "tak");
    }

    /**
     * Pr�buje skonwerowa� warto�� na Boolean:
     * null -> zwraca null
     * Boolean -> zwraca value
     * CharSequence -> zwraca Boolean.valueOf(value.toString())
     * w innym przypadku -> wyrzuca FieldValueCoerceException
     * @param value najcz�ciej String ale mo�e tak�e by� typ docelowy
     * @return
     * @throws FieldValueCoerceException gdy value jest nieobs�ugiwanego typu
     */
    @Override
    public Boolean simpleCoerce(Object value) throws FieldValueCoerceException {
        if(value == null){
            return null;
        }
        Boolean ret;

        if(value instanceof Boolean){
            ret = (Boolean) value;
        } else if (value instanceof CharSequence) {
        	if (value.equals("on"))
        		ret = true;
        	else
        		ret = Boolean.valueOf(value.toString());
        } else {
            throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + " cannot be converted to Boolean by BooleanField");
        }

        return ret;
    }
    
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
	}
}
