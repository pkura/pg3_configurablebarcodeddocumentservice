package pl.compan.docusafe.core.dockinds.field;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Pattern;

import static pl.compan.docusafe.core.users.Profile.*;

/**
 * Klasa reprezentująca pole wyliczeniowe zawierające użytkowników z systemu, w odróżnieniu
 * od DSUser, klucz w tym polu jest Integerem (jak inne enumy)
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id: DSUserEnumField.java,v 1.5 2010/07/28 07:19:40 pecet1 Exp $
 */
public final class DSUserEnumField extends AbstractEnumColumnField
{
    private static final Logger log = LoggerFactory.getLogger(DSUserEnumField.class);
    StringManager sm =
        GlobalPreferences.loadPropertiesFile(DSUserEnumField.class.getPackage().getName(),null);

    private String refField;
    private String refGuid;
	private boolean dsuserAsLtNFstNExtName = AvailabilityManager.isAvailable("dsuserFieldAsLastNameFirstNameExternalName");
    private static final Multimap<String, DSUserEnumField> usersToField
        = Multimaps.synchronizedListMultimap(ArrayListMultimap.<String, DSUserEnumField>create());

    public static final String STYLE_ALL_CHILD_USERS = "all-child-users";
    public static final Pattern STYLE_PROFILE_ID_ADDS = Pattern.compile("profileIdAdds='.*'");

    public DSUserEnumField(String refStyle, String id, String cn, String column, Map<String,String> names,
            boolean required, List<Condition> requiredWhen, List<Condition> availableWhen,boolean sort, String refField, String refGuid) throws EdmException
    {
        super(id, cn, column, names, DSUSER, refStyle, null, required, requiredWhen, availableWhen);
                usersToField.put(cn, this);
                this.refField = refField;
                this.refGuid = refGuid;
    	//super(refStyle,id,cn,column,names,required,requiredWhen,availableWhen,false);
    }

    @Override
    public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException {
        if(value == null){
            ps.setObject(index, null);
        } else {
            ps.setInt(index, simpleCoerce(value));
        }
    }

    public static void reloadForAll() throws EdmException {
    	for (DSUserEnumField field : usersToField.values()) {
    		field.loadEnumItem();
    	}
    }
    
    /**
     * Uaktualnia liste uzytkownikow do wyswietlenia
     * @throws EdmException
     */
	private synchronized void loadEnumItem() throws EdmException
	{
		List<EnumItem> items = new ArrayList<EnumItem>();
		//        SortedSet<EnumItem> all = new TreeSet<EnumItem>(EnumItem.COMPARATOR_BY_CN);

		List<DSUser> users = DSUser.listWithDeleted(DSUser.SORT_LASTNAME_FIRSTNAME);
		for (DSUser user : users)
		{
			boolean aviable = !(user.isDeleted() /*|| user.isLoginDisabled()*/);
			Map<String, String> titles = new HashMap<String, String>();
			if (dsuserAsLtNFstNExtName)
				titles.put("pl", user.getLastnameFirstnameWithOptionalIdentifier());
			else
				titles.put("pl", user.asLastnameFirstname());
			if (user.getId() > (long) Integer.MAX_VALUE)
			{
				throw new RuntimeException("utrata dokładności - niemożliwe stworzenie pola");
			}
			EnumItem ei = new EnumItemImpl(user.getId().intValue(), user.getName(), titles, new String[1], false, getCn(), aviable);
			items.add(ei);
		}
		setEnumItems(items);
	}
    
    /**
     * Możliwość definiowania listy użytkowników w logice dokumentu. przykład wywołania:
     *  @see ((DSUserEnumField)fm.getField(USER_CN)).loadEnumItem( DSUser.listAcceptanceSubordinates());
     * @param users
     * @throws EdmException 
     */
	public synchronized void loadEnumItem(List<DSUser> users) throws EdmException
	{
		List<EnumItem> items = new ArrayList<EnumItem>();
		for (DSUser user : users)
		{
			boolean aviable = !(user.isDeleted() /*|| user.isLoginDisabled()*/);
			Map<String, String> titles = new HashMap<String, String>();
			if (dsuserAsLtNFstNExtName)
				titles.put("pl", user.getLastnameFirstnameWithOptionalIdentifier());
			else
				titles.put("pl", user.asLastnameFirstname());
			if (user.getId() > (long) Integer.MAX_VALUE)
			{
				throw new RuntimeException("utrata dokładności - niemożliwe stworzenie pola");
			}
			EnumItem ei = new EnumItemImpl(user.getId().intValue(), user.getName(), titles, new String[1], false, getCn(), aviable);
			items.add(ei);
		}
		setEnumItems(items);
	}
    
    @Override
    public Object asObject(Object key) throws IllegalArgumentException, EdmException {
        if (key == null) {
            return null;
        } else {
            return getEnumItem((Integer)key);
        }
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        int i = rs.getInt(getColumn());
        Integer value = rs.wasNull() ? null : new Integer(i);
        return new FieldValue(this, value);
    }
   
    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EntityNotFoundException
    {
        Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
	

		if (isMultiple() && fieldsValues.get(getCn()) != null)
		{
			for (Object str : (Iterable) fieldsValues.get(getCn())) 
			{
				if (str instanceof Integer)
					enumSelected.add(((Integer)str).toString());
				else if (str instanceof String)
					enumSelected.add((String)str);
			}
		}
		else if (!isMultiple())
		{
			enumValues.put("","--wybierz--");
			if (fieldsValues != null && fieldsValues.get(getCn()) != null)
			{
				enumSelected.add(fieldsValues.get(getCn()).toString());
	        } else 
	        {
				enumSelected.add("");
	        }
		}

		if (getRefField()!=null)
		{
            List<DSUser> users = new ArrayList<DSUser>();
			Integer id = null;
            if (fieldsValues.get(getRefField()) instanceof String) {
                id = new Integer((String)fieldsValues.get(getRefField()));
            }
			else {
                id = (Integer)fieldsValues.get(getRefField());
            }
            if (id!=null)
			{
                try {
                    if (STYLE_ALL_CHILD_USERS.equalsIgnoreCase(getRefStyle())) {
                        users = Arrays.asList(DSDivision.findById(id).getUsers(true));
                        Collections.sort(users, DSUser.LASTNAME_COMPARATOR);
                    } else {
                        users = Arrays.asList(DSDivision.findById(id).getUsers());
                        Collections.sort(users, DSUser.LASTNAME_COMPARATOR);
                    }

                    restrainUsersToProfile(users);
                } catch (EdmException e1) {
                    System.out.println(e1.getMessage());
                }
            }
			// pobranie wartosci enumFielda
			for (DSUser user : users)
			{
				if (!user.isDeleted() || enumSelected.contains(user.getId().toString()))
					if (dsuserAsLtNFstNExtName)
						enumValues.put(user.getId().toString(), user.getLastnameFirstnameWithOptionalIdentifier());
					else
						enumValues.put(user.getId().toString(), user.asLastnameFirstname());
			}
			
		}
		else if (getRefGuid()!=null)
		{
			List<DSUser> users = new ArrayList<DSUser>();

			try
			{
				users = Arrays.asList(DSDivision.find(getRefGuid()).getUsers());
				Collections.sort(users, DSUser.LASTNAME_COMPARATOR);
			} catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}

			restrainUsersToProfile(users);

			// pobranie wartosci enumFielda
			boolean addToEnumValues = true;
			for (DSUser user : users)
			{

				enumValues.put(user.getId().toString(), user.asLastnameFirstname());
				if (enumSelected.contains(user.getId()))
					addToEnumValues = false;
			}
			try
			{
				if (addToEnumValues && enumSelected.size() > 0 && !"".equals(enumSelected.get(0)))
				{
					DSUser selectedUser = DSUser.findById(Long.valueOf(enumSelected.get(0)));
					if (dsuserAsLtNFstNExtName)
						enumValues.put(selectedUser.getId().toString(), selectedUser.getLastnameFirstnameWithOptionalIdentifier());
					else
						enumValues.put(selectedUser.getId().toString(), selectedUser.asLastnameFirstname());
				}
			} catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}

		}	
		else 
		{
            if (!fillEnumUsersByProfile(enumValues, enumSelected)){
                for (EnumItem item : getEnumItems())
                {
                    if (item.isAvailable() || enumSelected.contains(item.getId()))
                        enumValues.put(item.getId().toString(), item.getTitle());
                }
            }
		}
		return new EnumValues(enumValues, enumSelected);		
    }

	private boolean fillEnumUsersByProfile(Map<String, String> enumValues, List<String> enumSelected)
	{
		String profileId = getProfileId();
		if (profileId == null)
			return false;

		try
		{
			Profile profile = findById(Long.parseLong(profileId));
			Set<DSUser> usersSet = profile.getUsers();
			List<DSUser> users = new ArrayList<DSUser>(usersSet);
			Collections.sort(users, DSUser.LASTNAME_COMPARATOR);

			for (DSUser user : users)
			{
				if (!user.isDeleted() || enumSelected.contains(user.getId().toString()))
					if (dsuserAsLtNFstNExtName)
						enumValues.put(user.getId().toString(), user.getLastnameFirstnameWithOptionalIdentifier());
					else
						enumValues.put(user.getId().toString(), user.asLastnameFirstname());
			}
		} catch (Exception e)
		{
			log.error("Nie odnaleziono profilu o id=" + profileId + " dla danego rodzaju pola");
		}
		return true;
	}

    private void restrainUsersToProfile(List<DSUser> users) {
        String profileId = getProfileId();
        if (profileId!=null){
            List<DSUser> copy = users;
            users = new ArrayList<DSUser>();
            try{
                Profile profile = findById(Long.parseLong(profileId));
                for (DSUser u : copy){
                    for (String pn : u.getProfileNames()){
                        if (profile.getName().equals(pn)){
                            users.add(u);
                            break;
                        }
                    }
                }
            }catch(Exception e){
                log.error("Nie odnaleziono profilu o id="+profileId+" dla danego rodzaju pola");
            }
        }
    }

    public String getProfileId(){
        String profileName = null;
        if (STYLE_PROFILE_ID_ADDS.matcher(getRefStyle()).find())
            profileName = TextUtils.getTextFromSignToNextSign(getRefStyle(),"'",1);
        return StringUtils.isNotBlank(profileName) ? Docusafe.getAdditionProperty(profileName) : null;
    }

	public String getRefField() {
		return refField;
	}

	public void setRefField(String refField) {
		this.refField = refField;
	}

	public String getRefGuid() {
		return refGuid;
	}

	public void setRefGuid(String refGuid) {
		this.refGuid = refGuid;
	}

    @Override
    public void setEnumItems(Map<Integer, EnumItem> enumItems) {
        enumDefault.setEnumItems(enumItems);
    }

    @Override
    public void setRepoField(boolean repoField) {
    }

    @Override
    public void setItemsReferencedForAll(Map<Integer, EnumItem> itemsReferencedForAll) {

    }

    @Override
    public void setRefEnumItemsMap(Map<String, SortedSet<EnumItem>> refEnumItemsMap) {
        enumDefault.setRefEnumItemsMap(refEnumItemsMap);
    }

    @Override
    public void setAvailableItems(Set<EnumItem> availableItems) {
        enumDefault.setAvailableItems(availableItems);
    }

    @Override
    public void setAllItems(Set<EnumItem> allItems) {
        enumDefault.setAllItems(allItems);
    }

    @Override
    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, String id, String temp) throws EntityNotFoundException {
        return getEnumItemsForDwr(fieldsValues);
    }
}
