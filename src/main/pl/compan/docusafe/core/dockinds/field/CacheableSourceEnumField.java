package pl.compan.docusafe.core.dockinds.field;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.enums.EnumDataSourceLogic;
import pl.compan.docusafe.core.dockinds.field.enums.EnumSourceException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by kk on 18.02.14.
 */
public class CacheableSourceEnumField extends ExternalSourceEnumField implements EnumDataSourceLogic {

    private static final Logger log = LoggerFactory.getLogger(CacheableSourceEnumField.class);

    private final LoadingCache<Integer, List<EnumItem>> ENUM_CACHE = CacheBuilder.newBuilder()
            .softValues().expireAfterWrite(15, TimeUnit.SECONDS).build(new CacheLoader<Integer, List<EnumItem>>() {
                @Override
                public List<EnumItem> load(Integer key) throws Exception {
                    return getEnumSourceLogic().getEnumItemsFromSource();
                }
            });

    private final LoadingCache<String, List<EnumItem>> ENUM_BY_REFERENCE_CACHE = CacheBuilder.newBuilder()
            .softValues().expireAfterWrite(15, TimeUnit.SECONDS).build(new CacheLoader<String, List<EnumItem>>() {
                @Override
                public List<EnumItem> load(String refId) throws Exception {
                    return getEnumSourceLogic().getEnumItemsFromSource(refId);
                }
            });

    @Override
    public List<EnumItem> getEnumItemsFromSource() {
        try {
            return ENUM_CACHE.get(0);
        } catch (ExecutionException e) {
            log.error(e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    @Override
    public List<EnumItem> getEnumItemsFromSource(String refId) throws EnumSourceException {
        try {
            return ENUM_BY_REFERENCE_CACHE.get(refId);
        } catch (ExecutionException e) {
            throw new EnumSourceException(e);
        }
    }

    public CacheableSourceEnumField(String id, String cn, String column, Map<String, String> names, boolean auto, String type, String logic, String refField, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, column, names, type, auto, logic, refField, refStyle, classname, required, requiredWhen, availableWhen);
    }
}