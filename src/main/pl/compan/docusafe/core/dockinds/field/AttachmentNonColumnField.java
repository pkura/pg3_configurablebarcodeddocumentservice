package pl.compan.docusafe.core.dockinds.field;

import org.apache.commons.fileupload.FileItem;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.AttachmentValue;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MimetypesFileTypeMap;
import pl.compan.docusafe.web.viewserver.ViewServer;
import pl.compan.docusafe.webwork.FormFile;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Kamil
 */
public class AttachmentNonColumnField extends NonColumnField implements CustomPersistence, AttachmentField {
	private static final long serialVersionUID = 2303759897940373975L;
	private static final Logger log = LoggerFactory.getLogger(AttachmentNonColumnField.class);
	private Attachment attachment = null;
	protected AttachmentFieldLogic logic;

	public AttachmentNonColumnField(String id, String logic, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen,
			List<Condition> availableWhen, boolean multiple) throws EdmException {
		super(id, cn, names, Field.ATTACHMENT, null, null, required, requiredWhen, availableWhen);
		
		if (logic != null)
        {
            try
            {
                Class<?> c = Class.forName(logic);
                this.logic = (AttachmentFieldLogic) c.getConstructor().newInstance();
            }
            catch (Exception ex)
            {
                log.error(ex.getMessage(), ex);
            }
        } else if(multiple) {
        	this.logic = new DocumentMultipleAttachmentFieldLogic();
        }
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        FieldValue fieldValue = new FieldValue(this, null, null);

        if(logic != null) {
            this.attachment = logic.provideFieldValue(this, fieldValue, documentId, rs);
        } else {

            Document doc = Document.find(documentId);
            Attachment att = doc.getAttachmentByFieldCn(this.getCn());
            if (att != null) {
                AttachmentRevision revision = att.getMostRecentRevision();
                fieldValue.setKey(revision.getId());
                fieldValue.setValue(att);
                attachment = att;
            }
        }

		return fieldValue;
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
		if (value == null)
			return null;
		else if (value instanceof File) {
			File file = (File) value;
			FormFile formFile = new FormFile(file, file.getName(), MimetypesFileTypeMap.getInstance().getContentType(file));
			return formFile;
		} else if (value instanceof Attachment) {
			return ((Attachment) value).getId();
		}
		return null;
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(),
				pl.compan.docusafe.core.dockinds.dwr.Field.Type.ATTACHMENT);
	}

	@Override
	public AttachmentValue getAttachmentForDwr(Long attachmentId) throws EdmException {
		AttachmentValue attVal = null;
        if(attachmentId == null)
            return attVal;
        Attachment value = DSApi.getObject(Attachment.class, attachmentId);
        AttachmentRevision mostRecentRevision = value.getMostRecentRevision();
        attVal = new AttachmentValue();
        attVal.setAttachmentId(value.getId());
        attVal.setTitle(value.getTitle());
        attVal.setUserSummary(DSUser.safeToFirstnameLastname(mostRecentRevision.getAuthor()));
        attVal.setCtime(mostRecentRevision.getCtime());
        attVal.setRevisionId(mostRecentRevision.getId());
        attVal.setRevisionMime(mostRecentRevision.getMime());
        attVal.setMimeAcceptable(ViewServer.mimeAcceptable(attVal.getRevisionMime()));
        attVal.setMimeXml(ViewServer.mimeXml(attVal.getRevisionMime()));


//		attVal.setAttachmentId(attachment.getId());
//		attVal.setTitle(attachment.getTitle());
//		attVal.setUserSummary(DSUser.safeToFirstnameLastname(attachment.getAuthor()));
//		attVal.setCtime(attachment.getCtime());
//		attVal.setRevisionId(attachment.getMostRecentRevision().getId());
//		attVal.setRevisionMime(attachment.getMostRecentRevision().getMime());
//		attVal.setMimeAcceptable(ViewServer.mimeAcceptable(attVal.getRevisionMime()));
//		attVal.setMimeXml(ViewServer.mimeXml(attVal.getRevisionMime()));

		return attVal;

	}

	@Override
	public void persist(Document doc, Object key) throws EdmException {
		if(this.logic != null) {
			try {
				this.logic.persist(doc, this, (List<FileItem>) key);
			} catch(IOException e) {
				throw new EdmException(e.getLocalizedMessage(), e);
			}
		} else {
			FormFile formFile = (FormFile) key;
			doc.setPermissionsOn(false);
			Attachment attachment = new Attachment(formFile.getNameWithoutExtension());
			attachment.setFieldCn(this.getCn());
			attachment.setEditable(false);
			doc.createAttachment(attachment);
			attachment.createRevision(formFile.getFile()).setOriginalFilename(formFile.getName());
		}
	}

}
