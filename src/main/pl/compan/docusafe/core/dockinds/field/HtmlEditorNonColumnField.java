package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.HtmlEditorValue;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class HtmlEditorNonColumnField extends NonColumnField implements HtmlEditorField{
	
	private static final Logger log = LoggerFactory.getLogger(HtmlEditorNonColumnField.class);
	
	private String value;
	
	public HtmlEditorNonColumnField(String id, String logic, String cn,
			Map<String, String> names, boolean required,
			List<Condition> requiredWhen, List<Condition> availableWhen,
			String value) throws EdmException {
		super(id, cn, names, Field.HTML_EDITOR, null, null, required, requiredWhen, availableWhen);
		if(logic != null && !logic.isEmpty())
			this.logic = logic;
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), 
				pl.compan.docusafe.core.dockinds.dwr.Field.Type.HTML_EDITOR);
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs)
			throws SQLException, EdmException {
		return new FieldValue(this, value, value);
	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
		if(value == null) {
			this.value = null;
			return null;
		}
		if(value instanceof String) {
			this.value = (String) value;
		} else if(value instanceof HtmlEditorValue) {
			this.value = ((HtmlEditorValue)value).getHtml();
		} else {
			throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + " cannot be converted to String by HtmlEditorField");
		}
		return this.value;
	}

	@Override
	public String reset() {
		this.value = "";
		return this.value;
	}
}
