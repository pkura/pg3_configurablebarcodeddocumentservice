package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.Field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 *
 * Date: 11.07.13
 * Time: 10:48
 * Gosia
 */
public class IntegerNonColumnField extends NonColumnField implements IntegerField {

    protected Integer value;

    public IntegerNonColumnField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, names, pl.compan.docusafe.core.dockinds.field.Field.INTEGER, null, null, required, requiredWhen, availableWhen);
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        return new FieldValue(this, value, value);
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        if(value == null){
            return null;
        }
        if ((value instanceof String && String.valueOf(value).length() == 0)) return null;
        if(value instanceof Integer) {
            return (Integer) value;
        }

        if(value instanceof CharSequence){
            try {
                return Integer.valueOf(value.toString().replaceAll(" ",""));
            } catch (NumberFormatException ex) {
                throw new FieldValueCoerceException(value.toString().replaceAll(" ","") + " cannot be converted to Integer by IntegerNonColumnField");
            }
        }

        throw new FieldValueCoerceException("type " + value.getClass().getSimpleName() + " cannot be converted to Integer by IntegerNonColumnField");
    }

    @Override
    public Field getDwrField() throws EdmException {
        return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER);
    }

    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getCn() == null) ? 0 : getCn().hashCode());
        return result;
    }
}
