package pl.compan.docusafe.core.dockinds.field;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.office.*;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.field.DocumentPersonField.PersonType;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.StringManager;

public class DocumentParamsField extends NonColumnField implements CustomPersistence  {

	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Field.class.getPackage().getName(), null);

	private final ParamName type;
	
	public enum ParamName
	{
		// wystepujace w pismach wychodzących
		WEIGHTKG, WEIGHTG, STAMPFEE, WEIGHT, ZPO, ADDITIONAL_ZPO
		// pimsa przychodzace:
	}
	
	public DocumentParamsField(String id, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, String paramName, String format) throws EdmException {
		super(id, cn, names, Field.DOCUMENT_PARAMS, null, null, required, requiredWhen, availableWhen);
		this.setFormat(format);
		this.type = ParamName.valueOf(paramName.toUpperCase());
	}
	
	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		OfficeDocument doc = OfficeDocument.find(documentId);
		
		if(ParamName.WEIGHTKG == type)
		{
			return new FieldValue(this, ((OutOfficeDocument)doc).getWeightKg());
		}
		else if(ParamName.WEIGHTG == type)
		{
			return new FieldValue(this, ((OutOfficeDocument)doc).getWeightG());
		}
		else if(ParamName.STAMPFEE == type)
		{
			return new FieldValue(this, ((OutOfficeDocument)doc).getStampFee());
		}
		else if(ParamName.ZPO == type)
        {
            return new FieldValue(this, ((OutOfficeDocument)doc).getZpo());
        }
        else if(ParamName.ADDITIONAL_ZPO == type)
        {
            return new FieldValue(this, ((OutOfficeDocument)doc).getAdditionalZpo());
        }
		else if(ParamName.WEIGHT == type)
		{
			if(((OutOfficeDocument)doc).getWeightKg() != null)
			{
				if(((OutOfficeDocument)doc).getWeightG() != null)
				{
					return new FieldValue(this, new BigDecimal( (double)((OutOfficeDocument)doc).getWeightKg() + (double)((OutOfficeDocument)doc).getWeightG()/(float)1000  ));
				}
				else
				{
					return new FieldValue(this, new BigDecimal( (double)((OutOfficeDocument)doc).getWeightKg()));
				}
			}
			else if(((OutOfficeDocument)doc).getWeightG() != null)
			{
				return new FieldValue(this, (double)((OutOfficeDocument)doc).getWeightG()/(float)1000);
			}
			else
			{
				return null;
			}
		}
		
		return new FieldValue(this, null);
	}


	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException {
		if (value == null)
			return null;		
		Object paramValue = null;
		// Typy Integer
		if(ParamName.WEIGHTKG == type || ParamName.WEIGHTG == type)
		{
			if (value instanceof Integer)
				paramValue = (Integer)value;
			else if (value instanceof String){
				if(StringUtils.isBlank((String) value))
					value= "0";
			
					paramValue = Integer.valueOf((String)value);
			}
			else
			{
				throw new FieldValueCoerceException(this.getName()
	                    + ": " + sm.getString("NieMoznaSkonwertowacWartosci")
	                    + " " + value + " " + "do typu Integer");
			}		
		}		
		else if(ParamName.STAMPFEE == type || ParamName.WEIGHT == type)
		{
			if(AvailabilityManager.isAvailable("stampfee.dontFillValueIfNotComplited")) {
				if (value instanceof BigDecimal) {
					paramValue = (BigDecimal)value;
				} else if (value instanceof String){
					if( StringUtils.isBlank((String) value)){
						value= "";
					}
					if(((String) value).length() > 0){
						paramValue = new BigDecimal(  (String)value);
					} else {
						paramValue = null;
					}
				}
			} else {
				if (value instanceof BigDecimal) 
					paramValue = (BigDecimal)value;
				else if (value instanceof String){
					if( StringUtils.isBlank((String) value))
						value= "0";
					
					paramValue = new BigDecimal(  (String)value);
				}else
				{
					throw new FieldValueCoerceException(this.getName()
		                    + ": " + sm.getString("NieMoznaSkonwertowacWartosci")
		                    + " " + value + " " + "do typu BigDecimal");
				}	
			}
		}
		else if(ParamName.ZPO == type)
		{
			if(value == null)
				paramValue = new Boolean(false);
			else if (value instanceof Boolean)
				paramValue = (Boolean)value;
			else if (value instanceof String)
				paramValue = new Boolean((String)value);
			else
			{
				throw new FieldValueCoerceException(this.getName()
	                    + ": " + sm.getString("NieMoznaSkonwertowacWartosci")
	                    + " " + value + " " + "do typu Boolean");
			}
		}
        else if(ParamName.ADDITIONAL_ZPO == type)
        {
            if(value == null)
                paramValue = new Boolean(false);
            else if (value instanceof Boolean)
                paramValue = (Boolean)value;
            else if (value instanceof String)
                paramValue = new Boolean((String)value);
            else
            {
                throw new FieldValueCoerceException(this.getName()
                        + ": " + sm.getString("NieMoznaSkonwertowacWartosci")
                        + " " + value + " " + "do typu Boolean");
            }
        }
        return paramValue;
	}

	public void persist(Document doc, Object key) throws EdmException {
		//zmiany 24-07 dodanie sprawdzenia czy null i czy String a takze zmienionoe weight type
		if(key==null)return;
		if(ParamName.WEIGHTKG == type)
		{	
			int temp;
			if(key instanceof String)temp=Integer.parseInt((String)key);
			else temp=(Integer)key;
			((OutOfficeDocument)doc).setWeightKg(temp);
		}
		else if(ParamName.WEIGHTG == type)
		{
			int temp;
			if(key instanceof String)temp=Integer.parseInt((String)key);
			else temp=(Integer)key;
			((OutOfficeDocument)doc).setWeightG(temp);
		}
		else if(ParamName.STAMPFEE == type)
		{	
			BigDecimal temp;
			if(key instanceof String)temp=BigDecimal.valueOf(Long.parseLong((String)key));
			else temp=(BigDecimal)key;
			((OutOfficeDocument)doc).setStampFee(temp);
		}
		else if(ParamName.ZPO == type)
		{
			boolean temp;
			if(key instanceof String)temp=Boolean.parseBoolean((String)key);
			else temp=(Boolean)key;
			((OutOfficeDocument)doc).setZpo(temp);
		}
        else if(ParamName.ADDITIONAL_ZPO == type)
        {
            boolean temp;
            if(key instanceof String)temp=Boolean.parseBoolean((String)key);
            else temp=(Boolean)key;
            ((OutOfficeDocument)doc).setAdditionalZpo(temp);
        }
		else if(ParamName.WEIGHT == type)
		{	
			BigDecimal temp;
			if(key instanceof String){temp=new BigDecimal((String)key);}
			else if(key instanceof BigDecimal){temp=(BigDecimal)key;}
			else return;
			temp = temp.setScale(3, RoundingMode.HALF_UP);
            int grossPart = temp.intValue();
            int fracPart = temp.subtract(new BigDecimal(grossPart)).movePointRight(3).intValue();
            ((OutOfficeDocument)doc).setWeightG(fracPart);
            ((OutOfficeDocument)doc).setWeightKg(grossPart);
			
		}
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException 
	{
		if(ParamName.WEIGHTKG == type || ParamName.WEIGHTG == type)
		{
			return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER);
		}
		else if (ParamName.WEIGHT == type)
		{
			return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY);
		}
		else if (ParamName.STAMPFEE == type)
		{
			return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY);
		}
		else if (ParamName.ZPO == type)
		{
			return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
        else if (ParamName.ADDITIONAL_ZPO == type)
        {
            return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        }
		return null;
	}
}
