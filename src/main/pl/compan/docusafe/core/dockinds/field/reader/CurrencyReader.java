package pl.compan.docusafe.core.dockinds.field.reader;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.CurrencyEnumField;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.Field;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class CurrencyReader implements FieldReader{

    private static final String CURRENCY_TABLENAME = "dsg_currency";

    @Override
    public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Field.Condition> availableWhen, List<Field.Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {
        CurrencyEnumField field;
        boolean auto = (elType.attributeValue("auto") != null) && Boolean.parseBoolean(elType.attributeValue("auto"));

        field = new CurrencyEnumField(
                el.attributeValue("id"),
                el.attributeValue("cn"),
                el.attributeValue("column"),
                names,
                Field.DATA_BASE,
                refStyle,
                required,
                requiredWhen,
                availableWhen,
                CURRENCY_TABLENAME,
                elType.attributeValue("ref-field"),
                elType.attributeValue("asso-field"),
                elType.attributeValue("correction"),
                auto,
                "true".equals(elType.attributeValue("empty-on-start")),
                "true".equals(elType.attributeValue("allow-not-applicable-item")),
                elType.attributeValue("reload-at-each-dwr-get")==null ? true : "true".equals(elType.attributeValue("reload-at-each-dwr-get")),
                "true".equals(elType.attributeValue("repository-field")));

        String sort = elType.attributeValue("sort");
        if (sort != null)
        {
            field.setSort(DataBaseEnumField.Sort.valueOf(sort.toUpperCase()));
        }

        field.initialize();
        return field;
    }
}
