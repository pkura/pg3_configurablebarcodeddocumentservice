package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.EnumRefField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.util.XmlUtils;

public class EnumRefFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		Field field;
		Element elRef = elType.element("field-ref");
		String fieldRefCn = elRef != null ? elRef.attributeValue("cn") : null;
		boolean withRefCodes = elRef != null && "true".equals(elRef.attributeValue("with-ref-codes"));
		EnumRefField enumField = new EnumRefField(refStyle, fieldRefCn, (el.attributeValue("id")), el.attributeValue("cn"), el.attributeValue("column"), names, required, requiredWhen, availableWhen, withRefCodes, "true".equals(el.attributeValue("sort")));
		// wczytywanie poszczególnych wartości wyliczeniowych
		List elItemsList = elType.elements("enum-items");
		if (elItemsList != null && elItemsList.size() > 0)
		{
			for (Iterator itemsIter = elItemsList.iterator(); itemsIter.hasNext();)
			{
				Element elItems = (Element) itemsIter.next();
				List elItemList = elItems.elements("enum-item");
				for (Iterator itemIter = elItemList.iterator(); itemIter.hasNext();)
				{
					Element elItem = (Element) itemIter.next();
					// wczytywanie nazwy wartości wyliczeniowej w róznych
					// językach
					Map<String, String> titles = XmlUtils.readLanguageValues(elItem, "title");
					// wczytywanie dodatkowych argumentów
					List<String> args = new ArrayList<String>(6);
					for (int i = 1; i <= 5; i++)
					{
						String attrName = "arg" + i;
						if (elItem.attributeValue(attrName) != null)
						{
							args.add(elItem.attributeValue(attrName));
						}
						else
						{
							break;
						}
					}
					enumField.addEnum(new Integer(elItems.attributeValue("discriminator")), new Integer(elItem.attributeValue("id")), elItem.attributeValue("cn"), titles, (String[]) args.toArray(new String[args.size()]), "true".equals(elItem.attributeValue("force-remark")), enumField.getCn());
				}
			}
		}
		field = enumField;
		return field;
	}

}
