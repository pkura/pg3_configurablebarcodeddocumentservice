package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.DSDivisionEnumField;
import pl.compan.docusafe.core.dockinds.field.UserDivisionField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class UserDivisionReader implements FieldReader {

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		UserDivisionField enumField = new UserDivisionField(elType.attributeValue("discrminator"), refStyle, el.attributeValue("id"), el.attributeValue("cn"), el.attributeValue("column"), names, required, requiredWhen, availableWhen, "true".equals(el.attributeValue("sort")),
				elType.attributeValue("ref-field"));

        enumField.initialize();

		return enumField;
	}

}
