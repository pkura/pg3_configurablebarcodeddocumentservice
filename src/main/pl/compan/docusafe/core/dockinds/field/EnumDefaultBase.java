package pl.compan.docusafe.core.dockinds.field;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.Aspect;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * Date: 01.08.13
 * Time: 14:56
 *
 */
public class EnumDefaultBase implements EnumBase {

    private Map<String, SortedSet<EnumItem>> refEnumItemsMap;
    private Set<EnumItem> availableItems;
    private Set<EnumItem> allItems;
    private Map<Integer, EnumItem> itemsReferencedForAll;

    /** Nazwa tabeli w bazie danych z kt�rej zaci�gane s� warto�ci do pola s */
    private String tableName;
    /** Pole oznacz�jce od kt�rego pola zale�y */
    private String refField;
    /** Pole oznacz�jce kt�re pole jest zale�ne */
    private String assoField;
    /** Pole enum typu autocomplete */
    private boolean auto;

    private boolean emptyOnStart;
    private boolean allowNotApplicableItem;
    private boolean reloadAtEachDwrGet;
    private boolean repoField;

    private static final int NOT_APPLICABLE_ITEM_ID = -1;
    private static final String NOT_APPLICABLE_ITEM_ID_STRING = Integer.toString(NOT_APPLICABLE_ITEM_ID);

    //w przypadku wybranej pozycji, kt�rej refValue=0, wymuszamy �adowanie wszystkich pozycji, dla kt�rych refValue=0;
    public static final String REF_ID_FOR_ALL_FIELD = "0";

    public static final String REPO_REF_FIELD_PREFIX = "REF_";
    StringManager sm =
            GlobalPreferences.loadPropertiesFile(XmlEnumColumnField.class.getPackage().getName(), null);
    protected static Logger log = LoggerFactory.getLogger(EnumDefaultBase.class);

    private Map<Integer, EnumItem> enumItems;
    private volatile Map<Integer, EnumItem> availableEnumItems = new LinkedHashMap<Integer, EnumItem>();
    private String Cn;
    private Field field;
    private List<EnumItem> enumItemList;

    /** po czym mo�na sortowa� - domy�lnie CN */
    public enum Sort
    {
        TITLE, CN, ID
    }

    private Sort sort = Sort.CN;

    public EnumDefaultBase(Field field) {
        this.field = field;
        Cn = field.getCn();

        if (field instanceof DataBaseEnumField) {
            DataBaseEnumField castedField = (DataBaseEnumField) field;
            emptyOnStart = castedField.isEmptyOnStart();
            allowNotApplicableItem = castedField.isAllowNotApplicableItem();
            repoField = castedField.isRepoField();
            reloadAtEachDwrGet = castedField.isReloadAtEachDwrGet();
        }
    }

    @Override
    public void addEnum(Integer id, String cn, Map<String, String> titles, String[] args, boolean forceRemark, String fieldCn, boolean available) {

        if (enumItems == null)
            enumItems = new LinkedHashMap<Integer, EnumItem>();
        if (availableEnumItems == null)
            availableEnumItems = new LinkedHashMap<Integer, EnumItem>();

        EnumItem ei = new EnumItemImpl(id, cn, titles, args, forceRemark, fieldCn, available);
        enumItems.put(id, ei);
        if (available)
            availableEnumItems.put(id, ei);
    }

    @Override
    public List<EnumItem> getAvailableEnumItems() {
        return availableEnumItems != null ? new ArrayList<EnumItem>(availableEnumItems.values()) : Collections.EMPTY_LIST;
    }

    public List<EnumItem> getEnumItemList() {
        return enumItemList = field.getEnumItems();
    }
    @Override
    public List<EnumItem> getEnumItems() {
        return enumItems != null ? new ArrayList<EnumItem>(enumItems.values()) : Collections.EMPTY_LIST;
    }

    @Override
    public List<EnumItem> getAllEnumItemsByAspect(String aspectCn, DocumentKind documentKind) throws EdmException {
        List<EnumItem> items = getEnumItems();
        if(aspectCn != null)
        {
            Aspect aspect = documentKind.getDockindInfo().getAspectsMap().get(aspectCn);
            items = aspect.getEnumItems() != null && aspect.getEnumItems().containsKey(Cn) ? aspect.getEnumItems().get(Cn) : getEnumItems();

        }
        return items;
    }

    @Override
    public List<EnumItem> getEnumItemsByAspect(String aspectCn, DocumentKind documentKind) throws EdmException {
        List<EnumItem> items = getAvailableEnumItems();
        if(aspectCn != null)
        {
            Aspect aspect = documentKind.getDockindInfo().getAspectsMap().get(aspectCn);
            items = aspect.getEnumItems() != null && aspect.getEnumItems().containsKey(Cn) ? aspect.getEnumItems().get(Cn) : getAvailableEnumItems();


        }
        return items;
    }

    @Override
    public List<EnumItem> getEnumItemsByAspect(String aspectCn, FieldsManager fm) throws EdmException {
        List<EnumItem> items = getAvailableEnumItems();

        if (aspectCn != null)
        {
            Aspect aspect = fm.getDocumentKind().getDockindInfo().getAspectsMap().get(aspectCn);
            items = aspect.getEnumItems() != null && aspect.getEnumItems().containsKey(Cn) ? aspect.getEnumItems().get(Cn) : getAvailableEnumItems();

            if (fm.getKey(Cn) != null)
            {
                Boolean contains = false;
                for (EnumItem item : items)
                {
                    if (item.getId().equals(fm.getKey(Cn)))
                    {
                        contains = true;
                        break;
                    }
                }
                if (!contains)
                {
                    items = new ArrayList<EnumItem>();
                    items.add(getEnumItem((Integer) fm.getKey(Cn)));
                    // setReadOnly(true);
                }
            }
        }

        return items;
    }

    @Override
    public void setEnumItems(Map<Integer, EnumItem> enumItems) {
        this.enumItems = enumItems;
    }

    @Override
    public void setEnumItems(List<EnumItem> enumItems) {
        this.enumItems = new LinkedHashMap<Integer, EnumItem>();
        this.availableEnumItems = new LinkedHashMap<Integer, EnumItem>();
        for (EnumItem enumItem : enumItems)
        {
            this.enumItems.put(enumItem.getId(), enumItem);
            if (enumItem.isAvailable())
            {
                this.availableEnumItems.put(enumItem.getId(), enumItem);
            }
        }
    }

    @Override
    public List<EnumItem> getSortedEnumItems() {
        List<EnumItem> list = getAvailableEnumItems();
        Collections.sort(list, new EnumItem.EnumItemComparator());
        return list;
    }

    @Override
    public EnumItem getEnumItem(Integer id) throws EntityNotFoundException {
        EnumItem item = enumItems != null ? (EnumItem) enumItems.get(id) : null;
        if (item == null)
            throw new EntityNotFoundException(sm.getString("NieZnalezionoWartosciWyliczeniowejOidentyfikatorze") + " = " + id + " " + sm.getString("DlaPola") + " " + Cn);
        return item;
    }

    @Override
    public EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException {
        if (enumItems != null)
        {
            for (EnumItem i : enumItems.values())
            {
                if (cn.equals(i.getCn()))
                    return i;
            }
        }
        throw new EntityNotFoundException(sm.getString("NieZnalezionoWartosciWyliczeniowejOcn") + " = " + cn + " " + sm.getString("DlaPola") + " " + Cn);
    }

    @Override
    public EnumItem getEnumItemByTitle(String title) throws EntityNotFoundException {
        if (enumItems != null)
        {
            for (EnumItem i : enumItems.values())
            {
                if (title.equals(i.getTitle()))
                    return i;
            }
        }
        throw new EntityNotFoundException(sm.getString("NieZnalezionoWartosciWyliczeniowejOtitle") + " = " + title + " " + sm.getString("DlaPola") + " " + Cn);
    }

    @Override
    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, String id, String temp) throws EntityNotFoundException {
    	DocumentKind kind = null;
    	Long documentId = null;
    	if(WebContextFactory.get() != null){
    		HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
    		if(req != null){
    			if(req.getSession().getAttribute("dwrSession") != null){
    				String docCn = ((Map<String, String>)req.getSession().getAttribute("dwrSession")).get("dockindKind");
    				documentId = ((Map<String, Long>)req.getSession().getAttribute("dwrSession")).get("documentId");
    				if(docCn != null){
    					try{
    						kind = DocumentKind.findByCn(docCn);
    					} catch(EdmException e){
    						log.error("", e);
    						kind = null;
    					}
    				}
    			}
    		}
    	}
    	setSort(Sort.valueOf(temp.toUpperCase()));
    	Map<String, String> enumValues = new LinkedHashMap<String, String>();
    	Set<String> enumSelectedSet = Sets.newHashSet();

    	if (isAllowNotApplicableItem() &&
    			fieldsValues != null && fieldsValues.get(Cn) != null && fieldsValues.get(Cn).toString().equals(NOT_APPLICABLE_ITEM_ID_STRING)) {

    		String notApplicableTitle = "nie dotyczy";
    		try {
    			EnumItem forcedItem = getEnumItem(NOT_APPLICABLE_ITEM_ID);
    			notApplicableTitle = forcedItem != null ? forcedItem.getTitle() : notApplicableTitle;
    		} catch (EntityNotFoundException e) {
    		}
    		HashMap<String, String> allOptions = new HashMap<String,String>() {{put("", sm.getString("select.wybierz"));}};
    		allOptions.put(NOT_APPLICABLE_ITEM_ID_STRING, notApplicableTitle);
    		ArrayList<String> selected = new ArrayList<String>();
    		selected.add(NOT_APPLICABLE_ITEM_ID_STRING);
    		return new EnumValues(allOptions, selected);
    	}

    	if (isEmptyOnStart() && (fieldsValues == null ||
    			(
    					Strings.isNullOrEmpty(id) && isFieldAndRefFieldNotSelected(fieldsValues)
    					)
    					||
    					(
    							!Strings.isNullOrEmpty(id) && isFieldNotSelected(fieldsValues)
    							)
    			)
    			) {
    		return new EnumValues(Collections.EMPTY_MAP, Collections.EMPTY_LIST);
    	}

    	if (getRefFieldWithRepo() != null)
    	{
    		if ("document_id_for_ref".equals(getRefField())) {
    			//dodanie docId na potrzeby ref-field
    			if (WebContextFactory.get() != null && WebContextFactory.get().getSession() != null && WebContextFactory.get().getSession().getAttribute(DwrFacade.DWR_SESSION_NAME) != null) {
    				try {
    					Map<String,Object> attributes = (Map<String, Object>) WebContextFactory.get().getSession().getAttribute(DwrFacade.DWR_SESSION_NAME);
    					String docId = attributes.get("documentId")!=null?attributes.get("documentId").toString():"";
    					if (StringUtils.isNotEmpty(docId)){
    						fieldsValues.put("document_id_for_ref", docId);
    					}
    				} catch (Exception e) {
    					log.error("B��d w odczytwaniu docId na potrzeby refValue");
    				}
    			}
    		}

    		if (!itemsReferencedForAll.isEmpty() && StringUtils.isNotEmpty(id) && isSelected(fieldsValues) && isRefFieldNotSelected(fieldsValues)) {
    			try {
    				if (itemsReferencedForAll.containsKey(Ints.tryParse(fieldsValues.get(Cn).toString()))) {
    					fieldsValues.put(getRefField(), REF_ID_FOR_ALL_FIELD);
    				}
    			} catch (NumberFormatException e) {
    				log.debug(e.getMessage(),e);
    			}
    		}

    		if (isRepoField() && isSelected(fieldsValues) && !fieldsValues.containsValue(getRefFieldWithRepo())) {
    			try {
    				EnumItem item = getEnumItem(Integer.valueOf(fieldsValues.get(field.getCn()).toString()));
    				if (item != null && item.getRefValue() != null) {
    					fieldsValues.put(REPO_REF_FIELD_PREFIX + Cn, item.getRefValue());
    				}
    			} catch (EntityNotFoundException e) {
    				log.debug(e.getMessage(),e);
    			}
    		}

    		if (fieldsValues.get(getRefFieldWithRepo()) != null && refEnumItemsMap.get(fieldsValues.get(getRefFieldWithRepo()).toString()) != null)
    		{
    			enumValues.put("", sm.getString("select.wybierz"));

    			if (StringUtils.isNotEmpty(id) && fieldsValues.containsKey(Cn)) {
    				enumSelectedSet.add(fieldsValues.get(Cn).toString());
    				enumValues.put(fieldsValues.get(Cn).toString(), getEnumItem(Integer.valueOf(fieldsValues.get(Cn).toString())).getTitle());
    			}

    			for (EnumItem item : refEnumItemsMap.get((fieldsValues.get(getRefFieldWithRepo()).toString())))
    			{
    				/** dodanie wybranej wartosci */
    				if (field.isMultiple() && fieldsValues.get(Cn) != null)
    				{
    					for (Object str : (Iterable) fieldsValues.get(Cn))
    					{
    						if (str instanceof Integer)
    							enumSelectedSet.add(((Integer) str).toString());
    						else if (str instanceof String)
    							enumSelectedSet.add((String) str);
    					}
    				}
    				else if (!field.isMultiple())
    				{
    					if (isSelected(fieldsValues))
    						enumSelectedSet.add(fieldsValues.get(Cn).toString());
    					else if (field.getDefaultValue() != null && field.getDefaultValue().equals(item.getCn()))
    					{
    						enumSelectedSet.clear();
    						enumSelectedSet.add(item.getId().toString());
    					}
    				}

    				if (enumSelectedSet.isEmpty())
    					enumSelectedSet.add("");

    				String title;
    				if (field.getRefStyle() != null && field.getRefStyle().equals(Field.STYLE_CN))
    					title = item.getCn() + " - " + item.getTitle();
    				else if (field.getRefStyle() != null && field.getRefStyle().equals(Field.STYLE_CN_COLON))
    					title = item.getCn() + ": " + item.getTitle();
    				else if (field.getRefStyle() != null && field.getRefStyle().equals(Field.STYLE_CN_ONLY))
    					title = item.getCn();
    				else
    					title = item.getTitle();

    				if (item.isAvailable() || enumSelectedSet.contains(item.getId().toString()))
    					enumValues.put(item.getId().toString(), title);	
    			}
    			if (refEnumItemsMap.containsKey("NULL") && AvailabilityManager.isAvailable("allowNullRefValues"))
    			{
    				for (EnumItem item : refEnumItemsMap.get("NULL"))
    				{
    					String title;
    					if (field.getRefStyle() != null && field.getRefStyle().equals(Field.STYLE_CN))
    						title = item.getCn() + " - " + item.getTitle();
    					else
    						title = item.getTitle();
    					enumValues.put(item.getId().toString(), title);

    					/** dodanie wybranej wartosci */
    					if (field.isMultiple() && fieldsValues.get(Cn) != null)
    					{
    						for (Object str : (Iterable) fieldsValues.get(Cn))
    						{
    							if (str instanceof Integer)
    								enumSelectedSet.add(((Integer) str).toString());
    							else if (str instanceof String)
    								enumSelectedSet.add((String) str);
    						}
    					}
    					else if (!field.isMultiple())
    					{
    						if (isSelected(fieldsValues))
    							enumSelectedSet.add(fieldsValues.get(Cn).toString());
    						else if (field.getDefaultValue() != null && field.getDefaultValue().equals(item.getCn()))
    						{
    							enumSelectedSet.clear();
    							enumSelectedSet.add(item.getId().toString());
    						}
    					}

    					if (enumSelectedSet.isEmpty())
    						enumSelectedSet.add("");
    				}
    			}
    			/* Ustawiamy wy�wietlane warto�ci*/
    			if(kind != null)
    				try{
    					if(kind.logic().modifyEnumValues(enumValues, Lists.newArrayList(enumSelectedSet), Cn, documentId) != null){
    						enumValues = kind.logic().modifyEnumValues(enumValues, Lists.newArrayList(enumSelectedSet), Cn, documentId);
    					}
    				}catch(EdmException e){
    					log.error("", e);
    				}
    			return new EnumValues(enumValues, Lists.newArrayList(enumSelectedSet));
    		}
    		else
    			/* Ustawiamy wy�wietlane warto�ci*/
    			if(kind != null)
    				try{
    					if(kind.logic().modifyEnumValues(enumValues, Lists.newArrayList(enumSelectedSet), Cn, documentId) != null){
    						enumValues = kind.logic().modifyEnumValues(enumValues, Lists.newArrayList(enumSelectedSet), Cn, documentId);
    					}
    				}catch(EdmException e){
    					log.error("", e);
    				}
    		return new EnumValues(enumValues, Lists.newArrayList(enumSelectedSet));
    	}

    	// pobranie wartosci enumFielda
    	List<EnumItem> sortedList = getEnumItemList();

    	if (getSort() != null && getSort().equals(Sort.ID))
    	{
    		Collections.sort(sortedList, new EnumItem.EnumItemComparatorByID());
    	}
    	else if (field.getRefStyle() != null && (field.getRefStyle().equals(Field.STYLE_CN) || field.getRefStyle().equals(Field.STYLE_CN_COLON) || field.getRefStyle().equals(Field.STYLE_CN_ONLY)))
    	{
    		Collections.sort(sortedList, new EnumItem.EnumItemCnComparator());
    	}
    	else if (getSort() != null && getSort().equals(Sort.CN))
    	{
    		Collections.sort(sortedList, new EnumItem.EnumItemComparatorByCn());
    	}
    	else
    	{
    		Collections.sort(sortedList, new EnumItem.EnumItemComparator());
    	}

    	for (EnumItem item : sortedList)
    	{
    		String title;
    		if (field.getRefStyle() != null && field.getRefStyle().equals(Field.STYLE_CN))
    			title = item.getCn() + " - " + item.getTitle();
    		else if (field.getRefStyle() != null && field.getRefStyle().equals(Field.STYLE_CN_COLON))
    			title = item.getCn() + ": " + item.getTitle();
    		else if (field.getRefStyle() != null && field.getRefStyle().equals(Field.STYLE_CN_ONLY))
    			title = item.getCn();
    		else
    			title = item.getTitle();

    		if (field.isMultiple() && fieldsValues.get(Cn) != null)
    		{
    			for (Object str : (Iterable) fieldsValues.get(Cn))
    			{
    				if (str instanceof Integer)
    					enumSelectedSet.add(((Integer) str).toString());
    				else if (str instanceof String)
    					enumSelectedSet.add((String) str);
    			}
    		}
    		else if (!field.isMultiple())
    		{
    			enumValues.put("", sm.getString("select.wybierz"));
    			if (isSelected(fieldsValues))
    			{
    				String selectedId = fieldsValues.get(Cn).toString();
    				if (!enumSelectedSet.contains(selectedId))
    					enumSelectedSet.add(selectedId);
    			}
    			else if (field.getDefaultValue() != null && field.getDefaultValue().equals(item.getCn()))
    			{
    				enumSelectedSet.clear();
    				enumSelectedSet.add(item.getId().toString());
    			}
    		}

    		if (enumSelectedSet.isEmpty())
    		{
    			enumSelectedSet.add("");
    		}
    		if (item.isAvailable() || enumSelectedSet.contains(item.getId().toString()))
    			enumValues.put(item.getId().toString(), title);
    	}
    	/* Ustawiamy wy�wietlane warto�ci*/
    	if(kind != null)
    		try{
    			if(kind.logic().modifyEnumValues(enumValues, Lists.newArrayList(enumSelectedSet), Cn, documentId) != null){
    				enumValues = kind.logic().modifyEnumValues(enumValues, Lists.newArrayList(enumSelectedSet), Cn, documentId);
    			}
    		}catch(EdmException e){
    			log.error("", e);
    		}
    	return new EnumValues(enumValues, Lists.newArrayList(enumSelectedSet));
    }

    private boolean isFieldAndRefFieldNotSelected(Map<String, Object> fieldsValues) {
        return (fieldsValues.get(Cn) == null || Strings.isNullOrEmpty(fieldsValues.get(Cn).toString()))
                && (fieldsValues.get(getRefFieldWithRepo()) == null || Strings.isNullOrEmpty(fieldsValues.get(getRefFieldWithRepo()).toString()));
    }

    private boolean isRefFieldNotSelected(Map<String, Object> fieldsValues) {
        return fieldsValues.get(getRefFieldWithRepo()) == null
                || Strings.isNullOrEmpty(fieldsValues.get(getRefFieldWithRepo()).toString());
    }

    private boolean isFieldNotSelected(Map<String, Object> fieldsValues) {
        return fieldsValues.get(Cn) == null
                || Strings.isNullOrEmpty(fieldsValues.get(Cn).toString());
    }

    private boolean isSelected(Map<String, Object> fieldsValues) {
        return fieldsValues != null && fieldsValues.get(Cn) != null;
    }

    public String getTableName()
    {
        return tableName;
    }

    public String getRefField() throws EntityNotFoundException {
        return field.getRefField();
    }

    public String getRefFieldWithRepo() {
        String ref = null;

        try {
            ref = getRefField();
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        return isRepoField()?REPO_REF_FIELD_PREFIX+Cn:ref;
    }

    public String getAssoField()
    {
        return assoField;
    }

    public void setAssoField(String assoField)
    {
        this.assoField = assoField;
    }

    public boolean isEmptyOnStart() {
        return emptyOnStart;
    }

    public void setEmptyOnStart(boolean emptyOnStart) {
        this.emptyOnStart = emptyOnStart;
    }

    public boolean isAllowNotApplicableItem() {
        return allowNotApplicableItem;
    }

    public void setAllowNotApplicableItem(boolean allowNotApplicableItem) {
        this.allowNotApplicableItem = allowNotApplicableItem;
    }

    public boolean isReloadAtEachDwrGet() {
        return reloadAtEachDwrGet;
    }

    public void setReloadAtEachDwrGet(boolean reloadAtEachDwrGet) {
        this.reloadAtEachDwrGet = reloadAtEachDwrGet;
    }

    public boolean isRepoField() {
        return repoField;
    }

    public void setRepoField(boolean repoField) {
        this.repoField = repoField;
    }

    @Override
    public void setItemsReferencedForAll(Map<Integer, EnumItem> itemsReferencedForAll) {
        this.itemsReferencedForAll = itemsReferencedForAll;
    }

    @Override
    public void setRefEnumItemsMap(Map<String, SortedSet<EnumItem>> refEnumItemsMap) {
        this.refEnumItemsMap = refEnumItemsMap;
    }

    @Override
    public void setAvailableItems(Set<EnumItem> availableItems) {
        this.availableItems = availableItems;
    }

    @Override
    public void setAllItems(Set<EnumItem> allItems) {
        this.allItems = allItems;
    }

    public Sort getSort()
    {
        return sort;
    }

    public void setSort(Sort sort)
    {
        this.sort = sort;
    }

    @Override
    public Set<EnumItem> getEnumItemsByRef(String id) {
        Preconditions.checkState(refEnumItemsMap != null, "RefEnumMaps wasnt initialized");
        return refEnumItemsMap.get(id);
    }
}
