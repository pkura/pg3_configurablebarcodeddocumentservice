package pl.compan.docusafe.core.dockinds.field;

import static pl.compan.docusafe.util.I18nUtils.string;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.office.AssignmentObjective;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DocumentOfficeCasePriorityField  extends NonColumnField implements CustomPersistence, EnumNonColumnField, SearchPredicateProvider
{

	private static final Logger log = LoggerFactory.getLogger(DocumentOfficeCasePriorityField.class);
	private String type = "in";
	private List<EnumItem> enumItems = new ArrayList<EnumItem>();
	
	public DocumentOfficeCasePriorityField(String id, String type, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException
	{
		super(id, cn, names, Field.DOCUMENT_OFFICE_CASE_PRIORITY, null, null, required, requiredWhen, availableWhen);
		if (type != null)
		{
			this.type = type;
		}

		Map<String,String> titles = new HashMap<String,String>();
		for (AssignmentObjective kind : AssignmentObjective.list())
		{
			titles = new HashMap<String,String>();
			titles.put("pl", kind.getName());
			enumItems.add(new EnumItemImpl(kind.getId(), "CN_"+kind.getId(), titles, new String[3], false,this.getCn()));
		}
	}

	@Override
	public void persist(Document doc, Object key) throws EdmException
	{
		Integer kindId = null;
        	if (key instanceof String)
        		kindId = Integer.valueOf((String) key);
        	else
        		kindId = (Integer) key;
        	if (kindId != null)
            {
        	if (doc instanceof InOfficeDocument) 
        	    ((InOfficeDocument) doc).setKind(InOfficeDocumentKind.find(kindId));
        	else
        		throw new EdmException("RodzajDokumentuPrzychodzacegoMoznaDodacTylkoWpismiePrzychodzacym");
            }
	}

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		OfficeDocument doc = OfficeDocument.find(documentId);

		if (doc instanceof InOfficeDocument && ((InOfficeDocument)doc).getKind() != null )
			return new FieldValue(this, ((InOfficeDocument)doc).getKind().getId().toString(), ((InOfficeDocument)doc).getKind().getName());
		else
			return new FieldValue(this, null, null);

	}

	@Override
	public Object simpleCoerce(Object value) throws FieldValueCoerceException
	{
		if (value == null)
			return null;
		if (value instanceof InOfficeDocumentKind)
			return ((InOfficeDocumentKind) value).getId();
		try
		{
			return Integer.parseInt(value.toString());
		}
		catch (NumberFormatException e)
		{
			throw new FieldValueCoerceException(string("NieMoznaSkonwertowacWartosci") + " " + value + " " + string("DoLiczbyCalkowitej"));
		}
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException
	{
		return new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
	}
	
	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EdmException
	{
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("", "--wybierz--");

		if (fieldsValues != null && fieldsValues.get(getCn()) != null)
			enumSelected.add(fieldsValues.get(getCn()).toString());
		else
			enumSelected.add("");

		for (InOfficeDocumentKind kind : InOfficeDocumentKind.list())
			enumValues.put(kind.getId().toString(), kind.getName()+" (Dni: " +kind.getDays()+")");
		
		return new EnumValues(enumValues, enumSelected);
	}

	public final List<EnumItem>  getAllEnumItemsByAspect(String documentAspectCn,DocumentKind kind)
	{
		
		return this.enumItems;
	}

	
	public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) 
	{
		String  valS = (String) values.get(this.getCn());
		Integer valI = valS != null && valS.length() > 0 ? Integer.parseInt(valS) : null;
		log.debug(" {} {} ",valS,valI);
		dockindQuery.inOfficeKind(valI);	
	}
	
}
