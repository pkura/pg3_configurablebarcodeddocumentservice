package pl.compan.docusafe.core.dockinds.field;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.fileupload.FileItem;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrAttachmentStorage;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Domyślna implementacja zapisu wielu załączników do dokumentu
 */
public class DocumentMultipleAttachmentFieldLogic implements AttachmentFieldLogic {
    protected List<Attachment> savedAtts = null;

	@Override
	public void persist(Document doc, Field field, List<FileItem> fileItems) throws EdmException, IOException {
		doc.setPermissionsOn(false);
        savedAtts = new ArrayList<Attachment>();

		for(FileItem fileItem : fileItems) {
            if(fileItem == null)
                continue;
			Attachment att = new Attachment(fileItem.getName());
			att.setWparam(doc.getId());
			doc.createAttachment(att);
			att.createRevision(fileItem.getInputStream(), Long.valueOf(fileItem.getSize()).intValue(), fileItem.getName());
            savedAtts.add(att);
        }
        javax.servlet.http.HttpServletRequest request = ServletActionContext.getRequest();
        try {
            if(request != null && request.getSession(false) != null){
                // znajduje requesta tylko przy przesylaniu wiadomosci dalej
                DwrAttachmentStorage.getInstance().removeAll(request.getSession(false), field.getCn());
            }else{
                // przy odpowiadaniu na maila ServletActionContext.getRequest() zwraca nulla
                DwrAttachmentStorage.getInstance().removeAll(WebContextFactory.get().getSession(false), field.getCn());
            }
        } catch(NullPointerException e) {
            LoggerFactory.getLogger(DwrAttachmentStorage.class).error(e.getMessage(), e);
        }
	}

    @Override
    public Attachment provideFieldValue(Field field, FieldValue fieldValue, long documentId, ResultSet rs) throws EdmException {
        Document doc = Document.find(documentId);
        Attachment att = doc.getAttachmentByFieldCn(field.getCn());
        if (att != null) {
            AttachmentRevision revision = att.getMostRecentRevision();
            fieldValue.setKey(revision.getId());
            fieldValue.setValue(att);
        }

        return att;
    }
}
