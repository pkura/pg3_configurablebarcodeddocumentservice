package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface CustomPersistence {

    public void persist(Document doc, Object key) throws EdmException;
}
