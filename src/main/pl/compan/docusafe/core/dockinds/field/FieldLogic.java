package pl.compan.docusafe.core.dockinds.field;

public interface FieldLogic {
	
	
	/**
	 * Metoda s�u�y wygenerowaniu unikatowego komunikatu dla danego pola w przypadku zmodyfikowania jego warto�ci
	 * 
	 * @param oldDesc aktualny opis modyfikowanego pola 
	 * @param oldKey  aktualna warto�� modyfikowanego pola
	 * @param newDesc nowy opis dla modyfikowanego pola 
	 * @param newKey nowa warto�� dla modyfikowanego pola
	 * @return <code>null</code> je�li pole nie uleg�o zmianie
	 */
	public abstract String prepareCustomFieldHistoryEntry(Object oldDesc, Object oldKey, Object newDesc, Object newKey);
	
	/**
	 * Metoda ustawia referencje do pola powi�zanego z dan� logik�
	 */
	public abstract void setField(Field field);
	
	public abstract Field getField();
	
}


