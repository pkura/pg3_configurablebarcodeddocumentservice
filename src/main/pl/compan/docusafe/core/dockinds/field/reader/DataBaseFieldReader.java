package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField.Sort;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;

public class DataBaseFieldReader implements FieldReader
{

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		DataBaseEnumField field;
		boolean auto = (elType.attributeValue("auto") != null) && Boolean.parseBoolean(elType.attributeValue("auto"));

		field = new DataBaseEnumField(el.attributeValue("id"), el.attributeValue("cn"), el.attributeValue("column"), names, Field.DATA_BASE, refStyle, required, requiredWhen, availableWhen, elType.attributeValue("tableName"), elType.attributeValue("ref-field"), elType.attributeValue("asso-field"),
				elType.attributeValue("correction"), auto, "true".equals(elType.attributeValue("empty-on-start")), "true".equals(elType.attributeValue("allow-not-applicable-item")), elType.attributeValue("reload-at-each-dwr-get")==null ? true : "true".equals(elType.attributeValue("reload-at-each-dwr-get")), "true".equals(elType.attributeValue("repository-field")));
        field.setDockindCn(di.getCn());
		String sort = elType.attributeValue("sort");
		if (sort != null)
		{
			field.setSort(Sort.valueOf(sort.toUpperCase()));
		}

		field.initialize();
		return field;
	}

}
