package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.dockinds.DockindQuery;

import java.util.Map;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface SearchPredicateProvider {
    void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues);
}
