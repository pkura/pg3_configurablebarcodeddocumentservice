package pl.compan.docusafe.core.dockinds.field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
class TitleFormatter implements FieldFormatter<EnumItem> {
    final static Logger LOG =  LoggerFactory.getLogger(FieldFormatter.class);

    public void format(EnumItem fieldValue, Appendable app) throws IOException {
         app.append(fieldValue.getTitle());
    }
}
