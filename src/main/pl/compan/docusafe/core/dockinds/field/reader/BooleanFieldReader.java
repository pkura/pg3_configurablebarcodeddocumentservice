package pl.compan.docusafe.core.dockinds.field.reader;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.BooleanColumnField;
import pl.compan.docusafe.core.dockinds.field.BooleanNonColumnField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.core.dockinds.field.NonColumnField;

public class BooleanFieldReader implements FieldReader {

	@Override
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen,
			List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {
		String column = el.attributeValue("column");

		if (column == null) {
			NonColumnField field = new BooleanNonColumnField(el.attributeValue("id"), el.attributeValue("cn"), names, required, requiredWhen, availableWhen);
			
			boolean reset = Boolean.parseBoolean(elType.attributeValue("reset"));
			field.setResettable(reset);
			
			return field;
		} else {
			return new BooleanColumnField(el.attributeValue("id"), el.attributeValue("cn"), column, names, required, requiredWhen, availableWhen);
		}
	}
}
