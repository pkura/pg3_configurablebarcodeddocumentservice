package pl.compan.docusafe.core.dockinds.field.reader;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.core.dockinds.field.LongColumnField;
import pl.compan.docusafe.core.dockinds.field.LongNonColumnField;

import java.util.List;
import java.util.Map;

public class LongFieldReader implements FieldReader {

    @Override
    public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException {
        String column = el.attributeValue("column");

        if (column == null) {
            return new LongNonColumnField((el.attributeValue("id")), el.attributeValue("cn"), names, required, requiredWhen, availableWhen);
        } else {
            return new LongColumnField((el.attributeValue("id")), el.attributeValue("cn"), el.attributeValue("column"), names, required, requiredWhen, availableWhen);
        }

    }
}
