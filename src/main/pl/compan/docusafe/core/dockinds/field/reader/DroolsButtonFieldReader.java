package pl.compan.docusafe.core.dockinds.field.reader;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.ResourceType;
import org.drools.definition.KnowledgePackage;
import org.drools.io.ResourceFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.field.DroolsButtonField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.Condition;
import pl.compan.docusafe.core.drools.DroolsUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DroolsButtonFieldReader implements FieldReader
{

	private static final Logger log = LoggerFactory.getLogger(DroolsButtonFieldReader.class);

	/**
	 * Tworzy DroolsButtonField z xml'a
	 * 
	 * @param el
	 *             - Element "field"
	 * @param elType
	 *             - Element "type" (zwykle pod elementem "field")
	 * @param availableWhen
	 *             - lista warunk�w, kiedy dany przycisk jest widoczny
	 * @param names
	 *             - nazwy
	 * @return nowa instancja przycisku, wraz z zapisan� regu�� biznesow�
	 * @throws EdmException
	 */
	public Field read(DockindInfoBean di, Element el, Element elType, String refStyle, boolean required, List<Condition> availableWhen, List<Condition> requiredWhen, Map<String, String> names, boolean disabled) throws EdmException
	{
		String cn = el.attributeValue("cn");
		DroolsButtonField ret = new DroolsButtonField(el.attributeValue("id"), cn, names, availableWhen);
		KnowledgeBuilder builder = DroolsUtils.createKnowledgeBuilder();
		readSources(elType, builder);

		if (builder.hasErrors())
		{
			DroolsUtils.reportErrors(builder);
			throw new RuntimeException("B��d podczas kompilacji regu� biznesowych");
		}
		Collection<KnowledgePackage> packages = builder.getKnowledgePackages();
		log.info("drools compilation successful " + packages.size() + " packages");
		ret.getKbase().addKnowledgePackages(packages);
		log.info("returning DroolsButtonField");
		return ret;
	}

	private static void readSources(Element elType, KnowledgeBuilder builder)
	{
		for (Element source : (List<Element>) elType.elements("source"))
		{
			log.info("DROOLS SOURCE : + \n" + source.getText());
			try
			{
				builder.add(ResourceFactory.newByteArrayResource(source.getText().getBytes("UTF-8")), ResourceType.DRL);
			}
			catch (UnsupportedEncodingException e)
			{
				log.error("to nigdy nie powinno si� pojawi� : " + e.getLocalizedMessage(), e);
				throw new RuntimeException(e);
			}
		}
	}

}
