package pl.compan.docusafe.core.dockinds.field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.util.TextUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Date: 10.07.13
 * Time: 11:55
 * Gosia
 */
public class XmlEnumNonColumnField extends AbstractEnumNonColumnField implements EnumNonColumnField,SearchPredicateProvider{

    private static final Logger log = LoggerFactory.getLogger(XmlEnumNonColumnField.class);
    public static final String REPO_REF_FIELD_PREFIX = "REF_";

    /** Nazwa tabeli w bazie danych z kt�rej zaci�gane s� warto�ci do pola s */
    private String tableName;
    /** Pole oznacz�jce od kt�rego pola zale�y */
    private String refField;
    /** Pole oznacz�jce kt�re pole jest zale�ne */
    private String assoField;
    /** Pole enum typu autocomplete */
    private boolean auto;

    private boolean emptyOnStart;
    private boolean allowNotApplicableItem;
    private boolean reloadAtEachDwrGet;
    private boolean repoField;


    private Map<String, SortedSet<EnumItem>> refEnumItemsMap;
    private Set<EnumItem> availableItems;
    private Set<EnumItem> allItems;
    private Map<Integer, EnumItem> itemsReferencedForAll;

    private static final int NOT_APPLICABLE_ITEM_ID = -1;
    private static final String NOT_APPLICABLE_ITEM_ID_STRING = Integer.toString(NOT_APPLICABLE_ITEM_ID);

    //w przypadku wybranej pozycji, kt�rej refValue=0, wymuszamy �adowanie wszystkich pozycji, dla kt�rych refValue=0;
    public static final String REF_ID_FOR_ALL_FIELD = "0";

    /** po czym mo�na sortowa� - domy�lnie CN */
    public enum Sort
    {
        TITLE, CN, ID
    }

    private Sort sort = Sort.CN;

    public XmlEnumNonColumnField(String refStyle, String id, String cn, Map<String, String> names,
                                 boolean required, List<Field.Condition> requiredWhen, List<Field.Condition> availableWhen, boolean sort) throws EdmException
    {
        super(id, cn, names, ENUM, refStyle, null, required, requiredWhen, availableWhen);
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        return new FieldValue(this, null, null);
    }

    @Override
    public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) {
        if (values.get(getCn()) == null){
            return;
        }
        Integer[] enumIds = TextUtils.parseIntoIntegerArray(values.get(getCn()));
        if(!isMultiple()){
            dockindQuery.enumField(this, enumIds);
        } else {
            dockindQuery.InField(this,enumIds);
        }
        fieldValues.put(getCn(),enumIds);
    }

    @Override
    public Object asObject(Object key) throws IllegalArgumentException, EdmException {
        if(key == null){
            return null;
        } else {
            return getEnumItem((Integer)key);
        }
    }

    public List<EnumItem> getSortedEnumItems(boolean onlyAvailable)
    {
        List<EnumItem> list = getEnumItems(onlyAvailable);
        Collections.sort(list, getComparator());
        return list;
    }

    public List<EnumItem> getSortedEnumItems()
    {
        return getSortedEnumItems(false);
    }

    @Override
    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, String id, String temp) throws EntityNotFoundException {
        return getEnumItemsForDwr(fieldsValues, id, sort);
    }

    public String getRefField()
    {
        return refField;
    }

    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EntityNotFoundException {
        return getEnumItemsForDwr(fieldsValues, null, sort);
    }

    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, String id, Sort sort) throws EntityNotFoundException {
        String temp = sort.toString();
        return enumDefault.getEnumItemsForDwr(fieldsValues, id, temp);
    }

    @Override
    public void setRefEnumItemsMap(Map<String, SortedSet<EnumItem>> refEnumItemsMap) {
        enumDefault.setRefEnumItemsMap(refEnumItemsMap);
    }

    @Override
    public void setAvailableItems(Set<EnumItem> availableItems) {
        enumDefault.setAvailableItems(availableItems);
    }

    @Override
    public void setAllItems(Set<EnumItem> allItems) {
        enumDefault.setAllItems(allItems);
    }

    @Override
    public void setEnumItems(Map<Integer, EnumItem> enumItems) {
        enumDefault.setEnumItems(enumItems);
    }

    public void setRepoField(boolean repoField) {
        this.repoField = repoField;
    }

    @Override
    public void setItemsReferencedForAll(Map<Integer, EnumItem> itemsReferencedForAll) {
        enumDefault.setItemsReferencedForAll(itemsReferencedForAll);
    }

    private Comparator<EnumItem> getComparator()
    {
        switch (getSort())
        {
            case CN:
                return EnumItem.COMPARATOR_BY_CN;
            case TITLE:
                return EnumItem.COMPARATOR_BY_TITLE;
            case ID:
                return EnumItem.COMPARATOR_BY_ID;
            default:
                throw new IllegalStateException("Nieobs�ugiwany Sort " + getSort());
        }
    }

    public Sort getSort()
    {
        return sort;
    }

    public void setSort(Sort sort)
    {
        this.sort = sort;
    }

}
