package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DocumentKind;

public class DocListField extends NonColumnField
{
	private String dockind;
	private String discriminator;
	private boolean groupingByEnum;
	private boolean showAttachment;
	private List<String> editFields;
	private List<String> groupingBys;
	private Map<String,String> relatedFields;
	private List<EnumItem> brakeEnumItems;
	private Map<String,String> newDocumentKinds;
	private String alertThisField;
	private String alertFromField;
	private String alertOperator;

	public DocListField(String id, String cn,Map<String, String> names, String type,Integer length, boolean required,
			List<Condition> requiredWhen, List<Condition> availableWhen)throws EdmException
	{
		super(id, cn, names, DOCLIST, null, null, required,
				requiredWhen, availableWhen);
	}

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        return new FieldValue(this, null);
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        return value;
    }

    public String getDockind() {
		return dockind;
	}

    public void setDockind(String dockind) {
		this.dockind = dockind;
	}
	/**Zwraca field grupujacy list�*/
	public Field getDiscriminatorField()
	{
		return DocumentKind.getDockindInfo(dockind).getDockindFieldsMap().get(discriminator);
	}

	public String getDiscriminator() {
		return discriminator;
	}

	public void setDiscriminator(String discriminator) {
		this.discriminator = discriminator;
	}

	public boolean isGroupingByEnum() {
		return groupingByEnum;
	}

	public void setGroupingByEnum(boolean groupingByEnum) {
		this.groupingByEnum = groupingByEnum;
	}

	public boolean isShowAttachment() {
		return showAttachment;
	}

	public void setShowAttachment(boolean showAttachment) {
		this.showAttachment = showAttachment;
	}

	public List<String> getEditFields() {
		return editFields;
	}

	public void setEditFields(List<String> editFields) {
		this.editFields = editFields;
	}

	public Map<String,String> getRelatedFields() {
		return relatedFields;
	}

	public void setRelatedFields(Map<String,String> relatedFields) {
		this.relatedFields = relatedFields;
	}

	public void setBrakeEnumItems(List<EnumItem> brakeEnumItems) {
		this.brakeEnumItems = brakeEnumItems;
	}

	public List<EnumItem> getBrakeEnumItems() {
		return brakeEnumItems;
	}

	public void setNewDocumentKinds(Map<String,String> newDocumentKinds) {
		this.newDocumentKinds = newDocumentKinds;
	}

	public Map<String,String> getNewDocumentKinds() {
		return newDocumentKinds;
	}

	public void setGroupingBys(List<String> groupingBys) {
		this.groupingBys = groupingBys;
	}

	public List<String> getGroupingBys() {
		return groupingBys;
	}

	public String getAlertThisField() {
		return alertThisField;
	}

	public void setAlertThisField(String alertThisField) {
		this.alertThisField = alertThisField;
	}

	public String getAlertFromField() {
		return alertFromField;
	}

	public void setAlertFromField(String alertFromField) {
		this.alertFromField = alertFromField;
	}

	public String getAlertOperator() {
		return alertOperator;
	}

	public void setAlertOperator(String alertOperator) {
		this.alertOperator = alertOperator;
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
			throws EdmException {
		// TODO Auto-generated method stub
		return null;
	}

}
