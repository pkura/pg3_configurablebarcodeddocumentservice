package pl.compan.docusafe.core.dockinds.field;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.ButtonValue;

public class ButtonField extends NonColumnField {
	private String label;
	private String value;
	private boolean clicked = false;
	protected CommonBooleanLogic item;

	public ButtonField(String id, String logic, String cn, Map<String, String> names, boolean required, List<Condition> requiredWhen,
			List<Condition> availableWhen, String value, String label) throws EdmException {
		super(id, cn, names, Field.BUTTON, null, null, required, requiredWhen, availableWhen);
		this.label = label;
		this.value = value;
		this.setDefaultValue(value);
		item = new CommonBooleanLogic(this);
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() throws EdmException {
		pl.compan.docusafe.core.dockinds.dwr.Field dwrField = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BUTTON);
		dwrField.setSubmitForm(getSubmitForm());
		dwrField.setListenersList(getListenersList());
        dwrField.setValidate(isValidate());
		return dwrField;
	}

	public ButtonValue getButtonForDWR() {
		return new ButtonValue(label, value, clicked);
	}

	public Boolean simpleCoerce(Object value) throws FieldValueCoerceException {
		if(value instanceof ButtonValue) {
			ButtonValue buttonValue = (ButtonValue) value;
			return buttonValue.isClicked();
		} else {
			return item.simpleCoerce(value);
		}
	}

	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
		return item.provideNonColumnFieldValue(documentId, rs);
	}

	public Boolean reset() {
		return item.reset();
	}
}
