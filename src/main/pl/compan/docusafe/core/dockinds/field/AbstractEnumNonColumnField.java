package pl.compan.docusafe.core.dockinds.field;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.util.StringManager;

import java.util.*;

/**
 * Date: 10.07.13
 * Time: 11:58
 * Gosia
 */
public abstract class AbstractEnumNonColumnField extends NonColumnField implements EnumBase,EnumerationField {

    StringManager sm = GlobalPreferences.loadPropertiesFile(XmlEnumNonColumnField.class.getPackage().getName(), null);

    protected EnumBase enumDefault;


    public AbstractEnumNonColumnField(String id, String cn, Map<String, String> names, String type, String refStyle, String classname, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen) throws EdmException {
        super(id, cn, names, type, refStyle, classname, required, requiredWhen, availableWhen);
        enumDefault = new EnumDefaultBase(this);
    }

    public abstract Object asObject(Object key) throws IllegalArgumentException, EdmException;

    public final void addEnum(Integer id, String cn, Map<String, String> titles, String[] args, boolean forceRemark, String fieldCn, boolean available)
    {
       enumDefault.addEnum(id, cn, titles, args, forceRemark, fieldCn, available);
    }

    /**
     * Lista dost�pnych do wyboru rekord�w
     *
     * @return
     */
    public List<EnumItem> getAvailableEnumItems()
    {
        return enumDefault.getAvailableEnumItems();
    }

    public List<EnumItem> getEnumItems()
    {
       return enumDefault.getEnumItems();
    }

    public final List<EnumItem> getAllEnumItemsByAspect(String aspectCn, DocumentKind documentKind) throws EdmException
    {
        return enumDefault.getAllEnumItemsByAspect(aspectCn, documentKind);
    }

    public final List<EnumItem> getEnumItemsByAspect(String aspectCn, DocumentKind documentKind) throws EdmException
    {
        return enumDefault.getAllEnumItemsByAspect(aspectCn, documentKind);
    }

    public final List<EnumItem> getEnumItemsByAspect(String aspectCn,  FieldsManager fm) throws EdmException
    {
        return enumDefault.getEnumItemsByAspect(aspectCn, fm);
    }

    public void setEnumItems(List<EnumItem> enumItems)
    {
       enumDefault.setEnumItems(enumItems);
    }

    public List<EnumItem> getSortedEnumItems()
    {
       return enumDefault.getSortedEnumItems();
    }

    public EnumItem getEnumItem(Integer id) throws EntityNotFoundException
    {
       return enumDefault.getEnumItem(id);
    }

    public EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException
    {
        return enumDefault.getEnumItemByCn(cn);
    }

    public EnumItem getEnumItemByTitle(String title) throws EntityNotFoundException
    {
        return enumDefault.getEnumItemByTitle(title);
    }

    @Override
    public Set<EnumItem> getEnumItemsByRef(String id) {
        return enumDefault.getEnumItemsByRef(id);
    }

    public Integer simpleCoerce(Object value) throws FieldValueCoerceException
    {
        if (value instanceof EnumItem)
        {
            return ((EnumItem) value).getId();
        }
        else
        {
            return FieldUtils.intCoerce(this, value);
        }
    }

    public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField()
    {
        pl.compan.docusafe.core.dockinds.dwr.Field field;

        if (isMultiple())
        {
            field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM_MULTIPLE);
        } else {
            field = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_" + this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM);
        }
        if (isAuto())
            field.setAutocomplete(true);
        field.setAutocompleteRegExp(getAutocompleteRegExp());

        return field;
    }

}
