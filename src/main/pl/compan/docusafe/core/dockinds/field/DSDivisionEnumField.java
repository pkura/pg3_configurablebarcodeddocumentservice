package pl.compan.docusafe.core.dockinds.field;

import com.google.common.base.Function;
import com.google.common.collect.*;
import com.google.common.primitives.Ints;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class DSDivisionEnumField extends AbstractEnumColumnField
{
	private final String disc;
	private final String refField;

	StringManager sm = GlobalPreferences.loadPropertiesFile(DSDivisionEnumField.class.getPackage().getName(), null);
	protected static Logger log = LoggerFactory.getLogger(DSDivisionEnumField.class);

	private static final Multimap<String, DSDivisionEnumField> divisionToField = Multimaps.synchronizedListMultimap(ArrayListMultimap.<String, DSDivisionEnumField> create());

	public DSDivisionEnumField(String disc, String refStyle, String id, String cn, String column, Map<String, String> names, boolean required, List<Condition> requiredWhen, List<Condition> availableWhen, boolean sort, String refField) throws EdmException
	{
		super(id, cn, column, names, DSDIVISION, refStyle, null, required, requiredWhen, availableWhen);
		divisionToField.put(cn, this);
		this.disc = disc;
		this.refField = refField;
		// super(refStyle,id,cn,column,names,required,requiredWhen,availableWhen,false);
	}

	@Override
	public void set(PreparedStatement ps, int index, Object value) throws FieldValueCoerceException, SQLException
	{
		if (value == null)
		{
			ps.setObject(index, null);
		}
		else
		{
			ps.setInt(index, simpleCoerce(value));
		}
	}

	public static void reloadForAll() throws EdmException
	{
		for (DSDivisionEnumField field : divisionToField.values())
		{
			field.loadEnumItem();
		}
	}

    public void initialize() throws EdmException {
        loadEnumItem();
    }

    /**
	 * Uaktualnia liste uzytkownikow do wyswietlenia
	 * 
	 * @throws EdmException
	 */
    private synchronized void loadEnumItem() throws EdmException {
        Map<String, SortedSet<EnumItem>> refEnumItemsMap = new LinkedHashMap<String, SortedSet<EnumItem>>();
        //LinkedHashMap<Integer, EnumItem> enumItems = new LinkedHashMap<Integer, EnumItem>();
        SortedSet<EnumItem> availableEnumItems = new TreeSet<EnumItem>(EnumItem.COMPARATOR_BY_TITLE);
        SortedSet<EnumItem> enumItems = new TreeSet<EnumItem>(EnumItem.COMPARATOR_BY_TITLE);

        for (DSDivision division : DSDivision.getAllDivisions()) {
            if (division.isDivision()) {
                if (division.getId() > (long) Integer.MAX_VALUE) {
                    throw new RuntimeException("utrata dok�adno�ci - niemo�liwe stworzenie pola");
                }

                boolean available = !division.isHidden();
                EnumItem ei = new EnumItemImpl(division.getId().intValue(), division.getGuid(), ImmutableMap.of("pl", division.getName()), new String[] {division.getCode()}, false, getCn(), available);
                enumItems.add(ei);
                if (available) {
                    availableEnumItems.add(ei);
                }

                if (getRefField() != null) {
                    DSUser[] users = division.getUsersWithDeleted(true);
                    for (DSUser user : users) {
                        if (!refEnumItemsMap.containsKey(user.getId().toString())) {
                            refEnumItemsMap.put(user.getId().toString(), new TreeSet<EnumItem>(EnumItem.COMPARATOR_BY_TITLE));
                        }
                        refEnumItemsMap.get(user.getId().toString()).add(ei);
                    }
                }
            }
        }

        ImmutableMap<Integer, EnumItem> enumItemsMap = Maps.uniqueIndex(enumItems, new Function<EnumItem, Integer>() {
            @Override
            public Integer apply(EnumItem input) {
                return input.getId();
            }
        });

        setEnumItems(enumItemsMap);
        setAvailableItems(availableEnumItems);
        setRefEnumItemsMap(refEnumItemsMap);
    }

	@Override
	public Object asObject(Object key) throws IllegalArgumentException, EdmException
	{
        if (key == null) {
            return null;
        } else {
            return getEnumItem((Integer) key);
        }
    }

	@Override
	public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException
	{
		int i = rs.getInt(getColumn());
		Integer value = rs.wasNull() ? null : new Integer(i);
		return new FieldValue(this, value);
	}

	public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues) throws EntityNotFoundException
	{
		Map<String, String> enumValues = new LinkedHashMap<String, String>();
		List<String> enumSelected = new ArrayList<String>();
		enumValues.put("", "--wybierz--");

        Integer selectedId = null;
        if (fieldsValues != null && fieldsValues.get(getCn()) != null) {
            selectedId = Ints.tryParse(fieldsValues.get(getCn()).toString());
            enumSelected.add(fieldsValues.get(getCn()).toString());
        } else {
            enumSelected.add("");
        }

        DSDivision[] onlyDivisions = new DSDivision[0];
        try {
            onlyDivisions = DSDivision.getOnlyDivisions(true);
        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        }

        if (refField == null) {
            for (EnumItem item :getEnumItems())
            {
                if (this.disc != null)
                {
                    List<DSDivision> divs = null;
                    try
                    {
                        if (this.disc.equals("user"))
                            divs = Arrays.asList(DSApi.context().getDSUser().getDivisionsWithoutGroup());
                        else
                        {
                            divs = new ArrayList<DSDivision>();
                            for (DSDivision div : onlyDivisions)
                            {
                                String divDescription = ((DivisionImpl) div).getDescription();
                                if ((!div.isHidden() || enumSelected.contains(div.getId().toString()))
                                        && (divDescription != null && divDescription.equals(this.disc)))
                                    divs.add(div);
                            }
                        }
                        if (Integer.parseInt(enumSelected.get(0)) == item.getId())
                            enumValues.put(item.getId().toString(), item.getTitle());
                    }
                    catch (NumberFormatException e1)
                    {
                        log.error(e1.getMessage());
                    }
                    catch (EdmException e)
                    {
                        log.info(e.getMessage());
                    }
                    for (DSDivision div : divs)
                    {
                        if (item.getId().longValue() == div.getId())
                            enumValues.put(item.getId().toString(), item.getTitle());
                    }
                }
                else{
                    boolean toAdd = true;

//                    try {
//                        Integer divId = item.getId();
//                        DSDivision div = DSDivision.find(divId);
//                        if (div.isHidden() && !enumSelected.contains(divId.toString()))
//                            toAdd = false;
//                    } catch (EdmException e) {
//                        log.error("Nie mozna odnalezc dzialu na podstawie id w enum field.",e);
//                    }

                    // wersja bez ka�dorazowego wyszukiania dzia�u po id
                    Integer divId = item.getId();
                    if (divId != null){
                        DSDivision div = getDivisionById (onlyDivisions,divId.intValue());
                        toAdd = div==null || !div.isHidden() || enumSelected.contains(divId.toString());
                    }

					if (toAdd) {
						if (AvailabilityManager.isAvailable("divisionFieldAddParentTitleToKierownik")) {
							if (item.getTitle().equalsIgnoreCase("kierownik") ) {
								String parrentTitle;
								try {
									DSDivision div = DSDivision.find(item.getId());
									if(div!=null && div.getParent()!=null && div.getParent().getName()!=null ){
									parrentTitle = div.getParent().getName();
									enumValues.put(item.getId().toString(),parrentTitle +" - "+ item.getTitle());
								}else {
									enumValues.put(item.getId().toString(), item.getTitle());
								}
								}catch (EdmException e) {
									log.error("B�ad przy pobieraniu rodzica ", e);
									continue;
								}
							}else{
								enumValues.put(item.getId().toString(), item.getTitle());
							}
						}else{
						enumValues.put(item.getId().toString(), item.getTitle());
					}
					}
				}
            }
        } else {
            if (fieldsValues.get(getRefField()) != null) {
                String refFieldId = fieldsValues.get(getRefField()).toString();
                Set<EnumItem> enumItemsByRef = getEnumItemsByRef(refFieldId);
                if (enumItemsByRef != null) {
                    for (EnumItem item : enumItemsByRef) {
                        if (!item.isAvailable() && (selectedId == null || !selectedId.equals(item.getId()))) {
                            continue;
                        }
                        enumValues.put(item.getId().toString(), item.getTitle());
                    }
                }
            }
        }

        return new EnumValues(enumValues, enumSelected);
	}
    private static DSDivision getDivisionById(DSDivision[] divisions, long id){
        for (DSDivision div : divisions)
            if (div.getId().equals(id))
                return div;
        return null;
    }


	public String getRefField()
	{
		return refField;
	}

    @Override
    public void setRefEnumItemsMap(Map<String, SortedSet<EnumItem>> refEnumItemsMap) {
        enumDefault.setRefEnumItemsMap(refEnumItemsMap);
    }

    @Override
    public void setAvailableItems(Set<EnumItem> availableItems) {
        enumDefault.setAvailableItems(availableItems);
    }

    @Override
    public void setAllItems(Set<EnumItem> allItems) {
        enumDefault.setAllItems(allItems);
    }

    @Override
    public EnumValues getEnumItemsForDwr(Map<String, Object> fieldsValues, String id, String temp) throws EntityNotFoundException {
        return getEnumItemsForDwr(fieldsValues);
    }

    @Override
    public void setEnumItems(Map<Integer, EnumItem> enumItems) {
        enumDefault.setEnumItems(enumItems);
    }

    @Override
    public void setRepoField(boolean repoField) {
    }

    @Override
    public void setItemsReferencedForAll(Map<Integer, EnumItem> itemsReferencedForAll) {

    }

}
