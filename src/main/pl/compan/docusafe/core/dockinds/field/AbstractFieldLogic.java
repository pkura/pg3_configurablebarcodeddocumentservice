package pl.compan.docusafe.core.dockinds.field;

import java.util.Map;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

public abstract class AbstractFieldLogic implements FieldLogic {

	static final StringManager sm = GlobalPreferences.loadPropertiesFile(AbstractFieldLogic.class.getPackage().getName(),null);
	protected Field field;
	
	
	/**
	 * Defaultowa metoda tworz�ca wpis do historii dokumentu o takiej samej implementacji jak odpowiadajacy jej funkcyjnie kod z metody {@link pl.compan.docusafe.core.dockinds.DocumentKind#setWithHistory(Long, Map, boolean, String, String)}
	 */
	@Override
	public String prepareCustomFieldHistoryEntry(Object oldDesc, Object oldKey, Object newDesc, Object newKey) {
		if(field != null){
			StringBuilder historyBuilder = null;
			String change = null;
	        if (oldDesc == null || "".equals(oldDesc))
	            change = field.getName() + " " + sm.getString("ZwartosciPustejNa",newDesc);
	        else if (newDesc == null || "".equals(newDesc))
	            change = field.getName() + " " + sm.getString("ZNaWartoscPusta",oldDesc);
	        else
	            change = field.getName() + " " + sm.getString("Zna",oldDesc,newDesc);
	        if (historyBuilder == null)
	            historyBuilder = new StringBuilder(sm.getString("ZmienionoWartosciPol")+ " ").append(change);
	        else
	            historyBuilder.append(", ").append(change);
			return historyBuilder.toString();
		}else{
			return null;
		}
	}

	public Field getField() {
		return field;
	}

	
	public void setField(Field field) {
		this.field = field;
	}

}
