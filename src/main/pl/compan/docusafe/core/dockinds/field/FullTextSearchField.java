package pl.compan.docusafe.core.dockinds.field;

import com.google.common.base.CaseFormat;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.dom4j.Element;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.lucene.IndexStatus;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.*;

/**
 * Pole obsługujące indeks do wyszukiwania pełnotekstowego
 *
 * Pole nie jest widoczne na formatkach edycji ale umożliwia wyszukiwanie po zindeksowanych wartościach
 *
 * @author Michał Sankowski <michal.sankowski@docusafe.pl>
 */

public final class FullTextSearchField extends NonColumnField implements SearchPredicateProvider, ArchiveListener{
    private final static Logger LOG = LoggerFactory.getLogger(FullTextSearchField.class);

    private final FieldValue nullFieldValue;
    private final String indexColumn;
    private Syntax syntax;

    /** Obiekty dostarczające tekst do indeksowania */
    private final List<Function<Document, CharSequence>> textProviders = Lists.newArrayList();

    /**
     * Składnia sql'a do generowania predykatu
     */
    public enum Syntax {
        /** Składnia dla indeksów typu CTXSYS.CONTEXT */
        ORACLE_CONTEXT_INDEX,
        MSSQL_CONTEXT_INDEX
        
    }

    public FullTextSearchField(String id, String cn, String column, Map < String, String > names) throws EdmException {
        super(id, cn, names, Field.FULL_TEXT_SEARCH, null, null, false, Collections.<Condition>emptyList(), Collections.<Condition>emptyList());
        this.indexColumn = column;
        this.nullFieldValue = new FieldValue(this, null);
    }

    @Override
    public FieldValue provideFieldValue(long documentId, ResultSet rs) throws SQLException, EdmException {
        return nullFieldValue;
    }

    @Override
    public Object simpleCoerce(Object value) throws FieldValueCoerceException {
        return null;
    }

    @Override
    public boolean isHidden() {
        return true;
    }

    @Override
    public boolean isSearchShow() {
        return true;
    }   

    @Override
    public void addSearchPredicate(DockindQuery dockindQuery, Map<String, Object> values, Map<String, Object> fieldValues) {
        Object query = values.get(getCn());
//        LOG.info("query = '{}'", query);
        if(query == null) return;
        switch (getSyntax()){
            case ORACLE_CONTEXT_INDEX:
                dockindQuery.addOracleContains(indexColumn, query.toString());
                return;
            case MSSQL_CONTEXT_INDEX:
            	dockindQuery.addMSSQLContains(indexColumn, query.toString());
            	return;
            default:
                throw new UnsupportedOperationException("unsuported syntax " + getSyntax());
        }
    }

    @Override
    public void onArchive(Document document, ArchiveSupport as) throws EdmException{        
        StringBuilder sb = new StringBuilder(50);
        for(Function<Document, CharSequence> textProvider : getTextProviders()){
            sb.append(textProvider.apply(document));
        }
        if(sb.length() < 1 )
        	return;
        PreparedStatement ps = null;
        try {
            String sql = "update " + document.getDocumentKind().getTablename() + " set " + indexColumn + " = ? where document_id = ? ";
            LOG.info("sql = \n'{}'\ncollected text = \n'{}'", sql, sb);
            ps = DSApi.context().prepareStatement(sql);
            ps.setString(1, sb.toString());
            ps.setLong(2, document.getId());
            int updated = ps.executeUpdate();
            assert updated > 0;
        } catch(Exception ex) {
            throw new EdmException(ex); 
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }
    
    public String getIndexColumn() {
		return indexColumn;
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field getDwrField() {
    		pl.compan.docusafe.core.dockinds.dwr.Field stringField = new pl.compan.docusafe.core.dockinds.dwr.Field("DWR_"+this.getCn(), this.getName(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING);
    		return stringField;
	}

	public Syntax getSyntax()
	{
		return syntax;
	}

	public void setSyntax(Syntax syntax)
	{
		this.syntax = syntax;
	}

	public List<Function<Document, CharSequence>> getTextProviders()
	{
		return textProviders;
	}
}
