package pl.compan.docusafe.core.dockinds;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;

/**
 * Resolver wyciosany z wn�trza DocumentKind
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
class OldSchemeResolver implements SchemeResolver {
    private final Logger log = LoggerFactory.getLogger(OldSchemeResolver.class);
    private final List<DockindScheme> schemeList;// = getDockindInfo().getDockindScheme();

    OldSchemeResolver(List<DockindScheme> schemeList) {
        this.schemeList = schemeList;
    }

    public DockindScheme provideSheme(FieldsManager fm) throws EdmException {
        if(fm == null) {
            log.error("DOCKIND SHEME ERROR : FM is null", new Throwable());
            return null;
        }
        dockindSchemeLoop:
        for (DockindScheme dockindSheme : schemeList) {
            boolean andMixing = dockindSheme.getConditionMixing().equals(DockindScheme.CONDITION_MIXING_AND);
            log.trace("Scheme : {}", dockindSheme.getCn());
            if(andMixing) {
                for (Condition con : dockindSheme.getConditions()) {
                    if(!con.toFieldsManagerPredicate().apply(fm)){
                        continue dockindSchemeLoop;
                    }
                }
                log.info("returning Scheme : {}", dockindSheme.getCn());
                return dockindSheme;
            } else {
                for (Condition con : dockindSheme.getConditions()) {
                    if(con.toFieldsManagerPredicate().apply(fm)){
                        log.info("returning Scheme : {}", dockindSheme.getCn());
                        return dockindSheme;
                    }
                }
            }
        }
        log.info("returning null Scheme");
        return null;
    }

	@Override
	public DockindScheme provideSheme(FieldsManager fm, String activity) throws EdmException {
		// TODO Auto-generated method stub
		return null;
	}
}
