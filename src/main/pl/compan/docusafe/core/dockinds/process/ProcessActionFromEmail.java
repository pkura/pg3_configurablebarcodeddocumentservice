package pl.compan.docusafe.core.dockinds.process;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailConstants;

import com.google.common.base.Objects;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.ActivitiWorkflowFactory;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.pg.ZadanieLogic;
import pl.compan.docusafe.process.DocuSafeActivitiesProcessView;
import pl.compan.docusafe.process.DocuSafeProcessDefinition;
import pl.compan.docusafe.process.ProcessEngine;
import pl.compan.docusafe.process.ProcessEngineType;
import pl.compan.docusafe.process.ProcessForDocument;
import pl.compan.docusafe.process.form.ActivityProvider;
import pl.compan.docusafe.process.form.ActivityProviderFactory;
import pl.compan.docusafe.process.form.Button;
import pl.compan.docusafe.service.ServiceDriverNotSelectedException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.ServiceNotFoundException;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.ExtendedRenderBean;
import pl.compan.docusafe.web.common.RenderBean;
import pl.compan.docusafe.web.office.common.BaseTabsAction;
import pl.compan.docusafe.web.office.common.DocumentArchiveTabAction;
import pl.compan.docusafe.web.office.common.ProcessBaseTabsAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;



public class ProcessActionFromEmail extends EventActionSupport
{

	protected static Logger log = LoggerFactory.getLogger(ProcessActionFromEmail.class);
	private String processName;
	private String processId;
	private StringManager sm = GlobalPreferences.loadPropertiesFile("", null);
	private String processAction;
	private String processActionCorrectionInfo;
	private boolean processActionStored;
	private boolean doAction;
	private String basicUrl = Docusafe.getBaseUrl() + "/office/process-action-from-email.action?";
	private String urlArchiveAcrion = Docusafe.getBaseUrl() +"/office/internal/document-archive.action?documentId=";
	private String ActionUrl;
	private String hashedActionsUrl;
	private String activityId;
	private String returnUrl =Docusafe.getBaseUrl() +"/office/tasklist/user-task-list.action";
	private long userId;
	public String haschString = "COM-PAN-SYSTEM_sp_z.o.o";
	public static final String DEFAULT_ENCODING = "UTF-8";
	private ProcessInstance procesInstance;
	private long documentId;
	private static MultiKeyMap mapActionUsersToUrl = new MultiKeyMap();
	private LinkedList<RenderBean> processRenderBeans;
	private final static Logger logger = LoggerFactory.getLogger(ProcessActionFromEmail.class);


	public void sendEmail(Long documentId, String usernameDekretujacy, String odbiorcy, boolean przygtujEmail) throws EdmException
	{
		String a = "";
		String[] usersNames = odbiorcy.split(",");
		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
		MultiKeyMap actionsForUsers = generateUrlsMapForUsers(doc.getId(), usersNames);
		for (String userName : usersNames)
		{
			sendEmailToRecipient(actionsForUsers, userName, doc);
		}

	}

	public void sendDWRejected(Long documentId, String usernameDekretujacy, String odbiorcy, boolean przygtujEmail)
	{
		String a = "";
		String[] as = odbiorcy.split(",");

	}

	public Collection<String> getUsersList(Long documentId) throws DocumentNotFoundException, EdmException {
		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
		List<String> users = new ArrayList<String>();
		 users = doc.getDocumentKind().logic().getUsersToProcesActionFromEmail(documentId);
		Collection<String> col = users;
		return col;
    }
	public String getAuthor(Long documentId) throws DocumentNotFoundException, EdmException {
		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
		
		return doc.getAuthor();
    }
	public String getCurrentUser() throws DocumentNotFoundException, EdmException {
		
		return DSApi.context().getPrincipalName();
    }
	
	
	private void sendEmail(String autor ,String odbiorcaEmail ,String title,String tresc) throws ServiceNotFoundException, ServiceDriverNotSelectedException, EdmException{
		((Mailer) ServiceManager.getService(Mailer.NAME)).send(odbiorcaEmail, odbiorcaEmail,
				title , tresc, EmailConstants.TEXT_HTML);	
	}
	
	public void setDecision(long docId, boolean wybor, String userName)
	{
		try
		{
			Document document = Document.find(docId, false);
			DSUser autor = DSUser.findByUsername(document.getAuthor());
			DSUser curUser = DSApi.context().getDSUser();
			Remark remark;
			if (wybor)
			{
				remark = new Remark(sm.getString("procesActionFromEmail.wykonalem"), userName);
				sendEmail(
						DSApi.context().getDSUser().asFirstnameLastname(),
						autor.getEmail(),
						sm.getString("procesActionFromEmail.realizationStatusTitle", curUser.asFirstnameLastname(), docId)+addHtmlBreakeLineTag(2),
						sm.getString("procesActionFromEmail.realizationStatus", curUser.asFirstnameLastname(), document.getDescription()+addHtmlBreakeLineTag(2),
								sm.getString("procesActionFromEmail.wykonalem")+addHtmlBreakeLineTag(2)));
			} else
			{
				remark = new Remark(sm.getString("procesActionFromEmail.odrzucilem"), userName);
				sendEmail(
						DSApi.context().getDSUser().asFirstnameLastname(),
						autor.getEmail(),
						sm.getString("procesActionFromEmail.realizationStatusTitle", curUser.asFirstnameLastname(), docId)+addHtmlBreakeLineTag(2),
						sm.getString("procesActionFromEmail.realizationStatus", curUser.asFirstnameLastname(), document.getDescription()+addHtmlBreakeLineTag(2),
								sm.getString("procesActionFromEmail.odrzucilem")+addHtmlBreakeLineTag(2)));
			}
			document.addRemark(remark);


		} catch (DocumentNotFoundException e)
		{
			e.printStackTrace();

		} catch (EdmException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void setup()
	{


		FillForm fillForm = new FillForm();



		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)

		.append(new FillForm()).appendFinally(CloseHibernateSession.INSTANCE);


		//		registerListener("doSearch")
		//		.append(OpenHibernateSession.INSTANCE)
		//		.append(new Search())
		//		.append(new FillForm())
		//		.appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event)
		{

			try
			{

				DSContext ctx = null;
				try
				{
					ctx = DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
					ctx.begin();
					hashedActionsUrl = ServletActionContext.getRequest().getRequestURI();

					getProcessAction();
					getProcessId();
					getProcessAction();
					getProcessActionCorrectionInfo();
					getActivityId();


					if (doAction && ctx.getDSUser().getId().equals(getUserId()))
					{
						OfficeDocument doc = OfficeDocument.findOfficeDocument(getDocumentId(), false);
						String ok;
						List<JBPMTaskSnapshot> taskSnapshots = JBPMTaskSnapshot.findByDocumentId(getDocumentId());
						for (JBPMTaskSnapshot ts : taskSnapshots)
						{
							if ("activiti".equals(ts.getProcess()) && ts.getDsw_resource_res_key().equals(DSUser.findById(getUserId()).getName()))
							{
								executeAction(doc, ts);
								String desc = sm.getString("procesActionFromEmail.confirmAction",getProcessAction(), getDocumentId(),(doc.getOfficeNumber()!=null ?doc.getOfficeNumber() : "Brak"), doc.getDescription());
								event.addActionMessage(desc);
								break; 
							}

						}
					} else
					{
						event.addActionError(sm.getString("procesActionFromEmail.denyAction",getProcessAction()));
						ctx._rollback();
						String nieok;
					}


					ctx.commit();

				} catch (DocumentNotFoundException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (EdmException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} finally
			{
				DSApi._close();
			}




		}





	}

	private static String xorMessage(String message, String key)
	{
		try
		{
			if (message == null || key == null)
				return null;

			char[] keys = key.toCharArray();
			char[] mesg = message.toCharArray();

			int ml = mesg.length;
			int kl = keys.length;
			char[] newmsg = new char[ml];

			for (int i = 0; i < ml; i++)
			{
				newmsg[i] = (char) (mesg[i] ^ keys[i % kl]);
			}// for i
			mesg = null;
			keys = null;
			return new String(newmsg);
		} catch (Exception e)
		{
			return null;
		}
	}// xorMessage

	public void sendEmailToRecipient(MultiKeyMap actionsForUsers, String userName, OfficeDocument doc) throws UserNotFoundException, EdmException
	{

		DSUser autor = DSUser.findByUsername(doc.getAuthor());
		DSUser odbiorca = DSUser.findByUsername(userName);
		boolean existTask = false;
		if (odbiorca.getEmail() == null)
			log.error(sm.getString("procesActionFromEmail.brakEmail", odbiorca.asFirstnameLastname()));
		else
		{
			MapIterator it = actionsForUsers.mapIterator();
			StringBuilder emailTresc = new StringBuilder();
			emailTresc.append(sm.getString("procesActionFromEmail.tresc", autor.asFirstnameLastname(), doc.getDescription()));
			addHtmlBreakeLineTag(emailTresc,1);
			addAbstractDescriprion(emailTresc, doc);
			addHtmlBreakeLineTag(emailTresc,1);
			addSpecifyDescription(emailTresc ,odbiorca,  doc);
			addHtmlBreakeLineTag(emailTresc,1);
			while (it.hasNext())
			{
				it.next();
				MultiKey mk = (MultiKey) it.getKey();
				String reciver = (String) mk.getKey(0);
				String buttonName = (String) mk.getKey(1);
				if (reciver.contains(odbiorca.getName()))
				{
					if(!AvailabilityManager.isAvailable("procesActionFromEmail.ActionlinkDisabled")){
						emailTresc.append(getActionLinkToEmail(userName, buttonName, it.getValue(), doc));
					}else {
						emailTresc.append("Link do zadania : ");
						emailTresc.append(getActionLinkToEmail(doc.getId(), "Przejd�"));
					}
					
					addHtmlBreakeLineTag(emailTresc,1);
					existTask = true;
				}
			}
			if (existTask)
				((Mailer) ServiceManager.getService(Mailer.NAME)).send(odbiorca.getEmail(), odbiorca.getEmail(),
						sm.getString("procesActionFromEmail.title", autor.asFirstnameLastname()), emailTresc.toString(), EmailConstants.TEXT_HTML);
		}
	}
	
	//addHtmlBreakeLineTag(sb, 1);
	
	private String  getActionLinkToEmail(Long id, String name)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<a href='" + urlArchiveAcrion+id + "'>" + name + "</a>");
		addHtmlBreakeLineTag(sb, 1);

		return sb.toString();
	}

	private void addSpecifyDescription(StringBuilder emailTresc, DSUser odbiorca, OfficeDocument doc) throws EdmException
	{
		doc.getDocumentKind().logic().addSpecialValueToSendingActionEmail(doc, emailTresc, odbiorca);
		
	}

	private void addAbstractDescriprion(StringBuilder emailTresc, OfficeDocument doc)
	{
		if(doc.getAbstractDescription()!=null && !doc.getAbstractDescription().isEmpty()){
			emailTresc.append(" Opis Zadania :");
			emailTresc.append(doc.getAbstractDescription());
		}
		
	}

	public String szyfrujParametry(String doZakodowania)
	{

		return base64encode(xorMessage(doZakodowania, haschString));
	}

	public String odszyfrujParametry(String doOdkodowania)
	{

		return xorMessage(base64decode(doOdkodowania), haschString);
	}

	private static String base64encode(String text)
	{
		try
		{
			String rez = String.valueOf(new Base64().encode(text.getBytes(DEFAULT_ENCODING)));
			return rez;
		} catch (UnsupportedEncodingException e)
		{
			return null;
		}
	}

	private static String base64decode(String text)
	{

		try
		{
			return String.valueOf(new Base64().decode(text.getBytes(DEFAULT_ENCODING)));
		} catch (IOException e)
		{
			return null;
		}

	}

	/**
	 * Metoda przygotowuej mape user  , akcja (...)-> URL  akcji i urla zwiazanego z mo�liw� wykonaniem akcji na procesie dla danego uzytkownika(odbiorcy dekretacji) 
	 * @throws EdmException
	 */
	private MultiKeyMap generateUrlsMapForUsers(Long documentId, String[] userNames) throws EdmException
	{

		mapActionUsersToUrl = new MultiKeyMap();

		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);

		//Kolekcja objekt�w procesowych dla documentu i widoku u�ytkownika
		Collection<RenderBean> collectionProcessRenderBeans = ProcessUtils.renderAllProcesses(doc, ProcessActionContext.ARCHIVE_VIEW);
		//lista tasksnapshotow dla dokumentu w celu przypiecia poprawnego activity id do wykonania akcji na procesie
		//List<JBPMTaskSnapshot> taskSnapshots = JBPMTaskSnapshot.findByDocumentId(documentId);
		//odbiorcy 
		for (String userName : userNames)
		{
			String buttonName = "";
			for (Iterator<RenderBean> iterator = collectionProcessRenderBeans.iterator(); iterator.hasNext();)
			{
				RenderBean rb = iterator.next();

				Collection<Objects> objects = (Collection<Objects>) ((ExtendedRenderBean) rb).get("activities");
				if (objects!=null && !objects.isEmpty())
				{
					Button[] actionsByttons = objects.toArray(new Button[0]);
					for (Button button : actionsByttons)
					{
						DSUser user = DSUser.findByUsername(userName);
						StringBuilder actionUrl = new StringBuilder();
						actionUrl.append("documentId=" + doc.getId());
						actionUrl.append("&userId=" + user.getId());
						actionUrl.append("&processAction=" + button.getName());
						buttonName = button.getLabel();
						actionUrl.append("&doAction=true");
						String niezaszyfrow = basicUrl + actionUrl.toString();
						String szyfrowurl = basicUrl + "&params=" + szyfrujParametry(actionUrl.toString());
						//mapActionToUrl.put(userName, actionUrl.toString());
						mapActionUsersToUrl.put(userName, buttonName, niezaszyfrow);

					}
				}

				/*	for (JBPMTaskSnapshot ts : taskSnapshots)
					{
						if ("activiti".equals(ts.getProcess()))
						{
							if (ts.getDsw_resource_res_key().equals(user.getName()))
							{ts.getActivity_lastActivityUser();
								actionUrl.append("&activityId=" + ts.getActivityKey() + "," + ts.getProcess());
								actionUrl.append("&doAction=true");
								String niezaszyfrow = basicUrl + actionUrl.toString();
								String szyfrowurl = basicUrl + szyfrujParametry(actionUrl.toString());
								//mapActionToUrl.put(userName, actionUrl.toString());
								mapActionUsersToUrl.put(userName, buttonName, szyfrowurl);
								//mapActionUsersToUrl.put(userName, buttonName,  szyfrowurl);
								break;
							}
						}

					}

				}*/
			}
		}

		return mapActionUsersToUrl;
	}

	public void addHtmlBreakeLineTag(StringBuilder sb ,Integer countElements){
		if(countElements!=null){
			for (int i = 0 ; i<=countElements; i++){
				sb.append("<br></br>");
			}
		}else
		sb.append("<br></br>");
		
	}
	public String addHtmlBreakeLineTag(Integer countElements){
		StringBuilder sb = new StringBuilder();
		if(countElements!=null){
			for (int i = 0 ; i<=countElements; i++){
				sb.append("<br></br>");
			}
		}else
		sb.append("<br></br>");
		
		return sb.toString();
		
	}
	public String getActionLinkToEmail(String userName, String buttonName, Object actionLink, OfficeDocument doc)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<b>" + buttonName + "</b>\n");
		String link = "<a href='" + actionLink + "'>" + buttonName + "</a>";
		sb.append(sm.getString("procesActionFromEmail.akcja", buttonName, link));
		sb.append("\n");

		return sb.toString();

	}

	/**
	 * MEtoda  pzygotowuje instancje procesu na kt�rej wykoanna bedzie akcja  
	 * @param doc
	 * @param ts 
	 * @throws EdmException
	 */
	public void executeAction(OfficeDocument doc, JBPMTaskSnapshot ts) throws EdmException
	{

		ProcessInstance pi = null;
		String activitiId = ts.getProcess() + "," + ts.getActivityKey();
		List<String> processids = ProcessForDocument.getProcessIdsForDocument(documentId, ProcessEngineType.ACTIVITI);
		Collection<ProcessDefinition> pdefs = doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcessDefinitions();
		for (ProcessDefinition pd : pdefs)
		{
			if (ProcessEngineType.ACTIVITI.getKey().equals(((DocuSafeProcessDefinition) pd).getProcessEngineType().getKey()))
			{
				pi = pd.getLocator().lookupForId(processids.get(0));
				executeProcessAction(getProcessAction(), activitiId, doc, pi, pd);
			}
		}
	}

	/**
	 * Metoda wykonuje podan� akcj� na procesie
	 * @param processAction
	 * @param activityId
	 * @param doc
	 * @param pi
	 * @param pd
	 * @throws EdmException
	 */
	private void executeProcessAction(String processAction, String activityId, OfficeDocument doc, ProcessInstance pi, ProcessDefinition pd)
			throws EdmException
	{
		pl.compan.docusafe.core.dockinds.process.ProcessAction pa; 
		pd.getLogic().process(pi, pa = ProcessActionContext.action(processAction).document(doc).stored(false).activityId(activityId));
	}

	/**
	 * Metoda przygotowuej obiekty do wykonania akcji na procesie okreslonej w urlu 
	 * @throws EdmException
	 */
	public void doProcessAction() throws EdmException
	{

		ProcessInstance pi = null;
		OfficeDocument doc = null;

		doc = OfficeDocument.findOfficeDocument(documentId, false);

		Collection<ProcessDefinition> pdefs = doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcessDefinitions();
		for (ProcessDefinition pd : pdefs)
		{

			if (ProcessEngineType.ACTIVITI.getKey().equals(((DocuSafeProcessDefinition) pd).getProcessEngineType().getKey()))
			{
				pi = pd.getLocator().lookupForId(getProcessId());
				executeProcessAction(getProcessAction(), getActivityId(), doc, pi, pd);
			}
		}
	}

	/**
	 * METODA SZYFRUJE  STRING METODA AES
	 * @param urlToHash
	 * @return hashedString
	 */
	public String hashUrlParamaters(String urlToHash)
	{

		String hashedUrl = "";
		try
		{
			Key aesKey = new SecretKeySpec(haschString.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES");

			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			hashedUrl = new String(cipher.doFinal(urlToHash.getBytes()));

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return hashedUrl;
	}

	/**
	 * METODA ODSZYFROWUJE   STRING METODA AES
	 * @param hashedUrl
	 * @return unHashed String
	 */
	private String unHashUrlParameters(String hashedUrl)
	{

		String unHashed = "";
		try
		{

			Key aesKey = new SecretKeySpec(haschString.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES");

			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encrypted = cipher.doFinal(hashedUrl.getBytes());
			unHashed = new String(cipher.doFinal(encrypted));


		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return unHashed;
	}

	public LinkedList<RenderBean> getProcessRenderBeans()
	{
		return processRenderBeans;
	}

	public void setProcessRenderBeans(LinkedList<RenderBean> processRenderBeans)
	{
		this.processRenderBeans = processRenderBeans;
	}

	public String getProcessName()
	{
		return processName;
	}

	public void setProcessName(String processName)
	{
		this.processName = processName;
	}

	public String getProcessId()
	{
		return processId;
	}

	public void setProcessId(String processId)
	{
		this.processId = processId;
	}

	public String getProcessAction()
	{
		return processAction;
	}

	public void setProcessAction(String processAction)
	{
		this.processAction = processAction;
	}

	public String getProcessActionCorrectionInfo()
	{
		return processActionCorrectionInfo;
	}

	public void setProcessActionCorrectionInfo(String processActionCorrectionInfo)
	{
		this.processActionCorrectionInfo = processActionCorrectionInfo;
	}

	public boolean isProcessActionStored()
	{
		return processActionStored;
	}

	public void setProcessActionStored(boolean processActionStored)
	{
		this.processActionStored = processActionStored;
	}

	public boolean isDoAction()
	{
		return doAction;
	}

	public void setDoAction(boolean doAction)
	{
		this.doAction = doAction;
	}

	public String getBasicUrl()
	{
		return basicUrl;
	}

	public void setBasicUrl(String basicUrl)
	{
		this.basicUrl = basicUrl;
	}

	public String getActionUrl()
	{
		return ActionUrl;
	}

	public void setActionUrl(String actionUrl)
	{
		ActionUrl = actionUrl;
	}

	public String getHashedActionsUrl()
	{
		return hashedActionsUrl;
	}

	public void setHashedActionsUrl(String hashedActionsUrl)
	{
		this.hashedActionsUrl = hashedActionsUrl;
	}

	public ProcessInstance getProcesInstance()
	{
		return procesInstance;
	}

	public void setProcesInstance(ProcessInstance procesInstance)
	{
		this.procesInstance = procesInstance;
	}

	public Long getDocumentId()
	{
		return documentId;
	}

	public void setDocumentId(Long documentId)
	{
		this.documentId = documentId;
	}

	public static Logger getLogger()
	{
		return logger;
	}

	public String getActivityId()
	{
		return activityId;
	}

	public void setActivityId(String activityId)
	{
		this.activityId = activityId;
	}

	public String getReturnUrl()
	{
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl)
	{
		this.returnUrl = returnUrl;
	}

	public static MultiKeyMap getMapActionUsersToUrl()
	{
		return mapActionUsersToUrl;
	}

	public static void setMapActionUsersToUrl(MultiKeyMap mapActionUsersToUrl)
	{
		ProcessActionFromEmail.mapActionUsersToUrl = mapActionUsersToUrl;
	}

	public long getUserId()
	{
		return userId;
	}

	public void setUserId(long userId)
	{
		this.userId = userId;
	}


}
