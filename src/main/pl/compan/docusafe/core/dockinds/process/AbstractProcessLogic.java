package pl.compan.docusafe.core.dockinds.process;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;

import java.util.Map;

/**
 */
public abstract class AbstractProcessLogic implements ProcessLogic{

    public void startProcess(OfficeDocument od, Map<String, ?> params) throws EdmException {
    }
}
