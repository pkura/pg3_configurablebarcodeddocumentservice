package pl.compan.docusafe.core.dockinds.process;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.EnumMap;
import java.util.Map;

/**
 * Parametry przekazywane podczas tworzenia obiekt�w
 */
public class InstantiationParameters {
    private final static Logger log = LoggerFactory.getLogger(InstantiationParameters.class);

    Map<Param, String> params = new EnumMap<Param, String>(Param.class);

    /**
     * Nazwy parametr�w
     */
    public static enum Param {
        /** Maksymalna liczba proces�w dla danego dokumentu */
        MAX_INSTANCES_PER_DOCUMENT,
        PROCESS_NAME,
        STATUS_FIELD,
        ARCHIVE_VIEW_SHOW,
        JBPM4_PROCESS_NAME;

        String xmlName(){
            return name().toLowerCase().replace("_","-");
        }
    }

    /**
     * Sprawdza czy dany parametr zosta� okre�lony
     * @param param
     * @return
     */
    public boolean hasParam(Param param){
        return params.containsKey(param);
    }

    /**
     * Sprawdza czy dany parametr zosta� okre�lony - je�li nie, wyrzuca wyj�tek
     * @param param
     */
    public void assertParam(Param param){
        if(!hasParam(param)){
            throw new IllegalArgumentException(
                "parameter "
                + param.xmlName()
                + " needs to be specified; specified parameters " + params.toString());
        }
    }

    InstantiationParameters(Map<String, String> rawMap){
        log.info("utworzono InstantiationParameters, rozmiar" + rawMap.size());
        for(Map.Entry<String, String> parameter : rawMap.entrySet()){
            params.put(fromXmlName(parameter.getKey()), parameter.getValue());
        }
    }

    public String getParam(Param param){
        return params.get(param);
    }

    public String getParamOrDefault(Param param, String def){
        if(hasParam(param)){
            return getParam(param);
        } else {
            return def;
        }
    }

    private Param fromXmlName(String name){
        return Param.valueOf(name.toUpperCase().replace('-','_'));
    }
}
