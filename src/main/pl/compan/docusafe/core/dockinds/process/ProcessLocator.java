package pl.compan.docusafe.core.dockinds.process;

import java.util.Collection;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;

/**
 * Klasa pozwalaj�ca wyszukanie isniej�cych proces�w dla r�nych element�w systemu Docusafe.
 * @author Micha� Sankowski
 */
public interface ProcessLocator {

	/**
	 * Zwraca kolekcj� proces�w zwi�zanych z tym dokumentem, je�li nie istnieje zwraca pust� list�
	 * @param doc nie mo�e by� null
	 * @return mo�e by� pusta, nigdy nie mo�e by� null
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	Collection<? extends ProcessInstance> lookupForDocument(Document doc) throws EdmException;

	/**
	 * Zwraca proces o danym id lub null je�li taki proces nie istnieje
	 * @param id
	 * @return mo�e by� null
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	ProcessInstance lookupForId(Object id) throws EdmException;

    /**
     * Zwraca instancj� procesu dla danego taska
     * @param id
     * @return
     * @throws EdmException
     */
    ProcessInstance lookupForTaskId(String id) throws EdmException;
}
