package pl.compan.docusafe.core.dockinds.process;
import pl.compan.docusafe.core.ActionEventAdapter;
import pl.compan.docusafe.core.Messages;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * Klasa reprezentująca pojedyncze polecenie wykonywane na procesie.
 * Pozwala przekazać logice procesu wymagane argumenty z widoku i viceversa
 * @author Michał Sankowski
 */
public class ProcessAction extends ProcessActionContext{
	private final static Logger LOG = LoggerFactory.getLogger(ProcessAction.class);

    //widok -> logika
	private final String actionName;
	private Long documentId;
	private OfficeDocument document;
	private String activityId;
    private boolean stored;

    //obiekt przekazany z widoku służący do wysyłania komunikatów logika -> widok
	private Messages event = NULL_HANDLER;

    //logika -> widok
    boolean reassigned;

	private ProcessAction(ActionType actionType, String name){
		super(actionType);
		this.actionName = name;
	}

	static ProcessAction specific(String name){
		return new ProcessAction(ActionType.SPECIFIC_ACTION, name);
	}

	static ProcessAction adminSpecific(String name){
		return new ProcessAction(ActionType.ADMIN_SPECIFIC_ACTION, name);
	}

	public ProcessAction event(ActionEvent event){
		this.event = new ActionEventAdapter(event);
		return this;
	}

	public ProcessAction document(OfficeDocument doc){
		this.document = doc;
		this.documentId = doc.getId();
		return this;
	}

	public ProcessAction activityId(String activityId){
		this.activityId = activityId;
		return this;
	}

	public ProcessAction docId(long id){
		this.documentId = id;
		return this;
	}

    public ProcessAction reassigned(boolean res){
        reassigned = res;
        return this;
    }

    public ProcessAction stored(boolean stored){
        this.stored = stored;
        return this;
    }

    public boolean isReassigned(){
        return reassigned;
    }

	public String getActionName() {
		return actionName;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public OfficeDocument getDocument() {
		try {
			if (document == null && documentId != null) {
				document = OfficeDocument.find(documentId);
			}
		} catch (Exception e) {
			addActionError(e.getLocalizedMessage());
			LOG.error(e.getLocalizedMessage(), e);
		}
		return document;
	}

	public String getActivityId() {
		return activityId;
	}

	public void addActionError(String s) {
		event.addActionError(s);
	}

	public void addActionMessage(String s) {
		event.addActionMessage(s);
	}

    public boolean isStored() {
        return stored;
    }

    private static final Messages NULL_HANDLER = new Messages(){
		public void addActionError(String s) {
		}
		public void addActionMessage(String s) {
		}
	};
}
