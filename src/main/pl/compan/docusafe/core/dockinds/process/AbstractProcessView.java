package pl.compan.docusafe.core.dockinds.process;
import java.util.ArrayList;
import java.util.Collection;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.web.common.RenderBean;

/**
 * Klasa bazowa implementacji ProcessView
 * @author Micha� Sankowski
 */
public abstract class AbstractProcessView implements ProcessView{
    /** Pusty szablon - nie wy�wietla nic */
    protected final static String EMPTY_TEMPLATE = "empty.vm";

	public final Collection<? extends RenderBean> render(Collection<? extends ProcessInstance> pis) throws EdmException{
		ArrayList<RenderBean> ret = new ArrayList<RenderBean>(pis.size());
		for(ProcessInstance pi: pis){
			ret.add(this.render(pi));
		}
		return ret;
	}

	public Collection<? extends RenderBean> render(Collection<? extends ProcessInstance> pis, ProcessActionContext context)throws EdmException {
		ArrayList<RenderBean> ret = new ArrayList<RenderBean>(pis.size());
		for(ProcessInstance pi: pis){
			ret.add(this.render(pi, context));
		}
		return ret;
	}

	public RenderBean render(ProcessInstance pi)throws EdmException {
		return render(pi, ProcessActionContext.VIEW);
	}
}
