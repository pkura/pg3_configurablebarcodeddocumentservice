package pl.compan.docusafe.core.dockinds.process;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;

import java.util.Map;

/**
 * Klasa odpowiedzialna za logik� procesu.
 * @author Micha� Sankowski
 */
public interface ProcessLogic {

    /**
     * Rozpoczyna dany proces
     * @param od dokument dla kt�rego rozpocz�c proces
     * @param params parametry do tworzonego procesu
     */
    public void startProcess(OfficeDocument od, Map<String, ?> params) throws EdmException;

    /**
     * Wykonuje podan� akcj� na podanym procesie
     * @param pi nie mo�e by� null
     * @param pa nie mo�e by� null
     * @throws EdmException
     */
	public void process(ProcessInstance pi, ProcessAction pa) throws EdmException;
}
