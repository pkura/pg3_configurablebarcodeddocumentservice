package pl.compan.docusafe.core.dockinds.process;
import java.util.*;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.paa.NormalLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.annotation.Nullable;

/**
 * Klasa obs�uguj�ca wszystkie procesy danego dockindu. Jest wype�niana podczas parsowania xmlowego dockinda.
 * @author Micha� Sankowski
 */
public class ProcessManager {

	private final static Logger log = LoggerFactory.getLogger(ProcessManager.class);
	private final static ProcessManager EMPTY = new ProcessManager();

	private final Map<String, ProcessDefinition> processDefinitions;

	/**
	 * Zwraca managera nieobs�uguj�cego �adnego procesu.
	 */
	public static ProcessManager empty(){
		return EMPTY;
	}

	private ProcessManager(){
		this.processDefinitions = Collections.emptyMap();
	}

	public ProcessManager(Map<String, ProcessDefinition> processDefinitions){
		this.processDefinitions = processDefinitions;
	}

    /**
     * Zwraca wszystkie definicje proces�w
     * @return
     */
	public Collection<ProcessDefinition> getProcessDefinitions(){
		return processDefinitions.values();
	}

    public void onStart(OfficeDocument doc) throws EdmException {
        onStart(doc, Collections.<String, Object>emptyMap());
    }

    /**
     * Tworzy wszystkie procesy, kt�re powinny by� utworzone przy dodaniu nowego dokumentu
     * @param doc nowy dokument
     * @param processParams parametry przy tworzeniu dokumentu
     * @throws EdmException
     */
    public void onStart(OfficeDocument doc, Map<String, Object> processParams) throws EdmException {
        for(ProcessDefinition pd: processDefinitions.values()){
            log.info("onStart " + pd.getName() + " creationType " + pd.getCreationType());
            if(pd.getCreationType() == ProcessDefinition.CreationType.ON_CREATE) {
            	DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), "process_creation", null);
                pd.getLogic().startProcess(doc, processParams);
            }
        }
    }

    //TODO cache'owa�
    public Collection<ProcessDefinition> getManuallyStarted(OfficeDocument document){
        return Collections2.filter(processDefinitions.values(),new Predicate<ProcessDefinition>(){
            public boolean apply(ProcessDefinition processDefinition) {
                return processDefinition.getCreationType() == ProcessDefinition.CreationType.MANUAL;
            }
        });
    }

    /**
     * Zwraca definicj� procesu o danej nazwie lub null je�li proces nie istnieje
     * @param name nazwa procesu, nie mo�e by� null
     * @return definicja procesu lub null
     */
    public @Nullable ProcessDefinition getProcessOrNull(String name){
        return processDefinitions.get(name);
    }

    /**
     * Zwraca definicj� procesu o danej nazwie
     * @param name nazwa procesu, nie mo�e by� null
     * @return definicja procesu
     * @throws java.lang.IllegalArgumentException je�li proces nie istnieje
     */
	public ProcessDefinition getProcess(String name) throws IllegalArgumentException {
        ProcessDefinition ret = getProcessOrNull(name);
        if(ret == null) {
            throw new IllegalArgumentException("Brak procesu '" + name + "'");
        }
		return ret;
	}

    /**
     * Metoda tworzy nowy proces manual/read-only
     * @param od - OfficeDocument
     * @param params - Parametry procesu
     * @throws EdmException
     */
    public void assignment(OfficeDocument od, String activityId, Boolean startNewProcess, Map<String, Object> params) throws EdmException {
        log.debug("[assignment] begin, od.id = {}, activityId = {}", od.getId(), activityId);

        String forkOrPush = (String) params.get(Jbpm4Constants.KEY_FORK_PUSH);
        if (startNewProcess == null || !startNewProcess) {
            log.debug("[assignment] remove KEY_FORK_PUSH");
            params.remove(Jbpm4Constants.KEY_FORK_PUSH);
        }
        //startNewProcess != null && startNewProcess
        if(Boolean.TRUE.equals(startNewProcess) || Jbpm4Constants.FORK.equals(forkOrPush)) {
            log.debug("[assignment] assignmentFork and makeHistory");
            assignmentFork(od, params);
            makeHistory(od, params);
        } else {
            log.debug("[assignment] assignmentPush");
            assignmentPush(od, activityId, params);
        }
    }

    /**
     * Metoda przekazuje dokument w prcesie manual
     * @param od - OfficeDocument
     * @param params - Parametry procesu
     * @throws EdmException
     */
    private void assignmentPush(OfficeDocument od, String activityId, Map<String,Object> params) throws EdmException {
        log.debug("assignmentPush");
        if(params.containsKey(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM)) {
            if (AvailabilityManager.isAvailable("PAA")) {
                paaAssignmentPush(od,activityId,params);
            }else
                Jbpm4WorkflowFactory.reassign(
                        activityId,
                        od.getId(),
                        DSApi.context().getPrincipalName(),
                        (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM),
                        params);
        }else {
            Jbpm4WorkflowFactory.reassign(
                    activityId,
                    od.getId(),
                    DSApi.context().getPrincipalName(),
                    null,
                    new String[]{(String) params.get(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM)},
                    params);
        }
        if (AvailabilityManager.isAvailable("setupDocumentAssignmentsFromOnStartProcesParams"))
        {

            if (params.containsKey(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM))
            {
                DSUser u = DSUser.findByUsername((String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM));
                od.setCurrentAssignmentUsername(u.getName());
                if (u.getOriginalDivisionsWithoutGroup().length > 0)
                {
                    String division = u.getOriginalDivisionsWithoutGroup()[0].getGuid();
                    od.setCurrentAssignmentGuid(division);
                    od.setAssignedDivision(division);

                }
            } else if (params.containsKey(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM))
            {

                String[] divs = new String[] { (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM) };
                od.setCurrentAssignmentGuid(divs[0]);
                od.setAssignedDivision(divs[0]);
            }
        }
    }

    private void paaAssignmentPush(OfficeDocument od, String activityId, Map<String, Object> params) throws HibernateException, EdmException
    {
        // PAA  weryfikacja nadawcy i odbiorcy po przynaleznosci do departament?w
        // jezeli r?ne to zwraca dyrektora nadawcy departamentu na kt?rego jest dekretacja
        String toUsername = (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM);
        String fromUsername = DSApi.context().getPrincipalName();
        String newToUsername = od.getDocumentKind().logic().onStartManualAssignment(od, fromUsername, toUsername);
        //je?li odbiorca to sobiecka to wszystkie pisma id? tylko do niej - bez wzgl?du na departamenty
        if(toUsername.equals(Docusafe.getAdditionProperty("PAA.DecretWitoutValidateDivisionUSERNAME")))
        {
            Jbpm4WorkflowFactory.reassign(activityId, od.getId(), DSApi.context().getPrincipalName(),
                    (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM), params);
        }
        //jeslei odbiorta to nie sobiecka  i inny departament
        else if (!toUsername.equals(newToUsername))
        {
            String targetUsers[] = new String[2];
            String targetGuids[]= new String[0];
            String sekretariat = NormalLogic.getUserPelniocyRoleSekretariatu(newToUsername);

            targetUsers[0] = newToUsername;
            targetUsers[1] = sekretariat;

            Jbpm4WorkflowFactory.reassign(activityId, od.getId(), DSApi.context().getPrincipalName(), targetUsers, targetGuids);

        } else
        {
            Jbpm4WorkflowFactory.reassign(activityId, od.getId(), DSApi.context().getPrincipalName(),
                    (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM), params);
        }
    }

    /**
     * Metoda tworzy nowy proces manual/read-only
     * @param od - OfficeDocument
     * @param params - Parametry procesu
     * @throws EdmException
     */
    private void assignmentFork(OfficeDocument od, Map<String,Object> params) throws EdmException {
        log.debug("assignmentFork");
        String kind = (String) params.get(Jbpm4Constants.KEY_PROCESS_NAME);
        params.remove(Jbpm4Constants.KEY_PROCESS_NAME);
        params.put(Jbpm4WorkflowFactory.REASSIGNMENT_TIME_VAR_NAME, new Date());

        od.getDocumentKind()
                .getDockindInfo()
                .getProcessesDeclarations()
                .getProcess(kind)
                .getLogic()
                .startProcess(od, params);
    }

    public void makeHistory(OfficeDocument od, Map<String,Object> params) throws EdmException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setCtime(new Date());

        if(Jbpm4Constants.MANUAL_PROCESS_NAME.equals((String) params.get(Jbpm4Constants.KEY_PROCESS_NAME))) {
            ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
            ahe.setProcessName(Jbpm4Constants.MANUAL_PROCESS_NAME);
        } else {
            ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_CC);
            ahe.setProcessName(Jbpm4Constants.READ_ONLY_PROCESS_NAME);
        }

        if(params.containsKey(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM)) {
            ahe.setTargetUser((String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM));
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
        } else {
            ahe.setTargetGuid((String) params.get(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM));
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
        }

        if(params.containsKey(Jbpm4WorkflowFactory.OBJECTIVE)) {
            ahe.setObjective((String) params.get(Jbpm4WorkflowFactory.OBJECTIVE));
        }else if(AvailabilityManager.isAvailable("PAA")){
            ahe.setObjective((String)params.get(Jbpm4WorkflowFactory.DOCDESCRIPTION));
        }

        if(params.containsKey(Jbpm4WorkflowFactory.DEADLINE_TIME)) {
            ahe.setDeadline(DateUtils.formatJsDate((Date) params.get(Jbpm4WorkflowFactory.DEADLINE_TIME)));
        }

        if(params.containsKey(Jbpm4WorkflowFactory.CLERK)) {
            ahe.setClerk((String) params.get(Jbpm4WorkflowFactory.CLERK));
        }

        od.addAssignmentHistoryEntry(ahe);
    }
    public List<String> getNames() {
        List<String> names = new ArrayList<String>();
        for (ProcessDefinition pdef : getProcessDefinitions())
            if (StringUtils.isNotEmpty(pdef.getName()))
                names.add(pdef.getName());
        return names;
    }
    public List<String> getTitles() {
        List<String> titles = new ArrayList<String>();
        for (ProcessDefinition pdef : getProcessDefinitions())
            if (StringUtils.isNotEmpty(pdef.getTitle()))
                titles.add(pdef.getTitle());
        return titles;
    }
    public Map<String,String> getMappedNamesToTitles() {
        Map<String,String> titles = new HashMap<String,String>();
        for (ProcessDefinition pdef : getProcessDefinitions())
            if (StringUtils.isNotEmpty(pdef.getTitle()) && StringUtils.isNotEmpty(pdef.getName()))
                titles.put(pdef.getName(), pdef.getTitle());
        return titles;
    }
}
