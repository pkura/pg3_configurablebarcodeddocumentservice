package pl.compan.docusafe.core.dockinds.process;
import java.util.Collection;
import java.util.Collections;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;

/**
 * Klasa zawierająca domyślną implementację dla ProcessLocator - zwracającą puste zbiory.
 * @author Michał Sankowski
 */
public class AbstractProcessLocator implements ProcessLocator{

	public Collection<? extends ProcessInstance> lookupForDocument(Document doc) throws EdmException {
		return Collections.emptyList();
	}

	public ProcessInstance lookupForId(Object id) throws EdmException {
		return null;
	}

    public ProcessInstance lookupForTaskId(String id) throws EdmException {
        return null;
    }
}
