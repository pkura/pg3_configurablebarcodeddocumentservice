package pl.compan.docusafe.core.dockinds.process;

/**
 * Pojedynczy proces biznesowy.
 * @author Micha� Sankowski
 */
public interface ProcessInstance {

	public String getProcessName();
	public Object getId();

}
