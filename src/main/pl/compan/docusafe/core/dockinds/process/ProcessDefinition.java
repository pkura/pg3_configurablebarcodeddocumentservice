package pl.compan.docusafe.core.dockinds.process;

/**
 * Klasa opisuj�ca jeden rodzaj procesu biznesowego.
 * @author Micha� Sankowski
 */
public interface ProcessDefinition {
	String getName();
	
	ProcessView getView();
	ProcessLogic getLogic();
	ProcessLocator getLocator();

    CreationType getCreationType();
    String getTitle();

    enum CreationType {
        ON_CREATE,
        MANUAL,
        INTERNAL;
    }
}
