package pl.compan.docusafe.core.dockinds.process.manual;

import pl.compan.docusafe.api.user.office.ManualAssignmentDto;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.ManualMultiAssignmentTabAction;
import system.NotImplementedException;

import java.util.List;

/**
 * Klasa przeznaczona do Tworzenia obiektu ManualMultiAssignmentManager, aby mo�na by�o u�ywa� dekretacji r�cznej
 * w innych miejscach w systemie
 *
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class ManualMultiAssignmentFactory {

    private static final Logger log = LoggerFactory.getLogger(ManualMultiAssignmentFactory.class);

    public static ManualMultiAssignmentManager createUser(DSUser user, Document document) {
        throw new NotImplementedException("metoda do zaimplementowania dla 1 usera");
    }

    public static ManualMultiAssignmentManager createUser(List<DSUser> user, Document document) {
        throw new NotImplementedException("metoda do zaimplementowania dla 1isty user�w");
    }

    public static ManualMultiAssignmentManager createDivision(DSDivision user, Document document) {
        throw new NotImplementedException("metoda do zaimplementowania dla 1 dzia�u");
    }

    public static ManualMultiAssignmentManager createDivision(List<DSDivision> user, Document document) {
        throw new NotImplementedException("metoda do zaimplementowania dla 1isty dzia�u");
    }

    /**
     * Konwersja obiektu dS API na obiekt managera recznej dekretacji
     * @param assigns
     * @param document
     * @return
     */
    public static ManualMultiAssignmentManager create(List<ManualAssignmentDto> assigns, Document document) throws EdmException {
        ManualMultiAssignmentManager manager = new ManualMultiAssignmentManager();
        String[] targets = new String[assigns.size()];
        String[] kinds = new String[assigns.size()];
        String[] targetsKind = new String[assigns.size()];

        for(int i =0 ; i< assigns.size(); i++){
            ManualAssignmentDto ma = assigns.get(i);
            targets[i] = ma.getTargetName();
            kinds[i] = Boolean.TRUE.equals(ma.isKind()) ? ManualMultiAssignmentTabAction.KIND_EXECUTE : ManualMultiAssignmentTabAction.KIND_CC;
            targetsKind[i] = Boolean.TRUE.equals(ma.isTargetKind()) ? ManualMultiAssignmentTabAction.TO_USER : ManualMultiAssignmentTabAction.TO_DIVISION;
        }

        manager.setTargets(targets);
        manager.setKinds(kinds);
        manager.setTargetsKind(targetsKind);
        manager.setDocumentId(document.getId());
        manager.setActivity(getActivity(document));

        return manager;
    }

    private static String getActivity(Document document) throws EdmException {
        List<JBPMTaskSnapshot> tasks = JBPMTaskSnapshot.findByDocumentId(document.getId());

        for(JBPMTaskSnapshot t : tasks){
            log.trace("{} {}", t.getDsw_resource_res_key(), t.getActivityKey());
            // jesli pismo jest u zalogowanego na li�cie
            if(DSApi.context().getPrincipalName().equals(t.getDsw_resource_res_key())) {
                return t.getProcess() +","+t.getActivityKey();
            }
        }

        return null;
    }
}
