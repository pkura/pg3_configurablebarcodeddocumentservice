package pl.compan.docusafe.core.dockinds.process;
import org.apache.commons.lang.StringUtils;
import org.simpleframework.xml.*;
import org.simpleframework.xml.core.Persister;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Micha� Sankowski
 */
public class ProcessDefinitionXMLParser {

	private final static Logger log = LoggerFactory.getLogger(ProcessDefinitionXMLParser.class);

	public static ProcessManager parse (String xml) throws EdmException{
		try {
            ProcessMap pm = new Persister().read(ProcessMap.class, xml);
			return new ProcessManager(pm.toMap());
		} catch (Exception ex) {
			throw new EdmException(ex);
		}
	}

	@Root(name="processes")
	static class ProcessMap{

        @ElementList(inline=true, entry="spring-process-definition", required = false, empty=false)
        Collection<String> springBeans;
		
		@ElementList(inline=true, entry="process", required = false, empty=false)
		Collection<AnnotatedProcessBean> map;

		Map<String, ProcessDefinition> toMap() throws Exception{
			Map<String, ProcessDefinition> ret = new HashMap<String, ProcessDefinition>();
            for(String beanName: springBeans){
                ProcessDefinition pd = Docusafe.getBean(beanName, ProcessDefinition.class);
                ret.put(pd.getName(), pd);
            }
			for(AnnotatedProcessBean ap: map){
				ret.put(ap.getName(), new BusinessProcessSkeleton(ap));
				log.info(ap.getName() + " - " + ap);
			}
			return ret;
		}
    }

    static ProcessDefinition.CreationType creationTypeFromString(String s) {
        if (StringUtils.isBlank(s)) {
            return ProcessDefinition.CreationType.INTERNAL;
        } else {
            return ProcessDefinition.CreationType.valueOf(s.toUpperCase().replace("-","_"));
        }
    }

	private static class BusinessProcessSkeleton implements ProcessDefinition{

		private final String name;
        private final String title;
        private final ProcessView view;
        private final ProcessLogic logic;
        private final ProcessLocator locator;
        private final CreationType creationType;

        BusinessProcessSkeleton(AnnotatedProcessBean bean) throws Exception {
            this.name = bean.getName();
            this.view = bean.createView();
            this.logic = bean.createLogic();
            this.locator = bean.createLocator();
            this.creationType = creationTypeFromString(bean.creationType);
            this.title = bean.getTitle();
        }

		public String getName() {
			return name;
		}

		public ProcessView getView() {
			return view;
		}

		public ProcessLogic getLogic() {
			return logic;
		}

		public ProcessLocator getLocator() {
			return locator;
		}

        public CreationType getCreationType() {
            return creationType;
        }

        @Override
        public String getTitle() {
            return title;
        }
    }

	@Root(name="process")
	static class AnnotatedProcessBean {

		@Attribute(name="name")
		String name;

		@Element(name="view")
		String viewClassName;

		@Element(name="logic")
		String logicClassName;

		@Element(name="locator")
		String locatorClassName;

        @Attribute(name="creation",required = false)
        String creationType;

        @Attribute(name="title",required = false)
        String title;

        @ElementMap(name = "params",entry="param", key="name", attribute=true, inline=false, required = false)
        Map<String, String> params;

        InstantiationParameters parameters;

		String getName(){
			return name;
		}

        InstantiationParameters getParams(){
            if(parameters == null){
                params.put(InstantiationParameters.Param.PROCESS_NAME.name(), name);
                parameters = new InstantiationParameters(params);
            }
            return parameters;
        }

		ProcessView createView() throws Exception{
            Class clazz = Class.forName(viewClassName);
            try {
                return (ProcessView) clazz.getConstructor(InstantiationParameters.class).newInstance(getParams());
            } catch (NoSuchMethodException ex) {
                if(params != null && !params.isEmpty()){
                    log.info(viewClassName + " nie przyjmuje parametr�w w konstruktorze, obiekt b�dzie utworzony za pomoc� konstruktora bezparametrowego");
                }
            }
			return (ProcessView) clazz.newInstance();
		}

		ProcessLogic createLogic() throws Exception{
            Class clazz = Class.forName(logicClassName);
            try {
                return (ProcessLogic) clazz.getConstructor(InstantiationParameters.class).newInstance(getParams());
            } catch (NoSuchMethodException ex) {
                if(params != null && !params.isEmpty()){
                    log.info(logicClassName + " nie przyjmuje parametr�w w konstruktorze, obiekt b�dzie utworzony za pomoc� konstruktora bezparametrowego");
                }
            }
			return (ProcessLogic) clazz.newInstance();
		}

		ProcessLocator createLocator() throws Exception {
            Class clazz = Class.forName(locatorClassName);
            try {
                return (ProcessLocator) clazz.getConstructor(InstantiationParameters.class).newInstance(getParams());
            } catch (NoSuchMethodException ex) {
                if(params != null && !params.isEmpty()){
                    log.info(locatorClassName + " nie przyjmuje parametr�w w konstruktorze, obiekt b�dzie utworzony za pomoc� konstruktora bezparametrowego");
                }
            }
			return (ProcessLocator) clazz.newInstance();
		}

        public String getTitle() {
            return title;
        }
    }
}
