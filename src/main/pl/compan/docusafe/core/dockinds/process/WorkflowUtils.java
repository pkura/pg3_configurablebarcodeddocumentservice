package pl.compan.docusafe.core.dockinds.process;

import static com.google.common.base.Preconditions.*;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;

/**
 * Klasa narz�dziowa do czynno�ci
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class WorkflowUtils {

    /**
     * Zwraca informacj� czy dany proces mo�e utworzy� proces do wiadomo�ci,
     * wszelkie zmiany regu� najlepiej dodawa� w tej metodzie
     * @param activityId
     * @return true je�li na podstawie czynno�ci mo�na utworzy� proces do wiadomo�ci
     */
    public static boolean canSpawnReadOnlyProcess(String activityId, DocumentKind kind) throws EdmException {
        checkNotNull(activityId, "activityId cannot be null");

        ProcessDefinition pd = kind.getDockindInfo().getProcessesDeclarations().getProcessOrNull(Jbpm4Constants.READ_ONLY_PROCESS_NAME);
        if(pd == null){//brak odpowiedniego procesu
            return false;
        } else if(WorkflowFactory.getInstance().getProcessName(activityId).equals(Jbpm4Constants.READ_ONLY_PROCESS_NAME)){
            return false;
        }
        return true;
    }

    /**
     * Zwraca informacj� czy dany proces mo�e by� przekazany do koordynatora
     * @param activityId
     * @return true je�li mo�e
     * @throws EdmException
     */
    public static boolean canAssignToCoordinator(String activityId) throws EdmException {
        checkNotNull(activityId, "activityId cannot be null");
        String processName = WorkflowFactory.getInstance().getProcessName(activityId);
        return !(processName.equals(Jbpm4Constants.ZAMOWIENIE) || AvailabilityManager.isAvailable("ifpan")
                    || processName.equals(Jbpm4Constants.OFERTA))
                && !processName.equals(Jbpm4Constants.READ_ONLY_PROCESS_NAME);
    }

    private WorkflowUtils(){}
}
