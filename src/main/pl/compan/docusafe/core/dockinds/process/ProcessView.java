package pl.compan.docusafe.core.dockinds.process;

import java.util.Collection;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.web.common.RenderBean;

/**
 * Klasa wyświetlająca użytkownikowi stan danego procesu.
 * @author Michał Sankowski
 */
public interface ProcessView {

	/**
	 * Przygotowuje RenderBean reprezentujący widok danego procesu. Metoda ta korzysta z dostępu do bazy danych.
	 * @param pi - instancja procesu
	 * @return
	 */
	public RenderBean render(ProcessInstance pi) throws EdmException;
	public RenderBean render(ProcessInstance pi, ProcessActionContext context)throws EdmException;

	public Collection<? extends RenderBean> render(Collection<? extends ProcessInstance> pi)throws EdmException;
	public Collection<? extends RenderBean> render(Collection<? extends ProcessInstance> pi, ProcessActionContext context)throws EdmException;
}
