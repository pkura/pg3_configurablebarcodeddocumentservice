package pl.compan.docusafe.core.dockinds.process;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Bean okre�laj�cy kontekst, w kt�rym wykonywana jest dana akcja lub wy�wietlany jest proces/procesy
 * @author Micha� Sankowski
 */
public class ProcessActionContext {
	private final static Logger LOG = LoggerFactory.getLogger(ProcessActionContext.class);

	public final static ProcessActionContext ARCHIVE_VIEW       = new ProcessActionContext(ActionType.ARCHIVE_VIEW);
	public final static ProcessActionContext VIEW               = new ProcessActionContext(ActionType.USER_VIEW);
    public final static ProcessActionContext SUMMARY            = new ProcessActionContext(ActionType.SUMMARY_VIEW);
	public final static ProcessActionContext PROCESS_ADMIN_VIEW = new ProcessActionContext(ActionType.ADMIN_VIEW);

	private final ActionType actionType;

	ProcessActionContext(ActionType actionType){
		this.actionType = actionType;
	}

	public ActionType getActionType() {
		return actionType;
	}

	/**
	 * Rodzaje akcji wsp�lne dla wszystkich proces�w
	 */
	public enum ActionType{
        /** Podgl�d pisma przez administratora przez panel workflow */
		ADMIN_VIEW,
        /** Widok w kancelarii */
		USER_VIEW,
		/** Widok w archiwum*/
		ARCHIVE_VIEW,
        /** Widok w podsumowaniu */
        SUMMARY_VIEW,

        /** czynno�ci dla procesu */
        /** wykonana przez u�ytkownika */
		SPECIFIC_ACTION,
        /** wykonana przez administratora przez panel workflow */
		ADMIN_SPECIFIC_ACTION;
	}

	public static ProcessAction adminAction(String name){
		return ProcessAction.adminSpecific(name);
	}

	public static ProcessAction action(String name){
		return ProcessAction.specific(name);
	}
}


