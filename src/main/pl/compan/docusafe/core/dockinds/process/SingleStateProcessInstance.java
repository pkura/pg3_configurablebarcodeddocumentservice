package pl.compan.docusafe.core.dockinds.process;

/**
 * Proces z pojedynczym stanem
 */
public interface SingleStateProcessInstance extends ProcessInstance {
    /**
     * Zwraca nazw� stanu, w kt�rym znajduje si� process
     * @return
     */
    String getSingleState();
}
