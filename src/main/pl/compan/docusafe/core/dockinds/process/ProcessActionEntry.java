package pl.compan.docusafe.core.dockinds.process;

import javax.persistence.*;
import java.util.Date;

/**
 * Zapisana w bazie danych czynno�� wykonana przez u�ytkownika
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
@Entity
@Table(name = "ds_process_action_entry")
public class ProcessActionEntry {

    @Id
    @SequenceGenerator(name = "ds_process_action_entry_seq", sequenceName = "ds_process_action_entry_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ds_process_action_entry_seq")
    private Long id;

    /** U�ytkownik wykonuj�cy czynno�� */
    @Column(name = "username", nullable = false, length = 62)
    private String username;

    /** Czas wykonania czynno�ci */
    @Column(name = "date_", nullable = false)
    private Date date;

    /** Id powi�zanego dokumentu */
    @Column(name="document_id")
    private Long documentId;

    /** Id procesu (ProcessInstance#getId().toString()) */
    @Column(name="process_id", length = 255)
    private String processId;

    /** Nazwa procesu (DocuSafe'owa) */
    @Column(name="process_name", length = 255)
    private String processName;

    /** Nazwa czynno�ci - w jbpm4 to nazwa transition */
    @Column(name="action_name", length = 255)
    private String actionName;

    @Deprecated //Dla Hibernate
    public ProcessActionEntry(){}

    public ProcessActionEntry(String actionName, Long documentId, ProcessInstance processInstance, String username) {
        this.actionName = actionName;
        this.documentId = documentId;
        this.processId = processInstance.getId().toString();
        this.processName = processInstance.getProcessName();
        this.username = username;
        this.date = new Date();
    }

    public Long getId() {
        return id;
    }

    public Long getDocumentId() {
        return documentId;
    }
}
