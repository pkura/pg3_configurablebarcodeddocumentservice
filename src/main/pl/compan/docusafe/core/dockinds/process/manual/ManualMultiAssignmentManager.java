package pl.compan.docusafe.core.dockinds.process.manual;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.process.ProcessManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.ActivitiDecretationManager;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import static pl.compan.docusafe.web.office.common.ManualMultiAssignmentTabAction.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Klasa przeznaczona do wykonwywania r�cznej dekretacji nie tylko w akcjach
 * kontroler�w ale r�wnie� na interfejsie DS API ( REST, RPC, SOAP ),
 * oraz na zbiorczej dekretacji r�cznej.
 *
 * Logika r�cznej dekretacji zosta�a wyci�gni�ta, aby mo�na by�o jej u�ywa�
 * w innych miejscach, z klasy:
 *
 * @see pl.compan.docusafe.web.office.common.ManualMultiAssignmentTabAction
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class ManualMultiAssignmentManager {

    private static final Logger log = LoggerFactory.getLogger(ManualMultiAssignmentManager.class);
    private final static StringManager SM = GlobalPreferences.loadPropertiesFile(ManualMultiAssignmentManager.class.getPackage().getName(), null);

    private String[] targets;
    /** @var typ dekretacji */
    private String[] kinds;
    /** @var cel dekretacji */
    private String[] targetsKind;
    /** @var referent */
    private String[] clerks;
    /** @var temat dekretacji */
    private String[] objectives;
    /** @var deadline */
    private String[] deadlines;

    private boolean lastone;

    private Long documentId;

    private String activityIds[];
    private String activity;

    private Boolean startNewProcess;
    private String clerk;

    public ManualMultiAssignmentManager(){
    }

    /**
     * Metoda wykonuje dekretacj� r�czn�. Przyk�ad konifguracji obiektu
     * Manual assign params ManualMultiAssignmentManager{
        <b>targets=[eoda],
     kinds=[EXECUTE],
     targetsKind=[U],
     clerks=null,
     objectives=null,
     deadlines=null,
     lastone=false,
     documentId=418,
     activityIds=null,
     activity='jbpm4,1160017',
     startNewProcess=null,
     clerk='null'}

     </b>
     Konfigurcja obiektu dla wielu elementu do dekretacji

      <b>
         targets=[eoda, eodc],
         kinds=[EXECUTE, EXECUTE],
         targetsKind=[U, U],
         clerks=null,
         objectives=null,
         deadlines=null,
         lastone=false,
         documentId=417,
         activityIds=null,
         activity='jbpm4,1160007',
         startNewProcess=null,
         clerk='null'}

     </b>

     Przyk�ad konfiguracji obiektu dla dekretacji dzia�u i u�ytkownika:

     <b> targets=[99133E2557187E76140ED63E3775E96915F, eodd],
     kinds=[EXECUTE, EXECUTE], targetsKind=[D, U],
     clerks=null,
     objectives=null,
     deadlines=null, lastone=false,
     documentId=417,
     activityIds=null,
     activity='jbpm4,1160007',
     startNewProcess=null, clerk='null'}
     </b>

     Przyk�ad do wiadomo�ci na dzia�:

     <b>targets=[99133E2557187E76140ED63E3775E96915F],
     kinds=[CC],
     targetsKind=[D],
     clerks=null,
     objectives=null,
     deadlines=null,
     lastone=false,
     documentId=408,
     activityIds=null,
     activity='jbpm4,1100017',
     startNewProcess=null,
     clerk='null'}
     </b>
     * @throws Exception
     */
    public void manualAssign() throws Exception
    {
        log.error("Manual assign params {}", toString());
        String[] guids = new String[targets.length];
        List<Map<String,Object>> paramsList = Lists.newArrayList();
        int i = initIndex(), k = initIndex();
        int j=0;
        int  sizeUserDekretation  = 0 ;

        if(AvailabilityManager.isAvailable("ams.mma.AddToWatch")){
            addToWatch();
        }

        // Bardzo brzydki fix dla UTP
        // je?li dekretacja jest na division (tj. nie na usera) i jest "do wiadomo?ci" (CC)
        // wtedy dekretuj pokolei na pojedynczych u?ytkownik?w z danego divisionu.
        // efekt koncowy to powiekszenie tablic kinds, targetsKind, itd.
        if(AvailabilityManager.isAvailable("kancelaria.dekretacja.division-to-user-cc-decretation")) {
            log.debug("special cc decretation for i = {}, targets.length = {}", i, targets.length);
            specialCcDecretation(i);
            i = 0; //initIndex();
            log.debug("current i {}",i);
            log.debug("targets s {} targets {}",targets.length,targets);
        }

        int manualProcessCounter = 0;
        for ( ; k < targets.length; k++) {
            if (KIND_EXECUTE.equals(kinds[k]) || KIND_EXECUTE_W.equals(kinds[k])) {
                manualProcessCounter ++;
            }
        }

        Map<String,Object> clerkParams = Maps.newHashMap();
        for ( ; i < targets.length; i++) {
            Map<String,Object> params = Maps.newHashMap();
            //TYP DEKRETACJI
            if(KIND_EXECUTE.equals(kinds[i]))
            { params.put(Jbpm4Constants.KEY_PROCESS_NAME,Jbpm4Constants.MANUAL_PROCESS_NAME);
            }
            else if (KIND_CCK.equals(kinds[i])) {
                params.put(Jbpm4Constants.KEY_PROCESS_NAME,Jbpm4Constants.READ_ONLY_PROCESS_NAME);
                params.put(Jbpm4Constants.KEY_FORK_PUSH,Jbpm4Constants.FORK);

            } else if (KIND_EXECUTE_W.equals(kinds[i])){
                params.put(Jbpm4Constants.KEY_PROCESS_NAME,Jbpm4Constants.MANUAL_PROCESS_NAME);
            }
            else {
                params.put(Jbpm4Constants.KEY_PROCESS_NAME,Jbpm4Constants.READ_ONLY_PROCESS_NAME);
                params.put(Jbpm4Constants.KEY_FORK_PUSH,Jbpm4Constants.FORK);

            }

            // po dodaniu np 3 os�b do forka realizajci idzie do 3 osob ale  pismo pozostaje na liscie zadan urzytkowniak dekretujacego -
            // trzeba zliczy� ilosc dekretacji do user i jako ostatni� z fork/ zmienic na push
            // puki co takie rozwiazanie
            if(AvailabilityManager.isAvailable("mma.LastMultiExecDecretationAsPush") && sizeUserDekretation == 0 )
                sizeUserDekretation = countDecretationToUser(targetsKind);

            //CEL DEKRETACJI
            if(TO_USER.equals(targetsKind[i])) {
                putAsUser(params, clerkParams, i);

                if (AvailabilityManager.isAvailable("mma.makeForkPush") && AvailabilityManager.isAvailable("mma.LastMultiExecDecretationAsPush")){
                    if (KIND_EXECUTE.equals(kinds[i]) || KIND_EXECUTE_W.equals(kinds[i])){
                        if (manualProcessCounter-- == 1 && (startNewProcess == null || startNewProcess == false)) {
                            params.put(Jbpm4Constants.KEY_FORK_PUSH,Jbpm4Constants.PUSH);
                        }
                    }
                }
                else if (AvailabilityManager.isAvailable("mma.LastMultiExecDecretationAsPush") &&  sizeUserDekretation--   == 1)
                    params.put(Jbpm4Constants.KEY_FORK_PUSH,Jbpm4Constants.PUSH);

            } else {
                j = putAsDivision(guids, i, j, params);
            }
            //TEMAT DEKRETACJI
            if(objectives != null && objectives[i] != null && objectives[i].length() > 0) {
                params.put(Jbpm4WorkflowFactory.OBJECTIVE, objectives[i]);
            } else if (kinds[i].contains(KIND_CCK)){
                params.put(Jbpm4WorkflowFactory.OBJECTIVE, kindLang.get(KIND_CCK));
            } else if (kinds[i].contains(KIND_CC)) {
                params.put(Jbpm4WorkflowFactory.OBJECTIVE, kindLang.get(KIND_CC));
            } else if (kinds[i].contains(KIND_EXECUTE_W)) {
                params.put(Jbpm4WorkflowFactory.OBJECTIVE, kindLang.get(KIND_EXECUTE_W));
            } else {
                params.put(Jbpm4WorkflowFactory.OBJECTIVE,kindLang.get(KIND_EXECUTE));
            }

            //DEADLINE
            if(deadlines != null && deadlines[i] != null && deadlines[i].length() > 0) {

            	Date deadline = DateUtils.parseJsDate(deadlines[i]);
            	if (deadline == null){
            		deadline = DateUtils.parseDateAnyFormat(deadlines[i]);
            	}
				
                params.put(Jbpm4WorkflowFactory.DEADLINE_TIME, deadline);
            }

            //REFERENT
            if(clerks != null && clerks.length > 0) {
                params.put(Jbpm4WorkflowFactory.CLERK, clerks[i]);
            }

            //TYP
            if(kinds[i].equals(KIND_CC)){
                params.put(Jbpm4WorkflowFactory.DOCDESCRIPTION,kindLang.get(KIND_CC));
            }
            else if (kinds[i].equals(KIND_CCK)){
                params.put(Jbpm4WorkflowFactory.DOCDESCRIPTION,kindLang.get(KIND_CCK));
            }
            else if (kinds[i].equals(KIND_EXECUTE_W)){
                params.put(Jbpm4WorkflowFactory.DOCDESCRIPTION,kindLang.get(KIND_EXECUTE_W));
            } else {
                params.put(Jbpm4WorkflowFactory.DOCDESCRIPTION,kindLang.get(KIND_EXECUTE));
            }

            //availables
            log.debug("availables kinds[i]", kinds[i]);
            params.put(AvailabilityManager.AVAILABILITY_MAP_PROCESS_VARIABLE, availabilityMaps.get(kinds[i]));

            paramsList.add(params);
        }
        try
        {
            DSApi.context().begin();
            
            if(checkAndAssingActivitiWorkflow(paramsList, documentId, activityIds)){
            	log.info("Zadekretowano proces activiti");
            	boolean commpeted = true;
            	DSApi.context().commit();
            } else {
                OfficeDocument od = null;
                if(activityIds != null)
                {
                    for(String aid : activityIds)
                    {
                        List<Map<String, Object>> paramsListClone = getParamsListClone(paramsList);
                        od = OfficeDocument.find(Jbpm4ProcessLocator.documentIdForActivity(aid.split(",")[1]));
                        for(Map<String,Object> params : paramsListClone)
                        {
                            ProcessManager.empty().assignment(od, aid, startNewProcess, params);
                        }
                        TaskSnapshot.updateByDocumentId(od.getId(), "anyType");
                    }
                }
                else
                {
                    od = OfficeDocument.find(documentId);
                    for(Map<String,Object> params : paramsList)
                    {
                        ProcessManager.empty().assignment(od, activity, startNewProcess, params);
                    }
                    TaskSnapshot.updateByDocumentId(od.getId(), "anyType");
                }
                DSApi.context().commit();

                if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                    if(od != null) {
                        JackrabbitXmlSynchronizer.createEvent(od.getId());
                    }
                }
            }
        }
        catch(Exception e)
        {
            log.error(e.getMessage(), e);
            DSApi.context()._rollback();

            throw new EdmException(SM.getString("PrzekazanoZadania"));
        }
    }

    private boolean checkAndAssingActivitiWorkflow(List<Map<String, Object>> paramsList, Long docId, String[] activityIds) throws DocumentNotFoundException, EdmException
	{
        ArrayList<Long> docIds = new ArrayList<Long>();
    	if(docId == null) {
            for(String aid: activityIds) {
                docIds.add(Jbpm4ProcessLocator.documentIdForActivity(aid.split(",")[1]));
            }
        } else {
            docIds.add(docId);
        }

        boolean is = false;
        for(Long id: docIds) {
            is = is || checkAndAssingActivitiWorkflow(paramsList, id);
        }

        return is;
	}

    private boolean checkAndAssingActivitiWorkflow(List<Map<String, Object>> paramsList, Long documentId) throws EdmException {
        OfficeDocument od = OfficeDocument.findOfficeDocument(documentId, false);

        List<JBPMTaskSnapshot> taskSnapshots = JBPMTaskSnapshot.findByDocumentId(od.getId());
        boolean is = false;
        for (JBPMTaskSnapshot ts : taskSnapshots)
        {
            if ("activiti".equals(ts.getProcess()) && ts.getDsw_resource_res_key().equals(DSApi.context().getDSUser().getName())) {
                ActivitiDecretationManager adm = new ActivitiDecretationManager();
                adm.assignDocument(ts, paramsList, od);
                is = true;
                break;
            }
        }

        return is;
    }

	private int initIndex() {
        if(lastone) {
            return targets.length -1;
        } else {
            return 0;
        }
    }

    /**
     * <p>Dodaje dokumenty do obserwacji metod? <code>DSApi.context().watch(URN.create(document))</code>.</p>
     *
     * <p>Je?li <code>documentId</code> nie jest <code>null</code>, wtedy obserwowany jest tylko dokument z tego id.
     * W przeciwnym razie obserwowane s? dokumenty na podstawie <code>activityIds</code> (dokumenty pobieramy metoda <code>Jbpm4ProcessLocator.documentIdForActivity</code></p>
     *
     * @throws EdmException
     */
    private void addToWatch() throws EdmException {
        if(! AvailabilityManager.isAvailable("addToWatch")) {
            log.warn("[addToWatch] addToWatch is not available, not watching documents");
        } else {
            log.info("[addToWatch] addToWatch is available");

            // gdy documentId nie jest null
            if(documentId != null) {
                log.debug("[addToWatch] documentId = {}", documentId);
                OfficeDocument document = OfficeDocument.find(documentId);

                DSApi.context().watch(URN.create(document));
            }
            // w przeciwnym razie pobieramy dokumenty z activityIds
            else {
                log.debug("[addToWatch] documentId is null, getting documents from activityIds = {}", (Object)activityIds);
                if(activityIds == null) {
                    log.error("[addToWatch] activityIds is null, not watching anything");
                } else {
                    for(String aid: activityIds)
                    {
                        OfficeDocument document = OfficeDocument.find(Jbpm4ProcessLocator.documentIdForActivity(aid.split(",")[1]));
                        log.debug("[addToWatch]     document.id = {}", document.getId());
                        DSApi.context().watch(URN.create(document));
                    }
                }
            }
        }
    }

    private void specialCcDecretation(int i) throws EdmException {
        List<String> targetsList = new ArrayList<String>();
        List<String> kindsList = new ArrayList<String>();
        List<String> objectivesList = new ArrayList<String>();
        List<String> clerksList = new ArrayList<String>();
        List<String> targetsKindList = new ArrayList<String>();
        List<String> deadlinesList = new ArrayList<String>();

        log.debug("[specialCcDecretation] targets = {}", (Object[]) targets);

        for ( ; i < targets.length; i++) {

            log.debug("[specialCcDecretation] kinds[{}] = {}, targets[{}] = {}", i, kinds[i], i, targets[i]);
            if(KIND_CC.equals(kinds[i]) && TO_DIVISION.equals(targetsKind[i])) {
                final List<String> usernames = computeUsernameList(targets[i]);

                log.debug("[specialCcDecretation] usernames = {}", usernames);

                for(String username: usernames) {
                    targetsList.add(username);
                    kindsList.add(kinds == null ? null : kinds[i]);
                    objectivesList.add(objectives == null ? null : objectives[i]);
                    clerksList.add(clerks == null ? null : clerks[i]);
                    targetsKindList.add(TO_USER);
                    deadlinesList.add(deadlines == null ? null : deadlines[i]);
                }

            } else {
                targetsList.add(targets[i]);
                kindsList.add(kinds == null ? null : kinds[i]);
                objectivesList.add(objectives == null ? null : objectives[i]);
                clerksList.add(clerks == null ? null : clerks[i]);
                targetsKindList.add(targetsKind == null ? null : targetsKind[i]);
                deadlinesList.add(deadlines == null ? null : deadlines[i]);
            }

        }

        int s = targetsList.size();
        log.debug("size {}",s);

        targets = targetsList.toArray(new String[targetsList.size()]);
        kinds = kindsList.toArray(new String[kindsList.size()]);
        objectives = objectivesList.toArray(new String[objectivesList.size()]);
        clerks = clerksList.toArray(new String[clerksList.size()]);
        targetsKind = targetsKindList.toArray(new String[targetsKindList.size()]);
        deadlines = deadlinesList.toArray(new String[deadlinesList.size()]);

        log.debug("targets size {} targets {}",targets.length, targets);
    }

    private List<String> computeUsernameList(String divisionGuid) throws EdmException {

        DSDivision division = DSDivision.find(divisionGuid);

        final DSUser[] users = division.getUsers();

        List<String> ret = new ArrayList<String>();

        for(DSUser user: users) {
            ret.add(user.getName());
        }

        return ret;
    }

    private int countDecretationToUser(String[] targetsKind)
    { 	int i = 0;
        for (String kind :targetsKind){
            if(TO_USER.equals(kind))
                i++;
        }
        return i ;
    }

    private int putAsDivision(String[] guids, int i, int j, Map<String, Object> params) throws Exception {
        params.put(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, DSDivision.find(targets[i]).getGuid());
        if(!(DSDivision.find(targets[i]).getUsers().length>0)){
            throw new Exception ("W dziale "+DSDivision.find(targets[i]).getName()+" nie ma u?ytkownik?w - dekretacja niemo?liwa");
        }
        if(AvailabilityManager.isAvailable("blockManualMultiAssigmentToDivision")){
            throw new Exception ("Dekretacja na dzial zostala zablokowana");
        }
        if(AvailabilityManager.isAvailable("manual-multi-assignment.PIG")){
            guids[j] = (String)params.get(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM);
            j++;
        }
        // ustawienie paramsa: "KEY_FORK_PUSH" jedynie dla manuala bo dla read-only jest ustawiany wczesniej
        if (KIND_EXECUTE.equals(kinds[i]) || KIND_EXECUTE_W.equals(kinds[i])) {
            //isStartNewProcess() != null && isStartNewProcess()
            if (Boolean.TRUE.equals(startNewProcess)) {
                params.put(Jbpm4Constants.KEY_FORK_PUSH,Jbpm4Constants.FORK);
            } else {
                params.put(Jbpm4Constants.KEY_FORK_PUSH,Jbpm4Constants.PUSH);
            }
        }
        return j;
    }

    private List<Map<String, Object>> getParamsListClone(List<Map<String, Object>> paramsList) {
        List<Map<String, Object>> paramsListClone = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> params : paramsList) {
            paramsListClone.add(new HashMap<String, Object>(params));
        }
        return paramsListClone;
    }

    private void putAsUser(Map<String, Object> params, Map<String, Object> clerkParams, int i) {
        params.put(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM,targets[i]);

        if (AvailabilityManager.isAvailable("mma.makeForkPush")) {
            if (KIND_EXECUTE.equals(kinds[i]) || KIND_EXECUTE_W.equals(kinds[i])) {
                params.put(Jbpm4Constants.KEY_FORK_PUSH,Jbpm4Constants.FORK);
            }
        } else if(clerkParams != null && (KIND_EXECUTE.equals(kinds[i])) || KIND_EXECUTE_W.equals(kinds[i])) {
            clerkParams.put(Jbpm4Constants.KEY_FORK_PUSH,Jbpm4Constants.FORK);
            params.put(Jbpm4Constants.KEY_FORK_PUSH,Jbpm4Constants.PUSH);
            if(clerks != null)
                clerk = clerks[0];
            if(targets[i].equals(clerk)) {
                clerkParams = null;
            } else {
                clerkParams = params;
            }
        } else if(clerkParams == null && (KIND_EXECUTE.equals(kinds[i])) || KIND_EXECUTE_W.equals(kinds[i]))  {
            params.put(Jbpm4Constants.KEY_FORK_PUSH,Jbpm4Constants.FORK);
        }

    }

    public void setTargets(String[] targets) {
        this.targets = targets;
    }

    public void setKinds(String[] kinds) {
        this.kinds = kinds;
    }

    public void setTargetsKind(String[] targetsKind) {
        this.targetsKind = targetsKind;
    }

    public void setClerks(String[] clerks) {
        this.clerks = clerks;
    }

    public void setObjectives(String[] objectives) {
        this.objectives = objectives;
    }

    public void setDeadlines(String[] deadlines) {
        this.deadlines = deadlines;
    }

    public void setLastone(boolean lastone) {
        this.lastone = lastone;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public void setActivityIds(String[] activityIds) {
        this.activityIds = activityIds;
    }

    public void setActivity(String activity) {
        //z jakich� wzgl�d�w do activiti dodaje si� spacja i kolejne warto�ci
        // ten kod sprawdza to i ewentualnie usuwa niepotrzebne fragmenty
        if(activity != null && activity.indexOf(' ') >= 0){
            //ignoruj ci�g znak�w do pierwszej spacji
            this.activity = activity.substring(0, activity.indexOf(' ') - 1);
        } else {
            this.activity = activity;
        }
    }

    public void setStartNewProcess(Boolean startNewProcess) {
        this.startNewProcess = startNewProcess;
    }

    public void setClerk(String clerk) {
        this.clerk = clerk;
    }

    @Override
    public String toString() {
        return "ManualMultiAssignmentManager{" +
                "targets=" + Arrays.toString(targets) +
                ", kinds=" + Arrays.toString(kinds) +
                ", targetsKind=" + Arrays.toString(targetsKind) +
                ", clerks=" + Arrays.toString(clerks) +
                ", objectives=" + Arrays.toString(objectives) +
                ", deadlines=" + Arrays.toString(deadlines) +
                ", lastone=" + lastone +
                ", documentId=" + documentId +
                ", activityIds=" + Arrays.toString(activityIds) +
                ", activity='" + activity + '\'' +
                ", startNewProcess=" + startNewProcess +
                ", clerk='" + clerk + '\'' +
                '}';
    }
}
