package pl.compan.docusafe.core.dockinds.process;
import java.util.*;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Objects;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.process.DocuSafeProcessDefinition;
import pl.compan.docusafe.process.ProcessEngineType;
import pl.compan.docusafe.process.ProcessForDocument;
import pl.compan.docusafe.process.form.Button;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.ExtendedRenderBean;
import pl.compan.docusafe.web.common.RenderBean;

/**
 * Klasa narz�dziowa do proces�w
 * @author Micha� Sankowski
 */
public class ProcessUtils {

	private final static Logger log = LoggerFactory.getLogger(ProcessUtils.class);

	public static Collection<RenderBean> renderAllProcesses(Document document, ProcessActionContext context) throws EdmException{
		LinkedList<RenderBean> ret = new LinkedList<RenderBean>();
		for(ProcessDefinition pd: document.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcessDefinitions()){
			log.info("=== Process definition:" + pd.getName() + " === ");
			ret.addAll(pd.getView().render(pd.getLocator().lookupForDocument(document), context));
		}
		return ret;
	}

    public static Collection<RenderBean> renderProcessesForActivityId(Document document, String activityId, ProcessActionContext context) throws EdmException{
        log.info("=== renderProcessesForActivityId:" + activityId + " === ");
		LinkedList<RenderBean> ret = new LinkedList<RenderBean>();
		for(ProcessDefinition pd: document.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcessDefinitions()){
			ProcessInstance pi = pd.getLocator().lookupForTaskId(activityId);//TODO zoptymalizowa� i poprawi�
            if(pi != null && pi.getProcessName().equals(pd.getName())){
                return Collections.singletonList(pd.getView().render(pi));
            }
		}
		return ret;
	}


    /**
     * Zwraca list� przycisk�w w procesie dokumentu
     * @param document
     * @param context
     * @return
     * @throws EdmException
     */
    public static List<Button> getProcessActions(Document document, ProcessActionContext context) throws EdmException {
        if(!JBPMTaskSnapshot.isOnLoggedTaskList(document))
            throw new EdmException("Dokument nie znajduje si� na twojej li�cie zada�");
        String activitiId = getActivitiByDocumentId(document);
        if(activitiId == null)
            throw new EdmException("Brak procesu dla tego dokumentu");
        Collection<RenderBean> processRenderBeans = ProcessUtils.renderProcessesForActivityId(document, activitiId, context);
        List<Button> buttons = Lists.newArrayList();
        for(RenderBean rb : processRenderBeans){
            Collection<Objects> objects = (Collection<Objects>) ((ExtendedRenderBean) rb).get("activities");
            buttons.addAll(Arrays.asList(objects.toArray(new Button[0])));
        }

        return buttons;
    }

    /**
     * Zwraca activiti id danego procesu dokumentu
     * @param document
     * @return
     */
    private static String getActivitiByDocumentId(Document document) throws EdmException {
        List<JBPMTaskSnapshot> tasks = JBPMTaskSnapshot.findByDocumentIdAndResourceKey(document.getId(), DSApi.context().getPrincipalName());
        for(JBPMTaskSnapshot task : tasks){
            return task.getProcess() + ',' + task.getActivityKey();
        }
        return null;
    }

    public static void validateOnList(OfficeDocument document) throws EdmException {
        List<JBPMTaskSnapshot> taskSnapshots = JBPMTaskSnapshot.findByDocumentId(document.getId());
        for (JBPMTaskSnapshot ts : taskSnapshots)
        {
            boolean isOnLoggedUserTaskList = ts.getDsw_resource_res_key().equals(DSApi.context().getPrincipalName());
            if(!isOnLoggedUserTaskList){
                throw new EdmException("Dokument nie znajduje si� na twojej li�cie zada�");
            }
        }
    }

    public static void executeProcessAction(OfficeDocument document,String processAction) throws EdmException {
        List<JBPMTaskSnapshot> taskSnapshots = JBPMTaskSnapshot.findByDocumentId(document.getId());
        for (JBPMTaskSnapshot ts : taskSnapshots)
        {
            boolean isOnLoggedUserTaskList = ts.getDsw_resource_res_key().equals(DSApi.context().getPrincipalName());
            if(!isOnLoggedUserTaskList){
                throw new EdmException("Dokument nie znajduje si� na twojej li�cie zada�");
            }

            if ("activiti".equals(ts.getProcess()) )
            {
                executeAction(document, ts, processAction);
                break;
            } else {
                executeActionJbpm(document, ts, processAction);
                break;
            }
        }
    }

    private static void executeActionJbpm(OfficeDocument doc, JBPMTaskSnapshot ts, String processAction) throws EdmException {
        String activitiId = ts.getProcess() + "," + ts.getActivityKey();
        Collection<ProcessDefinition> pdefs = doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcessDefinitions();
        for (ProcessDefinition pd : pdefs)
        {
            Collection<? extends ProcessInstance> processInstances = pd.getLocator().lookupForDocument(doc);
            for(ProcessInstance pi : processInstances){
                pd.getLogic().process(pi, ProcessActionContext.action(processAction).document(doc));
                break;
            }
            break;
        }
    }

    /**
     * Metoda  pzygotowuje instancje procesu na kt�rej wykoanna bedzie akcja
     * @param doc
     * @param ts
     * @throws EdmException
     */
    private static void executeAction(OfficeDocument doc, JBPMTaskSnapshot ts, String processAction) throws EdmException
    {

        ProcessInstance pi = null;
        String activitiId = ts.getProcess() + "," + ts.getActivityKey();
        List<String> processids = ProcessForDocument.getProcessIdsForDocument(doc.getId(), ProcessEngineType.ACTIVITI);
        Collection<ProcessDefinition> pdefs = doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcessDefinitions();
        for (ProcessDefinition pd : pdefs)
        {
        	if (pd instanceof DocuSafeProcessDefinition){
	            if (ProcessEngineType.ACTIVITI.getKey().equals(((DocuSafeProcessDefinition) pd).getProcessEngineType().getKey()))
	            {
	                pi = pd.getLocator().lookupForId(processids.get(0));
	                executeProcessAction(processAction, activitiId, doc, pi, pd);
	            }
        	}
        }
    }

    /**
     * Metoda wykonuje podana akcj� na procesie dla Activiti
     * @param processAction
     * @param activityId
     * @param doc
     * @param pi
     * @param pd
     * @throws EdmException
     */
    private static void executeProcessAction(String processAction, String activityId, OfficeDocument doc, ProcessInstance pi, ProcessDefinition pd)
            throws EdmException
    {
        pd.getLogic().process(pi, ProcessActionContext.action(processAction).document(doc).stored(false).activityId(activityId));
    }
}
