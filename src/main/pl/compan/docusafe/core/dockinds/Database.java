package pl.compan.docusafe.core.dockinds;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public interface Database {

    static class Name {
        public final String name;

        public Name(String name) {
            this.name = name;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Name))
                return name.equals(obj);
            Name name = (Name) obj;
            return this.name.equalsIgnoreCase(name.name);
        }

        @Override
        public String toString() {
            return getName();
        }

        public String getName() {
            return name;
        }
    }

    public static class Table extends Name {
        public Table(String name) {
            super(name);
        }
    }

    public static class Column extends Name {
        public Column(String name) {
            super(name);
        }
    }
}
