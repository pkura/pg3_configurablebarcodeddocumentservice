package pl.compan.docusafe.core.dockinds;

import pl.compan.docusafe.core.FormQuery;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.expression.Expression;

import java.util.Map;
/* User: Administrator, Date: 2007-03-07 12:40:17 */

/**
 * Klasa wspomagaj�ca wyszukiwanie dokument�w z danym rodzajem dokumentu (zobacz
 * {@link DockindQuery}). Metody tej klasy b�d� odpowiednio wywoy�ywane dla ka�dego
 * warunku WHERE dodanego do zapytania.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class DockindQueryVisitor extends FormQuery.QueryVisitorAdapter
{
    private Map<String, TableAlias> tables;
    private WhereClause where;

    private Junction OR;

    public DockindQueryVisitor(Map<String,TableAlias> tables, WhereClause where)
    {
        this.tables = tables;
        this.where = where;
       // for (TableAlias alias : tables.values())
       // {
       // }
    }

    private class Attribute
    {
        String tableName;
        String columnName;

        public Attribute(String attributeString) throws EdmException
        {
            String[] parts = attributeString.split("\\s*"+DockindQuery.ATTRIBUTE_SEPARATOR+"\\s*");
            if (parts.length != 2)
            {
               // for (String x : parts)
                throw new EdmException("Napotkano nieprawid�ow� reprezentacj� atrybutu tabeli: "+attributeString+" "+parts.length);
            }
            tableName = parts[0];
            columnName = parts[1];
        }
    }

    public void visitEq(FormQuery.Eq expr) throws EdmException
    {
        Attribute attr = new Attribute(expr.getAttribute());

        if (OR != null)
        {
            OR.add(Expression.eq(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue()));
        }
        else
        {
            where.add(Expression.eq(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue()));
        }

    }
    
    public void visitIn(FormQuery.In expr) throws EdmException
    {
        Attribute attr = new Attribute(expr.getAttribute());

        if (OR != null)
        {
            OR.add(Expression.in(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue()));
        }
        else
        {
            where.add(Expression.in(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue()));
        }
 
    }
    
    public void visitNotIn(FormQuery.NotIn expr) throws EdmException
    {
        Attribute attr = new Attribute(expr.getAttribute());

        if (OR != null)
        {
            OR.add(Expression.notIn(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue()));
        }
        else
        {
            where.add(Expression.notIn(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue()));
        }
 
    }

    public void visitGe(FormQuery.Ge expr) throws EdmException
    {
        Attribute attr = new Attribute(expr.getAttribute());

        if (OR != null)
        {
            OR.add(Expression.ge(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue()));
        }
        else
        {
            where.add(Expression.ge(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue()));
        }
    }

    public void visitLe(FormQuery.Le expr) throws EdmException
    {
        Attribute attr = new Attribute(expr.getAttribute());

        if (OR != null)
        {
            OR.add(Expression.le(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue()));
        }
        else
        {
            where.add(Expression.le(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue()));
        }
    }

    public void visitLike(FormQuery.Like expr) throws EdmException 
    {
        //nie ma sensu dodawa� "%" do ko�c�wki wyszukiwania - lepiej doda� je do tworzonego obiektu Like
        Attribute attr = new Attribute(expr.getAttribute());
        if (OR != null)
        {
            OR.add(Expression.like(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue(),expr.isIgnoreCase()));
        }
        else
        {
            where.add(Expression.like(tables.get(attr.tableName).attribute(attr.columnName), expr.getValue(),expr.isIgnoreCase()));
        }
    }

    public void startDisjunction() throws EdmException
    {
        if (OR != null) throw new EdmException("Zagnie�d�one OR jest niedozwolone");
        OR = Expression.disjunction();
    }

    public void endDisjunction() throws EdmException
    {
        where.add(OR);
        OR = null;
    }
}
