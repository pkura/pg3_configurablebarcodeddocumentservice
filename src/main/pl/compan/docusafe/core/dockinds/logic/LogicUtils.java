package pl.compan.docusafe.core.dockinds.logic;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.field.DocumentPersonField;
import pl.compan.docusafe.core.office.DSPermission;

import java.math.BigInteger;
import java.util.*;
import java.util.regex.Pattern;

import com.google.common.collect.Lists;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class LogicUtils {

    /**
     * <i>[Metoda projektowana pod k�tem wykorzystania w beforeSetWithHistory w logice dokumentu]</i><br></br>
     * Wstawia do mapy nowych warto�ci aktualizowanych s�ownik�w pozosta�e z fields managera dla wskazanego s�ownika.
     */
    public static void putOldMultipleDictionaryValues(Map<String, Object> mDict, FieldsManager oldFm, String dictionaryCn) throws EdmException {
        List<Integer> mDictIds = getDicIds(mDict, dictionaryCn);
        putOldMultipleDictionaryValues(mDict, oldFm, dictionaryCn, mDictIds);
    }
    /**
     * <i>[Metoda projektowana pod k�tem wykorzystania w beforeSetWithHistory w logice dokumentu]</i><br></br>
     * Wstawia do mapy nowych warto�ci aktualizowanych s�ownik�w pozosta�e z fields managera dla wskazanego s�ownika.
     */
    public static void putOldMultipleDictionaryValues(Map<String, Object> mDict, FieldsManager oldFm, String targetDictionaryCn, String ... dictionaryCns) throws EdmException {
        List<Integer> allIds = new ArrayList<Integer>();
        for (String dictCn : dictionaryCns){
            List<Integer> mDictIds = getDicIds(mDict, dictCn);
            allIds.addAll(mDictIds);
        }
        putOldMultipleDictionaryValues(mDict, oldFm, targetDictionaryCn, allIds);
    }

    public static void clearMultipleDictionaryValues(Map<String,Object> mDict, FieldsManager oldFm, String dictionaryCn) throws EdmException {
        List<Map<String, Object>> oldValuesWithDicPrefix = (List<Map<String, Object>>) oldFm.getValue(dictionaryCn);
        String dicPrefix = dictionaryCn + "_";

//        int index = mDictIds.size();
//        for (Map<String, Object> oldVal : oldValuesWithDicPrefix) {
//
//            Object idO = oldVal.get(dicPrefix + "ID");
//            Integer id = getId(idO);
//
//            if (!mDictIds.contains(id)) {
//                index++;
//                String newDicKey = dictionaryCn + "_" + index;
//
//                Map<String, Object> temp = new HashMap<String, Object>();
//                for (Map.Entry<String,Object> value : oldVal.entrySet()){
//                    if (value.getKey().startsWith(dicPrefix)){ // w zasadzie wykluczenie tylko kluczy "ID", pozostaj� SLOWNIK_JEDEN_CN_POLE_1_CN
//                        Field.Type type = getFieldDataType (value.getValue());
//                        if (type!=null){
//                            String key = value.getKey().substring(dicPrefix.length());
//                            temp.put(key, new FieldData(type,value.getValue()));
//                        }
//                    }
//                }
//                mDict.put(newDicKey, temp);
//            }
//        }
    }

    /**
     * <i>[Metoda projektowana pod k�tem wykorzystania w beforeSetWithHistory w logice dokumentu]</i><br></br>
     * <b>[UWAGA! Problem ze s�ownikami, kt�re stanowi� przedrostek nazwy innych s�ownik�w]</b><br></br>
     * Wstawia do mapy nowych warto�ci aktualizowanych s�ownik�w pozosta�e z fields managera dla wskazanego s�ownika. Pomijane s� te, kt�re znajduj� sie na przekazanej li�cie.
     *
     * @param mDict wszystkie warto�ci aktualizowanych s�ownik�w (modyfikowany argument). Mapa typu: {@code SLOWNIK_JEDEN_CN_1 {ID, POLE_1, ..}, SLOWNIK_JEDEN_CN_2 {ID, ..} , SLOWNIK_DWA_CN_1 {ID, ..}}
     * @param oldFm
     * @param dictionaryCn Cn s�ownika wielowarto�ciowego
     * @param mDictIds Id-ki pomijanych pozycji. Kt�re ju� znajduja si� w {@code mDict} lub kt�re zosta�y usuni�te.
     * @throws EdmException
     */
    public static void putOldMultipleDictionaryValues(Map<String, Object> mDict, FieldsManager oldFm, String dictionaryCn, List<Integer> mDictIds) throws EdmException {
        List<Map<String, Object>> oldValuesWithDicPrefix = (List<Map<String, Object>>) oldFm.getValue(dictionaryCn);
        String dicPrefix = dictionaryCn +    "_";

        int index = mDictIds.size();
        for (Map<String, Object> oldVal : oldValuesWithDicPrefix) {

            Object idO = oldVal.get(dicPrefix + "ID");
            Integer id = getId(idO);

            if (!mDictIds.contains(id)) {
                index++;
                String newDicKey = dictionaryCn + "_" + index;

                Map<String, Object> temp = new HashMap<String, Object>();
                for (Map.Entry<String,Object> value : oldVal.entrySet()){
                    if (value.getKey().startsWith(dicPrefix)){ // w zasadzie wykluczenie tylko kluczy "ID", pozostaj� SLOWNIK_JEDEN_CN_POLE_1_CN
                        Field.Type type = getFieldDataType (value.getValue());
                        if (type!=null){
                            String key = value.getKey().substring(dicPrefix.length());
                            temp.put(key, new FieldData(type,value.getValue()));
                        }
                    }
                }
                mDict.put(newDicKey, temp);
            }
        }
    }

    public static void moveAllValuesToOtherMultipleDict (Map<String, Object> mDict, FieldsManager oldFm, String dictionaryCnTo, String dictionaryCnFrom) throws EdmException {
        Pattern dicToPattern = Pattern.compile(dictionaryCnTo+"_\\d+$");
        Pattern dicFromPattern = Pattern.compile(dictionaryCnFrom+"_\\d+$");

        int lastId = 0;
        for (String key : mDict.keySet()){
            if (dicToPattern.matcher(key).find()){
                String idKeyString = key.substring(key.lastIndexOf('_')+1);
                int id = Integer.parseInt(idKeyString);
                if (lastId < id)
                    lastId = id;
            }
        }

        //pozycje do przeniesienia
        Map<String, Object> movingValues = new HashMap<String, Object>();
        for (Map.Entry<String, Object> dictRow : mDict.entrySet()){
            if (dicFromPattern.matcher(dictRow.getKey()).find()){
                movingValues.put(dictRow.getKey(), dictRow.getValue());
            }
        }

        //usuniecie z from
        for (Map.Entry<String, Object> mv : movingValues.entrySet()) {
            mDict.remove(mv.getKey());
        }

        //dodanie do to
        for (Map.Entry<String, Object> mv : movingValues.entrySet()) {
            ++lastId;
            mDict.put(dictionaryCnTo+'_'+lastId, mv.getValue());
        }
    }

    /** Zamienia id na integer. Dla typ�w: {@code BigInteger, String} wy�uskiwana jest warto�� lub obiekt jest parsowany, wpp. cast na Integer*/
    public static Integer getId(Object id) {
        if (id instanceof BigInteger)
            return((BigInteger) id).intValue();
        if (id instanceof String)
            return Integer.parseInt((String)id);
        return (Integer)id;
    }

    /**
     * Z mapy warto�ci s�ownik�w wyci�gane s� id ({@code "ID"} ) wskazanego s�ownika
     * @param mDict Mapa typu: {@code SLOWNIK_JEDEN_CN_1 {ID, POLE_1, ..}, SLOWNIK_JEDEN_CN_2 {ID, ..} , SLOWNIK_DWA_CN_1 {ID, ..}}
     * @param dictionaryCn
     * @return
     */
    private static List<Integer> getDicIds (Map<String,Object> mDict, String dictionaryCn){
        List<Integer> dictIdsList = new ArrayList<Integer>();

        Pattern pattern = Pattern.compile('('+dictionaryCn+"$)|("+dictionaryCn+"_\\d+$)");
        for (Map.Entry<String,Object> mDictEntryObj : mDict.entrySet()) {
            if(pattern.matcher(mDictEntryObj.getKey()).find()){
                Map<String, FieldData> values = (Map<String, FieldData>) mDictEntryObj.getValue();
                FieldData fieldDataId = values != null ? values.get("ID") : null;
                if (fieldDataId!=null){
                    String data = fieldDataId.getStringData();
                    Integer id = StringUtils.isNotBlank(data) ? Integer.parseInt(data) : null;

                    if(id!=null)
                        dictIdsList.add(id);
                }
            }
        }
        return dictIdsList;
    }

    private static Field.Type getFieldDataType(Object value) {
        //LINK, DATE, STRING, INTEGER, LONG, BOOLEAN, ENUM, TIMESTAMP, LIST_VALUE, ENUM_AUTOCOMPLETE, MONEY, ENUM_MULTIPLE, DICTIONARY, TEXTAREA, ATTACHMENT, HTML, HTML_EDITOR, BUTTON
        if (value instanceof Integer || value instanceof BigInteger)
            return Field.Type.INTEGER;
        if (value instanceof Long)
            return Field.Type.LONG;
        if (value instanceof Boolean)
            return Field.Type.BOOLEAN;
        if (value instanceof String)
            return Field.Type.STRING;
        if (value instanceof Double)
            return Field.Type.MONEY;
        if (value instanceof EnumValues)
            return Field.Type.ENUM;
        return null;
    }
    
    /**
     * Ustawia uprawnienia na slownikach w zale�no�ci od uprawnien posiadanych przez zalogowanego uzytkownika
     * @see DSPermission.SLOWNIK_OSOB_USUWANIE, DSPermission.SLOWNIK_OSOB_DODAWANIE
     * @param fm
     * @param cns
     * @throws EdmException
     */
	public static void setPopupButtonsOnPersonDictionaryByPermission(FieldsManager fm, String... cns) throws EdmException {
		List<String> buttons = Lists.newArrayList("showPopup", "doClear", "doSelect");
		if (DSApi.context().hasPermission(DSPermission.SLOWNIK_OSOB_DODAWANIE)) {
			buttons.add("doAdd");
			buttons.add("doEdit");
		}

		if (DSApi.context().hasPermission(DSPermission.SLOWNIK_OSOB_USUWANIE)) {
			buttons.add("doRemove");
		}
		
		for(String s : cns){
			pl.compan.docusafe.core.dockinds.field.Field f = (pl.compan.docusafe.core.dockinds.field.Field) fm.getField(s);
			if(f instanceof AbstractDictionaryField){
				((AbstractDictionaryField)f).setPopUpButtons(buttons);
			} else if(f instanceof DocumentPersonField){
				((DocumentPersonField)f).setPopUpButtons(buttons);
			}
			
		}
	}
}
