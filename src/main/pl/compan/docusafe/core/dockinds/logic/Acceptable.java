package pl.compan.docusafe.core.dockinds.logic;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;

/**
 * Interfejs dla systemu akceptacji
 * 
 * @author @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public interface Acceptable 
{
            /**
	 * Metoda zwraca nastepna akceptacje
	 * @param document
	 * @return AcceptanceCondition
	 * @throws EdmException
	 */
	public AcceptanceCondition nextAcceptanceCondition(Document document) throws EdmException;
	
	/** Metoda wywolywana po dekretacji przy uzyciu guzika 'dekretuj do nastepnej akceptacji' ale przed commitem i refreshem
	 * @param document - dokument przesylany do nastepnej akceptacji
	 * @param condition - akceptacja na ktora zostanie wyslany dokument
	 * @throws EdmException
	 */
	public void onSendDecretation(Document document, AcceptanceCondition condition) throws EdmException;
	
	public void onAccept(String acceptanceCn, Long documentId) throws EdmException;
	
	public void onWithdraw(String acceptanceCn, Long documentId) throws EdmException;
	
	public boolean canSendDecretation(FieldsManager fm) throws EdmException;
	
	//@SuppressWarnings("unchecked")
	//public void onPush() throws EdmException;
		
}
