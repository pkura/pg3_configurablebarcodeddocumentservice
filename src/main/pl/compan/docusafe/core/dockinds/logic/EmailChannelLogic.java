package pl.compan.docusafe.core.dockinds.logic;

import com.sun.xml.xsom.impl.scd.Iterators;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.parametrization.wssk.LogicExtension_Reopen;

import java.sql.SQLException;
import java.util.Map;

/**
 * Interfejs implementuj�cy logik� dla kana��w email (/admin/edit-email-channel.action / DSEmailChannel) przyk�adowa implementacja w klasie IlpolCokEmailChannelLogic
 * Created with IntelliJ IDEA.
 * User: Krzysztof Modzelewski
 * Date: 14.11.13
 * Time: 09:32
 */
public interface EmailChannelLogic {

    /**
     * Obowi�zkowa kolumna w tabeli logiki dockinda powi�zanego z kana�em email zawieraj�ca id tego kana�u
     */
    public static String EMAIL_CHANNEL_ID_COLUMN_NAME = "EMAIL_CHANNEL_ID";

    /**
     * available.properties odpowiedzialny za w��czenie funkcji wi���cej kana� email z dockindem (/admin/edit-email-channel.action / EditEmailChannelAction)
     */
    public static String EMAIL_CHANNEL_DWR_PROPERTIES = "emailChannelDwr";

    /**
     * Metoda wywo�ywana w trakcie tworzenia nowego kana�u email z u�yciem warto�ci dockinda
     * @param emailChannel obiekt kana�u email
     * @param dwrValues mapa warto�ci z dockinda (DWR)
     * @throws EdmException
     */
    public void onCreate(DSEmailChannel emailChannel, Map<String, String> dwrValues) throws EdmException, SQLException;

    /**
     * Deklaracja metody wywo�ywanej w trakcie aktualizacji istniej�cego kana�u email z u�yciem warto�ci dockinda
     * @param emailChannel obiekt kana�u email
     * @param dwrValues mapa warto�ci z dockinda (DWR)
     * @throws EdmException
     */
    public void onUpdate(DSEmailChannel emailChannel, Map<String, String> dwrValues) throws EdmException, SQLException;

    /**
     * Deklaracja metody wywo�ywanej w trakcie usuwania powi�zania pomi�dzy kana�em email a dockindem
     * @param emailChannel
     * @throws EdmException
     */
    public void onDelete(DSEmailChannel emailChannel) throws EdmException, SQLException;
}
