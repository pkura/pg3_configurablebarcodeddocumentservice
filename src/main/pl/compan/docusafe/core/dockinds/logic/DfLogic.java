package pl.compan.docusafe.core.dockinds.logic;


import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DfInst;
import pl.compan.docusafe.core.dockinds.dictionary.DicAegonCommission;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:piotr.komisarski@com-pan.pl">Piotr Komisarski</a>
 * @version $Id$
 */


public class DfLogic extends AbstractDocumentLogic
{
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(DfLogic.class.getPackage().getName(),null);
    
    public static final String KLASA_FIELD_CN = "KLASA";
    public static final String TYP_FIELD_CN = "TYP";
    public static final String DATA_FIELD_CN = "DATA";
    public static final String NIP_FIELD_CN = "NIP";
    public static final String NUMER_AGENTA_FIELD_CN = "NUMER_AGENTA";
    public static final String OKRES_ROK_FIELD_CN = "OKRESROK";
    public static final String OKRES_MIESIAC_FIELD_CN = "OKRESMIESIAC";
    public static final String STATUS_FIELD_CN = "STATUS";
    public static final String INST_FIELD_CN = "INST";
    public static final String NUMER_RACHUNKU_FIELD_CN = "NUMER_RACHUNKU";
    public static final String NUMERY_TRANSAKCJI_FIELD_CN = "NUMERY_TRANSAKCJI";
    
    private static final String FAKTURY_CN = "FAKTURY";
    private static final String PROWIZJE_CN = "PROWIZJE";
    private static final String TFI_CN = "TFI";
    private static final String BUYSELL_CN = "BUYSELL";
    private static final String WYCIAGI_CN = "WYCIAGI";
    private static final String NOTY_CN = "NOTY";
    private static final String ROZLICZENIA_CN = "ROZLICZENIA";
    private static final String PODATEK_CN = "PODATEK";
    private static final String DOKUMENTY_CN = "DOKUMENTY";
    private static final String SPRAWOZDANIA_CN = "SPRAWOZDANIA";
    
    private static final int FAKTURA_WARTOSC_TYPU = 30;
    private static final int INNE_WARTOSC_TYPU = 40;
    private static final int CIT2_WARTOSC_TYPU = 200;
    private static final int PIT8A_WARTOSC_TYPU = 210;
    private static final int PIT4_WARTOSC_TYPU = 220;
    private static final int VAT7_WARTOSC_TYPU = 230;
    
    //  statusy
    public static final Integer STATUS_PRZYJETY = 1;
    public static final Integer STATUS_W_REALIZACJI = 2;
    public static final Integer STATUS_ZAWIESZONY = 3;
    public static final Integer STATUS_ZREALIZOWANY = 4;
    public static final Integer STATUS_ODRZUCONY = 5;
    public static final Integer STATUS_ARCHIWALNY = 6;
    
    private static final String[] MONTHS = new String[] {"Stycze�", "Luty", "Marzec",
                                               "Kwiecie�","Maj","Czerwiec","Lipiec","Sierpie�",
                                               "Wrzesie�","Pa�dziernik","Listopad","Grudzie�"};
    
    private static DfLogic instance;

    public static DfLogic getInstance()
    {
        if (instance == null)
            instance = new DfLogic();
        return instance;
    }

    
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
        if (values.get(KLASA_FIELD_CN) == null)
            throw new EdmException("Nie wybrano kategorii dokumentu");
    }

    private static String getStringDate(Date date)
    {
         return DateUtils.formatSqlDate(date != null ? date : new Date());//DockindsUtil.printDateAsDay(date);
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        
        String previousFolderPath = document.getFolderPath();
        String previousTitle = document.getTitle();
        
        Calendar cal = Calendar.getInstance();
        String data=null;
        Date dat = (Date)fm.getValue(DATA_FIELD_CN);
        if(dat==null)
            dat = new Date();
        cal.setTime(dat);
        int month = cal.get(Calendar.MONTH)+1;
        int year = cal.get(Calendar.YEAR);
        //data = year+"-"+month+"-"+cal.get(Calendar.DAY_OF_MONTH);
        data = getStringDate(cal.getTime());
        
        EnumItem kategoriaItem = fm.getEnumItem(KLASA_FIELD_CN);
        String title = fm.getValue(TYP_FIELD_CN).toString();
        StringBuffer description = new StringBuffer(title);
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Archiwum Dokument�w Finansowych");
        folder = folder.createSubfolderIfNotPresent(fm.getValue(KLASA_FIELD_CN).toString());
        
        
        if(kategoriaItem.getCn().equals(FAKTURY_CN))
        {
            folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)));
            folder = folder.createSubfolderIfNotPresent(MONTHS[cal.get(Calendar.MONTH)]);
            folder = folder.createSubfolderIfNotPresent(title);
            DfInst inst = (DfInst)fm.getValue(INST_FIELD_CN);
            title = inst.getName()+"("+inst.getNip()+", "+data+")";
            description.append(" (");
            description.append(inst.getName());
            description.append(")");
        }
        else if (kategoriaItem.getCn().equals(PROWIZJE_CN))
        {
            String yearFromOptions = String.valueOf(fm.getValue(OKRES_ROK_FIELD_CN));
            String monthFromOptions = String.valueOf(fm.getValue(OKRES_MIESIAC_FIELD_CN));
            if(yearFromOptions==null)
            {
                folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)));
                folder = folder.createSubfolderIfNotPresent(MONTHS[0]);
            }
            else
            {
                folder = folder.createSubfolderIfNotPresent(yearFromOptions);
                if(monthFromOptions==null)
                    folder = folder.createSubfolderIfNotPresent(MONTHS[0]);
                else
                    folder = folder.createSubfolderIfNotPresent(monthFromOptions);
            }
            folder = folder.createSubfolderIfNotPresent(title);
            if(fm.getValue(TYP_FIELD_CN).toString().equals("Faktura prowizyjna"))
            {
            	
            	String numer_agenta = (String) fm.getValue(NUMER_AGENTA_FIELD_CN);
            	String nip = (String) fm.getValue(NIP_FIELD_CN);
            	String nazwa_posrednika_agencji = numer_agenta;
            	
            	if(fm.getValue("AGENCJA_POSREDNIK") != null)
            	{
            		DicAegonCommission dic = (DicAegonCommission) fm.getValue("AGENCJA_POSREDNIK");
            		numer_agenta = dic.getNumer_agenta();
            		nazwa_posrednika_agencji = dic.getNazwa_posrednik_agencja(); //bug 17 w bugzilli
            		nip = dic.getNip();            		
            	}
            	
            	title = numer_agenta+"("+nip+")";
            	description.append(" (");
                description.append(nazwa_posrednika_agencji);
                description.append(")");
            }
            else 
            {
                title = "Prowizje inne";
                description = new StringBuffer("Inne");
            }
        }
        else if (kategoriaItem.getCn().equals(TFI_CN))
        {
            folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)));
            folder = folder.createSubfolderIfNotPresent(MONTHS[cal.get(Calendar.MONTH)]);
            DfInst inst = (DfInst)fm.getValue(INST_FIELD_CN);
            folder = folder.createSubfolderIfNotPresent(inst.getName());
            folder = folder.createSubfolderIfNotPresent(title);
            title = inst.getName()+"("+data+")";
            description.append(" (");
            description.append(inst.getName());
            description.append(")");
        }
        else if (kategoriaItem.getCn().equals(BUYSELL_CN))
        {
            folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)));
            folder = folder.createSubfolderIfNotPresent(MONTHS[cal.get(Calendar.MONTH)]);
            title = "("+data+")";
        }
        else if(kategoriaItem.getCn().equals(WYCIAGI_CN))
        {
            folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)));
            folder = folder.createSubfolderIfNotPresent(MONTHS[cal.get(Calendar.MONTH)]);
            StringBuffer sb = getDescriptionWithTransactionNumber(fm);
            sb.append("(");
            sb.append(data);
            sb.append(")");
            title = sb.toString();
            folder = folder.createSubfolderIfNotPresent(fm.getDescription(NUMER_RACHUNKU_FIELD_CN).toString());
            description.append(" (");
            description.append(fm.getDescription(NUMER_RACHUNKU_FIELD_CN).toString());
            description.append(")");
        }
        else if (kategoriaItem.getCn().equals(NOTY_CN))
        {
            folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)));
            folder = folder.createSubfolderIfNotPresent(MONTHS[cal.get(Calendar.MONTH)]);
            StringBuffer sb = getDescriptionWithTransactionNumber(fm);
            String tmp = sb.toString();
            sb.append("(");
            sb.append(data);
            sb.append(")");
            title = sb.toString();
            description.append(" (");
            description.append(tmp);
            description.append(")");
        }
        else if (kategoriaItem.getCn().equals(ROZLICZENIA_CN))
        {
            folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)));
            folder = folder.createSubfolderIfNotPresent(MONTHS[cal.get(Calendar.MONTH)]);
            StringBuffer sb = getDescriptionWithTransactionNumber(fm);
            String tmp = sb.toString();
            sb.append("(");
            sb.append(data);
            sb.append(")");
            folder = folder.createSubfolderIfNotPresent(title);
            title = sb.toString();            
            description.append(" (");
            description.append(tmp);
            description.append(")");
        }
        else if (kategoriaItem.getCn().equals(PODATEK_CN))
        {
            folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getValue(OKRES_ROK_FIELD_CN)));
            folder = folder.createSubfolderIfNotPresent(title);
            DfInst inst = (DfInst)fm.getValue(INST_FIELD_CN);
            if(inst!=null)
                title = title+" ("+inst.getName()+")";
            else
            {
                String strMonth = fm.getKey(OKRES_MIESIAC_FIELD_CN).toString();
                if(strMonth.length()<2)
                    strMonth = "0" + strMonth;
                title = title+" ("+strMonth+")";  
               // folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getValue(OKRES_MIESIAC_FIELD_CN)));
            }
            //folder = folder.createSubfolderIfNotPresent(title);
            description = new StringBuffer(title);
        }
        else if (kategoriaItem.getCn().equals(DOKUMENTY_CN))
        {
            DfInst inst = (DfInst)fm.getValue(INST_FIELD_CN);
            folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)));
            folder = folder.createSubfolderIfNotPresent(inst.getName());
            description = new StringBuffer(title);
            description.append(" (");
            description.append(inst.getName());
            description.append(")");
            title = title+"("+data+")";
        }
        else if (kategoriaItem.getCn().equals(SPRAWOZDANIA_CN))
        {
            folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getValue(OKRES_ROK_FIELD_CN)));
            title = title;
            description = new StringBuffer(title);
            description.append(" (");
            description.append(String.valueOf(fm.getValue(OKRES_ROK_FIELD_CN)));
            description.append(")");
        }
        if (document instanceof OfficeDocument)
            ((OfficeDocument)document).setSummaryOnly(description.toString());
        document.setFolder(folder);
        document.setTitle(title);
        document.setDescription(description.toString());
        
        
         if (document instanceof OfficeDocument)
        {                      
            // wpis do historii o archiwizacji - je�li obecny folder inny od poprzedniego
            if (!document.getFolderPath().equals(previousFolderPath))
                ((OfficeDocument) document).addWorkHistoryEntry(
                       Audit.create("df", DSApi.context().getPrincipalName(),
                            sm.getString("DokumentZarchiwizowanoWfolderze",document.getFolderPath()),
                            null, null));
            
            if (!document.getTitle().equals(previousTitle))
                ((OfficeDocument) document).addWorkHistoryEntry(
                       Audit.create("df", DSApi.context().getPrincipalName(),
                            sm.getString("ZmianaOpisuPismaNa",document.getTitle()),
                            null, null));
        }  
    }
    
    /**
     * Zwraca StringBuffer z opisem pola NUMERY_TRANSAKCJI.
     */
    private StringBuffer getDescriptionWithTransactionNumber(FieldsManager fm) throws EdmException
    {
        StringBuffer sb = new StringBuffer();
        String nrTransakcji = (String) fm.getDescription(NUMERY_TRANSAKCJI_FIELD_CN);
        if (nrTransakcji != null)
            sb.append(nrTransakcji);
       /* for(int i=1;i<6;i++)
        {
            Object o = fm.getDescription("NumTrans"+i);
            if(o!=null)
            {
                sb.append(o.toString());
                sb.append(", ");
            }
        }*/
        return sb;
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {        
        Map<String,Object> values = new HashMap<String,Object>();
        Date data = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        
        values.put(DATA_FIELD_CN, data);
        //values.put(OKRES_MIESIAC_FIELD_CN, cal.get(Calendar.MONTH));//ew+-1
        //values.put(OKRES_ROK_FIELD_CN, cal.get(Calendar.YEAR)%100+1);
        if (DocumentLogic.TYPE_ARCHIVE == type)
            values.put(STATUS_FIELD_CN, STATUS_ARCHIWALNY);
        else
            values.put(STATUS_FIELD_CN, STATUS_PRZYJETY);
        fm.reloadValues(values);
    }

    @Override
    public void correctValues(Map<String,Object> values, DocumentKind kind) throws EdmException
    {
        Date data = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        if(values == null )
        	return;
        if(values.get(TYP_FIELD_CN) != null)
        {
	        int typ = Integer.parseInt(values.get(TYP_FIELD_CN).toString());
	        if(values.get(OKRES_ROK_FIELD_CN)==null)
	            values.put(OKRES_ROK_FIELD_CN, cal.get(Calendar.YEAR)%100+1);
	        if(typ==INNE_WARTOSC_TYPU|| typ==FAKTURA_WARTOSC_TYPU||
	           typ==CIT2_WARTOSC_TYPU|| typ==PIT4_WARTOSC_TYPU||
	           typ==PIT8A_WARTOSC_TYPU|| typ==VAT7_WARTOSC_TYPU)
	        {   
	            if(values.get(OKRES_MIESIAC_FIELD_CN)==null)
	                values.put(OKRES_MIESIAC_FIELD_CN, cal.get(Calendar.MONTH)+1);
	        }
        }
    }


    public void documentPermissions(Document document) throws EdmException 
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        EnumItem kategoriaItem = fm.getEnumItem(KLASA_FIELD_CN);
        DSApi.context().clearPermissions(document);
        if (kategoriaItem.getCn().equals(FAKTURY_CN))
        {
            DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DF_READ_FAKTURY", ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DF_ADMIN_FAKTURY", ObjectPermission.GROUP);
        }
        else if(kategoriaItem.getCn().equals(PROWIZJE_CN))
        {
            DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DF_READ_PROWIZJE", ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DF_ADMIN_PROWIZJE", ObjectPermission.GROUP);
        }
        else if(kategoriaItem.getCn().equals(TFI_CN))
        {
            DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DF_READ_TFI", ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DF_ADMIN_TFI", ObjectPermission.GROUP);
        }
        else if(kategoriaItem.getCn().equals(BUYSELL_CN))
        {
            DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DF_READ_BUYSELL", ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DF_ADMIN_BUYSELL", ObjectPermission.GROUP);
        }
        else if(kategoriaItem.getCn().equals(WYCIAGI_CN))
        {
            DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DF_READ_WYCIAGI", ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DF_ADMIN_WYCIAGI", ObjectPermission.GROUP);
        }
        else if(kategoriaItem.getCn().equals(NOTY_CN))
        {
            DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DF_READ_NOTY", ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DF_ADMIN_NOTY", ObjectPermission.GROUP);
        }
        else if(kategoriaItem.getCn().equals(ROZLICZENIA_CN))
        {
            DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DF_READ_ROZLICZENIA", ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DF_ADMIN_ROZLICZENIA", ObjectPermission.GROUP);
        }
        else if(kategoriaItem.getCn().equals(PODATEK_CN))
        {
            DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DF_READ_PODATEK", ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DF_ADMIN_PODATEK", ObjectPermission.GROUP);
        }
        else if(kategoriaItem.getCn().equals(DOKUMENTY_CN))
        {
            DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DF_READ_DOKUMENTY", ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DF_ADMIN_DOKUMENTY", ObjectPermission.GROUP);
        }
        else if(kategoriaItem.getCn().equals(SPRAWOZDANIA_CN))
        {
            DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DF_READ_SPRAWOZDANIA", ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DF_ADMIN_SPRAWOZDANIA", ObjectPermission.GROUP);
        }
        
        
        DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DF_READ", ObjectPermission.GROUP);
        DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DF_ADMIN", ObjectPermission.GROUP);
        try {
            DSApi.context().session().flush();
        }
        catch (HibernateException ex) {
            throw new EdmHibernateException(ex);
        }        
    }

    
    public boolean canReadDocumentDictionaries() throws EdmException    
    {
        return DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_ODCZYTYWANIE);
    }

    public boolean isReadPermissionCode(String permCode) 
    { 
        return permCode.startsWith("DF_READ") || permCode.startsWith("DF_ADMIN"); 
    } 
    
}
