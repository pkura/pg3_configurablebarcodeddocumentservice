package pl.compan.docusafe.core.dockinds.logic;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class FieldsPrintProvider {

    protected static Logger log = LoggerFactory.getLogger(FieldsPrintProvider.class);

    public String getPatternOfFormatter() {
        return "### ### ##0.0000;-### ### ##0.0000";
    }
    public void setDefaultFormatter(Map<Class, Format> defaultFormatters){
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        defaultFormatters.put(Double.class, new DecimalFormat(getPatternOfFormatter(), symbols));
        defaultFormatters.put(BigDecimal.class, new DecimalFormat(getPatternOfFormatter(), symbols));
    }


    public final void setAdditionalTemplateValuesUnsafe(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        fm.initialize();

        setAdditionalTemplateValuesUnsafe(values, defaultFormatters, fm, doc);
    }

    public void setAdditionalTemplateValuesUnsafe(Map<String, Object> values, Map<Class, Format> defaultFormatters, FieldsManager fm, OfficeDocument doc) throws EdmException {
        setDefaultFormatter(defaultFormatters);
        putExtractedValues(values, fm);
    }

    public void putExtractedValues(Map<String, Object> values, FieldsManager fm) {
        FieldsManagerUtils.FieldValuesGetter getter = new FieldsManagerUtils.DateEnumValuesGetter(false);
        //FieldsManagerUtils.FieldValuesGetter getterNullable = new FieldsManagerUtils.DateEnumValuesGetter(true);
        try {
            for (pl.compan.docusafe.core.dockinds.field.Field field : fm.getFields()) {
                try {
                    String fieldCn = field.getCn();
                    if (field instanceof DictionaryField) {
                        List<Map<String, Object>> dictTemplateValues = FieldsManagerUtils.getMultiDictionaryExtractedValues(fm, fieldCn, getter);
                        values.put(fieldCn, dictTemplateValues);
                    } else if (field instanceof DocumentPersonField) {
                        Long personId = (Long) fm.getKey(field.getCn());
                        Person person = Person.find(personId.longValue());
                        Map<String, Object> templatePerson = toTemplate(person);
                        values.put(fieldCn, templatePerson);
                    } else if (field instanceof DataBaseEnumField) {
                        Object fieldValue = fm.getValue(fieldCn);
                        values.put(fieldCn, fieldValue);
                    } else if (field instanceof AttachmentField) {
                        Attachment fieldValue = (Attachment) fm.getValue(fieldCn);
                        if (fieldValue != null)
                            values.put(fieldCn, fieldValue.getTitle());
                    } else {
                        Object fieldValue = fm.getValue(fieldCn);
                        Object extractedValue = getter.getValue(fieldValue);
                        if (extractedValue != null)
                            values.put(fieldCn, extractedValue);
                        if (fieldValue instanceof List)
                            values.put(fieldCn, fieldValue);
                    }
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    public Map<String, Object> toTemplate(Person person) {
        Map<String, Object> template = new HashMap<String, Object>();
        template.put("ORGANIZATION", person.getOrganization());
        template.put("STREET", person.getStreet());
        template.put("ZIP", person.getZip());
        template.put("LOCATION", person.getLocation());
        try {
            String country = DataBaseEnumField.getEnumItemForTable("dsr_country", Integer.valueOf(person.getCountry())).getTitle();
            template.put("COUNTRY", country);
        } catch (Exception e) {
            log.error(e.getMessage());
            template.put("COUNTRY", "---");
        }
        template.put("NIP", person.getNip());
        template.put("REGON", person.getRegon());
        return template;
    }
}
