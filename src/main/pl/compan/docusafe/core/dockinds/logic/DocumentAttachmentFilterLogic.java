package pl.compan.docusafe.core.dockinds.logic;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;

public interface DocumentAttachmentFilterLogic {
	/**
	 * Metoda zwracaj�ca list� przefiltrowanych za��cznik�w dokumentu
	 * @param document dokument kt�rego za��czniki b�d� przefiltrowane
	 * @return ista przefiltrowanych za��cznik�w dokumentu
	 * @throws EdmException
	 */
	public List<Attachment> filterAttachments(Document document, List<Attachment> attachments) throws EdmException;
}
