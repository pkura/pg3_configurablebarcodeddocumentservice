package pl.compan.docusafe.core.dockinds.logic;

import pl.compan.docusafe.core.base.Document;

/**
 * <p>
 *     Metoda tego interfaceu b�dzie odpalana przy imporcie pism.
 * </p>
 */
public interface ImportLogic {

    public void onImportAction(Document document);

}
