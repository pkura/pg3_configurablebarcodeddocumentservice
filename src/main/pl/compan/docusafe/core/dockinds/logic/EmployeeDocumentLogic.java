package pl.compan.docusafe.core.dockinds.logic;

import static pl.compan.docusafe.util.FolderInserter.to2Letters;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.encryption.EncryptedAttachmentRevision;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.WorkerDictionary;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
/**
 * Logika dokumentu pracowniczego
 *
 * @author Mariusz Kilja�czyk
 */
public class EmployeeDocumentLogic extends AbstractDocumentLogic
{

	private final static Logger LOG = LoggerFactory.getLogger(EmployeeDocumentLogic.class);

    // nazwy kodowe poszczeg�lnych p�l dla tego rodzaju dokumentu
    public static final String KLASA_FIELD_CN = "KLASA";
    public static final String TYP_FIELD_CN = "TYP";
    public static final String ZWOLNIONY_FIELD_CN = "ZWOLNIONY";
    public static final String PRACOWNIK_FIELD_CN = "PRACOWNIK";
    public static final String NUMER_PORZADKOWY_FIELD_CN = "NUMER";
    public static final String DATA_FIELD_CN = "DATA";
    public static final String OPIS_FIELD_CN = "OPIS";
   // public static final String _FIELD_CN = "KLASA";

    public static final Integer KLSA_A_ID = 10;
    public static final Integer KLSA_B_ID = 20;
    public static final Integer KLSA_C_ID = 30;

    public static final Integer TYP_ZASWIADCZENIE_INNE_ID = 90;
    public static final Integer TYP_DYPLOM_UKONCZENIA_KURSU_ID = 1040;
    public static final Integer TYP_LIST_GRATULACYJNY_PREMIA_POLROCZNA_ID = 1080;
    public static final Integer TYP_LIST_GRATULACYJNY_PREMIA_ROCZNA_ID = 1090;
    public static final Integer TYP_ZASWIADCZENIE_O_UKONCZENIA_SZKOLENIA_PRZECIWDZIALANIE_PRANIU_PIENIEDZY_ID = 1320;
    public static final Integer TYP_OSWIADCZENIE_O_ZAMIARZE_WSPOLNEGO_OPODATKOWANIA_DOCHODOW_ID = 1330;
    public static final Integer TYP_PISMO_W_SPRAWIE_ID = 2040;
    public static final Integer TYP_SWIADECTWO_PRACY_ID = 2020;
    public static final Integer TYP_INNE_B_ID = 1500;

	@Override
	public void correctValues(Map<String, Object> values, DocumentKind kind) throws EdmException {
		values.remove(NUMER_PORZADKOWY_FIELD_CN);
	}

	/**
	 *
	 * @param document
	 * @param type je�li - -500 to nie uaktualniaj s�ownik�w
	 * @throws EdmException
	 */
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        /** Walidacja */
        if(fm.getValue(PRACOWNIK_FIELD_CN) == null)
        		throw new EdmException("Nie wybrano pracownika");
        if(fm.getValue(KLASA_FIELD_CN) == null)
    		throw new EdmException("Nie wybrano klasy dokumentu");
        if(fm.getValue(TYP_FIELD_CN) == null)
    		throw new EdmException("Nie wybrano typu dokumentu");

        /**Ustawienie folderu*/
        WorkerDictionary worker = (WorkerDictionary) fm.getValue(PRACOWNIK_FIELD_CN);
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Archiwum Dokument�w Pracowniczych");
        if(worker.getPracuje())
        {
        	folder = folder.createSubfolderIfNotPresent("Dokumenty pracownik�w zatrudnionych");
        }
        else
        {
        	folder = folder.createSubfolderIfNotPresent("Dokumenty pracownik�w zwolnionych");
        }
        if(worker.getInstytucja() != null && worker.getInstytucja().equals(WorkerDictionary.LIFE))
        {
        	folder = folder.createSubfolderIfNotPresent("Life");
        }
        else
        {
        	folder = folder.createSubfolderIfNotPresent("PTE");
        }
		folder = folder.createSubfolderIfNotPresent(to2Letters(worker.getNazwisko()));
        folder = folder.createSubfolderIfNotPresent(worker.getNazwiskoImie());
        folder = folder.createSubfolderIfNotPresent((String) fm.getValue(KLASA_FIELD_CN));
        document.setFolder(folder);

        /**Ustawienie tytu�u oraz opisu*/
        String title = "";
        if(KLSA_A_ID.equals(fm.getKey(KLASA_FIELD_CN)))
        {
        	if(TYP_ZASWIADCZENIE_INNE_ID.equals(fm.getKey(TYP_FIELD_CN)))
            {
        		title = fm.getValue(TYP_FIELD_CN)+" "+(fm.getValue(OPIS_FIELD_CN) != null ? fm.getValue(OPIS_FIELD_CN) : " ")+" "+worker.getNazwiskoImieWithBackets();
        		document.setTitle(fm.getValue("NUMER_TECZKI")+" "+title);
            	document.setDescription(title);
            	try{((OfficeDocument)document).setSummaryOnly(title);}catch (Exception e){}
            }
            else
            {
            	title = fm.getValue(TYP_FIELD_CN)+" "+ worker.getNazwiskoImieWithBackets();
            	document.setTitle(fm.getValue("NUMER_TECZKI")+" "+title);
            	document.setDescription(title);
            	try{((OfficeDocument)document).setSummaryOnly(title);}catch (Exception e){}
            }
        }
        else if(KLSA_B_ID.equals(fm.getKey(KLASA_FIELD_CN)))
        {
        	if(TYP_DYPLOM_UKONCZENIA_KURSU_ID.equals(fm.getKey(TYP_FIELD_CN)) || TYP_LIST_GRATULACYJNY_PREMIA_POLROCZNA_ID.equals(fm.getKey(TYP_FIELD_CN))
        		|| TYP_LIST_GRATULACYJNY_PREMIA_ROCZNA_ID.equals(fm.getKey(TYP_FIELD_CN)) || TYP_OSWIADCZENIE_O_ZAMIARZE_WSPOLNEGO_OPODATKOWANIA_DOCHODOW_ID.equals(fm.getKey(TYP_FIELD_CN))
        		|| TYP_ZASWIADCZENIE_O_UKONCZENIA_SZKOLENIA_PRZECIWDZIALANIE_PRANIU_PIENIEDZY_ID.equals(fm.getKey(TYP_FIELD_CN)) 
        		|| TYP_INNE_B_ID.equals(fm.getKey(TYP_FIELD_CN)))
        	{
        		title = fm.getValue(TYP_FIELD_CN)+" "+(fm.getValue(OPIS_FIELD_CN) != null ? fm.getValue(OPIS_FIELD_CN) : " ");
        		document.setTitle(fm.getValue("NUMER_TECZKI")+" "+title);
        		document.setDescription(title+" "+worker.getNazwiskoImieWithBackets());
        		try{((OfficeDocument)document).setSummaryOnly(title+" "+worker.getNazwiskoImieWithBackets());}catch (Exception e){}
        	}
        	else
        	{
        		title = fm.getValue(TYP_FIELD_CN)+" "+ DateUtils.formatJsDateOrEmptyString((Date) fm.getKey(DATA_FIELD_CN));
        		document.setTitle(title);
        		document.setDescription(title+" "+worker.getNazwiskoImieWithBackets());
        		try{((OfficeDocument)document).setSummaryOnly(title+" "+worker.getNazwiskoImieWithBackets());}catch (Exception e){}
        	}
        }
        else if(KLSA_C_ID.equals(fm.getKey(KLASA_FIELD_CN)))
        {
        	if(TYP_PISMO_W_SPRAWIE_ID.equals(fm.getKey(TYP_FIELD_CN)))
        	{
        		title = fm.getValue(TYP_FIELD_CN)+" "+ (fm.getValue(OPIS_FIELD_CN) != null ? fm.getValue(OPIS_FIELD_CN) : " ");
        		document.setTitle(fm.getValue("NUMER_TECZKI")+" "+title);
            	document.setDescription(title+" "+ DateUtils.formatJsDateOrEmptyString((Date) fm.getKey(DATA_FIELD_CN))+" "+worker.getNazwiskoImieWithBackets());
            	try{((OfficeDocument)document).setSummaryOnly(title+" "+ DateUtils.formatJsDateOrEmptyString((Date) fm.getKey(DATA_FIELD_CN))+" "+worker.getNazwiskoImieWithBackets());}catch (Exception e){}
        	}
        	else
        	{
        		title = fm.getValue(TYP_FIELD_CN)+" "+ DateUtils.formatJsDateOrEmptyString((Date) fm.getKey(DATA_FIELD_CN));
            	document.setTitle(fm.getValue("NUMER_TECZKI")+" "+title);
            	document.setDescription(title+" "+worker.getNazwiskoImieWithBackets());
            	try{((OfficeDocument)document).setSummaryOnly(title+" "+worker.getNazwiskoImieWithBackets());}catch (Exception e){}
        	}
        	if(TYP_SWIADECTWO_PRACY_ID.equals(fm.getKey(TYP_FIELD_CN)))
        	{
        		if(type != -500 && fm.getBoolean(ZWOLNIONY_FIELD_CN) && Boolean.TRUE.equals(worker.getPracuje())){
					worker.setPracuje(false);

					DSApi.context().session().flush();

					DockindQuery query = new DockindQuery(0,0);
					query.setDocumentKind(DocumentKind.findByCn(DocumentLogicLoader.EMPLOYEE_KIND));
					query.field(fm.getField(PRACOWNIK_FIELD_CN), worker.getId());
					Iterator<Document> it = DocumentKindsManager.search(query);
					while(it.hasNext()){
						Document doc = it.next();
						LOG.info("od�wierzanie dokumentu: " + doc.getId());
						this.archiveActions(doc, type);
					}
				}
        	}
        }
        else
        {
        	throw new EdmException("B��dna klasa dokumentu");
        }
                
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
    	perms.add(new PermissionBean(ObjectPermission.READ,"PRACOWNICZY_READ",ObjectPermission.GROUP,"Dokument pracowniczy - odczyt"));
    	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"PRACOWNICZY_READ",ObjectPermission.GROUP,"Dokument pracowniczy - odczyt"));
    	
    	perms.add(new PermissionBean(ObjectPermission.MODIFY,"PRACOWNICZY_MODIFY",ObjectPermission.GROUP,"Dokument pracowniczy - modyfikacja"));
    	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"PRACOWNICZY_MODIFY",ObjectPermission.GROUP,"Dokument pracowniczy - modyfikacja"));
    	perms.add(new PermissionBean(ObjectPermission.READ,"PRACOWNICZY_MODIFY",ObjectPermission.GROUP,"Dokument pracowniczy - modyfikacja"));
    	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"PRACOWNICZY_MODIFY",ObjectPermission.GROUP,"Dokument pracowniczy - modyfikacja"));
	    this.setUpPermission(document, perms);
	}

	public AttachmentRevision createAttachmentRevision() throws EdmException
	{
		return new EncryptedAttachmentRevision();
	}

}
