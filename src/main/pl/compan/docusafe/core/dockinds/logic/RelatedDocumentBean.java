package pl.compan.docusafe.core.dockinds.logic;

/**
 * Bean dokument�w powi�zanych 
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class RelatedDocumentBean
{
	/** ID dokumentu */
	private Long id;
	
	/** tytu� dokumentu */
	private String title;
	
	/** klucz czynno�ci */
	private String activityKey;
	
	/** in, out, internal */
	private String docType;
	
	/** attachment id */
	private Long attachmentId;
	
	/** attachment mime */
	private String attachmentMime;
	
	public RelatedDocumentBean(Long id, String title, String activityKey, String docType, Long attachmentId, String attachmentMime)
	{
		this.id = id;
		this.title = title;
		this.activityKey = activityKey;
		this.docType = docType;
		this.attachmentId = attachmentId;
		this.attachmentMime = attachmentMime;
	}
	
	public Long getId()
	{
		return id;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public String getActivityKey()
	{
		return activityKey;
	}
	
	public String getDocType()
	{
		return docType;
	}
	
	public Long getAttachmentId()
	{
		return attachmentId;
	}
	
	public String getAttachmentMime()
	{
		return attachmentMime;
	}
}
