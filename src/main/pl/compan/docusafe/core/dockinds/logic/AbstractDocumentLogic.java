package pl.compan.docusafe.core.dockinds.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import org.dom4j.DocumentException;
import org.hibernate.classic.Lifecycle;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.StandardAttachmentRevision;
import pl.compan.docusafe.core.base.encryption.EncryptedAttachmentRevision;
import pl.compan.docusafe.core.base.permission.DefaultPermissionPolicy;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.base.permission.PermissionPolicy;
import pl.compan.docusafe.core.crypto.DocusafeKeyStore;
import pl.compan.docusafe.core.crypto.IndividualRepositoryKey;
import pl.compan.docusafe.core.dockinds.BoxLine;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.PdfLayerParams;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.acceptances.JBPMAcceptanceManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.dockinds.measure.ProcessParameterBean;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.jackrabbit.JackrabbitAttachmentRevision;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.ocr.FileToOcrSupport;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.handlers.DocumentMailHandler;
import pl.compan.docusafe.parametrization.archiwum.PublicDocument;
import pl.compan.docusafe.service.epuap.EpuapExportDocument;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytka;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.zebra.ZebraPrinterManager;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.web.office.common.DwrDocumentMainTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionType;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullPobierzTyp;

import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.text.Format;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 * Abstrakcyjna logika dokumentu. Wszystkie klasy implementuj�ce logik� dokumentu powinny 
 * dziedziczy� po tej klasie.
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public abstract class AbstractDocumentLogic implements DocumentLogic
{
	private StringManager sm = GlobalPreferences.loadPropertiesFile(AbstractDocumentLogic.class.getPackage().getName(),null);
    private final static Logger log = LoggerFactory.getLogger(AbstractDocumentLogic.class);
    static final String CREATE_NEXT_FOR_THIS_USER = "documentMain.createNextForThisUser";
    public static final String DWR_ = "DWR_";

    public PermissionPolicy getPermissionPolicy()
	{
		return new DefaultPermissionPolicy();
	}

    protected Object getValue(DocumentKind documentKind, Map<String,Object> values, String fieldCn) throws EdmException
    {
        return documentKind.getFieldByCn(fieldCn).coerce(values.get(fieldCn));
    }

    public PdfLayerParams getPdfLayerParams(Document document) throws EdmException
    {
    	return null;
    }
    /**
     * Metoda sprawdza czy uprawnienie istnieje jesli nie istniej to go dodaje
     * Nastepnie sprawdza czy nie ma uprawnien nadmiarowych w dokumencie
     * @throws EdmException
     */
    public final void setUpPermission(Document document, Set<PermissionBean> permissions) throws EdmException
    {
    	Set<PermissionBean> toGrant = new HashSet<PermissionBean>();
    	Set<PermissionBean> toRevoke = new HashSet<PermissionBean>();
    	Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
    	Map<String,PermissionBean> documentPermissionsMap = new HashMap<String,PermissionBean>();
    	Map<String,PermissionBean> permissionsMap = new HashMap<String,PermissionBean>();
    	for (PermissionBean perm : documentPermissions)    		
    	{
    		// buduje mape uprawnien w dokumencie
    		documentPermissionsMap.put(perm.getKey(), perm);
    	}
    	//log.trace("Iteruje uprawnienia do dodania ");
    	for (PermissionBean perm : permissions)
    	{
    		//sprawdzam czy uprawnienia nowe znajduja sie juz w dokumencie
    		//log.trace(perm.getKey());
    		if (documentPermissionsMap.get(perm.getKey())==null)    		    		
    		{  			
    			toGrant.add(perm);
    		}
    		//a przy okazji buduje mape do sprawdzenie uprawnien nowych
    		permissionsMap.put(perm.getKey(), perm);
    	}
    	//log.trace("Iteruje uprawnienia dokumentu");
    	for (PermissionBean  perm : documentPermissions)
    	{
    		//sprawdzam uprawnienia w dokumencie do usuniecia
    		//log.trace(perm.getKey());
    		if (permissionsMap.get(perm.getKey())==null)
    		{    			
    			toRevoke.add(perm);
    		}
    	}
    	for (PermissionBean rev : toRevoke)    		
    	{
    		//log.trace("do usuniecia {}",rev.getKey());
    		DSApi.context().revoke(document, rev);
    	}
    	for (PermissionBean add : toGrant)    		
    	{
    		//log.trace("do dodania {}",add.getKey());
    		if (add.isCreateGroup() && add.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
    		{
    			String groupName = add.getGroupName();
    			if (groupName == null)
    			{
    				groupName = add.getSubject();
    			}
    			createOrFind(document, groupName,add.getSubject());
    		}
    		DSApi.context().grant(document, add);
    		
    		if(AvailabilityManager.isAvailable("Zastepstwa.nadawajPrawaZastepowanemuGdyPrzejmujeZastepujacy")&&add.getSubjectType().equalsIgnoreCase(ObjectPermission.USER)&&(OfficeDocument.find(document.getId()).getAssignmentHistory().size()>2)){
    			DSUser[] zastepowani=DSUser.findByUsername(add.getSubject()).getSubstituted();
    			//TODO w przypadku gdy dekretuje sie na zastepowanego, dodawany jest wpis do historri dekretacji na ta osobe, a nastepnie na zastepujacego, trzeba znalezc inny sposob by to znalezc
    			//jesli ten target tego wpisu == zastepowanemu to wowczas i jemu daje te same prawa co zastepujacemu
    			for(DSUser user:zastepowani){
    				if(OfficeDocument.find(document.getId()).getAssignmentHistory().get(OfficeDocument.find(document.getId()).getAssignmentHistory().size()-2).getTargetUser().equals(user.getName())){
    					add.setSubject(user.getName());
    					DSApi.context().grant(document, add);
    				}
    			}
    		}

    	}
    }
    
    /**
     * Bazowy GUID dla nowotworzonych grup domyslnie rootDivision
     * @param document
     * @throws EdmException
     */
    protected String getBaseGUID(Document document) throws EdmException
    {
    	return Docusafe.getAdditionProperty("baseGuid");
    }
    
    
    /**
     * Iteruje po Kolekcji uprawnien i uzywa funkcji w @see
     * 
     * @see this.createOrFind(Document document,String name, String guid)
     * @param document
     * @param perms
     * @return
     * @throws EdmException
     */
    protected void createOrFind(Document document, Set<PermissionBean> perms) throws EdmException
    {
    	for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup()&& perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
		}
//    	DSApi.context().session().flush();
    }
    
    /**
     * Metoda odnajduje grupe jesli nie istnieje to tworzy
     * @param name
     * @param guid
     * @throws EdmException
     */
    protected DSDivision createOrFind(Document document,String name, String guid) throws EdmException
    {        
        try
        {
            return DSDivision.find(guid.trim());
        }
        catch (DivisionNotFoundException e)
        {
            DSDivision parent = DSDivision.find(getBaseGUID(document));
            return parent.createGroup(name, guid); 
        }
        
    }
    
    /**
     * Standardowa implementacja - nic nie robi
     */
    public void correctValues(Map<String,Object> values, DocumentKind kind) throws EdmException
    {
    }
    
    /**
     * Standardowa implementacja - nic nie robi
     */
    public  void correctValues(Map<String,Object> values, DocumentKind kind , long documentId ) throws EdmException
	{
	}
    /**
     * Standardowa implementacja - nic nie robi
     */
   public  void setAfterCreateBeforeSaveDokindValues(DocumentKind kind ,Long id, Map<String, Object> dockindKeys)throws EdmException 
   {
	   
   }
   /**
    * Odwo�anie do logiki dokumentu aby uzupelnic dokinda o  dane  z emaila 
    * Standardowa implementacja - nic nie robi
    * @FieldsManager
    * @param mailMessage
    * @param content
    * @throws EdmException
    * 
    */
   /*	public void setInitialValuesFromEmail(FieldsManager fm , MailMessage mailMessage, String emailContent) throws EdmException 
   	{
   		
   	}*/

    public boolean validateSuccess(Map<String,Object> values, DocumentKind kind) throws EdmException
    {
        try
        {
            validate(values, kind, null);
            return true;
        }
        catch (ValidationException e)
        {
            return false;
        }
    }
    
    public void validateAcceptances(Map<String,Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
        // NARAZIE TU NIC NIE ROBIE, BO OBECNIE AKCEPTACJE ZALEZA CO NAJWYZEJ OD CENTROW KOSZTOWYCH
        // KTORE SA ZAPISYWANE W INNY SPOSOB
        
      /*  FieldsManager fm = kind.getFieldsManager(documentId);
        fm.initialize();
        
        if (fm.getAcceptancesDefinition() != null)
        {
            fm.initializeAcceptances();
            
            for (DocumentAcceptance docAcc : fm.getAcceptances())
            {
                Acceptance acc = fm.getAcceptancesDefinition().getAcceptances().get(docAcc.getAcceptanceCn());                
                if (acc != null && acc.getFieldCn() != null)
                    // gdy dokonana akceptacja zale�y od pola steruj�cego, to wymuszam pozostawienie dotychczasowej warto�ci w tym polu
                    // TODO: obadac moze lepiej walic tu Exception informujacy o tym??
                    values.put(acc.getFieldCn(), acc.getCurrentFieldValue());
            }
        }*/
    }
    /**
     * Waliduj� map� z warto�ciami wed�ug regu� dla dokument�w posiadaj�cych t� logik�.
     * 
     * @param values
     * @param kind
     * dokument jeszcze nie istnieje (importy, przymowanie dokumentu etc)
     * @throws EdmException
     */
    public void validate(Map<String, Object> values, DocumentKind kind) throws EdmException
    {
    	validate(values, kind, null);
    }


    /**
     * Metoda s�u�y do opakowania walidacji logiki dokumentu w celu u�ycia jej na innych interfejsach nie tylko www.
     * metoda u�ywana na interfejsie API oraz importera. W celu przepakowania mapy na map� u�ywan� na metodzie.
     * validateDWR
     *
     * @param values   najprawdopodbniej jest to Map<String,String>
     * @param document
     * @throws pl.compan.docusafe.core.EdmException
     */
    @Override
    public void globalValidate(Map<String, Object> values, Document document) throws EdmException {
        FieldsManager fm = document.getFieldsManager();
        Map<String, Object> fieldDatas = FieldsManagerUtils.convert(values, fm);
        validate(fieldDatas, document.getDocumentKind(), document.getId());
    }

    public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
    {    	
    	return null;
    }

    /**
     * Metoda umo�liwia zmodyfikowanie w�asno�ci p�l dwra przed wy�wietleniem ich na formatce u�ytkownika.
     * Rozszerzenie funkcjonalno�ci dostarczonej przez droolsy, kt�ra umo�liwiaja wy��cznie modyfikacj� w�asno�ci p�l dwra w zale�no�ci od warto�ci p�l typu enum.
     * @param dwrFields
     * @param fm
     * @throws EdmException
     */
    public void modifyDwrFields(List<pl.compan.docusafe.core.dockinds.dwr.Field> dwrFields, Map<String, Object> dwrFieldsValues, FieldsManager fm) throws EdmException
    {
        return;
    }
    
    public String getInOfficeDocumentKind()
    {
        return "Dokument Biznesowy";
    }
    
    public boolean canAddToRS() throws EdmException
    {
        return false;
    }
    
    public boolean canCreateManually() throws EdmException
    {
        return true;
    }
    
    public boolean needsEditDoctypeAction()
    {
        return false;
    }
    
    public boolean canReadDocumentDictionaries() throws EdmException
    {
        return true;
    }
    
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
    {
    }
    
    public void specialSetDictionaryValues(Map<String, ?> dockindFields)
    {
    }
    
    public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
    {
    }
    
    @Override
    public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
    {
    }
    
    public void onStartProcess(OfficeDocument document) throws EdmException {
    }
    /**
     * Metoda uzmowliwia uruchomienie procesu oraz  zadekretowanie zaimportowanego dokumentu na  kilka punkt�w kancelaryjnych 
     * po rozpoznainiu prefixu w nazwie pliku  na poczatku nazwy piku np  K1_nazwapliku K2_nazwapliku
     * w adsach na logice dokumentu trzeba sparametryzowac dla jakiego prefixu na jaki dzia� ma isc dekretacja i ustawic odpowiednie dzia�y 
	 * domyslnie nic nie robi 
     * @param prefix
     */
    public void onStartProcess(OfficeDocument document, ActionEvent event ,String filePrefix) throws EdmException {
    	
    }

    public void onStartProcess(OfficeDocument document,ActionEvent event) throws EdmException {
    	//sprawdzenie, czy w logice dokumentu nie ma starej metody onStartProcess(OfficeDocument document)
    	//je�li jest to wywo�ujemy j�
    	boolean oldMethod = false;
    	try {
			this.getClass().getDeclaredMethod("onStartProcess", OfficeDocument.class);
			oldMethod = true;
			onStartProcess(document);
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		}
    	
    	if (!oldMethod) {
    		if (Jbpm4Provider.getInstance().isJbpm4Supported()) {
        		String docName = document.getDocumentKind()!=null?document.getDocumentKind().getName():"no name";
        		
        		log.info("--- "+docName+" : START PROCESS !!! ---- {}", event);
        		try
        		{
                    {
                        Map<String, Object> map = Maps.newHashMap();
                        Object user = event.getAttribute(ASSIGN_USER_PARAM);
                        if(user != null){
                            map.put(ASSIGN_USER_PARAM, user);
                        }
                        Object division = event.getAttribute(ASSIGN_DIVISION_GUID_PARAM);
                        if(division != null){
                            map.put(ASSIGN_DIVISION_GUID_PARAM, division);
                        }
                        document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
                    }
        			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

        			DSUser author = DSApi.context().getDSUser();
        			String user = author.getName();
        			String fullName = author.asFirstnameLastname();

        			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
        			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
        			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
        			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
        			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

        			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        			perms.addAll(documentPermissions);
        			this.setUpPermission(document, perms);
        			
        			if (AvailabilityManager.isAvailable("addToWatch")) {
        				DSApi.context().watch(URN.create(document));
        			}
        		}
        		catch (Exception e)
        		{
        			log.error(e.getMessage(), e);
        			throw new EdmException(e.getMessage());
        		}
        	} else {
        		onStartProcess(document);
        	}
    	}
    }
    
    public void onEndProcess(OfficeDocument document, boolean isManual) throws EdmException
    {
    }
    
    public void onDecretation(Document document) throws EdmException 
    {	
    }
    
    public void onGiveAcceptance(Document document, String acceptanceCn) throws EdmException 
    {	
    }
    
    public void onWithdrawAcceptance(Document document, String acceptanceCn) throws EdmException 
    {	
    }
    
    public boolean canAcceptDocument(FieldsManager fm, String acceptanceCn) throws EdmException 
	{
    	return true;
    }
    
    /**
     * Poriownuje sciezki folderow przedstawione jako listy Stringow. W zasadzie to porownuje po prostu dwie listy Stringow.
     * @param path
     * @param required
     * @return Zwraca true gdy listy SA ROZNE
     */
    public boolean checkFolderPaths(List<String> path, List<String> required)
    {
    	boolean requireChange = false;

    	if(path.size()!=required.size()) return true;
		for(int i=0; i<required.size();i++)
		{
			if(!required.get(i).equals(path.get(i))) 
			{
				requireChange = true;
				break;
			}
		}
		return requireChange;
		
    }
    
    /**
     * Sprawdza, czy wyst�pi�a sytuacja, w kt�rej u�ytkownik koniecznie musia� doda� uwag� do dokumentu
     * (i czy proces powinien zosta� zako�czony) - u�ywane np. w fakturach kosztowych dla Aegon, gdzie
     * po ustaleniu statusu jako 'Odrzucona', konieczne jest dodanie uwagi (przyczyny odrzucenia), a po 
     * zapisaniu zmian zadanie jest ko�czone.     
     * Je�li tak, to uwaga dodawana jest do dokumentu.
     * 
     * @return true <=> nale�y dodatkowo zako�czy� obecne zadanie
     */
    public boolean checkRemarkField(Document document, Map<String,Object> values, String remark) throws EdmException
    {
        DocumentKind documentKind = document.getDocumentKind();
        String finishFieldCn = documentKind.getProperties().get(DocumentKind.REMARK_FIELD_CN);
        if (finishFieldCn != null)
        {
            Field finishField = documentKind.getFieldByCn(finishFieldCn);
            Integer enumId = (Integer) finishField.coerce(values.get(finishFieldCn));
            if (enumId == null)
                return false;
            EnumItem chosenItem = finishField.getEnumItem(enumId);
            if (chosenItem.isForceRemark() && remark != null) {
                if (document instanceof OfficeDocument) {
                    remark = "Przyczyna odrzucenia - " + remark;
                    ((OfficeDocument) document).addRemark(new Remark(StringUtils.left(remark, 4000), DSApi.context().getPrincipalName()));
                }
                
                if ("true".equals(documentKind.getProperties().get(DocumentKind.FINISH_WITH_REMARK_ADD)))
                    return true;
            }
        }
        return false;
    }
    
    /**
     * Przekazuje true <=> dla dokument�w tego rodzaju mo�na generowa� obraz dokumentu w postaci PDF.
     */
    public boolean canGenerateDocumentView()
    {
        return false;
    }
    
    public boolean canGenerateCsvView() 
    {
    	return false;
    }
    
    public boolean canChangeDockind(Document document) throws EdmException
    {
    	return true;
    }
    
    /**
     * Dla podanego dokumentu generowany jest jego obraz w postaci PDF.
     * 
     * @return wygenerowany plik PDF
     */
    public File generateDocumentView(Document document) throws EdmException
    {        
        return null;
    }
    
    public File generateCsvDocumentView(Document document) throws EdmException 
    {
    	return null;
    }
    
    /**
     * Zwraca polityk� bezpiecze�stwa dla dokument�w tego rodzaju.
     */
    public int getPermissionPolicyMode()
    {
        return PermissionManager.BUSINESS_LIGHT_POLICY;
    }

	/**
     * Metoda zwraca obiekt zawieraj�cy dane wykorzystywane do wygenerowania listy zada�
     * @throws EdmException
	 */
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException{
		return TaskListParams.fromDockindProperties(kind, documentId);
	}
	
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
		return getTaskListParams(kind, documentId);
	}
    
    public void correctImportValues(Document document, Map<String, Object> values, StringBuilder builder) throws EdmException
    {
    }
    
    public String getPhoneNumber(Document document) throws EdmException
    {
    	return null;
    }
    
    public JBPMAcceptanceManager getAcceptanceManager() {
    	return null;
    }          
    
    public void manualFinish(Document document) throws EdmException
    {
    }
    
    public boolean accept(Document doc, Object... context) throws EdmException{    	
    	return this.getAcceptanceManager()!= null && this.getAcceptanceManager().accept(doc, context);
    }

    
    /**
     * Jesli nikt nie zaimplementuje tej metody to nie bedzie ona wyznaczac koordynatora
     */
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
    {
        return null;
    }
    /**
     * Standardowa i w 100% poprawna implementacja metody, jednak w przypadku skomplikowanej procedury okreslenia koordynatora
     * metoda ta ze wzgledow wydajnosciowych powinna byc zastapiona prostszym rozwiazaniem
     */
    public boolean isProcessCoordinator(OfficeDocument document) throws EdmException
    {
    	return getProcessCoordinator(document) != null;
    }
    
    /**
     * Standardowa implementacja - ignoruje jakiekolwiek zdarzenia
     */
	public boolean processActions(OfficeDocument document, String activity) throws EdmException
	{
		return false;
	}

	/**
	 * Standardowa implementacja - nic nie robi
	 */
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
	{
    }

	/**
	 * Standardowa implementacja - wczytuje warto�ci z xml'a (szukaj 'default-value')
	 */
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for(Field f : fm.getFields()){
            if(f.getDefaultValue() != null){
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
            }
        }
         if (AvailabilityManager.isAvailable(CREATE_NEXT_FOR_THIS_USER, fm.getDocumentKind().getCn()))
        {
           if (fm != null && fm.getDocumentKind().getFieldByCn(NormalLogic.RECIPIENT_HERE) != null)
            {
                saveFastAssignmentToSession(toReload);
            }
        }
        log.info("toReload = {}", toReload);
        fm.reloadValues(toReload);
    }

    /**
	 * Standardowa implementacja - powinny byc sprawdzane, jesli nie chcemy nadpisujemy
	 * metode
	 */
	public boolean searchCheckPermissions(Map<String, Object> values)
    {
        return true;
    }
	
	/**
	 * Standardowa implementacja - zadne sprawdzenie nie jest wykonywane
	 */
	public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
    {
    }
	
	/**
	 * Standardowa implementacja zwraca true
	 */
	public boolean canFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
    {
		return true;
    }
	
	public String getExtendedPermissionPolicy()
	{
		return null;
	}
	
	public ProcessParameterBean getProcessParameter()
	{
		return null;
	}

	public List<ProcessParameterBean> getProcessParameter(Document document) throws EdmException
	{
		return null;
	}
	
	public AttachmentRevision createAttachmentRevision() throws EdmException
	{
        if(AvailabilityManager.isAvailable("jackrabbit.repositories")) {
            return JackrabbitAttachmentRevision.create();
        } else if(AvailabilityManager.isAvailable("alwaysEncryptAttachments")) {
			return new EncryptedAttachmentRevision();
        } else {
			return new StandardAttachmentRevision();
        }
	}
	
	public DocusafeKeyStore getKeyStoreInstance()
	{
		return new IndividualRepositoryKey();
	}
	
	public String countAttachmentRevisionVerficationCode(AttachmentRevision ar)
	{
		if(AvailabilityManager.isAvailable("attachments.integrity.check"))
		{
			try
			{
				InputStream is = ar.getBinaryStream();
				String ret = "";
		    	MessageDigest md = MessageDigest.getInstance("SHA-256");
		    	byte[] input = new byte[256];
				while(is.read(input)>0)
				{
					md.update(input);
				}
				ret = IOUtils.toString(Base64.encodeBase64(md.digest()));
				return ret;
			}
			catch (Exception e) 
			{
				log.trace(e.getMessage(), e);
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	
	public boolean isPersonalRightsAllowed()
	{
		return true;
	}
	
	public boolean isReadPermissionCode(String permCode)
	{
		return true;
	}

	public void acceptTask(Document document, ActionEvent event, String activity)
	{
	}

    public void updateDocumentBarcodeVersion(Document document) throws Exception
    {
    }

	public String[] forceEqualsSearchList(Map<String,Object> values) throws EdmException
	{
		return new String[0];
	}
	
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values,Map<String, Object> dockindKeys) throws EdmException
	{
		log.error("brak implementacji");
		event.addActionError("Brak obs�ugi zdarzenia");
	}
	
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException
	{
		doDockindEvent(eventActionSupport, event, document, activity, values, null);
	}
	
    /**
     * Funkcja wywo�ywana na samym ko�cu podczas procesu tworzenia dokumentu
     * @todo narazie jest tylko w internal
     * @param doc Document
     */
    public void onEndCreateProcess(Document document) throws EdmException
    {
    }
    
    /**
     * Sprawdza czy mo�e odrzuci� dokument, przycisk jest pod formularzem
     * @param fm
     * @param document
     */
    
    public boolean canRemarkDocument(FieldsManager fm,Document document) throws EdmException
    {
        return false;
    }
    
    /**
     * Funkcja wywolywana przed manualFinish(document, !doWiadomo�ci)  iprzed updatem TaskSnashot
     * oraz commitem
     * w akcji DocumentArchiveTab$ManualFinish
     * 
     * @see DocumentArchiveTabAction$ManualFinish
     * @param document Document
     * @param isDoWiadomosci Boolean
     * @throws EdmException
     */
    public void onCommitManualFinish(Document document, Boolean isDoWiadomosci) throws EdmException
    {
    }
    
    /**
     * Funkcja wywolywana podczas akcji 'Dekretuj do akceptacji' podczas gdy u�ytkownik wybierze osob� z listy
     * (lista osob jest defioniowana zawsze w logice dokumentu)
     * @param document Document
     * @param userName String
     * @throws EdmException 
     */
    public void onDecretationWhenUsernameSet(Document document,String userName) throws EdmException
    {
    }
	 /**
     * Na podstawie warto�ci atrybut�w danego dokumentu zwraca id Pudla z linni
     * @throws EdmException 
     */
    public Long getBoxLine(Document document) throws EdmException
    {
		BoxLine boxline = document.getDocumentKind().getBoxLine(document);
		String lineCn = null;
		if(boxline == null && document.getDocumentKind().getProperties() != null)
		{
			lineCn = document.getDocumentKind().getProperties().get(DocumentKind.BOX_LINE_KEY);
		}
		else
		{
			lineCn = boxline.getCn();
		}
		return lineCn != null ? GlobalPreferences.getBoxId(lineCn) : null;
    }
     
    public String getBoxLineCn(Document document) throws EdmException
    {
		BoxLine boxline = document.getDocumentKind().getBoxLine(document);
		String lineCn = null;
		if(boxline == null && document.getDocumentKind().getProperties() != null)
		{
			lineCn = document.getDocumentKind().getProperties().get(DocumentKind.BOX_LINE_KEY);
		}
		else
		{
			lineCn = boxline.getCn();
		}
		return lineCn;
    }
    
    public void onLoadData(FieldsManager fm) throws EdmException
    {
    }
    
    public void bindToJournal(OfficeDocument document,ActionEvent event) throws EdmException
    {
    }
    
    public List<RelatedDocumentBean> getRelatedDocuments(FieldsManager fm) throws EdmException
    {
    	return null;
    }
    
    public Map<Long, String> getAvailableArchiveBoxes(FieldsManager fm)
    		throws EdmException
    {
    	return null;
    }
    
    public void canWithdrawAcceptance(Document doc, String acceptanceCn) throws EdmException
    {	
    }
    
    public boolean canDeleteAttachments(Document document) throws EdmException
    {
    	// domyslnie tak jak edycja dokumentu
    	return document.canModify();
    }
    
    public boolean assignee(OfficeDocument doc, Assignable assignable,
			OpenExecution openExecution, String acceptationCn,
			String fieldCnValue) throws EdmException {
		return false;
    	
    }

	@Override
    public final void archiveActions(Document document, int type) throws EdmException {
        archiveActions(document, type, ArchiveSupport.get(document));
    }

    public abstract void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException;

    /**
     * Brak wsparcia dla OCR
     */
    private final static OcrSupport NULL_OCR_SUPPORT = new OcrSupport() {
        public void checkForOcr(Document doc, AttachmentRevision ar) throws EdmException {
            log.info("NULL_OCR_SUPPRORT");
        }

		@Override
		public void putToOcr(AttachmentRevision ar) throws EdmException
		{
		}
    };
    
    /**
     * Metoda zwraca handler obs�uguj�cy ��dania ocrowani dokument�w
     * Domy�lnie zwracany jest pusty handler dla za��cznik�w
     * @return handler obs�uguj�cy ocr, nigdy null
     */
    public OcrSupport getOcrSupport(){
    	if(AvailabilityManager.isAvailable("ocr.file"))
    		return FileToOcrSupport.get();
    	else
    		return NULL_OCR_SUPPORT;
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values){
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters){
        setAdditionalTemplateValues(docId, values);
    }
    
    
   /**
     * sprawdza czy user jest autorem dokumentu
     * @param document
     * @return 
     */
    public boolean isAuthor(Document document, String userName)
    {
        if(userName.equals(document.getAuthor()))
            return true;
        else
            return false;
    }
   /**
     * sprawdza czy zalogowany user jest autorem dokumentu
     * @param document
     * @return 
     */
    public boolean isAuthor(Document document)
    {
        return isAuthor(document, DSApi.context().getPrincipalName());
    }
    
    /**
     * Sprawdza czy zalogowany u�ytkownik zaakceptowa� dokument czy te� odrzuci�
     * false -> nie wykona� rzadnej akcji
     * true - >zaakceptowa� lub te� odrzuci�
     * @param document
     * @return
     * @throws EdmException 
     */
    public boolean isAcceptedOrRejected(Document document) throws EdmException
    {
        return isAcceptedOrRejected(document, DSApi.context().getPrincipalName(), null);
    }
    /**
     * Sprawdza czy u�ytkownik zaakceptowa� dokument czy te� odrzuci�
     * false -> nie wykona� rzadnej akcji
     * true -> zaakceptowa� lub te� odrzuci�
     * @see isUserAccepted, isUserRejected
     * @param document
     * @param loggedUsername
     * @param acceptanceCn
     * @return
     * @throws EdmException 
     */
    public boolean isAcceptedOrRejected(Document document, String username, String acceptanceCn) throws EdmException
    {
        boolean isAccepted = isUserAccepted(document,username,acceptanceCn);
        boolean isRejected = isUserRejected(document, username);
        
        if(isAccepted && isRejected)
            return false;
        else
            return true;
    }
    
    /**
     * Sprawdza czy zalogowany u�ytkownik zaakceptowa� dokument
     */
    public boolean isUserAccepted(Document document) throws EdmException
    {
        return isUserAccepted(document, DSApi.context().getPrincipalName(), null);
    }
    
    /**
     * Sprawdza czy u�ytkownik zaakceptowa� dokument
     */
    public boolean isUserAccepted(Document document, String username,String acceptanceCn) throws EdmException
    {
        return ((List<DocumentAcceptance>)DocumentAcceptance.find(document.getId(),username,acceptanceCn, null)).isEmpty();
        
    }
    
    /**
     * Sprawdza czy zalogowany u�ytkownik odrzuci� dokument
     * @see isUserRejected(document, username)
     * @param document
     */
    public boolean isUserRejected(Document document) throws EdmException
    {
        return isUserRejected(document, DSApi.context().getPrincipalName());
    }
    /**
     * Sprawdza czy u�ytkownik odrzuci� dokument
     * @param document
     * @param username
     * @return boolean
     * @throws EdmException
     */
    public boolean isUserRejected(Document document, String username) throws EdmException
    {
        return ((List<Remark>)Remark.find(document.getId(), username)).isEmpty();
    }
    /**
     * Sprawdza czy u�ytkownik mo�e przywr�ci� na liste zada�
     * rzuca wyj�tkiem
     * @throws EdmException
     */
    @Override
    public void canReopenDocument(Document document) throws EdmException
    {
    }
    
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
    {
    	return null;
    }
    
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues,Long documentId)
    {
    	
    	return setMultipledictionaryValue(dictionaryName, values);
    }
    
    /**
     * Metoda do por�wnywania starych i nowych wartosci na dokumencie
     * @param newFm
     * @param oldFm
     */
    public void specialCompareData(FieldsManager newFm, FieldsManager oldFm)  throws EdmException
    {
    }
    
    public void onEndManualAssignment(OfficeDocument doc,  String fromUsername, String toUsername)
    {
    }
    
    
    
    @Override
	public String onStartManualAssignment(OfficeDocument doc, String fromUsername, String toUsername)
	{
		return null;
	}

	/**
	 * Metoda wywo�ywana w RejectedListener
	 * @see pl.compan.docusafe.core.jbpm4.RejectedListener
	 * @param doc
	 */
	public void onRejectedListener(Document doc) throws EdmException
	{
	}
	
	/**
	  * Metoda wywo�ywana w AcceptedListenerze
	  * @see pl.compan.docusafe.core.jbpm4.AcceptancesListener
	 */
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
		
	}
	
	public Map<String, String> setInitialValuesForReplies(Long inDocumentId) throws EdmException
	{
		return new HashMap<String, String>();
	}
	
	public void removeValuesBeforeSet(Map<String, Object> dockindKeys)
	{
	}
	
	public void setAdditionalValues(FieldsManager fm, Integer valueOf, String activity) throws EdmException
	{
		 Map<String, Object> toReload = Maps.newHashMap();
	        for(Field f : fm.getFields()){
	            if(fm.getKey(f.getCn()) == null && f.getDefaultValue() != null ){
	                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
	            }
	        }
	        log.info("toReload = {}", toReload);
	        fm.reloadValues(toReload);	
	}

	@Override
	public boolean sendMailNotification(FieldsManager fieldsManager) throws EdmException {
		return true;
	}

	@Override
	public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
		return null;
	}
	
	/** metoda wywolywana przez serwis importuj�cy dokumenty z ePUAP.
	 * Pozwala na specyficzn� dla danego rodzaju dokumentu inicjalizacj� p�l tego dokumentu. */
	public void setDocValuesAfterImportFromEpuap (OfficeDocument document, EpuapSkrytka skrytka, boolean dokument1_upo0) throws EdmException
	{
	}
	
	/** metoda wywolywana przez serwis eksportuj�cy do ePUAP.
	 * Okre�la ktora skrytka ma byc jako nadawca dokumentu epuap.
	 * Jesli zwroci null, bedzie uzyta domyslna skrytka
	 * (co jest uzyteczne zwlaszcza, gdy uzywana jest w ogole tylko jedna) */
	public EpuapSkrytka wybierzSkrytkeNadawcyEpuap (OfficeDocument document) throws EdmException {
		return null;
	}
	
	/** przekazuje do logiki id, jakie dokument dostal z epuapu oraz date wyslania.
	 * Domyslnie nic nie robi. */
	@Override
	public void saveEpuapDetails(Document doc, Long epuapId, Date dataWyslania)
	{
	}

	/**
	 * Odwo�anie do logiki dokumentu aby uzupelnic dokinda o  dane  z odpowiedzi z ePUAP
	 * @param odpowiedz
	 * @throws EdmException
	 */
	@Override
	public void setAdditionalValuesFromEpuapDocument(OdpowiedzPullPobierzTyp odpowiedz, InOfficeDocument doc,Map<String, Object> values) throws EdmException, DocumentException, IOException
	{
	}
	
		
	
	/** wpisuje do dokumentu info o doreczeniu (ePUAP Doreczyciel). Domyslnie nie robi zupelnie nic.
	 * 
	 * @param upoDoc	- dokument DS zrobiony na podstawie UPO, moze byc NULL, jesli tego nie robiono.
	 * 					Jesli nie jest null to tym UPO jest UPD doreczenia dokumenu doc.
	 * @param doc		- dokument, kt�ry pomy�lnie doreczono
	 * @param upo		- obiekt wyslany do nas przez ePUAP
	 * @param dtDoreczenia - data doreczenia
	 * @param kod		- kod wyniku operacji zwracany przez ePUAP. (Dostawalem 1 jako sukces) */
	public void oznaczDokumentJakoDostarczony(Document upoDoc, Document doc,
			OdpowiedzPullPobierzTyp upo, Date dtDoreczenia, int kod)
	{
	}
	
	/** dodaje do dokumentu link do dokumentu zawierajacego UPO. Domyslnie nic nie robi. */
	public void dodajLinkDoUPO(Document upoDoc, Document doc, String nazwaUpo,
			OdpowiedzPullPobierzTyp upo) throws EdmException
	{
	}

	@Override
	public boolean canUnarchive(Document document) throws EdmException
	{
		boolean canUnarchive = document.getDocumentKind() != null && !DocumentKind.NORMAL_KIND.equals(document.getDocumentKind().getCn());
        canUnarchive &= DSApi.context().getDSUser().isAdmin();
        
        return canUnarchive;
	}

	@Override
	public void globalActionDocument(Document document, int documentType, String actionName) throws EdmException
	{
		
	}

	@Override
	public void sendEpuapExportStatus(OutOfficeDocument officeDocument, String komunikat, EpuapExportDocument epuapDocument)
	{
		
	}

    @Override
    public String getMailForJbpmMailListener(Document document, String mailForLogicCn) {
        return null;
    }

    @Override
    public void prepareMailToSend(Document document, String[] eventParams, DocumentMailHandler.DocumentMailBean documentMailBean) {
    }

    @Override
    public void addSpecialValueForProcessMailNotifier(FieldsManager fm, Map<String, Object> ctext) throws EdmException
    {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Zapisuje w warto�ciach do za�adowania - login i dzial uzytkownika dla pola Odbiorca - RECIPIENT_HERE
     */
    private void saveFastAssignmentToSession(Map<String, Object> toReload)
    {
        javax.servlet.http.HttpServletRequest request = null;
        HttpSession session = null;


        try
        {
            session = WebContextFactory.get().getSession();
        }
        catch (Exception e)
        {
            log.warn("Blad w czasie pobierania requestu",e);
        }

        if (session != null && session.getAttribute(DwrDocumentMainTabAction.FAST_ASSIGNMENT_SELECT_DIVISION) != null)
        {
            StringBuilder userDivision = new StringBuilder("");
            if (session.getAttribute(DwrDocumentMainTabAction.FAST_ASSIGNMENT_SELECT_USER) != null)
                userDivision.append("u:").append(session.getAttribute(DwrDocumentMainTabAction.FAST_ASSIGNMENT_SELECT_USER));
            if (session.getAttribute(DwrDocumentMainTabAction.FAST_ASSIGNMENT_SELECT_USER) != null)
                userDivision.append(";d:").append(session.getAttribute(DwrDocumentMainTabAction.FAST_ASSIGNMENT_SELECT_DIVISION));
            if (session.getAttribute(DwrDocumentMainTabAction.DELIVERY_TYPE) != null)
            {
                Integer deliveryId = (Integer) session.getAttribute(DwrDocumentMainTabAction.DELIVERY_TYPE);
                toReload.put(DwrDocumentMainTabAction.DOC_DELIVERY,deliveryId);
            }
            toReload.put(NormalLogic.RECIPIENT_HERE, userDivision.toString());
        }
    }
    
    @Override
    public void printLabel(Document document, int type, Map<String, Object> dockindKeys) throws EdmException
    {
    	if(!AvailabilityManager.isAvailable("print.barcode.create", document.getDocumentKind().getCn()))
    		return;
    	String barcode = null;
//    	System.out.println(GlobalPreferences.getUseBarcodePrinter());
//    	;
    	if(document instanceof OfficeDocument)
    	{
    		barcode = ((OfficeDocument)document).getBarcode();
    		 if(StringUtils.isEmpty(barcode))
    		 {
    			 ((OfficeDocument)document).setBarcode(BarcodeUtils.getKoBarcode((OfficeDocument)document));
    			 barcode = ((OfficeDocument)document).getBarcode();
    		 }
    	}
    	Map<String,String> fields = new LinkedHashMap<String,String>();
		fields.put(sm.getString("NumerPisma"),TextUtils.nullSafeObject(((OfficeDocument)document).getOfficeNumber()));
		fields.put(sm.getString("Przyjmujacy"),DSUser.findByUsername(document.getAuthor()).asFirstnameLastname());
		if(document instanceof InOfficeDocument)
		{
			fields.put(sm.getString("DataPrzyjecia"),DateUtils.formatCommonDate(((InOfficeDocument)document).getDocumentDate()));
			fields.put(sm.getString("Login"),((InOfficeDocument)document).getTrackingNumber());
			fields.put(sm.getString("Haslo"),((InOfficeDocument)document).getTrackingPassword());
		}
    	ZebraPrinterManager zm = new ZebraPrinterManager();
		zm.printLabel(barcode, fields);
    }

    public void onAddRevision(Document document)throws EdmException
    {

    }

    public void onDeletedRevision(Document document) throws EdmException
    {

	}
   
    public boolean canSearchAllJournals() {
    return true;
    }

    @Override
    public void beforeSetWithHistory(Document document, Map<String, Object> values, String activity) throws EdmException
    {

    }

    public List<Journal> getJournalsPermissed(String journalType) throws EdmException {
    	return Journal.listAllByType(journalType);
    }

    @Override
    public void modifyTaskViewOnList(Document document, String tabName, Task task){

    }

	@Override
	public List<DSUser> getUserWithAccessToDocument(Long documentId) {
		return null;
	}
	
	public List<DSUser>  getUsersAsClerk(DSUser currentUser, List<DSUser> users) throws EdmException {
		return DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
	}

	public void addWeightAndCostsLettersSelectedByValues(OutOfficeDocument document, Integer rodzajGabaryt, boolean listyKrajowe, boolean listEkonomiczny,
			HashMap<Integer, Map<Integer, BigDecimal>> mapaKosztow, Map<Integer, String> mapaRodzajowKraj, HashMap<Integer, Map<Integer, Integer>> tempIloscPoszczegolnychWag){
		
	}

    @Override
    public List<String> getActionMessage(ActionType source, Document doc) {
        List<String> messages = Lists.newLinkedList();

        switch (source) {
            case OFFICE_CREATE_DOCUMENT:
                messages.add(sm.getString("newDocument.createdDocumentWithId", (doc instanceof OfficeDocument ? ((OfficeDocument) doc).getFormattedOfficeNumber() : "-"), doc.getId().toString()));
                break;
            default:
                break;
        }

        return messages;
    }

    @Override
    public String onAlternativeUpdate(Document document, Map<String, Object> values, String activity) throws EdmException {
        return sm.getString("ZarchiwizowanoDokument");
    }

    @Override
    public boolean sendToExternaRepostiory(FieldsManager fieldsManager, String propUuid) throws EdmException {
        return false;
    }

    @Override
    public void updateInCmisApi(Document document, Map<String, Object> fieldValues) throws EdmException {
        document.getDocumentKind().setWithHistory(document.getId(), fieldValues, false);
    }
    
    @Override
    public Map<String, String> modifyEnumValues(Map<String, String> enumValues, ArrayList<String> enumSelectedSetm, String cn, Long id) throws EdmException {
        return null;
    }
   
   public  void revaluateFastAssignmentSelectetValuesOnDocument(OfficeDocument doc, String fastAssignmentSelectUser, String fastAssignmentSelectDivision) throws UserNotFoundException, EdmException
	{
    	
	}
	/**
	 * metoda powinn zwraca� liste userNames�w dla ktorych mamy wys�a� maila z akcj� na procesie 
	 * @return
	 * @throws EdmException 
	 * @throws DocumentNotFoundException 
	 */
   @Override
   public List<String> getUsersToProcesActionFromEmail(Long documentId ) throws DocumentNotFoundException, EdmException{
	return null;
    	
	}
   /**
	 * metoda umozliwia dodanie dodatkowej tresci do wysylanego maila podczas wysylaniu o mozliwych akcjach  akcji  na procesie  z emaila  
	 * @return
	 * @throws EdmException 
	 * @throws DocumentNotFoundException 
	 */
   @Override
   public void addSpecialValueToSendingActionEmail(OfficeDocument doc, StringBuilder emailTresc, DSUser odbiorca) throws EdmException
   {
	   
   }
   @Override
   public String getTemplateFileName(long id, String templateName) {
	   if (templateName.toLowerCase().endsWith(".rtf")) {
		   return templateName.replaceAll("[\\.][(r|R)][(t|T)][(f|F)]", "");
	   }
	   return templateName;
   }

    /**
     * Zwraca null lub obiekt FieldData z mapy je�li zawiera dany Cn lub je�li cn nie zaczyna si� na DWR_ to doda i sprawdzi
     * je�li bedzie to zwr�ci.
     * @param values
     * @param cn
     * @return
     */
    public static FieldData containsField(Map<String, FieldData> values, String cn) {
        FieldData ret = null;

        if(StringUtils.isBlank(cn))
            return ret;

        if(values.containsKey(cn)){
            ret = values.get(cn);
        } else if(!cn.startsWith(DWR_)){
            String cn2 = DWR_ + cn;
            if(values.containsKey(cn2)){
                ret = values.get(cn2);
            }
        }

        return ret;
    }

    /**
     * Zwraca warto�� z mapy, null je�li nie ma
     * je�li bedzie to zwr�ci.
     * @param values
     * @param cn
     * @return
     */
    public static Object containsField(Map<String, Object> values, String cn) {
        Object ret = null;

        if(StringUtils.isBlank(cn))
            return ret;

        if(values.containsKey(cn))
            ret = values.get(cn);

        return ret;
    }
    /**
     * MEtoda umozliwiajaca zmienienie ukrycie wyswietlanych pozycji podczas ladowania warto�ci s�ownika wielowarto�ciowego ze s�ownika
     *  domyslnei nic nie robi
     * @param abstractDictionaryField
     * @param dictionaryIds przekazane id wybranych pozycji kt�re zostan� wyswietlone na s�owniku
     * @throws EdmException 
     * @throws AccessDeniedException 
     * @throws DocumentNotFoundException 
     */

	public void revaluateMultipleDictionaryValues(Long documentId, AbstractDictionaryField abstractDictionaryField, Collection<Long> dictionaryIds) throws DocumentNotFoundException, AccessDeniedException, EdmException{
    	
    }

    @Override
    public Integer getOfficeDocumentsCount(OfficeDocument doc)  {
        return 1;
    }

    public PublicDocument getPublicDocument(OfficeDocument doc) throws EdmException {
        PublicDocument publicDocument = new PublicDocument(doc.getId(),
                DSUser.findByUsername(doc.getAuthor()).getFirstnameLastnameName(),
                doc.getTitle(),
                null,//"abstrakt",
                doc.getCtime(),
                null,//"organizationalUnit",
                null,//"location",
                null, null, null, null);//"type");

        return publicDocument;
    }
}

