package pl.compan.docusafe.core.dockinds.logic;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.FolderResolver;
import pl.compan.docusafe.core.dockinds.field.ArchiveListener;
import pl.compan.docusafe.util.FolderInserter;

import java.util.Collections;
import java.util.Map;

/**
 * Zbi�r narz�dzi przydatnych podczas archiwizacji.
 *
 * Metody w tej klasie udost�pniaj� mechanizmy dost�pne z innych miejsc w systemie ale niekoniecznie znane.
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class ArchiveSupport {

    //w przysz�o�ci mo�e b�dzie konieczno�� rozr�niania
    private final static ArchiveSupport INSTANCE = new ArchiveSupport();
    public static ArchiveSupport get(Document document) {
        return INSTANCE;
    }

    /**
     * Skr�t do FolderInserter.root()
     * @return FolderInserter.root()
     * @throws EdmException
     */
    public FolderInserter getFolderInserter() throws EdmException {
        return FolderInserter.root();
    }

    /**
     * U�ywa FolderResolver dla danego dokumentu
     * @param doc Dokument do obsadzenia
     * @throws EdmException w przypadku b��d�w FolderResolvera
     * @throws NullPointerException je�li FolderResolver nie zosta� okre�lony
     */
    public void useFolderResolver(Document doc) throws EdmException {
        doc.getDocumentKind().getDockindInfo().getFolderResolver().insert(doc.getId(), Collections.<String, Object>emptyMap());
    }

    /**
     * U�ywa FolderResolver dla danego dokumentu
     * @param doc Dokument do obsadzenia
     * @param params dodatkowe parametry dla mechanizmu z xml'a
     * @throws EdmException w przypadku b��d�w FolderResolvera
     * @throws NullPointerException je�li FolderResolver nie zosta� okre�lony
     */
    public void useFolderResolver(Document doc, Map<String,?> params) throws EdmException {
        doc.getDocumentKind().getDockindInfo().getFolderResolver().insert(doc.getId(), params);
    }

    /**
     * Pr�buje wype�ni� dost�pne pola dokumentu korzystaj�c z dost�pnych provider�w
     * ustawianych w xml'u
     * @param document - dokument do uzupe�nienia
     * @throws EdmException
     */
    public void useAvailableProviders(Document document) throws EdmException {
        DockindInfoBean dib = document.getDocumentKind().getDockindInfo();
        DocFacade doc = Documents.document(document.getId());
        if(dib.getTitleProvider() != null){
            document.setTitle(dib.getTitleProvider().apply(doc));
        }
        if(dib.getDescriptionProvider() != null){
            document.setDescription(dib.getDescriptionProvider().apply(doc));
        }
    }

    /**
     * Odpala wszystkie listenery archiwizacji
     * @param document
     * @throws EdmException
     */
    public void fireArchiveListeners(Document document) throws EdmException {
        DockindInfoBean dib = document.getDocumentKind().getDockindInfo();
        for(ArchiveListener al : dib.getArchiveListeners()){
            al.onArchive(document, this);
        }
    }
}
