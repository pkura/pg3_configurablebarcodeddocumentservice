package pl.compan.docusafe.core.dockinds.logic;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.DocumentWatch.Type;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.acceptances.IntercarsAcceptanceMode;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.dictionary.InvoiceContractDictionary;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.other.InvoiceInfo;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.viewserver.ViewServer;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.asprise.util.tiff.A;
import com.lowagie.text.Chapter;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.ListItem;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Logika dla rodzaju dokumentu 'invoice'.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class InvoiceLogic extends AbstractDocumentLogic implements Discardable, Acceptable
{
    public final static Logger log = LoggerFactory.getLogger(InvoiceLogic.class);
    //  nazwy kodowe poszczeg�lnych p�l dla tego rodzaju dokumentu
    public static final String DATA_WYSTAWIENIA_FIELD_CN = "DATA_WYSTAWIENIA";
    public static final String DATA_PLATNOSCI_FIELD_CN = "DATA_PLATNOSCI";
    public static final String DATA_ZAKSIEGOWANIA_FIELD_CN = "DATA_ZAKSIEGOWANIA";
    public static final String NUMER_TRANSAKCJI_FIELD_CN = "NUMER_TRANSAKCJI";
    public static final String DOSTAWCA_FIELD_CN = "DOSTAWCA";
    public static final String NUMER_FAKTURY_FIELD_CN = "NUMER_FAKTURY";
    public static final String STATUS_FIELD_CN = "STATUS";
    public static final String DATA_ZAPLATY_FIELD_CN = "DATA_ZAPLATY";
    public static final String ZAPLACONO_FIELD_CN = "ZAPLACONO";
    public static final String AKCEPTACJA_FINALNA_FIELD_CN = "AKCEPTACJA_FINALNA";
    public static final String KWOTA_BRUTTO_FIELD_CN = "KWOTA_BRUTTO";
    public static final String KWOTA_NETTO_FIELD_CN = "KWOTA_NETTO";
    public static final String CENTRUM_KOSZTOWE_FIELD_CN = "CENTRUM_KOSZTOWE";
    public static final String OPIS_TOWARU_FIELD_CN = "OPIS_TOWARU";
    public static final String NUMER_ZESTAWIENIA_FIELD_CN = "NUMER_ZESTAWIENIA";
    public static final String NUMER_RACHUNKU_BANKOWEGO_CN = "NUMER_RACHUNKU_BANKOWEGO";
    public static final String PION_CN = "PION";
    public static final String VAT_CN = "VAT";
    public static final String NAZWA_PROJEKTU_FIELD_CN = "NAZWA_PROJEKTU";
    public static final String POTWIERDZENIE_ZGODNOSCI_FIELD_CN = "POTWIERDZENIE_ZGODNOSCI";
    public static final String CURRENCY_FIELD_CN = "CURRENCY";
    @Deprecated //u�ywa� AcceptanceMode
    public static final String _LEGACY_SIMPLE_ACCEPTANCE_FIELD_CN = "SIMPLE_ACCEPTANCE";
    public static final String WALUTA_FIELD_CN = "WALUTA";
    public static final String REJESTR_FIELD_CN = "REJESTR";

    public static final String DATA_WPLYWU_CN = "DATA_WPLYWU";
    public static final String DATA_KSIEGOWANIA_CN = "DATA_KSIEGOWANIA";
    public static final String LOKALIZACJA_CN = "LOKALIZACJA";
    public static final String DZIAL_FIELD_CN = "DZIAL";
    public static final String UMOWA_FIELD_CN = "UMOWA";

    public static final String AKCEPTACJA_PROSTA_CN = "AKCEPTACJA_PROSTA";
    public static final String AKCEPTACJA_BEZ_KWOTY_CN = "AKCEPTACJA_BEZ_KWOTY";
    /**
     * nazwa kodowa pola: Identyfikator procesu jbpm, je�li te pole == null to dokument idzie starym trybem - nie mo�na korzysta� z JBPMAcceptanceManager
     */
    public static final String JBPM_PROCESS_ID = "JBPM_PROCESS_ID";
    public static final String RODZAJ_FAKTURY_CN = "RODZAJ_FAKTURY";
    public static final String FAKTURA_SPECJALNA_CN = "FAKTURA_SPECJALNA";
    
    public static final String ZAIMPORTOWANO_DO_OF_CN = "ZAIMPORTOWANO_DO_OF";

    // statusy
    public static final Integer STATUS_W_TRAKCIE = 10;
    public static final Integer STATUS_ZAKSIEGOWANA = 20;
    public static final Integer STATUS_ODRZUCONA = 30;

    public static final String KOORDYNATOR_KSIEGOWOSC = "GUID_KOR_KSI";

    protected static final String[] MONTHS = new String[] {"Stycze�", "Luty", "Marzec",
                                           "Kwiecie�","Maj","Czerwiec","Lipiec","Sierpie�",
                                           "Wrzesie�","Pa�dziernik","Listopad","Grudzie�"};
    private static final String DIVISION_TO_DISCARD = "DIVISION_TO_DISCARD";

    //(thread safe)
    public static final Pattern GROUP_READ_PERMISSION_PATTERN = Pattern.compile("\\AINVOICE.*READ\\z");

    private static InvoiceLogic instance;

    public static synchronized InvoiceLogic getInstance()
    {
        if (instance == null)
            instance = new InvoiceLogic();
        return instance;
    }

    @Override
    public boolean isReadPermissionCode(String permCode) {
        return GROUP_READ_PERMISSION_PATTERN.matcher(permCode).matches();
    }

    @Override
    public void correctValues(Map<String,Object> values, DocumentKind kind) throws EdmException
    {
        // wg. specyfikacji, gdy odznaczono pole "Zap�acono" status ma zmienia� si�
        // automatycznie na "Zaksi�gowana"
        if(!kind.getCn().equals(DocumentLogicLoader.INVOICE_IC_KIND))
        {
            Boolean zaplacono = (Boolean) getValue(kind, values, ZAPLACONO_FIELD_CN);
            if (zaplacono != null && zaplacono.booleanValue())
                values.put(STATUS_FIELD_CN, STATUS_ZAKSIEGOWANA);
        }
    }

    public void archiveActionsVAT(Document document, int type) throws EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

        Calendar cal = Calendar.getInstance();

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Map<String, Object> values = new HashMap<String, Object>();
        Double oldVat = (Double) fm.getKey(VAT_CN);

        Double vat = (Double) fm.getKey(KWOTA_BRUTTO_FIELD_CN) - (Double) fm.getKey(KWOTA_NETTO_FIELD_CN);
        if(oldVat == null || !oldVat.equals(vat))
        {
            values.put(VAT_CN, vat);
        }
       // fm.initialize();
       // fm.initializeAcceptances();
        //if(fm.getValue(DATA_KSIEGOWANIA_CN) == null)
            //values.put(DATA_KSIEGOWANIA_CN, fm.getValue(DATA_WPLYWU_CN));

        if(fm.getKey(STATUS_FIELD_CN) == null)
            values.put(STATUS_FIELD_CN, 10);

        if(values != null && values.size() > 0)
            document.getDocumentKind().setOnly(document.getId(), values);

        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Archiwum Dokument�w Finansowych");
        folder = folder.createSubfolderIfNotPresent((String) fm.getValue(PION_CN));

        Date dat = (Date) fm.getValue(DATA_WYSTAWIENIA_FIELD_CN);
        cal.setTime(dat);

        folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR))+"-"+String.valueOf(cal.get(Calendar.MONTH)));
        document.setFolder(folder);

        DicInvoice inst = (DicInvoice) fm.getValue(DOSTAWCA_FIELD_CN);
        String summary = "Brak kontrahenta";
        if(inst != null)
        {
            summary = "Faktura kontrahenta nr."+inst.getNumerKontrahenta();
        }

        if (document instanceof OfficeDocument)
            ((OfficeDocument)document).setSummaryOnly(summary);

        document.setTitle(summary);
        document.setDescription(summary);
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

        if(document.getDocumentKind().equals(DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND)))
        {
            archiveActionsVAT(document, type);
            return;
        }

        Calendar cal = Calendar.getInstance();

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Folder folder = Folder.getRootFolder();

        Date dat = (Date) fm.getValue(DATA_WYSTAWIENIA_FIELD_CN);

        if(fm.getValue(DATA_ZAKSIEGOWANIA_FIELD_CN) != null) //bug fix 441
            dat = (Date) fm.getValue(DATA_ZAKSIEGOWANIA_FIELD_CN);

        cal.setTime(dat);
        folder = folder.createSubfolderIfNotPresent("Archiwum Faktur Kosztowych");
        folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)));
        folder = folder.createSubfolderIfNotPresent(String.valueOf(MONTHS[cal.get(Calendar.MONTH)]));
        document.setFolder(folder);

        DicInvoice inst = (DicInvoice) fm.getValue(DOSTAWCA_FIELD_CN);
        String title = inst.getName() + " (";
        if (fm.getDescription(NUMER_TRANSAKCJI_FIELD_CN) != null)
            title +=  fm.getDescription(NUMER_TRANSAKCJI_FIELD_CN) + ", ";
        title += fm.getDescription(NUMER_FAKTURY_FIELD_CN);
        title += ", "  +fm.getDescription(DATA_WYSTAWIENIA_FIELD_CN) + ")";
        String summary = inst.getName() + ", " + fm.getDescription(NUMER_FAKTURY_FIELD_CN) + ", " + fm.getDescription(DATA_WYSTAWIENIA_FIELD_CN);

        if (document instanceof OfficeDocument)
            ((OfficeDocument)document).setSummaryOnly(summary);

        document.setTitle(title);
        document.setDescription(title);
        
        if(fm.getBoolean(AKCEPTACJA_FINALNA_FIELD_CN))
        {
        	String fieldValue = (String) fm.getKey("EVENT_IDS");
			String[] ids = new String[0];
			if(fieldValue != null && fieldValue.length()>0)
			{
				ids = fieldValue.split(";");
			}
			for(int i=0;i<ids.length;i++)
			{
				EventFactory.setEventStatus(Long.valueOf(ids[i]), EventFactory.DONE_EVENT_STATUS);
			}
        }
    }
    
    @Override
    public void validate(Map<String, Object> values, DocumentKind kind,
    		Long documentId) throws EdmException
    {
    	Integer compliance = null;
    	if (values.get(POTWIERDZENIE_ZGODNOSCI_FIELD_CN) != null)
    		compliance = Integer.parseInt(values.get(POTWIERDZENIE_ZGODNOSCI_FIELD_CN).toString());
    	
    	Object agreement = values.get(UMOWA_FIELD_CN);
    	if (compliance != null && compliance.equals(30) && agreement != null)
    	{
    		values.put(UMOWA_FIELD_CN, null);
    		throw new EdmException("Nie mo�na wybra� umowy w przypadku wybrania opcji: Brak potwierdzenia umowy");
    	}
    }

    @Override
    public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

//        if (document instanceof OutOfficeDocument)
//            return;
    
       String error = "Nie mo�na zako�czy� pracy z dokumentem: ";
    
       FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

       if(document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_IC_KIND))
       {
            if(fm.getValue("NUMER_KSIEGOWANIA") == null || ((String)fm.getValue("NUMER_KSIEGOWANIA")).length() < 1  )
            {
                throw new AccessDeniedException(error + " numer ksi�gowania musi by� uzupe�niony");
            }
            if(fm.getValue("REJESTR") == null)
            {
                throw new AccessDeniedException(error + " rejest musi by� uzupe�niony");
            }
            fm.initializeAcceptances();

            if (fm.getBoolean(AKCEPTACJA_FINALNA_FIELD_CN)) {
                return;
            } else {
                Boolean simpleAcceptance = (IntercarsAcceptanceMode.from(fm) == IntercarsAcceptanceMode.SIMPLE2);

                if (!(fm.getAcceptancesState().isCentrumFieldAccepted(null,
                        simpleAcceptance) && fm.getAcceptancesState()
                        .isDocumentAccepted())) {
                    throw new AccessDeniedException(error
                            + " wszystkie akceptacje musza by� wykonane");
                }
            }
       }
       else if(document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND) || 
    		   document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_PTE))
       {
           if (STATUS_W_TRAKCIE.equals(fm.getKey(STATUS_FIELD_CN)))
                throw new AccessDeniedException(error + "Faktura musi by� odrzucona albo zaksi�gowana i zaakceptowana finalnie"); //debug.log


            if (STATUS_ZAKSIEGOWANA.equals(fm.getKey(STATUS_FIELD_CN)))
            {
                Boolean akcFinalna = (Boolean) fm.getKey(AKCEPTACJA_FINALNA_FIELD_CN);
                if (akcFinalna == null || !akcFinalna)
                    throw new AccessDeniedException(error+"Status musi by� ustawiony jako Odrzucona lub dokument musi by� zaakceptowany finalnie");
            }
       }
    }

    /**
     * Nadawanie wedlug nowej metody
     */
    public void documentPermissions(Document document) throws EdmException
    {
        Set<PermissionBean> perms = new HashSet<PermissionBean>();

//        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
//        List<CentrumKosztowDlaFaktury> list = (List<CentrumKosztowDlaFaktury>) fm.getValue(CENTRUM_KOSZTOWE_FIELD_CN);
//
//        for (CentrumKosztowDlaFaktury centrum : list)
//        {
//            //createPermission(document, "Faktury kosztowe - MPK "+centrum.getCentrumCode()+" - odczyt", "INVOICE_"+centrum.getCentrumId()+"_READ", new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS});
//            String readName = "Faktury kosztowe - MPK "+centrum.getCentrumCode()+" - odczyt";
//            String readGuid = "INVOICE_"+centrum.getCentrumId()+"_READ";
//
//            perms.add(new PermissionBean(ObjectPermission.READ,readGuid,ObjectPermission.GROUP,readName));
//            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,readGuid,ObjectPermission.GROUP,readName));
//
//            //createPermission(document, "Faktury kosztowe - MPK "+centrum.getCentrumCode()+" - modyfikacja", "INVOICE_"+centrum.getCentrumId()+"_MODIFY", new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS});
//
//            String writeName = "Faktury kosztowe - MPK "+centrum.getCentrumCode()+" - modyfikacja";
//            String writeGuid = "INVOICE_"+centrum.getCentrumId()+"_MODIFY";
//
//            perms.add(new PermissionBean(ObjectPermission.MODIFY, writeGuid,ObjectPermission.GROUP,writeName));
//            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, writeGuid,ObjectPermission.GROUP,writeName));
//
//            AccountNumber an = centrum.getAccountNumber() != null ? AccountNumber.findByNumber(centrum.getAccountNumber()) : null;
//            if(document.getDocumentKind().getCn().equals(DocumentKind.INVOICE_KIND) && an != null)
//            {
//                //createPermission(document, "Faktury kosztowe - KONTO "+an.getNumber()+" - odczyt", "INVOICE_ACCOUNT_"+an.getId()+"_READ", new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS});
//
//                readName = "Faktury kosztowe - KONTO "+an.getNumber()+" - odczyt";
//                readGuid = "INVOICE_ACCOUNT_"+an.getId()+"_READ";
//
//                perms.add(new PermissionBean(ObjectPermission.READ,readGuid,ObjectPermission.GROUP,readName));
//                perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,readGuid,ObjectPermission.GROUP,readName));
//
//            }
//        }
        //DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "INVOICE_READ", ObjectPermission.GROUP);
        perms.add(new PermissionBean(ObjectPermission.READ,"INVOICE_READ",ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"INVOICE_READ",ObjectPermission.GROUP));

        if( DocumentLogicLoader.INVOICE_IC_KIND.equals(document.getDocumentKind().getCn()))
        {
            //wydaje mi sie ze INVOICE IC ma juz swoja logike i jest to niepotrzebne
            //DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.READ_ATTACHMENTS}, "INVOICE_MODIFY", ObjectPermission.GROUP);
            perms.add(new PermissionBean(ObjectPermission.MODIFY,"INVOICE_MODIFY",ObjectPermission.GROUP));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"INVOICE_MODIFY",ObjectPermission.GROUP));

            //DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "INVOICE_ADMIN", ObjectPermission.GROUP);
            perms.add(new PermissionBean(ObjectPermission.MODIFY,"INVOICE_ADMIN",ObjectPermission.GROUP));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"INVOICE_ADMIN",ObjectPermission.GROUP));
        }
        else
        {
            //DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "INVOICE_MODIFY", ObjectPermission.GROUP);
            perms.add(new PermissionBean(ObjectPermission.MODIFY,"INVOICE_MODIFY",ObjectPermission.GROUP));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"INVOICE_MODIFY",ObjectPermission.GROUP));
        }

        setUpPermission(document, perms);
    }

    /**
     * W InvoiceLogic jest jest inny GUID dla tworzonych grup
     */
    @Override
    protected String getBaseGUID(Document document) throws EdmException
    {
        String parentGuid = document.getDocumentKind().getProperties().get(DocumentKind.CENTRA_DIVISION);
        DSDivision parent = DSDivision.find(parentGuid != null ? parentGuid : DSDivision.ROOT_GUID);
        return parent.getGuid();
    }

    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
    {
        String channel = document.getDocumentKind().getChannel(document.getId());
        if(channel != null)
        {
             if( ProcessCoordinator.find(DocumentLogicLoader.INVOICE_IC_KIND,channel) != null)
                    return ProcessCoordinator.find(DocumentLogicLoader.INVOICE_IC_KIND,channel).get(0);
             else
                 return null;
        }
        return null;
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        if(fm.getField(KWOTA_NETTO_FIELD_CN) != null)
        {
            values.put(LOKALIZACJA_CN, 267);
        }
        if (DocumentLogic.TYPE_ARCHIVE != type)
            values.put(STATUS_FIELD_CN, STATUS_W_TRAKCIE);
        if(DocumentLogicLoader.INVOICE_IC_KIND.equals(fm.getDocumentKind().getCn()))
        {
            values.put(WALUTA_FIELD_CN, 290);
        }

        fm.reloadValues(values);
    }


    /**
     * Przekazuje true <=> dla dokument�w tego rodzaju mo�na generowa� obraz dokumentu w postaci PDF.
     */
    public boolean canGenerateDocumentView()
    {
        return true;
    }

    /**
     * Dla podanego dokumentu generowany jest jego obraz w postaci PDF.
     *
     * @return wygenerowany plik PDF
     */
    public File generateDocumentView(Document document) throws EdmException
    {
        File pdf = null;
        try
        {
            pdf = File.createTempFile("docusafe_tmp_", ".pdf");
            OutputStream os = new BufferedOutputStream(new FileOutputStream(pdf));

            PdfUtils.attachmentsAsPdf(new Long[] {document.getId()},
                    /*uwagi*/false, os,
                    /*wstep*/createDocumentSummary(document),
                    /*keywords*/null,
                    /*zako�czenie*/null,
                    /*pomini�te na pocz�tku*/false);
            os.close();

            return pdf;
        }
        catch (Exception e)
        {
        	log.error("",e);
            if (pdf != null)
                pdf.delete();
            throw new EdmException("B��d podczas generowania obrazu dokumentu", e);
        }
    }

    /**
     * Generuje elementu dokumentu PDF opisuj�cy g��wne informacje danego dokumentu.
     */
    @SuppressWarnings("unchecked")
    private Element createDocumentSummary(Document document) throws EdmException
    {
        StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

        DocumentKind documentKind = document.getDocumentKind();
        FieldsManager fm = documentKind.getFieldsManager(document.getId());
        fm.initialize();
        fm.initializeAcceptances();

        OfficeDocument officeDocument = null;
        if (document instanceof OfficeDocument)
            officeDocument = (OfficeDocument) document;

        try
        {
            File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(baseFont, 18);
            Font smallFont = new Font(baseFont, 14);

            // PODSTAWOWE DANE
            Paragraph paragraph = new Paragraph("FAKTURA KOSZTOWA", font);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setSpacingBefore(15);
            Chapter beginning = new Chapter(paragraph, 2);
            beginning.setNumberDepth(0);

            if (officeDocument != null && officeDocument.getOfficeNumber() != null)
                beginning.add(createParagraph(sm.getString("NumerKancelaryjny")+": "+officeDocument.getOfficeNumber()+"\n\n", Element.ALIGN_CENTER, font));
            else
                beginning.add(createParagraph(sm.getString("Identyfikator")+": "+document.getId()+"\n\n", Element.ALIGN_CENTER, font));

            beginning.add(new Paragraph(sm.getString("DokumentBiznesowy"), font));

            if (officeDocument != null && officeDocument.getType() == DocumentType.INCOMING)
                beginning.add(new Paragraph(sm.getString("DataPrzyjecia")+": "+DateUtils.formatCommonDate(((InOfficeDocument) officeDocument).getIncomingDate()), font));
            else
                beginning.add(new Paragraph(sm.getString("DataPrzyjecia")+": "+DateUtils.formatCommonDate(document.getCtime()), font));

            // ATRYBUTY BIZNESOWE
            beginning.add(createParagraph(fm, DATA_WYSTAWIENIA_FIELD_CN, font));
            beginning.add(createParagraph(fm, DATA_PLATNOSCI_FIELD_CN, font));
            beginning.add(createParagraph(fm, DATA_ZAPLATY_FIELD_CN, font));
            if(fm.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND))
                beginning.add(createParagraph(fm, DATA_ZAKSIEGOWANIA_FIELD_CN, font));
            beginning.add(createParagraph(fm, NUMER_FAKTURY_FIELD_CN, font));
            beginning.add(createParagraph(fm, DOSTAWCA_FIELD_CN, font));
            if(fm.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND))
                beginning.add(createParagraph(fm, KWOTA_BRUTTO_FIELD_CN, font));
            else
                beginning.add(createParagraph(fm, KWOTA_NETTO_FIELD_CN, font));
            // NUMER KONTA BANKOWEGO
            beginning.add(createParagraph(fm, NUMER_RACHUNKU_BANKOWEGO_CN, font));
            beginning.add(new Paragraph(" "));

            // OPIS TOWARU
            if(fm.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND))
                beginning.add(createParagraph(fm, OPIS_TOWARU_FIELD_CN, font));
            beginning.add(new Paragraph(" "));

            // AKCEPTACJE OG�LNE
            beginning.add(createParagraph("Akceptacje og�lne", Element.ALIGN_CENTER, font));

            for (DocumentAcceptance documentAcceptance : fm.getAcceptancesState().getGeneralAcceptances())
                beginning.add(new Paragraph(documentAcceptance.getDescription(), font));

            beginning.add(new Paragraph(" "));

            // AKCEPTACJE CENTRUM KOSZTOWEGO
            paragraph = createParagraph("Akceptacje centrum kosztowego", Element.ALIGN_CENTER, font);
            beginning.add(paragraph);

            int[] widths = new int[1+fm.getAcceptancesDefinition().getFieldAcceptances().size()];
            for (int i=0; i < widths.length; i++)
                widths[i] = 1;
            PdfPTable table = new PdfPTable(widths.length);
            table.setWidths(widths);
            table.setWidthPercentage(100);

            // tworzenie nag��wka tabeli
            table.addCell(new Phrase("MPK (Kwota)", font));
            for (Acceptance acceptance : fm.getAcceptancesDefinition().getFieldAcceptances().values())
            {
                table.addCell(new Phrase(acceptance.getName(), font));
            }

            // tworzenie tre�ci tabeli
            String numerKontaTmp = "";
            List<CentrumKosztowDlaFaktury> list = (List<CentrumKosztowDlaFaktury>) fm.getValue(fm.getAcceptancesDefinition().getCentrumKosztFieldCn());
            for (CentrumKosztowDlaFaktury centrum : list)
            {
                CentrumKosztow ck = CentrumKosztow.find(centrum.getCentrumId());

                if(centrum.getAccountNumber() != null)
                {
                    AccountNumber an = AccountNumber.findByNumber(centrum.getAccountNumber());
                    numerKontaTmp = an.getNumber()+" "+an.getName();
                }
                else
                    numerKontaTmp = "(brak konta)";
                table.addCell(new Phrase(ck.getSymbol()+" "+ck.getName()+"\n"+numerKontaTmp+"\nKwota "+centrum.getAmount(), font));
                for (String acceptanceCn : fm.getAcceptancesDefinition().getFieldAcceptances().keySet())
                {
                    DocumentAcceptance documentAcceptance = fm.getAcceptancesState().getFieldAcceptance(acceptanceCn, centrum.getId());
                    table.addCell(new Phrase(documentAcceptance != null ? documentAcceptance.getDescriptionWithoutCn() : "", font));
                }
            }

            table.setSpacingBefore(12);
            beginning.add(table);

            beginning.add(new Paragraph(" "));

            // UWAGI
            if (officeDocument != null)
            {
                List lstRemarks = officeDocument.getRemarks();
                com.lowagie.text.List remarks = new com.lowagie.text.List(false,10);
                boolean done = false;
                for (Iterator iter=lstRemarks.iterator(); iter.hasNext(); )
                {
                    if (!done) {
                        paragraph = new Paragraph(sm.getString("Uwagi")+" :", smallFont);
                        paragraph.setSpacingBefore(20);
                        paragraph.setAlignment(Element.ALIGN_CENTER);
                        beginning.add(paragraph);
                        done = true;
                    }
                    Remark remark = (Remark) iter.next();
                    remarks.add(new ListItem(remark.getContent()+" ("+DSUser.safeToFirstnameLastname(remark.getAuthor())+" "+DateUtils.formatJsDateTime(remark.getCtime())+")", smallFont));
                }
                beginning.add(remarks);
            }
            return beginning;
        }
        catch (DocumentException e)
        {
        }
        catch (IOException e)
        {
        }
        return null;
    }

    /**
     * Tworzy paragraf dla dokumentu PDF opisuj�cy warto�� danego pola z dockinda.
     */
    private Paragraph createParagraph(FieldsManager fm, String fieldCn, Font font) throws EdmException
    {
        Field field = fm.getField(fieldCn);
        Object description = fm.getDescription(fieldCn);
        return new Paragraph(field.getName()+": "+(description != null ? description.toString() : ""), font);
    }

    /**
     * Tworzy paragraf zawieraj�cy dany tekst.
     */
    private Paragraph createParagraph(String text, int alignment, Font font) throws EdmException
    {
        Paragraph paragraph = new Paragraph(text, font);
        paragraph.setAlignment(alignment);
        return paragraph;
    }

    /**
     * Zwraca polityk� bezpiecze�stwa dla dokument�w tego rodzaju.
     */
    public int getPermissionPolicyMode()
    {
        return PermissionManager.BUSINESS_LIGHT_POLICY;
    }

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException {
        return TaskListParams.fromMagicArray(getTaskListParam(kind, documentId));
    }

    private Object[] getTaskListParam(DocumentKind dockind,Long id) throws EdmException
    {
        Object[] result = new Object[10];
        StringBuilder tmp = new StringBuilder();
        List<CentrumKosztowDlaFaktury> centrumList =  CentrumKosztowDlaFaktury.findByDocumentId(id);
        for (CentrumKosztowDlaFaktury centrum : centrumList)
        {
            tmp.append(centrum.getDescription()+" | ");
        }
        FieldsManager fm = dockind.getFieldsManager(id);
        DicInvoice dic = (DicInvoice) fm.getValue(DOSTAWCA_FIELD_CN);

        result[0] = fm.getValue(STATUS_FIELD_CN);

        //NumberFormat nf = new DecimalFormat("############0.00");
        //result[2] = new BigDecimal((Double) fm.getValue(KWOTA_BRUTTO_FIELD_CN)).setScale(2);
        //result[2] = String.valueOf(fm.getValue(KWOTA_BRUTTO_FIELD_CN));
        LoggerFactory.getLogger("tomekl").debug("kb {}",fm.getValue(KWOTA_BRUTTO_FIELD_CN));
        result[4] = StringUtils.left(tmp.toString(), 20);
        result[5] =  dic != null ? StringUtils.left(dic.getNumerKontrahenta().toString(), 20) : "brak";
        result[6] = fm.getValue(DATA_PLATNOSCI_FIELD_CN) != null ? DateUtils.formatCommonDate((Date)fm.getValue(DATA_PLATNOSCI_FIELD_CN)) : "brak";
        //result[7] = fm.getValue(NAZWA_PROJEKTU_FIELD_CN);
        result[7] = String.valueOf(fm.getValue(KWOTA_BRUTTO_FIELD_CN));
        result[8] = fm.getValue(DATA_WYSTAWIENIA_FIELD_CN);

        return result;
    }

    public boolean canReadDocumentDictionaries() throws EdmException
    {
        return true;
    }

    public void discard(Document doc, String act) throws EdmException
    {
        WorkflowFactory.getInstance().manualFinish(act, false);

        FieldsManager fm = doc.getDocumentKind().getFieldsManager(doc.getId());
        Calendar cal = Calendar.getInstance();

        fm.initialize();
        Map<String, Object> values = fm.getFieldValues();
        values.put(STATUS_FIELD_CN, 6);

        doc.getDocumentKind().setWithHistory(doc.getId(), values, false);

        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Archiwum Dokument�w Finansowych");
        folder = folder.createSubfolderIfNotPresent("Dokumenty odrzucone");

        Date dat = (Date) fm.getValue(DATA_WYSTAWIENIA_FIELD_CN);
        cal.setTime(dat);

        folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR))+"-"+String.valueOf(cal.get(Calendar.MONTH)));

        doc.setFolder(folder);
    }

    public void returnToIM(OfficeDocument doc, String activity,ActionEvent event) throws EdmException
    {
        String[] documentActivity = new String[1];
        documentActivity[0] = activity;
        String plannedAssignment = "";

        plannedAssignment = InvoiceLogic.DIVISION_TO_DISCARD+"//"+WorkflowFactory.wfManualName()+"/*/realizacja";

        String curUser = DSApi.context().getDSUser().getName();

        WorkflowFactory.multiAssignment(documentActivity,curUser, new String[] {plannedAssignment}, plannedAssignment, event);
    }



    public void discardToUser(OfficeDocument doc, String activity,ActionEvent event,DSUser user) throws EdmException
    {
        StringManager sm = GlobalPreferences.loadPropertiesFile(AcceptancesManager.class.getPackage().getName(),null);
        FieldsManager fm = doc.getDocumentKind().getFieldsManager(doc.getId());
        fm.initialize();
        fm.initializeAcceptances();
        List<DocumentAcceptance> daList = fm.getAcceptancesState().getFieldAcceptances();
        daList.addAll(fm.getAcceptancesState().getGeneralAcceptances());
        DocumentAcceptance acceptance = null;
        for (DocumentAcceptance documentAcceptance : daList)
        {
            if(documentAcceptance.getUsername().equals(user.getName()))
            {
                acceptance = documentAcceptance;
                break;
            }
        }
        if(acceptance == null)
        {
            event.addActionError("Nie znaleziono akceptacji");
            return;
        }
        Map<String, Object> values = fm.getFieldValues();
        if(acceptance.getAcceptanceCn().equals("zwykla"))
        {
            values.put("AKCEPTACJA_ZWYKLA", 0);
        }

        Long objectId = acceptance.getObjectId();

        Persister.delete(acceptance);

        String cn = acceptance.getAcceptanceCn();
        while((cn = doc.getDocumentKind().getAcceptancesDefinition().getNextCn(cn)) != null)
        {
            log.debug("InvoiceLogic.java:cn.prev = {}",cn);

            DocumentAcceptance ac = DocumentAcceptance.find(doc.getId(), cn, objectId);
            if(ac == null){
                ac = DocumentAcceptance.find(doc.getId(), cn, null);
            }
            if(ac != null){
                Persister.delete(ac);
                if(objectId != null)
                {
                    if (doc instanceof OfficeDocument)
                    {
                        CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance().find(objectId);
                        String tmp = sm.getString("CofnietoAkceptacjeDokumenuDlaCentrumKosztowego",ac.getAcceptanceCn(),centrum.getCentrumCode());
                        ((OfficeDocument) doc).addWorkHistoryEntry(
                            Audit.create("withdrawAccept", DSApi.context().getPrincipalName(),
                                tmp,ac.getAcceptanceCn(), null));
                    }
                }
                else
                {
                    if (doc instanceof OfficeDocument)
                    {
                        String tmp = sm.getString("CofnietoAkceptacjeDokumenu",ac.getAcceptanceCn());
                        ((OfficeDocument) doc).addWorkHistoryEntry(
                            Audit.create("withdrawAccept", DSApi.context().getPrincipalName(),
                                tmp,ac.getAcceptanceCn(), null));
                    }
                }
                event.addActionMessage(sm.getString("Usunieto") + " "+ ac.getAsFirstnameLastname() + ' ' + ac.getDescription());

            }
        }

        if(objectId != null)
        {
            if (doc instanceof OfficeDocument)
            {
                CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance().find(objectId);
                String tmp = sm.getString("CofnietoAkceptacjeDokumenuDlaCentrumKosztowego",acceptance.getAcceptanceCn(),centrum.getCentrumCode());
                ((OfficeDocument) doc).addWorkHistoryEntry(
                    Audit.create("withdrawAccept", DSApi.context().getPrincipalName(),
                        tmp,acceptance.getAcceptanceCn(), null));
            }
        }
        else
        {
            if (doc instanceof OfficeDocument)
            {
                String tmp = sm.getString("CofnietoAkceptacjeDokumenu",acceptance.getAcceptanceCn());
                ((OfficeDocument) doc).addWorkHistoryEntry(
                    Audit.create("withdrawAccept", DSApi.context().getPrincipalName(),
                        tmp,acceptance.getAcceptanceCn(), null));
            }
        }

        values.put(STATUS_FIELD_CN, 6);

        doc.getDocumentKind().setWithHistory(doc.getId(), values, false);



    }

    public void correctImportValues(Document document, Map<String, Object> values, StringBuilder builder) throws EdmException
    {
        DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND);
        Map<String, Object> valuesTmp = new HashMap<String, Object>(values);
        Set<String> valuesSet = valuesTmp.keySet();
        if(document instanceof InOfficeDocument)
        {
            ((InOfficeDocument)document).setSummary(documentKind.getName());
        }

        if(document instanceof InOfficeDocument && values.get("DOSTAWCA") != null)
        {
            DicInvoice dic = DicInvoice.getInstance().find(new Long(values.get("DOSTAWCA").toString()));
            if(dic != null)
            {
                Sender sender = new Sender();

                if(dic.getName() != null)
                    sender.setFirstname(TextUtils.trimmedStringOrNull(dic.getName(), 50));
                if(dic.getMiejscowosc() != null)
                    sender.setLocation(TextUtils.trimmedStringOrNull(dic.getMiejscowosc() , 50));
                if(dic.getKod() != null)
                    sender.setZip(TextUtils.trimmedStringOrNull(dic.getKod(), 6));
                if(dic.getNip() != null)
                {
                    sender.setNip(TextUtils.trimmedStringOrNull(dic.getNip(), 20));
                }
                if(dic.getUlica() != null)
                    sender.setStreet(TextUtils.trimmedStringOrNull(dic.getUlica(), 50));
                if(dic.getNumerKontaBankowego() != null)
                    sender.setRemarks(TextUtils.trimmedStringOrNull(dic.getNumerKontaBankowego(), 200));
                sender.create();
                ((InOfficeDocument)document).setSender(sender);
            }
        }
    }

    public static List<InvoiceInfo> parseAndSaveFVZakupFile(File file) throws EdmException
    {
        BufferedReader reader = null;
        List<InvoiceInfo> list = new ArrayList<InvoiceInfo>();
        try
        {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "cp1250"));
            String line;
            int lineCount = 0;
            while ((line = reader.readLine()) != null)
            {

                lineCount++;
                line = line.replace("\"","");

                line = line.trim();
                // pierwsza linia to nag��wek, kt�ry omijam
                if (lineCount == 1 || line.startsWith("#"))
                    continue;

                InvoiceInfo info = new InvoiceInfo();
                info.setCtime(new Date());
                info.setCreatingUser(DSApi.context().getPrincipalName());
                info.setLineNumber(lineCount);
                info.setProcessStatus(InvoiceInfo.PROCESS_STATUS_NEW);
                info.setInvoiceType(InvoiceInfo.INVOICE_TYPE_ZAKUP);

                String[] parts = line.split(";");

                if (parts.length > 10)
                {
                    if(parts[0].length()==0) continue;
                    info.setInvoiceNumber(parts[3].trim());

                    try
                    {
                        info.setInvDate(DateUtils.parseDateAnyFormat(parts[4].replace(".", "-")));
                    }
                    catch(Exception e)
                    {
                        throw new EdmException(e);
                    }
                    info.setNumerKontrahenta(parts[5].trim());

                    String amount = parts[10].trim();
                    amount = amount.replace("\"", "");
                    amount = amount.replace(" ", "");
                    amount = amount.replace(",", ".");
                    try
                    {
                        info.setAmountBrutto(Double.parseDouble(amount));
                    }
                    catch (NumberFormatException e)
                    {
                        log.debug("", e);
                        info.setStatus(InvoiceInfo.STATUS_PARSE_ERROR);
                        info.setErrorInfo("niepoprawna kwota");
                        info.setAmountBrutto(null);
                    }
                }
                else
                {
                    info.setStatus(InvoiceInfo.STATUS_PARSE_ERROR);
                    info.setErrorInfo("nie podano wszystkich informacji");
                }

                list.add(info);

                if (info.getStatus() == null)
                {
                    // poprawnie odczytano z pliku - szukamy odpowiedni dokument
                    DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND);
                    DockindQuery query = new DockindQuery(0,0);
                    query.setDocumentKind(kind);

                    query.field(kind.getFieldByCn(InvoiceLogic.NUMER_FAKTURY_FIELD_CN), info.getInvoiceNumber());
                    query.otherClassField(kind.getFieldByCn(InvoiceLogic.DOSTAWCA_FIELD_CN),"numerKontrahenta", info.getNumerKontrahenta());
                    //query.field(kind.getFieldByCn(InvoiceLogic.REJESTR_FIELD_CN),info.getNip());
                    SearchResults<Document> searchResults = DocumentKindsManager.search(query);
                    if (searchResults.totalCount() == 0)
                        info.setStatus(InvoiceInfo.STATUS_NOT_FOUND);
                    else
                    {
                        Document[] documents = searchResults.results();
                        checkDocument(documents[0], info);

                        if (documents.length > 1)
                        {
                            info.setStatus(InvoiceInfo.STATUS_TOO_MANY_FOUND);
                            for (int i=1; i < documents.length; i++)
                            {
                                InvoiceInfo copy = new InvoiceInfo();
                                copy.setAmountBrutto(info.getAmountBrutto());
                                copy.setCreatingUser(info.getCreatingUser());
                                copy.setCtime(info.getCtime());
                                copy.setInvoiceNumber(info.getInvoiceNumber());
                                copy.setLineNumber(info.getLineNumber());
                                copy.setInvoiceType(InvoiceInfo.INVOICE_TYPE_ZAKUP);
                                checkDocument(documents[i], copy);
                                copy.setStatus(InvoiceInfo.STATUS_TOO_MANY_FOUND);
                                Persister.create(copy);
                                list.add(copy);
                            }
                        }
                    }
                }
                Persister.create(info);

            }
            return list;
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas czytania pliku z rejestrem VAT");
        }
        catch(Exception e)
        {
            throw new EdmException(e);
        }
        finally
        {
            if (reader!=null)
            {
                try { reader.close(); } catch (Exception e1) { }
            }
        }
    }

    public static List<Map<String,Object>> prepareInvoice2Beans(List<InvoiceInfo> invoiceInfos) throws EdmException
    {
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        for (InvoiceInfo info : invoiceInfos)
        {
            Map<String,Object> bean = new LinkedHashMap<String, Object>();
            bean.put("id", info.getId());
            bean.put("lineNumber", info.getLineNumber());
            bean.put("status", info.getStatus());
            bean.put("statusDescription", info.getStatusDescription());
            bean.put("invoiceNumber", info.getInvoiceNumber());
            bean.put("amount", info.getAmountBrutto());
            bean.put("documentId", info.getDocumentId());
            bean.put("processStatus", info.getProcessStatus());
            bean.put("origAmount", info.getOrigAmountBrutto() == null ? 0 : info.getOrigAmountBrutto());
            bean.put("numerKontrahenta", info.getNumerKontrahenta());
            if (info.getDocumentId() != null)
            {
                Document document = Document.find(info.getDocumentId());
                if (document.getAttachments() != null && document.getAttachments().size() > 0)
                {
                    AttachmentRevision revision = document.getAttachments().get(0).getMostRecentRevision();
                    if (revision != null && ViewServer.mimeAcceptable(revision.getMime()))
                    {
                        bean.put("viewerLink", "/viewserver/viewer.action?id="+revision.getId()+"&fax=false&width=1000&height=750");
                    }
                }
            }
            list.add(bean);
        }
        return list;
    }

    public static void deleteInvoiceInfos(String username, Integer type) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement("delete from DSG_INVOICE_INFO where creatingUser = ? and invoiceType=?");
            ps.setString(1, username);
            ps.setInt(2, type);
            ps.executeUpdate();

        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
        }
    }

    public static void deleteInvoiceInfos(String username, Long[] ids) throws EdmException
    {
        PreparedStatement ps = null;

        for(Long id:ids)
        {
            try
            {
                ps = DSApi.context().prepareStatement("delete from DSG_INVOICE_INFO where creatingUser = ? and id = ?");
                ps.setString(1, username);
                ps.setLong(2, id);
                ps.executeUpdate();

            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }
            finally
            {
                DSApi.context().closeStatement(ps);
            }
        }
    }

    public static void chanegeProcessStatusInvoiceInfos(String username, Long[] ids, Integer processStatus) throws EdmException
    {
        PreparedStatement ps = null;

        for(Long id:ids)
        {
            try
            {
                ps = DSApi.context().prepareStatement("update DSG_INVOICE_INFO set processStatus = ? where creatingUser = ? and id = ?");
                ps.setInt(1, processStatus);
                ps.setString(2, username);
                ps.setLong(3, id);
                ps.executeUpdate();

            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }
            finally
            {
                DSApi.context().closeStatement(ps);
            }
        }
    }

    private static void checkDocument(Document document, InvoiceInfo info) throws EdmException
    {
        info.setDocumentId(document.getId());
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Double amount = (Double) fm.getKey(InvoiceLogic.KWOTA_BRUTTO_FIELD_CN);
        info.setOrigAmountBrutto(amount);
        try
        {
            info.setStatus(InvoiceInfo.STATUS_OK);
            if(!info.getAmountBrutto().equals(info.getOrigAmountBrutto()))
        	{
        		info.setStatus(InvoiceInfo.STATUS_WRONG_AMOUNT);
        	}
            if(info.getInvoiceType().equals(InvoiceInfo.INVOICE_TYPE_KOSZT) && (((DicInvoice)fm.getValue(InvoiceLogic.DOSTAWCA_FIELD_CN)).getNip()!=null && !((DicInvoice)fm.getValue(InvoiceLogic.DOSTAWCA_FIELD_CN)).getNip().equals("")))
            {
                if(!((DicInvoice)fm.getValue(InvoiceLogic.DOSTAWCA_FIELD_CN)).getNip().replaceAll("\\D", "").equals(info.getNip()))
                    info.setStatus(InvoiceInfo.STATUS_WRONG_NIP);
            }
            else if(((Date)fm.getValue(DATA_WYSTAWIENIA_FIELD_CN)).compareTo(info.getInvDate())!=0)
                info.setStatus(InvoiceInfo.STATUS_WRONG_DATE);
            else if (!info.getAmountBrutto().equals(amount))
                info.setStatus(InvoiceInfo.STATUS_NOT_EVERYTHING_MATCH);

        }
        catch(Exception e){
            new EdmException(e);
            info.setStatus(InvoiceInfo.STATUS_NOT_EVERYTHING_MATCH);
        }
    }

    
    /** to trzeba wywalic, logika nie moze miec dostepu do akcji **/
    @Deprecated
    public void passToAuthorizedPerson(String activityId,String skip_EV_FILL,ActionEvent event, Map<Integer, String> centrumAcceptanceCn, Long documentId) throws EdmException
    {
        for(Integer objectId :centrumAcceptanceCn.keySet())
        {
             CentrumKosztowDlaFaktury centrumKosztowe = CentrumKosztowDlaFaktury.getInstance().find(Long.valueOf(objectId));
             Integer centrunId = centrumKosztowe.getAcceptingCentrumId();
             List<CentrumKosztowDlaFaktury> list = CentrumKosztowDlaFaktury.find(documentId, centrunId);
             BigDecimal fullCentrumAmount = new BigDecimal(0);
             for (CentrumKosztowDlaFaktury faktury : list)
             {
            	 fullCentrumAmount = fullCentrumAmount.add(faktury.getRealAmount());
             }
             List<AcceptanceCondition> acc = AcceptanceCondition.findByCentrumAmount(centrunId,fullCentrumAmount);
             if(acc == null || acc.size() < 1)
             {
                 throw new EdmException("Nie masz prawa do wykonania akceptacji centrum na kwote "+fullCentrumAmount +
                 		" Nie znaleziono osoby mog�cej zaakceptowa� dany MPK");
             }
             acc.get(0).getUsername();
             WorkflowFactory.simpleAssignment(activityId,
            		 acc.get(0).getDivisionGuid(), 
            		 acc.get(0).getUsername(), 
					DSApi.context().getPrincipalName(), 
					null, 
					acc.get(0).getDivisionGuid() != null ? WorkflowFactory.SCOPE_DIVISION : WorkflowFactory.SCOPE_ONEUSER, 
					null, 
					null);
             event.addActionMessage("Nie masz prawa do wykonania akceptacji centrum na kwote "+fullCentrumAmount+". Zadnaie przekazano do dalszych akceptacji");
             event.skip(skip_EV_FILL);
			 event.setResult("confirm");
        }
    }

	public void discardToCn(OfficeDocument document, String activity, ActionEvent event, String cn, Object objectId) throws EdmException {
		throw new UnsupportedOperationException("discardToCn not supported");
	}
	
	/**
	 * Metoda zwraca nastepna akceptacje dla faktury
	 * @param document
	 * @return AcceptanceCondition
	 * @throws EdmException
	 */
	public AcceptanceCondition nextAcceptanceCondition(Document document) throws EdmException
	{
		final String SZEFA_CENTRUM = "szefa_centrum";
		final String KSIEGOWA = "ksiegowa";
		
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		fm.initialize();
		fm.initializeAcceptances();
		
		/** CENTRA KOSZTOW {SZEFA_CENTRUM} **/
		for(CentrumKosztowDlaFaktury centrum : (List<CentrumKosztowDlaFaktury>) fm.getValue(CENTRUM_KOSZTOWE_FIELD_CN))	
		{
			if(fm.getAcceptancesState().getFieldAcceptance(SZEFA_CENTRUM, centrum.getId()) == null) //brak akceptacji szefa centrum
			{
				List<AcceptanceCondition> res = AcceptanceCondition.findByAmount(centrum.getCentrumId(), centrum.getRealAmount(), SZEFA_CENTRUM, fm.getBoolean(AKCEPTACJA_BEZ_KWOTY_CN)); //narazie na slepo
				if (res.size() > 0)
					return res.get(0);
				throw new EdmException("Nie wyznaczono szefa dla centrum kosztowego z odpowiednia sum�!");
			}
		}
		
		/** OGOLNE {KSIEGOWA} **/
		
		Double centrumAmmount = (Double) fm.getValue(KWOTA_BRUTTO_FIELD_CN);
		BigDecimal fullCentrumAmount = BigDecimal.valueOf(centrumAmmount); //new BigDecimal(0);
		
		return AcceptanceCondition.findByAmount(null, fullCentrumAmount, KSIEGOWA).get(0); //narazie na slepo		
	}
	
	public boolean canChangeDockind(Document document) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		Boolean of = (Boolean) fm.getKey(ZAIMPORTOWANO_DO_OF_CN);
		if(of == null)
			return true;
		else
			return !of.booleanValue();		
	}
	
	public void onSendDecretation(Document document,AcceptanceCondition condition) throws EdmException 
	{
		try
		{
			//kasowanie
			FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
			String fieldValue = (String) fm.getKey("EVENT_IDS");
			String[] ids = new String[0];
			if(fieldValue != null && fieldValue.length()>0)
			{
				ids = fieldValue.split(";");
			}
			for(int i=0;i<ids.length;i++)
			{
				EventFactory.setEventStatus(Long.valueOf(ids[i]), EventFactory.DONE_EVENT_STATUS);
			}
			
			//zapisywanie
			StringBuffer eventIds = new StringBuffer();
			Integer invoiceDays = DSApi.context().systemPreferences().node("invoice-mail").getInt("invoiceDays", 0);
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR)+invoiceDays);
			
			
			if(condition.getUsername() != null)
			{
				EventFactory.registerEvent("immediateTrigger", "DocumentWatchSender",document.getId()+";"+condition.getUsername()+";"+Type.INVOICE_ASSIGNED.getValue(),null, new Date());
				if(invoiceDays > 0)
				{
					Long id = EventFactory.registerEvent("immediateTrigger", "DocumentWatchSender",document.getId()+";"+condition.getUsername()+";"+Type.INVOICE_ASSIGNED.getValue(),null, cal.getTime());
					eventIds.append(id);
					eventIds.append(';');
				}
			}
			else if(condition.getDivisionGuid() != null)
			{
				for(DSUser user : DSDivision.find(condition.getDivisionGuid()).getUsers())
				{
					EventFactory.registerEvent("immediateTrigger", "DocumentWatchSender",document.getId()+";"+user.getName()+";"+Type.INVOICE_ASSIGNED.getValue(),null, new Date());
					if(invoiceDays > 0)
					{
						Long id = EventFactory.registerEvent("immediateTrigger", "DocumentWatchSender",document.getId()+";"+user.getName()+";"+Type.INVOICE_ASSIGNED.getValue(),null, cal.getTime());
						eventIds.append(id);
						eventIds.append(';');
					}
				}
			}
			else
			{
				throw new EdmException("brak informacji o dziale/osobie do maila");
			}
			Map<String, Object> values = new HashMap<String, Object>();
	        values.put("EVENT_IDS", eventIds.toString());
	        document.getDocumentKind().setOnly(document.getId(), values);
		}
		catch (Exception e) 
		{
			log.debug(e.getMessage(),e);
			e.printStackTrace();
		}
	}
	
	public void onAccept(String acceptanceCn, Long documentId) throws EdmException
	{
		//nic nie robimy
	}
	
	public void onWithdraw(String acceptanceCn, Long documentId) throws EdmException
	{
		//nic nie robimy
	}
	
	public boolean canSendDecretation(FieldsManager fm) throws EdmException {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public boolean canDeleteAttachments(Document document) throws EdmException
	{
		// je�li akceptacja finalna to zablokowanie mo�liwo�� usuwania za��cznik�w
		return !document.getDocumentKind().getFieldsManager(document.getId()).getBoolean(AKCEPTACJA_FINALNA_FIELD_CN);
	}
	
	/**
	 * Sprawdza czy mo�na cofn�� akceptacj�
	 * @param document
	 * @param acceptanceCn
	 */
	@Override
	public void canWithdrawAcceptance(Document document, String acceptanceCn) throws EdmException
	{
		final String KSIEGOWA = "ksiegowa";
		if (document.getDocumentKind().getFieldsManager(document.getId()).getBoolean(AKCEPTACJA_FINALNA_FIELD_CN) &&
				!KSIEGOWA.equals(acceptanceCn))
			throw new EdmException("Faktura jest zaakceptowana finalnie! Mo�na cofn�� tylko akceptacj� ksi�gowej. " +
					"Przedekretuj faktur� do u�ytkownika z odpowiednimi uprawnieniami!");
	}
}
