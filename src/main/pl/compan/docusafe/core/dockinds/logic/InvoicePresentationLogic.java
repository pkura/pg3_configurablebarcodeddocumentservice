package pl.compan.docusafe.core.dockinds.logic;

import java.util.Calendar;
import java.util.Date;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;

public class InvoicePresentationLogic extends AbstractDocumentLogic
{

	private static final InvoicePresentationLogic instance = new InvoicePresentationLogic();
	
	public static InvoicePresentationLogic getInstance() 
	{
		return instance;
	}


    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		
		Folder folder = Folder.getRootFolder();
    	folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
    	
    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    	
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime((Date) fm.getValue("DATA_WPLYWU"));
    	
    	folder = folder.createSubfolderIfNotPresent((String) fm.getValue("DZIAL"));
    	folder = folder.createSubfolderIfNotPresent(""+calendar.get(Calendar.YEAR));
		folder = folder.createSubfolderIfNotPresent(""+(calendar.get(Calendar.MONTH)+1));
		
		document.setFolder(folder);
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ,"INVOICE_PRESENTATION_READ",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"INVOICE_PRESENTATION_ATT_READ",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"INVOICE_PRESENTATION_MODIFY",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"INVOICE_PRESENTATION_ATT_MODIFY",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" zalacznik - modyfikacja"));
   	 	
   	 	this.setUpPermission(document, perms);
	}

}
