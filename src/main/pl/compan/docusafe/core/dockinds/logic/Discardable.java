package pl.compan.docusafe.core.dockinds.logic;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * Interfejs implementowany przez klasy logiki, dokument�w, kt�re mo�na odrzuci� do jakiego� usera.
 * @author Micha� Sankowski
 */
public interface Discardable {

	void discardToUser(OfficeDocument doc, String activity, ActionEvent event, DSUser user) throws EdmException;

	void discardToCn(OfficeDocument document, String activity, ActionEvent event, String cn, Object objectId) throws EdmException;

}
