package pl.compan.docusafe.core.dockinds.logic;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import edu.emory.mathcs.backport.java.util.Arrays;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgencja;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgent;
import pl.compan.docusafe.core.dockinds.dictionary.DaaDictionary;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class DaaLogic extends AbstractDocumentLogic
{
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(DaaLogic.class.getPackage().getName(),null);
    private static DaaLogic instance;

    public static DaaLogic getInstance()
    {
        if (instance == null)
            instance = new DaaLogic();
        return instance;
    }

    //  nazwy kodowe poszczeg�lnych p�l dla tego rodzaju dokumentu
    public static final String DATA_WPLYWU_FIELD_CN = "DATA_WPLYWU";
    public static final String STATUS_FIELD_CN = "STATUS";
    public static final String KATEGORIA_FIELD_CN = "KATEGORIA";
    public static final String AGENT_FIELD_CN = "AGENT";
    public static final String AGENCJA_FIELD_CN = "AGENCJA";
    public static final String TYP_DOKUMENTU_FIELD_CN = "TYP_DOKUMENTU";
    public static final String KLASA_RAPORTU_FIELD_CN = "KLASA_RAPORTU";
    public static final String RODZAJ_SIECI_FIELD_CN = "RODZAJ_SIECI";
    public static final String OPIS_ANEKS_CN = "OPIS_ANEKS";
    public static final String NR_POLISY_CN = "NR_POLISY";
    
    //  nazwy kodowe poszczeg�lnych kategorii dokumentu
    public static final String AGENT_CN = "RODZAJ_AGENT";
    public static final String AGENCJA_CN = "RODZAJ_AGENCJA";
    public static final String RAPORTY_CN = "RODZAJ_RAPORT";
    public static final String INNE_CN = "RODZAJ_INNE";
        
    //  statusy
    public static final Integer STATUS_PRZYJETY = 10;
    public static final Integer STATUS_W_REALIZACJI = 20;
    public static final Integer STATUS_ZAWIESZONY = 30;
    public static final Integer STATUS_ZREALIZOWANY = 40;
    public static final Integer STATUS_ODRZUCONY = 50;
    public static final Integer STATUS_ARCHIWALNY = 60;
    
    // rodzaje sieci
    public static final String SIEC_WLASNA_CN = "SIEC_WLASNA";
    public static final String SIEC_ZEWNETRZNA_CN = "SIEC_ZEWNETRZNA";
    public static final String BANKI_CN = "BANKI";
    
    public static final String ANEKS_PREMIOWY_CN = "ANEKS_PREMIOWY";
    
    /** Komplet dokument�w agenta */
    public static final Integer[] SET_OF_DOCUMENTS = new Integer[] {60, 70, 110, 130, 230, 454, 500, 510, 520, 540, 560, 580, 131};
    
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {

    }

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException {
        TaskListParams ret = new TaskListParams();
        FieldsManager fm = kind.getFieldsManager(documentId);
        ret.setAttribute(fm.getStringValue(STATUS_FIELD_CN), 0);
        return ret;
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        
        // TODO: usunac ponizsze przypisanie doctype'a - narazie jest robione tylko na wszelki wypadek
        //       gdyby jakies komplikacje wyszly (poki do konca doctype'ow nie uporzadkujemy)  
        document.setDoctypeOnly(Doctype.findByCn("daa"));
        
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        
        EnumItem kategoriaItem = fm.getEnumItem(KATEGORIA_FIELD_CN);
        EnumItem rodzajSieciItem = fm.getEnumItem(RODZAJ_SIECI_FIELD_CN);
        
        DaaAgent agent = (DaaAgent) fm.getValue(AGENT_FIELD_CN); //zwiazac z folderem
        DaaAgencja agencja = (DaaAgencja) fm.getValue(AGENCJA_FIELD_CN); //zwiazac z folderem
        
        String klasaRaportu = (String) fm.getValue(KLASA_RAPORTU_FIELD_CN);
        if (kategoriaItem == null)
            throw new EdmException("Nie wybrano kategorii dokumentu");
        if (rodzajSieciItem == null)
            throw new EdmException("Nie wybrano rodzaju sieci");
        
        // ustawianie folder�w
        Map<String, DaaDictionary> daaPath = new LinkedHashMap<String, DaaDictionary>();
        List<String> path = new ArrayList<String>();
        
        path.add("Archiwum Agenta");
        daaPath.put("Archiwum Agenta", null);
        
        path.add(rodzajSieciItem.getTitle());
        daaPath.put(rodzajSieciItem.getTitle(), null);
        
        // komplet dokument�w
        hasSetOfDocuments(document);
        
        if (SIEC_WLASNA_CN.equals(rodzajSieciItem.getCn())) 
        {
            if (AGENCJA_CN.equals(kategoriaItem.getCn()))
            {
                if (agencja != null && agencja.getNazwa() != null)
                {
                    path.add(agencja.getNazwa().trim());
                    daaPath.put(agencja.getNazwa().trim(), agencja);
                }
                path.add("Dokumenty Biura");
                daaPath.put("Dokumenty Biura", null);
            }
            else if (AGENT_CN.equals(kategoriaItem.getCn()))
            {
                if (agencja != null && agencja.getNazwa() != null)
                {
                    path.add(agencja.getNazwa().trim());
                    daaPath.put(agencja.getNazwa().trim(), agencja);
                }
                
                path.add("Dokumenty OFWCA");
                daaPath.put("Dokumenty OFWCA", null);
                
                if (agent != null)
                {   
                    String x = getDescriptionWithNumer(agent);
                    
                    path.add(getPartitionX(x,1));
                    daaPath.put(getPartitionX(x,1), agent);
                    
                    path.add(x);
                    daaPath.put(x, agent);
                    
                    Integer typ = (Integer) fm.getKey(TYP_DOKUMENTU_FIELD_CN);
                    if (Arrays.asList(SET_OF_DOCUMENTS).contains(typ))
                    	daaPath.put("komplet dokument�w", null);
                    else
                    	daaPath.put("pozosta�e dokumenty", null);
                }
            }    
            else if (RAPORTY_CN.equals(kategoriaItem.getCn()))
            {
                if (agencja != null && agencja.getNazwa() != null)
                {
                    path.add(agencja.getNazwa().trim());
                    daaPath.put(agencja.getNazwa().trim(), agencja);
                    
                    path.add("Dokumenty zg�oszeniowe");                   
                    daaPath.put("Dokumenty zg�oszeniowe", null);
                }
                else
                {
                    path.add("_Dokumenty zg�oszeniowe");
                    daaPath.put("_Dokumenty zg�oszeniowe", null);
                }
                
                if (klasaRaportu != null)
                {
                	path.add(klasaRaportu);
                	daaPath.put(klasaRaportu, null);
                }
            }
            else if (INNE_CN.equals(kategoriaItem.getCn()))
            {
            	if (agencja != null && agencja.getNazwa() != null)
                {
                    path.add(agencja.getNazwa().trim());
                    daaPath.put(agencja.getNazwa().trim(), agencja);
                }
            	
            	path.add("INNE / Dokumenty biura/agencji/banku");
                daaPath.put("INNE / Dokumenty biura/agencji/banku", null);
            }
        }
        else if (SIEC_ZEWNETRZNA_CN.equals(rodzajSieciItem.getCn()))
        {
            if (AGENCJA_CN.equals(kategoriaItem.getCn()))
            {
                if (agencja != null)
                {
                    String x = getDescriptionWithNumer(agencja);
                    
                    path.add(getPartitionQuarter(x));
                    daaPath.put(getPartitionQuarter(x), agencja);
                    
                    path.add(x);
                    daaPath.put(x, agencja);
                    
                    path.add("Dokumenty Agencji");
                    daaPath.put("Dokumenty Agencji", null);
                }
            }
            else if (AGENT_CN.equals(kategoriaItem.getCn()))
            {
                if (agencja != null)
                {
                    String x = getDescriptionWithNumer(agencja);
                    
                    path.add(getPartitionQuarter(x));
                    daaPath.put(getPartitionQuarter(x), agencja);
                    
                    path.add(x);
                    daaPath.put(x, agencja);
                }
                
                path.add("Dokumenty OFWCA");
                daaPath.put("Dokumenty OFWCA", null);
                
                if (agent != null)
                {
                    String x = getDescriptionWithNumer(agent);
                    
                    path.add(getPartitionX(x,1));
                    daaPath.put(getPartitionX(x,1), agent);
                    
                    path.add(x);
                    daaPath.put(x, agent);
                    
                    Integer typ = (Integer) fm.getKey(TYP_DOKUMENTU_FIELD_CN);
                    if (Arrays.asList(SET_OF_DOCUMENTS).contains(typ))
                    	daaPath.put("komplet dokument�w", null);
                    else
                    	daaPath.put("pozosta�e dokumenty", null);
                }
            }
            else if (RAPORTY_CN.equals(kategoriaItem.getCn()))
            {
                if (agencja != null && getDescriptionWithNumer(agencja).length() > 0)
                {
                    String x = getDescriptionWithNumer(agencja);
                    
                    path.add(getPartitionQuarter(x));
                    daaPath.put(getPartitionQuarter(x), agencja);
                    
                    path.add(x);
                    daaPath.put(x, agencja);
                    
                    path.add("Dokumenty zg�oszeniowe");
                    daaPath.put("Dokumenty zg�oszeniowe", null);
                }
                else
                {
                    path.add("_Dokumenty zg�oszeniowe");
                    daaPath.put("_Dokumenty zg�oszeniowe", null);
                }
                
                if (klasaRaportu != null)
                {
                    path.add(klasaRaportu);
                    daaPath.put(klasaRaportu, null);
                }
            }
            else if (INNE_CN.equals(kategoriaItem.getCn()))
            {
            	if (agencja != null)
                {
                    String x = getDescriptionWithNumer(agencja);
                    
                    path.add(getPartitionQuarter(x));
                    daaPath.put(getPartitionQuarter(x), agencja);
                    
                    path.add(x);
                    daaPath.put(x, agencja);
                }
            	
            	path.add("INNE / Dokumenty biura/agencji/banku");
                daaPath.put("INNE / Dokumenty biura/agencji/banku", null);
            }
        }
        else if (BANKI_CN.equals(rodzajSieciItem.getCn()))
        {
            if (AGENCJA_CN.equals(kategoriaItem.getCn()))
            {
                if (agencja != null)
                {
                    String x = getDescription(agencja);
                    
                    path.add(x);
                    daaPath.put(x, agencja);
                    
                    path.add("Dokumenty Banku");
                    daaPath.put("Dokumenty Banku", null);
                }
            }
            else if (AGENT_CN.equals(kategoriaItem.getCn()))
            {
                if (agencja != null)
                {
                    String x = getDescription(agencja);
                    path.add(x);
                    daaPath.put(x, agencja);
                }
                
                path.add("Dokumenty OFWCA");
                daaPath.put("Dokumenty OFWCA", null);
                
                if (agent != null)
                {
                    String x = getDescriptionWithNumer(agent);
                    
                    path.add(getPartitionX(x,1));
                    daaPath.put(getPartitionX(x,1), agent);
                    
                    path.add(x);
                    daaPath.put(x, agent);  
                    
                    Integer typ = (Integer) fm.getKey(TYP_DOKUMENTU_FIELD_CN);
                    if (Arrays.asList(SET_OF_DOCUMENTS).contains(typ))
                    	daaPath.put("komplet dokument�w", null);
                    else
                    	daaPath.put("pozosta�e dokumenty", null);
                }
            }
            else if (RAPORTY_CN.equals(kategoriaItem.getCn()))
            {
                if (agencja != null)
                {
                    String x = getDescription(agencja);
                    
                    path.add(x);
                    daaPath.put(x, agencja);
                    
                    path.add("Dokumenty zg�oszeniowe");
                    daaPath.put("Dokumenty zg�oszeniowe", null);
                }
                else
                {
                    path.add("_Dokumenty zg�oszeniowe");
                    daaPath.put("_Dokumenty zg�oszeniowe", null);
                }
                
                if (klasaRaportu != null)
                {
                    path.add(klasaRaportu);
                    daaPath.put(klasaRaportu, null);
                }
            }
            else if (INNE_CN.equals(kategoriaItem.getCn()))
            {
            	if (agencja != null)
                {
                    String x = getDescriptionWithNumer(agencja);
                    
                    path.add(getPartitionQuarter(x));
                    daaPath.put(getPartitionQuarter(x), agencja);
                    
                    path.add(x);
                    daaPath.put(x, agencja);
                }
            	
            	path.add("INNE / Dokumenty biura/agencji/banku");
                daaPath.put("INNE / Dokumenty biura/agencji/banku", null);
            }
        }                               

        //String previousFolderPath = document.getFolderPath();
        
        Folder folder = Folder.getRootFolder();
        /*for (Iterator iter = path.iterator(); iter.hasNext();) 
        {
            String pathSegment = (String) iter.next();
            DSApi.context().grant(folder, new String[] {ObjectPermission.READ}, "DAA", ObjectPermission.GROUP);
            DSApi.context().grant(folder, new String[] {ObjectPermission.CREATE}, "DAA", ObjectPermission.GROUP);
            folder = folder.createSubfolderIfNotPresent(pathSegment);            
        }*/
        
        for(String pathSegment : daaPath.keySet())
        {
        	DSApi.context().grant(folder, new String[] {ObjectPermission.READ}, "DAA", ObjectPermission.GROUP);
            DSApi.context().grant(folder, new String[] {ObjectPermission.CREATE}, "DAA", ObjectPermission.GROUP);
            folder = folder.createSubfolderIfNotPresent(pathSegment);
            
            try
            {
	            if(daaPath.get(pathSegment) != null)
	            {
	            	DaaDictionary.folderClearLog.trace("zapisuje folder {}", folder.getId());
	            	daaPath.get(pathSegment).addFolder(folder.getId());
	            }
            }
            catch (Exception e) 
            {
            	DaaDictionary.folderClearLog.trace(e.getMessage());
			}
        }
        
        document.setFolder(folder);
          
        String dataWplywu = "brak";
        if(fm.getValue(DATA_WPLYWU_FIELD_CN) != null)
        {
        	dataWplywu = DateUtils.formatJsDate((Date) fm.getValue(DATA_WPLYWU_FIELD_CN));
        }
        else
        {
        	dataWplywu = DateUtils.formatJsDate(document.getCtime());
        }
        
        // ustawianie tytu�u i opisu
        String title;
        Integer typDokumentu = (Integer) fm.getKey(TYP_DOKUMENTU_FIELD_CN);
        if (typDokumentu == 640 || typDokumentu == 630) // Aneks niestandardowy lub produktowy
        	title = ((String) fm.getValue(TYP_DOKUMENTU_FIELD_CN)) + " " + ((String) fm.getValue(OPIS_ANEKS_CN)) + " (" + 
        		dataWplywu + ")";
        else if (typDokumentu == 275)
        	title = ((String) fm.getValue(TYP_DOKUMENTU_FIELD_CN)) + " " + 
        		dataWplywu + ", " + ((String) fm.getValue(NR_POLISY_CN));
        else
        	title = ((String) fm.getValue(TYP_DOKUMENTU_FIELD_CN)) + " (" + 
        		dataWplywu + ")";
        
        String description = title;
        if (AGENT_CN.equals(kategoriaItem.getCn()))
        {
            if (agent != null) 
                description = description + " (" + getDescription(agent) + ")";
            else 
                description += " (nieprzypisany)";
        }
        else if (AGENCJA_CN.equals(kategoriaItem.getCn()))
        {
            if (agencja != null) 
                description = description + " (" + agencja.getNazwa() + ")";
            else 
                description += " (nieprzypisany)";
        }
        else if (RAPORTY_CN.equals(kategoriaItem.getCn()))
        {
            if (agencja != null) 
                description = description + " (" + agencja.getNazwa() + ")";
            else            
                description = description + " (" + rodzajSieciItem.getTitle() + ")";
        }
        else if (INNE_CN.equals(kategoriaItem.getCn()))
        {
        	if (agent != null) 
                description = description + " (" + getDescription(agent) + ")";
            else 
                description += " (nieprzypisany)";
        }
        document.setTitle(title);
        document.setDescription(description);
        
        
        if (document instanceof OfficeDocument)
        {
            ((OfficeDocument) document).setSummaryOnly(description);
        }
        
        /*    
            
            // wpis do historii o archiwizacji - je�li obecny folder inny od poprzedniego
            if (!document.getFolderPath().equals(previousFolderPath))
                ((OfficeDocument) document).addWorkHistoryEntry(
                       Audit.create("daa", DSApi.context().getPrincipalName(),
                            sm.getString("DokumentZarchiwizowanoWfolderze",document.getFolderPath()),
                            null, null));
        }*/
        
        // je�li jest komplet dokument�w - trzeba przenie�� dokumenty do folderu: xxx xxxx (data) - komplet dokument�w
//        if (hasSetOfDocuments && agent != null && AGENT_CN.equals(kategoriaItem.getCn()))
//        {
//        	// update SQL'em kompletu dokument�w tego agenta do nowego folderu
//        	int res = DSApi.context().session().createSQLQuery("UPDATE DS_DOCUMENT SET FOLDER_ID = :folder " +
//        			"WHERE ID IN (SELECT d3.DOCUMENT_ID FROM DSG_DOCTYPE_3 d3 " +
//        			"WHERE d3.FIELD_4 = :agent AND d3.FIELD_3 IN (:ids))")
//    			.setLong("folder", folder.getId())
//    			.setLong("agent", agent.getId())
//    			.setParameterList("ids", SET_OF_DOCUMENTS)
//    			.executeUpdate();
//        }
    }
    
    static private String getDescriptionWithNumer(DaaAgent agent)
    {
        StringBuilder x = new StringBuilder("");
        if (agent.getNazwisko() != null)
            x.append(agent.getNazwisko().trim());
        if (agent.getImie() != null)
        {
            if (x.length() > 0)
                x.append(" ");
            x.append(agent.getImie().trim());
        }
        if (agent.getNumer() != null)
            x.append(" (").append(agent.getNumer().trim()).append(")");
        
        return x.toString();
    }
   
    static private String getDescription(DaaAgent agent)
    {
        StringBuilder x = new StringBuilder("");
        if (agent.getNazwisko() != null)
            x.append(agent.getNazwisko().trim());
        if (agent.getImie() != null)
        {
            if (x.length() > 0)
                x.append(" ");
            x.append(agent.getImie().trim());
        }
        
        return x.toString();
    }    
    
    static private String getDescriptionWithNumer(DaaAgencja agencja)
    {
        StringBuilder x = new StringBuilder(agencja.getNazwa() != null ? agencja.getNazwa().trim() : "");
        if (agencja.getNip() != null || agencja.getNumer() != null)
        {
            x.append(" (");
            if (agencja.getNumer() != null)
            {
                x.append(agencja.getNumer().trim());
                if (agencja.getNip() != null)
                    x.append(", ");
            }
            if (agencja.getNip() != null)
                x.append(agencja.getNip().trim());
            x.append(")");                            
        }
        return x.toString();
    }
    
    static private String getDescription(DaaAgencja agencja)
    {
        StringBuilder x = new StringBuilder(agencja.getNazwa() != null ? agencja.getNazwa().trim() : "");
        if (agencja.getNip() != null)
        {
            x.append(" (");
            x.append(agencja.getNip().trim());
            x.append(")");  
        }
        return x.toString();
    }
    
    /**
     * Zwraca przedzia� - �wiartk� alfabetu, kt�rej odpowiada przekazany napis.
     */
    static private String getPartitionQuarter(String x)
    {
        if (x == null || x.length() == 0)
        	return "a-f";
        
        char c = x.toLowerCase().charAt(0);
        if (c == '�') return "a-f";
        else if (c == '�') return "a-f";
        else if (c == '�') return "a-f";
        else if (c == '�') return "g-l";
        else if (c == '�') return "m-s";
        else if (c == '�') return "m-s";
        else if (c == '�') return "t-z";
        else if (c == '�') return "t-z";
        else if (c <= 'f')
            return "a-f";
        else if(c <= 'l')
            return "g-l";
        else if(c <= 's')
            return "m-s";
        else if(c <= 'z')
            return "t-z";
        else
            // na wszelki wypadek - dla wyj�tkowych sytuacji, kt�rych wy�ej nie obs�u�yli�my
            return "a-f";
    }
    
    static char alfabet[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 
        'o', 'p',  'r', 's', 't', 'u', 'w', 'x', 'y', 'z', ' ', ' '};

    
    /**
     * Zwraca przedzia� o d�ugo�ci 'n' liter, kt�remu odpowiada przekazany napis.
     */
    private static String  getPartitionX(String x, int n) 
    {
    	if (x == null || x.length() == 0)
        	return "a-f";
    	
        char s = x.toLowerCase().charAt(0);
        int l = alfabet.length-1;
        int i = 0;
        int chk;
        String str = "a-b";

        while (i < l - n) {
            chk = i + n  ;
            if (chk > l) chk = l ;
            if(chk == 0) chk = 1;
            if (alfabet[i] <= s && alfabet[chk] >= s) {
                return "" + alfabet[i] + "-" + alfabet[chk];
            }
            if(s == '�') return "a-b";
            if(s == '�') return "c-d";
            if(s == '�') return "e-f";
            if(s == '�') return "k-l";
            if(s == '�') return "m-n";
            if(s == '�') return "o-p";
            if(s == '�') return "r-s";
            if(s == '�' || s ==  '�') return "�-�";
            i++;
            i += n;
        }
        return str;
    }
    
    public void documentPermissions(Document document) throws EdmException
    {
        //Czyszczenie uprawnien w archiwum. Jesli nie wyczyscimy a zostanie zmieniony
        //dockind to moze byc naruszenie klucza wielowartosciowego
        DSApi.context().clearPermissions(document);
        
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());        
        if(fm.getEnumItemCn(TYP_DOKUMENTU_FIELD_CN).equals(ANEKS_PREMIOWY_CN))
        {
        	DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DAA_ANEKS_PREMIOWY", ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DAA_ANEKS_PREMIOWY_OBSLUGA", ObjectPermission.GROUP);
        }
        else
        {
	        DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DAA", ObjectPermission.GROUP);
	        DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DAA - obs�uga", ObjectPermission.GROUP);
        }
        //DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS, ObjectPermission.DELETE}, "admin", ObjectPermission.GROUP);

        try {
            DSApi.context().session().flush();
        }
        catch (HibernateException ex) {
            throw new EdmHibernateException(ex);
        }
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        values.put(KATEGORIA_FIELD_CN, fm.getField(KATEGORIA_FIELD_CN).getEnumItemByCn(AGENT_CN).getId());
        //values.put(DATA_WPLYWU_FIELD_CN, new Date());
        values.put(STATUS_FIELD_CN, STATUS_PRZYJETY);
        fm.reloadValues(values);
    }
   
   
    public boolean canReadDocumentDictionaries() throws EdmException
    {
        return DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_ODCZYTYWANIE);
    }
    
    
    private boolean hasSetOfDocuments(Document document) throws EdmException
    {
    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    	DaaAgent agent = (DaaAgent) fm.getValue(AGENT_FIELD_CN);
    	//DaaAgent agent = (DaaAgent) fm.getKey(AGENT_FIELD_CN);
    	EnumItem kategoriaItem = fm.getEnumItem(KATEGORIA_FIELD_CN);
    	
    	boolean hasSetOfDocuments = false;
    	if (fm.getKey(AGENT_FIELD_CN) != null && agent != null && (AGENT_CN.equals(kategoriaItem.getCn())))
    	{
    		// sorawdzenie czy agent ma przypisane wszystkie wybrane dokumenty, je�li tak
    		// to zaznaczenie kompletu dokument�w
    		String sql = "select COUNT(d.DOCUMENT_ID) from DSG_DOCTYPE_3 d where d.FIELD_4 = :agent AND FIELD_3 IN (:ids) group by FIELD_3";
    		@SuppressWarnings("unchecked")
    		List<Object> res = DSApi.context().session().createSQLQuery(sql)
    			.setParameter("agent", agent.getId())
    			.setParameterList("ids", SET_OF_DOCUMENTS)
    			.list();
    		
    		if (res.size() == SET_OF_DOCUMENTS.length)
    			// agent z kompletem dokument�w
    			hasSetOfDocuments = true;
    		
    		agent.setSetOfDocuments(hasSetOfDocuments);
    		DSApi.context().session().save(agent);
    	}
    	
    	return hasSetOfDocuments;
    }
}
