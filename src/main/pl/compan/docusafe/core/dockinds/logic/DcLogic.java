/*
 * DcLogic.java
 *
 */

package pl.compan.docusafe.core.dockinds.logic;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.DateUtils;

/**
 *
 * @author Piotr Komisarski
 */
public class DcLogic extends AbstractDocumentLogic{

    public  static final String KATEGORIA_FIELD_CN = "KATEGORIA";
    public  static final String NR_RACHUNKU_FIELD_CN = "NR_RACHUNKU";
    public  static final String IMIE_FIELD_CN = "IMIE";
    public  static final String NAZWISKO_FIELD_CN = "NAZWISKO";
    public  static final String PESEL_FIELD_CN = "PESEL";
    private static final String STATUS_REJESTRACJI_FIELD_CN = "STATUS_REJESTRACJI";
    private static final String STATUS_ZUS_FIELD_CN = "STATUS_ZUS";
    private static final String TYP_UMOWY_FIELD_CN = "TYP_UMOWY";
    public  static final String SYGNATURA_FIELD_CN = "SYGNATURA";
    private static final String DATA_OTRZYMANIA_FIELD_CN = "DATA_OTRZYMANIA";
    public  static final String DATA_FIELD_CN = "DATA";
    public  static final String TYP_FIELD_CN = "TYP";
    public  static final String NUMER_SPRAWY_FIELD_CN = "NUMER_SPRAWY";
    public  static final String IMIE_UPOSAZONEGO_FIELD_CN = "IMIE_UPOSAZONEGO";
    public  static final String NAZWISKO_UPOSAZONEGO_FIELD_CN = "NAZWISKO_UPOSAZONEGO";
    public  static final String DATA_UPOSAZONEGO_FIELD_CN = "DATA_UPOSAZONEGO";
    public  static final String STATUS_FIELD_CN = "STATUS";
    public  static final String NUMER_PLIKU_FIELD_CN = "NUMER_PLIKU";
    public  static final String NIEZDEFINIOWANY_CN = "Niezdefiniowany";
    public static final Integer KORESPONDENCJA_WYCH_CN=40;
    
    public  static final String KATEGORIA_REKLAMACJE_CN = "REKLAMACJE";
    public  static final String KATEGORIA_WYPLATY_CN = "WYPLATY";
    public  static final String KATEGORIA_OSWIADCZENIA_CN = "OSWIADCZENIA_O1";
    public  static final String KATEGORIA_ROZWODY_CN = "ROZWODY";
    public  static final String KATEGORIA_ZMIANA_DANYCH_CN = "ZMIANA_DANYCH";
    
    
    public  static final String TYP_INSTRUKCJE_DO_OSWIADCZEN = "INSTRUKCJA_DO_OSWIADCZEN_O";
    
    public  static final String STATUS_ARCHIWALNY_CN = "ARCHIWALNY";
    public static final String CZLONEK_CN = "Czlonek";
    
    private static DecimalFormat formatterWithDots;
    private static DecimalFormat formatterWithZeros;
    
    private static DcLogic instance;

    private static Map<String, String> shortcuts = new HashMap<String, String>();
    
    
    static{
        
        shortcuts.put(KATEGORIA_REKLAMACJE_CN, "CRS");
        shortcuts.put(KATEGORIA_OSWIADCZENIA_CN, "O");
        shortcuts.put(KATEGORIA_WYPLATY_CN, "W");
        shortcuts.put(KATEGORIA_ROZWODY_CN, "R");
        
        // w niemieckim separatorem grupy cyfr jest kropka (w naszym spacja)
        NumberFormat nf = NumberFormat.getInstance(Locale.GERMAN);
        DecimalFormat myFormatter = (DecimalFormat)nf;
        myFormatter.applyPattern("0,000,000,000");
        formatterWithDots = myFormatter;
        
        
        DecimalFormat formatter = new DecimalFormat();
        //formatter.applyPattern("0000000000");
        formatter.applyPattern("000000000");
        formatterWithZeros = formatter;
    }
    
    public static DcLogic getInstance()
    {
        if (instance == null)
            instance = new DcLogic();
        return instance;
    }

    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
        if (values.get(KATEGORIA_FIELD_CN) == null)
            throw new EdmException("Nie wybrano kategorii dokumentu");
    }

    /*
     * Dla 123456 zwroci 0.000.123.456
     */
    private String getFormattedLongWithDots(Long l)
    {
        return formatterWithDots.format(l);
    }
    
    /*
     * Dla 123456 zwroci 0000123456
     */
    private String getFormattedLongWithZeros(Long l)
    {
        return formatterWithZeros.format(l);
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {

        if (document.getDocumentKind() == null)
            throw new NullPointerException("document is null");
        
        //kolumna jest sztuczna, nie zapamiętujemy bo słownik jest tylko podpowiadaczem
        Map<String, Object> fieldValues = new HashMap<String, Object>();
        fieldValues.put("Czlonek", null);
		document.getDocumentKind().setOnly(document.getId(), fieldValues);
		
		
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        fm.initialize();
        Long tmpnrRachunku = (Long)fm.getValue(NR_RACHUNKU_FIELD_CN);

        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Archiwum dokument\u00f3w cz\u0142onkowskich");
        
        if(fm.getValue(KATEGORIA_FIELD_CN).equals(NIEZDEFINIOWANY_CN))
        {
        	folder = folder.createSubfolderIfNotPresent("Poczekalnia dokumentów DOK");   
        }
        else if(tmpnrRachunku!=null)
        {
            Long nrRachunku = tmpnrRachunku;
            Long prefix=0L;
           


            prefix+=(nrRachunku/100000)*100000;
            nrRachunku%=100000;
            folder = folder.createSubfolderIfNotPresent(getFormattedLongWithDots(prefix) + " - " + getFormattedLongWithDots(prefix+99999));

            prefix+=(nrRachunku/5000)*5000;
            nrRachunku%=5000;
            folder = folder.createSubfolderIfNotPresent(getFormattedLongWithDots(prefix) + " - " + getFormattedLongWithDots(prefix+4999));

            prefix+=(nrRachunku/500)*500;
            nrRachunku%=500;
            folder = folder.createSubfolderIfNotPresent(getFormattedLongWithDots(prefix) + " - " + getFormattedLongWithDots(prefix+499));

            prefix+=(nrRachunku/25)*25;
            folder = folder.createSubfolderIfNotPresent(getFormattedLongWithDots(prefix) + " - " + getFormattedLongWithDots(prefix+24));

            nrRachunku = Long.parseLong(fm.getValue(NR_RACHUNKU_FIELD_CN).toString());
            folder = folder.createSubfolderIfNotPresent(getFormattedLongWithZeros(nrRachunku));

            Long numerSprawy = (Long)fm.getValue(NUMER_SPRAWY_FIELD_CN);
            String tmp = "";
            try{
            	
            
		        if(numerSprawy!=null)
		        {
		            Long numerS = numerSprawy;
		            tmp = "("+getFormattedLongWithZeros(numerS);//+")"
		        }
            }
            catch(NumberFormatException e){
            	throw new ValidationException("Nieprawidłowy numer sprawy" );
            }
            
            String shortcut;
            
            if(shortcuts.containsKey(fm.getEnumItem(KATEGORIA_FIELD_CN).getCn()))
                shortcut = "-"+shortcuts.get(fm.getEnumItem(KATEGORIA_FIELD_CN).getCn()) + ")";
            else
                shortcut = ")";
            
            
            
            String lastFolder = fm.getValue(KATEGORIA_FIELD_CN).toString() + tmp + shortcut;
            folder = folder.createSubfolderIfNotPresent(lastFolder);                
           

        }
        else
        {
            folder = folder.createSubfolderIfNotPresent("Instrukcja do oswiadczen");
        }

        document.setFolder(folder);
        EnumItem typDokumentu = fm.getEnumItem(TYP_FIELD_CN);


        Object o = fm.getValue(DATA_FIELD_CN);
        String date = (o==null)?"":" (" +DateUtils.formatSqlDate((Date)o) + ")";
        
            
        String title = null;
        if(typDokumentu!=null) 
            title = typDokumentu.getTitle();
        else
            title = "Podstawowy dokument";
        title=fm.getValue(KATEGORIA_FIELD_CN).toString();
        if(fm.getValue("NUMER_SPRAWY")!=null){
        	title+="("+fm.getValue("NUMER_SPRAWY")+")";
        }
        
        if(fm.getValue(KATEGORIA_FIELD_CN).equals(NIEZDEFINIOWANY_CN))
        {
        	/*String DATE_FORMAT = "yyyy-MM-dd"; 
        	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);
        	String currentTime = sdf.format(new Date());*/ 
        	title = "Niezdefiniowana klasa "+date;
        }                     
        document.setTitle(title);                                   //tytul
        String description = title;

        
        if(fm.getValue(TYP_FIELD_CN)!=null)
        {
        	description = fm.getValue(TYP_FIELD_CN).toString();
        	 if(fm.getValue("NUMER_SPRAWY")!=null){
             	description+="("+fm.getValue("NUMER_SPRAWY")+")";
             	
             }
        }       
        document.setDescription(description);
        if(document instanceof OfficeDocument)
            ((OfficeDocument)document).setSummaryOnly(description);      //opis
    }

    public void documentPermissions(Document document) throws EdmException
    {

       DSApi.context().clearPermissions(document);
       /*
       try
       {
            DSApi.context().session().clear();
       }
       catch (HibernateException ex)
       {
            throw new EdmHibernateException(ex);
       }
       */
       DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DC_READ", ObjectPermission.GROUP);
       DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DC_ADMIN", ObjectPermission.GROUP);

       try
       {
            DSApi.context().session().flush();
       }
       catch (HibernateException ex)
       {
            throw new EdmHibernateException(ex);
       }
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
    
    	Map<String,Object> values = new HashMap<String,Object>();
    	if(DocumentLogic.TYPE_OUT_OFFICE==type)
    	{
    		values.put(TYP_FIELD_CN, KORESPONDENCJA_WYCH_CN);
    	
    		fm.reloadValues(values);
    	}
    }

	@SuppressWarnings("unchecked")
	public List<Long> findIdsByCategory(Integer docTypeId) throws EdmException
	{
		List<BigDecimal> ids = (List<BigDecimal>) DSApi.context().session().createSQLQuery(
				"SELECT c.DOCUMENT_ID FROM DSG_CZLONKOWSKI c WHERE c.KATEGORIA = :cat")
			.setParameter("cat", docTypeId)
			.list();
		
		List<Long> ret = new ArrayList<Long>();
		for (BigDecimal bd : ids)
			ret.add(bd.longValue());
		
		return ret;
			
	}
}