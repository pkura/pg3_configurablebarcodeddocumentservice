package pl.compan.docusafe.core.dockinds.logic;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.Format;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentException;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.permission.PermissionPolicy;
import pl.compan.docusafe.core.crypto.DocusafeKeyStore;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.PdfLayerParams;
import pl.compan.docusafe.core.dockinds.acceptances.JBPMAcceptanceManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.measure.ProcessParameterBean;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.handlers.DocumentMailHandler;
import pl.compan.docusafe.service.epuap.EpuapExportDocument;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytka;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionType;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullPobierzTyp;

/**
 * Odpowiada za funkcjonalno�ci rodzaju dokumentu.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public interface DocumentLogic extends Serializable
{
    // typy dokument�w 
    /** pismo przychodz�ce */
    int TYPE_IN_OFFICE = 1;
    /** pismo wewn�trzne */
    int TYPE_INTERNAL_OFFICE = 2;
    /** pismo wychodz�ce */
    int TYPE_OUT_OFFICE = 3;
    /** dokument archiwalny */
    int TYPE_ARCHIVE = 6;
    /** zapis z saveAll */
    int TYPE_SAVE_ALL = 9;

    /**
     * Zwraca pozycje, jakie zostan� dodane przez addActionMessage do odpowiedniej akcji okre�lonej przez typ {@code source}.
     *
     * <p>Domy�lna implementacja zwraca {@code LinkedList}.
     * @param source
     * @param doc
     * @return
     */
    Iterable<String> getActionMessage(ActionType source, Document doc);

    /**
     * Waliduj� map� z warto�ciami wed�ug regu� dla dokument�w posiadaj�cych t� logik�.
     * 
     * @param values
     * @param kind
     * @param documentId id dokumentu, mo�e by� null w przypadku gdy
     * dokument jeszcze nie istnieje (importy, przymowanie dokumentu etc)
     * @throws EdmException
     */
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException;
    
    /**
     * Waliduj� map� z warto�ciami wed�ug regu� dla dokument�w posiadaj�cych t� logik�.
     * 
     * @param values
     * @param kind
     * dokument jeszcze nie istnieje (importy, przymowanie dokumentu etc)
     * @throws EdmException
     */
    public void validate(Map<String, Object> values, DocumentKind kind) throws EdmException;


    /**
     * Metoda s�u�y do opakowania walidacji logiki dokumentu w celu u�ycia jej na innych interfejsach nie tylko www.
     * metoda u�ywana na interfejsie API oraz importera. W celu przepakowania mapy na map� u�ywan� na metodzie.
     * validateDWR
     *
     * @param values  najprawdopodbniej jest to Map<String,String>
     * @param document
     * @throws EdmException
     */
    public void globalValidate(Map<String, Object> values, Document document) throws EdmException;

    /**
     * Waliduje(koryguje) map� z warto�ciami na podstawie dokonanych wcze�niej akceptacji - pola, kt�re s�
     * u�ywane do sterowania akceptacj�, nie mog� zosta� zmienione, gdy jest dokonana chocia� jedna akceptacja
     * korzystaj�ca z tego pola.
     * 
     * @param values
     * @param kind
     * @param documentId
     * @throws EdmException
     */
    public void validateAcceptances(Map<String,Object> values, DocumentKind kind, Long documentId) throws EdmException;
    
    /**
     * Poprawia warto�ci otrzymane od u�ytkownika. U�ywane np. do wpisania domy�lnych warto�ci
     * p�l, kt�rych u�ytkownik nie musia� uzupe�nia�.
     * 
     * @param values
     * @param kind
     * @throws EdmException
     */
    public void correctValues(Map<String,Object> values, DocumentKind kind) throws EdmException;
    
    /**
     * Poprawia warto�ci otrzymane od u�ytkownika. U�ywane np. do wpisania domy�lnych warto�ci
     * p�l, kt�rych u�ytkownik nie musia� uzupe�nia�.
     * 
     * @param values
     * @param kind
	 * @param documentId
     * @throws EdmException
     */
    public void correctValues(Map<String,Object> values, DocumentKind kind , long documentId ) throws EdmException;
    
    /**
     * Waliduj� map� z warto�ciami wed�ug regu� dla dokument�w posiadaj�cych t� logik�.
     * 
     * @param values
     * @param kind
     * @return true <=> nie by�o �adnego b��du walidacji
     * @throws EdmException
     */
    public boolean validateSuccess(Map<String,Object> values, DocumentKind kind) throws EdmException;
    
    /**
     * Wykonanie akcji zwi�zanych z archiwizacj� dokumentu. Powinno by� wo�ane z ka�dej akcji,
     * kt�ra zmienia warto�ci p�l danego typu dokumentu. Trzeba pami�ta�, aby przed wywo�aniem tej
     * funkcji by�o uruchamiana funkcja documentKind.set(documentId, values), bo tutaj dane s�
     * wczytywane z bazy.
     *
     * @param document
     * @param type typ dokumentu (archiwalny,przychodz�cy,wychodz�cy,wewn�trzny)
     * @throws EdmException
     */
    public void archiveActions(Document document, int type) throws EdmException;
    
    
    /***
     * Drukowanie etykiety, w tej metodzie system wysy�a do rukarki zebra dane do drukowania. 
     * Nale�y doda� obs�ug� pisma wychodzacego i archiwalnego (jesli b�dzie potrzeba)
     * @param document
     * @param type
     * @throws EdmException
     */
    public void printLabel(Document document, int type, Map<String, Object> dockindKeys) throws EdmException;
    
    
    /**
     * Edytowanie warto�ci dla dockindFields 
     * 
     * @param dockindFields
     */
    public void specialSetDictionaryValues(Map<String, ?> dockindFields);
    public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values);
    public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields);
    
    /**
     * Wykonanie akcji zwi�zanych z zadaniem.
     *
     * @param document
     * @param activity
     * @throws EdmException
     * @return true <=> zakonczyl proces
     */
    public boolean processActions(OfficeDocument document, String activity) throws EdmException;

    /**
     * Ustalenie uprawnie� dla danego dokumentu. Trzeba pami�ta�, aby przed wywo�aniem tej
     * funkcji by�o uruchamiana funkcja documentKind.set(documentId, values), bo tutaj dane s�
     * wczytywane z bazy.
     * 
     * @param document
     * @throws EdmException
     */   
    public void documentPermissions(Document document) throws EdmException;
       

    /**
     * Ustawienie warto�ci pocz�tkowych
     * @param fm
     * @param type typ dokumentu (archiwalny,przychodz�cy,wychodz�cy,wewn�trzny)
     * @throws EdmException
     */
    public void setInitialValues(FieldsManager fm, int type) throws EdmException;

    /**
     * Sprawdza na podstawie danych warto�ci (kryteri�w wyszukiwania) czy podczas wykonywania zapytania
     * do bazy powinny by� sprawdzane uprawnienia do dokument�w (np. zwi�zku z wydajno�ci�).
     *
     * @param values warto�ci p�l wed�ug 'cn' w postaci kryteri�w wyszukiwania (por�wnaj z akcj� wyszukuj�c� dokumenty)
     * @return zwraca true gdy uprawnienia maj� by� sprawdzane podczas zapytania wyszukuj�cego, wpp
     * uprawnienia b�d� sprawdzane r�cznie po wykonaniu zapytania
     *
     * @see pl.compan.docusafe.web.archive.repository.search.SearchDockindDocumentsAction
     */
    public boolean searchCheckPermissions(Map<String,Object> values);
    
    /**
     * Sprawdza, czy proces obi�gu r�cznego dla danego dokumentu mo�e by� zako�czony - czyli czy dokument
     * spe�nia odpowiednie warunki. Gdy jaki� warunek niespe�niony rzuczany jest wyj�tek 
     * {@link pl.compan.docusafe.core.security.AccessDeniedException} wyja�niaj�cy pow�d. 
     * 
     * @param document   
     * @throws AccessDeniedException
     * @throws EdmException 
     */
    public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException;
 
    
    
    /**
     * Sprawdza, czy mozna zamknac proces.
     * @param document
     * @return
     * @throws AccessDeniedException
     * @throws EdmException
     */
	public boolean canFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException;
	
    /**
     * Odpowiada na pytanie czy do danego typu dokumentu mozna wyznaczyc koordynatora 
     * Metoda powinna byc uzywana w miejscach gdzie nalezy podjac decyzje o wstawiecniu kontretnej funkcji 
     * do formatki 
     * @param document
     * @return
     * @throws EdmException
     */
    public boolean isProcessCoordinator(OfficeDocument document) throws EdmException;
    /**
     * Na podstawie warto�ci atrybut�w danego dokumentu, zwraca osob�, kt�ra jest koordynatorem dokument�w 
     * takich jak ten. Gdy zostanie zwr�cony null, to oznacza, �e do dokument�w tego typu nie nikogo nie 
     * przydzielono - w�wczas proces powinien zosta� przypisany do osoby tworz�cej dokument.
     * @throws EdmException 
     */
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException;
    
    /**
     * Zwraca nazw� rodzaju pisma (istotne tylko dla pism przychodz�cych), kt�ry ma by� ustawiany dla
     * dokument�w danego rodzaju. Gdy zwracane null to oznacza, �e na ekranie przyj�cia pisma b�dzie dost�pny 
     * r�czny wyb�r rodzaju pisma.
     * 
     * TODO: przerobic zeby nie bylo po nazwach, ale jakos inteligentniej
     */
    public String getInOfficeDocumentKind();
    
    /**
     * Czy mo�na doda� dokument do rejestru szk�d. U�ywane tylko ze wzgl�du na rodzaj 'nationwide',
     * bo innych dokument�w nie mo�na dodawa� do rejestru szk�d.
     */
    public boolean canAddToRS() throws EdmException;
    
    /**
     * Zwraca true je�li aktualnie zalogowany u�ytkownik mo�e utworzy� dokument danego rodzaju 
     * "bezpo�rednio z formatki" (czyli na pewno nie b�d� tu zaliczane dokumenty rejestru szk�d, bo je mo�na
     * tworzy� tylko po uprzednim utworzeniu dokumentu 'nationwide').
     */
    public boolean canCreateManually() throws EdmException;
    
    /**
     * Zwraca true je�li aktualnie zalogowany u�ytkownik mo�e czyta�(przegl�da�) s�owniki u�ywane
     * dla typu dokumentu zwi�zanego z t� logik�. Gdy zwracana false, to w�wczas na stronach JSP
     * "disablowane" s� przyciski do obs�ugi s�onwik�w.
     */
    public boolean canReadDocumentDictionaries() throws EdmException;
    
    /**
     * Zwraca true <=> edycja/archiwizacja dokument�w danego rodzaju powinna by� obs�ugiwana przez 
     * akcje dla doctype'�w, a nie dla dockind'�w.
     */
    public boolean needsEditDoctypeAction();
    
    /**
     * akcje wykonywane w momencie zapisywania atrybut�w biznesowych - czyli wo�ane tylko z metody
     * {@link pl.compan.docusafe.core.dockinds.DocumentKind#set(Long, Map)}
     */
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException;
    
    
    /**
     * akcja wywo�ana po utworzeniu procesu w documentmainaction i aldimporthandler.
     * (Ald chcialo jeden kanal, zeby po utworzeniu procesu od razu sie on zamykal)
     */ 
    public void onStartProcess(OfficeDocument document) throws EdmException;

    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException;
    /**
     * Metoda uzmowliwia uruchomienie procesu oraz  zadekretowanie zaimportowanego dokumentu na  kilka punkt�w kancelaryjnych 
     * po rozpoznainiu prefixu w nazwie pliku  na poczatku nazwy piku np  K1_nazwapliku K2_nazwapliku
     * w adsach na logice dokumentu trzeba sparametryzowac dla jakiego prefixu na jaki dzia� ma isc dekretacja i ustawic odpowiednie dzia�y 
	 * domyslnie nic nie robi 
     * @param prefix
     */
    public void onStartProcess(OfficeDocument document,ActionEvent event, String filePrefix) throws EdmException;

    /***
     * Metoda wywoywana podczs wczytywania danych w FieldsManager.initialize()
     * @param fm
     * @throws EdmException 
     */
    public void onLoadData(FieldsManager fm) throws EdmException;
    

    
    
    /**
     * akcje wywolywana po zakonczeniu procesu
     * @param document
     */
    public void onEndProcess(OfficeDocument document, boolean isManual) throws EdmException;
    
    /**
     * Akcja wykonywana podczas wywo�ania akcji sendToDecretation w DocumentArchiveTabAction
     * 
     * @param document
     * @throws EdmException
     */
    public void onDecretation(Document document) throws EdmException;
    
    /**
     * Akcja wykonywana podczas wywo�ania akcji doGiveAcceptance w DocumentArchiveTabAction
     * 
     * @param document
     * @throws EdmException
     */
    public void onGiveAcceptance(Document document, String acceptanceCn) throws EdmException;
    
    /**
     * Akcja wykonywana podczas wywo�ania akcji doWithdrawAcceptance w DocumentArchiveTabAction
     * 
     * @param document
     * @throws EdmException
     */
    public void onWithdrawAcceptance(Document document, String acceptanceCn) throws EdmException;
    
    public boolean canAcceptDocument(FieldsManager fm, String acceptanceCn) throws EdmException;
    
    /**
     * Sprawdza, czy wyst�pi�a sytuacja, w kt�rej u�ytkownik koniecznie musia� doda� uwag� do dokumentu
     * (i czy proces powinien zosta� zako�czony) - u�ywane np. w fakturach kosztowych dla Aegon, gdzie
     * po ustaleniu statusu jako 'Odrzucona', konieczne jest dodanie uwagi (przyczyny odrzucenia), a po 
     * zapisaniu zmian zadanie jest ko�czone.     
     * Je�li tak, to uwaga dodawana jest do dokumentu.
     * 
     * @return true <=> nale�y dodatkowo zako�czy� obecne zadanie
     */
    public boolean checkRemarkField(Document document, Map<String,Object> values, String remark) throws EdmException;
    
    /**
     * Przekazuje true <=> dla dokument�w tego rodzaju mo�na generowa� obraz dokumentu w postaci PDF.
     */
    public boolean canGenerateDocumentView();
    /**
     * Przekazuje true <=> dla dokument�w tego rodzaju mo�na generowa� zrzut dokumentu w postaci CVS.
     */
    public boolean canGenerateCsvView();
    
    /**
     * Przekazuje true <=> dla dokument�w w ktorych mozna zmienic dockind
     */
    public boolean canChangeDockind(Document document) throws EdmException;
    
    /**
     * Dla podanego dokumentu generowany jest jego obraz w postaci PDF.
     * 
     * @return wygenerowany plik PDF
     */
    public File generateDocumentView(Document document) throws EdmException;
    
    /**
     * Dla podanego dokumentu generowany jest zrzut dokumentu w postaci CSV.
     * 
     * @return wygenerowany plik CSV
     */
    public File generateCsvDocumentView(Document document) throws EdmException;
    
    /**
     * Zwraca polityk� bezpiecze�stwa dla dokument�w tego rodzaju.
     */
    public int getPermissionPolicyMode();

	public PermissionPolicy getPermissionPolicy();
    
	TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException;
	
	TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException;

    /**
     * Metoda robi specyficzne rzeczy podczas importu przez serwis DockindImportHandler
     * @param document
     * @param values
     * @param builder 
     * @throws EdmException 
     */
	public void correctImportValues(Document document, Map<String, Object> values, StringBuilder builder) throws EdmException;
	
    public String getPhoneNumber(Document document) throws EdmException;
    
    /**Zwraca parametry do dodania znaku wodnego na generowanym PDF-e*/
    public PdfLayerParams getPdfLayerParams(Document document) throws EdmException;
	
	/**
	 * Metoda wywo�ywana podaczas ko�czenia pracy z dokumentme
	 * @param document
	 * @throws EdmException 
	 */
	public void manualFinish(Document document) throws EdmException;
	
	/**
	 * Zwraca obiekt odpowiedzialny za akceptacje w procesie jbpm
	 * @return null gdy dany dokument nie korzysta z jbpm'a w procesie akceptacji
	 */
	JBPMAcceptanceManager getAcceptanceManager();
	
	/**
	 * Pr�buje nada� akceptacj� dokumentowi.
	 * @param doc
	 * @return true je�li akceptacja zosta�a nadana, false je�li dokument nie uczestniczy w procesach akceptacji obs�ugiwany przez DocumentLogic.
	 * @throws EdmException
	 */
	boolean accept(Document doc, Object... context) throws EdmException;
	
	/**
     * Wykonuje akcj� po wci�ni�ciu Dockind button
     *
     * Funkcja NIE jest wykonywana w tranzakcji
	 * @param dockindKeys 
     *
     **/
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException;
    
	
	/**
     * Wykonuje akcj� po wci�ni�ciu Dockind button
     *
     * Funkcja NIE jest wykonywana w tranzakcji
	 * @param dockindKeys 
     *
     **/
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values, Map<String, Object> dockindKeys) throws EdmException;
        /**
         * Funkcja wykonywana na samym ko�cu procesu tworzenia dokumentu
         * @todo zaimplementowana tylko w wewn�trznych
         * @param doc Document
         */
        public void onEndCreateProcess(Document document) throws EdmException;
        
        /**
         * Funkcja wywolywana podczas akcji 'Dekretuj do akceptacji' podczas gdy u�ytkownik wybierze osob� z listy
         * (lista osob jest defioniowana zawsze w logice dokumentu)
         * @param document Document
         * @param userName String
         * @throws EdmException 
         */
        public void onDecretationWhenUsernameSet(Document document,String userName) throws EdmException;
        
        /**
         * Funkcja wywolywana przed manualFinish(document, !doWiadomo�ci)  iprzed updatem TaskSnashot
         * oraz commitem
         * w akcji DocumentArchiveTab$ManualFinish
         * 
         * @see DocumentArchiveTabAction$ManualFinish
         * @param document Document
         * @param isDoWiadomosci Boolean
         * @throws EdmException
         */
        public void onCommitManualFinish(Document document, Boolean isDoWiadomosci) throws EdmException;
	/**
	 * Zwraca zaawansowana polise bezpieczenstwa (mapowanie specjalnych uprawnien systemowych) 
	 * @return null - bez zaawansowanej polisy
	 */
	public String getExtendedPermissionPolicy();
	
	/**
	 * Zwraca liste ProcessParameterBean zawieraj�c� informacje o procesach zadania,
	 * Klsa ProcessParameterBean zawiera liste CheckPoint�w i wiele innych :)
	 * @return
	 * @throws EdmException 
	 */
	public List<ProcessParameterBean> getProcessParameter(Document document) throws EdmException;
	
	/**
	 * Metoda zwraca instancje AttachmentRevision odpowiedniej klasy. 
	 * EncryptedAttachmentRevision - szyfrowane zalaczniki
	 * StandardAttachmentRevision - zalaczniki jawne
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	public AttachmentRevision createAttachmentRevision() throws EdmException;
	
	
	/**
	 * Metoda zwraca instancje klucza obslugujacego dany dockind
	 * @return
	 */
	public DocusafeKeyStore getKeyStoreInstance();
	
	/**
	 * Czy logika dopuszcza nadawanie uprawnien bezposrednio osobom
	 * @return - true - tak, false nie
	 * Gdy zwrocone jest tak to przy szukaniu dokumentu zostanie uwzglednione pytanie o uprawnienia dla tej
	 * osoby
	 */
	public boolean isPersonalRightsAllowed();
	
	/**
	 * Czy uprawnienie o zadanym kodzie jest uprawnieniem do odczytu tego dockindu.
	 * Funkcja sluzy do filtrowania grup wchodzacych w zapytanie o uprawnienia
	 * @param permCode - kod uprawnienia 
	 * @return
	 */
	public boolean isReadPermissionCode(String permCode);
        
        public boolean canRemarkDocument(FieldsManager fm,Document document) throws EdmException;
	
	/***
	 * Akcja wywolywana podczas akceptacji zadania
	 * @param activity 
	 * @param event 
	 * @param document 
	 */
	public void acceptTask(Document document, ActionEvent event, String activity);
	
	/**
	 * Metoda liczy skrot z tresci zalacznika, mogacy sluzyc do werfykiacji integralnosc zalacznikow. 
	 * @param ar wersja zalacznika do wyliczenia kodu
	 * @return Wyliczony skrot reprezentowany w BASE64
	 */
	public String countAttachmentRevisionVerficationCode(AttachmentRevision ar) throws EdmException;
	
	/**
	 * Metoda zwraca tablice fieldCn'ow dla ktorych nalezy wyszukiwac po equals
	 * @param values
	 * @return
	 * @throws EdmException
	 */
	public String[] forceEqualsSearchList(Map<String,Object> values) throws EdmException;
	
	 /**
     * Na podstawie warto�ci atrybut�w danego dokumentu zwraca id Pudla z linni
     * @throws EdmException 
     */
    public Long getBoxLine(Document document) throws EdmException;
    
    public String getBoxLineCn(Document document) throws EdmException;
    
    /**
     * Metoda wywao�ywana w momencie wpisu do dziennika
     * @param document
     * @param event
     * @throws EdmException
     */
    public void bindToJournal(OfficeDocument document,ActionEvent event) throws EdmException;
    
    /**
     * Metoda zwracaj�ca powi�zane dokumenty dla danego dokumentu
     * @return
     * @throws EdmException
     */
    public List<RelatedDocumentBean> getRelatedDocuments(FieldsManager fm) throws EdmException;
    
    /**
     * Zwraca map� dost�pnych pude� dla danego dokumentu
     * @param fm
     * @return
     * @throws EdmException
     */
    public Map<Long, String> getAvailableArchiveBoxes(FieldsManager fm) throws EdmException;

    /**
     * Metoda sprawdzaj�ca czy mo�na cofn�� akceptacj�
     * @param doc
     */
	public void canWithdrawAcceptance(Document doc, String acceptanceCn) throws EdmException;

	/**
	 * Czy mo�na usuwa� za��czniki (mo�liwe, �e pewien stan procesu biznesowego na to nie pozwala)
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	public boolean canDeleteAttachments(Document document) throws EdmException;

	
    public boolean assignee(OfficeDocument doc, Assignable assignable,
			OpenExecution openExecution, String acceptationCn,
			String fieldCnValue) throws EdmException;

    /**
     * Zwraca wsparcie dla OCR dla danego rodzaju dokument�w
     * @return
     */
    OcrSupport getOcrSupport();

	public Field validateDwr(Map<String, FieldData> values,FieldsManager fm) throws EdmException;

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values);

    /**
     *
     * @param docId
     * @param values
     * @param defaultFormatters Mapa domy�lnych formater�w. Wrzucamy do niej jako klucz klas�, kt�rej dotyczy� ma formatowanie
     *                          oraz warto�� - formater wszystkich p�l instancji klasy klucza.<br/>
     *                          np: defaultFormatters.put(Date.class, new SimpleDateFormat("dd-MM-yyyy"))<br/><br/>
     *
     *                          Tylko dla silnika RtfTemplate, wi�c special=true.
     */
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters);

    /**
     * Sprawdza czy u�ytkownik mo�e przywr�ci� na liste zada�
     */
	public void canReopenDocument(Document document) throws EdmException;

    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values);
    
    
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues,Long documentId);
    
    /**
     * Metoda do por�wnywania starych i nowych wartosci na dokumencie
     * @param newFm
     * @param oldFm
     */
    public void specialCompareData(FieldsManager newFm, FieldsManager oldFm) throws EdmException;

    /**
     * JBPM4WorkflowFactory.reassign
     * @param doc
     */
	public void onEndManualAssignment(OfficeDocument doc,  String fromUsername, String toUsername);

    /**
     *  Metoda wywo�ywana przy starcie dekretacj r�cznej
     * @param doc
     */
	public String onStartManualAssignment(OfficeDocument doc,  String fromUsername, String toUsername);
	/**
	 * Metoda wywo�ywana w RejectedListener
	 * @see pl.compan.docusafe.core.jbpm4.RejectedListener
	 * @param doc
	 */
	public void onRejectedListener(Document doc) throws EdmException;
	
	/**
	  * Metoda wywo�ywana w AcceptedListenerze
	  * @see pl.compan.docusafe.core.jbpm4.AcceptancesListener
	 */
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException;
	
	public Map<String, String> setInitialValuesForReplies(Long inDocumentId) throws EdmException;

	public void removeValuesBeforeSet(Map<String, Object> dockindKeys);

	
	// ----------------------------------------------------------------------------
	// ---------------------------------- EPUAP -----------------------------------
	// ----------------------------------------------------------------------------
	
	public void setAdditionalValues(FieldsManager fieldsManager, Integer valueOf, String activity) throws EdmException;
	
	/**
	 * Wysyla powiadomienie 
	 * @param fieldsManager
	 * @return
	 * @throws EdmException
	 */
	public boolean sendMailNotification(FieldsManager fieldsManager) throws EdmException;
	
	public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException;
	
	/** metoda wywolywana przez serwis importuj�cy z ePUAP.
	 * Pozwala na specyficzn� dla danego rodzaju dokumentu inicjalizacj� p�l tego dokumentu. */
	public void setDocValuesAfterImportFromEpuap (OfficeDocument document, EpuapSkrytka skrytka, boolean dokument1_upo0) throws EdmException;
	
	/** metoda wywolywana przez serwis eksportuj�cy do ePUAP.
	 * Okre�la ktora skrytka ma byc jako nadawca dokumentu epuap.
	 * Jesli zwroci null, bedzie uzyta domyslna skrytka
	 * (co jest uzyteczne zwlaszcza, gdy uzywana jest w ogole tylko jedna) */
	public EpuapSkrytka wybierzSkrytkeNadawcyEpuap (OfficeDocument document) throws EdmException;

	/** przekazuje do logiki id, jakie dokument dostal z epuapu oraz date wyslania.
	 * Domyslnie nic nie robi. */
	public void saveEpuapDetails(Document doc, Long epuapId, Date dataWyslania);
	
	/** wpisuje do dokumentu info o doreczeniu (ePUAP Doreczyciel). Domyslnie nie robi zupelnie nic.
	 * 
	 * @param upoDoc	- dokument DS zrobiony na podstawie UPO, moze byc NULL, jesli tego nie robiono.
	 * 					Jesli nie jest null to tym UPO jest UPD doreczenia dokumenu doc.
	 * @param doc		- dokument, kt�ry pomy�lnie doreczono
	 * @param upo		- obiekt wyslany do nas przez ePUAP
	 * @param dtDoreczenia - data doreczenia
	 * @param kod		- kod wyniku operacji zwracany przez ePUAP. (Dostawalem 1 jako sukces) */
	public void oznaczDokumentJakoDostarczony(Document upoDoc, Document doc,
			OdpowiedzPullPobierzTyp upo, Date dtDoreczenia, int kod);

	/** dodaje do dokumentu link do dokumentu zawierajacego UPO */
	public void dodajLinkDoUPO(Document upoDoc, Document doc, String nazwaUpo,
			OdpowiedzPullPobierzTyp upo) throws EdmException;
	
	// ----------------------------------------------------------------------------
	
	/**
	 * Funkcja okre�la czy dokument mo�e mie� cofni�t� archiwizacj�.
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	boolean canUnarchive(Document document) throws EdmException;
	
	/**
	 * Metoda wywo�ywana podczas wykonywania operacji SearchDockindDocumentsAction.moveToArchive()
	 * Pozwala na zdefiniowanie w�asnych p�l na dokumencie podczas operacji globalnych na dokumencie.
	 * Nale�y doda� do selecta na jsp oraz przekazane actionName
	 * @param document
	 * @param documentType
	 * @throws EdmException
	 */
	void globalActionDocument(Document document, int documentType, String actionName) throws EdmException;

	/**
	 * Metoda po wykonanym exporcie do EPUAP w celu wykonanai jaki� czynno�ci
	 * @param officeDocument
	 * @param komunikat
	 * @param epuapDocument
	 */
	public void sendEpuapExportStatus(OutOfficeDocument officeDocument, String komunikat, EpuapExportDocument epuapDocument);

    /**
     * Zwraca maila na podstawie Stringa jaki zosta� przekazany,
     * czasem potrzebne aby wyci�gn�� wiadomo�� email z innej tabeli w powiadomieniach email jbpm.
     *
     * @see pl.compan.docusafe.core.jbpm4.SendMailListener
     * @param document
     * @param mailForLogicCn
     * @return
     */
    public String getMailForJbpmMailListener(Document document, String mailForLogicCn);

    /**
     * Metoda pozwala zdefiniowa� parametry potrzebne do wys�ania maila.
     * Pierwszy parametr to parametr documentId
     * @param document
     * @param eventParams
     * @param documentMailBean
     */
    public void prepareMailToSend(Document document, String[] eventParams, DocumentMailHandler.DocumentMailBean documentMailBean);

    /**
     * Metoda umo�liwia dodanie dodatkowych warto�ci do maila, kt�ry jest wysy�any przez {@link pl.compan.docusafe.core.jbpm4.SendMailListener}
     * @param fm FieldsManager dokumentu, na potrzeby kt�rego u�yto powiadomienia w procesie
     * @param ctext Map<String, Object> mapa z warto�ciami przekazywana do szablonu maila (klucz jest u�yty w szablonie)
     */
    public void addSpecialValueForProcessMailNotifier(FieldsManager fm, Map<String, Object> ctext) throws EdmException;

    public void onAddRevision(Document document)throws EdmException;

    void onDeletedRevision(Document document) throws EdmException;

    /**
     Metoda wywo�ywana przed zapisem warto�ci z dokumentu do bazy.

     @param document
     */
    public void beforeSetWithHistory(Document document, Map<String, Object> values, String activity) throws EdmException;

  
  /**
     * Metoda okresla czy przy wyszukiwaniu potrzebna jest weryfikacja uprawnien do dziennik�w
     * @return
     */
	boolean canSearchAllJournals();
	
    /**
     * Metoda zwraca liste dziennik�w danego typu do kt�rych urzytkownik szukaj�cy ma uprawnienia
     * W przyapdku nie okreslenia listy w logice dokumentu zwaracana jest pe�na lista dziennik�w danego typu
     * @param journalType
     * @return List<Journmal>
     * @throws EdmException
     */
	public List<Journal> getJournalsPermissed(String journalType) throws EdmException;


    /**
     * Metoda przeznaczona do modyfikacji informacji o dokumencie, kt�re s� wy�wietlane na li�cie zada�
     *
     * @param document dokument opisywany przez Obiekt task
     * @param tabName nazwa zak�adki, kt�ra zosta�a wywo�ana na li�cie zada� u�ytkownika
     * @param task zadanie (DSW_JBPM_TASKLIST) powi�zanie z dokumentem
     */
    public void modifyTaskViewOnList(Document document, String tabName, Task task);
	
	/**
	 * Pobiera u�ytkownik�w kt�rzy maj� dost�p do dokumentu.
	 * @return
	 */
	public List<DSUser>  getUserWithAccessToDocument(Long documentId);

	
	public void addWeightAndCostsLettersSelectedByValues(OutOfficeDocument document, Integer rodzajGabaryt, boolean listyKrajowe, boolean listEkonomiczny,
			HashMap<Integer, Map<Integer, BigDecimal>> mapaKosztow, Map<Integer, String> mapaRodzajowKraj, HashMap<Integer, Map<Integer, Integer>> tempIloscPoszczegolnychWag);
/**
 * Medota pobiera z logiki dokumentu  dla bierzacego uzytkownika - liste userow mozliwiych do przypisania jako referent
 * @param user 
 * @param users
 * @return list <DSUsers>
 * @throws EdmException 
 */
	public List<DSUser> getUsersAsClerk(DSUser currentUser, List<DSUser> users) throws EdmException;


    /**
     * Odwo�anie wykonywane w akcji onAltertantiveUpdate, po zapisaniu danych z formatki
     * @param document
     * @param values
     * @param activity
     * @return action message
     * @throws EdmException
     */
    String onAlternativeUpdate(Document document, Map<String, Object> values, String activity) throws EdmException;

    /**
     * Wysy�a dokument do zewn�trznego repozytorium, mechanizm powinnien zostac zdefiniowany w ten akcji
     * @param fieldsManager
     * @return
     */
    boolean sendToExternaRepostiory(FieldsManager fieldsManager, String propUuid) throws EdmException;
/**
 * Odwo�anie do logiki dokumentu  po utworzneiu dokumentu ale przed zapisaniem vartosci  multi, oraz dictionary z dokinda
 * domyslnie nic nie robi 
 * @param kind 
 * @param id
 * @param dockindKeys
 */
	void setAfterCreateBeforeSaveDokindValues(DocumentKind kind, Long documentId, Map<String, Object> dockindKeys) throws EdmException;

/**
 * Odwo�anie do logiki dokumentu aby uzupelnic dokinda o  dane  z odpowiedzi z ePUAP
 * @param odpowiedz
 * @param doc 
 * @param values 
 * @throws EdmException
 */
	void setAdditionalValuesFromEpuapDocument(OdpowiedzPullPobierzTyp odpowiedz, InOfficeDocument doc, Map<String, Object> values) throws EdmException, DocumentException, IOException;

    /**
     * Metoda ma zazadanie wykona� dodatkowe operacje podczas aktulizacji dokumentu przez Cmis Api
     * @param document
     * @param fieldValues
     */
    void updateInCmisApi(Document document,Map<String, Object> fieldValues) throws EdmException;

    /**
     * Metoda ma za zadanie podnie�� wersj� identyfikatora dokumentu po wykonanej na nim akcji
     * @param document
     * @throws Exception
     */
    void updateDocumentBarcodeVersion(Document document) throws Exception;

    /**
     * Odwo�anie do logiki dokumentu aby zmieni� warto�ci wy�wietlania p�l enum przy s�ownikach multi
     * @param enumValues
     * @param enumSelectedSet
     * @param cn
     * @return
     * @throws EdmException 
     */
    public Map<String, String> modifyEnumValues(Map<String, String> enumValues, ArrayList<String> enumSelectedSet, String cn, Long id) throws EdmException;

    /**
     * Odwolanie do dokuemntu  wywolywane podczas  tworzenia noweg odokuemntuw metodzie setBeforeCreate() w celu dodania poprawnych wpisow na dokumencie w kolumnach currentAssignedGuid i jesli jest currentassignemtusername  - w przyapdku gdy na dokindzie wystepuje field document-user-division-second a nie chcemy aby pismo trafilo do odbiorcy okreslonym na dokindzie 
     * bo w metodzie setBeforeCreate(doc) ustawiane sa  domyslnie wartosci zczytywane z fielda (division i user) 
     * @param doc
     * @param fastAssignmentSelectUser
     * @param fastAssignmentSelectDivision
     * @throws EdmException 
     * @throws UserNotFoundException 
     */
	void revaluateFastAssignmentSelectetValuesOnDocument(OfficeDocument doc, String fastAssignmentSelectUser, String fastAssignmentSelectDivision)  throws UserNotFoundException, EdmException;

	/**
	 * Zwraca nazw� dla pliku bez rozszerzenia. Standardowa implementacja zwraca <code>templateName</code> przekazany w prametrze.
	 * @param id
	 * @param templateName
	 * @return
	 */
	String getTemplateFileName(long id, String templateName);

	/**
	 * metoda powinn zwraca� liste userNames�w dla ktorych mamy wys�a� maila z akcj� na procesie 
	 * @param documentId 
	 * @return
	 * @throws EdmException 
	 * @throws DocumentNotFoundException 
	 */
	List<String> getUsersToProcesActionFromEmail(Long documentId) throws DocumentNotFoundException, EdmException;
	
	void addSpecialValueToSendingActionEmail(OfficeDocument doc, StringBuilder emailTresc, DSUser odbiorca) throws EdmException;
/**
 * MEtoda umozliwiajaca zmienienie ukrycie wyswietlanych pozycji podczas ladowania warto�ci s�ownika wielowarto�ciowego
 * @param documentId 
 * 
 * @param abstractDictionaryField
 * @param dictionaryIds przekazane id wybranych pozycji kt�re zostan� wyswietlone na s�owniku
 * @throws EdmException 
 * @throws AccessDeniedException 
 * @throws DocumentNotFoundException 
 */
	void revaluateMultipleDictionaryValues(Long documentId, AbstractDictionaryField abstractDictionaryField, Collection<Long> dictionaryIds) throws DocumentNotFoundException,  AccessDeniedException, EdmException;

    /**
     * Metoda zwracaj�ca ilo�� dokument�w dla jednego pisma zarejestrowanego w systemie.
     * Domy�lnie b�dzie to warto�� 1.
     * */
    Integer getOfficeDocumentsCount(OfficeDocument doc);

   
}


