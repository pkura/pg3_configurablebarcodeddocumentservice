package pl.compan.docusafe.core.dockinds.logic;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;

import java.util.List;
import java.util.Map;
/* User: Administrator, Date: 2007-02-27 14:10:41 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class NormalLogic extends AbstractDocumentLogic
{
    public static final String RECIPIENT_HERE = "RECIPIENT_HERE";
    public static final String ADD_RECIPIENT_HERE = "ADD_RECIPIENT_HERE";
    private static NormalLogic instance;

    public static NormalLogic getInstance()
    {
        if (instance == null)
            instance = new NormalLogic();
        return instance;
    }

    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {

    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        if (DocumentLogic.TYPE_ARCHIVE == type)
            document.setFolder(Folder.getRootFolder());
        else
            document.setFolder(Folder.findSystemFolder(Folder.SN_OFFICE));
        document.setDoctypeOnly(null);
    }
    
    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
    {
    	if(document instanceof InOfficeDocument)
    	{
    		InOfficeDocumentDelivery delivery = ((InOfficeDocument)document).getDelivery();
    		if(delivery != null && "ePUAP".equals(delivery.getName()))
    		{
    			List<ProcessCoordinator> list =  ProcessCoordinator.find(DocumentKind.NORMAL_KIND, "epuap");
    			if(list != null && list.size() > 0)
    				return list.get(0);
    		}
    	}
    	return null;
    }

	public void documentPermissions(Document document) throws EdmException 
	{	
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ,"DOCUMENT_READ",ObjectPermission.GROUP,"Dokumenty - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"DOCUMENT_READ_ATT_READ",ObjectPermission.GROUP,"Dokumenty zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"DOCUMENT_READ_MODIFY",ObjectPermission.GROUP,"Dokumenty - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"DOCUMENT_READ_ATT_MODIFY",ObjectPermission.GROUP,"Dokumenty zalacznik - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.DELETE,"DOCUMENT_READ_DELETE",ObjectPermission.GROUP,"Dokumenty - usuwanie"));
   	 	
   	 	for (PermissionBean perm : perms)    		
   	 	{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
   	 	}
   	 	//this.setUpPermission(document, perms);	
	}

    /**
     * Dla zwyk�ych dokument�w musi by� r�czny wyb�r rodzaju pisma przychodz�cego - dlatego zwracam null.
     */
  //  public String getInOfficeDocumentKind()
    {
 //       return null;
    }
    
    /**
     * Zwraca polityk� bezpiecze�stwa dla dokument�w tego rodzaju.
     */
    public int getPermissionPolicyMode()
    {
        return PermissionManager.NORMAL_POLICY;
    }
    
    @Override
    public boolean isPersonalRightsAllowed()
    {
    	return true;
    }

}
