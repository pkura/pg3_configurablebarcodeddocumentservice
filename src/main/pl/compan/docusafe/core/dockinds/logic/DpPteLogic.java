package pl.compan.docusafe.core.dockinds.logic;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.ModifyStrictPolicy;
import pl.compan.docusafe.core.base.permission.PermissionPolicy;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DpInst;
import pl.compan.docusafe.core.dockinds.dictionary.DpStrona;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import static pl.compan.docusafe.util.FolderInserter.*;
import static java.lang.String.format;

/**
 *
 * @author Micha� Sankowski
 */
public class DpPteLogic extends AbstractDocumentLogic
{

    /*******
     * POLA
     *******/
    public final static String DATA_FIELD_CN = "DATA";
    public final static String DATA_UMOWY_FIELD_CN = "DATA_UMOWY";
    public final static String DATA_ANEKSU_FIELD_CN = "DATA_ANEKSU";
    public final static String DATA_POWIADOMIENIA_FIELD_CN = "DATA_POWIADOMIENIA";
    public final static String KLASA_FIELD_CN = "KLASA";
    public final static String TYP_FIELD_CN = "TYP";
    public final static String INSTYTUCJA_KONTRAHENT_FIELD_CN = "INSTYTUCJA_KONTRAHENT";
    public final static String INSTYTUCJA_FIELD_CN = "INSTYTUCJA";
    public final static String KANCELARIA_FIELD_CN = "KANCELARIA";
    public final static String KANCELARIA_KOMORNICZA_FIELD_CN = "KANCELARIA_KOMORNICZA";
    public final static String NUMER_PORZADKOWY_FIELD_CN = "NUMER_PORZADKOWY";
    public final static String REPERTORIUM_FIELD_CN = "REPERTORIUM";
    public final static String SYGNATURA_FIELD_CN = "SYGNATURA";
    public final static String NAZWISKO_FIELD_CN = "NAZWISKO";
    public final static String IMIE_FIELD_CN = "IMIE";
    public final static String DLUZNIK_FIELD_CN = "DLUZNIK";
    public final static String PODTYP_DOKUMENTY_REJESTROWE_FIELD_CN = "PODTYP_DOKUMENTY_REJESTROWE";
    public final static String PODTYP2_DOKUMENTY_REJESTROWE_FIELD_CN = "PODTYP_DOKUMENTY_REJESTROWE2";
    public final static String OPIS_UMOWY_CN = "OPIS_UMOWY";
    /**********
     * KLASY
     **********/
    public final static String UMOWY_CN = "UMOWY";
    public final static String PROTOKOLY_ZARZADU_CN = "PROTOKOLY_ZARZADU";
    public final static String PROTOKOLY_WALNEGO_ZGROMADZENIA_CN = "PROTOKOLY_WALNEGO_ZGROMADZENIA";
    public final static String PROTOKOLY_RADY_NADZORCZEJ_CN = "PROTOKOLY_RADY_NADZORCZEJ";
    public final static String KORESPONDENCJA_KNF_CN = "KORESPONDENCJA_KNF";
    public final static String KONTROLA_KNF_CN = "KONTROLA_KNF";
    public final static String KONTROLA_INNE_CN = "KONTROLA_INNE";
    public final static String SPRAWY_EGZEKUCYJNE_CN = "SPRAWY_EGZEKUCYJNE";
    public final static String DOKUMENTY_REJESTROWE_CN = "DOKUMENTY_REJESTROWE";
    public final static String PELNOMOCNICTWA_CN = "PELNOMOCNICTWA";
    public final static String DECYZJE_KNF_CN = "DECYZJE_KNF";
    public final static String KSIEGA_AKCYJNA_CN = "KSIEGA_AKCYJNA";
    public final static String UMOWY_AKWIZYCYJNE_CN = "UMOWY_AKWIZYCYJNE";
    public final static String UMOWY_INWESTYCYJNE_CN = "UMOWY_INWESTYCYJNE";
    public final static String UMOWY_OFE_CN = "UMOWY_OFE";
    public final static String UMOWY_PTE_CN = "UMOWY_PTE";
    public final static String POLACZNIE_PTE_SKARBIEC = "POLACZNIE_PTE_SKARBIEC";
    public final static String SPRAWY_SADOWE_CN = "SPRAWY_SADOWE";
    public final static String KORESPONDENCJA_CN = "KORESPONDENCJA";
    /********
     * TYPY
     ********/
    public final static String PROTOKOL_ZARZADU_CN = "PROTOKOL_ZARZADU";
    public final static String UCHWALA_ZARZADU_CN = "UCHWALA_ZARZADU";
    public final static String PROTOKOL_WALNEGO_ZGROMADZENIA_CN = "PROTOKOL_WZ";
    public final static String UCHWALA_WALNEGO_ZGROMADZENIA_CN = "UCHWALA_WZ";
    public final static String PROTOKOL_RADY_NADZORCZEJ_CN = "PROTOKOL_RADY";
    public final static String UCHWALA_RADY_NADZORCZEJ_CN = "UCHWALA_RADY";
    public final static String TYP_OPISOWY_CN = "TYP_OPISOWY";
//	public final static String _CN ="";
    public static final String STATUS_FIELD_CN = "STATUS";
    public static final Integer STATUS_PRZYJETY = 1;
    public static final Integer STATUS_W_REALIZACJI = 2;
    public static final Integer STATUS_ZAWIESZONY = 3;
    public static final Integer STATUS_ZREALIZOWANY = 4;
    public static final Integer STATUS_ODRZUCONY = 5;
    public static final Integer STATUS_ARCHIWALNY = 6;
    private final static Logger log = LoggerFactory.getLogger(DpPteLogic.class);
    //(thread safe)
    public static final Pattern GROUP_READ_PERMISSION_PATTERN = Pattern.compile("\\ADP_PTE.*READ\\z");

    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
        log.trace("DpPteLogic#validate");
        String klasaCN = kind.getFieldByCn(KLASA_FIELD_CN).getEnumItem(Integer.parseInt(values.get(KLASA_FIELD_CN).toString())).getCn();

        log.trace("Klasa:{}", klasaCN);
        if (klasaCN.equals(PELNOMOCNICTWA_CN))
        {
            if (!((values.get(INSTYTUCJA_KONTRAHENT_FIELD_CN) != null)
                    ^ (values.get(NAZWISKO_FIELD_CN) != null || values.get(IMIE_FIELD_CN) != null)))
            {
                throw new EdmException("Nale�y wype�ni� pola imi�, nazwisko albo Insytucja/kontrahent");
            }
        }
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        log.trace("DpPteLogic#archiveActions");

        if (document.getDocumentKind() == null)
        {
            throw new NullPointerException("document.getDocumentKind()");
        }

        String newTitle = null;
        String newDescription = null;

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

        String klasa = fm.getEnumItemCn(KLASA_FIELD_CN);
        String typCN = fm.getEnumItemCn(TYP_FIELD_CN);

        FolderInserter inserter = root().
                subfolder("Archiwum Dokument�w Prawnych").
                subfolder(fm.getValue(KLASA_FIELD_CN).toString());

        if (klasa.equals(UMOWY_CN)
                || klasa.equals(UMOWY_AKWIZYCYJNE_CN)
                || klasa.equals(UMOWY_INWESTYCYJNE_CN)
                || klasa.equals(UMOWY_OFE_CN)
                || klasa.equals(UMOWY_PTE_CN))
        {
            if (klasa.equals(UMOWY_INWESTYCYJNE_CN))
            {
                inserter = inserter.subfolder(fm.getValue(TYP_FIELD_CN).toString());
            }

            DpInst inst = (DpInst) fm.getValue(INSTYTUCJA_KONTRAHENT_FIELD_CN);
            inserter = inserter.subfolder(toQuarter(inst.getName())).
                    subfolder(inst.getName());

            String typ = (String) fm.getValue(TYP_FIELD_CN);
            if (fm.getKey(TYP_FIELD_CN).equals(510) || klasa.equals(UMOWY_INWESTYCYJNE_CN)) //typ opisowy
            {
                typ = ((String) fm.getValue(OPIS_UMOWY_CN));
            }

            newTitle = format("%s (%s)",
                    typ,
                    fm.getValue(DATA_UMOWY_FIELD_CN) == null
                    ? toYearMonthDay(fm.getValue(DATA_ANEKSU_FIELD_CN))
                    : toYearMonthDay(fm.getValue(DATA_UMOWY_FIELD_CN)));

            newDescription = format("%s (%s)", typ, inst.getName());

        } else if (klasa.equals(PROTOKOLY_ZARZADU_CN))
        {
            Object date = fm.getValue(DATA_FIELD_CN);
            inserter = inserter.subfolder(toYear(date)).subfolder(toNumberMonth(date));

            newTitle = format("%s (%s/%s)",
                    fm.getValue(TYP_FIELD_CN),
                    fm.getValue(NUMER_PORZADKOWY_FIELD_CN),
                    toYearMonthDay(date));

            newDescription = newTitle;
        } else if (klasa.equals(PROTOKOLY_WALNEGO_ZGROMADZENIA_CN))
        {
            Object date = fm.getValue(DATA_FIELD_CN);
            inserter = inserter.subfolder(toYear(date)).subfolder(toNumberMonth(date));

            newTitle = typCN.equals(PROTOKOL_WALNEGO_ZGROMADZENIA_CN)
                    ? format("%s (%s/%s)", fm.getValue(TYP_FIELD_CN),
                    fm.getValue(REPERTORIUM_FIELD_CN),
                    toYearMonthDay(date))
                    : format("%s (%s)", fm.getValue(TYP_FIELD_CN),
                    toYearMonthDay(date));


            newDescription = typCN.equals(PROTOKOL_WALNEGO_ZGROMADZENIA_CN)
                    ? format("%s (%s/%s)", fm.getValue(KLASA_FIELD_CN),
                    fm.getValue(REPERTORIUM_FIELD_CN),
                    toYearMonthDay(date))
                    : format("%s (%s)", fm.getValue(TYP_FIELD_CN),
                    toYearMonthDay(date));

        } else if (klasa.equals(KORESPONDENCJA_KNF_CN))
        {
            Object date = fm.getValue(DATA_FIELD_CN);
            inserter = inserter.subfolder(toYear(date)).
                    subfolder(toNumberMonth(date)).
                    subfolder(fm.getValue(SYGNATURA_FIELD_CN).toString());

            newTitle = format("%s (%s)", fm.getValue(TYP_FIELD_CN),
                    toYearMonthDay(date));

            newDescription = newTitle;
        } else if (klasa.equals(KONTROLA_KNF_CN))
        {
            Object date = fm.getValue(DATA_POWIADOMIENIA_FIELD_CN);
            inserter = inserter.subfolder(toYear(date)).
                    //subfolder(toNumberMonth(date)).
                    subfolder(fm.getValue(SYGNATURA_FIELD_CN).toString());

            newTitle = format("%s (%s)", fm.getValue(TYP_FIELD_CN),
                    toYearMonthDay(date));

            newDescription = newTitle;
        } else if (klasa.equals(PROTOKOLY_RADY_NADZORCZEJ_CN))
        {
            Object date = fm.getValue(DATA_FIELD_CN);
            inserter = inserter.subfolder(toYear(date)).
                    subfolder(toNumberMonth(date));

            newTitle = //typCN.equals(PROTOKOL_RADY_NADZORCZEJ_CN)?
                    format("%s (%s/%s)", fm.getValue(TYP_FIELD_CN),
                    fm.getValue(NUMER_PORZADKOWY_FIELD_CN),
                    toYearMonthDay(date));//:
            //format("%s (%s)",fm.getValue(TYP_FIELD_CN),
            //toYearMonthDay(date));

            newDescription = //typCN.equals(PROTOKOL_RADY_NADZORCZEJ_CN)?
                    format("%s (%s/%s)", fm.getValue(TYP_FIELD_CN),
                    fm.getValue(NUMER_PORZADKOWY_FIELD_CN),
                    toYearMonthDay(date));//:
            //format("%s (%s)",fm.getValue(TYP_FIELD_CN),
            //toYearMonthDay(date));
        } else if (klasa.equals(KONTROLA_INNE_CN))
        {
            Object date = fm.getValue(DATA_FIELD_CN);
            inserter = inserter.subfolder(fm.getValue(INSTYTUCJA_FIELD_CN).toString()).
                    subfolder(toYear(date)).
                    subfolder(toNumberMonth(date)).
                    subfolder(fm.getValue(SYGNATURA_FIELD_CN).toString());

            newTitle = format("%s (%s)", fm.getValue(TYP_FIELD_CN),
                    toYearMonthDay(date));
            newDescription = newTitle;
        } else if (klasa.equals(SPRAWY_EGZEKUCYJNE_CN))
        {

            inserter = inserter.subfolder(to2Letters(fm.getValue(DLUZNIK_FIELD_CN).toString())).
                    subfolder(fm.getValue(DLUZNIK_FIELD_CN).toString());

            newTitle = format("%s (%s)", fm.getValue(TYP_FIELD_CN),
                    toYearMonthDay(fm.getValue(DATA_FIELD_CN)));
            newDescription = newTitle;
        } else if (klasa.equals(DOKUMENTY_REJESTROWE_CN))
        {
            Object date = fm.getValue(DATA_FIELD_CN);
            String typ = fm.getValue(TYP_FIELD_CN).toString();

            if (typ.substring(0, 4).equals("PTE"))
            {
                String podtyp = fm.getValue(PODTYP_DOKUMENTY_REJESTROWE_FIELD_CN).toString();
                inserter = inserter.subfolder(typ.substring(0, 4)). //PTE lub OFE
                        subfolder(typ.substring(4)).
                        subfolder(podtyp.substring(0, 4)). //RHB lub KRS
                        subfolder(toYear(date));

            } else if (typ.equals("AEGON PTE"))
            {
                inserter = inserter.subfolder(typ).subfolder(toYear(date));
            } else
            {
                inserter = inserter.subfolder(typ.substring(0, 4)). //PTE lub OFE
                        subfolder(typ.substring(4)).
                        subfolder(toYear(date));
            }

            newTitle = format("%s (%s)", fm.getValue(TYP_FIELD_CN),
                    toYearMonthDay(date));
            newDescription = newTitle;
        } else if (klasa.equals(PELNOMOCNICTWA_CN))
        {
            Object date = fm.getValue(DATA_FIELD_CN);

            String name = StringUtils.trimToEmpty(fm.getStringValue(NAZWISKO_FIELD_CN)) + " "
                    + StringUtils.trimToEmpty(fm.getStringValue(IMIE_FIELD_CN)) + " "
                    + StringUtils.trimToEmpty(fm.getStringValue(INSTYTUCJA_KONTRAHENT_FIELD_CN));

            name = name.trim();
            name += ", " + fm.getValue(TYP_FIELD_CN);

            log.trace("name =\"{}\"", name);

            inserter = inserter.subfolder(to2Letters(name));

            newTitle = format("%s (%s)", name, toYearMonthDay(date));
            newDescription = newTitle;
        } else if (klasa.equals(DECYZJE_KNF_CN))
        {
            Object date = fm.getValue(DATA_FIELD_CN);
            inserter = inserter.subfolder(toYear(date)).subfolder(toNumberMonth(date)).subfolder(fm.getValue(SYGNATURA_FIELD_CN).toString());

            newTitle = format("%s / %s / %s / %s ", fm.getValue(KLASA_FIELD_CN), fm.getValue(TYP_FIELD_CN), fm.getValue(SYGNATURA_FIELD_CN), toYearMonthDay(date));

            newDescription = newTitle;
        } else if (klasa.equals(KSIEGA_AKCYJNA_CN))
        {
            Object date = fm.getValue(DATA_FIELD_CN);
            inserter = inserter.subfolder(toYear(date)).subfolder(toNumberMonth(date));

            newTitle = format("%s (%s)", fm.getValue(KLASA_FIELD_CN), toYearMonthDay(date));

            newDescription = newTitle;

        } /*else if(klasa.equals(UMOWY_AKWIZYCYJNE_CN))
        {
        
        }
        else if(klasa.equals(UMOWY_INWESTYCYJNE_CN))
        {
        
        }
        else if(klasa.equals(UMOWY_OFE_CN))
        {
        
        }
        else if(klasa.equals(UMOWY_PTE_CN))
        {
        
        }*/ else if (klasa.equals(POLACZNIE_PTE_SKARBIEC))
        {
            Object date = fm.getValue(DATA_FIELD_CN);
            inserter = inserter.subfolder(toYear(date)).subfolder(toNumberMonth(date));

            String typ = ((String) fm.getValue(OPIS_UMOWY_CN));
            newTitle = format("%s (%s)", fm.getValue(OPIS_UMOWY_CN), toYearMonthDay(date));
            newDescription = newTitle;
        } else if (klasa.equals(SPRAWY_SADOWE_CN))
        {
            Date date = (Date) fm.getValue(DATA_FIELD_CN);
            newTitle = format("%s (%s)", fm.getValue(TYP_FIELD_CN), toYearMonthDay(date));
            newDescription = newTitle;

            DpStrona strona = (DpStrona) fm.getValue("STRONA");

            //inserter = inserter.subfolder(document.getDocumentKind().getFieldByCn(KLASA_FIELD_CN).getEnumItemByCn(SPRAWY_SADOWE_CN).getTitle());
            inserter = inserter.subfolder(strona.getNazwisko() + " " + strona.getImie() + " " + strona.getSygnatura());
        } else if (klasa.equals(KORESPONDENCJA_CN))
        {
            Date date = (Date) fm.getValue(DATA_FIELD_CN);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            newTitle = format("%s (%s)", fm.getValue(TYP_FIELD_CN), toYearMonthDay(date));
            newDescription = newTitle;

            DpInst inst = (DpInst) fm.getValue("INSTYTUCJA_KONTRAHENT");

            //inserter = inserter.subfolder(document.getDocumentKind().getFieldByCn(KLASA_FIELD_CN).getEnumItemByCn(KORESPONDENCJA_CN).getTitle());
            inserter = inserter.subfolder(inst.getName());
            inserter = inserter.subfolder(String.valueOf(cal.get(Calendar.YEAR)));
            inserter = inserter.subfolder(FolderInserter.toMonth(cal.getTime()));

        } else
        {
            throw new RuntimeException("Nieobs�ugiwana klasa dokumentu");
        }

        inserter.insert(document);

        document.setTitle(newTitle);
        document.setDescription(newDescription);

        /*if(document instanceof OfficeDocument){
        ((OfficeDocument)document).setSummary(newDescription);
        }*/
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        log.trace("DpPteLogic#setInitialValues");
        //fm.reloadValues(Collections.singletonMap(DATA_FIELD_CN, (Object)new Date()));
        //fm.reloadValues(Collections.singletonMap(DATA_POWIADOMIENIA_FIELD_CN, (Object)new Date()));

        Map<String, Object> values = new HashMap<String, Object>();
//        values.put(DATA_FIELD_CN, new Date());
        if (DocumentLogic.TYPE_ARCHIVE == type)
        {
            values.put(STATUS_FIELD_CN, STATUS_ARCHIWALNY);
        } else
        {
            values.put(STATUS_FIELD_CN, STATUS_PRZYJETY);
        }

        values.put(DATA_FIELD_CN, (Object) new Date());
        values.put(DATA_POWIADOMIENIA_FIELD_CN, (Object) new Date());

        fm.reloadValues(values);

    }

    @Override
    public boolean isReadPermissionCode(String permCode)
    {
        return GROUP_READ_PERMISSION_PATTERN.matcher(permCode).matches();
    }

    public void documentPermissions(Document document) throws EdmException
    {
        log.trace("DpPteLogic#documentPemissions");
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        EnumItem klasa = fm.getEnumItem(KLASA_FIELD_CN);

        DSApi.context().clearPermissions(document);

        DSApi.context().grant(document, new String[]
                {
                    ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS
                }, "DP_PTE_" + klasa.getCn() + "_READ", ObjectPermission.GROUP);
        DSApi.context().grant(document, new String[]
                {
                    ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS
                }, "DP_PTE_" + klasa.getCn() + "_MODIFY", ObjectPermission.GROUP);

        DSApi.context().grant(document, new String[]
                {
                    ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS
                }, "DP_PTE_READ", ObjectPermission.GROUP);
        DSApi.context().grant(document, new String[]
                {
                    ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS
                }, "DP_PTE_MODIFY", ObjectPermission.GROUP);

        try
        {
            DSApi.context().session().flush();
        } catch (HibernateException ex)
        {
            throw new EdmHibernateException(ex);
        }
    }

    @Override
    public PermissionPolicy getPermissionPolicy()
    {
        return ModifyStrictPolicy.getInstance();
    }

    @Override
    public void correctValues(Map<String, Object> values, DocumentKind kind) throws EdmException
    {
        log.trace("DpPteLogic#correctValues");
        if (values.get(KLASA_FIELD_CN).toString().equals(
                kind.getFieldByCn(KLASA_FIELD_CN).getEnumItemByCn(KONTROLA_KNF_CN).getId().toString()))
        {
            if (values.get(DATA_POWIADOMIENIA_FIELD_CN) == null)
            {
                values.put(DATA_POWIADOMIENIA_FIELD_CN, new Date());
            }
        } else
        {
            if (values.get(DATA_FIELD_CN) == null)
            {
                values.put(DATA_FIELD_CN, new Date());
            }
        }
    }
}
