package pl.compan.docusafe.core.dockinds.logic;

import pl.compan.docusafe.parametrization.aegon.InvoicePteLogic;
import pl.compan.docusafe.parametrization.ald.ALDLogic;
import pl.compan.docusafe.parametrization.ald.ALDUmowyLogic;
import pl.compan.docusafe.parametrization.altmaster.AltmasterLogic;
import pl.compan.docusafe.parametrization.archiwum.SpisDokDoAPLogic;
import pl.compan.docusafe.parametrization.bialystok.ProtokolDocLogic;
import pl.compan.docusafe.parametrization.bialystok.WykazDocLogic;
import pl.compan.docusafe.parametrization.ilpol.BackofficeLogic;
import pl.compan.docusafe.parametrization.ilpol.CrmAppLogic;
import pl.compan.docusafe.parametrization.ilpol.CrmBusinessTaskLogic;
import pl.compan.docusafe.parametrization.ilpol.CrmMarketingTaskLogic;
import pl.compan.docusafe.parametrization.ilpol.CrmTaskLogic;
import pl.compan.docusafe.parametrization.ilpol.CrmVindicationLogic;
import pl.compan.docusafe.parametrization.ilpol.DLBinder;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.parametrization.ins.CostInvoiceLogic;
import pl.compan.docusafe.parametrization.ins.WniosekZapDoDostLogic;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralLogic;
import pl.compan.docusafe.parametrization.opzz.OpzzCasesLogic;
import pl.compan.docusafe.parametrization.opzz.OpzzDocLogic;
import pl.compan.docusafe.parametrization.opzz.OpzzDocOpinionLogic;
import pl.compan.docusafe.parametrization.opzz.OpzzOdznakaLogic;
import pl.compan.docusafe.parametrization.opzz.OpzzProblemLogic;
import pl.compan.docusafe.parametrization.opzz.OpzzQuestLogic;
import pl.compan.docusafe.parametrization.opzz.OpzzCzlonkostwoLogic;
import pl.compan.docusafe.parametrization.pika.PikaLogic;
import pl.compan.docusafe.parametrization.presale.BankLogic;
import pl.compan.docusafe.parametrization.presale.ComplainIronLogic;
import pl.compan.docusafe.parametrization.presale.DJLogic;
import pl.compan.docusafe.parametrization.presale.UmowaPresentationLogic;
import pl.compan.docusafe.parametrization.presale.ZamowienieLogic;
import pl.compan.docusafe.parametrization.prosika.ProsikaBlanklogic;
import pl.compan.docusafe.parametrization.prosika.Prosikalogic;
import pl.compan.docusafe.parametrization.ra.RockwellLogic;
import pl.compan.docusafe.parametrization.umwawa.AktNotarialnyLogic;
import pl.compan.docusafe.parametrization.wssk.*;
import pl.compan.docusafe.parametrization.adm.*;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.parametrization.ukw.PlanStudiowLogic;
import pl.compan.docusafe.parametrization.ukw.PodzialStudiowNaGrupyLogic;
import pl.compan.docusafe.parametrization.ukw.WniosekOPozyczkeLogic;
import pl.compan.docusafe.parametrization.ukw.WniosekOZapomogeLogic;
import pl.compan.docusafe.parametrization.ukw.WniosekODelegacjeLogic;
import pl.compan.docusafe.parametrization.ukw.UmowaCywilnoPrawnaLogic;
import pl.compan.docusafe.parametrization.ukw.WniosekOUrlopLogic;



/**
 * Klasa odpowiedzialna za ladowanie logiki dokumentu
 * Celem jej jest wyciagniecie z klasy DocumentLogic funkcji ladowania
 * Pozniejszym celem jest zrobienie ladowania klas w sposob bardziej elegancki
 * @author wkutyla
 */
public class DocumentLogicLoader 
{
	private static final Logger log = LoggerFactory.getLogger(DocumentLogicLoader.class);
	
	/** rodzaj dokumentu okre�laj�cy zwyk�� dokumenty nie-biznesowe */
    public static final String NORMAL_KIND = "normal";
    /** rodzaj okre�laj�cy dokumenty biznesowe dla Nationwide */
    public static final String NATIONWIDE_KIND = "nationwide";
    /** rodzaj okre�laj�cy dokumenty rejestru szk�d dla Nationwide */
    public static final String NATIONWIDE_DRS_KIND = "nationwide_drs";
    /** rodzaj okre�laj�cy dokumenty archiwum agenta dla Nationwide */
    public static final String DAA_KIND = "daa";
    /** rodzaj okre�laj�cy dokumenty dla ALD */
    public static final String ALD_KIND = "ald";
    /** rodzaj okre�laj�cy dokumenty leasingowe dla ALD */
    public static final String ALD_UMOWY_KIND = "aldumowy";
    /** rodzaj okre�laj�cy dokumenty prawne dla Nationwide */
    public static final String DP_KIND = "dp";

    public static final String DP_PTE_KIND = "dp_pte";
    /** rodzaj okre�laj�cy dokumenty finansowe dla Nationwide */
    public static final String DF_KIND = "df";
    /** #NIEWSPIERANY# rodzaj okre�laj�cy dokumenty Backoffice dla Impulsu */
    public static final String BACKOFFICE_KIND = "backoffice";
    /** rodzaj okre�laj�cy dokumenty leasingowe dla Impulsu */
    public static final String DL_KIND = "dl";
    /**Teczka dokumentow leasingowych*/
    public static final String DL_BINDER = "dlbinder";
    /** rodzaj okre�laj�cy dokumenty dla Rockwella */
    public static final String ROCKWELL_KIND = "rockwell";
    /** rodzaj okre�laj�cy dokumenty cz�onkowskie dla Aegon PTE */
    public static final String DC_KIND = "dc";
    /** rodzaj okre�laj�cy faktury kosztowe dla Nationwide */
    public static final String INVOICE_KIND = "invoice";

    /** rodzaj okre�laj�cy faktury kosztowe dla Intercars */
    public static final String INVOICE_IC_KIND = "invoice_ic";
	public static final String SAD_KIND = "sad";

	/** likwidacja szkody AC + OC */
	public static final String HARM_KIND = "harm";

    /** rodzaj okre�laj�cy faktury kosztowe dla banku bph */
    public static final String BANK_KIND = "bank";


    /** #NIEWSPIERANY# application czyli zg�oszenie  ---  modu� CRM */
    public static final String CRMAPP_KIND = "crmApp";
    /** #NIEWSPIERANY# Task czyli zadanie ---  modu� CRM */
    public static final String CRMTASK_KIND = "crmTask";
    /** #NIEWSPIERANY# Zadnia handlowe --- modu� CRM */
    public static final String CRMBUSINESS_TASK_KIND = "crmBusinessTask";

    /**Zadanie windykacji IMPULS*/
    public static final String CRM_VINDICATION_KIND = "crmVindication";
    /**Zadanie kontaktu z klientem IMPULS*/
    public static final String CRM_MARKETING_TASK_KIND = "crmMarketingTask";


    public static final String COMPLAIN_IRON_KIND = "complainIron";

    public static final String JYSK_KIND = "jysk";

    public static final String ZAMOWIENIE = "zamowienie";

    public static final String PROSIKA = "prosika";
    public static final String PROSIKA_BLANK = "prosikablank";

    public static final String BLANK_DOCUMENT_KIND = "blank";

    public static final String AKT_NOTARIALNY = "akt_notarialny";

	public static final String EMPLOYEE_KIND = "pracowniczy";
	
	public static final String P4_KIND = "p4";
	public static final String P4_PARCEL_REGISTER = "parcel_registry";
	
	public static final String ALTMASTER = "altmaster";
	
	public static final String INVOICE_PRESENTATION = "invoice_pn";
	public static final String UMOWA_PRESENTATION = "umowa_pn";
	
	public static final String INVOICE_PTE = "invoice-pte";
	
	/** R�ne dokumenty dla Intercars */
	public static final String CONTRACT_IC_KIND = "contract_ic";

	public static final String EUROPOL_INV = "europol_inv";

	public static final String DOCSPR_PRESENTATION = "DOCSPR_PRESENTATION";

	public static final String RADWAR_PRESENTATION = "radwar";

	public static final String BISK_REK_PRESENTATION = "bisk_reklamacja";

	public static final String HOLIDAY_REQUEST = "holiday-request";

	public static final String COMPLAINT = "complaint";

	public static final String ABSENCE_REQUEST = "absence-request";

	public static final String ABSENCE_WITHOUT_PROCCES = "abs-without-procces";
	/**
     * OPZZ - integracja z Liferay
     */
    public static final String OPZZ_CASES = "opzz_cases";
    public static final String OPZZ_DOC = "opzz_doc";
    public static final String OPZZ_DOC_OPINION ="opzz_doc_opinion";
    public static final String OPZZ_DOC_QUEST ="opzz_quest";
    public static final String OPZZ_PROBLEM = "opzz_problem";
    public static final String OPZZ_CZLONKOSTWO = "opzz_czlonkostwo";
    public static final String OPZZ_ODZNAKA = "opzz_odznaka";
	
	// Bialystok - archiwum
	public static final String WYKAZ_ZDAWCZO_ODBIORCZY = "wykaz";		// przy zmianie nazwy, zmienic jej wystapienie w plikach .jsp
	public static final String PROTOKOL_BRAKOWANIA = "protokol_br";		// przy zmianie nazwy, zmienic jej wystapienie w plikach .jsp

	//PAA
	public static final String PAA_DOC_IN = "paadocin";
	public static final String PAA_DOC_OUT = "paadocout";
	public static final String PAA_DOC_INT = "paadocint";
	public static final String PAA_PLAN_PRACY = "paa_planpracy";
	public static final String PAA_WNIOSEK_URLOPOWY = "paa_wniosek_urlopowy";
	public static final String PAA_PLAN_PRACY_ZAD = "paa_planpracy_zad";
	public static final String PAA_PLAN_PRACY_PODZAD = "paa_planpracy_podzad";
	
	//ADMINISTRACJA
	public static final String ADM_DOC_IN = "normal_in";
	public static final String ADM_DOC_OUT = "normal_out";
	public static final String ADM_DOC_INT = "normal_int";
	public static final String ADM_DOC_BOK = "normal_bok";
	
	// WSSK
	public static final String WSSK_WYKORZYSTANIE_SPRZETU_PRYW	= "prywatny_sprzet";
	public static final String WSSK_UTWORZENIE_KONT_SYSTEMOWYCH	= "konta_syst";
	public static final String WSSK_WYDANIE_PIECZATKI		= "wydanie_pieczatki";
	public static final String WSSK_ZAPOTRZEBOWANIE_WEW		= "zapotrzebowanie_wew";
	public static final String WSSK_KARTA_UNIEWAZNIENIE		= "karta_uniewaznienie";
	public static final String WSSK_KARTA_ZAPOTRZEBOWANIE	= "karta_zapotrzebowanie";
	public static final String WSSK_ZWOLNIONY_PRACOWNIK		= "zwolniony_pracownik";
	public static final String WSSK_NOWY_PRACOWNIK			= "nowy_pracownik";
	public static final String WSSK_ZMIANA_KADROWA_PRACOWNIKA = "zmiana_kadrowa_pracownika";
	public static final String WSSK_URLOP_PRACOWNIKA		= "urlop_pracownika";
	public static final String WSSK_UDO						= "udo";
	public static final String WSSK_POWIADOMIENIE_WYPADEK	= "powiadomienie_wypadek";
	public static final String WSSK_UDOSTEP_DOK_ZEWN		= "udostep_dok_zewn";
	public static final String WSSK_UDOSTEP_DOK_ODP			= "udostep_dok_odp";
	public static final String WSSK_UDOSTEP_DOK_INTERN		= "udostep_dok_intern";
	public static final String WSSK_PACJENT_NIEUBEZP		= "pacjent_nieubezp";
	public static final String WSSK_REOPEN					= "reopen";
	public static final String WSSK_FAKTURA					= "faktura";
	public static final String WSSK_FAKTURA2				= "faktura_opis";
	
	/** PIKA **/
	public static final String PIKA_DOCKIND                 = "pika";
	/** PIKA - PLG */
	public static final String PLG_DOCKIND 					= "plg";
	public static final String PLG_OUT_DOCKIND 				= "plgout";
	
	/** NFOS */
	public static final String NFOS_BLANK				= "blank";
	public static final String NFOS_EMAIL               = "nfosEmail";
	
	
	/** UTP */
	public static final String UTP               = "karta_zastepcza";
	public static final String EMAIL         = "email";
	/** Archiwum Zak�adowe*/
	public static final String AR_PROT_BRAK 				= "ar_prot_brak";
	
	/** ARCHIWUM */
	public static final String ARCHIVES               = "spiszdawczoodbiorczy";
	/** ARCHIWUM UDOSTEPNINIE*/
	public static final String ARCHIVES_UDOSTEPNIENIE               = "udostepnienie";
	/** ARCHIWUM WYCOFANIE*/
	public static final String ARCHIVES_WYCOFANIE               = "wycofanie";
	/** ARCHIWUM PRZEKAZANIE DO AP*/
	public static final String ARCHIVES_PRZEKAZANIE_AP               = "SpisDokDoAP";
    /** INS faktura kosztowa */
    public static final String INS_COST_INVOICE               = "insCostInvoice";
	public static final String WNIOSEK_ZAP				="WniosekZapDoDost";
	
	/** UKW */
	public static final String UKW_WEW_AKTY_PRAW               = "ukw_wew_akty_praw";
	public static final String UKW_WNIOSEK_O_UDZIELENIE_PO             = "ukw_pozyczka";
	public static final String UKW_WNIOSEK_O_ZAPOMOGE             = "ukw_zapomoga";
	public static final String UKW_WNIOSEK_O_DELEGACJE             = "ukw_delegacja";
	public static final String UKW_UMOWA_CYWILNO_PRAWNA            = "ukw_umowa_cyw_praw";
	public static final String UKW_WNIOSEK_O_URLOP				= "wniosek_o_urlop";
	public static final String UKW_PLAN_STUDIOW					= "plan_studiow";
	public static final String UKW_PODZIAL_STUDIOW_STACJ				= "ukw_podzial_studiow";

	/** FAKTURA OG�LNA*/
	public static final String INVOICE_GENERAL= "InvoiceGeneral";
	/**
	 * Pobiera logike dockindu
	 * @param cn
	 * @param logicClass
	 * @return
	 */
	public DocumentLogic getDocumentLogic(String cn,String logicClass)
	{
		DocumentLogic logic = null;
		if (logicClass!=null)
		{
			//TODO zapewnic pojedyncza inicjalizacje (singleton)
			try{
				logic = (DocumentLogic) Class.forName(logicClass).newInstance();
			} catch (Exception e){
				log.error(e.getMessage(), e);
				throw new IllegalStateException("B��d inicjalizacji logiki - klasa: "+logicClass );
			}
		}
		else if (DAA_KIND.equals(cn))
			logic = (DocumentLogic) DaaLogic.getInstance();
		else if (ALD_KIND.equals(cn))
			logic = (DocumentLogic) ALDLogic.getInstance();
		else if (NORMAL_KIND.equals(cn))
			logic = (DocumentLogic) NormalLogic.getInstance();
		else if (DP_KIND.equals(cn))
			logic = (DocumentLogic) DpLogic.getInstance();
		else if (DF_KIND.equals(cn))
			logic = (DocumentLogic) DfLogic.getInstance();
		else if (BACKOFFICE_KIND.equals(cn))
			logic = (DocumentLogic) BackofficeLogic.getInstance();
		else if (ROCKWELL_KIND.equals(cn))
			logic = (DocumentLogic) RockwellLogic.getInstance();
		else if (DC_KIND.equals(cn))
			logic = (DocumentLogic) DcLogic.getInstance();
		else if (INVOICE_KIND.equals(cn))
			logic = (DocumentLogic) InvoiceLogic.getInstance();
		else if (DL_KIND.equals(cn))
			logic = (DocumentLogic) DlLogic.getInstance();
		else if (CRMAPP_KIND.equals(cn))
			logic = (DocumentLogic) CrmAppLogic.getInstance();
		else if (CRMTASK_KIND.equals(cn))
			logic = (DocumentLogic) CrmTaskLogic.getInstance();
		else if (CRMBUSINESS_TASK_KIND.equals(cn))
			logic = (DocumentLogic) CrmBusinessTaskLogic.getInstance();
		else if (CRM_VINDICATION_KIND.equals(cn))
			logic = (DocumentLogic) CrmVindicationLogic.getInstance();
		else if (COMPLAIN_IRON_KIND.equals(cn))
			logic = (DocumentLogic) ComplainIronLogic.getInstance();
		else if (JYSK_KIND.equals(cn))
			logic = (DJLogic) DJLogic.getInstance();
		else if (ALD_UMOWY_KIND.equals(cn))
			logic = (ALDUmowyLogic) ALDUmowyLogic.getInstance();
		else if (BANK_KIND.equals(cn))
			logic = (BankLogic) BankLogic.getInstance();
		else if (ZAMOWIENIE.equals(cn))
			logic = (ZamowienieLogic) ZamowienieLogic.getInstance();
		else if (CRM_MARKETING_TASK_KIND.equals(cn))
			logic = (CrmMarketingTaskLogic) CrmMarketingTaskLogic.getInstance();
		else if (PROSIKA.equals(cn))
			logic = (Prosikalogic) Prosikalogic.getInstance();
		else if (PROSIKA_BLANK.equals(cn))
			logic = (ProsikaBlanklogic) ProsikaBlanklogic.getInstance();
		else if (DL_BINDER.equals(cn))
			logic = (DLBinder) DLBinder.getInstance();
		else if (BLANK_DOCUMENT_KIND.equals(cn))
			logic = (BlankLogic) BlankLogic.getInstance();
		else if (AKT_NOTARIALNY.equals(cn))
			logic = (AktNotarialnyLogic) AktNotarialnyLogic.getInstance();
		else if ((ALTMASTER).equals(cn))
			logic = (AltmasterLogic) AltmasterLogic.getInstance();
		else if ((INVOICE_PRESENTATION).equals(cn))
			logic = (InvoicePresentationLogic) InvoicePresentationLogic.getInstance();
		else if ((UMOWA_PRESENTATION).equals(cn))
			logic = (UmowaPresentationLogic) UmowaPresentationLogic.getInstance();
		else if (INVOICE_PTE.equals(cn))
			logic = (InvoicePteLogic) InvoicePteLogic.getInstance();
		else if(OPZZ_CASES.equals(cn))
			logic = (OpzzCasesLogic) OpzzCasesLogic.getInstance();
		else if(OPZZ_DOC.equals(cn))
			logic = (OpzzDocLogic) OpzzDocLogic.getInstance();
		else if(OPZZ_DOC_OPINION.equals(cn))
			logic = (OpzzDocOpinionLogic) OpzzDocOpinionLogic.getInstance();
		else if(OPZZ_DOC_QUEST.equals(cn))
			logic = (OpzzQuestLogic) OpzzQuestLogic.getInstance();
		else if(OPZZ_PROBLEM.equals(cn))
			logic = (OpzzProblemLogic) OpzzProblemLogic.getInstance();
		else if(OPZZ_CZLONKOSTWO.equals(cn))
			logic = (OpzzCzlonkostwoLogic) OpzzCzlonkostwoLogic.getInstance();
		else if(OPZZ_ODZNAKA.equals(cn))
			logic = (OpzzOdznakaLogic) OpzzOdznakaLogic.getInstance();
		else if(WYKAZ_ZDAWCZO_ODBIORCZY.equals(cn))
			logic = (DocumentLogic) WykazDocLogic.getInstance();
		else if(PROTOKOL_BRAKOWANIA.equals(cn))
			logic = (DocumentLogic) ProtokolDocLogic.getInstance();
		//PAA 
		else if(PAA_DOC_IN.equals(cn))
			logic = (DocumentLogic) pl.compan.docusafe.parametrization.paa.PaaDocInLogic.getInstance();
		else if(PAA_DOC_OUT.equals(cn))
			logic = (DocumentLogic) pl.compan.docusafe.parametrization.paa.PaaDocOutLogic.getInstance();
		else if(PAA_DOC_INT.equals(cn))
			logic = (DocumentLogic) pl.compan.docusafe.parametrization.paa.NormalLogic.getInstance();
		else if(PAA_PLAN_PRACY.equals(cn))
			logic = (DocumentLogic) pl.compan.docusafe.parametrization.paa.NormalLogic.getInstance();
		else if(PAA_WNIOSEK_URLOPOWY.equals(cn))
			logic = (DocumentLogic) pl.compan.docusafe.parametrization.paa.PaaWniosekUrlopowyLogic.getInstance();
		else if(PAA_PLAN_PRACY_ZAD.equals(cn))
			logic = (DocumentLogic) pl.compan.docusafe.parametrization.paa.NormalLogic.getInstance();
		else if(PAA_PLAN_PRACY_PODZAD.equals(cn))
			logic = (DocumentLogic) pl.compan.docusafe.parametrization.paa.NormalLogic.getInstance();
		//NFOS
		else if(NFOS_BLANK.equals(cn))
			logic = (DocumentLogic) pl.compan.docusafe.parametrization.nfos.Minimal_Logic.getInstance();
		
		//UTP
		//Archiwum Zak�adowe
		else if(AR_PROT_BRAK.equals(cn))
			logic = (DocumentLogic) pl.compan.docusafe.parametrization.archiwum.ProtokolBrakowaniaLogic.getInstance();
		else if(UTP.equals(cn))
			logic = (DocumentLogic) pl.compan.docusafe.parametrization.bialystok.NormalLogic.getInstance();
		//Archiwum Zak�adowe
		else if(AR_PROT_BRAK.equals(cn))
			logic = (DocumentLogic) pl.compan.docusafe.parametrization.archiwum.ProtokolBrakowaniaLogic.getInstance();
		//UTP Archiwum
		else if(ARCHIVES.equals(cn))
					logic = (DocumentLogic) pl.compan.docusafe.parametrization.archiwum.SpiszdawczoodbiorczyLogic.getInstance();
		//UTP Archiwum udostepnienie
		else if(ARCHIVES_UDOSTEPNIENIE.equals(cn))
					logic = (DocumentLogic) pl.compan.docusafe.parametrization.archiwum.UdostepnienieLogic.getInstance();
		//UTP Archiwum udostepnienie
		else if(ARCHIVES_WYCOFANIE.equals(cn))
							logic = (DocumentLogic) pl.compan.docusafe.parametrization.archiwum.WycofanieLogic.getInstance();
		//Spis dok do archiwum panstwowego
		else if(ARCHIVES_PRZEKAZANIE_AP.equals(cn))
				logic=(DocumentLogic)SpisDokDoAPLogic.getInstance();
		//UTP email
		else if(EMAIL.equals(cn))
				logic = (DocumentLogic) pl.compan.docusafe.parametrization.utp.EmailLogic.getInstance();
				
		//ADMINISTRACJA
	
		else if(ADM_DOC_IN.equals(cn))
			logic = (DocumentLogic) AdmDocInLogic.getInstance();
		else if(ADM_DOC_OUT.equals(cn))
			logic = (DocumentLogic) AdmDocOutLogic.getInstance();
		else if(ADM_DOC_INT.equals(cn))
			logic = (DocumentLogic) AdmDocIntLogic.getInstance();
		else if(ADM_DOC_BOK.equals(cn))
				logic = (DocumentLogic) AdmDocBokLogic.getInstance();
		//wssk poczatek ----------------------------------
		else if(WSSK_WYKORZYSTANIE_SPRZETU_PRYW.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_UTWORZENIE_KONT_SYSTEMOWYCH.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_WYDANIE_PIECZATKI.equals(cn))
			logic = (DocumentLogic) StampLogic.getInstance();
		else if(WSSK_ZAPOTRZEBOWANIE_WEW.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_KARTA_UNIEWAZNIENIE.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_KARTA_ZAPOTRZEBOWANIE.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_ZWOLNIONY_PRACOWNIK.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_UDO.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_NOWY_PRACOWNIK.equals(cn))
			logic = (DocumentLogic) NewEmployeeLogic.getInstance();
		else if(WSSK_POWIADOMIENIE_WYPADEK.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_UDOSTEP_DOK_ZEWN.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_UDOSTEP_DOK_ODP.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_UDOSTEP_DOK_INTERN.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_PACJENT_NIEUBEZP.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_REOPEN.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_FAKTURA.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		else if(WSSK_FAKTURA2.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		//wssk koniec ----------------------------------
		//ukw pocz�tek----------------------------------
		else if(UKW_WEW_AKTY_PRAW.equals(cn))
			logic = (DocumentLogic) MinimalLogic.getInstance();
		//ukw koniec----------------------------------
		else if(PIKA_DOCKIND.equals(cn))
			logic = (DocumentLogic) PikaLogic.getInstance();
		//else throw new IllegalStateException("Napotkano nieznany typ dokumentu o cn='"+cn+"'");

        else if(INS_COST_INVOICE.equals(cn))
            logic = (DocumentLogic) CostInvoiceLogic.getInstance();
        else if(WNIOSEK_ZAP.equals(cn))
        	logic=(DocumentLogic) WniosekZapDoDostLogic.getInstance();
        else if(INVOICE_GENERAL.equals(cn))
        	logic=(DocumentLogic)InvoiceGeneralLogic.getInstance();
        else if(UKW_WNIOSEK_O_UDZIELENIE_PO.equals(cn))
        	logic=(DocumentLogic)WniosekOPozyczkeLogic.getInstance();
        else if(UKW_WNIOSEK_O_ZAPOMOGE.equals(cn))
        	logic=(DocumentLogic)WniosekOZapomogeLogic.getInstance();
        else if(UKW_WNIOSEK_O_DELEGACJE.equals(cn))
        	logic=(DocumentLogic)WniosekODelegacjeLogic.getInstance();
		else if(UKW_UMOWA_CYWILNO_PRAWNA.equals(cn))
        	logic=(DocumentLogic)UmowaCywilnoPrawnaLogic.getInstance();
 		else if(UKW_WNIOSEK_O_URLOP.equals(cn))
        	logic=(DocumentLogic)WniosekOUrlopLogic.getInstance();
		else if(UKW_PLAN_STUDIOW.equals(cn))
 			logic=(DocumentLogic)PlanStudiowLogic.getInstance();
 		else if(UKW_PODZIAL_STUDIOW_STACJ.equals(cn))
        	logic=(DocumentLogic)PodzialStudiowNaGrupyLogic.getInstance();		
		
		return logic;	
	}
}
