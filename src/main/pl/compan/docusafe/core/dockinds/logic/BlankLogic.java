package pl.compan.docusafe.core.dockinds.logic;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class BlankLogic extends AbstractDocumentLogic 
{

	private static BlankLogic instance;
	protected static Logger log = LoggerFactory.getLogger(BlankLogic.class);
	
	public static synchronized BlankLogic getInstance()
    {
        if (instance == null)
            instance = new BlankLogic();
        return instance;
    }
	
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		Folder folder = Folder.getRootFolder();
    	folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
		document.setFolder(folder);
		
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		document.setTitle(""+(String) fm.getValue(fm.getMainFieldCn()));
		document.setDescription(""+(String) fm.getValue(fm.getMainFieldCn()));
		
	}
	
	public int getPermissionPolicyMode()
    {
        return PermissionManager.NORMAL_POLICY;
    }
	
	@Override
	public boolean searchCheckPermissions(Map<String, Object> values) 
	{
		return false;
	}


	public void documentPermissions(Document document) throws EdmException {}

    public void onStartProcess(OfficeDocument document,ActionEvent event)
    {
    	try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			//map.put(ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
    }
}
