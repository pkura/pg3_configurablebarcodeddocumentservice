package pl.compan.docusafe.core.dockinds.logic;

import java.util.List;

import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.base.absences.FreeDay;

public interface AbsenceLogic 
{
	public List<FreeDay> getFreeDays();
	/**
	 * Zwraca list� urlop�w pracownika w bierzacym roku, dla widoku w dockind-specific-additions.jsp
	 * 
	 * @return
	 */
	public List<EmployeeAbsenceEntry> getEmplAbsences();
	/**
	 * Zwraca kart� pracownika
	 * 
	 * @return
	 */
	public EmployeeCard getEmployeeCard();
}
