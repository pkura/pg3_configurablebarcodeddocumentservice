package pl.compan.docusafe.core.dockinds.logic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DpInst;
import pl.compan.docusafe.core.dockinds.dictionary.DpStrona;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * Logika dokument�w prawnych (dla Aegon).
 * 
 * @author <a href="mailto:piotr.komisarski@com-pan.pl">Piotr Komisarski</a>
 * @version $Id$
 */
public class DpLogic extends AbstractDocumentLogic {
	private StringManager sm = GlobalPreferences.loadPropertiesFile(
			DpLogic.class.getPackage().getName(), null);
	private static DpLogic instance;

	// nazwy kodowe poszczeg�lnych p�l dla tego rodzaju dokumentu
	public static final String KLASA_FIELD_CN = "KLASA";
	public static final String TYP_FIELD_CN = "TYP";
	public static final String DATA_UMOWY_FIELD_CN = "DATA_UMOWY";
	public static final String DATA_ANEKSU_FIELD_CN = "DATA_ANEKSU";
	public static final String DATA_POCZATKOWA_FIELD_CN = "DATA_POCZATKOWA";
	public static final String DATA_KONCOWA_FIELD_CN = "DATA_KONCOWA";
	public static final String DATA_FIELD_CN = "DATA";
	public static final String STRONA_FIELD_CN = "STRONA";
	public static final String SYGNATURA_FIELD_CN = "SYGNATURA";
	public static final String STATUS_FIELD_CN = "STATUS";
	public static final String RODZAJ_SPRAWY_FIELD_CN = "RODZAJ_SPRAWY";
	public static final String INST_FIELD_CN = "INST";
	public static final String IMIE_FIELD_CN = "IMIE";
	public static final String NAZWISKO_FIELD_CN = "NAZWISKO";
	public static final String MIEJSCOWOSC_FIELD_CN = "MIEJSCOWOSC";
	public static final String PRODUKT_FIELD_CN = "PRODUKT";
	public static final String NUMERY_PORZADKOWE_CN = "NUMERY_PORZADKOWE";
	public static final String SAD_REJESTROWY_CN = "SAD_REJESTROWY";

	// nazwy kodowe poszczeg�lnych kategorii dokumentu
	private static final String UMOWY_CN = "UMOWY";
	private static final String UMOWY_TP_CN = "UMOWY Z TPSA";
	private static final String UMOWY_NAJMU_CN = "UMOWY NAJMU";
	private static final String PROTOKO�Y_ZARZADU_CN = "PROTOKOLY ZARZADU";
	private static final String PROTOKO�Y_WALNEGO_CN = "PROTOKOLY WALNEGO ZGROMADZENIA";
	private static final String PROTOKO�Y_RADY_CN = "PROTOKOLY RADY NADZORCZEJ";
	private static final String KORESPONDENCJA_CN = "KORESPONDENCJA";
	private static final String SRAWY_SADOWE_CN = "SPRAWY SADOWE";
	private static final String CERTYFIKATY_CN = "CERTYFIKATY";
	private static final String PELNOMOCNICTWA_CN = "PELNOMOCNICTWA";
	private static final String KRS_CN = "KRS";

	private static final String TYP_OPISOWY_CN = "TYP_OPISOWY"; // 210
	private static final String OPIS_UMOWY_CN = "OPIS_UMOWY";

	private static final String UMOWA_ENUM_REF = "Umowa";
	private static final String ANEKS_DO_UMOWY_ENUM_REF = "Aneks do umowy";
	private static final String POLISA_ENUM_REF = "Polisa";
	private static final String ANEKS_DO_POLISY_ENUM_REF = "Aneks do polisy";

	// Logger
	protected static final Logger log = LoggerFactory.getLogger(DpLogic.class);

	// statusy
	public static final Integer STATUS_PRZYJETY = 1;
	public static final Integer STATUS_W_REALIZACJI = 2;
	public static final Integer STATUS_ZAWIESZONY = 3;
	public static final Integer STATUS_ZREALIZOWANY = 4;
	public static final Integer STATUS_ODRZUCONY = 5;
	public static final Integer STATUS_ARCHIWALNY = 6;

	public static final String DEADLINE_CN = "DEADLINE";
	private static final String[] MONTHS = new String[] { "Stycze�", "Luty",
			"Marzec", "Kwiecie�", "Maj", "Czerwiec", "Lipiec", "Sierpie�",
			"Wrzesie�", "Pa�dziernik", "Listopad", "Grudzie�" };

	public static DpLogic getInstance() {
		if (instance == null)
			instance = new DpLogic();
		return instance;
	}

	public void validate(Map<String, Object> values, DocumentKind kind,
			Long documentId) throws EdmException {
		if (values.get(KLASA_FIELD_CN) == null)
			throw new EdmException("Nie wybrano klasy dokumentu");
	}

	private static String getStringDate(Date date) {
		return DateUtils.formatSqlDate(date != null ? date : new Date());
	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		if (document.getDocumentKind() == null)
			throw new NullPointerException("document.getDocumentKind()");

		String previousFolderPath = document.getFolderPath();
		String previousTitle = document.getTitle();

		FieldsManager fm = document.getDocumentKind().getFieldsManager(
				document.getId());
		Object o = fm.getValue(DATA_FIELD_CN);
		String data = "";
		Calendar cal = Calendar.getInstance();
		if (o == null)
			cal.setTime(new Date());
		else
			cal.setTime((Date) o);
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);

		data = getStringDate(cal.getTime());

		EnumItem kategoriaItem = fm.getEnumItem(KLASA_FIELD_CN);
		String miasto = (String) fm.getValue(MIEJSCOWOSC_FIELD_CN);
		if (miasto == null)
			miasto = "inne miasta";

		String title = null;
		String description = null;
		Folder folder = Folder.getRootFolder();
		folder = folder
				.createSubfolderIfNotPresent("Archiwum Dokument�w Prawnych");

		if (kategoriaItem.getCn().equals(UMOWY_CN)) {
			folder = folder.createSubfolderIfNotPresent("Umowy");
			EnumItem tmp = fm.getEnumItem("TYP");
			DpInst inst = (DpInst) fm.getValue(INST_FIELD_CN);
			String str = inst.getName();
			str = str.toLowerCase();

			/*
			 * Character c = str.charAt(0); if (c.compareTo(new Character('g'))
			 * < 0) { folder = folder.createSubfolderIfNotPresent("A-F"); } else
			 * if (c.compareTo(new Character('m')) < 0) { folder =
			 * folder.createSubfolderIfNotPresent("G-�"); } else if
			 * (c.compareTo(new Character('t')) < 0) { folder =
			 * folder.createSubfolderIfNotPresent("M-S"); } else { folder =
			 * folder.createSubfolderIfNotPresent("T-�"); }
			 */

			folder = folder.createSubfolderIfNotPresent(FolderInserter
					.toQuarter(str));

			String titlePart = tmp.getTitle();
			if (titlePart.equals(UMOWA_ENUM_REF)) {
				title = titlePart
						+ " ("
						+ getStringDate((Date) fm.getValue(DATA_UMOWY_FIELD_CN))
						+ ")";
				description = tmp.getTitle() + " (" + inst.getName() + ")";
				;
			} else if (titlePart.equals(ANEKS_DO_UMOWY_ENUM_REF)) {
				title = titlePart
						+ " ("
						+ getStringDate((Date) fm
								.getValue(DATA_ANEKSU_FIELD_CN)) + ")";
				description = tmp.getTitle() + " (" + inst.getName() + ")";
				;
			} else if (tmp.getId() == 210) {
				title = fm.getValue(OPIS_UMOWY_CN)
						+ " ("
						+ FolderInserter.toYearMonthDay(fm
								.getValue(DATA_UMOWY_FIELD_CN)) + ")";
				description = fm.getValue(OPIS_UMOWY_CN) + " ("
						+ inst.getName() + ")";
				;
			} else {
				title = titlePart
						+ " ("
						+ getStringDate((Date) fm
								.getValue(DATA_POCZATKOWA_FIELD_CN))
						+ "-"
						+ getStringDate((Date) fm
								.getValue(DATA_KONCOWA_FIELD_CN)) + ")";
				description = tmp.getTitle() + " (" + inst.getName() + ")";
				;
			}
			folder = folder.createSubfolderIfNotPresent(inst.getName());
			// wedlug mnie trzeba to zrobic tak
			// title = title + " ("+inst.getName()+")";
			// description = title;
			// title = titlePart.to;

			// description = tmp.getTitle()+" ("+data + ")";
			// description = titlePart+" ("+inst.getName()+")";
		} else if (kategoriaItem.getCn().equals(UMOWY_TP_CN)) {
			folder = folder.createSubfolderIfNotPresent("Umowy z TPSA");

			folder = folder.createSubfolderIfNotPresent(miasto);
			title = fm.getValue(TYP_FIELD_CN) + " (" + data + ")";
			description = "Umowa z TPSA (" + miasto + ")";

		} else if (kategoriaItem.getCn().equals(UMOWY_NAJMU_CN)) {
			folder = folder.createSubfolderIfNotPresent("Umowy najmu");
			folder = folder.createSubfolderIfNotPresent(miasto);
			String typeTitle = (String) fm.getValue(TYP_FIELD_CN);
			title = typeTitle + " (" + data + ")";
			description = typeTitle + " (" + miasto + ")";
		} else if (kategoriaItem.getCn().equals(PROTOKO�Y_ZARZADU_CN)) {
			folder = folder.createSubfolderIfNotPresent("Protoko�y Zarz�du");
			folder = folder.createSubfolderIfNotPresent(String.valueOf(year));
			// folder = folder.createSubfolderIfNotPresent(MONTHS[month-1]);
			folder = folder.createSubfolderIfNotPresent(FolderInserter
					.toMonth(cal.getTime()));
			title = "Protoko�y Zarz�du ("
					+ getDescriptionWithTransactionNumber(fm).toString() + ")";
			description = title;
		} else if (kategoriaItem.getCn().equals(PROTOKO�Y_WALNEGO_CN)) {
			title = "Protoko�y Walnego Zgromadzenia " + data;
			folder = folder
					.createSubfolderIfNotPresent("Protoko�y Walnego Zgromadzenia");
			description = title;

		} else if (kategoriaItem.getCn().equals(PROTOKO�Y_RADY_CN)) {
			title = "Protoko�y Rady Nadzorczej (" + data + ")";
			description = title;
			folder = folder
					.createSubfolderIfNotPresent("Protoko�y Rady Nadzorczej");
		} else if (kategoriaItem.getCn().equals(KORESPONDENCJA_CN)) {
			DpInst inst = (DpInst) fm.getValue(INST_FIELD_CN);
			EnumItem tmp = fm.getEnumItem("TYP");

			title = tmp.getTitle() + " (" + data + ")";
			description = tmp.getTitle() + " (" + inst.getName() + ")";
			folder = folder
					.createSubfolderIfNotPresent("Korespondencja z urz�dami");
			folder = folder.createSubfolderIfNotPresent(inst.getName());
			folder = folder.createSubfolderIfNotPresent(String.valueOf(year));
			// folder = folder.createSubfolderIfNotPresent(MONTHS[month-1]);
			folder = folder.createSubfolderIfNotPresent(FolderInserter
					.toMonth(cal.getTime()));
		} else if (kategoriaItem.getCn().equals(SRAWY_SADOWE_CN)) {
			DpStrona strona = (DpStrona) fm.getValue(STRONA_FIELD_CN);
			EnumItem tmp = fm.getEnumItem("TYP");
			title = tmp.getTitle() + " (" + data + ")";
			description = tmp.getTitle() + " (" + strona.getImie() + " "
					+ strona.getNazwisko() + ")";
			folder = folder.createSubfolderIfNotPresent("Sprawy s�dowe");
			StringBuffer sb1 = new StringBuffer();
			sb1.append(strona.getNazwisko());
			sb1.append(" ");
			sb1.append(strona.getImie());
			sb1.append(" (");
			sb1.append(strona.getSygnatura());
			sb1.append(")");
			folder = folder.createSubfolderIfNotPresent(sb1.toString());
		} else if (kategoriaItem.getCn().equals(CERTYFIKATY_CN)) {
			title = "Certyfikat IT (" + data + ")";
			description = "Certyfikat IT ("
					+ fm.getDescription(PRODUKT_FIELD_CN) + ")";
			folder = folder.createSubfolderIfNotPresent("Certyfikaty");
			folder = folder.createSubfolderIfNotPresent(fm.getDescription(
					PRODUKT_FIELD_CN).toString());
		} else if (kategoriaItem.getCn().equals(PELNOMOCNICTWA_CN)) {
			String name;
			DpInst inst = fm.getKey(INST_FIELD_CN) != null ? (DpInst) fm
					.getValue(INST_FIELD_CN) : null;
			String imie = null;
			String nazwisko = null;
			if (fm.getValue(IMIE_FIELD_CN) != null)
				imie = fm.getValue(IMIE_FIELD_CN).toString();
			if (fm.getValue(NAZWISKO_FIELD_CN) != null)
				nazwisko = fm.getValue(NAZWISKO_FIELD_CN).toString();

			if (inst == null) {
				if (imie == null || nazwisko == null)
					throw new EdmException(
							"Podaj instytucje/kontrahenta lub nazwisko i imie. Jedno z dw�ch musi by� wype�nione");
				name = nazwisko + " " + imie;
			} else {
				if (imie != null || nazwisko != null)
					throw new EdmException(
							"Podaj instytucje/kontrahenta lub nazwisko i imie. Nie oba naraz.");
				name = inst.getName();
			}

			String tmp = name.toLowerCase();
			folder = folder.createSubfolderIfNotPresent("Pe�nomocnictwa");
			Character c = tmp.charAt(0);
			if (c.compareTo(new Character('c')) < 0)
				folder = folder.createSubfolderIfNotPresent("A-B");
			else if (c.compareTo(new Character('e')) < 0 || c.equals('�'))
				folder = folder.createSubfolderIfNotPresent("C-D");
			else if (c.compareTo(new Character('g')) < 0)
				folder = folder.createSubfolderIfNotPresent("E-F");
			else if (c.compareTo(new Character('i')) < 0)
				folder = folder.createSubfolderIfNotPresent("G-H");
			else if (c.compareTo(new Character('k')) < 0)
				folder = folder.createSubfolderIfNotPresent("I-J");
			else if (c.compareTo(new Character('m')) < 0 || c.equals('�'))
				folder = folder.createSubfolderIfNotPresent("K-�");
			else if (c.compareTo(new Character('o')) < 0)
				folder = folder.createSubfolderIfNotPresent("M-N");
			else if (c.compareTo(new Character('r')) < 0)
				folder = folder.createSubfolderIfNotPresent("O-P");
			else if (c.compareTo(new Character('t')) < 0 || c.equals('�'))
				folder = folder.createSubfolderIfNotPresent("R-S");
			else if (c.compareTo(new Character('w')) < 0)
				folder = folder.createSubfolderIfNotPresent("T-V");
			else
				folder = folder.createSubfolderIfNotPresent("W-�");

			title = name + ", " + fm.getValue(TYP_FIELD_CN) + " (" + data + ")";
			description = "Pe�nomocnictwo (" + name + ")";
		} else if (kategoriaItem.getCn().equals(SAD_REJESTROWY_CN)) {
			folder = folder.createSubfolderIfNotPresent(fm.getField(
					KLASA_FIELD_CN).getEnumItemByCn(SAD_REJESTROWY_CN)
					.getTitle());
			if (document.getTitle() != null
					&& document.getTitle().length() > 0
					&& !document.getTitle().equalsIgnoreCase(
							document.getDocumentKind().getName())) {
				title = document.getTitle();
			} else {
				title = fm.getField(TYP_FIELD_CN).getEnumItemByCn(KRS_CN)
						.getTitle();
			}

			if (document.getDescription() != null
					&& document.getDescription().length() > 0
					&& !document.getDescription().equalsIgnoreCase(
							document.getDocumentKind().getName())) {
				description = document.getDescription();
			} else {
				description = title;
			}
		}
		if (document instanceof OfficeDocument)
			((OfficeDocument) document).setSummary(description);
		// ((OfficeDocument)document).setSummaryOnly(description);

		document.setFolder(folder);
		document.setTitle(title);
		document.setDescription(description);

		if (document instanceof OfficeDocument) {
			// wpis do historii o archiwizacji - je�li obecny folder inny od
			// poprzedniego
			if (!document.getFolderPath().equals(previousFolderPath))
				((OfficeDocument) document).addWorkHistoryEntry(Audit.create(
						"dp", DSApi.context().getPrincipalName(), sm.getString(
								"DokumentZarchiwizowanoWfolderze", document
										.getFolderPath()), null, null));
			if (!document.getTitle().equals(previousTitle))
				((OfficeDocument) document).addWorkHistoryEntry(Audit.create(
						"df", DSApi.context().getPrincipalName(), sm.getString(
								"ZmianaOpisuPismaNa", document.getTitle()),
						null, null));

		}
                //Klasa UMOWY
                if(!fm.getKey(KLASA_FIELD_CN).equals(10))
                    return;
		// Pobranie wartosci "Dolaczono oryginalny za��cznik"

		boolean zalaczono = fm.getValue("ORGINAL_ZALACZNIK").toString().equals(
				"tak") ? true : false;

		if (!zalaczono) {
			// Sprawdzenie, czy nie ma juz zarejestrowanych zdarzen
			int documentEvents = EventFactory.getDocumentEventsCount(document
					.getId());

			boolean hasEvents = documentEvents > 0 ? true : false;

			// zarejestrowanych zdarzen

			if (!hasEvents) {

				// Rejestracja zdarzenia przypominajacego o dolaczeniu orginalu
				// dokumentu

				List<String> users = new ArrayList<String>();
								
				// Pobranie wlasciciela biznesowego
				Long buisnessOwnerId = Long.parseLong(((Integer)fm.getKey("WLASCICIEL_BIZNESOWY")).toString());
				
				UserImpl business_owner = UserImpl.find(buisnessOwnerId);
				
				String businessOwner = business_owner.getName();
				users.add(businessOwner);

				// Pobranie daty deadline
				Date deadline = (Date) fm.getValue("DEADLINE");

				// Pobranie uzytkownikow w roli administratora
				PreparedStatement ps = null;
				try {
					ps = DSApi
							.context()
							.prepareStatement(
									"SELECT user_id FROM ds_user_to_archive_role WHERE role=?");
					ps.setString(1, "admin");
					ResultSet rs = ps.executeQuery();

					while (rs.next()) {
						Long userId = rs.getLong(1);

						users.add(UserImpl.find(userId).getName());
					}
					rs.close();

				} catch (Exception e) {
					// e.printStackTrace();
					log.debug(e.getMessage(), e);

				} finally {
					DSApi.context().closeStatement(ps);
				}

				// Dla kazdego uzytkownika rejestrujemy zdarzenie
				for (String userName : users) {
					// Rejestrowanie zdarzen

					EventFactory.registerEvent("immediateTrigger",
							"TaskEventNotifier", userName + "|"
									+ deadline.toString() + "|"
									+ document.getId(), null, deadline,
							document.getId());
				}
			}
		}
		else
		{
			// Do dokumentu jest dolaczony oryginalny zalacznik
			// Jezeli istnieja - usuwamy wszystkie zdarzenia dla dokumentu
			
			// Sprawdzenie, czy nie ma juz zarejestrowanych zdarzen
			int documentEvents = EventFactory.getDocumentEventsCount(document
					.getId());

			boolean hasEvents = documentEvents > 0 ? true : false;
			
			if (hasEvents)
			{
				// Zamykamy wszystkie zadania
				EventFactory.closeEventsByDocumentId(document.getId());
			}

			
		}

	}

	private StringBuffer getDescriptionWithTransactionNumber(FieldsManager fm)
			throws EdmException {
		/*
		 * StringBuffer sb = new StringBuffer(); for(int i=1;i<6;i++) { Object o
		 * = fm.getDescription("NumTrans"+i); if(o!=null) {
		 * sb.append(o.toString()); sb.append(", "); } }
		 */
		StringBuffer sb = new StringBuffer();
		String nrTransakcji = (String) fm.getDescription(NUMERY_PORZADKOWE_CN);
		if (nrTransakcji != null)
			sb.append(nrTransakcji);
		return sb;
	}

	public void documentPermissions(Document document) throws EdmException {
		FieldsManager fm = document.getDocumentKind().getFieldsManager(
				document.getId());
		EnumItem kategoriaItem = fm.getEnumItem(KLASA_FIELD_CN);

		DSApi.context().clearPermissions(document);

		if (kategoriaItem.getCn().equals(UMOWY_CN)) {
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.READ,
							ObjectPermission.READ_ATTACHMENTS },
					"DP_READ_UMOWY", ObjectPermission.GROUP);
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.MODIFY,
							ObjectPermission.MODIFY_ATTACHMENTS },
					"DP_ADMIN_UMOWY", ObjectPermission.GROUP);
		} else if (kategoriaItem.getCn().equals(UMOWY_TP_CN)) {
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.READ,
							ObjectPermission.READ_ATTACHMENTS },
					"DP_READ_UMOWY_Z_TPSA", ObjectPermission.GROUP);
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.MODIFY,
							ObjectPermission.MODIFY_ATTACHMENTS },
					"DP_ADMIN_UMOWY_Z_TPSA", ObjectPermission.GROUP);
		} else if (kategoriaItem.getCn().equals(UMOWY_NAJMU_CN)) {
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.READ,
							ObjectPermission.READ_ATTACHMENTS },
					"DP_READ_UMOWY_NAJMU", ObjectPermission.GROUP);
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.MODIFY,
							ObjectPermission.MODIFY_ATTACHMENTS },
					"DP_ADMIN_UMOWY_NAJMU", ObjectPermission.GROUP);
		} else if (kategoriaItem.getCn().equals(PROTOKO�Y_ZARZADU_CN)) {
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.READ,
							ObjectPermission.READ_ATTACHMENTS },
					"DP_READ_PROTOKO�Y_ZARZADU", ObjectPermission.GROUP);
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.MODIFY,
							ObjectPermission.MODIFY_ATTACHMENTS },
					"DP_ADMIN_PROTOKO�Y_ZARZADU", ObjectPermission.GROUP);
		} else if (kategoriaItem.getCn().equals(PROTOKO�Y_WALNEGO_CN)) {
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.READ,
							ObjectPermission.READ_ATTACHMENTS },
					"DP_READ_PROTOKO�Y_WALNEGO_ZGROMADZENIA",
					ObjectPermission.GROUP);
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.MODIFY,
							ObjectPermission.MODIFY_ATTACHMENTS },
					"DP_ADMIN_PROTOKO�Y_WALNEGO_ZGROMADZENIA",
					ObjectPermission.GROUP);
		} else if (kategoriaItem.getCn().equals(PROTOKO�Y_RADY_CN)) {
			DSApi.context()
					.grant(
							document,
							new String[] { ObjectPermission.READ,
									ObjectPermission.READ_ATTACHMENTS },
							"DP_READ_PROTOKO�Y_RADY_NADZORCZEJ",
							ObjectPermission.GROUP);
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.MODIFY,
							ObjectPermission.MODIFY_ATTACHMENTS },
					"DP_ADMIN_PROTOKO�Y_RADY_NADZORCZEJ",
					ObjectPermission.GROUP);
		} else if (kategoriaItem.getCn().equals(KORESPONDENCJA_CN)) {
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.READ,
							ObjectPermission.READ_ATTACHMENTS },
					"DP_READ_KORESPONDENCJA", ObjectPermission.GROUP);
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.MODIFY,
							ObjectPermission.MODIFY_ATTACHMENTS },
					"DP_ADMIN_KORESPONDENCJA", ObjectPermission.GROUP);
		} else if (kategoriaItem.getCn().equals(SRAWY_SADOWE_CN)) {
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.READ,
							ObjectPermission.READ_ATTACHMENTS },
					"DP_READ_SPRAWY_SADOWE", ObjectPermission.GROUP);
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.MODIFY,
							ObjectPermission.MODIFY_ATTACHMENTS },
					"DP_ADMIN_SPRAWY_SADOWE", ObjectPermission.GROUP);
		} else if (kategoriaItem.getCn().equals(CERTYFIKATY_CN)) {
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.READ,
							ObjectPermission.READ_ATTACHMENTS },
					"DP_READ_CERTYFIKATY", ObjectPermission.GROUP);
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.MODIFY,
							ObjectPermission.MODIFY_ATTACHMENTS },
					"DP_ADMIN_CERTYFIKATY", ObjectPermission.GROUP);
		} else if (kategoriaItem.getCn().equals(PELNOMOCNICTWA_CN)) {
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.READ,
							ObjectPermission.READ_ATTACHMENTS },
					"DP_READ_PE�NOMOCNICTWA", ObjectPermission.GROUP);
			DSApi.context().grant(
					document,
					new String[] { ObjectPermission.MODIFY,
							ObjectPermission.MODIFY_ATTACHMENTS },
					"DP_ADMIN_PE�NOMOCNICTWA", ObjectPermission.GROUP);
		}

		DSApi.context().grant(
				document,
				new String[] { ObjectPermission.READ,
						ObjectPermission.READ_ATTACHMENTS }, "DP_READ",
				ObjectPermission.GROUP);
		DSApi.context().grant(
				document,
				new String[] { ObjectPermission.MODIFY,
						ObjectPermission.MODIFY_ATTACHMENTS }, "DP_ADMIN",
				ObjectPermission.GROUP);
		try {
			DSApi.context().session().flush();
		} catch (HibernateException ex) {
			throw new EdmHibernateException(ex);
		}

	}

	@Override
	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {

		Map<String, Object> values = new HashMap<String, Object>();

		Calendar calendar = GregorianCalendar.getInstance(TimeZone
				.getTimeZone("Europe/Warsaw"));
		calendar.add(GregorianCalendar.DAY_OF_MONTH, 21);

		values.put(DATA_FIELD_CN, new Date());
		values.put(DEADLINE_CN, calendar.getTime());

		if (DocumentLogic.TYPE_ARCHIVE == type)
			values.put(STATUS_FIELD_CN, STATUS_ARCHIWALNY);
		else
			values.put(STATUS_FIELD_CN, STATUS_PRZYJETY);

		fm.reloadValues(values);
	}

	@Override
	public void correctValues(Map<String, Object> values, DocumentKind kind)
			throws EdmException {
		if (values.get(DATA_FIELD_CN) == null)
			values.put(DATA_FIELD_CN, new Date());
	}

	public String getInOfficeDocumentKind() {
		return "Dokument prawny";
	}

	public boolean canReadDocumentDictionaries() throws EdmException {
		return DSApi.context().hasPermission(
				DSPermission.DP_SLOWNIK_ODCZYTYWANIE)
				|| DSApi.context().getDSUser().isAdmin();
	}

	public boolean isReadPermissionCode(String permCode) {
		return permCode.startsWith("DP_READ")
				|| permCode.startsWith("DP_ADMIN");
	}

}
