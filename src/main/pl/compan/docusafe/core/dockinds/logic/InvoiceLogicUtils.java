package pl.compan.docusafe.core.dockinds.logic;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.parametrization.ins.CostInvoiceLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class InvoiceLogicUtils
{
	private static Logger log = LoggerFactory.getLogger(InvoiceLogicUtils.class);
	
	public static final String DWR_KWOTA_NETTO = "DWR_KWOTA_NETTO";
	public static final String DWR_KWOTA_BRUTTO = "DWR_KWOTA_BRUTTO";
	public static final String DWR_VAT = "DWR_VAT";
	public static final String DWR_KWOTA_W_WALUCIE = "DWR_KWOTA_W_WALUCIE";
	public static final String DWR_KURS = "DWR_KURS";
	public static final String DWR_KW_POZ_BRUTTO = "DWR_KW_POZ_BRUTTO";
	public static final String DWR_KW_POZ_NETTO = "DWR_KW_POZ_NETTO";
	public static final String DWR_KW_POZ_VAT = "DWR_KW_POZ_VAT";

	public static void setBruttoAsNettoPlusVat(Map<String, FieldData> values)
	{
		try
		{
			BigDecimal netto = values.get(DWR_KWOTA_W_WALUCIE).getMoneyData();
			if (values.get(DWR_VAT).getData() != null)
			{
				BigDecimal brutto = values.get(DWR_VAT).getMoneyData();
				brutto = brutto.add(netto).setScale(2, RoundingMode.HALF_UP);
				values.get(DWR_KWOTA_BRUTTO).setMoneyData(brutto.setScale(2, RoundingMode.HALF_UP));
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public static void setNetto(Map<String, FieldData> values)
	{
		try
		{
			BigDecimal kwotaWWalucie = values.get(DWR_KWOTA_W_WALUCIE).getMoneyData();
			BigDecimal kurs = (BigDecimal)values.get(DWR_KURS).getData();
			BigDecimal netto = kwotaWWalucie.multiply(kurs).setScale(2, RoundingMode.HALF_UP);
			//values.get(DWR_KWOTA_NETTO).setMoneyData(netto.setScale(2, RoundingMode.HALF_UP));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

    public static void setTerminPlatnosci(Map<String, FieldData> values) {
        try
        {
            Date dataWystawienia = values.get("DWR_DATA_WYSTAWIENIA").getDateData();
            Calendar c = Calendar.getInstance();
            EnumValues sposobPlatnosci = values.get("DWR_SPOSOB").getEnumValuesData();
            int liczDni = getLiczDniFromSposobId(sposobPlatnosci.getSelectedId());
            c.setTime(dataWystawienia);
            c.add(Calendar.DATE, liczDni);

            values.get("DWR_TERMIN_PLATNOSCI").setDateData(c.getTime());
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }

    }

    private static int getLiczDniFromSposobId(String selectedId) throws EdmException, SQLException {
        if (selectedId.equals("") || selectedId == null)
            throw new EdmException("Nie podano ID sposobu p�atnosci");

        boolean contextOpened = false;
        PreparedStatement ps = null;
        if (!DSApi.isContextOpen())
            contextOpened = DSApi.openContextIfNeeded();
        if (!DSApi.context().inTransaction())
            DSApi.context().begin();

        try {
            String sql = "SELECT liczdni FROM simple_erp_per_docusafe_warpla_view " +
                    "  where id = ?";
            ps = DSApi.context().prepareStatement(sql);
            ps.setObject(1, selectedId);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                Long liczDni = rs.getLong("liczdni");
                ps.close();
                return liczDni.intValue();
            }

        } catch (Exception e){
            log.error("B�ad podczas wyciagania liczby dni dla sposobu p�atnosci. "+e);
        } finally {
            ps.close();
            DSApi.closeContextIfNeeded(contextOpened);
        }

        return 0;
    }

    public static void setKwotaPozostala(Map<String, FieldData> values) {
        try
        {
            BigDecimal brutto = values.get(DWR_KWOTA_BRUTTO).getMoneyData();
            BigDecimal netto = values.get(DWR_KWOTA_W_WALUCIE).getMoneyData();
            BigDecimal vat = values.get(DWR_VAT).getMoneyData();

            BigDecimal sumaBrutto = BigDecimal.ZERO;
            BigDecimal sumaVat = BigDecimal.ZERO;
            BigDecimal sumaNetto = BigDecimal.ZERO;

            BigDecimal kwPozBrutto;
            BigDecimal kwPozNetto;
            BigDecimal kwPozVat;
            Map stawkiVat = values.get("DWR_MPK").getDictionaryData();
            int i = 1;
            while(i != -1){

                BigDecimal pozNetto = getNullSafeDWRMoney(stawkiVat.get("MPK_NETTO_"+ i));
                Integer pozStawkaVat = getNullSafeDWREnumItemId(stawkiVat.get("MPK_STAWKA_" + i));
                BigDecimal procVat = null;
                if (pozStawkaVat != null) {
                    EnumItem enumItem = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.ERP_STAWKI_VAT_TABLENAME, pozStawkaVat);
                    try {
                        procVat = new BigDecimal(enumItem.getCn());
                    }catch (Exception e){
                        log.error("Error podczas konwersji procentu VAT: "+e);
                    }
                }
                BigDecimal pozKwotaVat = null;
                BigDecimal pozBrutto = null;

                if (pozNetto != null && procVat != null){
                    pozKwotaVat = pozNetto.multiply(procVat.divide(new BigDecimal("100")));
                    pozBrutto = pozNetto.add(pozKwotaVat);

                    sumaNetto = sumaNetto.add(pozNetto);
                    sumaVat = sumaVat.add(pozKwotaVat);
//                    values.get("DWR_STAWKI_VAT").getDictionaryData().get("STAWKI_VAT_KWOTA_VAT_"+i).setMoneyData(pozKwotaVat.setScale(2, RoundingMode.HALF_UP));
//                    values.get("DWR_STAWKI_VAT").getDictionaryData().get("STAWKI_VAT_BRUTTO_"+i).setMoneyData(pozBrutto.setScale(2, RoundingMode.HALF_UP));
                }

                if(pozBrutto != null){
                    sumaBrutto = sumaBrutto.add(pozBrutto);
                    i++;
                }else{
                    i=-1;
                }
            }
            kwPozBrutto = brutto.subtract(sumaBrutto);
            kwPozNetto = netto.subtract(sumaNetto);
            kwPozVat = vat.subtract(sumaVat);
            values.get(DWR_KW_POZ_BRUTTO).setMoneyData(kwPozBrutto.setScale(2, RoundingMode.HALF_UP));
            values.get(DWR_KW_POZ_NETTO).setMoneyData(kwPozNetto.setScale(2, RoundingMode.HALF_UP));
            values.get(DWR_KW_POZ_VAT).setMoneyData(kwPozVat.setScale(2, RoundingMode.HALF_UP));
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }

    }

    private static BigDecimal getNullSafeDWRMoney(Object el) {
        try{
            FieldData fieldData = (FieldData) el;
            return fieldData.getMoneyData();
        } catch (Exception e){
            log.error("B��d podczas pobierania: "+e);
        }
        return null;
    }

    private static Integer getNullSafeDWREnumItemId(Object el) {
        try{
            FieldData fieldData = (FieldData) el;
            return Integer.valueOf(fieldData.getEnumValuesData().getSelectedId());
        } catch (Exception e){
            log.error("B��d podczas pobierania: "+e);
        }
        return null;
    }
}
