package pl.compan.docusafe.core.dockinds.logic;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;

import java.text.Format;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class MarkerFieldsPrintProvider<E extends Enum<E>> extends FieldsPrintProvider {

    private Enum<E> [] enumMarkers;

    public MarkerFieldsPrintProvider(Enum<E> [] markers) {
        enumMarkers = markers;
    }

    @Override
    public void setAdditionalTemplateValuesUnsafe(Map<String, Object> values, Map<Class, Format> defaultFormatters, FieldsManager fm, OfficeDocument doc) throws EdmException {
        super.setAdditionalTemplateValuesUnsafe(values, defaultFormatters, fm, doc);

        for (Enum<E> marker : enumMarkers){
            String value = getMarkerValue (marker, fm, doc);
            values.put(marker.name(),value);
        }
    }

    public abstract String getMarkerValue(Enum<E> marker, FieldsManager fm, OfficeDocument doc);
}
