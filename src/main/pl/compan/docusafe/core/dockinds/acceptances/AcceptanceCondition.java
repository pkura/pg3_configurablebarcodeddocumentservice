package pl.compan.docusafe.core.dockinds.acceptances;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Maps;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Warunek akceptacji, czyli definicja tego kto mo�e dan� akceptacj� wykona�. Jeden warunek mo�e
 * dotyczy� wszystkich u�ytkownik�w (gdy nie podamy ani u�ytkownika ani dzia�u), jednego u�ytkownika
 * lub jednego dzia�u. 
 * UWAGA: nie mo�na w jednym warunku okre�li� zar�wno u�ytkownika jak i dzia�u. 
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class AcceptanceCondition {
    public final static Logger log = LoggerFactory.getLogger(AcceptanceCondition.class);
    /**
     * Cache centr�w kosztowych dla u�ytkownik�w (user.name -> lista centr�w mo�liwych do nadania (IC))
     */
    private static LoadingCache<String, Map<Integer, String>> costCenterSelectCache = CacheBuilder.newBuilder()
            .concurrencyLevel(10)
            .maximumSize(100)
            .expireAfterAccess(10, TimeUnit.MINUTES)
            .build(new CacheLoader<String, Map<Integer, String>>() {
                @Override
                public Map<Integer, String> load(String username) throws Exception {
                    List<Integer> centraList = AcceptanceCondition.findByUserGuids(username);
                    Map<Integer, String> centra = Maps.newLinkedHashMap();
                    CentrumKosztow ck;
                    for (Integer ckId : centraList) {
                        ck = CentrumKosztow.find(ckId);
                        if (ck.isAvailable()) {
                            centra.put(ckId, ck.getSymbol() + " (" + ck.getName() + ")");
                        }
                    }
                    return centra;
                }
            });

    private final static class AcceptanceCheck {
        final String username;
        final String acceptanceCn;
        final Integer centrumId;

        private AcceptanceCheck(String acceptanceCn, Integer centrumId, String username) {
            this.acceptanceCn = acceptanceCn;
            this.centrumId = centrumId;
            this.username = username;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AcceptanceCheck that = (AcceptanceCheck) o;

            if (acceptanceCn != null ? !acceptanceCn.equals(that.acceptanceCn) : that.acceptanceCn != null)
                return false;
            if (centrumId != null ? !centrumId.equals(that.centrumId) : that.centrumId != null) return false;
            if (username != null ? !username.equals(that.username) : that.username != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = username != null ? username.hashCode() : 0;
            result = 31 * result + (acceptanceCn != null ? acceptanceCn.hashCode() : 0);
            result = 31 * result + (centrumId != null ? centrumId.hashCode() : 0);
            return result;
        }
    }

    /**
     * Cache trzymaj�cy informacje o mo�liwo�ciach akceptacji dotycz�cych pojedynczych u�ytkownik�w
     */
    private static LoadingCache<AcceptanceCheck, Boolean> acceptanceCheckCache = CacheBuilder.newBuilder()
            .concurrencyLevel(10)
            .maximumSize(500)
            .expireAfterAccess(5, TimeUnit.MINUTES)
            .build(new CacheLoader<AcceptanceCheck, Boolean>() {
                @Override
                public Boolean load(AcceptanceCheck acceptanceCheck) throws Exception {
                    return _checkAcceptancePermission(
                            DSUser.findByUsername(acceptanceCheck.username),                            
                            acceptanceCheck.centrumId,
                            acceptanceCheck.acceptanceCn);
                }
            });

    private Long id;
    /** nazwa kodowa akceptacji */
    private String cn;
    /** 
     * warto�c steruj�ca - gdy r�wna null oznacza, �e niezale�nie od warto�ci pola zwi�zanego z 
     * akceptacj� akceptowa� mo�e podany u�ytkownik/dzia� (to, kt�re pole jest zwi�zane z akceptacj�
     * 'cn' podaje si� w pliku XML definiuj�cym proces akceptacji)
     */
    private String fieldValue;    
    /** u�ytkownik, kt�ry ma prawo akceptowa� */
    private String username;    
    /** dzia�, kt�rego u�ytkownicy maj� prawo akceptowa� */
    private String divisionGuid;
    /** maksymalna kwota dla akceptacji */
    private Double maxAmountForAcceptance;

    public static AcceptanceCondition find(Long id) throws EdmException
    {
        return Finder.find(AcceptanceCondition.class, id);
    }
    
    public static List<AcceptanceCondition> list() throws EdmException
    {
        return Finder.list(AcceptanceCondition.class, "id");
    }
    
    public static List<AcceptanceCondition> listBy(String by) throws EdmException
    {
        return Finder.list(AcceptanceCondition.class, by);
    }
    
    public static List<Map<String, Object>> getConditionsBeans(String by)
    	throws EdmException
    {
    	if (DSApi.isSqlServer())
    		return buildConditionsBeansFromSqlServer(by);
    	
    	return buildConditionsBeans(by);
    }

    /**
     * Zwraca map� ID -> Opis centrum do selekta dla u�ytkownika wybieraj�cego centr�w (dla IC)
     * @param username
     * @return
     */
    public static Map<Integer, String> getCentraSelectForUser(String username) throws EdmException {
        try {
//            log.info("cache stat : {}", costCenterSelectCache.stats());
            log.info("getCentraSelectForUser {}",  costCenterSelectCache.get(username));
            return costCenterSelectCache.get(username);
        } catch (ExecutionException e) {
            throw new EdmException(e.getCause());
        }
    }

    public static void clearCaches() {
        costCenterSelectCache.invalidateAll();
        acceptanceCheckCache.invalidateAll();
    }

    /**
     * Sprawdza uprawnienia do akceptacji dla danego u�ytkownika, cache'uje odpowiedzi _checkAcceptancePermission
     * @param user u�ytkownik akceptuj�cy
     * @param acceptingCentrumId akceptowane centrum (mo�e by� null dla akceptacji og�lnych)
     * @param cn kod akceptacji
     * @throws EdmException
     */
    public static boolean checkAcceptancePermission(DSUser user, Integer acceptingCentrumId, String cn) throws EdmException{
        log.info("acceptanceCheckCache stats = {}", acceptanceCheckCache.stats());
        try {
            return acceptanceCheckCache.get(new AcceptanceCheck(cn, acceptingCentrumId, user.getName()));
        } catch (ExecutionException e) {
            log.error(e.getMessage() ,e);
            return false;
        }
    }

    private static boolean _checkAcceptancePermission(DSUser user, Integer acceptingCentrumId, String cn) throws EdmException{
        if(!IntercarsAcceptanceMode.getAllAcceptanceCodes().contains(cn)){
            throw new IllegalArgumentException(MessageFormat.format("kod akceptacji {0} nie istnieje", cn));
        }

        Criteria crit = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        crit.add(Restrictions.eq("cn", cn));
        if(acceptingCentrumId != null){
            crit.add(Restrictions.eq("fieldValue", acceptingCentrumId.toString()));
        } else {
            crit.add(Restrictions.isNull("fieldValue"));
        }

        List<AcceptanceCondition> acs = crit.list();
        crit = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        crit.add(Restrictions.eq("cn", "all_" + cn));
        acs.addAll(crit.list());
        if(acs.isEmpty()){
            throw new EdmException("brak zdefiniowanych zasad akceptacji dla kodu " + cn
                    + (acceptingCentrumId==null?"":" dla centrum ID:" + acceptingCentrumId));
        }
        for(AcceptanceCondition ac: acs){
            if(user.getName().equals(ac.getUsername())){
                return true;
            } else if(ac.getDivisionGuid()!=null && user.inDivisionByGuid(ac.getDivisionGuid())){
                return true;
            }
        }
        for(DSUser substituted: user.getSubstituted()){
            try{
                if(_checkAcceptancePermission(substituted, acceptingCentrumId, cn)) return true;
                //brak wyj�tku -> akceptuj za zast�pstwo

            } catch(EdmException e) { //wyj�tek -> ignoruj ten u�ytkownik nie mo�e akceptowa�
            } catch(Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return false;
        //throw new EdmException("brak uprawnie� do dokonania akceptacji o kodzie " + cn
        //		+ (acceptingCentrumId==null?"":" dla centrum " + CentrumKosztow.find(acceptingCentrumId).getSymbol()));
    }
    
    private static List<Map<String, Object>> buildConditionsBeans(String by)
    	throws EdmException
    {
    	Set<String> acceptancesToHide = getAcceptancesToHide();
    	List<AcceptanceCondition> list = listBy(by != null ? by : "id");
    	List<Map<String, Object>> conditions = new LinkedList<Map<String,Object>>();
        for (AcceptanceCondition condition : list)
        {
        	if(acceptancesToHide.contains(condition.getCn()))
        		continue;

        	Map<String,Object> bean = new LinkedHashMap<String, Object>();
            bean.put("cn", condition.getCn());
            bean.put("value", condition.getFieldValue());
            
            if (condition.getFieldValue() != null && StringUtils.isNumeric(condition.getFieldValue()))
            	try
				{
            		bean.put("mpk", CentrumKosztow.find(Integer.parseInt(condition.getFieldValue())).getSearchTitle());
				} catch (EdmException e)
				{
					log.warn(e.getMessage());
				}
            else
            	bean.put("mpk", "-");
            
            if(condition.getUsername() != null && condition.getUsername().startsWith("ext:"))
            {
            	bean.put("user", condition.getUsername().replace("ext", "uzytkownik zewnetrzny"));
            }
            else
            {
            	bean.put("user", DSUser.safeToFirstnameLastname(condition.getUsername()));
            }
            
            bean.put("division", DSDivision.safeGetName(condition.getDivisionGuid()));
            bean.put("maxAmountForAcceptance", condition.getMaxAmountForAcceptance());
            bean.put("deleteLink", "/admin/acceptances.action?doDelete=true&id="+condition.getId());
            conditions.add(bean);
        }
        
        return conditions;
    }
    
    private static List<Map<String, Object>> buildConditionsBeansFromSqlServer(String by)
		throws EdmException
    {
    	Set<String> acceptancesToHide = getAcceptancesToHide();
    	by = by != null ? by : "id";
    	if (by.equals("maxAmountForAcceptance"))
    		by = "ac.maximumAmountForAcceptance";
    	else if (by.equals("symbol"))
    		by = "ck." + by;
    	else
    		by = "ac." + by;
    	
    	@SuppressWarnings("unchecked")
    	List<Object[]> results = DSApi.context().session().createSQLQuery(
    			"select ac.id, ac.cn, ac.fieldValue, ac.username, ac.divisionGuid, ac.maximumAmountForAcceptance, ck.symbol, ck.name " +
    			"from ds_acceptance_condition ac left join DSG_CENTRUM_KOSZTOW ck on " +
    			"ac.fieldValue = CONVERT(varchar, ck.id) order by " + by)
    			.list();
    	
    	List<Map<String, Object>> conditions = new LinkedList<Map<String,Object>>();
    	for (Object[] row : results)
    	{
    		if(acceptancesToHide.contains(row[1]))
        		continue;

        	Map<String,Object> bean = new LinkedHashMap<String, Object>();
            bean.put("cn", row[1]);
            bean.put("value", row[2]);
            
            if (row[6] != null)
            		bean.put("mpk", row[6] + " - " + row[7]);
            else
            	bean.put("mpk", "-");
            
            if(row[3] != null && row[3].toString().startsWith("ext:"))
            {
            	bean.put("user", row[3].toString().replace("ext", "uzytkownik zewnetrzny"));
            }
            else if (row[3] != null)
            {
            	bean.put("user", DSUser.safeToFirstnameLastname(row[3].toString()));
            }
            else
            	bean.put("user", "-");
            
            if (row[4] != null)
            	bean.put("division", DSDivision.safeGetName(row[4].toString()));
            else
            	bean.put("division", "-");
            
            bean.put("maxAmountForAcceptance", row[5]);
            bean.put("deleteLink", "/admin/acceptances.action?doDelete=true&id="+row[0]);
            conditions.add(bean);
    	}
    	
    	return conditions;
    }
    
    public static Set<String> getAcceptancesToHide() throws EdmException
    {
    	Set<String> acceptancesToHide = new HashSet<String>();
        acceptancesToHide.add("pracownik");
        
        for(DocumentKind kind : DocumentKind.list())
        {
        	if(AvailabilityManager.isAvailable("hideAcceptancesInPersons", kind.getCn()) && kind.getAcceptancesDefinition() != null)
        	{
        		acceptancesToHide.addAll(kind.getAcceptancesDefinition().getAcceptances().keySet());                		
        	}
        }
        
        return acceptancesToHide;
    }
    
    public static List<AcceptanceCondition> find(String cn) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
            c.add(Expression.eq("cn", cn));
            c. addOrder(Order.asc("id"));
            
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static List<AcceptanceCondition> find(String cn,String fieldValue) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
            c.add(Expression.eq("cn", cn));
            if(fieldValue != null)
            {
            	c.add(Expression.eq("fieldValue", fieldValue));
            }
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * znajduje po cn i username, dostajemy podwladnych usera
     */
    public static List<AcceptanceCondition> findByUserName(String cn, String userName) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
            c.add(Expression.eq("cn", cn));
            if(userName != null)
            {
            	c.add(Expression.eq("username", userName));
            }
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static AcceptanceCondition find(String cn,String fieldValue, String username) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
            c.add(Expression.eq("cn", cn));
            if(fieldValue != null)
            	c.add(Expression.eq("fieldValue", fieldValue));
            if(username != null)
            	c.add(Expression.eq("username", username));
            List<AcceptanceCondition> tmpList = c.list();
            
            if(tmpList.size() > 0)
                return tmpList.get(0);
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static List<AcceptanceCondition> findWithNullField(String cn) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
            c.add(Expression.eq("cn", cn));
           	c.add(Expression.isNull("fieldValue"));
            
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static List<AcceptanceCondition> findByFieldValue(String fieldValue) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
            c.add(Expression.eq("fieldValue", fieldValue));
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static List<AcceptanceCondition> findByGuid(String guid){
    	try {
			Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
			c.add(Expression.eq("divisionGuid", guid));
			return c.list();
		} catch (HibernateException e) {
			log.error("AcceptanceCondition.java:",e);
		} catch (EdmException e) {
			log.error("AcceptanceCondition.java:"+e,e);
		}
		return null;
    }
    

	/**Zwraca liste centr�w jakie mo�e akceptowa� ten (curennt) u�ytkownik
     * @throws EdmException
     * @throws SQLException
     * @throws HibernateException */
	public static List<Integer> findByUserGuids() throws HibernateException, SQLException, SQLException, EdmException{
		return findByUserGuids(DSApi.context().getPrincipalName());
	}

    /**Zwraca liste centr�w jakie mo�e akceptowa� uzytkownik
     * @throws EdmException 
     * @throws SQLException 
     * @throws HibernateException */
    public static List<Integer> findByUserGuids(String username) throws HibernateException, SQLException, EdmException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Integer> fieldValues = new ArrayList<Integer>();
		try {
            ps = DSApi.context().prepareStatement("select ac.id from ds_acceptance_condition ac, ds_user_to_division ud, ds_division div, ds_user usr " +
                    "where " +
                    "  ac.cn like 'all%' " +
                    "      and ((ud.division_id = div.id and div.guid = ac.divisionGuid and ud.user_id = usr.id and usr.name = ? ) " +
                    "  or " +
                    "   (ud.division_id = div.id and usr.name = ac.username and ud.user_id = usr.id and usr.name = ? ))");
            ps.setString(1, username);
            ps.setString(2, username);
            rs = ps.executeQuery();
            if(rs.next()){ //ma uprawnienie all
                rs.close();
                DSApi.context().closeStatement(ps);
                ps = DSApi.context().prepareStatement("select id from dsg_centrum_kosztow where available = 1 order by symbol");
                rs = ps.executeQuery();
                while(rs.next()){
                    fieldValues.add(rs.getInt(1));
                }
            } else {
                ps = DSApi.context().prepareStatement(
                        "select " +
                        "distinct ds_accep_1.fieldValue " +
                        "from " +
                        "ds_division div, ds_user_to_division ud, ds_acceptance_condition ds_accep_1, ds_user usr " +
                        "where " +
                        "(ud.division_id = div.id and div.guid = ds_accep_1.divisionGuid and ud.user_id = usr.id and usr.name = ? ) " +
                        "or " +
                        "(ud.division_id = div.id and usr.name = ds_accep_1.username and ud.user_id = usr.id and usr.name = ? )");
                ps.setString(1, username);
                ps.setString(2, username);
                rs = ps.executeQuery();
                while (rs.next()) {
                    String value = rs.getString(1);
                    if(value!=null){
                        fieldValues.add(new Integer(value));
                    }
                }
            }
		} finally {
            DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
		}
		for(DSUser substitute: DSUser.findByUsername(username).getSubstituted()){
			fieldValues.addAll(findByUserGuids(substitute.getName()));
		}
		return fieldValues;
	}
    
    /**
	 * Zwraca liste centr�w jakie mo�e akceptowa� uzytkownik
	 * 
	 * @throws EdmException
	 * @throws SQLException
	 * @throws HibernateException
	 */
	public static Map<String, AcceptanceCondition> findAcceptanceConditionByUserGuids(String username) throws HibernateException, SQLException, EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<String,AcceptanceCondition> accList = new HashMap<String,AcceptanceCondition>();
		try
		{
			ps = DSApi.context().prepareStatement(
					"select " + "distinct ds_accep_1.id " + "from " + "ds_division div, ds_user_to_division ud, ds_acceptance_condition ds_accep_1, ds_user usr " + "where "
							+ "(ud.division_id = div.id and div.guid = ds_accep_1.divisionGuid and ud.user_id = usr.id and usr.name = ? ) " + "or "
							+ "(ud.division_id = div.id and usr.name = ds_accep_1.username and ud.user_id = usr.id and usr.name = ? )");
			ps.setString(1, username);
			ps.setString(2, username);
			rs = ps.executeQuery();
			while (rs.next())
			{
				Long value = rs.getLong(1);
				if (value != null)
				{
					AcceptanceCondition ac = AcceptanceCondition.find(value);
					accList.put(ac.getCn(),ac);
				}
			}
		}
		finally
		{
			if (rs != null)
			{
				rs.close();
			}
			DSApi.context().closeStatement(ps);
		}
		for (DSUser substitute : DSUser.findByUsername(username).getSubstituted())
		{
			accList.putAll(findAcceptanceConditionByUserGuids(substitute.getName()));
		}
		return accList;
	}
    
    /**Zwraca liste centr�w jakie mo�e akceptowa� zalogowany uzytkownik
     * @throws EdmException 
     * @throws SQLException 
     * @throws HibernateException */
    public static Boolean canCentrumAccept(Integer objectId,String cn) throws HibernateException, SQLException, EdmException
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	Boolean returnValue = false;
        try
        {
        	ps = DSApi.context().prepareStatement(
        			"select "+
        				"distinct ds_accep_1.fieldValue "+
        			"from "+ 
        				"ds_division div, ds_user_to_division ud, ds_acceptance_condition ds_accep_1, ds_user usr "+
        			"where "+ 
						"(ud.division_id = div.id and div.guid = ds_accep_1.divisionGuid and ds_accep_1.fieldValue = ? and ds_accep_1.cn = ? and ud.user_id = usr.id and usr.name = ? ) "+
					"or "+
						"(ud.division_id = div.id and usr.name = ds_accep_1.username and ds_accep_1.fieldValue = ? and ds_accep_1.cn = ? and ud.user_id = usr.id and usr.name = ? )");
        	ps.setInt(1, objectId);
        	ps.setString(2, cn);
        	ps.setString(3, DSApi.context().getPrincipalName());
        	ps.setInt(4, objectId);
        	ps.setString(5, cn);
        	ps.setString(6, DSApi.context().getPrincipalName());
            rs = ps.executeQuery();
            returnValue = rs.next();            
    	}
    	finally
    	{
    		if(rs != null){rs.close();}
    		DSApi.context().closeStatement(ps);
		}
    	return returnValue;
    }
    
    /**Akceptacje kt�re mo�e wykona� u�ytkownik (i tylko on)*/
    public static List<AcceptanceCondition> userAcceptances(String username) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
            c.add(Expression.eq("username", username));
            List<AcceptanceCondition> list = c.list();
            List<AcceptanceCondition> returnList = new ArrayList<AcceptanceCondition>();
            if(list != null)
            {
            	for (AcceptanceCondition acceptanceCondition : list) 
            	{
					if(!returnList.contains(acceptanceCondition))
					{
						returnList.add(acceptanceCondition);
					}
				}
            }
            return returnList;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /** Maksymalna kwota jaka moze zaakceptowac uzytkownik.
     *  (Dla null i 0 powinien akceptowa� dowoln� kwote) 
     */
    public static Double getUserMaximumAmountForAcceptance(String cn, Integer fieldValue, String userName) throws EdmException {
    	Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        c.add(Expression.eq("cn", cn));
        if (fieldValue != null)
        	c.add(Expression.eq("fieldValue", fieldValue.toString()));
        c.add(Expression.eq("username", userName));        
        if (c.list().size() > 0)
        	return ((AcceptanceCondition)c.list().get(0)).getMaxAmountForAcceptance();
        else
        {
        	for (DSDivision division : DSApi.context().getDSUser().getAllDivisions())
        	{
        		Double amount = getDivisionMaximumAmountForAcceptance(cn, fieldValue, division.getGuid());
        		if (amount != null && (amount >= 0))
        			return amount;
        	}
        }
        return null;        
    }    

    /** Maksymalna kwota jaka moze zaakceptowac uzytkownik danego dzialu.
     *  (Dla null i 0 powinien akceptowa� dowoln� kwote) 
     */
    public static Double getDivisionMaximumAmountForAcceptance(String cn, Integer fieldValue, String divisionGuid) throws EdmException
    {
    	Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        c.add(Expression.eq("cn", cn));
        if (fieldValue != null)
        	c.add(Expression.eq("fieldValue", fieldValue.toString()));
        c.add(Expression.eq("divisionGuid", divisionGuid));
        if (c.list().size() > 0)
        	return ((AcceptanceCondition) c.list().get(0)).getMaxAmountForAcceptance();
        else
        	return null;
    }
    
    /**
     * Akceptacje kt�re mo�e wykona� u�ytkownik (tak�e te, kt�re wynikaj� z bycia cz�onkiem dzia�u)     
     * @throws EdmException 
     */
    public static List<AcceptanceCondition> fullUserAcceptances(DSUser user) throws EdmException{
    	HashSet<AcceptanceCondition> ret = new HashSet<AcceptanceCondition>();
    	ret.addAll(userAcceptances(user.getName()));
    	
    	DSDivision[] divisions;
    	
    	if((divisions = user.getAllDivisions()).length>0){
    		Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
    		Criterion e = Expression.eq("divisionGuid", divisions[0].getGuid());
    		
    		for(int i=1; i<divisions.length; ++i){
    			e = Expression.or(e, Expression.eq("divisionGuid", divisions[i].getGuid()));
    		}
    		c.add(e);
    		ret.addAll(c.list());
    	}
    	
    	return new ArrayList<AcceptanceCondition>(ret);
    }
    
    public static Boolean canCentrumAccept(Long objectId,String cn,String userName) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        c.add(Expression.eq("cn", cn));
        c.add(Expression.eq("fieldValue", objectId.toString()));
        c.add(Expression.eq("username", userName));
        
        return c.list().size() > 0;
    }
    
    public static Boolean canCentrumAcceptByDivision(Long objectId,String cn,String divisionGuid) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        c.add(Expression.eq("cn", cn));
        c.add(Expression.eq("fieldValue", objectId.toString()));
        c.add(Expression.eq("divisionGuid", divisionGuid));
        return c.list().size() > 0;
    }
    
    public static Boolean canCentrumAcceptByDivision(Long objectId,String divisionGuid) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        c.add(Expression.eq("fieldValue", objectId.toString()));
        c.add(Expression.eq("divisionGuid", divisionGuid));
        return c.list().size() > 0;
    }
    
    public static Boolean canAcceptDocumentByUser(String cn,String userName) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        c.add(Expression.eq("cn", cn));
        //c.add(Expression.isNull("fieldValue"));
        c.add(Expression.eq("username", userName));
        return c.list().size() > 0;
    }
    
    public static Boolean canAcceptByUser(String cn,String userName) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        c.add(Expression.eq("cn", cn));
        c.add(Expression.eq("username", userName));
        if(c.list().size() < 1)
        {
        	DSUser user = DSUser.findByUsername(userName);
        	DSDivision[] divisions  = user.getDivisions();
        	for (int i = 0; i < divisions.length; i++) 
        	{
        		if(canAcceptByDivision(cn, divisions[i].getGuid()))
        		{
        			return true;
        		}
			}
        }
        return c.list().size() > 0;
    }
    /**Sprawdza czy u�ytkownik mo�e dokona� akceptacji na danym MPK-u*/
    public static Boolean canCentrumAcceptByUser(Long objectId,String userName) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        c.add(Expression.eq("fieldValue", objectId.toString()));
        c.add(Expression.eq("username", userName));
        if(c.list().size() < 1)
        {
        	DSUser user = DSUser.findByUsername(userName);
        	DSDivision[] divisions  = user.getDivisions();
        	for (int i = 0; i < divisions.length; i++) 
        	{
        		if(canCentrumAcceptByDivision(objectId,divisions[i].getGuid()))
        		{
        			return true;
        		}
			}
        }
        return c.list().size() > 0;
    }
    
    
    public static Boolean canAcceptDocumentByDivision(String cn,String divisionGuid) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        c.add(Expression.eq("cn", cn));
        c.add(Expression.isNull("fieldValue"));
        c.add(Expression.eq("divisionGuid", divisionGuid));
        return c.list().size() > 0;
    }
    
    public static Boolean canAcceptByDivision(String cn,String divisionGuid) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
        c.add(Expression.eq("cn", cn));
        c.add(Expression.eq("divisionGuid", divisionGuid));
        return c.list().size() > 0;
    }

	/**
	 * Dekretuje zgodnie z informacj� zawart� w tym acceptance condition
	 * @param activityId
	 */
	public void makeAssignmentForAcceptance(String activityId) throws EdmException{
//		WorkflowFactory.simpleAssignment(activityId, cn, username, username, cn, cn, username, null);
		if(divisionGuid == null){
			WorkflowFactory.simpleAssignment(activityId,
				DSUser.findByUsername(username).getDivisions()[0].getGuid(),
				username,
				DSApi.context().getPrincipalName(),
				null,
				WorkflowFactory.SCOPE_ONEUSER,
				"do akceptacji",
				null);
		} else {
			WorkflowFactory.simpleAssignment(activityId,
				divisionGuid,
				null,
				DSApi.context().getPrincipalName(),
				null,
				WorkflowFactory.SCOPE_DIVISION,
				"do akceptacji",
				null);
		}
	}
    
    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getFieldValue()
    {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue)
    {
        this.fieldValue = fieldValue;
    }        
    
    @Override
    public boolean equals(Object obj) {
    	return (obj instanceof AcceptanceCondition) && ((AcceptanceCondition)obj).id.equals(id);
    }
    
    @Override
    public int hashCode() {
    	return (int)id.longValue();
    }        
    
    /**
     * Tylko dla centr�w kosztowych
     */
    public boolean isSameType(AcceptanceCondition b){
    	return this.cn.equals(b.cn) && (b.divisionGuid!= null) && b.divisionGuid.equals(this.divisionGuid); 
    	//&& ((this.fieldValue==null && b.fieldValue==null) || (this.fieldValue!= null && this.fieldValue.equals(b.fieldValue)));
    }
    
	public Double getMaxAmountForAcceptance() {
		return maxAmountForAcceptance;
	}

	public void setMaxAmountForAcceptance(Double maxAmountForAcceptance) {
		this.maxAmountForAcceptance = maxAmountForAcceptance;
	}

	public static List<AcceptanceCondition> findByCentrumAmount(Integer centrunId, BigDecimal fullCentrumAmount) throws EdmException 
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class);
            c.add(Restrictions.eq("fieldValue", centrunId.toString()));
            c.add(Restrictions.ge("maxAmountForAcceptance", fullCentrumAmount.doubleValue()));
            c.addOrder(Order.asc("maxAmountForAcceptance"));
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
	}
	
	public static List<AcceptanceCondition> findByAmount(Integer centrumId, BigDecimal fullCentrumAmount, String cn) throws EdmException
	{
		try
		{
			 Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class)			 	
			 	.add(Restrictions.ge("maxAmountForAcceptance", fullCentrumAmount.doubleValue()))
			 	.add(Restrictions.eq("cn", cn))
			 	.addOrder(Order.asc("maxAmountForAcceptance"));
			 
			 if(centrumId != null)
				 c.add(Restrictions.eq("fieldValue", centrumId.toString()));
			 else
				 c.add(Restrictions.isNull("fieldValue"));
			 
			 List<AcceptanceCondition> conditions = c.list();
			 if(!conditions.isEmpty())
				 return conditions;
			 
			 /** jesli nie znaleziono szukamy wsrod coditionow z 0 lub NULL ktore moga akceptowac dowolna kwote **/
			 
			 c = DSApi.context().session().createCriteria(AcceptanceCondition.class)
			 	.add(Restrictions.or(Restrictions.eq("maxAmountForAcceptance", Double.valueOf(0)), 
			 						Restrictions.isNull("maxAmountForAcceptance")))
			 	.add(Restrictions.eq("cn", cn));
			 
			 if(centrumId != null)
				 c.add(Restrictions.eq("fieldValue", centrumId.toString()));
			 else
				 c.add(Restrictions.isNull("fieldValue"));
			 
			 return c.list();
		}
		catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
	}
	
	public static List<AcceptanceCondition> findByAmount(Integer centrumId, BigDecimal fullCentrumAmount, String cn, boolean skipCheckAmout) throws EdmException
	{
		if(skipCheckAmout == false)
		{
			return findByAmount(centrumId, fullCentrumAmount, cn);
		}
		else
		{
			Criteria c = DSApi.context().session().createCriteria(AcceptanceCondition.class).add(Restrictions.eq("cn", cn));
			if(centrumId != null)
				 c.add(Restrictions.eq("fieldValue", centrumId.toString()));
			 else
				 c.add(Restrictions.isNull("fieldValue"));
			
			return c.list();
		}
	}
	
}
