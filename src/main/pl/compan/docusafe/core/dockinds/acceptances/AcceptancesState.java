package pl.compan.docusafe.core.dockinds.acceptances;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.LoggerFactory;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.AvailabilityManager;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.aegon.InvoicePteLogic;
import pl.compan.docusafe.util.Logger;

import static pl.compan.docusafe.util.LoggerFactory.getLogger;


/**
 * Stan dokonanych akceptacji pod k�tem aktualnie zalogowanego u�ytkownika (u�ywane g��wnie 
 * przez klas� {@link FieldsManager}).
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class AcceptancesState implements Serializable
{
    private static final Logger log = getLogger(pl.compan.docusafe.core.dockinds.acceptances.AcceptancesState.class);
    
    private Long documentId;
    private FieldsManager fm;
    private AcceptancesDefinition acceptancesDefinition;
    private List<DocumentAcceptance> generalAcceptances;
    private List<DocumentAcceptance> fieldAcceptances;
    /** 
     * okre�la definicje akceptacji, kt�re z�o�y� zalogowany U�YTKOWNIK, gdy r�wne null lub lista pusta, to 
     * nie z�o�y� �adnej akceptacji og�lnej (ale jak�� akceptacj� zwi�zan� z polem m�g� ju� ��o�y�) 
     */
    private List<Acceptance> givenGeneralAcceptancesDef;
    /**
     * lista identyfikator�w centr�w kosztowych dla faktury ({@link CentrumKosztowDlaFaktury}), dla 
     * kt�rych U�YTKOWNIK dokona� jakiej� akceptacji 
     */
    private List<Long> centraAccepted;
    /**
     * okre�la akceptacje, kt�re zosta�y z�o�one dla poszczeg�lnych centr�w (chodzi o WSZYSTKIE
     * akceptacje, nie tylko aktualnego u�ytkownika!)
     */
    private Map<Long,Set<String>> givenFieldAcceptances;
    private Map<String, Permissions> permissions;           
    /**  
     * pomocnicza mapa centrow kosztowych - potrzebne na stronach JSP, gdy chcemy na podstawie identyfikator 
     * CentrumKosztowDlaFaktury dostac identyfikator CentrumKosztow.
     */  
    private Map<Long,CentrumKosztowDlaFaktury> tmpCentra;
    
    public AcceptancesState(Long documentId, AcceptancesDefinition acceptancesDefinition, FieldsManager fm)
    {
        this.documentId = documentId;
        this.acceptancesDefinition = acceptancesDefinition;   
        this.fm = fm;
    }
    
    public void initialize() throws EdmException
    {
        // wczytywanie wykonanych ju� akceptacji na tym dokumencie
        generalAcceptances = DocumentAcceptance.find(documentId, true);
        fieldAcceptances = DocumentAcceptance.find(documentId, false); 
        
        permissions = new LinkedHashMap<String, Permissions>();
        if(!DocumentLogicLoader.INVOICE_IC_KIND.equals(fm.getDocumentKind().getCn()))
        {
	        for (Acceptance acceptance : acceptancesDefinition.getAcceptances().values())
	        {
	            Permissions permission = new Permissions();
	            permission.setCanAccept(AcceptancesManager.checkAcceptance(acceptance.getCn(), permission.getConditions()));
	            permissions.put(acceptance.getCn(), permission);
	        }
        }

        givenGeneralAcceptancesDef = new ArrayList<Acceptance>();
        for (DocumentAcceptance acceptance : generalAcceptances)
        {
            acceptance.initDescription();
            if (DSApi.context().getPrincipalName().equals(acceptance.getUsername()))
                givenGeneralAcceptancesDef.add(acceptancesDefinition.getAcceptances().get(acceptance.getAcceptanceCn()));                 
        } 
        
        centraAccepted = new ArrayList<Long>();
        tmpCentra = new LinkedHashMap<Long, CentrumKosztowDlaFaktury>();
        givenFieldAcceptances = new LinkedHashMap<Long, Set<String>>();

        for (DocumentAcceptance acceptance : fieldAcceptances)
        {
            acceptance.initDescription();             
            if (DSApi.context().getPrincipalName().equals(acceptance.getUsername()))
                centraAccepted.add(acceptance.getObjectId());
            if (givenFieldAcceptances.get(acceptance.getObjectId()) == null)
                givenFieldAcceptances.put(acceptance.getObjectId(), new HashSet<String>());
            givenFieldAcceptances.get(acceptance.getObjectId()).add(acceptance.getAcceptanceCn());
        }
        fm.initialize();
        if (fm != null && fm.getValue(acceptancesDefinition.getCentrumKosztFieldCn()) != null)
        {
            List<CentrumKosztowDlaFaktury> list = (List<CentrumKosztowDlaFaktury>) fm.getValue(acceptancesDefinition.getCentrumKosztFieldCn());
            for (CentrumKosztowDlaFaktury centrum : list)
            {
                tmpCentra.put(centrum.getId(), centrum);
            }
        }
    }    
    
    public List<Acceptance> getGivenGeneralAcceptancesDef()
    {
        return givenGeneralAcceptancesDef;
    }

    public boolean isCentrumAccepted(Long objectId)
    {
        return centraAccepted.contains(objectId);
    }
    
    /**
     * Przekazuje wszystkie akceptacje og�lne wykonane na danym dokumencie.
     */
    public List<DocumentAcceptance> getGeneralAcceptances()
    {
        return generalAcceptances;
    } 
    
    /**
     * Przekazuje wszystkie akceptacje og�lne wykonane na danym dokumencie.
     */
    public Map<String,DocumentAcceptance> getGeneralAcceptancesAsMap()
    {
    	Map<String,DocumentAcceptance> returMap = new TreeMap<String, DocumentAcceptance>();
       for (DocumentAcceptance da : generalAcceptances) 
       {
    	   returMap.put(da.getAcceptanceCn(), da);
       }
       return returMap;
    } 
    
    /**
     * Przekazuje wszystkie akceptacje centrum kosztowego wykonane na danym dokumencie.
     */
    public List<DocumentAcceptance> getFieldAcceptances()
    {
        return fieldAcceptances;
    }
    
    public Permissions getPermission(String acceptanceCn)
    {
        return permissions.get(acceptanceCn);
    }
    
    /**
     * Przekazuje dokonan� akceptacj� danego rodzaju powi�zan� z danym obiektem (warto�ci� pola, od
     * kt�rego zale�y akceptacja tego rodzaju).
     */
    public DocumentAcceptance getFieldAcceptance(String acceptanceCn, Long objectId)
    {
        // jak nie podano id obiektu powi�zanego z akceptacj� to zwracam null
        if (objectId == null)
            return null;
        for (DocumentAcceptance acc : fieldAcceptances)
        {
            if (acc.getAcceptanceCn().equals(acceptanceCn) && objectId.equals(acc.getObjectId()))
            {
                return acc;
            }
        }
        return null;
    }    
    
    /**
     * Przekazuje tylko te definicje akceptacji, kt�re s� zale�ne od pola 'fieldCn' (gdy 'fieldCn' lub 'objectId' r�wne null,
     * zwracana jest pusta mapa) i kt�re aktualnie zalogowany u�ytkownik ma prawo wykona� dla warto�ci zale�nego
     * pola r�wnej 'objectId'. 
     */
    public Map<String,Acceptance> getFieldAcceptancesDef(String fieldCn, Long objectId)
    {
		log.trace("getFieldAcceptancesDef");
        if (!acceptancesDefinition.isGivingDifferentAcceptancesEnabled() && givenGeneralAcceptancesDef.size() > 0){
            // w��czona zasada drugiej pary oczu i dokonana ju� jaka� akceptacja og�lna - zatem zwracamy pust� map�
			log.trace("return empty map");
            return Collections.emptyMap();
		}

        Map<String,Acceptance> map = new LinkedHashMap<String, Acceptance>();
        if (fieldCn != null && objectId != null)
        {
        	try
        	{
        		if (!DSApi.isContextOpen())
        		{
        			// otwarcie kontekstu dla wywo�ania tej funkcji w widoku
        			DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
        			DSApi.context().begin();
        		}
        		
        		CentrumKosztowDlaFaktury centrum = tmpCentra.get(objectId);
                
        		DocumentLogic documentLogic = Document.find(fm.getDocumentId()).getDocumentKind().logic();
                if (documentLogic instanceof InvoicePteLogic)
                	((InvoicePteLogic) documentLogic).setCentrum(centrum);
                
                for (Acceptance acc : acceptancesDefinition.getFieldAcceptances().values())
                {
                    boolean contains = permissions.get(acc.getCn()).isCanAccept();
                    if (!contains && centrum != null)
                        for (Object condition : permissions.get(acc.getCn()).getConditions())
                        {                        
                            if (condition.equals(centrum.getCentrumId().toString()))
                                contains = true;
                        }
                    if (fieldCn.equals(acc.getFieldCn()) && contains//permissions.get(acc.getCn()).getConditions().contains(centrumId.toString())
                    && (givenFieldAcceptances.get(objectId) == null || !givenFieldAcceptances.get(objectId).contains(acc.getCn()))
                    && documentLogic.canAcceptDocument(fm, acc.getCn()))
                        // gdy jaka� akceptacja danego rodzaju zosta�a ju� dokonana to obecnie nie pozwalamy na drug� tak� sam� 
                        // (nawet innego u�ytkownika) 
                    {
                        if (!acceptancesDefinition.isGivingDifferentAcceptancesEnabled())
                        {
                            // zasada drugiej pary oczu w��czona - dodajemy do mapy tylko, gdy tego centrum wcze�niej nie akceptowali�my
                            if (!centraAccepted.contains(objectId))
                                map.put(acc.getCn(), acc);
                        }
                        else
                        {
                            // zasada drugiej pary oczu wy��czona - dodajemy zawsze do mapy
                            map.put(acc.getCn(), acc);
                        }
                    }
                }
                
                if (!DSApi.isContextOpen()) // commit
                	DSApi.context().commit();
        	}
        	catch (EdmException ex)
        	{
        		log.error(ex.getMessage(), ex);
        		DSApi.context().setRollbackOnly();
        	}
        	finally
        	{
        		if (!DSApi.isContextOpen()) // zamkniecie kontekstu
        			DSApi._close();
        	}
        }
        return map;
    }
    
    /**
     * Przekazuje tylko te definicje og�lnych akceptacji, kt�re u�ytkownik ma prawo wykona� (sytuacja
     * jest inna w zale�no�ci od tego, czy w��czona jest zasada drugiej pary oczu).
     * @throws EdmException 
     */
    public Map<String,Acceptance> getGeneralAcceptancesDef() throws EdmException
    {
        //long startTime = System.currentTimeMillis();
        Map<String,Acceptance> map = new LinkedHashMap<String, Acceptance>();
        if (!acceptancesDefinition.isGivingDifferentAcceptancesEnabled() && 
            (givenGeneralAcceptancesDef.size() > 0 || givenFieldAcceptances.size() > 0))
            // w��czona zasada drugiej pary oczu i dokonana ju� jaka� akceptacja - zatem zwracamy pust� map�
            return map;
        DSUser user = DSUser.findByUsername(DSApi.context().getPrincipalName());
        DSDivision[] division = user.getDivisions();
        
        DocumentLogic documentLogic = Documents.document(fm.getDocumentId()).getDocument().getDocumentKind().logic();
        for (Acceptance acc : acceptancesDefinition.getGeneralAcceptances().values())
        {
    	
            if (!givenGeneralAcceptancesDef.contains(acc))
            {
                boolean canAccepting = documentLogic.canAcceptDocument(fm, acc.getCn());
            	if((AvailabilityManager.isAvailable("archiwizacja.akceptacja.pracownik", fm.getDocumentKind().getCn()) && canAccepting) || (AcceptanceCondition.canAcceptDocumentByUser(acc.getCn(), DSApi.context().getPrincipalName()) && 
                        canAccepting))
            	{
            		map.put(acc.getCn(), acc);
            	}
            	else
            	{
            		for (int i = 0; i < division.length; i++)
            		{
            			if(AcceptanceCondition.canAcceptDocumentByDivision(acc.getCn(), division[i].getGuid())
            			   && documentLogic.canAcceptDocument(fm, acc.getCn()))
            			{
            				map.put(acc.getCn(), acc);
            				break;
            			}
					}
            	}
            		
            }
        }
        //log.info("getGeneralAcceptancesDef -> {} ms", System.currentTimeMillis() - startTime);
        return map;
    }
    
    /**
     * Uprawnienia aktualnie zalogowanej osoby do wykonania danej akceptacji.   
     */
    public class Permissions implements Serializable
    {
        /** uprawnienie do akceptacji dla aktualnie zalogowanego u�ytkownika */
        private boolean canAccept;
        /** warunki pola steruj�cego na mo�liwo�� dokonania tej akceptacji */
        private List<Object> conditions;
        
        public boolean isCanAccept()
        {
            return canAccept;
        }
        
        public void setCanAccept(boolean canAccept)
        {
            this.canAccept = canAccept;
        }
        
        public List<Object> getConditions()
        {
            if (conditions == null)
                conditions = new ArrayList<Object>();
            return conditions;
        }
        
        public void setConditions(List<Object> conditions)
        {
            this.conditions = conditions;
        }               
    }
    

    @Deprecated
    /**
     * 
     * @param simpleAcceptance 
     * @return Zwraca nastepna akceptracje jaka powinna zostac wykonana. Sprawdza czy nie jest to akceptacja  thisAcceptation
     * @throws EdmException 
     */
    public String getNextAcceptation(CentrumKosztowDlaFaktury centrum, Boolean simpleAcceptance) throws EdmException
    {
        Map<String,Acceptance> tmp = fm.getAcceptancesDefinition().getFieldAcceptances(fm.getAcceptancesDefinition().getCentrumKosztFieldCn());
        
       tmp = new LinkedHashMap<String,Acceptance>(fm.getAcceptancesDefinition().getFieldAcceptances());       
       Set<String> wyszystkieRodzajeAkceptacji = tmp.keySet();
       //usuniecie niepotrzebnych akceptacj centrum JBPM
       if(fm.getValue(InvoiceLogic.JBPM_PROCESS_ID) != null && centrum != null
    		   && centrum.getAcceptanceModeInfo() != null )
       {
    	   for (String cn : fm.getAcceptancesDefinition().getFieldAcceptances().keySet())
    	   {
    		   if(!centrum.getAcceptanceCnsNeeded().contains(cn))
    		   {
    			   wyszystkieRodzajeAkceptacji.remove(cn);
    		   }
    	   }
       }
       //TODO przenie�� to definicji akceptacji (rozszerzy� je�li jest to konieczne)
       if(simpleAcceptance)
       {
    	   wyszystkieRodzajeAkceptacji.remove("szefa_dzialu");
       }
       
       //usuniecie niepotrzebnych akceptacji ogolnych JBPM
       tmp = new LinkedHashMap<String,Acceptance>(fm.getAcceptancesDefinition().getGeneralAcceptances());
       Set<String> wyszystkieRodzajeAkceptacjiGeneral = tmp.keySet();
       if(fm.getValue(InvoiceLogic.JBPM_PROCESS_ID) != null && centrum != null
    		   && centrum.getAcceptanceModeInfo() != null )
       {
    	   for (String cn : fm.getAcceptancesDefinition().getGeneralAcceptances().keySet())
    	   {
    		   if(!centrum.getAcceptanceCnsNeeded().contains(cn)){
    			   wyszystkieRodzajeAkceptacjiGeneral.remove(cn);
    		   }
    	   }
       }       
       
       Integer minLevel = Integer.MAX_VALUE;
       String nextAcceptance = null;
       
	   List<CentrumKosztowDlaFaktury> centraDoAkceptacji = (List<CentrumKosztowDlaFaktury>) fm.getValue(fm.getAcceptancesDefinition().getCentrumKosztFieldCn());
	   if(centrum == null)
	   {
		   for(CentrumKosztowDlaFaktury centr : centraDoAkceptacji)
	       {
			 //  centr.getAcceptanceModeInfo().isAcceptanceNeeded(cn)
			   for(String rodzaj : wyszystkieRodzajeAkceptacji)
		       {
				   if(fm.getAcceptancesDefinition().getFieldAcceptances().get(rodzaj) != null && fm.getAcceptancesState().getFieldAcceptance(rodzaj, centr.getId()) == null)
				   {
	  	    			if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel() < minLevel)
	  	    			{
	  	    				minLevel = fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel();    	        				
	  	    			}
	  	    			else if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel().equals(minLevel))
	  	    			{
	  	    				nextAcceptance = rodzaj;
	  	    			}
	  	    			else if(nextAcceptance == null)
	  	    			{
	  	    				nextAcceptance = rodzaj;
	  	    			}
	  	    			else if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel() < 
	  	    					fm.getAcceptancesDefinition().getAcceptances().get(nextAcceptance).getLevel())
	  	    			{
	  	    				nextAcceptance = rodzaj;
	  	    			}
				   }
		       }
	       }
		   if(nextAcceptance == null && minLevel < 100 )
		   {
			   for(String rodzaj : wyszystkieRodzajeAkceptacjiGeneral)
		       {
				   if(fm.getAcceptancesDefinition().getFieldAcceptances().get(rodzaj) == null && fm.getAcceptancesState().getGeneralAcceptancesAsMap().get(rodzaj) == null)
				   {
	  	    			if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel() < minLevel)
	  	    			{
	  	    				nextAcceptance = rodzaj;
	  	    				minLevel = fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel();    	        				
	  	    			}
	  	    			else if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel().equals(minLevel))
	  	    			{
	  	    				nextAcceptance = rodzaj;
	  	    			}
	  	    			else if(nextAcceptance == null)
	  	    			{
	  	    				nextAcceptance = rodzaj;
	  	    			}
	  	    			else if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel() < 
	  	    					fm.getAcceptancesDefinition().getAcceptances().get(nextAcceptance).getLevel())
	  	    			{
	  	    				nextAcceptance = rodzaj;
	  	    			}
				   }
		       }
		   }
		   else if(nextAcceptance == null)
		   {
			   for(String rodzaj : wyszystkieRodzajeAkceptacjiGeneral)
		       {
				   if(fm.getAcceptancesDefinition().getFieldAcceptances().get(rodzaj) == null && fm.getAcceptancesState().getGeneralAcceptancesAsMap().get(rodzaj) == null)
				   {
	  	    			if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel() < minLevel)
	  	    			{
	  	    				minLevel = fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel();    	        				
	  	    			}
	  	    			else if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel().equals(minLevel +1))
	  	    			{
	  	    				nextAcceptance = rodzaj;
	  	    			}
				   }
		       }
		   }
	       
	   }
	   else
	   {
		   Integer thisLevel = Integer.MAX_VALUE;
		   for(String rodzaj : wyszystkieRodzajeAkceptacji)
	       {
			   if(fm.getAcceptancesState().getFieldAcceptance(rodzaj, centrum.getId()) == null)
			   {
 	    			if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel() < minLevel)
 	    			{
 	    				minLevel = fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel();    
 	    			}
 	    			else if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel() < thisLevel)
  	    			{
  	    				thisLevel = fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel();
  	    				nextAcceptance = rodzaj;
  	    			}
			   }
	       }
		   if(nextAcceptance == null)
		   {
			   for(String rodzaj : wyszystkieRodzajeAkceptacjiGeneral)
		       {
				   if(AcceptanceCondition.find(rodzaj).get(0).getFieldValue() == null && fm.getAcceptancesState().getGeneralAcceptancesAsMap().get(rodzaj) == null)
				   {					   
	 	    			if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel() < minLevel)
	 	    			{
	 	    				minLevel = fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel();    	        				
	 	    			}
	 	    			else if(fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel() < thisLevel)
	  	    			{
	  	    				thisLevel = fm.getAcceptancesDefinition().getAcceptances().get(rodzaj).getLevel();
	  	    				nextAcceptance = rodzaj;
	  	    			}
				   }
		       }
		   }
	   }
       return nextAcceptance;
    }
    
    @Deprecated
    /**
     * 
     * @param simpleAcceptance 
     * @return Zwraca akceptacje o najni�szym poziomie nie wykonana jeszcze na tym dokum�cie
     * @throws EdmException
     */
    public AcceptanceCondition getThisAcceptation(
			CentrumKosztowDlaFaktury centrum, Boolean simpleAcceptance)
			throws EdmException {
		Map<String, Acceptance> tmp = fm.getAcceptancesDefinition()
				.getFieldAcceptances(fm.getAcceptancesDefinition().getCentrumKosztFieldCn());
		Set<String> rodzajeAkceptacjiDlaCentrumKosztowego = tmp.keySet();
		List<AcceptanceCondition> conditions = new ArrayList<AcceptanceCondition>();			

		tmp = fm.getAcceptancesDefinition().getAcceptances();
		Set<String> wyszystkieRodzajeAkceptacji = tmp.keySet();
       
       for(String rodzaj : wyszystkieRodzajeAkceptacji)
       {
    	   if(!(simpleAcceptance && rodzaj.equals("szefa_dzialu")))
    	   {
    		   conditions.addAll(AcceptanceCondition.find(rodzaj));
    	   }    		   
       }
        
        for(String rodzaj : rodzajeAkceptacjiDlaCentrumKosztowego)
        {
     	   if(!(simpleAcceptance && rodzaj.equals("szefa_dzialu")))
    	   {
    		   conditions.addAll(AcceptanceCondition.find(rodzaj));
    	   }
        }

       AcceptanceCondition thisAcceptance = null;
       Integer minLevel = Integer.MAX_VALUE;
       if(centrum == null)
       {
    	   List<CentrumKosztowDlaFaktury> centraDoAkceptacji = (List<CentrumKosztowDlaFaktury>) fm.getValue(fm.getAcceptancesDefinition().getCentrumKosztFieldCn());
		   for(CentrumKosztowDlaFaktury centrumTmp : centraDoAkceptacji)
	       {
			   for (AcceptanceCondition condition : conditions)
		    	{
		    		if(condition.getFieldValue() != null && condition.getFieldValue().equalsIgnoreCase(""+centrumTmp.getCentrumId()) && fm.getAcceptancesState().getFieldAcceptance(condition.getCn(), centrumTmp.getId()) == null)
		        	{
		    			if(fm.getAcceptancesDefinition().getAcceptances().get(condition.getCn()).getLevel() < minLevel)
		    			{    	        		
		    				thisAcceptance = condition;
		    				minLevel = fm.getAcceptancesDefinition().getAcceptances().get(condition.getCn()).getLevel();    	        				
		    			}
		        	}		    		
		    	}
	       }
       }
       else
       {
	    	for (AcceptanceCondition condition : conditions)
	    	{
	    		if(condition.getFieldValue() != null && condition.getFieldValue().equalsIgnoreCase(""+centrum.getCentrumId()) && fm.getAcceptancesState().getFieldAcceptance(condition.getCn(), centrum.getId()) == null)
	        	{
	    			if(fm.getAcceptancesDefinition().getAcceptances().get(condition.getCn()).getLevel() < minLevel)
	    			{    	        		
	    				thisAcceptance = condition;
	    				minLevel = fm.getAcceptancesDefinition().getAcceptances().get(condition.getCn()).getLevel();    	        				
	    			}
	        	}	    		
	    	}
       }
    	//jesli null to nie ma juz zadnej dla pola
    	if(thisAcceptance == null)
    	{
    		for (AcceptanceCondition condition : conditions)
        	{
    			if(condition.getFieldValue() == null && fm.getAcceptancesState().getGeneralAcceptancesAsMap().get(condition.getCn()) == null)
            	{
        			if(fm.getAcceptancesDefinition().getAcceptances().get(condition.getCn()).getLevel() < minLevel)
        			{    	        		
        				thisAcceptance = condition;
        				minLevel = fm.getAcceptancesDefinition().getAcceptances().get(condition.getCn()).getLevel();    	        				
        			}
            	}
        	}
    	}
    	return thisAcceptance;
    }



    @Deprecated
    public Map<String,String> userToNextAcceptance(CentrumKosztowDlaFaktury centrum, Boolean simpleAcceptance) throws EdmException
    {
    		Map<String, String> userToAcceptance = new LinkedHashMap<String, String>();
			List<DSUser> userList = new ArrayList<DSUser>();
		
	       	String nextAcceptance = getNextAcceptation(centrum,simpleAcceptance);

        	Integer  tmpInt =  centrum.getAcceptingCentrumId();
	       	
        	try {
				if (nextAcceptance != null) {
					List<AcceptanceCondition> ac = null;
					ac = AcceptanceCondition.find(nextAcceptance, tmpInt.toString());
					if (ac == null || ac.size() < 1) {
						ac = AcceptanceCondition.find(nextAcceptance, null);
					}
					for (AcceptanceCondition acceptanceCondition : ac) {
						String userName = acceptanceCondition.getUsername();
						String divisionName = acceptanceCondition.getDivisionGuid();
						DSDivision division;
						if (userName != null) {
							userList.add(DSUser.findByUsername(userName));
						} else {
							division = DSDivision.find(divisionName);
							DSUser[] users = division.getUsers();
							for (DSUser user : users) {
								userList.add(user);
							}
						}
					}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		Collections.sort(userList, DSUser.LASTNAME_COMPARATOR);
		for(DSUser user: userList){
			userToAcceptance.put(user.getName(), user.asLastnameFirstname());
		}
	    return userToAcceptance;
    }
    
    /**Zwaraca mape uzytkownik�w bior�cych udzia� w procesie akceptacji
     * @throws EdmException 
     * @throws UserNotFoundException */
    public Map<String,String> getAcceptancesUserList() throws UserNotFoundException, EdmException
    {
    	TreeMap<String, String> userMap = new TreeMap<String, String>();
    	List<DocumentAcceptance> dcList = getFieldAcceptances();
    	for (DocumentAcceptance documentAcceptance : dcList)
    	{
			if(!documentAcceptance.isAutomatic()){
				userMap.put(documentAcceptance.getUsername(), DSUser.findByUsername(documentAcceptance.getUsername()).asFirstnameLastname());
			}
		}
    	dcList = getGeneralAcceptances();
    	for (DocumentAcceptance documentAcceptance : dcList) 
    	{
			if(!documentAcceptance.isAutomatic()){
				userMap.put(documentAcceptance.getUsername(), DSUser.findByUsername(documentAcceptance.getUsername()).asFirstnameLastname());
			}
		}
    	return userMap;
    }
    /**Sprawdza czy dane centrum ma ju� nadane wszystkie akceptacje centrum koszowego
     *Je�li centrum == null sprawdzi czy wszystkie centra dla dokumentu s� zaakceptowane*/
    public Boolean isCentrumFieldAccepted(CentrumKosztowDlaFaktury centrum,Boolean simpleAcceptance) throws EdmException
    {
    	List<CentrumKosztowDlaFaktury> centraDoAkceptacji = (List<CentrumKosztowDlaFaktury>) fm.getValue(fm.getAcceptancesDefinition().getCentrumKosztFieldCn());

    	Map<String,Acceptance> tmp = new LinkedHashMap<String, Acceptance>(fm.getAcceptancesDefinition().getFieldAcceptances()); 
    	Set<String> wyszystkieRodzajeAkceptacji = tmp.keySet();
    	if(simpleAcceptance)
    	{
    		wyszystkieRodzajeAkceptacji.remove("szefa_dzialu");
    	}
        
        if(centrum == null)
        {
        	if(centraDoAkceptacji == null)
        		return false;
	        for (CentrumKosztowDlaFaktury centr : centraDoAkceptacji)
	        {
				for (String rodzaj : wyszystkieRodzajeAkceptacji) 
				{
					if(fm.getAcceptancesDefinition().getFieldAcceptances().get(rodzaj) != null && fm.getAcceptancesState().getFieldAcceptance(rodzaj, centr.getId()) == null)
					{
						return false;
					}
				}
			}
        }
        else
        {
        	for (String rodzaj : wyszystkieRodzajeAkceptacji) 
			{
				if(fm.getAcceptancesDefinition().getFieldAcceptances().get(rodzaj) != null && fm.getAcceptancesState().getFieldAcceptance(rodzaj, centrum.getId()) == null)
				{
					return false;
				}
			}
        }    	
    	return true;
    }
    /**Sprawdeza czy wszystkie akceptacje og�lne dla dokumentu zosta�y wykonane*/
    public Boolean isDocumentAccepted() throws EdmException
    {
	
    	Map<String,Acceptance> tmp = fm.getAcceptancesDefinition().getGeneralAcceptances();
        Set<String> wyszystkieRodzajeAkceptacjiGeneral = tmp.keySet();
        
    	for(String rodzaj : wyszystkieRodzajeAkceptacjiGeneral)
	       {
    			
			   if(AcceptanceCondition.find(rodzaj) != null && AcceptanceCondition.find(rodzaj).get(0).getFieldValue() == null && fm.getAcceptancesState().getGeneralAcceptancesAsMap().get(rodzaj) == null)
			   {
				   return false;
			   }
	       }
    	return true;
    }
    
    
    
}
