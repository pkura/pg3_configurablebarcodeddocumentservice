package pl.compan.docusafe.core.dockinds.acceptances;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import java.util.Set;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.checker.ChannelAcceptanceChecker;
import pl.compan.docusafe.core.dockinds.acceptances.checker.AccountantAcceptanceChecker;
import pl.compan.docusafe.core.dockinds.acceptances.checker.CentrumAcceptanceChecker;
import pl.compan.docusafe.core.dockinds.acceptances.checker.CompleteChannelAcceptanceChecker;
import pl.compan.docusafe.core.dockinds.acceptances.checker.DivisorAcceptanceChecker;
import pl.compan.docusafe.core.dockinds.acceptances.checker.GlobalAcceptanceChecker;
import pl.compan.docusafe.core.dockinds.acceptances.checker.ProductManagerChecker;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * Tryby akceptacji intercarsa, r�ne kana�y mog� mie� r�ne tryby
 * z tego enuma korzysta dockind (invoice_ic.xml)
 * @author Micha� Sankowski
 */
public enum IntercarsAcceptanceMode implements CompleteChannelAcceptanceChecker
{	
	/* !! Nie zmienia� kolejno�ci !! */
	FULL(start("pelny").
			centrum("zwykla").
			centrum("szefa_dzialu").
			centrum("szefa_pionu").
			global("dyrektora_fin").
			accountant("ksiegowa").
//			accountant("kontrola_jakosci").
		end()),
	/* !! Nie zmienia� kolejno�ci !! */
	SIMPLE2(start("uproszczony2").
			centrum("zwykla").
			centrum("szefa_pionu").
			global("dyrektora_fin").
			accountant("ksiegowa").
//			accountant("kontrola_jakosci").
		end()),
	/* !! Nie zmienia� kolejno�ci !! */
	SIMPLE3(start("uproszczony3").
			centrum("zwykla").
			centrum("szefa_dzialu").
			global("dyrektora_fin").
			accountant("ksiegowa").
//			accountant("kontrola_jakosci").
		end()),
	/* !! Nie zmienia� kolejno�ci !! */
	FAST(start("szybki").
			centrum("zwykla").
			accountant("ksiegowa").
//			accountant("kontrola_jakosci").
		end()),
	/* !! Nie zmienia� kolejno�ci !! */
	SIMPLE4(start("uproszczony4").
			centrum("zwykla").
			centrum("szefa_dzialu").
			centrum("szefa_pionu").
			accountant("ksiegowa").
//			accountant("kontrola_jakosci").
		end()),
	/* !! Nie zmienia� kolejno�ci !! */
	SIMPLE5(start("uproszczony5").
			centrum("zwykla").
			centrum("szefa_pionu").
			accountant("ksiegowa").
//			accountant("kontrola_jakosci").
		end()),
	/* !! Nie zmienia� kolejno�ci !! */
	SIMPLE6(start("uproszczony6").
			centrum("zwykla").
			global("dyrektora_fin").
//			accountant("kontrola_jakosci").
		end()),
			
	/* !! Nie zmienia� kolejno�ci !! */
	SIMPLE7(start("uproszczony7").
			centrum("zwykla").
			centrum("szefa_dzialu").
			accountant("ksiegowa").
//			accountant("kontrola_jakosci").
		end()),

	/* !! Nie zmienia� kolejno�ci !! */
	PRODUCT_MANAGER(start("product_manager").
			centrum("zwykla").
			product("product_manager").
			accountant("ksiegowa").
//			accountant("kontrola_jakosci").
		end()),
	/* !! Nie zmienia� kolejno�ci !! */
	SIMPLEST(start("najprostszy").
			centrum("zwykla").
			accountant("ksiegowa").
		end()),
	/* !! Nie zmienia� kolejno�ci !! */
	ALL(start("wszystkie_poziomy").
			centrum("zwykla").
			centrum("szefa_dzialu").
			centrum("szefa_pionu").
			product("product_manager").
			accountant("dyrektora_fin").
			accountant("ksiegowa").
//			global("kontrola_jakosci").
		end()),
	/* !! Nie zmienia� kolejno�ci !! */
	LAST_ONLY(start("ostatnie_poziomy")
			.accountant("ksiegowa")
//			.accountant("kontrola_jakosci")
		.end()),

	XXX(start("Zwykla").
			centrum("zwykla").
//			global("ksiegowa").
//			global("kontrola_jakosci").
			end()),

	ACCOUNANT_ONLY(start("tylko_ksi�gowa")
			.accountant("ksiegowa")
		.end()),

    SIMPLE_TO_ACCOUNTANT(start("simpleToAccountant").
			centrum("zwykla").
			global("dyrektora_fin").
			accountant("ksiegowa").
//			accountant("kontrola_jakosci").
			end());
	
	static final Logger log = LoggerFactory.getLogger(IntercarsAcceptanceMode.class);
	final Map<String,ChannelAcceptanceChecker> acceptanceCheckers;
    /** lista cn�w akceptacji w odpowiedniej kolejno�ci */
	private final List<String> list;
	final private String name;

    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(IntercarsAcceptanceManager.class.getPackage().getName(),null);
	
	IntercarsAcceptanceMode(CreationBean bean){
		acceptanceCheckers = Collections.unmodifiableMap(bean.map);
		name = bean.name;
		list = bean.list;
	}

	public boolean isAcceptanceNeeded(String cn){
		return acceptanceCheckers.containsKey(cn);
	}

	public Set<String> getCNs(){
		return acceptanceCheckers.keySet();
	}
	
	public boolean checkAcceptanceCondition(Document doc, String cn, CentrumKosztowDlaFaktury ckdf, String activityId) throws EdmException{
		log.trace("IntercarsAcceptanceMode#checkAcceptanceCondition(doc.id = "+doc.getId()+", cn = {}, objectId = {})", cn, ckdf);
		if(ckdf.getAcceptanceCnsNeeded().contains(cn)){//je�li dany cn jesty wymagany dla tego trybu
			return acceptanceCheckers.get(cn).checkAcceptanceCondition(doc, cn, ckdf, activityId);
		} else {
			return false;
		}
	}

	/**
	 * Zwraca kody akceptacji przed podanym kodem (nie w��czaj�c podanego kodu)
	 * @param cn
	 * @return
	 * @throws IllegalArgumentException je�li kod nie istnieje
	 */
	public List<String> cnsBefore(String cn){
		return getBefore(cn, list);
	}

	/**
	 * Zwraca kody akceptacji po podanym kodzie (w��czaj�c podany kod)
	 * @param cn
	 * @return
	 * @throws IllegalArgumentException je�li kod nie istnieje
	 */
	public List<String> cnsAfter(String cn){
		return getAfter(cn, list);
	}

	public static List<String> allCnsAfter(String cn){
		return getAfter(cn, ALL.list);
	}

	private static List<String> getAfter(String cn, Collection<String> all) {
		List<String> ret = new ArrayList<String>(all.size());
		boolean found = false;
		for (String thisCn : all) {
			if (!found && cn.equals(thisCn)) {
				found = true;
				ret.add(thisCn);
			} else if(found){
				ret.add(thisCn);
			}
		}
		if(found){
			return ret;
		} else {
			throw new IllegalArgumentException("Kod akceptacji " + cn + " nie istnieje");
		}
	}

	private static List<String> getBefore(String cn, List<String> all) {
		List<String> ret = new ArrayList<String>(all.size());
		for (String thisCn : all) {
			if (cn.equals(thisCn)) {
				return ret;
			} else {
				ret.add(thisCn);
			}
		}
		throw new IllegalArgumentException("Kod akceptacji " + cn + " nie istnieje");
	}

	/**
	 * Zwraca tryb akceptacji bazuj�c na polach documentu,
	 * @param fm - fields manager dokumentu intercarsa
	 * @return mo�e zwr�ci� null
	 * @deprecated tryb akceptacji b�dzie zale�e� od kana�u i dokumentu
	 */
	@Deprecated
	@SuppressWarnings("deprecation")
	public static IntercarsAcceptanceMode from(FieldsManager fm) throws EdmException{
		/*
		 * Stary tryb akceptacji uproszczonej 
		 */
		Boolean legacySimpleAcceptance = (Boolean) fm.getKey(InvoiceLogic._LEGACY_SIMPLE_ACCEPTANCE_FIELD_CN);		
		if(legacySimpleAcceptance == Boolean.TRUE){
			return IntercarsAcceptanceMode.SIMPLE2;
		}
		return FULL; //doko�czy�		
	}
	
	static AcceptanceCheckerBuilder start(String name){		
		return new AcceptanceCheckerBuilder(name);
	}

	public String getName() {
		return sm.getString(name);
	}

	/**
	 * Dla pracownik�w Intercarsa
	 */
	public String getSimpleName(){
		if(this != FULL){
			return sm.getString("uproszczony");
		} else {
			return sm.getString("pelny");
		}
	}
	
	public int getId(){
		return this.ordinal();
	}

    private static final Set<String> allAcceptanceCodes;
	static{ //init _allAcceptanceCodes
        Set<String> _allAcceptanceCodes = new HashSet<String>();
		for(IntercarsAcceptanceMode m : IntercarsAcceptanceMode.values()){
			_allAcceptanceCodes.addAll(m.acceptanceCheckers.keySet());
		}
        allAcceptanceCodes = Collections.unmodifiableSet(_allAcceptanceCodes);
	}

	public static Set<String> getAllAcceptanceCodes(){
		return allAcceptanceCodes;
	}

	public List<String> getCnList() {
		return list;
	}


}
class CreationBean{	
	String name;
	Map<String,ChannelAcceptanceChecker> map = new HashMap<String, ChannelAcceptanceChecker>();
	List<String> list = new LinkedList<String>();
}

class AcceptanceCheckerBuilder{
	
	final public CreationBean ret = new CreationBean();
	
	AcceptanceCheckerBuilder(String name){
		ret.name = name ;
	}	
	
	AcceptanceCheckerBuilder global(String cn){
		ret.list.add(cn);
		ret.map.put(cn, new GlobalAcceptanceChecker());
		return this;
	}
	
	AcceptanceCheckerBuilder centrum(String cn){
		ret.list.add(cn);
		ret.map.put(cn, new CentrumAcceptanceChecker());
		return this;
	}

	AcceptanceCheckerBuilder product(String cn){
		ret.list.add(cn);
		ret.map.put(cn, new ProductManagerChecker());
		return this;
	}

	AcceptanceCheckerBuilder lucas(String cn){
		ret.list.add(cn);
		ret.map.put(cn, new DivisorAcceptanceChecker());
		return this;
	}
	
	AcceptanceCheckerBuilder accountant(String cn){
		ret.list.add(cn);
		ret.map.put(cn, new AccountantAcceptanceChecker());
		return this;
	}
	
	CreationBean end(){
		return ret;
	}
}
