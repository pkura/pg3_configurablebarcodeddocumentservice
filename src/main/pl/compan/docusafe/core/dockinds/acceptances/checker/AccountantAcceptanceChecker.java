package pl.compan.docusafe.core.dockinds.acceptances.checker;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.acceptances.NoAcceptanceConditionDefined;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import static java.lang.String.format;

/**
 * ChannelAcceptanceChecker sprawdzaj�cy poziom akceptacji ksi�gowych (dla intercarsa).
 * @author MSankowski 
 */
public class AccountantAcceptanceChecker implements ChannelAcceptanceChecker {
	
	private final static Logger log = LoggerFactory.getLogger(AccountantAcceptanceChecker.class);
	private final static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(CentrumAcceptanceChecker.class.getPackage().getName(),null);
	private final String SPECIAL_SUFFIX = "_sup";

	
	public boolean checkAcceptanceCondition(Document doc,
			String cn, CentrumKosztowDlaFaktury ckdf, String activityId) throws EdmException {
		log.trace(format("AccountantAcceptanceChecker#checkAcceptanceCondition(doc.id = %d, cn = %s, centrumId = %s)",doc.getId(), cn, ckdf));
		/* Kod wyboru ksi�gowej */
		FieldsManager fm = doc.getFieldsManager();
		Field enums = fm.getField(InvoiceLogic.RODZAJ_FAKTURY_CN);
		EnumItem item = enums.getEnumItem((Integer)(doc.getFieldsManager().getKey(InvoiceLogic.RODZAJ_FAKTURY_CN)));
		
		if (DocumentAcceptance.find(doc.getId(), cn, null) != null){
			return false;
		}
		
		log.trace("enum: cn = {}",item.getCn());
		log.trace("enum: cn = {}",item.getTitle());

		String acceptanceConditionCN = cn +"_" + item.getCn();

		//TODO wyrzuci� budowanie cn'�w
		List<AcceptanceCondition> acs = AcceptanceCondition.find(acceptanceConditionCN);
		
		if(acs.size()<1){
			throw new NoAcceptanceConditionDefined(sm.getString(
					"BrakZdefiniowanejGlobalnejAkceptacji", acceptanceConditionCN));
					
		} else if(activityId != null){
			
			if(fm.getBoolean(InvoiceLogic.FAKTURA_SPECJALNA_CN)){
				acceptanceConditionCN += SPECIAL_SUFFIX;
				List<AcceptanceCondition> acsups = AcceptanceCondition.find(acceptanceConditionCN);
				if(acsups.size() > 0){
					log.trace("AccountantAcceptanceChecker: dekretacja na " + acs.get(0).getDivisionGuid() + " i " + acsups.get(0).getDivisionGuid());
					WorkflowFactory.assignForMany(activityId, DSApi.context().getPrincipalName(), 
							acs.get(0).getDivisionGuid(),
							acsups.get(0).getDivisionGuid());
				} else {
					log.trace("AccountantAcceptanceChecker: dekretacja na {}",acs.get(0).getDivisionGuid());
					WorkflowFactory.simpleAssignment(activityId,
							acs.get(0).getDivisionGuid(), 
							acs.get(0).getUsername(), 
							DSApi.context().getPrincipalName(), 
							null, 
							acs.get(0).getDivisionGuid() != null ? WorkflowFactory.SCOPE_DIVISION : WorkflowFactory.SCOPE_ONEUSER, 
							null, 
							null);					
				}
			} else if(fm.getBoolean(InvoiceICLogic.KOREKTY_FIELD_CN)
					|| fm.getBoolean(InvoiceICLogic.CREDIT_NOTY_FIELD_CN)){
				log.debug("Dekretacja CREDIT NOTY lub KOREKTY");
				acceptanceConditionCN += SPECIAL_SUFFIX;
				List<AcceptanceCondition> acsups = AcceptanceCondition.find(acceptanceConditionCN);
				if(acsups.size()<1){
					throw new NoAcceptanceConditionDefined(sm.getString(
					"BrakZdefiniowanejAkceptacji", acceptanceConditionCN,
					""));
				}
				acsups.get(0).makeAssignmentForAcceptance(activityId);
			} else {
				log.trace("AccountantAcceptanceChecker: dekretacja na {}", acs.get(0).getDivisionGuid());
				acs.get(0).makeAssignmentForAcceptance(activityId);
			}			
		}	
		return true;
	}			
}
