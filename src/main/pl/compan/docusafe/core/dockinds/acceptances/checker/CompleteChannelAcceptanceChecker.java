package pl.compan.docusafe.core.dockinds.acceptances.checker;

/**
 * Obiekt, kt�ry potrafi sprawdza� akceptacje dla ca�ego kana�u
 * @author Micha� Sankowski
 */
public interface CompleteChannelAcceptanceChecker extends ChannelAcceptanceChecker{
	int getId();
	
	String getName();
}
