package pl.compan.docusafe.core.dockinds.acceptances;

import pl.compan.docusafe.core.dockinds.process.AbstractProcessLogic;
import pl.compan.docusafe.core.dockinds.process.ProcessAction;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public class AcceptanceLogic extends AbstractProcessLogic {

	private final static Logger LOG = LoggerFactory.getLogger(AcceptanceLogic.class);

	public void process(ProcessInstance pi, ProcessAction pa) {
		
	}
}
