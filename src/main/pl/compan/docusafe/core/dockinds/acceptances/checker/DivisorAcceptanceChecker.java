package pl.compan.docusafe.core.dockinds.acceptances.checker;
import java.math.BigDecimal;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.acceptances.NoAcceptanceConditionDefined;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import static java.lang.String.format;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DivisorAcceptanceChecker extends CentrumAcceptanceChecker{

	private static final Logger log = LoggerFactory.getLogger(DivisorAcceptanceChecker.class);
	private final static StringManager sm =
        GlobalPreferences.loadPropertiesFile(CentrumAcceptanceChecker.class.getPackage().getName(),null);


	@Override
	public boolean checkAcceptanceCondition(Document doc,
			String cn, CentrumKosztowDlaFaktury ckdf, String activityId) throws EdmException {
		log.trace(format("CentrumAcceptanceChecker#checkAcceptanceCondition(doc.id = %d, cn = %s, centrumId = %s)",doc.getId(), cn, ckdf));

		if (DocumentAcceptance.find(doc.getId(), cn, ckdf.getId())!=null){
			return false;
		} else {
			Integer centrumId = ckdf.getAcceptingCentrumId();

			BigDecimal kwota = (BigDecimal)doc.getFieldsManager().getKey(InvoiceICLogic.KWOTA_NETTO_FIELD_CN);

			if(kwota.compareTo(new BigDecimal("10000"))<0){
				cn = cn + "_10000";
			} else if (kwota.compareTo(new BigDecimal("20000"))<0) {
				cn = cn + "_20000";
			} else if (kwota.compareTo(new BigDecimal("50000"))<0) {
				cn = cn + "_50000";
			} else if (kwota.compareTo(new BigDecimal("100000"))<0) {
				cn = cn + "_100000";
			} else {
				cn = cn + "_X";
			}

			List<AcceptanceCondition> acs = AcceptanceCondition.find(
                    cn, centrumId.toString());

			if (acs.size() < 1) {
				throw new NoAcceptanceConditionDefined(sm.getString(
						"BrakZdefiniowanejAkceptacji", cn,
						""));
			}

			if(activityId != null){
				WorkflowFactory.simpleAssignment(activityId,
					acs.get(0).getDivisionGuid(),
					acs.get(0).getUsername(),
					DSApi.context().getPrincipalName(),
					null,
					acs.get(0).getDivisionGuid() != null ? WorkflowFactory.SCOPE_DIVISION : WorkflowFactory.SCOPE_ONEUSER,
					null,
					null);

				log.trace("CentrumAcceptanceChecker dekretacja na {}",acs.get(0).getDivisionGuid());
			}

			return true;
		}
	}

}
