package pl.compan.docusafe.core.dockinds.acceptances.checker;

import org.jbpm.context.exe.ContextInstance;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;

/**
 * Obiekt szukaj�cy niespe�nianych warunk�w akceptacji dla danego poziomu.
 * @author MSankowski 
 */
public interface AcceptanceChecker {

	/**
	 * Zwraca informacje czy dla danego kodu akceptacji s� potrzebne jeszcze akceptacje. Dodatkowo w razie potrzeby dekretuje na odpowiedni dzia� i dodaje wpis do historii.
	 * @param activityId mo�e by� null - wtedy nie ma dekretacji
	 * @return true je�li na wskazanym poziomie wymagane s� jeszcze jakie� akceptacje
	 */
	boolean checkAcceptanceCondition(ContextInstance contextInstance, Document doc, String acceptanceCodeName, String activityId) throws EdmException;	
}
