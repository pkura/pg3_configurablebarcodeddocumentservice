package pl.compan.docusafe.core.dockinds.acceptances;
import com.google.common.base.Preconditions;
import org.jbpm.graph.def.Transition;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.office.workflow.JBPMFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * Klasa zawieraj�ca informacje o procesia akceptacji intercarsa
 * @author Micha� Sankowski
 */
public class IntercarsAcceptanceProcess implements ProcessInstance{

	public final static String PROCESS_NAME = "AkceptacjeIC";
	private final static Logger log = LoggerFactory.getLogger(IntercarsAcceptanceProcess.class);
	private final org.jbpm.graph.exe.ProcessInstance jbpmProcess;
	private final String status;
	private org.jbpm.context.exe.ContextInstance contextInstance;
	private final boolean processStarted;
	private final long documentId;

	public IntercarsAcceptanceProcess(org.jbpm.graph.exe.ProcessInstance jbpmProcess, Document document) throws EdmException{
        Preconditions.checkNotNull(jbpmProcess);
		this.jbpmProcess = jbpmProcess;
		processStarted = DocumentAcceptance.hasAcceptance(document.getId());
		status = this.jbpmProcess.getRootToken().getNode().getName();
		documentId = document.getId();
//		log.debug("Process Started = " + processStarted+ " " + this);
	}

	public IntercarsAcceptanceProcess(org.jbpm.graph.exe.ProcessInstance jbpmProcess) throws EdmException{
		this.jbpmProcess = jbpmProcess;
		processStarted = DocumentAcceptance.hasAcceptance((Long)jbpmProcess.getContextInstance().getVariable(IntercarsAcceptanceManager.DOC_ID));
		status = this.jbpmProcess.getRootToken().getNode().getName();
		documentId = (Long)jbpmProcess.getContextInstance().getVariable(IntercarsAcceptanceManager.DOC_ID);
//		log.debug("Process Started = " + processStarted+ " " + this);
	}

	/**
	 * @return the documentId
	 */
	public long getDocumentId() {
		return documentId;
	}

	public static enum Actions{
		PROCESS_ADMIN_REJECT
	}

	public boolean hasStarted(){
		return processStarted;
	}

	public boolean hasEnded(){
		return getJbpmProcess().hasEnded();
	}

    public Collection<String> getActions(){
        ArrayList<String> ret = new ArrayList<String>();
        for(Transition transition:  (Set<Transition>)jbpmProcess.getRootToken().getAvailableTransitions() ){
            if(Character.isUpperCase(transition.getName().charAt(0))){
                ret.add(transition.getName());
            }
        }
        return ret;
    }

	public void initJBPMValues() throws EdmException {
		this.contextInstance = jbpmProcess.getContextInstance();
        if(this.contextInstance.getVariables() != null){
		    this.contextInstance.getVariables().size();
        } 

		this.jbpmProcess.getRootToken().getNode().getName();
        this.jbpmProcess.getRootToken().getAvailableTransitions();
	}

	public org.jbpm.context.exe.ContextInstance getContextInstance() {
		return contextInstance;
	}

	public org.jbpm.graph.exe.ProcessInstance getJbpmProcess() {
		return jbpmProcess;
	}

	public String getProcessName() {
		return PROCESS_NAME;
	}

	public String getStatus(){
		return status;
	}

	public Object getId() {
		return jbpmProcess.getId();
	}
}
