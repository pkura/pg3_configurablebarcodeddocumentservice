package pl.compan.docusafe.core.dockinds.acceptances.checker;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import org.jbpm.context.exe.ContextInstance;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;

public class IntercarsAcceptanceChecker implements AcceptanceChecker {
	
	static final Logger log = LoggerFactory.getLogger(IntercarsAcceptanceChecker.class);
	public static final String CENTRUM_DLA_FAKTURY_ID = "centrumId";

	public boolean checkAcceptanceCondition(ContextInstance contextInstance, Document doc, String acceptanceCodeName, String activityId) throws EdmException {		
		log.trace("IntercarsAcceptanceChecker - doc.id = {}, acceptanceCN = {} ",doc.getId(), acceptanceCodeName);
		
		if(contextInstance.hasVariable(CENTRUM_DLA_FAKTURY_ID)){
			contextInstance.deleteVariable(CENTRUM_DLA_FAKTURY_ID);
		}
		boolean needed = false;
        CentrumKosztowDlaFaktury neededCkdf = null;
		for(CentrumKosztowDlaFaktury ckdf: CentrumKosztowDlaFaktury.findByDocumentId(doc.getId())){
			if(ckdf.getAcceptanceModeInfo() == null){
				//nie wybrano jeszcze trybu akceptacji - koniec sprawdzania
				log.error("IntercarsAcceptanceChecker brak wybranego trybu akceptacji: ckdf.getId() = "+ckdf.getId()+",doc.id = "+doc.getId()+", acceptanceCN = {} ", acceptanceCodeName);
				needed = true;
			} else {
				log.trace("ckdf.getAcceptanceModeInfo() = {}",ckdf.getAcceptanceModeInfo());
				needed = ckdf.getAcceptanceModeInfo().checkAcceptanceCondition(doc, acceptanceCodeName, ckdf, null);  //brak dekretacji
                if(needed){
                    neededCkdf = ckdf;
                }
			}

			if(neededCkdf != null){
                //poni�ej szukamy centrum, kt�re u�ytkownik mo�e zaakceptowa�
                contextInstance.createVariable(CENTRUM_DLA_FAKTURY_ID, neededCkdf.getId());
                //jezeli u�ytkownik mo�e akceptowa� to zwr�� to centrum w zmiennej CENTRUM_DLA_FAKTURY_ID, je�li nie
                //id� dalej (mo�e u�ytkownik mo�e akceptowa� inne centra), ale zwr�� true poprzez needed
                if("ksiegowa".equals(acceptanceCodeName) || "dyrektora_fin".equals(acceptanceCodeName)){
                    //dekretacja
                    neededCkdf.getAcceptanceModeInfo().checkAcceptanceCondition(doc, acceptanceCodeName, neededCkdf, activityId);
                    return true;
                }
                if(AcceptanceCondition.checkAcceptancePermission(
                            DSApi.context().getDSUser(),
                            neededCkdf.getAcceptingCentrumId(),
                            acceptanceCodeName)) {
				    log.trace("utworzenie zmiennej CENTRUM_DLA_FAKTURY_ID:{}", CENTRUM_DLA_FAKTURY_ID);
                    //dekretacja
                    neededCkdf.getAcceptanceModeInfo().checkAcceptanceCondition(doc, acceptanceCodeName, neededCkdf, activityId);
				    return true;
                }
			}
		}
        if (neededCkdf != null) {
            neededCkdf.getAcceptanceModeInfo().checkAcceptanceCondition(doc, acceptanceCodeName, neededCkdf, activityId);
        } else {
            log.trace("IntercarsAcceptanceChecker wszystkie warunki akceptacji spe�nione doc.id = {}, acceptanceCN = {} ",doc.getId(), acceptanceCodeName);
        }
		return neededCkdf != null;
	}
}
