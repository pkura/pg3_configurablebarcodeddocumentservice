package pl.compan.docusafe.core.dockinds.acceptances;

import java.util.HashMap;
import java.util.Map;
import org.jbpm.JbpmContext;
import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.exe.ProcessInstance;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.core.office.workflow.DSjBPM;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class LucasAcceptanceManager extends IntercarsAcceptanceManager{

	@Override
	public Long start(Document doc) {
		JbpmContext ctx = null;
		try {
			ctx = DSjBPM.context();

			ProcessInstance pi = ctx.newProcessInstance("AkceptacjeLucas");
			ContextInstance ci = pi.getContextInstance();

			ci.createVariable(DOC_ID, doc.getId());
			Map<String, Object> map = new HashMap<String,Object>();
			map.put(InvoiceICLogic.JBPM_PROCESS_ID, (Object)pi.getId());
			map.put(InvoiceICLogic.AKCEPTACJA_FINALNA_FIELD_CN, false);
			doc.getDocumentKind().setOnly(doc.getId(),map);
			pi.signal();

			ctx.save(pi);
			log.debug("poprawnie utworzono nowy proces jbpm");

			return pi.getId();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			return null;
		}
	}


}
