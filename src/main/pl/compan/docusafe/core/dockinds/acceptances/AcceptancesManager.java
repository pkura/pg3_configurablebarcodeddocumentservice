package pl.compan.docusafe.core.dockinds.acceptances;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionAmountException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.FinalAcceptance;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;

/**
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class AcceptancesManager
{
	 private final static StringManager sm = 
	        GlobalPreferences.loadPropertiesFile(AcceptancesManager.class.getPackage().getName(),null);
    /**
     * Wykonanie akceptacji (o zadanym kodzie) dla danego dokumentu.
     * @throws SQLException 
     * @throws HibernateException 
     */    
    public static void giveAcceptance(String cn, Long documentId) throws EdmException, DSException
    {
        Document document = Document.find(documentId);
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        document.getDocumentKind().initialize();

        if (!canAccept(cn, document))
            throw new DSException(sm.getString("NieMaszPrawaDoWykonaniaTejAkceptacji") + ":" + cn);
        
        if(document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND)) {
            FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        	Double invoiceAmount = (Double)fm.getKey(InvoiceLogic.KWOTA_BRUTTO_FIELD_CN);
        	Double maxAmount = AcceptanceCondition.getUserMaximumAmountForAcceptance(cn, null/*centrunId*/, DSApi.context().getPrincipalName());
        	if (!fm.getBoolean(InvoiceLogic.AKCEPTACJA_BEZ_KWOTY_CN) && maxAmount != null && maxAmount != 0 && invoiceAmount > maxAmount)
        		throw new DSException("Nie masz prawa do wykonania akceptacji. Maksymalna kwota kt�r� mo�esz zatwierdzi� to " + maxAmount.toString());
    	}
        
        if(cn.equalsIgnoreCase("uproszczona"))
        	giveSimpleAcceptance(document);
        
        DocumentAcceptance acceptance = new DocumentAcceptance();
        acceptance.setAcceptanceCn(cn);
        acceptance.setDocumentId(documentId);
        acceptance.setUsername(DSApi.context().getPrincipalName());
        acceptance.setAcceptanceTime(new Date());
        Persister.create(acceptance);
        
        DataMartEvent dme = new DataMartEvent(true, document.getId(), null,DataMartDefs.ACCEPTANCE_GIVE, cn ,null, null, null);
        //DataMartManager.storeEvent(dme);
        if (document instanceof OfficeDocument)
        {
            String tmp = sm.getString("DokonoanoAkceptacjiDokumentu",acceptance.getAcceptanceCn());
            ((OfficeDocument) document).addWorkHistoryEntry(
                Audit.create("giveAccept", DSApi.context().getPrincipalName(),
                    tmp,acceptance.getAcceptanceCn(), null));
        }
        
        checkFinalAcceptance(document);
    }
    
    
    /** 
     * Wykonanie akceptacji prostej na dokumencie
     * Akceptacja prosta dokonuje akceptacji wszystkich mozliwych akceptacji na centrach kosztowych faktury
     * @throws SQLException 
     * @throws HibernateException 
     * */
    @SuppressWarnings("unchecked")
	public static void giveSimpleAcceptance(Document document) throws EdmException, DSException
    {
    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    	fm.initialize();
    	if(!"tak".equalsIgnoreCase(fm.getValue(InvoiceLogic.AKCEPTACJA_PROSTA_CN).toString()))
    		throw new DSException(sm.getString("NieMoznaWykonacTejAkceptacji"));
    	
    	ArrayList<CentrumKosztowDlaFaktury> centraToAccept = new ArrayList<CentrumKosztowDlaFaktury>();
    	if(fm.getField(InvoiceLogic.CENTRUM_KOSZTOWE_FIELD_CN).isMultiple())
    	{
    		centraToAccept = (ArrayList<CentrumKosztowDlaFaktury>) fm.getValue(InvoiceLogic.CENTRUM_KOSZTOWE_FIELD_CN);
    	}
    	else
    	{
    		centraToAccept.add((CentrumKosztowDlaFaktury)fm.getValue(InvoiceLogic.CENTRUM_KOSZTOWE_FIELD_CN));
    	}
    	
    	ArrayList<String> acceptancesToDo = new ArrayList<String>();
		for(String str : fm.getAcceptancesDefinition().getAcceptances().keySet())
		{
			if(fm.getAcceptancesDefinition().getAcceptances().get(str).getFieldCn() != null)
			{
				acceptancesToDo.add(fm.getAcceptancesDefinition().getAcceptances().get(str).getCn());
			}                			
		}
    	
		Map<Integer,String> acceptances = new HashMap<Integer, String>();
		for(String str : acceptancesToDo)
		{
			for(CentrumKosztowDlaFaktury ckdf : centraToAccept)
			{
				acceptances.put(ckdf.getId().intValue(), str);
			}
			AcceptancesManager.giveCentrumAcceptance(acceptances, document.getId(), false);
			acceptances.clear();
		}
    }
    
    /**
     * Wykonanie wybranych akceptacji dla danego dokumentu i centr�w kosztowych (czyli wykonywanie
     * akceptacji �ci�le zwi�zanych z polem Field.CENTRUM_KOSZT).
     *
     * @param centrumAcceptanceCn  mapa (identyfikator centrumDlaFaktury, 'cn' akceptacji kt�r� wykona� da tego centrum)
     * @param documentId  identyfikator dokumentu
     * @throws EdmException 
     * @throws SQLException 
     * @throws HibernateException 
     */
    @Deprecated
    public static void giveCentrumAcceptance(Map<Integer,String> centrumAcceptanceCn, Long documentId) throws EdmException, DSException
    {
    	giveCentrumAcceptance(centrumAcceptanceCn,documentId,true);
    }
    
    /**
     * Wykonanie pojedynczej akceptacji danego kana�u dla wybranego dokumentu
     * @throws SQLException 
     * @throws HibernateException 
     */
    public static void giveCentrumAcceptance(Long centrumDlaFakturyId, String acceptanceCn, Document document, Boolean checkPermission) throws DSException, EdmException, HibernateException, SQLException{
    	giveCentrumAcceptance(Collections.singletonMap(centrumDlaFakturyId, acceptanceCn), document, checkPermission);
    }
    
    @Deprecated
    public static void giveCentrumAcceptance(Map<Integer,String> centrumAcceptanceCn, Long documentId, Boolean checkPermisions) throws EdmException, DSException{
    	Map<Long, String> properMap = new HashMap<Long, String>();
    	for(Integer i: centrumAcceptanceCn.keySet()){
    		properMap.put(Long.valueOf(i), centrumAcceptanceCn.get(i));
    	}
    	giveCentrumAcceptance(properMap, Document.find(documentId), checkPermisions);
    }
    
    private static void giveCentrumAcceptance(Map<Long,String> centrumAcceptanceCn, Document document, Boolean checkPermisions) throws EdmException, DSException
    {    	
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        document.getDocumentKind().initialize();

        for (Long objectId : centrumAcceptanceCn.keySet())
        {
            String cn = centrumAcceptanceCn.get(objectId);
            
            CentrumKosztowDlaFaktury centrumKosztowe = CentrumKosztowDlaFaktury.getInstance().find(objectId);
            Integer centrunId = centrumKosztowe.getAcceptingCentrumId();

            if (cn == null)
                continue;

           // if (!AcceptanceCondition.canCentrumAccept(objectId, cn, DSApi.context().getPrincipalName()))
            if(document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_IC_KIND))
            {
            	//if(!canCentrumAcceptIC(cn, document, objectId))
            //	if(checkPermisions && !canCentrumAccept(cn, document, objectId))
            //		throw new DSException("Nie masz prawa do wykonania akceptacji "+cn);
            	//List<Long> centraList = AcceptanceCondition.findByUserGuids();
            	try
            	{
	            	if(!AcceptanceCondition.canCentrumAccept(centrunId,cn))
	            		throw new DSException("Nie masz prawa do wykonania akceptacji "+cn);
            	}
				catch (HibernateException e)
				{
					throw new EdmException("");
				} catch (SQLException e) 
				{
					throw new EdmException("");
				}
            }
            else
            {
            	if(document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND)) {
                	Double maxAmount = AcceptanceCondition.getUserMaximumAmountForAcceptance(cn, null/*centrunId*/, DSApi.context().getPrincipalName());
                	 List<CentrumKosztowDlaFaktury> list = CentrumKosztowDlaFaktury.find(document.getId(), centrunId);
                	 BigDecimal fullCentrumAmount = new BigDecimal(0); 
                	 for (CentrumKosztowDlaFaktury faktury : list)
                	 {
                		 fullCentrumAmount = fullCentrumAmount.add(faktury.getRealAmount());
					 }
                	 
                	 FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
                	 //fm.getBoolean(InvoiceLogic.AKCEPTACJA_BEZ_KWOTY_CN)
                	 
                	 if (!fm.getBoolean(InvoiceLogic.AKCEPTACJA_BEZ_KWOTY_CN) && maxAmount != null && maxAmount != 0 && fullCentrumAmount.doubleValue() > maxAmount)
                		throw new PermissionAmountException("Nie masz prawa do wykonania akceptacji. Maksymalna kwota kt�r� mo�esz zatwierdzi� to " + maxAmount.toString());
            	}
            	if(checkPermisions && !canCentrumAccept(cn, document, objectId))
            		throw new DSException("Nie masz prawa do wykonania akceptacji "+cn);
            }
            
            DocumentAcceptance acceptance = new DocumentAcceptance();
            acceptance.setAcceptanceCn(cn);
            acceptance.setDocumentId(document.getId());
            acceptance.setUsername(DSApi.context().getPrincipalName());
            acceptance.setAcceptanceTime(new Date());
            acceptance.setObjectId(objectId);
            Persister.create(acceptance);
            DataMartEvent dme = new DataMartEvent(true, document.getId(), null,DataMartDefs.ACCEPTANCE_GIVE_CENTRUM, cn ,null, null, null);
            DataMartManager.storeEvent(dme);

            if (document instanceof OfficeDocument)
            {
                CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance().find(objectId);
                String tmp = sm.getString("DokonoanoAkceptacjiDokumentuDlaCentrumKosztowego",acceptance.getAcceptanceCn(),centrum.getCentrumCode());
                ((OfficeDocument) document).addWorkHistoryEntry(
                    Audit.create("giveAccept", DSApi.context().getPrincipalName(),
                        tmp,acceptance.getAcceptanceCn(), null));
            }
        }        
        checkFinalAcceptance(document);
    }

	 /**
     * Wycofanie akceptacji og�lnej dla dokumentu wykonanej przez danego u�ytkownika. Z regu�y powinna by� max. jedna akceptacja,
     * ale je�li jest wi�cej usuwam wszystkie. Po odrzuceniu nie dekretuje na odpowiedniego u�ytkownika.
     */
	public static Boolean withdrawAcceptance(Long documentId, String acceptanceCn) throws EdmException {
		return withdrawAcceptanceAndAssign(Document.find(documentId), acceptanceCn, null);
	}

    /**
     * Wycofanie akceptacji og�lnej dla dokumentu wykonanej przez danego u�ytkownika. Z regu�y powinna by� max. jedna akceptacja,
     * ale je�li jest wi�cej usuwam wszystkie.
	 * @param activityId je�li != null i dokument jest w procesie akceptacji JBPM to b�dzie zadekretowany do odpowiedniego dzia�u
     */
    public static Boolean withdrawAcceptanceAndAssign(Document document, String acceptanceCn, String activityId) throws EdmException
    {
    	Boolean isReturn = false;
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        document.getDocumentKind().initialize();

        FieldsManager fm = document.getFieldsManager();
        if(document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_IC_KIND)
			&& fm.getBoolean(InvoiceICLogic.AKCEPTACJA_FINALNA_FIELD_CN)){
			throw new EdmException("Nie mo�na odrzuci� akceptacji po zako�czeniu procesu akceptacji");
		}

        List<DocumentAcceptance> acceptances = DocumentAcceptance.find(document.getId(), DSApi.context().getPrincipalName(), acceptanceCn, null);
        for (DocumentAcceptance acceptance : acceptances)
        {
        	isReturn = true;
            Persister.delete(acceptance);
            
            DataMartEvent dme = new DataMartEvent(true, document.getId(), null,DataMartDefs.ACCEPTANCE_WITHDRAW, acceptance.getAcceptanceCn() ,null, null, null);
            //DataMartManager.storeEvent(dme);
            if (document instanceof OfficeDocument)
            {
                String tmp = sm.getString("CofnietoAkceptacjeDokumenu",acceptance.getAcceptanceCn());
                ((OfficeDocument) document).addWorkHistoryEntry(
                    Audit.create("withdrawAccept", DSApi.context().getPrincipalName(),
                        tmp,acceptance.getAcceptanceCn(), null));
            }
        }

        //document.getDocumentKind().getAcceptancesDefinition().setGivenAcceptanceId(null);

		//JBPM
		if(document.getDocumentKind().logic().getAcceptanceManager() != null){
			document.getDocumentKind().logic().getAcceptanceManager().refresh(document,fm, null);
		}
        checkFinalAcceptance(document);
        return isReturn;
    }

	 /**
     * Wycofanie akceptacji dla dokumentu (zwi�zanej z danym centrum) wykonanej przez danego u�ytkownika.
     * Z regu�y powinna by� max. jedna akceptacja, ale je�li jest wi�cej usuwam wszystkie.
     * @param documentId
     * @param objectId  identyfikator obiektu, dla kt�rego wycofa� akceptacj� (obecnie zawsze jest to obiekt
     *        klasy {@link CentrumKosztowDlaFaktury})
     */
	 public static Boolean withdrawCentrumAcceptance(Long documentId, Long objectId) throws EdmException{
		 return withdrawCentrumAcceptanceAndAssign(Document.find(documentId), objectId, null);
	 }

    /**
     * Wycofanie akceptacji dla dokumentu (zwi�zanej z danym centrum) wykonanej przez danego u�ytkownika.
     * Z regu�y powinna by� max. jedna akceptacja, ale je�li jest wi�cej usuwam wszystkie.
     * @param document
     * @param objectId  identyfikator obiektu, dla kt�rego wycofa� akceptacj� (obecnie zawsze jest to obiekt
     *        klasy {@link CentrumKosztowDlaFaktury})
	 * @param activityId activityId je�li != null i dokument jest w procesie akceptacji JBPM to b�dzie zadekretowany do odpowiedniego dzia�u
     */
    public static Boolean withdrawCentrumAcceptanceAndAssign(Document document, Long objectId, String activityId) throws EdmException
    {
		if(document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_IC_KIND)
			&& document.getFieldsManager().getBoolean(InvoiceICLogic.AKCEPTACJA_FINALNA_FIELD_CN)){
			throw new EdmException("Nie mo�na odrzuci� akceptacji po zako�czeniu procesu akceptacji");
		}

    	Boolean isReturn = false;
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        document.getDocumentKind().initialize();

        // TODO: moze zmienic (w przypadku gdy nie ma zasady drugiej pary oczu), ze mozna wycofac oddzielnie akceptacje X dla centrum i
        //         oddzielnie akceptacje Y dla tego samego centrum
        List<DocumentAcceptance> acceptances = DocumentAcceptance.find(document.getId(), DSApi.context().getPrincipalName(), null, objectId);
        for (DocumentAcceptance acceptance : acceptances)
        {
        	isReturn = true;

            Persister.delete(acceptance);
            DataMartEvent dme = new DataMartEvent(true, document.getId(), null,DataMartDefs.ACCEPTANCE_WITHDRAW_CENTRUM, acceptance.getAcceptanceCn() ,null, null, null);
            DataMartManager.storeEvent(dme);
            if (document instanceof OfficeDocument)
            {
                CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance().find(objectId);
                String tmp = sm.getString("CofnietoAkceptacjeDokumenuDlaCentrumKosztowego",acceptance.getAcceptanceCn(),centrum.getCentrumCode());
                ((OfficeDocument) document).addWorkHistoryEntry(
                    Audit.create("withdrawAccept", DSApi.context().getPrincipalName(),
                        tmp,acceptance.getAcceptanceCn(), null));
            }
        }
        checkFinalAcceptance(document);

		//JBPM
		if(document.getDocumentKind().logic().getAcceptanceManager() != null){
			document.getDocumentKind().logic().getAcceptanceManager().refresh(document,document.getFieldsManager(), null);
		}

		checkFinalAcceptance(document);
        return isReturn;
    }

    /**
     * Sprowadza czy aktualnie zalogowany u�ytkownik ma prawo do dokonania og�lnej akceptacji danego typu
     * na danym dokumencie.
     *
     * @param cn  'cn' akceptacji
     * @param document
     */
    private static boolean canAccept(String cn, Document document) throws EdmException
    {
        DocumentKind documentKind = document.getDocumentKind();
        documentKind.initialize();
        AcceptancesState state = new AcceptancesState(document.getId(), documentKind.getAcceptancesDefinition(), documentKind.getFieldsManager(document.getId()));
        state.initialize();
        
        return (state.getGeneralAcceptancesDef().get(cn) != null);
    }

    /**
     * Sprowadza czy aktualnie zalogowany u�ytkownik ma prawo do dokonania og�lnej akceptacji danego typu
     * na danym dokumencie.
     *
     * @param acceptanceCn  'cn' akceptacji
     * @param document
     * @param objectId
     */
    private static boolean canCentrumAccept(String acceptanceCn, Document document, Long objectId) throws EdmException
    {
        DocumentKind documentKind = document.getDocumentKind();
        documentKind.initialize();
        //CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.find(objectId);
        AcceptancesState state = new AcceptancesState(document.getId(), documentKind.getAcceptancesDefinition(), documentKind.getFieldsManager(document.getId()));
        state.initialize();
        Map<String,Acceptance> tmp = state.getFieldAcceptancesDef(documentKind.getAcceptancesDefinition().getCentrumKosztFieldCn(), objectId);
        Set<String> tmp2 = tmp.keySet();

        return (state.getFieldAcceptancesDef(documentKind.getAcceptancesDefinition().getCentrumKosztFieldCn(), objectId).get(acceptanceCn) != null);
    }

    private static boolean canCentrumAcceptIC(String acceptanceCn, Document document, Long objectId) throws EdmException
    {
        if(AcceptanceCondition.canCentrumAccept(objectId, acceptanceCn, DSApi.context().getPrincipalName()))
        	return true;
        else
        {
        	DSUser user = DSUser.findByUsername(DSApi.context().getPrincipalName());
        	DSDivision[] divisions = user.getDivisions();
        	for (int i = 0; i < divisions.length; i++) 
        	{
        		if(AcceptanceCondition.canCentrumAcceptByDivision(objectId, acceptanceCn, divisions[i].getGuid()))
        		{
        			return true;
        		}
			}
        }
		return false;
    }
    
    
    /**
     * Sprawdza uprawnienie do danej akceptacji dla aktualnie zalogowanego u�ytkownika. Zwracana warto�� true,
     * gdy u�ytkownik ma zawsze prawo to pole akceptowa�. Natomiast w przeciwnym przypadku do listy 'requiredValues'
     * dodawane s� warto�ci, kt�re musi posiada� pole steruj�ce akceptacj�, aby ten u�ytkownik m�g� wykona� t� akceptacj�.
     *
     * UWAGA: s� tu sprawdzane tylko warunki wynikaj�ce z definicji akceptacji, nie jest sprawdzane czy ju�
     *        kto� wykona� akceptacj� tego typu lub czy spe�niona jest zasada drugiej pary oczu
     *
     * @param acceptanceCn
     * @param requiredValues
     * @return
     * @throws EdmException
     */
    public static boolean checkAcceptance(String acceptanceCn, List<Object> requiredValues) throws EdmException
    {
        List<AcceptanceCondition> conditions = AcceptanceCondition.find(acceptanceCn);
        for (AcceptanceCondition condition : conditions)
        {
            if (DSApi.context().getPrincipalName().equals(condition.getUsername()))
            {
                // warunek na zalogowanego u�ytkownika
            	boolean userTest = false;
            	try
            	{
            		userTest = DSUser.findByUsername(condition.getFieldValue()) != null;
            	}
            	catch (Exception e) 
            	{
					userTest = false;
				}
            	
                if (condition.getFieldValue() == null)
                {
                    return true;
                }
                else if(userTest)
                {
                	return true;
                }
                else
                {
                    requiredValues.add(condition.getFieldValue());
                }   
            	//return true;
            }
            else if (condition.getDivisionGuid() != null)
            {
                // warunek na ca�y jeden dzia�
                DSDivision division = DSDivision.find(condition.getDivisionGuid());
                if(DSDivision.isInDivision(division))
               // if (DSApi.context().getDSUser().inDivision(division))
                {
                    if (condition.getFieldValue() == null)
                        return true;
                    else
                        requiredValues.add(condition.getFieldValue());
                }
            }
            else if (condition.getUsername() == null)
            {
                // warunek na wszystkich
                if (condition.getFieldValue() == null)
                    return true;
                else
                    requiredValues.add(condition.getFieldValue());
            }
        }
        return false;
    }

    /**
     * Sprawdza czy dokument jest ju� "finalnie zaakceptowany" por�wnuj�c istniej�ca akceptacje dokumentu
     * z definicj� finalnej akceptacji dla rodzaju podanego dokumentu.
     *
     * @param documentId
     * @throws EdmException
     */    
    public static void checkFinalAcceptance(Document document) throws EdmException {
        {
			AcceptancesDefinition accDefinition = 
				document.getDocumentKind().getAcceptancesDefinition();
			
			if (accDefinition == null){
				return;
			}
			// throw new
			// EdmException("Dla tego rodzaju dokumentu nie podano definicji akceptacji");

			FinalAcceptance finalAcceptance = accDefinition.getFinalAcceptance();

			// nie okre�lono poprawnej akceptacji finalnej dla danego rodzaju
			// dokumentu - nic nie robimy
			if (finalAcceptance == null || finalAcceptance.getFieldCn() == null)
				return;

			Map<String, Object> fieldValues = new LinkedHashMap<String, Object>();
			fieldValues.put(finalAcceptance.getFieldCn(), Boolean
					.valueOf(isFinalAccepted(document)));
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
		} 
    }

    /**
     * Na podstawie wszystkich akceptacji danego dokumentu zwraca warto�� logiczn� oznaczaj�c�
     * czy dany dokument jest w tym momencie zaakceptowany finalnie.
     *  
     */
    @SuppressWarnings("unchecked")
    private static boolean isFinalAccepted(Document document) throws EdmException        
    {    	
        DocumentKind documentKind = document.getDocumentKind();
        AcceptancesDefinition acceptancesDefinition = documentKind.getAcceptancesDefinition();
        FinalAcceptance finalAcceptance = acceptancesDefinition.getFinalAcceptance();
        FieldsManager fm = documentKind.getFieldsManager(document.getId());
        
        //if(fm.getBoolean(finalAcceptance.getFieldCn())){
        //	return true;
        //}

        // sprawdzam, kt�re akceptacje zosta�y wykonane ca�kowicie (czyli dla akceptacji zwi�zanych
        // z centrami koszt�w musi by� dokonana akceptacja dla ka�dego centrum oddzielnie)
        Set<String> givenAcceptances = new LinkedHashSet<String>();
        for (String acceptanceCn : acceptancesDefinition.getAcceptances().keySet())
        {
            Acceptance acceptance = acceptancesDefinition.getAcceptances().get(acceptanceCn);
            if (acceptance.getFieldCn() != null)
            {
                // akceptacja zwi�zana z centrami koszt�w
                boolean ok = true;
                List<Long> ids = (List<Long>) fm.getKey(acceptance.getFieldCn());
                if(ids!=null && ids.size()>0)
                {
                    for (Long objectId : ids)
                    {
                        if (DocumentAcceptance.find(document.getId(), null, acceptanceCn, objectId).size() == 0)
                        {
                            ok = false;
                            break;
                            }
                    }
                }
                if (ok)
                    givenAcceptances.add(acceptanceCn);
            }
            else
            {
                // akceptacja og�lna
                if (DocumentAcceptance.find(document.getId(), null, acceptanceCn, null).size() > 0)
                    givenAcceptances.add(acceptanceCn);
            }
        }

        // ilo�� r�nych akceptacji
        if (finalAcceptance.getMinDifferentAcceptances() > 0)
        {
            if (givenAcceptances.size() < finalAcceptance.getMinDifferentAcceptances())
                return false;
        }

        // sprawdzanie czy wykonano wymagane akceptacje
        
        
        
        if (finalAcceptance.getRequiredAcceptances() != null)
        {
            for (String acceptanceCn : finalAcceptance.getRequiredAcceptances())
            {   
            	if (!givenAcceptances.contains(acceptanceCn))
                    return false;
            }
        }

        // sprawdzanie czy suma kwot w wybranych centrach r�wna si� kwocie z podanego pola
        if (finalAcceptance.getExactAmountFieldCn() != null)
        {
            Object object = fm.getKey(finalAcceptance.getExactAmountFieldCn());
            if (object == null)
                return false;
            if (!(object instanceof Double))
                throw new EdmException("Napotkano b��d podczas sprawdzania akceptacji finalnej : pole "+finalAcceptance.getExactAmountFieldCn()+" powinno by� typu Double");
            Double fullAmount = (Double) object;
            List<Object> list = (List<Object>) fm.getValue(acceptancesDefinition.getCentrumKosztFieldCn());

            Double tmpAmount = new Double(0);
            for (Object o : list)
            {
                Object amount = fm.getField(acceptancesDefinition.getCentrumKosztFieldCn()).invokeMethod("getAmount", "getAmount", o);
                if (amount instanceof Double)
                    tmpAmount = tmpAmount + (Double) amount;
                else if (amount != null)
                    throw new IllegalStateException("Wynik funkcji 'getAmount jest innego typu ni� 'Float'");
            }
            
            if(Math.abs(tmpAmount-fullAmount)>0.1f)
                return false;
            //if (!tmpAmount.equals(fullAmount))
              //  return false;
        }

        return true;
    }
    
}
