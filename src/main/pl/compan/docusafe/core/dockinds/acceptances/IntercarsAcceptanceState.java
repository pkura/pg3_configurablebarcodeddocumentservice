package pl.compan.docusafe.core.dockinds.acceptances;
import java.util.List;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public class IntercarsAcceptanceState {

	private final static Logger log = LoggerFactory.getLogger(IntercarsAcceptanceState.class);

	public IntercarsAcceptanceState(org.jbpm.graph.exe.ProcessInstance jbpmProcess){
		
	}

	List<List<DocumentAcceptance>> getCentrumAcceptances(){
		return null;
	}

	List<DocumentAcceptance> getLocalAcceptances(){
		return null;
	}

	public String getProcessName() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public Object getId() {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
