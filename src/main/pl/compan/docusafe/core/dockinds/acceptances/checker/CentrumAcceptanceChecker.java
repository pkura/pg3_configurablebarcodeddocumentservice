package pl.compan.docusafe.core.dockinds.acceptances.checker;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.acceptances.NoAcceptanceConditionDefined;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import static java.lang.String.format;

/**
 * Sprawdza akceptacj� bazuj�c� na centrum kosztowym  (na razie intercars)
 * @author MSankowski 
 */
public class CentrumAcceptanceChecker implements ChannelAcceptanceChecker{		
	private static final Logger log = LoggerFactory.getLogger(CentrumAcceptanceChecker.class);
	private final static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(CentrumAcceptanceChecker.class.getPackage().getName(),null);

	public boolean checkAcceptanceCondition(Document doc,
			String cn, CentrumKosztowDlaFaktury ckdf, String activityId) throws EdmException {
		log.trace(format("CentrumAcceptanceChecker#checkAcceptanceCondition(doc.id = %d, cn = %s, centrumId = %s)",doc.getId(), cn, ckdf));
		
		if (DocumentAcceptance.find(doc.getId(), cn, ckdf.getId())!=null){
			return false;
		} else {
			Integer centrumId = ckdf.getAcceptingCentrumId();
			
			List<AcceptanceCondition> acs = AcceptanceCondition.find(
                    cn, centrumId.toString());
								
			if (acs.size() < 1) {
                CentrumKosztow ck = CentrumKosztow.find(centrumId);
				throw new NoAcceptanceConditionDefined(sm.getString(
						"BrakZdefiniowanejAkceptacji", cn,
						ck.getCn()));
			}
			
			if(activityId != null){
				acs.get(0).makeAssignmentForAcceptance(activityId);

				log.trace("CentrumAcceptanceChecker dekretacja na {}",acs.get(0).getDivisionGuid());
			}
			
			return true;
		}
	}

}
