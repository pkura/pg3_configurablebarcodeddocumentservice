package pl.compan.docusafe.core.dockinds.acceptances.checker;
import java.util.List;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.NoAcceptanceConditionDefined;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 *
 * @author Micha� Sankowski
 */
public class ProductManagerChecker extends CentrumAcceptanceChecker {

	private final static Logger log = LoggerFactory.getLogger(ProductManagerChecker.class);
	private final static StringManager sm =
        GlobalPreferences.loadPropertiesFile(CentrumAcceptanceChecker.class.getPackage().getName(),null);


	@Override
	public boolean checkAcceptanceCondition(Document doc, String cn, CentrumKosztowDlaFaktury ckdf, String activityId) throws EdmException {
		boolean ret = super.checkAcceptanceCondition(doc, cn, ckdf, null);
		if(ret){
			Integer centrumId = ckdf.getAcceptingCentrumId();
			List<AcceptanceCondition> acs = AcceptanceCondition.find(
					cn + "_disp", centrumId.toString());

			if (acs.size() < 1) {
                CentrumKosztow ck = CentrumKosztow.find(centrumId);
				throw new NoAcceptanceConditionDefined(sm.getString(
						"BrakZdefiniowanejAkceptacji", cn + "_disp",
						ck.getCn()));
			}

			if(activityId != null){
				WorkflowFactory.simpleAssignment(activityId,
					acs.get(0).getDivisionGuid(),
					acs.get(0).getUsername(),
					DSApi.context().getPrincipalName(),
					null,
					acs.get(0).getDivisionGuid() != null ? WorkflowFactory.SCOPE_DIVISION : WorkflowFactory.SCOPE_ONEUSER,
					null,
					null);

				log.trace("ProductManagerChecker dekretacja na {}",acs.get(0).getDivisionGuid());
			}
		}
		return ret;
	}
}
