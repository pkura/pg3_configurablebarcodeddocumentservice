package pl.compan.docusafe.core.dockinds.acceptances;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;

/**
 * Opisuje jedn� wykonan� akceptacj� na danym dokumencie.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class DocumentAcceptance implements Serializable
{
	private static final Log log = LogFactory.getLog(DocumentAcceptance.class);
    private Long id;
    private String username;
    private Date acceptanceTime;

    /** okre�la rodzaj akceptacji */
    private String acceptanceCn;
    private Long documentId;
	private boolean automatic;


	//nie zapisywane w bazie danych
    transient private String asFirstnameLastname;

    /** 
     * opis akceptacji - nie jest zapisywany do bazy, ale wyliczany r�cznie (w metodzie 
     * {@link #initDescription()} z pozosta�ych danych 
     */
    private String description;
    /**
     * opis akceptacji (bez informacji o nazwie akceptacji) wyliczany r�cznie 
     */
    private String descriptionWithoutCn;
    /**
     * Identyfiktor obiektu (warto�ci pola, od kt�rego zale�y akceptacja 'acceptanceCn') - przewa�nie
     * jest to obiekt typu {@link CentrumKosztowDlaFaktury}. Mo�e b� null-em.
     */
    private Long objectId;

	/**
	 * Zwraca informacje czy dla danego dokumentu zosta�y wykonane dowolne akceptacje
	 * @param documentId identyfikator dokumentu
	 * @return true - je�li istnieje przynajmniej jedna akceptacja dla danego dokumentu
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	public static boolean hasAcceptance(Long documentId) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(DocumentAcceptance.class);
			if (documentId != null) {
				criteria.add(Restrictions.eq("documentId", documentId));
			} else {
				throw new IllegalArgumentException("documentId cannot be null");
			}
			criteria.setMaxResults(1);

			return !criteria.list().isEmpty();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	/**
	 * Tworzy nowy obiekt typu DocumentAcceptance z minimalnym zestawem danych (nie zapisuje go w bazie danych).
	 * Jako czas akceptacji ustawia moment utworzenia obiektu.
	 * @param documentId
	 * @param username
	 * @param acceptanceCn
	 * @param objectId mo�e by� null
	 * @return
	 */
	public static DocumentAcceptance create(long documentId, String username, String acceptanceCn, Long objectId){
		DocumentAcceptance ret = new DocumentAcceptance();
		ret.setDocumentId(documentId);
		ret.setObjectId(objectId);
		ret.setUsername(username);
		ret.setAcceptanceCn(acceptanceCn);
		ret.setAcceptanceTime(DateUtils.getCurrentTime());

		return ret;
	}

	/**
	 * Tworzy obiekt typu DocumentAcceptance i zapisuje go w bazie danych
	 * @param documentId
	 * @param username
	 * @param acceptanceCn
	 * @param objectId
	 * @return
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	public static DocumentAcceptance createAndSave(Document document, String username, String acceptanceCn, Long objectId) throws EdmException{
		DocumentAcceptance ret = create(document.getId(), username, acceptanceCn, objectId);
		DSApi.context().session().save(ret);
		return ret;
	}

    /**
     * Wyszukiwanie akceptacji spe�niaj�ce podane warunki. Gdy jako dany parametr przekazano null,
     * nie jest on wog�le dodawany do warunk�w wyszukiwania. 
     * 
     * @param documentId   identyfikator dokumentu
     * @param username     login u�ytkownika, kt�ry dokona� akceptacji
     * @param acceptanceCn identyfikator ('cn') rodzaju akceptacji
     * @param objectId     identyfiaktor obiektu, dla kt�rego dokonano akceptacji
     */
    @SuppressWarnings("unchecked")
    public static List<DocumentAcceptance> find(Long documentId, String username, String acceptanceCn, Long objectId) throws EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(DocumentAcceptance.class);
            if (documentId != null)
                criteria.add(Expression.eq("documentId", documentId));
            if (username != null)
                criteria.add(Expression.eq("username", username)); 
            if (acceptanceCn != null)
                criteria.add(Expression.eq("acceptanceCn", acceptanceCn));
            if (objectId != null)
                criteria.add(Expression.eq("objectId", objectId));
            
            List<DocumentAcceptance> list = criteria.list();
            
            return list;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * 
     * @param documentId
     * @param cn
     * @param objectId
     * @return
     * @throws EdmException
     */
    public static DocumentAcceptance find(Long documentId, String cn, Long objectId) throws EdmException{
    	Criteria criteria = DSApi.context().session().createCriteria(DocumentAcceptance.class);
    	criteria.add(Expression.eq("documentId", documentId));
    	criteria.add(Expression.eq("acceptanceCn", cn));
    	if (objectId != null){
    		criteria.add(Expression.eq("objectId", objectId));
    	} else {
			criteria.add(Expression.isNull("objectId"));
		}
    	// tu sie pierniczylo - taki zestaw wynikow nie zawsze zwraca UniqueResult
    	//Generalnie jesli jest wiecej niz jedna akceptacja tego typu zwracamy pierwsza    	
    	List results = criteria.list();
    	if (results.size()==0)
    		return null;
    	if (results.size()>1)
    		log.warn("Podwojna akceptacja "+cn+" dla dokumentu id="+documentId + " centrum="+objectId);
    	return (DocumentAcceptance) results.get(0);
    }
    
    /**
     * Wyszukiwanie akceptacji spe�niaj�ce podane warunki.
     * 
     * @param documentId identyfikator dokumentu
     * @param general true => wyszukujemy akceptacji og�lnych; false => szukamy akceptacji zale�nych od p�l
     */
    @SuppressWarnings("unchecked")
    public static List<DocumentAcceptance> find(Long documentId, boolean general) throws EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(DocumentAcceptance.class);
            criteria.add(Expression.eq("documentId", documentId));
            if (general)
                criteria.add(Expression.isNull("objectId"));
            else
                criteria.add(Expression.isNotNull("objectId"));
            
            List<DocumentAcceptance> list = criteria.list();
            
            return list;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }   
    
    /**
     * Wyszukiwanie akceptacji do dokumenmtu.
     * 
     * @param documentId identyfikator dokumentu
     */
    @SuppressWarnings("unchecked")
    public static List<DocumentAcceptance> find(Long documentId) throws EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(DocumentAcceptance.class);
            criteria.add(Expression.eq("documentId", documentId)).addOrder(Order.desc("id"));            
            List<DocumentAcceptance> list = criteria.list();
            
            return list;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static List<DocumentAcceptance> find(Long documentId, String acceptanceCn) throws EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(DocumentAcceptance.class);
            criteria.add(Expression.eq("documentId", documentId)).addOrder(Order.desc("id"));
            criteria.add(Expression.eq("acceptanceCn", acceptanceCn));
            List<DocumentAcceptance> list = criteria.list();
            
            return list;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static DocumentAcceptance findById(Long id) throws EdmException
    {
    	try
    	{
    		Criteria criteria = DSApi.context().session().createCriteria(DocumentAcceptance.class);
    		criteria.add(Expression.eq("id", id)).addOrder(Order.desc("id"));
    		List<DocumentAcceptance> list = criteria.list();
    		
    		if (list != null && list.size()>0) {
    			return list.get(0);
    		}
    		
    		return null;
    	}
    	catch (HibernateException e)
    	{
    		throw new EdmHibernateException(e);
    	}
    }
    
    public void initDescription() throws EdmException
    {
		if (automatic) {
			asFirstnameLastname = "<auto>";
		} else {
			asFirstnameLastname = DSUser.safeToFirstnameLastname(username);
		}
		descriptionWithoutCn = asFirstnameLastname + " " + DateUtils.formatJsDateTime(acceptanceTime);
		description = descriptionWithoutCn + " (" + acceptanceCn + ")";
    }
    
    public String getDescription()
    {
        return description;
    }

    public String getDescriptionWithoutCn()
    {
        return descriptionWithoutCn;
    }

    public String getAcceptanceCn()
    {
        return acceptanceCn;
    }

    public void setAcceptanceCn(String acceptanceCn)
    {
        this.acceptanceCn = acceptanceCn;
    }

    public Date getAcceptanceTime()
    {
        return acceptanceTime;
    }

    public void setAcceptanceTime(Date acceptanceTime)
    {
        this.acceptanceTime = acceptanceTime;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Long getObjectId()
    {
        return objectId;
    }

    public void setObjectId(Long objectId)
    {
        this.objectId = objectId;
    }

    public String getAsFirstnameLastname() {
            return asFirstnameLastname;
    }

    public boolean isAutomatic() {
            return automatic;
    }

    public void setAutomatic(boolean automatic) {
            this.automatic = automatic;
            if(automatic){
                    this.username = "";
            }
    }
    
    /**
     * Zwraca ilo�� akceptacji
     * @param id
     * @return 
     */
    public static int count(Long id) throws EdmException
    {
        List<DocumentAcceptance> acceptances = find(id);
        
        if(acceptances.isEmpty())
            return 0;
        
        return acceptances.size();
    }
    
	public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	
}
