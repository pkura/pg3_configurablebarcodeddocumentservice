package pl.compan.docusafe.core.dockinds.acceptances.checker;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.acceptances.NoAcceptanceConditionDefined;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * Klasa sprawdzaj�ca akceptacj�, ktor� mo�e przeprowadzi� jednen dzia� w ca�ej organizacji
 * @author MSankowski 
 */
public class GlobalAcceptanceChecker implements ChannelAcceptanceChecker{
	
	private final static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(CentrumAcceptanceChecker.class.getPackage().getName(),null);

	public boolean checkAcceptanceCondition(Document doc,
			String cn, CentrumKosztowDlaFaktury ckdf, String activityId) throws EdmException {

		if (DocumentAcceptance.find(doc.getId(), cn, null) != null) {
			return false;
		} else {
			List<AcceptanceCondition> acs = AcceptanceCondition
					.find(cn);

			if (acs.size() < 1) {
				throw new NoAcceptanceConditionDefined(sm.getString(
						"BrakZdefiniowanejGlobalnejAkceptacji",
                        cn));
			} else if(activityId != null) {
				WorkflowFactory.simpleAssignment(activityId,
						acs.get(0).getDivisionGuid(), 
						acs.get(0).getUsername(), 
						DSApi.context().getPrincipalName(), 
						null, 
						acs.get(0).getDivisionGuid() != null ? WorkflowFactory.SCOPE_DIVISION : WorkflowFactory.SCOPE_ONEUSER, 
						null, 
						null);
			}
			return true;
		}
	}
}
