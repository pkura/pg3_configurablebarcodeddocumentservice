package pl.compan.docusafe.core.dockinds.acceptances;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Definiuje system akceptacji dla dokument�w danego rodzaju (dla ka�dego 'dockind-a' AcceptancesDefinition
 * definiowane jest niezale�nie).
 * 
 * Idea akceptacji: Dla ka�dego rodzaju dokumentu mo�na zdefiniowa� dowoln� ilo�� akceptacji, kt�re na dokumentach mog� 
 * wykonywa� u�ytkownicy. Akceptacje dzielimy na dwa rodzaje: akceptacje og�lne i akceptacje zale�ne od 
 * pola biznesowego (obecnie mo�liwe s� tylko akceptacje zale�ne od pola centrum-koszt i s�
 * nazywane "akceptacjami centrum kosztowego"). Akceptacja og�lna dotyczy jakby ca�ego dokumentu i mo�e 
 * wykona� j� u�ytkownik je�li ma prawo wykonywa� takie akceptacje, natomiast akceptacja centrum 
 * kosztowego dotyczy jednego centrum kosztowego wybranego dla dokumentu i mo�e j� wykona� u�ytkownik, 
 * kt�ry ma prawo wykonywa� takie akceptacje dok�adnie dla tego centrum. Ka�d� wykonan� akceptacj� mo�na
 * wycofa�, ale mo�e to zrobi� tylko autor tej akceptacji.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class AcceptancesDefinition implements Serializable
{
    /** mapa wszystkich okre�lonych akceptacji dla tego pola */
    private Map<String,Acceptance> acceptances = new LinkedHashMap<String, Acceptance>();
    /** mapa og�lnych akceptacji */
    private Map<String,Acceptance> generalAcceptances = new LinkedHashMap<String, Acceptance>();
    /** 
     * mapa akceptacji zale�nych od jakiego� pola 
     * (<- OBECNIE ZAK�ADAM �E S� TO TYLKO AKCEPTACJE ZALEZNE OD JEDNEGO POLA TYPU Field.CENTRUM_KOSZT ->)
     */
    private Map<String,Acceptance> fieldAcceptances = new LinkedHashMap<String, Acceptance>();
    /** 'cn' pola Field.CENTRUM_KOSZT, z kt�rym zwi�zane s� niekt�re akceptacje */
    private String centrumKosztFieldCn;
    /** 
     * true <=> wy��czona zasada drugiej pary oczu (czyli jedna osoba mo�e dokona� dw�ch r�nych akceptacji).
     * Dla akceptacji zale�nych od jakiego� pola (centrum koszt�w) ta zasada (gdy w��czona) stosuje si� nast�puj�co:
     * mo�na akceptowa� ka�de centrum inn� akceptacj�, ale dla jednego centrum mo�na da� tylko akceptacj� jednego typu. 
     */
    private boolean givingDifferentAcceptancesEnabled;
    /** definicja finalnej akceptacji */
    private FinalAcceptance finalAcceptance;
    
    private Map<Integer,Acceptance> levelAcceptances = null;
    private Integer maxLevel = null;
    
    
    public int getMaxLevel(){
    	if(maxLevel == null){
    		createLevelAcceptances();
    	}
    	return maxLevel;
    }
    
    /**
     * Zwraca map� poziom akceptacji -> akceptacja, nie u�ywa� w �rodowsku wielow�tkowym
     */
    public Map<Integer,Acceptance> getLevelAcceptances(){
    	if(levelAcceptances == null){
    		createLevelAcceptances();
    	}
    	return Collections.unmodifiableMap(levelAcceptances);
    }
    
    /**
	 * Zwraca list� akceptacji posortowan� po levelu
	 */
	public List<Acceptance> getLevelAcceptancesAsList()
	{
		if (levelAcceptances == null)
		{
			createLevelAcceptances();
		}
		LinkedList<Acceptance> list = new LinkedList<Acceptance>(levelAcceptances.values());
		Collections.sort(list);
		return list;
	}
    
    /**
     * Zwraca kod akceptacji o nizszym poziomie lub null gdy akceptacja ma najni�szy poziom 
     */
    public String getPrevCn(String cn){
    	if(levelAcceptances == null){
    		createLevelAcceptances();
    	}
    	Acceptance ac = acceptances.get(cn);
    	if(ac == null){
    		return null;
    	} else {
    		return levelAcceptances.get(ac.getLevel() - 1) != null ? levelAcceptances.get(ac.getLevel() - 1).getCn():null;
    	}
    }
    
    /**
     * Zwraca kod akceptacji o wy�szym poziomie lub null gdy akceptacja ma najwy�szy poziom
     */
    public String getNextCn(String cn){
    	if(levelAcceptances == null){
    		createLevelAcceptances();
    	}
    	Acceptance ac = acceptances.get(cn);
    	if(ac == null){
    		return null;
    	} else {    		
    		return levelAcceptances.get(ac.getLevel() + 1) != null ? levelAcceptances.get(ac.getLevel() + 1).getCn():null;
    	}
    }
        
    private void createLevelAcceptances() {
    	levelAcceptances = new HashMap<Integer,Acceptance>();
    	
    	Integer maxLevel = Integer.MIN_VALUE;
    		
    	for(Acceptance ac: acceptances.values()){
    		maxLevel = Math.max(ac.getLevel(), maxLevel);
    		levelAcceptances.put(ac.getLevel(), ac);
    	}
    	this.maxLevel = maxLevel;
	}

	public Map<String,Acceptance> getAcceptances()
    {
        return Collections.unmodifiableMap(acceptances);
    } 
    
    public Map<String,Acceptance> getGeneralAcceptances()
    {
        return Collections.unmodifiableMap(generalAcceptances);
    }
    
    public Map<String,Acceptance> getFieldAcceptances()
    {
        return Collections.unmodifiableMap(fieldAcceptances);
    }
    
    /**
     * Przekazuje tylko te akceptacje, kt�re s� zale�ne od pola 'fieldCn' (gdy 'fieldCn' r�wne null,
     * to przekazywane te akceptacje, kt�re s� niezale�ne od jakichkolwiek p�l).
     */
    public Map<String,Acceptance> getFieldAcceptances(String fieldCn)
    {
        /*Map<String,Acceptance> map = new LinkedHashMap<String, Acceptance>();
        for (Acceptance acc : getAcceptances().values())
        {
            if ((fieldCn == null && acc.getFieldCn() == null) || 
            (fieldCn != null && fieldCn.equals(acc.getFieldCn())))
                map.put(acc.getCn(), acc);
        }
        return map;*/
        return Collections.unmodifiableMap(fieldAcceptances);
        
    }   
    
    public String getCentrumKosztFieldCn()
    {
        return centrumKosztFieldCn;
    }

    public void setCentrumKosztFieldCn(String centrumKosztFieldCn)
    {
        this.centrumKosztFieldCn = centrumKosztFieldCn;
    }

    public boolean isGivingDifferentAcceptancesEnabled()
    {
        return givingDifferentAcceptancesEnabled;
    }

    public void setGivingDifferentAcceptancesEnabled(boolean givingDifferentAcceptancesEnabled)
    {
        this.givingDifferentAcceptancesEnabled = givingDifferentAcceptancesEnabled;
    }

    public FinalAcceptance getFinalAcceptance()
    {
        return finalAcceptance;
    }

    public void setFinalAcceptance(FinalAcceptance finalAcceptance)
    {
        this.finalAcceptance = finalAcceptance;
    }

    public void addAcceptance(String cn, String name, String fieldCn,String level, String transitionName)
    {
        if (cn == null)
            throw new NullPointerException("cn");
        if (name == null)
            throw new NullPointerException("name");
        
        Acceptance acceptance = new Acceptance();
        acceptance.setCn(cn);
        acceptance.setName(name);
        acceptance.setFieldCn(fieldCn);   
        acceptance.setTransitionName(transitionName);
        if(level != null)
        	acceptance.setLevel(Integer.parseInt(level));
        else
        	acceptance.setLevel(1);
        acceptances.put(cn, acceptance);
        
        	
        if (fieldCn == null)
            generalAcceptances.put(cn, acceptance);
        else
            fieldAcceptances.put(cn, acceptance);
            
    }
    
    public String getAcceptanceName(String cn)
    {
    	for (Entry<String, Acceptance> entry : acceptances.entrySet())
    	{
    		if (cn.equals(entry.getKey()))
    			return entry.getValue().getName();
    	}
    	return "Nie znany CN";
    }
    
    public class Acceptance implements Comparable<Acceptance>, Serializable
    {
        /** 'cn' tej akceptacji */
        private String cn;
        /** nazwa tej akceptacji */
        private String name;   
        /** 'cn' pola steruj�cego t� akceptacj� - mo�e by� null */
        private String fieldCn;
        /**Poziom akcekptacji*/
        private Integer level;
        /**nazwa wyjscia ze stanu procesu - jbpm4*/
        private String transitionName;
        
        public void setCn(String cn)
        {
            this.cn = cn;
        }

	public void setTransitionName(String transitionName)
	{
		this.transitionName = transitionName;
		
	}

	public void setName(String name)
        {
            this.name = name;
        }

        public String getCn()
        {
            return cn;
        }
        
        public String getName()
        {
            return name;
        }

        public String getFieldCn()
        {
            return fieldCn;
        }

        public void setFieldCn(String fieldCn)
        {
            this.fieldCn = fieldCn;
        }

		public Integer getLevel() {
			return level;
		}

		public void setLevel(Integer level) {
			this.level = level;
		}

		public int compareTo(Acceptance o)
		{
			return this.level.compareTo(o.getLevel());
		}

		public String getTransitionName()
		{
			return transitionName;
		}

    }
    
    public static class FinalAcceptance implements Serializable
    {
        /** 'cn' pola typu bool z akceptacj� finaln� */
        private String fieldCn;
        /** ile musi by� r�nych akceptacji, aby by�a akceptacja finalna */
        private int minDifferentAcceptances;                
        /** lista akceptacji, kt�re musz� by� zaakceptowane */
        private String[] requiredAcceptances;
        /** 'cn' pola z kwot�, kt�ra musi si� sumowa� do kwot z pola CENTRUM_KOSZT */
        private String exactAmountFieldCn;        
        
        public FinalAcceptance()
        {            
            minDifferentAcceptances = 0;
        }
        
        public String getFieldCn()
        {
            return fieldCn;
        }

        public void setFieldCn(String fieldCn)
        {
            this.fieldCn = fieldCn;
        }

        public int getMinDifferentAcceptances()
        {
            return minDifferentAcceptances;
        }

        public void setMinDifferentAcceptances(int minDifferentAcceptances)
        {
            this.minDifferentAcceptances = minDifferentAcceptances;
        }

        public String getExactAmountFieldCn()
        {
            return exactAmountFieldCn;
        }

        public void setExactAmountFieldCn(String exactAmountFieldCn)
        {
            this.exactAmountFieldCn = exactAmountFieldCn;
        }

        public String[] getRequiredAcceptances()
        {
            return requiredAcceptances;
        }

        public void setRequiredAcceptances(String[] requiredAcceptances)
        {
            this.requiredAcceptances = requiredAcceptances;
        }                
    }
}
