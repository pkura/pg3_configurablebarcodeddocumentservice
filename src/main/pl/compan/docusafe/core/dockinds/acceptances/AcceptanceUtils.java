package pl.compan.docusafe.core.dockinds.acceptances;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.Discardable;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * Klasa zawieraj�ca narz�dzia u�atwiaj�ce prac� z akceptacjami. 
 * @author Micha� Sankowski
 */
public class AcceptanceUtils {

	private final static Logger log = LoggerFactory.getLogger(AcceptanceUtils.class);

	public static void discardToUser(OfficeDocument document, String activity, ActionEvent event, DSUser user) throws EdmException {
		if(document.getDocumentKind().logic() instanceof Discardable){
			((Discardable)document.getDocumentKind().logic()).discardToUser(document, activity, event, user);
		} else {
			throw new IllegalArgumentException("Dokument nie mo�e by� odrzucany");
		}
	}

	//na razie tylko dla IC
	public static void discardToCn(OfficeDocument document, String activity, ActionEvent event, String cn, Object objectId) throws EdmException{
		if(document.getDocumentKind().logic() instanceof Discardable){
			((Discardable)document.getDocumentKind().logic()).discardToCn(document, activity, event, cn, objectId);
		} else {
			throw new IllegalArgumentException("Dokument nie mo�e by� odrzucany");
		}
	}

	/**
	 * Dokonuje serii akceptacji mo�liwych do wykonania przez obecnego u�ytkownika. Wyrzuca wyj�tek je�li u�ytkownik nie m�g� wykona� �adnej akceptacji.
	 * @param documentId
	 * @param activityId
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	public static void fullAccept(Long documentId, String activityId) throws EdmException {
		Document document = Document.find(documentId);
		fullAcceptStart(document, null);
		DSApi.context().session().flush();
		document.getDocumentKind().logic().getAcceptanceManager().refresh(document, document.getFieldsManager(), activityId);
	}

    /**
	 * Dokonuje serii akceptacji mo�liwych do wykonania przez obecnego u�ytkownika.
     * Wyrzuca wyj�tek je�li u�ytkownik nie m�g� wykona� �adnej akceptacji. Na koniec przekazuje faktur� wybranemu
     * u�ytkownikowi
	 * @param documentId
	 * @param activityId
     * @param username - login u�ytkownika
	 * @throws pl.compan.docusafe.core.EdmException
	 */
    public static void fullAcceptWithNextUser(Long documentId, String activityId, String username) throws EdmException {
        Document document = Document.find(documentId);
        fullAcceptStart(document, null);
        DSApi.context().session().flush();
        DSUser user = DSUser.findByUsername(username);
        document.getDocumentKind().logic().getAcceptanceManager().refresh(document, document.getFieldsManager(), null);
        WorkflowFactory.simpleAssignment(activityId,
                user.getDivisions()[0].getGuid(),  username,
                DSApi.context().getPrincipalName(),
                null,
				WorkflowFactory.SCOPE_ONEUSER,
				"do akceptacji",
				null);
    }

    /**
     * Metoda wykonuje seri� akceptacji ale nie przekazuje nikomu pisma (dlatego jest private)
     * @param document
     * @param activity
     * @throws EdmException
     */
    private static void fullAcceptStart(Document document, String activity) throws EdmException{
        log.debug("seria akceptacji: pocz�tek");

        //je�li u�ytkownik nie m�g� dokona� nawet 1 akceptacji to powiadom go o tym poprzez wyj�tek
        document.getDocumentKind().logic().accept(document);
        log.debug("seria akceptacji: pierwsza akceptacja");

        try {    //akceptuj a� pojawi si� wyj�tek braku uprawnie�
            int counter = 1000; //dla bezpiecze�stwa
            while (counter-- > 0) {
                //nie dekretuj ale od�wie�aj proces
                document.getDocumentKind().logic().accept(document);
                log.debug("seria akceptacji: akceptacja");
            }
            log.error("B��d niesko�czonej p�tli:\n user={}, ID={}",
                    DSApi.context().getPrincipalName(),
                    document.getId());
            throw new RuntimeException("Wyst�pi� nieznany b��d");
        } catch (EdmException e) {
            log.debug("Wyj�tek podczas ci�gu akceptacji " + e.getMessage(), e);
        } catch (Exception e) {
            DSApi.context().setRollbackOnly();
        }
    }
}
