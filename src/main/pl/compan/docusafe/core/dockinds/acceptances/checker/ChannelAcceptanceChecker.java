package pl.compan.docusafe.core.dockinds.acceptances.checker;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;

/**
 * Sprawdza czy dany poziom akceptacji zosta� spe�niony dla danego kana�u. AcceptanceChecker mo�e sprawdza� pojedy�czy poziom lub ka�dy poziom w procesie akceptacji.
 * @author MSankowski 
 */
public interface ChannelAcceptanceChecker {
	

	/**
	 * Zwraca jeden niespe�niony warunek akceptacji na danym poziomie
	 *
     * @param doc dokument, kt�rego akceptacje sprawdzamy
     * @param cn
     *@param ckdf @return true je�li znaleziono niespe�niony warunek, false w przeciwnym przypadku
	 * @throws NoAcceptanceConditionDefined gdy nie ma zdefiniowanego warunku akceptacji dla akceptacji o danym kodzie cho� jest ona wymagana 
	 */
	boolean checkAcceptanceCondition(Document doc, String cn, CentrumKosztowDlaFaktury ckdf, String activityId) throws EdmException;
}
