package pl.compan.docusafe.core.dockinds.acceptances;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

import static org.hibernate.criterion.Restrictions.eq;

/**
 * Regu�y dotycz�ce dost�pu do tryb�w akceptacji dla poszczeg�lnych centr�w kosztowych
 *
 * G��wnie dla IC
 *
 * @author Micha� Sankowski michal.sankowski@docusafe.pl
 */
@Entity
@Table(name = "ds_acceptance_mode_rule")
public class AcceptanceModeRule {
    private final static Logger LOG = LoggerFactory.getLogger(AcceptanceModeRule.class);

    @Id
    @SequenceGenerator(name = "ds_acceptance_mode_rule_seq", sequenceName = "ds_acceptance_mode_rule_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ds_acceptance_mode_rule_seq")
    private Long id;

    @Column(name = "cost_center_id")
    private int costCenterId;

    @Column(name = "mode_id")
    private int acceptanceModeId;

    @Column(name="min_amount")
    private BigDecimal minAmount;

    @Column(name="mode_name")
    private String acceptanceModeName;

    public static AcceptanceModeRule findOrCreate(int costCenterId, int acceptanceModeId) throws EdmException {
        List list =
            DSApi.context().session().createCriteria(AcceptanceModeRule.class)
                .add(eq("costCenterId", costCenterId))
                .add(eq("acceptanceModeId",acceptanceModeId))
                .list();
        if(list.isEmpty()){
            AcceptanceModeRule ret = new  AcceptanceModeRule(costCenterId, acceptanceModeId);
            DSApi.context().session().save(ret);
            return ret;
        } else {
            return (AcceptanceModeRule) list.get(0);
        }
    }

    public static AcceptanceModeRule get(int costCenterId, int acceptanceModeId) throws EdmException {
        List list =
                DSApi.context().session().createCriteria(AcceptanceModeRule.class)
                        .add(eq("costCenterId", costCenterId))
                        .add(eq("acceptanceModeId",acceptanceModeId))
                        .list();
        if(list.isEmpty()){
           return null;
        } else {
            return (AcceptanceModeRule) list.get(0);
        }
    }

    public AcceptanceModeRule(int costCenterId, int acceptanceModeId) {
        this.minAmount = BigDecimal.ZERO;
        this.costCenterId = costCenterId;
        this.acceptanceModeId = acceptanceModeId;
        this.acceptanceModeName = IntercarsAcceptanceMode.values()[acceptanceModeId].getName();
    }

    //Dla JPA
    protected AcceptanceModeRule() {
    }

    public boolean isModeAvailable(CentrumKosztowDlaFaktury ckdf){
        //abs bo mo�e by� ujemna kwota
        if(!ckdf.getCentrumId().equals(this.costCenterId)){
            throw new IllegalArgumentException("invalid id ("+ckdf.getCentrumId()+") for rule id (" + this.costCenterId  +")");
        }
        return ckdf.getRealAmount().abs().compareTo(this.getMinAmount()) >= 0;
    }

    public Long getId() {
        return id;
    }

    public int getCostCenterId() {
        return costCenterId;
    }

    public void setCostCenterId(int costCenterId) {
        this.costCenterId = costCenterId;
    }

    public int getAcceptanceModeId() {
        return acceptanceModeId;
    }

    public void setAcceptanceModeId(int acceptanceModeId) {
        this.acceptanceModeId = acceptanceModeId;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public String getAcceptanceModeName() {
        return acceptanceModeName;
    }

    public void setAcceptanceModeName(String acceptanceModeName) {
        this.acceptanceModeName = acceptanceModeName;
    }
}
