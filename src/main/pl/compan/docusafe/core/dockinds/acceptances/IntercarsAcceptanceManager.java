package pl.compan.docusafe.core.dockinds.acceptances;

import java.text.MessageFormat;
import java.util.*;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.jbpm.JbpmContext;
import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.DelegationException;
import org.jbpm.graph.def.Transition;
import org.jbpm.graph.exe.ExecutionContext;
import org.jbpm.graph.exe.ProcessInstance;

import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.ProcessEventType;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.checker.AcceptanceChecker;
import pl.compan.docusafe.core.dockinds.acceptances.checker.IntercarsAcceptanceChecker;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.dockinds.process.ProcessAction;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.DSjBPM;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.parametrization.ic.AssecoImportManager;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.process.ProcessLocator;
import pl.compan.docusafe.core.dockinds.process.ProcessLogic;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub;

import static pl.compan.docusafe.parametrization.ic.IcUtils.hasVatDate;

public class IntercarsAcceptanceManager implements JBPMAcceptanceManager, ProcessLogic, ProcessLocator {
	static final Logger log = LoggerFactory.getLogger(IntercarsAcceptanceManager.class);
    static final Logger exportLog = LoggerFactory.getLogger("export_log");
	private static final String PROCESS_NAME = "AkceptacjeIC"; 
	static final String DOC_ID = "docId";
	static final String ACTIVITY_ID = "aId";	
	
	private static final String WAIT_FOR_FINISH_STATE = "czekaj_na_koniec";
	static final String WAIT_FOR_CENTER_STATE = "czekaj_na_centrum";
	
	private static final String TRANSITION_KONIEC = "koniec";
	private static final String TRANSITION_CHECK_ACCEPTANCES = "sprawdz_akceptacje";
	static final String TRANSITION_NO_CENTER = "nie_ma_centrum";
	static final String TRANSITION_ALREADY_ACCEPTED = "juz_zaakceptowane";
	
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(IntercarsAcceptanceManager.class.getPackage().getName(),null);

	private static boolean isInJBPM(Document doc, FieldsManager fm) throws EdmException{
		return null != fm.getValue(InvoiceLogic.JBPM_PROCESS_ID);
	}

    public void startProcess(OfficeDocument od, Map<String, ?> params) {
        start(od);
    }

    /* (non-Javadoc)
      * @see pl.compan.docusafe.core.dockinds.acceptances.JBPMAcceptanceManager#start(java.lang.Long)
      */
	public Long start(Document doc) {				
		JbpmContext ctx = null;
		try {			
			ctx = DSjBPM.context();
						
			ProcessInstance pi = ctx.newProcessInstance(PROCESS_NAME);
			ContextInstance ci = pi.getContextInstance();
			
			ci.createVariable(DOC_ID, doc.getId());

			DataMartEventBuilder.get()
				.processEvent(ProcessEventType.JBMP_PROCESS_INSTANCE_CREATE, PROCESS_NAME)
				.documentId(doc.getId())
				.objectId(pi.getId());

			Map<String, Object> map = new HashMap<String,Object>();
			map.put(InvoiceICLogic.JBPM_PROCESS_ID, pi.getId());
			map.put(InvoiceICLogic.AKCEPTACJA_FINALNA_FIELD_CN, false);
			doc.getDocumentKind().setOnly(doc.getId(),map);
            
			pi.signal();
						
			ctx.save(pi);
			log.debug("poprawnie utworzono nowy proces jbpm");
			
			return pi.getId();
		} catch (Exception e) {
			log.debug(e.getLocalizedMessage(), e);
			return null;
		} 
	}
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.dockinds.acceptances.JBPMAcceptanceManager#finish(java.lang.Long)
	 */
	public void finish(Document doc) {
		JbpmContext ctx = null;
		try {
            FieldsManager fm = Documents.document(doc.getId()).getFieldsManager();
			if (!isInJBPM(doc, fm)) {
				return;
			}

			Long piId = (Long) fm.getKey(InvoiceICLogic.JBPM_PROCESS_ID);
			ctx = DSjBPM.context();

			ProcessInstance pi = ctx.getProcessInstance(piId);

			String name = pi.getRootToken().getName();
			if (!name.equals(WAIT_FOR_FINISH_STATE)) {
				throw new EdmException("Wrong state, wait for finish expected");
			}
			pi.signal(TRANSITION_KONIEC);

			ctx.save(pi);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
	}
	
	static final Map<String, Integer> stateToStateField ;
	
	static{
		Map<String, Integer> map = new HashMap();
		//przesuni�cie o jeden do ty�u
		map.put(WAIT_FOR_CENTER_STATE, 10);
		map.put("zwykla", 10);
		map.put("szefa_dzialu", 1);
		map.put("szefa_pionu", 2);
		map.put("dyrektora_fin", 3);
		map.put("product_manager",33);
		map.put("ksiegowa", 4);
		map.put("kontrola_jakosci",5);
		map.put(WAIT_FOR_FINISH_STATE, 77);
		
		stateToStateField = Collections.unmodifiableMap(map);
	}
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.dockinds.acceptances.JBPMAcceptanceManager#refresh(java.lang.Long)
	 */
//	private void refresh(Document doc, String activityId) throws EdmException {
//		log.trace("IntercarsAcceptanceManager.java:refresh");
//        FieldsManager fm = doc.getFieldsManager();
//        refresh(doc, fm, activityId);
//	}

    public void refresh(Document doc, FieldsManager fm, String activityId) throws EdmException {
        JbpmContext ctx = null;
        ProcessInstance pi = null;
        if (!isInJBPM(doc, fm)) {
            log.trace("IntercarsAcceptanceManager.java:non jbpm");
            return;
        }

        Long piId = (Long) fm.getKey(InvoiceICLogic.JBPM_PROCESS_ID);
        ctx = DSjBPM.context();
        pi = ctx.getProcessInstance(piId);

        log.info("TRANSITIONS:" + pi.getRootToken().getAvailableTransitions().size());
        for(Transition transition:  (Set<Transition>)pi.getRootToken().getAvailableTransitions() ){
            if(Character.isUpperCase(transition.getName().charAt(0))){
                log.info(transition.getName());
            }
        }

        if(pi.hasEnded()){ //zako�czono proces nie ma co od�wierza�
            return;
        }

        ContextInstance cx = pi.getContextInstance();

        if (activityId != null) {
            cx.setTransientVariable(ACTIVITY_ID, activityId);
        }

        String name = pi.getRootToken().getNode().getName();

        log.trace("IntercarsAcceptanceManager.java:refresh - document rootToken.node.name = {}", pi.getRootToken().getNode().getName());

        // nie ma centrum, dalej czekamy
        //if (name.equals(WAIT_FOR_CENTER_STATE)) {// && !canFinish(doc)) {
            if (!CentrumKosztowDlaFaktury.existsForDocument(doc.getId())) {
                doc.getDocumentKind().setOnly(doc.getId(), Collections.singletonMap(InvoiceLogic.STATUS_FIELD_CN, (Object) 10));
                return;
            }
        //}
        try{
		    pi.signal(TRANSITION_CHECK_ACCEPTANCES);
        } catch (DelegationException de) {
            if(de.getCause() instanceof NoAcceptanceConditionDefined){
                throw (NoAcceptanceConditionDefined)de.getCause();
            } else {
                throw de;
            }
        }
        //ponowne pobieranie fields managera - zmiany podczas dzia�ania refresh
        doc.getDocumentKind().handleLabels(doc, Documents.document(doc.getId()).getFieldsManager());
        TaskSnapshot.updateByDocument(doc);
        log.debug("IntercarsAcceptanceManager.java:refresh end - document rootToken.node.name = {}", pi.getRootToken().getNode().getName());
    }

    /**
	 * @throws EdmException 
	 * @throws AccessDeniedException 
	 * @throws DocumentLockedException 
	 * @throws DocumentNotFoundException
     **/
	 static void push(ExecutionContext context) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException {
		String activityId =  (String) context.getContextInstance().getTransientVariable(ACTIVITY_ID);
		log.trace("push ACTIVITY_ID = {}", activityId);
		
		Long docId = (Long) context.getContextInstance().getVariable(DOC_ID);
		
		Document doc = Document.find(docId);		
		
		String cn = context.getProcessInstance().getRootToken().getNode().getName();
		
		log.debug("push({})...",cn);
		if (!acceptancesRequired(context.getContextInstance(), cn, doc, activityId)) {
			log.debug("...pushing");
			context.leaveNode(TRANSITION_ALREADY_ACCEPTED);
		} else {
			doc.getDocumentKind().
				setOnly(docId, Collections.singletonMap(InvoiceLogic.STATUS_FIELD_CN, (Object)stateToStateField.get(cn)));
			log.debug("...waiting for acceptance: {}", cn);
		}
	}
	
	static boolean acceptancesRequired(ContextInstance contextInstance, String cn, Document doc, String activityId) throws EdmException {		
		AcceptanceChecker checker = new IntercarsAcceptanceChecker();		
		return checker.checkAcceptanceCondition(contextInstance, doc, cn, activityId);
	}

	static void finalizeProcess(ExecutionContext context) throws Exception {
		Long docId = (Long)context.getContextInstance().getVariable(DOC_ID);	

		Document doc = Document.find(docId);
		DocumentKind kind = doc.getDocumentKind();

        String exportTypeCn = doc.getFieldsManager().getEnumItemCn("EXPORT_TYPE");

		Map<String,Object> toChange = new HashMap<String,Object>();

        if("ASSECO".equals(exportTypeCn)){
            //exportAsseco(doc);
            toChange.put(InvoiceICLogic.STATUS_FIELD_CN, InvoiceICLogic.STATUS_SENT_TO_ASSECO_ID);
            toChange.put(InvoiceICLogic.STATUS_FOR_USER_FIELD_CN, InvoiceICLogic.STATUS_SENT_TO_ASSECO_ID);
        } else {
		    toChange.put(InvoiceICLogic.STATUS_FIELD_CN, stateToStateField.get(WAIT_FOR_FINISH_STATE));
            //toChange.put(InvoiceICLogic.STATUS_FOR_USER_FIELD_CN, stateToStateField.get(WAIT_FOR_FINISH_STATE));
        }
		toChange.put(InvoiceICLogic.AKCEPTACJA_FINALNA_FIELD_CN, Boolean.TRUE);
		kind.setOnly(docId,toChange);

		context.leaveNode(TRANSITION_KONIEC);
		log.trace("Zako�czono proces id:{}", context.getProcessInstance().getId());
	}

    /**
     * Tymczasowe (mam nadziej�) zabezpieczenie przed ponownym importem, synchronizowa� poprzez lastImportedDocuments
     */
   // private static LinkedList<Long> lastImportedDocuments = Lists.newLinkedList();
   // private static HashSet<Long> importedDocuments = Sets.newHashSet();

    public static void exportAsseco(Document doc) throws Exception {
        long start = System.currentTimeMillis();
        AssecoImportManager manager = new AssecoImportManager();
        IcDataProcessorStub.ImportAnswer answer = manager.importDokZk(doc).get_return();
        if (!answer.getSuccess() || !StringUtils.isBlank(answer.getError())) {
            exportLog.error("...export nieudany : " + answer.getError());
            DSApi.context().setRollbackOnly();
            throw new EdmException(answer.getError());
        } else {
            exportLog.error("...wys�anie udane : " + answer.getError() + " " + answer.getLog() + " " + answer.getId());
            exportLog.error("sprawdzanie stanu exportu...");

            IcDataProcessorStub.DokZkAnswer ret = manager.dajDokZkInfo(doc.getId());
            if (ret.getSuccess()
                    && ret.getSafoId() != Integer.MIN_VALUE) {
                exportLog.error("poprawny stan exportu: " + ret.getStatus() + " | " + ret.getError() );
                DSApi.context().messages().addActionMessage("Eksport udany : " + ret.getStatus());
                DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND).setOnly(doc.getId(),
                        Collections.singletonMap(InvoiceICLogic.SAFO_LAST_MESSAGE_FIELD_CN, ret.getStatus()));
                LabelsManager.addLabelByName(doc.getId(), InvoiceICLogic.ASSECO_LABEL, DSApi.context().getPrincipalName(), false);
            } else {
                DSApi.context().setRollbackOnly();
                throw new SafoExportException(ret.getStatus());
            }
        }
    }

    public static class SafoExportException extends EdmException {
        public SafoExportException(String message) {
            super(message);
        }
    }

	//TODO poprawi�
	public Boolean isFinalAccepted(Document doc, FieldsManager fm) throws EdmException {
		return fm.getBoolean(InvoiceLogic.AKCEPTACJA_FINALNA_FIELD_CN);
	}
	
	/**
	 * @param context centrumKosztowDlaFakturyId (mo�e by� null), kod akceptacji(mo�e by� null)
	 * (wszystkie kombinacje null/nie null mo�liwe)
	 */
	public boolean accept(Document doc, Object... context) throws EdmException {
		if(!CentrumKosztowDlaFaktury.hasCentrum(doc.getId())){
			throw new EdmException(sm.getString("dokumentNieMaPrzypisanychCentrowKosztowych",doc.getId()));
		}

		//nowa poprawka -> sprawdzi� od�wierzanie
        FieldsManager fm = doc.getFieldsManager();
		refresh(doc, fm, null);

		JbpmContext ctx = null;
		ProcessInstance pi = null;

		if(!isInJBPM(doc, fm)){
			throw new EdmException("Dokument nie posiada rozpocz�tego procesu akceptacji");
		}

        Long piId = (Long) fm.getKey(InvoiceICLogic.JBPM_PROCESS_ID);
		ctx = DSjBPM.context();
		pi = ctx.getProcessInstance(piId);

		if (pi.hasEnded()) {
			throw new EdmException(sm.getString("procesAkceptacjiDlaDokumentuZostalZakonczony", doc.getId()));
		}

		//wyst�powa� b��d przy odrzucaniu, trzeba b�dzie to usun��
		if (fm.getEnumItemCn(InvoiceICLogic.STATUS_FIELD_CN).equals(InvoiceICLogic.STATUS_ODRZUCONY)) {
			pi.signal(TRANSITION_CHECK_ACCEPTANCES);
		}

		String cn = getAcceptanceCn(context, pi);

        //Sprawdzanie czy przed akceptacjami globalnymi zosta�y wykonane pozosta�e akceptacje
		if((cn.equals("kontrola_jakosci") || cn.equals("dyrektora_fin") || cn.equals("ksiegowa"))
                && !cn.equals(pi.getRootToken().getNode().getName())){
			throw new EdmException(sm.getString("BrakWymaganychAkceptacji", doc.getId()));
		}

		if (cn.equals(IntercarsAcceptanceManager.WAIT_FOR_CENTER_STATE)) {
			throw new EdmException(sm.getString("BrakPrzydzielonychCentrowKosztowych", doc.getId()));
		}

		//TODO: umie�ci� sprawdzanie takich warunk�w gdzie� indziej (np. naprawi� dockind)
		if (cn.equals("ksiegowa")){
            validateCKDFs(fm);
            if(fm.getValue(InvoiceLogic.REJESTR_FIELD_CN) == null){
			    throw new EdmException("Akceptacja wymaga wype�nionych p�l: Rejestr");
            }
            if(hasVatDate(fm) && fm.getKey(InvoiceICLogic.DATA_VAT) == null){
                throw new EdmException("Akceptacja wymaga wype�nionych p�l: Data VAT");
            }
		}

		//TODO: usun�� "ksi�gowa", "dyrektora_fin" etc
		if (!cn.equals("ksiegowa") && !cn.equals("dyrektora_fin")) {
			Long centrumDlaFakturyId = getAcceptanceObjectId(context, pi);

			if (centrumDlaFakturyId == null) {
				log.warn("CKDF null at cn=" + cn);
				handleNullCKDF(pi, doc);
			} else {
				CentrumKosztowDlaFaktury ckdf = CentrumKosztowDlaFaktury.getInstance().find(centrumDlaFakturyId);
				cn = verifyCn(doc, ckdf, cn);
				centrumAcceptance(ckdf, doc, cn, pi);
			}
		} else {
			log.debug("akceptacja {}", cn);
			giveAcceptance(doc, null, cn, fm);
		}
		return true;
	}

    private void validateCKDFs(FieldsManager fm) throws EdmException {
        // fm.getValue();
        log.info("Validating ckdfs");
        if (InvoiceICLogic.FAKTURY_ZAKUPOWE_CNS.contains(fm.getEnumItemCn(InvoiceICLogic.RODZAJ_FAKTURY_CN))) {
            return;
        }
        List<CentrumKosztowDlaFaktury> ckdfs = (List<CentrumKosztowDlaFaktury>) fm.getValue(InvoiceICLogic.CENTRUM_KOSZTOWE_FIELD_CN);
        for (CentrumKosztowDlaFaktury ckdf : ckdfs) {
            if (ckdf.getClassTypeId() == null) {
                throw new EdmException("Pole \"Klasyfikacja podatkowa w koszty\" jest obowi�zkowe przy akceptacji ksi�gowej (centrum " + ckdf.getDescription() + ")");
            }
            if (ckdf.getVatTypeId() == null) {
                throw new EdmException("Pole \"VAT w koszty\" jest obowi�zkowe przy akceptacji ksi�gowej (centrum " + ckdf.getDescription() + ")");
            }
            if (ckdf.getConnectedTypeId() == null) {
                throw new EdmException("Pole \"Powi�zane/Niepowi�zane\" jest obowi�zkowe przy akceptacji ksi�gowej (centrum " + ckdf.getDescription() + ")");
            }
        }
    }

    private static void giveAcceptance(Document doc, CentrumKosztowDlaFaktury ckdf, String cn, FieldsManager fm) throws EdmException{
		if(!AcceptanceCondition.checkAcceptancePermission(
                DSUser.findByUsername(DSApi.context().getPrincipalName()),
				ckdf == null ? null : ckdf.getAcceptingCentrumId(),
				cn)){
            throw new EdmException("brak uprawnie� do dokonania akceptacji o kodzie " + cn
            		+ (ckdf==null?"":" dla centrum " + ckdf.getCentrumCode()));
        }

        if (cn.equals("ksiegowa")){
            if("ASSECO".equals(fm.getEnumItemCn("EXPORT_TYPE"))){
                try {
                    exportAsseco(doc);
                } catch (EdmException e) {
                    throw e;
                } catch (Exception ex) {
                    throw new EdmException(ex.getMessage(),ex);
                }
            } else {
                if(fm.getValue(InvoiceICLogic.NUMER_KSIEGOWANIA_FIELD_CN) == null){
                    throw new EdmException("Akceptacja wymaga wype�nionych p�l: Numer ksi�gowania");
                }
            }
        }

		DocumentAcceptance.createAndSave(doc, 
				DSApi.context().getPrincipalName(),
				cn,
				ckdf == null? null : ckdf.getId());

		String tmp = null;

		if(ckdf == null){
			tmp = sm.getString("DokonoanoAkceptacjiDokumentu",cn);
		} else {
			tmp = sm.getString("DokonoanoAkceptacjiDokumentuDlaCentrumKosztowego",cn, ckdf.getCentrumCode());
		}

        DocumentKind dk = doc.getDocumentKind();
        //nowy status wskazuje na ostatnio wykonywan� akceptacje
        if(dk.getFieldByCn(InvoiceICLogic.STATUS_FOR_USER_FIELD_CN) != null){
            dk.setOnly(doc.getId(),
                        Collections.singletonMap(InvoiceICLogic.STATUS_FOR_USER_FIELD_CN,
                        dk.getFieldByCn(InvoiceICLogic.STATUS_FOR_USER_FIELD_CN).getEnumItemByCn(cn.toUpperCase()).getId()));
        }

        try{
			DataMartManager.addHistoryEntry(doc.getId(),
				Audit.create("giveAccept",
					DSApi.context().getPrincipalName(),
					tmp,
					cn,
					null));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} 
	}

	public Collection<pl.compan.docusafe.core.dockinds.process.ProcessInstance> lookupForDocument(Document doc) throws EdmException {
		JbpmContext ctx = null;
		ProcessInstance pi = null;
		Long id = (Long) Documents.document(doc.getId()).getFieldsManager().getValue(InvoiceLogic.JBPM_PROCESS_ID);
		if (id == null) {
			return Collections.emptyList();
		}
		ctx = DSjBPM.context();
		pi = ctx.getProcessInstance(id);
		return Collections.<pl.compan.docusafe.core.dockinds.process.ProcessInstance>singleton(new IntercarsAcceptanceProcess(pi, doc));
	}

	private static void centrumAcceptance(CentrumKosztowDlaFaktury ckdf, Document doc, String cn, ProcessInstance pi) throws EdmException {
		log.debug("akceptacja dla centrum " + ckdf.getCentrumCode());
		giveAcceptance(doc, ckdf, cn, doc.getFieldsManager());
		pi.getContextInstance().deleteVariable(IntercarsAcceptanceChecker.CENTRUM_DLA_FAKTURY_ID);
	}

	private String getAcceptanceCn(Object[] context, ProcessInstance pi) {
		String cn = (context.length > 1 && context[1] != null)
                    ? context[1].toString()
                    : pi.getRootToken().getNode().getName();
		return cn;
	}

	private Long getAcceptanceObjectId(Object[] context, ProcessInstance pi) {
		Long centrumDlaFakturyId = (context.length > 0 && context[0] != null) ? (Long) context[0] : (Long) pi.getContextInstance().getVariable(IntercarsAcceptanceChecker.CENTRUM_DLA_FAKTURY_ID);
		return centrumDlaFakturyId;
	}

	private void handleNullCKDF(ProcessInstance pi, Document doc) throws EdmException {
		pi.signal(TRANSITION_CHECK_ACCEPTANCES);
		Long centrumDlaFakturyId = (Long) pi.getContextInstance().getVariable(IntercarsAcceptanceChecker.CENTRUM_DLA_FAKTURY_ID);
		String newcn = pi.getRootToken().getNode().getName();
		if (centrumDlaFakturyId == null) {
			giveAcceptance(doc, null, newcn, doc.getFieldsManager());
		} else {
			centrumAcceptance(CentrumKosztowDlaFaktury.getInstance().find(centrumDlaFakturyId), doc, newcn, pi);
		}
	}

	//TODO: wyeliminowac konieczno�� sprawdzania cn�w
	//sprawdza czy przypadkiem nie potrzeba dalszego cna
	private String verifyCn(Document doc, CentrumKosztowDlaFaktury ckdf, String cn) throws EdmException {
		if(ckdf.getAcceptanceCnsNeeded().contains(cn) && DocumentAcceptance.find(doc.getId(), cn, ckdf.getId()) == null){
            List<String> cnsBefore = ckdf.getAcceptanceModeInfo().cnsBefore(cn);
            if(cnsBefore.isEmpty() || DocumentAcceptance.find(doc.getId(),cnsBefore.get(cnsBefore.size()-1), ckdf.getId()) != null){
			    return cn;
            } else {
                throw new EdmException("Akceptacja nie mo�e by� wykonana przed akceptacj� '" + cnsBefore.get(cnsBefore.size() - 1) + "'");
            }
		//rzadka sytuacja - proba akceptacji w mpk o cn wyzszego poziomu niz stan procesu, tylko dla dokumentow o wielu mpk
		} else {
			for(String ncn: ckdf.getAcceptanceCnsList()){
				if(ckdf.getAcceptanceCnsNeeded().contains(ncn) && DocumentAcceptance.find(doc.getId(), ncn, ckdf.getId()) == null){
					return ncn;
				}
			}
			throw new RuntimeException("Akceptacje dla " + ckdf.getCentrumCode()+ " s� ju� wykonane");
		}
	}

	public pl.compan.docusafe.core.dockinds.process.ProcessInstance lookupForId(Object id) throws EdmException {
		return new IntercarsAcceptanceProcess(DSjBPM.context().getProcessInstance(Long.parseLong(id.toString())));
	}

    public pl.compan.docusafe.core.dockinds.process.ProcessInstance lookupForTaskId(String id) throws EdmException {
        return null;
    }

    public void process(pl.compan.docusafe.core.dockinds.process.ProcessInstance pi, ProcessAction pa) throws EdmException {
		if(pi instanceof IntercarsAcceptanceProcess){
			process((IntercarsAcceptanceProcess)pi, pa);
		} else {
			throw new IllegalArgumentException("Nieznany proces:" + pi.getClass());
		}
	}

	private void process(IntercarsAcceptanceProcess pi, ProcessAction pa) throws EdmException {
		if (pa.getActionName().equals("PROCESS_ADMIN_REJECT")) {
			WorkflowFactory.simpleAssignment(pa.getActivityId(),
					AcceptanceCondition.find("admin").get(0).getDivisionGuid(),
					null,
					DSApi.context().getPrincipalName(),
					null,
					WorkflowFactory.SCOPE_DIVISION,
					"odrzucenie do administratora",
					null);
		} else if(pa.getActionName().equals("PROCESS_RESTART")) {
			start(pa.getDocument());
		}
	}
}
