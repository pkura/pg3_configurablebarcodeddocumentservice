package pl.compan.docusafe.core.dockinds.acceptances;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;

public class Finalizer implements ActionHandler {

	public void execute(ExecutionContext ec) throws Exception {
		Long docId = (Long)ec.getContextInstance().getVariable(IntercarsAcceptanceManager.DOC_ID);
		if(CentrumKosztowDlaFaktury.hasCentrum(docId)){
			IntercarsAcceptanceManager.finalizeProcess(ec);
		} else {
			ec.leaveNode(IntercarsAcceptanceManager.TRANSITION_NO_CENTER);
		}
	}
}
