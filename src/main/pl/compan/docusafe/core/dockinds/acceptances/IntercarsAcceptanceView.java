package pl.compan.docusafe.core.dockinds.acceptances;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.process.AbstractProcessView;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.RenderBean;
import pl.compan.docusafe.web.common.SimpleRenderBean;

/**
 *
 * @author Micha� Sankowski
 */
public class IntercarsAcceptanceView extends AbstractProcessView{

	private final static Logger log = LoggerFactory.getLogger(IntercarsAcceptanceView.class);
	private final static String IC_ACCEPTANCES_TEMPLATE = "ic-acceptances.vm";
	private final static String IC_AJAX_ACCEPTANCES_VIEW_TEMPLATE = "ic-acceptances-archive-view.vm";
	private final static String IC_ADMIN_TEMPLATE = "ic-admin.vm";

	public RenderBean render(ProcessInstance pi, ProcessActionContext context)throws EdmException {
		if(pi instanceof IntercarsAcceptanceProcess){
			return render((IntercarsAcceptanceProcess) pi, context);
		} else {
			throw new IllegalArgumentException("IntercarsAcceptanceView nie wspiera danego procesu");
		}
	}

	private static RenderBean render (IntercarsAcceptanceProcess pi, ProcessActionContext context) throws EdmException{
		switch(context.getActionType()){
			case ADMIN_VIEW:
				pi.initJBPMValues();
				return new SimpleRenderBean(pi, IC_ADMIN_TEMPLATE);
			case ARCHIVE_VIEW:
				return new SimpleRenderBean(pi, IC_AJAX_ACCEPTANCES_VIEW_TEMPLATE);
			default:
                pi.initJBPMValues();
				return new SimpleRenderBean(pi, IC_ACCEPTANCES_TEMPLATE);
		}
	}
}
