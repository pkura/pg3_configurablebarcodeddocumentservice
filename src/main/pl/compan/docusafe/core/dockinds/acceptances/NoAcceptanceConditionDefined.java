package pl.compan.docusafe.core.dockinds.acceptances;

import pl.compan.docusafe.core.EdmException;

/**
 * Wyj�tek wyrzucany w przypadku gdy nie zdefiniowano wymaganego warunku akceptacji 
 * @author MSankowski 
 */
public class NoAcceptanceConditionDefined extends EdmException{

	private static final long serialVersionUID = 666+1L;

	public NoAcceptanceConditionDefined(String message) {
		super(message);
	}
}
