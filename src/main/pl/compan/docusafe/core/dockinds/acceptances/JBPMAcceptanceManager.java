package pl.compan.docusafe.core.dockinds.acceptances;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;

public interface JBPMAcceptanceManager {

	/**
	 * Rozpoczyna proces akceptacji w jBPM dla dokumentu
	 * @param documentId 
	 * @return identyfikator procesu jBPM
	 */
	public Long start(Document doc);

	/** zakonczenie procesu */
	public void finish(Document doc);

	/**
	 * Uaktualnienie statnu procesu akceptacji, za ka�dym razem jak akceptacja zostanie dodana, odrzucona czy pojawi si� nowy kana� nale�y wywo�a� t� metod�.
	 * Metoda ta dekretuje dokument na odpowiedni dzia�.
	 * @param documentId id dokumentu
	 */
	public void refresh(Document doc, FieldsManager fm, String activityId) throws EdmException ;

//	/**
//	 * Zwraca stan w procesie akceptacji w jakim znajduje si� dany dokument, nazwy stan�w zale�� od implementacji JBPMAcceptanceManager
//	 * @param documentId id dokumentu
//	 * @return nazwa stanu
//	 */
//	public String getState(Document doc);
	
	/**
	 * Pr�buje nada� akceptacje dla warunku, kt�ry blokuje proces akceptacji. Sprawdzane s� wszystkie wymogi, kt�re musi spe�ni� u�ytkownik by nada� akceptacj�. W razie problem�w wyrzucane s� wyj�tki. 
	 * @param doc 
	 * @param context dodatkowe warto�ci zale�ne od implementacji
	 * @return true, je�li akceptacja zosta�a nadana, false je�li ten dokument nie podlega procesowi akceptacji przez JBPM. 
	 */
	public boolean accept(Document doc, Object... context) throws EdmException ;
	
	/**
	 * Zwraca informacje czy dokument zosta� ca�kowicie zaakceptowany
	 * @param doc
	 * @return true, je�li dokument zosta� zaakceptowany, false je�li proces jbpm jeszcze nie zosta� zako�czony, null gdy proces jbpm nie istnieje
	 */
	public Boolean isFinalAccepted(Document doc, FieldsManager fm) throws EdmException;
	
	
}