package pl.compan.docusafe.core.dockinds.acceptances;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

public class PushHandler implements ActionHandler {

	public void execute(ExecutionContext context) throws Exception {
		IntercarsAcceptanceManager.push(context);
	}
}
