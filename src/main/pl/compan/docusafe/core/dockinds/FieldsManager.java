package pl.compan.docusafe.core.dockinds;

import org.hibernate.HibernateException;
import org.springframework.util.CollectionUtils;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesState;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Klasa daj�ca prosty dost�p do p�l i ich warto�ci z danego rodzaju dokumentu.
 * Zanim si� u�yje obiektu tej klasy na stronie JSP, powinien on zosta�
 * zainicjowany (wczytany z bazy). Inicjowanie nast�pi poprzez odwo�anie do
 * danego pola/warto�ci, ale tak�e mo�na zrobi� to r�cznie poprzez funkcje
 * 'initialize()'.
 * 
 * FIXME: UWAGA, mozliwosc Stack Overflow!!!
 * Uzycie pol typu document-field w dw�ch dokumentach i stworzenie w nich linkow,
 * ktore pokazuja na siebie nawzajem (czyli np. Wniosek i Odpowiedz na wniosek:
 * wniosek ma link do odpowiedzi a odpowied� ma link do wniosku) prowadzi do 
 * Stack overflow, przy wywolaniu loadData() na dowolnym z tych dokumentow.
 * Np. przy wczytywaniu nadrzednego brane sa wszystkie wartosci,
 * w tym m.in. dokument podrzedny, ktory tez ma pole document-field, wiec i on bedzie mial
 * wczytywane wartosci, tym razem z dokumentu nadrzednego,
 * wiec znowu beda ladowane wartosci z nadrzednego... i tak w nieskonczonosc.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class FieldsManager implements Serializable
{
	private static final long serialVersionUID = -352751533217059918L;
	private final static Logger log = LoggerFactory.getLogger(FieldsManager.class);

	private DocumentKind documentKind;
	private Long documentId;
    private String activityId;
	
    /**
     * Mapa warto�ci, je�li null to jest inicjalizowana
     */
	private Map<String, FieldValue> values;
	private AcceptancesState acceptanceState;
	private DockindScheme dockindScheme;
	//private Boolean enableSheme = true;

	public FieldsManager(Long documentId, DocumentKind documentKind)
	{
		this.documentId = documentId;
		this.documentKind = documentKind;
	}

	private Map<String, FieldValue> getValues() throws EdmException
	{
		if (values == null)
			loadData();
		return values;
	}

    public FieldsManager withActivityId(String activityId) {
        LoggerFactory.getLogger("tomekl").debug("withActivityId {}",activityId);
        this.activityId = activityId;
        return this;
    }

    /**
     * �aduje warto�ci zapisane w bazie danych,
     *
     * zwykle powinno wystarczy� wywo�anie initialize(), kt�ra wywo�uj� t� metod�
     *
     * @throws EdmException
     */
    public final void initializeValues() throws EdmException
	{
		for(Field field : this.getFields())
		{
			if(field.getEditPermissions() != null)
			{
				field.checkPermissions();
			}
		}
		documentKind.initialize();
		if (values == null){
			loadData();
        }

		if(documentKind.logic() != null)
			documentKind.logic().onLoadData(this);
	}

    /**
     * �aduje dane potrzebne by "wypu�ci�" FieldsManagera na formatk�
     * - tak by nie musia� ju� si� ��czy� z baz� danych
     * @throws EdmException
     */
	public final void initialize() throws EdmException
	{
		initializeValues();

        if (dockindScheme == null) {
			dockindScheme = documentKind.getSheme(this);
		}
	}

	/**
	 * inicjalizacja akceptacji (sprawdzanie uprawnie� do poszczeg�lnych
	 * akceptacji i pobieranie z bazy wykonanych wcze�niej akceptacji)
	 */
	public void initializeAcceptances() throws EdmException
	{
		if(!AvailabilityManager.isAvailable("fieldsManager.initializeAcceptances"))
			return;
		if (documentKind.getAcceptancesDefinition() == null)
			return;

		acceptanceState = new AcceptancesState(documentId, getAcceptancesDefinition(), this);
		acceptanceState.initialize();
	}

	public List<Field> getFields() throws EdmException
	{
		return documentKind.getFields();
	}

	public List<Field> getFieldsByAspect(String cn) throws EdmException
	{
		return documentKind.getFieldsByAspect(cn);
	}

	public Field getField(String cn) throws EdmException
	{
		return documentKind.getFieldByCn(cn);// getValues().get(cn).getField();
	}

    /**
     * Zwraca pola s�ownika
     * @param dictionaryCn
     * @return
     * @throws EdmException
     */
    public List<Field> getDictionaryFields(String dictionaryCn) throws EdmException {
        Field dic = getField(dictionaryCn);
        if(CollectionUtils.isEmpty(dic.getFields())){
            throw new IllegalArgumentException(dictionaryCn + " nie jest elementem s�ownika!");
        } else {
            return dic.getFields();
        }
    }

    /**
     * Zwraca konretne pole s�ownika
     * @param dictionaryCn
     * @param fieldCn
     * @return
     * @throws EdmException
     */
    public Field getDictionaryFieldByCn(String dictionaryCn, String fieldCn) throws EdmException {
        for(Field f : getDictionaryFields(dictionaryCn)){
            if(f.getCn().equals(fieldCn))
                return f;
        }

        throw new IllegalArgumentException("Brak takiego pola w s�owniku");
    }

	public String getOnclick(String cn) throws EdmException
	{
		return ((DockindButtonField) documentKind.getFieldByCn(cn)).getOnclick();
	}

	/**
	 * Zwraca warto�� danego pola przekonwertowan� do czytelnej postaci. Dla
	 * cz�sci p�l getValue(cn) == getKey(cn), ale np. dla pola typu CLASS getKey
	 * zwraca id obiektu a getValue ten obiekt, oraz dla type ENUM getKey zwraca
	 * id warto�ci wyliczeniowej, a getValue nazw� tej warto�ci wyliczeniowej
	 * (wy�wietlan� u�ytkownikowi).
	 * 
	 * @param cn
	 * @return
	 * @throws EdmException
	 */
	public Object getValue(String cn) throws EdmException
	{
		// if(getValues() == null)
		// return null;
		// else
		if (getValues().get(cn) == null)
			return null;
		else
			return getValues().get(cn).getValue();
	}

	public Object getValue(Enum<?> cn) throws EdmException
	{
		return getValue(cn.toString());
	}

	/**
	 * Zwraca String je�li getValue nie zwr�ci null
	 * 
	 * @param cn
	 * @return
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	public String getStringValue(String cn) throws EdmException
	{
		Object t;
		return (t = getValue(cn)) == null ? null : t.toString();
	}
       
    /**
	 * Zwraca Float je�li getValue nie zwr�ci null
	 * 
	 * @param cn
	 * @return Float
	 * @throws pl.compan.docusafe.core.EdmException
	 */
        public Float getFloatValue(String cn) throws EdmException
        {
            Object t;
            return ( t = getValue(cn)) == null ? null : Float.parseFloat(t.toString());
        }
        /**
	 * Zwraca Long je�li getValue nie zwr�ci null
	 * 
	 * @param cn
	 * @return Long
	 * @throws pl.compan.docusafe.core.EdmException
	 */
        public Long getLongValue(String cn) throws EdmException
        {
            Object t;
            return ( t = getValue(cn)) == null ? null : Long.parseLong(t.toString());
        }
        /**
	 * Zwraca Integer je�li getValue nie zwr�ci null
	 * 
	 * @param cn
	 * @return Integer
	 * @throws pl.compan.docusafe.core.EdmException
	 */
        public Integer getIntegerValue(String cn) throws EdmException
        {
            Object t;
            return ( t = getValue(cn)) == null ? null : Integer.parseInt(t.toString());
        }
	/**
	 * Zwraca warto�� danego pola, kt�r� mo�na wy�wietli� u�ytkownikowi. R�ni
	 * si� od getValue(cn) tylko dla p�l typu CLASS - zwraca opis danego
	 * obiektu, zamiast rzeczywistego obiektu.
	 * 
	 * @param cn
	 * @return
	 * @throws EdmException
	 */
	public Object getDescription(String cn) throws EdmException
	{
        Field field = documentKind.getFieldByCn(cn);
        if(field == null){
            return null;
        }
		if(Field.DOCLIST.equalsIgnoreCase(documentKind.getFieldByCn(cn).getType()))
		{
			return documentKind.getFieldByCn(cn).getName();
		}
		if (getValues().get(cn) == null)
			return null;
		else
		{
			log.trace("getDescription for : {}", cn);
			return getValues().get(cn).getDescription();
		}
	}

	/**
	 * Zwraca rzeczywist� warto�� danego pola (czyli tak� jaka jest w tabeli w
	 * bazie zapisana).
	 * 
	 * @param cn
	 * @return dla S�ownik�w multi zwaraca LinkedList<Long>
	 * @throws EdmException
	 */
	public Object getKey(String cn) throws EdmException
	{
		if (getValues() == null)
			return null;
		else if (getValues().get(cn) == null)
			return null;
		else
			return getValues().get(cn).getKey();
	}
	
    /**
	 * Zwraca Long je�li getKEy nie zwr�ci null
	 * 
	 * @param cn
	 * @return Long
	 * @throws pl.compan.docusafe.core.EdmException
	 */
     public Long getLongKey(String cn) throws EdmException
     {
         Object t;
         return ( t = getKey(cn)) == null ? null : Long.parseLong(t.toString());
     }
    /**
	 * Zwraca Float je�li getKEy nie zwr�ci null
	 * 
	 * @param cn
	 * @return Float
	 * @throws pl.compan.docusafe.core.EdmException
	 */
     public Float getFloatKey(String cn) throws EdmException
     {
         Object t;
         return ( t = getKey(cn)) == null ? null : Float.parseFloat(t.toString());
     }
    /**
	 * Zwraca String je�li getKEy nie zwr�ci null
	 * 
	 * @param cn
	 * @return String
	 * @throws pl.compan.docusafe.core.EdmException
	 */
     public String getStringKey(String cn) throws EdmException
     {
         Object t;
         return ( t = getKey(cn)) == null ? null : t.toString();
     }
     
    /**
	 * Zwraca Integer je�li getKEy nie zwr�ci null
	 * 
	 * @param cn
	 * @return Integer
	 * @throws pl.compan.docusafe.core.EdmException
	 */
     public Integer getIntegerKey(String cn) throws EdmException
     {
         Object t;
         return ( t = getKey(cn)) == null ? null : Integer.parseInt(t.toString());
     }
     
	/**
	 * Zwraca warto�� boolowsk� danego pola (zwraca false gdy pole = null)
	 * 
	 * @param cn
	 * @return
	 * @throws EdmException
	 * @throws ClassCastException
	 *             gdy pole nie jest boolem
	 */
	public boolean getBoolean(String cn) throws EdmException, ClassCastException
	{
		Boolean ret;
		if (getKey(cn) instanceof Integer)
			ret = ((Integer)getKey(cn)).equals(1)? true:false;
		else
			ret = (Boolean) getKey(cn);
		return ret != null && ret;
	}

	public String getMainFieldCn()
	{
		return documentKind.getMainFieldCn();
	}

	/**
	 * Zwraca warto�� pola w postaci obiektu EnumItem (ma sens tylko dla p�l
	 * ENUM i ENUM_REF)
	 */
	public EnumItem getEnumItem(String fieldCn) throws EdmException
	{
		Field field = getField(fieldCn);
		if (field == null) {
			throw new IllegalArgumentException("Brak pola o cn = " + fieldCn + " dost�pne pola : " + getFieldsCns());
		}

		Object key = getKey(fieldCn);
		if (key == null)
			return null;

		return field.getEnumItem((Integer) key);
	}

	/**
	 * Zwraca warto�� pola w postaci nazwy kodowej (cn) obiektu EnumItem (ma
	 * sens tylko dla p�l ENUM i ENUM_REF)
     * Zwraca null je�li pole by�o puste
	 */
	public String getEnumItemCn(String fieldCn) throws EdmException {
		EnumItem item = getEnumItem(fieldCn);
        return item == null ? null : item.getCn();
	}

	/**
	 * Wczytuje z bazy warto�ci poszczeg�lnych p�l. Je�li documentId r�wny null,
	 * to oznacza �e ten FieldsManager zosta� pobrany dla nowo tworzonego
	 * dokumentu, zatem wszystkie warto�ci b�d� null-ami.
	 * 
	 * Ta metoda moze prowadzic do Stack Overflow - czytaj opis do klasy (na gorze)
	 * 
	 * @throws EdmException
	 */
	private void loadData() throws EdmException
	{
        if(log.isTraceEnabled()){
            log.trace("initializing fields manager ", new Throwable());
        }

		/* if(values == null) */
		values = new HashMap<String, FieldValue>();

		if (documentId == null)
		{
			return;
		}

		PreparedStatement pst = null;
		try
		{
			pst = DSApi.context().prepareStatement("select * from " + documentKind.getTablename() + " where document_id = ?");
			pst.setLong(1, documentId);
			ResultSet rs = pst.executeQuery();
			if (rs.next())
			{
				List<Field> fields = documentKind.getFields();
				for (int i = 0; i < fields.size(); i++)
				{	
					values.put(fields.get(i).getCn(), getFieldValue(rs, fields.get(i))); // TODO : fatalne w skutkach, gdy istnieje (dosc naturalne biznesowo) zapetlenie linkow 	
				}
			}
		}
		catch (SQLException e)
		{
			log.error("", e);
			throw new EdmSQLException(e);
		}
		catch (HibernateException e)
		{
			log.error("", e);
			throw new EdmHibernateException(e);
		}
		finally
		{
			DSApi.context().closeStatement(pst);
		}
	}

	/**
	 * Zmienia/uaktualnia warto�ci p�l (tylko w pami�ci, nie w bazie)
	 * przekazanymi warto�ciami 'values'
	 */
	public void reloadValues(Map<String, Object> values) throws EdmException {
        checkNotNull(values, "values cannot be null");
		for (String fieldCn : values.keySet()) {
			// dodany poniewa� nie wszystkoie pola zwracaja wartosc
			try
			{
				Field field = getField(fieldCn);
				Object key = field.coerce(values.get(fieldCn));
				getValues().put(fieldCn, new FieldValue(field, key));
			}
			catch (Exception e)
			{
				log.error("Nie prze�adowa� warto�ci pola field_cn = " + fieldCn + " message = " + e.getMessage());
			}
		}
	}
	
	public void resetValues() throws EdmException {
		if(values != null) {
			for(Map.Entry<String, FieldValue> entry : values.entrySet()) {
				String fieldCn = entry.getKey();
				Field field = getField(fieldCn);
				if(field instanceof NonColumnField) {
					NonColumnField nonColumnField = (NonColumnField) field;
					if(nonColumnField.isResettable()) {
						Object key = nonColumnField.coerce(nonColumnField.reset());
						entry.setValue(new FieldValue(nonColumnField, key));
					}
				}
			}
		}
	}

	/**
	 * Tworzy i przekazuje map� z warto�ciami wszystkich p�l (czyli jest to
	 * "zrzut FieldsManagera do mapy").
	 * 
	 * @return
	 * @throws EdmException
	 */
	public Map<String, Object> getFieldValues() throws EdmException
	{
		Map<String, Object> values = new HashMap<String, Object>();
		for (Field field : getFields())
		{
			values.put(field.getCn(), getKey(field.getCn()));
		}
		return values;
	}

	/**
	 * Na podstawie wyniku otrzymanego z zapytania do bazy (pobieranego z 'rs')
	 * tworzony jest obiekt FieldValue odpowiadaj�cy za warto�� pola 'field'.
	 * 
	 * @param rs
	 * @param field
	 * @return
	 * @throws SQLException
	 * @throws EdmException
	 */
	private FieldValue getFieldValue(ResultSet rs, Field field) throws SQLException, EdmException
	{
		if (field.isMultiple())
		{
			// atrybut wielowarto�ciowy
			if (field instanceof CostCenterField || field instanceof AbstractDictionaryField || field instanceof DocumentUserDivisionSecondField || field instanceof AttachmentField)
			{
				// centra kosztowe, s�owniki, odbiorcy
                return field.provideFieldValue(documentId, rs);
			}
			else
			{
				// zwyk�y atrybut wielowarto�ciowy
                return Field.readMultiple(field, documentId, documentKind.getMultipleTableName());
			}
		}
		else
		{
            if(field instanceof ProcessVariableField) {
                return ((ProcessVariableField) field).provideFieldValue(documentId, activityId);
            }
            else if (Field.DOCLIST.equals(field.getType())) {
                return getDocListFieldValue(rs, (DocListField) field);
            } else {
                return field.provideFieldValue(documentId, rs);
            }
        }
	}
/**
 * TO jest tylko dla impulsa jak zmianimy na dwr to bedzie mozna usunac
 * @param rs
 * @param field
 * @return
 * @throws EdmException
 * @throws SQLException
 */
    private FieldValue getDocListFieldValue(ResultSet rs, DocListField field) throws EdmException, SQLException
	{
    	Map<String,Boolean> canEditMap = new HashMap<String, Boolean>();
		// dockind z ktorego pobieramy dokumenty
		DocumentKind docKind = DocumentKind.findByCn(field.getDockind());
		Map<String, Field> fMap = docKind.getDockindInfo().getDockindFieldsMap();
		for (String fieldCn : fMap.keySet())
		{
			fMap.get(fieldCn).checkPermissions();
		}
		Map<String, String> refValues = new HashMap<String, String>();

		Map<String, Field> thisFMap = documentKind.getDockindInfo().getDockindFieldsMap();

		List<EnumItem> enumItems = null;
		List<Field> brakeFields = null;
		boolean isExistEnumItems = false;
		// narazie zrobione tylko dla ILPL
		try
		{
			enumItems = ((DlLogic) docKind.logic()).getEnumItms(docKind);
			enumItems.addAll(((DlLogic) docKind.logic()).getEnumItms(docKind));
			brakeFields = ((DlLogic) docKind.logic()).getFields(docKind);
			isExistEnumItems = true;
		}
		catch (Exception e)
		{
			log.debug("", e);
			isExistEnumItems = false;
		}

		for (String rf : field.getRelatedFields().keySet())
		{
			Field descField = fMap.get(rf);
			String val = rs.getString(descField.getColumn());
			if (val != null)
			{
				refValues.put(rf, val);
			}
		}
		if (refValues.size() < 1)
			return new FieldValue(field, null, null);

		// refValue = rs.getString(descField.getColumn());
		List<DocListBean> doclistbeans = new ArrayList<DocListBean>();
		PreparedStatement ps = null;
		DocListItem root = null;
		try
		{
			StringBuilder select = new StringBuilder("select ");
			select.append("distinct doc.id, doc.title");
			for (String fieldCn : field.getEditFields())
			{
				select.append(", ");
				select.append("dsg." + fMap.get(fieldCn).getColumn());
			}
			if(brakeFields != null)
			{
				for (Field breakeField : brakeFields)
				{
					select.append(", ");
					select.append("dsg." + breakeField.getColumn());
				}
			}

			for (String groupBy : field.getGroupingBys())
			{
				select.append(", ");
				select.append("dsg." + fMap.get(groupBy).getColumn());
			}
			Integer operator = 0;
			boolean isAlertFromIsField = false;
			if (field.getAlertOperator() != null)
				operator = Condition.getOperatorAsInteger(field.getAlertOperator());
			// Dodanie warto�ci odpowiedzialnej za powiadomienie jesli jest
			// polem
			if (fMap.get(field.getAlertFromField()) != null)
			{
				select.append(", ");
				select.append("dsg." + fMap.get(field.getAlertFromField()).getColumn());
				isAlertFromIsField = true;
			}
			// wartosc z dokumentu jesli jest polem
			String alertValue;
			if (thisFMap.get(field.getAlertThisField()) != null)
			{
				alertValue = rs.getString(thisFMap.get(field.getAlertThisField()).getColumn());
			}
			else
			{
				alertValue = field.getAlertThisField();
			}

//			StringBuilder from = new StringBuilder(" from " + docKind.getTablename() + " dsg, ds_document doc");
//			StringBuilder where = new StringBuilder(" where " + "doc.id = dsg.document_id and doc.DELETED = 0 ");

//			boolean isFirstMulti = true;
//			boolean first = true;
//			if (refValues.keySet().size() > 0)
//				where.append(" and ( ");
//			for (String rf : refValues.keySet())
//			{
//				Field descField = fMap.get(rf);
//				if (descField.isMultiple())
//				{
//					if (isFirstMulti)
//					{
//						from.append(", ");
//						from.append(docKind.getMultipleTableName());
//						from.append(" multi ");
//						isFirstMulti = false;
//					}
//					if (!first)
//					{
//						where.append(" and ");
//						first = false;
//					}
//					else
//					{
//						first = false;
//					}
//					where.append("( multi.document_id = doc.id and multi.field_cn = '");
//					where.append(descField.getCn() + "'");
//					where.append(" and multi.field_val = ? ) ");
//				}
//				else
//				{
//					if (!first)
//					{
//						where.append(" and ");
//						first = false;
//					}
//					else
//					{
//						first = false;
//					}
//					where.append(" ( dsg." + descField.getColumn() + " = ? ) ");
//				}
//			}
//			if (refValues.keySet().size() > 0)
//				where.append(" )");
			StringBuilder fromWhereJoin = new StringBuilder(" from dsg_leasing dsg join ds_document doc on doc.id=dsg.document_id");
			if(getKey("NUMER_WNIOSKU") != null)
				fromWhereJoin.append(" join dsg_leasing_multiple_value multi on  multi.document_id=doc.id and multi.field_cn = 'NUMER_WNIOSKU' and multi.field_val = ").append(getKey("NUMER_WNIOSKU") );
			if(getKey("NUMER_UMOWY") != null)
				fromWhereJoin.append(" join dsg_leasing_multiple_value multi2 on multi2.document_id=doc.id and multi2.field_cn = 'NUMER_UMOWY' and multi2.field_val = ").append(getKey("NUMER_UMOWY"));
			fromWhereJoin.append(" where doc.DELETED = 0");
			System.out.println(select.toString() + fromWhereJoin);
			
			ps = DSApi.context().prepareStatement(select.toString()+ fromWhereJoin +" order by doc.id");

//			int count = 1;
//			for (String rf : refValues.keySet())
//			{
//				ps.setString(count, refValues.get(rf));
//				count++;
//			}
			String lastItem = null;
			if(field.getGroupingBys() != null && field.getGroupingBys().size() > 0)
			{
				lastItem = field.getGroupingBys().get(field.getGroupingBys().size() - 1);
			}

			Integer index = 0;
			String bg = field.getGroupingBys().get(index);
			Field groupField = fMap.get(bg);
			DocListItem dli = new DocListItem();
			dli.setParent(null);
			dli.setTitle(groupField.getName());
			dli.setId(DocListItem.getNextId());
			dli.setColumn(groupField.getColumn());
			dli.setBinderId(getDocumentId());
			if(field.getGroupingBys().size() <= 1)
			{
				dli.setLastChildrenFromEnum(groupField.getEnumItems(), index, field.getGroupingBys(), lastItem, fMap,getDocumentId());
			}
			else
			{
				dli.setChildrenFromEnum(groupField.getEnumItems(), index, field.getGroupingBys(), lastItem, fMap,getDocumentId());
			}
			
			root = dli;

			ResultSet rs2 = ps.executeQuery();
			while (rs2.next())
			{
				DocListBean doclistbean = new DocListBean();
				List<EditField> editFields = new ArrayList<EditField>();

				for (String fieldCn : field.getEditFields())
				{
					EditField ef = new EditField();
					ef.setField(fMap.get(fieldCn));
					ef.setCanEdit(isCanEdit(fMap.get(fieldCn), canEditMap));
					ef.setValue(rs2.getString(fMap.get(fieldCn).getColumn()));
					editFields.add(ef);
				}
				if (isExistEnumItems)
				{
					for (Field f : brakeFields)
					{
						String val = rs2.getString(f.getColumn());
						if (val != null)
						{
							enumItems.remove(f.getEnumItem(new Integer(val)));
						}
					}
				}

				doclistbean.setEditFields(editFields);
				doclistbean.setTitle(rs2.getString(2));
				doclistbean.setId(rs2.getLong(1));
				if (field.isShowAttachment())
				{
					Attachment atta = Attachment.findFirstAttachment(rs2.getLong(1));
					if (atta != null)
						doclistbean.setAttachmentRevision(atta.getMostRecentRevision());
				}
				boolean isAlert = false;
				if (field.getAlertFromField() != null)
				{
					String docAlertValue;
					if (isAlertFromIsField)
						docAlertValue = rs2.getString(fMap.get(field.getAlertFromField()).getColumn());
					else
						docAlertValue = field.getAlertFromField();
					
					
					if (docAlertValue != null)
					{  
						Integer result = alertValue.compareTo(docAlertValue); // docAlertValue.compareTo(alertValue);
						if (((operator < 0 && result < 0) || (operator > 0 && result > 0) || (operator == 0 && result == 0)))
						{
							isAlert = true;
						}
					}
					else
					{
						isAlert = true;
					}
				}
				DocListItem thisITM = root.getLeaf(rs2, isAlert);
				thisITM.getDoclistbeans().add(doclistbean);
				thisITM.containsDoc();
				doclistbeans.add(doclistbean);
			}
			if (isExistEnumItems)
			{
				Set<EnumItem> ei = new HashSet<EnumItem>(enumItems);
				List<EnumItem> eil = new ArrayList<EnumItem>(ei);
				Collections.sort(eil, new EnumItem.EnumItemComparator());
				field.setBrakeEnumItems(eil);
			}
		}
		catch (SQLException e)
		{
			log.error("", e);
			throw new EdmSQLException(e);
		}
		catch (HibernateException e)
		{
			log.error("", e);
			throw new EdmHibernateException(e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return new FieldValue(field, root, "Dokumenty");
	}
    
/////////////////////////do uprawnien chwilowo start
    @Deprecated
    public boolean checkPermissions(Field field)
    {   
    	boolean canEdit = false;
        if (field.isReadOnly())
        {
            return false;
        }
        if (DSApi.context().isAdmin())
        {
            return true;
        }
        else
            canEdit = false;
        if (field.getEditPermissions() != null && field.getEditPermissions().size() > 0)
        {
        	try
        	{
	        	DSDivision[] divisions = DSUser.findByUsername(DSApi.context().getPrincipalName()).getDivisions();
	        	
	            for (DSDivision division : divisions)
	            {
	                if (field.getEditPermissions().contains(division.getGuid()))
	                {
	                    canEdit = true;
	                    break;
	                }
	            }
        	}
        	catch (EdmException e) {
				log.error(e.getMessage(),e);
			}
        }
        else
        {
            canEdit = true;
        }
        return canEdit;
    }
    @Deprecated
    private boolean isCanEdit(Field field,Map<String,Boolean> canEditMap)
    {
    	if(canEditMap.get(field.getCn()) == null)
    		canEditMap.put(field.getCn(), checkPermissions(field));
    	return canEditMap.get(field.getCn());
    }
    
/////////////////////////do uprawnien chwilowo koniec

    public String getShemeColor(String cn)
	{
//		if(log.isTraceEnabled()){
			log.info("getSchemeColor('{}') = '{}'",cn, dockindScheme.getSchemeFields().get(cn));
//		}
		return dockindScheme.getSchemeFields().get(cn).getColor();
	}

    /**
     * Zwraca atrybuty danego pola (w formie "style ='...' x='...'")
     * @param cn
     * @param canEdit
     * @return
     */
	public String getStyle(String cn, boolean canEdit) {
		StringBuilder style = new StringBuilder("");
		StringBuilder other = new StringBuilder("");
		SchemeField sf = null;
		try {
			if (dockindScheme != null){
				sf = dockindScheme.getSchemeFields().get(cn);
            }
			if (sf != null) {
				style.append("style=\"");
				if (sf.getColor() != null) {
					style.append("background:#" + sf.getColor() + ";");
                }
				if (!sf.isVisible()) {
					style.append("display:none;");
				} else if (sf.getDisabled()) {
					other.append(" disabled=\"disabled\"");
					style.append("background:#ffffc4;");
				}
				style.append("\"");
//                log.info("getStyle('{}') == {}", cn, style);
			}
			if (!canEdit) {
				other.append(" readonly=\"readonly\"");
			}
		}
		catch (Exception e)
		{
			log.error("", e);
		}

		if(log.isTraceEnabled()){
			log.trace("getSchemeStyle('{}') = '{}'",cn, style.append(other).toString());
		}

		return style.append(other).toString();
	}

	public String getShowAs(String cn)
	{
		return dockindScheme.getSchemeFields().get(cn).getShowAs();
	}

	/**
	 * Zwraca ju� stringa kt�rego nale�y wstawi� w style po atrybucie display:
	 * np: display:none
	 * 
	 * @param cn
	 * @return
	 */
	public String getShemeDisplay(String cn) {
		if(log.isTraceEnabled()){
			log.trace("getSchemeDisplay('{}') = '{}'",cn, getShemeVisible(cn) ? "" : "none");
		}
		return getShemeVisible(cn) ? "" : "none";
	}
	
	/**
	 * Zwraca ju� stringa kt�rego nale�y wstawi� w style po atrybucie display:
	 * np: display:none
	 * 
	 * @param cn
	 * @return
	 */
	public boolean getShemeVisible(String cn) {
		if(log.isTraceEnabled()){
			log.debug("getSchemeDisplay('{}') = '{}'",cn, dockindScheme.getSchemeFields().get(cn).isVisible() ? "" : "none");
		}

		SchemeField sf = null;

		if (dockindScheme != null) {
			sf = dockindScheme.getSchemeFields().get(cn);
        }

        return sf == null || sf.isVisible();
    }

    public boolean containsField(Object cn){
        return values.containsKey(cn);
    }

    public Set<String> getFieldsCns(){
        return values.keySet();
    }

	public boolean getDisabled(String cn)
	{
		return dockindScheme != null
                && dockindScheme.getSchemeFields().containsKey(cn)
                && dockindScheme.getSchemeFields().get(cn).getDisabled();
	}

	public Long getDocumentId()
	{
		return documentId;
	}

	public DocumentKind getDocumentKind()
	{
		return documentKind;
	}

	public AcceptancesDefinition getAcceptancesDefinition()
	{
		return documentKind.getAcceptancesDefinition();
	}

	public AcceptancesState getAcceptancesState()
	{
		return acceptanceState;
	}

	public boolean getEnableSheme(String cn){
        boolean ret = dockindScheme != null && dockindScheme.getSchemeFields().get(cn) != null;
        if(log.isTraceEnabled()){
			log.debug("getEnableSheme('{}') = '{}'",cn, ret);
		}
		return ret;
	}

	public DockindScheme getDockindScheme() {
		return dockindScheme;
	}

	public void setDockindScheme(DockindScheme dockindScheme) {
		this.dockindScheme = dockindScheme;
	}
}
