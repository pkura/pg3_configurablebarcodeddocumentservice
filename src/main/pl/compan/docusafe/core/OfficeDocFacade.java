package pl.compan.docusafe.core;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * Fasada u�atwiaj�ca dost�p do dokument�w kancelaryjnych
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface OfficeDocFacade extends DocFacade {
    /**
     * Zwraca dany dokument kancelaryjny (mo�liwe lazy initialization)
     * @return instancja OfficeDocument
     * @throws EdmException np w przypadku op�nionej inicjalizacji
     */
    OfficeDocument getDocument() throws EdmException;
    
    OfficeFacade getOffice();
}
