package pl.compan.docusafe.core.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.BlobInputStream;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Resource.java,v 1.9 2009/07/14 14:21:16 mariuszk Exp $
 */
public class Resource
{
    private Long id;
    private String systemName;
    private String fileName;
    private String mime;
    private int size;

    public static Resource findResource(String systemName) throws EdmException
    {
        try
        {
            List res = DSApi.context().classicSession().find(
                "select res from res in class "+Resource.class.getName()+
                " where res.systemName = ?", systemName, Hibernate.STRING);
            if (res.size() > 0)
                return (Resource) res.get(0);
            throw new EntityNotFoundException("Nie znaleziono zasobu "+systemName);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Resource create(String systemName, String filename, String mime,
                                  InputStream is, int size)
        throws EdmException
    {
        try
        {
            Resource resource = new Resource();
            resource.setSystemName(systemName);
            resource.setFileName(filename);
            resource.setMime(mime);
            resource.setSize(size);
            DSApi.context().session().save(resource);
            DSApi.context().session().flush();

            resource.update(is, size);

            return resource;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Resource find(Long id) throws EdmException
    {
        return (Resource) Finder.find(Resource.class, id);
    }

    /**
     * Pobiera strumie� zawieraj�cy dane z za��cznika binarnego.
     * Metoda powinna by� wywo�ywana tylko i wy��cznie w ramach
     * transakcji, w innym przypadku niekt�re sterowniki JDBC
     * b�d� pr�bowa�y wczyta� ca�� tre�� bloba do pami�ci.
     * <p>
     * Zwracany strumie� zawiera w sobie obiekt Statement u�yty
     * do odczytania BLOB-a, dlatego powinien by� zawsze zamykany
     * przy pomocy metody closeStream(). Metoda close() rzuca
     * wyj�tek.
     * @return
     * @throws EdmException
     */
    public BlobInputStream getBinaryStream()
        throws EdmException
    {
    	boolean closeStm = true;
        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select contentdata from ds_resource where id = ?");
            pst.setLong(1, getId());
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
                throw new EdmException("Nie znaleziono zasobu "+id);

            InputStream is = rs.getBinaryStream(1);
            // sterownik dla SQLServera nie wspiera ResultSet.getBlob
            //Blob blob = rs.getBlob(1);
            if (is != null)
            {
            	closeStm = false;
                return new BlobInputStream(is, pst);
            }
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally 
        {
        	if (closeStm)
        	{
        		DSApi.context().closeStatement(pst);
        	}
        }
        // Nie mo�na zamkn�� obiektu Statement od razu po zwr�ceniu strumienia,
        // poniewa� w�wczas nie b�dzie mo�liwe odczytanie danych ze strumienia.
        // Statement jest zamykane w metodzie BlobInputStream.closeStream()
    }

    public void update(InputStream is, int size) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            int rows;
            if (DSApi.isOracleServer())
            {
                ps = DSApi.context().prepareStatement(
                    "select contentdata from "+DSApi.getTableName(Resource.class)+
                    " where id = ? for update");
                ps.setLong(1, id.longValue());

                ResultSet rs = ps.executeQuery();
                if (!rs.next())
                    throw new EdmException("Nie mo�na odczyta� tre�ci zasobu "+id);

                rows = 1;

                Blob blob = rs.getBlob(1);
                if (!(blob instanceof oracle.sql.BLOB))
                    throw new EdmException("Spodziewano si� obiektu oracle.sql.BLOB");

                OutputStream out = ((oracle.sql.BLOB) blob).getBinaryOutputStream();
                byte[] buf = new byte[2048];
                int count;
                while ((count = is.read(buf)) > 0)
                {
                    out.write(buf, 0, count);
                }
                out.close();
                org.apache.commons.io.IOUtils.closeQuietly(is);
            }
            else
            {
                ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(Resource.class)+
                    " set contentdata = ? where id = ?");
                ps.setBinaryStream(1, is, size);
                ps.setLong(2, id.longValue());
                rows = ps.executeUpdate();
                org.apache.commons.io.IOUtils.closeQuietly(is);
            }

            if (rows != 1)
                throw new EdmException("Nie uda�o si� zmodyfikowa� zasobu "+id);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas aktualizacji zasobu "+id, e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getSystemName()
    {
        return systemName;
    }

    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public String getMime()
    {
        return mime;
    }

    public void setMime(String mime)
    {
        this.mime = mime;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }
}
