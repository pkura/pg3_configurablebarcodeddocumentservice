package pl.compan.docusafe.core.repository;

import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class SimpleFileSystemRepository implements Repository{
    private final static Logger log = LoggerFactory.getLogger(SimpleFileSystemRepository.class);

    private final File root;

    public SimpleFileSystemRepository(File rootDirectory) {
        Preconditions.checkNotNull(rootDirectory, "rootDirectory cannot be null");
        Preconditions.checkArgument(rootDirectory.isDirectory(), "'"+rootDirectory.getAbsolutePath()+"' must be existing directory");
        this.root = rootDirectory;
    }

    public SimpleFileSystemRepository(File rootDirectory, boolean createDir) {
        log.debug("[SimpleFileSystemRepository] rootDirectory = {}, createDir = {}", rootDirectory, createDir);
        Preconditions.checkNotNull(rootDirectory, "rootDirectory cannot be null");

        boolean exists = rootDirectory.exists();
        boolean isDirectory = rootDirectory.isDirectory();
        log.debug("[SimpleFileSystemRepository] rootDirectory.exists = {}, rootDirectory.isDirectory = {}, createDir = {}", exists, isDirectory, createDir);

        rootDirectory.mkdirs();

        Preconditions.checkArgument(rootDirectory.isDirectory(), "'"+rootDirectory.getAbsolutePath()+"' must be directory");

//        if(! isDirectory && createDir) {
//            if(exists) {
//                Preconditions.checkArgument(rootDirectory.isDirectory(), "'"+rootDirectory.getAbsolutePath()+"' must be directory");
//            } else {
//                log.debug("[SimpleFileSystemRepository] mkdir");
//                final boolean mkdir = rootDirectory.mkdirs();
//
//                if(! mkdir) {
//                    log.error("[SimpleFileSystemRepository] cannot create directory");
//                }
//            }
//        } else {
//            Preconditions.checkArgument(rootDirectory.isDirectory(), "'"+rootDirectory.getAbsolutePath()+"' must be existing directory");
//        }

        this.root = rootDirectory;
    }

    public boolean contains(String name) {
        return new File(root, name).isFile();
    }

    public boolean add(String name, InputStream content) {
        Preconditions.checkArgument(!contains(name), "already stored data with name :" + name);
        return store(name, content);
    }

    public boolean store(String name, InputStream content) {
        File file = new File(root, name);
        OutputStream os = null;
        try {
            os = new FileOutputStream(file);
            IOUtils.copy(content, os);
        } catch (Exception ex) {
            log.error("[store] error", ex);
            return false;
        } finally {
            IOUtils.closeQuietly(os);
        }
        return true;
    }

    public InputStream get(String name) {
        if(contains(name)){
            try {
                return new FileInputStream(new File(root, name));
            } catch (FileNotFoundException ex) {
                log.warn("file not found in repository:" + ex.getMessage(), ex);
                return null;
            }
        } else {
            return null;
        }
    }

    public boolean remove(String name) {
        final File file = new File(root, name);

        if(log.isDebugEnabled()) {
            log.debug("[remove] name = {}, exists = {}, isFile = {}", name, file.exists(), file.isFile());
        }

        if(file.exists() && file.isFile()) {
            return file.delete();
        }
        return false;
    }

    public List<String> listAsString() {
        List<String> ret = new ArrayList<String>();

        final File[] files = root.listFiles();

        if(files == null) {
            log.warn("[listAsString] files == null");
            return ret;
        }

        for(File f: files) {
            ret.add(f.getName());
        }

        return ret;
    }

    public boolean delete() {
        try {
            FileUtils.deleteDirectory(root);
            return true;
        } catch (IOException e) {
            log.error("[delete] cannot delete root = {}", root);
            return false;
        }
    }
}
