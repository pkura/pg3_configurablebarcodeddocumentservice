package pl.compan.docusafe.core.repository;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.templating.RtfTemplating;
import pl.compan.docusafe.core.templating.TemplateEngine;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class RepositoryProvider {
    private final static Logger log = LoggerFactory.getLogger(RepositoryProvider.class);

    private static Repository templateRepository;

    private RepositoryProvider(){
    }

    public static InputStream getRepositoryFile(String name)
    {
    	return templateRepository.get(name);
    }
    
    public static void init() throws EdmException {
        log.info("Inicjalizacja repozytori�w");

        try {
            templateRepository = new SimpleFileSystemRepository(new File(Docusafe.getHome(), "templates"));
        } catch (Exception ex) {
            log.warn("Inicjalizacja by�a niemo�liwa - przyczyna : " + ex.getMessage(), ex);
        }

//        test();
    }

    public static Repository getTemplateRepository(){
        return templateRepository;
    }

    public static SimpleFileSystemRepository getDocumentRepository(Long documentId, Long snapshotId) throws EdmException {
        SimpleFileSystemRepository ret;
        try {
            ret = new SimpleFileSystemRepository(new File(Docusafe.getHome()+"/templates/documents/"+documentId.toString(), snapshotId.toString()), true);
        } catch(Exception ex) {
            log.error("[getDocumentRepository] cannot load document repository for documentId = {}, snapshotId = {}", documentId, snapshotId, ex);
            throw new EdmException("Nie mo�na pobra� listy dokument�w", ex);
        }

        return ret;
    }

    public static SimpleFileSystemRepository getUserTemplateRepository(String username, String dockind) throws EdmException {
        SimpleFileSystemRepository ret;
        try {
            ret = new SimpleFileSystemRepository(new File(Docusafe.getHome()+"/templates/user_templates/"+username, dockind), true);
        } catch(Exception ex) {
            log.error("[getUserTemplateRepository] cannot load user repository for dockind = {}, user = {}", dockind, username, ex);
            throw new EdmException("Nie mo�na pobra� listy plik�w", ex);
        }

        return ret;
    }

    public static SimpleFileSystemRepository getGlobalTemplateRepository(String dockind) throws EdmException {
        SimpleFileSystemRepository ret;
        try {
            ret = new SimpleFileSystemRepository(new File(Docusafe.getHome()+"/templates/global_templates/", dockind), true);
        } catch(Exception ex) {
            log.error("[getGlobalTemplateRepository] cannot load user repository for dockind = {}", dockind, ex);
            throw new EdmException("Nie mo�na pobra� listy plik�w", ex);
        }

        return ret;
    }

    private static void test() {
        try {
            InputStream is = templateRepository.get("UmowaZamowienie.rtf");
            if(is != null){
                log.info("znaleziono plik template.txt");
//                TemplateEngine te = new TextTemplating("UTF8");
                TemplateEngine te = new RtfTemplating();
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                te.process(is, os, Collections.singletonMap("name", "\\��\\���\\��\\�k��"));
                os.flush();
                templateRepository.store("result.rtf", new ByteArrayInputStream(os.toByteArray()));
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }
    
    public static void createDocument(Map<String, Object> values, String fileName)
    {
        try {
            InputStream is = templateRepository.get(fileName + ".rtf");
            if(is != null){
                log.info("znaleziono plik template " + fileName + ".rtf");
                TemplateEngine te = new RtfTemplating();
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                te.process(is, os, values);
                os.flush();
                templateRepository.store(fileName + "Result.rtf", new ByteArrayInputStream(os.toByteArray()));
                is.close();
                os.close();
        }
        } catch (Exception ex) 
        {
            log.error(ex.getMessage(), ex);
        }
    }
}
