package pl.compan.docusafe.core.repository;

import java.io.InputStream;

/**
 * Obiekt s�u��cy do zapisywania binarnych danych
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface Repository {

    boolean contains(String name);
    boolean add(String name, InputStream content);
    boolean store(String name, InputStream content);

    /**
     * Daje dost�p do strumienia danych z pliku w repozytorium
     * @param name
     * @return InputStream (nale�y go zamkn��), null gdy plik nie istnieje
     */
    InputStream get(String name);
}
