package pl.compan.docusafe.core.alfresco;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Property;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Obsluga encji DockindMappedFieldStore
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class DockindMappedFieldStore implements Serializable {
    private final static Logger log = LoggerFactory.getLogger(DockindMappedFieldStore.class);

    private static Criteria getCriteria() throws EdmException {
        return DSApi.context().session().createCriteria(DockindMappedField.class);
    }

    public static List<DockindMappedField> findByDockindCn(String dockindCn) throws EdmException {
         return getCriteria().add(Restrictions.eq("dockindCn", dockindCn)).list();
    }

    /**
     * Ustawia do mapy warto�ci dla danego dockindu
     * @param dockindCn
     * @param cmisObj Obiekt alfresco z kt�rego pobieramy parametry
     * @param values mapa wartosci do ustawienia
     * @throws EdmException
     */
    public static void setDockindMappedFields(String dockindCn, CmisObject cmisObj, Map<String, Object> values) throws EdmException {
        Preconditions.checkNotNull(cmisObj, "Obiekt alfresco nie mo�e by� pusty!");
        List<DockindMappedField> dmfs = findByDockindCn(dockindCn);
        log.error("prop {}", cmisObj.getProperties());
        Map<String, Property<?>> properties = asMapProperties(cmisObj.getProperties());

        for(DockindMappedField entry : dmfs){
            log.trace("dockindMappedField={}", entry);
            if(!properties.containsKey(entry.getExternalField())){
                log.error("Obiekt alfresco nie posiada parametru '" + entry.getExternalField() + '\'');
                continue;
            }

            Property prop = properties.get(entry.getExternalField());
            values.put(entry.getDockindField(), prop.getValue() );
        }

        log.info("values result = {}", values.toString());
    }

    /**
     * Konweruje list� parametr�w alfresco do mapy
     * @param properties
     * @return
     */
    private static Map<String, Property<?>> asMapProperties(List<Property<?>> properties) {
        Map<String, Property<?>> dmfs = Maps.newHashMapWithExpectedSize(properties.size());
        for(Property<?> mf : properties){
            log.trace("{}", mf);
            dmfs.put(mf.getLocalName(), mf);
        }
        return dmfs;
    }

    /**
     * Konwertuje na mape
     * @param byDockindCns
     * @return
     */
    private static Map<String, DockindMappedField> asMap(List<DockindMappedField> byDockindCns) {
        Map<String, DockindMappedField> dmfs = Maps.newHashMapWithExpectedSize(byDockindCns.size());
        for(DockindMappedField mf : byDockindCns){
            dmfs.put(mf.getExternalField(), mf);
        }
        return dmfs;
    }
}
