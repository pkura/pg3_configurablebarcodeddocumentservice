package pl.compan.docusafe.core.alfresco;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Encja ma za zadanie mapowa� pola z Alfresco do dockindowych.
 * Mo�na u�y� do innych mechanizm�w importuj�cych z zewn�trznych system�w
 *
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
@Entity
@Table(name = "dsa_mapped_field")
public class DockindMappedField implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "mapping_type")
    private MappingType mappingType = MappingType.ALFRESCO;

    @Column(name = "dockind_cn")
    private String dockindCn;

    @Column(name = "dockind_field_cn")
    private String dockindField;

    @Column(name = "external_field_cn")
    private String externalField;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MappingType getMappingType() {
        return mappingType;
    }

    public void setMappingType(MappingType mappingType) {
        this.mappingType = mappingType;
    }

    public String getDockindCn() {
        return dockindCn;
    }

    public void setDockindCn(String dockindCn) {
        this.dockindCn = dockindCn;
    }

    public String getDockindField() {
        return dockindField;
    }

    public void setDockindField(String dockindField) {
        this.dockindField = dockindField;
    }

    public String getExternalField() {
        return externalField;
    }

    public void setExternalField(String externalField) {
        this.externalField = externalField;
    }

    @Override
    public String toString() {
        return "DockindMappedField{" +
                "id=" + id +
                ", mappingType=" + mappingType +
                ", dockindCn='" + dockindCn + '\'' +
                ", dockindField='" + dockindField + '\'' +
                ", externalField='" + externalField + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DockindMappedField)) return false;

        DockindMappedField that = (DockindMappedField) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public enum MappingType{
        ALFRESCO;
    }
}
