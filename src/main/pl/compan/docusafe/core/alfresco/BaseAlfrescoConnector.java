package pl.compan.docusafe.core.alfresco;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Map;

/**
 * Podstawowa klasa sluzaca do zestawienia polaczenia do Alfresco
 * Lukasz Wozniak <lukasz.wozniak@docusafe.pl>.
 */
public class BaseAlfrescoConnector extends AlfrescoConnector{

    private final static Logger log = LoggerFactory.getLogger(BaseAlfrescoConnector.class);


    private static final String _ALFRESCO_USER = "alfresco.user";
    private static final String _ALFRESCO_PASSWORD = "alfresco.password";
    private static final String _ALFRESCO_URL = "alfresco.url";
    private static final String _ALFRESCO_FACTORY_CLASS = "alfresco.factory";


    BaseAlfrescoConnector() {
    }

    protected static String getUser()
    {
        return Docusafe.getAdditionProperty(_ALFRESCO_USER);
    }

    protected static String getPassword()
    {
        return Docusafe.getAdditionProperty(_ALFRESCO_PASSWORD);
    }

    protected static String getUrl()
    {
        return Docusafe.getAdditionProperty(_ALFRESCO_URL);
    }

    protected static String getFactory()
    {
        return Docusafe.getAdditionProperty(_ALFRESCO_FACTORY_CLASS);
    }

    /**
     * Zwraca parametry polaczenia
     * @return parametry polaczenia
     */
    protected Map<String, String> getConnectionParameters()
    {
//        Preconditions.checkNotNull(getUser(), "Uzytkownik do polaczenia z Alfresco Api jest pusty!");
//        Preconditions.checkNotNull(getPassword(),"Haslo do polaczenia z Alfresco Api jest puste!");
        Preconditions.checkNotNull(getUrl(),"Brak adresu url Alfresco Api!");
        Map<String, String> parameter = Maps.newHashMap();

        // Set the user credentials
        parameter.put("cmis.workbench.folder.includeAcls","false");
        parameter.put("cmis.workbench.object.includeAcls","false");
        parameter.put(SessionParameter.USER, getUser());
        parameter.put(SessionParameter.PASSWORD, getPassword());

        // Specify the connection settings
        parameter.put(SessionParameter.ATOMPUB_URL, getUrl());
        parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
        // Set the alfresco object factory
        parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.apache.chemistry.opencmis.client.runtime.repository.ObjectFactoryImpl");

        return parameter;
    }
}
