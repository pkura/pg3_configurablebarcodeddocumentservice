package pl.compan.docusafe.core.alfresco;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.CharSetUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Obiekt mapy przechowuj�cy propertisy z pomocniczymi metodami.
 * Zawiera sta�e zdefiniowane nazwy kluczy dla metryki dokumentow wysylanych do Asseco REST API
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class AlfrescoProperties extends HashMap<String, Object>
{

    private final static Logger log = LoggerFactory.getLogger(AlfrescoProperties.class);
	private static final long serialVersionUID = -3681539110156174612L;
    
	/**
	 * Zwraca null je�li nie ustawi
	 * @param key
	 * @param val
	 * @return
	 */
	public Object putDateValue(String key, Date val)
	{
		if(val != null)
			return put(key, val);
		
		return null;
	}
	
	/**
	 * zwraca null je�li warto�� jest pusta. Nie dodaje.
	 * @param key
	 * @param val
	 * @return
	 */
	public Object putIntValue(String key, String val)
	{
		if(StringUtils.isNotBlank(val))
			return put(key, Integer.valueOf(val));
		
		return null;
		
	}
	
	public Object putLongValue(String key, String val)
	{
		if(StringUtils.isNotBlank(val))
			return put(key, Long.valueOf(val));
		
		return null;
		
	}
	
	/**
	 * Zwraca null je�li nie doda i warto�� jest pusta;
	 * @param key
	 * @param val
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public Object putStringValue(String key, String val)
	{
		try
		{
			if(StringUtils.isNotBlank(val))
				return put(key, convertToRequestCharset(val));
		}catch(Exception e){
			return put(key, val);
		}
		return null;
	}

    public static String convertToRequestCharset(String ss){
        String s = null;
        try
        {
            s = new String(ss.getBytes(AlfrescoUtils.getRequestCharset()));
        }catch(Exception e){
            log.error("", e);
        }
        return s;
    }
}
