package pl.compan.docusafe.core.alfresco;

import com.google.common.collect.Maps;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.PropertyId;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.commons.io.FileUtils;
import org.apache.tika.io.IOUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * Klasa zawiera pomocnicze metody nie zwiazane bezposrednio z interfejsem.
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class AlfrescoUtils {
    private final static Logger log = LoggerFactory.getLogger(AlfrescoUtils.class);

    private static StringManager sm = GlobalPreferences.loadPropertiesFile(AlfrescoUtils.class.getPackage().getName(), null);
    private static final String _FOLDER_SLASH = "/";
    public static final String ALFRESCO_FILE_NAME = "cmis:name";

    /**
     * Dodaje do historii pisma informacje o aktualizacji zalacznika w Alfresco
     * @param document
     * @param att
     * @throws Exception
     */
    public static void addAttachmentUpdateHistoryEntry(Document document, Attachment att) throws Exception
    {
        String fileName = att.getTitle();
        log.info("Aktualizuje zalacznik dokumentu w Alfresco {}", document.getId());
        addAttachmentHistoryEntry(document, sm.getString("ZaktualizowanoZalacznikWAlfresco", att.getTitle()));
    }

    /**
     * Dodaje do historii pisma informacje o aktualizacji zalacznika w Alfresco
     * @param document
     * @param att
     * @throws Exception
     */
    public static void addAttachmentCreateHistoryEntry(Document document, Attachment att, String uuid) throws Exception
    {
        String fileName = att.getTitle();
        log.info("Zapisuje zalacznik dokumentu w Alfresco {}", document.getId());
        addAttachmentHistoryEntry(document, sm.getString("ZapisanoZalacznikWAlfresco", att.getTitle(), uuid));
    }


    /**
     * Dodaje do historii pisma informacje o utworzeniu dokumentu w alfresco
     * @param document
     * @param uuid
     * @throws Exception
     */
    public static void addAttachmentCreateHistoryEntry(Document document, String uuid) throws Exception{
        String title = document.getTitle();
        AlfrescoUtils.addAttachmentHistoryEntry(document, sm.getString("ZapisanoDokumentWAlfresco", title, uuid));
    }

    /**
     * Dodaje wpis do historii pisma o utworzeniu za��cznika w Alfrescoo
     * @param document
     * @param text
     * @throws Exception
     */
    private static void addAttachmentHistoryEntry(Document document, String text) throws Exception
    {
        DataMartManager.addHistoryEntry(document.getId(), Audit.create("AlfrescoCreatedocument", DSApi.context().getPrincipalName(), text));
    }

    public static ContentStream getContentStream(Attachment att) throws Exception {
        return getContentStream(att.getMostRecentRevision());
    }
    /**
     * Zwraca content za��cznika do przekazana do Alfresco
     * @param ar
     * @return
     * @throws Exception
     */
    public static ContentStream getContentStream(AttachmentRevision ar) throws Exception
    {
        File f = ar.saveToTempFile();
        try{
            return getContentStream(f, ar.getMime(), ar.getAttachment().getTitle(), ar.getSize());
        }finally{
            FileUtils.deleteQuietly(f);
        }
    }

    public static ContentStream getContentStream(File f, String mime, String fileName, Integer size) throws EdmException, IOException {
        byte[] content = FileUtils.readFileToByteArray(f);
        InputStream stream = new ByteArrayInputStream(content);
        ContentStream contentStream = null;

        try{
            contentStream = new ContentStreamImpl(fileName, BigInteger.valueOf((long) size), mime, stream);
        }catch(Exception e){
            log.error("", e);
        }finally{
            IOUtils.closeQuietly(stream);
        }

        return contentStream;
    }


    public static String getRequestCharset(){
        return Docusafe.getAdditionPropertyOrDefault("alfresco.request.charset", "UTF-8");
    }

    /**
     * Mapuje warto�ci dockinda na map� wartosci do wys�ana do alfresco
     * @param document
     * @return
     * @throws EdmException
     */
    public static Map<String, Object> buildDocumentProperties(Document document) throws EdmException {
        AlfrescoProperties properties = newAlfrescoProperties(document);
        List<DockindMappedField> fields = DockindMappedFieldStore.findByDockindCn(document.getDocumentKind().getCn());
        FieldsManager fm = document.getFieldsManager();

        for(DockindMappedField f : fields){
            properties.put(addPrefix(f.getExternalField()), (String) fm.getValue(f.getDockindField()));
        }

        log.error("prop {}", properties);
        return properties;
    }

    private static AlfrescoProperties newAlfrescoProperties(Document document) {
        AlfrescoProperties alfrescoProperties = new AlfrescoProperties();
        alfrescoProperties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
        return alfrescoProperties;
    }

    private static String addPrefix(String externalField) {
        return Docusafe.getAdditionPropertyOrDefault("alfresco.property.prefix","cmis:") + externalField;
    }

    /**
     * MEtoda s�u�y do obliczenia wielko�ci za��cznika, pobranego z Alfresco.
     * Zdarza si� tak w wersjach alfresco, �e Cmis Alfrescowy zamiast poprawnie obliczonej wielko�ci pliku,
     * zwraca warto��c 0 lub ujemn�. Dlatego sami musimy to policzyc
     * @param contentStream
     * @param stream
     * @return
     */
    public static int countStreamSize(ContentStream contentStream, InputStream stream) {
        int result = 0;
        try
        {
            //liczba zwracana przez alfresco
            int def = ((Long)contentStream.getLength()).intValue();
            log.error("Wielkosc pliku {} -> {}", contentStream.getLength(), def);
            if(def <=0){
                byte[] bytes = IOUtils.toByteArray(stream);
                result = bytes.length;
            }
        }catch(Exception e){
            log.error("blad pobierania wielosci pliku z alfresco: {}", contentStream.getBigLength());
        }

        return result;
    }
}
