package pl.compan.docusafe.core.alfresco;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.filter.CasUtils;

import java.util.Map;

/**
 * Configuruje parametry polaczenia do alfresco w przypadku gdy uzywany
 * jest CAS.
 *
 * Lukasz Wozniak <lukasz.wozniak@docusafe.pl>.
 */
public class CasProxyAlfrescoConnector extends BaseAlfrescoConnector {

    private final static Logger log = LoggerFactory.getLogger(BaseAlfrescoConnector.class);
    CasProxyAlfrescoConnector() {
    }

    /**
     * Zwraca parametry polaczenia
     * @return parametry polaczenia
     */
    protected Map<String, String> getConnectionParameters()
    {
        Map<String, String> parameter = super.getConnectionParameters();

        if(AvailabilityManager.isAvailable("cas.proxy.alfresco.passwords")){
            String username = DSApi.context().getPrincipalName();
            String password = getPassword(username);
            log.debug("username {}/{}", username, password);
            parameter.put(SessionParameter.USER, username);
            parameter.put(SessionParameter.PASSWORD, password);
        } else {
            parameter.put(SessionParameter.USER, Docusafe.getAdditionPropertyOrDefault("alfresco.cas-proxy.username",""));
            parameter.put(SessionParameter.PASSWORD, CasUtils.getTicket());
        }

        return parameter;
    }

    private String getPassword(String username) {
        return AdditionManager.getPropertyAsMap("alfresco.auth").get(username);
    }
}
