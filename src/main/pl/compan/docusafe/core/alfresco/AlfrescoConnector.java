package pl.compan.docusafe.core.alfresco;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.springframework.web.context.request.RequestContextListener;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * Bazowa kalsa odpowiedzialna za zestawienie polaczenia do alfresco
 * Lukasz Wozniak <lukasz.wozniak@docusafe.pl
 */
public abstract class AlfrescoConnector implements Serializable {
    private final static Logger log = LoggerFactory.getLogger(AlfrescoConnector.class);

    private Session session = null;

    /**
     * tworzy polczenie
     * @throws Exception
     */
    public void connect() throws Exception
    {
        Map<String, String> parameter= getConnectionParameters();

        principalInfo();
        // Create a session
        if(session == null)
        {
            log.info("connect to Alfresco Api");
            SessionFactory factory= SessionFactoryImpl.newInstance();
            session= factory.getRepositories(parameter).get(0).createSession();
        }
    }

    private void principalInfo() {
        if(ServletActionContext.getRequest() != null)
            try {
                log.info("userprincipal=" + ServletActionContext.getRequest().getUserPrincipal());
            }catch ( Exception e) {
                log.error("blad");
            }
    }

    /**
     * rozlacza sesje
     * @throws Exception
     */
    public void disconnect() throws Exception
    {
        session.clear();
        session = null;
        log.info("disconnect from Alfresco Api");
    }


    public Session getSession() throws Exception {
        if(session == null)
            connect();

        return session;
    }

    /**
     * Zwraca parametry polaczenia do alfresco
     * @return
     */
    protected abstract Map<String, String> getConnectionParameters() throws IOException;
}
