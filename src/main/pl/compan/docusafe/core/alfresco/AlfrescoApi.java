package pl.compan.docusafe.core.alfresco;

import com.google.common.base.Preconditions;
import org.alfresco.cmis.client.AlfrescoDocument;
import org.apache.chemistry.opencmis.client.api.*;
import org.apache.chemistry.opencmis.commons.data.Acl;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisConnectionException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;
import java.util.Map;

/**
 * Klasa implementujaca metody alfresco do operowania na folderach
 * oraz dokumentach
 * Lukasz Wozniak <lukasz.wozniak@docusafe.pl>.
 */
public class AlfrescoApi implements Serializable {
    private final static Logger log = LoggerFactory.getLogger(AlfrescoApi.class);
    /** @var przechowuje polaczenie do alfresco */
    private AlfrescoConnector connector = null;

    public AlfrescoApi() {
        setConnector();
    }

    public AlfrescoApi(AlfrescoConnector connector) {
        this.connector = connector;
    }

    /**
     * ustawia polaczenie w zaleznosci od configuracji
     */
    private void setConnector(){
        String connectorType = Docusafe.getAdditionPropertyOrDefault("alfresco.base.connector","default");
        if(connectorType.equals("cas-proxy"))
            connector = new CasProxyAlfrescoConnector();
        else
            connector = new BaseAlfrescoConnector();
    }

    public static void checkConnection() throws EdmException {
        AlfrescoApi api = new AlfrescoApi();
        try {
            api.connect();
            api.connector.getSession();
        } catch(CmisConnectionException e){
            log.error(e.getMessage());
            throw new EdmException("Repozytorium Alfresco jest niedost�pne!");
        } catch (Exception e) {
            log.error("", e);
            throw new EdmException("B��d w dost�pie do Alfresco!");
        } finally {
            api.disconnect();
        }
    }
    /**
     * Zwraca rootFolder
     * @return
     */
    public Folder getRootFolder(){
        try {
            return connector.getSession().getRootFolder();
        } catch (Exception e) {
            log.error("", e);
        }

        return null;
    }
    /**
     * Zwraca obiekt jesli istnieje w repozytorium. Jesli nie znajdzie zwraca null
     * @return
     */
    public CmisObject getObject(ObjectId obj) throws Exception
    {
        CmisObject object = null;
        try
        {
            object = connector.getSession().getObject(obj);
        }catch(CmisObjectNotFoundException e)
        {
        }catch(Exception e){
            log.error("", e);
        }

        return object;
    }

    /**
     * Zwraca obiekt jesli istnieje w repozytorium. Jesli nie znajdzie zwraca null
     * @return
     */
    public CmisObject getObject(String uuid) throws Exception
    {
        Preconditions.checkNotNull(uuid, "uuid nie moze byc puste!");
        CmisObject object = null;

        try
        {
            //obcinam do ostatniego srednika gdzie podaje nagle numer wersji.
            //String fixedUuid = StringUtils.splitPreserveAllTokens(uuid, ";")[0];
            object =  connector.getSession().getObject(uuid);
        }catch(CmisObjectNotFoundException e){
        }catch(Exception e){
            log.error("", e);
        }

        return object;
    }

    /**
     * Zwraca obiekt jesli istnieje w repozytorium. Jesli nie znajdzie zwraca null
     * @param path
     * @return
     */
    public CmisObject getObjectByPath(String path) throws Exception {
        CmisObject object = null;
        try
        {
            object = connector.getSession().getObjectByPath(path);
        }catch(CmisObjectNotFoundException e)
        {
        }catch(Exception e){
            log.error("", e);
        }
        return object;
    }

    /**
     * Tworzy dokument w alfresco
     * @param properties
     * @param folderId
     * @param contentStream
     * @param versioningState
     * @return
     * @throws Exception
     */
    public Document createDocument(Map<String, Object> properties,ObjectId folderId, ContentStream contentStream, VersioningState versioningState) throws Exception
    {
        try
        {
            ObjectId doc = null;
            if(contentStream != null)
                doc = connector.getSession().createDocument(properties, folderId, contentStream, versioningState);
            else
                doc = connector.getSession().createItem(properties, folderId);
            return (Document)getObject(doc);
        }catch(Exception e){
            log.error("", e);
            throw e;
        }finally{
            connector.disconnect();
        }
    }

    /**
     * Aktualizuje wlasciwosci dokumentu w Alfresco
     * @param uuid
     * @param properties
     * @return
     * @throws Exception
     */
    public CmisObject updateProperties(String uuid, Map<String, Object> properties) throws Exception
    {
        try
        {
            AlfrescoDocument aDoc= (AlfrescoDocument) connector.getSession().getObject(uuid);
            logProperties(uuid, properties);
            return aDoc.updateProperties(properties);
        }catch(Exception e){
            log.error("", e);
        }finally{
            connector.disconnect();
        }

        return null;
    }

    /**
     *
     * @param document
     * @param att
     * @param folderUuid
     */
    public void createDocument(pl.compan.docusafe.core.base.Document document, Attachment att, String folderUuid) throws Exception {
        ObjectId folderId = getObject(folderUuid);
        ContentStream contentStream = att == null ? null : AlfrescoUtils.getContentStream(att);
        Map<String, Object> entries = AlfrescoUtils.buildDocumentProperties(document);
        setDefaultValuesForProperties(document, att, entries);

        ObjectId objectId = createDocument(entries, folderId, contentStream, VersioningState.MAJOR);
        String uuid = objectId == null ? null : objectId.getId();
        log.info("Zapisany dokument {} w alfresco {}", document.getId(), uuid);
        if(uuid == null)
            return;
        //zapisuje na dokument jesli att == null
        if(att == null){
            document.setUuid(uuid);
            AlfrescoUtils.addAttachmentCreateHistoryEntry(document, uuid);
            //dodac zapis do historii pisma
        } else {
            att.setUuid(uuid);
            if(StringUtils.isEmpty(document.getUuid())){
                document.setUuid(uuid);
            }
            //doda� zapis do historii pisma
        }
    }

    /**
     * Metoda w przypadku potrzebu powinna wywo�a� metode z logiki dokumentu, je�li ustawienie w�a�ciwo�ci wynika z logiki
     *
     * @param document
     * @param att
     */
    private void setDefaultValuesForProperties(pl.compan.docusafe.core.base.Document document, Attachment att,Map<String, Object> entries) {
        if(att == null){
            return;
        }
        //Musimy nadpisa� name nazw� za��cznika
        if(entries.containsKey(AlfrescoUtils.ALFRESCO_FILE_NAME)){
            entries.remove(AlfrescoUtils.ALFRESCO_FILE_NAME);
        }

        entries.put(AlfrescoUtils.ALFRESCO_FILE_NAME, att.getMostRecentRevision().getOriginalFilename());

        //doda� wywo�anie metody z logiki
    }

    /**
     * Pobiera zawartosc dokumentu w alfresco
     * @param id
     * @return
     */
    public ContentStream getContentStream(ObjectId id) throws Exception
    {
        try{
            return connector.getSession().getContentStream(id);
        }catch(Exception e){
            log.error("", e);
        }finally{
            connector.disconnect();
        }
        return null;
    }

    /**
     * Aktualizuje zawarto�� za��cznika w Alfresco
     * @throws Exception
     */
    public void updateDocumentContentStream(pl.compan.docusafe.core.base.Document document, Attachment att, boolean withMetric) throws Exception
    {
        log.trace("updateDocumentContentStream");
        ContentStream contentStream = AlfrescoUtils.getContentStream(att);
        Map<String, Object> entries = AlfrescoUtils.buildDocumentProperties(document);

        connector.connect();
        //updateContentAndProperties(att.getUUID(), contentStream, withMetric ? entries : null);
        connector.disconnect();

        AlfrescoUtils.addAttachmentUpdateHistoryEntry(document, att);
        log.trace("updateDocumentContentStream-> end");
    }

    /**
     * Aktualizuje zawartosc oraz wlasciwosci documentu
     * @param UUID
     * @param contentStream
     * @param entries
     * @return
     * @throws Exception
     */
    private CmisObject updateContentAndProperties(String UUID, ContentStream contentStream, Map<String, Object> entries)	throws Exception
    {
        AlfrescoDocument newDoc = (AlfrescoDocument) getObject(UUID);
        if(newDoc == null)
            throw new EdmException("Nie znalazl dokumentu w Alfresco = " + UUID);

        log.info("contentStream = {}", contentStream);
        log.info("AlfrescoObject {} -> name: {}, id: {} ", newDoc.toString(), newDoc.getName(), newDoc.getId());
        if(contentStream != null)
            newDoc.setContentStream(contentStream, true, true);

        logProperties(UUID, entries);
        if(entries != null)
        {
            newDoc.updateProperties(entries);
        }

        return newDoc;
    }

    /**
     * Zwraca uprawnienia obiektu.
     */
    public Acl getAcl(ObjectId ObjectId)
    {
        Acl acl = null;

        try{
            acl = connector.getSession().getAcl(ObjectId,true);
        }catch(Exception e){
            log.error("", e);
        }

        return acl;
    }

    /**
     * Loguje parametry jakie byly exportowane do Alfresco
     * @param uuid
     * @param entries
     */
    public void logProperties(String uuid, Map<String, Object> entries)
    {
        if(!log.isDebugEnabled())
            return;

        if(entries == null){
            log.debug("METRIC EXPORT DANE {}, entries == null", uuid);
        } else {
            StringBuilder logBuffer = new StringBuilder();
            logBuffer.append("--- MetricExport sendAlfresco ---\n");
            for(String key : entries.keySet())
                logBuffer.append(key).append(" -- ").append(entries.get(key)).append('\n');

            log.debug("METRIC EXPORT DANE {} : {}", uuid, logBuffer.toString());
        }
    }



    public void connect() {
        try
        {
            connector.connect();
        }catch(Exception e){
            log.error("", e);
        }
    }

    public void disconnect() {
        try
        {
            connector.disconnect();
        }catch(Exception e){}
    }



}
