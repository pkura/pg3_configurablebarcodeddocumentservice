package pl.compan.docusafe.core.cmis;

import com.google.common.collect.Lists;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ObjectData;
import org.apache.chemistry.opencmis.commons.data.Properties;

import org.apache.chemistry.opencmis.commons.data.PropertyData;
import org.apache.chemistry.opencmis.commons.definitions.PropertyDefinition;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.Cardinality;
import org.apache.chemistry.opencmis.commons.enums.PropertyType;
import org.apache.chemistry.opencmis.commons.enums.Updatability;
import org.apache.chemistry.opencmis.commons.exceptions.CmisInvalidArgumentException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisRuntimeException;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

import static pl.compan.docusafe.core.cmis.DSCmisUtils.convertProperty;

/**
 * Klasa pomocnicza do konwertowania warto�ci
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class DSCmisUtils {
    private static final Logger log = LoggerFactory.getLogger(DSCmisUtils.class);

    public static PropertyData<String> convertProperty(String value) {
        return convertProperty(value, PropertyType.STRING);
    }

    /**
     * konwertuje PropertyType.ID
     *
     * @param value
     * @return
     */
    public static PropertyData<String> convertPropertyID(String value) {
        return convertProperty(value, PropertyType.ID);
    }

    /**
     * konwertuje PropertyType.HTML
     *
     * @param value
     * @return
     */
    public static PropertyData<String> convertPropertyHTML(String value) {
        return convertProperty(value, PropertyType.HTML);
    }

    /**
     * @param properties
     * @param propIds    from PropertyIds
     * @param prop
     */
    public static void addProperty(Properties properties, String propIds, PropertyData<?> prop) {
        ((AbstractPropertyData) prop).setId(propIds);
        ((PropertiesImpl) properties).addProperty(prop);
    }

    /**
     * konwertuje PropertyType.URI
     *
     * @param value
     * @return
     */
    public static PropertyData<String> convertPropertyURI(String value) {
        return convertProperty(value, PropertyType.URI);
    }


    public static PropertyData<Boolean> convertProperty(Boolean value) {
        return convertProperty(value, PropertyType.BOOLEAN);
    }

    public static PropertyData<BigInteger> convertProperty(BigInteger value) {
        return convertProperty(value, PropertyType.INTEGER);
    }

    public static PropertyData<BigDecimal> convertProperty(BigDecimal value) {
        return convertProperty(value, PropertyType.DECIMAL);
    }

    public static PropertyData<GregorianCalendar> convertProperty(GregorianCalendar value) {
        return convertProperty(value, PropertyType.DATETIME);
    }

    public static PropertyData<GregorianCalendar> convertProperty(Date value) {
        GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
        cal.setTime(value);
        return convertProperty(cal, PropertyType.DATETIME);
    }

    /**
     * Konwertue parametry
     *
     * @param value
     * @param propertyType
     * @param <T>
     * @return
     */
    private static <T> PropertyData<T> convertProperty(T value, PropertyType propertyType) {

        PropertiesImpl result = new PropertiesImpl();
        AbstractPropertyData<T> property = null;
/*        if (id == null) {
            throw new CmisRuntimeException("Invalid property!");
        }*/

        switch (propertyType) {
            case STRING:
                property = (AbstractPropertyData<T>) new PropertyStringImpl();
                ((PropertyStringImpl) property).setValue((String) value);
                break;
            case ID:
                property = (AbstractPropertyData<T>) new PropertyIdImpl();
                ((PropertyIdImpl) property).setValue((String) value);
                break;
            case BOOLEAN:
                property = (AbstractPropertyData<T>) new PropertyBooleanImpl();
                ((PropertyBooleanImpl) property).setValue((Boolean) value);
                break;
            case INTEGER:
                property = (AbstractPropertyData<T>) new PropertyIntegerImpl();
                ((PropertyIntegerImpl) property).setValue((BigInteger) value);
                break;
            case DECIMAL:
                property = (AbstractPropertyData<T>) new PropertyDecimalImpl();
                ((PropertyDecimalImpl) property).setValue((BigDecimal) value);
                break;
            case DATETIME:
                property = (AbstractPropertyData<T>) new PropertyDateTimeImpl();
                ((PropertyDateTimeImpl) property).setValue((GregorianCalendar) value);
                break;
            case HTML:
                property = (AbstractPropertyData<T>) new PropertyHtmlImpl();
                ((PropertyHtmlImpl) property).setValue((String) value);
                break;
            case URI:
                property = (AbstractPropertyData<T>) new PropertyUriImpl();
                ((PropertyUriImpl) property).setValue((String) value);
                break;
            default:
                throw new CmisRuntimeException("Nieznany typ wartosci!");
        }

//        property.setId(id);
//        property.setDisplayName(getString(jsonPropertyMap, JSON_PROPERTY_DISPLAYNAME));
//        property.setQueryName(getString(jsonPropertyMap, JSON_PROPERTY_QUERYNAME));
//        property.setLocalName(getString(jsonPropertyMap, JSON_PROPERTY_LOCALNAME));
//
//        convertExtension(jsonPropertyMap, property, PROPERTY_KEYS);
        return property;
    }

    /**
     * Check if property exists
     * @param properties
     * @param propIds @see PropertyIds
     * @return PropertyData
     * @throws org.apache.chemistry.opencmis.commons.exceptions.CmisInvalidArgumentException
     */
    protected static PropertyData<?> checkProperty(Properties properties, String propIds) throws CmisInvalidArgumentException{
        PropertyData<?> objectNameProperty = properties.getProperties().get(propIds);
        if (objectNameProperty == null || !(objectNameProperty.getFirstValue() instanceof String)) {
            throw new CmisInvalidArgumentException("W�a�ciwo�� '"+ propIds + "' musi by� ustawiona!");
        }

        return objectNameProperty;
    }

    /**
     * Ustawia definicje wlasciwosci
     * @param fTypeDefinition
     * @param objData
     */
    public static void setPropertyDefinitions(FolderTypeDefinitionImpl fTypeDefinition, ObjectData objData) {
        Map<String,PropertyDefinition<?>> propertyDefinitions = fTypeDefinition.getPropertyDefinitions();
        if(fTypeDefinition.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT))
            addFieldsManagerPropertyDefinition(fTypeDefinition, objData, propertyDefinitions);

        addAttachmentPropertyDefinition(fTypeDefinition);

        for (PropertyData<?> property : objData.getProperties().getPropertyList()){
            if(property.getId().equals(PropertyIds.OBJECT_ID)){
               //continue;
            }
            if (property instanceof PropertyStringImpl) {
                PropertyStringDefinitionImpl propertyDefinition = new PropertyStringDefinitionImpl();
                propertyDefinition.setPropertyType(PropertyType.STRING);
                convertToPropertyDefinition((PropertyStringImpl) property, propertyDefinition);
                propertyDefinitions.put(property.getId(), propertyDefinition);
            } else if (property instanceof PropertyIdImpl) {
                PropertyIdDefinitionImpl propertyDefinition = new PropertyIdDefinitionImpl();
                propertyDefinition.setPropertyType(PropertyType.ID);
                convertToPropertyDefinition((PropertyIdImpl) property, propertyDefinition);
                propertyDefinitions.put(property.getId(), propertyDefinition);
            } else if (property instanceof PropertyBooleanImpl) {
                PropertyBooleanDefinitionImpl propertyDefinition = new PropertyBooleanDefinitionImpl();
                propertyDefinition.setPropertyType(PropertyType.BOOLEAN);
                convertToPropertyDefinition((PropertyBooleanImpl) property, propertyDefinition);
                propertyDefinitions.put(property.getId(), propertyDefinition);
            } else if (property instanceof PropertyIntegerImpl) {
                PropertyIntegerDefinitionImpl propertyDefinition = new PropertyIntegerDefinitionImpl();
                propertyDefinition.setPropertyType(PropertyType.INTEGER);
                convertToPropertyDefinition((PropertyIntegerImpl) property, propertyDefinition);
                propertyDefinitions.put(property.getId(), propertyDefinition);
            } else if (property instanceof PropertyDecimalImpl) {
                PropertyDecimalDefinitionImpl propertyDefinition = new PropertyDecimalDefinitionImpl();
                propertyDefinition.setPropertyType(PropertyType.DECIMAL);
                convertToPropertyDefinition((PropertyDecimalImpl) property, propertyDefinition);
                propertyDefinitions.put(property.getId(), propertyDefinition);
            } else if (property instanceof PropertyDateTimeImpl) {
                PropertyDateTimeDefinitionImpl propertyDefinition = new PropertyDateTimeDefinitionImpl();
                propertyDefinition.setPropertyType(PropertyType.DATETIME);
                convertToPropertyDefinition((PropertyDateTimeImpl) property, propertyDefinition);
                propertyDefinitions.put(property.getId(), propertyDefinition);
            } else if (property instanceof PropertyHtmlImpl) {
                PropertyHtmlDefinitionImpl propertyDefinition = new PropertyHtmlDefinitionImpl();
                propertyDefinition.setPropertyType(PropertyType.HTML);
                convertToPropertyDefinition((PropertyHtmlImpl) property, propertyDefinition);
                propertyDefinitions.put(property.getId(), propertyDefinition);
            } else if (property instanceof PropertyUriImpl) {
                PropertyUriDefinitionImpl propertyDefinition = new PropertyUriDefinitionImpl();
                propertyDefinition.setPropertyType(PropertyType.URI);
                convertToPropertyDefinition((PropertyUriImpl) property, propertyDefinition);
                propertyDefinitions.put(property.getId(), propertyDefinition);
            }
        }
    }

    private static void addAttachmentPropertyDefinition(FolderTypeDefinitionImpl fTypeDefinition) {
        String[] attDefinitions = new String[]{
          PropertyIds.CONTENT_STREAM_FILE_NAME, PropertyIds.CONTENT_STREAM_ID, PropertyIds.IS_LATEST_VERSION,
          PropertyIds.CONTENT_STREAM_LENGTH, PropertyIds.CONTENT_STREAM_MIME_TYPE
        };

        for(String s : attDefinitions){
            PropertyStringImpl prop = (PropertyStringImpl) convertProperty("s");
            prop.setId(s);
            PropertyStringDefinitionImpl propertyDefinition = new PropertyStringDefinitionImpl();
            propertyDefinition.setPropertyType(PropertyType.STRING);
            convertToPropertyDefinition(prop, propertyDefinition);
            fTypeDefinition.getPropertyDefinitions().put(prop.getId(), propertyDefinition);
        }

    }

    /**
     * Dodaje definicj� pol dokument�w
     * @param fTypeDefinition
     * @param objData
     */
    private static void addFieldsManagerPropertyDefinition(FolderTypeDefinitionImpl fTypeDefinition, ObjectData objData, Map<String,PropertyDefinition<?>> propertyDefinitions) {
        try
        {
            List<DocumentKind> dks = DocumentKind.list(true);
            List<Field> fields = Lists.newArrayList();

            for(DocumentKind dk : dks)
               fields.addAll(dk.getFields());

            /** @TODO zkeszowa� to jako� */
            for(Field f : fields){
                String cmisCn = DSObjectDataConverter.cmisCn(f.getCn());
                setStringProperty(propertyDefinitions, cmisCn, f.getName());
            }
            setStringProperty(propertyDefinitions, DSObjectDataConverter._DS_DOCUMENT_KIND, "Rodzaj dokumentu");
            setStringProperty(propertyDefinitions, DSObjectDataConverter._DS_IMPORT_KIND, "Typ pisma");

        }catch(Exception e){
            log.error("", e);
            throw new NullPointerException(e.getMessage());
        }
    }

    private static void setStringProperty(Map<String, PropertyDefinition<?>> propertyDefinitions, String cmisCn, String cmisName) {
        PropertyStringDefinitionImpl propertyDefinition = new PropertyStringDefinitionImpl();
        propertyDefinitions.put(cmisCn, propertyDefinition);

        propertyDefinition.setPropertyType(PropertyType.STRING);
        propertyDefinition.setId(cmisCn);
        propertyDefinition.setDisplayName(cmisName);
        propertyDefinition.setLocalName(propertyDefinition.getLocalName());
        propertyDefinition.setDefaultValue(new ArrayList<String>());
        propertyDefinition.setCardinality(Cardinality.SINGLE);
        //uprawnienie do tworzenia folder�w
        propertyDefinition.setUpdatability(Updatability.READWRITE);
    }

    /**
     * @param property
     * @param propertyDefinition
     * @param <T>
     */
    private static <T> void convertToPropertyDefinition(PropertyData<T> property,AbstractPropertyDefinition<T> propertyDefinition) {
        propertyDefinition.setId(property.getId());
        propertyDefinition.setDisplayName(property.getDisplayName());
        propertyDefinition.setLocalName(propertyDefinition.getLocalName());
        propertyDefinition.setDefaultValue(new ArrayList<T>());
        propertyDefinition.setCardinality(Cardinality.SINGLE);
        //uprawnienie do tworzenia folder�w
        propertyDefinition.setUpdatability(Updatability.READWRITE);
    }
}
