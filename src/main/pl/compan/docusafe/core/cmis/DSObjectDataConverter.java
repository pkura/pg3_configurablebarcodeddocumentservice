package pl.compan.docusafe.core.cmis;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.enums.Action;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.exceptions.CmisInvalidArgumentException;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.AllowableActionsImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectDataImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectInFolderDataImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.PropertiesImpl;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.cmis.model.CmisPrepend;
import pl.compan.docusafe.core.cmis.model.DSCmisManager;
import pl.compan.docusafe.core.cmis.model.DictionaryFolderDSCmis;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static pl.compan.docusafe.core.cmis.DSCmisUtils.*;
import static pl.compan.docusafe.core.cmis.DSCmisUtils.convertProperty;

/**
 * Klasa s�u�y do konwertowania obiekt�w Docusafe na obiekty CMIS API
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class DSObjectDataConverter {

    private static final Logger log = LoggerFactory.getLogger(DSObjectDataConverter.class);
    /** @var pole oznacza pola dockinda */
    public static final String _DS_PREFIX = "ds:";
    public static final String _CMIS_PREFIX = "cmis:";
    public static final Character _SEPARATOR = '/';

    public static final String _DS_DOCUMENT_KIND = "ds:DOCUMENT_KIND";
    public static final String _DS_IMPORT_KIND = "ds:IMPORT_KIND";

    /**
     * Tworzy obiekt do WS z folderu
     *
     * @param folder
     * @return
     */
    public static ObjectData convert(Folder folder) throws EdmException {
        Preconditions.checkNotNull(folder, "Folder nie mo�e by� pusty!");
        Preconditions.checkState(folder.getId() != null, "Folder musi mie� id!");
        ObjectDataImpl objectData = new ObjectDataImpl();
        setFolderProperties(folder, objectData);
        setFolderAllowableActions(folder, objectData);
        return objectData;
    }

    /**
     * Tworzy folder abstrakcyjny dokumentu reprezentuj�cy dane dokumentu
     * @param document
     * @return
     * @throws EdmException
     */
    public static ObjectData convertDocumentFolder(Document document) throws EdmException{
        Preconditions.checkNotNull(document, "Dokument nie mo�e by� pusty!");
        Preconditions.checkState(document.getId() != null, "Dokument musi mie� id!");
        ObjectDataImpl objectData = new ObjectDataImpl();
        setDocumentFolderProperties(document, objectData);
        setDocumentFolderAllowableActions(document, objectData);
        return objectData;
    }

    private static void setDocumentFolderAllowableActions(Document document, ObjectDataImpl objectData) {
        AllowableActionsImpl actions = new AllowableActionsImpl();
        objectData.setAllowableActions(actions);

        actions.getAllowableActions().add(Action.CAN_GET_CHILDREN);
    }

    private static void setDocumentFolderProperties(Document document, ObjectDataImpl objectData) throws EdmException {
        PropertiesImpl properties = setProperties(objectData);
        String objectId = CmisPrepend.DOCUMENT_FOLDER.build(document.getId());
        String name = DSCmisManager.getDocumentFolderName(document);
        String path = buildPath(document.getFolder(), name);
        String parentId = CmisPrepend.FOLDER.build(document.getFolderId());

        addProperty(properties, PropertyIds.NAME, convertProperty(name));
        addProperty(properties, PropertyIds.OBJECT_ID, convertProperty(objectId));
        addProperty(properties, PropertyIds.OBJECT_TYPE_ID, convertPropertyID(BaseTypeId.CMIS_FOLDER.value()));
        addProperty(properties, PropertyIds.BASE_TYPE_ID, convertProperty(BaseTypeId.CMIS_FOLDER.value()));
        addProperty(properties, PropertyIds.CREATED_BY, convertProperty(document.getAuthor())); //?
        addProperty(properties, PropertyIds.CREATION_DATE, convertProperty(document.getCtime()));
        addProperty(properties, PropertyIds.CHANGE_TOKEN, convertProperty(objectId));
        addProperty(properties, PropertyIds.DESCRIPTION, convertProperty(document.getDescription()));
        addProperty(properties, PropertyIds.LAST_MODIFICATION_DATE, convertProperty(document.getCtime()));
        addProperty(properties, PropertyIds.PATH, convertProperty(path));
        addProperty(properties, PropertyIds.PARENT_ID, convertProperty(String.valueOf(parentId)));
        log.trace("{}", objectData.getProperties().getProperties());
    }

    /**
     * Converts an object into folder webservice object.
     */
    public static List<ObjectInFolderData> convertDataInFolderData(ObjectData... obj) throws EdmException {
        List<ObjectInFolderData> result = Lists.newArrayList();
        for(ObjectData o : obj){
            ObjectInFolderDataImpl inFolder = new ObjectInFolderDataImpl();
            inFolder.setObject(o);
            result.add(inFolder);

        }
        return result;
    }
    /**
     * Tworzy obiekt WS abstrakcyjnego folderu na s�owniki
     * @param parentName
     * @return
     * @throws EdmException
     */
    public static ObjectData convertDictionaryFolder(DictionaryFolderDSCmis.AbstractDictionaryFolderCmis dic, String parentName, String path) throws EdmException {
        Folder root = Folder.getRootFolder();
        ObjectDataImpl objectData = new ObjectDataImpl();
        String objectId = CmisPrepend.DICTIONARY_FOLDER.build(dic.getId());
        PropertiesImpl properties = setProperties(objectData);

        addProperty(properties, PropertyIds.NAME, convertProperty(dic.getName()));
        addProperty(properties, PropertyIds.OBJECT_ID, convertProperty(objectId));
        addProperty(properties, PropertyIds.OBJECT_TYPE_ID, convertPropertyID(BaseTypeId.CMIS_FOLDER.value()));
        addProperty(properties, PropertyIds.BASE_TYPE_ID, convertProperty(BaseTypeId.CMIS_FOLDER.value()));
        addProperty(properties, PropertyIds.CREATED_BY, convertProperty(root.getAuthor())); //?
        addProperty(properties, PropertyIds.CREATION_DATE, convertProperty(root.getCtime()));
        addProperty(properties, PropertyIds.CHANGE_TOKEN, convertProperty(objectId));
        addProperty(properties, PropertyIds.DESCRIPTION, convertProperty("Folder zawieraj�cy s�owniki dokument�w"));
        addProperty(properties, PropertyIds.LAST_MODIFICATION_DATE, convertProperty(root.getCtime()));
        //@TODO do zrobienia potem
        addProperty(properties, PropertyIds.PATH, convertProperty(path + '/' + dic.getName()));
        addProperty(properties, PropertyIds.PARENT_ID, convertProperty(String.valueOf(parentName)));
        return objectData;
    }

    /**
     * Tworzy obiekt WS, metadanych dokumentu oraz za��cznik�w
     * @param document
     * @return
     */
    public static ObjectData convert(Document document, Attachment attachment) throws EdmException {
        Preconditions.checkNotNull(document, "Dokument nie mo�e by� pusty!");

        ObjectDataImpl objectData = new ObjectDataImpl();
        setDocumentProperties(document, attachment, objectData);
        setDocumentAllowableActions(document, objectData);
        return objectData;
    }

    /**
     * @TODO do przeniesienia do innej klasy
     * @param document
     * @param properties
     * @throws EdmException
     */
    public static void update(Document document, Properties properties) throws EdmException {
        FieldsManager fm = document.getFieldsManager();
        fm.initialize();
        Set<String> fieldCns = fm.getFieldsCns();
        Map<String, Object> fieldValues = Maps.newHashMap();

        for(PropertyData<?> prop : properties.getPropertyList()){
            log.info("{}", prop.getId());
            boolean isDocumentField = prop.getId().startsWith(_DS_PREFIX) && fieldCns.contains(prop.getId().substring(_DS_PREFIX.length()));

            if(prop.getId().startsWith(_CMIS_PREFIX)){
                setBaseCmisProperty(document, prop);
            } else if(isDocumentField){
                fieldValues.put(prop.getId().substring(_DS_PREFIX.length()), prop.getFirstValue().toString());
            } else {
                throw new CmisInvalidArgumentException("Nieznana w�a�no�� do aktualizacji dla tego typu dokumentu: " + prop.getId());
            }
        }

        if(!fieldValues.isEmpty())
            document.getDocumentKind().logic().updateInCmisApi(document, fieldValues);
    }

    /**
     * Aktualizuje bazowe w�asno�ci dokumentu
     * @param document
     * @param prop
     */
    private static void setBaseCmisProperty(Document document, PropertyData<?> prop) {
    }

    /**
     * @TODO do przeniesienia do innej klasy
     * @param folder
     * @param properties
     * @throws EdmException
     */
    public static void update(Folder folder, Properties properties) throws EdmException {
        PropertyData<?> objectNameProperty = properties.getProperties().get(PropertyIds.NAME);

        if(objectNameProperty != null){
            String name = objectNameProperty.getFirstValue().toString();
            folder.setTitle(name);
        }
    }

    /**
     * Tworzy w�a�ciwo�ci folderu
     * @param folder
     * @param objectData
     */
    private static void setFolderProperties(Folder folder, ObjectDataImpl objectData) throws EdmException {
        PropertiesImpl properties = setProperties(objectData);
        String path = buildPath(folder, null);
        String objectId = CmisPrepend.FOLDER.build(folder.getId());
        String parentId = CmisPrepend.FOLDER.build(folder.getParentId());
        //base properties
        addProperty(properties, PropertyIds.NAME, convertProperty(folder.getTitle()));
        addProperty(properties, PropertyIds.OBJECT_ID, convertProperty(objectId));
        addProperty(properties, PropertyIds.OBJECT_TYPE_ID, convertPropertyID(BaseTypeId.CMIS_FOLDER.value()));
        addProperty(properties, PropertyIds.BASE_TYPE_ID, convertProperty(BaseTypeId.CMIS_FOLDER.value()));
        addProperty(properties, PropertyIds.CREATED_BY, convertProperty(folder.getAuthor())); //?
        addProperty(properties, PropertyIds.CREATION_DATE, convertProperty(folder.getCtime()));
        addProperty(properties, PropertyIds.CHANGE_TOKEN, convertProperty(objectId));
        addProperty(properties, PropertyIds.DESCRIPTION, convertProperty(path));
        addProperty(properties, PropertyIds.LAST_MODIFICATION_DATE, convertProperty(folder.getCtime()));
        addProperty(properties, PropertyIds.PATH, convertProperty(path));
        addProperty(properties, PropertyIds.PARENT_ID, convertProperty(parentId));
        log.trace("{}", objectData.getProperties().getProperties());
    }


    /**
     * Ustawia obiekty metdane dokumentu i za��czniki
     * @param document
     * @param attachment
     * @param objectData
     * @throws EdmException
     */
    private static void setDocumentProperties(Document document, Attachment attachment, ObjectDataImpl objectData) throws EdmException {
        PropertiesImpl properties = setProperties(objectData);
        String objectId = null;
        String name = null;
        String parentId = CmisPrepend.DOCUMENT_FOLDER.build(document.getId());

        if(attachment != null) {
            objectId = CmisPrepend.ATTACHMENT.build(attachment.getId());
            name = attachment.getMostRecentRevision().getOriginalFilename();
            addAttachmentProperties(attachment, properties);
        } else {
            objectId = CmisPrepend.DOCUMENT_METADATA.build(document.getId());
            name = DSCmisManager.getDocumentMetaDataName(document);
            addFieldsManagerProperties(properties, document);
        }

        addProperty(properties, PropertyIds.NAME, convertProperty(name));
        addProperty(properties, PropertyIds.OBJECT_ID, convertProperty(objectId));
        addProperty(properties, PropertyIds.OBJECT_TYPE_ID, convertPropertyID(BaseTypeId.CMIS_DOCUMENT.value()));
        addProperty(properties, PropertyIds.BASE_TYPE_ID, convertProperty(BaseTypeId.CMIS_DOCUMENT.value()));
        addProperty(properties, PropertyIds.CREATED_BY, convertProperty(document.getAuthor())); //?
        addProperty(properties, PropertyIds.CREATION_DATE, convertProperty(document.getCtime()));
        addProperty(properties, PropertyIds.CHANGE_TOKEN, convertProperty(objectId));
        addProperty(properties, PropertyIds.DESCRIPTION, convertProperty(document.getDescription()));
        addProperty(properties, PropertyIds.LAST_MODIFICATION_DATE, convertProperty(document.getCtime()));
        addProperty(properties, PropertyIds.PARENT_ID, convertProperty(String.valueOf(parentId)));
        log.trace("{}", objectData.getProperties().getProperties());
    }

    /**
     * Dodaje wymagane parametry za��cznika
     * @param attachment
     */
    private static void addAttachmentProperties(Attachment attachment,Properties properties) throws EdmException {
        AttachmentRevision rev = attachment.getMostRecentRevision();
        addProperty(properties, PropertyIds.IS_LATEST_VERSION, convertProperty("true"));
        addProperty(properties, PropertyIds.CONTENT_STREAM_ID, convertProperty(rev.getRevision().toString()));
        addProperty(properties, PropertyIds.CONTENT_STREAM_FILE_NAME, convertProperty(rev.getFileName()));
        addProperty(properties, PropertyIds.CONTENT_STREAM_LENGTH, convertProperty(rev.getSize().toString()));
        addProperty(properties, PropertyIds.CONTENT_STREAM_MIME_TYPE, convertProperty(rev.getMime()));
    }

    /**
     * Ustawia pola dokumentu jako wlasciwosci dokumentu
     * @param properties
     * @param document
     */
    private static void addFieldsManagerProperties(PropertiesImpl properties, Document document) {
        try
        {
            FieldsManager fm = document.getFieldsManager();
            Map<String, Object> fields = fm.getFieldValues();

            for(Map.Entry<String, Object> entry : fields.entrySet()){
                //do weryfikacji wartosci jakie s� zwracane
                /** @TODO cacheowa� to jako� */
                addProperty(properties, _DS_DOCUMENT_KIND, convertProperty(document.getDocumentKind().getCn()));
                addProperty(properties, _DS_IMPORT_KIND, convertProperty(document.getType().getName()));
                addProperty(properties, cmisCn(entry.getKey()), convertProperty(String.valueOf(entry.getValue())));
            }

        }catch(Exception e){
            log.error("", e);
            throw new NullPointerException(e.getMessage());
        }
    }

    /**
     * Dodaje ds: do pola jako rozpoznanie pol DS
     * @param cn
     * @return
     */
    public static String cmisCn(String cn){
        return  _DS_PREFIX + cn;
    }

    /**
     * Ucina tag ds: ktory jest rozpoznawany w cmis jako CN pola dokumentu
     * @param cn
     * @return
     */
    public static String cutDSCmisCn(String cn){
        return cn.startsWith(_DS_PREFIX) ? cn.substring(_DS_PREFIX.length()) : cn;
    }
    /**
     * Ustawia w�a�ciwo�ci
     * @param objectData
     * @return
     */
    private static PropertiesImpl setProperties(ObjectDataImpl objectData) {
        PropertiesImpl properties = new PropertiesImpl();
        objectData.setProperties(properties);
        return properties;
    }

    private static String buildPath(Folder folder, String name) {
        StringBuilder path = new StringBuilder();
        try{
            path = path.append(folder.getPrettyPath());
            if(!path.toString().startsWith(_SEPARATOR.toString()))
                path.insert(0, _SEPARATOR);

            if(StringUtils.isNotBlank(name))
                path.append(_SEPARATOR).append(name);
        }catch(EdmException e){
            log.error("", e);
        }

        return path.toString();
    }
    /**
     * Ustawia mo�liwe do wykonania akcje na folderze
     * @param folder
     * @param objectData
     */
    private static void setFolderAllowableActions(Folder folder, ObjectDataImpl objectData) {
        AllowableActionsImpl actions = new AllowableActionsImpl();
        objectData.setAllowableActions(actions);

        actions.getAllowableActions().add(Action.CAN_GET_CHILDREN);
        actions.getAllowableActions().add(Action.CAN_GET_ACL);
        actions.getAllowableActions().add(Action.CAN_APPLY_ACL);
    }

    private static void setDocumentAllowableActions(Document document, ObjectDataImpl objectData){
        AllowableActionsImpl actions = new AllowableActionsImpl();
        objectData.setAllowableActions(actions);


    }
}
