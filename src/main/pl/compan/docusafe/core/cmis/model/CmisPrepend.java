package pl.compan.docusafe.core.cmis.model;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;

/**
 * buduje id przekazywane w interfejsach
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public enum CmisPrepend {
    FOLDER("folder-"),
    //reprezentuje folder dokumentu
    DOCUMENT_FOLDER("document-"),
    //reprezentuje obiekt metryki dokumentu
    DOCUMENT_METADATA("document-metadata-"),
    ATTACHMENT("attachment-"),
    DICTIONARY_FOLDER("dicfolder-"),
    DICTIONARY("dicitonary-")
    ;

    String prepend;

    CmisPrepend(String value) {
        this.prepend = value;
    }

    public String getPrepend(){
        return prepend;
    }


    /**
     * Buduje id jakie b�dzie przekazywane danego obiektu.
     * Buduje document-id-zalacznikId
     * @param documentId
     * @return
     */
    public String build(Object id){
        if(id == null)
            return null;
        StringBuilder sb = new StringBuilder(this.getPrepend());
        sb.append(id.toString());

        return sb.toString();
    }

    /**
     * Parsuje otrzymane id jakie dostajemy
     * @param objectId
     * @return null jestli objectId jest null
     *
     */
    public static CmisPrepend parse(String objectId){
        CmisPrepend returned = null;
        if(StringUtils.isEmpty(objectId))
            return returned;

        for(CmisPrepend cmisId : CmisPrepend.values()){
            if(objectId.startsWith(cmisId.getPrepend()))
                returned = cmisId;
        }

        return returned;
    }

    /**
     * Zwraca ostatnie id z przekazanego ciagu znak�w
     * @param objectId
     * @return
     */
    public static Long lastId(String objectId){
        Long returned = null;
        if(StringUtils.isEmpty(objectId))
            return returned;

        String[] ids = StringUtils.split(objectId, '-');

        if(ids != null && ids.length> 0){
            returned = Long.parseLong(ids[ids.length-1]);
        } else {
            returned = Long.parseLong(objectId);
        }

        return returned;
    }

    /**
     * Zwraca ostatnie id z przekazanego ciagu znak�w
     * @param objectId
     * @return
     */
    public static String lastStringId(String objectId){
        String returned = null;
        if(StringUtils.isEmpty(objectId))
            return returned;

        String[] ids = StringUtils.split(objectId, '-');

        if(ids != null && ids.length> 0){
            returned = ids[ids.length-1];
        }

        return returned;
    }
}
