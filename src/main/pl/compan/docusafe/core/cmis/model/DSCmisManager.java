package pl.compan.docusafe.core.cmis.model;


import com.google.common.collect.Maps;

import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.io.IOUtils;
import org.hibernate.classic.Lifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.cmis.DSObjectDataConverter;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIManager;
import pl.compan.docusafe.service.imports.dsi.DefaultDSIHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Klasa zarzadzajaca obiektami typu @see Cmisable
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class DSCmisManager {

    private static final Logger log = LoggerFactory.getLogger(DSCmisManager.class);

    public static final String _METADANE = "METADANE";

    /**
     * Zwraca isntancj� reprezentowanego obiektu
     * @param objectId
     * @return
     */
    public static Cmisable getInstance(String objectId, String repositoryId) throws EdmException {
        CmisPrepend prepend = CmisPrepend.parse(objectId);
        Cmisable cmisable = null;
        switch(prepend){
            case FOLDER: cmisable = new FolderDSCmis(objectId, repositoryId); break;
            case DOCUMENT_FOLDER: cmisable = new DocumentFolderDSCmis(objectId, repositoryId); break;
            case DOCUMENT_METADATA: cmisable = new DocumentMetadataDSCmis(objectId, repositoryId); break;
            case ATTACHMENT: cmisable = new AttachmentDSCmis(objectId, repositoryId); break;
            case DICTIONARY: break;
            case DICTIONARY_FOLDER: cmisable = new DictionaryFolderDSCmis(objectId, repositoryId);break;
        }

        return cmisable;
    }
    /**
     * Zwraca isntancj� reprezentowanego obiektu
     * @param id obiekt Docusafe Document, Attachment, Folder
     * @return
     */
    public static Cmisable getInstanceByDSId(Long id, String repositoryId, CmisPrepend prepend) throws EdmException {
        String objectId =prepend.build(id);
        return getInstance(objectId, repositoryId);
    }

    /**
     * Zwraca obiekt po �cie�ce
     * @param repositoryId
     * @param path
     * @return
     * @throws EdmException
     */
    public static ObjectData getObjectByPath(String repositoryId, String path) throws EdmException, SQLException {
        log.trace("getObjectByPath {}", path);
        ObjectData objectData = null;

        if("/".equals(path)){ // zwracam ga��� g��wn� je�li jest tylko '/' w �cie�ce
            objectData = getInstance(repositoryId, repositoryId).getObject();
        } else {
            String[] elements = StringUtils.split(path, '/');
            if(ArrayUtils.isEmpty(elements)){
                throw new CmisObjectNotFoundException("Brak Element�w do wyszukania!");
            }

            String lastElement = elements[elements.length-1];

            if(lastElement.startsWith(_METADANE)){ // tutaj sprawdzamy czy chodzi o obiekt metdanych
                String documentName = elements[elements.length-2];
                Long documentId = getDocumentFromMetaData(documentName);
                objectData = getInstanceByDSId(documentId, repositoryId, CmisPrepend.DOCUMENT_METADATA).getObject();
            } else { //Tu mo�e by� za��cznikiem, folderem, lub abstrakcyjnym folderem dokumentu
                Folder folder = Folder.findByTitle(lastElement);
                if(folder != null){ // folder
                    //sprawdzi� katalog wy�ej
                    objectData = new FolderDSCmis(folder, repositoryId).getObject();
                } else {
                    AttachmentRevision attachment = AttachmentRevision.findByOrginalFileName(lastElement);
                    if(attachment != null){
                        //sprawdzi� czy katalog wyzej si� zgadza, je�li znazl, powinnnine by� to tytu� dokumentu
                        objectData = new AttachmentDSCmis(attachment.getAttachment(), repositoryId).getObject();
                    } else {
                        //pobra� dokument po tytule dokumentu i zwr�ci� obiekt dokumentu abstrakcyjnego
                        List<Document> docs = Document.findByTitle(lastElement);
                        if(docs != null && !docs.isEmpty()){
                            objectData = new DocumentMetadataDSCmis(docs.iterator().next(),repositoryId).getObject();
                        }
                    }
                }
            }
        }
        return objectData;
    }

    /**
     * Wyszukuje dokument po nazwie obiektu metadanych
     * @param name
     * @return
     */
    public static Long getDocumentFromMetaData(String name) throws EdmException {
        String[] elements = StringUtils.split(name, '-');
        Long id = null;
        if(!ArrayUtils.isEmpty(elements)){
             id = Long.parseLong(elements[elements.length-1]);
        }

        return id;
    }

    /**
     * Zwraca warto�� jaka jest ustalona jako nazwa wyswetlana dla folderu dokumentu
     * @param document
     * @return
     */
    public static String getDocumentFolderName(Document document){
        return document.getTitle();
    }

    /**
     * Zwraca nazw� dokumentu dla obiektu metadanych
     * @param document
     * @return
     */
    public static String getDocumentMetaDataName(Document document) {
        return StringUtils.join(new String[] {_METADANE, "-",document.getId().toString() });
    }

    /**
     * Metoda tworzy dokument
     * @param repositoryId
     * @param properties
     * @param folderId
     * @param contentStream
     * @param versioningState
     * @param policies
     * @param addAces
     * @param removeAces
     * @param extension
     * @return
     */
    public static String createDocument(String repositoryId, Properties properties, String folderId,
                                        ContentStream contentStream, VersioningState versioningState, List<String> policies,
                                        Acl addAces, Acl removeAces, ExtensionsData extension) throws IOException, EdmException {

        String documentKind = getProperty(properties, DSObjectDataConverter._DS_DOCUMENT_KIND);
        String importKind   = getProperty(properties, DSObjectDataConverter._DS_IMPORT_KIND);
        DocumentType documentType = DocumentType.forName(importKind);
        if(documentType == null)
            throw new EdmException("brak typu importu");

        DocumentKind kind = DocumentKind.findByCn(documentKind);
        Map<String, Object> props = transform(properties.getProperties());

        kind.validateFields(documentKind, props);


        DSIBean bean = new DSIBean();
        bean.setImageArray(IOUtils.toByteArray(contentStream.getStream()));
        bean.setImageName(contentStream.getFileName());

        Long id = DSIManager.persistDocument(documentKind, props, documentType.getDSiImportKindCode(), bean, new DefaultDSIHandler());
        if(bean.getImportStatus() < 0){ //import nie uda� si�
            throw new EdmException(bean.getErrCode());
        }
        return CmisPrepend.DOCUMENT_METADATA.build(id);
    }

    private static String getProperty(Properties properties, String name) throws EdmException {
        String documentKind = null;
        PropertyData<?> dk = (PropertyData<?>) properties.getProperties().get(name);
        if(dk == null)
            throw new EdmException("Brak parametru " + name);
        else {
            documentKind = (String)dk.getFirstValue();
            //properties.getProperties().remove(name);
        }

        return documentKind;
    }

    private static Map<String, Object> transform(Map<String, PropertyData<?>> properties) {
        Map<String, Object> props = Maps.newHashMap();

        for(Map.Entry<String, PropertyData<?>> e : properties.entrySet()){
            if(canAdd(e))
            props.put(DSObjectDataConverter.cutDSCmisCn(e.getKey()), e.getValue().getFirstValue());
        }
        return props;
    }

    private static boolean canAdd(Map.Entry<String, PropertyData<?>> e) {
        if(e.getKey().equals(DSObjectDataConverter._DS_DOCUMENT_KIND) || e.getKey().equals(DSObjectDataConverter._DS_IMPORT_KIND))
            return false;

        return true;
    }


}
