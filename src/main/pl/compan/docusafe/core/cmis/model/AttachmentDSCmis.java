package pl.compan.docusafe.core.cmis.model;

import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.alfresco.AlfrescoUtils;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.cmis.DSObjectDataConverter;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * Reprezentuje za��cznik
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class AttachmentDSCmis extends AbstractCmisable<Attachment> {

    private static final Logger log = LoggerFactory.getLogger(AttachmentDSCmis.class);

    private Attachment attachment;
    public AttachmentDSCmis(String objectId, String repositoryId) throws EdmException {
        super(objectId, repositoryId,  BaseTypeId.CMIS_DOCUMENT);
        this.attachment = getDSObject();
    }

    public AttachmentDSCmis(Attachment att, String repositoryId) {
        super(CmisPrepend.ATTACHMENT.build(att.getId()), repositoryId, BaseTypeId.CMIS_DOCUMENT);
        this.attachment = att;
    }

    @Override
    public ObjectInFolderList getChildren() {
        return null;
    }

    @Override
    public Class getDSClass() {
        return Attachment.class;
    }

    @Override
    public ObjectData getObject() throws EdmException {
        return DSObjectDataConverter.convert(attachment.getDocument(), attachment);
    }

    /**
     * Jako rodzica zwracamy DOCUMENT_FOLDER
     * @return
     * @throws EdmException
     */
    @Override
    public List<ObjectParentData> getObjectParents() throws EdmException {
        ObjectData objectData = DSObjectDataConverter.convertDocumentFolder(attachment.getDocument());
        return buildObjectParents(objectData);
    }

    @Override
    public void updateProperties(Properties properties) throws EdmException {
        Document document = attachment.getDocument();
        DSObjectDataConverter.update(document, properties);
        DSApi.context().session().update(document);
    }

    @Override
    public void deleteObjectOrCancelCheckOut() throws EdmException {
        //@TODO usuwanie za��cznika
        AttachmentHelper.removeAttachments(attachment.getDocument().getId(), attachment.getId());
        AttachmentHelper.removeAttachments(attachment.getDocument().getId());
    }

    @Override
    public TypeDefinition getTypeDefinition() {
        return null;
    }

    @Override
    public ContentStream getContentStream(String streamId, BigInteger offset, BigInteger length, ExtensionsData extension) throws Exception {
        AttachmentRevision revision = streamId == null ? attachment.getMostRecentRevision() : attachment.getRevisionByNumber(getRevisionNumber(streamId));
        log.info("revision Id = {}", revision.getId());
;       ContentStream cs = AlfrescoUtils.getContentStream(revision);
        return cs;
    }

    private Integer getRevisionNumber(String streamId) throws EdmException {
        Integer rev = null;

        try{
            rev = Integer.valueOf(streamId);
        }catch(NumberFormatException e){
            log.error("Warto�� nie jest numerem rewizji " + streamId);
            throw new EdmException("Warto�� nie jest numerem rewizji");
        }
        return rev;
    }

    @Override
    public void setContentStream(ContentStream contentStream, Boolean overwriteFlag, String value, ExtensionsData extension) throws EdmException, IOException {
       File file = pl.compan.docusafe.util.FileUtils.createTempFile(contentStream.getFileName(), contentStream.getStream());
       try{
           attachment.createRevision(file).setOriginalFilename(contentStream.getFileName());
       }finally{
           FileUtils.deleteQuietly(file);
       }
    }

    @Override
    public ObjectData getFolderParent() throws EdmException {
         ObjectData objectData = DSObjectDataConverter.convertDocumentFolder(attachment.getDocument());
        return objectData;
    }
}
