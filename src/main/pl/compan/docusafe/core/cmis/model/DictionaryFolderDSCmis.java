package pl.compan.docusafe.core.cmis.model;

import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.cmis.DSObjectDataConverter;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Obiekt abstrakcyjnego folderu s�ownika dokumentu. G��wny folder bedzie zawiera�
 * folder "S�owniki" @see FolderDSCmis.setDictionaryFolderChildren().
 *
 * Folder ten b�dzie zawiera� katalogi z poszczeg�lnymi polami s�ownik�w wyst�puj�cymi
 * w dockindach danego wdro�enia.
 *
 * Poszczeg�lne foldery s�ownik�w, b�d� zawiera�y kolejno abstrakcyjnego dokumenty, kt�re nie s� dokumentami,
 * a poszczeg�lnymi s�ownikami systemu.
 *
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class DictionaryFolderDSCmis extends AbstractCmisable<DictionaryFolderDSCmis.AbstractDictionaryFolderCmis> {

    private Folder rootFolder;

    final static AbstractDictionaryFolderCmis dictionaryRootFolder = new AbstractDictionaryFolderCmis("0", "S�owniki");

    /**
     * je�li jest null przy tworzeniu obiektu, tzn ze jest to g��wny katalog slownik�w
     */
    private AbstractDictionaryFolderCmis dictionary;

    protected DictionaryFolderDSCmis(String objectId, String repostioryId) throws EdmException {
        super(objectId, repostioryId, BaseTypeId.CMIS_FOLDER);
        this.rootFolder = Folder.getRootFolder();
        this.dictionary = getDSObject();
    }

    @Override
    public ObjectInFolderList getChildren() throws EdmException, SQLException {
        List<ObjectInFolderData> objects = new ArrayList<ObjectInFolderData>();
        setDictionariesFolder(objects);
        return buildChildrenObject(objects);
    }

    private void setDictionariesFolder(List<ObjectInFolderData> objects) throws EdmException {
        objects.addAll(DictionaryFolderCmisManager.convertDictionaryToFolder(
                dictionaryRootFolder,
                CmisPrepend.FOLDER.build(rootFolder.getId()),
                rootFolder.getPrettyPath()));
    }

    @Override
    public Class getDSClass() {
        return Object.class;
    }

    @Override
    public AbstractDictionaryFolderCmis getDSObject() throws EdmException {
        String dictionaryName = CmisPrepend.DICTIONARY_FOLDER.lastStringId(objectId);
        //tzn. �e jest folderem g��wnym s�ownik�w
        if(dictionaryRootFolder.getId().equals(dictionaryName)){
            return dictionaryRootFolder;
        } else {
            return new AbstractDictionaryFolderCmis(DwrDictionaryFacade.dictionarieObjects.get(dictionaryName));
        }
    }

    @Override
    public ObjectData getObject() throws EdmException, SQLException {
        return DSObjectDataConverter.convertDictionaryFolder(
                dictionaryRootFolder,
                CmisPrepend.FOLDER.build(rootFolder.getId()),
                rootFolder.getPrettyPath());
    }

    @Override
    public List<ObjectParentData> getObjectParents() throws EdmException {
        return buildObjectParents(DSObjectDataConverter.convert(rootFolder));
    }

    @Override
    public void updateProperties(Properties properties) throws EdmException {

    }

    @Override
    public void deleteObjectOrCancelCheckOut() throws EdmException {

    }

    @Override
    public TypeDefinition getTypeDefinition() {
        return null;
}

    @Override
    public ObjectData getFolderParent() throws EdmException {
        return DSObjectDataConverter.convert(rootFolder);
    }

    /**
     * klasa reprezentuj�ca folder abstrakcyjny s�ownik�w
     */
    public static class AbstractDictionaryFolderCmis{
        String id;
        String name;
        DwrDictionaryBase dictionaryBase;

        AbstractDictionaryFolderCmis(String id, String name) {
            this.id = id;
            this.name = name;
        }

        AbstractDictionaryFolderCmis(String id, String name, DwrDictionaryBase dicBase) {
            this.id = id;
            this.name = name;
            this.dictionaryBase = dicBase;
        }

        AbstractDictionaryFolderCmis(DwrDictionaryBase dicBase) {
            this.dictionaryBase = dicBase;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }


    }
}
