package pl.compan.docusafe.core.cmis.model;

import com.google.common.collect.Lists;
import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectInFolderDataImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectInFolderListImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectParentDataImpl;
import org.apache.commons.lang.NotImplementedException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cmis.DSObjectDataConverter;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * Klasa abstracyjna implementuj�ca wsp�lne mechanizmy
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public abstract class AbstractCmisable<T> implements Cmisable<T> {
    /**
     *  @var Id obiektu przekazane w WS
     */
    protected String objectId;

    protected CmisPrepend cmisPrepend;
    /** @var obiekt jaki jest reprezentowany w CMIS API */
    protected BaseTypeId baseTypeId;

    /** ID repozytorium */
    protected String repositoryId;


    protected AbstractCmisable(String objectId, String repostioryId, BaseTypeId baseTypeId) {
        this.objectId = objectId;
        this.repositoryId = repostioryId;
        this.cmisPrepend = CmisPrepend.parse(objectId);
        this.baseTypeId = baseTypeId;
    }

    @Override
    public CmisPrepend getCmisPrepend() {
        return cmisPrepend;
    }

    @Override
    public BaseTypeId getBaseTypeId() {
        return baseTypeId;
    }

    /**
     * Buduje obiekt rodzica danego obiektu
     * @param objectParentData
     * @return
     */
    protected List<ObjectParentData> buildObjectParents(ObjectData objectParentData) {
        List<ObjectParentData> result = Lists.newArrayList();
        ObjectParentDataImpl objectParentImpl = new ObjectParentDataImpl(objectParentData);
        result.add(objectParentImpl);
        return result;
    }
    /**
     * Dodaje obiekt WS do listy zwracanych element�w
     * @param document
     * @param att
     * @param results
     * @throws EdmException
     */
    protected static void addObjectInFolderData(Document document, Attachment att,  List<ObjectInFolderData> results) throws EdmException {
        ObjectData obj = DSObjectDataConverter.convert(document, att);
        results.addAll(DSObjectDataConverter.convertDataInFolderData(obj));
    }


    @Override
    public T getDSObject() throws EdmException {
        return (T) Finder.find(getDSClass(), CmisPrepend.lastId(objectId));
    }

    @Override
    public ContentStream getContentStream(String streamId, BigInteger offset, BigInteger length, ExtensionsData extension) throws Exception {
        return null;
    }

    @Override
    public void setContentStream(ContentStream contentStream, Boolean overwriteFlag, String value, ExtensionsData extension) throws EdmException, IOException {
          throw new IllegalArgumentException("Ten obiekt nie ma mo�liwo�ci aktualizacji zawarto�ci");
    }

    /**
     * Tworzy obiekt jaki zwraca metoda getChildren()
     */
    protected ObjectInFolderListImpl buildChildrenObject(List<ObjectInFolderData> objects) {
        ObjectInFolderListImpl result = new ObjectInFolderListImpl();
        result.setObjects(objects);
        result.setHasMoreItems(false);
        result.setNumItems(new BigInteger("" + (objects.size())));
        return result;
    }

    /**
     * Dodanie uprawnie� do obiektu
     *
     * @param addAces
     * @param removeAces
     * @return
     */
    @Override
    public Acl applyAcl(Acl addAces, Acl removeAces) throws EdmException {
        throw new NotImplementedException();
    }

    /**
     * Zwraca uprawnienia obiektu
     *
     * @param onlyBasicPermissions
     * @param extension
     * @return
     */
    @Override
    public Acl getAcl(Boolean onlyBasicPermissions, ExtensionsData extension) throws EdmException {
        throw new NotImplementedException();
    }

    /**
     * Dodanie uprawnie� do obiektu
     *
     * @param aces@return
     */
    @Override
    public Acl applyAcl(Acl aces) throws EdmException {
        throw new NotImplementedException();
    }
}
