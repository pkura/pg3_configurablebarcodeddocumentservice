package pl.compan.docusafe.core.cmis.model;


import com.google.common.collect.Lists;
import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectInFolderDataImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectInFolderListImpl;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cmis.DSObjectDataConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa reprezetnuj�ca folder dokument�w kt�ry posiada obiekt metadanych w sobie
 * wraz z za��cznikami
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class DocumentFolderDSCmis extends AbstractCmisable<Document> {

    private Document document;
    public DocumentFolderDSCmis(String objectId, String repositoryId) throws EdmException {
        super(objectId, repositoryId, BaseTypeId.CMIS_FOLDER);
        this.document = getDSObject();
    }

    public DocumentFolderDSCmis(Document doc, String repositoryId) {
        super(CmisPrepend.DOCUMENT_FOLDER.build(doc.getId()), repositoryId, BaseTypeId.CMIS_FOLDER);
        this.document = doc;
    }

    @Override
    public ObjectInFolderList getChildren() throws EdmException {
        List<ObjectInFolderData> objects = new ArrayList<ObjectInFolderData>();
        objects.addAll(convertObjectInFolder(document));
        return buildChildrenObject(objects);
    }

    @Override
    public Class getDSClass() {
        return Document.class;
    }

    @Override
    public ObjectData getObject() throws EdmException {
        return DSObjectDataConverter.convertDocumentFolder(document);
    }

    @Override
    public List<ObjectParentData> getObjectParents() throws EdmException {
        //zwracamy folder nadrz�dny dokumentu
        return buildObjectParents(DSObjectDataConverter.convert(document.getFolder()));
    }

    @Override
    public void updateProperties(Properties properties) throws EdmException {
        // folder dokumentu nie ma aktualizacji metryku
    }

    @Override
    public void deleteObjectOrCancelCheckOut() {
        // Folder dokumentu jest zale�ny od tego czy dokument zosta� usuni�ty
    }

    @Override
    public TypeDefinition getTypeDefinition() {
        return null;
    }

    @Override
    public ObjectData getFolderParent() throws EdmException {
        return DSObjectDataConverter.convert(document.getFolder());
    }

    /**
     * Konwertuje obiekty do zwr�cenia do listy jako dzieci
     * @param document
     * @return
     * @throws EdmException
     */
    private static List<ObjectInFolderData> convertObjectInFolder(Document document) throws EdmException {
        List<ObjectInFolderData> results = Lists.newArrayList();
        List<Attachment> attachments = document.getAttachments();
        addObjectInFolderData(document, null, results);
        //pusta lista b�dzie zawiera� tylko metryk� dokumentu
        if(attachments != null && !attachments.isEmpty()){
            for(Attachment att : attachments){
                addObjectInFolderData(document, att, results);
            }
        }

        return results;
    }


}
