package pl.compan.docusafe.core.cmis.model;

import com.google.common.collect.Lists;
import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.AccessControlEntryImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.AccessControlListImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.AccessControlPrincipalDataImpl;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.cmis.DSObjectDataConverter;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Reprezentuje obiekt metadanych dokumentu
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class DocumentMetadataDSCmis extends AbstractCmisable<Document> {

    private Document document;

    public DocumentMetadataDSCmis(String objectId, String repositoryId) throws EdmException {
        super(objectId, repositoryId, BaseTypeId.CMIS_DOCUMENT);
        this.document = getDSObject();
    }

    public DocumentMetadataDSCmis(Document doc, String repositoryId) {
        super(CmisPrepend.DOCUMENT_METADATA.build(doc.getId()), repositoryId, BaseTypeId.CMIS_DOCUMENT);
        this.document = doc;
    }

    /**
     * nie ma dzieci
     * @return
     */
    @Override
    public ObjectInFolderList getChildren() {
        return null;
    }

    @Override
    public Class getDSClass() {
        return Document.class;
    }

    @Override
    public ObjectData getObject() throws EdmException {
        return DSObjectDataConverter.convert(document, null);
    }

    /**
     * Jako rodzica zwracamy DOCUMENT_FOLDER
     * @return
     * @throws EdmException
     */
    @Override
    public List<ObjectParentData> getObjectParents() throws EdmException {
        ObjectData objectData = DSObjectDataConverter.convertDocumentFolder(document);
        return buildObjectParents(objectData);
    }

    @Override
    public void updateProperties(Properties properties) throws EdmException {
        DSObjectDataConverter.update(document, properties);
        DSApi.context().session().update(document);
    }

    @Override
    public void deleteObjectOrCancelCheckOut() throws EdmException {
        if(!document.canDelete())
            throw new EdmException("Dokumentu nie mo�na usun��!");
        if(document.isDeleted())
            throw new EdmException("Dokumentu jest usuni�ty!");
        document.delete();
    }

    @Override
    public TypeDefinition getTypeDefinition() {
        return null;
    }

    @Override
    public void setContentStream(ContentStream contentStream, Boolean overwriteFlag, String value, ExtensionsData extension) throws EdmException {
        File file = null;
        try{
            file = pl.compan.docusafe.util.FileUtils.createTempFile(contentStream.getFileName(), contentStream.getStream());
            AttachmentHelper.createAttachment(document.getId(), file, contentStream.getFileName(), contentStream.getFileName(),null);
        }catch(Exception e){
            //log.error("", e);
            throw new EdmException("");
        }finally{
            FileUtils.deleteQuietly(file);
        }

    }

    @Override
    public ObjectData getFolderParent() throws EdmException {
        ObjectData objectData = DSObjectDataConverter.convertDocumentFolder(document);
        return objectData;
    }

    /**
     * Zwraca uprawnienia obiektu
     *
     * @param onlyBasicPermissions
     * @param extension
     * @return
     */
    @Override
    public Acl getAcl(Boolean onlyBasicPermissions, ExtensionsData extension) throws EdmException {
        Map<String, List<String>> permissions = ObjectPermission.findAndConvert(Document.class, document.getId());
        AccessControlListImpl acl = new AccessControlListImpl();
        List<Ace> aces = Lists.newArrayList();

        for(Map.Entry<String, List<String>> entry : permissions.entrySet()){
            aces.add(new AccessControlEntryImpl(new AccessControlPrincipalDataImpl(entry.getKey()), entry.getValue()));
        }

        acl.setAces(aces);
        return acl;
    }
}
