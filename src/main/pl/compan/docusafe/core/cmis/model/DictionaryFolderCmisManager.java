package pl.compan.docusafe.core.cmis.model;

import com.google.common.collect.Lists;
import org.apache.chemistry.opencmis.commons.data.ObjectData;
import org.apache.chemistry.opencmis.commons.data.ObjectInFolderData;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cmis.DSObjectDataConverter;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.cmis.model.DictionaryFolderDSCmis.AbstractDictionaryFolderCmis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Klasa zawiera metody pomocnicze do obs�ugi abstrakcyjnych folder�w s�ownik�w wielowarto�ciowych
 *
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class DictionaryFolderCmisManager {
    private static final Logger log = LoggerFactory.getLogger(DictionaryFolderCmisManager.class);

    /**
     * Konwertuje s�owniki Dockind�w na obiekty Cmisowe.Abstrakcyjne foldery s�ownik�w.
     * @param dictionaryRootFolder
     * @param prettyPath
     * @return
     * @throws EdmException
     */
    public static List<ObjectInFolderData> convertDictionaryToFolder(AbstractDictionaryFolderCmis dictionaryRootFolder,
                                                                     String parentName,  String prettyPath) throws EdmException {
        //wynik
        List<ObjectInFolderData> result = new ArrayList<ObjectInFolderData>();

        String parentPath = prettyPath + DSObjectDataConverter._SEPARATOR + dictionaryRootFolder.getName();
        //abstrakcyjna lista slownik�w
        List<ObjectData> dicCmis = Lists.newArrayList();

        buildDictionaryList(dicCmis, parentName, parentPath);

        result.addAll(DSObjectDataConverter.convertDataInFolderData(dicCmis.toArray(new ObjectData[0])));
        return result;
    }

    /**
     * Buduje list� z istniej�cych s�ownik�w
     * @param dicCmis
     */
    private static void buildDictionaryList(List<ObjectData> dicCmis, String parentName, String path) throws EdmException {
        Map<String,DwrDictionaryBase> dictionaries = DwrDictionaryFacade.dictionarieObjects;
        if(dictionaries != null && !dictionaries.isEmpty()){
            for(Map.Entry<String, DwrDictionaryBase> entry : dictionaries.entrySet()){
                log.error("{} {}", entry.getKey(), entry.getValue());
                AbstractDictionaryFolderCmis dicFolder = new AbstractDictionaryFolderCmis(entry.getKey(), entry.getValue().getOldField().getName());
                dicCmis.add(DSObjectDataConverter.convertDictionaryFolder(dicFolder, parentName, path));
            }
         }
    }
}
