package pl.compan.docusafe.core.cmis.model;

import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinitionList;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.hibernate.classic.Lifecycle;
import pl.compan.docusafe.core.EdmException;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.List;

/**
 * Klasa abstrakcyjna reprezentuj�ca obiekt bytu w CMIS API w systemie DS.
 * Repozytorium DS w CMIS API przedstawia si� nast�puj�co:
 *
 * Ze wzgledu na to i� dokument ma wiele za��cznik�w,
 * zwracana �cie�ka dla dokument�w:
 *
 * /folder/folder_dokumentu/metadanedokument
 * /folder/folder_dokumentu/zalacznik_1
 *
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public interface Cmisable<T> extends Serializable{

    /**
     * Zwraca przedrostek definiuj�cy jakiego typu jest dany obiekt
     * @return
     */
    CmisPrepend getCmisPrepend();
    /**
     * Zwraca dzieci danego obiektu
     * @return
     */
    ObjectInFolderList getChildren() throws EdmException, SQLException;

    /**
     * Metoda zwraca obiekt w �wiecie DS
     * @return
     */
    T getDSObject() throws EdmException;

    /**
     * Zwraca typ klasy jak� obiekt reprezentuje w DS
     * @return
     */
    Class getDSClass();

    /**
     * Zwraca BaseTypeId przypisany do tego typu
     * @return
     */
    BaseTypeId getBaseTypeId();
    /**
     * Zwraca w�a�ciwo�ci danego obiektu reprezentuj�cego dany byt w CMIS API
     * @return
     */
    ObjectData getObject() throws EdmException, SQLException;

    /**
     * Zwraca rodzica danego obiektu
     * @return
     */
    List<ObjectParentData> getObjectParents() throws EdmException;

    /**
     * aktualizuje w�a�ciwo�ci obiektu
     * @param properties
     */
    void updateProperties(Properties properties) throws EdmException;

    /**
     * usuwa obiekt lub anuluje checkout
     */
    void deleteObjectOrCancelCheckOut() throws EdmException;

//    TypeDefinitionList getTypeChildren();

    /**
     * Zwraca definicje typ�w danego obiektu
     * @return
     */
    TypeDefinition getTypeDefinition();

    ContentStream getContentStream(String streamId, BigInteger offset, BigInteger length, ExtensionsData extension) throws Exception;

    /**
     * Aktualizuje zawarto�� za��czniku
     * @param contentStream
     * @param overwriteFlag
     * @param value
     * @param extension
     */
    void setContentStream(ContentStream contentStream, Boolean overwriteFlag, String value, ExtensionsData extension) throws EdmException, IOException;


    ObjectData getFolderParent() throws EdmException;

    /**
     * Dodanie uprawnie� do obiektu
     * @param addAces
     * @param removeAces
     * @return
     */
    Acl applyAcl(Acl addAces, Acl removeAces) throws EdmException;

    /**
     * Zwraca uprawnienia obiektu
     * @param onlyBasicPermissions
     * @param extension
     * @return
     */
    Acl getAcl(Boolean onlyBasicPermissions, ExtensionsData extension) throws EdmException;

    /**
     * Dodanie uprawnie� do obiektu
     * @param addAces
     * @param removeAces
     * @return
     */
    Acl applyAcl(Acl aces) throws EdmException;
}
