package pl.compan.docusafe.core.cmis.model;

import com.beust.jcommander.internal.Maps;
import com.google.common.collect.Lists;
import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.*;
import org.springframework.util.CollectionUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentHelper;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.cmis.DSObjectDataConverter;
import pl.compan.docusafe.core.security.SecurityHelper;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Klasa definiuje potrzebne metody do interfejsu CMIS API.
 * Reprezentacja folderu
 *
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class FolderDSCmis extends AbstractCmisable<Folder> {

    private Folder folder;

    public FolderDSCmis(String obiectId, String repositoryId) throws EdmException {
        super(obiectId, repositoryId, BaseTypeId.CMIS_FOLDER);
        this.folder = getDSObject();
    }

    public FolderDSCmis(Folder folder, String repositoryId) {
        super(CmisPrepend.FOLDER.build(folder.getId()), repositoryId, BaseTypeId.CMIS_FOLDER);
        this.folder = folder;
    }

    @Override
    public ObjectInFolderList getChildren() throws EdmException, SQLException {
        List<ObjectInFolderData> objects = new ArrayList<ObjectInFolderData>();

        setFoldersChildren(folder, objects);
        setDocumentsChildren(folder, objects);
        //setDictionaryFolderChildren(objects);

        return buildChildrenObject(objects);
    }



    @Override
    public ObjectData getObject() throws EdmException, SQLException {
         return DSObjectDataConverter.convert(folder);
    }

    @Override
    public List<ObjectParentData> getObjectParents() throws EdmException {
        Folder f = this.folder;
        //dziwne ale z tym ifem dzia�a
        if(!objectId.equals(repositoryId))
            f = Folder.find(f.getParentId());

        return buildObjectParents(DSObjectDataConverter.convert(f));
    }



    @Override
    public void updateProperties(Properties properties) throws EdmException {
        DSObjectDataConverter.update(folder, properties);
        DSApi.context().session().update(folder);
    }

    @Override
    public void deleteObjectOrCancelCheckOut() throws EdmException {
        DSContext ctx = DSApi.context();
        DocumentHelper documentHelper = ctx.getDocumentHelper();
        documentHelper.safeDeleteFolder(folder);
    }

    @Override
    public TypeDefinition getTypeDefinition() {
        return null;
    }

    @Override
    public ObjectData getFolderParent() throws EdmException {
        Folder f = this.folder;
        //dziwne ale z tym ifem dzia�a
        if(!objectId.equals(repositoryId))
            f = Folder.find(f.getParentId());

        return DSObjectDataConverter.convert(f);
    }

    @Override
    public Class getDSClass() {
        return Folder.class;
    }

    /**
     * Set Folder Children
     * @param folder
     * @param objects
     * @throws EdmException
     */
    private static void setFoldersChildren(Folder folder, List<ObjectInFolderData> objects) throws EdmException {
        List<Folder> childrens = folder.getChildFolders();

        if (childrens != null) {
            for (Folder child : childrens) {
                objects.addAll(convertObjectInFolder(child));
            }
        }
    }

    private static void setDocumentsChildren(Folder folder, List<ObjectInFolderData> objects) throws EdmException, SQLException {
        List<Long> docs = Document.findByFolder(folder.getId());

        if(docs != null && !docs.isEmpty()){
            for(Long id : docs){
                objects.addAll(convertObjectInFolder(Document.find(id)));
            }
        }
    }

    /**
     * Zwraca abstrakcyjny katalog s�ownik�w
     * @param objects
     */
    private void setDictionaryFolderChildren( List<ObjectInFolderData> objects) throws EdmException {
        ObjectData obj = DSObjectDataConverter.convertDictionaryFolder(DictionaryFolderDSCmis.dictionaryRootFolder, CmisPrepend.FOLDER.build(folder.getId()), folder.getPrettyPath());
        objects.addAll(DSObjectDataConverter.convertDataInFolderData(obj));
    }

    private static List<ObjectInFolderData> convertObjectInFolder(Document document) throws EdmException {
        //zwracam tylko Abstrakcyjne foldery dokuemnt�w
        ObjectData obj = DSObjectDataConverter.convertDocumentFolder(document);
        return DSObjectDataConverter.convertDataInFolderData(obj);
    }

    /**
     * Converts an object into folder webservice object.
     */
    public static List<ObjectInFolderData> convertObjectInFolder(Folder child) throws EdmException {
        ObjectData obj = DSObjectDataConverter.convert(child);
        return DSObjectDataConverter.convertDataInFolderData(obj);
    }

    protected FolderDSCmis(String objectId, String repostioryId, BaseTypeId baseTypeId) {
        super(objectId, repostioryId, baseTypeId);
    }

    /**
     * Dodanie uprawnie� do obiektu, oraz usuni�cie
     *
     * @param addAces
     * @param removeAces
     * @return
     */
    @Override
    public Acl applyAcl(Acl addAces, Acl removeAces) throws EdmException {
        AccessControlListImpl acl = new AccessControlListImpl();

        Map<String, List<String>> addedPerms = convertAclToMap(addAces);
        Map<String, List<String>> removePerms = convertAclToMap(removeAces);

        SecurityHelper.createPermission(folder.getId(), Folder.class, addedPerms);
        SecurityHelper.removePermissions(removePerms, ObjectPermission.find(Folder.class, folder.getId()));

        return acl;
    }

    /**
     * Dodanie uprawnie� do obiektu, oraz usuni�cie
     *
     * @param addAces
     * @return
     */
    @Override
    public Acl applyAcl(Acl addAces) throws EdmException {
        AccessControlListImpl acl = new AccessControlListImpl();

        Map<String, List<String>> addedPerms = convertAclToMap(addAces);

        SecurityHelper.applyObjectPermissions(folder.getId(), addedPerms, Folder.class);

        return acl;
    }

    /**
     * Konweruje Aclki na map�
     * @param addAces
     * @return
     */
    private Map<String, List<String>> convertAclToMap(Acl addAces) {
        Map<String, List<String>> permissions = Maps.newHashMap();

        if(addAces == null || CollectionUtils.isEmpty(addAces.getAces()))
            return permissions;

        for(Ace ace : addAces.getAces()){
            if(!permissions.containsKey(ace.getPrincipalId()))
                permissions.put(ace.getPrincipalId(), ace.getPermissions());
        }
        return permissions;
    }

    /**
     * Zwraca uprawnienia obiektu
     *
     * @param onlyBasicPermissions
     * @param extension
     * @return
     */
    @Override
    public Acl getAcl(Boolean onlyBasicPermissions, ExtensionsData extension) throws EdmException {
        Map<String, List<String>> permissions = ObjectPermission.findAndConvert(Folder.class, folder.getId());

        List<Ace> aces = Lists.newArrayList();

        for(Map.Entry<String, List<String>> entry : permissions.entrySet()){
            aces.add(new AccessControlEntryImpl(new AccessControlPrincipalDataImpl(entry.getKey()), entry.getValue()));
        }

        AccessControlListImpl acl = new AccessControlListImpl(aces);
        return acl;
    }
}
