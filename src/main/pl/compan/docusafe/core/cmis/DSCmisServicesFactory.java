package pl.compan.docusafe.core.cmis;

import org.apache.chemistry.opencmis.commons.impl.server.AbstractServiceFactory;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.apache.chemistry.opencmis.commons.server.CmisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.web.filter.CasUtils;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class DSCmisServicesFactory extends AbstractServiceFactory {
    private static final Logger log = LoggerFactory.getLogger(DSCmisServicesFactory.class);

    private static final String REPOSITORY_ID = "repository.id";
    private static final String REPOSITORY_ID_DEFAULT = "test-rep";

    private static final String REPOSITORY_NAME = "repository.name";
    private static final String REPOSITORY_NAME_DEFAULT = "Test Repository";


    private DSCmisService service;

    @Override
    public void init(Map<String, String> parameters) {
        // create a repository service
        service = new DSCmisService();
        log.info("Initialized DS repository");
    }

    @Override
    public void destroy() {
        log.info("Destroyed DS repository");
        DSApi._close();
    }

    @Override
    public CmisService getService(CallContext context) {
        log.trace("Wywo�anie Cmis przez {}", context.getUsername());
        try {
            Subject subject = null;
            if(context.getUsername() == null || context.getUsername().equals("ticket")) {
                String username = CasUtils.checkProxyTicket(context.getPassword());
                LoginContext lc = AuthUtil.getLoginContext(username, "", null);
                lc.login();
                subject = lc.getSubject();
            } else {
                subject = AuthUtil.login(context.getUsername(), context.getPassword(), null);
            }
            DSApi.open(subject);
        } catch (LoginException e) {
            log.error("B��d logowania {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        } catch (EdmException e) {
            log.error("B��d {}", e);
            throw new RuntimeException("B��d wewn�trzny");
        } catch (UnsupportedEncodingException e) {
            log.error("B��d {}", e);
            throw new RuntimeException("B��d wewn�trzny");
        }
        return service;
    }


}
