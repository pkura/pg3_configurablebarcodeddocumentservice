package pl.compan.docusafe.core.cmis.servlet;

import com.sun.xml.ws.api.handler.MessageHandlerContext;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.apache.chemistry.opencmis.server.impl.webservices.AbstractService;
import org.apache.chemistry.opencmis.server.impl.webservices.AuthHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.ws.authorization.AbstractAuthenticator;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Klasa weryfikująca logowanie w Cmis WebService
 */
public class DSCmisAuthHandler extends AuthHandler {

    private static final Logger log = LoggerFactory.getLogger(DSCmisAuthHandler.class);

    @Override
    public boolean handleMessage(MessageHandlerContext context) {
        boolean resp = super.handleMessage(context);
        return resp;
    }
}
