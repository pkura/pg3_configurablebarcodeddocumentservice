package pl.compan.docusafe.core.cmis.servlet;

import org.apache.catalina.connector.Response;
import org.apache.chemistry.opencmis.commons.enums.CmisVersion;
import org.apache.chemistry.opencmis.commons.exceptions.*;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.apache.chemistry.opencmis.commons.server.CmisService;
import org.apache.chemistry.opencmis.server.impl.ServerVersion;
import org.apache.chemistry.opencmis.server.impl.atompub.*;
import org.apache.chemistry.opencmis.server.shared.*;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.certificates.auth.PublicKeyManager;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.FormCallbackHandler;
import pl.compan.docusafe.util.NetUtil;
import pl.compan.docusafe.web.filter.HostVerificationFilter;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import static org.apache.chemistry.opencmis.server.impl.atompub.AbstractAtomPubServiceCall.*;
import static org.apache.chemistry.opencmis.server.impl.atompub.AbstractAtomPubServiceCall.RESOURCE_ACL;
import static org.apache.chemistry.opencmis.server.impl.atompub.AbstractAtomPubServiceCall.RESOURCE_POLICIES;
import static org.apache.chemistry.opencmis.server.shared.Dispatcher.*;
import static org.apache.chemistry.opencmis.server.shared.Dispatcher.METHOD_GET;

/**
 * Klasa REST CMIS APi
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class DSCmisAtomPubServlet extends AbstractCmisHttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(CmisAtomPubServlet.class.getName());

    private static final long serialVersionUID = 1L;

    private final Dispatcher dispatcher = new Dispatcher();

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        // set the binding
        setBinding(CallContext.BINDING_ATOMPUB);

        // get and CMIS version
        String cmisVersionStr = config.getInitParameter(PARAM_CMIS_VERSION);
        if (cmisVersionStr != null) {
            try {
                setCmisVersion(CmisVersion.fromValue(cmisVersionStr));
            } catch (IllegalArgumentException e) {
                LOG.warn("CMIS version is invalid! Setting it to CMIS 1.0.");
                setCmisVersion(CmisVersion.CMIS_1_0);
            }
        } else {
            LOG.warn("CMIS version is not defined! Setting it to CMIS 1.0.");
            setCmisVersion(CmisVersion.CMIS_1_0);
        }

        // initialize resources
        addResource("", METHOD_GET, new RepositoryService.GetRepositories());
        addResource(RESOURCE_TYPES, METHOD_GET, new RepositoryService.GetTypeChildren());
        addResource(RESOURCE_TYPES, METHOD_POST, new RepositoryService.CreateType());
        addResource(RESOURCE_TYPESDESC, METHOD_GET, new RepositoryService.GetTypeDescendants());
        addResource(RESOURCE_TYPE, METHOD_GET, new RepositoryService.GetTypeDefinition());
        addResource(RESOURCE_TYPE, METHOD_PUT, new RepositoryService.UpdateType());
        addResource(RESOURCE_TYPE, METHOD_DELETE, new RepositoryService.DeleteType());
        addResource(RESOURCE_CHILDREN, METHOD_GET, new NavigationService.GetChildren());
        addResource(RESOURCE_DESCENDANTS, METHOD_GET, new NavigationService.GetDescendants());
        addResource(RESOURCE_FOLDERTREE, METHOD_GET, new NavigationService.GetFolderTree());
        addResource(RESOURCE_PARENTS, METHOD_GET, new NavigationService.GetObjectParents());
        addResource(RESOURCE_CHECKEDOUT, METHOD_GET, new NavigationService.GetCheckedOutDocs());
        addResource(RESOURCE_ENTRY, METHOD_GET, new ObjectService.GetObject());
        addResource(RESOURCE_OBJECTBYID, METHOD_GET, new ObjectService.GetObject());
        addResource(RESOURCE_OBJECTBYPATH, METHOD_GET, new ObjectService.GetObjectByPath());
        addResource(RESOURCE_ALLOWABLEACIONS, METHOD_GET, new ObjectService.GetAllowableActions());
        addResource(RESOURCE_CONTENT, METHOD_GET, new ObjectService.GetContentStream());
        addResource(RESOURCE_CONTENT, METHOD_PUT, new ObjectService.SetOrAppendContentStream());
        addResource(RESOURCE_CONTENT, METHOD_DELETE, new ObjectService.DeleteContentStream());
        addResource(RESOURCE_CHILDREN, METHOD_POST, new ObjectService.Create());
        addResource(RESOURCE_RELATIONSHIPS, METHOD_POST, new ObjectService.CreateRelationship());
        addResource(RESOURCE_ENTRY, METHOD_PUT, new ObjectService.UpdateProperties());
        addResource(RESOURCE_ENTRY, METHOD_DELETE, new ObjectService.DeleteObject());
        addResource(RESOURCE_CHILDREN, METHOD_DELETE, new ObjectService.DeleteTree()); // 1.1
        addResource(RESOURCE_DESCENDANTS, METHOD_DELETE, new ObjectService.DeleteTree());
        addResource(RESOURCE_FOLDERTREE, METHOD_DELETE, new ObjectService.DeleteTree());
        addResource(RESOURCE_BULK_UPDATE, METHOD_POST, new ObjectService.BulkUpdateProperties());
        addResource(RESOURCE_CHECKEDOUT, METHOD_POST, new VersioningService.CheckOut());
        addResource(RESOURCE_VERSIONS, METHOD_GET, new VersioningService.GetAllVersions());
        addResource(RESOURCE_VERSIONS, METHOD_DELETE, new VersioningService.DeleteAllVersions());
        addResource(RESOURCE_QUERY, METHOD_GET, new DiscoveryService.Query());
        addResource(RESOURCE_QUERY, METHOD_POST, new DiscoveryService.Query());
        addResource(RESOURCE_CHANGES, METHOD_GET, new DiscoveryService.GetContentChanges());
        addResource(RESOURCE_RELATIONSHIPS, METHOD_GET, new RelationshipService.GetObjectRelationships());
        addResource(RESOURCE_UNFILED, METHOD_POST, new MultiFilingService.RemoveObjectFromFolder());
        addResource(RESOURCE_ACL, METHOD_GET, new AclService.GetAcl());
        addResource(RESOURCE_ACL, METHOD_PUT, new AclService.ApplyAcl());
        addResource(RESOURCE_POLICIES, METHOD_GET, new PolicyService.GetAppliedPolicies());
        addResource(RESOURCE_POLICIES, METHOD_POST, new PolicyService.ApplyPolicy());
        addResource(RESOURCE_POLICIES, METHOD_DELETE, new PolicyService.RemovePolicy());
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        QueryStringHttpServletRequestWrapper qsRequest = new QueryStringHttpServletRequestWrapper(request);

        // set default headers
        response.addHeader("Cache-Control", "private, max-age=0");
        response.addHeader("Server", ServerVersion.OPENCMIS_SERVER);

        // create a context object, dispatch and handle exceptions
        CallContext context = null;
        try {
            if (!validateCert(request)) {
                LOG.error("Nie udane logowanie certyfikatem ssl dla hosta {} -> {}", request.getRemoteHost(), request.getRemoteAddr());
                // response.sendRedirect(request.getContextPath() + "/ssl-login-error.jsp");
                String msg = "Nieudane logowanie certyfikatem ssl dla danego hosta " + request.getRemoteHost();
                response.setHeader("Reason", msg);
                response.sendError(HttpServletResponse.SC_FORBIDDEN, msg);
                response.setStatus(Response.SC_BAD_REQUEST);
            }
            context = createContext(getServletContext(), qsRequest, response);
            dispatch(context, qsRequest, response);
        } catch (Exception e) {
            if (e instanceof CmisUnauthorizedException) {
                response.setHeader("WWW-Authenticate", "Basic realm=\"CMIS\"");
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authorization Required");
            } else if (e instanceof CmisPermissionDeniedException) {
                if ((context == null) || (context.getUsername() == null)) {
                    response.setHeader("WWW-Authenticate", "Basic realm=\"CMIS\"");
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authorization Required");
                } else {
                    response.sendError(getErrorCode((CmisPermissionDeniedException) e), e.getMessage());
                }
            } else {
                printError(e, response);
            }
        }

        // we are done.
        response.flushBuffer();
    }

    /**
     * Waliduje przes�any certyfiakt
     *
     * @param request
     * @return
     */
    private boolean validateCert(HttpServletRequest request) {
        String certBase64 = request.getHeader(HostVerificationFilter.CERTIFICATE_SSL);
        LOG.trace("cert {}", certBase64);
        boolean isOk = false, openCotext = false;

        try {
            openCotext = DSApi.openContextIfNeeded();
            isOk = PublicKeyManager.validateByHostName(request.getRemoteAddr(), fixUri(request.getRequestURI()), certBase64);
        } catch (Exception e) {
            LOG.error("", e);
        } finally {
            DSApi.closeContextIfNeeded(openCotext);
        }

        return isOk;
    }

    private String fixUri(String uri) {
        if (uri.startsWith("/docusafe"))
            uri = uri.substring(("/docusafe").length()); // drobny fix

        return uri;
    }

    /**
     * Registers a new resource.
     */
    protected void addResource(String resource, String httpMethod, ServiceCall serviceCall) {
        dispatcher.addResource(resource, httpMethod, serviceCall);
    }

    /**
     * Dispatches to feed, entry or whatever.
     */
    private void dispatch(CallContext context, HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        CmisService service = null;
        try {
            // get the service
            service = getServiceFactory().getService(context);

            // analyze the path
            String[] pathFragments = HttpUtils.splitPath(request);

            if (pathFragments.length < 2) {
                // root -> service document
                dispatcher.dispatch("", METHOD_GET, context, service, null, request, response);
                return;
            }

            String method = request.getMethod();
            String repositoryId = pathFragments[0];
            String resource = pathFragments[1];

            // dispatch
            boolean callServiceFound = dispatcher.dispatch(resource, method, context, service, repositoryId, request,
                    response);

            // if the dispatcher couldn't find a matching service
            // -> return an error message
            if (!callServiceFound) {
                response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Unknown operation");
            }
        }finally {
            if (service != null) {
                service.close();
            }
        }
    }

    /**
     * Translates an exception in an appropriate HTTP error code.
     */
    protected int getErrorCode(CmisBaseException ex) {
        if (ex instanceof CmisConstraintException) {
            return 409;
        } else if (ex instanceof CmisContentAlreadyExistsException) {
            return 409;
        } else if (ex instanceof CmisFilterNotValidException) {
            return 400;
        } else if (ex instanceof CmisInvalidArgumentException) {
            return 400;
        } else if (ex instanceof CmisNameConstraintViolationException) {
            return 409;
        } else if (ex instanceof CmisNotSupportedException) {
            return 405;
        } else if (ex instanceof CmisObjectNotFoundException) {
            return 404;
        } else if (ex instanceof CmisPermissionDeniedException) {
            return 403;
        } else if (ex instanceof CmisStorageException) {
            return 500;
        } else if (ex instanceof CmisStreamNotSupportedException) {
            return 403;
        } else if (ex instanceof CmisUpdateConflictException) {
            return 409;
        } else if (ex instanceof CmisVersioningException) {
            return 409;
        }

        return 500;
    }

    /**
     * Prints the error HTML page.
     */
    protected void printError(Exception ex, HttpServletResponse response) {
        int statusCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        String exceptionName = "runtime";

        if (ex instanceof CmisRuntimeException) {
            LOG.error(ex.getMessage(), ex);
        } else if (ex instanceof CmisBaseException) {
            statusCode = getErrorCode((CmisBaseException) ex);
            exceptionName = ((CmisBaseException) ex).getExceptionName();
        } else {
            LOG.error(ex.getMessage(), ex);
        }

        if (response.isCommitted()) {
            LOG.warn("Failed to send error message to client. Response is already committed.", ex);
            return;
        }

        try {
            PrintWriter pw = response.getWriter();
            response.setStatus(statusCode);
            response.setContentType("text/html");

            pw.print("<html><head><title>Apache Chemistry OpenCMIS - "
                    + exceptionName
                    + " error</title>"
                    + "<style><!--H1 {font-size:24px;line-height:normal;font-weight:bold;background-color:#f0f0f0;color:#003366;border-bottom:1px solid #3c78b5;padding:2px;} "
                    + "BODY {font-family:Verdana,arial,sans-serif;color:black;font-size:14px;} "
                    + "HR {color:#3c78b5;height:1px;}--></style></head><body>");
            pw.print("<h1>HTTP Status " + statusCode + " - <!--exception-->" + exceptionName + "<!--/exception--></h1>");
            pw.print("<p><!--message-->" + StringEscapeUtils.escapeHtml(ex.getMessage()) + "<!--/message--></p>");

            String st = ExceptionHelper.getStacktraceAsString(ex);
            if (st != null) {
                pw.print("<hr noshade='noshade'/><!--stacktrace--><pre>\n" + st
                        + "\n</pre><!--/stacktrace--><hr noshade='noshade'/>");
            }

            pw.print("</body></html>");
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            try {
                response.sendError(statusCode, ex.getMessage());
            } catch (Exception en) {
            }
        }
    }


}
