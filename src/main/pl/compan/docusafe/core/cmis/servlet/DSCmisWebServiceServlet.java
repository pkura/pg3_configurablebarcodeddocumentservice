package pl.compan.docusafe.core.cmis.servlet;

import com.sun.xml.ws.transport.http.servlet.WSServletDelegate;
import org.apache.catalina.connector.Response;
import org.apache.chemistry.opencmis.server.impl.atompub.CmisAtomPubServlet;
import org.apache.chemistry.opencmis.server.impl.webservices.CmisWebServicesServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.certificates.auth.PublicKeyManager;
import pl.compan.docusafe.web.filter.HostVerificationFilter;
import pl.compan.docusafe.ws.authorization.AbstractAuthenticator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * klasa WebSerwisu
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class DSCmisWebServiceServlet extends CmisWebServicesServlet {

    private static final Logger log = LoggerFactory.getLogger(CmisAtomPubServlet.class.getName());

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            if (!validateCert(request)) {
                log.error("Nie udane logowanie certyfikatem ssl dla hosta {} -> {}", request.getRemoteHost(), request.getRemoteAddr());
                // response.sendRedirect(request.getContextPath() + "/ssl-login-error.jsp");
                String msg = "Nieudane logowanie certyfikatem ssl dla danego hosta " + request.getRemoteHost();
                response.setHeader("Reason", msg);
                response.sendError(HttpServletResponse.SC_FORBIDDEN, msg);
                response.setStatus(Response.SC_BAD_REQUEST);
                return;
            }
            super.service(request, response);
        }catch(Exception e){
            log.error("", e);
        }finally{
            DSApi._close();
        }
    }

    /**
     * Waliduje przes�any certyfiakt
     *
     * @param request
     * @return
     */
    private boolean validateCert(HttpServletRequest request) {
        String certBase64 = request.getHeader(HostVerificationFilter.CERTIFICATE_SSL);
        log.trace("cert {}", certBase64);
        boolean isOk = false, openCotext = false;

        try {
            openCotext = DSApi.openContextIfNeeded();
            isOk = PublicKeyManager.validateByHostName(request.getRemoteAddr(), fixUri(request.getRequestURI()), certBase64);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            DSApi.closeContextIfNeeded(openCotext);
        }

        return isOk;
    }

    private String fixUri(String uri) {
        if (uri.startsWith("/docusafe"))
            uri = uri.substring(("/docusafe").length()); // drobny fix

        return uri;
    }

    @Override
    protected WSServletDelegate getDelegate(ServletConfig servletConfig) {
        WSServletDelegate delegate = null;
        try{
            delegate = super.getDelegate(servletConfig);
        }catch(Exception e){
            log.error("", e);
        }

        return delegate;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        try{
            super.init(config);
        }catch(Exception e){
            log.error("", e);
        }
    }
}
