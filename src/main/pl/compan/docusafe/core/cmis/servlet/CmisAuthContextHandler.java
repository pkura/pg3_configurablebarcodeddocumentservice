package pl.compan.docusafe.core.cmis.servlet;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.chemistry.opencmis.commons.impl.Base64;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.apache.chemistry.opencmis.server.shared.CallContextHandler;
import org.apache.commons.configuration.web.ServletContextConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.users.auth.FormCallbackHandler;
import pl.compan.docusafe.util.NetUtil;
import pl.compan.docusafe.util.ServletUtils;

import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class CmisAuthContextHandler implements CallContextHandler, Serializable {


    private static final Logger log = LoggerFactory.getLogger(CmisAuthContextHandler.class);
    @Override
    public Map<String, String> getCallContextMap(HttpServletRequest request) {
        Map<String, String> result = null;

        log.error("{}", request);
        String authHeader = request.getHeader("Authorization");
        if ((authHeader != null) && (authHeader.trim().toLowerCase(Locale.ENGLISH).startsWith("basic "))) {
            int x = authHeader.lastIndexOf(' ');
            if (x == -1) {
                return result;
            }

            String credentials = null;
            try {
                credentials = new String(Base64.decode(authHeader.substring(x + 1).getBytes("ISO-8859-1")),
                        "ISO-8859-1");
            } catch (Exception e) {
                return result;
            }

            x = credentials.indexOf(':');
            if (x == -1) {
                return result;
            }

            // extract user and password and add them to map
            result = new HashMap<String, String>();
            result.put(CallContext.USERNAME, credentials.substring(0, x));
            result.put(CallContext.PASSWORD, credentials.substring(x + 1));
        }

        return result;
    }
}
