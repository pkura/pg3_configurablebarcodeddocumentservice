package pl.compan.docusafe.core.cmis;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.data.Properties;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinitionList;
import org.apache.chemistry.opencmis.commons.enums.*;
import org.apache.chemistry.opencmis.commons.exceptions.*;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.*;
import org.apache.chemistry.opencmis.commons.impl.server.AbstractCmisService;
import org.apache.chemistry.opencmis.commons.server.ObjectInfo;
import org.apache.chemistry.opencmis.commons.spi.Holder;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentHelper;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.cmis.model.CmisPrepend;
import pl.compan.docusafe.core.cmis.model.DSCmisManager;
import pl.compan.docusafe.core.cmis.model.FolderDSCmis;
import pl.compan.docusafe.service.imports.dsi.DSIManager;

import java.math.BigInteger;
import java.util.*;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class DSCmisService extends AbstractCmisService {
    private static final Logger log = LoggerFactory.getLogger(DSCmisService.class);

    private static final RepositoryInfoImpl fRepInfo = new RepositoryInfoImpl();

    public DSCmisService() {
    }

    /**
     * Ustawiamy ogolne informacje o repozytorium
     */
    private void getRootRepository() {
        if (StringUtils.isNotBlank(fRepInfo.getId()))
            return;
        try
        {
            setRootRepository();
            setVendorRepoInfo();
            setRepositoryCapabilities();
        }catch(Exception e){
            log.error("", e);
        }
    }

    /**
     * Ustawia informacje o wentorze repozytorium
     */
    private void setVendorRepoInfo() {
        fRepInfo.setVendorName("COM-PAN System Sp. z o.o.");
        fRepInfo.setProductName("Docusafe");
        fRepInfo.setProductVersion("1.0");
    }

    /**
     * Metoda ustawia korze� repozytorium
     * @throws EdmException
     */
    private void setRootRepository() throws EdmException {
        Folder rootFolder = Folder.getRootFolder();
        String rootId = CmisPrepend.FOLDER.build(rootFolder.getId());
        fRepInfo.setId(rootId);
        fRepInfo.setName(rootFolder.getTitle());
        fRepInfo.setDescription(rootFolder.getTitle());
        fRepInfo.setCmisVersionSupported("1.0");
        fRepInfo.setRootFolder(rootId);
    }

    /**
     * Metoda ustawia mo�liwo�ci jakie mo�e spe�nia� repozytorium
     * Do rozgryzienia co robi� poszczeg�lne metody i co oznaczaj�.
     */
    private void setRepositoryCapabilities() {
        RepositoryCapabilitiesImpl repositoryCapabilities = new RepositoryCapabilitiesImpl();
        repositoryCapabilities.setCapabilityAcl(CapabilityAcl.MANAGE);
        repositoryCapabilities.setAllVersionsSearchable(false);
        repositoryCapabilities.setSupportsGetDescendants(false);
        repositoryCapabilities.setSupportsGetFolderTree(false);
        repositoryCapabilities.setSupportsMultifiling(false);
        repositoryCapabilities.setIsPwcSearchable(false);
        repositoryCapabilities.setIsPwcUpdatable(false);
        repositoryCapabilities.setSupportsUnfiling(false);
        repositoryCapabilities.setSupportsVersionSpecificFiling(false);

        fRepInfo.setCapabilities(repositoryCapabilities);
    }

    /**
     * Zwraca informacje o repozytoirum. Metoda jest praktycznie zawsze wywo�ywana przy ka�dej metodzie.
     * Nawet kilka razy przez Cmis. Pozwala to obs�ugiwa� kilak repozytorium.
     * @param repositoryId
     * @param extension
     * @return
     */
    @Override
    public RepositoryInfo getRepositoryInfo(String repositoryId, ExtensionsData extension) {
        log.info("getRepositoryInfo repId={} ext={}", repositoryId, extension);
        getRootRepository();

        if (StringUtils.isBlank(fRepInfo.getId()) && !fRepInfo.getId().equals(repositoryId)) {
            throw new CmisObjectNotFoundException( "Repozytorium '" + repositoryId + "' nie istnieje!");
        }

        return fRepInfo;
    }

    @Override
    public List<RepositoryInfo> getRepositoryInfos(ExtensionsData extension) {
        getRootRepository();
        log.info("getRepositoryInfos {} rep={}", extension, fRepInfo);
        return Collections.singletonList((RepositoryInfo) fRepInfo);
    }

    @Override
    public ObjectInFolderList getChildren(String repositoryId, String folderId,
                                          String filter, String orderBy, Boolean includeAllowableActions,
                                          IncludeRelationships includeRelationships, String renditionFilter,
                                          Boolean includePathSegment, BigInteger maxItems,
                                          BigInteger skipCount, ExtensionsData extension) {
        log.info("getChildren repoId={} folderId={} filter={}",repositoryId, folderId, filter);
        ObjectInFolderList objects = null;

        try{
            objects = DSCmisManager.getInstance(folderId, repositoryId).getChildren();
        }catch(Exception e){
            log.error("", e);
            throw new CmisInvalidArgumentException(e.getMessage());
        }

        return objects;
    }

    /**
     *
     * @param repositoryId
     * @param objectId
     * @param filter
     * @param includeAllowableActions
     * @param includeRelationships
     * @param renditionFilter
     * @param includePolicyIds
     * @param includeAcl
     * @param extension
     * @return
     */
    @Override
    public ObjectData getObject(String repositoryId, String objectId,
                                String filter, Boolean includeAllowableActions,
                                IncludeRelationships includeRelationships, String renditionFilter,
                                Boolean includePolicyIds, Boolean includeAcl,
                                ExtensionsData extension) {
        log.info("getObject repId={} obId={} filter={} includeAlloAction={} {} {} {} {} {}", repositoryId, objectId,
                filter, includeAllowableActions,
                includeRelationships, renditionFilter,
                includePolicyIds, includeAcl,
                extension);

        ObjectData objData = null;
        try
        {
            objData = DSCmisManager.getInstance(objectId, repositoryId).getObject();
        }catch(Exception e){
            log.error("", e);
        }
        Preconditions.checkNotNull(objData, "Nie znaleziono obiektu %s w repozytorium %s", objectId, repositoryId);
        return objData;
    }

    @Override
    protected ObjectInfo getObjectInfoIntern(String repositoryId, ObjectData object) {
        try{
            return super.getObjectInfoIntern(repositoryId, object);
        }catch(Exception e){
            log.error("", e);
            throw new CmisInvalidArgumentException(e.getMessage());
        }
    }

    @Override
    public ObjectData getFolderParent(String repositoryId, String folderId, String filter, ExtensionsData extension) {
        log.info("getFolderParent {} {} {}", repositoryId, folderId, filter);
        ObjectData result = null;

        try{
            result = DSCmisManager.getInstance(folderId, repositoryId).getFolderParent();
        }catch(Exception e){
            log.error("", e);
            throw new CmisInvalidArgumentException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<ObjectParentData> getObjectParents(String repositoryId,
                                                   String objectId, String filter, Boolean includeAllowableActions,
                                                   IncludeRelationships includeRelationships, String renditionFilter,
                                                   Boolean includeRelativePathSegment, ExtensionsData extension) {
        log.info("getObjectParents {} {} {}", repositoryId, objectId, filter);
        List<ObjectParentData> result = null;

        try{
            result = DSCmisManager.getInstance(objectId, repositoryId).getObjectParents();
        }catch(Exception e){
            log.error("", e);
            throw new CmisInvalidArgumentException(e.getMessage());
        }
        return result;
    }

    @Override
    public String create(String repositoryId, Properties properties, String folderId, ContentStream contentStream, VersioningState versioningState, List<String> policies, ExtensionsData extension) {
        try{
            return super.create(repositoryId, properties, folderId, contentStream, versioningState, policies, extension);
        }catch(CmisInvalidArgumentException e){
            log.error("", e);
            throw new CmisInvalidArgumentException(e.getMessage());
        }
    }



    @Override
    public String createFolder(String repositoryId, Properties properties, String folderId, List<String> policies, Acl addAces, Acl removeAces, ExtensionsData extension) {
        log.info("createFolder repoId={} folderId={} props={}", repositoryId, properties, folderId);

        PropertyData<?> objectNameProperty = DSCmisUtils.checkProperty(properties, PropertyIds.NAME);
        String name = objectNameProperty.getFirstValue().toString();
        try{
            Folder parentFolder = (Folder) DSCmisManager.getInstance(folderId, repositoryId).getDSObject();

            if(parentFolder == null)
                throw new CmisInvalidArgumentException("Taki folder nie istnieje, w kt�rym ma zosta� utworzony folder: " + name);

            DSApi.context().begin();
            Folder targetFolder = parentFolder.createSubfolderIfNotPresent(name);
            DSApi.context().commit();
            log.error("create id {}", CmisPrepend.FOLDER.build(targetFolder.getId()));
            return CmisPrepend.FOLDER.build(targetFolder.getId());
        }catch(Exception e){
            log.error("", e);
            DSApi.context()._rollback();
            throw new CmisInvalidArgumentException(e.getMessage());
        }
    }

    /**
     * Aktualizacja obiekt�w
     * @param repositoryId
     * @param objectId
     * @param changeToken
     * @param properties
     * @param extension
     */
    @Override
    public void updateProperties(String repositoryId, Holder<String> objectId, Holder<String> changeToken, Properties properties, ExtensionsData extension) {
        log.info("updateProperties repo={} ob={} prop={}",repositoryId,objectId, properties);
        //updateProperties repo=folder-0 ob=Holder(document-259) prop=Properties Data [properties=[Property [id=cmis:description, display Name=null, local name=null, query name=null, values=[jakis fajny opis tego pisma]][extensions=null]]][extensions=null]
        try{
            DSApi.context().begin();
            DSCmisManager.getInstance(objectId.getValue(), repositoryId).updateProperties(properties);

            DSApi.context().commit();
        }catch(Exception e){
            log.error("", e);
            throw new CmisInvalidArgumentException(e.getMessage());
        }
    }

    @Override
    public void deleteObjectOrCancelCheckOut(String repositoryId, String objectId, Boolean allVersions, ExtensionsData extension) {
        log.info("deleteObject rep={} object={}", repositoryId, objectId);

        try{
            DSApi.context().begin();
            DSCmisManager.getInstance(objectId, repositoryId).deleteObjectOrCancelCheckOut();
            DSApi.context().commit();
        }catch(Exception e){
            log.error("", e);
            DSApi.context()._rollback();
            throw new CmisInvalidArgumentException(e.getMessage());
        }
    }

    @Override
    public TypeDefinitionList getTypeChildren(String repositoryId,
                                              String typeId, Boolean includePropertyDefinitions,
                                              BigInteger maxItems, BigInteger skipCount, ExtensionsData extension) {
        log.info("getTypeChildren rep={} typeId{} ", repositoryId, typeId);
        TypeDefinitionListImpl typeDefinitionLists = new TypeDefinitionListImpl();
        List<TypeDefinition> typeDefinitions = Lists.newArrayList();
        typeDefinitionLists.setList(typeDefinitions);

        try{
            FolderTypeDefinitionImpl fTypeDefinition = new FolderTypeDefinitionImpl();
            Folder folder = (Folder) DSCmisManager.getInstance(repositoryId, repositoryId).getDSObject();;
            ObjectData objData = DSObjectDataConverter.convert(folder);

            fTypeDefinition.setId(BaseTypeId.CMIS_FOLDER.value());
            fTypeDefinition.setBaseTypeId(BaseTypeId.CMIS_FOLDER);
            fTypeDefinition.setLocalName(folder.getTitle());

            DSCmisUtils.setPropertyDefinitions(fTypeDefinition, objData);

            typeDefinitions.add(fTypeDefinition);
        }catch(Exception e){
            log.error("", e);
        }

        return typeDefinitionLists;
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * <b>Implementation Hints:</b>
     * <ul>
     * <li>Bindings: AtomPub, Web Services, Browser, Local</li>
     * <li>Implementation is optional.</li>
     * <li>Object infos should contain the returned object.</li>
     * </ul>
     */
    public ObjectData getObjectByPath(String repositoryId, String path, String filter, Boolean includeAllowableActions,
                                      IncludeRelationships includeRelationships, String renditionFilter, Boolean includePolicyIds,
                                      Boolean includeAcl, ExtensionsData extension) {
        log.info("getObjectByPath rep={} path={}", repositoryId, path);
        ObjectData objectData = null;
        try{
            objectData = DSCmisManager.getObjectByPath(repositoryId, path);
        }catch(Exception e){
            log.error("", e);
        }

        Preconditions.checkNotNull(objectData, "Nie znaleziono obiektu %s w repozytorium %s", path,  repositoryId);
        return objectData;
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * <b>Implementation Hints:</b>
     * <ul>
     * <li>Bindings: AtomPub, Web Services, Browser, Local</li>
     * <li>Implementation is optional.</li>
     * </ul>
     */
    public ContentStream getContentStream(String repositoryId, String objectId, String streamId, BigInteger offset,
                                          BigInteger length, ExtensionsData extension) {
        log.info("getContentStream repo={} obId={} streamId={} offset={} length={} ext={}", repositoryId,
                objectId, streamId, offset, length, extension);
        ContentStream stream = null;
        //Client Cmis Api defaultowo wywoluje metod� getContentStream(docId, null, null, null);
       // String streamIdFixed = StringUtils.isBlank(streamId) ? "1" : streamId;
        try{
            stream = DSCmisManager.getInstance(objectId, repositoryId).getContentStream(streamId, offset, length, extension);
        }catch(EdmException e){
            log.error("", e);
            throw new CmisNotSupportedException(e.getMessage());
        }catch(Exception e){
            log.error("", e);
            throw new CmisNotSupportedException("Nieoczekiwany b��d!");
        }

        return stream;
    }

    @Override
    public String createDocument(String repositoryId, Properties properties, String folderId, ContentStream contentStream, VersioningState versioningState, List<String> policies, Acl addAces, Acl removeAces, ExtensionsData extension) {
        log.info("createDocument repo={}, folderId={}, contentStream={}, versioningState={}, polic={},addAces={},removeAces={} ext={}",
                repositoryId, properties, folderId, contentStream, versioningState,
                policies, addAces, removeAces, extension);
        try{
            return DSCmisManager.createDocument(repositoryId, properties, folderId, contentStream, versioningState, policies, addAces, removeAces, extension);
        }catch(Exception e){
            log.error("", e);
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public String createDocumentFromSource(String repositoryId, String sourceId, Properties properties, String folderId, VersioningState versioningState, List<String> policies, Acl addAces, Acl removeAces, ExtensionsData extension) {
        log.info("createDocument repo={}, folderId={}, sourceId={}, versioningState={}, polic={},addAces={},removeAces={} ext={}",
                repositoryId, properties, folderId, sourceId, versioningState,
                policies, addAces, removeAces, extension);
        return super.createDocumentFromSource(repositoryId, sourceId, properties, folderId, versioningState, policies, addAces, removeAces, extension);
    }

    @Override
    public void setContentStream(String repositoryId, Holder<String> objectId, Boolean overwriteFlag,
                                 Holder<String> changeToken, ContentStream contentStream, ExtensionsData extension) {
        log.info("setContentStream repo={} holder={} owerrideFlag={} changeToken={} contentStream={} ext={}",
                repositoryId, objectId, overwriteFlag, changeToken, contentStream, extension);

        try{
            DSApi.context().begin();
            DSCmisManager.getInstance(objectId.getValue(), repositoryId).setContentStream(contentStream, overwriteFlag,
                    changeToken.getValue(), extension);
            DSApi.context().commit();
        }catch(Exception e){
            log.error("", e);
            throw new IllegalArgumentException("Nieoczekiwany b��d!");
        }
    }

    @Override
    public TypeDefinition getTypeDefinition(String repositoryId, String typeId, ExtensionsData extension) {
        log.info("getTypeDefinition {} {} {}", repositoryId, typeId, extension);
        FolderTypeDefinitionImpl fTypeDefinition = new FolderTypeDefinitionImpl();
        try{
            Folder folder =(Folder) DSCmisManager.getInstance(repositoryId, repositoryId).getDSObject();;
            ObjectData objData = DSObjectDataConverter.convert(folder);

            fTypeDefinition.setId(typeId);
            fTypeDefinition.setBaseTypeId(BaseTypeId.fromValue(typeId));
            fTypeDefinition.setLocalName(folder.getTitle());

            DSCmisUtils.setPropertyDefinitions(fTypeDefinition, objData);
        }catch(Exception e){
            log.error("", e);
            throw new IllegalArgumentException("Nieoczekiwany b��d!");
        }

        return fTypeDefinition;
    }

    @Override
    public void close() {
        super.close();
        DSApi._close();
    }


    /**
     * {@inheritDoc}
     * <p/>
     * <p/>
     * <b>Implementation Hints:</b>
     * <ul>
     * <li>Bindings: Web Services, Browser, Local</li>
     * <li>Implementation is optional.</li>
     * </ul>
     *
     * @param repositoryId
     * @param objectId
     * @param addAces
     * @param removeAces
     * @param aclPropagation
     * @param extension
     */
    @Override
    public Acl applyAcl(String repositoryId, String objectId, Acl addAces, Acl removeAces, AclPropagation aclPropagation, ExtensionsData extension) {
        log.info("applyAcl WS repId={} objId={} addAces={} removeAces={} aclPropa={} exten={}", repositoryId, objectId, addAces, removeAces, aclPropagation, extension);
        Acl acl = null;
        try{
            DSApi.context().begin();
            acl = DSCmisManager.getInstance(objectId, repositoryId).applyAcl(addAces, removeAces);
            DSApi.context().commit();
            acl = getAcl(repositoryId, objectId, false, null);
        }catch(Exception e){
            log.error("", e);
            DSApi.context()._rollback();
            throw new IllegalArgumentException("Nieoczekiwany b��d!");
        } 
        return acl;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p/>
     * <b>Implementation Hints:</b>
     * <ul>
     * <li>Bindings: AtomPub</li>
     * <li>Implementation is optional.</li>
     * </ul>
     *
     * @param repositoryId
     * @param objectId
     * @param aces
     * @param aclPropagation
     */
    @Override
    public Acl applyAcl(String repositoryId, String objectId, Acl aces, AclPropagation aclPropagation) {
        log.info("applyAcl repId={} objId={} addAces={} aclPropa={} exten={}", repositoryId, objectId, aces, aclPropagation);
        Acl acl = null;
        try{
            DSApi.context().begin();
            acl = DSCmisManager.getInstance(objectId, repositoryId).applyAcl(aces);
            DSApi.context().commit();

            acl = getAcl(repositoryId, objectId, false, null);
        }catch(Exception e){
            log.error("", e);
            DSApi.context()._rollback();
            throw new IllegalArgumentException("Nieoczekiwany b��d!");
        }

        return acl;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p/>
     * <b>Implementation Hints:</b>
     * <ul>
     * <li>Bindings: AtomPub, Web Services, Browser, Local</li>
     * <li>Implementation is optional.</li>
     * </ul>
     *
     * @param repositoryId
     * @param objectId
     * @param onlyBasicPermissions
     * @param extension
     */
    @Override
    public Acl getAcl(String repositoryId, String objectId, Boolean onlyBasicPermissions, ExtensionsData extension) {
       log.info("getAcl repoId={} objectId={} onlyBasicPermi={} extData={}", repositoryId, objectId, onlyBasicPermissions, extension);
        Acl acl = null;
        try{
            acl = DSCmisManager.getInstance(objectId, repositoryId).getAcl(onlyBasicPermissions, extension);
        }catch(Exception e){
            log.error("", e);
            throw new IllegalArgumentException("Nieoczekiwany b��d");
        }
        log.info("acl.size() -> {}", acl.getAces().size());
        return acl;
    }
}