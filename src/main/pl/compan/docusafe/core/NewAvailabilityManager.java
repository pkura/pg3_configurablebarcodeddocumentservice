package pl.compan.docusafe.core;

import java.io.File;
import java.util.Iterator;

import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.tree.OverrideCombiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.cfg.Configuration;

/**
 * Nowy AvalibilityManager kozystajacy z apache commons configuration
 * @author tomek
 *
 */
public class NewAvailabilityManager
{

	private static final Logger log = LoggerFactory.getLogger(NewAvailabilityManager.class);
	public static final String HOME_CONFIG = "homeConfig";
	public static final String LICENSE_CONFIG = "licenseConfig";
	public static final String DEFAULT_CONFIG = "defaultConfig";
	
	
	private static NewAvailabilityManager instance;
	
	private CombinedConfiguration configuration;
	
	private NewAvailabilityManager()
	{
		
			PropertiesConfiguration defaultConfig = new PropertiesConfiguration();
			try
			{
				defaultConfig.load(Docusafe.class.getClassLoader().getResourceAsStream("available.properties"));
			}
			catch (Exception e) 
			{
				log.error(e.getMessage(),e);
			}
				
			PropertiesConfiguration licenseConfig = new PropertiesConfiguration();
			try
			{
				if(Configuration.hasExtra("business"))
	            {
					licenseConfig.load(Docusafe.class.getClassLoader().getResourceAsStream("availableBusiness.properties"));
	            }
				else
				{
					licenseConfig.load(Docusafe.class.getClassLoader().getResourceAsStream("availableAdministration.properties"));
				}
			}
			catch (Exception e) 
			{
				log.error(e.getMessage(),e);
			}
			
			PropertiesConfiguration homeConfig = new PropertiesConfiguration();
			try
			{
				homeConfig.load(new File(Docusafe.getHome() + "/available.properties"));
			}
			catch (Exception e) 
			{
				log.error(e.getMessage(),e);
			}
			
			OverrideCombiner combiner = new OverrideCombiner();
			configuration = new CombinedConfiguration(combiner);
			
			configuration.addConfiguration(homeConfig, HOME_CONFIG);
			configuration.addConfiguration(licenseConfig, LICENSE_CONFIG);
			configuration.addConfiguration(defaultConfig, DEFAULT_CONFIG);
			
			configuration.setThrowExceptionOnMissing(true);
		
	}
	
	private static NewAvailabilityManager getInstance()
	{
		if(instance == null)
			instance = new NewAvailabilityManager();
		
		return instance;
	}
	
	private Boolean getValueOrNull(String key, String configName)
	{
		try
		{
			if(configName != null)
			{
				return new Boolean(this.configuration.getConfiguration(configName).getBoolean(key));
			}
			else
			{
				return new Boolean(this.configuration.getBoolean(key));
			}
		}
		catch (Exception e) 
		{
			return null;
		}
	}
	
	private boolean getValue(String key, String configName)
	{
		try
		{
			if(configName != null)
			{
				return this.configuration.getConfiguration(configName).getBoolean(key);
			}
			else
			{
				return this.configuration.getBoolean(key);
			}
		}
		catch (Exception e) 
		{
			if(log.isTraceEnabled())
				log.trace("NewAvailabilityManager "+e.getMessage());
			return false;
		}
	}
	
	// metody publiczne dla availability managera
	
	/**
	 * zwraca iterator po kluczach dostepnych w ogolnej konfiguracji (czyli po wszystkich)
	 */
	public static Iterator getAvailablesKeysIterator()
	{
		return getInstance().configuration.getKeys();
	}
	
	/**
	 * Metoda ktora powinno sie sprawdzac wartosc availabla
	 * @param key
	 * @return true jesli opcja jest wlaczona, false jesli opcja jest wylaczona lub nie ma dla niej klucza
	 */
	public static boolean isAvailable(String key)
	{
		return getInstance().getValue(key, null);
	}
	
	/**
	 * metoda dla sprawdzenia wartosci availabla w konkretnej konfiguracji
	 * @param key
	 * @param configName
	 * @return null jesli nie istnieje klucz w konkretnej konfiguracji, jesli istnieje to true albo false
	 */
	public static Boolean isAvailableOrNull(String key, String configName)
	{
		return getInstance().getValueOrNull(key, configName);
	}
	
	/**
	 * metoda przeladowuje available w systemie
	 */
	public static void reload()
	{
		NewAvailabilityManager.instance = null;		
	}
	
}
