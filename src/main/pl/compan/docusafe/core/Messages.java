package pl.compan.docusafe.core;

/**
* @author Micha� Sankowski <michal.sankowski@docusafe.pl>
*/
public interface Messages {
    void addActionError(String s);
    void addActionMessage(String s);
}
