package pl.compan.docusafe.core;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * Klasa u�atwiaj�ca planowanie dekretacja (taki ParamObject dla Workflowactory."simple"Assignment)
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class AssignmentBuilder {

    private final String activityId;
    private final String guid;
    private final String username;

    private ActionEvent actionEvent = null;
    private String objective = null;
    private String usernameFrom = null;

    private final Function<AssignmentBuilder, Boolean> assigner;

    /**
     * Tworzy obiekt assignment builder - wymaga podania u�ytkownika lub dzia� na kt�ry dekretujemy
     * @param username
     * @param guid
     * @param assigner
     * @throws EdmException
     */
    AssignmentBuilder(String activityId, String username, String guid, Function<AssignmentBuilder, Boolean> assigner) throws EdmException {
        Preconditions.checkNotNull(activityId, "activityId cannot be null");
        Preconditions.checkNotNull(assigner, "assigner cannot be null");
        Preconditions.checkArgument(username != null || guid != null, "username and guid cannot be both null");
        this.username = username;
        if(guid == null){
            this.guid = DSUser.findByUsername(username).getDivisionsGuid()[0];
        } else {
            this.guid = guid;
        }
        this.assigner = assigner;
        this.activityId = activityId;
    }

    /**
     * Wykonuje dekretacje;
     */
    public void assign(){
//        WorkflowFactory.simpleAssignment();
        assigner.apply(this);
    }

    /**
     * ActionEvent do dodawania ewentualnych komunikat�w
     * @param actionEvent
     * @return
     */
    public AssignmentBuilder event(ActionEvent actionEvent){
        this.actionEvent = actionEvent;
        return this;
    }

    /**
     * Cel dekretacji - mo�e by� dowolny
     * @param objective
     * @return
     */
    public AssignmentBuilder objective(String objective){
        this.objective = objective;
        return this;
    }

    /**
     * U�ytkownik, kt�ry dekretuje pismo - domy�lnie ten co jest zalogowany
     * @param username
     * @return
     */
    public AssignmentBuilder fromUser(String username){
        this.usernameFrom = username;
        return this;
    }
}
