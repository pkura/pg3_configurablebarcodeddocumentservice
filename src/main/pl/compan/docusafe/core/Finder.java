package pl.compan.docusafe.core;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Finder.java,v 1.9 2009/01/14 10:14:54 pecet1 Exp $
 */
public class Finder
{
    /**
     * Zwraca wszystkie obiekty przekazanej klasy sortuj�c je opcjonalnie
     * po atrybucie o przekazanej nazwie.
     * @param clazz Klasa, do kt�rej maj� nale�e� zwracane obiekty
     * @param orderByProperty W�asno�� obiektu, po kt�rej maj� by� posortowane
     *   zwracane obiekty; mo�e by� r�wna null.
     */
    public static <T> List<T> list(Class<T> clazz, String orderByProperty) throws EdmException
    {
        try
        {
            List<T> list = (List<T>) DSApi.context().classicSession().find(
                "select d from d in class "+clazz.getName()+
                (orderByProperty != null ? " order by d."+orderByProperty : ""));
            return list;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca obiekt danej klasy o przekazanym id.
     * @param clazz
     * @param id
     * @return
     * @throws EntityNotFoundException je�eli obiekt o tym id nie zostanie znaleziony.
     */
    public static <T> T find(Class<T> clazz, Serializable id) throws EdmException
    {
		T object = null;
		try{
			object = (T) DSApi.getObject(clazz, id);
		} catch (EdmException e) {
			throw new EdmException(e.getMessage() + " (" + clazz.getName() + ":" + id + ")", e);
		}

        if (object == null){
            throw new EntityNotFoundException(clazz, id);
		}

        return object;
    }
}
