package pl.compan.docusafe.core;
/* User: Administrator, Date: 2005-10-27 15:31:44 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ValidationException.java,v 1.1 2005/10/28 14:58:55 lk Exp $
 */
public class ValidationException extends EdmException
{
    public ValidationException(String message)
    {
        super(message);
    }
}
