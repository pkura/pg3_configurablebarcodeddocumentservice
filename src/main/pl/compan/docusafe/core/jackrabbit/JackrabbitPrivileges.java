package pl.compan.docusafe.core.jackrabbit;

import com.google.common.annotations.Beta;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentPermission;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.base.permission.rule.OnTasklistRule;
import pl.compan.docusafe.core.jackrabbit.entities.DocumentAclEntity;
import pl.compan.docusafe.core.jackrabbit.entities.DocumentPrivilegeEntity;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.admin.doctype.DoctypesAction;

import javax.ws.rs.client.WebTarget;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

/**
 * Klient REST do obs�ugi uprawnie� u�ytkownik�w do dokument�w w Jackrabbit.
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class JackrabbitPrivileges extends JackrabbitClient {

    private static JackrabbitPrivileges instance;
    private final static Logger log = LoggerFactory.getLogger(JackrabbitPrivileges.class);

    public JackrabbitPrivileges(JackrabbitServer server) {
        super(server);
    }

    public static JackrabbitPrivileges instance() {
        if(instance == null) {
            instance = new JackrabbitPrivileges(JackrabbitRepository.instance().getNextServer());
        }
        return instance;
    }

    public void propagate(Long documentId) throws Exception {

        if(! AvailabilityManager.isAvailable("jackrabbit.permissions")) {
            log.warn("[propagate] jackrabbit.permissions not available, stopping");
            return;
        }


        Document document = Document.find(documentId, false);

        PrivilegesProvider provider = new PrivilegesProvider(document);

        List<String> users = provider.getReadUsers();
        List<String> groups = provider.getReadGroups();

        resetPermissions(documentId, users, groups);
    }


    private void resetPermissions(Long documentId, List<String> users, List<String> groups) throws Exception {
        // remove duplicates
        users = new ArrayList<String>(new HashSet<String>(users));

        // remove duplicates
        groups = new ArrayList<String>(new HashSet<String>(groups));

        log.debug("[resetPermissions] documentId = {}, users = {}, groups = {}", documentId, users, groups);

        if(AvailabilityManager.isAvailable("jackrabbit.users.externalName")) {
            log.debug("[resetPermissions] converting users to externalNames");
            users = getExternalNames(users);
            log.debug("[resetPermissions] documentId = {}, users -> externalNames: {}", documentId, users);
        }

        List principals = ListUtils.union(users, groups);

        log.debug("[resetPermissions] documentId = {}, principals = {}", documentId, principals);

        WebTarget target = getWebTarget("documents/"+documentId+"/acl");

        OfficeDocument document = OfficeDocument.findOfficeDocument(documentId, false);

        DocumentAclEntity aclEntity = new DocumentAclEntity();
        aclEntity.principalNames = principals;
        aclEntity.action = "add";

        delete(target);
        put(target, aclEntity);
    }

    private List<String> getExternalNames(List<String> usernames) throws EdmException {
        List<DSUser> users = DSUser.findByUsernames(usernames);

        if(users == null) {
            log.error("[getExternalNames] cannot get external names for usernames = {}", usernames);
            throw new EdmException("Nie mo�na pobra� zewn�trznych login�w u�ytkownik�w");
        }

        return FluentIterable.from(users).transform(new Function<DSUser, String>() {
            @Override
            public String apply(DSUser user) {
                return user.getExternalName();
            }
        }).filter(Predicates.notNull()).toList();
    }

    public void addReadPrivilege(Object id, Object assignee, Object o, Object sourceTasklist) {

    }

    public void revokeReadPrivilege(Object o, Object name, Object name1, Object sourcePermission) {

    }
}
