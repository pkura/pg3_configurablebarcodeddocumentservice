package pl.compan.docusafe.core.jackrabbit;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;

/**
 * Zapisane dane dotycz�ce po��czenia z repozytorium jackrabbit
 *
 * Zak�adam, �e nie b�dzie ich zbyt wiele, wszystkie s� cacheowane w JackrabbitRepository
 *
 * @author msankowski
 */
@Entity @Table(name = "ds_jcr_repository")
public class JackrabbitServer {
    private final static Logger LOG = LoggerFactory.getLogger(JackrabbitServer.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ds_jackrabbit_server_seq")
    @SequenceGenerator(name = "ds_jackrabbit_server_seq", sequenceName = "ds_jackrabbit_server_seq")
    private int id;

    public enum RepositoryStatus {
        /** Repozytorium jest aktywne i przyjmuje nowe za��czniki */
        ACTIVE("Aktywny"),
        /** Repozytorium jest aktywne ale umo�liwia tylko odczyt zapisanych za��cznik�w - nowe
         *  wersje s� zapisywane w tych ze statusem ACTIVE */
        ACTIVE_READ_ONLY("Tylko do odczytu"),
        /**
         * Repozytorium nie dzia�a - nie powinny istnie� za��czniki powi�zane z tym repozytorium,
         * pr�ba pobrania takiego za��cznika sko�czy si� b��dem (DocuSafe nie b�dzie pr�bowa� po��czy� si� z takim
         * repozytorium)
         **/
        DISABLED("Nieaktywny");

        private final String description;

        private RepositoryStatus(String description) {
            this.description = description;
        }

        public String getDescription(){
            return description;
        }
    }

    /**
     * Informacja czy dane repozytorium ma by� wykorzystywane
     */
    @Column(name ="status", nullable=false)
    @Enumerated(EnumType.STRING)
    private RepositoryStatus status;

    /**
     * Pocz�tek url wykorzystywany w komunikacji DocuSafe -> Jackrabbit
     * NIE ko�czy si� '/'
     */
    @Column(name = "internal_url", nullable = false)
    private String internalUrlPrefix;

    /**
     * Pocz�tek urla wykorzystywany w generowaniu link�w do komunikacji u�ytkownik -> Jackrabbit
     * NIE ko�czy si� '/'
     */
    @Column(name="user_url", nullable = false)
    private String userUrlPrefix;

    /**
     * Login wykorzystywany do autoryzacji podczas komunikacji DocuSafe -> Jackrabbit, null oznacza po��czenie bez autoryzacji
     */
    @Column(name="login", nullable = true)
    private String login;

    /**
     * Has�o wykorzystywane do autoryzacji podczas komunikacji DocuSafe -> Jackrabbit, warto�� ignorowana je�li login == null
     */
    @Column(name="password", nullable = true)
    private String password;

    /**
     * True oznacza, �e wszystkie b��dy zwi�zane z nieprawid�owymi certyfikatami ssl b�d� ignorowane dla tego repozytorium,
     * przydatne na systemach testowych i do tymczasowego ;) rozwi�zywania problem�w z certyfikatami
     */
    @Column(name="ignore_ssl_errors", nullable = false)
    private boolean ignoreSslErrors;

    public void testConnection() throws EdmException {
        JackrabbitRepository.instance().testConnection(this);
    }

    /**
     * <p>
     *     Pobiera relatywn� do jackrabbita �cie�k� do webapi z adds�w <code>jackrabbit.webApiPath</code>
     *     (domy�lnie <code>/webapi</code>). Przyk�adowo je�li jackrabbit znajduje si� na <code>http://localhost:8080/jcr</code>
     *     i <code>jackrabbit.webApiPath=/webapi</code> to metoda zwr�ci <code>http://localhost:8080/jcr/webapi</code>
     * </p>
     * @return
     */
    public String getWebApiUrl() {
        String webApiPath = AdditionManager.getPropertyOrDefault("jackrabbit.webApiPath", "/webapi");
        return getInternalUrlPrefix() + webApiPath;
    }

    public int getId() {
        return id;
    }

    public RepositoryStatus getStatus() {
        return status;
    }

    public String getInternalUrlPrefix() {
        return internalUrlPrefix;
    }

    public String getUserUrlPrefix() {
        return userUrlPrefix;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public boolean isIgnoreSslErrors() {
        return ignoreSslErrors;
    }

    public void setStatus(RepositoryStatus status) {
        this.status = status;
    }

    /**
     * Ustawia internalUrlPrefix usuwaj�c '/' na ko�cu
     * @param internalUrlPrefix
     */
    public void setInternalUrlPrefix(String internalUrlPrefix) {
        if(internalUrlPrefix.endsWith("/")){
            internalUrlPrefix = internalUrlPrefix.replaceAll("/*$","");
        }
        this.internalUrlPrefix = internalUrlPrefix;
    }

    /**
     * Ustawia userUrlPrefix usuwaj�c '/' na ko�cu
     * @param userUrlPrefix
     */
    public void setUserUrlPrefix(String userUrlPrefix) {
        if(userUrlPrefix.endsWith("/")){
            userUrlPrefix = userUrlPrefix.replaceAll("/*$","");
        }
        this.userUrlPrefix = userUrlPrefix;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIgnoreSslErrors(boolean ignoreSslErrors) {
        this.ignoreSslErrors = ignoreSslErrors;
    }
}
