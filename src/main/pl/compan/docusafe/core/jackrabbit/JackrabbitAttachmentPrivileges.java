package pl.compan.docusafe.core.jackrabbit;


import pl.compan.docusafe.core.jackrabbit.entities.AttachmentPrivilegeEntity;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.ws.rs.client.WebTarget;

@Deprecated
public class JackrabbitAttachmentPrivileges extends JackrabbitClient {

    private static JackrabbitAttachmentPrivileges instance;
    private final static Logger log = LoggerFactory.getLogger(JackrabbitAttachmentPrivileges.class);

    public JackrabbitAttachmentPrivileges(JackrabbitServer server) {
        super(server);
    }

    public static JackrabbitAttachmentPrivileges instance() {
        if(instance == null) {
            instance = new JackrabbitAttachmentPrivileges(JackrabbitRepository.instance().getNextServer());
        }
        return instance;
    }

    /**
     * <p>
     *     Nadaje (<code>add == true</code>) lub usuwa (<code>add == false</code>)
     *     uprawnienia odczytu w jackrabbicie na serwerze <code>server</code>
     *     dla za��cznika o id <code>attachmentRevisionId</code>.
     * </p>
     * @param attachmentRevisionId
     * @param username
     * @param add
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     * @throws Exception
     */
    public void changeReadPrivilege(Long attachmentRevisionId, String username, boolean add) throws Exception {
        WebTarget target = getWebTarget("privileges/attachments/"+attachmentRevisionId.toString());

        AttachmentPrivilegeEntity ap = new AttachmentPrivilegeEntity();
        ap.username = username;
        ap.revisionId = attachmentRevisionId.toString();
        ap.action = add ? "add" : "revoke";

        put(target, ap);
    }

    public void clearAllPrivileges(Long attachmentRevisionId) throws Exception {
        WebTarget target = getWebTarget("privileges/attachments/"+attachmentRevisionId.toString());
        delete(target);
    }

    public void revokeReadPrivilege(Long attachmentRevisionId, String username) throws Exception {
        changeReadPrivilege(attachmentRevisionId, username, false);
    }

    public void addReadPrivilege(Long attachmentRevisionId, String username) throws Exception {
        changeReadPrivilege(attachmentRevisionId, username, true);
    }

}
