package pl.compan.docusafe.core.jackrabbit;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentPermission;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public class PrivilegesProvider {

    private final static Logger log = LoggerFactory.getLogger(PrivilegesProvider.class);
    private final Document document;

    public PrivilegesProvider(Document document) {
        this.document = document;
    }

    public List<String> getReadUsers() throws EdmException {

        // ds_document_permission
        List<String> users = new ArrayList<String>(getReadSubjectsFromDocumentPermissions(document.getId(), ObjectPermission.USER));

        // referent
        if(document instanceof OfficeDocument) {
            String clerk = ((OfficeDocument) document).getClerk();

            if(clerk != null) {
                users.add(clerk);
                log.debug("[getReadUsers] add clerk, doc.id = {}, clerk = {}", document.getId(), clerk);
            }
        }

        // lista zada�
        users.addAll(getUsersFromTasklist());

        // lista obserwowanych
        users.addAll(getUsersFromWatchlist());

        log.debug("[getReadUsers] return, doc.id = {}, users = {}", document.getId(), users);

        return users;
    }

    public List<String> getUsersFromTasklist() throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(JBPMTaskSnapshot.class);
        criteria.add(Restrictions.eq("documentId", document.getId()));

        List<JBPMTaskSnapshot> list = criteria.list();

        ImmutableList<String> ret = FluentIterable.from(list).transform(new Function<JBPMTaskSnapshot, String>() {
            @Override
            public String apply(JBPMTaskSnapshot jbpmTaskSnapshot) {
                return jbpmTaskSnapshot.getDsw_resource_res_key();
            }
        }).toList();

        log.debug("[getUsersFromTasklist] doc.id = {}, users = {}", document.getId(), ret);

        return ret;
    }

    private List<String> getUsersFromWatchlist() {
        try {
            List<String> watches = DSApi.context().watches(document);

            log.debug("[getUsersFromWatchList] doc.id = {}, watches = {}", document.getId(), watches);

            return watches;
        } catch (EdmException e) {
            log.error("[getUsersFromWatchlist] cannot get watches from document, doc.id = {}", document.getId());
            return new ArrayList<String>();
        }
    }

    public List<String> getReadGroups() throws EdmException {

        List<String> groups = new ArrayList<String>(getReadSubjectsFromDocumentPermissions(document.getId(), ObjectPermission.GROUP));

        if(document instanceof OfficeDocument) {
            String assignedDivision = ((OfficeDocument) document).getAssignedDivision();

            if(StringUtils.isNotBlank(assignedDivision)) {
                assignedDivision = "READ_DEP_DOCS_"+assignedDivision;
                groups.add(assignedDivision);

                log.debug("[getReadGroups] doc.id = {}, add assignedDivision = {}", document.getId(), assignedDivision);
            }
        }

        log.debug("[getReadGroups] return, doc.id = {}, groups = {}", document.getId(), groups);

        return groups;
    }

    private Collection<String> getReadSubjectsFromDocumentPermissions(Long documentId, String type) throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(DocumentPermission.class);
        criteria.add(Restrictions.eq("documentId", documentId));
        criteria.add(Restrictions.eq("name", ObjectPermission.READ));
        criteria.add(Restrictions.eq("subjectType", type));

        List<DocumentPermission> list = criteria.list();

        Collection<String> ret = Collections2.transform(list, new Function<DocumentPermission, String>() {
            @Override
            public String apply(DocumentPermission documentPermission) {
                return documentPermission.getSubject();
            }
        });

        log.debug("[getReadSubjectsFromDocumentPermissions] doc.id = {}, type = {}, ret = {}", documentId, type, ret);

        return ret;
    }

    private Set<String> selectUsernamesForDocument(Long document_id) throws Exception {
        Set<String> usernames = new HashSet<String>();
        String sql = "select distinct(username) from dsj_jcrpermissions where document_id = ? or document_id is null";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement(sql);
            ps.setLong(1,document_id);
            rs = ps.executeQuery();
            while(rs.next()) {
                usernames.add(rs.getString(1));
            }
        } catch (Exception e) { log.debug(e.getMessage(),e); }
        finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(ps);
        }
        return usernames;
    }


}
