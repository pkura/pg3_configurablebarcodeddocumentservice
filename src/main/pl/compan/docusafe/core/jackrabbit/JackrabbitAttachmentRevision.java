package pl.compan.docusafe.core.jackrabbit;

import com.asprise.util.tiff.TIFFReader;
import com.google.common.io.Files;
import com.lowagie.tools.plugins.Watermarker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.ImageUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ShUtils;

import javax.imageio.ImageIO;
import javax.mail.MessagingException;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.nio.charset.Charset;

/**
 * Za��cznik przechowywany w oddzielnym repozytorium Jackrabbit
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */

public class JackrabbitAttachmentRevision extends AttachmentRevision {
    private final static Logger LOG = LoggerFactory.getLogger(JackrabbitAttachmentRevision.class);

    /**
     * Id repozytorium jackrabbita (encja JackrabbitServer)
     */
    private int jackrabbitServerId;

    /** dla Hibernate */
    protected JackrabbitAttachmentRevision() {
    }

    /**
     * Tworzy now� instancje JackrabbitAttachmentRevision
     * metoda wydzielona obok konstruktowa, tak by nie generowa� niepotrzebnie jackrabbitServerId
     * @return nowa instancja JackrabbitAttachmentRevision z uzupe�nionym jackrabbitServerId
     * @throws RuntimeException je�li nie istnieje skonfigurowany, aktywny JackrabbitServer
     */
    public static JackrabbitAttachmentRevision create() throws RuntimeException{
        JackrabbitAttachmentRevision ret = new JackrabbitAttachmentRevision();
        ret.jackrabbitServerId = JackrabbitRepository.instance().getNextServerId();
        return ret;
    }

    File getFileByPage(int page) throws EdmException {
        return getFileByPage(page, AttachmentRevision.PNG, false);
    }

    File getFileByPage(int page, String format, Boolean fax) throws EdmException {
        try {
            File file = getFile();
            if (mime.startsWith("image/tiff")) {
                String path = getPathToDecompressedImage(id, format, fax, page);

                File imageFile = new File(path);
                if(!imageFile.isFile()){
                    //log.info("tworzenie pliku " + imageFile.getAbsolutePath());
                    imageFile.createNewFile();
                }

                TIFFReader reader = new TIFFReader(file);
                RenderedImage im = reader.getPage(page);
                ImageIO.write(im, format, imageFile);
                return imageFile;
            } else {
                return file;
            }
        } catch (IOException e) {
            throw new EdmException(e);
        } catch (MessagingException e) {
            throw new EdmException(e);
        }
    }

    public InputStream getUnsafeStream() throws EdmException {
        return JackrabbitRepository.instance().downloadFile(this);
    }

    @Override
    public final InputStream getBinaryStream() throws EdmException {
        return getAttachmentStream();
    }

    @Override
    public String getAnnotations() throws DocumentNotFoundException, AccessDeniedException, EdmException, IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateAnnotations(String source) throws EdmException, IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateBinaryStreamRevisionSpecific(InputStream stream, int fileSize) throws EdmException {
        File temp = null;
        OutputStream os = null;
        try {
            // Tworzymy tymczasowy plik i kopiujemy do niego
            // zawarto�� ze strumienia stream.
            temp = Files.createTempDir();
            File file = new File(temp, getOriginalFilename());
            if(!file.createNewFile()){
                throw new IOException("wyst�pi� problem podczas tworzenia pliku " + file.getAbsolutePath());
            }
            os = new FileOutputStream(file);
            IOUtils.copy(stream, os);
            os.flush();
            JackrabbitRepository.instance().uploadFile(this, file);
//            JackrabbitRepository.instance().storeAdditionalData(this.jackrabbitServerId, this.id.toString(), "TEST".getBytes(Charset.forName("UTF-8")));
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(os);
            FileUtils.deleteQuietly(temp);
        }
    }

    /**
     * Zwraca tymczasowy link do pobrania za��cznika - linku nie mo�na udost�pni� na stronie do klikni�cia, gdy�
     * jest on wa�ny ograniczony czas, na stronie powinien by� dost�pny link do ViewAttachmentRevisionAction (view-attachment-revision.do),
     * kt�ry korzysta z tej metody
     * U�ytkownicy DocuSafe powinni pobiera� za��czniki korzystaj�c z tego linku
     * @return tymczasowy link do pobrania za��cznika
     */
    public String getDownloadLink() throws EdmException {
        return JackrabbitRepository.instance().getDownloadLink(this);
    }

    public String getPageLink(int page) throws EdmException {
        return JackrabbitRepository.instance().getPageLink(this, page);
    }

    public long saveToFile(File file) throws EdmException, IOException {
        if (file.exists() && !file.canWrite()) {
            throw new EdmException("Brak uprawnie� do zapisu pliku " + file);
        }
        if(!file.getParentFile().exists()) {
            (new File(file.getParent())).mkdirs();
        }
        int total;
        OutputStream os = null;
        InputStream is = null;
        try {
            is = getUnsafeStream();
            os  = new BufferedOutputStream(new FileOutputStream(file));
            total = IOUtils.copy(is, os);
        } finally {
            org.apache.commons.io.IOUtils.closeQuietly(is);
            org.apache.commons.io.IOUtils.closeQuietly(os);
        }
        return total;
    }

    public File saveToTempFile() throws EdmException, IOException {
        String suffix;
        if (mime == null || mime.indexOf('/') < 0) {
            suffix = ".tmp";
        } else if (mime.indexOf("image/tiff") >= 0) {
            suffix = ".tif";
        } else if (mime.indexOf("image/jpeg") >= 0) {
            suffix = ".jpg";
        } else if (mime.indexOf("application/pdf") >= 0) {
            suffix = ".pdf";
        } else {
            String ms = mime.substring(mime.indexOf('/') + 1);
            if (ms.indexOf(';') > 0) {
                ms = ms.substring(0, ms.indexOf(';'));
            }
            if (ms.length() == 0) {
                ms = "tmp";
            }
            suffix = "." + ms;
        }
        File file = File.createTempFile("docusafe_pdf", suffix);
        saveToFile(file);
        return file;
    }

    public InputStream getStreamByPage(int page, String format, Boolean fax) throws EdmException {
        throw new UnsupportedOperationException("getStreamByPage is not supported");
    }

    @Override
    public InputStream getAttachmentStream() throws EdmException {
        try
        {
            File file;
            file = saveToTempFile();
            file.deleteOnExit();
            return new FileInputStream(file);
        }
        catch (IOException e)
        {
            throw new EdmException(e);
        }
    }

    /**
     * Zwraca file do zapisu tymczasowego na dysku
     *
     * @return the file
     * @throws EdmException the edm exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws MessagingException the messaging exception
     */
    private File getFile() throws EdmException, IOException, MessagingException {
        File file = new File(Attachment.getPath(getId()));
        if (!file.exists() || !file.isFile() || file.length() != getSize().intValue()) {
            ShUtils.delete(file);
            saveToFile(file);
        }
        return file;
    }

    public Integer getPageCount() throws EdmException {
        try {
            if (mime.contains("tif")) {
                File file = null;
                try {
                    file = saveToTempFile();
                    TIFFReader reader = new TIFFReader(file);
                    return reader.countPages();
                } catch (IllegalArgumentException e) {
                    return 1;
                } finally {
                    boolean deleted = FileUtils.deleteQuietly(file);
                    if(!deleted && file != null){
                        LOG.warn("file {} was not deleted, marking for delete", file);
                        file.deleteOnExit();
                    }
                }
            } else return 1;
        } catch (IOException e) {
            throw new EdmException(e);
        }
    }

    public RenderedImage getPage(int page) throws EdmException {
        try {
            TIFFReader reader = new TIFFReader(getFile());
            int pageCount = reader.countPages();
            if (page >= pageCount) {
                page = pageCount - 1;
            }
            return reader.getPage(page);
        } catch (IOException e) {
            throw new EdmException(e);
        } catch (MessagingException e) {
            throw new EdmException(e);
        }
    }

    @Override
    public String toString() {
        return "JackrabbitAttachmentRevision [id=" + id + ", revision=" + revision + "]";
    }

    public void updateFileName(String filename) throws EdmException {
        JackrabbitRepository.instance().updateFileName(this, filename);
    }

    public int getJackrabbitServerId() {
        return jackrabbitServerId;
    }
}