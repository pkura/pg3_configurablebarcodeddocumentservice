package pl.compan.docusafe.core.jackrabbit;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.XmlGenerator;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.*;

/**
 * <p>
 *     Obs�uga wysy�ania metadanych dokumentu od jackrabbit.
 * </p>
 */
public class JackrabbitMetadata {

    private final static Logger log = LoggerFactory.getLogger(JackrabbitMetadata.class);

    private Long documentId;
    private byte[] xml;
    private String sha1;
    private OfficeDocument document;

    public JackrabbitMetadata(Long documentId) throws EdmException {
        this.documentId = documentId;
    }

    public void init() throws EdmException, IOException, ParserConfigurationException, TransformerException {
        if(! AvailabilityManager.isAvailable("jackrabbit.metadata")) {
            throw new EdmException("[init] `jackrabbit.metadata` is not available, documentId = "+documentId);
        }

        if(! AvailabilityManager.isAvailable("jackrabbit.repositories")) {
            throw new EdmException("[init] `jackrabbit.repositories` is not available, documentId = "+documentId);
        }

        document = OfficeDocument.find(documentId);
        XmlGenerator xmlGenerator = new XmlGenerator(document).generateXml();

        xml = xmlGenerator.getXml();
        sha1 = xmlGenerator.getSha1();
    }

    /**
     * <p>
     *     Przesy�a dokument o id {@code documentId} w postaci xml
     *     do Jackrabbita. Metoda zwraca SHA1 z wygenerowanego pliku xml.
     * </p>
     *
     * @return
     * @throws EdmException
     * @throws IOException
     * @throws TransformerException
     * @throws ParserConfigurationException
     */
    public void send() throws EdmException, IOException, TransformerException, ParserConfigurationException {
        log.debug("[send] documentId = {}", documentId);

        String path = getJcrPathFromDocument(document)+"/document";

        log.debug("[send] path = {}", path);

        JackrabbitRepository jackrabbitRepository = JackrabbitRepository.instance();
        jackrabbitRepository.storeAdditionalData(JackrabbitRepository.instance().getNextServerId(),
                documentId.toString(), path, xml);
    }

//    public String generateXml(Long documentId) throws EdmException, ParserConfigurationException, TransformerException, IOException {

//    private void appendAdditionalData(Element rootElement, Document xmlDoc, OfficeDocument document) throws EdmException {

    public static String getJcrPathFromDocument(OfficeDocument document) {
        Date ctime = document.getCtime();

        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(ctime);

        return "documents/" + cal.get(Calendar.YEAR)+"/"+calendarGet(cal, Calendar.MONTH)+"/"+calendarGet(cal, Calendar.DAY_OF_MONTH)+"/"+calendarGet(cal, Calendar.HOUR_OF_DAY)+"/"+document.getId().toString();
    }

    private static String calendarGet(Calendar cal, int type) {
        int v = cal.get(type);

        if(type == Calendar.MONTH) {
            v++;
        }

        String value = String.valueOf(v);
        return StringUtils.leftPad(value, 2, '0');
    }

    public String getSha1() {
        return sha1;
    }

    public void setSha1(String sha1) {
        this.sha1 = sha1;
    }

}