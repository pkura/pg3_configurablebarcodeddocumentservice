package pl.compan.docusafe.core.jackrabbit;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.jackrabbit.JackrabbitClient;
import pl.compan.docusafe.core.jackrabbit.JackrabbitRepository;
import pl.compan.docusafe.core.jackrabbit.JackrabbitServer;
import pl.compan.docusafe.core.jackrabbit.entities.UserEntity;
import pl.compan.docusafe.core.users.DSUser;

import javax.ws.rs.client.WebTarget;

/**
 * <p>
 *     Klient REST do obs�ugi u�ytkownik�w w Jackrabbit.
 * </p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class JackrabbitUsers extends JackrabbitClient {

    private static JackrabbitUsers instance;

    public JackrabbitUsers(JackrabbitServer server) {
        super(server);
    }

    public static JackrabbitUsers instance() {
        if(instance == null) {
            instance = new JackrabbitUsers(JackrabbitRepository.instance().getNextServer());
        }
        return instance;
    }

    public void createUser(String username, String password) throws Exception {
        UserEntity user = new UserEntity();
        user.username = username;
        user.password = password;

        WebTarget target = getWebTarget("users/"+username);

        post(target, user);
    }

    public void createUser(DSUser user, String password) throws Exception {
        String username;

        if(AvailabilityManager.isAvailable("jackrabbit.users.externalName")) {
            username = user.getExternalName();
        } else {
            username = user.getName();
        }

        createUser(username, password);
    }

    public void deleteUser(String username) throws Exception {
        WebTarget target = getWebTarget("users/"+username);
        delete(target);
    }

}
