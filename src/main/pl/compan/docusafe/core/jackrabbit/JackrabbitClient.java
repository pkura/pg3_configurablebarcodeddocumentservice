package pl.compan.docusafe.core.jackrabbit;


import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * <p>
 *     Klient REST (JAX-RS) do repozytorium Jackrabbit.
 *     UWAGA: JCR 2.0 nie definiuje RESTa - REST po stronie Jackrabbita jest napisany przez nas.
 * </p>
 */
public class JackrabbitClient {

    private static final Logger log = LoggerFactory.getLogger(JackrabbitClient.class);

    protected final JackrabbitServer server;
    private static JackrabbitClient instance;

    public static JackrabbitClient instance() {
        if(instance == null) {
            instance = new JackrabbitClient(JackrabbitRepository.instance().getNextServer());
        }
        return instance;
    }

    public JackrabbitClient(JackrabbitServer server) {
        this.server = server;
    }

    /**
     * <p>
     *     Zwraca obiekt {@link WebTarget} tworzony na podstawie danych
     *     z <code>server</code> (url, http authentication login+password)
     *     oraz relatywnej �ciezki <code>path</code>.
     * </p>
     * @param path
     * @return
     */
    protected WebTarget getWebTarget(String path) {
        String webApiUrl = server.getWebApiUrl();

        log.info("[getWebTarget] webApiUrl = {}", webApiUrl);

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(server.getLogin(), server.getPassword());

        Client client = ClientBuilder.newClient();
        client.register(feature);
        WebTarget target = client.target(webApiUrl).path(path);

        return target;
    }

    protected void checkResponse(Response res, WebTarget target) throws Exception {
        int status = res.getStatus();

        log.debug("[checkResponse] status = {}", status);

        if(status != 200) {
            log.debug("[checkResponse] response = {}", res.getEntity().toString());
            throw new Exception("Cannot execute action for webapi. HTTP Status = "+status+", target.uri = "+target.getUri());
        }
    }

    /**
     * <p>
     *     Wywo�uje REST PUT obiektu <code>obj</code> w kontek�cie <code>target</code>.
     * </p>
     * @param target
     * @param entity
     * @return HTTP Response
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     * @throws Exception
     */
    protected Response put(WebTarget target, Object entity) throws Exception {
        log.trace("[put] target = {}, entity = {}", target.getUri(), entity.toString());

        Invocation.Builder invocationBuilder = target.request(MediaType.TEXT_PLAIN_TYPE);

        Entity xmlEntity = Entity.xml(entity);
        final Response res = invocationBuilder.put(xmlEntity);

        checkResponse(res, target);

        return res;
    }

    /**
     * <p>
     *     Wywo�uje REST DELETE w <code>target</code>.
     * </p>
     * @param target
     * @return
     * @throws Exception
     */
    protected Response delete(WebTarget target) throws Exception {
        Invocation.Builder invocationBuilder = target.request(MediaType.TEXT_PLAIN_TYPE);

        final Response res = invocationBuilder.delete();

        checkResponse(res, target);

        return res;
    }


    /**
     * <p>
     *     Wywo�uje REST GET w <code>target</code>.
     * </p>
     * @param target
     * @return
     * @throws Exception
     */
    protected Response get(WebTarget target) throws Exception {
        return get(target, MediaType.TEXT_PLAIN_TYPE);
    }

    /**
     * <p>
     *     Wywo�uje REST GET w <code>target</code>.
     * </p>
     * @param target
     * @return
     * @throws Exception
     */
    protected Response get(WebTarget target, MediaType type) throws Exception {
        Invocation.Builder invocationBuilder = target.request(type);

        final Response res = invocationBuilder.get();

        checkResponse(res, target);

        return res;
    }

    /**
     * <p>
     *     Wywo�uje REST POST obiektu <code>obj</code> w kontek�cie <code>target</code>.
     * </p>
     * @param target
     * @param obj
     * @return
     * @throws Exception
     */
    protected Response post(WebTarget target, Object obj) throws Exception {
        Invocation.Builder invocationBuilder = target.request(MediaType.TEXT_PLAIN_TYPE);

        Entity entity = Entity.xml(obj);
        final Response res = invocationBuilder.post(entity);

        checkResponse(res, target);

        return res;
    }

}
