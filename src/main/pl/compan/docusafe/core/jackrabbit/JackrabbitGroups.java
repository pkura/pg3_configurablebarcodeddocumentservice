package pl.compan.docusafe.core.jackrabbit;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.jackrabbit.entities.GroupEntity;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.annotation.Nullable;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *     Klient REST do obs�ugi grup w Jackrabbit.
 * </p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */

public class JackrabbitGroups extends JackrabbitClient {

    private static JackrabbitGroups instance;

    private static final Logger log = LoggerFactory.getLogger(JackrabbitGroups.class);

    public JackrabbitGroups(JackrabbitServer server) {
        super(server);
    }

    public static JackrabbitGroups instance() {
        if(instance == null) {
            instance = new JackrabbitGroups(JackrabbitRepository.instance().getNextServer());
        }
        return instance;
    }

    public void createOrUpdate(String guid, List<String> users) throws Exception {
        GroupEntity group = new GroupEntity();
        group.guid = guid;
        group.users = users;

        WebTarget target = getWebTarget("groups/"+guid);
        put(target, group);
    }

    /**
     * <p>
     *     Create or update group.
     * </p>
     * @param division
     * @throws Exception
     */
    public void createOrUpdate(DSDivision division) throws Exception {
        GroupEntity group = new GroupEntity();
        group.guid = division.getGuid();
        group.users = usersToNames(Arrays.asList(division.getOriginalUsers()));
        group.action = "createOrUpdate";

        put(group);
    }

    /**
     * <p>
     *     Add users to group.
     * </p>
     * @param guid
     * @param users
     * @throws Exception
     */
    public void addUsers(String guid, List<DSUser> users) throws Exception {
        GroupEntity group = new GroupEntity();
        group.guid = guid;
        group.users = usersToNames(users);
        group.action = "addUsers";

        put(group);
    }

    /**
     * <p>
     *     Delete users from group.
     * </p>
     * @param guid
     * @param users
     * @throws Exception
     */
    public void deleteUsers(String guid, List<DSUser> users) throws Exception {
        GroupEntity group = new GroupEntity();
        group.guid = guid;
        group.users = usersToNames(users);
        group.action = "removeUsers";

        put(group);
    }

    public void delete(String guid) throws Exception {
        WebTarget target = getWebTarget("groups/"+guid);
        delete(target);
    }

    private Response put(GroupEntity group) throws Exception {
        WebTarget target = getWebTarget("groups/"+group.guid);
        return put(target, group);
    }

    private List<String> usersToNames(List<DSUser> users) {
        return FluentIterable.from(users).transform(new Function<DSUser, String>() {
            @Override
            public String apply(DSUser dsUser) {
                String username;
                if(AvailabilityManager.isAvailable("jackrabbit.users.externalName")) {
                    username = dsUser.getExternalName();
                } else {
                    username = dsUser.getName();
                }
                if(username == null) {
                    throw new NullPointerException("[usersToNames] username cannot be null, user.id = "+dsUser.getId());
                } else {
                    return username;
                }
            }
        }).toList();
    }
}

