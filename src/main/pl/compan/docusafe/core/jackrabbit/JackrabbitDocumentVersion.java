package pl.compan.docusafe.core.jackrabbit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jackrabbit.entities.DocumentVersionEntity;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.docusafe.utils.JsonUtils;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Klasa s�u�aca do pobierania danych odno�nie wersji dokumentu
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class JackrabbitDocumentVersion extends JackrabbitClient{

    private final static Logger log = LoggerFactory.getLogger(JackrabbitRepository.class);

    private static JackrabbitDocumentVersion instance;

    protected JackrabbitDocumentVersion(JackrabbitServer server) {
        super(server);
    }

    public static JackrabbitDocumentVersion instance(){
        if(instance == null){
            instance = new JackrabbitDocumentVersion(JackrabbitRepository.instance().getNextServer());
        }

        return instance;
    }

    /**
     * zwraca list� wersji dokument�w
     * @param documentId
     * @return
     * @throws Exception
     */
    public List<DocumentVersionEntity> getVersionList(Long documentId) throws Exception {
        WebTarget target = getWebTarget("/documentVersion/" + documentId);

        Response res = target.request()
                .accept(MediaType.APPLICATION_JSON).get();
        String result = res.readEntity(String.class);
        checkResponse(res, target);

        ObjectMapper mapper = new ObjectMapper();
        List<DocumentVersionEntity> versions = JsonUtils.parseToList(mapper.readTree(result),
                                                    DocumentVersionEntity.class);

        return versions;
    }


    /**
     * Zwraca zawarto�c danej wersji
     * @param documentId
     * @param uuid
     * @return
     * @throws Exception
     */
    public DocumentVersionEntity getVersion(Long documentId, String uuid) throws Exception {
        String result = getVersionJson(documentId, uuid);
        return (DocumentVersionEntity) JsonUtils.readValue(result, DocumentVersionEntity.class);
    }

    /**
     * Zwraca zawarto�c danej wersji
     * @param documentId
     * @param uuid
     * @return
     * @throws Exception
     */
    public String getVersionJson(Long documentId, String uuid) throws Exception {
        WebTarget target = getWebTarget("/documentVersion/" + documentId + "/uuid/"+uuid);
        Response res = target.request().accept(MediaType.APPLICATION_JSON).get();

        checkResponse(res, target);

        return res.readEntity(String.class);
    }
}
