package pl.compan.docusafe.core.jackrabbit;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.*;
import com.google.common.io.Files;

import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.conn.BasicClientConnectionManager;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Base64;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import pl.compan.docusafe.util.MimetypesFileTypeMap;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Klasa do komunikacji z repozytorium Jackrabbit
 *
 * Korzysta z us�ug tego repozytorium:
 * - wczytywanie i zapisywanie za��cznik�w
 * - generowanie link�w do za��cznik�w
 *
 * Wygenerowane linki s� udost�pniane na stronie i mog� by� wykorzystywane przez u�ytkownik�w do bezpo�reniego pobierania
 * za�cznik�w, stron etc., linki wa�ne s� przez ograniczony czas
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class JackrabbitRepository {
    private final static Logger log = LoggerFactory.getLogger(JackrabbitRepository.class);
    private final static Logger PERFORMANCE_LOG = LoggerFactory.getLogger("repositoryserver.performance");

    /** Czas wa�no�ci (po stronie repozytorium) wygenerowanego linku (w minutach), domy�lnie 5, w cache'ach odejmowane jest
     * 1 ze wzgl�du na czas przesy�ania linku i wy�wietlania strony u�ytkownikowi, warto�� musi by� aktualizowana z analogiczn� po stronie Repozytorium */
    public static final int VALID_LINK_DURATION = Docusafe.getIntAdditionPropertyOrDefault("jackrabbit.link.duration", 5);
    /** Instancja signletona */
    private final static JackrabbitRepository INSTANCE = new JackrabbitRepository();
    /** Connection manager dla HttpClient */
    private final static ClientConnectionManager connectionManager = getConnectionManager();

//    private final static JackrabbitServer jackrabbitClusterServer = new JackrabbitServer();

    private static PoolingClientConnectionManager getConnectionManager() {
        PoolingClientConnectionManager ret = new PoolingClientConnectionManager();
        ret.setMaxTotal(30);
        ret.setDefaultMaxPerRoute(30);
        return ret;
    }

    /** Cache link�w (fragment�w REPOSITORY_RESOURCE) do ca�ych za��cznik�w */
    private final static Cache<Long, String> attachmentLinks = CacheBuilder.newBuilder()
            .expireAfterWrite(VALID_LINK_DURATION - 1, TimeUnit.MINUTES)
            .build();

    /** Cache link�w (fragment�w REPOSITORY_RESOURCE) do stron za��cznik�w */
    private final static Cache<PageRef, String> pageLinks = CacheBuilder.newBuilder()
            .expireAfterWrite(VALID_LINK_DURATION - 1, TimeUnit.MINUTES)
            .build();
            
    /** Cache wszystkich serwer�w (mapa id->JackrabbitServer, zrzut z tabeli przechowuj�cej JackrabbitServer) */
    private final static Supplier<JackrabbitServers> cachedServers = Suppliers.memoizeWithExpiration(
            new Supplier<JackrabbitServers>(){
        @Override
        public JackrabbitServers get() {
            try {
                return new JackrabbitServers(DSApi.context().session().createCriteria(JackrabbitServer.class).list());
            } catch (EdmException e) {
                throw new RuntimeException(e);
            }
        }
    },5, TimeUnit.MINUTES);

    private final static AtomicInteger NEXT_REPOSIOTORY_INDEX = new AtomicInteger(0);

    private static class JackrabbitServers {
        private final Map<Integer, JackrabbitServer> servers;
        private final List<JackrabbitServer> activeServers;

        private void replaceServersWithClusterServer(Collection<JackrabbitServer> servers) {
            JackrabbitServer cluster = new JackrabbitServer();
            cluster.setInternalUrlPrefix(AdditionManager.getProperty("jackrabbit.cluster.internalUrl"));
            cluster.setIgnoreSslErrors("true".equals(AdditionManager.getProperty("jackrabbit.cluster.ignoreSslErrors")));
            cluster.setLogin(AdditionManager.getProperty("jackrabbit.cluster.login"));
            cluster.setPassword(AdditionManager.getProperty("jackrabbit.cluster.password"));
            cluster.setUserUrlPrefix(AdditionManager.getProperty("jackrabbit.cluster.userUrl"));

            for(JackrabbitServer s: servers) {
                s.setUserUrlPrefix(cluster.getUserUrlPrefix());
                s.setPassword(cluster.getPassword());
                s.setLogin(cluster.getLogin());
                s.setInternalUrlPrefix(cluster.getInternalUrlPrefix());
                s.setIgnoreSslErrors(cluster.isIgnoreSslErrors());
            }
        }

        private JackrabbitServers(Collection<JackrabbitServer> servers) {
            if(AvailabilityManager.isAvailable("jackrabbit.cluster")) {
                replaceServersWithClusterServer(servers);
            }

            this.servers = Maps.uniqueIndex(servers, new Function<JackrabbitServer, Integer>() {
                @Override
                public Integer apply(JackrabbitServer jackrabbitServer) {
                    return jackrabbitServer.getId();
                }
            });
            Collection<JackrabbitServer> list = Lists.newArrayList();
            for(JackrabbitServer server : servers){
                if(server.getStatus() == JackrabbitServer.RepositoryStatus.ACTIVE){
                    list.add(server);
                }
            }
            activeServers = ImmutableList.copyOf(list);
        }

        JackrabbitServer get(int id) {
            return servers.get(id);
        }

        /**
         * Zwraca aktywny serwer (taki do kt�rego mo�na zapisywa� za��czniki)
         * Je�eli jest kilka serwer�w metoda zwraca r�ne serwery tak by ka�dy
         * by� zwracany z (prawie) tak� sam� cz�stotliwo�ci�
         * @return aktywny JackrabbitServer
         */
        JackrabbitServer getActiveServer(){
            if(activeServers.isEmpty()){
                throw new RuntimeException("Brak skonfigurowanego aktywnego serwera Jackrabbit!");
            }
            NEXT_REPOSIOTORY_INDEX.compareAndSet(Integer.MIN_VALUE, 0);
            return activeServers.get(NEXT_REPOSIOTORY_INDEX.getAndIncrement() % activeServers.size());
        }
    }

    /**
     *  Informacje do wyci�gania stron
     */
    private final static class PageRef {
        long revisionId;
        int page;

        private PageRef(long revisionId, int page) {
            this.page = page;
            this.revisionId = revisionId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PageRef pageRef = (PageRef) o;

            if (page != pageRef.page) return false;
            if (revisionId != pageRef.revisionId) return false;

            return true;
        }

        Integer hashCode;
        @Override
        public int hashCode(){
            if(hashCode == null) hashCode = _hashCode();
            return hashCode;
        }

        private int _hashCode() {
            int result = (int) (revisionId ^ (revisionId >>> 32));
            result = 31 * result + page;
            return result;
        }
    }

    public static JackrabbitRepository instance(){
        return INSTANCE;
    }

    /**
     * Zapisuje dodatkowe dane w repozytorium Jackrabbit
     * @param jackrabbitServerId id serwera jackrabbita
     * @param dataId identyfikator danych
     * @param dataPath �cie�ka w JCR
     * @param contents zawarto�� binarna (je�li istnieje dla danego id to zostanie nadpisana)
     */
    public void storeAdditionalData(int jackrabbitServerId, String dataId, String dataPath, byte[] contents) throws EdmException {
        int retries = getRetries();

        storeAdditionalData(jackrabbitServerId, dataId, dataPath, contents, retries);
    }


    public void storeAdditionalData(int jackrabbitServerId, String dataId, String dataPath, byte[] contents, int retries) throws EdmException {
        log.debug("[storeAdditionalData] jackrabbitServerId = {}, dataId = {}, dataPath = {}, retries = {}", jackrabbitServerId, dataId, dataPath, retries);

        checkNotNull(dataId, "dataId cannot be null");

        try {

            JackrabbitServer server = cachedServers.get().get(jackrabbitServerId);
            HttpClient httpClient = getHttpClient(server);
            HttpEntity resEntity = null;
            HttpPost post = null;
            try {
                String documentStoreUrl = server.getInternalUrlPrefix() + "/DocumentStore";
                post = new HttpPost(documentStoreUrl);

                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                builder.addBinaryBody("content", contents, ContentType.APPLICATION_OCTET_STREAM, "data");
                builder.addTextBody("id", dataId);
                builder.addTextBody("path", dataPath);
                post.setEntity(builder.build());

                HttpResponse response = httpClient.execute(post);

                resEntity = response.getEntity();

                try {
                    switch(response.getStatusLine().getStatusCode()){
                        case 200:
                            break;
                        default:
                            throw new RuntimeException("Nieoczekiwany b��d " + response.getStatusLine().getReasonPhrase());
                    }
                } finally {
                    EntityUtils.consume(resEntity);
                }
                log.info("[storeAdditionalData] success for id = {}", dataId);
            } catch (ClientProtocolException e) {
                throw new EdmException(e);
            } catch (UnsupportedEncodingException e) {
                throw new EdmException(e);
            } catch (IOException e) {
                throw new EdmException(e);
            } finally {
                if(post != null) {
                    log.debug("[storeAdditionalData] release connection jackrabbitServerId = {}, dataId = {}, dataPath = {}", jackrabbitServerId, dataId, dataPath);
                    post.releaseConnection();
                }
            }
        } catch(Exception ex) {
            if(retries > 0) {

                // TODO
                // zrobi� asynchronicznie (pewnie �atwiej b�dzie to zrobi�
                // po przepisaniu mechanizmu na Jersey REST)
                try {
                    Thread.currentThread().sleep(100);
                } catch (InterruptedException e1) {
                    log.error("[storeAdditionalData] cannot sleep", e1);
                }

                retries = retries - 1;
                storeAdditionalData(jackrabbitServerId, dataId, dataPath, contents, retries);

            } else {
                log.error("[storeAdditionalData] failure sending attachment, no retries left, throwing exception, jackrabbitServerId = {}, dataId = {}, dataPath = {}", jackrabbitServerId, dataId, dataPath);
                throw new EdmException(ex);
            }
        }

    }

    /**
     * Usuwa za��cznik z repozytorium Jackrabbit
     * @param rev
     * @throws EdmException
     */
    public void deleteAttachment(JackrabbitAttachmentRevision rev) throws EdmException {
        JackrabbitServer server = getServer(rev);
        HttpClient httpClient = getHttpClient(server);
        HttpEntity resEntity;
        try {
            String interopUrl = server.getInternalUrlPrefix() + "/TigerInterop";
            HttpDelete delete = new HttpDelete(interopUrl + "?attachmentRevisionId="+rev.getId());
            HttpResponse response = httpClient.execute(delete);
            resEntity = response.getEntity();
            try {
                switch(response.getStatusLine().getStatusCode()){
                    case 200:
                        break;
                    default:
                        throw new RuntimeException("Nieoczekiwany b��d " + response.getStatusLine().getReasonPhrase());
                }
            } finally {
                EntityUtils.consume(resEntity);
            }
        } catch (ClientProtocolException e) {
            throw new EdmException(e);
        } catch (UnsupportedEncodingException e) {
            throw new EdmException(e);
        } catch (IOException e) {
            throw new EdmException(e);
        }
    }

    /**
     * Pobiera id do za��cznika z serwera Jackrabbit, null je�li Jackrabbit nie jest w stanie znale�� zasobu
     * @param rev JackrabbitAttachmentRevision,
     * @param page nr strony, mo�e by� null (w takim przypadku zwr�cony b�dzie link do ca�ego za��cznika)
     * @return id do za��cznika lub "" je�li strony nie ma jeszcze w repozytorium
     * @throws EdmException
     */
    static String getLink(JackrabbitAttachmentRevision rev, Integer page) throws EdmException {
        long startTime = -1;
        if(PERFORMANCE_LOG.isTraceEnabled()){
            startTime = System.currentTimeMillis();
        }
        JackrabbitServer server = getServer(rev);
        HttpClient httpClient = getHttpClient(server);
        HttpEntity resEntity;
        try {
            //https://localhost:8444/repository/link
            String linkUrl = server.getInternalUrlPrefix() + "/link";
            HttpGet httpget = new HttpGet(linkUrl + "?attachmentRevisionId="+rev.getId() + (page != null ? ("&pageNumber=" + page) : ""));
            HttpResponse response = httpClient.execute(httpget);

            resEntity = response.getEntity();

            try {
                switch(response.getStatusLine().getStatusCode()){
                    case 404:
                        return "";
                    case 200:
                        break;
                    default:
                        throw new RuntimeException("Nieoczekiwany b��d " + response.getStatusLine().getReasonPhrase());
                }

                if (resEntity != null) {
                    String userUrl = server.getUserUrlPrefix();
                    return userUrl + "/att/" + IOUtils.toString(resEntity.getContent(), "US-ASCII"); //wynik jest zwyk�ym ascii, nie potrzeba kodowania
                } else {
                    throw new EdmException("B��d podczas pobierania za��cznika : " + response.getStatusLine());
                }
            } finally {
                EntityUtils.consume(resEntity);
            }
        } catch (ClientProtocolException e) {
            throw new EdmException(e);
        } catch (UnsupportedEncodingException e) {
            throw new EdmException(e);
        } catch (IOException e) {
            throw new EdmException(e);
        } finally {
            if(PERFORMANCE_LOG.isTraceEnabled()){
                long totalTime = System.currentTimeMillis() - startTime;
                PERFORMANCE_LOG.trace("getLink:time {}ms", totalTime);
            }
        }
    }

    InputStream downloadFile(JackrabbitAttachmentRevision rev) throws EdmException {
        int retries = getRetries();

        return downloadFile(rev, retries);
    }

    /**
     * Pobiera plik z serwera Jackrabbit
     * @param rev
     * @return
     * @throws EdmException
     */
    InputStream downloadFile(JackrabbitAttachmentRevision rev, int retries) throws EdmException {
        long startTime = -1;
        if(PERFORMANCE_LOG.isTraceEnabled()){
            startTime = System.currentTimeMillis();
        }

        try {

            JackrabbitServer server = getServer(rev);
            HttpClient httpClient = getHttpClient(server);

            boolean throwError = true;
            HttpEntity resEntity = null;

            HttpGet httpget = null;
            try {
                String interopUrl = server.getInternalUrlPrefix() + "/TigerInterop";
                httpget = new HttpGet(interopUrl + "?attachmentRevisionId="+rev.getId());

                HttpResponse response = httpClient.execute(httpget);
                resEntity = response.getEntity();

                switch(response.getStatusLine().getStatusCode()){
                    case 404:
                        throw new RuntimeException("Brak za��cznika w repozytorium " + rev.getId());
                    case 200:
                        break;
                    default:
                        throw new RuntimeException("Nieoczekiwany b��d " + response.getStatusLine().getReasonPhrase());
                }

                if (resEntity != null) {
                    throwError = false;
                    return resEntity.getContent();
                } else {
                    throw new EdmException("B��d podczas pobierania za��cznika : " + response.getStatusLine());
                }
            } catch (ClientProtocolException e) {
                throw new EdmException(e);
            } catch (UnsupportedEncodingException e) {
                throw new EdmException(e);
            } catch (IOException e) {
                throw new EdmException(e);
            } finally {

                // je�li by� b��d to wyczyszczamy content
                if(throwError && resEntity != null) {
                    try {
                        log.debug("[downloadFile] consuming entity, rev.id = {}", rev.getId());
                        EntityUtils.consume(resEntity);
                    } catch(IOException e) {
                        log.error("[downloadFile] cannot consume content, rev.id = {}", rev.getId());
                    }

                    if(httpget != null) {
                        log.debug("[downloadFile] release connection, rev.id = {}", rev.getId());
                        httpget.releaseConnection();
                    }
                }

                if(PERFORMANCE_LOG.isTraceEnabled()){
                    long totalTime = System.currentTimeMillis() - startTime;
                    PERFORMANCE_LOG.trace("downloadFile:time {}ms", totalTime);
                }
            }
        } catch(Exception ex) {
            if(retries > 0) {

                // TODO
                // zrobi� asynchronicznie (pewnie �atwiej b�dzie to zrobi�
                // po przepisaniu mechanizmu na Jersey REST)
                try {
                    Thread.currentThread().sleep(100);
                } catch (InterruptedException e1) {
                    log.error("[storeAdditionalData] cannot sleep", e1);
                }

                retries = retries - 1;
                return downloadFile(rev, retries);

            } else {
                log.error("[downloadFile] failure, no retries left, throwing exception, rev.id = {}", rev.getId());
                throw new EdmException(ex);
            }
        }

    }

    /**
     * Zwraca link do za��cznika (do pobrania), za��cznik musi by� w repozytorium
     * @param jackrabbitAttachmentRevision
     * @return
     */
    public String getDownloadLink(final JackrabbitAttachmentRevision jackrabbitAttachmentRevision) throws EdmException {
        try {
            return attachmentLinks.get(jackrabbitAttachmentRevision.getId(),
                    new Callable<String>() {
                        @Override
                        public String call() throws Exception {
                            return getLink(jackrabbitAttachmentRevision, null);
                        }
                    });
        } catch (ExecutionException ex) {
            throw new EdmException(ex.getMessage(), ex);
        }
    }

    /**
     * Zwraca link do strony (do wyswietlenia), ma sens tylko dla tiffow
     * @param rev
     * @return
     */
    public String getPageLink(final JackrabbitAttachmentRevision rev, final int page) throws EdmException {
//        StopwatchLog sl = new StopwatchLog();
        if(!rev.getMime().startsWith("image/tiff")){
            log.warn("trying to get page of non-tiff attachment");
            return getDownloadLink(rev);
        }
        try {
            Callable<String> cacheLoader = new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return getLink(rev, page);
                }
            };
            PageRef pr = new PageRef(rev.getId(), page);
            String id = pageLinks.get(pr, cacheLoader);
            if(id.length() == 0){  //jackrabbit nie ma jeszcze tego pliku - nalezy go wgrac
                log.info("wgrywanie do repozytorium strony {} dla za��cznika {}", page, rev.getId());
                uploadPage(rev, page, rev.getFileByPage(page));
                pageLinks.invalidate(pr);
            }
            id = pageLinks.get(pr, cacheLoader);
            if(id.length() == 0){ //z jakichs powodow jackrabbit wciaz nie ma tego pliku
                throw new RuntimeException("Wyst�pi� nieoczekiwany blad podczas pobierania strony " + page + " dla wersji zalacznika " + rev.getId());
            }
            return id;
        } catch (ExecutionException ex) {
            throw new EdmException(ex.getMessage(), ex);
        } finally {
//            sl.end();
        }
    }

    public int getNextServerId(){
        return cachedServers.get().getActiveServer().getId();
    }

    public JackrabbitServer getNextServer() {
        final int id = getNextServerId();
        return getServer(id);
    }

    void uploadPage(JackrabbitAttachmentRevision rev, int page, File file) throws EdmException {
        int retries = getRetries();

        uploadPage(rev, page, file, retries);
    }

    void uploadPage(JackrabbitAttachmentRevision rev, int page, File file, int retries) throws EdmException {
        log.info("uploading file {} for page {}", file.getAbsolutePath(), page);
        long startTime = -1;
        if(PERFORMANCE_LOG.isTraceEnabled()){
            startTime = System.currentTimeMillis();
        }

        try {
            JackrabbitServer server = getServer(rev);
            HttpClient httpClient = getHttpClient(server);

            HttpPost httppost = null;
            try {
                httppost = preparePost(rev, page, file, server);

                HttpResponse response = httpClient.execute(httppost);
                HttpEntity resEntity = response.getEntity();

                try {
                    switch(response.getStatusLine().getStatusCode()){
                        case 200:
                            break;
                        default:
                            throw new RuntimeException("Nieoczekiwany blad " + response.getStatusLine().getReasonPhrase());
                    }
                } finally {
                    EntityUtils.consume(resEntity);
                }
            } catch (ClientProtocolException e) {
                throw new EdmException(e);
            } catch (UnsupportedEncodingException e) {
                throw new EdmException(e);
            } catch (IOException e) {
                throw new EdmException(e);
            } finally {
                if(httppost != null) {
                    log.debug("[uploadPage] rev.id = {}, release connection", rev.getId());
                    httppost.releaseConnection();
                }
                if(PERFORMANCE_LOG.isTraceEnabled()){
                    long totalTime = System.currentTimeMillis() - startTime;
                    PERFORMANCE_LOG.trace("uploadPage:time {}ms", totalTime);
                }
            }
        } catch(Exception ex) {
            if(retries > 0) {
                // TODO
                // zrobic asynchronicznie (pewnie latwiej bedzie to zrobic
                // po przepisaniu mechanizmu na Jersey REST)
                try {
                    Thread.currentThread().sleep(100);
                } catch (InterruptedException e1) {
                    log.error("[uploadPage] cannot sleep", e1);
                }

                retries = retries - 1;

                uploadPage(rev, page, file, retries);
            } else {
                log.error("[uploadPage] failure, no retries left, throwing exception, rev.id = {}, page = {}, file = {}", rev.getId(), page, file);
                throw new EdmException(ex);
            }

        }
    }

    void uploadFile(JackrabbitAttachmentRevision rev, File file) throws EdmException {
        int retries = getRetries();

        uploadFile(rev, file, retries);
    }

    int getRetries() {
        int retries = AdditionManager.getIntPropertyOrDefault("jackrabbit.socketexception.retries", 0);

        if(retries < 0) {
            log.error("[getRetries] retries cannot be negative, setting to 0, retries = {}", retries);
            retries = 0;
        }

        return retries;
    }

    /**
     * @param rev
     * @param file
     * @param retries liczba requestow do wyslania ponownie gdy nastapi SocketException
     * @throws EdmException
     */
    void uploadFile(JackrabbitAttachmentRevision rev, File file, int retries) throws EdmException{

        log.debug("[uploadFile] rev.id = {}, retries = {}", rev.getId(), retries);

        try {

            long startTime = -1;
            if(PERFORMANCE_LOG.isTraceEnabled()){
                startTime = System.currentTimeMillis();
            }
            JackrabbitServer server = getServer(rev);
            HttpClient httpClient = getHttpClient(server);
            HttpPost httppost = null;
//            ----------------------------------
            
            String param = String.valueOf(rev.getId());
//            File textFile = new File("/path/to/file.txt");
            File binaryFile = file;
            String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
            String CRLF = "\r\n"; // Line separator required by multipart/form-data.

			String url = server.getInternalUrlPrefix() + "/TigerInterop";;
			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			String c = "admin"+":"+"admin";
			byte[] message = c.getBytes("UTF-8");
			String encoded = new String(Base64.encode(message));
			connection.setRequestProperty("Authorization", "Basic "+encoded);
	        connection.setDoInput(true);
	        connection.setDoOutput(true);
	        connection.setUseCaches(false);
	        connection.setChunkedStreamingMode(1024);
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            connection.connect();

            String charset = "ISO-8859-1";
			try {
                OutputStream output = connection.getOutputStream();
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset ), true);
                // Send normal param.
                writer.append("--" + boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"attachmentRevisionId\"").append(CRLF);
                writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
                writer.append(CRLF).append(param).append(CRLF).flush();

                // Send binary file.
                writer.append("--" + boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"content\"; filename=\"" + binaryFile.getName() + "\"").append(CRLF);
                writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
                writer.append("Content-Transfer-Encoding: binary").append(CRLF);
                writer.append(CRLF).flush();
                Files.copy(binaryFile, output);
                output.flush(); // Important before continuing with writer!
                writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.

                // End of multipart/form-data.
                writer.append("--" + boundary + "--").append(CRLF).flush();
            
                writer.close();
                output.close();
                
                connection.getResponseCode();
                
                connection.disconnect();
//            try {
//                httppost = preparePost(rev, null, file, server);
//           
//                HttpResponse response = httpClient.execute(httppost);
//                HttpEntity resEntity = response.getEntity();
//
//                try {
//                    switch(response.getStatusLine().getStatusCode()){
//                        case 200:
//                            log.debug("[uploadFile] rev.id = {} success", rev.getId());
//                            break;
//                        default:
//                            throw new RuntimeException("Nieoczekiwany b��d " + response.getStatusLine().getReasonPhrase());
//                    }
//                } finally {
//                    EntityUtils.consume(resEntity);
//                }
//            } catch (ClientProtocolException e) {
//                throw new EdmException(e);
//            } catch (UnsupportedEncodingException e) {
//                throw new EdmException(e);
//            } catch (SocketException e) {
//            	log.debug("socket closed = {}", e.getMessage());
//                throw new EdmException(e);
//            } catch (IOException e) {
//                throw new EdmException(e);
//            } finally {
//                if(httppost != null) {
//                    log.debug("[uploadFile] rev.id = {}, release connection", rev.getId());
//                    httppost.releaseConnection();
//                }
//                if(PERFORMANCE_LOG.isTraceEnabled()){
//                    long totalTime = System.currentTimeMillis() - startTime;
//                    PERFORMANCE_LOG.trace("uploadPage:time {}ms", totalTime);
//                }
            } catch(Exception ex) {
            	throw new Exception(ex);

        } }catch(Exception ex) {
            if(retries > 0) {

                // TODO
                //  zrobi� asynchronicznie (pewnie �atwiej b�dzie to zrobi�
                // po przepisaniu mechanizmu na Jersey REST)
                try {
                    Thread.currentThread().sleep(100);
                } catch (InterruptedException e1) {
                    log.error("[uploadFile] cannot sleep", e1);
                }

                retries = retries - 1;
                uploadFile(rev, file, retries);

            } else {
                log.error("[uploadFile] failure sending attachment, no retries left, throwing exception, rev.id = {}", rev.getId());
                throw new EdmException(ex);
            }
        }

    }

    private static JackrabbitServer getServer(JackrabbitAttachmentRevision rev){
        return cachedServers.get().get(rev.getJackrabbitServerId());
    }

    private static JackrabbitServer getServer(int serverId){
        return cachedServers.get().get(serverId);
    }

    void updateFileName(JackrabbitAttachmentRevision rev, String fileName) throws EdmException {
        JackrabbitServer server = getServer(rev);
        HttpClient httpClient = getHttpClient(server);
        try {
            List<NameValuePair> formparams = new ArrayList<NameValuePair>();
            formparams.add(new BasicNameValuePair("attachmentRevisionId", rev.getId().toString()));
            formparams.add(new BasicNameValuePair("fileName", fileName));
            //https://localhost:8444/repository/AttachmentUpdate
            String updateUrl = cachedServers.get().get(rev.getJackrabbitServerId()).getInternalUrlPrefix() + "/AttachmentUpdate";
            HttpPost httppost = new HttpPost(updateUrl);
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, "ISO-8859-2");
            httppost.setEntity(entity);

            HttpResponse response = httpClient.execute(httppost);
            HttpEntity resEntity = response.getEntity();

            try {
                switch(response.getStatusLine().getStatusCode()){
                    case 200:
                        break;
                    default:
                        throw new RuntimeException("Nieoczekiwany b��d " + response.getStatusLine().getReasonPhrase());
                }
            } finally {
                EntityUtils.consume(resEntity);
            }
        } catch (ClientProtocolException e) {
            throw new EdmException(e);
        } catch (UnsupportedEncodingException e) {
            throw new EdmException(e);
        } catch (IOException e) {
            throw new EdmException(e);
        }
    }

    private boolean action(JackrabbitServer server, HttpRequestBase base, List<NameValuePair> params, String expectedResponse) {
        HttpClient httpClient = getHttpClient(server);
        try {
//            String url = server.getInternalUrlPrefix() + "/" + action;
//            HttpPost httpPost = new HttpPost(url);

            HttpResponse response = httpClient.execute(base);
            HttpEntity resEntity = response.getEntity();

            final String content = getStringFromStream(resEntity.getContent());

            if(content == null || ! content.equals(expectedResponse)) {
                return false;
            } else {
                return true;
            }

        } catch(Exception ex) {
            log.error("[userAction] error, params = "+params, ex);
            return false;
        }
    }

    private boolean postAction(String action, List<NameValuePair> params, String expectedResponse) throws UnsupportedEncodingException {
        JackrabbitServer server = getNextServer();
        String url = server.getInternalUrlPrefix() + "/" + action;

        HttpPost httpPost = new HttpPost(url);
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "ISO-8859-2");
        httpPost.setEntity(entity);

        return action(server, httpPost, params, expectedResponse);
    }

    private boolean getAction(String action, List<NameValuePair> params, String expectedResponse) {
        log.debug("[getAction] action = {}, params = {}, expectedResponse = {}");

        JackrabbitServer server = getNextServer();
        String url = server.getInternalUrlPrefix() + "/" + action;

        String postfix = "";
        if(params.size() > 0) {
            postfix = "?";
            List<String> attrs = new ArrayList<String>();
            for(NameValuePair param: params) {
                attrs.add(param.getName()+"="+param.getValue());
            }

            postfix += StringUtils.join(attrs, "&");

        }

        url += postfix;

        log.debug("[getAction] url = {}", url);

        HttpGet httpGet = new HttpGet(url);
        return action(server, httpGet, params, expectedResponse);
    }

    /**
     * Testuje po��czenie z danym serwerem
     * @param server
     * @throws EdmException
     */
    public void testConnection(JackrabbitServer server) throws EdmException{
        HttpClient httpClient = getHttpClient(server);
        HttpEntity resEntity;
        try {
            String linkUrl = server.getInternalUrlPrefix() + "/admin-access/repository-stats";
            HttpGet httpget = new HttpGet(linkUrl);
            HttpResponse response = httpClient.execute(httpget);

            resEntity = response.getEntity();
            try {
                switch(response.getStatusLine().getStatusCode()){
                    case 200:
                        log.info("RESPONSE:\n", resEntity.toString());
                        break;
                    default:
                        throw new EdmException("B��d po��czenia : " + response.getStatusLine().getReasonPhrase());
                }
            } finally {
                EntityUtils.consume(resEntity);
            }
        } catch (ClientProtocolException e) {
            throw new EdmException(e);
        } catch (UnsupportedEncodingException e) {
            throw new EdmException(e);
        } catch (IOException e) {
            throw new EdmException(e);
        }
    }

    public static HttpClient getHttpClient(JackrabbitServer server) {
        if(server.isIgnoreSslErrors()){
            return getUnsafeHttpClient(server);
        } else {
            DefaultHttpClient ret = new DefaultHttpClient(connectionManager, getClientParams());
            ret.setCredentialsProvider(getCredentialsProvider(server));
            return ret;
        }
    }

    private static CredentialsProvider getCredentialsProvider(JackrabbitServer server){
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        if(server.getLogin() != null){
            credsProvider.setCredentials(AuthScope.ANY,  new UsernamePasswordCredentials(server.getLogin(), server.getPassword()));
        }
        return credsProvider;
    }

    /**
     * Przygotowywuj� wiadomo�� post do wys�ania Jackrabbitowi
     * @param rev za��cznik
     * @param page numer strony (mo�e by� null dla ca�ego za��cznika)
     * @param file tre�� do wys�ania
     * @return
     * @throws UnsupportedEncodingException
     */
    private HttpPost preparePost(JackrabbitAttachmentRevision rev, Integer page, File file, JackrabbitServer server) throws UnsupportedEncodingException {
        //cachedServers.get().get(rev.getJackrabbitServerId()).getInternalUrlPrefix()
        String interopUrl = server.getInternalUrlPrefix() + "/TigerInterop";
        HttpPost httppost = new HttpPost(interopUrl);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        String mime = MimetypesFileTypeMap.getInstance().getContentType(file);
        FileBody bin = new FileBody(file);
        builder.addPart("content", bin);
//        builder.addBinaryBody(file.getName(), file, ContentType.DEFAULT_BINARY, file.getName());
        builder.addTextBody("attachmentRevisionId", rev.getId().toString());

        if(page != null){
            builder.addTextBody("page", page.toString());
        }

        httppost.setEntity(builder.build());
        return httppost;
    }

    /**
     * Metoda dostarcza "niebezpiecznego" HttpClient'a. Przydatne podczas test�w.
     * @return
     */
    private static HttpClient getUnsafeHttpClient(JackrabbitServer server)   {
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            // set up a TrustManager that trusts everything
            sslContext.init(null, new TrustManager[] { new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) { }
                public void checkServerTrusted(X509Certificate[] certs, String authType) { }
            } }, new SecureRandom());

            SSLSocketFactory sf = new SSLSocketFactory(sslContext, new AllowAllHostnameVerifier());
            Scheme httpsScheme = new Scheme("https", sf, 443);
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(httpsScheme);
            ClientConnectionManager cm = new BasicClientConnectionManager(schemeRegistry);
            DefaultHttpClient ret = new DefaultHttpClient(cm, getClientParams());
            ret.setCredentialsProvider(getCredentialsProvider(server));
            return ret;
        } catch (KeyManagementException ex) {
            throw new RuntimeException(ex);
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static HttpParams getClientParams(){
        HttpParams params = new BasicHttpParams();

        HttpConnectionParams.setConnectionTimeout(params, Docusafe.getIntAdditionPropertyOrDefault("jackrabbitRepository.connectionTimeout", 100000));
        HttpConnectionParams.setSoTimeout(params,  Docusafe.getIntAdditionPropertyOrDefault("jackrabbitRepository.soTimeout", 100000));

        params.setParameter("http.protocol.version", HttpVersion.HTTP_1_1);
        params.setParameter("http.connection-manager.timeout", 10000L);
        params.setParameter("http.connection-manager.max-total", 100);
        Map hostToLimit = new ConcurrentHashMap();
        hostToLimit.put(HostConfiguration.ANY_HOST_CONFIGURATION, 100);
        params.setParameter("http.connection-manager.max-per-host", hostToLimit);

        return params;
    }

    private static String getStringFromStream(InputStream stream) throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(stream, writer);
        String contentString = writer.toString();
        return contentString;
    }

}