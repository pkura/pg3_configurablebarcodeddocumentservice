package pl.compan.docusafe.core.jackrabbit.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;
import java.util.List;

@XmlRootElement
public class DocumentAclEntity {

    final public static String ACTION_ADD = "add";
    final public static String ACTION_DELETE = "delete";
    final public static String ACTION_REVOKE = "revoke";

    public DocumentAclEntity() {}

    public DocumentAclEntity(String action, List<String> principalNames) {
        this.action = action;
        this.principalNames = principalNames;
    }

    public DocumentAclEntity(String action, String principalName) {
        this.action = action;
        this.principalNames = Arrays.asList(principalName);
    }

    @XmlElement
    public String action;

    @XmlElement
    @XmlList
    public List<String> principalNames;

    @Override
    public String toString() {
        return "DocumentAclEntity{" +
                "action='" + action + '\'' +
                ", principalNames=" + principalNames +
                '}';
    }

}