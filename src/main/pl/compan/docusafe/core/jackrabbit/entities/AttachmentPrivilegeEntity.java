package pl.compan.docusafe.core.jackrabbit.entities;

import javax.ws.rs.client.Entity;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>
 *     Encja przesy�ana do Jackrabbita
 * </p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
@XmlRootElement
@Deprecated
public class AttachmentPrivilegeEntity {

    @XmlElement
    public String revisionId;

    @XmlElement
    public String username;

    @XmlElement
    public String action;

    @Override
    public String toString() {
        return "AttachmentPrivilegeEntity{" +
                "revisionId='" + revisionId + '\'' +
                ", username='" + username + '\'' +
                ", action='" + action + '\'' +
                '}';
    }
}
