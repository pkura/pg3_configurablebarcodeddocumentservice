package pl.compan.docusafe.core.jackrabbit.entities;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
@XmlRootElement
public class DocumentVersionEntity {
    private String uuid;
    private String path;
    private String content;

    public DocumentVersionEntity() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}


