package pl.compan.docusafe.core.jackrabbit.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 *     Encja przesy�ana do Jackrabbita
 * </p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
@XmlRootElement
@Deprecated
public class DocumentPrivilegeEntity {

    @XmlElement
    public String documentId;

    @XmlElement
    public String path;

    @XmlElement
    @XmlList
    public List<String> usernames;

    @XmlElement
    @XmlList
    public List<String> groups;

    @XmlElement
    public String action;

    @Override
    public String toString() {
        return "DocumentPrivilegeEntity{" +
                "documentId='" + documentId + '\'' +
                ", path='" + path + '\'' +
                ", usernames=" + usernames +
                ", groups='" + groups + '\'' +
                ", action='" + action + '\'' +
                '}';
    }
}

