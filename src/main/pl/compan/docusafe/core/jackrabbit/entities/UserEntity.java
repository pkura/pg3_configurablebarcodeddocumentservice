package pl.compan.docusafe.core.jackrabbit.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>
 *     Encja przesy�ana do Jackrabbita
 * </p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
@XmlRootElement
public class UserEntity {

    @XmlElement
    public String username;

    @XmlElement
    public String password;

}
