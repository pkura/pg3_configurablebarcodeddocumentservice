package pl.compan.docusafe.core.jackrabbit.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class GroupEntity {

    @XmlElement
    public String guid;

    @XmlElement
    public List<String> users;

    @XmlElement
    public String action;

    @Override
    public String toString() {
        return "GroupEntity{" +
                "guid='" + guid + '\'' +
                ", users=" + users +
                ", action=" + action +
                '}';
    }
}


