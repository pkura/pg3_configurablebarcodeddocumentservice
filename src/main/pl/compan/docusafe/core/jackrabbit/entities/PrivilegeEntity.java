package pl.compan.docusafe.core.jackrabbit.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PrivilegeEntity {

    final public static String ACTION_ADD = "add";
    final public static String ACTION_DELETE = "delete";
    final public static String ACTION_REVOKE = "revoke";

    @XmlElement
    public String path;

    @XmlElement
    public String action;

    @XmlElement
    public String principalName;

    @Override
    public String toString() {
        return "PrivilegeEntity{" +
                "path='" + path + '\'' +
                ", action=" + action +
                ", principalName='" + principalName + '\'' +
                '}';
    }
}
