package pl.compan.docusafe.core;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: GlobalConstants.java,v 1.8 2006/02/06 16:05:56 lk Exp $
 */
public interface GlobalConstants
{
    /**
     * Pod tym kluczem filtr {@link pl.compan.docusafe.web.filter.PrepareEnvironmentFilter}
     * umieszcza w obiekcie request oryginaln� warto�� HttpServletRequest.requestURI
     * pozbawion� �cie�ki kontekstowej.
     */
    String ORIGINAL_REQUEST_URI = GlobalConstants.class.getName()+".ORIGINAL_REQUEST.URI";
    String PASSWORD_EXPIRED = GlobalConstants.class.getName()+".PASSWORD_EXPIRED";

    /**
     * Uri, pod jakim w pliku freemap.xml znajduj� si� rozszerzone atrybuty
     * dotycz�ce Docusafe.
     */
    //String FREEMAP_URI = "http://docusafe.pl/freemap";
}
