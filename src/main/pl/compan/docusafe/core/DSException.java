package pl.compan.docusafe.core;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class DSException extends EdmException
{
    private static final Log log = LogFactory.getLog(DSException.class);

    private List<String> messages;

    /**
     * Tworzy wyj�tek z komunikatem, bez informacji o wyj�tku
     * �r�d�owym. Tre�� komunikatu powinna istnie� i by� lokalizowana,
     * najlepiej przy pomocy {@link pl.compan.docusafe.util.StringManager}.
     */
    public DSException(String message)
    {
        super(message);
        log.error(message, this);
    }

    public DSException(String message, boolean noLogging)
    {
        super(message);
        if (!noLogging)
            log.error(message, this);
    }
    
    //tl
    public DSException(String message, boolean noLogging, boolean isCritical) 
    {
    	super(message);
    	if(!noLogging) {
    		if(!isCritical) {
        		log.debug(message, this);
        	} else log.error(message, this);	
    	}
    }

    public DSException(String message, Throwable cause)
    {
        super(message, cause);
        log.error(message, this);
    }

    public DSException(Throwable cause)
    {
        super(cause);
        log.error("", this);
    }

    public DSException(String message, Collection<String> messages)
    {
        super(message);
        this.messages = new ArrayList<String>(messages);
        log.error(message+" ("+messages+")", this);
    }

    public List getMessages()
    {
        if (messages != null)
            return Collections.unmodifiableList(messages);
        else
            return null;
    }

    public void addMessage(String message)
    {
        if (messages == null) messages = new ArrayList<String>();
        messages.add(message);
    }
}
