package pl.compan.docusafe.core.graphs;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import org.dom4j.Document;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.*;
import pl.docusafe.core.graph.Graph;
import pl.docusafe.core.graph.GraphDumper;
import pl.docusafe.core.graph.GraphModel;
import pl.docusafe.core.graph.ProcessEvent;
import pl.docusafe.core.graph.dot.DOTDumper;
import pl.docusafe.core.graph.graphml.GraphMLDumper;


/**
 *    
 * @author Grzegorz Filip
 *  Klasa do generowania tworzenia grafu przeplywu akcji(dekretacja , historia pisma) na dokumencie  wykozystujac Wojtka K. jara
 */
public class GraphGenerator
{
	private OfficeDocument document;
	private final static Logger log = LoggerFactory.getLogger(GraphGenerator.class);

    public GraphGenerator() {
    }

	public GraphGenerator(OfficeDocument doc)
	{
		this.document = doc;
	}

    /**
     * Generuje GraphML/DOT do Base64
     * @param docId
     * @param asDot
     * @param asGraphMl
     * @return
     */
    public String generateGraphFromDocIdBase64(Long docId, boolean asDot, boolean asGraphMl)
    {
        String base64file = null;
        File f;

        try
        {
            OfficeDocument doc = OfficeDocument.findOfficeDocument(docId);

            if(asGraphMl) {
                 f = createGraphFileFromOfficeDocument(doc, false, true);
            } else {
                 f = createGraphFileFromOfficeDocument(doc, true, false);
        }

            base64file = FileUtils.encodeByBase64(f);

        } catch (Exception e)
        {
            log.error("", e);
        }

        return base64file;
    }

    public Graph generateGraph()
    {
        Graph graph = null;

        try
        {

            GraphModel graphModel = new GraphModel();
            graphModel.registerDumper(new GraphMLDumper());
            graphModel.registerDumper(new DOTDumper());

            for (AssignmentHistoryEntry ahe : document.getAssignmentHistory())
            {

                addAssignmentHistoryElement(ahe, graphModel);
            }
            for (Audit audit : document.getWorkHistory())
            {
                addWorkHistoryElement(audit, graphModel);
            }

            graph = graphModel.buildModel();

        } catch (Exception e)
        {
            log.error("", e);
        }
        return graph;
    }

	public void generateGraph(boolean asDot, boolean asGraphMl)
	{
        try {
			streamFile(generateGraph(), asDot, asGraphMl);

		} catch (Exception e)
		{
			log.error("", e);
		}
	}

	private void streamFile(Graph graph, boolean asDot, boolean asGraphMl) throws IOException
	{
        streamFile(createFileFromGraph(graph, asDot, asGraphMl));
	}

    private File createGraphFileFromOfficeDocument(OfficeDocument docId, boolean asDot, boolean asGraphMl) throws IOException {

        GraphGenerator graphGenerator = new GraphGenerator(docId);
        Graph graph = graphGenerator.generateGraph();

        File sFile = new File(prepareFilePath(docId, asGraphMl, asDot));
        if (sFile.exists())
        {
            sFile.delete();
        }
        try
        {
            sFile.createNewFile();
        } catch (IOException e1)
        {
            log.error("", e1);
        }

        OutputStream oStream = new BufferedOutputStream(new FileOutputStream(sFile));
        if (asGraphMl)
        {
            GraphDumper dumper = new GraphMLDumper();

            Document doc = dumper.dumpGraphAsXML(graph);
            oStream.write(doc.asXML().getBytes());

        } else if (asDot)
        {
            GraphDumper dumper = new DOTDumper();
            String doc = dumper.dumpGraphAsString(graph);
            oStream.write(doc.getBytes());

        }
        oStream.close();
        return sFile;

    }

    private File createFileFromGraph(Graph graph, boolean asDot, boolean asGraphMl) throws IOException
    {
        File sFile = new File(prepareFilePath(document, asGraphMl, asDot));
        if (sFile.exists())
        {
            sFile.delete();
        }
        try
        {
            sFile.createNewFile();
        } catch (IOException e1)
        {
            log.error("", e1);
        }

        OutputStream oStream = new BufferedOutputStream(new FileOutputStream(sFile));
        if (asGraphMl)
        {
            GraphDumper dumper = new GraphMLDumper();

            Document doc = dumper.dumpGraphAsXML(graph);
            oStream.write(doc.asXML().getBytes());

			/*XMLWriter output = new XMLWriter(
			            new FileWriter(sFile));
			        output.write(doc );
			        output.close();*/
            //System.out.println(doc.asXML());
        } else if (asDot)
        {
            GraphDumper dumper = new DOTDumper();
            String doc = dumper.dumpGraphAsString(graph);
            oStream.write(doc.getBytes());
            //System.out.println(doc);
        }
        oStream.close();
        return sFile;
    }

	/**
	 * Metoda przygotowuje  plik   ktoru zostanie zapisany w katalogu home/graph/ 
	 * @param serializeObject 
	 * 
	 * @return
	 */
	private String prepareFilePath(OfficeDocument doc, boolean asGraphML, boolean asGraphDOT) throws IOException

    {
		StringBuilder path = new StringBuilder();
		path.append(getGraphsFolderPath());
		path.append("DOCUMENT_ID_" + doc.getId());
		path.append("_DATA_" + new DateUtils().formatCommonDateTimeWithoutWhiteSpaces(new Date()));

		if (asGraphML)
			path.append(".graphml");
		if (asGraphDOT)
			path.append(".txt");
		return path.toString();

	}

    private String getGraphsFolderPath() throws IOException {
        String path = Docusafe.getHome().getAbsolutePath() + "/graphs/";
        File fPath = new File(path);
        if(!fPath.exists()){
            fPath.mkdir();
        }

        return path;
    }

    private void streamFile(File sFile)
	{
		try
		{
			ServletUtils.streamFile(ServletActionContext.getResponse(), sFile, "application/xml",
					"Content-Disposition: attachment; filename=\"" + sFile.getName() + "\"");
		} catch (IOException e)
		{
			log.error("", e);
		}
	}

	private void addWorkHistoryElement(Audit audit, GraphModel graphModel) throws UserNotFoundException, EdmException
	{
		DSUser user = DSUser.findByUsername(audit.getUsername());
		StringBuilder divisionsNames = new StringBuilder();
		for (DSDivision div : user.getOriginalDivisionsWithoutGroup())
		{
			divisionsNames.append(div.getName() + " / ");
		}
		ProcessEvent graphEvent = new ProcessEvent();
		graphEvent.setEventCode(audit.getProperty());
		graphEvent.setEventDate(audit.getCtime());
		graphEvent.setEventDescription(audit.getDescription());
		graphEvent.setEventGuidName(divisionsNames.toString());
		graphEvent.setEventGuid("");
		graphEvent.setEventUser(user.getName());
		graphEvent.setEventUserName(user.getLastnameFirstnameWithOptionalIdentifier());

		graphModel.registerEvent(graphEvent);
	}

	private void addAssignmentHistoryElement(AssignmentHistoryEntry ahe, GraphModel graphModel) throws EdmException
	{

		DSUser sourceUser = DSUser.findByUsername(ahe.getSourceUser());
		DSDivision sourceDivision = null;
		DSUser targetUser = null;
		DSDivision targetDivision = null;
		boolean isDecretation = false;
		boolean decretationToUser = false;

		if (sourceUser.getOriginalDivisionsWithoutGroup().length > 0)
		{

			for (DSDivision div : sourceUser.getOriginalDivisionsWithoutGroup())
			{
				sourceDivision = div;
				break;
			}

		} else
		{
			sourceDivision = DSDivision.find(DSDivision.ROOT_GUID);
		}

		if ((ahe.getTargetUser() != null && !ahe.getTargetUser().equals("x")) || (ahe.getTargetGuid() != null && !ahe.getTargetGuid().equals("x")))
		{
			isDecretation = true;
		}
		//dekretacja na usera
		if (isDecretation && ahe.getTargetUser() != null && !ahe.getTargetUser().equals("x"))
		{
			decretationToUser = true;
			targetUser = DSUser.findByUsername(ahe.getTargetUser());

			if (targetUser.getOriginalDivisionsWithoutGroup().length > 0)
			{
				for (DSDivision div : sourceUser.getOriginalDivisionsWithoutGroup())
				{
					targetDivision = div;
					break;
				}

			}

			//dekretacja na DZIAL
		} else if (isDecretation)
		{
			targetDivision = DSDivision.findByName(ahe.getTargetGuid());
		}

		fillDecretatrionElement(graphModel, ahe, isDecretation, decretationToUser, sourceUser, sourceDivision, targetUser, targetDivision);

	}

	private void fillDecretatrionElement(GraphModel graphModel, AssignmentHistoryEntry ahe, boolean isDecretation, boolean decretationToUser,
			DSUser sourceUser, DSDivision sourceDivision, DSUser targetUser, DSDivision targetDivision) throws DivisionNotFoundException, EdmException
	{
		ProcessEvent graphEvent = new ProcessEvent();
		if (isDecretation)
		{
			if (decretationToUser)
			{
				graphEvent.setEventCode(ahe.getType().name());
				graphEvent.setEventDate(ahe.getCtime());
				graphEvent.setEventDescription(ahe.getObjective() + (ahe.getStatus() != null ? "Status dok:" + ahe.getStatus() : ""));
				graphEvent.setEventGuidName("");
				graphEvent.setEventGuid("");
				graphEvent.setEventUser(targetUser.getName());
				graphEvent.setEventUserName(targetUser.getLastnameFirstnameWithOptionalIdentifier());

			}
			//na dzial
			else
			{
				graphEvent.setEventCode(ahe.getType().name());
				graphEvent.setEventDate(ahe.getCtime());
				graphEvent.setEventDescription(ahe.getObjective() + (ahe.getStatus() != null ? "Status dok:" + ahe.getStatus() : ""));
				graphEvent.setEventGuidName(ahe.getTargetGuid());
				graphEvent.setEventGuid(DSDivision.findByName(ahe.getTargetGuid()).getGuid());
				graphEvent.setEventUser("");
				graphEvent.setEventUserName("");
			}
			//brak dekretacji czyli  start procesu lub przyjecie zadania	
		} else
		{
			graphEvent.setEventCode(ahe.getType().name());
			graphEvent.setEventDate(ahe.getCtime());
			graphEvent.setEventDescription(ahe.getObjective() + (ahe.getStatus() != null ? "Status dok:" + ahe.getStatus() : ""));
			graphEvent.setEventGuidName("");
			graphEvent.setEventGuid("");
			graphEvent.setEventUser(sourceUser.getName());
			graphEvent.setEventUserName(sourceUser.getLastnameFirstnameWithOptionalIdentifier());

		}
		graphModel.registerEvent(graphEvent);
	}

    public OfficeDocument getDocument() {
        return document;
    }

    public void setDocument(OfficeDocument document) {
        this.document = document;
    }
}
