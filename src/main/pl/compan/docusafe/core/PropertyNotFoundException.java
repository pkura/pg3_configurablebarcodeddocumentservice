package pl.compan.docusafe.core;

import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PropertyNotFoundException.java,v 1.4 2006/02/20 15:42:16 lk Exp $
 */
public class PropertyNotFoundException extends EntityNotFoundException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public PropertyNotFoundException(String propertyName)
    {
        super(sm.getString("propertyNotFoundException", propertyName), true);
    }
}
