package pl.compan.docusafe.core.naming;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author Micha� Sankowski
 */
public class StringResource {
	private final static Logger log = LoggerFactory.getLogger(StringResource.class);

    /** cache do string�w */
    private final static LoadingCache<String, String> stringCache = CacheBuilder.newBuilder()
            .expireAfterAccess(60, TimeUnit.MINUTES)
            .build(new CacheLoader<String, String>() {
                public String load(String id) throws Exception {
                    return Finder.find(StringResource.class, id).value;
                }
            });
	
	private String id;
	private String value;

	StringResource(){}

	private StringResource(String id, String value){
		this.id = id;
		this.value = value;
	}

	public static String get(String id) throws EdmException{
		try{
			return stringCache.get(id);
		} catch(Exception e){
			return null;
		}
	}

	public static void put(String id, String value) throws EdmException{
		DSApi.context().session().saveOrUpdate(new StringResource(id, value));
        stringCache.invalidate(id);
	}

	public String getId() {
		return id;
	}

	void setId(String id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
