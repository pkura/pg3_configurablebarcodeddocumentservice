package pl.compan.docusafe.core;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Klasa bazowa dla wyj�tk�w rzucanych w ramach aplikacji EDM.
 * Komunikaty zawarte w wyj�tkach tej klasy powinny by� w j�zyku
 * polskim i by� zrozumia�e dla u�ytkownika aplikacji, poniewa�
 * mog� zosta� wy�wietlone w przegl�darce.
 * <p>
 * Je�eli przechwytywany jest wyj�tek tej klasy i opakowywany
 * w kolejny wyj�tek (np. RuntimeException), w�wczas powinien zosta�
 * u�yty nast�puj�cy idiom:
 * <code><pre>
 * catch (EdmException e) {
 *     throw new RuntimeException(e.getMessage(), e);
 * }
 * </pre></code>
 * <p>
 * Kluczowym elementem jest tu tworzenie nowego wyj�tku przy pomocy
 * konstruktora pobieraj�cego komunikat oraz wyj�tek �r�d�owy. Je�eli
 * u�yty zostanie konstruktor pobieraj�cy wy��cznie wyj�tek �r�d�owy,
 * w�wczas metoda getMessage() nowego wyj�tku zwr�ci r�wnie� nazw�
 * wyj�tku �r�d�owego opr�cz samego komunikatu, co nie jest po��dane,
 * jako �e wyj�tki klasy EdmException zawsze posiadaj� komunikaty
 * i zawsze s� one we w�a�ciwym j�zyku.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EdmException.java,v 1.13 2010/05/19 06:31:43 pecet1 Exp $
 */


public class EdmException extends Exception
{
	static final long serialVersionUID = 23223;
    private static final Logger log = LoggerFactory.getLogger(EdmException.class);
        
    private List<String> messages;

    /**
     * Tworzy wyj�tek z komunikatem, bez informacji o wyj�tku
     * �r�d�owym. Tre�� komunikatu powinna istnie� i by� lokalizowana,
     * najlepiej przy pomocy {@link pl.compan.docusafe.util.StringManager}.
     */
    public EdmException(String message)
    {
        super(message);
        log.debug(message, this);
    }

    public EdmException(String message, boolean noLogging)
    {
        super(message);
        if (!noLogging)
            log.debug(message, this);
    }
    
    
    public EdmException(String message, Throwable cause)
    {
        super(message, cause);
        log.debug(message, this);
    }

    public EdmException(Throwable cause)
    {
        super(cause);
        log.debug("", this);
    }
    
    
    public EdmException(String message, Collection<String> messages)
    {
        super(message);
        this.messages = new ArrayList<String>(messages);
        log.debug (message+" ("+messages+")", this);
    }

    public List<String> getMessages()
    {
        if (messages != null)
            return Collections.unmodifiableList(messages);
        else
            return null;
    }

    public void addMessage(String message)
    {
        if (messages == null) messages = new ArrayList<String>();
        messages.add(message);
    }
}
