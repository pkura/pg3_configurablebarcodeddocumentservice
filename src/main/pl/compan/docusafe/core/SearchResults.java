package pl.compan.docusafe.core;

import java.util.Iterator;

/**
 * Obiekty implementuj�ce ten interfejs powinny by� zwracane przez
 * funkcje, kt�re zwracaj� dane porcjami.
 * <p>
 * Wszystkie wyniki zawarte w obiekcie mo�na odczyta� korzystaj�c
 * z funkcji interfejsu Iterator rozszerzanego interfejs SearchResults.
 *
 * @ see pl.compan.docusafe.core.base.DocumentHelper#getDocuments(int, int, boolean, java.lang.String, boolean, java.util.Date, java.util.Date, java.lang.String)
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SearchResults.java,v 1.9 2006/02/14 16:00:05 lk Exp $
 */
public abstract interface SearchResults<T> extends Iterator<T>
{
    /**
     * Metoda zwraca warto�� <code>true</code>, je�eli zwr�cone
     * wyniki nie wyczerpuj� zapytania, np. w sytuacji, gdy u�ytkownik
     * za��da� tylko okre�lonej liczby wynik�w, mniejszej ni�
     * liczba wszystkich mo�liwych wynik�w.
     */
    boolean hasMore();

    /**
     * Zwraca liczb� wynik�w w tym obiekcie.
     */
    int count();

    /**
     * Zwraca ca�kowit� liczb� wynik�w lub -1, je�eli ta informacja nie
     * jest dost�pna.
     */
    int totalCount();

    /**
     * Zwraca tablic� obiekt�w, kt�re zostan� zwr�cone przez ten iterator.
     */
    T[] results();

    /**
     * Przekazuje obiekt Mapper.  Mapper jest u�ywany do podmiany
     * wynik�w wyszukiwania pobieranych metod� next() (nie ma wp�ywu
     * na tablic� wynik�w zwracan� przez results()).  Je�eli przekazany
     * obiekt jest r�ny od null, metoda next() zwr�ci zamiast w�a�ciwego
     * obiektu wynik wywo�ania Mapper.map() na tym obiekcie.
     */
    void setMapper(Mapper<T> mapper);

    public interface Mapper<T>
    {
        T map(T o);
    }
}
