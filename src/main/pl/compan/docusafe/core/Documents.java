package pl.compan.docusafe.core;

import com.google.common.base.Preconditions;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa pomocnicza wyci�gaj�ca DocFacade z cache'a
 * - obiekty pomocnicze udost�pniaj�ce szereg metod do obs�ugi dokument�w
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class Documents {
    private final static Logger LOG = LoggerFactory.getLogger(Documents.class);

    public static DocFacade document(long id){
        //LOG.info("access for id " + id, new Throwable());
        return DocumentsCache.getInstance().getDocFacade(id);
    }

    /**
     * Zwraca dany dokument w procesie kancelaryjnym
     * @param id identyfikator dokumentu
     * @param activityId activity id danego procesu kancelaryjnego, nie mo�e by� null
     * @return instancja OfficeDocFacade
     */
    public static OfficeDocFacade officeDocument(long id, String activityId){
        //LOG.info("access for id " + id, new Throwable());
        Preconditions.checkNotNull(activityId, "activityId cannot be null");
        return DocumentsCache.getInstance().getOfficeDocFacade(id, activityId);
    }

    /**
     * Zapisuje (nie kommituje) wszystkie zmiany dotycz�ce danego dokumentu
     * @param id
     */
    public static void flushChangesForDocument(long id) throws EdmException{
        DocumentsCache.getInstance().flushDocument(id);
    }
}
