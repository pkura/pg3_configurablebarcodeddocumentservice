package pl.compan.docusafe.core.base;

import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import org.hibernate.criterion.Restrictions;

public class FaxCache {
	private Long id;
	private Date ctime;
    private String fileName;
    private Long documentId;
    
    public FaxCache()
    {};
    
    public void create() throws EdmException
    {
    	if(this.ctime == null)
    		this.ctime = new Date();
         Persister.create(this);
    }
    
    public static List<Box> findByFileName(String fileName) throws EdmException
    {
        return DSApi.context().session().createCriteria(FaxCache.class).
        add(Restrictions.eq("fileName", fileName).ignoreCase()).list();
    }

    public static List<Box> findByFileNameAndDocId(String fileName, Long documentId) throws EdmException
    {
        return DSApi.context().session().createCriteria(FaxCache.class)
                .add(Restrictions.eq("fileName", fileName).ignoreCase())
                .add(Restrictions.eq("documentId", documentId))
                .list();
    }

	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
