package pl.compan.docusafe.core.base;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.util.StringManager;

/**
 * Informacja o podpisie elektronicznym z�o�onym na danym dokumencie.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class DocumentSignature
{
    private Long id;
    private Document document;
    private String author;
    private Date ctime;
    private Boolean correctlyVerified;
    private Integer type;
    
    static final StringManager sm =
        StringManager.getManager(Constants.Package);

    static public final int BLOB_XML = 1;
    static public final int BLOB_PDF = 2;
    static public final int BLOB_SIG = 3;
    
    static public final int TYPE_SIG = 2;
    static public final int TYPE_ACCEPT = 2;

    static public final String SIG_EXTENSION = ".pem";

    public synchronized void create() throws EdmException
    {
        try 
        {
            this.ctime = new Date();
            this.author = DSApi.context().getPrincipalName();
            
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static DocumentSignature find(Long id) throws EdmException
    {
        return (DocumentSignature) Finder.find(DocumentSignature.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public static List<DocumentSignature> findByDocumentId(Long documentId) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(DocumentSignature.class);
        
        try
        {  
            c.createCriteria("document").add(org.hibernate.criterion.Expression.eq("id",documentId));
            c.addOrder(org.hibernate.criterion.Order.desc("ctime"));
            return c.list();
           
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static DocumentSignature findMostRecentSignature(Long documentId/*, boolean office*/) throws EdmException
    {
        try
        {
            List signatures = DSApi.context().classicSession().find("select h from h in class " + DocumentSignature.class.getName() + " where h.ctime = (select max(hmax.ctime) from hmax in class " + DocumentSignature.class.getName() + /*" where hmax.isOffice = " + isoffice +*/ " where hmax.document.id = " + documentId + /*") and h.isOffice = " + isoffice +*/ ") and h.document.id = " + documentId);
    
            if (signatures != null && signatures.size() > 0 && signatures.get(0) instanceof DocumentSignature)
            {
                return (DocumentSignature) signatures.get(0);
            }
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zapisuje do bazy zawartosc podpisu z pliku podanego jako parametr
     * lub zawartosc pdf podpisywanego dokumentu.
     *
     * @param file plik z trescia
     * @param type oznacza co zapisujemy: podpis,pdf lub xml
     * @throws EdmException
     */
    public void saveContent(File file, int type) throws EdmException
    {
        FileInputStream stream = null;
        try {
            stream = new FileInputStream(file);
            int fileSize = (int) file.length();
            saveContent(stream, type, fileSize);
        }
        catch (IOException e)
        {
            throw new EdmException("B�?d odczytu pliku", e);
        }
    }

    /**
     * Zapisuje do bazy zawartosc podpisu z pliku podanego jako parametr
     * lub zawartosc pdf podpisywanego dokumentu.
     * 
     * @param str tresc
     * @param type oznacza co zapisujemy: podpis,pdf lub xml
     * @throws EdmException
     */
    public void saveContent(String str, int type) throws EdmException
    {
        byte[] buf = str.getBytes();
        saveContent(new ByteArrayInputStream(buf), type, str.length());
    }


    private void saveContent(InputStream stream, int type, int fileSize) throws EdmException
    {
        //FileInputStream stream = null;
        String blob_name = null;
        if (BLOB_PDF == type)
            blob_name = "doccontent";
        else if (BLOB_XML == type)
            blob_name = "xmlcontent";
        else if (BLOB_SIG == type)
            blob_name = "sigcontent";

        try 
        {
           // stream = new FileInputStream(file);
           // int fileSize = (int) file.length();
            PreparedStatement ps = null;
            
            try 
            {
                DSApi.context().session().save(this);
                DSApi.context().session().flush();
                
                int rows;
                if (DSApi.isOracleServer())
                {
                    ps = DSApi.context().prepareStatement("select "+blob_name+" from "+DSApi.getTableName(DocumentSignature.class)+
                        " where id = ? for update");
                    ps.setLong(1, this.getId().longValue());
    
                    ResultSet rs = ps.executeQuery();
                    if (!rs.next())
                        throw new EdmException("Nie mo�na odczyta� tre?ci za�?cznika "+this.getId());
    
                    rows = 1;
    
                    Blob blob = rs.getBlob(1);
                    if (!(blob instanceof oracle.sql.BLOB))
                        throw new EdmException("Spodziewano si� obiektu oracle.sql.BLOB");
    
                    OutputStream out = ((oracle.sql.BLOB) blob).getBinaryOutputStream();
                    byte[] buf = new byte[2048];
                    int count;
                    while ((count = stream.read(buf)) > 0)
                    {
                        out.write(buf, 0, count);
                    }
                    out.close();
                    stream.close();
                }
                else
                {
                    ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(DocumentSignature.class)+
                        " set "+blob_name+" = ? where id = ?");
                    ps.setBinaryStream(1, stream, fileSize);
                    ps.setLong(2, this.getId().longValue());
                    rows = ps.executeUpdate();
                }
                
                if (rows != 1)
                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
            }
            catch (IOException e)
            {
                throw new EdmException("B�?d podczas zapisu danych", e);
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }
            finally
            {
            	DSApi.context().closeStatement(ps);                
            }
        }
       /* catch (IOException e)
        {
            throw new EdmException("B�?d odczytu pliku", e);
        }*/
        finally
        {
            if (stream != null) try { stream.close(); } catch (Exception e) { }
        }
    }
    
    
    /**
     * Pobiera strumie� binarny z podpisem dokumentu lub z dokumentem
     * podpisywanym (gdy parametr r�wny false pobieramy dokument).
     * Metoda powinna by� wywo�ywana tylko i wy��cznie w ramach
     * transakcji, w innym przypadku niekt�re sterowniki JDBC
     * b�d� pr�bowa�y wczyta� ca�� tre�� bloba do pami�ci.
     * <p>
     * Zwracany strumie� zawiera w sobie obiekt Statement u�yty
     * do odczytania BLOB-a, dlatego powinien by� zawsze zamykany
     * przy pomocy metody closeStream(). Metoda close() rzuca
     * wyj�tek.
     * @return
     * @throws EdmException
     */
    public BlobInputStream getBinaryStream(int type)
        throws EdmException
    {
        String blob_name = null;
        if (BLOB_PDF == type)
            blob_name = "doccontent";
        else if (BLOB_XML == type)
            blob_name = "xmlcontent";
        else if (BLOB_SIG == type)
            blob_name = "sigcontent";

     /*   if (!attachment.getDocument().canReadAttachments())
            throw new AccessDeniedException("Brak uprawnie� do odczytu tre�ci " +
                "za��cznika");*/
        boolean closeStm = true;
        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select "+blob_name+" from ds_document_signature where id = ?");
            pst.setLong(1, getId());
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
                throw new EdmException(sm.getString("documentHelper.blobRetrieveFailed", getId()));

            InputStream is = rs.getBinaryStream(1);
            // sterownik dla SQLServera nie wspiera ResultSet.getBlob
            //Blob blob = rs.getBlob(1);
            if (is != null)
            {
            	closeStm = false;
                return new BlobInputStream(is, pst);
            }
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "documentSignature="+getId());
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	if (closeStm)
        	{
        		DSApi.context().closeStatement(pst);
        	}
        }
        
        // Nie mo�na zamkn�� obiektu Statement od razu po zwr�ceniu strumienia,
        // poniewa� w�wczas nie b�dzie mo�liwe odczytanie danych ze strumienia.
        // Statement jest zamykane w metodzie BlobInputStream.closeStream()
    }
    
    public String getTypeAsString()
    {
    	if(this.type == null || this.type.equals(1))
    	{
    		return sm.getString("podpis");
    	}
    	else
    	{
    		return sm.getString("akceptacja");
    	}
    }
   
    
    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }
    
    public Document getDocument()
    {
        return document;
    }

    public void setDocument(Document document)
    {
        this.document = document;
    }
    
    public String getAuthor()
    {
        return author;
    }

    private void setAuthor(String author)
    {
        this.author = author;
    }
    
    public Date getCtime()
    {
        return ctime;
    }

    private void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }
    
    public Boolean getCorrectlyVerified()
    {
        return correctlyVerified;
    }

    public void setCorrectlyVerified(Boolean correctlyVerified)
    {
        this.correctlyVerified = correctlyVerified;
    }

	public void setType(Integer type)
	{
		this.type = type;
	}

	public Integer getType()
	{
		return type;
	}
}
