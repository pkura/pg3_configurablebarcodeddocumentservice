package pl.compan.docusafe.core.base;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.*;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Collection;
import java.util.Vector;
import java.util.Iterator;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentHelper.java,v 1.72 2010/02/23 14:29:57 tomekl Exp $
 */
public class DocumentHelper
{
    private static final Logger log = LoggerFactory.getLogger(DocumentHelper.class);
    private static StringManager sm =
        StringManager.getManager(Constants.Package);

    // TODO: po��czy� searchDocuments i getDocuments w jedn� metod� Document.search(FormQuery)

    /**
     * Wyszukuje dokumenty na podstawie podanych kryteri�w.
     * @param folder Folder, w kt�rym poszukiwane s� dokumenty, musi by� podany.
     * @param offset
     * @param limit
     * @param showDeleted Je�eli true, zostan� uwzgl�dnione dokumenty oznaczone
     *  jako skasowane.
     * @param sortProperty Atrybut obiektu Document, po jakim powinny by�
     *  sortowane wyniki.
     * @param ascending Kierunek sortowania.
     * @return Obiekt SearchResults, kt�rego metoda totalCount() zwraca ca�kowit�
     *  liczb� wynik�w.
     * @throws EdmException
     */
    public SearchResults<Document> searchDocuments(int offset, int limit, Folder folder,
                                         boolean showDeleted,
                                         String sortProperty, boolean ascending)
        throws EdmException
    {
        if (folder == null)
            throw new NullPointerException(sm.getString("nullParameter", "folder"));

        String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";

        PreparedStatement pst = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ResultSet rsCount = null;
        Collection<Object> variables = new Vector<Object>();
        try
        {
         

            DSUser user = DSApi.context().getDSUser();

            String from;

            // tworz� cz�� WHERE zapytania odczytuj�cego dokumenty z folderu
            // tabela ds_document musi mie� alias doc, tabela ds_document_permission
            // musi mie� alias perm
            /**
             * @TODO - przerobic to na preparedStatement
             */
            StringBuilder where = new StringBuilder();
            
            where.append("where doc.folder_id = ?");
            variables.add(folder.getId());

           if (!DSApi.context().isAdmin()) {
				from = "from " + DSApi.getTableName(Document.class) + " doc " + nolock + ", " +
						DSApi.getTableName(DocumentPermission.class) + " perm " + nolock;

				where.append(
						" and perm.document_id = doc.id" +
						" and perm.name = '" + ObjectPermission.READ + //"' and perm.negative = 0 " +
						"' and (");

				where.append("perm.subjecttype = '" + ObjectPermission.ANY + "' ");
				where.append(" OR (perm.subjecttype = '" + ObjectPermission.USER + "' " +
						"and perm.subject=?)");
				variables.add(user.getName());
			
				// tylko grupy mog� mie� uprawnienia
				for (DSDivision division : user.getDivisions()) {
					if (!division.isGroup()) {
						continue;
					}
					where.append(" OR (perm.subjecttype = '" + ObjectPermission.GROUP + "' " +
							"and perm.subject = ?)");
					variables.add(division.getGuid());
				}

				where.append(")");
			} else {
				from = "from " + DSApi.getTableName(Document.class) + " doc " + nolock;
			}

            if (log.isDebugEnabled())
                log.debug("searchDocuments: where="+where);

            // ca�kowita liczba dokument�w
            pst = DSApi.context().prepareStatement("select count(distinct doc.id) " + from + where.toString());
            Iterator it = variables.iterator();
            int licznik = 1;
            while (it.hasNext())
            {
            	Object obj = it.next();
            	pst.setObject(licznik, obj);
            	licznik++;
            }
            rsCount = pst.executeQuery();
            if (!rsCount.next())
                throw new EdmException("Nie mo�na pobra� liczby dokument�w");

            int totalCount = rsCount.getInt(1);
            if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu")){
            where.append(" and doc.czy_aktualny = ?");
            variables.add(true);
            }
            if (log.isDebugEnabled())
                log.debug("searchDocuments: totalCount="+totalCount);

            rsCount.close();
            DSApi.context().closeStatement(pst);

            // wyszukiwanie dokument�w

            // doc.<sortProperty> jest potrzebne, poniewa� po tej kolumnie
            // wykonywane jest sortowanie (b��d ORA-01791)
            
            
            	
            String sql = "select distinct doc.id, doc."+sortProperty+" "+
                from +
                where.toString()+
                ( " AND doc.deleted= ? ") +
                " order by doc."+sortProperty+
                (ascending ? " asc" : " desc");
            variables.add(showDeleted);
            log.trace("--> zapytanie sql = " + sql);//TODO mkulbaka
            
            // zak�adam, �e baza obs�uguje LIMIT (supportsLimit=true)
            boolean supportsOffset = DSApi.getDialect().supportsLimitOffset();

            // dodanie parametr�w LIMIT i OFFSET
            // je�eli baza nie wspiera podawania offsetu, limit jest zwi�kszany
            // o warto�� offsetu

            sql = DSApi.getDialect().getLimitString(sql, supportsOffset ? offset : 0,
                supportsOffset ? limit : limit+offset);
           
            //sql = DSApi.getDialect().getLimitString(sql, offset,limit);
//            if (DSApi.isPostgresServer() && !(DSApi.getDialect().bindLimitParametersFirst())
//    				&& supportsOffset && offset!=0)
//            	sql+= "offset ?";
	        log.trace(sql);

            ps = DSApi.context().prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            it = variables.iterator();
            licznik = 1;

			if(DSApi.getDialect().bindLimitParametersFirst()){
				licznik ++;
				ps.setObject(1, limit);
			}

			if(log.isDebugEnabled()){
				log.debug("PreparedStatement == {}", sql);
				log.debug("variables == {}", variables.toString());
			}

			// firebird
            if (DSApi.getDialect().bindLimitParametersFirst() && supportsOffset && offset > 0) {
				ps.setObject(licznik++, offset);
			}

            while (it.hasNext()) {
				Object obj = it.next();
				ps.setObject(licznik, obj);
				licznik++;
			}
			
            if (!(DSApi.getDialect().bindLimitParametersFirst())
				&& supportsOffset) {
				if (offset == 0) {
					ps.setInt(licznik, limit);     //Hibernate 3 inaczej ustawia offsety
				} else {
					if (DSApi.getDialect().bindLimitParametersInReverseOrder()) {
						if(DSApi.isOracleServer())
						{
							ps.setInt(licznik, limit+offset);
							licznik++;
							ps.setInt(licznik, offset);
						}
						else{
							ps.setInt(licznik, limit);
							ps.setInt(licznik + 1, offset);
						}
					} else {
						ps.setInt(licznik, offset);
						ps.setInt(licznik + 1, limit);
					}
				}
			}

            rs = ps.executeQuery();

            // je�eli nie mo�na u�y� OFFSET, przewijam ResultSet do ��danego
            // elementu (driver do MSSQL nie wspiera ResultSet.absolute/relative)
            if (!supportsOffset && offset > 0) {
				rs.absolute(offset);
			}

            List<Document> documents = new LinkedList<Document>();

            while (rs.next())
            {
                // TODO: getDocument albo (bez wyj�tku) metoda pomocnicza
                // canReturnDocument() u�yta r�wnie� w getDocument
                Document document = (Document) DSApi.context().session().
                    load(Document.class, new Long(rs.getLong("id")));
                documents.add(document);
            }

            rs.close();

            return new SearchResultsAdapter<Document>(documents, offset, totalCount, Document.class);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e, "folder="+folder+", sortProperty="+sortProperty+
                ", offset="+offset+", limit="+limit);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "folder="+folder+", sortProperty="+sortProperty+
                ", offset="+offset+", limit="+limit);
        }
        finally
        {
        	DbUtils.closeQuietly(rs);
        	DbUtils.closeQuietly(rsCount);
        	DSApi.context().closeStatement(pst);
            DSApi.context().closeStatement(ps);            
        }
    }

    /**
     * Usuwa folder razem z folderami podrz�dnymi, je�eli nie znajduj�
     * si� w nim (ani w folderach podrz�dnych) �adne dokumenty. Foldery
     * przed usuni�ciem s� blokowane w trybie UPGRADE.
     * <p>
     * Nie jest mo�liwe skasowanie folderu g��wnego.
     * @param folder
     * @throws EdmException
     */
    
    public void safeDeleteFolder(Folder folder) throws EdmException, NotEmptyFoldersFoundException, AccessDeniedException
    {
    	safeDeleteFolder(folder, true);
    }
    
    public void safeDeleteFolder(Folder folder, boolean checkPerm)
        throws EdmException, NotEmptyFoldersFoundException, AccessDeniedException
    {
        if (folder == null)
            throw new NullPointerException(sm.getString("nullParameter", "folder"));

        List<Folder> foldersToDelete = new LinkedList<Folder>();

        foldersToDelete.add(folder);
        // pozycja na li�cie folderu, z kt�rego aktualnie pobierane
        // s� foldery potomne i umieszczane na li�cie, kiedy wska�nik
        // znajdzie si� za ko�cem listy, na li�cie s� wszystkie foldery
        int ptr = 0;
        while (ptr < foldersToDelete.size())
        {
            foldersToDelete.addAll(foldersToDelete.get(ptr).getChildFolders());
            Folder f = foldersToDelete.get(ptr);
            if (checkPerm)
            {
            	if(!f.canDelete())
            	throw new AccessDeniedException(sm.getString("documentHelper.cannotDeleteFolder", f.getPrettyPath()));
            }
                
            if (getDocumentCount(f) > 0)
                throw new NotEmptyFoldersFoundException(sm.getString("ZnalezionoCoNajmniejJedenFolderZawierajacyDokumenty")+": "+f.getPrettyPath()+" "
                		+sm.getString("WusuwanejStrukturze.NiepusteFolderyNieMogaBycUsuwane"));
            ptr++;
        }

        try
        {
            for (Folder f : foldersToDelete)
            {
                DSApi.context().session().lock(f, LockMode.UPGRADE_NOWAIT);
            }

            for (Folder f : foldersToDelete)
            {
                DSApi.context().session().delete(f);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "folder="+folder+", foldersToDelete="+foldersToDelete);
        }
    }

    /**
     * Zwraca liczb� dokument�w znajduj�cych si� w folderze.
     */
    private int getDocumentCount(Folder folder) throws EdmException
    {
        if (folder == null)
            throw new NullPointerException(sm.getString("nullParameter", "folder"));
        try
        {        	
        	Query query = DSApi.context().session().createQuery("select count(doc.id) from doc in class "+Document.class.getName()+" where doc.folderId = ?");
            query.setLong(0, folder.getId());
        	Long resp = (Long) query.list().get(0);
        	return resp.intValue();        	
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "folder="+folder);
        }
    }

    /**
     * tworzenie pliku XML dla dokumentu z danymi dokumentu i za��cznikiem w postaci BASE64
     * @param documentId
     * @return
     * @throws EdmException
     */
    public static boolean createDocumentMetadataFileEvent(Long documentId) throws EdmException {
        if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
            JackrabbitXmlSynchronizer.createEvent(documentId);
        }
        return true;
    }
    /**
     * Metoda sprawdza czy nie ma warunku blokujacego edycje
     * @param document
     * @return true - nie wykonujemy operacji false wykonujemy
     */
    public static boolean cannotUpdate(Document document)
    {
        if (document.getType() == DocumentType.OUTGOING)
        {
            if (document instanceof OutOfficeDocument && !((OutOfficeDocument) document).canBeUpdate())
            {
                return true;
            }
        }
        return false;
    }

	public Document getDocument(Long documentId) throws EdmException
	{
		return DSApi.context().load(Document.class, documentId);
	}
}
