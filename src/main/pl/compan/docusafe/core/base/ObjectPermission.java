package pl.compan.docusafe.core.base;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ObjectPermission.java,v 1.9 2009/12/22 15:06:51 tomekl Exp $
 */
public abstract class ObjectPermission
    implements java.io.Serializable, Cloneable
{
    public static final String USER = "user";
    public static final String GROUP = "group";
    public static final String ANY = "any";

    public static final String READ = "read";
    public static final String READ_ATTACHMENTS = "read_att";
    public static final String READ_ORGINAL_ATTACHMENTS = "read_org_att";
    public static final String MODIFY = "modify";

    public static final String SOURCE_DOCUMENT = "source_document";
    public static final String SOURCE_TASKLIST = "source_tasklist";
    public static final String SOURCE_PERMISSION = "source_permission";
    public static final String SOURCE_WATCH = "source_watch";
    public static final String SOURCE_DIVISION = "source_division";
    public static final String SOURCE_CLERK = "source_clerk";

    /**
     * Modyfikowanie (w tym tworzenie i usuwanie) za��cznik�w.
     */
    public static final String MODIFY_ATTACHMENTS = "modify_att";
   
    public static final String DELETE = "delete";
    /**
     * Uprawnienie folderu - tworzenie dokument�w w folderze.
     */
    public static final String CREATE = "create";

    private String subject;
    private String subjectType;
    private String name;
    
    /**
     * Uprawnienia negatywne nie maja praktycznego zastosowania, a moga obnizac wydajnosc
     */
    @Deprecated 
    private boolean negative;

    public static List find(Class clazz, Long id)
        throws EdmException
    {
        Criteria crit;

        if (clazz == Document.class)
        {
            crit = DSApi.context().session().createCriteria(DocumentPermission.class);
            crit.add(Expression.eq("documentId", id));
        }
        else if (clazz == Folder.class)
        {
            crit = DSApi.context().session().createCriteria(FolderPermission.class);
            crit.add(Expression.eq("folderId", id));
        }
        else
        {
            throw new EdmException("Za��dano nieznanego rodzaju uprawnie�: "+clazz);
        }

        try
        {
            return crit.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static List<ObjectPermission> findObjectPermission(Class clazz, Long id) throws EdmException
    {
    	Criteria crit;

        if (clazz == Document.class)
        {
            crit = DSApi.context().session().createCriteria(DocumentPermission.class)
            	.add(Restrictions.eq("documentId", id));
        }
        else if (clazz == Folder.class)
        {
            crit = DSApi.context().session().createCriteria(FolderPermission.class)
            	.add(Restrictions.eq("folderId", id));
        }
        else
        {
            throw new EdmException("Za��dano nieznanego rodzaju uprawnie�: "+clazz);
        }

        try
        {
            return crit.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Konwertuje uprawnienia obiektu na map�, kt�ra jest podobna do mapy na formatach tj.:
     * Map.put("eoda/user", Lists.newArrayList("create","read"))
     *
     * @param permissions
     * @return
     */
    public static Map<String, List<String>> findAndConvert(Class clazz,Long objectId) throws EdmException {
        List<ObjectPermission> permissions = find(clazz, objectId);
        return convertToPermissionsMap(permissions);
    }

    /**
     * Konwertuje uprawnienia obiektu na map�, kt�ra jest podobna do mapy na formatach tj.:
     * Map.put("eoda/user", Lists.newArrayList("create","read"))
     *
     * @param permissions
     * @return
     */
    public static Map<String, List<String>> convertToPermissionsMap(List<ObjectPermission> permissions){
        Map<String, List<String>> result = Maps.newHashMap();

        for(ObjectPermission perm : permissions)
        {
            String subject = perm.getSubjectAndSubjectType();
            //Je�li taki principal si� znajduje w mapi� dodajemy do listy
            if(result.containsKey(subject)){
                result.get(subject).add(perm.getName());
            } else {
                // nie ma, tworzymy liste z nowym
                List<String> s = Lists.newArrayList(perm.getName());
                result.put(subject, s);
            }
        }

        return result;
    }

    public static void create(Class clazz, Long id, String subject, String subjectType, String name) throws EdmException
    {
        ObjectPermission permission;
        if (clazz == Document.class)
        {
            permission = new DocumentPermission(id);
        }
        else if (clazz == Folder.class)
        {
            permission = new FolderPermission(id);
        }
        else
        {
            throw new EdmException("Nieznany rodzaj uprawnienia dla klasy "+clazz);
        }

        permission.setSubject(subject);
        permission.setSubjectType(subjectType);
        permission.setName(name);
        permission.setNegative(false);

        try
        {
            DSApi.context().session().save(permission);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void revoke() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public ObjectPermission cloneObject()
    {
        try
        {
            return (ObjectPermission) super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            throw new Error(e.getMessage(), e);
        }
    }


    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getSubjectType()
    {
        return subjectType;
    }

    public void setSubjectType(String subjectType)
    {
        this.subjectType = subjectType;
    }

    public String getSubjectAndSubjectType(){
        return subject + '/' + subjectType;
    }
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Deprecated
    public boolean isNegative()
    {
        return negative;
    }

    @Deprecated
    public void setNegative(boolean negative)
    {
        this.negative = negative;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ObjectPermission)) return false;

        final ObjectPermission objectPermission = (ObjectPermission) o;

        if (negative != objectPermission.negative) return false;
        if (name != null ? !name.equals(objectPermission.name) : objectPermission.name != null) return false;
        if (subject != null ? !subject.equals(objectPermission.subject) : objectPermission.subject != null) return false;
        if (subjectType != null ? !subjectType.equals(objectPermission.subjectType) : objectPermission.subjectType != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (subject != null ? subject.hashCode() : 0);
        result = 29 * result + (subjectType != null ? subjectType.hashCode() : 0);
        result = 29 * result + (name != null ? name.hashCode() : 0);
        result = 29 * result + (negative ? 1 : 0);
        return result;
    }
}
