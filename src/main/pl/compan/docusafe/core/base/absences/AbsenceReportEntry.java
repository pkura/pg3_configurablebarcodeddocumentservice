package pl.compan.docusafe.core.base.absences;

import java.io.Serializable;
import java.util.Date;

/**
 * Obiekty tej klasy s� wykorzystywane podczas generowania raportu wykorzystanego
 * urlopu w wybranym roku kalendarzowym
 * 
 * @see AbsenceFactory
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class AbsenceReportEntry implements Serializable 
{
	/**
	 * Serial Version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Liczba porz�dkowa
	 */
	private int lp;
	
	/**
	 * Identyfikator wpisu urlopu
	 */
	private Long id;
	
	/**
	 * Identyfikator karty pracownika
	 */
	private Long empCardId;
	
	/**
	 * Numer ewidencyjny karty pracownika
	 */
	private String KPX;
	
	/**
	 * Nazwisko i imi� pracownika
	 */
	private String name;
	
	/**
	 * login pracownika
	 */
	private String login;
	
	/**
	 * Liczba dni wymiaru urlopu nale�nego
	 */
	private int dueDays;
	
	/**
	 * Liczba dni wymiaru urlopu zaleg�ego
	 */
	private int overdueDays;
	
	/**
	 * Liczba dni wymiaru urlopu dodatkowego
	 */
	private int additionalDays;
	
	/**
	 * Liczba dni urlopu wykorzystanego
	 */
	private int executedDays;
	
	/**
	 * Dodatkowe uwagi
	 */
	private String info;
	
	/**
	 * Nazwa firmy
	 */
	private String companyName;
	
	/**
	 * Nazwa dzia�u
	 */
	private String divisionName;
	
	/**
	 * Data, od kt�rej trwa zatrudnienie pracownika
	 */
	private Date employmentStartDate;
	
	/**
	 * Data zwolnienia pracownika
	 */
	private Date employmentEndDate;
	
	/**
	 * Domy�lny konstruktor
	 */
	public AbsenceReportEntry() 
	{
	}
	
	/**
	 * Konstruktor ustawiaj�cy wszystkie pola
	 * 
	 * @param lp
	 * @param kPX
	 * @param name
	 * @param dueDays
	 * @param overdueDays
	 * @param additionalDays
	 * @param executedDays
	 */
	public AbsenceReportEntry(int lp, Long id, String kPX, String name, int dueDays,
			int overdueDays, int additionalDays, int executedDays) 
	{
		super();
		this.lp = lp;
		this.id = id;
		KPX = kPX;
		this.name = name;
		this.dueDays = dueDays;
		this.overdueDays = overdueDays;
		this.additionalDays = additionalDays;
		this.executedDays = executedDays;
	}

	public int getLp() 
	{
		return lp;
	}

	public void setLp(int lp) 
	{
		this.lp = lp;
	}

	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public String getKPX() 
	{
		return KPX;
	}

	public void setKPX(String kPX) 
	{
		KPX = kPX;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public int getDueDays() 
	{
		return dueDays;
	}

	public void setDueDays(int dueDays) 
	{
		this.dueDays = dueDays;
	}

	public int getOverdueDays() 
	{
		return overdueDays;
	}

	public void setOverdueDays(int overdueDays) 
	{
		this.overdueDays = overdueDays;
	}

	public int getAdditionalDays() 
	{
		return additionalDays;
	}

	public void setAdditionalDays(int additionalDays) 
	{
		this.additionalDays = additionalDays;
	}

	public int getExecutedDays() 
	{
		return executedDays;
	}

	public void setExecutedDays(int executedDays) 
	{
		this.executedDays = executedDays;
	}
	
	public int getRestOfDays()
	{
		return dueDays + overdueDays - executedDays;
	}

	public void setEmpCardId(Long empCardId) 
	{
		this.empCardId = empCardId;
	}

	public Long getEmpCardId() 
	{
		return empCardId;
	}

	public String getInfo() 
	{
		return info;
	}

	public void setInfo(String info) 
	{
		this.info = info;
	}

	public String getCompanyName() 
	{
		return companyName;
	}

	public void setCompanyName(String companyName) 
	{
		this.companyName = companyName;
	}

	public String getDivisionName() 
	{
		return divisionName;
	}

	public void setDivisionName(String divisionName) 
	{
		this.divisionName = divisionName;
	}
	
	public String getLogin() 
	{
		return login;
	}
	
	public void setLogin(String login) 
	{
		this.login = login;
	}

	public Date getEmploymentStartDate() {
		return employmentStartDate;
	}

	public void setEmploymentStartDate(Date employmentStartDate) {
		this.employmentStartDate = employmentStartDate;
	}

	public Date getEmploymentEndDate() {
		return employmentEndDate;
	}

	public void setEmploymentEndDate(Date employmentEndDate) {
		this.employmentEndDate = employmentEndDate;
	}
}
