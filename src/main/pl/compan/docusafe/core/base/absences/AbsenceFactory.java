
package pl.compan.docusafe.core.base.absences;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.commons.codec.Decoder;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.LoadQueryInfluencers;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.impl.CriteriaImpl;
import org.hibernate.impl.SessionImpl;
import org.hibernate.loader.OuterJoinLoader;
import org.hibernate.loader.criteria.CriteriaLoader;
import org.hibernate.persister.entity.OuterJoinLoadable;
import org.jfree.util.Log;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.absences.CardServiceStub;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.CardPdfRequest;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.CardPdfResponse;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.GetPdfCard;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.GetPdfCardResponse;

import com.lowagie.text.Cell;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Klasa fabryki, generuje raporty oraz listy urlop�w pracownik�w
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class AbsenceFactory 
{
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AbsenceFactory.class);
	
	/**
	 * Kod wymiaru urlopu nale�nego
	 */
	private static final String DUEDAYS = "WUN";
	
	/**
	 * Kod wymiaru urlopu zaleg�ego
	 */
	private static final String OVERDUEDAYS = "WUZ";
	
	/**
	 * Flaga dni urlopu dodatkowego
	 */
	private static final int ADDITIONAL_DAYS = 1;
	
	/**
	 * Flaga dni urlopu zaleg�ego
	 */
	private static final int EXECUTED_DAYS = 0;
	
	/**
	 * Zapytanie HQL, kt�re wykorzystywane jest do wygenerowania raportu
	 * wymiar�w urlopowych wszystkich lub wybranych pracownik�w
	*/
	private static final String REPORT_FROM_YEAR_HQL = 
		    //"select emc "
		    "from " + EmployeeCard.class.getName() + " emc join fetch emc.absences abs "
		  + "where abs.year = :YEAR "
		  + ":SEARCH_QUERY "
		  + "order by emc.lastName asc, emc.firstName asc";
	
	/**
	 * Zapytanie HQL zwracaj�ce ilo�� wynik�w 
	 */
	private static final String COUNT_REPORT_RESULTS_HQL =
			"select count(emc.id) "
		  + "from " + EmployeeCard.class.getName() + " emc "
		  + "where emc.id in (select emc2.id from " + Absence.class.getName() + " abs join abs.employeeCard emc2 where abs.year = :YEAR)"
		  + ":SEARCH_QUERY ";
	
	/**
	 * Zapytanie HQL zwracaj�ce wszystkie wpisy urlopowe danego pracownika w podanym roku
	 */
	private static final String EMPLOYEE_ABSENCES_FROM_YEAR_HQL =
		"from " + Absence.class.getName() + " abs where abs.employeeCard = ? and abs.year = ? order by abs.startDate asc";
	
	/**
	 * Generuje raport wymiar�w urlopowych dla wszystkich pracownik�w w podanym 
	 * roku kalendarzowym.
	 * 
	 * @param year
	 * @return
	 */
	public static List<AbsenceReportEntry> generateReportFromYear(int year)
		throws EdmException
	{
		return generateReportFromYear(year, null);
	}
	
	/**
	 * Generuje raport wymiar�w urlopowych dla wszystkich pracownik�w w podanym 
	 * roku kalendarzowym.
	 * 
	 * @param year
	 * @param pageNumber
	 * @param maxResults
	 * @return
	 */
	public static List<AbsenceReportEntry> generateReportFromYear(int year, int pageNumber, int maxResults)
		throws EdmException
	{
		return generateReportFromYear(year, null, pageNumber, maxResults);
	}
	
	/**
	 * Generuje raport wymiar�w urlopowych dla wybranych pracownik�w w
	 * podanym roku. Obiekt employeeCard ustala pola po jakich ma by� 
	 * przeprowadzone wyszukiwanie.
	 * 
	 * @param year
	 * @param employeeCard
	 * @return
	 * @throws EdmException
	 */
	public static List<AbsenceReportEntry> generateReportFromYear(int year, EmployeeCard employeeCard)
		throws EdmException
	{
		// utworzenie mapy z parametrami wyszukiwania

		Map<String, Object[]> queryParams = buildSearchQueryMapByEmployeeCard(employeeCard);

		Criteria criteria = DSApi.context().session().createCriteria(EmployeeCard.class).setFetchMode("absences", FetchMode.JOIN)
				.createAlias("absences", "abs");

		if( year < 1000)
			year = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		System.out.println("Y "+year);
		criteria.add(Restrictions.eq("abs.year", year));
		if(employeeCard != null && employeeCard.getCompany() != null)
		{
			criteria.add(Restrictions.like("company", employeeCard.getCompany() + "%").ignoreCase());
			System.out.println("company "+employeeCard.getCompany()+ "%");
		}
		if(employeeCard != null && employeeCard.getDepartment() != null)
		{
			criteria.add(Restrictions.like("department", employeeCard.getDepartment() + "%").ignoreCase());
			System.out.println("department "+employeeCard.getDepartment()+ "%");
		}
		if(employeeCard != null && employeeCard.getUser()!= null && employeeCard.getUser().getLastname() != null)
		{
			criteria.add(Restrictions.or(Restrictions.like("lastName", employeeCard.getUser().getLastname()).ignoreCase(),
						Restrictions.like("externalUser", "%" + employeeCard.getUser().getLastname()).ignoreCase()));
			System.out.println("lastName "+employeeCard.getUser().getLastname()+ "%");
			System.out.println("externalUser "+employeeCard.getUser().getLastname()+ "%");
		}
		if(employeeCard != null && employeeCard.getPosition() != null)	
		{
			criteria.add(Restrictions.like("position", employeeCard.getPosition() + "%").ignoreCase());
			System.out.println("position "+employeeCard.getPosition() + "%");
		}
		if(employeeCard != null && employeeCard.getKPX() != null)
		{
			criteria.add(Restrictions.like("KPX", employeeCard.getKPX() + "%"));
			System.out.println("KPX "+employeeCard.getKPX() + "%");
		}
		if( employeeCard != null && employeeCard.getId() != null)
		{
			criteria.add(Restrictions.eq("id", employeeCard.getId()));
			System.out.println("id "+employeeCard.getId());
		}
		
		if(employeeCard != null && employeeCard.getUser() != null && employeeCard.getUser().getFirstname() != null)
		{
			criteria.add(Restrictions.like("firstName", employeeCard.getUser().getFirstname() + "%").ignoreCase());
			System.out.println("firstName "+employeeCard.getUser().getFirstname() + "%");
		}
		
		criteria.addOrder(Order.asc("lastName")).addOrder(Order.asc("firstName"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		try {
			System.out.println("SQL");
			System.out.println(GenerateSQL(criteria));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		@SuppressWarnings("unchecked")
//		utworzenie raportu wynikowego
		System.out.println("SQL");
		List<EmployeeCard> rows = createQueryObject(queryParams, year).setFirstResult(0).setMaxResults(Integer.MAX_VALUE).list();
		System.out.println("SQL");
		//tak nie mo�e by� bo nie dzia�a raport, olewa wyszukanie i bierze wszystkie abs poniewa� �aduje ca�y obiejt emp
//		List<EmployeeCard> rows = criteria.list();
		
		return buildAbsenceReportEntryList(rows, 0);
	}
	
	public static String GenerateSQL(Criteria criteria) throws Exception
    {
		org.hibernate.impl.CriteriaImpl c =  (org.hibernate.impl.CriteriaImpl) criteria;
		  SessionImpl s = (SessionImpl) c.getSession();
		  SessionFactoryImplementor factory = (SessionFactoryImplementor) s.getSessionFactory();
		  String[] implementors = factory.getImplementors(c.getEntityOrClassName());
		  LoadQueryInfluencers lqis = new LoadQueryInfluencers();
		  CriteriaLoader loader = new CriteriaLoader((OuterJoinLoadable) factory.getEntityPersister(implementors[0]), factory, c, implementors[0], lqis);
		  java.lang.reflect.Field f = OuterJoinLoader.class.getDeclaredField("sql");
		  f.setAccessible(true);
		  String sql = (String) f.get(loader);
		  return sql;
    }
	
	/**
	 * Generowanie raportu urlopowego dla u�ytkownik�w o podanym zbiorze login�w
	 * 
	 * @param year
	 * @param usernames
	 * @return
	 * @throws EdmException
	 */
	public static List<AbsenceReportEntry> generateReportFromYearByUsers(int year, Set<String> usernames) throws EdmException
	{
		// parametry zapytania
		Map<String, Object[]> queryParams = new HashMap<String, Object[]>();
		if (usernames != null && usernames.size() > 0)
			queryParams.put("emc.externalUser IN (:usernames)", new Object[] {"usernames", usernames});
			
		@SuppressWarnings("unchecked")
		List<EmployeeCard> rows = createQueryObject(queryParams, year).setFirstResult(0).setMaxResults(Integer.MAX_VALUE).list();
		return buildAbsenceReportEntryList(rows, 0);
	}
	
	/**
	 * Zwr�cenie reszty aktywnych kart pracowniczych na podstawie przekazanej karty
	 * 
	 * @param card
	 * @return
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	private static List<EmployeeCard> getActiveEmployeeCards(EmployeeCard card) throws EdmException
	{
		return DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " e where e.employmentEndDate is null and e.id != ? and e.user = ?")
				//.setString(0, empCard.getKPX())
				.setLong(0, card.getId())
				.setParameter(1, card.getUser())
			.list();
	}
	
	/**
	 * Za�adowanie pdf'a karty urlopowej z webservice
	 * 
	 * @param userName
	 * @param year
	 * @return
	 * @throws EdmException
	 */
	private static File loadPdfCardFromWS(String userName, int year) throws EdmException
	{
		FileOutputStream outputStream = null;
		ByteArrayInputStream inputStream = null;
		try
		{
			CardServiceStub cardServiceStub = new CardServiceStub();
			cardServiceStub._getServiceClient().getOptions().setTo(
					new EndpointReference(Docusafe.getAdditionProperty("cardService.endPointReference")));
			
			// parametry requesta
			CardPdfRequest request = new CardPdfRequest();
			request.setUserName(userName);
			request.setYear(year);
			
			// ustawienie
			GetPdfCard pdfCard = new GetPdfCard();
			pdfCard.setRequest(request);
			
			// wywolanie i odpowiedz
			GetPdfCardResponse cardResponse = cardServiceStub.getPdfCard(pdfCard);
			CardPdfResponse response = cardResponse.get_return();
			
			byte[] bytes = Base64.decodeBase64(response.getCardArray());
			
			File file = File.createTempFile("docusafe_", "_tmp");
			outputStream = new FileOutputStream(file);
			inputStream = new ByteArrayInputStream(bytes);
			
			byte[] buf = new byte[1024];
            while ((inputStream.read(buf)) > 0)
            {
                outputStream.write(buf);
            }
            
            outputStream.close();
            inputStream.close();
            
            return file;
		}
		catch (Exception ex)
		{
			LOG.error(ex.getMessage(), ex);
			throw new EdmException(ex.getMessage());
		}
		finally
		{
			try
			{
				if (outputStream != null)
					outputStream.close();
				if (inputStream != null)
					inputStream.close();
			} 
			catch (IOException ex){}
		}
	}
	
	/**
	 * Generuje karte pracownika do pdf
	 * 
	 * @param userName
	 * @param year
	 * @return
	 * @throws EdmException
	 */
	public static File pdfCard(String userName, int year) throws EdmException
	{
		System.out.println("Mariusz");
		File pdfFile = null;
		EmployeeCard card = null;
		try
		{
			if (AvailabilityManager.isAvailable("aegon.docusafe.remote"))
			{
				return loadPdfCardFromWS(userName, year);
			}
			
			card = getActiveEmployeeCardByUserName(userName);
			AbsenceReportEntry entry = generateReportFromYear(year, card).get(0);
			
			
			
			File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            
            Font fontSuperSmall = new Font(baseFont, (float) 6);
            Font fontSmall = new Font(baseFont, (float) 8);
            Font fontNormal = new Font(baseFont, (float) 12);
            Font fontLarge = new Font(baseFont, (float) 16);            
            
            pdfFile = File.createTempFile("docusafe_", "_tmp");
            pdfFile.deleteOnExit();
            com.lowagie.text.Document pdfDoc = new com.lowagie.text.Document(PageSize.A4);
            PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));
            
            pdfDoc.open();
            
            Table topTable = new Table(2);
            topTable.setCellsFitPage(true);
            topTable.setWidth(100);
            topTable.setPadding(3);
            topTable.setBorder(0);
            
            Paragraph title = new Paragraph();
            title.add(new Phrase("Wniosek urlopowy",fontLarge));
            title.add(new Phrase(" / Vacation Request Form",fontSmall)); 
            
            Cell titleCell = new Cell(title);
            titleCell.setBorder(0);
            topTable.addCell(titleCell);
            // logo
            Image logo;
			try {
				logo = Image.getInstance(Docusafe.getHome().getAbsolutePath() + "/img/"+Docusafe.getAdditionProperty("pdfCardGif"));
				Cell logoCell = new Cell(logo);
				logoCell.setBorder(0);
				topTable.addCell(logoCell);
			} catch (FileNotFoundException e) {
				Log.error(e.getMessage(), e);
			}
            
            //pdfDoc.add(title);
            pdfDoc.add(topTable);
            //pdfDoc.add(Chunk.NEWLINE);
//            if (card.getUser() != null)
//            	pdfDoc.add(new Paragraph("Nazwisko i imi� : "+card.getUser().asLastnameFirstname(),fontNormal));
//            else
//            {
//            	pdfDoc.add(new Paragraph("Nazwisko i imi� : "+card.getExternalUser(),fontNormal));
//            }
            pdfDoc.add(new Paragraph("Nazwisko i imi� : "+card.getLastName() + " " + card.getFirstName(),fontNormal));
            
            StringBuffer kpxs = new StringBuffer(card.getKPX());
            StringBuffer nazwas = new StringBuffer(card.getCompany());
            for(EmployeeCard othercard : getActiveEmployeeCards(card))
            {
            	kpxs.append(" , ");
            	kpxs.append(othercard.getKPX());
            	nazwas.append(" , ");
            	nazwas.append(othercard.getCompany());
            }
            pdfDoc.add(new Paragraph("Numer pracownika : "+kpxs.toString(),fontNormal));
            pdfDoc.add(new Paragraph("Nazwa sp�ki : "+nazwas.toString(),fontNormal));
            pdfDoc.add(new Paragraph("Departament : "+card.getDepartment(),fontNormal));
            pdfDoc.add(new Paragraph("Dzia� : "+card.getDivision(),fontNormal));
            //pdfDoc.add(Chunk.NEWLINE);  
            
            Table table = new Table(2);
            table.setCellsFitPage(true);
            table.setWidth(100);
            table.setPadding(3);
            table.setBorder(0);
            
            List<String> texts = new ArrayList<String>();
            texts.add("Nale�ny urlop : "+entry.getDueDays());
        	texts.add("Pozosta�o z poprzedniego roku : "+entry.getOverdueDays());
        	
        	if (card.getTINW() != null && card.getTINW())
        	{
        		entry.setOverdueDays(entry.getOverdueDays() + card.getInvalidAbsenceDaysNum());
        		texts.add("Urlop inwalidzki : "+card.getInvalidAbsenceDaysNum());
        	}
            if (card.getT188KP() != null && card.getT188KP())
            {		
            	texts.add("Urlop nale�ny 188 Kp : " + card.getT188kpDaysNum());
            }
            
            texts.add("��cznie : "+(entry.getDueDays()+entry.getOverdueDays()));
            texts.add("Rok : "+year);
            
            for(String text : texts)
            {
            	Cell cell = new Cell(new Phrase(text,fontNormal));
            	cell.setBorder(0);
            	table.addCell(cell);
            }
            
            //table.addCell(new Phrase("Nale�ny urop : "+entry.getDueDays(),fontNormal));
            //table.addCell(new Phrase("Pozosta�o z poprzedniego roku : "+entry.getOverdueDays(),fontNormal));
            //table.addCell(new Phrase("��cznie : "+(entry.getDueDays()+entry.getOverdueDays()),fontNormal));
            //table.addCell(new Phrase("Rok : "+year,fontNormal));
            
            pdfDoc.add(table);
            
            int[] widths = new int[] { 2, 1 , 2, 2, 2, 1, 3};
            table = new Table(7);
            table.setCellsFitPage(true);
            table.setWidth(100);
            table.setWidths(widths);
            table.setPadding(3);
            
            
            String[] headers = new String[] {"Data akceptacji wniosku","Liczba dni","Od","Do","Typ urlopu","Pozosta�o dni","Podpis prze�o�onego"};
            
            for(String text : headers)
            {
            	Cell cell = new Cell(new Phrase(text,fontSmall));
            	cell.setHorizontalAlignment(Cell.ALIGN_CENTER);            	
            	cell.setVerticalAlignment(Cell.ALIGN_CENTER);
            	table.addCell(cell);
            }
            
            for(EmployeeAbsenceEntry absEntry : AbsenceFactory.getEmployeeAbsences(card, year))
            {
            	if(String.valueOf("WUZ").equalsIgnoreCase(absEntry.getAbsence().getAbsenceType().getCode())
            		|| String.valueOf("WUN").equalsIgnoreCase(absEntry.getAbsence().getAbsenceType().getCode()))
            	{
            		continue;
            	}
            	
            	table.addCell(new Phrase(DateUtils.formatJsDate(absEntry.getAbsence().getCtime()),fontSmall));
            	table.addCell(new Phrase(String.valueOf(absEntry.getAbsence().getDaysNum()),fontSmall));
            	table.addCell(new Phrase(DateUtils.formatJsDate(absEntry.getAbsence().getStartDate()),fontSmall));
            	table.addCell(new Phrase(DateUtils.formatJsDate(absEntry.getAbsence().getEndDate()),fontSmall));
            	table.addCell(new Phrase(String.valueOf(absEntry.getAbsence().getAbsenceType().getCode()),fontSmall));
            	table.addCell(new Phrase(String.valueOf(absEntry.getRestOfDays()),fontSmall));
            	//table.addCell(new Phrase(" ",fontSmall));
            	if (absEntry.getAbsence().getEditor() != null)
            		table.addCell(new Phrase(absEntry.getAbsence().getEditor().asLastnameFirstname(), fontSmall));
            	else
            		table.addCell(new Phrase((StringUtils.isNotEmpty(absEntry.getAbsence().getEditorFullName()) 
            				? absEntry.getAbsence().getEditorFullName() : ""), fontSmall));
            }
            
            pdfDoc.add(table);
            
            table = new Table(3);
            table.setCellsFitPage(true);
            table.setWidth(100);
            table.setPadding(1);
            table.setBorder(0);
            
            for(AbsenceType type : Finder.list(AbsenceType.class, "code"))
            {
            	Cell cell = new Cell(new Phrase(type.getName()+" = "+type.getCode(),fontSuperSmall));
            	cell.setBorder(0);
            	table.addCell(cell);
            }
            
            pdfDoc.add(table);
            pdfDoc.close();
		}
		catch (Exception e) 
		{
			LoggerFactory.getLogger("kamilj").debug(e.getMessage(), e);
			throw new EdmException(e);
		}
		
		return pdfFile;
	}
	
	/**
	 * Generuje raport wymiar�w urlopowych dla wybranych pracownik�w w
	 * podanym roku. Obiekt employeeCard ustala pola po jakich ma by� 
	 * przeprowadzone wyszukiwanie. 
	 * 
	 * @param year
	 * @param employeeCard
	 * @param pageNumber Numer strony
	 * @param maxResults Liczba rezultat�w
	 * @return
	 * @throws EdmException
	 */
	public static List<AbsenceReportEntry> generateReportFromYear(int year, EmployeeCard employeeCard, int pageNumber, int maxResults)
		throws EdmException
	{
		// utworzenie mapy z parametrami wyszukiwania
		Map<String, Object[]> queryParams = buildSearchQueryMapByEmployeeCard(employeeCard);
	
		@SuppressWarnings("unchecked")
		// utworzenie raportu wynikowego
		List<EmployeeCard> rows = createQueryObject(queryParams, year).setFirstResult(pageNumber).setMaxResults(maxResults).list();
		return buildAbsenceReportEntryList(rows, pageNumber);
	}
	
	/**
	 * Metoda zwraca list� urlop�w pracownika w danym roku kalendarzowym
	 * 
	 * @param empCard
	 * @return
	 * @throws EdmException
	 */
	public static List<EmployeeAbsenceEntry> getEmployeeAbsences(EmployeeCard empCard, int year)
		throws EdmException
	{
		@SuppressWarnings("unchecked")
		// pobranie listy urlop�w danego pracownika w podanym roku
//		List<Absence> absences = DSApi.context().session().createQuery(EMPLOYEE_ABSENCES_FROM_YEAR_HQL)
//			.setParameter(0, empCard)
//			.setInteger(1, year)
//			.list();
		
		
		Criteria criteria = DSApi.context().session().createCriteria(Absence.class.getName());
		criteria.add(Restrictions.eq("employeeCard", empCard));
		criteria.add(Restrictions.eq("year", year));
		List<Absence> absences = criteria.list();
		
		
		
		int sumDays = 0;
		int i = 1; // liczba porzadkowa
		
		// utworzenie listy urlop�w z liczb� porz�dkow� i obliczeniami pozosta�ych dni urlopu
		LinkedList<EmployeeAbsenceEntry> absenceEntries = new LinkedList<EmployeeAbsenceEntry>();
		
		// posortowanie wpisow
		for (Absence absence : absences)
		{
			EmployeeAbsenceEntry absenceEntry = new EmployeeAbsenceEntry();
			absenceEntry.setAbsence(absence);
			
			if(absence.getAbsenceType() != null)
			{	
				if (absence.getAbsenceType().getCode().equalsIgnoreCase("WUN") && 
						absenceEntries.size() > 0 && absenceEntries.getFirst().getAbsence().getAbsenceType().getCode().equalsIgnoreCase("WUN"))
					absenceEntries.add(1, absenceEntry);
				else if (absenceEntry.getAbsence().getAbsenceType().getCode().equalsIgnoreCase("WUN"))
					absenceEntries.addFirst(absenceEntry);
				else if (absenceEntry.getAbsence().getAbsenceType().getCode().equalsIgnoreCase("WUZ") && absenceEntries.size() > 0)
					absenceEntries.add(1, absenceEntry);
				else
					absenceEntries.add(absenceEntry);
			}
		}
		
		// wyliczenie dni
		for (EmployeeAbsenceEntry entry : absenceEntries)
		{
			entry.setLp(i++);
			
			if (entry.getAbsence().getAbsenceType().isAdditional())
			{
				sumDays += entry.getAbsence().getDaysNum();
			}
			else if (entry.getAbsence().getAbsenceType().isNegative())
			{
				sumDays -= entry.getAbsence().getDaysNum();
			}
			entry.setRestOfDays(sumDays);
		}
		
		// dodatkowe urlopy wyznaczone w karcie pracownika
		if (empCard.getTINW() != null && empCard.getTINW())
		{
			// dodatkowy urlop inwalidzki
			EmployeeAbsenceEntry absenceEntry = new EmployeeAbsenceEntry();
			absenceEntry.setAbsence(createInvalidAbsenceObject(year, empCard));
			absenceEntry.setLp(i++);
			
			sumDays += empCard.getInvalidAbsenceDaysNum();
			absenceEntry.setRestOfDays(sumDays);
			absenceEntries.add(absenceEntry);
		}
		
		return absenceEntries;
	}
	
	/**
	 * Tworzy obiekt urlopu nale�nego inwalidzkiego
	 * 
	 * @param year
	 * @param empCard
	 * @return
	 */
	public static Absence createInvalidAbsenceObject(Integer year, EmployeeCard empCard)
	{
		Absence absence = new Absence();
		absence.setAbsenceType(new AbsenceType("INW", "Urlop nale�ny Inwalidzki", "", 2));
		
		Calendar start = Calendar.getInstance();
		start.set(year, 0, 1);
		Calendar end = Calendar.getInstance();
		end.set(year, 11, 31);
		absence.setStartDate(start.getTime());
		absence.setEndDate(end.getTime());
		
		absence.setDaysNum(empCard.getInvalidAbsenceDaysNum());
		absence.setInfo("Nadany urlop nale�ny inwalidzki");
		
		return absence;
	}
	
	/**
	 * Zwraca liczb� rezultat�w wg podanych parametr�w wyszukiwania
	 * 
	 * @param year
	 * @param employeeCard
	 * @return
	 */
	public static int getCountReportResults(int year, EmployeeCard employeeCard) throws EdmException
	{
		// utworzenie mapy z parametrami wyszukiwania
		Map<String, Object[]> queryParams = buildSearchQueryMapByEmployeeCard(employeeCard);
		
		Query query = DSApi.context().session().createQuery(
								COUNT_REPORT_RESULTS_HQL.replaceFirst(":SEARCH_QUERY", createWhereClause(queryParams)))
								.setInteger("YEAR", year);
		
		// ustawienie dodatkowych parametr�w zapytania z warto�ci mapy
		if (queryParams != null)
			for (Object[] param : queryParams.values())
				query.setParameter((String) param[0], param[1]);
		
		Long count = (Long) query.uniqueResult();
		return Integer.parseInt(count + "");
	}
	
	/**
	 * Nalicza urlopy na bie��cy rok na podstawie roku poprzedniego
	 * 
	 * @throws EdmException
	 */
	public static void chargeAbsencesFromPreviousYear() throws EdmException
	{
		int currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		int previousYear = (Integer) DSApi.context().session().createQuery(
				"select a.year from " + Absence.class.getName() + " a group by a.year order by a.year desc")
				.setMaxResults(1)
				.uniqueResult();
		
		if (previousYear >= currentYear)
			return;
		
		// wprowadzenie korekt w urlopach
		correctAbsencesFromYear(previousYear);
		
		// wygenerowanie raportu z poprzedniego roku
		List<AbsenceReportEntry> reportEntries = generateReportFromYear(previousYear);
		
		final int BATCH_SIZE = 20;
		int i = 0;
		final int DUE_DAYS = 26;
		AbsenceType dueAbsenceType = Finder.find(AbsenceType.class, "WUN");
		AbsenceType overdueAbsenceType = Finder.find(AbsenceType.class, "WUZ");
		// naliczenie urlop�w na rok bie��cy
		for (AbsenceReportEntry reportEntry : reportEntries)
		{
			EmployeeCard employeeCard = Finder.find(EmployeeCard.class, reportEntry.getEmpCardId());
			
			// urlop nale�ny
			Absence dueAbsence = new Absence();
			dueAbsence.setAbsenceType(dueAbsenceType);
			dueAbsence.setCtime(new Date());
			dueAbsence.setDaysNum(DUE_DAYS);
			dueAbsence.setStartDate(
					new GregorianCalendar(currentYear, GregorianCalendar.JANUARY, 1).getTime());
			dueAbsence.setEndDate(
					new GregorianCalendar(currentYear, GregorianCalendar.DECEMBER, 31).getTime());
			dueAbsence.setEmployeeCard(employeeCard);
			dueAbsence.setYear(currentYear);
			
			// urlop zaleg�y
			Absence overdueAbsence = new Absence();
			overdueAbsence.setAbsenceType(overdueAbsenceType);
			overdueAbsence.setCtime(new Date());
			overdueAbsence.setDaysNum(reportEntry.getRestOfDays());
			overdueAbsence.setStartDate(
					new GregorianCalendar(currentYear, GregorianCalendar.JANUARY, 1).getTime());
			overdueAbsence.setEndDate(
					new GregorianCalendar(currentYear, GregorianCalendar.DECEMBER, 31).getTime());
			overdueAbsence.setEmployeeCard(employeeCard);
			overdueAbsence.setYear(currentYear);
			
			employeeCard.getAbsences().add(dueAbsence);
			employeeCard.getAbsences().add(overdueAbsence);
			
			if (AvailabilityManager.isAvailable("employeeCard.hasSpecialAbsences"))
				clearSpecialAbsencesFromEmployeeCard(employeeCard);
			
			DSApi.context().session().update(employeeCard);
			DSApi.context().session().save(dueAbsence);
			DSApi.context().session().save(overdueAbsence);
			
			i++;
			if (BATCH_SIZE == i)
			{
				// flush
				DSApi.context().session().flush();
				i = 0;
			}
		}
		
		// wyszyszczenie sesji hibernate'a
		DSApi.context().session().clear();
	}
	
	/**
	 * Przy naliczeniu urlopow na nowy rok czysci karty pracownikow z dodatkowych mozliwych urlopow.
	 * Dotyczy tylko 188 KP i UJ.
	 * 
	 * @param employeeCard
	 */
	private static void clearSpecialAbsencesFromEmployeeCard(EmployeeCard employeeCard)
	{
		employeeCard.setT188KP(Boolean.FALSE);
		employeeCard.setT188kpDaysNum(0);
		
		employeeCard.setTUJ(Boolean.FALSE);
		employeeCard.setTujDaysNum(0);
		
		//employeeCard.setTINW(Boolean.FALSE);
		//employeeCard.setInvalidAbsenceDaysNum(0);
	}
	
	/**
	 * Naprawia wpisy urlopowe, kt�rych wnioski zosta�y z�o�one pod koniec roku, a data zako�czenia urlopu
	 * przechodzi na rok nast�pny.
	 * 
	 * @param year
	 * @throws EdmException
	 */
	public static void correctAbsencesFromYear(Integer year) throws EdmException
	{
		for (EmployeeCard empCard : getAllActiveEmployeeCards())
		{
			correctEmployeeCardAbsencesFromYear(year, empCard);
		}
		
		DSApi.context().session().flush();
	}
	
	/**
	 * Naprawia wpisy urlopowe dla podanej karty pracownika, kt�rych wnioski zosta�y z�o�one 
	 * pod koniec roku, a data zako�czenia urlopu
	 * przechodzi na rok nast�pny.
	 * 
	 * @param year
	 * @param empCard
	 * @throws EdmException
	 */
	private static void correctEmployeeCardAbsencesFromYear(Integer year, EmployeeCard empCard) 
		throws EdmException
	{
		Calendar newYear = Calendar.getInstance();
		newYear.set(year + 1, Calendar.JANUARY, 1);
		
		for (Absence absence : getEmployeeAbsencesToCorrect(empCard, year))
		{	
			if (absence.getStartDate().before(newYear.getTime()))
			{
				// przypadek je�li data pocz�tkowa urlopu jest przed 01.01
				Absence copy = copyAbsence(absence, empCard);
				copy.setYear(year + 1);
				
				copy.setStartDate(newYear.getTime());
				
				List<FreeDay> freeDays = getFreeeDaysFromYear(year);
				Calendar tmpDate = (Calendar) newYear.clone();
				
				int countFreeDays = 0;
				while (!tmpDate.getTime().after(copy.getEndDate()))
				{
					// sprawdzenie czy jest to dzien wolny
					int dayOfWeek = tmpDate.get(Calendar.DAY_OF_WEEK);
					if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY)
						++countFreeDays;
					else
						for (FreeDay day : freeDays)
							if (day.getDate().equals(tmpDate.getTime()))
								++countFreeDays;
						
					tmpDate.set(Calendar.DATE, tmpDate.get(Calendar.DATE) + 1);
				}
				
				Long days = (copy.getEndDate().getTime() - newYear.getTime().getTime()) / 1000 / 60 / 60 / 24 + 1;
				days -= countFreeDays;
				copy.setDaysNum(Integer.parseInt(days.toString()));
				
				// odpowiednia uwaga
				copy.setInfo("Wniosek o urlop z poprzedniego roku");
				
				// zapisanie urlopu
				empCard.getAbsences().add(copy);
				DSApi.context().session().update(empCard);
				DSApi.context().session().save(copy);
				
				// poprawka w dacie urlopu i ilosi dni z poprzedniego roku i update
				Calendar lastDayOfYear = Calendar.getInstance();
				lastDayOfYear.set(year, Calendar.DECEMBER, 31);
				absence.setEndDate(lastDayOfYear.getTime());
				absence.setDaysNum(absence.getDaysNum() - Integer.parseInt(days.toString()));
				DSApi.context().session().update(absence);
			}
			else
			{
				// je�li data pocz�tkowa urlopu jest r�wna lub wi�ksza od 01.01
				// przeniesienie wpisu na rok kolejny
				absence.setYear(year + 1);
				absence.setInfo("Wniosek o urlop z poprzedniego roku");
				DSApi.context().session().update(absence);
			}
		}
	}
	
	/**
	 * Zwraca list� urlop�w do kerekty z danej karty pracownika wg podanego roku.
	 * 
	 * @param empCard
	 * @param year
	 * @return
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	private static List<Absence> getEmployeeAbsencesToCorrect(EmployeeCard empCard, Integer year) throws EdmException
	{
		Calendar newYear = Calendar.getInstance();
		newYear.set(year + 1, Calendar.JANUARY, 1);
		return (List<Absence>) DSApi.context().session().createQuery("from " + Absence.class.getName() + " a " 
				+ " where a.year = ? and a.endDate >= ? and a.employeeCard = ?")
				.setInteger(0, year)
				.setParameter(1, newYear.getTime())
				.setParameter(2, empCard)
				.list();
	}
	
	/**
	 * Metoda buduj�ca map� z dodatkowymi parametrami wyszukiwania na podstawie obiektu employeeCard
	 * 
	 * @param employeeCard
	 * @return
	 */
	public static Map<String, Object[]> buildSearchQueryMapByEmployeeCard(EmployeeCard employeeCard)
	{
		if (employeeCard == null)
			return null;
		
		// Posta� mapy jest nast�puj�ca: 
		// String - cz�� zapytania HQL
		// Object[] - tablica z dwoma elementami: nazwa parametru i warto�� parametru
		Map<String, Object[]> queryParams = new HashMap<String, Object[]>();
		
		// Identyfikator
		if (employeeCard.getId() != null)
			queryParams.put("emc.id = :ID", 
					new Object[] {"ID", employeeCard.getId()});
		// Numer ewidencyjny
		if (!StringUtils.isEmpty(employeeCard.getKPX()))
			queryParams.put("emc.KPX like :KPX", 
					new Object[] {"KPX", employeeCard.getKPX() + "%"});
		
		if (employeeCard.getUser() != null)
		{
			// Nazwisko
			if (!StringUtils.isEmpty(employeeCard.getUser().getLastname()))
				queryParams.put("(lower(emc.lastName) like :LASTNAME or emc.externalUser like :LASTNAME)", 
						new Object[] {"LASTNAME", employeeCard.getUser().getLastname().toLowerCase() + "%"});
			// Imi�
			if (!StringUtils.isEmpty(employeeCard.getUser().getFirstname()))
				queryParams.put("lower(emc.firstName) like :FIRSTNAME", 
						new Object[] {"FIRSTNAME", employeeCard.getUser().getFirstname().toLowerCase() + "%"});
		}
		
		// Stanowisko
		if (!StringUtils.isEmpty(employeeCard.getPosition()))
			queryParams.put("lower(emc.position) like :POSITION", 
					new Object[] {"POSITION", "%" + employeeCard.getPosition().toLowerCase() + "%"});
		// Departament
		if (!StringUtils.isEmpty(employeeCard.getDepartment()))
			queryParams.put("lower(emc.department) like :DEPARTMENT", 
					new Object[] {"DEPARTMENT", "%" + employeeCard.getDepartment().toLowerCase() + "%"});
		// Sp�ka
		if (!StringUtils.isEmpty(employeeCard.getCompany()))
			queryParams.put("lower(emc.company) like :COMPANY", 
					new Object[] {"COMPANY", "%" + employeeCard.getCompany().toLowerCase() + "%"});
		
		return queryParams;
	}
	
	/**
	 * Tworzy obiekt query zapytania
	 * 
	 * @param queryParams
	 * @param year
	 * @return
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	private static Query createQueryObject(Map<String, Object[]> queryParams, int year)
		throws EdmException
	{
		Query query = DSApi.context().session().createQuery(
				REPORT_FROM_YEAR_HQL.replaceFirst(":SEARCH_QUERY", createWhereClause(queryParams)))
				.setInteger("YEAR", year);
		LOG.debug(" QUERY URLOPOWE=" + query.getQueryString());
		
		// ustawienie dodatkowych parametr�w zapytania z warto�ci mapy
		if (queryParams != null)
			for (Object[] param : queryParams.values())
				if ((param[1] instanceof Collection))
					query.setParameterList((String) param[0], (Collection) param[1]);
				else if (param[1].getClass().isArray())
					query.setParameterList((String) param[0], (Object[]) param[1]);
				else
					query.setParameter((String) param[0], param[1]);
		LOG.debug(" QUERY URLOPOWE=" + query.getQueryString());
		return query;
	}
	
	private static Criteria createCriteriaObject(Set<String> queryParams, int year) throws EdmException
	{
		Criteria c = DSApi.context().session().createCriteria(EmployeeCard.class);
		c.add(Restrictions.in("externalUser", queryParams));
		return c;
	}

	
	/**
	 * Metoda buduj�ca wynikowy raport wymiar�w urlopowych z podanej listy obiekt�w.
	 * 
	 * @param rows
	 * @return
	 */
	private static List<AbsenceReportEntry> buildAbsenceReportEntryList(List<EmployeeCard> rows, int pageNumber)
	{
		List<AbsenceReportEntry> absenceReportEntries = new ArrayList<AbsenceReportEntry>();
		int i = 1;
		if (pageNumber > 0) 
		{
			i += pageNumber;
		}
		for (EmployeeCard empCard : rows)
		{
			AbsenceReportEntry entry = new AbsenceReportEntry();
			entry.setLp(i++);
			entry.setEmpCardId(empCard.getId());
			entry.setLogin(empCard.getExternalUser());
			entry.setKPX(empCard.getKPX());
			
			// OBSLUGA Z WPISAMI W KOLUMNIE IMIE I NAZWISKO
			entry.setName(empCard.getLastName() + " " + empCard.getFirstName());
			
			entry.setCompanyName(empCard.getCompany());
			entry.setDivisionName(empCard.getDivision());
			
			int dueDays = 0; // suma dni nale�nych z bierz�cego roku
			int overdueDays = 0; // suma dni zaleg�ych
			int additionalDays = 0; // suma dni urlopu dodatkowego
			int executedDays = 0; // suma dni wykorzystanych
			
			for (Absence absence : empCard.getAbsences())
			{
//				System.out.println("absence.getYear() = "+absence.getYear());
				//to jest bez sensu a jak szukam wpis�w z innego roku ni� bierz�cy
//				if(absence.getYear() !=  GregorianCalendar.getInstance().get(GregorianCalendar.YEAR))
//					continue;
				if (absence.getAbsenceType().getCode().equalsIgnoreCase(DUEDAYS))
					dueDays += absence.getDaysNum();
				else if (absence.getAbsenceType().getCode().equalsIgnoreCase(OVERDUEDAYS))
					overdueDays += absence.getDaysNum();
				else if (absence.getAbsenceType().getFlag() == ADDITIONAL_DAYS)
					additionalDays += absence.getDaysNum();
				else if (absence.getAbsenceType().getFlag() == EXECUTED_DAYS)
					executedDays += absence.getDaysNum();
			}
			
			entry.setDueDays(dueDays);
			entry.setOverdueDays(overdueDays);
			entry.setAdditionalDays(additionalDays);
			entry.setExecutedDays(executedDays);
			entry.setEmploymentStartDate(empCard.getEmploymentStartDate());
			entry.setEmploymentEndDate(empCard.getEmploymentEndDate());

			absenceReportEntries.add(entry);
			LOG.debug("ENTRYXXXgetRestOfDays=" + entry.getRestOfDays());
			LOG.debug("ENTRYXXXgetAdditionalDays=" + entry.getAdditionalDays());
			LOG.debug("ENTRYXXXgetDueDays=" + entry.getDueDays());
			LOG.debug("ENTRYXXXgetExecutedDays=" + entry.getExecutedDays());
		}
		return absenceReportEntries;
	}
	
	/**
	 * Buduje klauzul� where z podanych parametr�w zapytania
	 * 
	 * @param queryParams
	 * @return
	 */
	public static String createWhereClause(Map<String, Object[]> queryParams)
	{
		if (queryParams == null)
			return "";
			
		StringBuffer hql = new StringBuffer();
		Object[] keys = queryParams.keySet().toArray();
		if (keys.length > 0)
			hql.append(" and ");
		// p�tla buduj�ca ci�g zapytania
		for (int i = 0; i < keys.length; i++)
		{
			hql.append(keys[i]);
			if (i + 1 < keys.length)
				hql.append(" and ");
		}
		
		return hql.toString();
	}
	
	public static Long whoSubSuser(Long usernameId) throws SQLException, EdmException
	{
		System.out.println("User id "+ usernameId); 
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		System.out.println(" select a.SUBSTITUTION from dsg_absence_request a where a.PERIOD_FROM <= ? and a.PERIOD_TO >= ? "+ 
    				" and a.DSUSER = ? order by a.PERIOD_TO desc");
    		ps = DSApi.context().prepareStatement(" select a.SUBSTITUTION from dsg_absence_request a where a.PERIOD_FROM <= ? and a.PERIOD_TO >= ? "+ 
    				" and a.DSUSER = ? order by a.PERIOD_TO desc");
    		Calendar cal = Calendar.getInstance();
    		cal.setTime(new Date());
    		cal.set(Calendar.HOUR_OF_DAY, 0);
    		cal.set(Calendar.MINUTE, 0);
    		cal.set(Calendar.SECOND, 0);
    		cal.set(Calendar.MILLISECOND, 0);
    		ps.setTimestamp(1, new java.sql.Timestamp(cal.getTime().getTime()));
    		ps.setTimestamp(2, new java.sql.Timestamp(cal.getTime().getTime()));
    		ps.setLong(3, usernameId);
			rs = ps.executeQuery();
			if(rs.next())
			{
				return rs.getLong(1);
			}
			else
			{
                DSApi.context().closeStatement(ps);
	    		ps = DSApi.context().prepareStatement(" select a.SUBSTITUTION from DSG_ABS_WITHOUT_PR a where a.PERIOD_FROM <= ? and a.PERIOD_TO >= ? "+
				" and a.DSUSER = ? order by a.PERIOD_TO desc");   		
				ps.setTimestamp(1, new java.sql.Timestamp(cal.getTime().getTime()));
				ps.setTimestamp(2, new java.sql.Timestamp(cal.getTime().getTime()));
				ps.setLong(3, usernameId);
				rs = ps.executeQuery();
				if(rs.next())
				{
					return rs.getLong(1);
				}
			}
    	}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DSApi.context().closeStatement(ps);
    	}
    	return null;
	}
	
	
	public static Date whenUserRetern(String username) throws SQLException, EdmException
	{
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement(" select a.END_DATE from ds_absence a, ds_user u, ds_employee_card c where a.START_DATE <= ? and a.END_DATE >= ? "+ 
    				" and a.employee_id = c.id and c.user_id = u.name and u.name = ? and a.absence_type <> 'WUN' and a.absence_type <> 'WUZ' order by a.END_DATE desc");
    		Calendar cal = Calendar.getInstance();
    		cal.setTime(new Date());
    		cal.set(Calendar.HOUR_OF_DAY, 0);
    		cal.set(Calendar.MINUTE, 0);
    		cal.set(Calendar.SECOND, 0);
    		cal.set(Calendar.MILLISECOND, 0);    		
    		ps.setTimestamp(1, new java.sql.Timestamp(cal.getTime().getTime()));
    		ps.setTimestamp(2, new java.sql.Timestamp(cal.getTime().getTime()));
    		ps.setString(3, username);
			rs = ps.executeQuery();
			if(rs.next())
			{
				return rs.getDate(1);
			}
    	}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
    	}
    	return new Date();
	}
	
	public static Boolean isUserAtWork(String username) throws SQLException, EdmException
	{
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement(" select count(1) from ds_absence a, ds_user u, ds_employee_card c where a.START_DATE <= ? and a.END_DATE >= ? "+ 
    				" and a.employee_id = c.id and c.user_id = u.name and u.name = ? and a.absence_type <> 'WUN' and a.absence_type <> 'WUZ'");
    		Calendar cal = Calendar.getInstance();
    		cal.setTime(new Date());
    		cal.set(Calendar.HOUR_OF_DAY, 0);
    		cal.set(Calendar.MINUTE, 0);
    		cal.set(Calendar.SECOND, 0);
    		cal.set(Calendar.MILLISECOND, 0);    		
    		ps.setTimestamp(1, new java.sql.Timestamp(cal.getTime().getTime()));
    		ps.setTimestamp(2, new java.sql.Timestamp(cal.getTime().getTime()));
    		ps.setString(3, username);
			rs = ps.executeQuery();
			if(rs.next())
			{
				Integer i =rs.getInt(1); 
				return i < 1;
			}
    	}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
    	}
    	return true;
	}
	
	public static Boolean isUserAtWork(Long userId) throws SQLException, EdmException
	{
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement(" select count(1) from ds_absence a, ds_employee_card c where a.START_DATE < ? and a.END_DATE > ? "+ 
    				" and a.employee_id = c.id and c.user_id = ? and a.absence_type <> 'WUN' and a.absence_type <> 'WUZ'");
    		ps.setTimestamp(1, new java.sql.Timestamp(new Date().getTime()));
    		ps.setTimestamp(2, new java.sql.Timestamp(new Date().getTime()));
    		ps.setLong(3, userId);
			rs = ps.executeQuery();
			if(rs.next())
			{
				Integer i =rs.getInt(1); 
				return i < 1;
			}
    	}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
    	}
    	return true;
	}
	
	/**
	 * Zwraca aktywn� kart� pracownika na podstawie przekazanego obiektu usera.
	 * 
	 * @param userImpl
	 * @return
	 * @throws EdmException
	 */
	public static EmployeeCard getActiveEmployeeCardByUser(UserImpl userImpl) throws EdmException
	{
		return  (EmployeeCard) DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " e where e.user = ? and e.employmentEndDate is null order by e.employmentStartDate desc")
				.setParameter(0, userImpl)
				.setMaxResults(1)
				.uniqueResult();
	}
	
	/**
	 * Zwraca aktywn� kart� pracownika na podstawie nazwy usera.
	 * 
	 * @param userName
	 * @return
	 * @throws EdmException
	 */
	public static EmployeeCard getActiveEmployeeCardByUserName(String userName) throws EdmException
	{
		return  (EmployeeCard) DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " e where e.externalUser = ? and e.employmentEndDate is null order by e.employmentStartDate desc")
				.setParameter(0, userName)
				.setMaxResults(1)
				.uniqueResult();
	}
	
	/**
	 * Zwraca wszystkie aktywne karty pracownicze.
	 * 
	 * @return
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static List<EmployeeCard> getAllActiveEmployeeCards() throws EdmException
	{
		return  (List<EmployeeCard>) DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " e where e.employmentEndDate is null")
				.list();
	}
	
	/**
	 * Sprawdza czy w danym roku jest przypisany nale�ny urlop dla karty pracownika
	 * 
	 * @param year
	 * @param empCard
	 * @return
	 * @throws Exception
	 */
	public static boolean hasChargedAbsencesInCurrentYear(int year, EmployeeCard empCard) throws EdmException
	{	
		String hql = "select 1 from " + Absence.class.getName() + " a where a.year = ? and a.absenceType.code like ? and a.employeeCard = ?";
		Integer has = (Integer) DSApi.context().session().createQuery(hql)
								.setInteger(0, year)
								.setString(1, "WUN")
								.setParameter(2, empCard)
								.setMaxResults(1)
								.uniqueResult();
		if (has == null)
			return false;
		
		return true;
	}
	
	/**
	 * Wylicza i zwraca liczb� dost�pnych dni urlopu dla danej karty pracownika w bierz�cym roku
	 * 
	 * @param employeeCard
	 * @return
	 * @throws EdmException
	 */
	public static int getAvailableDaysForCurrentYear(EmployeeCard employeeCard) throws EdmException
	{
		int sumDays = 0;
		if (employeeCard != null)
		{
			@SuppressWarnings("unchecked")
			List<Absence> absences = DSApi.context().session().createQuery("from " + Absence.class.getName() + " a "
					+ "where a.year = ? and a.employeeCard = ?")
					.setInteger(0, GregorianCalendar.getInstance().get(GregorianCalendar.YEAR))
					.setParameter(1, employeeCard)
					.list();
			
			for (Absence absence : absences)
			{
				if (absence.getAbsenceType().isAdditional())
				{
					sumDays += absence.getDaysNum();
				}
				if (absence.getAbsenceType().isNegative())
				{
					sumDays -= absence.getDaysNum();
				}
			}
			
			if (employeeCard.getTINW() != null && employeeCard.getTINW())
				sumDays += employeeCard.getInvalidAbsenceDaysNum();
		}
		
		return sumDays;
	}
	
	/**
	 * Usuwa ostatni wpis urlopowy z karty pracownika lub je�li documentId 
	 * jest r�ne od null to wyszukuje wpis dla danego wniosku urlopowego i go usuwa.
	 * 
	 * @param card
	 * @param documentId 
	 * @throws EdmException
	 */
	public static void deleteLastAbsence(EmployeeCard card, Long documentId) throws EdmException
	{
		Absence absence;
		if (documentId != null && documentId > 0)
			// cofniecie ostatniego wpisu urlopowego tego wnioskodawcy z glownej karty
			absence = (Absence) DSApi.context().session().createQuery("from " + Absence.class.getName() + " a " 
					+ "where a.employeeCard = :card AND a.documentId = :documentId order by a.ctime desc")
				.setParameter("card", card)
				.setParameter("documentId", documentId)
				.setMaxResults(1)
				.uniqueResult();
		else
			// cofniecie ostatniego wpisu urlopowego tego wnioskodawcy z glownej karty 
			// (DO USUNIECIA TA CZESC METODY, POZOSTAJE TYLKO DLA STARYCH WNIOSKOW URLOPOWYCH)
			absence = (Absence) DSApi.context().session().createQuery("from " + Absence.class.getName() + " a " 
					+ "where a.employeeCard = :card order by a.ctime desc")
				.setParameter("card", card)
				.setMaxResults(1)
				.uniqueResult();
		if (absence != null) DSApi.context().session().delete(absence);
	}
	
	/**
	 * Zwraca reszte aktywnych kart pracownika na podstawie przekazanej karty
	 * 
	 * @param card
	 * @return
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static List<EmployeeCard> getRestActiveEmployeeCards(EmployeeCard card) throws EdmException
	{
		return DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " e where e.employmentEndDate is null and e.id != ? and e.externalUser = ?")
				//.setString(0, empCard.getKPX())
				.setLong(0, card.getId())
				.setString(1, card.getExternalUser())
			.list();
	}
	
	/**
	 * Zwraca list� wszystkich dni wolnych od pracy
	 * 
	 * @return
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static List<FreeDay> getFreeDays() throws EdmException
	{
		return (List<FreeDay>) DSApi.context().session().createQuery(
				"from " + FreeDay.class.getName() + " f order by f.date desc")
				.list();
	}
	
	/**
	 * Zwraca list� wolnych dni z bierz�cego roku.
	 * 
	 * @throws EdmException
	 */
	public static List<FreeDay> getFreeDaysFromCurrentYear() throws EdmException
	{
		return getFreeeDaysFromYear(Calendar.getInstance().get(Calendar.YEAR));
	}
	
	/**
	 * Zwraca list� wolnych dni w podanym roku.
	 * 
	 * @param year
	 * @return
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static List<FreeDay> getFreeeDaysFromYear(Integer year) throws EdmException
	{
		Calendar firstDate = GregorianCalendar.getInstance();
		firstDate.set(year, Calendar.JANUARY, 1);
		Calendar secondDate = GregorianCalendar.getInstance();
		secondDate.set(year, Calendar.DECEMBER, 31);
		return (List<FreeDay>) DSApi.context().session().createQuery(
				"from " + FreeDay.class.getName() + " f where f.date >= ? and f.date <= ?")
				.setDate(0, firstDate.getTime())
				.setDate(1, secondDate.getTime())
				.list();
	}
	
	/**
	 * Dodanie wpisu urlopowego dla wnioskodawcy, do aktywnych kart pracowniczych
	 * 
	 * @param empCard
	 * @param from
	 * @param to
	 * @param type
	 * @param info
	 * @param numDays
	 * @throws EdmException
	 */
	public static void chargeAbsence(EmployeeCard empCard, Date from, Date to, String type, String info, int numDays) throws EdmException
	{
		chargeAbsence(empCard, from, to, type, info, numDays, null, null, null);
	}
	
	/**
	 * Dodanie wpisu urlopowego dla wnioskodawcy, do aktywnych kart pracowniczych
	 * 
	 * @param empCard
	 * @param from
	 * @param to
	 * @param type
	 * @param info
	 * @param numDays
	 * @param editor
	 * @param editorFullName
	 * @param documentId
	 * @throws EdmException
	 */
	public static void chargeAbsence(EmployeeCard empCard, Date from, Date to, String type, 
			String info, int numDays, UserImpl editor, String editorFullName, Long documentId) throws EdmException
	{
		if (empCard == null)
			throw new IllegalStateException("Brak karty pracownika! Nie mo�na przypisa� urlopu!");
		
		int year = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		if (!AbsenceFactory.hasChargedAbsencesInCurrentYear(year, empCard))
			throw new IllegalStateException("Brak przypisanego nale�nego wymiaru urlopowego na bierz�cy rok!");
		
		// utworzenie wpisu urlopu
		Absence absence = new Absence();
		
		absence.setYear(year);
		absence.setEmployeeCard(empCard);
		
		// ustawienie dat
		absence.setStartDate(from);
		absence.setEndDate(to);
		
		// ustawienie typu urlopu
		AbsenceType absenceType = Finder.find(AbsenceType.class, type);
		absence.setAbsenceType(absenceType);
		
		// ustawienie liczby dni
		absence.setDaysNum(numDays);
		
		// ustawienie opisu
		absence.setInfo(info);
		
		// data utworzenia wpisu
		absence.setCtime(new Date());
		
		if (editor != null)
			// kto zaakceptowa�
			absence.setEditor(editor);
		else if (editorFullName != null)
			absence.setEditorFullName(editorFullName);
		
		// id dokumentu
		absence.setDocumentId(documentId);
		
		// zapisanie naliczonych urlop�w na wszystkie karty pracownika
		chargeAbsences(empCard, absence);
	}
	
	/**
	 * Naliczenie urlopu.
	 * 
	 * @param empCard
	 * @param absence
	 * @throws EdmException
	 */
	public static void chargeAbsence(EmployeeCard empCard, Absence absence) throws EdmException
	{
		if (empCard == null || absence == null)
			throw new IllegalStateException("Brak karty pracownika lub nie podano urlopu! Nie mo�na przypisa� urlopu!");
		
		if (!hasChargedAbsencesInCurrentYear(absence.getYear(), empCard))
			throw new IllegalStateException("Brak przypisanego nale�nego wymiaru urlopowego na bie��cy rok!");
		
		// zapisanie naliczonych urlop�w na wszystkie karty pracownika
		chargeAbsences(empCard, absence);
	}
	
	/**
	 * Zapisuje naliczone urlopy na wszystkie karty pracownika
	 * 
	 * @param empCard
	 * @param absence
	 * @throws EdmException
	 */
	private static void chargeAbsences(EmployeeCard empCard, Absence absence) throws EdmException
	{
		empCard.getAbsences().add(absence);
		
		// zapisanie do bazy
		DSApi.context().session().update(empCard);
		DSApi.context().session().save(absence);
		
		// pobranie wszystkich kart pracownika, z roznych spolek i dodanie do nich tego samego wpisu urlopowego
		List<EmployeeCard> cards = getRestActiveEmployeeCards(empCard);
		if (cards != null && cards.size() > 0)
		{
			for (EmployeeCard card : cards)
			{
				Absence abs = copyAbsence(absence, card);
				card.getAbsences().add(abs);
				DSApi.context().session().update(card);
				DSApi.context().session().save(abs);
			}
		}
	}
	
	/**
	 * Kopiuje obiekt absencji
	 * 
	 * @param absence
	 * @param empCard
	 * @return
	 */
	private static Absence copyAbsence(Absence absence, EmployeeCard empCard)
	{
		Absence newAbsence = new Absence();
		newAbsence.setYear(absence.getYear());
		newAbsence.setEmployeeCard(empCard);
		newAbsence.setStartDate(absence.getStartDate());
		newAbsence.setEndDate(absence.getEndDate());
		newAbsence.setAbsenceType(absence.getAbsenceType());
		newAbsence.setCtime(absence.getCtime());
		newAbsence.setDaysNum(absence.getDaysNum());
		newAbsence.setEditor(absence.getEditor());
		newAbsence.setEditorFullName(absence.getEditorFullName());
		newAbsence.setInfo(absence.getInfo());
		newAbsence.setDocumentId(absence.getDocumentId());
		
		return newAbsence;
	}
	
	/**
	 * Usuwa ostatni lub o okre�lonym documentId wpis urlopu z kart pracowniczych
	 * 
	 * @param empCard
	 * @param documentId
	 * @throws EdmException
	 */
	public static void unchargeLastAbsences(EmployeeCard empCard, Long documentId) throws EdmException
	{
		// usuniecie z glowenj karty
		deleteLastAbsence(empCard, documentId);
		
		// cofniecie ostatniego wpisu urlopowego z kart z roznych spolek
		List<EmployeeCard> cards = getRestActiveEmployeeCards(empCard);
		if (cards != null && cards.size() > 0)
		{
			for (EmployeeCard card : cards)
			{
				deleteLastAbsence(card, documentId);
			}
		}
	}
	
	/**
	 * Sprawdza czy istnieje ju� urlop pomi�dzy podanymi datami.
	 * 
	 * @param empCard
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws EdmException
	 */
	public static boolean hasAbsenceBetween(EmployeeCard empCard , Date startDate, Date endDate) throws EdmException
	{
		//and a.absenceType.code not like :WU
		LOG.debug("TESTURLOPU ");
		Integer hasAbsence = (Integer) DSApi.context().session().createQuery("select 1 from " + Absence.class.getName() 
				+ " a where a.employeeCard = :empCard  and a.absenceType.code <> 'WUN' and a.absenceType.code <> 'WUZ' and " 
				+ "((a.startDate <= :startDate  and a.endDate >= :startDate) "
				+ "or (a.startDate <= :endDate and a.endDate >= :endDate))")
				.setParameter("empCard", empCard)
				.setDate("startDate", startDate)
				.setDate("endDate", endDate)
				.setMaxResults(1)
				.uniqueResult();
		LOG.debug("TESTURLOPU " + hasAbsence);
		if (hasAbsence != null)
			return true;
		return false;
	}
	
	/**
	 * Zwraca sum� dni danego typu urlopu z podanej karty praconwnika w roku bierz�cym.
	 * 
	 * @param empCard
	 * @param type
	 * @return
	 * @throws EdmException
	 */
	public static Long getAbsenceSumDaysFromCurrentYear(EmployeeCard empCard, String type) throws EdmException
	{
		int currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		Long days =  (Long) DSApi.context().session().createQuery("select sum(a.daysNum) from " 
				+ Absence.class.getName() + " a where a.employeeCard = ? and absenceType.code = ? and a.year = ?")
				.setParameter(0, empCard)
				.setString(1, type)
				.setInteger(2, currentYear)
				.uniqueResult();
		
		return days;
	}
	
	public static EmployeeCard createCard(DSUser user) throws HibernateException, EdmException, Exception
	{
		return createCard(user, null, null, null, null);
	}
	/**
	 * Twaorzy kart� pracownika dla podanego user'a.
	 * 
	 * @param user
	 * @return
	 * @throws HibernateException
	 * @throws EdmException
	 */
	public static EmployeeCard createCard(DSUser user, String company, String position, String department, String division) throws HibernateException, EdmException, Exception
 {
		DSApi.context().begin();
		Calendar cld = Calendar.getInstance();
		Date date = new Date();
		EmployeeCard card = new EmployeeCard();
		if (company == null)
			card.setCompany("BD");
		else 
			card.setCompany(company);
		card.setEmploymentStartDate(new Date());
		card.setKPX("BD");
		card.setUser((UserImpl) user);
		card.setFirstName(user.getFirstname());
		card.setLastName(user.getLastname());
		System.out.println(card.getLastName());
		card.setExternalUser(user.getName());
		if (position == null)
			card.setPosition("BD");
		else
			card.setPosition(position);
		if (department == null)
			card.setDepartment("BD");
		else
			card.setDepartment(department);
		if (division == null)
			card.setDivision("BD");
		else
			card.setDivision(division);
		DSApi.context().session().save(card);

						
		if(AvailabilityManager.isAvailable("newUser.addEmployeeCardAndAbsences"))
		{
			Absence absence = new Absence();	
			Absence absence2 = new Absence();	
			AbsenceType dueAbsenceType = Finder.find(AbsenceType.class, "WUN");
			AbsenceType overdueAbsenceType = Finder.find(AbsenceType.class, "WUZ");
			absence.setAbsenceType(dueAbsenceType);
			absence2.setAbsenceType(overdueAbsenceType);
			absence.setDaysNum(26);
			absence2.setDaysNum(0);
			
			cld.set(Calendar.DAY_OF_YEAR,1); //first day of the year.
			absence.setStartDate(cld.getTime());
			absence2.setStartDate(cld.getTime());
			absence.setYear(cld.get(Calendar.YEAR));
			absence2.setYear(cld.get(Calendar.YEAR));
			absence.setCtime(date);
			absence2.setCtime(date);
	
			cld.add(Calendar.YEAR,1);
			cld.set(Calendar.DAY_OF_YEAR,1);
			cld.add(Calendar.YEAR,-1);   //last day of the year.
			absence.setEndDate(cld.getTime());
			absence2.setEndDate(cld.getTime());
			LOG.debug("Id karty: ");
			LOG.debug("Id karty: " + card.getId());
			absence.setEmployeeCard(card);
			absence2.setEmployeeCard(card);
			
			Set<Absence> setA = new  HashSet<Absence>(); 
			setA.add(absence);
			setA.add(absence2); 	
			card.setAbsences(setA);
			DSApi.context().session().update(card);
			DSApi.context().session().save(absence);
			DSApi.context().session().save(absence2);
			DSApi.context().commit();
		}
		return card;
	}
	
	/**
	 * Sprawdza poprawno�� daty startowej dla wniosku urlopowego.
	 * 
	 * @param startDate
	 * @param limit
	 * @return
	 * @throws EdmException
	 */
	public static boolean isCorrectStartDate(Date startDate, final int limit) throws EdmException
	{
		Calendar today = Calendar.getInstance();
		Calendar start = Calendar.getInstance();
		start.setTime(startDate);
		
		if (start.before(today))
		{
			int days = (int) (today.getTimeInMillis() - start.getTimeInMillis()) / 1000 / 60 / 60 / 24;
			if (days > limit)
				return false;
		}
		
		return true;
	}

	/**
	 * Transfer urlop�w na przekazan� kart� pracownika z poprzedniej karty je�li
	 * u�ytkownik mia� tak� za�o�on�.
	 * 
	 * @param empCard
	 */
	public static boolean transferAbsences(EmployeeCard empCard) throws EdmException
	{
		if (empCard == null || empCard.getId() == null || empCard.getExternalUser() == null)
			return false;
		
		EmployeeCard lastCard = getCardBefore(empCard);
		if (lastCard != null)
		{
			empCard.setAbsences(new HashSet<Absence>());
			Set<Absence> absences = new HashSet<Absence>();
			for (Absence absence : lastCard.getAbsences())
			{
				// kopia absencji na nowej karcie
				Absence copy = copyAbsence(absence, empCard);
				empCard.getAbsences().add(copy);
				DSApi.context().session().update(empCard);
				DSApi.context().session().save(copy);
				absences.add(copy);
			}
			return true;
		}
		
		return false;
	}
	
	/**
	 * Dla podanej karty danego u�ytkownika zwraca (je�li istnieje) poprzedni� kart�.
	 * 
	 * @param empCard
	 * @return
	 * @throws EdmException 
	 */
	public static EmployeeCard getCardBefore(EmployeeCard empCard) throws EdmException
	{
		return (EmployeeCard) DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " e " +
				"where e.externalUser = :user and e.id < :id order by e.employmentStartDate")
			.setLong("id", empCard.getId())
			.setString("user", empCard.getExternalUser())
			.setMaxResults(1)
			.uniqueResult();

	}
	
	/** Sprawdza czy dany dzien jest wolny od pracy trzeba popracowa� jeszce nad t� metoda, ale wyglada ze dziala
	 * wg. http://pl.wikipedia.org/wiki/Dni_wolne_od_pracy_w_Polsce */
	public static boolean isHoliday (Calendar day)
	{
         if (day.get(Calendar.DAY_OF_WEEK)== Calendar.SUNDAY) return true;
         if (day.get(Calendar.DAY_OF_WEEK)== Calendar.SATURDAY) return true;
         if (day.get(Calendar.MONTH) == 1 && day.get(Calendar.DAY_OF_WEEK) == 1) return true; // Nowy Rok
         if (day.get(Calendar.MONTH) == 1 && day.get(Calendar.DAY_OF_WEEK) == 6) return true; // Swieto Trzech kroli 
         if (day.get(Calendar.MONTH) == 5 && day.get(Calendar.DAY_OF_WEEK) == 1) return true; // 1 maja
         if (day.get(Calendar.MONTH) == 5 && day.get(Calendar.DAY_OF_WEEK) == 3) return true; // 3 maja
         if (day.get(Calendar.MONTH) == 8 && day.get(Calendar.DAY_OF_WEEK) == 15) return true; // Wniebowzi�cie Naj�wi�tszej Marii Panny, �wi�to Wojska Polskiego
         if (day.get(Calendar.MONTH) == 11 && day.get(Calendar.DAY_OF_WEEK) == 1) return true; // Dzie� Wszystkich �wi�tych
         if (day.get(Calendar.MONTH) == 11 && day.get(Calendar.DAY_OF_WEEK) == 11) return true; // Dzie� Niepodleg�o�ci 
         if (day.get(Calendar.MONTH) == 12 && day.get(Calendar.DAY_OF_WEEK) == 25) return true; // Bo�e Narodzenie
         if (day.get(Calendar.MONTH) == 12 && day.get(Calendar.DAY_OF_WEEK) == 26) return true; // Bo�e Narodzenie
         // swieta ruchome, pierwszy dzien wielkanocy, drugi dzien wielkanocy, pierszy dzien zielonych swiatek, boze cialo
         /** wielaknoc - dla kalendarze gragoraniskiego dla lat 1900 - 2099 A=24, B=5 */
         // Dzielimy liczb� roku przez 19 i znajdujemy reszt� a.
         int a = day.YEAR % 19;
         // Dzielimy liczb� roku przez 4 i znajdujemy reszt� b.
         int b = day.YEAR % 4;
         // Dzielimy liczb� roku przez 7 i znajdujemy reszt� c.
         int c = day.YEAR % 7;
         // Reszt� a mno�ymy przez 19, do iloczynu dodajemy liczb� A, sum� dzielimy przez 30 i znajdujemy reszt� d.
         int d = (a * 19 + 24) % 30;
         // Dzielimy sum� iloczyn�w 2b + 4c + 6d + B przez 7 i znajdujemy reszt� e.
         int e = (2 * b + 4 * c + 6 * d + 5) % 7;
         // Sum� reszt d + e dodajemy do daty 22 marca i otrzymujemy dat� Wielkanocy. Ponizej wyjatki:
         if (d == 29 && e == 6) 
     	    d -= 7;
         if (d == 28 && e == 6 && a > 10) 
     	    d -= 7;
         Calendar easter = new GregorianCalendar(day.get(Calendar.YEAR), 3, 22);
         easter.add(Calendar.DAY_OF_YEAR, d+e); // wielkanoc
         Calendar lany = new GregorianCalendar(day.YEAR, 3, 22);
         lany.add(Calendar.DAY_OF_YEAR, d+e+1); // wielkanoc
         Calendar bozeCialo = new GregorianCalendar(easter.get(Calendar.YEAR), easter.get(Calendar.MONTH), easter.get(Calendar.DAY_OF_MONTH));
         bozeCialo.add(Calendar.DAY_OF_YEAR, -60); // boze cialo
         Calendar zielony = new GregorianCalendar(easter.get(Calendar.YEAR), easter.get(Calendar.MONTH), easter.get(Calendar.DAY_OF_MONTH));
         zielony.add(Calendar.DAY_OF_YEAR, 50); // boze cialo
         if (lany.compareTo(day) == 0)
                 return true; // Wielkanoc (poniedzia�ek)
         if (bozeCialo.compareTo(day) == 0)
                 return true; // Bo�e Cia�o
         if (zielony.compareTo(day) == 0)
              return true; // wniebowstapienie zielone siwatek
         return false;
	}
}

