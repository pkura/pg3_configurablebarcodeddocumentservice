package pl.compan.docusafe.core.base.absences;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa odworowuj�ca tabelk� DS_ABSENCE
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class Absence implements Serializable 
{
	private Logger log = LoggerFactory.getLogger(Absence.class);
	
	/**
	 * Serial VersionUID 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identyfikator
	 */
	private Long id;
	
	/**
	 * Rok, w kt�rym dany urlop zosta� z�o�ony
	 */
	private int year;
	
	/**
	 * Data pocz�tkowa urlopu
	 */
	private Date startDate;
	
	/**
	 * Data zako�czenia urlopu
	 */
	private Date endDate;
	
	/**
	 * Liczba dni urlopu
	 */
	private int daysNum;
	
	/**
	 * Dodatkowe uwagi
	 */
	private String info;
	
	/**
	 * Data utworzenia wpisu
	 */
	private Date ctime;
	
	/**
	 * Karta pracownika, do kt�rej dany urlop jest przypisany
	 */
	private EmployeeCard employeeCard;
	
	/**
	 * Typ urlopu
	 */
	private AbsenceType absenceType;
	
	/**
	 * Kto modyfikowa� dany wpis urlopu
	 */
	private UserImpl editor;
	
	private String editorFullName;
	
	/**
	 * ID dokumentu, do kt�rego przypisano urlop
	 */
	private Long documentId;
	
	/**
	 * Data modyfikacji wpisu
	 */
	private Date mtime;

	public Absence() 
	{
	}

	/**
	 * Konstruktor ustawiaj�cy wszystkie pola
	 * 
	 * @param year
	 * @param startDate
	 * @param endDate
	 * @param daysNum
	 * @param info
	 * @param ctime
	 * @param employeeCard
	 * @param absenceType
	 * @param editor
	 * @param mtime
	 */
	public Absence(int year, Date startDate, Date endDate, int daysNum,
			String info, Date ctime, EmployeeCard employeeCard,
			AbsenceType absenceType, UserImpl editor, Date mtime) 
	{
		super();
		this.year = year;
		this.startDate = startDate;
		this.endDate = endDate;
		this.daysNum = daysNum;
		this.info = info;
		this.ctime = ctime;
		this.employeeCard = employeeCard;
		this.absenceType = absenceType;
		this.editor = editor;
		this.mtime = mtime;
	}
	

    public void delete() throws Exception
    {
    	log.debug("deleting " + this);
        try
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
            if(AvailabilityManager.isAvailable("absence.createEventOnCreate"))
            {
            	CalendarFactory.getInstance().deleteEventsByRelatedAbsence(getId());
            }
        }
        catch (HibernateException e)
        {
            log.error("Blad usuniecia wpisu");
            throw new EdmException("Blad usuniecia wpisu. "+e.getMessage());
        }
    }
    

	public void create() throws Exception, EdmException
	{
		log.debug("create()");
        DSApi.context().session().save(this);
        DSApi.context().session().flush(); 
	}

	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public int getYear() 
	{
		return year;
	}

	public void setYear(int year) 
	{
		this.year = year;
	}

	public Date getStartDate() 
	{
		return startDate;
	}

	public void setStartDate(Date startDate) 
	{
		this.startDate = startDate;
	}

	public Date getEndDate() 
	{
		return endDate;
	}

	public void setEndDate(Date endDate) 
	{
		this.endDate = endDate;
	}

	public int getDaysNum() 
	{
		return daysNum;
	}

	public void setDaysNum(int daysNum) 
	{
		this.daysNum = daysNum;
	}

	public String getInfo() 
	{
		return info;
	}

	public void setInfo(String info) 
	{
		this.info = info;
	}

	public Date getCtime() 
	{
		return ctime;
	}

	public void setCtime(Date ctime) 
	{
		this.ctime = ctime;
	}

	public EmployeeCard getEmployeeCard() 
	{
		return employeeCard;
	}

	public void setEmployeeCard(EmployeeCard employeeCard) 
	{
		this.employeeCard = employeeCard;
	}

	public AbsenceType getAbsenceType() 
	{
		return absenceType;
	}

	public void setAbsenceType(AbsenceType absenceType) 
	{
		this.absenceType = absenceType;
	}

	public UserImpl getEditor() 
	{
		return editor;
	}

	public void setEditor(UserImpl editor) 
	{
		this.editor = editor;
	}
	
	public String getEditorFullName() 
	{
		return editorFullName;
	}
	
	public void setEditorFullName(String editorFullName) 
	{
		this.editorFullName = editorFullName;
	}

	public Date getMtime() 
	{
		return mtime;
	}

	public void setMtime(Date mtime) 
	{
		this.mtime = mtime;
	}

	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(Long documentId) 
	{
		this.documentId = documentId;
	}

	/**
	 * @return the documentId
	 */
	public Long getDocumentId() 
	{
		return documentId;
	}
}
