package pl.compan.docusafe.core.base.absences;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

public class FreeDay implements Serializable 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	
	private Date date;
	
	private String info;
	
        public static List<FreeDay> find(Date date) throws EdmException
        {
            try
            {
                Criteria criteria = DSApi.context().session().createCriteria(FreeDay.class);
                criteria.add(Restrictions.eq("date",date));

                List<FreeDay> freeDays = (List<FreeDay>)criteria.list();
                
                return freeDays;

            } catch (HibernateException ex)
            {
                throw new EdmHibernateException(ex);
            }
        }
        /**
         * Rzuca wyjatkiem je�li wpis istnieje w bazie
         * @param checkDate
         * @throws EdmException 
         */
        public static void isFreeDay(Date checkDate) throws EdmException
        {
            try
            {
                List<FreeDay> freeDays = find(checkDate);
                
                if(!freeDays.isEmpty())
                    throw new EdmException("Ten dzie� znajduje si� w kalendarzu dni wolnych!");
            }  
            catch (NullPointerException ex)
            {
                    throw new EdmException("B��d w dacie!");
            }
        }
	/**
	 * domy�lny konstruktor
	 */
	public FreeDay() 
	{
	}

	/**
	 * Konstruktor ustawiaj�cy pola
	 * 
	 * @param date
	 * @param info
	 */
	public FreeDay(Date date, String info) 
	{
		super();
		this.date = date;
		this.info = info;
	}

	public long getId() 
	{
		return id;
	}

	public void setId(long id) 
	{
		this.id = id;
	}

	public Date getDate() 
	{
		return date;
	}

	public void setDate(Date date) 
	{
		this.date = date;
	}

	public String getInfo() 
	{
		return info;
	}

	public void setInfo(String info) 
	{
		this.info = info;
	}
}
