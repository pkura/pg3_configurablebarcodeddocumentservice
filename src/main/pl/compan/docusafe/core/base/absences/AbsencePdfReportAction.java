package pl.compan.docusafe.core.base.absences;

import java.io.File;

import javax.servlet.http.HttpServletResponse;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class AbsencePdfReportAction extends EventActionSupport 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AbsencePdfReportAction.class);
	
	private String empCardName;
	private Integer currentYear;
	
	@Override
	protected void setup() 
	{
		registerListener("doPdf")
			.append(OpenHibernateSession.INSTANCE)
			.append(new PdfReport())
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class PdfReport implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				HttpServletResponse resp = ServletActionContext.getResponse();
				LOG.error("pdfcreate empCardName=" + empCardName);
				File card = AbsenceFactory.pdfCard(empCardName, currentYear);
				ServletUtils.streamFile(resp, card, "application/pdf", "Content-Disposition: attachment; filename=\"employeeCard.pdf\"");
			}
			catch (Exception e) 
			{
				addActionError(e.getMessage());
                LOG.error(e.getMessage(), e);
			}
		}
	}

	public String getEmpCardName() 
	{
		return empCardName;
	}
	
	public void setEmpCardName(String empCardName) 
	{
		this.empCardName = empCardName;
	}

	public Integer getCurrentYear() 
	{
		return currentYear;
	}

	public void setCurrentYear(Integer currentYear) 
	{
		this.currentYear = currentYear;
	}
}
