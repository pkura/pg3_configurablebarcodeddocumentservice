package pl.compan.docusafe.core.base.absences;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.jfree.util.Log;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.polcz.AbsenceIntegrationService;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa odwzorowuj�ca tabelk� DS_ABSENCE_TYPE
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class AbsenceType implements Serializable 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	public static final Logger log = LoggerFactory.getLogger(AbsenceType.class);
	
	
	/**
	 * Nazwa kodowa typu urlopu
	 */
	private String code;
	
	/**
	 * Nazwa urlopu
	 */
	private String name;
	
	/**
	 * Dodatkowe uwagi
	 */
	private String info;
	
	/**
	 * Czy urlop dodatkowy
	 */
	private boolean additional;
	
	/**
	 * okre�la typ urlopu
	 */
	private int flag;
	

	public enum FLAG {
		NEGATIVE(0),
		NEUTRAL(1),
		ADDITIONAL(2);
		
		private int code;
		
		private FLAG(int c) 
		{
			code = c;
		}
		
		public int getCode()
		{
			return code;
		}
	}
	

	
	/**
	 * Konstruktor domy�lny
	 */
	public AbsenceType() 
	{
	}

	/**
	 * Konstruktor, kt�ry ustawia wszystkie pola
	 * 
	 * @param code
	 * @param name
	 * @param info
	 */
	public AbsenceType(String code, String name, String info, int flag) 
	{
		super();
		this.code = code;
		this.name = name;
		this.info = info;
		this.flag = flag;
	}

	public String getCode() 
	{
		return code;
	}

	public void setCode(String code) 
	{
		this.code = code;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getInfo() 
	{
		return info;
	}

	public void setInfo(String info) 
	{
		this.info = info;
	}

	public boolean isAdditional() 
	{
		if (flag == FLAG.ADDITIONAL.code)
			return true;
		return false;
	}
	
	public boolean isNegative()
	{
		if (flag == FLAG.NEGATIVE.code)
			return true;
		return false;
	}
	
	public boolean isNeutral()
	{
		if (flag == FLAG.NEUTRAL.code)
			return true;
		return false;
	}

	public int getFlag() 
	{
		return flag;
	}

	public void setFlag(int flag) 
	{
		this.flag = flag;
	}

}
