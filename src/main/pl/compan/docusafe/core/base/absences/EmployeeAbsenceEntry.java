package pl.compan.docusafe.core.base.absences;

import java.io.Serializable;

/**
 * Obiekty tej klasy wykorzystywane s� do generowania listy urlop�w danego pracownika
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EmployeeAbsenceEntry implements Serializable 
{
	/**
	 * Serial Version UID 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Liczba porz�dkowa
	 */
	private int lp;
	
	/**
	 * obiekt urlopu
	 */
	private Absence absence;
	
	/**
	 * Liczba pozosta�ych dni urlopu
	 */
	private int restOfDays;
	
	/**
	 * Domy�lny konstruktor
	 */
	public EmployeeAbsenceEntry() 
	{
	}

	/**
	 * Konstruktor ustawiaj�cy wszystkie w�a�ciwo�ci
	 * 
	 * @param lp
	 * @param absence
	 * @param restOfDays
	 */
	public EmployeeAbsenceEntry(int lp, Absence absence, int restOfDays) 
	{
		super();
		this.lp = lp;
		this.absence = absence;
		this.restOfDays = restOfDays;
	}
	
	public int getLp() 
	{
		return lp;
	}

	public void setLp(int lp) 
	{
		this.lp = lp;
	}

	public Absence getAbsence() 
	{
		return absence;
	}

	public void setAbsence(Absence absence) 
	{
		this.absence = absence;
	}

	public int getRestOfDays() 
	{
		return restOfDays;
	}

	public void setRestOfDays(int restOfDays) 
	{
		this.restOfDays = restOfDays;
	}
}
