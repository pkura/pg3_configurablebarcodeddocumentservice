package pl.compan.docusafe.core.base;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.CallbackException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

public class ExampleDoc implements Lifecycle
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(ExampleDoc.class.getPackage().getName(),null);
    private Long id;
    private String name;
    private Date creation_date;
  
    
    ExampleDoc()
    {
    }
   
    public static List<ExampleDoc> findByName(String name) throws EdmException
    {
        return DSApi.context().session().createCriteria(ExampleDoc.class).
        add(Restrictions.like("name", "%"+name+"%").ignoreCase()).list();
    }

   

    public ExampleDoc(String name)
    {
        if (name == null)
            throw new NullPointerException("name");
        this.name = name.toUpperCase();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

   
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

	private void setCreationDate(Date creation_date) {
		this.creation_date = creation_date;
	}

	private Date getCreationDate() {
		return creation_date;
	}
    
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Box)) return false;

        final ExampleDoc doc = (ExampleDoc) o;

        if (id != null ? !(id.equals(doc.id)) : doc.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public boolean onSave(Session s) throws CallbackException
    {      
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }
    
    public String toString() {
    	return ""+id+" "+name;
    }


	

}
