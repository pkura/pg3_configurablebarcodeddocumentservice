package pl.compan.docusafe.core.base;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentPermission.java,v 1.3 2005/11/24 16:16:36 lk Exp $
 */
public class DocumentPermission extends ObjectPermission
{
    private Long documentId;

    /**
     * Konstruktor u�ywany przez Hibernate.
     */
    DocumentPermission()
    {
    }

    public DocumentPermission(Long documentId)
    {
        this.documentId = documentId;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof DocumentPermission)) return false;
        if (!super.equals(o)) return false;

        final DocumentPermission documentPermission = (DocumentPermission) o;

        if (documentId != null ? !documentId.equals(documentPermission.documentId) : documentPermission.documentId != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result = super.hashCode();
        result = 29 * result + (documentId != null ? documentId.hashCode() : 0);
        return result;
    }
}
