package pl.compan.docusafe.core.base;

import pl.compan.docusafe.util.StringManager;


/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AttachmentNotFoundException.java,v 1.3 2005/12/29 15:56:15 lk Exp $
 */
public class AttachmentNotFoundException extends EntityNotFoundException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public AttachmentNotFoundException(Long id)
    {
        super(sm.getString("attachmentNotFoundException", id));
    }

    public AttachmentNotFoundException(String message)
    {
        super(message);
    }
}
