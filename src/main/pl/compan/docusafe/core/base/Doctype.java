package pl.compan.docusafe.core.base;

import org.hibernate.*;
import org.hibernate.dialect.Dialect;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Expression;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.archive.repository.Constants;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;

/* User: Administrator, Date: 2005-05-04 12:55:07 */

/** <pre>
 * <doctype>
 *    <fields>
 *       <field id="0" cn="NR_POLISY" name="Numer polisy" type="string" length="20"/>
 *       <field id="1" cn="PODPISANA" name="Podpisana" type="bool" />
 *       <field id="2" cn="DATA" name="Data" type="date" />
 *    </fields>
 * </doctype>
 *  </pre>
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Doctype.java,v 1.62 2009/02/28 09:28:34 wkuty Exp $
 */
@Deprecated
public class Doctype implements Lifecycle
{
	private static final StringManager sm =
        StringManager.getManager(Constants.Package);
    private static final Log log = LogFactory.getLog(Doctype.class);

    //public static final String TABLE_PREFIX = "DSG_";
    public static final String TABLE_NAME_PREFIX = "DSG_DOCTYPE_";
    public static final String FIELD_NAME_PREFIX = "FIELD_";

    private Long id;
    private Integer hversion;
    private String name;
    /**
     * Nazwa tabeli.  Jest tworzona w momencie tworzenia obiektu Doctype
     * i ma zawsze posta� {@link #TABLE_NAME_PREFIX} + {@link #id},
     * np. "DSG_DOCTYPE_4".
     */
    private String tablename;
    private boolean enabled;
    private String cn;

    // zapami�tane pola typu; odczytywane przez loadFields
    private List<Field> fields;

    Doctype()
    {
    }

    public Doctype(String name)
    {
        this.name = name;
    }

    public static List<Doctype> list() throws EdmException
    {
        return Finder.list(Doctype.class, "name");
    }

    @SuppressWarnings("unchecked")
    public static List<Doctype> list(boolean onlyEnabled) throws EdmException
    {
        if (onlyEnabled)
        {
            Criteria criteria = DSApi.context().session().createCriteria(Doctype.class);
            criteria.add(Expression.eq("enabled", Boolean.TRUE));
            try
            {
                return (List<Doctype>) criteria.list();
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
        }
        else
        {
            return list();
        }
    }

    public static Doctype find(Long id) throws EdmException
    {
        return (Doctype) Finder.find(Doctype.class, id);
    }

    public static Doctype findByName(String name)
        throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(Doctype.class);
        criteria.add(Expression.eq("name", name));

        try
        {
            List list = criteria.list();
            if (list.size() > 0)
            {
                return (Doctype) list.get(0);
            }
            throw new EntityNotFoundException(sm.getString("NieZnalezionoTypuDokumentuOnazwie")+" "+name);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Doctype findByCn(String cn) throws EdmException
    {
        try
        {
            List list = DSApi.context().classicSession().find("from d in class "+Doctype.class.getName()+
                " where d.cn = ?", cn, Hibernate.STRING);

            if (list.size() > 0)
            {
                return (Doctype) list.get(0);
            }

            return null;//throw new EntityNotFoundException("Nie znaleziono typu dokumentu o cn = "+cn);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void create() throws EdmException
    {
        Persister.create(this);
    }

    public void delete() throws EdmException
    {
        if (inUse())
            throw new EdmException(sm.getString("NieMoznaUsunacTypuDokumentuBedacegoWuzyciu"));
        Persister.delete(this);
    }

    /**
     * @return Liczba wi�ksza od zera okre�laj�ca liczb� dokument�w, kt�re
     *  korzystaj� z tego typu, -1 je�eli nie ma takich dokument�w, ale
     *  istnieje tabela w bazie danych lub 0, je�eli typ jest nieu�ywany.
     */
    private int countDocuments() throws EdmException
    {
        PreparedStatement pst = null;
        try
        {
            // szukam liczby dokument�w odwo�uj�cych si� do tego typu
            pst = DSApi.context().prepareStatement("select count(*) from "+DSApi.getTableName(Document.class)+ " where doctype = ?");
            pst.setLong(1, getId());
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
            {
                throw new EdmException(sm.getString("NieMoznaOdczytacLiczbyDokumentowKorzystajacychZtypu")
                		+" "+getName()+" ("+getId()+")");
            }

            int count = rs.getInt(1);
            if (count > 0)
            {
                return count;
            }
            else
            {
                // szukam tabeli w bazie danych, XXX: testowane tylko na Firebirdzie
                rs = DSApi.context().session().connection().
                    getMetaData().getTables(null, null, tablename, null);
                if (rs.next())
                    return 0;
            }

            return -1;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }

    public boolean inUse() throws EdmException
    {
        return countDocuments() >= 0;
    }

    private org.dom4j.Document xmlDocument;

    /**
     * Zwraca XML opisuj�cy typ dokumentu.  Ponowne wywo�ania
     * nie odwo�uj� si� do bazy, tylko zwracaj� dokument odczytany
     * za pierwszym razem.
     * <p>
     * Je�eli w bazie nie ma XML opisuj�cego typ dokumentu, zwracany jest
     * utworzony ad hoc dokument zawieraj�cy tylko element g��wny.
     */
    public org.dom4j.Document getXML() throws EdmException
    {
        if (xmlDocument != null)
            return xmlDocument;

        Statement st = null;
        try
        {
        	/**
        	 * @todo - zmienic miejsce pobierania pliku XML na ds_document Kind
        	 */
            st = DSApi.context().createStatement();
            ResultSet rs = st.executeQuery("select content from " +
                DSApi.getTableName(Doctype.class)+" where id = "+getId());
            if (!rs.next())
                throw new EdmException(sm.getString("NieMoznaOdczytacObiektu")+getId());

            Blob blob = rs.getBlob(1);

            // je�eli w bazie by� XML, odczytuj� go, w przeciwnym razie
            // tworz� pusty dokument (tylko g��wny element)
            if (blob != null)
            {
                InputStream is = blob.getBinaryStream();
                SAXReader reader = new SAXReader();
                xmlDocument = reader.read(is);
                org.apache.commons.io.IOUtils.closeQuietly(is);              
            }
            else
            {
                org.dom4j.DocumentFactory factory = new org.dom4j.DocumentFactory();
                xmlDocument = factory.createDocument(factory.createElement("doctype"));
            }

            return xmlDocument;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "id="+id);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (Exception e)
        {
            throw new EdmException(sm.getString("BladOdczytuXML"), e);
        }
        finally
        {
            DSApi.context().closeStatement(st);
        }
    }

    private void loadFields() throws EdmException
    {
        if (fields != null)
            return;

        org.dom4j.Document doc = getXML();

        org.dom4j.Element elFields = doc.getRootElement().element("fields");
        if (elFields != null)
        {
            List fieldTags = elFields.elements("field");
            if (fieldTags != null)
            {
                fields = new ArrayList<Field>(fieldTags.size());
                for (Iterator iter=fieldTags.iterator(); iter.hasNext(); )
                {
                    org.dom4j.Element el = (Element) iter.next();
                    /**
                     * @todo - typ pobierac bedziemy z innego miejsca
                     */
                    String type = el.attributeValue("type");

                    Integer length = null;
                    if (el.attribute("length") != null && Field.STRING.equals(type))
                        length = new Integer(el.attributeValue("length"));

                    Field field = new Field(
                        Integer.valueOf(el.attributeValue("id")),
                        el.attributeValue("cn"),
                        el.attributeValue("column"),
                        el.attributeValue("name"),
                        type,
                        length);

                    if (el.attribute("match") != null && Field.STRING.equals(type))
                        field.setMatch(el.attributeValue("match"));
                    if (el.attribute("hidden") != null)
                        field.setHidden(Boolean.valueOf(el.attributeValue("hidden")).booleanValue());
                    if (el.attribute("readonly") != null)
                        field.setReadOnly(Boolean.valueOf(el.attributeValue("readonly")).booleanValue());

                    if (Field.ENUM.equals(type))
                    {
                        List elItems = el.elements("enum-item");
                        if (elItems != null && elItems.size() > 0)
                        {
                            for (Iterator itemIter=elItems.iterator(); itemIter.hasNext(); )
                            {
                                Element elItem = (Element) itemIter.next();

                                List<String> args = new ArrayList<String>(6);
                                for (int i=1; i <= 5; i++)
                                {
                                    String attrName = "arg"+i;
                                    if (elItem.attributeValue(attrName) != null)
                                    {
                                        args.add(elItem.attributeValue(attrName));
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                                field.addEnum(
                                    new Integer(elItem.attributeValue("id")),
                                    elItem.attributeValue("cn"),
                                    elItem.attributeValue("title"),
                                    (String[]) args.toArray(new String[args.size()]));
                            }
                        }
                    }

                    fields.add(field);
                }
            }
        }

        if (fields == null)
            fields = new LinkedList<Field>();
    }

    /**
     * Zwraca list� p�l okre�lonych dla typu dokumentu. Zwracana
     * tablica mo�e by� pusta.
     * @return
     * @throws EdmException
     */
    public Field[] getFieldsAsArray() throws EdmException
    {
        loadFields();
        return (Field[]) fields.toArray(new Field[fields.size()]);
    }

    public List<Field> getFields() throws EdmException
    {
        loadFields();
        return fields;
    }

    public Field getFieldById(Integer fieldId) throws EdmException
    {
        Field[] fs = getFieldsAsArray();
        for (int i=0; i < fs.length; i++)
        {
            if (fs[i].getId().equals(fieldId))
                return fs[i];
        }
        throw new EdmException(sm.getString("NieZnalezionoPolaMajacegoIdentyfikator")+" "+fieldId);
    }

    public Field getFieldByCn(String cn) throws EdmException
    {
        Field[] fs = getFieldsAsArray();
        for (int i=0; i < fs.length; i++)
        {
            if (fs[i].getCn() != null && fs[i].getCn().equals(cn))
                return fs[i];
        }
        return null;//throw new EntityNotFoundException("Nie znaleziono pola maj�cego cn "+cn);
    }

    /**
     * Dodaje pole do definicji typu.
     * @param name
     * @param type
     * @param length
     * @throws EdmException Je�eli typ dokumentu jest u�ywany ({@link #inUse()} zwraca true).
     */
    public void addField(String cn, String name, String type, Integer length,
                         Map enumValues)
        throws EdmException
    {
        if (name == null)
            throw new NullPointerException("name");
        if (type == null)
            throw new NullPointerException("type");

        if (inUse())
            throw new EdmException(sm.getString("TypDokumentuJestUzywanyNieMoznaZmienicDefinicjiPol"));

        if (Field.STRING.equals(type) && length == null)
            throw new EdmException(sm.getString("PoleTypuNapisMusiMiecPodanaDlugosc"));
        if (!Field.STRING.equals(type) && length != null)
            throw new EdmException(sm.getString("TylkoPoleTypuNapisMozeMiecOkreslonaDlugosc"));

        loadFields();

        int maxFieldId = -1;
        for (Iterator iter=fields.iterator(); iter.hasNext(); )
        {
            Integer fid = ((Field) iter.next()).getId();
            if (fid != null && fid.intValue() > maxFieldId)
                maxFieldId = fid.intValue();
        }

        Integer fieldId = new Integer(maxFieldId+1);

        Field field = new Field(fieldId, cn, FIELD_NAME_PREFIX+fieldId, name, type, length);

        fields.add(field);
    }

    public void updateField(Integer fieldId, String cn, String name, String type, Integer length) throws EdmException
    {
        if (fieldId == null)
            throw new NullPointerException("fieldId");
        if (name == null)
            throw new NullPointerException("name");
        if (type == null)
            throw new NullPointerException("type");

        if (Field.STRING.equals(type) && length == null)
            throw new EdmException(sm.getString("PoleTypuNapisMusiMiecPodanaDlugosc"));
        if (!Field.STRING.equals(type) && length != null)
            throw new EdmException(sm.getString("TylkoPoleTypuNapisMozeMiecOkreslonaDlugosc"));

        Field field = getFieldById(fieldId);

        if (inUse())
        {
            if (!type.equals(field.getType()))
                throw new EdmException(sm.getString("NieMoznaZmienicTypuPolaGdyTypDokumentuJestUzywany"));
            if (length != null && field.getLength() != null && !field.getLength().equals(length))
                throw new EdmException(sm.getString("NieMoznaZmienicDlugosciPolaGdyTypDokumentuJestUzywany"));
        }

        Field newField = new Field(fieldId, cn, FIELD_NAME_PREFIX+fieldId, name, type, length);

        for (int i=0; i < fields.size(); i++)
        {
            if (fieldId.equals(((Field) fields.get(i)).getId()))
            {
                fields.set(i, newField);
                break;
            }
        }
    }

    public void saveChanges() throws EdmException
    {
        org.dom4j.DocumentFactory factory = new org.dom4j.DocumentFactory();
        org.dom4j.Document doc = factory.createDocument(factory.createElement("doctype"));

        org.dom4j.Element elFields = doc.getRootElement().addElement("fields");

        for (Iterator iter=fields.iterator(); iter.hasNext(); )
        {
            Field field = (Field) iter.next();
            org.dom4j.Element elField = elFields.addElement("field");
            elField.addAttribute("id", field.getId().toString());
            if (field.getCn() != null)
                elField.addAttribute("cn", field.getCn());
            elField.addAttribute("column", field.getColumn());
            elField.addAttribute("name", field.getName());
            elField.addAttribute("type", field.getType());
            if (Field.STRING.equals(field.getType()))
                elField.addAttribute("length", String.valueOf(field.getLength()));
            if (Field.ENUM.equals(field.getType()))
            {
                for (Iterator enumIter=field.getEnumItems().iterator(); enumIter.hasNext(); )
                {
                    EnumItem item = (EnumItem) enumIter.next();
                    org.dom4j.Element elItem = elField.addElement("enum-item");
                    elItem.addAttribute("id", item.getId().toString());
                    if (item.getCn() != null)
                        elItem.addAttribute("cn", item.getCn());
                    elItem.addAttribute("title", item.getTitle());
                }
            }
        }

        try
        {
            ByteArrayOutputStream os = new ByteArrayOutputStream(2048);
            XMLWriter xmlWriter = new XMLWriter(os);
            xmlWriter.write(doc);

            PreparedStatement ps = null;
            ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(Doctype.class)+
                " set content = ? where id = ?");
            ps.setBinaryStream(1, new ByteArrayInputStream(os.toByteArray()), os.size());
            ps.setLong(2, getId().longValue());
            int rows = ps.executeUpdate();
            DSApi.context().closeStatement(ps);
            if (rows != 1)
                throw new EdmException(sm.getString("NieUdaloSieZapisacDanychTypuDokumentu"));
          
            xmlDocument = doc;
        }
        catch (IOException e)
        {
            throw new EdmException(sm.getString("WystapilBladPodczasTworzeniaXML"), e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (EdmException e)
        {
            throw e;
        }
    }

    public static class EnumItem
    {
        private Integer id;
        private String cn;
        private String title;
        private String[] args;

        public EnumItem(Integer id, String cn, String title, String[] args)
        {
            this.id = id;
            this.cn = cn;
            this.title = title;
            this.args = args;
        }

        public Integer getId()
        {
            return id;
        }

        public String getCn()
        {
            return cn;
        }

        /**
         * Zwraca warto�� argumentu zwi�zanego z warto�ci�.
         * @param position Pozycja argumentu liczona od 1.
         * @return Warto�� argumentu lub null.
         */
        public String getArg(int position)
        {
            if (position > 0 && (position-1) < args.length)
                return args[position-1];
            return null;
        }

        public String getArg1()
        {
            return getArg(1);
        }

        public String getArg2()
        {
            return getArg(2);
        }

        public String getArg3()
        {
            return getArg(3);
        }

        public String getArg4()
        {
            return getArg(4);
        }

        public String getArg5()
        {
            return getArg(5);
        }

        public String getTitle()
        {
            return title;
        }

        public String toString()
        {
            StringBuilder result = new StringBuilder("[id=");
            result.append(id);
            result.append(" cn=");
            result.append(cn);
            result.append(" title=");
            result.append(title);
            if (args != null && args.length > 0)
            {
                for (int i=0; i < args.length; i++)
                {
                    result.append(" arg");
                    result.append(i+1);
                    result.append('=');
                    result.append(args[i]);
                }
            }
            result.append(']');
            return result.toString();
        }
    }

    public static class Field
    {
        public static final String DATE = "date";
        public static final String STRING = "string";
        public static final String BOOL = "bool";
        public static final String ENUM = "enum";
        public static final String INTEGER = "integer";
        public static final String LONG = "long";


        //archiwum agenta - w celu ustawienia wszystkich parametrow w doctype
        public static final String AGENT = "agent";
        public static final String AGENCJA = "agencja";

        public static final String MATCH_EXACT = "exact";

        private Integer id;
        private String cn;
        private String column;
        private String name;
        private String type;
        private Integer length;
        private String match;
        private Map<Integer, EnumItem> enumItems;
        private boolean hidden;
        private boolean readOnly;

        private Field(Integer id, String cn, String column, String name, String type, Integer length)
            throws EdmException
        {
            if (id == null)
                throw new NullPointerException("id");
            if (column == null)
                throw new NullPointerException("column");
            if (name == null)
                throw new NullPointerException("name");
            if (type == null)
                throw new NullPointerException("type");

            if (!STRING.equals(type) && !DATE.equals(type) && !BOOL.equals(type) &&
                !ENUM.equals(type) && !INTEGER.equals(type) && !LONG.equals(type) && !AGENT.equals(type) && !AGENCJA.equals(type))
                throw new EdmException("Nieznany typ pola: "+type);

            if (STRING.equals(type) && length == null)
                throw new EdmException("Pole napisowe musi mie� okre�lon� d�ugo��");
            if (!STRING.equals(type) && length != null)
                throw new EdmException("Tylko pole typu napis mo�e mie� okre�lon� d�ugo��");

            this.id = id;
            this.cn = cn;
            this.column = column;
            this.name = name;
            this.type = type;
            this.length = length;
        }

        public boolean isHidden()
        {
            return hidden;
        }

        public void setHidden(boolean hidden)
        {
            this.hidden = hidden;
        }

        public boolean isReadOnly()
        {
            return readOnly;
        }

        public void setReadOnly(boolean readOnly)
        {
            this.readOnly = readOnly;
        }

        private void setMatch(String match)
        {
            this.match = match;
        }

        public String getMatch()
        {
            return match;
        }

        public void addEnum(Integer id, String cn, String title, String[] args)
        {
            if (id == null)
                throw new NullPointerException("id");
            if (title == null)
                throw new NullPointerException("title");

            if (enumItems == null)
                enumItems = new LinkedHashMap<Integer, EnumItem>();

            enumItems.put(id, new EnumItem(id, cn, title, args));
        }

        /**
         * Zwraca list� obiekt�w {@link EnumItem}.  Je�eli pole nie jest
         * typu wyliczeniowego, zwracana jest warto�� null.
         */
        public List getEnumItems()
        {
            if (!ENUM.equals(type))
                throw new IllegalStateException("Wywo�anie getEnumItems() ma sens tylko " +
                    "dla pola typu "+ENUM);
            return enumItems != null ? new ArrayList<EnumItem>(enumItems.values()) : Collections.EMPTY_LIST;
        }

        public EnumItem getEnumItem(Integer id) throws EntityNotFoundException
        {
            EnumItem item = enumItems != null ? (EnumItem) enumItems.get(id) : null;
            if (item == null)
                throw new EntityNotFoundException("Nie znaleziono warto�ci wyliczeniowej " +
                    "o identyfikatorze = "+id);
            return item;
        }


        public EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException
        {
            if (!ENUM.equals(type))
                throw new IllegalStateException("Wywo�anie getEnumItemByCn() ma sens tylko " +
                    "dla pola typu "+ENUM);
            if (cn == null)
                throw new NullPointerException("cn");
            if (enumItems != null)
            {
                for (Iterator iter=enumItems.values().iterator(); iter.hasNext(); )
                {
                    EnumItem item = (EnumItem) iter.next();
                    if (cn.equals(item.getCn()))
                        return item;
                }
            }
            throw new EntityNotFoundException("Nie znaleziono warto�ci wyliczeniowej " +
                "o kodzie = "+cn);
        }


        public EnumItem getEnumItemById(Integer id) throws EntityNotFoundException
        {
            if (!ENUM.equals(type))
                throw new IllegalStateException("Wywo�anie getEnumItemByCn() ma sens tylko " +
                    "dla pola typu "+ENUM);
            if (id == null)
                throw new NullPointerException("id");
            if (enumItems != null)
            {
                for (Iterator iter=enumItems.values().iterator(); iter.hasNext(); )
                {
                    EnumItem item = (EnumItem) iter.next();
                    if (id.equals(item.getId()))
                        return item;
                }
            }
            throw new EntityNotFoundException("Nie znaleziono warto�ci wyliczeniowej " +
                "o kodzie = "+id);
        }


        /**
         * szuka po arg1
         * @param arg1
         * @return
         * @throws EntityNotFoundException
         */
        public EnumItem getEnumItemByArg1(String arg1) throws EntityNotFoundException
        {
            if (!ENUM.equals(type))
                throw new IllegalStateException("Wywo�anie getEnumItemByArg1() ma sens tylko " +
                    "dla pola typu "+ENUM);
            if (arg1 == null)
                throw new NullPointerException("arg1");
            if (enumItems != null)
            {
                for (Iterator iter=enumItems.values().iterator(); iter.hasNext(); )
                {
                    EnumItem item = (EnumItem) iter.next();
                    if (arg1.equals(item.getArg1()))
                        return item;
                }
            }
            throw new EntityNotFoundException("Nie znaleziono warto�ci wyliczeniowej " +
                "o kodzie = "+arg1);
        }

        /**
         * Konwertuje przekazany obiekt do typu reprezentowanego przez
         * to pole.  Je�eli obiekt jest ju� ��danej klasy, nie s� w nim
         * dokonywane �adne zmiany z wyj�tkiem sytuacji, gdy jest to
         * napis przekraczaj�cy d�ugo�� pola - w�wczas warto�� jest skracana.
         * <p>
         * Dla pola typu ENUM klas� jest java.lang.Integer.
         * <p>
         * Je�eli warto�� nie mo�e by� bezb��dnie skonwertowana (np.
         * niepoprawna tekstowa posta� daty), rzucany jest wyj�tek
         * FieldValueCoerceException.
         * @param value
         * @return
         * @throws FieldValueCoerceException
         */
        public Object coerce(Object value) throws FieldValueCoerceException
        {
            if (STRING.equals(type))
            {
                if (value == null)
                    return null;
                String s = String.valueOf(value);
                if (s.length() > length.intValue())
                    s = s.substring(0, length.intValue());
                return s;
            }
            else if (DATE.equals(type))
            {
                if (value == null)
                    return null;

                Date date;
                if (!(value instanceof Date))
                {
                    try
                    {
                        date = DateUtils.parseJsDate(String.valueOf(value));
                        if (!DSApi.isLegalDbDate(date))
                            throw new FieldValueCoerceException("Przekazana data nie znajduje si� " +
                                "w zakresie wymaganym przez baz� danych: "+DateUtils.formatJsDate(date));
                    }
                    catch (FieldValueCoerceException e)
                    {
                        throw e;
                    }
                    catch (Exception e)
                    {
                        log.error(e.getMessage(), e);
                        throw new FieldValueCoerceException("Nie mo�na skonwertowa� warto�ci '"+value+"' do daty");
                    }
                }
                else
                {
                    date = (Date) value;
                    if (!DSApi.isLegalDbDate(date))
                        throw new FieldValueCoerceException("Przekazana data nie znajduje si� " +
                            "w zakresie wymaganym przez baz� danych: "+DateUtils.formatJsDate(date));
                }
                return date;
            }
            else if (BOOL.equals(type))
            {
                if (value == null)
                    return null;

                return Boolean.valueOf(String.valueOf(value));
            }
            else if (ENUM.equals(type))
            {
                if (value == null)
                    return null;

                if (value instanceof String &&
                    TextUtils.trimmedStringOrNull((String) value) == null)
                    return null;

                if (value instanceof Integer)
                    return value;

                try
                {
                    return new Integer(String.valueOf(value));
                }
                catch (NumberFormatException e)
                {
                    throw new FieldValueCoerceException("Nie mo�na skonwertowa� warto�ci "+value+" do liczby ca�kowitej");
                }
            }
            else if (INTEGER.equals(type))
            {
                if (value == null)
                    return null;

                if (value instanceof Integer)
                    return value;

                try
                {
                    return new Integer(Integer.parseInt(value.toString()));
                }
                catch (NumberFormatException e)
                {
                    throw new FieldValueCoerceException("Nie mo�na skonwertowa� warto�ci "+value+" do liczby ca�kowitej");
                }
            }
            else if (LONG.equals(type))
            {
                if (value == null)
                    return null;

                if (value instanceof Long)
                    return value;

                try
                {
                    return new Long(Long.parseLong(value.toString()));
                }
                catch (NumberFormatException e)
                {
                    throw new FieldValueCoerceException("Nie mo�na skonwertowa� warto�ci "+value+" do liczby ca�kowitej");
                }
            }
            else if (AGENT.equals(type)) {
                if(value == null) return null;

                if(value instanceof Long) return value;
                try {
                    return new Long(Long.parseLong(value.toString()));
                } catch(NumberFormatException e) {
                    throw new FieldValueCoerceException("Nie mo�na skonwertowa� warto�ci "+value+" do liczby ca�kowitej");
                }
            }
            else if (AGENCJA.equals(type)) {
                if(value == null) return null;

                if(value instanceof Long) return value;
                try {
                    return new Long(Long.parseLong(value.toString()));
                } catch(NumberFormatException e) {
                    throw new FieldValueCoerceException("Nie mo�na skonwertowa� warto�ci "+value+" do liczby ca�kowitej");
                }
            }
            else
            {
                throw new IllegalStateException("Nieznany typ pola: "+type);
            }
        }

        void set(PreparedStatement ps, int index, Object value)
            throws FieldValueCoerceException, SQLException
        {
            Object coerced = coerce(value);

            if (coerced == null)
            {
                ps.setObject(index, null);
                return;
            }
            if (STRING.equals(type))
            {
                    ps.setString(index, (String) coerced);
            }
            else if (DATE.equals(type))
            {

                    ps.setDate(index, new java.sql.Date(((Date) coerced).getTime()));
            }
            else if (BOOL.equals(type))
            {
                    ps.setBoolean(index, ((Boolean) coerced).booleanValue());
            }
            else if (ENUM.equals(type))
            {
                    ps.setInt(index, ((Integer) coerced).intValue());
            }
            else if (INTEGER.equals(type))
            {
                    ps.setInt(index, ((Integer) coerced).intValue());
            }
            else if (LONG.equals(type))
            {
                    ps.setLong(index, ((Long) coerced).longValue());
            }
             else if (AGENT.equals(type))
            {
                    ps.setLong(index, ((Long) coerced).longValue());
            }
            else if (AGENCJA.equals(type))
            {
                    ps.setLong(index, ((Long) coerced).longValue());
            }
            // nie sprawdzam, czy pole jest nieznanego typu, bo robi to wywo�anie coerce()
        }

        public Integer getId()
        {
            return id;
        }

        public String getCn()
        {
            return cn;
        }

        public String getColumn()
        {
            return column;
        }

        public String getName()
        {
            return name;
        }

        public String getType()
        {
            return type;
        }

        public Integer getLength()
        {
            return length;
        }
    }

    /**
     * Tworzy tabel� o nazwie {@link #tablename}. Je�eli tabela ju�
     * istnia�a, jest usuwana i tworzona ponownie.
     * @throws EdmException
     */
    public void commit() throws EdmException
    {
        if (inUse())
            throw new EdmException("Typ dokumentu jest u�ywany, nie mo�na ponownie utworzy� tabeli");

        Field[] doctypeFields = getFieldsAsArray();
        if (doctypeFields.length == 0)
            throw new EdmException("Typ dokumentu nie zawiera p�l");

        Dialect dialect = DSApi.getDialect();

        StringBuilder ddl = null;
        try
        {
            ddl = new StringBuilder("CREATE TABLE "+tablename+"(\n");
            ddl.append("DOCUMENT_ID NUMERIC(18,0) NOT NULL,");
            for (int i=0; i < doctypeFields.length; i++)
            {
                ddl.append(doctypeFields[i].getColumn());
                ddl.append(" ");
                if (Field.STRING.equals(doctypeFields[i].getType()))
                {
                    ddl.append(dialect.getTypeName(Types.VARCHAR, doctypeFields[i].getLength().intValue(),0,0));
                    //ddl.append(varchar+"("+doctypeFields[i].getLength()+")");
                }
                else if (Field.DATE.equals(doctypeFields[i].getType()))
                {
                    ddl.append(dialect.getTypeName(Types.DATE));
                    //ddl.append(date);
                }
                else if (Field.BOOL.equals(doctypeFields[i].getType()))
                {
                    ddl.append(dialect.getTypeName(Types.SMALLINT));
                    //ddl.append(bool);
                }
                else if (Field.ENUM.equals(doctypeFields[i].getType()))
                {
                    ddl.append(dialect.getTypeName(Types.INTEGER));
                    //ddl.append(integer);
                }
                else if (Field.INTEGER.equals(doctypeFields[i].getType()))
                {
                    ddl.append(dialect.getTypeName(Types.INTEGER));
                }
                else if (Field.LONG.equals(doctypeFields[i].getType()))
                {
                    ddl.append(dialect.getTypeName(Types.BIGINT));
                }
                else if (Field.AGENCJA.equals(doctypeFields[i].getType()))
                {
                    ddl.append(dialect.getTypeName(Types.BIGINT));
                }
                else if (Field.AGENT.equals(doctypeFields[i].getType()))
                {
                    ddl.append(dialect.getTypeName(Types.BIGINT));
                }
                if (i < doctypeFields.length-1) ddl.append(",");
            }
            ddl.append(")");
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }

        if (log.isDebugEnabled())
            log.debug("commit: "+ddl);

        Statement st = null;
        try
        {
            Connection conn = DSApi.context().session().connection();
            st = conn.createStatement();

            ResultSet rs = conn.getMetaData().getTables(null, null, tablename, null);
            if (rs.next())
            {
                st.executeUpdate("DROP TABLE "+tablename);
            }

            st.executeUpdate(ddl.toString());
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            if (st != null) try { st.close(); } catch (Exception e) { }
        }
    }

    public void release() throws EdmException
    {
        Statement st = null;
        try
        {
            Connection conn = DSApi.context().session().connection();
            st = conn.createStatement();
            st.executeUpdate("update "+DSApi.getTableName(Document.class)+
                " set doctype = NULL where doctype = "+id);

            ResultSet rs = conn.getMetaData().getTables(null, null, tablename, null);
            if (rs.next())
            {
                st.executeUpdate("DROP TABLE "+tablename);
            }

            enabled = false;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            if (st != null) try { st.close(); } catch (Exception e) { }
        }
    }

    //private DateFormat dateTypeFormat = new SimpleDateFormat("dd-MM-yyyy");

    public void set(Long documentId, Map valueMap) throws EdmException
    {
        for (Iterator iter=getFields().iterator(); iter.hasNext(); )
        {
            Field field = (Field) iter.next();
            set(documentId, field.getId(), valueMap.get(field.getId()));
        }
    }

    public void set(Long documentId, Integer fieldId, Object value) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            Field field = getFieldById(fieldId);

            ps = DSApi.context().prepareStatement(
                "update "+getTablename()+" set "+field.getColumn()+" = ? "+
                " where document_id = ?");
            field.set(ps, 1, value);
            ps.setLong(2, documentId.longValue());

            // je�eli nie zaktualizowano p�l, nale�y je doda� (INSERT)
            if (ps.executeUpdate() < 1)
            {
            	DSApi.context().closeStatement(ps);                
                ps = DSApi.context().prepareStatement(
                    "insert into "+getTablename()+" (document_id, "+field.getColumn()+") " +
                    "values (?, ?)");
                ps.setLong(1, documentId.longValue());
                field.set(ps, 2, value);
                //setDoctypeFields(ps, field, 2, value);
                ps.executeUpdate();
            }
            //zamkniecie kontekstu w finally
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }

    public void set(Long documentId, String cn, Object value) throws EdmException
    {
        set(documentId, getFieldByCn(cn).getId(), value);
    }

    public void remove(Long documentId) throws EdmException
    {
        if (documentId == null)
            throw new NullPointerException("documentId");

        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("delete from "+getTablename()+" where document_id=?");
            pst.setLong(1, documentId);
            pst.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }


    /**
     * Zwraca tablic� warto�ci p�l indeksowan� ich identyfikatorami.
     * @return
     * @throws EdmException
     */
    public Map<Integer, Object> getValueMap(Long documentId) throws EdmException
    {
        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select * from "+getTablename()+" where document_id = ?");
            pst.setLong(1, documentId);
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
            {
                return new HashMap<Integer, Object>();
            }
            else
            {
                Map<Integer, Object> valueMap = new HashMap<Integer, Object>();
                Field[] doctypeFields = getFieldsAsArray();
                for (int i=0; i < doctypeFields.length; i++)
                {
                    valueMap.put(doctypeFields[i].getId(), getFieldValue(rs, doctypeFields[i],
                        doctypeFields[i].getColumn()));
                }
                return valueMap;
            }
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }

    /**
     * Zwraca tablic� warto�ci p�l indeksowan� ich atrybutami cn.  Je�eli
     * dla danego dokumentu nie istniej� rozszerzone atrybuty tego typu,
     * zwraca pust� kolekcj�.
     * @return
     * @throws EdmException
     */
    public Map<String, Object> getValueMapByCn(Long documentId) throws EdmException
    {
        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select * from "+getTablename()+" where document_id = ?");
            pst.setLong(1, documentId);
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
            {
                return Collections.emptyMap();
            }
            else
            {
                Map<String, Object> valueMap = new HashMap<String, Object>();
                Field[] doctypeFields = getFieldsAsArray();
                for (int i=0; i < doctypeFields.length; i++)
                {
                    if (doctypeFields[i].getCn() != null)
                    {
                        valueMap.put(doctypeFields[i].getCn(), getFieldValue(rs, doctypeFields[i],
                            doctypeFields[i].getColumn()));
                    }
                }
                return valueMap;
            }
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }

    private Object getFieldValue(ResultSet rs, Field field, String column)
        throws SQLException, EdmException
    {
        if (Field.STRING.equals(field.getType()))
        {
            return rs.getString(column);
        }
        else if (Field.DATE.equals(field.getType()))
        {
            java.sql.Date date = rs.getDate(column);
            return date != null ? new java.util.Date(date.getTime()) : null;
        }
        else if (Field.BOOL.equals(field.getType()))
        {
            boolean bool = rs.getBoolean(column);
            return rs.wasNull() ? null : Boolean.valueOf(bool);
        }
        else if (Field.ENUM.equals(field.getType()))
        {
            int i = rs.getInt(column);
            return rs.wasNull() ? null : new Integer(i);
        }
        else if (Field.INTEGER.equals(field.getType()))
        {
            int i = rs.getInt(column);
            return rs.wasNull() ? null : new Integer(i);
        }
        else if (Field.LONG.equals(field.getType()))
        {
            long l = rs.getLong(column);
            return rs.wasNull() ? null : new Long(l);
        }
         else if (Field.AGENCJA.equals(field.getType()))
        {
            long l = rs.getLong(column);
            return rs.wasNull() ? null : new Long(l);
        }
        else if (Field.AGENT.equals(field.getType()))
        {
            long l = rs.getLong(column);
            return rs.wasNull() ? null : new Long(l);
        }
        else
        {
            throw new EdmException("Nieznany typ pola: "+field.getType());
        }
    }

    public int getDocumentCount() throws EdmException
    {
        int count = countDocuments();
        return count >= 0 ? count : 0;
    }

    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }

    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getTablename()
    {
        return tablename;
    }

    private void setTablename(String tablename)
    {
        this.tablename = tablename;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    /* Lifecycle */

    public boolean onSave(Session s) throws CallbackException
    {
        if (id != null)
        {
            tablename = TABLE_NAME_PREFIX + id;
        }
        else
        {
            try
            {
                List count = DSApi.context().classicSession().find("select count(d) from d in class "+
                    getClass().getName());
                if (count != null && count.size() > 0 && count.get(0) instanceof Integer)
                {
                    tablename = TABLE_NAME_PREFIX + (((Integer) count.get(0)).intValue() + 1);
                }
                else
                {
                    tablename = TABLE_NAME_PREFIX + "1";
                }
            }
            catch (HibernateException e)
            {
                throw new CallbackException(e.getMessage(), e);
            }
            catch (EdmException e)
            {
                throw new CallbackException(e.getMessage(), e);
            }
        }
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Doctype)) return false;

        final Doctype doctype = (Doctype) o;

        if (id != null ? !id.equals(doctype.getId()) : doctype.getId() != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }
}
