package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LockFailureException.java,v 1.1 2004/05/16 20:10:23 administrator Exp $
 */
public class LockFailureException extends EdmException
{
    public LockFailureException(String message)
    {
        super(message);
    }

    public LockFailureException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public LockFailureException(Throwable cause)
    {
        super(cause);
    }
}
