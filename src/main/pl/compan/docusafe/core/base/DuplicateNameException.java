package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DuplicateNameException.java,v 1.1 2004/05/16 20:10:22 administrator Exp $
 */
public class DuplicateNameException extends EdmException
{
    public DuplicateNameException(String message)
    {
        super(message);
    }

    public DuplicateNameException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DuplicateNameException(Throwable cause)
    {
        super(cause);
    }
}
