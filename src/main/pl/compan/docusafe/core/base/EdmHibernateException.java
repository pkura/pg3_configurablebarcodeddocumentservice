package pl.compan.docusafe.core.base;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EdmHibernateException.java,v 1.3 2006/12/07 13:51:05 mmanski Exp $
 */
public class EdmHibernateException extends EdmException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    private String debugMessage;

    public static String getMessage(HibernateException cause)
    {
        return sm.getString("EdmHibernateException", cause.getMessage());
    }

    public EdmHibernateException(HibernateException cause)
    {
        super(getMessage(cause), cause);
    }

    /**
     * @param cause Wyj�tek �r�d�owy.
     * @param debugMessage Komunikat, kt�ry nie zostanie zwr�cony przez
     *  getMessage(), ale zostanie pokazany przez toString().
     */
    public EdmHibernateException(HibernateException cause, String debugMessage)
    {
        super(getMessage(cause), cause);
        this.debugMessage = debugMessage;
    }

    public String toString()
    {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        return message != null ?
            (s + ": "+ message + " (" + debugMessage +")") :
            (s + ": " + debugMessage);
    }
}
