package pl.compan.docusafe.core.base;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ExtendedAttributes.java,v 1.1 2004/05/16 20:10:22 administrator Exp $
 */
public class ExtendedAttributes implements DynaBean
{

    /* DynaBean */

    public boolean contains(String s, String s1)
    {
        return false;
    }

    public Object get(String s)
    {
        return null;
    }

    public Object get(String s, int i)
    {
        return null;
    }

    public Object get(String s, String s1)
    {
        return null;
    }

    public DynaClass getDynaClass()
    {
        return null;
    }

    public void remove(String s, String s1)
    {
    }

    public void set(String s, Object o)
    {
    }

    public void set(String s, int i, Object o)
    {
    }

    public void set(String s, String s1, Object o)
    {
    }
}
