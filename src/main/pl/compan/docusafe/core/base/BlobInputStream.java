package pl.compan.docusafe.core.base;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Statement;

/**
 * Strumie� danych pochodz�cy z obiektu {@link java.sql.Blob}.
 * Od zwyczajnego obiektu InputStream r�ni go spos�b zamykania;
 * strumie� musi by� zamkni�ty przy pomocy metody {@link #close},
 * kt�ra jednocze�nie zamyka obiekt Statement u�yty do pobrania
 * Bloba z bazy danych.
 * <p>
 * Zezwalanie aplikacji na zamykanie strumienia (i Statement) we
 * w�asnym zakresie jest niebezpieczne, poniewa� mo�e prowadzi�
 * do niepozamykanych zapyta�.  W tym celu zaimplementowano metod�
 * finalize, kt�ra zapisze w logach informacje o miejscu, gdzie
 * tworzony jest niezamykany obiekt BlobInputStream.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: BlobInputStream.java,v 1.8 2008/10/29 20:26:08 wkuty Exp $
 */
public final class BlobInputStream extends FilterInputStream
{
    private Statement statement;
    /**
     * Obiekt Throwable tworzony w konstruktorze w celu zapami�tania
     * miejsca w kodzie �r�d�owym, gdzie utworzono BlobInputStream.
     * Je�eli w metodzie closeStream() wyst�pi wyj�tek, w logach
     * zostanie wypisana zawarto�� obiektu Throwable, co pomo�e
     * w znalezieniu miejsca, gdzie strumie� jest pozostawiany
     * w stanie otwartym.
     */
    private Throwable t;

    public BlobInputStream(InputStream in, Statement statement)
    {
        super(in);
        this.statement = statement;
        this.t = new Throwable();
    }

    /**
     * Zamyka strumie� i obiekt Statement, kt�ry pos�u�y�
     * do odczytania Bloba.
     * @throws IOException
     */
    public void close() throws IOException
    {
        try
        {
        	DSApi.context().closeStatement(statement);
            statement = null;
        }        
        finally
        {
            super.close();
        }
    }

    /**
     * Logowanie w tej funkcji pozwoli wy�apa� miejsca, w kt�rych strumie�
     * nie jest zamykany przez aplikacj�.
     * @throws Throwable
     */
    protected void finalize() throws Throwable
    {
        try
        {
            if (statement != null)
            {
                try
                {
                    close();
                }
                catch (IOException e)
                {
                    LogFactory.getLog(BlobInputStream.class).error("", e);
                    LogFactory.getLog(BlobInputStream.class).error("Gdzie utworzono obiekt", t);
                    LogFactory.getLog("eprint").debug("", e);
                    LogFactory.getLog("eprint").debug("", t);
                    System.err.println("B��d podczas zamykania strumienia:");
                    LogFactory.getLog("eprint").debug("", e);
                    LogFactory.getLog("eprint").debug("", t);
                }
            }
        }
        finally
        {
            super.finalize();
        }
    }
}
