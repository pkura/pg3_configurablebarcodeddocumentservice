package pl.compan.docusafe.core.base;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import bsh.This;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;



	@Entity
	@Table(name = "DSO_OFFICE_PREFIX")
	public class OfficePointPrefix implements Serializable {

	    private static final long serialVersionUID = 1L;
	    
	    public OfficePointPrefix(){
	    	
	    }    
	    
		public OfficePointPrefix(Boolean available, Long id, String prefixName) {
			super();
			this.available = available;
			this.id = id;
			this.prefixName = prefixName;
		}

		private Boolean available;
	    private Long id;
	    private String prefixName;
		
	    @Id
	    @GeneratedValue
	    @Column(name = "id", nullable = false)
	    public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

	    @Column(name = "prefixName",length = 10,nullable = false) 
	    public String getPrefixName() {
			return prefixName;
		}

		public void setPrefixName(String prefixName) {
			this.prefixName = prefixName;
		}
	    @Column(name = "available", nullable = false)
	    public Boolean getAvailable() {
			return available;
		}
	    public void setAvailable(Boolean available) {
			this.available = available;
		}
		public void save() throws EdmException {
	        try {
	            DSApi.context().session().save(this);
	        } catch (HibernateException e) {
	            throw new EdmHibernateException(e);
	        }
	    }

		public static OfficePointPrefix findById(Long selectedPref) throws EdmException {
			Criteria  crit = DSApi.context().session().createCriteria(OfficePointPrefix.class);
			crit.add(Restrictions.eq("id", selectedPref));
			if(!crit.list().isEmpty())
			return (OfficePointPrefix) crit.list().get(0);
			else 
				return null;
			}

}
