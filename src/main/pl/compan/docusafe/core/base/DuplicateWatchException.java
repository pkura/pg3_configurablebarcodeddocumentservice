package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DuplicateWatchException.java,v 1.1 2004/06/27 20:56:52 administrator Exp $
 */
public class DuplicateWatchException extends EdmException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public DuplicateWatchException(Long documentId, String username)
    {
        super(sm.getString("DuplicateWatchException", documentId, username));
    }
}
