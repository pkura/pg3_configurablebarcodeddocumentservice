package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LockTimeoutException.java,v 1.1 2004/05/16 20:10:23 administrator Exp $
 */
public class LockTimeoutException extends EdmException
{
    public LockTimeoutException(String message)
    {
        super(message);
    }

    public LockTimeoutException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public LockTimeoutException(Throwable cause)
    {
        super(cause);
    }
}
