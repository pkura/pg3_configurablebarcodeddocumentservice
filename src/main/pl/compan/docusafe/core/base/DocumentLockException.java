package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentLockException.java,v 1.1 2004/11/01 14:55:05 lk Exp $
 */
public class DocumentLockException extends EdmException
{
    public DocumentLockException(DocumentLockMode mode, Long id)
    {
        super("Nie mo�na zablokowa� dokumentu "+id+" w trybie "+mode);
    }
}
