package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.StringManager;

import java.io.PrintStream;
import java.sql.SQLException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EdmSQLException.java,v 1.4 2009/02/09 08:37:10 mariuszk Exp $
 */
public class EdmSQLException extends EdmException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    private String debugMessage;

    public static String getMessage(SQLException cause)
    {
        return sm.getString("EdmSQLException", cause.getMessage());
    }

    public EdmSQLException(SQLException cause)
    {
        super(getMessage(cause), cause);
    }
    
    public EdmSQLException(String msg,SQLException cause)
    {
        super(msg, cause);
    }

    /**
     * @param cause Wyj�tek �r�d�owy.
     * @param debugMessage Komunikat, kt�ry nie zostanie zwr�cony przez
     *  getMessage(), ale zostanie pokazany przez toString().
     */
    public EdmSQLException(SQLException cause, String debugMessage)
    {
        super(getMessage(cause), cause);
    }

    public void printStackTrace(PrintStream s)
    {
        super.printStackTrace(s);
    }

    public String toString()
    {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        return message != null ?
            (s + ": "+ message + " (" + debugMessage +")") :
            (s + ": " + debugMessage);
    }
}
