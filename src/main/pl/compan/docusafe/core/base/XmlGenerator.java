package pl.compan.docusafe.core.base;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class XmlGenerator {


	private final static Logger log = LoggerFactory.getLogger(XmlGenerator.class);
    private final OfficeDocument document;
    private byte[] xml;
    private String sha1;
    private Document xmlDoc;

    public XmlGenerator(OfficeDocument document) {
        this.document = document;
    }

    /**
     * <p>
     *     Tworzy dokument XML i zapisuje go wewn�trz obiektu.
     *     Metoda zwraca SHA1 z xml.
     * </p>
     * @return
     * @throws TransformerException
     * @throws EdmException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    public XmlGenerator generateXml() throws TransformerException, EdmException, IOException, ParserConfigurationException {
        Long documentId = document.getId();

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        xmlDoc = docBuilder.newDocument();

        Element rootElement = xmlDoc.createElement("document");
        rootElement.setAttribute("id",documentId.toString());
        
        Element baseElement = xmlDoc.createElement("base-info");
        appendBaseInfo(baseElement);
        rootElement.appendChild(baseElement);

        Element authorElement = xmlDoc.createElement("author");
        appendBaseAuthor(authorElement);
        rootElement.appendChild(authorElement);

        Element specificElement = xmlDoc.createElement("additional-data");
        appendAdditionalData(specificElement);
        rootElement.appendChild(specificElement);

        Element attachments = xmlDoc.createElement("attachments");
        appendAttachments(attachments);
        rootElement.appendChild(attachments);

        // generate uuid to guarantee uniqueness of xml
//    Element generateId = xmlDoc.createElement("generateId");
//    String uuid = UUID.randomUUID().toString();
//    generateId.setTextContent(uuid);
//    rootElement.appendChild(generateId);
        addData(xmlDoc, rootElement);

        
        xmlDoc.appendChild(rootElement);
        addXls(xmlDoc, rootElement);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(xmlDoc);
        StringWriter stringWriter = new StringWriter();
        StreamResult result = new StreamResult(stringWriter);

        transformer.transform(source, result);
        
        xml = stringWriter.toString().getBytes(Charset.forName("UTF-8"));
        sha1 = DigestUtils.shaHex(xml);
     
        return this;
    }
 
    public void addData(Document xmlDoc, Element rootElement)
	{
		
	}
    public void addXls(Document xmlDoc, Element rootElement)
   	{
   		
   	}

	public void createElementWithTextValue(Element rootElement, String elementName, String elementValue) {
        Element element = xmlDoc.createElement(elementName);
        element.setTextContent(elementValue);
        rootElement.appendChild(element);
    }

    private void appendBaseInfo(Element rootElement) throws EdmException {
        createElementWithTextValue(rootElement, "creation-date", DateUtils.formatCommonDate(document.getCtime()));
        createElementWithTextValue(rootElement, "summary", document.getSummary());
        createElementWithTextValue(rootElement, "title", document.getTitle());
        Element remarks = xmlDoc.createElement("remarks");
        for (Remark remark: document.getRemarks()){
            createElementWithTextValue(remarks, "remark_"+remark.getId(), remark.getContent());
        }
    }


    private void appendBaseAuthor(Element rootElement) throws EdmException {
        try{
            DSUser user = DSUser.findByUsername(document.getAuthor());
            createElementWithTextValue(rootElement, "firstname", user.getFirstname());
            createElementWithTextValue(rootElement, "lastname", user.getLastname());
            createElementWithTextValue(rootElement, "externalname", user.getExternalName());
        } catch (UserNotFoundException e){
            log.error("User not found: " + e);
        }
        try{
            createElementWithTextValue(rootElement, "division", DSDivision.find(document.getAssignedDivision()).getName());
        } catch (DivisionNotFoundException e) {
            log.error("Division not found: " + e);
        }
    }


    //    createElementWithTextValue(rootElement,xmlDoc,entry.getKey(),delivery.getName());
//    Map<String, Object> map = DocumentSnapshotSerializer.getMapFromDocumentNew(document);
//    DocumentKind dk = document.getDocumentKind();
    private void appendAttachments(Element attachments) throws EdmException, IOException {

        String nonBase64encoding = AdditionManager.getProperty("jackrabbit.metadata.notBase64Charset");
        List<String> nonBase64ContentTypes = AdditionManager.getPropertyAsList("jackrabbit.metadata.notBase64");

        for(Attachment attachment: document.getAttachments()) {
            AttachmentRevision rev = attachment.getMostRecentRevision();

            rev.getMime();

            Element contentElement = xmlDoc.createElement("content");
            if(asText(rev, nonBase64ContentTypes)) {
                String content = getText(rev, nonBase64encoding);
                CDATASection cdataSection = xmlDoc.createCDATASection(content);
                contentElement.appendChild(cdataSection);
            } else {
                String content = Base64.encodeBase64String(IOUtils.toByteArray(rev.getAttachmentStream()));
                contentElement.setTextContent(content);
            }

            Element attachmentElement = xmlDoc.createElement("attachment");
            attachmentElement.setAttribute("id", attachment.getId().toString());
            createElementWithTextValue(attachmentElement, "title", attachment.getTitle());
            createElementWithTextValue(attachmentElement, "mime", rev.getMime());
            createElementWithTextValue(attachmentElement, "original-filename", rev.getOriginalFilename());

            attachmentElement.appendChild(contentElement);

            attachments.appendChild(attachmentElement);
        }
    }

    private String getText(AttachmentRevision rev, String encoding) throws EdmException, IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(rev.getAttachmentStream(), writer, encoding);
        return writer.toString();
    }

    private boolean asText(AttachmentRevision rev, List<String> nonBase64ContentTypes) {
        if(rev.getMime() == null) {
            return false;
        } else {
            String mime = rev.getMime();

            if(nonBase64ContentTypes.indexOf(mime) == -1) {
                return false;
            } else {
                return true;
            }
        }
    }

    private void appendAdditionalData(Element rootElement) throws EdmException {
        DocumentKind dk = document.getDocumentKind();
        Map<String,Object> map = document.getFieldsManager().getFieldValues();


        for (Map.Entry<String,Object> entry: map.entrySet()){
            if(entry.getValue()!=null){
                if (entry.getValue() instanceof String){
                    String fieldType = dk.getFieldByCn(entry.getKey()).getType();
                    if ("document-office-delivery".equalsIgnoreCase(fieldType)){
                        if (document instanceof OutOfficeDocument){
                            OutOfficeDocumentDelivery delivery = OutOfficeDocumentDelivery.find(Integer.valueOf(entry.getValue().toString()));
                            createElementWithTextValue(rootElement, entry.getKey(), delivery.getName());
                        }else{
                            InOfficeDocumentDelivery delivery = InOfficeDocumentDelivery.find(Integer.valueOf(entry.getValue().toString()));
                            createElementWithTextValue(rootElement, entry.getKey(), delivery.getName());
                        }
                    }else if ("document-journal".equalsIgnoreCase(fieldType)){
                        Journal journal = Journal.find(Long.valueOf(entry.getValue().toString()));
                        createElementWithTextValue(rootElement, entry.getKey(), journal.getDescription());
                    } else{
                        createElementWithTextValue(rootElement, entry.getKey(), (String) entry.getValue());
                    }
                }else if (entry.getValue() instanceof Date){
                    createElementWithTextValue(rootElement, entry.getKey(),
                            DateUtils.formatCommonDate((Date) entry.getValue()));
                }else if (entry.getValue() instanceof Boolean){
                    createElementWithTextValue(rootElement, entry.getKey(),
                            entry.getValue().toString());
                }else if (entry.getValue() instanceof Integer){
                    String fieldType = dk.getFieldByCn(entry.getKey()).getType();
                    if ("dsuser".equalsIgnoreCase(fieldType)){
                        try {
                            DSUser user = DSUser.findById(((Integer) entry.getValue()).longValue());
                            createElementWithTextValue(rootElement, entry.getKey(),user.asFirstnameLastname());
                        }catch (Exception e){
                            log.error("Error: "+e);
                        }
                    }else if ("dsdivision".equalsIgnoreCase(fieldType)){
                        try {
                            DSDivision division = DSDivision.find((Integer) entry.getValue());
                            createElementWithTextValue(rootElement, entry.getKey(),division.getName());
                        }catch (Exception e){
                            log.error("Error: "+e);
                        }
                    } else if ("enum".equalsIgnoreCase(fieldType)){
                        Field field = dk.getFieldByCn(entry.getKey());
                        createElementWithTextValue(rootElement, entry.getKey(), field.getEnumItem((Integer) entry.getValue()).getTitle());
                    } else if ("dataBase".equalsIgnoreCase(fieldType)){
                        Field field = dk.getFieldByCn(entry.getKey());
                        createElementWithTextValue(rootElement, entry.getKey(), field.getEnumItem((Integer) entry.getValue()).getTitle());
                    } else{
                        createElementWithTextValue(rootElement, entry.getKey(), entry.getValue().toString());
                    }
                } else if (entry.getValue() instanceof BigDecimal){
                    createElementWithTextValue(rootElement, entry.getKey(), entry.getValue().toString());
                } else if (entry.getValue() instanceof Long){
                    String fieldType = dk.getFieldByCn(entry.getKey()).getType();

                    // handle document-person
                    if ("document-user-division-second".equalsIgnoreCase(fieldType) ||
                            "document-person".equalsIgnoreCase(fieldType)){
                        Person person = Person.findIfExist((Long) entry.getValue());

                        Element xmlPerson = personToXml(entry.getKey(), person);

                        rootElement.appendChild(xmlPerson);
                    } else{
                        createElementWithTextValue(rootElement, entry.getKey(),entry.getValue().toString());
                    }
                }
            }
        }
    }

    private Element personToXml(String key, Person person) {
        Element xmlPerson = xmlDoc.createElement(key);

        xmlPerson.setAttribute("id", String.valueOf(person.getId()));
        mapAppendToXml(xmlPerson, personToMap(person));

        return xmlPerson;
    }

    public Map<String, String> personToMap(Person person) {
        Map<String, String> map = new HashMap<String, String>();

        map.put("title", person.getTitle());
        map.put("firstname", person.getFirstname());
        map.put("lastname", person.getLastname());
        map.put("organization", person.getOrganization());
        map.put("street", person.getStreet());
        map.put("zip", person.getZip());
        map.put("location", person.getLocation());
        map.put("country", person.getCountry());
        map.put("email", person.getEmail());
        map.put("phoneNumber", person.getPhoneNumber());
        map.put("fax", person.getFax());
        map.put("pesel", person.getPesel());
        map.put("nip", person.getRegon());
        map.put("remarks", person.getRemarks());

        return map;
    }

    private void mapAppendToXml(Element root, Map<String, String> map) {
        for(Map.Entry<String, String> entry : map.entrySet()) {
            Element element = xmlDoc.createElement(entry.getKey());
            element.setTextContent(entry.getValue());
            root.appendChild(element);
        }
    }

    public String getSha1() {
        return sha1;
    }

    public byte[] getXml() {
        return xml;
    }

	
	public Document getXmlDoc()
	{
		return xmlDoc;
	}

	
	public void setXmlDoc(Document xmlDoc)
	{
		this.xmlDoc = xmlDoc;
	}

}
