package pl.compan.docusafe.core.base;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import org.apache.catalina.tribes.util.StringManager;
import org.apache.commons.dbutils.DbUtils;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.record.contracts.Contract;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class AttachmentHelper {

	private static final Logger log = LoggerFactory.getLogger(AttachmentHelper.class);
	private static StringManager sm = StringManager.getManager(Constants.Package);

	public SearchResults getAttachment(QueryForm queryForm) throws EdmException {
		
		FromClause from = new FromClause();
		TableAlias att = from.createTable(DSApi.getTableName(Attachment.class),true);
		TableAlias attRe = from.createJoinedTable(DSApi.getTableName(AttachmentRevision.class), true, att,FromClause.LEFT_OUTER, "$l.id=$r.ATTACHMENT_ID");
		TableAlias doc = from.createJoinedTable(DSApi.getTableName(Document.class), true, att, FromClause.LEFT_OUTER, "$l.DOCUMENT_ID=$r.ID");
		WhereClause where = new WhereClause();
	    Junction OR = Expression.disjunction();
		if (queryForm.hasProperty("title")) {
			String title = (String) queryForm.getProperty("title");
			String titleCol = DSApi.getColumnName(Attachment.class, "title");
			OR.add(Expression.like(Function.upper(att.attribute(titleCol)),"%" + title + "%"));
		}

		if (queryForm.hasProperty("originalFilename")) {
			String originalFilename = (String) queryForm.getProperty("originalFilename");
			String originalFilenameCol = DSApi.getColumnName(AttachmentRevision.class, "originalFilename");
			OR.add(Expression.like(	Function.upper(attRe.attribute(originalFilenameCol)), "%"+ originalFilename + "%"));
		}
		where.add(OR);
		
		where.add(Expression.eq(att.attribute("deleted"), Boolean.FALSE));
		
		if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu")){
			where.add(Expression.eq(doc.attribute("czy_Aktualny"), Boolean.TRUE));
		}
		
		SelectClause selectId = new SelectClause(true);
		SelectClause selectCount = new SelectClause();

		SelectColumn idCol = selectId.add(attRe, "id");
		SelectColumn countCol = selectCount.addSql("count(distinct "
				+ att.getAlias() + ".id)");
		
		OrderClause orderClause = new OrderClause();

		if (queryForm.hasSortFields()) {
			for (int i = 0; i < queryForm.getSortFields().size(); i++) {
				QueryForm.SortField field = (QueryForm.SortField) queryForm
						.getSortFields().get(i);
				orderClause.add(attRe.attribute(DSApi.getColumnName(
						AttachmentRevision.class, field.getName())), field
						.isAscending() ? OrderClause.ASC : OrderClause.DESC);
				
				selectId.add(attRe,
						DSApi.getColumnName(AttachmentRevision.class, field.getName()));
			}
		}

		SelectQuery query;

		int totalCount;
		ResultSet rs = null;
		try {
			query = new SelectQuery(selectCount, from, where, null);

			rs = query.resultSet();
			if (!rs.next())
				throw new EdmException("Nie mo�na pobra� liczby dokument�w.");

			totalCount = rs.getInt(countCol.getPosition());

			rs.close();
			DSApi.context().closeStatement(rs.getStatement());

			query = new SelectQuery(selectId, from, where, orderClause);
			query.setMaxResults(queryForm.getLimit());
			query.setFirstResult(queryForm.getOffset());

			rs = query.resultSet();

			List<AttachmentRevision> results = new ArrayList<AttachmentRevision>(
					queryForm.getLimit());

			while (rs.next()) {
				Long id = new Long(rs.getLong(idCol.getPosition()));
				results.add(DSApi.context().load(AttachmentRevision.class, id));
			}

			rs.close();
			DSApi.context().closeStatement(rs.getStatement());

			return new SearchResultsAdapter<AttachmentRevision>(results,
					queryForm.getOffset(), totalCount, AttachmentRevision.class);

		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		} catch (SQLException e) {
			throw new EdmSQLException(e);
		} finally {
			DbUtils.closeQuietly(rs);
		}

	}
	 public static boolean createAttachment(Long docId, File file, String fileName, String title, String barcode, String fileExtension, boolean encrypted) throws EdmException{
	        Document document = Document.find(docId);
	        if (DocumentHelper.cannotUpdate(document)){
	            return false;
	        }
	        Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull(title, 254));
	        attachment.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
	        attachment.setFileExtension(fileExtension);
	        attachment.setEncrypted(encrypted);
	        document.createAttachment(attachment);
	        if(document instanceof InOfficeDocument){
	            ((InOfficeDocument)document).setNumAttachments(document.getAttachments().size());
	        }

	        attachment.createRevision(file).setOriginalFilename(fileName);
	        TaskSnapshot.updateAllTasksByDocumentId(document.getId(), document.getStringType());

	        return true;
	    }
    /**
     * Tworzy za��cznik, zwraca warto�� logiczn� czy si� to uda�o
     * @param docId
     * @param file
     * @param fileName
     * @param title
     * @return
     * @throws EdmException
     */
    public static boolean createAttachment(Long docId, File file, String fileName, String title, String barcode) throws EdmException{
        Document document = Document.find(docId);
        if (DocumentHelper.cannotUpdate(document)){
            return false;
        }
        Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull(title, 254));
        attachment.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
        document.createAttachment(attachment);
        if(document instanceof InOfficeDocument){
            ((InOfficeDocument)document).setNumAttachments(document.getAttachments().size());
        }

        attachment.createRevision(file).setOriginalFilename(fileName);
        TaskSnapshot.updateAllTasksByDocumentId(document.getId(), document.getStringType());

        return true;
    }
    /**
     * Usuwa za��czniki daneg odokumentu
     * @param attachmentIds
     * @throws EdmException
     */
    public static  void removeAttachments(Long docId, Long... attachmentIds) throws EdmException{
        Document document = Document.find(docId);
        if (DocumentHelper.cannotUpdate(document)){
            return;
        }

        for (Long id : attachmentIds)
        {
            document.removeAttachment(id);
        }
        if(document instanceof InOfficeDocument){
            ((InOfficeDocument)document).setNumAttachments(document.getAttachments().size());
        }
        if(Docusafe.moduleAvailable(Modules.MODULE_CONTRACTS) && document instanceof InOfficeDocument) {

            if(Contract.findByDocumentId(document.getId())==null){
                document.getDocumentKind().logic().onDeletedRevision(document);
            } else
                log.info("DokumentJestUmowa");
        }else {
            document.getDocumentKind().logic().onDeletedRevision(document);
        }

        TaskSnapshot.updateAllTasksByDocumentId(document.getId(), document.getStringType());
    }
}
