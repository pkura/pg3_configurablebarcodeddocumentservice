package pl.compan.docusafe.core.base.permission.rule;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Dost�p do dokumentu z listy zada�
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class OnTasklistRule extends AbstractPermissionRule {

    private final static Logger log = LoggerFactory.getLogger(OnTasklistRule.class);

    @Override
    public boolean validate(Document document) throws EdmException {
        return PermissionManager.isDocumentInTaskList(document);
    }

    @Override
    protected Logger getLog() {
        return log;
    }
}
