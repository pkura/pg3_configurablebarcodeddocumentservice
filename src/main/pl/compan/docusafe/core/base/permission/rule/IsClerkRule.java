package pl.compan.docusafe.core.base.permission.rule;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class IsClerkRule extends AbstractPermissionRule {

    private final static Logger log = LoggerFactory.getLogger(IsClerkRule.class);

    @Override
    public boolean validate(Document officeDoc) throws EdmException {
        if(officeDoc instanceof OfficeDocument){
            String clerk = ((OfficeDocument) officeDoc).getClerk();
            if (StringUtils.isNotBlank(clerk) && DSApi.context().getPrincipalName().equals(clerk)) {
                return true;
            }

        }
        return false;
    }

    @Override
    protected Logger getLog() {
        return log;
    }
}
