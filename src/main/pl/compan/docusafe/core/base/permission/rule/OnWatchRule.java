package pl.compan.docusafe.core.base.permission.rule;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Dost�p do dokumentu z Listy obserwowane
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class OnWatchRule implements PermissionRule {

    private final static Logger log = LoggerFactory.getLogger(OnWatchRule.class);

    @Override
    public boolean validateRead(Document document) throws EdmException {
        boolean ok = DSApi.context().hasWatch(URN.create(document));
        log.trace("validateRead doc={} {}",document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateModify(Document document) throws EdmException {
        boolean ok = false;
        log.trace("validateModify doc={} {}",document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateReadAttachments(Document document) throws EdmException {
        boolean ok = DSApi.context().hasWatch(URN.create(document));
        log.trace("validateReadAttachments doc={} {}",document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateModifyAttachments(Document document) throws EdmException {
        boolean ok = false;
        log.trace("validateModifyAttachments doc={} {}",document.getId(), ok);
        return ok;
    }
}
