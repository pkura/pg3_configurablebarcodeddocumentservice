package pl.compan.docusafe.core.base.permission.rule;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * Warunek reguly uprawnie� dla autora dokumentu
 *
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class IsAuthorRule implements PermissionRule {
    @Override
    public boolean validateRead(Document document) throws EdmException {
        return false;
    }

    @Override
    public boolean validateModify(Document document) throws EdmException {
        if(isAuthor(document) && isNewStatus(document))
            return true;

        return false;
    }

    @Override
    public boolean validateReadAttachments(Document document) throws EdmException {
        return false;
    }

    @Override
    public boolean validateModifyAttachments(Document document) throws EdmException {
        if(isAuthor(document) && isNewStatus(document))
            return true;

        return false;
    }


    /**
     * Zwraca true jesli zalogowany user jest autorem
     * @param document
     * @return
     */
    private boolean isAuthor(Document document) {
        return document.isLoggedAuthor();
    }

    /**
     * Zwraca true jestli dokument am status new
     * @param document
     * @return
     */
    private boolean isNewStatus(Document document) {
        if(document instanceof OfficeDocument && ((OfficeDocument) document).isOfficeStatusNew())
            return true;

        return false;
    }
}
