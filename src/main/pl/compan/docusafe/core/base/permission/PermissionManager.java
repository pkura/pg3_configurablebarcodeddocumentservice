package pl.compan.docusafe.core.base.permission;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.annotations.Unchecked;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.rule.PermissionRule;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.adm.AdmDwrPermissionFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Interfejs definiuj�cy metody jakie s� u�ywane do kontroli uprawnie�.
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public abstract class PermissionManager implements Serializable {

    private final static Logger log = LoggerFactory.getLogger(PermissionManager.class);

    public final static int NORMAL_POLICY = 0;
    public final static int BUSINESS_POLICY = 1;
    public final static int BUSINESS_LIGHT_POLICY = 2;
    /**Polityka dokumentu leasingowego*/
    public final static int DL_POLICY = 3;

    /**
     * Inicjalizuje managera uprawnie� w zale�no�ci od wymaga� wdro�enia:
     * W addsach do ustawienia i nadpisania przyk�ad:
     * permission.class = pl.compan.docusafe.core.base.permission.ClassicPermissionManager
     * @return
     */
    public static PermissionManager getInstance() {
        PermissionManager pm = null;
        try{
            Class cc = Class.forName(Docusafe.getAdditionPropertyOrDefault("permission.class", "pl.compan.docusafe.core.base.permission.ClassicPermissionManager"));
            pm = (PermissionManager) cc.newInstance();
        }catch(ClassNotFoundException e){
            log.error("B��d initializacji definicji klasy definiuje ClassicPermissionMaanger");
            pm = new ClassicPermissionManager();
        } catch (Exception e) {
            log.error("", e);
        }

        return pm;
    }

    /**
     * Metoda sprawdza czy mo�e otworzy� dokument
     * @param document
     * @return
     * @throws EdmException
     */
    public abstract boolean canRead(Document document, Long binderId) throws EdmException;



    /**
     *  Metoda sprawdza uprawnienia modyfikacji do dokumentu.
     */
    public abstract boolean canModify(Document document, Long binderId) throws EdmException;

    /**
     * Metoda sprawdza czy moge czytac zalaczniki w dokumencie.
     */
    public abstract boolean canReadAttachments(Document document,Long binderId) throws EdmException;

    /**
     * Metoda sprawdza czy mog� modyfikowa� za��czniki
     * @param document
     * @return
     * @throws EdmException
     */
    public abstract boolean canModifyAttachments(Document document, Long binderId) throws EdmException ;


    /**
     * **************************************STATE METODY D OWYWALENIA W PRZYSZ�OSCI
     */

    /**
     * Metoda sprawdza czy moge czytac orginaly zalaczniki w dokumencie.
     */
    @Deprecated
    public boolean canReadOrginalAttachments(Document document,Long binderId) throws EdmException {
        log.trace("PermissionManager#canReadOrginalAttachments({})",document.getId());
        if(AvailabilityManager.isAvailable("perimission.READ_ORGINAL_ATTACHMENTS"))
        {
            if(document.getDocumentKind() == null || document.getDocumentKind().logic().getPermissionPolicy() == null){
                return defaultCanReadOrginalAttachments(document);
            } else {
                return document.getDocumentKind().logic().getPermissionPolicy().canReadOrginalAttachments(document,binderId);
            }
        }
        else
            return canReadAttachments(document, binderId);
    }

    /**
     *
     * @param officeDoc
     * @return
     * @throws EdmException
     */
    @Deprecated
    protected boolean canReadArchiveDocumentViaOfficePermissions(OfficeDocument officeDoc) throws EdmException
    {
        DSUser user = DSApi.context().getDSUser();
        if (DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD,
                                        DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE,
                                        DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD))
        {
            return true;
        }

        if (AvailabilityManager.isAvailable("search.withoutPermissionTable")){
            if(isDocumentInTaskList(officeDoc))
                return true;
            else
                return   AdmDwrPermissionFactory.canReadDocument(officeDoc);
        }

        if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD, DSPermission.PISMO_KOMORKA_MODYFIKACJE))
        {
            if (DSDivision.containsDivisionGuid(officeDoc.getDivisionGuid(), user.getDivisions()))
                 return true;
            
            if(AvailabilityManager.isAvailable("Permission.PismoKomorkaPodglad.newVersion")){
            	for(DSDivision division: user.getDivisions()){
            		for(DSUser userInDivision: division.getUsers(false)){
            			if(PermissionManager.isDocumentInTaskList(officeDoc.getId(), userInDivision))
            				return true;
            		}
            	}
            }
        }


        if (isDocumentInTaskList(officeDoc))
            return true;


        if (officeDoc.getClerk() != null && user.getName().equals(officeDoc.getClerk()))
        {
            return true;
        }

        if (officeDoc.getAuthor() != null && user.getName().equals(officeDoc.getAuthor()))
            return true;

        return false;
    }

    /**
     *
     * @param officeDoc
     * @return
     * @throws EdmException
     */
    @Deprecated
    protected boolean canModifyArchiveDocumentViaOfficePermissions(OfficeDocument officeDoc) throws EdmException {
        if (DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE)) {
            return true;
        }

        if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_MODYFIKACJE)) {
            if (DSDivision.containsDivisionGuid(officeDoc.getDivisionGuid(),  DSApi.context().getDSUser().getDivisions()))
                return true;
        }

        if (isDocumentInTaskList(officeDoc)) {
            return true;
        }

        return false;
    }



    /**
     * Sprawdza czy dany dokument znajduje si� na li�cie aktualnie zalogowanego u�ytkownika.
     */
    @Unchecked
    public static boolean isDocumentInTaskList(Document document) throws EdmException {
        return isDocumentInTaskList(document.getId());
    }

    /**
     * Sprawdza czy dany dokument znajduje si� na li�cie aktualnie zalogowanego u�ytkownika.
     */
    @Unchecked
    public static boolean isDocumentInTaskList(Long document) throws EdmException {
        Boolean response = false;
        response = isDocumentInTaskList(document, DSApi.context().getDSUser());

        if (!response)
        {
            DSUser[] users = DSApi.context().getDSUser().getSubstituted();
            for (DSUser user : users) {
                if (isDocumentInTaskList(document, user)) {
                    response = true;
                    break;
                }
            }
        }
        return response;
    }

    /**
     * Sprawdza czy dokument jest na liscie zadan
     * @param documentId
     * @param user
     * @return
     * @throws EdmException
     * @deprecated - na takie cos powinen odpowiadac WorkflowManager
     */
    protected static boolean isDocumentInTaskList(Long documentId, DSUser user) throws EdmException
    {
        if (log.isTraceEnabled())
        {
            log.trace("isDocumentInTaskList id={}, user={}",documentId,user.getName());
        }
        PreparedStatement ps = null;
        try
        {
            boolean resp = false;
            if ("true".equalsIgnoreCase(Docusafe.getAdditionProperty("addition.jbpm.available"))
                    || AvailabilityManager.isAvailable("tasklist.jbpm4"))
            {
                log.trace("jbpm");
                ps = DSApi.context().prepareStatement("select * from dsw_jbpm_tasklist where assigned_resource = ? and document_id = ?");
            }
            else
            {
                ps = DSApi.context().prepareStatement("select * from dsw_tasklist where dsw_resource_res_key = ? and dso_document_id = ?");
            }
            ps.setString(1, user.getName());
            ps.setLong(2, documentId);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                resp = true;
            }
            rs.close();
            log.trace("resp={}",resp);
            return resp;
        }
        catch (SQLException e)
        {
            throw new EdmException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
        }
    }

    /**
     * Czy uzytkownik ma prawo do modyfikowania dokumentu
     * @param document
     * @return
     * @throws EdmException
     */
    @Deprecated
    boolean defaultCanModify(Document document) throws EdmException {
        // zarchiwizowany dokument kancelaryjny nie moze byc modyfikowany
        log.trace("default.canModify {} isArchived{}", document.getId(), document.getIsArchived());
        if( document instanceof OfficeDocument && document.getIsArchived() != null && document.getIsArchived()
                && !DSApi.context().hasPermission(DSPermission.EDYCJA_ZARCHIWIZOWANYCH_DOKUMENTOW))
            return false;

        if (DSApi.context().isAdmin() || document.getId() == null) {
            return true;
        }
        if (Configuration.hasExtra("business") && document.getAuthor() != null && document.getAuthor().equals(DSApi.context().getPrincipalName())) {
            return true;
        }

        if (!document.isForceArchivePermissions() && document instanceof OfficeDocument) {
            if (canModifyArchiveDocumentViaOfficePermissions((OfficeDocument) document)) {
                return true;
            }
        }
        return DSApi.context().documentPermission(document.getId(), ObjectPermission.MODIFY);
    }

    /**
     * Czy uzytkownika ma prawo do czytania dokumentu
     * @param document
     * @return
     * @throws EdmException
     */
    @Deprecated
    boolean defaultCanRead(Document document) throws EdmException {
        if (document.getId() == null || DSApi.context().isAdmin()) {
            return true;
        }
        if (!document.isForceArchivePermissions() && document instanceof OfficeDocument) {
            if (canReadArchiveDocumentViaOfficePermissions((OfficeDocument) document)) {
                return true;
            }
        }
        return DSApi.context().documentPermission(document.getId(), ObjectPermission.READ);
    }

    @Deprecated
    boolean defaultCanReadAttachments(Document document) throws EdmException {
		/*
		 * Zasada jest taka
		 * Dokument tylko archiwalny - zgodnie z uprawnieniemi archiwum
		 * Dokument kancelaryjny - normalny lub null - zgodnie z uprawnienieami do pisma
		 * Dokument kancelaryjny o innym dockindzie - zgodnie z uprawnieniami archiwum
		 */
        if (document.getId() == null || DSApi.context().isAdmin()) {
            return true;
        }

        if (checkDocumentPolicyPermission(document))
            return true;

        return DSApi.context().documentPermission(document.getId(), ObjectPermission.READ_ATTACHMENTS);
    }

    @Deprecated
    boolean defaultCanReadOrginalAttachments (Document document) throws EdmException {
		/*
		 * Zasada jest taka
		 * Dokument tylko archiwalny - zgodnie z uprawnieniemi archiwum
		 * Dokument kancelaryjny - normalny lub null - zgodnie z uprawnienieami do pisma
		 * Dokument kancelaryjny o innym dockindzie - zgodnie z uprawnieniami archiwum
		 */
        if (document.getId() == null || DSApi.context().isAdmin()) {
            return true;
        }

        if (checkDocumentPolicyPermission(document))
           return true;

        return DSApi.context().documentPermission(document.getId(), ObjectPermission.READ_ORGINAL_ATTACHMENTS);
    }

    private boolean checkDocumentPolicyPermission(Document document) throws EdmException {
      if (document instanceof OfficeDocument) {
            int policy = document.getDocumentKind() == null ? NORMAL_POLICY : document.getDocumentKind().logic().getPermissionPolicyMode();
            if (policy == BUSINESS_LIGHT_POLICY)
            {
                if (canReadArchiveDocumentViaOfficePermissions((OfficeDocument) document)) {
                    return true;
                }
            }
            else if (policy == NORMAL_POLICY)
            {
                // w warunkach administracji publicznej taka polityka jest akceptowalna
                return true;
            }
      }
      return false;
    }

    @Deprecated
    boolean defaultCanModifyAttachments(Document document) throws EdmException
    {
        if (document.getId() == null || DSApi.context().isAdmin()){
            return true;
        }

        if (checkDocumentPolicyPermission(document))
            return true;


        return DSApi.context().documentPermission(document.getId(), ObjectPermission.MODIFY_ATTACHMENTS);
    }
    /**
     * Sprawdza czy u�ytkownik kt�ry posiada nowe uprawnienia do wyszukiwania wszystkich pism  mo�e podglada� jego za�aczniki / edytowa� pismo itp
     * @param document
     */
    @Deprecated
    public static boolean userCanView(Document document) throws EdmException {
        // return true; }

        if (DSApi.context().isAdmin() || !DSApi.context().hasPermission(DSPermission.WYSZ_WSZYSTKICH_PISM)
                || DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE,DSPermission.PISMO_WSZYSTKIE_PODGLAD)
                || document.getId() == null || isDocumentInTaskList(document.getId()))
            return true;

        else {

            DSUser user = DSApi.context().getDSUser();
            OfficeDocument officeDocument = OfficeDocument.find(document.getId());

            String officeStatus = officeDocument.getOfficeStatus();
            // TODO: przyjmowanie pism w wydzia�ach
            if (officeStatus != null && officeStatus.equals("new")) {
                return true;

            } else {// jest jako referent pisma
                if (officeDocument.getClerk() != null && user.getName().equals(officeDocument.getClerk())) {
                    return true;
                } else {
                    // posiada podgl�d w swojej komurce
                    if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD, DSPermission.PISMO_KOMORKA_MODYFIKACJE)) {
                        String documentDivision = officeDocument.getDivisionGuid();
                        if (documentDivision == null){
                            return true;
                        }

                        DSDivision[] divisions = user.getSubordinateDivisions();
                        if (divisions.length > 0) {
                            for (int i = 0; i < divisions.length; i++) {
                                if (divisions[i].isDivision()|| divisions[i].isPosition()) {
                                    DSDivision parent = divisions[i].getParent();
                                    if (parent != null)
                                        if (documentDivision.equals(parent.getGuid()))
                                            return true;
                                }
                                DSDivision[] children_POSITION = divisions[i].getChildren(DSDivision.TYPE_POSITION);
                                if (children_POSITION.length > 0) {
                                    for (int j = 0; j < children_POSITION.length; j++) {
                                        if (documentDivision.equals(children_POSITION[j].getGuid()))
                                            return true;
                                    }
                                }
                                DSDivision[] children_asDIVISION = divisions[i].getChildren(DSDivision.TYPE_DIVISION);
                                if (children_asDIVISION.length > 0) {
                                    for (int j = 0; j < children_asDIVISION.length; j++) {
                                        if (documentDivision.equals(children_asDIVISION[j].getGuid()))
                                            return true;
                                    }
                                }
                            }
                        }// podgl�d pism w kancelari og�lnej
                        if (DSApi.context().hasPermission(DSPermission.PISMO_KO_PODGLAD, DSPermission.PISMO_KO_MODYFIKACJE)) {
                            if (documentDivision.equals(DSDivision.ROOT_GUID))
                                return true;
                        }// je�eli prace nad pismem zosta�y zakonczone a uzytkownik pracowa� nad tym dokumentem i dosta�
                        // go na list� w procesie do wiadomo�ci
                        List<AssignmentHistoryEntry> assigmenthistory = officeDocument.getAssignmentHistory();

                        for (AssignmentHistoryEntry entry: assigmenthistory){
                            if (entry.getSourceUser().contains(user.getName())&& entry.isFinished()&& entry.getProcessType() == 2) {
                                return true;


                            }
                        }

                    }
                }
            }
        }

        return false;
    }

	public  SearchPermmisionFactory getSearchPermisionFactory()
	{
		return null;
		
		
	}
}
