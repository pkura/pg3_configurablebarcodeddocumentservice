package pl.compan.docusafe.core.base.permission.rule;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public abstract class AbstractPermissionRule implements PermissionRule{

    private final static Logger log = LoggerFactory.getLogger(HasDocumentPermissionRule.class);

    @Override
    public boolean validateRead(Document document) throws EdmException {
        boolean ok  =validate(document);
        getLog().trace("validateRead doc={} {}",document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateModify(Document document) throws EdmException {
        boolean ok  =validate(document);
        getLog().trace("validateModify doc={} {}",document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateReadAttachments(Document document) throws EdmException {
        boolean ok  =validate(document);
        getLog().trace("validateReadAttachments doc={} {}",document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateModifyAttachments(Document document) throws EdmException {
        boolean ok  =validate(document);
        getLog().trace("validateModifyAttachments doc={} {}",document.getId(), ok);
        return ok;
    }

    public abstract boolean validate(Document document) throws EdmException;

    protected abstract Logger getLog();
}
