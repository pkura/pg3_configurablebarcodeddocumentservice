package pl.compan.docusafe.core.base.permission;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;

/**
 * Polityka bezpieczeństwa danego dokumentu
 * @author Michał Sankowski
 */
public interface PermissionPolicy
{

	/**
	 * Metoda sprawdza uprawnienia modyfikacji do dokumentu.
	 */
	boolean canModify(Document document) throws EdmException;
	boolean canModify(Document document,Long binderId) throws EdmException;

	boolean canRead(Document document) throws EdmException;
	boolean canRead(Document document,Long binderId) throws EdmException;

	/**
	 *
	 * Metoda sprawdza czy moge czytac zalaczniki w dokumencie.
	 */
	boolean canReadAttachments(Document document) throws EdmException;
	boolean canReadAttachments(Document document,Long binderId) throws EdmException;
	boolean canReadOrginalAttachments(Document document,Long binderId) throws EdmException;
	
	boolean canModifyAttachments(Document document) throws EdmException;
	boolean canModifyAttachments(Document document,Long binderId) throws EdmException;
}
