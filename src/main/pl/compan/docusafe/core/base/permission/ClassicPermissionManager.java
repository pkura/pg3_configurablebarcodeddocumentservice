package pl.compan.docusafe.core.base.permission;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.common.collect.Lists;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.annotations.Unchecked;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.rule.PermissionRule;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.adm.AdmDwrPermissionFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.admin.UserToBriefcaseAction;

/** 
 * Klasa ktora bedzie odpowiedzialna za przejecie odpowiedzialnosci za rzeczy zwiazane
 * z ustalaniem uprawnien.
 */
public class ClassicPermissionManager extends PermissionManager
{
	private final static Logger log = LoggerFactory.getLogger(ClassicPermissionManager.class);

    @Override
    public boolean canRead(Document document,Long binderId) throws EdmException {
		log.trace("PermissionManager#canRead({})",document.getId());
		if(document == null || document.getDocumentKind() == null || document.getDocumentKind().logic() == null ||  document.getDocumentKind().logic().getPermissionPolicy() == null){
			return defaultCanRead(document);
		} else if(document.getDocumentKind().logic().getPermissionPolicy().canRead(document,binderId)){
			return  true;
		}
		else if (AvailabilityManager.isAvailable("menu.left.user.to.briefcase")
				&& UserToBriefcaseAction.hasPermisionToCase(DSApi.context().getDSUser(), document)){
			return true ;
		}
		else 
			return false;
	}

    @Override
    public boolean canModify(Document document,Long binderId) throws EdmException {
		log.trace("PermissionManager#canModify({})",document.getId());
		if(document.getDocumentKind() == null || document.getDocumentKind().logic().getPermissionPolicy() == null){
			return defaultCanModify(document);
		} else {
			return document.getDocumentKind().logic().getPermissionPolicy().canModify(document,binderId);
		}
	}
    

    @Override
    public boolean canReadAttachments(Document document,Long binderId) throws EdmException {
		log.trace("PermissionManager#canReadAttachments({})",document.getId());
		if(document.getDocumentKind() == null || document.getDocumentKind().logic().getPermissionPolicy() == null){
			return defaultCanReadAttachments(document);
		} else {
			return document.getDocumentKind().logic().getPermissionPolicy().canReadAttachments(document,binderId);
		}
	}

    @Override
    public boolean canModifyAttachments(Document document,Long binderId) throws EdmException
    {
    	log.trace("PermissionManager#canModifyAttachments({})",document.getId());
    	if(document.getDocumentKind() == null || document.getDocumentKind().logic().getPermissionPolicy() == null)
    	{
    		return defaultCanReadAttachments(document);
    	}
    	else
    	{
    		return document.getDocumentKind().logic().getPermissionPolicy().canModifyAttachments(document,binderId);
    	}
    }
}
