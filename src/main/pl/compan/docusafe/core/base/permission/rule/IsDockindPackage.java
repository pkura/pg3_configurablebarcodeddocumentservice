package pl.compan.docusafe.core.base.permission.rule;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Regu�a dla Admina
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class IsDockindPackage extends AbstractPermissionRule {

    private final static Logger log = LoggerFactory.getLogger(IsDockindPackage.class);

	@Override
	public boolean validateRead(Document document) throws EdmException
	{
		if (AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow") && Docusafe.getAdditionProperty("kind-package") != null
				&& !Docusafe.getAdditionProperty("kind-package").isEmpty())
			if (document.getDocumentKind().getCn().equals(Docusafe.getAdditionProperty("kind-package")))
			{
				return true;
			} else if (Boolean.TRUE.equals(document.getInPackage()))
			{
				return true;
			} else if (AvailabilityManager.isAvailable("permission.paczki.allow"))
			{
				return true;
			}
		return false;


	}

    @Override
    public boolean validateModify(Document document) throws EdmException {
		if (AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow") && Docusafe.getAdditionProperty("kind-package") != null
				&& !Docusafe.getAdditionProperty("kind-package").isEmpty())
			if (document.getDocumentKind().getCn().equals(Docusafe.getAdditionProperty("kind-package")))
			{
				return true;
			} else if (document.getInPackage())
			{
				return true;
			} else if (AvailabilityManager.isAvailable("permission.paczki.allow"))
			{
				return true;
			}
		return false;
    }
    @Override
    protected Logger getLog() {
        return log;
    }

	@Override
	public boolean validate(Document document) throws EdmException
	{
		if (AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow") && Docusafe.getAdditionProperty("kind-package") != null
				&& !Docusafe.getAdditionProperty("kind-package").isEmpty())
			if (document.getDocumentKind().getCn().equals(Docusafe.getAdditionProperty("kind-package")))
			{
				return true;
			} else if (document.getInPackage())
			{
				return true;
			} else if (AvailabilityManager.isAvailable("permission.paczki.allow"))
			{
				return true;
			}
		return false;
	}
}
