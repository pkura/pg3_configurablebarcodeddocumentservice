package pl.compan.docusafe.core.base.permission;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DlBinderPermissionPolicy implements PermissionPolicy
{
	private static Logger log = LoggerFactory.getLogger(DlBinderPermissionPolicy.class);

	/**Jesli z DS moze edytowac tylko jesli dokument jest na jego liscie, inne dzialy (DK,DO,DR) jesli opiekun.
	 * NastÍpnie sprawdza po regionie 
	 * */
	public boolean canModify(Document document, Long binderId) throws EdmException, EdmException
	{
		return canModify(document);
	}
	
	/**
	 * Jesli z DS moze edytowac tylko jesli dokument jest na jego liscie (ale nie mozemy tego sprwdzic tutaj wiec false),
	 * inne dzialy (DK,DO,DR) jesli opiekun. NastÍpnie sprawdza po regionie
	 **/
	public boolean canModify(Document document) throws EdmException, EdmException
	{
		return PermissionManager.getInstance().defaultCanModify(document);
	}

	public boolean canRead(Document document, Long binderId) throws EdmException
	{
		return canRead(document);
	}

	public boolean canRead(Document document) throws EdmException
	{
		return PermissionManager.getInstance().defaultCanRead(document) || (DlPermissionManager.isInHDivision() && DlPermissionManager.isOpiekun(document.getId()));
	}
	
	public boolean canReadAttachments(Document document, Long binderId) throws EdmException
	{
			return canRead(document);
	}
	
	public boolean canReadAttachments(Document document) throws EdmException
	{
		return PermissionManager.getInstance().defaultCanReadAttachments(document) || (DlPermissionManager.isInHDivision() && DlPermissionManager.isOpiekun(document.getId()));
	}
	
	public boolean canModifyAttachments(Document document,Long binderId) throws EdmException
	{
		return canModifyAttachments(document);
	}
	
	public boolean canModifyAttachments(Document document) throws EdmException
	{
		return PermissionManager.isDocumentInTaskList(document.getId()) || PermissionManager.getInstance().defaultCanModifyAttachments(document);
	}

	@Override
	public boolean canReadOrginalAttachments(Document document, Long binderId)
			throws EdmException {
		
		return true;
	}	
}
