package pl.compan.docusafe.core.base.permission.rule;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Czy posiada uprawnienia dokumentu
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class HasDocumentPermissionRule   implements PermissionRule {

    private final static Logger log = LoggerFactory.getLogger(HasDocumentPermissionRule.class);

    @Override
    public boolean validateRead(Document document) throws EdmException {
        boolean ok =  DSApi.context().documentPermission(document.getId(), ObjectPermission.READ);
        log.trace("valdiateRead doc={} {}", document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateModify(Document document) throws EdmException {
        boolean ok =  DSApi.context().documentPermission(document.getId(), ObjectPermission.MODIFY);
        log.trace("validateModify doc={} {}", document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateReadAttachments(Document document) throws EdmException {
        boolean ok =   DSApi.context().documentPermission(document.getId(), ObjectPermission.READ_ATTACHMENTS);
        log.trace("validateReadAttachments doc={} {}", document.getId(), ok);

        return ok;
    }

    @Override
    public boolean validateModifyAttachments(Document document) throws EdmException {
        boolean ok =   DSApi.context().documentPermission(document.getId(), ObjectPermission.MODIFY_ATTACHMENTS);
        log.trace("validateModifyAttachments doc={} {}", document.getId(), ok);
        return ok;
    }
}