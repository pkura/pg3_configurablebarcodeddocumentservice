package pl.compan.docusafe.core.base.permission.rule;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Czy uzytkownik ma dost�p do dokumentu na podstawie przynalezno�ci do dzialu, w kt�rym jest dokument
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class OnDivisionRule  implements PermissionRule {

    private final static Logger log = LoggerFactory.getLogger(OnDivisionRule.class);

    @Override
    public boolean validateRead(Document document) throws EdmException {
        boolean ok =  validateAllRead(document);
        log.trace("valdiateRead doc={} {}", document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateModify(Document document) throws EdmException {
        boolean ok =  validateAllModify(document);
        log.trace("validateModify doc={} {}", document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateReadAttachments(Document document) throws EdmException {
        boolean ok =  validateAllRead(document);
        log.trace("validateReadAttachments doc={} {}", document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateModifyAttachments(Document document) throws EdmException {
        boolean ok =  validateAllModify(document);
        log.trace("validateModifyAttachments doc={} {}", document.getId(), ok);
        return ok;
    }


    /**
     * Ogolna metoda prywatna do weryfiakcji uprawnienia modyfikacji
     * @param document
     * @return
     * @throws EdmException
     */
    private boolean validateAllModify(Document document) throws EdmException {
        boolean ok = false;
        if ((document instanceof OfficeDocument) && DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_MODYFIKACJE))
        {
            String assignedDivision = ((OfficeDocument)document).getCurrentAssignmentGuid();
            ok =DSDivision.containsDivisionGuid(assignedDivision, DSApi.context().getDSUser().getDivisions());
        }
        return ok;
    }

    private boolean validateAllRead(Document document) throws EdmException {
        boolean ok = false;
        if ((document instanceof OfficeDocument) &&
                (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD, DSPermission.PISMO_KOMORKA_MODYFIKACJE)||DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD_W_PODWYDZIALACH)))
        {
            String assignedDivision = ((OfficeDocument)document).getCurrentAssignmentGuid();
            if(DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE) &&  assignedDivision.equals("rootdivision"))
            	return true;
            
            if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD_W_PODWYDZIALACH)){
            	   ok =DSDivision.containsDivisionGuid(assignedDivision, DSApi.context().getDSUser().getSubordinateDivisionsWithoutGroup());
            }else
            	   ok =DSDivision.containsDivisionGuid(assignedDivision, DSApi.context().getDSUser().getDivisions());
         
        }
        if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD_W_PODWYDZIALACH)){
        	   String assignedDivision = ((OfficeDocument)document).getCurrentAssignmentGuid();
        	   ok =DSDivision.containsDivisionGuid(assignedDivision, DSApi.context().getDSUser().getSubordinateDivisionsWithoutGroup());
        }
     	  
        
        return ok;
    }

}
