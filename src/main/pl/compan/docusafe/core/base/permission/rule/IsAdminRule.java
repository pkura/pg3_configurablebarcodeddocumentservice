package pl.compan.docusafe.core.base.permission.rule;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Regu�a dla Admina
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class IsAdminRule extends AbstractPermissionRule {

    private final static Logger log = LoggerFactory.getLogger(IsAdminRule.class);

    @Override
    public boolean validate(Document document) throws EdmException {
        return DSApi.context().isAdmin();
    }

    @Override
    protected Logger getLog() {
        return log;
    }
}
