package pl.compan.docusafe.core.base.permission.rule;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;

import java.io.Serializable;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public interface PermissionRule extends Serializable {

    /**
     * Metoda sprawdza czy mo�e otworzy� dokument
     * @param document
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public boolean validateRead(Document document) throws EdmException;

    /**
     *  Metoda sprawdza uprawnienia modyfikacji do dokumentu.
     */
    public boolean validateModify(Document document) throws EdmException;

    /**
     * Metoda sprawdza czy moge czytac zalaczniki w dokumencie.
     */
    public boolean validateReadAttachments(Document document) throws EdmException;

    /**
     * Metoda sprawdza czy mog� modyfikowa� za��czniki
     * @param document
     * @return
     * @throws EdmException
     */
    public boolean validateModifyAttachments(Document document) throws EdmException;
}
