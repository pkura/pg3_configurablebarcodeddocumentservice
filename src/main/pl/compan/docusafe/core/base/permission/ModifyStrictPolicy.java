package pl.compan.docusafe.core.base.permission;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Polityka, w kt�rej dokument mo�na modyfiowa� tylko wtedy gdy ma si� do tego odpowiednie uprawnienie lub jest administratorem
 * @author Micha� Sankowski
 */
public class ModifyStrictPolicy extends AbstractPermissionPolicy {

	private final static Logger log = LoggerFactory.getLogger(ModifyStrictPolicy.class);

	private ModifyStrictPolicy(){

	}

	private final static ModifyStrictPolicy instance = new ModifyStrictPolicy();

	public final static PermissionPolicy getInstance(){
		return instance;
	}

	@Override
	public boolean canModify(Document document) throws EdmException {
		log.info("ModifyStrictPolicy#canModify");
		return DSApi.context().isAdmin()
				|| DSApi.context().documentPermission(document.getId(), ObjectPermission.MODIFY);
	}
}
