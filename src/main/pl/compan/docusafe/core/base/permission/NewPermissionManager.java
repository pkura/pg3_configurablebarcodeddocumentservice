package pl.compan.docusafe.core.base.permission;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.permission.rule.PermissionRule;

import java.util.List;

/**
 * Klasa implementuje nowe regu�y uprawnie� kt�re weryfukuje uprawnienia po regule
 * @see PermissionRule
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public abstract class NewPermissionManager extends PermissionManager {

    public boolean canRead(Document document, Long binderId) throws EdmException {
        for(PermissionRule r : getPermissionRules()){
            if(r.validateRead(document)){
                return true;
            }
        }
        return false;
    }

    public boolean canModify(Document document, Long binderId) throws EdmException {
        for(PermissionRule r : getPermissionRules()){
            if(r.validateModify(document)){
                return true;
            }
        }
        return false;
    }

    public boolean canReadAttachments(Document document,Long binderId) throws EdmException{
        for(PermissionRule r : getPermissionRules()){
            if(r.validateReadAttachments(document)){
                return true;
            }
        }
        return false;
    }

    public boolean canModifyAttachments(Document document, Long binderId) throws EdmException {
        for(PermissionRule r : getPermissionRules()){
            if(r.validateModifyAttachments(document)){
                return true;
            }
        }
        return false;
    }

    /**
     * Zwraca list� Regu� kt�re b�d� stosowane w danych wdro�eniu
     * @return
     */
    public abstract List<PermissionRule> getPermissionRules() throws EdmException;

}
