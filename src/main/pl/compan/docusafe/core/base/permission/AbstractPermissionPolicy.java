package pl.compan.docusafe.core.base.permission;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public abstract class AbstractPermissionPolicy implements PermissionPolicy{

	private final static Logger log = LoggerFactory.getLogger(AbstractPermissionPolicy.class);

	public boolean canModify(Document document,Long binderId) throws EdmException, EdmException {
		return PermissionManager.getInstance().defaultCanModify(document);
	}
	
	public boolean canModify(Document document) throws EdmException, EdmException {
		return PermissionManager.getInstance().defaultCanModify(document);
	}

	public boolean canRead(Document document,Long binderId) throws EdmException {
		return PermissionManager.getInstance().defaultCanRead(document);
	}
	
	public boolean canRead(Document document) throws EdmException {
		return PermissionManager.getInstance().defaultCanRead(document);
	}

	public boolean canReadAttachments(Document document,Long binderId) throws EdmException {
		return PermissionManager.getInstance().defaultCanReadAttachments(document);
	}
	
	public boolean canReadOrginalAttachments(Document document,Long binderId) throws EdmException {
		if(AvailabilityManager.isAvailable("perimission.READ_ORGINAL_ATTACHMENTS"))
		{
			return PermissionManager.getInstance().defaultCanReadOrginalAttachments(document);
		}
		else
		{
			return canReadAttachments(document);
		}
	}
	
	public boolean canReadAttachments(Document document) throws EdmException {
		return PermissionManager.getInstance().defaultCanReadAttachments(document);
	}
	
	public boolean canModifyAttachments(Document document) throws EdmException
	{
		return PermissionManager.getInstance().defaultCanModifyAttachments(document);
	}
	
	public boolean canModifyAttachments(Document document,Long binderId) throws EdmException
	{
		return PermissionManager.getInstance().defaultCanModifyAttachments(document);
	}
}
