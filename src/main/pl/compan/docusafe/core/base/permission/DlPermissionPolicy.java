package pl.compan.docusafe.core.base.permission;

import java.util.Date;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DlPermissionPolicy implements PermissionPolicy
{
	private static Logger log = LoggerFactory.getLogger(DlPermissionPolicy.class);

	/**Jesli z DS moze edytowac tylko jesli dokument jest na jego liscie, inne dzialy (DK,DO,DR) jesli opiekun.
	 * NastÍpnie sprawdza po regionie 
	 * */
	public boolean canModify(Document document, Long binderId) throws EdmException, EdmException
	{
		if (binderId == null)
			return canModify(document);
		else
		{
			if(DlPermissionManager.isInHDivision())
			{
				return DlPermissionManager.isOpiekun(document.getId()) || isInBinderAndInTasklist(binderId, document.getId()) || PermissionManager.getInstance().defaultCanModify(document);
			}
			else
			{
				return isInBinderAndInTasklist(binderId, document.getId());
			}
		}
	}
	
	/**
	 * Jesli z DS moze edytowac tylko jesli dokument jest na jego liscie (ale nie mozemy tego sprwdzic tutaj wiec false),
	 * inne dzialy (DK,DO,DR) jesli opiekun. NastÍpnie sprawdza po regionie
	 **/
	public boolean canModify(Document document) throws EdmException, EdmException
	{
		if(DlPermissionManager.isInHDivision())
		{
			return DlPermissionManager.isOpiekun(document.getId()) || PermissionManager.getInstance().defaultCanModify(document);
		}
		else
		{
			return ClassicPermissionManager.getInstance().defaultCanModify(document);
		}
	}

	public boolean canRead(Document document, Long binderId) throws EdmException
	{
		if (binderId == null)
			return canRead(document);
		else
			return DlPermissionManager.isOpiekun(document.getId()) || isInBinderAndInTasklist(binderId, document.getId()) || PermissionManager.getInstance().defaultCanRead(document);
	}

	public boolean canRead(Document document) throws EdmException
	{
		return DlPermissionManager.isOpiekun(document.getId()) || PermissionManager.getInstance().defaultCanRead(document);
	}
	
	public boolean canReadAttachments(Document document, Long binderId) throws EdmException
	{
		if (binderId == null)
			return canRead(document);
		else
			return DlPermissionManager.isOpiekun(document.getId()) || isInBinderAndInTasklist(binderId, document.getId()) || PermissionManager.getInstance().defaultCanReadAttachments(document);
	}
	
	public boolean canReadAttachments(Document document) throws EdmException
	{
		return DlPermissionManager.isOpiekun(document.getId()) || PermissionManager.getInstance().defaultCanReadAttachments(document);
	}
	
	public boolean canModifyAttachments(Document document,Long binderId) throws EdmException
	{
		LoggerFactory.getLogger("mariusz").debug("DLP canModifyAttachments "+ document.getId()+" "+binderId);
		return isInBinderAndInTasklist(binderId, document.getId()) || PermissionManager.getInstance().defaultCanModifyAttachments(document);
	}
	
	public boolean canModifyAttachments(Document document) throws EdmException
	{
		return PermissionManager.getInstance().defaultCanModifyAttachments(document);
	}
	
	/**Sprawdza czy dokument znajduje sie w teczce i czy teczka znajduje sie na liscie zadan uzytkownika*/
	private boolean isInBinderAndInTasklist(Long binderId, Long documentId) throws EdmException
	{
		if(binderId == null)
			return false;
		return DlPermissionManager.isInBinder(binderId, documentId) && PermissionManager.isDocumentInTaskList(binderId);
	}

	@Override
	public boolean canReadOrginalAttachments(Document document, Long binderId)
			throws EdmException {
		// TODO Auto-generated method stub
		return true;
	}
}
