package pl.compan.docusafe.core.base.permission;

import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;


public interface SearchPermmisionFactory
{

	 void getPerrmissionsQuery(Map<String, TableAlias> tables, FromClause from, WhereClause where, DockindQuery query, DocumentKind documentKind, TableAlias perm);
	 boolean canReadXXX(Document document) throws EdmException;
		
}
