package pl.compan.docusafe.core.base.permission;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.axis2.databinding.types.soapencoding.Array;
import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.ReferenceToHimselfException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DlPermissionManager
{
	private static Logger log = LoggerFactory.getLogger(DlPermissionPolicy.class);
	
	private static DSDivision DS_DIVISION;
	private static DSDivision DR_DIVISION;
	private static DSDivision DO_DIVISION;
	private static DSDivision DK_DIVISION;
	private static DSDivision DU_DIVISION;
	
	private static Map<String,List<String>> parentDivisions = new HashMap<String, List<String>>();
	
	static
	{
		try
		{
			
			DS_DIVISION = DSDivision.find("DL_BINDER_DS");
			DR_DIVISION = DSDivision.find("DL_BINDER_DR");
			DK_DIVISION = DSDivision.find("DL_BINDER_DK");
			DO_DIVISION = DSDivision.find("DL_BINDER_DO");		
			DU_DIVISION = DSDivision.find("DL_BINDER_DU");
		}
		catch (Exception e) 
		{
			log.error("",e);
		}
	}
	
	/**SprawdZa czy dokument znajduje sie w teczce binderID*/
	public static boolean isInBinder(Long binderId, Long documentId)
	{
		if(binderId == null)
			return false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try
		{
			ps = DSApi.context().prepareStatement(
					"select count(1) from dsg_dlbinder b, DSG_LEASING_MULTIPLE_VALUE m where "
							+ "( b.NUMER_UMOWY = m.FIELD_VAL and m.FIELD_CN = 'NUMER_UMOWY' and m.DOCUMENT_ID = ? and b.DOCUMENT_ID = ? ) "
							+ " or (b.NUMER_WNIOSKU = m.FIELD_VAL and m.FIELD_CN = 'NUMER_WNIOSKU' and m.DOCUMENT_ID = ? and b.DOCUMENT_ID = ? )");
			ps.setLong(1, documentId);
			ps.setLong(2, binderId);
			ps.setLong(3, documentId);
			ps.setLong(4, binderId);
			rs = ps.executeQuery();
			if (rs.next())
				count = rs.getInt(1);
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
		return count > 0;
	}
	
	/**Wyciaga nazwy uzytkownikoe kt�rzy sa opiekunami teczki w kt�rej znajduje sie dokument*/
	public static Map<String,String> getUsers(Long idWniosku,Long idUmowy)
	{
		if(idWniosku == null && idUmowy == null)
			return new HashMap<String,String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<String,String> map = new HashMap<String,String>();
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select b.ds_user_dk,b.ds_user_do,b.ds_user_dr,b.ds_user_ds from dsg_dlbinder b where ");
			if(idUmowy != null)
				sb.append(" b.numer_umowy = ? ");
			if(idWniosku != null && idUmowy != null)
				sb.append(" or ");
			if(idWniosku != null)
				sb.append(" b.numer_wniosku = ? ");
			
			ps = DSApi.context().prepareStatement(sb.toString());
			int column = 1;
			if(idUmowy != null)
			{
				ps.setLong(column, idUmowy);
				column++;
			}
			if(idWniosku != null)
			{
				ps.setLong(column, idWniosku);
			}

			rs = ps.executeQuery();
			if (rs.next())
			{
				if(rs.getString(1) != null)
				{
					map.put("ds_user_dk", rs.getString(1));
				}
				if(rs.getString(2) != null)
				{
					map.put("ds_user_do", rs.getString(2));
				}
				if(rs.getString(3) != null)
				{
					map.put("ds_user_dr", rs.getString(3));
				}
				if(rs.getString(4) != null)
				{
					map.put("ds_user_ds", rs.getString(4));
				}
			}
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
		return map;
	}

	/** Sprawdza czy u�ytkownik jest opiekunem teczki */
	public static boolean isOpiekun(Long documentId)
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try
		{
			ps = DSApi.context().prepareStatement(
					"select * from dsg_dlbinder b, dsg_leasing_multiple_value m where " + "(( b.numer_umowy = m.field_val and m.field_cn = 'NUMER_UMOWY' and m.document_id = ? ) or "
							+ "( b.numer_wniosku = m.field_val and m.field_cn = 'NUMER_WNIOSKU' and m.document_id = ? ) ) "
							+ "and (b.ds_user_dk = ? or b.ds_user_do = ? or b.ds_user_dr = ? or b.ds_user_ds = ?)");
			ps.setLong(1, documentId);
			ps.setLong(2, documentId);
			ps.setString(3, DSApi.context().getPrincipalName());
			ps.setString(4, DSApi.context().getPrincipalName());
			ps.setString(5, DSApi.context().getPrincipalName());
			ps.setString(6, DSApi.context().getPrincipalName());

			rs = ps.executeQuery();
			if (rs.next())
				return true;
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
		return false;
	}
	
	public static boolean isInHDivision() throws EdmException, EdmException
	{
		DSUser user = DSApi.context().getDSUser();
		return user.inOriginalDivision(DR_DIVISION) ||user.inOriginalDivision(DO_DIVISION) || user.inOriginalDivision(DK_DIVISION) || user.inOriginalDivision(DU_DIVISION) ;
	}
	
	
	private static void setupDivisionPermission(Set<PermissionBean> perms, List<String> divisions) throws DivisionNotFoundException, ReferenceToHimselfException, EdmException
	{
		for (String division : divisions) 
		{
			perms.add(new PermissionBean(ObjectPermission.READ, division, ObjectPermission.GROUP));
	        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, division, ObjectPermission.GROUP));
		}
		

	}
	
	private static void updateParentDivisions(DSDivision division) throws DivisionNotFoundException, ReferenceToHimselfException, EdmException
	{
		List<String> divs = new ArrayList<String>();
		divs.add(division.getGuid());
		DSDivision tmpDiv = division.getParent();
		while(!tmpDiv.isRoot())
		{
			divs.add(tmpDiv.getGuid());
			tmpDiv = tmpDiv.getParent();
		}
		parentDivisions.put(division.getGuid(), divs);
		
	}

	public static void setupPermission(String region, Long numerwniosku,Long numwerUmowy, Set<PermissionBean> perms, List<String> users, String author) 
	{	
        try
        {
//        	log.error("PERM : Wej�cie do perrm {} ", new Date().getTime());
            Map<String,String> map = DlPermissionManager.getUsers(numerwniosku,numwerUmowy);
//            log.error("PERM : Pobra� users ", new Date().getTime());
            Set<String> set = map.keySet();
            for (String key : set)
            {
                perms.add(new PermissionBean(ObjectPermission.READ, map.get(key), ObjectPermission.USER));
                perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, map.get(key), ObjectPermission.USER));
                perms.add(new PermissionBean(ObjectPermission.MODIFY, map.get(key), ObjectPermission.USER));
                perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, map.get(key), ObjectPermission.USER));
            }
            if(region != null && region.length() > 0)
            {     
            	List<String> divs = parentDivisions.get("DV_"+region);
            	if(divs == null)
            	{
            		updateParentDivisions(DSDivision.find("DV_"+region));
            	}
//            	log.error("PERM : setupDivisionPermission IN ", new Date().getTime());
            	setupDivisionPermission(perms, parentDivisions.get("DV_"+region));
//            	log.error("PERM : setupDivisionPermission OUT ", new Date().getTime());
            }
            String[]  userDivs = DSUser.findByUsername(author).getDivisionsGuidWithoutGroup();
            for (String userGuidDivs : userDivs)
            {
            	if(userGuidDivs.contains("DV_"))
            	{
            		List<String> divs = parentDivisions.get(userGuidDivs);
                	if(divs == null)
                	{
                		updateParentDivisions(DSDivision.find(userGuidDivs));
                	}
                	setupDivisionPermission(perms, parentDivisions.get(userGuidDivs));
            	}
			}
        }
        catch (Exception e) 
        {
            log.error(e.getMessage(),e);
        }
        
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_READ", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_READ", ObjectPermission.GROUP));
        //teczki

        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DR", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DO", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DK", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DU", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DR", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DO", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DK", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DU", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ,"DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ,"DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ,"DL_DEL_ATTA", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DL_DEL_ATTA", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_DEL_ATTA", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DL_DEL_ATTA", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_DEL_DOC", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY,"DL_DEL_DOC", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_DEL_DOC", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DL_DEL_DOC", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "DL_DEL_DOC", ObjectPermission.GROUP));
        for (String user : users) {
        	 perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER));
             perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER));
             perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER));
             perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER));
		}
       
	}
}
