package pl.compan.docusafe.core.base.permission.rule;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.adm.AdmDwrPermissionFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class OfficePermissionRule extends AbstractPermissionRule {

    private final static Logger log = LoggerFactory.getLogger(OfficePermissionRule.class);


    @Override
    public boolean validateModify(Document document) throws EdmException {
        boolean ok  =validateGlobalModify(document);
        getLog().trace("validateModify doc={} {}",document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validateModifyAttachments(Document document) throws EdmException {
        boolean ok  =validateGlobalModify(document);
        getLog().trace("validateModifyAttachments doc={} {}",document.getId(), ok);
        return ok;
    }

    @Override
    public boolean validate(Document document) throws EdmException {
        if(!document.isForceArchivePermissions() && document instanceof OfficeDocument
                && checkOfficeRules((OfficeDocument) document)){
            return true;
        }
        return false;
    }

    /**
     * U�ywane tylko przy modyfikacjach
     * @param document
     * @return
     * @throws EdmException
     */
    private boolean validateGlobalModify(Document document) throws EdmException {
        if (!document.isForceArchivePermissions() && document instanceof OfficeDocument &&
                DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE))
        {
            return true;
        }

        return false;
    }

    /**
     * U�ywane tylko przy odczycie.
     * @param officeDoc
     * @return
     * @throws EdmException
     */
    private boolean checkOfficeRules(OfficeDocument officeDoc) throws EdmException {
        if (DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD, DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE,
                DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD))
        {
            return true;
        }

        return false;
    }

    @Override
    protected Logger getLog() {
        return log;
    }
}
