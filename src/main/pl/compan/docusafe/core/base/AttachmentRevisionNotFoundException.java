package pl.compan.docusafe.core.base;

import pl.compan.docusafe.util.StringManager;


/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AttachmentRevisionNotFoundException.java,v 1.2 2004/06/10 21:21:56 administrator Exp $
 */
public class AttachmentRevisionNotFoundException extends EntityNotFoundException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public AttachmentRevisionNotFoundException(Long id)
    {
        super(sm.getString("AttachmentRevisionNotFoundException", id));
    }
}
