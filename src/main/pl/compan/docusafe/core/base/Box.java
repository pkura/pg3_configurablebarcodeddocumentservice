package pl.compan.docusafe.core.base;

import org.hibernate.*;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.util.StringManager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/* User: Administrator, Date: 2005-08-03 10:39:07 */

/**
 * Obiekt reprezentuj�cy pud�o archiwalne.
 * <pre>
 * Numer aktualnie otwartego pud�a zapisywany jest w Preferences.
 * Workflow:
 * - pud�o jest tworzone i jednocze�nie otwierane
 * - umieszczane s� w nim dokumenty
 * - pud�o jest zamykane
 * - mo�e zosta� otwarte nowe pud�o
 * </pre>
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Box.java,v 1.12 2008/11/24 21:47:06 wkuty Exp $
 */
public class Box implements Lifecycle
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(Box.class.getPackage().getName(),null);
    private Long id;
    private Integer hversion;
    private String name;
    private Date ctime;
    private Date openTime;
    private Date closeTime;
    private boolean open;
    private String line;
    private String description;
    
    Box()
    {
    }

    public static List<Box> list() throws EdmException
    {
        return Finder.list(Box.class, "name");
    }

    public static Box find(Long id) throws EdmException
    {
        return (Box) Finder.find(Box.class, id);
    }
    
    public static List<Box> findByDescription(String description) throws EdmException
    {
        return DSApi.context().session().createCriteria(Box.class).
        add(Restrictions.like("description", "%"+description+"%").ignoreCase()).list();
    }

    /**
     * Szuka pud�a po jego dok�adnej nazwie.  Nazwa jest przekszta�cana
     * do wielkich liter.
     */
    public static Box findByName(String line, String name) throws EdmException
    {
        if (name == null)
            throw new NullPointerException("name");
        
        try
        {
        	//Do przerobiena na QUERY
            List list = DSApi.context().classicSession().find("from b in class "+Box.class.getName()+
                " where b.name = ? and "+(line != null ? "b.line = '"+line+"'" : "b.line is null"), name.toUpperCase(), Hibernate.STRING);

            if (list.size() > 0)
                return (Box) list.get(0);

            throw new EntityNotFoundException(sm.getString("NieZnalezionoPudlaOnazwie")+" "+name);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void create() throws EdmException
    {
        try
        {
            findByName(line,name);
            throw new DuplicateNameException(sm.getString("IstniejeJuzPudloOtejNazwie"));
        }
        catch (EntityNotFoundException e)
        {
            Persister.create(this);
        }
    }

    public Box(String name)
    {
        if (name == null)
            throw new NullPointerException("name");
        this.name = name.toUpperCase();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getOpenTime()
    {
        return openTime;
    }

    public void setOpenTime(Date openTime)
    {
        this.openTime = openTime;
    }

    public Date getCloseTime()
    {
        return closeTime;
    }

    public void setCloseTime(Date closeTime)
    {
        this.closeTime = closeTime;
        if(this.closeTime == null) {
        	this.open = true;
        } else this.open = false;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public boolean isOpen()
    {
        return open;
    }

    public void setOpen(boolean open)
    {
        this.open = open;
    }
    
    //public void setReOpen(boolean open)
    //{
    //	this.open = open;
    //}
    
    public String getLine()
    {
        return line;
    }

    public void setLine(String line)
    {
        this.line = line;
    }
    
    public String getNameWithLine()
    {
        if (line != null)
            return name+" ("+line+")";
        else
            return name;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Box)) return false;

        final Box box = (Box) o;

        if (id != null ? !(id.equals(box.id)) : box.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    /* Lifecycle */

    public boolean onSave(Session s) throws CallbackException
    {
        if (ctime == null)
        {
            ctime = new Date();
        }

        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }
    
    public String toString() {
    	return ""+id+" "+name;
    }

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
