package pl.compan.docusafe.core.base;
/* User: Administrator, Date: 2006-02-14 13:06:53 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;

import java.util.Collections;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentType.java,v 1.5 2009/11/20 07:37:33 pecet1 Exp $
 */
public enum DocumentType
{
    PLAIN("plain", -1), //brak
    INCOMING("in", DSIImportHandler.INCOMING_DOCUMENT_IMPORT),
    OUTGOING("out", DSIImportHandler.OUTGOING_DOCUMENT_IMPORT),
    INTERNAL("internal", DSIImportHandler.INTERNAL_DOCUMENT_IMPORT),
    ORDER("order",-2), //brak
    ARCHIVE("archive", DSIImportHandler.ARCHIVE_ONLY_IMPORT);

    private static final Map<String, DocumentType> nameToInstance;
    private String name;
    private int dsiImportKindCode;

    static {
        Map<String, DocumentType> nti = Maps.newHashMap();
        for(DocumentType dt: DocumentType.values()){
            nti.put(dt.getName(), dt);
        }
        nameToInstance = Collections.unmodifiableMap(nti);
    }

    public static DocumentType forName(String name){
        Preconditions.checkArgument(nameToInstance.containsKey(name), "unknown name : = '" + name + "'");
        return nameToInstance.get(name);
    }

    DocumentType(String name, int i){
       this.name = name;
       this.dsiImportKindCode = i;
    }

    /**
     * Warto�� z DSI importera
     * @return
     */
    public int getDSiImportKindCode(){
        return dsiImportKindCode;
    }
    public String getName(){
        return name;
    }

	@Override
	public String toString() {
		return name;
	}
}
