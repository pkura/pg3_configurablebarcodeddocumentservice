package pl.compan.docusafe.core.base;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.ParameterBlock;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.ImageUtils;
import pl.compan.docusafe.util.ShUtils;

import com.asprise.util.tiff.TIFFReader;

public class StandardAttachmentRevision extends AttachmentRevision
{
	
	private File getFile() throws EdmException, IOException
    {
        File file = new File(Attachment.getPath(getId()));
        if(!Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM || !file.exists())
        {
            if (!file.exists() || !file.isFile() || file.length() != getSize().intValue())
            {            	
                ShUtils.delete(file);
                saveToFile(file);
            }
 
        }
        //jesli zalaczniki ogolnie sa trzymane na dysku 
        //oraz akurat ten egzemplarz jest na dysku 
        //to wystarczy go zwrocic
        return file;
    }
	
	public long saveToFile(File file) throws EdmException, IOException
    {
        if (file.exists() && !file.canWrite())
            throw new EdmException("Brak uprawnie� do zapisu pliku " + file);
        if(!file.getParentFile().exists())
         {
        	(new File(file.getParent())).mkdirs();
        }        
        OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
        InputStream is = getBinaryStream();
        byte[] buf = new byte[2048];
        int count;
        long total = 0;
        while ((count = is.read(buf)) > 0)
        {
            total += count;
            os.write(buf, 0, count);
        }
        org.apache.commons.io.IOUtils.closeQuietly(is);
        org.apache.commons.io.IOUtils.closeQuietly(os);        
        return total;
    }
	
	public File saveToTempFile() throws EdmException, IOException
    {
        String suffix;
        if (mime == null || mime.indexOf('/') < 0)
        {
            suffix = ".tmp";
        }
        else if (mime.indexOf("image/tiff") >= 0)
        {
            suffix = ".tif";
        }
        else if (mime.indexOf("image/jpeg") >= 0)
        {
            suffix = ".jpg";
        }
        else if (mime.indexOf("application/pdf") >= 0)
        {
            suffix = ".pdf";
        }
        else
        {
            String ms = mime.substring(mime.indexOf('/') + 1);
            if (ms.indexOf(';') > 0)
                ms = ms.substring(0, ms.indexOf(';'));
            if (ms.length() == 0)
                ms = "tmp";
            suffix = "." + ms;
        }
        
        File file = File.createTempFile("docusafe_pdf", suffix);
        saveToFile(file);
        return file;
    }
	
	public InputStream getBinaryStream() throws EdmException
    {
    	boolean closeStm = true;
        if (revision == null)
            throw new NullPointerException(sm.getString("nullParameter", "revision"));
      /* Wydaje sie ze w tym miejscu to i tak juz 10 razy sprawdzal
       *  if (attachment.isDocumentType())
        {
            if (!attachment.getDocument().canReadAttachments())
                throw new AccessDeniedException("Brak uprawnie� do odczytu tre�ci " + "za��cznika");
        }*/

        PreparedStatement pst = null;
        try
        {

            pst = DSApi.context().prepareStatement("select contentdata, onDisc from ds_attachment_revision where id = ?");
            pst.setLong(1, contentRefId != null ? contentRefId : getId());
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
                throw new EdmException(sm.getString("documentHelper.blobRetrieveFailed", getId()));

            InputStream is;
            int onDisc = rs.getInt(2);
            if (onDisc == AttachmentRevision.FILE_ON_DISC)
            {
                is = new FileInputStream(new File(Attachment.getPath((contentRefId != null ? contentRefId : getId()))));
                //log.error("ondisc");
            }
            else
            {
                is = rs.getBinaryStream(1);
                //log.error("notondisc");
            }
            
            if (is != null && onDisc != AttachmentRevision.FILE_ON_DISC)
            {
            	closeStm = false;
                return new BlobInputStream(is, pst);
            }
            else if (is != null)
            {
            	return is;
            }
            else
            {            	
                return null;
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "revision=" + revision);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (FileNotFoundException fnfe)
        {
            throw new EdmException("file not found");
        }
        finally
        {
        	if (closeStm)
        	{
        		DSApi.context().closeStatement(pst);
        	}
        }
        // Nie mo�na zamkn�� obiektu Statement od razu po zwr�ceniu strumienia,
        // poniewa� w�wczas nie b�dzie mo�liwe odczytanie danych ze strumienia.
        // Statement jest zamykane w metodzie BlobInputStream.closeStream()
    }
	
	public String getAnnotations() throws DocumentNotFoundException, AccessDeniedException, EdmException, IOException
    {
    	/*
        if (revision == null)
            throw new NullPointerException(sm.getString("nullParameter", "revision"));
        
        if (attachment.isDocumentType())
        {
            if (!attachment.getDocument().canReadAttachments())
                throw new AccessDeniedException("Brak uprawnie� do odczytu tre�ci " + "za��cznika");
        }
        */
        File f = null;
        if (Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM)
            f = new File(Attachment.getAnnotationsPath(id));
        else
            return null;
        if(!f.exists())
        {
        	return null;        	
        }
        
        FileReader fr = new FileReader(f);
        BufferedReader br = new BufferedReader(fr);
        String line;
        StringBuilder sb = new StringBuilder();
        while((line = br.readLine())!=null)
        {
        	sb.append(line);
        }
        return sb.toString();
    }
    
    public void updateAnnotations(String source) throws EdmException, IOException
    {
        File f = null;
        if (onDisc.equals(AttachmentRevision.FILE_ON_DISC))
        {
            f = new File(Attachment.getAnnotationsPath((contentRefId != null ? contentRefId : getId())));
        }
        else
            return;
        if(!f.exists())
        {
        	f.createNewFile();
        }
        try
        {
            OutputStream out = new FileOutputStream(f);
            ByteArrayInputStream str = new ByteArrayInputStream(source.getBytes());
            byte[] buf = new byte[1024];
            int len;
            while ((len = str.read(buf)) > 0)
            {
                out.write(buf, 0, len);
            }
            str.close();
            out.close();
        }
        catch (IOException ioe)
        {
            throw new EdmException("B��d przy kopiowanu pliku", ioe);
        }
    }
    
    public void updateBinaryStreamRevisionSpecific(InputStream stream, int fileSize) throws EdmException
    {
        
        PreparedStatement ps = null;
        int rows;
        Long currentId = this.getId().longValue();
        try
        {
            if (Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM)
            {
                ps = DSApi.context().prepareStatement("update " + DSApi.getTableName(AttachmentRevision.class) + " set contentdata = null where id = ?");
                ps.setLong(1, currentId);
                rows = ps.executeUpdate();
                if (rows != 1)
                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
                this.updateFile(stream);
            }
            else
            {
                ps = DSApi.context().prepareStatement("update " + DSApi.getTableName(AttachmentRevision.class) + " set contentdata = ? where id = ?");
                ps.setBinaryStream(1, stream, fileSize);
                ps.setLong(2, currentId);
                rows = ps.executeUpdate();
                
                if (rows != 1)
                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        	org.apache.commons.io.IOUtils.closeQuietly(stream);
        }
    }
    
    public InputStream getAttachmentStream() throws EdmException
    {
    	try
    	{
	    	File file = new File(Attachment.getPath(getId()));
	        if(this.getOnDisc()!=1 || !file.exists())
	        {
	            if (!file.exists() || !file.isFile() || file.length() != getSize().intValue())
	            {            	
	                ShUtils.delete(file);
	                saveToFile(file);
	            }
	        }
	        return new FileInputStream(file);
    	}
    	catch (Exception e) 
    	{
			throw new EdmException(e);
		}
    }

    
	public InputStream getStreamByPage(int page, String format, Boolean fax) throws EdmException 
	{
			File imageFile;
			File file;
			boolean fileConverted = false;
			try
			{
				file = getFile();
			    if (mime.startsWith("image/tiff"))
			    {            
			
			        fileConverted = true;
			        String path = getPathToDecompressedImage(id, format, fax, page);
			          
			        if (Attachment.IMAGES_DECOMPRESSED_IN_LOCAL_FILE_SYSTEM)
			        {
			            imageFile = new File(path);
			            if (!imageFile.exists())
				        {
				        	TIFFReader reader = new TIFFReader(file);
				            RenderedImage im = reader.getPage(page);
				            ImageUtils.save(ImageUtils.getBufferedImage(im), format, imageFile);
				        }
			            return new FileInputStream(imageFile);
			        }
			        else 
			        {
			            ByteArrayOutputStream bos = new ByteArrayOutputStream();
			            TIFFReader reader = new TIFFReader(file);
			            RenderedImage im = reader.getPage(page);
			            ParameterBlock pb = new ParameterBlock();
			            pb.addSource(ImageUtils.getBufferedImage(im));
			            pb.add(bos);
			            pb.add(format);
			            RenderedOp op = JAI.create("encode", pb);
			            op.dispose();
			            bos.close();
			            return new ByteArrayInputStream(bos.toByteArray());
			        }
			    }
			    else if(mime.startsWith("application/pdf"))
			    {
			    	
			    	    fileConverted = true;
				        String path = getPathToDecompressedImage(id, format, fax, page);
				          
				        if (Attachment.IMAGES_DECOMPRESSED_IN_LOCAL_FILE_SYSTEM)
				        {
				            imageFile = new File(path);
				            if (!imageFile.exists())
					        {
				            	 PDDocument document = PDDocument.load(file);
			                	 List<PDPage> pDPages = document.getDocumentCatalog().getAllPages();
			                	 PDPage pDPage = pDPages.get(page);
		                         int rotation = pDPage.findRotation();

	                             BufferedImage image = pDPage.convertToImage(BufferedImage.TYPE_BYTE_GRAY, 300);
	                             if (image != null && rotation != 0) {
	                                 image = ImageUtils.rotateImage(image, rotation);
	                             }
	                            
	                             ImageUtils.save(image, format, imageFile);
					        }
				            return new FileInputStream(imageFile);
				        }
				        else 
				        {
				            ByteArrayOutputStream bos = new ByteArrayOutputStream();
				            PDDocument document = PDDocument.load(file);
		                	 List<PDPage> pDPages = document.getDocumentCatalog().getAllPages();
		                	 PDPage pDPage = pDPages.get(page);
	                         int rotation = pDPage.findRotation();

                            BufferedImage im = pDPage.convertToImage(BufferedImage.TYPE_BYTE_GRAY, 300);
				            ParameterBlock pb = new ParameterBlock();
				            pb.addSource(im);
				            pb.add(bos);
				            pb.add(format);
				            RenderedOp op = JAI.create("encode", pb);
				            op.dispose();
				            bos.close();
				            return new ByteArrayInputStream(bos.toByteArray());
				        }
			    	
                         
			    }
			    else
			    {
			    	return new FileInputStream(file);
			    }
                
             
	         }
	         catch (Exception e)
	         {
	             throw new EdmException(e);
	         }
	}

	public Integer getPageCount() throws EdmException 
	{
		try
		{
			if(mime.endsWith("tiff")||mime.endsWith("tif"))
			{
				TIFFReader reader = new TIFFReader(getFile());
		        return reader.countPages();
			}
			else return 1;
		}
		catch (Exception e) 
		{
			throw new EdmException(e);
		}
	}
	
	public RenderedImage getPage(int page) throws EdmException 
	{
		try
		{
			TIFFReader reader = new TIFFReader(getFile());
			int pageCount = reader.countPages();
            if (page >= pageCount)
                 page = pageCount-1;
            return reader.getPage(page);
		}
		catch (Exception e) 
		{
			throw new EdmException(e);
		}
	}
	
	
}
