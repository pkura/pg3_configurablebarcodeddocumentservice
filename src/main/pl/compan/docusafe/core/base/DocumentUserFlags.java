package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.labels.LabelsManager;

/* User: Administrator, Date: 2005-12-10 11:40:25 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentUserFlags.java,v 1.10 2008/09/16 15:59:29 kubaw Exp $
 */
public class DocumentUserFlags extends Flags implements java.io.Serializable
{

    private transient Document document;

    private String username;

    DocumentUserFlags()
    {
    }

    public DocumentUserFlags(Document document)
    {
        this.document = document;
    }

    public boolean setFl(Long flag, boolean value) throws EdmException
    {
        LabelsManager.addLabel(getDocumentId(), flag, DSApi.context().getPrincipalName());

        addDocumentHistory(flag, value);

        return true;
    }

    public static String getFlagDescription(Long flag) throws EdmException
    {
        return LabelsManager.findLabelById(flag).getDescription();
    }

    public static String getFlagC(Long flag) throws EdmException
    {
    	return LabelsManager.findLabelById(flag).getName(); 
    }    
    
    public boolean canSetFl(int flag, boolean value) throws EdmException
    {
        return true;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final DocumentUserFlags that = (DocumentUserFlags) o;
        if (getDocumentId() != null ? !getDocumentId().equals(that.getDocumentId()) : getDocumentId() != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        return true;
    }

    public int hashCode()
    {
        int result;
        result = (getDocumentId() != null ? getDocumentId().hashCode() : 0);
        result = 29 * result + (username != null ? username.hashCode() : 0);
        return result;
    }
}
