package pl.compan.docusafe.core.base;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FolderPermission.java,v 1.3 2005/11/24 16:16:36 lk Exp $
 */
public class FolderPermission extends ObjectPermission
{
    private Long folderId;

    /**
     * Konstruktor u�ywany przez Hibernate.
     */
    FolderPermission()
    {
    }

    public FolderPermission(Long folderId)
    {
        this.folderId = folderId;
    }

    public Long getFolderId()
    {
        return folderId;
    }

    public void setFolderId(Long folderId)
    {
        this.folderId = folderId;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof FolderPermission)) return false;
        if (!super.equals(o)) return false;

        final FolderPermission folderPermission = (FolderPermission) o;

        if (folderId != null ? !folderId.equals(folderPermission.folderId) : folderPermission.folderId != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result = super.hashCode();
        result = 29 * result + (folderId != null ? folderId.hashCode() : 0);
        return result;
    }
}
