package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;

/**
 * Wyj�tek rzucany podczas usuwania folder�w, je�eli folder nie
 * mo�e by� usuni�ty, poniewa� znajduj� si� w nim dokumenty.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NotEmptyFoldersFoundException.java,v 1.1 2004/05/16 20:10:23 administrator Exp $
 */
public class NotEmptyFoldersFoundException extends EdmException
{
    public NotEmptyFoldersFoundException(String message)
    {
        super(message);
    }

    public NotEmptyFoldersFoundException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NotEmptyFoldersFoundException(Throwable cause)
    {
        super(cause);
    }
}
