package pl.compan.docusafe.core.base;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

/**
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 *
 */
public class BoxLine
{
    private Long id;
    private String line;
    private String name;

    @SuppressWarnings("unchecked")
    public static BoxLine find(String line) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(BoxLine.class);
        c.add(Restrictions.eq("line", line));
        List<BoxLine> list = c.list();
        if (list.size() > 0)
            return list.get(0);
        else
            return null;
    }
    
    public static List<BoxLine> list() throws EdmException
    {
        return Finder.list(BoxLine.class, null);
    }
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLine()
    {
        return line;
    }

    public void setLine(String line)
    {
        this.line = line;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }    
    
}
