package pl.compan.docusafe.core.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.util.StringManager;

/**
 * Klasa reprezentuj�ca encj� DS_IMAGE
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class Image implements Serializable 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * StringManager - klasa odpowiedzialna za komunikaty
	 */
	static final StringManager sm =
        StringManager.getManager(Constants.Package);
	
	/**
	 * Identyfikator obrazu
	 */
	private Long id;
	
	/**
	 * Sk�adowa okre�laj�ca nazw� obrazu
	 */
	private String name;
	
	/**
	 * Sk�adowa okre�laj�ca opis obrazu
	 */
	private String description;
	
	/**
	 * Content-Type obrazu
	 */
	private String contentType;
	
	/**
	 * Nazwa pliku obrazu
	 */
	private String filename;
	
	/**
	 * Konstruktor domy�lny
	 */
	public Image() 
	{
	}

	/**
	 * Konstruktor ustawiaj�cy wszystkie pola
	 * 
	 * @param name
	 * @param description
	 * @param contentType
	 * @param filename
	 */
	public Image(String name, String description, String contentType,
			String filename) 
	{
		super();
		this.name = name;
		this.description = description;
		this.contentType = contentType;
		this.filename = filename;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� id
	 * 
	 * @return
	 */
	public Long getId() 
	{
		return id;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� id
	 * 
	 * @param id
	 */
	public void setId(Long id) 
	{
		this.id = id;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� name
	 * 
	 * @return
	 */
	public String getName() 
	{
		return name;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� name
	 * 
	 * @param name
	 */
	public void setName(String name) 
	{
		this.name = name;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� description
	 * 
	 * @return
	 */
	public String getDescription() 
	{
		return description;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� description
	 * 
	 * @param description
	 */
	public void setDescription(String description) 
	{
		this.description = description;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� contentType
	 * 
	 * @return
	 */
	public String getContentType() 
	{
		return contentType;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� contentType
	 * 
	 * @param contentType
	 */
	public void setContentType(String contentType) 
	{
		this.contentType = contentType;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� filename
	 * 
	 * @return
	 */
	public String getFilename() 
	{
		return filename;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� filename
	 * 
	 * @param filename
	 */
	public void setFilename(String filename) 
	{
		this.filename = filename;
	}
	
	/**
	 * Zwraca BinaryStrem mapki dojazdu z bazy danych
	 * 
	 * @return
	 * @throws EdmException
	 */
	public BlobInputStream getImageBinaryStream()
		throws EdmException
	{
		boolean closeStm = true;
        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select dsimg.image from " + DSApi.getTableName(Image.class) + " dsimg  where dsimg.id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
                throw new EdmException(sm.getString("documentHelper.blobRetrieveFailed", id));

            InputStream is = rs.getBinaryStream(1);
            // sterownik dla SQLServera nie wspiera ResultSet.getBlob
            //Blob blob = rs.getBlob(1);
            if (is != null)
            {
            	closeStm = false;
                return new BlobInputStream(is, pst);
            }
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "id=" + id);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	if (closeStm)
        	{
        		DSApi.context().closeStatement(pst);
        	}
        }
        // Nie mo�na zamkn�� obiektu Statement od razu po zwr�ceniu strumienia,
        // poniewa� w�wczas nie b�dzie mo�liwe odczytanie danych ze strumienia.
        // Statement jest zamykane w metodzie BlobInputStream.closeStream()
	}
	
	public void updateImage(File file) throws Exception {
		 FileInputStream stream = null; 
	        try
	        {
	            stream = new FileInputStream(file);
	            int fileSize = (int) file.length();
	            PreparedStatement ps = null;
	            try
	            {
	                DSApi.context().session().save(this);
	                DSApi.context().session().flush();

	                int rows;
	                ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(Image.class)+
	                    " set image = ? where id = ?");
	                ps.setBinaryStream(1, stream, fileSize);
	               // ps.setBlob(1, stream,fileSize);
	                //ps.setB
	                ps.setLong(2, this.getId().longValue());
	                rows = ps.executeUpdate();
	                if (rows != 1)
	                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
	            }
	            catch (HibernateException e)
	            {
	                throw new EdmHibernateException(e);
	            }
	            catch (SQLException e)
	            {
	                throw new EdmSQLException(e);
	            }
	            finally
	            {
	                DSApi.context().closeStatement(ps);
	            }
	        }
	        catch (IOException e)
	        {
	            throw new EdmException("B��d odczytu pliku", e);
	        }
	        finally
	        {
	            if (stream != null) try { stream.close(); } catch (Exception e) { }
	        }
	}
	
	/**
	 * Metoda wstawiaj�ca InputStream obrazka do bazy danych
	 * 
	 * @param stream
	 * @param size
	 * @throws EdmException
	 */
	public void updateImageBinaryStream(InputStream stream, int size) 
		throws EdmException
    {
        PreparedStatement ps = null;
        try
        {

            int rows;
            ps = DSApi.context().prepareStatement("update " + DSApi.getTableName(Image.class) +
                        " set image = ? where id = ?");
            ps.setBinaryStream(1, stream, size);
            ps.setLong(2, this.getId().longValue());
            rows = ps.executeUpdate();

            if (rows != 1)
                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
        }
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
            if (stream != null) try { stream.close(); } catch (Exception e) { }
        }
    }
	
	/**
	 * Metoda wyszukuj�ca w bazie danych obiekt Image po podanym id
	 * 
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public static Image find(Long id) throws EdmException
	{
		return (Image) Finder.find(Image.class, id);
	}


}
