package pl.compan.docusafe.core.base;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.hibernate.usertype.UserType;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentWatch.java,v 1.23 2010/02/16 12:33:49 mariuszk Exp $
 */
public class DocumentWatch implements java.io.Serializable, Cloneable
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);
    private static final Log log = LogFactory.getLog(DocumentWatch.class);

    public static class Type 
    {
    	private int type;
    	
    	public static final Type DOCUMENT_MODIFIED = new Type(1);
        public static final Type ATTACHMENT_DELETED = new Type(2);
        public static final Type ATTACHMENT_ADDED = new Type(3);
        public static final Type ATTACHMENT_REVISION_ADDED = new Type(4);
        public static final Type ANY = new Type(5);
        public static final Type INVOICE_ASSIGNED = new Type(6);
    	
    	public Type(int type)
        {
            this.type = type;
        }
    	
    	private Object readResolve() {
            return getInstance(type);
        }

        public static Type getInstance(int type) {
            if (type == 1)
            	return DOCUMENT_MODIFIED;
            else if (type == 2)
            	return ATTACHMENT_DELETED;
            else if (type == 3)
            	return ATTACHMENT_ADDED;
            else
            	return ATTACHMENT_REVISION_ADDED;        
        }

        public int getValue()
        {
            return type;
        }
    }
    
    public static class EnumType implements UserType
    {    	
    	private static final int[] SQL_TYPES = {Types.INTEGER};
    	
    	public int[] sqlTypes() { return SQL_TYPES; }
    	public Class returnedClass() { return Type.class; }
    	public boolean equals(Object x, Object y) { return x == y; }
    	public Object deepCopy(Object value) { return value; }
    	public boolean isMutable() { return false; }
    	public Object replace(Object original, Object target, Object owner) throws HibernateException
    	{
    		return original;
    	}
    	public Object assemble(java.io.Serializable cached, Object owner) throws HibernateException
    	{
    		return cached;
    	}
    	public void nullSafeSet(PreparedStatement statement, Object value, int index) throws HibernateException, SQLException {	
			if (value == null) {
				statement.setObject(index, null);
			} else {
				statement.setInt(index, ((Type)value).getValue());
			}
		}
        public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner) throws HibernateException, SQLException {
			//String name = resultSet.getString(names[0]);
			int type = resultSet.getInt(names[0]);
			return resultSet.wasNull() ? null : Type.getInstance(type);
		}
    	public java.io.Serializable disassemble(Object value) throws HibernateException
    	{
    		return (Integer) value;
    	}
    	public int hashCode(Object x) throws HibernateException
    	{
    		return 0;
    	}
    }
    
   /* public static class Type extends PersistentEnumSupport implements java.io.Serializable
    {
        private int type;

        public static final Type DOCUMENT_MODIFIED = new Type(1);
        public static final Type ATTACHMENT_DELETED = new Type(2);
        public static final Type ATTACHMENT_ADDED = new Type(3);
        public static final Type ATTACHMENT_REVISION_ADDED = new Type(4);

        public static final Type[] types;
        static
        {
            checkUniqueness(Type.class);
            types = (Type[]) getValues(Type.class);
        }

        private Object readResolve() throws ObjectStreamException
        {
            return fromInt(type);
        }

        private Type(int type)
        {
            this.type = type;
        }

        public String getDescription()
        {
            return sm.getString("documentWatch.type."+type);
        }

        public int toInt()
        {
            return type;
        }

        public String toString()
        {
            return String.valueOf(type);
        }

        public static PersistentEnum fromInt(int value)
        {
            return fromInt(DocumentWatch.Type.class, value);
//            switch (value)
//            {
//                case 1: return DOCUMENT_MODIFIED;
//                case 2: return ATTACHMENT_DELETED;
//                case 3: return ATTACHMENT_ADDED;
//                case 4: return ATTACHMENT_REVISION_ADDED;
//                default: throw new IllegalArgumentException("type="+value);
//            }
        }
    }  */

    private Long id;
    private Long documentId;
    private String username;
    private Type type;


    public DocumentWatch cloneObject() throws EdmException
    {
        try
        {
            DocumentWatch documentWatch = (DocumentWatch) super.clone();
            documentWatch.duplicate();
            return documentWatch;
        }
        catch (CloneNotSupportedException e)
        {
            throw new Error(e);
        }
    }

    protected void duplicate() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }


    /**
     * Usuwa wskazany obiekt DocumentWatch.
     * @param id
     * @throws EdmException
     */
    public static void deleteWatch(Long id) throws EdmException
    {
        if (id == null)
            throw new NullPointerException("id");

        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(DocumentWatch.class);
            criteria.add(Restrictions.eq("id", id));

            List<DocumentWatch> list = criteria.list();
            if(list.size() > 0) {
                DocumentWatch documentWatch = list.get(0);

                DSApi.context().session().delete(documentWatch);
                DSApi.context().session().flush();
            } else {
                log.error("[deleteWatch] watch not found, id = "+id);
            }
//            DSApi.context().session().delete(
//                "from w in class "+DocumentWatch.class.getName()+
//                " where w.id = "+id);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca wszystkie obiekty DocumentWatch przypisane do zalogowanego
     * u�ytkownika.
     */
    public static DocumentWatch[] getWatches() throws EdmException
    {
        try
        {
        	org.hibernate.Query query = DSApi.context().session().createQuery("select w from w in class "+DocumentWatch.class.getName()+
                    " where w.username = ?");
        	query.setString(0, DSApi.context().getPrincipalName());        	
            List<DocumentWatch> watches = query.list();
            return watches.toArray(new DocumentWatch[watches.size()]);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public Object clone()
    {
        try
        {
            return super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            throw new Error(e.getMessage(), e);
        }
    }

    /**
     * Kolejkuje obiekt do wys�ania powiadomienia. Powiadomienie jest
     * wysy�ane dopiero po wywo�aniu metody execute.
     */
    void queue(Map<String, String> args) throws EdmException
    {
        if (DSApi.context().getPrincipalName().equals(username))
        {
            if (log.isDebugEnabled())
                log.debug("queue: Pomijanie u�ytkownika "+username+" - nie dostanie " +
                    "powiadomienia o w�asnych zmianach");
            return;
        }

        DSApi.context().queueWatch(this, args);
    }

    public void execute(Map<String, String> args) throws EdmException
    {
        if (log.isDebugEnabled())
            log.debug("Rejestrowanie powiadomienia "+toString()+" do u�ytkownika "+username+
                ", args="+args);
        EventFactory.registerEvent("immediateTrigger", "DocumentWatchSender",documentId+";"+username+";"+type.getValue(),null, new Date());
    }

    /**
     * Prywatny konstruktor u�ywany przez Hibernate.
     */
    private DocumentWatch()
    {
    }

    public DocumentWatch(Type type, Long documentId, String username)
    {
        if (type == null)
            throw new NullPointerException("type");
        if (documentId == null)
            throw new NullPointerException("documentId");
        if (username == null)
            throw new NullPointerException("username");

        this.type = type;
        this.documentId = documentId;
        this.username = username;
    }

    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    public String toString()
    {
        return getClass().getName()+"[id="+id+" type="+type+" username="+username+
            " documentId="+documentId+"]";
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof DocumentWatch)) return false;

        final DocumentWatch documentWatch = (DocumentWatch) o;

        if (documentId != null ? !documentId.equals(documentWatch.documentId) : documentWatch.documentId != null) return false;
        if (type != null ? !type.equals(documentWatch.type) : documentWatch.type != null) return false;
        if (username != null ? !username.equals(documentWatch.username) : documentWatch.username != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (documentId != null ? documentId.hashCode() : 0);
        result = 29 * result + (username != null ? username.hashCode() : 0);
        result = 29 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
