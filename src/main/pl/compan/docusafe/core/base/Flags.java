package pl.compan.docusafe.core.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.labels.DocumentToLabelBean;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Flags.java,v 1.32 2010/08/05 15:57:17 mariuszk Exp $
 */
public abstract class Flags
{
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(Flag.class.getPackage().getName(),null);
    
    private Long documentId;

    /* API do edycji flag (usuwanie, zmiana, dodawanie) */

    public static class FlagBean
    {
        private Long id;
        private String c;
        private String description;
        /** grupa maj�ca prawo nadawania tej flagi (gdy null - ka�dy mo�e nadawa�) */
        private String divisionGuid;
        private String viewersDivisionGuid;

        public FlagBean(Long id, String c, String description, String divisionGuid, String viewersDivisionGuid)
        {
            this.id = id;
            this.c = c;
            this.description = description;
            this.divisionGuid = divisionGuid;
            this.viewersDivisionGuid = viewersDivisionGuid;
        }

        public Long getId()
        {
            return id;
        }

        public String getC()
        {
            return c;
        }

        public String getDescription()
        {
            return description;
        }

        public String getDivisionGuid()
        {
            return divisionGuid;
        }

        public String getViewersDivisionGuid()
        {
            return viewersDivisionGuid;
        }        
    }

    public static FlagBean getFlagBeanForLabel(Label l)
    {
    	return new FlagBean(l.getId(),l.getName(),l.getDescription(), l.getWriteRights(), l.getReadRights());
    }
    
    
    /**
     * Zwraca list� flag posortowanych po kodzie.
     * @return Lista obiekt�w FlagBean.
     */
    public static List<FlagBean> listFlags(boolean userFlags)
        throws EdmException
    {
    	List<FlagBean> flags = new ArrayList<FlagBean>();
    	if(userFlags)
    	{
    		for(Label l:LabelsManager.getPrivateModifyLabelsForUser(DSApi.context().getPrincipalName(), Label.FLAG_TYPE))
    		{
    			flags.add(getFlagBeanForLabel(l));
    		}
    	}
    	else
    	{
    		for(Label l:LabelsManager.getNonPrivateModifyLabelsForUser(DSApi.context().getPrincipalName(), Label.FLAG_TYPE))
    		{
    			flags.add(getFlagBeanForLabel(l));
    		}
    	}
    	return flags;
      
    }
    

    /**
     * Tworzy now� flag�.
     * @param userFlags
     * @param c
     * @param description
     * @param divisionGuid grupa, kt�ra ma prawo nadawania tej flagi (null oznacza, �e ka�dy ma takie prawo)
     * @return Identyfikator utworzonej flagi.
     */
    public static void addFlag(boolean userFlags, String c, String description, String divisionGuid, String viewersDivisionGuid)
        throws EdmException
    {
        LabelsManager.createNewFlag(userFlags, c, description, divisionGuid, viewersDivisionGuid);
    }

    /**
     * Aktualizuje flag� o danym identyfikatorze.
     */
    public static void updateFlag(boolean userFlags, Long id,
                                  String c, String description, String divisionGuid,String viewersDivisionGuid)
        throws EdmException
    {
    	StringManager sm =
            GlobalPreferences.loadPropertiesFile(Flag.class.getPackage().getName(),null);
        //if (id < 1 || id > getMaxFlags(userFlags))
            //throw new EdmException(sm.getString("NiepoprawnyNumerFlagi",id));
        if (c == null)
            throw new NullPointerException("c");
        if (description == null)
            throw new NullPointerException("description");
        /*if (c.length() > 3)
            throw new EdmException(sm.getString("NiepoprawnaWartosc",c));*/

        LabelsManager.updateFlag(id, userFlags, c, description, divisionGuid, viewersDivisionGuid);
        /*Preferences prefs = getPreferences(userFlags);
        prefs.put(String.valueOf(id), c + ";" + description + (divisionGuid != null ? ";" + divisionGuid : ";all")+
                                    (viewersDivisionGuid != null ? ";" + viewersDivisionGuid : ";all"));*/
        
        
    }

    /**
     * Usuwa flag� o danym identyfikatorze, uprzednio ustawiaj�c
     * jej warto�� na false dla wszystkich dokument�w.
     */
    public static void removeFlag(boolean userFlags, Long id)
        throws EdmException
    {
        /*Preferences prefs = getPreferences(userFlags);
        prefs.remove(String.valueOf(id));
        unsetFlag(userFlags, id);*/
        LabelsManager.deleteLabel(id);
    }

    public static FlagBean getFlagBean(Long id)
        throws EdmException
    {
    	return getFlagBeanForLabel(LabelsManager.findLabelById(id));
    }



    public static class Flag
    {
        private Long id;
        private String c;
        private String description;
        private boolean value;
        private String author;
        private boolean canClear;
        private boolean canEdit;
        private boolean canView;
        private boolean userFlag;

        private Flag(Long id, String c, String description, boolean value,
             boolean userFlag, String author, boolean canClear, boolean canEdit, boolean canView)
        {
            this.id = id;
            this.c = c;
            this.description = description;
            this.value = value;
            this.userFlag = userFlag;
            this.author = author;
            this.canClear = canClear;
            this.canEdit = canEdit;
            this.canView = canView;
        }

        public boolean equals(Object o)
        {
        	if (o instanceof Flags.Flag) 
        	{
	    	      Flags.Flag c = (Flags.Flag) o;
	    	      if (this.id.equals(c.getId())) return true;
    	    }
    	    return false;
        }
        
        public boolean isCanClear()
        {
            return canClear;
        }

        public boolean isCanEdit()
        {
            return canEdit;
        }

        public boolean isUserFlag()
        {
            return userFlag;
        }

        public Long getId()
        {
            return id;
        }

        public String getC()
        {
            return c;
        }

        public String getDescription()
        {
            return description;
        }

        public String getAuthor()
        {
            return author;
        }
        
        public boolean isCanView()
        {
            return canView;
        }

        public boolean isValue()
        {
            return value;
        }
        
        public boolean getValue()
        {
            return value;
        }

        public String toString()
        {
            return "Flag[id="+id+" c="+c+" value="+value+" author="+author+
                " canClear="+canClear+" userFlag="+userFlag+
                "]";
        }
        
        public void setValue(boolean value)
        {
            this.value = value;
        }
    }


    /**
     * Zwraca true, je�eli zdefiniowano flag� o danym identyfikatorze.
     */
    public boolean flagPresent(Long flag) throws EdmException
    {
    	/*StringManager sm =
            GlobalPreferences.loadPropertiesFile(Flag.class.getPackage().getName(),null);*/

        // co najmniej trzy znaki "X;x"
        return LabelsManager.isFlagPresent(documentId, flag);
    }

    /**
     * Zwraca obiekt reprezentuj�cy flag�.
     * @param flag Numer flagi (1-MAX_FLAGS)
     * @return Obiekt flag lub null.
     */
    public Label getFl(Long flag) throws EdmException
    {
        if(LabelsManager.existsDocumentToLabelBean(getDocumentId(), flag))
        {
        	return LabelsManager.findLabelById(flag);
        }
        return null;
    }


  

    protected void addDocumentHistory(Long flag, boolean value) throws EdmException
    {
        Document document = Document.find(getDocumentId());
        Label l = LabelsManager.findLabelById(flag);
        if (document instanceof OfficeDocument)
        {
            String c = l.getName();
            String description = l.getDescription();

         //   String message = (value ? "Ustawiono" : "Usuni�to") +
         //       " flag� "+(this instanceof DocumentUserFlags ? "u�ytkownika" : "wsp�ln�") +
        //        ": "+description+" ("+c+")";
            String tmp = null;
            if (value)
                if (this instanceof DocumentUserFlags)
                    tmp = sm.getString("UstawionoFlageUzytkownika",description,c);
                else
                    tmp = sm.getString("UstawionoFlageWspolna",description,c);
            else
                if (this instanceof DocumentUserFlags)
                    tmp = sm.getString("UsunietoFlageUzytkownika",description,c);
                else
                    tmp = sm.getString("UsunietoFlageWspolna",description,c);

            ((OfficeDocument) document).addWorkHistoryEntry(Audit.create("flags", DSApi.context().getPrincipalName(),
                    tmp));
        }
    }

    /**
     * Ustawia warto�� wskazanej flagi.
     * @param flag Numer flagi (1-MAX_FLAGS)
     * @param value
     * @return True, je�eli warto�� flagi uleg�a zmianie.
     * TODO: chyba nie trzeba odczytywa�
     * @throws EdmException Je�eli numer flagi jest niepoprawny.
     */
    protected void setFlField(Long flag, boolean value) throws EdmException
    {
    	if(value)
    		LabelsManager.addLabel(flag, documentId, DSApi.context().getPrincipalName());
    	else LabelsManager.removeLabel(documentId, flag);      
    }

    /**
     * Zwraca nazw� u�ytkownika, kt�ry jako ostatni zmieni�
     * warto�� flagi lub null, je�eli flaga o danym numerze
     * nie zosta�a zdefiniowana.
     */
    public String getAuthor(Long flag) throws EdmException
    {
        List<DocumentToLabelBean> dtlbs = LabelsManager.findDocumentToLabelBean(getDocumentId(), flag);
        if(dtlbs!=null && dtlbs.size()>0)
        {
        	return dtlbs.get(0).getUsername();
        }
        else
        {
        	return null;
        }
    }

    public Date getMtime(Long flag) throws EdmException
    {
    	List<DocumentToLabelBean> dtlbs = LabelsManager.findDocumentToLabelBean(getDocumentId(), flag);
        if(dtlbs!=null && dtlbs.size()>0)
        {
        	return dtlbs.get(0).getCtime();
        }
        else
        {
        	return null;
        }
    }

    /**
     * @TODO - funkcja jest do dupy - i dziala odwrotnie jak ma dzialac
     * Ma sprawdzic jednym pytaniem i istniejace flagi i uprawnienia i zrobic obie listy na raz
     * 1. Jezeli sie tak nie da to chociaz odwrocic kolejnosc - najpierw pobrac flagi, a potem dla flag
     * dokumentow pobrac etykiety. 
     * @param userFlags
     * @return
     * @throws EdmException
     */
    public List<Flag> getDocumentFlags(boolean userFlags) throws EdmException
    {
    	List<Label> list = LabelsManager.getWriteLabelsForUser(DSApi.context().getPrincipalName(), Label.FLAG_TYPE);
    	List<Flag> res = new ArrayList<Flag>();
    	Flag f;
    	Map<Long,String> lMap = LabelsManager.getLabelBeanByDocumentId(documentId,Label.FLAG_TYPE);
    	for(Label l: list)
    	{
    		if(l.getWriteRights().equals(DSApi.context().getPrincipalName())!=userFlags) continue;
    		f = new Flag(l.getId(), l.getName(),l.getDescription(), lMap.containsKey(l.getId()), 
    				l.getWriteRights().equals(DSApi.context().getPrincipalName()), lMap.get(l.getId()),
    				true,true,true);
    		res.add(f);
    	}
    	list = LabelsManager.getReadLabelsForUser(DSApi.context().getPrincipalName(), Label.FLAG_TYPE);
    	for(Label l: list)
    	{
    		if(l.getReadRights().equals(DSApi.context().getPrincipalName())!=userFlags) continue;
    		f = new Flag(l.getId(), l.getName(),l.getDescription(), lMap.containsKey(l.getId()), 
    				l.getReadRights().equals(DSApi.context().getPrincipalName()),lMap.get(l.getId()),
    				false,false,true);
    		if(!res.contains(f))
    			res.add(f);
    	}
    	return res;
    }


    public Long getDocumentId()
    {
        return documentId;
    }

    void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }
}
