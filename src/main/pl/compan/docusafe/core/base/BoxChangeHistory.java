package pl.compan.docusafe.core.base;

import java.util.Date;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

public class BoxChangeHistory {
    private Long id;
    private Long idDoc;
    private String boxToName;
    private String boxFromName;
    private Date curDate;
    
    private static final Logger log = LoggerFactory.getLogger(BoxChangeHistory.class);
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania do historii"+e.getMessage());
            throw new EdmException("Blad Dodania Do Historii"+e.getMessage());
        }
    }
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdDoc() {
		return idDoc;
	}
	public void setIdDoc(Long idDoc) {
		this.idDoc = idDoc;
	}
	public String getBoxToName() {
		return boxToName;
	}
	public void setBoxToName(String boxToName) {
		this.boxToName = boxToName;
	}
	public String getBoxFromName() {
		return boxFromName;
	}
	public void setBoxFromName(String boxFromName) {
		this.boxFromName = boxFromName;
	}
	public Date getCurDate() {
		return curDate;
	}
	public void setCurDate(Date curDate) {
		this.curDate = curDate;
	}

}
