package pl.compan.docusafe.core.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.util.StringManager;

/**
 * User: Michal Manski
 * Date: 2006-05-16
 */
public class AttachmentRevisionSignature
{
    private Long id;
    private AttachmentRevision attachmentRevision;
    private String author;
    private Date ctime;
    private Boolean correctlyVerified;    
  
    static final StringManager sm =
        StringManager.getManager(Constants.Package);

    static public final String SIG_EXTENSION = ".pem";
    
    public synchronized void create() throws EdmException
    {
        try 
        {
            this.ctime = new Date();
            this.author = DSApi.context().getPrincipalName();
            
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static AttachmentRevisionSignature find(Long id) throws EdmException
    {
        return (AttachmentRevisionSignature) Finder.find(AttachmentRevisionSignature.class, id);
    }
    
    public static List<AttachmentRevisionSignature> findByAttachmentRevisionId(Long attachRevId) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(AttachmentRevisionSignature.class);
        
        try
        {  
            c.createCriteria("attachmentRevision").add(org.hibernate.criterion.Expression.eq("id",attachRevId));
            c.addOrder(org.hibernate.criterion.Order.desc("ctime"));
            return c.list();
           
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Zapisuje do bazy zawartosc podpisu z pliku podanego jako parametr.
     * 
     * @param file plik z podpisem
     * @throws EdmException
     */
    public void saveContent(File file) throws EdmException
    {
        FileInputStream stream = null;
        try 
        {
            stream = new FileInputStream(file);
            int fileSize = (int) file.length();
            PreparedStatement ps = null;
            
            try 
            {
                DSApi.context().session().save(this);
                DSApi.context().session().flush();
                
                int rows;
                if (DSApi.isOracleServer())
                {
                    ps = DSApi.context().prepareStatement("select contentdata from "+DSApi.getTableName(AttachmentRevisionSignature.class)+
                        " where id = ? for update");
                    ps.setLong(1, this.getId().longValue());
    
                    ResultSet rs = ps.executeQuery();
                    if (!rs.next())
                        throw new EdmException("Nie mo�na odczyta� tre�ci podpisu za��cznika "+this.getId());
    
                    rows = 1;
    
                    Blob blob = rs.getBlob(1);
                    if (!(blob instanceof oracle.sql.BLOB))
                        throw new EdmException("Spodziewano si� obiektu oracle.sql.BLOB");
    
                    OutputStream out = ((oracle.sql.BLOB) blob).getBinaryOutputStream();
                    byte[] buf = new byte[2048];
                    int count;
                    while ((count = stream.read(buf)) > 0)
                    {
                        out.write(buf, 0, count);
                    }
                    out.close();
                    stream.close();
                }
                else
                {
                    ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(AttachmentRevisionSignature.class)+
                        " set contentdata = ? where id = ?");
                    ps.setBinaryStream(1, stream, fileSize);
                    ps.setLong(2, this.getId().longValue());
                    rows = ps.executeUpdate();
                }
                
                if (rows != 1)
                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
            }
            catch (IOException e)
            {
                throw new EdmException("B��d podczas zapisu danych", e);
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }
            finally
            {
            	DSApi.context().closeStatement(ps);                
            }
        }
        catch (IOException e)
        {
            throw new EdmException("B��d odczytu pliku", e);
        }
        finally
        {
            if (stream != null) try { stream.close(); } catch (Exception e) { }
        }
    }
    
    /**
     * Pobiera strumie� binarny z podpisem dokumentu.
     * Metoda powinna by� wywo�ywana tylko i wy��cznie w ramach
     * transakcji, w innym przypadku niekt�re sterowniki JDBC
     * b�d� pr�bowa�y wczyta� ca�� tre�� bloba do pami�ci.
     * <p>
     * Zwracany strumie� zawiera w sobie obiekt Statement u�yty
     * do odczytania BLOB-a, dlatego powinien by� zawsze zamykany
     * przy pomocy metody closeStream(). Metoda close() rzuca
     * wyj�tek.
     * @return
     * @throws EdmException
     */
    public BlobInputStream getBinaryStream()
        throws EdmException
    {

     /*   if (!attachment.getDocument().canReadAttachments())
            throw new AccessDeniedException("Brak uprawnie� do odczytu tre�ci " +
                "za��cznika");*/

        PreparedStatement pst = null;
        boolean closeStm = true;
        try
        {
            pst = DSApi.context().prepareStatement("select contentdata from ds_attachment_signature where id = ?");
            pst.setLong(1, getId());
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
                throw new EdmException(sm.getString("documentHelper.blobRetrieveFailed", getId()));

            InputStream is = rs.getBinaryStream(1);
            // sterownik dla SQLServera nie wspiera ResultSet.getBlob
            //Blob blob = rs.getBlob(1);
            if (is != null)
            {
            	closeStm = false;
                return new BlobInputStream(is, pst);
            }
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "signature="+getId());
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally 
        {
        	if (closeStm)
        	{
        		DSApi.context().closeStatement(pst);
        	}
        }
        // Nie mo�na zamkn�� obiektu Statement od razu po zwr�ceniu strumienia,
        // poniewa� w�wczas nie b�dzie mo�liwe odczytanie danych ze strumienia.
        // Statement jest zamykane w metodzie BlobInputStream.closeStream()
    }
    
    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }
    
    public AttachmentRevision getAttachmentRevision()
    {
        return attachmentRevision;
    }

    public void setAttachmentRevision(AttachmentRevision attachmentRevision)
    {
        this.attachmentRevision = attachmentRevision;
    }
    
    public String getAuthor()
    {
        return author;
    }

    private void setAuthor(String author)
    {
        this.author = author;
    }
    
    public Date getCtime()
    {
        return ctime;
    }

    private void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }
    
    public Boolean getCorrectlyVerified()
    {
        return correctlyVerified;
    }

    public void setCorrectlyVerified(Boolean correctlyVerified)
    {
        this.correctlyVerified = correctlyVerified;
    }    
}
