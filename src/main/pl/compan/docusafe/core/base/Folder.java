package pl.compan.docusafe.core.base;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.emory.mathcs.backport.java.util.Arrays;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

/**
 * Folder, element hierarchicznej struktury, w kt�rej znajduj�
 * si� dokumenty.
 * <p>
 * Uprawnienia do wywo�ywania metod publicznych s� sprawdzane
 * w aspekcie FolderSA.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Folder.java,v 1.43 2009/12/22 15:06:51 tomekl Exp $
 */
public class Folder implements Lifecycle
{
	static final Logger log = LoggerFactory.getLogger(Folder.class);
    static final StringManager sm =
        StringManager.getManager(Constants.Package);

    // sta�e o nazwach zaczynaj�cych si� od SN_ s� warto�ciami
    // dla atrybutu Folder.systemName

    public static final String SN_TRASH = "trash";
    public static final String SN_OFFICE = "office";
    
    /**
     * Folder posiadaj�cy ten atrybut nie mo�e zosta� usuni�ty.
     */
    public static final int ATT_UNDELETABLE = 1;

    private Long id;
    private String title;
    private Date ctime;
    private Folder parent;
    private Long parentId;
    private Set<Folder> children;
    private Integer hversion;
    private String author;
    private String systemName;
    private int attributes;
    private String lparam;
    private Long wparam;
    
    /**przy przeladowaniu strony przegladarki widok ustawia sie automatycznie na pierwszego
     *ojca wskazanego folderu ktory ma ustawiona ta flage */
    private Integer anchor;

    /**
     * Metoda sprawdza czy jest folder podrzedny
     * @param title
     * @return
     * @throws EdmException 
     */
    public boolean hasChild(String title) throws EdmException
    {
    	log.trace("hasChild({})",title);
        if (getChildren() != null)
        {
            for (Folder folder :getChildren())
            {
                log.trace("compareTo={}",folder.getTitle());
                if (folder.getTitle().equals(title))
                    return true;
            }
        }
        return false;
    }

    /**
     * Metoda sprawdza czy folder istnieje, jesli nie to go tworzy
     * @param title
     * @return
     * @throws EdmException
     */

    public Folder createSubfolderIfNotPresent(String title)
        throws EdmException
    {
        return createSubfolderIfNotPresent(title, true);
    }

	/**
	 * Metoda sprawdza czy folder istnieje jesli nie ma go to go tworzy
	 * @param title
	 * @param copyPermissions
	 * @return
	 * @throws EdmException
	 */

    public Folder createSubfolderIfNotPresent(String title, boolean copyPermissions)
        throws EdmException
    {
    	log.trace("createSubfolderIfNotPresent({})",title);
        if (getChildren() != null)
        {
        	for (Folder folder : getChildren())
            {
            	log.trace("compareTo={}",folder.getTitle());                
                if (folder.getTitle().equals(title))
                    return folder;
            }
        }

        return createSubfolder(title, copyPermissions);
    }
    
    public Folder createSubfolderIfNotPresent(String title, String... copyPermissionsSubject) throws EdmException
	{
		log.trace("createSubfolderIfNotPresent({})",title);
	    if (getChildren() != null)
	    {
	    	for (Folder folder : getChildren())
	        {
	        	log.trace("compareTo={}",folder.getTitle());                
	            if (folder.getTitle().equals(title))
	                return folder;
	        }
	    }
	
	    return createSubfolder(title, copyPermissionsSubject);
	}

    public static Long findRootFolderChildIdByTitle(String title) throws FolderNotFoundException, EdmException
    {
    	Folder folder = Folder.getRootFolder();
    	if(folder.getChildren() != null)
    	{
    	
    		for(Folder fl : folder.getChildren())
    		{
    			if (fl.getTitle().equals(title))
    				return fl.getId();
    		}
    	}
    	
    	return Folder.getRootFolder().getId();
    }
    
    /**
     * Metoda tworzy podfolder, przepisujac uprawnienia
     * @param title
     * @return
     * @throws EdmException
     */
    public Folder createSubfolder(String title) throws EdmException
    {
        return createSubfolder(title, true);
    }

    public Folder createSubfolder(String title, boolean copyPermissions) throws EdmException
    {
    	if(copyPermissions)
    	{
    		return createSubfolder(title, ObjectPermission.USER, ObjectPermission.ANY, ObjectPermission.GROUP);
    	}
    	else
    	{
    		return createSubfolder(title, new String[0]);
    	}
    }
    
    /**
     * Metoda tworzy podfolder przepisujac uprawnienia
     * @param title
     * @param copyPermissions
     * @return
     * @throws EdmException
     */
    public Folder createSubfolder(String title, String... permissionSubjectType)
        throws EdmException
    {
    	log.trace("createSubfolder({})",title);
        if (title == null)
            throw new NullPointerException("title");

        if (getChildren() != null)
        {
        	for (Folder folder :getChildren())
            {              
                if (folder.getTitle().equals(title))
                    throw new DuplicateNameException("Istnieje ju� folder maj�cy tytu� \""+title+"\"");
            }
        }

        Folder child = new Folder();
        child.setTitle(title);
        child.setParent(this);
        addChildren(child);

        try
        {
            DSApi.context().session().save(child);

            /*DSApi.context().grant(child, new String[] {
                ObjectPermission.READ, ObjectPermission.CREATE,
                ObjectPermission.MODIFY, ObjectPermission.DELETE
            });*/

            /*if (copyPermissions)
            {
                // kopiowanie uprawnie� z folderu bie��cego na tworzony
                List permissions = ObjectPermission.find(Folder.class, id);

                for (Iterator iter=permissions.iterator(); iter.hasNext(); )
                {
                    ObjectPermission permission = (ObjectPermission) iter.next();

                    // bie��cy u�ytkownik dostaje uprawnienia oddzielnie
                    if (permission.getSubjectType().equals(ObjectPermission.USER) && permission.getSubject().equals(DSApi.context().getPrincipalName()))
                        continue;

                    FolderPermission clone = (FolderPermission) permission.cloneObject();
                    log.trace("copy {}/{}",permission.getName(),permission.getSubject());
                    clone.setFolderId(child.getId());
                    DSApi.context().session().save(clone);
                }
            }*/
            
            if (permissionSubjectType.length > 0)
            {
            	for(ObjectPermission permission : ObjectPermission.findObjectPermission(Folder.class, id))
            	{
            		/*if (permission.getSubjectType().equals(ObjectPermission.USER) && permission.getSubject().equals(DSApi.context().getPrincipalName()))
                        continue;*/
            		
            		if(Arrays.asList(permissionSubjectType).contains(permission.getSubjectType()))
            		{
            			FolderPermission clone = (FolderPermission) permission.cloneObject();
                        log.trace("copy {}/{}",permission.getName(),permission.getSubject());
                        clone.setFolderId(child.getId());
                        DSApi.context().session().save(clone);
            		}
            		
            	}
            }
            
            //poniewaz lecialy bledy podczas dodawania dokumentow
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }

        return child;
    }

	/**
     * Konstruktor u�ywany przez Hibernate.
     */
    Folder()
    {
    }


    public boolean canRead() throws EdmException
    {
        if (id == null)
            return true;

        if (DSApi.context().isAdmin())
            return true;

        if (isSystem(Folder.SN_OFFICE))
            return true;

        return DSApi.context().folderPermission(id, ObjectPermission.READ);
        // return DSApi.context().canRead(this);
    }

    public boolean canCreate() throws EdmException
    {
        if (id == null)
            return true;

        if (DSApi.context().isAdmin())
            return true;

        if (isSystem(Folder.SN_OFFICE))
            return true;

        return DSApi.context().folderPermission(id, ObjectPermission.CREATE);
        //return DSApi.context().canCreate(this);
    }

    public boolean canModify() throws EdmException
    {
        if (id == null)
            return true;

        if (DSApi.context().isAdmin())
            return true;

        if (isSystem(Folder.SN_OFFICE))
            return true;

        return DSApi.context().folderPermission(id, ObjectPermission.MODIFY);
        //return DSApi.context().canModify(this);
    }

    /**
     * Folderu nie mo�na usun�� r�wnie� wtedy, gdy posiada atrybut
     * {@link #ATT_UNDELETABLE}.
     */
    public boolean canDelete() throws EdmException
    {
        if (id == null)
            return true;

        if (DSApi.context().isAdmin())
            return true;

        if (isSystem(Folder.SN_OFFICE))
            return true;

        return DSApi.context().folderPermission(id, ObjectPermission.DELETE);
        //return DSApi.context().canDelete(this);
    }

    /**
     * Zwraca true, je�eli folder posiada atrybut {@link #ATT_UNDELETABLE}.
     */
    public boolean isUndeletable()
    {
        return (this.attributes & ATT_UNDELETABLE) == ATT_UNDELETABLE;
    }

    /**
     * Zwraca folder nadrz�dny. Je�eli u�ytkownik nie ma prawa
     * odczytu folderu nadrz�dnego, rzucany jest wyj�tek AccessDeniedException.
     * Je�eli bie��cy folder jest folderem g��wnym (metoda isRoot()
     * zwraca true), zwracana jest warto�� null.
     */
    public Folder parent(boolean checkAccess) throws EdmException, AccessDeniedException
    {
         if ((parentId != null && parent == null) ||
        		 ( parent!= null && parentId!= null && !parent.getId().equals(parentId)))
         	parent = Finder.find(Folder.class, parentId);
         
		if (checkAccess && parent != null && !parent.canRead())
            throw new AccessDeniedException(sm.getString("folder.parentAccessDenied"));
		
    	return parent;
    }
    
    public Folder parent() throws EdmException, AccessDeniedException
    {
    	return parent(true);
    }    

    Folder getParent() throws EdmException
    {
        return parent(false);
    }

    /**
     * Znajduje w hierarchii folder�w pierwszy z folder�w nadrz�dnych
     * w stosunku do bie��cego, do kt�rego u�ytkownik ma prawo odczytu.
     * Je�eli metoda parent() nie rzuca wyj�tku AccessDeniedException,
     * ta metoda zwraca t� sam� warto��, co parent().
     * Je�eli w hierarchii nie ma takiego folderu, zwracana jest warto�� null.
     */
    public Folder findFirstReadableAncestor() throws EdmException
    {
        try
        {
            return parent();
        }
        catch (AccessDeniedException e)
        {
            Folder ancestor = getParent();
            do
            {
                ancestor = ancestor.getParent();
                if (ancestor != null && ancestor.canRead())
                    return ancestor;
            }
            while (ancestor != null);

            return null;
        }
    }

    private Set<Folder> accessibleChildren;

    /**
     * Zwraca list� podrz�dnych folder�w, dla kt�rych zalogowany
     * u�ytkownik ma prawo Permission.READ. Foldery, kt�rych u�ytkownik
     * nie mo�e odczyta� nie s� umieszczane na li�cie; w zwi�zku z brakiem
     * uprawnie� do takich folder�w podrz�dnych nie s� rzucane wyj�tki.
     */
    public Set<Folder> _getChildFolders() throws EdmException
    {
        if (accessibleChildren != null)
            return accessibleChildren;

        accessibleChildren = new LinkedHashSet<Folder>();

        if (getChildren() != null)
        {
            for (Folder child:getChildren())
            {
                if (child.canRead())
                    accessibleChildren.add(child);
            }
        }
        return accessibleChildren;
    }

    public boolean hasChildFolders() throws EdmException
    {
        return countChildFolders() > 0;
    }

    public int countChildFolders() throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias folderTable = from.createTable(DSApi.getTableName(Folder.class), true);
        TableAlias permTable = null;
        if (!DSApi.context().isAdmin())
            permTable = from.createTable(DSApi.getTableName(FolderPermission.class), true);
        WhereClause where = new WhereClause();

        where.add(Expression.eq(folderTable.attribute("parent_id"), id));

        if (permTable != null)
        {
            where.add(Expression.eqAttribute(folderTable.attribute("id"),
                permTable.attribute("folder_id")));
            where.add(Expression.eq(permTable.attribute("name"), ObjectPermission.READ));
            where.add(Expression.eq(permTable.attribute("negative"), Boolean.FALSE));

            Junction OR = Expression.disjunction().
                add(Expression.eq(permTable.attribute("subjecttype"), ObjectPermission.ANY)).
                add(Expression.conjunction().
                    add(Expression.eq(permTable.attribute("subjecttype"), ObjectPermission.USER)).
                    add(Expression.eq(permTable.attribute("subject"), DSApi.context().getPrincipalName())));

            for (DSDivision division : DSApi.context().getDSUser().getDivisions())
            {
                // tylko grupy mog� mie� uprawnienia
                if (!division.isGroup())
                    continue;
                OR.add(Expression.conjunction().
                    add(Expression.eq(permTable.attribute("subjecttype"), ObjectPermission.GROUP)).
                    add(Expression.eq(permTable.attribute("subject"), division.getGuid())));
            }

            where.add(OR);
        }

        SelectClause selectId = new SelectClause();
        SelectColumn cntCol = selectId.addSql("count(*)");

        int count = 0;

        ResultSet rs = null;
        try
        {
            SelectQuery select = new SelectQuery(selectId, from, where, null);
            rs = select.resultSet();

            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby folder�w potomnych " +
                    "dla folderu "+id);

            count = rs.getInt(cntCol.getPosition());
            return count;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            if (rs != null) try { DSApi.context().closeStatement(rs.getStatement());rs.close(); } catch (Exception e) { }
        }
    }

    /**
     * Pobiera te foldery potomne, do kt�rych bie��cy u�ytkownik
     * ma uprawnienia.
     */
    public List<Folder> getChildFolders() throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias folderTable = from.createTable(DSApi.getTableName(Folder.class));
        TableAlias permTable = null;
        if (!DSApi.context().isAdmin())
            permTable = from.createTable(DSApi.getTableName(FolderPermission.class));
        WhereClause where = new WhereClause();

        where.add(Expression.eq(folderTable.attribute("parent_id"), id));

        if (permTable != null)
        {
            where.add(Expression.eqAttribute(folderTable.attribute("id"),
                permTable.attribute("folder_id")));
            where.add(Expression.eq(permTable.attribute("name"), ObjectPermission.READ));
            where.add(Expression.eq(permTable.attribute("negative"), Boolean.FALSE));

            Junction OR = Expression.disjunction().
                add(Expression.eq(permTable.attribute("subjecttype"), ObjectPermission.ANY)).
                add(Expression.conjunction().
                    add(Expression.eq(permTable.attribute("subjecttype"), ObjectPermission.USER)).
                    add(Expression.eq(permTable.attribute("subject"), DSApi.context().getPrincipalName())));

            for (DSDivision division : DSApi.context().getDSUser().getDivisions())
            {
                // tylko grupy mog� mie� uprawnienia
                if (!division.isGroup())
                    continue;
                OR.add(Expression.conjunction().
                    add(Expression.eq(permTable.attribute("subjecttype"), ObjectPermission.GROUP)).
                    add(Expression.eq(permTable.attribute("subject"), division.getGuid())));
            }

            where.add(OR);
        }

        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(folderTable, "id");
        selectId.add(folderTable, "title");

        OrderClause order = new OrderClause();
        order.add(folderTable.attribute("title"), OrderClause.ASC);

        List<Folder> childFolders = new LinkedList<Folder>();

        ResultSet rs = null;
        try
        {
            SelectQuery select = new SelectQuery(selectId, from, where, order);
            rs = select.resultSet();
            while (rs.next())
            {
                childFolders.add(DSApi.context().
                    load(Folder.class, new Long(rs.getLong(idCol.getPosition()))));
            }
            return childFolders;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            if (rs != null) try { 
            	DSApi.context().closeStatement(rs.getStatement());
            	rs.close();  } catch (Exception e) { }

        }
    }

    public boolean isSystem(String systemName) throws EdmException
    {
/*
        if (systemName == null)
            throw new NullPointerException("systemName");

        boolean result = (id != null &&
            DSApi.context().systemPreferences().node("environment/systemfolders").
                getLong(systemName, -1) == id.longValue());

        if (!result)
            result = systemName.equals(getSystemName());

        return result;
*/
        return systemName.equals(getSystemName());
    }

    public static Folder findByTitle(String title){
        try{
            Criteria c = DSApi.context().session().createCriteria(Folder.class);
            c.add(Restrictions.eq("title", title));
            return (Folder) c.uniqueResult();
        }catch(Exception e){
            log.error("", e);
        }

        return null;
    }

    public static Folder find(Long id)
        throws EdmException, FolderNotFoundException, AccessDeniedException
    {
        Folder folder = (Folder) DSApi.getObject(Folder.class, id);

        if (folder == null)
            throw new FolderNotFoundException(id);

        if (!folder.canRead())
            throw new AccessDeniedException(
                sm.getString("documentHelper.folderAccessDenied", id));

        return folder;
    }

    public static Folder getRootFolder()
        throws EdmException, FolderNotFoundException, AccessDeniedException
    {
        try
        {
            List ids = DSApi.context().classicSession().find("select f.id from f in class "+
                Folder.class.getName()+" where f.parentId is null");
            if (ids == null || ids.size() == 0)
                throw new FolderNotFoundException("Nie znaleziono folderu g��wnego");
            if (ids.size() > 1)
                throw new EdmException("Znaleziono wi�cej ni� jeden folder g��wny");

            return find((Long) ids.get(0));
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca folder podrz�dny w stosunku do bie��cego maj�cy
     * podan� nazw� systemow� (atrybut systemName). Je�eli taki
     * folder nie istnieje, rzucany jest wyj�tek FolderNotFoundException.
     * @param systemName
     */
    Folder getChildBySystemName(String systemName)
        throws FolderNotFoundException, EdmException
    {
        if (getChildren() != null)
        {
            for (Iterator<Folder> iterator = getChildren().iterator(); iterator.hasNext();)
            {
                Folder folder = (Folder) iterator.next();
                DSApi.initializeProxy(folder);
                if (folder.getSystemName() != null &&
                    folder.getSystemName().equals(systemName))
                    return folder;
            }
        }

        throw new FolderNotFoundException(systemName);
    }
    
    public static Folder findSystemFolder(String systemName) throws EdmException
    {

        Folder root = getRootFolder();
        DSApi.initializeProxy(root);
        return root.getChildBySystemName(systemName);
    }

    /**
     * Zwraca sciezke w postaci listy Stringow. Pierwszy element to root a dalej w dol.
     * @return
     * @throws EdmException
     */
    public List<String> getPath() throws EdmException
    {
    	List<String> path = new LinkedList<String>();
        Folder folder = this;
        do
        {
            path.add(folder.getTitle());
            folder = folder.getParent();
        }
        while (folder != null);
        Collections.reverse(path);
        return path;
    }
  
    /**
     * Zwraca �cie�k� nazw folder�w prowadz�c� do bie��cego folderu.
     * Nazwy rozdzielane s� parametrem separator. Separator nie jest
     * umieszczany na pocz�tku �cie�ki.
     * <p>
     * Je�eli w �cie�ce znajduj� si� foldery, do kt�rych u�ytkownik
     * nie ma prawa odczytu, zamiast nazwy tego folderu pojawi si�
     * napis "[...]".
     * <p>
     * Warto�� mo�e by� pobrana jedynie w�wczas, gdy sesja, z kt�r�
     * zwi�zany jest folder, jest otwarta.
     */
    public String getPrettyPath(String separator) throws EdmException
    {
        StringBuilder sb = new StringBuilder();
        List<Folder> path = new LinkedList<Folder>();
        Folder folder = this;
        do
        {
            path.add(folder);
            folder = folder.getParent();
        }
        while (folder != null);

        Collections.reverse(path);

        for (int i=0, n=path.size(); i < n; i++)
        {
            Folder f = (Folder) path.get(i);

            // je�eli u�ytkownik nie ma prawa odczytu do folderu na �cie�ce,
            // zamiast tytu�u tego folderu umieszczany jest ci�g [...]
            if (f != null && f.canRead())
                sb.append(f.getTitle());
            else
                sb.append("[...]");

            if (i < n-1)
                sb.append(separator);
        }

        return sb.toString();
    }

    /**
     * Odpowiada wywo�aniu getPrettyPath(" / ").
     * @see #getPrettyPath(java.lang.String)
     */
    public String getPrettyPath() throws EdmException
    {
        return getPrettyPath(" / ");
    }

    /**
     * Zwraca true, je�eli ten folder jest folderem g��wnym.
     * R�wnowa�ne z testem parent() == null.
     */
    public boolean isRoot()
    {
        return parent == null;
    }

    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    /**
     * Nadaje folderowi tytu�. Je�eli folder zosta� w pe�ni odczytany
     * przez Hibernate, sprawdzane jest, czy w folderze nadrz�dnym dla
     * tego folderu znajduje si� ju� folder o tym tytule. Je�eli tak,
     * rzucany jest wyj�tek DuplicateNameException.
     * @param title
     * @throws EdmException
     * @throws pl.compan.docusafe.core.security.AccessDeniedException
     * @throws DuplicateNameException
     */
    public void setTitle(String title)
        throws EdmException, AccessDeniedException, DuplicateNameException
    {
        if (title == null)
            throw new NullPointerException("title");

        if (!canModify())
            throw new AccessDeniedException(
                Folder.sm.getString("folder.setPropertyDenied", title));

        // folder g��wny mo�e mie� dowoln� nazw�, nie trzeba sprawdza�
        // unikalno�ci w�r�d folder�w na tym samym poziomie
        if (!isRoot())
        {
            Set<Folder> parentsChildren = parent.getChildren();
            if (parentsChildren != null)
            {
                for (Iterator<Folder> iter=parentsChildren.iterator(); iter.hasNext(); )
                {
                    Folder child = (Folder) iter.next();
                    if (!child.equals(this) && title.equals(child.getTitle()))
                        throw new DuplicateNameException(
                            Folder.sm.getString("folder.duplicateTitle"));
                }
            }
        }

        this.title = title;
    }

    public Date getCtime()
    {
        return ctime;
    }

    /**
     * Metoda u�ywana tylko przez Hibernate. Data utworzenia
     * folderu jest ustawiana w onSave().
     */
    private void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    private void setParent(Folder parent)
    {
        this.parent = parent;
    }

    /**
     * Dost�p do tej metody musi by� prywatny, aby tylko Hibernate
     * mog�o bezpo�rednio pobra� t� kolekcj�. API DocuSafe powinno
     * u�ywa� metody {@link #getChildFolders()}.
     * @throws EdmException 
     */
    Set<Folder> getChildren() throws EdmException
    {
        if (children == null && this.getId() != null)
        {
    		Criteria c = DSApi.context().session().createCriteria(Folder.class);
            c.add(Restrictions.eq("parentId", this.getId()));
            children = new HashSet<Folder>(c.list());
        }
        if(children == null)
        	children = new HashSet<Folder>();
        return children;
    }
    
    private void addChildren(Folder child) 
    {
		child.setParentId(this.getId());
		this.children.add(child);
	}

    private Integer getHversion()
    {
        return hversion;
    }

    private void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public String getAuthor()
    {
        return author;
    }

    /**
     * Metoda u�ywana tylko przez Hibernate. Autor folderu jest
     * ustawiany w metodzie onSave().
     */
    private void setAuthor(String author)
    {
        this.author = author;
    }

    /**
     * @deprecated {@link #isSystem(String)}
     */
    String getSystemName()
    {
        return systemName;
    }

    private void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public int getAttributes()
    {
        return attributes;
    }

    void setAttributes(int attributes)
    {
        this.attributes = attributes;
    }

    public String getLparam()
    {
        return lparam;
    }

    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }

    public Long getWparam()
    {
        return wparam;
    }

    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }
    /* Lifecycle */

    public boolean onSave(Session session) throws CallbackException
    {
        if (author == null)
        {
            author = DSApi.context().getPrincipalName();
        }

        if (ctime == null)
        {
            ctime = new Date();
        }

        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Folder)) return false;

        final Folder folder = (Folder) o;

        if (id != null ? !id.equals(folder.getId()) : folder.getId() != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public String toString()
    {
        return getClass().getName()+"[id="+id+" title="+title+
            (systemName != null ? " systemName="+systemName : "") + "]";
    }

	public Boolean getAnchor() {
		if (anchor == null || anchor == 0) {
			return false;
		} else {
			return true;
		}
	}

	public void setAnchor(Boolean anchor) {
		this.anchor = anchor ? 1 : 0;
	}
	
	public static Folder findDestAnchor(Folder folder) throws FolderNotFoundException, AccessDeniedException, EdmException {
		if(folder.getAnchor()) {
			return folder;
		}
		if (folder.isRoot()) {
			return folder;
		}
		return findDestAnchor(folder.getParent());
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

            /**
     * Tworzy katalog z pierwszej litery
     * @param title
     * @return
     * @throws EdmException 
     */
    public Folder createSubfolderByFirstLetter(String title) throws EdmException
    {
        Character c = StringUtils.lowerCase(title).charAt(0);
        if (c.compareTo(new Character('c')) < 0)
                return createSubfolderIfNotPresent("A-B");
        else if (c.compareTo(new Character('e')) < 0 || c.equals('�'))
                return createSubfolderIfNotPresent("C-D");
        else if (c.compareTo(new Character('g')) < 0)
                return createSubfolderIfNotPresent("E-F");
        else if (c.compareTo(new Character('i')) < 0)
                return createSubfolderIfNotPresent("G-H");
        else if (c.compareTo(new Character('k')) < 0)
                return createSubfolderIfNotPresent("I-J");
        else if (c.compareTo(new Character('m')) < 0 || c.equals('�'))
                return createSubfolderIfNotPresent("K-�");
        else if (c.compareTo(new Character('o')) < 0)
                return createSubfolderIfNotPresent("M-N");
        else if (c.compareTo(new Character('r')) < 0)
                return createSubfolderIfNotPresent("O-P");
        else if (c.compareTo(new Character('t')) < 0 || c.equals('�'))
                return createSubfolderIfNotPresent("R-S");
        else if (c.compareTo(new Character('w')) < 0)
                return createSubfolderIfNotPresent("T-V");
        else
                return createSubfolderIfNotPresent("W-�");
    }
}
