package pl.compan.docusafe.core.base;

public class DocumentChangelogView {

    public final String what;
    public final String description;
    public final String name;

    public DocumentChangelogView(DocumentChangelog changelog) {
        this.what = changelog.getWhat();
        this.description = changelog.getDescription();
        this.name = changelog.toString();
    }

}
