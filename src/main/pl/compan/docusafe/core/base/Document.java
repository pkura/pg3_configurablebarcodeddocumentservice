package pl.compan.docusafe.core.base;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.boot.WatchDog;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.lucene.IndexStatus;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

/**
 * Dokument. "Abstrakcyjny" obiekt reprezentuj�cy zbi�r binarnych
 * za��cznik�w oraz posiadaj�cy wsp�lne dla nich wszystkich
 * atrybuty.
 * <p>
 * Klonowanie:
 * utworzenie nowego obiektu: super.clone();
 * nowy obiekt powinien sklonowa� swoje kolekcje
 * final Document.clone()
 *  watches.clone()
 *  super.cloneMutables()
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Document.java,v 1.212 2010/08/20 13:21:47 pecet5 Exp $
 */
public class Document implements Cloneable, Lifecycle
{
    private static final Logger log = LoggerFactory.getLogger(Document.class);

    static final StringManager sm = GlobalPreferences.loadPropertiesFile(Document.class.getPackage().getName(),null);


    static PermissionManager permissionManager = PermissionManager.getInstance();
    /**
     * Bit ustawiany w Document.wparam - okre�la, �e dokument nie wymaga
     * umieszczenia w pudle archiwalnym.  Flaga tylko dla Nationwide.
     */
    public static final long WPARAMBIT_NW_NEEDS_NOT_BOX = 1;

    public static final String TYPE = "plain";

    /**
     * Zmienna mowi czy uprawnienia sa kontrolowane czy nie
     * Jesli w jakims momencie musimy je wylaczyc i zrobic cos co wymusza uprawnienia to robimy to
     */
    protected boolean permissionsOn = true;


	private Long id;
    protected String title;
    protected String description;
    protected String abstractDescription;
    protected String documentAutorDivision;
    protected Boolean inPackage = false;
    protected String documentBarcode;
    
	
	


	private Date ctime;
    private Date mtime;
    private String author;
    private String externalId;
    private List<Attachment> attachments;
    private Integer hversion;
    /**
     * Identyfikator folderu
     */
    private Long folderId;
    /**
     * Z bazy wyciagamy tylko folderId, folder dociagany na zyczenie
     */
    private Folder folder = null;
    private Doctype doctype;
    private DocumentKind documentKind;
    private boolean deleted;
    private String lastRemark;
    private List<Audit> workHistory = null;
    /**
     * Pole okre�laj�ce, �e dokument jest instancj� OfficeDocument.
     */
    private boolean office;

    private boolean forceArchivePermissions;

    //private PermissionsHolder permissionsHolder;

    private Set<DocumentWatch> watches = null;

    //odpowiedzi na dokument wewnetrzny - nie sa teraz mapowane przez hibernate
    private Set<Document> replies;
    private boolean repliesModified = true;

    private Long repliedDocumentId;

    private Box box;

    private Date boxingDate;

    private String source;

    private String lparam;
    private Long wparam;

    private DocumentFlags flags;
    private Boolean blocked;

    /** Wskazuje na dokument kt�rego jest duplikatem */
    private Long duplicateDoc;

    /** Name uzytkownika ktory zakonczyl prace z dokumentem*/
    private String finishUser;
    /** Data zako�czenia pracy z dokumentem*/
	private Date finishDate;

    /** Stan indeksu atrybut�w dokumentu */
    private int indexStatusId = IndexStatus.NOT_INDEXED.id;

    private long versionGuid;
    private int version;
    private String versionDesc;
    private Boolean czyCzystopis;
    private Boolean czyAktualny;
    private Boolean isArchived;
    private Date archivedDate;
    private Long accessToDocument;
    private String uuid;

    public DocumentType getType()
    {
        return DocumentType.PLAIN;
    }

    public String getStringType()
    {
    	return "plain";
    }

    /**
     * Konstruktor u�ywany przez Hibernate.
     */
    Document()
    {
    }

    /**
     * Konstruktor u�ywany przez API.
     * @param title Tytu� dokumentu.
     * @param description Opis dokumentu.
     */
    public Document(String title, String description)
    {
    	ctime = new java.util.Date();
        this.title = title;
        this.description = description;
    }

    public static Document clone(Document document) throws EdmException
    {
        // najpierw stworzenie nowego obiektu document
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda tworzy sklonowany obiekt (przy pomocy super.clone()),
     * a nast�pnie wywo�uje jego metod� duplicate() i zwraca obiekt.
     * @return
     * @throws EdmException
     */
    public final Document cloneObject(Long[] attachmentIds) throws EdmException
    {
        try
        {
            Document clone = (Document) super.clone();
            clone.duplicate(attachmentIds);
            return clone;
        }
        catch (CloneNotSupportedException e)
        {
            throw new Error(e);
        }
    }
    /**
     * Metoda tworzy sklonowany obiekt (przy pomocy super.clone()),
     * a nast�pnie wywo�uje jego metod� duplicate() i zwraca obiekt.
     * @return
     * @throws EdmException
     */
    public final Document cloneObject() throws EdmException
    {
        try
        {
            Document clone = (Document) super.clone();
         
            return clone;
        }
        catch (CloneNotSupportedException e)
        {
            throw new Error(e);
        }
    }
    /**
     * Metoda wywo�ywana na sklonowanym obiekcie zarz�dzanym przez Hibernate.
     * W tej metodzie nale�y sklonowa� wszelkie obiekty zwi�zane z bie��cym,
     * kt�re nie powinny by� dzielone przez instancje.
     * <p>
     * Je�eli w obiekcie dziedzicz�cym po Document znajdzie si� nowa wersje
     * tej metody, jej pierwsz� czynno�ci� powinno by� wywo�anie zas�oni�tej
     * metody z klasy nadrz�dnej.
     */
    protected void duplicate(Long[] attachmentIds) throws EdmException
    {
//        id = null;
//        hversion = null;
        this.author = DSApi.context().getDSUser().getName();
        List savedAttachments = this.getAttachments();
        this.attachments = null;
        Set<DocumentWatch> savedWatches = getWatches();
        this.watches = null;
        // folder pozostaje
        // extInf pozostaje
        this.id = null;
        this.replies = null;//TODO zastanowic sie czy kopiowac odpowiedzi na pismo

        try
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }

        changelog(DocumentChangelog.CLONED_DOCUMENT);

        if (savedAttachments != null)
        {
            List ids = attachmentIds != null ? Arrays.asList(attachmentIds) : null;
            this.attachments = new ArrayList<Attachment>(savedAttachments.size());
            for (Iterator iter=savedAttachments.iterator(); iter.hasNext(); )
            {
                Attachment attachment = (Attachment) iter.next();
                if (ids == null || ids.contains(attachment.getId()))
                {
                    attachment = attachment.cloneObject();
                    attachment.setDocument(this);
                    addAttachment(attachment);
                }
            }
        }

        if (savedWatches != null)
        {
            //this.watches = new HashSet<DocumentWatch>(savedWatches.size());
            for (DocumentWatch w : savedWatches)
            {
                DocumentWatch watch = w.cloneObject();
                watch.setDocumentId(id);
                this.addWatch(watch);
            }
        }
    }

    public boolean hasWparamBits(long bits)
    {
        return wparam != null && (wparam.longValue() & bits) == bits;
    }

    /**
     * Ustawia ��dane bity w wparam w dodatku do istniej�cych bit�w.
     * @return true, je�eli przynajmniej jeden z bit�w by� ju� ustawiony.
     */
    public boolean addWparamBits(long bits)
    {
        if (wparam == null)
        {
            wparam = new Long(bits);
            return false;
        }
        else
        {
            boolean result = (wparam.longValue() & bits) > 0;
            wparam = new Long(wparam.longValue() | bits);
            return result;
        }
    }

    public void addHistoryEntry(String property, String message) {
        if (this instanceof OfficeDocument)
        {
            ((OfficeDocument) this).addWorkHistoryEntry(
                    Audit.create(property,
                            DSApi.context().getPrincipalName(),
                            message,
                            null, null));
        }
    }

    public boolean clearWparamBits(long bits)
    {
        if (wparam == null)
        {
            return false;
        }
        else
        {
            boolean result = (wparam.longValue() & bits) > 0;
            wparam = new Long(wparam.longValue() & ~bits);
            return result;
        }
    }

    /**
     * Zapisuje wpis w changelogu dokumentu
     * @param id
     * @param username
     * @param what
     * @throws EdmException
     */


    @Deprecated
    public static void changelog(Long id, String username, DocumentChangelog what)
        throws EdmException
    {

        pl.compan.docusafe.core.datamart.DataMartManager.storeDocumentChangelog(id, username, what);
    }

    public static List<Long> getChangelogEntries(Long documentId, DocumentChangelog changelog)
        throws EdmException
    {
        return DataMartManager.getChangelogEntries(documentId, changelog);
    }

    /**
     * Zapisuje komunikat w dzienniku zmian dokumentu.  Je�eli dokument
     * zosta� w�a�nie utworzony, nale�y wykona� polecenie Session.flush().
     * @param what
     * @throws EdmException
     */
    @Deprecated
    public void changelog(DocumentChangelog what) throws EdmException
    {
    	DataMartManager.storeDocumentChangelog(id, DSApi.context().getPrincipalName(), what);
    }

    public void create() throws EdmException
    {
        if (getFolder(this) == null)
            throw new EdmException("Tworzony dokument nie znajduje si� w folderze.");

        try
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
            changelog(DocumentChangelog.CREATE_DOCUMENT);
            DataMartEvent dme = new DataMartEvent(true, this.id, null,DataMartDefs.DOCUMENT_CREATE, null,null, null, null);
            //DataMartManager.storeEvent(dme);
            if (!(this instanceof OfficeDocument) )
            {
                DSApi.context().grant(this,
                    new String[] {
                        ObjectPermission.READ,
                        ObjectPermission.MODIFY,
                        ObjectPermission.DELETE,
                        ObjectPermission.READ_ATTACHMENTS,
                        ObjectPermission.MODIFY_ATTACHMENTS
                    });
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /***
     * Zwraca dokument o podanym id. Sprawdza czy u�ytkownik mo�e czyta� dany dokument
     * @param id
     * @return
     * @throws EdmException
     * @throws DocumentNotFoundException
     * @throws DocumentLockedException
     * @throws AccessDeniedException
     */
    public static Document find(Long id)
	    throws EdmException, DocumentNotFoundException, DocumentLockedException,
	    AccessDeniedException
    {
    	return find(id, true,null);
    }

    /***
     * Zwraca dokument o podanym id. Sprawdza czy u�ytkownik mo�e czyta� dany dokument.
     * Sprawdza czy dokument znajduje si� w teczce nale��cej do u�ytkownika.
     * @param id
     * @return
     * @throws EdmException
     * @throws DocumentNotFoundException
     * @throws DocumentLockedException
     * @throws AccessDeniedException
     */
    public static Document find(Long id,Long binderId)
	    throws EdmException, DocumentNotFoundException, DocumentLockedException,
	    AccessDeniedException
    {
    	return find(id, true,binderId);
    }

    /***
     * Zwraca dokument o podanym id. Sprawdza czy u�ytkownik mo�e czyta� dany dokument.
     * Sprawdza czy dokument znajduje si� w teczce nale��cej do u�ytkownika.
     * @param id
     * @return
     * @throws EdmException
     * @throws DocumentNotFoundException
     * @throws DocumentLockedException
     * @throws AccessDeniedException
     */
    public static Document find(Long id,Boolean checkPermission)
	    throws EdmException, DocumentNotFoundException, DocumentLockedException,
	    AccessDeniedException
    {
    	return find(id, checkPermission,null);
    }

    /***
     * Zwraca dokument o podanym id, je�li checkPermission = true
     * sprawdza czy zalogowany u�ytkownik posiada uprawnienia na jego odczytu
     * @param id
     * @param checkPermission
     * @return
     * @throws EdmException
     * @throws DocumentNotFoundException
     * @throws DocumentLockedException
     * @throws AccessDeniedException
     */
    public static Document find(Long id,Boolean checkPermission,Long binderId)
        throws EdmException, DocumentNotFoundException, DocumentLockedException,
        AccessDeniedException
    {
    	log.trace("LADUJE DOKUMENT "+id);
        Document document = DSApi.getObject(Document.class, id);
        log.trace("ZALADOWAL DOKUMENT "+id);

        checkDocument(document, id, checkPermission, binderId);

        return document;
    }

    public static void checkDocument(Document document, Long documentId, boolean checkPermission, Long binderId) throws EdmException, DocumentNotFoundException, AccessDeniedException, DocumentLockedException {
        if (document == null) {
            throw new DocumentNotFoundException(documentId);
        }

        if (!(document instanceof OfficeDocument)) {
            checkLocked(documentId);
        }

        // dla dokument�w kancelaryjnych uprawnienia nie s� sprawdzane normalnie
        if (checkPermission && !document.canRead(binderId)) {
            throw new AccessDeniedException(sm.getString("documentHelper.documentAccessDenied", documentId));
        }

        // dokumenty oznaczone jako usuni�te mo�e przegl�da� tylko admin
        if (!DSApi.context().isAdmin() && document.isDeleted()) {
            throw new DocumentNotFoundException(documentId);
        }

        AccessLog.logAccess(document);
    }

    public static void checkDocument(Document document, Long documentId, boolean checkPermission) throws EdmException {
        checkDocument(document, documentId, checkPermission, null);
    }

    public static void checkDocument(Document document, Long documentId) throws EdmException {
        checkDocument(document, documentId, true, null);
    }

     public static List<Document> find(Collection<Long> ids) throws EdmException, DocumentNotFoundException, DocumentLockedException,
    	AccessDeniedException {

    	Criteria criteria = DSApi.context().session().createCriteria(Document.class);
        criteria.add(Restrictions.in("id", ids));
//        criteria.add(Restrictions.eq("nieaktywny",new Integer(0)));
        
        List<Document> docs = criteria.list();

	    if (docs == null)
	        throw new EdmException("Documents not found");

	    for (Document document : docs) {

	    	if (!(document instanceof OfficeDocument))
	    		checkLocked(document.getId());

		    // dla dokument�w kancelaryjnych uprawnienia nie s� sprawdzane normalnie
		    if (!(document instanceof OfficeDocument) && !document.canRead())
		        throw new AccessDeniedException(
		            sm.getString("documentHelper.documentAccessDenied", document.getId()));

		    // dokumenty oznaczone jako usuni�te mo�e przegl�da� tylko admin
		    if (!DSApi.context().isAdmin() && document.isDeleted())
		        throw new EdmException("Document deleted");

		    AccessLog.logAccess(document);
	    }
	    return docs;
     }

    public static List<Document> findSafe(Collection<Long> ids) throws EdmException, DocumentNotFoundException, DocumentLockedException, AccessDeniedException {
        Criteria criteria = DSApi.context().session().createCriteria(Document.class);
        criteria.add(Restrictions.in("id", ids));

        List<Document> docs = criteria.list();

        List<Document> ret = new ArrayList<Document>();

        if (docs == null) return ret;

        for (Document document: docs) {

            if (!(document instanceof OfficeDocument)) {
                try {
                    checkLocked(document.getId());
                } catch(DocumentLockedException ex) {
                    log.warn("[findSafe] document locked for document.id = {}, username = {}, expiration = {}", document.getId(), ex.getUsername(), ex.getExpiration());
                    continue;
                }
            }

            // dla dokument�w kancelaryjnych uprawnienia nie s� sprawdzane normalnie
            if (!(document instanceof OfficeDocument) && !document.canRead()) {
                log.warn("[findSafe] access denied for document.id = {}", document.getId());
                continue;
            }

            // dokumenty oznaczone jako usuni�te mo�e przegl�da� tylko admin
            if (!DSApi.context().isAdmin() && document.isDeleted()) {
                log.warn("[findSafe] document deleted for document.id = {}", document.getId());
                continue;
            }

            ret.add(document);

            AccessLog.logAccess(document);
        }
        return ret;
    }

    public static Iterator findByIdRange(Long startId, Long endId) throws EdmException
    {
        try
        {
            return DSApi.context().classicSession().iterate(
                "from d in class "+Document.class+" where id >= ? and id <= ?",
                new Object[] { startId, endId },
                new Type[] { Hibernate.LONG, Hibernate.LONG });
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List<Long> findByDocumentKind(Long docKindId) throws HibernateException, SQLException
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	List<Long> list = new ArrayList<Long>();
        try
        {
        	ps = DSApi.context().prepareStatement("select id from ds_document where dockind_id = ? order by MTIME");
        	ps.setLong(1, docKindId);
        	rs = ps.executeQuery();
        	while(rs.next())
        	{
        		list.add(rs.getLong("id"));
        	}
        }
		catch (EdmException e)
		{
			log.error("",e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
        return list;
    }
    
    public static List<Long> findByDocumentKind(String docKindCn)
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	List<Long> list = new ArrayList<Long>();
    	try
    	{
    		ps = DSApi.context().prepareStatement("select * from ds_document doc full outer join ds_document_kind kind on kind.id = doc.dockind_id where kind.cn = ? order by MTIME");
    		ps.setString(1, docKindCn);
    		rs = ps.executeQuery();
    		while(rs.next())
    		{
    			list.add(rs.getLong("id"));
    		}
    	}
    	catch (EdmException e)
    	{
    		log.error("",e);
    	} catch (SQLException e) {
    		log.error("",e);
		}
    	finally
    	{
    		DSApi.context().closeStatement(ps);
    	}
    	return list;
    }

    public static ByDoctypeQuery byDoctypeQuery(int offset, int limit) throws EdmException
    {
        return new ByDoctypeQuery(offset, limit);
    }

    public static List<Long> findByDocumentKindAndRange(Long docKindId, Long fromId, Long toId) throws HibernateException, SQLException
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	List<Long> list = new ArrayList<Long>();
        try
        {
        	ps = DSApi.context().prepareStatement("select id from ds_document where dockind_id = ? and id > ? and id < ?");
        	ps.setLong(1, docKindId);
        	ps.setLong(2, fromId);
        	ps.setLong(3, toId);
        	rs = ps.executeQuery();
        	while(rs.next())
        	{
        		list.add(rs.getLong("id"));
        	}
        }
		catch (EdmException e)
		{
			log.error("",e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
        return list;
    }

    public static List<Document> findByTitle(String title ) throws EdmException{
        return DSApi.context().session().createCriteria(Document.class)
                .add(Restrictions.eq("title", title)).list();
    }

    public static List<Long> findByFolder(Long folderId) throws HibernateException, SQLException, EdmException
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	List<Long> list = new ArrayList<Long>();
        try
        {
        	ps = DSApi.context().prepareStatement("select id from ds_document where folder_id = ?");
        	ps.setLong(1, folderId);
        	rs = ps.executeQuery();
        	while(rs.next())
        	{
        		list.add(rs.getLong("id"));
        	}
        }
		catch (EdmException e)
		{
			log.error("",e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
        return list;
    }

    /**
     * Poszukuje dokument na bazie obiektu doctype
     * @param query
     * @return
     * @throws EdmException
     * @deprecated - powinno sie korzystac z mechanizmu dockind
     */
    @Deprecated
    public static SearchResults searchByDoctype(final DoctypeQuery query) throws EdmException
    {
        final Doctype doctype = Doctype.find(query.doctypeId);

        FromClause from = new FromClause();
        final TableAlias doctypeTable = from.createTable(doctype.getTablename(), true);

        final TableAlias docTable = from.createTable("ds_document", true);
        final TableAlias agentTable = (query.isAgent)?from.createTable("daa_agent", true) : null;
        final TableAlias agencjaTable = (query.isAgencja)?from.createTable("daa_agencja", true) : null;


        TableAlias perm = null;
        if (!DSApi.context().isAdmin() && query.checkPermissions)
            perm = from.createTable(DSApi.getTableName(DocumentPermission.class), true);
        TableAlias changelogTable = null;
        class tablecontainer { TableAlias t; }

        final WhereClause where = new WhereClause();

        // sprawdzanie, czy potrzebna jest tabela changelog
        class flag { boolean b; };
        final flag fl = new flag();
        // wtedy, gdy poszukiwane s� dokumenty, na kt�rych dzia�a� u�ytkownik;
        // (ale takze gdy jest samo zapytanie na accessedAs)
        query.visitQuery(new FormQuery.QueryVisitorAdapter() {
            public void visitEq(FormQuery.Eq expr) throws EdmException
            {
                if (expr.getAttribute().equals("accessedBy") || (expr.getAttribute().equals("accessedAs")))
                    fl.b = true;
            }
        });

        if (fl.b)
        {
            changelogTable = from.createTable("ds_document_changelog", true);
            where.add(Expression.eqAttribute(docTable.attribute("id"),
                changelogTable.attribute("document_id")));
        }
        // kombinacje z klasami finalnymi, poniewa� dost�p do obiektu
        // tabeli realizowany jest z klasy zagnie�d�onej
        final tablecontainer changelogcontainer = new tablecontainer();
        changelogcontainer.t = changelogTable;

        where.add(Expression.eqAttribute(docTable.attribute("id"),
            doctypeTable.attribute("document_id")));
        if(query.isAgent)
            where.add(Expression.eqAttribute(agentTable.attribute("id"), doctypeTable.attribute("field_4")));
        if(query.isAgencja)
            where.add(Expression.eqAttribute(agencjaTable.attribute("id"), doctypeTable.attribute("field_5")));

        // tworzenie fragmentu zapytania WHERE na podstawie warto�ci z formularza
        // wyszukiwania wype�nionego przez u�ytkownika
        query.visitQuery(new FormQuery.QueryVisitorAdapter() {
            private Junction OR;

            public void visitEq(FormQuery.Eq expr) throws EdmException
            {
                if (expr.getAttribute().equals("boxId"))
                {
                    where.add(Expression.eq(docTable.attribute("box_id"), expr.getValue()));
                }
                else if (expr.getAttribute().equals("accessedBy"))
                {
                    where.add(Expression.eq(changelogcontainer.t.attribute("username"), expr.getValue()));
                }
                else if (expr.getAttribute().equals("accessedAs"))
                {
                    if (changelogcontainer.t != null)
                    {
                        if (OR != null)
                        {
                            OR.add(Expression.eq(changelogcontainer.t.attribute("what"), expr.getValue()));
                        }
                        else
                        {
                            where.add(Expression.eq(changelogcontainer.t.attribute("what"), expr.getValue()));
                        }
                    }
                } else if(expr.getAttribute().equals("agencja_nip")) {
                    where.add(Expression.eq(agencjaTable.attribute("nip"), expr.getValue()));
                } else if(expr.getAttribute().equals("agencja_id")) {
                    where.add(Expression.eq(agentTable.attribute("fk_daa_agencja"), expr.getValue()));
                }
                else
                {
                    Integer fieldId = new Integer(expr.getAttribute().substring("field_".length()));
                    Doctype.Field f = doctype.getFieldById(fieldId);
                    Expression e = null;

                    // napisy szukane s� operatorem LIKE, je�eli s� kr�tsze od
                    // maksymalnej d�ugo�ci napisu dla tego pola
                    // oraz "nie wymuszono" aby to pole bylo sprawdzane operatorem Eq
                    if (f.getType().equals(Doctype.Field.STRING) && (!query.forceSearchEq.contains(f.getCn())))
                    {
                        String svalue = (String) f.coerce(expr.getValue());
                        if (svalue != null)
                        {
                            // TODO: sprawy bez %
                            if (svalue.length() >= f.getLength().intValue())
                                e = Expression.eq(doctypeTable.attribute(f.getColumn()), svalue.substring(0, svalue.length()));
                            else if (Doctype.Field.MATCH_EXACT.equals(f.getMatch()))
                                e = Expression.eq(doctypeTable.attribute(f.getColumn()), svalue);
                            else
                                e = Expression.like(doctypeTable.attribute(f.getColumn()), svalue + "%");
                        }
                    }
                    else
                    {
                        e = Expression.eq(doctypeTable.attribute(f.getColumn()), f.coerce(expr.getValue()));
                    }

                    if (e != null)
                    {
                        if (OR != null) OR.add(e);
                        else where.add(e);
                    }
                }
            }

            public void visitGe(FormQuery.Ge expr) throws EdmException
            {
                if (expr.getAttribute().equals("accessedFrom"))
                {
                    where.add(Expression.ge(changelogcontainer.t.attribute("ctime"), expr.getValue()));
                }
                else if (expr.getAttribute().equals("mtimeFrom"))
                {
                    where.add(Expression.ge(docTable.attribute("mtime"), expr.getValue()));
                }
                else
                {
                    Integer fieldId = new Integer(expr.getAttribute().substring("field_".length()));
                    Doctype.Field f = doctype.getFieldById(fieldId);
                    where.add(Expression.ge(doctypeTable.attribute(f.getColumn()), f.coerce(expr.getValue())));
                }
            }

            public void visitLe(FormQuery.Le expr) throws EdmException
            {
                if (expr.getAttribute().equals("accessedTo"))
                {
                    where.add(Expression.le(changelogcontainer.t.attribute("ctime"), expr.getValue()));
                }
                else if (expr.getAttribute().equals("mtimeTo"))
                {
                    where.add(Expression.le(docTable.attribute("mtime"), expr.getValue()));
                }
                else
                {
                    Integer fieldId = new Integer(expr.getAttribute().substring("field_".length()));
                    Doctype.Field f = doctype.getFieldById(fieldId);
                    where.add(Expression.le(doctypeTable.attribute(f.getColumn()), f.coerce(expr.getValue())));
                }
            }

            public void visitLike(FormQuery.Like expr) throws EdmException {
                if(expr.getAttribute().equals("agent_imie")) {
                    where.add(Expression.like(agentTable.attribute("imie"), "%"+expr.getValue()+"%"));
                } else if(expr.getAttribute().equals("agent_nazwisko")) {
                    where.add(Expression.like(agentTable.attribute("nazwisko"), "%"+expr.getValue()+"%"));
                } else if(expr.getAttribute().equals("agencja_nazwa")) {
                    where.add(Expression.like(agencjaTable.attribute("nazwa"), "%"+expr.getValue()+"%"));
                } else if(expr.getAttribute().equals("agencja_numer")) {
                    where.add(Expression.like(agencjaTable.attribute("numer"), "%"+expr.getValue()+"%"));
                } else if(expr.getAttribute().equals("agent_numer")) {
                    where.add(Expression.like(agentTable.attribute("numer"), "%"+expr.getValue()+"%"));
                }
            }

            public void startDisjunction() throws EdmException
            {
                if (OR != null) throw new EdmException("Zagnie�d�one OR jest niedozwolone");
                OR = Expression.disjunction();
            }

            public void endDisjunction() throws EdmException
            {
                where.add(OR);
                OR = null;
            }
        });

        if (!DSApi.context().isAdmin())
        {
            where.add(Expression.eq(docTable.attribute("deleted"), Boolean.FALSE));
        }

        if (!DSApi.context().isAdmin() && query.checkPermissions)
        {
            where.add(Expression.eqAttribute(docTable.attribute("id"), perm.attribute("document_id")));
            where.add(Expression.eq(perm.attribute("name"), ObjectPermission.READ));
            where.add(Expression.eq(perm.attribute("negative"), Boolean.FALSE));
            // and ( perm.subjecttpe = Permission.SUBJECT_ANY

            Junction OR = Expression.disjunction().
                add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.ANY)).
                add(Expression.conjunction().
                    add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.USER)).
                    add(Expression.eq(perm.attribute("subject"), DSApi.context().getPrincipalName())));

            DSDivision[] divisions = DSApi.context().getDSUser().getDivisions();

            for (DSDivision division : divisions)
            {
                // tylko grupy mog� mie� uprawnienia
                if (!division.isGroup())
                    continue;
                OR.add(Expression.conjunction().
                    add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.GROUP)).
                    add(Expression.eq(perm.attribute("subject"), division.getGuid())));
            }

            where.add(OR);
        }

        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(doctypeTable, "document_id");

        OrderClause order = new OrderClause();

        FormQuery.OrderBy[] orderBy = query.getOrder();
        for (int i=0; i < orderBy.length; i++)
        {
            if ("id".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("id"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(docTable, "id");
            }
            else if ("title".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("title"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(docTable, "title");
            }
            else if ("ctime".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("ctime"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(docTable, "ctime");
            }
            else if ("mtime".equals(orderBy[i].getAttribute()))
            {
                order.add(docTable.attribute("mtime"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(docTable, "mtime");
            }
            else if (orderBy[i].getAttribute().startsWith("doctype."))
            {
                try
                {
                    Integer fieldId = new Integer(orderBy[i].getAttribute().substring("doctype.".length()));
                    String column = doctype.getFieldById(fieldId).getColumn();
                    if ("AGENT".equals(doctype.getFieldById(fieldId).getCn()))
                    {
                        order.add(agentTable.attribute("nazwisko"), Boolean.valueOf(!orderBy[i].isAscending()));
                        selectId.add(agentTable, "nazwisko");
                    }
                    else if ("AGENCJA".equals(doctype.getFieldById(fieldId).getCn()))
                    {
                        order.add(agencjaTable.attribute("nazwa"), Boolean.valueOf(!orderBy[i].isAscending()));
                        selectId.add(agencjaTable, "nazwa");
                    }
                    else
                    {
                        order.add(doctypeTable.attribute(column), Boolean.valueOf(orderBy[i].isAscending()));
                        selectId.add(doctypeTable, column);
                    }
                }
                catch (Exception e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }

/*
        for (Iterator iter=query.sortFields().iterator(); iter.hasNext(); )
        {
            AbstractQuery.SortField field = (AbstractQuery.SortField) iter.next();
            // je�eli dodana b�dzie mo�liwo�� sortowania po innych polach,
            // nale�y uaktualni� metod� Query.validSortField()
            if (field.getName().equals("id"))
            {
                order.add(docTable.attribute("id"), field.getDirection());
                selectId.add(docTable, "id");
            }
            else if (field.getName().equals("title"))
            {
                order.add(docTable.attribute("title"), field.getDirection());
                selectId.add(docTable, "title");
            }
            // TODO: pola z Doctype
        }
*/

        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(distinct "+docTable.getAlias()+".id)");

        SelectQuery selectQuery;

        int totalCount;
        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);

            ResultSet rs = selectQuery.resultSet(true);
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby dokument�w.");

            totalCount = rs.getInt(countCol.getPosition());

            rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            selectQuery = new SelectQuery(selectId, from, where, order);
            if (query.getLimit() > 0) {
                selectQuery.setMaxResults(query.getLimit());
                selectQuery.setFirstResult(query.getOffset());
            }

            rs = selectQuery.resultSet();

            List<Document> results = new ArrayList<Document>(query.getLimit());

            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(DSApi.context().load(Document.class, id));
            }

            rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            return new SearchResultsAdapter<Document>(results, query.getOffset(), totalCount,
                Document.class);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

    public void eventDocumentView() throws EdmException {
        if (AvailabilityManager.isAvailable("dataMart.event.documentView")) {
            boolean openedTransaction = false;
            if (!DSApi.context().inTransaction()) {
                DSApi.context().begin();
                openedTransaction = true;
            }
            DataMartEventProcessors.DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.USER_DOCUMENT_VIEW).documentId(getId());
            if (openedTransaction) {
                DSApi.context().commit();
            }
        }
    }

    public static class ByDoctypeQuery extends AbstractQuery
    {
        private int offset;
        private int limit;
        private Long doctypeId;
        /**
         * fieldId (Integer) -> value (String)
         */
        private Map<Integer, String> values = new HashMap<Integer, String>();

        public ByDoctypeQuery(int offset, int limit)
        {
            this.offset = offset;
            this.limit = limit;
        }

        protected boolean validSortField(String field)
        {
            return false;
        }

        public void setDoctypeId(Long doctypeId)
        {
            this.doctypeId = doctypeId;
        }

        public void addFieldQuery(Integer fieldId, String value)
        {
            values.put(fieldId, value);
        }
    }

    public static class DoctypeQuery extends FormQuery
    {
        private Long doctypeId;
        public boolean isAgent = false;
        public boolean isAgencja = false;
        public boolean checkPermissions = true;
        public Set<String> forceSearchEq = new HashSet<String>();

        private DoctypeQuery()
        {
        }

        public DoctypeQuery(int offset, int limit)
        {
            super(offset, limit);
        }

        public Long getDoctypeId()
        {
            return doctypeId;
        }

        public void doctype(Long id)
        {
            // to pole nie wchodzi do zapytania, bo jest odczytywane wcze�niej
            // przez metod� szukaj�c� dokument�w
            if (id == null)
                throw new NullPointerException("id");
            this.doctypeId = id;
        }

        public void field(Integer fieldId, Object value)
        {
            if (fieldId == null)
                throw new NullPointerException("fieldId");
            if (value == null)
                throw new NullPointerException("value");


            addEq("field_"+fieldId, value);
        }

        public void enumField(Integer fieldId, Integer[] values)
        {
            if (fieldId == null)
                throw new NullPointerException("fieldId");
            if (values == null)
                throw new NullPointerException("values");
            if (values.length == 0)
                return;

            DoctypeQuery or = new DoctypeQuery();
            for (int i=0; i < values.length; i++)
            {
                if (values[i] != null)
                    or.field(fieldId, values[i]);
            }

            addDisjunction(or);
        }

        public void fieldFrom(Integer fieldId, Object value)
        {
            if (fieldId == null)
                throw new NullPointerException("fieldId");
            if (value == null)
                throw new NullPointerException("value");
            addGe("field_"+fieldId, value);
        }

        public void fieldTo(Integer fieldId, Object value)
        {
            if (fieldId == null)
                throw new NullPointerException("fieldId");
            if (value == null)
                throw new NullPointerException("value");
            addLe("field_"+fieldId, value);
        }

        public void boxId(Long value)
        {
            if (value == null)
                throw new NullPointerException("value");
            addEq("boxId", value);
        }

        public void mtime(Date mtimeFrom, Date mtimeTo)
        {
            if (mtimeFrom != null)
                addGe("mtimeFrom", mtimeFrom);
            if (mtimeTo != null)
                addLe("mtimeTo", mtimeTo);
        }

        public void accessedBy(String username)
        {
            if (username == null)
                throw new NullPointerException("username");
            addEq("accessedBy", username);
        }

        private void accessedAs(String action)
        {
            addEq("accessedAs", action);
        }

        public void enumAccessedAs(String[] actions)
        {
            if (actions == null)
                throw new NullPointerException("actions");

            if (actions.length == 0)
                return;

            // TODO: dla AND dodawa� warunki do g��wnego obiektu

            int added = 0;
            DoctypeQuery or = new DoctypeQuery();
            for (int i=0; i < actions.length; i++)
            {
                if (actions[i] != null && actions[i].length() > 0)
                {
                    or.accessedAs(actions[i]);
                    added++;
                }
            }

            if (added > 0)
                addDisjunction(or);
        }

        public void accessedFromTo(Date accessedFrom, Date accessedTo)
        {
            if (accessedFrom != null)
                addGe("accessedFrom", accessedFrom);
            if (accessedTo != null)
                addLe("accessedTo", accessedTo);
        }

        // ---------------- parametry dla AA -----------------------
        public void agentImie(String imie) {
            if(imie != null) {
                addLike("agent_imie", imie+"%");
                isAgent = true;
            }
        }
        public void agentNazwisko(String nazwisko) {
            if(nazwisko != null) {
                addLike("agent_nazwisko", nazwisko+"%");
                isAgent = true;
            }
        }

        public void agentNumer(String numer) {
            if(numer != null) {
                addLike("agent_numer", numer+"%");
                isAgent = true;
            }
        }

        public void agencjaNazwa(String nazwa) {
            if(nazwa  != null) {
                addLike("agencja_nazwa", nazwa+"%");
                isAgencja = true;
            }
        }
        public void agencjaNumer(String numer) {
            if(numer != null) {
                addLike("agencja_numer", numer+"%");
                isAgencja = true;
            }
        }

        public void agencjaNip(String nip) {
            if(nip != null) {
                addEq("agencja_nip", nip);
                isAgencja = true;
            }
        }

        public void agencjaId(String id) {
            if(id != null) {
                addEq("agencja_id", id);
                isAgent = true;
            }
        }

        public void checkSortField(String field)
        {
            try {
                if (field != null && field.startsWith("doctype."))
                {
                    String fieldIdStr = field.substring("doctype.".length());
                    Integer fieldId = new Integer(fieldIdStr);
                    String fieldCn = Doctype.findByCn("daa").getFieldById(fieldId).getCn();
                    if ("AGENT".equals(fieldCn))
                        isAgent = true;
                    if ("AGENCJA".equals(fieldCn))
                        isAgencja = true;
                }
            }
            catch (EdmException e)
            {
            }
            catch (Exception f)
            {
            }
        }

        public boolean checkPermissions(String NR_POLISY)
        {
            if (NR_POLISY.length() >= 10)
                checkPermissions = false;
            return checkPermissions;
        }

        public void addForceSearchEq(String fieldCn)
        {
            forceSearchEq.add(fieldCn);
        }
    }

    /**
     * Tworzy kopi� dokumentu klonuj�c r�wnie� za��czniki. Dodatkowe
     * zestawy atrybut�w nie s� klonowane.
     * <p>
     * Wszystkie kolekcje powinny zosta� usuni�te przed wywo�aniem
     * metod duplicateHibernateObject() obiekt�w zale�nych.
     */
    public Object duplicateHibernateObject() throws EdmException
    {
        try
        {
            final Document clone = (Document) super.clone();
            clone.id = null;
            clone.hversion = null;
            clone.attachments = null;
            clone.watches = null;

            // klonowanie za��cznik�w (kt�re rekurencyjnie klonuj� nale��ce
            // do nich obiekty AttachmentRevision)
            if (getAttachments() != null)
            {
                for (Attachment attachment : getAttachments())
                {
                    attachment = (Attachment) attachment.duplicateHibernateObject();
                    attachment.setDocument(clone);
                }
            }
            if (getWatches() != null)
            {
                for (DocumentWatch watch : getWatches())
                {
                    watch = (DocumentWatch) watch.clone();
                    watch.setDocumentId(clone.getId());
                }
            }
            return clone;
        }
        catch (CloneNotSupportedException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Oznacza dokument jako usuni�ty i umieszcza go w folderze przeznaczonym
     * na usuni�te dokumenty. U�ytkownik musi mie� prawo DELETE do dokumentu,
     * dokument musi by� zwi�zany z sesj� Hibernate.
     */
    public void delete()
        throws EdmException, AccessDeniedException
    {
        if (!canDelete())
            throw new AccessDeniedException(
                sm.getString("documentHelper.cannotDeleteDocument", getId()));

        changelog(DocumentChangelog.DEL_DOCUMENT);
        PreparedStatement ps = null;
        try
        {
            // TODO: zamieni� na normalny SQL, bo Hibernate najpierw
            // odszukuje dokumenty po kolei i kasuje pojedynczo
            // W innych miejscach powodowa�o to deadlocki
        	ps = DSApi.context().prepareStatement("delete from ds_document_watch where document_Id = ?");
        	ps.setLong(1, id);
        	ps.execute();
        	ps.close();
        	ps = DSApi.context().prepareStatement("delete from ds_document_lock where document_Id = ?");
        	ps.setLong(1, id);
        	ps.execute();
        	ps.close();
        	ps = DSApi.context().prepareStatement("delete from ds_document_permission where document_Id = ?");
        	ps.setLong(1, id);
        	ps.execute();
        	ps.close();
//            DSApi.context().session().delete(
//                "from w in class "+DocumentWatch.class.getName()+
//                " where w.documentId = "+id);
//
//            DSApi.context().session().delete(
//                "from l in class "+DocumentLock.class.getName()+
//                " where l.documentId = "+id);
//
//            DSApi.context().session().delete(
//                "from p in class "+DocumentPermission.class.getName()+
//                " where p.documentId = "+id);
        }
        catch (Exception e)
        {
            throw new EdmException(e);
        }
        finally
        {
        	DbUtils.closeQuietly(ps);
        }

        setDeleted(true);
        this.folder = Folder.findSystemFolder(Folder.SN_TRASH);
        folderId = folder.getId();
    }

    public void scrubAttachments() throws EdmException
    {
        if (getAttachments() != null)
        {
            try
            {
                for (Iterator iter=getAttachments().iterator(); iter.hasNext(); )
                {
                    Attachment att = (Attachment)iter.next();
                    att.deleteRevisions();
                    DSApi.context().session().delete(att);
                }
                attachments.clear();

                if (this instanceof OfficeDocument)
                {
                    ((OfficeDocument) this).addWorkHistoryEntry(
                        Audit.create("attachments",
                            DSApi.context().getPrincipalName(),
                            sm.getString("TrwaleUsunietoZalaczniki"),
                            "ALLPERMDELETE"));
                }
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
        }
    }

    public void undelete() throws EdmException
    {
        changelog(DocumentChangelog.UNDEL_DOCUMENT);
        setDeleted(false);
        this.folder = Folder.getRootFolder();
        folderId = folder.getId();
    }

    /**
     * Zak�ada na dokumencie now� blokad�.
     * @param mode
     * @param expiration
     * @throws EdmException
     */
    public DocumentLock lock(DocumentLockMode mode, Date expiration) throws EdmException
    {
        if (mode == null)
            throw new NullPointerException("mode");
        if (expiration == null)
            throw new NullPointerException("expiration");
        if (expiration.before(new Date()))
            throw new EdmException("Data wyga�ni�cia blokady znajduje si� w przesz�o�ci: "+expiration);
        if (mode == DocumentLockMode.READ)
            throw new EdmException("Blokady typu "+DocumentLockMode.READ+
                " nie s� zaimplementowane");

        if (!canModify())
            throw new AccessDeniedException("Brak uprawnie� do blokowania dokumentu");

        if (!canLock(mode))
            throw new DocumentLockException(mode, id);

        clearExpiredLocks();
        try
        {
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }

        DocumentLock lock = new DocumentLock(id, expiration, mode);
        lock.create();

        return lock;
    }

    /**
     * Zwraca list� wszystkich aktualnych blokad zwi�zanych z dokumentem.
     * Po pierwszym wywo�aniu lista blokad jest zapami�tywana.
     */
    private List locks() throws EdmException
    {
        try
        {
        	org.hibernate.Query query = DSApi.context().session().createQuery("select l from l in class "+DocumentLock.class.getName()+
                    " where l.documentId = ? and l.expiration > current_timestamp");
        	query.setLong(0, id);
            return query.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca blokad� typu WRITE, je�eli taka istnieje. Dokument mo�e
     * mie� w danym momencie tylko jedn� blokad� tego typu.
     * @return
     * @throws EdmException
     */
    public DocumentLock getWriteLock() throws EdmException
    {
        try
        {
            List writeLocks = DSApi.context().classicSession().find(
                "select l from l in class "+DocumentLock.class.getName()+
                " where l.documentId = "+id+
                " and l.expiration > current_timestamp"+
                " and l.mode = ?",
                new Object[] { DocumentLockMode.WRITE },
//                new Type[] { new CustomType(DocumentLockMode.class, new Properties()) });
                new Type[] {  DSApi.context().session().getTypeHelper().custom(DocumentLockMode.class, new Properties()) });

            return writeLocks.size() > 0 ? (DocumentLock) writeLocks.get(0) : null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }


    private void clearExpiredLocks() throws EdmException
    {
        try
        {
            DSApi.context().session().delete("from l in class "+DocumentLock.class.getName()+
                " where l.expiration <= current_timestamp");
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }


    public boolean canLock(DocumentLockMode mode) throws EdmException
    {
        List locks = locks();

        if (mode == DocumentLockMode.WRITE)
        {
            return locks.size() == 0;
        }
        else if (mode == DocumentLockMode.READ)
        {
            for (Iterator iter=locks.iterator(); iter.hasNext(); )
            {
                if (((DocumentLock) iter.next()).getMode().equals(DocumentLockMode.WRITE))
                    return false;
            }
            return true;
        }
        else
        {
            throw new EdmException("Nieznany rodzaj bie��cej blokady: "+mode);
        }
    }

    /**
     * Usuwa wszystkie blokady za�o�one przez bie��cego u�ytkownika.
     */
    public void unlock() throws EdmException, AccessDeniedException
    {
        try
        {
            DSApi.context().classicSession().delete(
                "from l in class "+DocumentLock.class.getName()+
                " where l.documentId = "+id+
                " and l.username = ?",
                new Object[] { DSApi.context().getPrincipalName() },
                new Type[] { Hibernate.STRING });
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void releaseAllLocks() throws EdmException
    {
        if (!DSApi.context().isAdmin())
            throw new EdmException("Tylko administrator mo�e zwolni� wszystkie blokady");

        try
        {
            DSApi.context().session().delete(
                "from l in class "+DocumentLock.class.getName()+
                " where l.documentId = "+id);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Dodaje nowy obiekt DocumentWatch do kolekcji watches.
     *
     * @return Nowoutworzony obiekt DocumentWatch.
     * @throws EdmException Je�eli zalogowany u�ytkownik nie ma adresu email.
     * @throws DuplicateWatchException Je�eli w kolekcji watches znajduje
     * si� ju� obiekt DocumentWatch dodany przez tego u�ytkownika.
     */
    public DocumentWatch addWatch(DocumentWatch.Type watchType)
        throws EdmException, DuplicateWatchException
    {
        DSUser user = DSApi.context().getDSUser();

        if (StringUtils.isEmpty(user.getEmail()))
            throw new EdmException("U�ytkownik "+user.getName()+" nie ma adresu " +
                "email, nie mo�na doda� dokumentu do obserwowanych");
        
    	return addWatchUser(watchType, user.getName()); 
    }
    
    /**
     * Dodaje nowy obiekt DocumentWatch do kolekcji watches.
     *
     * @return Nowoutworzony obiekt DocumentWatch.
     * @throws EdmException Je�eli zalogowany u�ytkownik nie ma adresu email.
     * @throws DuplicateWatchException Je�eli w kolekcji watches znajduje
     * si� ju� obiekt DocumentWatch dodany przez tego u�ytkownika.
     */
    public DocumentWatch addWatchUser(DocumentWatch.Type watchType,String username)
        throws EdmException, DuplicateWatchException
    {

        if (hasWatch(watchType))
            throw new DuplicateWatchException(id,username);
        DocumentWatch watch = null;
        if (DocumentWatch.Type.DOCUMENT_MODIFIED.equals(watchType) || DocumentWatch.Type.ANY.equals(watchType))
        {
        	 watch = new DocumentWatch(DocumentWatch.Type.DOCUMENT_MODIFIED, id,username);
             this.addWatch(watch);
        }
        if (DocumentWatch.Type.ATTACHMENT_ADDED.equals(watchType) || DocumentWatch.Type.ANY.equals(watchType))
        {
        	 watch = new DocumentWatch(DocumentWatch.Type.ATTACHMENT_ADDED, id,username);
             this.addWatch(watch);
        }
        if (DocumentWatch.Type.ATTACHMENT_DELETED.equals(watchType) || DocumentWatch.Type.ANY.equals(watchType))
        {
        	 watch = new DocumentWatch(DocumentWatch.Type.ATTACHMENT_DELETED, id,username);
             this.addWatch(watch);
        }
        if (DocumentWatch.Type.ATTACHMENT_REVISION_ADDED.equals(watchType) || DocumentWatch.Type.ANY.equals(watchType))
        {
        	 watch = new DocumentWatch(DocumentWatch.Type.ATTACHMENT_REVISION_ADDED, id,username);
             this.addWatch(watch);
        }
        return watch;
    }

    /**
     * Zwraca true, je�eli zalogowany u�ytkownik obserwuje ju� bie��cy
     * dokument przy pomocy obiektu DocumentWatch danego typu.
     */
    public boolean hasWatch(DocumentWatch.Type watchType) throws EdmException
    {
        String username = DSApi.context().getPrincipalName();
        for (DocumentWatch watch : getWatches())
        {
            if (watch.getType() == watchType && watch.getUsername().equals(username))
            {
                    return true;
            }
        }
        return false;
    }
    

    /**
     * Zwraca true, je�eli u�ytkownik obserwuje ju� bie��cy
     * dokumen.
     */
    public boolean hasWatchAny(String username) throws EdmException
    {
        for (DocumentWatch watch : getWatches())
        {
            if (watch.getUsername().equals(username))
            {
                    return true;
            }
        }
        return false;
    }

    /**
     * Aktywuje wszystkie powiadomienia danego typu zwi�zane z dokumentem.
     */
    void fireWatches(DocumentWatch.Type watchType, Map<String, String> args)
        throws EdmException
    {
        if (log.isDebugEnabled())
            log.debug("id={} watches={}",id,watches);

        if (getWatches() == null || getWatches().size() == 0)
            return;

        for (DocumentWatch watch : getWatches())
        {
            if (watch.getType() == watchType)
                watch.queue(args);
        }
    }

    public void fireDocumentModified() throws EdmException
    {
        Map<String, String> args = new HashMap<String, String>();
        args.put("documentTitle", title);

        fireWatches(DocumentWatch.Type.DOCUMENT_MODIFIED, args);
    }

    void fireAttachmentAdded(String attachmentTitle) throws EdmException
    {
        Map<String, String> args = new HashMap<String, String>();
        args.put("documentTitle", title);
        args.put("attachmentTitle", attachmentTitle);

        fireWatches(DocumentWatch.Type.ATTACHMENT_ADDED, args);
    }

    void fireAttachmentDeleted(String attachmentTitle) throws EdmException
    {
        Map<String, String> args = new HashMap<String, String>();
        args.put("documentTitle", title);
        args.put("attachmentTitle", attachmentTitle);

        fireWatches(DocumentWatch.Type.ATTACHMENT_DELETED, args);
    }

    void fireAttachmentRevisionAdded(String attachmentTitle) throws EdmException
    {
        Map<String, String> args = new HashMap<String, String>();
        args.put("documentTitle", title);
        args.put("attachmentTitle", attachmentTitle);

        fireWatches(DocumentWatch.Type.ATTACHMENT_REVISION_ADDED, args);
    }

    /**
     *  sprawdzenie praw do dokumentu w archiwum (nie zwazajac czy to dokument kancelaryjny)
     */
    public boolean canReadXXX() throws EdmException
    {
        if (id == null)
            return true;

        if (DSApi.context().isAdmin())
            return true;

        return DSApi.context().documentPermission(id, ObjectPermission.READ);
        //return DSApi.context().canRead(this);
    }

    public boolean canRead() throws EdmException
    {
    	if (!permissionsOn)
    		return true;
        return permissionManager.canRead(this,null);
    }

    /**Sprawdza r�wnie� czy dokument znajduje si� w teczce uzytkownika*/
    public boolean canRead(Long binderId) throws EdmException
    {
    	if (!permissionsOn)
    		return true;

        return permissionManager.canRead(this,binderId);
    }

    /**
     * sprawdza czy mam prawo odczytu zalacznikow w archiwum
     */
    public boolean canReadAttachments() throws EdmException
    {
    	if (!permissionsOn)
    		return true;
        return permissionManager.canReadAttachments(this, null);
    }

    /**
     * sprawdza czy mam prawo odczytu zalacznikow w archiwum
     */
    public boolean canReadAttachments(Long binderId) throws EdmException
    {
    	if (!permissionsOn)
    		return true;
        return permissionManager.canReadAttachments(this,binderId);
    }
    
    /**
     * sprawdza czy mam prawo odczytu orginalu zalacznikow w archiwum
     */
    public boolean canReadOrginalAttachments(Long binderId) throws EdmException
    {
    	if (!permissionsOn)
    		return true;
        return permissionManager.canReadOrginalAttachments(this,binderId);
    }

    public boolean canModifyAttachments() throws EdmException
    {
    	if (!permissionsOn)
    		return true;

    	return permissionManager.canModifyAttachments(this, null);
    }

    public boolean canModifyAttachments(Long binderId) throws EdmException
    {
    	if (!permissionsOn)
    		return true;

    	return permissionManager.canModifyAttachments(this,binderId);
    }

    /**
     * Dla dokumentu to uprawnienie nie ma znaczenia (??? - za��czniki?)
     * @return true
     */
    public boolean canCreate() throws EdmException
    {
        return true;
    }

    /**
     * Dokument nie mo�e by� modyfikowany, je�eli jest oznaczony jako usuni�ty.
     */
    public boolean canModify() throws EdmException
    {
    	if (!permissionsOn)
    		return true;
        return permissionManager.canModify(this,null);
    }

    /**
     * Dokument nie mo�e by� modyfikowany, je�eli jest oznaczony jako usuni�ty.
     */
    public boolean canModify(Long binderId) throws EdmException
    {
    	if (!permissionsOn)
    		return true;
        return permissionManager.canModify(this,binderId);
    }

    /**
     * Dokument nie mo�e by� usuni�ty je�eli jest ju� oznaczony jako usuni�ty
     * oraz je�eli jest dokumentem kancelaryjnym.
     * @return
     * @throws EdmException
     */
    public boolean canDelete() throws EdmException
    {
    	if (!permissionsOn)
    		return true;

        if (id == null)
            return true;

        if(DSApi.context().hasPermission(DSPermission.DOKUMENT_USUN_GLOBALNA))
        	return true;
        if (this instanceof OfficeDocument)
            return false;

/*
        if (!forceArchivePermissions && this instanceof OfficeDocument)
            return true;
*/

        if (DSApi.context().isAdmin())
            return true;

        return DSApi.context().documentPermission(id, ObjectPermission.DELETE);
        //return DSApi.context().canDelete(this);
    }


    private static void checkLocked(Long id) throws EdmException
    {
        // je�eli dokument jest zablokowany w trybie WRITE przez
        // u�ytkownika innego ni� zalogowany, nie mo�e by� odczytany
        try
        {
/*
            Integer count = (Integer) DSApi.context().session().find(
                "select count(*) from l in class "+DocumentLock.class.getName()+
                " where l.documentId = "+id+
                " and l.expiration > current_timestamp "+
                " and l.mode = ? "+
                " and l.username != ?",
                new Object[] {
                    DocumentLockMode.WRITE,
                    DSApi.context().getDSUser().getName() },
                new Type[] { new CustomType(DocumentLockMode.class),
                             Hibernate.STRING }).get(0);

            if (count != null && count.intValue() > 0)
                return true;

            return false;
*/
            List locks = DSApi.context().classicSession().find(
                "select l from l in class "+DocumentLock.class.getName()+
                " where l.documentId = "+id+
                " and l.expiration > current_timestamp "+
                " and l.mode = ? "+
                " and l.username != ?",
                new Object[] {
                    DocumentLockMode.WRITE,
                    DSApi.context().getPrincipalName() },
                new Type[] { DSApi.context().session().getTypeHelper().custom(DocumentLockMode.class, new Properties()),
                             Hibernate.STRING });

            if (locks != null && locks.size() > 0)
            {
                DocumentLock lock = (DocumentLock) locks.get(0);
                throw new DocumentLockedException(id, lock.getUsername(), lock.getExpiration());
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }



    /**
     * Pobiera za��cznik o konkretnym ID, powi�zany z tym dokumentem.
     * Je�eli nie istnieje za��cznik o danym ID, lub istnieje, ale jest
     * oznaczony jako usuni�ty, zwracana jest warto�� null.
     * @throws EdmException 
     */
    public Attachment getAttachment(Long id)
        throws EdmException
    {
        if (id == null)
            throw new NullPointerException("id");

        if (getAttachments() != null)
        {
            for (Iterator<Attachment> iter = getAttachments().iterator(); iter.hasNext(); )
            {
                Attachment attachment = (Attachment) iter.next();
                if (!attachment.isDeleted() && attachment.getId().equals(id))
                    return attachment;
            }
        }

        throw new AttachmentNotFoundException(id);
    }

    public Attachment getAttachmentByCn(String cn) throws EdmException
    {
        if (cn == null)
            throw new NullPointerException("cn");

        if (getAttachments() != null)
        {
            for (Iterator<Attachment> iter = getAttachments().iterator(); iter.hasNext(); )
            {
                Attachment attachment = (Attachment) iter.next();
                if (!attachment.isDeleted() && cn.equals(attachment.getCn()))
                    return attachment;
            }
        }

        return null;
        //throw new AttachmentNotFoundException("Nie znaleziono za��cznika o cn="+cn);
    }
    
    public Attachment getAttachmentByFieldCn(String fieldCn) throws EdmException
    {
    	if(fieldCn == null)
    		throw new NullPointerException("fieldCn");
    	
    	for(Attachment att : getAttachments()) {
    		if(!att.isDeleted() && fieldCn.equals(att.getFieldCn())) {
    			return att;
    		}
    	}
    	
    	return null;
    }

    public void scrubAttachment(Long id) throws EdmException
    {
        if (id == null)
            throw new NullPointerException("id");

        if (getAttachments() != null)
        {
            for (Iterator iter = getAttachments().iterator(); iter.hasNext(); )
            {
                Attachment attachment = (Attachment) iter.next();
                if (attachment.getId().equals(id))
                {
                    iter.remove();
                    try
                    {
                        attachment.deleteRevisions();
                        DSApi.context().session().delete(attachment);

                        if (this instanceof OfficeDocument)
                        {
                            ((OfficeDocument) this).addWorkHistoryEntry(
                                Audit.create("attachments",
                                    DSApi.context().getPrincipalName(),
                                    sm.getString("TrwaleUsunietoZalacznik",attachment.getTitle()),
                                    "PERMDELETE", id));
                        }
                    }
                    catch (HibernateException e)
                    {
                        throw new EdmHibernateException(e);
                    }
                }
            }
        }
    }

    private DocumentUserFlags userFlags;

    /**
     * Zwraca flagi zalogowanego u�ytkownika zwi�zane z tym
     * dokumentem.  Je�eli u�ytkownik nie ustawia� flag, zwracany
     * jest nowy obiekt, kt�ry b�dzie zapisany w bazie tylko po
     * ustawieniu przynajmniej jednej flagi.
     */
    public DocumentUserFlags getUserFlags() throws EdmException
    {
        if (userFlags == null)
        {
            try
            {
                List list = DSApi.context().classicSession().find(
                    "select uf from uf in class "+DocumentUserFlags.class.getName()+
                    " where uf.documentId = ? and uf.username = ?",
                    new Object[] { id, DSApi.context().getPrincipalName() },
                    new Type[] { Hibernate.LONG, Hibernate.STRING });

                if (list.size() > 0)
                    userFlags = (DocumentUserFlags) list.get(0);
                else
                    userFlags = new DocumentUserFlags(this);
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
        }

        return userFlags;
    }

    /**
     * Zwraca �cie�k� do folderu, w kt�rym znajduje si� dokument.
     * Je�eli u�ytkownik nie ma prawa odczytu do folderu, w kt�rym
     * znajduje si� dokument (a wi�c getFolder() rzuca wyj�tek
     * AccessDeniedException), jest to jedyny spos�b na odczytanie
     * �cie�ki do tego folderu. Nazwy folder�w, do kt�rych u�ytkownik
     * nie ma prawa odczytu zostan� zast�pione przez "[...]" zgodnie
     * z dokumentacj� {@link Folder#getPrettyPath(java.lang.String)}.
     * <p>
     * Je�eli dokument nie znajduje si� w �adnym folderze, zwracana
     * jest warto�� null.
     * @param separator
     * @return
     * @throws EdmException
     */
    public String getFolderPath(String separator)
        throws EdmException
    {
        if (getFolder() != null)
            return getFolder().getPrettyPath(separator);
        else
            return null;
    }

    /**
     * @see Folder#getPrettyPath()
     */
    public String getFolderPath()
        throws EdmException
    {
        if (getFolder() != null)
            return getFolder().getPrettyPath();
        else
            return null;
    }

    /**
     * Zwraca identyfikator folderu, w kt�rym znajduje si� dokument
     * lub null, je�eli dokument nie znajduje si� w �adnym folderze.
     * Jest to jedyny spos�b na odczytanie identyfikatora folderu, je�eli
     * u�ytkownik nie ma do niego prawa odczytu.
     */
    public Long getFolderId()
    {
    	return folderId;
    }

    /**
     * Ustawia folder
     * @param folderId
     */
    public void setFolderId(Long folderId)
    {
		this.folderId = folderId;
		//Sprawdzamy czy zmiana Id nie zmieni nam obiektu folder
		if (folder !=null && !folder.getId().equals(folderId))
			folder = null;
	}

    /**
     * Zwraca folder, w kt�rym znajduje si� dokument. Je�eli dokument
     * nie znajduje si� w �adnym folderze, zwracana jest warto�� null,
     * je�eli u�ytkownik nie ma prawa odczytu tego folderu, rzucany jest
     * wyj�tek AccessDeniedException.
     * @return
     * @throws EdmException
     * @throws AccessDeniedException
     */
    public Folder getFolder()
        throws EdmException, AccessDeniedException
    {
        return getFolder(null);
    }

    /**
     * Pobiera folder, ewentualnie wykonuje tez jego inicjalizacje jesli folder == null lub jego id != folderId dokumentu
     * @param document
     * @return
     * @throws EdmException
     * @throws AccessDeniedException
     */
    protected Folder getFolder(Document document)
        throws EdmException, AccessDeniedException
    {
    	if (folderId == null)
    		return null;
        if (folder == null || !folder.getId().equals(folderId))
            folder = (Folder) Finder.find(Folder.class, folderId);

        if ((document == null || !(document instanceof OfficeDocument)) &&
            !folder.canRead())
            throw new AccessDeniedException("Brak uprawnie� do odczytania folderu, w kt�rym znajduje si� dokument "+id);

        return folder;
    }

    /**
     * Przypisuje dokument do innego folderu. U�ytkownik musi mie� prawo
     * tworzenia w danym folderze (CREATE).
     * @throws EdmException 
     * @throws AccessDeniedException 
     */
    public void setFolder(Folder folder) throws AccessDeniedException, EdmException
    {
    	setFolder(folder,false);
    }
    
    /**
     * Przypisuje dokument do innego folderu. U�ytkownik musi mie� prawo
     * tworzenia w danym folderze (CREATE).
     */
    public void setFolder(Folder folder,Boolean copyPermission)
        throws EdmException, AccessDeniedException
    {
        if (folder == null)
            throw new EdmException(sm.getString("document.nullFolderAssigned"));

        // je�eli nowy folder jest tym samym, do kt�rego dokument
        // jest ju� przypisany, nie kontroluj� uprawnie�
        if (this.folder != null && this.folder.equals(folder))
            return;

        if (this instanceof OfficeDocument && folder.isSystem(Folder.SN_OFFICE))
            //Folder.SN_OFFICE.equals(folder.getSystemName()))
        {
            this.folder = folder;
            this.folderId = folder.getId();
            return;
        }

        if (Configuration.hasExtra("business"))
        {
            Folder tmp = folder;
            while (tmp != null)
            {
                if (tmp.isSystem("businessarchive"))
                {
                    this.folder = folder;
                    this.folderId = folder.getId();
                    return;
                }
                tmp = tmp.getParent();
            }
        }

/*
        // w folderze TRASH mo�na umie�ci� ka�dy dokument bez sprawdzania
        // uprawnie�, je�eli dokument jest oznaczony jako usuni�ty
        if (isDeleted() && folder.isSystem(Folder.SN_TRASH))
            //Folder.SN_TRASH.equals(folder.getSystemName()))
        {
            this.folder = folder;
            return;
        }
*/

        // je�eli folder nie jest zwi�zany z sesj� Hibernate - rzucam wyj�tek
        if (folder.getId() == null)
            throw new EdmException("Dokument przypisano do folderu nie zapisanego w bazie danych");

        //if (!DSApi.context().canCreate(folder))

        /* TODO: narazie to ponizsze wylaczone na potrzeby DAA - sprawdzic czy dla innych
         * przypadkow nie jest to konieczne!!!                             */
        /*
        if (!folder.canCreate())
            throw new AccessDeniedException("Brak uprawnie� do tworzenia dokument�w w tym folderze: "+
                "documentId="+id+" folderId="+folder.getId());
        */
        this.folder = folder;
        this.folderId = folder.getId();
        /**Kopiujemy uprawnienia z folderu uprawnieniem any*/
        if(copyPermission)
        {
        	for(ObjectPermission permission : ObjectPermission.findObjectPermission(Folder.class, folderId))
        	{           		
        		if(ObjectPermission.USER.equals(permission.getSubjectType()) || ObjectPermission.GROUP.equals(permission.getSubjectType()))
        		{
        			DocumentPermission clone = new DocumentPermission(this.getId());
        			clone.setName(permission.getName());
        			clone.setSubject(permission.getSubject());
        			clone.setSubjectType(permission.getSubjectType());
        			//System.out.println(clone.getName()+" "+clone.getSubject()+" "+clone.getSubjectType());
                    log.trace("copy {}/{}",permission.getName(),permission.getSubject());
                    try
                    {
                    	DSApi.context().session().save(clone);
                    }
                    catch (NonUniqueObjectException e) {
						/**Jesli uprawnienie juz bylo*/
					}
        		}
        	}
        }
    }

    /**
     * Dodaje do dokumentu za��cznik. Wymagane jest prawo do modyfikacji
     * dokumentu. Za��cznik jest zapisywany w sesji (Session.saveOrUpdate()).
     *
     * @param attachment
     * @throws EdmException
     * @throws AccessDeniedException
     */
    public void createAttachment(Attachment attachment)
        throws EdmException, AccessDeniedException
    {
        createAttachment(attachment, false);
    }

    /**
     * Dodaje do dokumentu za��cznik. Wymagane jest prawo do modyfikacji
     * dokumentu. Za��cznik jest zapisywany w sesji (Session.saveOrUpdate()).
     *
     * @param attachment
     * @param compiled okre�la, czy dodawany za��cznik powsta� w wyniku kompilacji plik�w sprawy;
     * je�li tak, to do historii zostanie dodany inny wpis ni� standardowy i do dziennika zmian
     * dokumentu nie wpisana informacja ADD_ATTACHMENT
     *
     * @throws EdmException
     * @throws AccessDeniedException
     */
    public void createAttachment(Attachment attachment, boolean compiled)
        throws EdmException, AccessDeniedException
    {
        if (permissionsOn &&  !canModifyAttachments())
            throw new AccessDeniedException(sm.getString("document.addAttachmentDenied", id));

        // sprawdzenie, czy za��cznik jest zwi�zany z innym dokumentem
        if (attachment.getDocument() != null &&
            !attachment.getDocument().equals(this))
            throw new IllegalArgumentException(
                sm.getString("document.addingForeignAttachment", id));

        if (this instanceof OfficeDocument)
        {
            String tmp = null;
            if (compiled)
                tmp = sm.getString("DokonanoKompilacjiPlikowSprawy",attachment.getTitle());
            else
                tmp = sm.getString("DodanoZalacznik",attachment.getTitle());
            ((OfficeDocument) this).addWorkHistoryEntry(
                Audit.create("attachments",
                    DSApi.context().getPrincipalName(),
                    tmp,compiled ? "ADD" : "COMPILE", attachment.getId()));
        }

        if (!compiled)
            changelog(DocumentChangelog.ADD_ATTACHMENT);

        attachment.setDocument(this);
        attachment.setType(Attachment.ATTACHMENT_TYPE_DOCUMENT);
        addAttachment(attachment);
        this.mtime = new Date();

        fireAttachmentAdded(attachment.getTitle());

        try
        {
            DSApi.context().session().saveOrUpdate(this);
            DSApi.context().session().saveOrUpdate(attachment);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca list� za��cznik�w dokumentu. Zwracana lista nie jest
     * przeznaczona do modyfikacji. Na zwracanej li�cie nie pojawiaj�
     * si� za��czniki oznaczone jako usuni�te.
     * @return Lista za��cznik�w, mo�e by� pusta.
     * @throws EdmException 
     */
    public List<Attachment> listAttachments() throws EdmException
    {
            return Collections.unmodifiableList(getAttachments());
    }

/*
    public final String getBarcode()
    {
        final int space = 12;

        StringBuilder barcode = new StringBuilder(getBarcodePrefix());
        barcode.append('0');

        String ib = getIncarnationBarcode(space - barcode.length());
        // liczba zer do wstawienia, by dope�ni� kod do wymaganych <space> znak�w
        int zeros = space - barcode.length() - ib.length();
        if (zeros < 0)
            throw new IllegalArgumentException("Kod kreskowy nie zmie�ci si�" +
                " w "+space+" znakach");
        while (zeros-- > 0) barcode.append('0');
        barcode.append(ib);

        return barcode.toString();
    }

    protected char getBarcodePrefix()
    {
        return '0';
    }

    protected String getIncarnationBarcode(int max)
    {
        if (id == null)
            throw new IllegalStateException("Nie mo�na pobra� kodu kreskowego " +
                "dokumentu nie maj�cego identyfikatora");
        return id.toString();
    }
*/

    public Long getId()
    {
        return id;
    }

    protected void setId(Long id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitleNoCheck(String title) {
        this.title = title;
    }

    public void setTitle(String title)
        throws EdmException, AccessDeniedException
    {
        // klonowanie dokumentu - klon dostaje nowy tytu�
        if (!Configuration.hasExtra("business") && !canModify())
            throw new AccessDeniedException(sm.getString("document.setPropertyDenied"));

        // je�eli kto� obserwuje dokument, dostanie informacje o zmianach
        if (id != null && this.title != null && !this.title.equals(title))
        {
            fireDocumentModified();
            this.mtime = new Date();
        }

        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescriptionNoCheck(String description) {
        this.description = description;
    }

    public void setDescription(String description)
        throws EdmException, AccessDeniedException
    {
        if (!canModify())
            throw new AccessDeniedException(sm.getString("document.setPropertyDenied"));

        if (id != null && this.description != null && !this.description.equals(description))
        {
            fireDocumentModified();
            this.mtime = new Date();
        }
        this.description = description;
    }

    public Date getCtime()
    {
        return ctime;
    }

    /**
     * Nie uzywac jest w konstruktorze
     * @param ctime
     * @throws EdmException
     * @throws AccessDeniedException
     */
    @Deprecated
    public void setCtime(Date ctime)
        throws EdmException, AccessDeniedException
    {
    	if (this.ctime==null)
    	{
    		this.ctime = ctime;
    	}
    	else
    	{
    		log.warn("Nie nalezy uzywac settera, tworzone w konstruktorze zmiana nieskuteczna");
    	}
    }

    /**
     * Nie u�ywa�
     * @param ctime
     * @throws EdmException
     * @throws AccessDeniedException
     */
    @Deprecated
    public void setUnsafeCtime(Date ctime) throws EdmException, AccessDeniedException
    {
            this.ctime = ctime;
    }


    public String getAuthor()
    {
        return author;
    }

    public String getAuthorFirstLastName() throws EdmException {
        return DSUser.findByUsername(author).getLastnameFirstnameWithOptionalIdentifier();
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getExternalId()
    {
        return externalId;
    }

    public void setExternalId(String externalId)
        throws EdmException, AccessDeniedException
    {
        if (!canModify())
            throw new AccessDeniedException(sm.getString("document.setPropertyDenied"));

        this.externalId = externalId;
    }    

    /**
     * Zwraca list� za�acznik�w dokumentu. Zwracana lista mo�e by� pusta,
     * ale nie mo�e by� r�wna null.
     * @throws EdmException
     */
    public List<Attachment> getAttachments() throws EdmException
    {
        if (attachments == null && getId() != null)
        {
    		Criteria c = DSApi.context().session().createCriteria(Attachment.class);
            c.add(Restrictions.eq("documentId", this.getId()));
            c.add(Restrictions.eq("deleted", Boolean.FALSE));
            attachments = new ArrayList<Attachment>(c.list());
        }
        if(attachments == null && getId() == null )
        	attachments =  new ArrayList<Attachment>();
        return attachments;
    }    

    public void addAttachment(Attachment attachment) throws EdmException
    {	
    	getAttachments().add(attachment);
    	attachment.setDocument(this);
    }
    
    public void removeAttachment(Long id) throws EdmException
    {	
    	Attachment atta = getAttachment(id);
    	atta.delete();
    	getAttachments().remove(atta);
    }

    private boolean isOffice()
    {
        return office;
    }

    private void setOffice(boolean office)
    {
        this.office = office;
    }

    public boolean isForceArchivePermissions()
    {
        return forceArchivePermissions;
    }

    public void setForceArchivePermissions(boolean forceArchivePermissions)
    {
        this.forceArchivePermissions = forceArchivePermissions;
    }

    public Integer getHversion()
    {
        return hversion;
    }

    private void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public Date getMtime()
    {
        return mtime;
    }

    public void updateMtime(){
        this.mtime = new Date();
    }

    void setMtime(Date mtime) throws EdmException, AccessDeniedException
    {
        this.mtime = mtime;
    }

    public Doctype getDoctype()
    {
        return doctype;
    }

    public void setDoctypeOnly(Doctype doctype)
    {
        this.doctype = doctype;
    }
    
    /**
     * Uwagi
     */
    protected List<Remark> remarks = null;


    public void setDoctype(Doctype doctype) throws EdmException
    {
        // zmiana doctype
        if (id != null && this.doctype != null && doctype != null)
        {
            Doctype oldDoctype = this.doctype;
            Doctype newDoctype = doctype;

            // odczytanie aktualnych warto�ci z tabeli Doctype
            // usuni�cie wiersza z obecnej tabeli doctype (document_id=id)
            // wpisanie warto�ci do nowej tabeli (Doctype.set)

            Map values = oldDoctype.getValueMapByCn(id);

            for (Iterator iter=values.entrySet().iterator(); iter.hasNext(); )
            {
                Map.Entry entry = (Map.Entry) iter.next();
                try
                {
                    Doctype.Field f = newDoctype.getFieldByCn((String) entry.getKey());
                    newDoctype.set(id, f.getId(), entry.getValue());
                }
                // nie ma pola o takim CN
                catch (EntityNotFoundException e)
                {
                    continue;
                }
                // nie da�o si� skonwertowa� warto�ci mi�dzy polami o takim samym CN
                catch (FieldValueCoerceException e)
                {
                    continue;
                }
            }

            oldDoctype.remove(id);
        }
        else if (this.doctype != null && doctype == null)
        {
            this.doctype.remove(this.id);
        }

        this.doctype = doctype;
    }

    public DocumentKind getDocumentKind()
    {
        return documentKind;
    }

    public void setDocumentKind(DocumentKind documentKind) throws EdmException
    {
        if (id != null && this.documentKind != null && !(this.documentKind.equals(documentKind)))
        {
            if ((documentKind != null) && (this instanceof OfficeDocument))
            {
                OfficeDocument doc = (OfficeDocument) this;

                doc.changelog(DocumentChangelog.CHANGE_DOCKIND);
                doc.addWorkHistoryEntry(
                    Audit.create("changeDoctype",
                        DSApi.context().getPrincipalName(),
                        sm.getString("ZmienionoRodzajDokumentuZna",this.documentKind.getName(),documentKind.getName())));
            }
            if(documentKind != null)
            {
            	DataMartEvent dme = new DataMartEvent(true, this.getId(), null, DataMartDefs.DOCUMENT_DOCKIND_CHANGE, null, this.documentKind.getCn(), documentKind.getCn(), null);
     	        //DataMartManager.storeEvent(dme);
            }

            if(Boolean.parseBoolean(Docusafe.getAdditionProperty("deleteDataOnDockindChange")))
            	this.documentKind.remove(id);
            // czyszcze na wypadek gdyby stare typy to 'nationwide', 'nationwide_drs' lub 'daa'
            this.setDoctypeOnly(null);
        }
        this.documentKind = documentKind;
    }

    public boolean isDeleted()
    {
        return deleted;
    }

    private void setDeleted(boolean deleted)
    {
        this.deleted = deleted;
    }

    public Set<DocumentWatch> getWatches() throws EdmException
    {
    	if (watches == null)
    	{
    		log.trace("inicjujemy obserwowane");
//    		Query q = DSApi.context().session().createQuery("from pl.compan.docusafe.core.base.DocumentWatch where documentId = ?");
//    		q.setLong(0, id);

    		watches = new HashSet<DocumentWatch>(
					DSApi.context().session()
						.createCriteria(DocumentWatch.class)
							.add(Restrictions.eq("documentId", id)).list()
				);
    	}
        return watches;
    }
    
	public void deleteWatch(DocumentWatch watch) throws EdmException 
	{
    	getWatches();
    	watches.remove(watch);	
    	DocumentWatch.deleteWatch(watch.getId());
	}
    /**
     * Dodaje do obserwowanych dokumentow
     * @param watch
     * @throws EdmException
     */
    protected void addWatch(DocumentWatch watch) throws EdmException
    {
    	getWatches();
    	watch.setDocumentId(id);
    	DSApi.context().session().save(watch);
    	watches.add(watch);
    }
    public String getLparam()
    {
        return lparam;
    }

    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }

    public Long getWparam()
    {
        return wparam;
    }

    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }

    public Box getBox()
    {
        return box;
    }

    public void removeFromBox() throws EdmException{
    	if (this instanceof OfficeDocument)
        {
            ((OfficeDocument) this).addWorkHistoryEntry(
                Audit.create("box",
                    DSApi.context().getPrincipalName(),
                    sm.getString("DokumentWycofanoZPudla")));
        }
    	this.box=null;
    }
    public void setBox(Box box) throws EdmException
    {
        if (box != null &&
            (this.box == null || !(this.box.getId().equals(box.getId()))/*!this.box.equals(box)*/))
        {
            this.boxingDate = new Date();

            if (this.box != null)
            {
                changelog(DocumentChangelog.CHANGE_DOCUMENT_BOX);
            }
            else
            {
                changelog(DocumentChangelog.SET_DOCUMENT_BOX);
            }

            if (this instanceof OfficeDocument)
            {
                ((OfficeDocument) this).addWorkHistoryEntry(
                    Audit.create("box",
                        DSApi.context().getPrincipalName(),
                        sm.getString("DokumentUmieszczonoWpudleArchiwalnym",box.getName()),
                        null, box.getId()));
            }
        }
        else if (box == null && this.box != null)
        {
            this.boxingDate = null;

            changelog(DocumentChangelog.REMOVE_DOCUMENT_BOX);

            if (this instanceof OfficeDocument)
            {
                ((OfficeDocument) this).addWorkHistoryEntry(
                    Audit.create("boxremove",
                        DSApi.context().getPrincipalName(),
                        sm.getString("DokumentUsunietoZpudla",this.box.getName()),
                        null, this.box.getId()));
            }
        }

        this.box = box;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public Date getBoxingDate()
    {
        return boxingDate;
    }

    public void setBoxingDate(Date boxingDate)
    {
        this.boxingDate = boxingDate;
    }

    /**
     * Zwraca globalne flagi zwi�zane z tym dokumentem.  Je�eli
     * dokument nie posiada flag, zwracany jest nowy obiekt, kt�ry
     * b�dzie zapisany w bazie danych tylko wtedy, gdy zostanie
     * w nim ustawiona przynajmniej jedna flaga.
     */
    public DocumentFlags getFlags()
    {
        if (flags == null)
            flags = new DocumentFlags(this);
        flags.setDocumentId(this.getId());
        return flags;
    }

    public void setFlags(DocumentFlags flags)
    {
        this.flags = flags;
    }

    public Boolean getBlocked()
    {
        return blocked;
    }

    public void setBlocked(Boolean blocked)
    {
        this.blocked = blocked;
    }

    /* Lifecycle */

    /**
     * Przy tworzeniu nowej instancji dokumentu w bazie danych
     * wype�niane s� atrybuty author i ctime.
     * <p>
     * Je�eli dokument nie zosta� przypisany do folderu, rzucany
     * jest wyj�tek CallbackException.
     * <p>
     * Je�eli bie��cy kontekst ma ustawion� flag� FL_OFFICE, do
     * pola flags dodawana jest flaga ATT_OFFICE, w przeciwnym
     * wypadku flaga ta jest kasowana.
     */
    public boolean onSave(Session session) throws CallbackException
    {
        try
        {
        	if(folderId == null && folder != null && folder.getId() == null)
        		DSApi.context().session().flush();
        	if(folder != null && folder.getId() != null)
        		setFolderId(folder.getId());
        }
        catch (Exception e)
        {}

        if (folderId == null)
            throw new CallbackException(sm.getString("document.nullFolderOnSave"));

        if (author == null)
        {
            author = DSApi.context().getPrincipalName();
        }

        // czas utworzenia dokumentu w bazie danych
        if (ctime == null)
        {
            ctime = new Date();
        }

        if (mtime == null)
        {
            mtime = new Date();
        }

        if (this instanceof OfficeDocument)
        {
            office = true;
            String summary = ((OfficeDocument) this).getSummary();
            if (summary != null)
            {
                if (getDoctype() == null || !("daa".equals(getDoctype().getCn()))) {
                    title = summary;
                    description = summary;
                }
            }
        }
        try
        {
        	if(documentKind == null)
				documentKind = DocumentKind.findByCn(DocumentKind.NORMAL_KIND);
		}
        catch (EdmException e) 
        {
			log.error(e.getMessage(),e);
		}
        //this.office = DSApi.context().hasFlag(DSContext.FL_OFFICE);

        // nie ma potrzeby sprawdzania, czy dokument jest zapisywany w folderze
        // SN_OFFICE, poniewa� jest to wykonywane w ramach kontroli uprawnie�
        // przez SecuredEntity.hasPermission (Folder.canCreate)

        return false;
    }



    public boolean onUpdate(Session session) throws CallbackException {
        if (folder == null)
            throw new CallbackException(sm.getString("document.nullFolderOnSave"));

        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Document)) return false;

        final Document document = (Document) o;

        if (id != null ? !id.equals(document.id) : document.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public String toString()
    {
        return getClass().getName()+"#"+id;
    }

    public Long getDuplicateDoc()
    {
        return duplicateDoc;
    }

    public void setDuplicateDoc(Long duplicateDoc)
    {
        this.duplicateDoc = duplicateDoc;
    }

	public String getLastRemark() {
		return lastRemark;
	}

	public void setLastRemark(String lastRemark) {
		this.lastRemark = lastRemark;
	}
	
    public void addRemark(Remark remark) throws EdmException
    {
    	DataMartManager.storeDocumentChangelog(getId(), DSApi.context().getPrincipalName(), DocumentChangelog.ADD_REMARK);
    	remark.setDocumentId(this.getId());
    	List<Remark> rl = getRemarks();
    	rl.add(remark);
    	DSApi.context().session().save(remark);
        this.setLastRemark(StringUtils.left(remark.getContent(), 240));
        TaskSnapshot.updateByDocumentId(this.getId(),this.getStringType());

        if (getId() != null && remark != null)
        {
            if (!StringUtils.isEmpty(remark.getContent()))
            {
            	DataMartManager.addHistoryEntry(getId(),Audit.create("remark", DSApi.context().getPrincipalName(),
                        /*sm.getString("DodanoUwageH",remark.getContent())*/ "Dodano uwag� " + remark.getContent(), StringUtils.left(remark.getContent(),200)));
            }
        }
    }
    
    public void deleteRemark(Remark remark) throws EdmException
    {
    	DSApi.context().session().delete(remark);
    	
    	String remarkDigest = "\""+StringUtils.left(remark.getContent(), 50) + (remark.getContent().length() > 50 ? "..." : "") + "\"";
    	
        DataMartManager.addHistoryEntry(this.getId(), Audit.create("remarks", DSApi.context().getPrincipalName(),sm.getString("UsunietoUwageOTresciDodanaPrzez",remarkDigest,DSUser.safeToFirstnameLastname(remark.getAuthor())), "DELETE", null));
        
        try
        {
        	this.setLastRemark(StringUtils.left(this.getRemarks().get(this.getRemarks().size()-1).getContent(),240));
        }
        catch (Exception e) 
        {
        	this.setLastRemark(null);
		}
        //TaskSnapshot.updateByDocument(this);
        TaskSnapshot.updateByDocumentId(this.getId(),this.getStringType());
    }

    /**
     * Zwraca ostatnio dodana uwage z tabeli dso_document_remarks
     * @return
     * @throws EdmException
     */
    public Remark findLastRemark() throws EdmException
    {
    	Remark res;
    	List<Remark> rlist = getRemarks();
    	res= rlist.get(0);
    	if(res==null)
    		return res;
    	for(int i=1;i<rlist.size();i++){
    		if(rlist.get(i).getCtime().after(res.getCtime()))
    			res=rlist.get(i);
    	}
    	return res;
    }
    
    /**
	 * Zwraca list� uwag dodanych do dokumentu.
	 * 
	 * @return Lista uwag, kt�ra mo�e by� pusta.
	 * @throws EdmException
	 */
    public List<Remark> getRemarks() throws EdmException
    {
        if (remarks == null)
        {
        	remarks = Remark.documentRemarks(getId());
        }
        return remarks;
    }

	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	public String getFinishUser() {
		return finishUser;
	}

	public void setFinishUser(String finishUser) {
		this.finishUser = finishUser;
	}

    public IndexStatus getIndexStatus(){
        return IndexStatus.get(indexStatusId);
    }

    public void setIndexStatus(IndexStatus status){
        this.indexStatusId = status.id;
        log.info("setIndexStatus {}", indexStatusId);
    }

//	public void setReplies(Set<Document> replies) {
//		this.replies = replies;
//	}

	public Set<Document> getReplies() throws EdmException {
		if (!repliesModified)
			return replies;
		else {
			replies = getRepliesFromDB();
			repliesModified = false;
			return replies;
		}
	}

	public void addReply(Document d){
		repliesModified = true;
		d.setRepliedDocumentId(getId());
		//replies.add(d);
	}

	public void setRepliedDocumentId(Long repliedDocumentId) {
		this.repliedDocumentId = repliedDocumentId;
	}

	public Long getRepliedDocumentId() {
		return repliedDocumentId;
	}

	public FieldsManager getFieldsManager(){
		//TODO zastanowi� si� jak to dobrze cache'owac
		return getDocumentKind().getFieldsManager(this.getId());
	}

	public static class DocumentComparatorByDate implements Comparator<Document>
    {
        public int compare(Document e1, Document e2)
        {
            return e1.getCtime().compareTo(e2.getCtime());
        }
    }

	private Set<Document> getRepliesFromDB() throws EdmException
	{
		PreparedStatement ps = null;
		Set<Document> result = new HashSet<Document>();
		ResultSet rs = null;
		try {
			ps = DSApi.context().prepareStatement("select id from ds_document where replied_document_id = ?");
			ps.setLong(1, getId());
			rs = ps.executeQuery();
			while(rs.next())
			{
				Document doc = Document.find(rs.getLong("id"));
				if (doc != null)
				{
					result.add(doc);
				}
			}
			rs.close();
			return result;
		}
		catch (SQLException e)
		{
			throw new EdmException(e.getMessage());
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
	}


	//Przenios�em to z OfficeDocument poniewaz przydalo by sie zeby dokument archiwalny tez mial historie


    /**
     * Zwraca ostatni wpis z historii dokumentu
     * @return
     */
    public Audit getLastHistoryEntry()
    {
    	Audit resp = null;
    	try
    	{
    		resp = DataMartManager.getLastHistoryEntry(getId());
    	}
    	catch (Exception e)
    	{
    		log.error("",e);
    	}
    	return resp;
    }
    /**
     * Lista obiekt�w {@link Audit}.
     */
    public List<Audit> getWorkHistory()
    {
        if (workHistory == null)
            workHistory = DataMartManager.getWorkHistory(this);
        return workHistory;
    }

    public void setWorkHistory(List<Audit> workHistory)
    {
/*    	if(AvailabilityManager.isAvailable("dokument.usuwaStareWpisyZHistoriiZanimUstawiaNowa")){
    		try {
				PreparedStatement ps = DSApi.context().prepareStatement( "delete from dso_document_audit where document_id="+this.id.toString());
				ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    	}*/
        this.workHistory = workHistory;
    }

    /**
     *
     * @return
     */
    public boolean isPermissionsOn() {
		return permissionsOn;
	}

    /**
     * Wlaczenie wartosci false spowoduje ze zostana zignorowane wszelkie kontrole uprawniem
     * do odczytu, modyfikacji, odczytu zalacznika, modyfikacji zalacznika, usuniecia, etc.
     * Jest to pozyteczne gdy:
     * - jestesmy pewni ze uzytkownik ma prawo wykonac ta operacje, ale nie ma zapisanych ich jawnie.
     * - istotna jest wydajnosc (mniej operacji jest wykonywanych)
     * Wazne jest jest aby miec pewnosc ze uzytkownik jednak moze wykonac te operacje (uprawnienia
     * musza byc sprawdzone wczesniej)
     * @param permissionsOn - true (domyslne) - kontrola wlaczona - false, wylaczona
     */
	public void setPermissionsOn(boolean permissionsOn) {
		this.permissionsOn = permissionsOn;
	}	
	
	/**
	 * link url do dokument 
	 * @return
	 */
	public static String getArchiveUrl(Long documentId)
	{
		String baseUrl = "/office/incoming/document-archive.action?documentId=";
		
		if(Docusafe.pageContext != null)
		{
			baseUrl ="/" + Docusafe.pageContext + baseUrl + documentId;
		}
		
		return baseUrl;
	}
	
	public String getCurrentWorkflowLocation()
	{
		WatchDog wd = new WatchDog(200);
		wd.setMessageCode("Przekroczono limit dla wyciagania historii dekretacji");
		String rep = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			ps = DSApi.context().prepareStatement("select sourceuser,targetuser, targetguid, finished from dso_document_asgn_history where document_id = ? and processname not like 'do wiadomo%' order by ctime desc");
			ps.setLong(1, this.getId());
        	rs = ps.executeQuery();
        	if(rs.next())
        	{
        		String username = rs.getString("targetuser");
            	String guid  = rs.getString("targetguid");
            	Integer finished = rs.getInt("finished");
            	String source = rs.getString("sourceuser");
            	
            	if(finished==1)
            	{
            		try
        			{
        				rep = DSUser.findByUsername(source).asFirstnameLastname();
        				rep += " ("+sm.getString("zakonczone")+")";
        			}
        			catch (Exception e) {}
            	}
            	else if(username!=null && !"x".equals(username))
        		{
        			try
        			{
        				rep = DSUser.findByUsername(username).asFirstnameLastname();
        			}
        			catch (Exception e) {}
        		}
        		else if(guid!=null && !"x".equals(guid))
        		{
        			try
        			{
        				rep = DSDivision.find(guid).getName();
        			}
        			catch (Exception e) {}
        			
        		}
        		else if(source!=null && !"x".equals(source))
        		{
        			try
        			{
        				rep = DSUser.findByUsername(source).asFirstnameLastname();
        			}
        			catch (Exception e) {}
        		}
        	}
		}
		catch(Exception e)
		{
			log.error(e.getMessage(),e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		//System.out.println(rep);
		wd.tick();
		return rep;
	}

	/**
	 * Zwraca ostatni� uwag� u�ytkownika, mo�e zwr�ci� null
	 * @param username
	 * @return
	 */
	public Remark getLastUserRemark(String username)
	{
		List<Remark> remarks;
		try
		{
			remarks = getRemarks();
			
			for(Remark remark : remarks)
			{
				if(remark.getAuthor().equals(username))
					return remark;
			}
		
		} catch (EdmException e)
		{
			log.error("", e);
		}
		return null;
	}
	
	public long getVersionGuid()
	{
		return versionGuid;
	}
	
	public void setVersionGuid(long versionGuid)
	{
		this.versionGuid=versionGuid;
	}

	public int getVersion()
	{
		return version;
	}

	public void setVersion(int version)
	{
		this.version = version;
	}

	public String getVersionDesc()
	{
		return versionDesc;
	}

	public void setVersionDesc(String versionDesc)
	{
		this.versionDesc = versionDesc;
	}

	public Boolean getCzyCzystopis()
	{
		return czyCzystopis;
	}

	public void setCzyCzystopis(Boolean czyCzystopis)
	{
		this.czyCzystopis = czyCzystopis;
	}

	public Boolean getCzyAktualny()
	{
		return czyAktualny;
	}

	public void setCzyAktualny(Boolean czyAktualny) 
	{
		this.czyAktualny = czyAktualny;
	}
	

	public Date getArchivedDate() {
		return archivedDate;
	}

	public void setArchivedDate(Date archivedDate) {
		this.archivedDate = archivedDate;
	}

	public Boolean getIsArchived() {
		return isArchived;
	}

	public void setIsArchived(Boolean isArchived) {
		this.isArchived = isArchived;
	}
	
	public Long getAccessToDocument()
	{
		return accessToDocument;
	}
	
	public void setAccessToDocument(Long accessToDocument)
	{
		this.accessToDocument=accessToDocument;
	}
	public String getAbstractDescription()
	{
		return abstractDescription;
	}

	
	public void setAbstractDescription(String abstractDescription)
	{
		this.abstractDescription = abstractDescription;
	}
	public String getAutorDivision()
	{
		return documentAutorDivision;
	}

	public void setAutorDivision(String documentAutorDivision)
	{
		this.documentAutorDivision = documentAutorDivision;
	}

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
	public Boolean getInPackage()
	{
		return inPackage;
	}

	
	public void setInPackage(Boolean inPackage)
	{
		this.inPackage = inPackage;
	}

    /**
     * Zwraca true je�li zalogowany u�ytkownik jest autorem.
     * @return
     */
    public boolean isLoggedAuthor(){
        if(DSApi.context().getPrincipalName().equals(author))
            return true;

        return false;
    }
    

	public String getDocumentBarcode() {
		return documentBarcode;
	}

	public void setDocumentBarcode(String documentBarcode) {
		this.documentBarcode = documentBarcode;
	}

}
