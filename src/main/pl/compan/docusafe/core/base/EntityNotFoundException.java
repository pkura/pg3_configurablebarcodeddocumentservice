package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.StringManager;

import java.io.Serializable;

/**
 * Wyj�tek bazowy dla wszystkich wyj�tk�w informuj�cych, �e
 * jaki� obiekt nie zosta� znaleziony.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EntityNotFoundException.java,v 1.6 2008/11/08 13:59:34 wkuty Exp $
 */
public class EntityNotFoundException extends EdmException
{
	static final long serialVersionUID = 1341;
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public EntityNotFoundException(Class clazz, Serializable id)
    {
        super(sm.getString("entityNotFoundException", clazz.getName(), id));
    }

    public EntityNotFoundException(Class clazz, Serializable id, Throwable cause)
    {
        super(sm.getString("entityNotFoundException", clazz.getName(), id), cause);
    }

    public EntityNotFoundException(String message)
    {
        super(message);
    }

    protected EntityNotFoundException(String message, boolean noLogging)
    {
        super(message, noLogging);
    }
    
}
