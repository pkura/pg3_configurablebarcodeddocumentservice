package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.security.AccessDeniedException;

/* User: Administrator, Date: 2005-12-10 11:40:25 */

/**
 * Obiekt DocumentFlags jest przez aplikacj� tworzony przy pomocy
 * konstruktora DocumentFlags(Document).  Obiekt nie jest zapisywany
 * w bazie do momentu, gdy warto�� kt�rej� z flags zostanie zmieniona
 * na true.  Dop�ki wszystkie flagi maj� warto�� false, swoj� funkcj�
 * wystarczaj�co wype�nia obiekt tymczasowy.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentFlags.java,v 1.9 2008/12/15 13:14:33 pecet1 Exp $
 */
public class DocumentFlags extends Flags
{


    /**
     * Pole niezarz�dzane przez Hibernate.
     */
    private Document document;

    DocumentFlags()
    {
    }

    public DocumentFlags(Document document)
    {
        this.document = document;
    }
    
    public boolean canSetFl(Long flag) throws EdmException
    {
        return LabelsManager.canAddLabel(flag, DSApi.context().getPrincipalName());
    }

    /**
     *
     * @param flag
     * @param value
     * @return True, je�eli nast�pi�a zmiana warto�ci flagi.
     * @throws AccessDeniedException Je�eli u�ytkownik nie ma uprawnie� do
     *  zmiany warto�ci flagi.
     */
    public boolean setFl(Long flag, boolean value) throws EdmException
    {
		if(value){
			LabelsManager.addLabel(getDocumentId(), flag, DSApi.context().getPrincipalName());
		} else {
			LabelsManager.removeLabel(getDocumentId(), flag, DSApi.context().getPrincipalName());
		}

        addDocumentHistory(flag, value);

        return true;
    }

    public static String getFlagDescription(Long flag) throws EdmException
    {
        return LabelsManager.findLabelById(flag).getDescription();
    }

    public static String getFlagC(Long flag) throws EdmException
    {
    	return LabelsManager.findLabelById(flag).getName(); 
    }
}
