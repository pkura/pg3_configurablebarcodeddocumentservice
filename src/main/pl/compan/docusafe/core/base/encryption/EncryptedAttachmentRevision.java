package pl.compan.docusafe.core.base.encryption;

import java.awt.image.RenderedImage;
import java.awt.image.renderable.ParameterBlock;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;

import org.apache.commons.io.IOUtils;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.BlobInputStream;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.ShUtils;

import com.sun.media.jai.codec.FileCacheSeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;

public class EncryptedAttachmentRevision extends AttachmentRevision
{
	
	public EncryptedAttachmentRevision()
	{
		super();
	}

	
	private File getFile() throws EdmException, IOException
    {
        File file = new File(Attachment.getPath(getId()));
        if(!Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM || !file.exists())
        {
            if (!file.exists() || !file.isFile() || file.length() != getSize().intValue())
            {            	
                ShUtils.delete(file);
                saveToFile(file);
            }
 
        }
        //jesli zalaczniki ogolnie sa trzymane na dysku 
        //oraz akurat ten egzemplarz jest na dysku 
        //to wystarczy go zwrocic
        return file;
    }
	
	/**
	 * Nie zaimplementowane. Nie dopuszczamy zapisywania zalacznikow szyforwanych do plikow tymczasowych
	 */
	public File saveToTempFile() throws EdmException, IOException
    {
		throw new EdmException("Nie zaimplementowane");
    }
	
	/**
	 * metoda szyfruje oraz zapisuje dane do pliku
	 */
	public long saveToFile(File file) throws EdmException, IOException
	{
		
		if (file.exists() && !file.canWrite())
            throw new EdmException("Brak uprawnie� do zapisu pliku " + file);
        if(!file.getParentFile().exists())
         {
        	(new File(file.getParent())).mkdirs();
        }        
        OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
        InputStream is = getBinaryStream();
        InputStream encrypted = DSApi.context().credentials().encryptStream(is, this.getAttachment().getDocument().getDocumentKind().getCn());
        byte[] buf = new byte[2048];
        int count;
        long total = 0;
        while ((count = encrypted.read(buf)) > 0)
        {
            total += count;
            os.write(buf, 0, count);
        }
        org.apache.commons.io.IOUtils.closeQuietly(is);
        org.apache.commons.io.IOUtils.closeQuietly(encrypted);
        org.apache.commons.io.IOUtils.closeQuietly(os);        
        return total;
		//throw new EdmException("Nie zaimplementowane");
	}
	
	/**
	 * metoda zwraca rozszyfrowany strumien danych z zalcznika
	 */
	public InputStream getBinaryStream() throws EdmException
	{
		
		boolean closeStm = true;
        if (revision == null)
            throw new NullPointerException(sm.getString("nullParameter", "revision"));
        PreparedStatement pst = null;
        try
        {

            pst = DSApi.context().prepareStatement("select contentdata, onDisc from ds_attachment_revision where id = ?");
            pst.setLong(1, contentRefId != null ? contentRefId : getId());
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
                throw new EdmException(sm.getString("documentHelper.blobRetrieveFailed", getId()));

            InputStream rawIs;
            int onDisc = rs.getInt(2);
            if (onDisc == AttachmentRevision.FILE_ON_DISC)
                rawIs = new FileInputStream(new File(Attachment.getPath((contentRefId != null ? contentRefId : getId()))));
            else
                rawIs = rs.getBinaryStream(1);
            
            if (rawIs != null)
            {
            	closeStm = false;
            	BlobInputStream is = new BlobInputStream(rawIs, pst);
            	InputStream decrypted = DSApi.context().credentials().decryptStream(is, this.getAttachment().getDocument().getDocumentKind().getCn());
            	org.apache.commons.io.IOUtils.closeQuietly(is);
                return decrypted;
            }
            else
            {            	
                return null;
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "revision=" + revision);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (FileNotFoundException fnfe)
        {
            throw new EdmException("file not found");
        }
        finally
        {
        	if (closeStm)
        	{
        		DSApi.context().closeStatement(pst);
        	}
        }
        // Nie mo�na zamkn�� obiektu Statement od razu po zwr�ceniu strumienia,
        // poniewa� w�wczas nie b�dzie mo�liwe odczytanie danych ze strumienia.
        // Statement jest zamykane w metodzie BlobInputStream.closeStream()
	}
	
	/**
	 * metoda przyjmuje strumien danych nastepnie go szyfruje i zapisuje w domyslnym magazynie (dysk lub baza)
	 */
	public void updateBinaryStreamRevisionSpecific(InputStream stream, int fileSize) throws EdmException
	{
		PreparedStatement ps = null;
        int rows;
        Long currentId = this.getId().longValue();
        try
        {
        	InputStream encrypted = DSApi.context().credentials().encryptStream(stream, this.getAttachment().getDocument().getDocumentKind().getCn());
            if (Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM)
            {
                ps = DSApi.context().prepareStatement("update " + DSApi.getTableName(AttachmentRevision.class) + " set contentdata = null where id = ?");
                ps.setLong(1, currentId);
                rows = ps.executeUpdate();
                if (rows != 1)
                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
                
                this.updateFile(encrypted);
            }
            else
            {
                ps = DSApi.context().prepareStatement("update " + DSApi.getTableName(AttachmentRevision.class) + " set contentdata = ? where id = ?");
                ps.setBinaryStream(1, encrypted, encrypted.available());
                ps.setLong(2, currentId);
                rows = ps.executeUpdate();
                
                if (rows != 1)
                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (IOException e)
        {
        	throw new EdmException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        	org.apache.commons.io.IOUtils.closeQuietly(stream);
        }
	}

	/**
	 * TODO napisac implementacje
	 */
	public String getAnnotations() throws DocumentNotFoundException, AccessDeniedException, EdmException, IOException
    {
		throw new EdmException("Nie zaimplementowane");
    }
	
	/**
	 * TODO napisac implementacje
	 */
	public void updateAnnotations(String source) throws EdmException, IOException
	{
		throw new EdmException("Nie zaimplementowane");
	}
	
	/**
	 * metoda zwraca strumien zalacznika
	 */
	public InputStream getAttachmentStream() throws EdmException
	{
		try
    	{
	    	File file = new File(Attachment.getPath(getId()));
	        if(this.getOnDisc()!=1 || !file.exists())
	        {
	            if (!file.exists() || !file.isFile() || file.length() != getSize().intValue())
	            {            	
	                ShUtils.delete(file);
	                saveToFile(file);
	            }
	        }
	        InputStream is = DSApi.context().credentials().decryptStream(new FileInputStream(file),  this.getAttachment().getDocument().getDocumentKind().getCn());
	        return is;
    	}
    	catch (Exception e) 
    	{
			throw new EdmException(e);
		}
	}

	/**
	 * TODO napisac implementacje
	 */
	public InputStream getStreamByPage(int page, String format, Boolean fax) throws EdmException 
	{
		//System.out.println("Przegldam po stronach");
		File imageFile;

		try
		{
		    if (mime.startsWith("image/tiff"))
		    {            
		        String path = getPathToDecompressedImage(id, format, fax, page);
		        byte[] image = new byte[0];
		        if (Attachment.IMAGES_DECOMPRESSED_IN_LOCAL_FILE_SYSTEM)
		        {
		        	//System.out.println(path);
		            imageFile = new File(path);
		            
		            if (!imageFile.exists())
			        {
		            	image = readImageFile(getAttachmentStream(), format, page);
            	        
            	        InputStream is = DSApi.context().credentials().encryptStream(new ByteArrayInputStream(image), this.getAttachment().getDocument().getDocumentKind().getCn());
            		 	FileOutputStream fos = new FileOutputStream(imageFile);
            		 	IOUtils.copy(is, fos);
            		 	is.close();
            		 	fos.close();
            		 	
            		 	return new ByteArrayInputStream(image);
			        }
		            else
		            {
		            	InputStream is = new FileInputStream(imageFile);
		            	return DSApi.context().credentials().decryptStream(is, this.getAttachment().getDocument().getDocumentKind().getCn());
		            }
		            
		        }
		        else 
		        {
		        	image = readImageFile(getAttachmentStream(), format, page);
        		 	return new ByteArrayInputStream(image);
		        }
		    }
		    else
		    {
		    	return this.getAttachmentStream();
		    }         
         }
         catch (Exception e)
         {
             throw new EdmException(e);
         }
	}

	public RenderedImage getPage(int page) throws EdmException 
	{
		try
		{
			InputStream is = DSApi.context().credentials().decryptStream(new FileInputStream(getFile()), this.getAttachment().getDocument().getDocumentKind().getCn());
			SeekableStream ss = new FileCacheSeekableStream(is);
		 	ImageDecoder decoder = ImageCodec.createImageDecoder("tiff", ss, null);
			int pageCount = decoder.getNumPages();
            if (page >= pageCount)
                 page = pageCount-1;
            
            RenderedImage im = decoder.decodeAsRenderedImage(page); 
            return im;
		}
		catch (Exception e) 
		{
			throw new EdmException(e);
		}
	}

	public Integer getPageCount() throws EdmException 
	{
		try
		{
			if(mime.endsWith("tiff")||mime.endsWith("tif"))
			{
				InputStream is = DSApi.context().credentials().decryptStream(new FileInputStream(getFile()), this.getAttachment().getDocument().getDocumentKind().getCn());
				SeekableStream ss = new FileCacheSeekableStream(is);
			 	ImageDecoder decoder = ImageCodec.createImageDecoder("tiff", ss, null);
				int pageCount = decoder.getNumPages();
				return pageCount;
			}
			else return 1;
		}
		catch (Exception e) 
		{
			throw new EdmException(e);
		}
	}
	
	
	private byte[] readImageFile(InputStream is, String format, int page) throws Exception
	{
		SeekableStream ss = new FileCacheSeekableStream(is);
	 	ImageDecoder decoder = ImageCodec.createImageDecoder("tiff", ss, null);
	 	RenderedImage im = decoder.decodeAsRenderedImage(page); 	 		 
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ParameterBlock pb = new ParameterBlock();
        pb.addSource(im);
        pb.add(os);
        pb.add(format);
        RenderedOp op = JAI.create("encode", pb);
        op.dispose();
        os.close();
        byte[] image = os.toByteArray();
        return image;
	}
	
	/*public InputStream getImageAsPDF() throws EdmException
	{
		throw new EdmException("Nie zaimplementowane");
	}*/
}
