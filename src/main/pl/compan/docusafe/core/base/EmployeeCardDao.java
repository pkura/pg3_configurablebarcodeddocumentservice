package pl.compan.docusafe.core.base;

import java.util.ArrayList;
import java.util.List;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * 
 * @author �ukasz Wo�niak <l.g.wozniak@gmail.com>
 */
public class EmployeeCardDao
{
    protected static final Logger log = LoggerFactory.getLogger(EmployeeCardDao.class);
    /**
     * Zwraca lis� kart pracownik�w podwladnych
     * @see rev 2586 dla poprzedniej wersji funkcji
     */
    public static List<EmployeeCard> getSubordinatesCard(String username) throws EdmException
    {
        List<EmployeeCard> empCards = new ArrayList<EmployeeCard>();
        try
        {
            List<DSUser> users = DSUser.listAcceptanceSubordinates(username);
            for( DSUser user : users )
            {
//                .replace("ext:", "")
                try
                {
                    EmployeeCard empCard = getActiveEmployeeCardByUserName(user.getName());
                    if(empCard != null)
                        empCards.add(empCard);
                }
                catch ( Exception e )
                {
                }
            }
        }
        catch ( Exception e )
        {
            log.debug(e.getMessage(), e);
        }
        
        return empCards;
    }
    
     /**
     * Zwraca aktywn� kart� pracownika na podstawie nazwy usera.
     * 
     * @param userName
     * @return
     * @throws EdmException
     */
    public static EmployeeCard getActiveEmployeeCardByUserName(String userName) throws EdmException
    {
        return (EmployeeCard) DSApi.context().session().createQuery(
                "from " + EmployeeCard.class.getName() + " e where e.externalUser = ? and e.employmentEndDate is null order by e.employmentStartDate desc").setParameter(0, userName).setMaxResults(1).uniqueResult();
    }
}
