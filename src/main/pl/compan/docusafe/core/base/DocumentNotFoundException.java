package pl.compan.docusafe.core.base;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentNotFoundException.java,v 1.5 2006/02/20 15:42:17 lk Exp $
 */
public class DocumentNotFoundException extends EntityNotFoundException
{
    public DocumentNotFoundException(Long id)
    {
        super("Nie znaleziono dokumentu "+id, false);
    }
}
