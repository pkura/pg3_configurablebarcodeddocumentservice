package pl.compan.docusafe.core.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.db.criteria.*;
import pl.compan.docusafe.util.StringManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentChangelog.java,v 1.18 2010/01/22 14:27:50 pecet1 Exp $
 */
public enum DocumentChangelog
{
    CREATE_DOCUMENT("CREATE_DOCUMENT"),
    ADD_ATTACHMENT("ADD_ATTACHMENT"),
    ADD_ATTACHMENT_REVISION("ADD_ATTACHMENT_REVISION"),
    DEL_ATTACHMENT("DEL_ATTACHMENT"),
    DEL_DOCUMENT("DEL_DOCUMENT"),
    UNDEL_DOCUMENT("UNDEL_DOCUMENT"),
    SET_DOCUMENT_BOX("SET_DOCUMENT_BOX"),
    CHANGE_DOCUMENT_BOX("CHANGE_DOCUMENT_BOX"),
    REMOVE_DOCUMENT_BOX("REMOVE_DOCUMENT_BOX"),
    /**
     * Przyj�cie dekretacji.
     */
    WF_ACCEPT("WF_ACCEPT"),
    /**
     * Pojawienie si� pisma na li�cie zada�.
     */
    WF_ASSIGNED("WF_ASSIGNED"),
    ADD_REMARK("ADD_REMARK"),
    CLONED_DOCUMENT("CLONED_DOCUMENT"),
    NW_FILING("NW_FILING"),
    NW_CREATE_DRS("NW_CREATE_DRS"),
    WF_PUSH("WF_PUSH"),
    WF_FINISH("WF_FINISH"),
    WF_RETURN("WF_RETURN"),
    WF_START_MANUAL("WF_START_MANUAL"),
    SIGN_DOCUMENT("SIGN_DOCUMENT"),
    ACCEPT_DOCUMENT("ACCEPT_DOCUMENT"),
    UNARCHIVE_DOCUMENT("UNARCHIVE_DOCUMENT"),
    CHANGE_DOCKIND("CHANGE_DOCKIND");

    static StringManager sm ; //StringManager.getManager(Constants.Package);

    private static final Logger log = LoggerFactory.getLogger(DocumentChangelog.class);
    
    private String what;

    private static DocumentChangelog[] all;
    private static DocumentChangelog[] used;

    public static DocumentChangelog[] all()
    {
        if (all == null)
        {
            all = values().clone();
            Arrays.sort(all, new Comparator<DocumentChangelog>()
            {
                public int compare(DocumentChangelog o1, DocumentChangelog o2)
                {
                    return o1.getDescription().compareTo(o2.getDescription());
                }
            });
        }
        
        /**
         * Jesli mamy do czynienia z systemem bez extrasu nationwide to wyrzucamy z tablicy zwracanych enumow te przeznaczone dla aegon
         */
        try
        {
	        List<DocumentChangelog> newAll = new ArrayList<DocumentChangelog>(Arrays.asList(all));

            if(AvailabilityManager.isAvailable("electronicSignature.changelog.hide")){
                newAll.remove(SIGN_DOCUMENT);
            }

	        DocumentChangelog[] returnAll = new DocumentChangelog[newAll.size()];
	        return newAll.toArray(returnAll);
        }
        catch (Exception e)
        {
			log.debug(e.getMessage(),e);
		}
        
        return all;        
    }

    DocumentChangelog(String what)
    {
        this.what = what;
    }

    public String getWhat()
    {
        return what;
    }

    public String getDescription()
    {
        sm = GlobalPreferences.loadPropertiesFile(Constants.Package,null);
        return sm.getString("DocumentChangelog."+what);
    }

    public static DocumentChangelog[] used()
    {
        if (used == null)
        {
            all = values().clone();
            Arrays.sort(all, new Comparator<DocumentChangelog>()
            {
                public int compare(DocumentChangelog o1, DocumentChangelog o2)
                {
                    return o1.getDescription().compareTo(o2.getDescription());
                }
            });
        }
        List<DocumentChangelog> newAll = new ArrayList<DocumentChangelog>();
        try
        {
            DSApi.openContextIfNeeded();
            // proste zapytanie, wyci�gni�cie wbranych kolumn z warunkiem where
            NativeCriteria nc = DSApi.context().createNativeCriteria("ds_document_changelog", "d");
            nc.setProjection(NativeExps.projection()
                    // ustalenie kolumn
                    .addProjection("d.what").addAggregateProjection("d.what", NativeProjection.AggregateProjection.COUNT));


            // pobranie i wy�wietlenie wynik�w
            CriteriaResult cr = nc.criteriaResult();
            while (cr.next())
            {
                String what = cr.getString(0, "");
                for(DocumentChangelog log : all)
                {
                    if(log.what.equals(what))
                    {
                        newAll.add(log);
                    }
                }
            }
        }
        catch
            (Exception e)
        {

        }
        finally {
            DSApi._close();
        }
        DocumentChangelog[] returnAll = new DocumentChangelog[newAll.size()];
        return newAll.toArray(returnAll);
    }
}
