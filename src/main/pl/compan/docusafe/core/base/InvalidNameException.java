package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InvalidNameException.java,v 1.1 2004/05/16 20:10:23 administrator Exp $
 */
public class InvalidNameException extends EdmException
{
    public InvalidNameException(String message)
    {
        super(message);
    }

    public InvalidNameException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public InvalidNameException(Throwable cause)
    {
        super(cause);
    }
}
