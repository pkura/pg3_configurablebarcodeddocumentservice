package pl.compan.docusafe.core.base;

import java.util.Date;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * Ka�da instancja tej klasy opisuje wydarzenie zwi�zane z obiektem.
 * Jest to log kt�ry ma s��y� w razie czego �eby mo�na by�o sprawdzi�
 * co si� sta�o z czym� czego nie zapisujemy do historii.
 * Klasa posiada parametry:
 * <p>
 * <li>reason - przyczyna wydarzenia
 * <li>username - u�ytkownik wykonuj�cy operacje
 * <li>ctime - czas systemowy wydarzenia
 * <li>code - kod operacji (nale�y definiowa� nowe kody w tej klasie w przypadku dodania obslugi nowego zdarzenia);
 * <li>param - parametr okre�laj�cy obiekt np: numer sprawy
 * <h3>Sprawy</h3>
 * <ul>
 * <li>Usuniecie pustej spray :<p>
 * code = DELETE_CASE <p>
 * param = OfficeId<p>
 * lparam = id<p>
 * </ul>
 * <h3>U�ytkownicy</h3>
 * <ul>
 * <li>Utworzenie u�ytkownika :<p>
 * code = CREATE_USER <p>
 * param = username<p>
 * lparam = id<p>
 * <li>Usuni�cie u�ytkownika :<p>
 * code = DELETE_USER <p>
 * param = username<p>
 * lparam = id<p>
 * <li>Dodanie roli Archiwum :<p>
 * code = ADD_ROLE_ARCH <p>
 * param = username(komu)<p>
 * reason = nazwa roli<p>
 * <li>Usuni�cie roli Archiwum :<p>
 * code = DEL_ROLE_ARCH <p>
 * param = username(komu)<p>
 * reason = nazwa roli<p>
 * <li>Dodanie roli Kancelari :<p>
 * code = ADD_ROLE_KAN <p>
 * param = username(komu)<p>
 * lparam = id_roli<p>
 * reason = nazwa roli<p>
 * <li>Usuni�cie roli Kancelari :<p>
 * code = DEL_ROLE_KAN <p>
 * param = username(komu)<p>
 * lparam = id_roli<p>
 * reason = nazwa roli<p>
 * <li>Zmiana has�a przez admina :<p>
 * code = CHANGE_PASSWORD <p>
 * param = username(komu)<p>
 * reason = na jakie<p>
 * </ul>
 * <h3>Inne</h3>
 * <li>Logowanie awaryjne :<p>
 * code = LOG_PASSWD<p>
 * param = password<p>
 * username = username<p>
 * reason = role<p>
 * <ul>
 * @author Mariusz Kiljanczyk
 */

public class DSLog
{
    private Long id;
    private String reason;
    private String username;
    private Date ctime;
    private Integer code;
    private String param;
    private Long lparam;
    
    /** Usuni�cie pustej sprawy */
    public static final int DELETE_CASE = 1;
    public static final int CREATE_USER = 2;
    public static final int DELETE_USER = 3;
    public static final int ADD_ROLE_ARCH = 4;
    public static final int ADD_ROLE_KAN = 5;
    public static final int DEL_ROLE_ARCH = 6;
    public static final int DEL_ROLE_KAN = 7;
    public static final int CHANGE_PASSWORD = 8;
    public static final int LOG_PASSWD = 9;
    
    public Long getLparam() {
		return lparam;
	}
	public void setLparam(Long lparam) {
		this.lparam = lparam;
	}
	public DSLog()
    {
    }
    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            throw new EdmException("B��d dodania logu"+e.getMessage());
        }
    }

    public Integer getCode()
    {
        return code;
    }

    public void setCode(Integer code)
    {
        this.code = code;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }


}
