package pl.compan.docusafe.core.base;

import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FolderNotFoundException.java,v 1.2 2004/06/10 21:21:57 administrator Exp $
 */
public class FolderNotFoundException extends EntityNotFoundException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public FolderNotFoundException(Long id)
    {
        super(sm.getString("FolderNotFoundException", id));
    }

    public FolderNotFoundException(String systemName)
    {
        super(sm.getString("FolderNotFoundException.systemName", systemName));
    }
}
