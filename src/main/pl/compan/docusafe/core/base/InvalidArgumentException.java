package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;

/* User: Administrator, Date: 2005-07-12 10:25:55 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: InvalidArgumentException.java,v 1.1 2005/07/12 11:31:12 lk Exp $
 */
public class InvalidArgumentException extends EdmException
{
    public InvalidArgumentException(String message)
    {
        super(message);
    }

    public InvalidArgumentException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
