package pl.compan.docusafe.core.base.durables;

import java.io.Serializable;
import java.util.Date;

/**
 * �rodek trwa�y.
 * 
 * @author 
 */
public class Durable implements Serializable 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identyfikator
	 */
	private Long id;
	
	/**
	 * Numer inwentaryzacyjny
	 */
	private String inventoryNumber;
	
	/**
	 * Opis �rodka trwa�ego
	 */
	private String info;
	
	/**
	 * Numer seryjny
	 */
	private String serialNumber;
	
	/**
	 * Lokalizacja
	 */
	private Integer location;
	
	/**
	 * Dost�pno�� �rodka
	 */
	private Boolean available;
	
	/**
	 * Data utworzenia wpisu �rodka trwa�ego.
	 */
	private Date ctime;
	
	/**
	 * Konstruktor domy�lny
	 */
	public Durable() 
	{
		this.ctime = new Date();
	}
	
	/**
	 * Konstruktor tworzacy obiekt na podstawie przekazanych pol.
	 * 
	 * @param inventoryNumber
	 * @param info
	 * @param serialNumber
	 * @param location
	 * @param available
	 */
	public Durable(String inventoryNumber, String info, String serialNumber,
			Integer location, Boolean available) 
	{
		this.inventoryNumber = inventoryNumber;
		this.info = info;
		this.serialNumber = serialNumber;
		this.location = location;
		this.available = available;
		this.ctime = new Date();
	}



	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public String getInventoryNumber() 
	{
		return inventoryNumber;
	}
	
	public void setInventoryNumber(String inventoryNumber) 
	{
		this.inventoryNumber = inventoryNumber;
	}

	public String getInfo() 
	{
		return info;
	}

	public void setInfo(String info) 
	{
		this.info = info;
	}

	public String getSerialNumber() 
	{
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) 
	{
		this.serialNumber = serialNumber;
	}

	public Integer getLocation() 
	{
		return location;
	}

	public void setLocation(Integer location) 
	{
		this.location = location;
	}
	
	public Boolean getAvailable() 
	{
		return available;
	}
	
	public void setAvailable(Boolean available) 
	{
		this.available = available;
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	
}
