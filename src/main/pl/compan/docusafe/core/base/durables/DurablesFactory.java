package pl.compan.docusafe.core.base.durables;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

public class DurablesFactory 
{
	/**
	 * Singleton
	 */
	private static DurablesFactory instance;  
	
	/**
	 * Singleton
	 */
	private DurablesFactory() 
	{	
	}
	
	public static DurablesFactory getInstance()
	{
		if (instance != null)
			return instance;
		
		instance = new DurablesFactory();
		return instance;
	}
	
	/**
	 * Zwraca liste srodkow trwalych na podstawie przekazanego obiektu Durable z prametrami wyszukiwania.
	 * 
	 * @param durable
	 * @return
	 * @throws EdmException
	 */
	public List<Durable> getDurables(Durable durable) throws EdmException
	{
		Criteria criteria = buildCriteria(durable);
		return criteria.list();
	}
	
	/**
	 * Zwraca liste srodkow trwalych na podstawie przekazanego obiektu Durable z prametrami wyszukiwania.
	 * Bierze pod uwage ilosc i zakres wynikow.
	 * 
	 * @param durable
	 * @param offset
	 * @param maxResults
	 * @return
	 * @throws EdmException
	 */
	public List<Durable> getDurables(Durable durable, final int offset, final int maxResults) throws EdmException
	{
		Criteria criteria = buildCriteria(durable);
		criteria.setMaxResults(maxResults)
				.setFirstResult(offset);
		return criteria.list();
	}
	
	/**
	 * Zwraca wszystkie �rodki trwa�e z bazy danych
	 * 
	 * @return
	 * @throws EdmException
	 */
	public List<Durable> getAllDurables() throws EdmException
	{
		Criteria criteria = buildCriteria(null);
		return criteria.list();
	}
	
	/**
	 * Zlicza liczbe rekordow na podstawie parametrow wyszukiwania z obiektu durable
	 * 
	 * @param durable
	 * @return
	 * @throws EdmException
	 */
	public int getCountDurables(Durable durable) throws EdmException
	{
		Criteria criteria = buildCriteria(durable);
		criteria.setProjection(Projections.rowCount());
		Long count = (Long)criteria.uniqueResult();
		return count.intValue();
	}
	
	/**
	 * Usuwa srodki trwale o wskazanych identyfikatorach
	 * 
	 * @param ids
	 * @throws EdmException
	 */
	public void deleteDurablesByIds(Long[] ids) throws EdmException
	{
		DSApi.context().session().createQuery(
				"delete from " + Durable.class.getName() + " d where d.id in (:ids)")
				.setParameterList("ids", ids)
			.executeUpdate();
	}
	
	/**
	 * Wyszukuje �rodek trwa�y o podanym ID.
	 * @param id
	 * @return
	 * @throws EdmException 
	 */
	public Durable find(Long id) throws EdmException {
		return (Durable)Finder.find(Durable.class, id);
	}
	
	/**
	 * Buduje obiekt zapytania.
	 * 
	 * @param durable
	 * @return
	 * @throws EdmException
	 */
	private Criteria buildCriteria(Durable durable) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(Durable.class);
		// budowanie zapytania
		if (durable == null)
			return criteria;
		
		// numer inwentaryzacyjny
		if (StringUtils.isNotEmpty(durable.getInventoryNumber()))
			criteria.add(Restrictions.like("inventoryNumber", durable.getInventoryNumber() + "%"));
		
		// numer seryjny
		if (StringUtils.isNotEmpty(durable.getSerialNumber()))
			criteria.add(Restrictions.like("serialNumber", durable.getSerialNumber() + "%"));
		
		// lokalizacja
		if (durable.getLocation() != null && durable.getLocation() > 0)
			criteria.add(Restrictions.eq("location", durable.getLocation()));
		
		return criteria;
	}
}
