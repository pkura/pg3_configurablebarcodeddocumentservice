package pl.compan.docusafe.core.base;

import ognl.OgnlException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EdmOgnlException.java,v 1.2 2006/02/20 15:42:17 lk Exp $
 */
public class EdmOgnlException extends EdmException
{
    private static final Log log = LogFactory.getLog(EdmOgnlException.class);

    public EdmOgnlException(OgnlException e, String expression)
    {
        super("B��d podczas interpretacji skryptu OGNL, skontaktuj si� z administratorem systemu", e);
        log.error(expression, e);
    }
}
