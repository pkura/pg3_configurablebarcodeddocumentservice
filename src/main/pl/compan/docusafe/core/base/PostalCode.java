package pl.compan.docusafe.core.base;

import java.util.List;
import java.util.Locale;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * Klasa kod�w pocztowych
 * @author Leon
 *
 */

public class PostalCode
{
    private static final Logger log = LoggerFactory.getLogger(PostalCode.class);
    
    private Long   id;
    private Integer code_from ;
    private Integer code_to ;
    private static String city;
    private String province;
    
    
    StringManager sm = 
        GlobalPreferences.loadPropertiesFile(PostalCode.class.getPackage().getName(),null);
    
    public PostalCode()
    {
    }

    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania regionu "+e.getMessage());
            throw new EdmException("Blad dodania regionu"+e.getMessage());
        }
    }

    public static PostalCode find(Long id) throws EdmException
    {
        StringManager smL = GlobalPreferences.loadPropertiesFile(PostalCode.class.getPackage().getName(),null);
        PostalCode inst = DSApi.getObject(PostalCode.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono kodu pocztowego "+id);
        else 
            return inst;
    }
    
    public static List<PostalCode> findByLocation(String location)
    {
    	try
    	{
    		List<PostalCode> codes = DSApi.context().session().createCriteria(PostalCode.class)
    			.add(Restrictions.like("city", location).ignoreCase())
    			.list();
    		
    		if(codes == null)
    		{
    			throw new EdmException("Blad wyszukiwania miejscowosci "+location);
    		}
    		else
    		{
    			return codes;
    		}
    	}
    	catch (Exception e) 
    	{
    		log.trace(e.getMessage(),e);
			return null;
		}
    }
    
    public static PostalCode findByCode(Integer code)
    {
    	try
    	{
    		List<PostalCode> codes = DSApi.context().session().createCriteria(PostalCode.class)
    			.add(Restrictions.le("code_from", code))
    			.add(Restrictions.ge("code_to", code))
    			.list();
    		
    		if(codes != null && !codes.isEmpty())
    		{
    			return codes.get(0);
    		}
    		else
    		{
    			throw new EdmException("Blad wyszukania kodu "+code);
    		}
    	}
    	catch (Exception e) 
    	{
    		log.trace(e.getMessage(),e);
    		return null;
		}
    }
    
    public static String getFormattedCode(Integer code) {
    	String str = code.toString();
		str = StringUtils.leftPad(str, 5, '0');
		str = str.substring(0, 2) + '-' + str.substring(2);
    	
    	return str;
    }
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

	public Integer getCode_from() {
		return code_from;
	}

	public void setCode_from(Integer code_from) {
		this.code_from = code_from;
	}

	public Integer getCode_to() {
		return code_to;
	}

	public void setCode_to(Integer code_to) {
		this.code_to = code_to;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

}