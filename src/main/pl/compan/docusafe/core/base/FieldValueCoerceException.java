package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;

/* User: Administrator, Date: 2005-06-15 14:48:30 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: FieldValueCoerceException.java,v 1.1 2005/06/15 16:34:54 lk Exp $
 */
public class FieldValueCoerceException extends EdmException
{
    public FieldValueCoerceException(String message)
    {
        super(message);
    }
}
