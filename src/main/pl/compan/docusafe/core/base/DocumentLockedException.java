package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.StringManager;

import java.util.Date;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentLockedException.java,v 1.1 2004/06/27 20:56:52 administrator Exp $
 */
public class DocumentLockedException extends EdmException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    private Long id;
    private String username;
    private Date expiration;

    public DocumentLockedException(Long id, String username, Date expiration)
    {
        super(sm.getString("DocumentLockedException", id, username, expiration));
        this.id = id;
        this.username = username;
        this.expiration = expiration;
    }

    public Long getId()
    {
        return id;
    }

    public String getUsername()
    {
        return username;
    }

    public Date getExpiration()
    {
        return expiration;
    }
}
