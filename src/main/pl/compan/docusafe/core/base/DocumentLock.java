package pl.compan.docusafe.core.base;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;

import java.util.Date;

/**
 * Istniej� dwa rodzaje blokad dokumentu: <ul>
 * <li>Dzielona do odczytu (READ). U�ytkownik, kt�ry za�o�y� tak�
 *  blokad� mo�e tylko czyta� dokument, inni u�ytkownicy mog� r�wnie�
 *  za�o�y� na pliku blokad� typu READ, ale �aden u�ytkownik nie mo�e
 *  za�o�y� blokady typu WRITE, dop�ki istnieje chocia� jedna blokada
 *  typu READ. Blokady typu READ nie mo�na za�o�y�, je�eli istnieje chocia�
 *  jedna blokada typu WRITE.
 * <li>Wy��czna do zapisu (WRITE). Mo�e by� za�o�ona tylko, je�eli na
 *  pliku nie ma �adnej innej blokady. Inni u�ytkownicy nie mog� za�o�y�
 *  na pliku blokady dowolnego typu, je�eli plik posiada blokad� typu WRITE.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentLock.java,v 1.7 2006/12/07 13:50:59 mmanski Exp $
 */
public class DocumentLock implements java.io.Serializable
{
    private Long documentId;
    private String username;
    /**
     * Data wyga�ni�cia blokady. Pole obowi�zkowe.
     */
    private Date expiration;
    /**
     * Rodzaj blokady.
     * @see DocumentLockMode
     */
    private DocumentLockMode mode;

    /**
     * Konstruktor u�ywany przez Hibernate.
     */
    DocumentLock()
    {
    }

    DocumentLock(Long documentId, Date expiration, DocumentLockMode mode)
        throws EdmException
    {
        this.username = DSApi.context().getPrincipalName();
        this.documentId = documentId;
        this.expiration = expiration;
        this.mode = mode;
    }

    public void create() throws EdmException
    {
        Persister.create(this);
    }

    void delete() throws EdmException
    {
        Persister.delete(this);
    }

    public void release() throws EdmException
    {
        if (!DSApi.context().isAdmin() &&
            !DSApi.context().getPrincipalName().equals(username))
            throw new EdmException("Tylko u�ytkownik zak�adaj�cy blokad� mo�e j� zwolni�");

        delete();
        try
        {
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public String getUsername()
    {
        return username;
    }

    private void setUsername(String username)
    {
        this.username = username;
    }

    public Date getExpiration()
    {
        return expiration;
    }

    public void setExpiration(Date expiration)
    {
        this.expiration = expiration;
    }

    public DocumentLockMode getMode()
    {
        return mode;
    }

    public void setMode(DocumentLockMode mode)
    {
        this.mode = mode;
    }

    public String toString()
    {
        return getClass().getName()+"[id="+documentId+" username="+username+
            " mode="+mode+" expiration="+expiration+"]";
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof DocumentLock)) return false;

        final DocumentLock documentLock = (DocumentLock) o;

        if (documentId != null ? !documentId.equals(documentLock.documentId) : documentLock.documentId != null) return false;
        if (mode != null ? !mode.equals(documentLock.mode) : documentLock.mode != null) return false;
        if (username != null ? !username.equals(documentLock.username) : documentLock.username != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (documentId != null ? documentId.hashCode() : 0);
        result = 29 * result + (username != null ? username.hashCode() : 0);
        result = 29 * result + (mode != null ? mode.hashCode() : 0);
        return result;
    }
}
