package pl.compan.docusafe.core.base.bilings;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.Pair;

/**
 * Klasa factory dla biling�w
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class BilingsFactory 
{
	private final static Logger log = LoggerFactory.getLogger(BilingsFactory.class);
	/**Zwraca username wlasciciela telefonu*/
	public static String getUsernameFromPhone(String phoneNumberFrom) 
	{
		System.out.println(phoneNumberFrom);
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement("select name from ds_user where DELETED = 0 and (PHONE_NUM = ? or MOBILE_PHONE_NUM = ?)");
			ps.setString(1, phoneNumberFrom);
			ps.setString(2, phoneNumberFrom);
			rs = ps.executeQuery();
			if(rs.next())
				return rs.getString(1);
    	}
    	catch (Exception e)
    	{
			log.error("",e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DbUtils.closeQuietly(ps);
    	}
    	return null;
	}
	
	public static Biling getBilingByPhoneTime(String phoneNumberFrom,String phoneNumberTo, Date time)throws EdmException
	{
	//	System.out.println("SPRAWDZAM "+phoneNumberFrom+" "+phoneNumberTo+" "+DateUtils.formatCommonDateTime(time));
		
		Criteria c = DSApi.context().session().createCriteria(Biling.class);
		c.add(Restrictions.eq("phoneNumberFrom", phoneNumberFrom));
		c.add(Restrictions.eq("phoneNumberTo", phoneNumberTo));
		c.add(Restrictions.eq("dateTime", time));
		List<Biling> list = (List<Biling>) c.list();
		if(list != null && list.size() > 0)
			return list.get(0);
		else
			return null;
	}
	
	
	public static UserPhoneNumber findUserPhoneNumber(String phoneNumber,String username) 
	{
    	PreparedStatement ps = null; 
    	ResultSet rs = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement("select id from ds_user_phone_number where phone_number = ? and username = ?");
			ps.setString(1, phoneNumber);
			ps.setString(2, username);
			rs = ps.executeQuery();
			if(rs.next())
			{
				Long id = rs.getLong(1);
				UserPhoneNumber upn = DSApi.context().load(UserPhoneNumber.class, id);
				return upn;
			}
    	}
    	catch (Exception e)
    	{
			log.error("",e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DbUtils.closeQuietly(ps);
    	}
    	return null;
	}

	public static String getCallType(String phoneNumberTo,String username,String defaultType) 
	{
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement("select count(1) from ds_user_phone_number where phone_number = ? and username = ?");
			ps.setString(1, phoneNumberTo);
			ps.setString(2, username);
			rs = ps.executeQuery();
			rs.next();
			if(rs.getInt(1) > 0)
				return "PRIVATE";
    	}
    	catch (Exception e)
    	{
			log.error("",e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DbUtils.closeQuietly(ps);
    	}
    	return defaultType;
	}
	
	/**
	 * Domy�lne pole dla sortowania
	 */
	private static final String DEFAULT_SORT_FIELD = "dateTime";
	
	/**
	 * Maksymalna liczba rezultat�w
	 */
	public static final int MAX_RESULTS = 20;
	
	/**
	 * Zwraca list� bilingow� z podanego numeru strony
	 * 
	 * @param pageNumber
	 * @return
	 * @throws EdmException
	 */
	public static List<Biling> getBilings(int pageNumber) throws EdmException
	{
		return getBilings(null, null, pageNumber, MAX_RESULTS, DEFAULT_SORT_FIELD, false);
	}
	
	/**
	 * Zwraca list� bilingow� z podanego numeru strony i okre�lonej liczby rezultat�w
	 * 
	 * @param pageNumber
	 * @param maxResults
	 * @return
	 * @throws EdmException
	 */
	public static List<Biling> getBilings(int pageNumber, int maxResults) throws EdmException
	{
		return getBilings(null, null, pageNumber, maxResults, DEFAULT_SORT_FIELD, false);
	}
	
	/**
	 * Zwraca list� bilingow� z podanych parametr�w
	 * 
	 * @param biling
	 * @param pageNumber
	 * @param maxResults
	 * @return
	 * @throws EdmException
	 */
	public static List<Biling> getBilings(Biling biling, int pageNumber, int maxResults) throws EdmException
	{
		return getBilings(biling, null, pageNumber, maxResults, DEFAULT_SORT_FIELD, false);
	}
	
	/**
	 * Zwraca list� bilingow� z podanych parametr�w
	 * 
	 * @param timePeriod
	 * @param pageNumber
	 * @param maxResults
	 * @return
	 * @throws EdmException
	 */
	public static List<Biling> getBilings(Pair<Date> timePeriod, int pageNumber, int maxResults) throws EdmException
	{
		return getBilings(null, timePeriod, pageNumber, maxResults, DEFAULT_SORT_FIELD, false);
	}
	
	/**
	 * Zwaraca list� biling�w wg podanych parametr�w
	 * 
	 * @param biling
	 * @param pageNumber
	 * @param maxResults
	 * @return
	 * @throws EdmException
	 */
	public static List<Biling> getBilings(Biling biling, Pair<Date> timePeriod, int pageNumber, 
			int maxResults, String sortField, boolean ascending)
			throws EdmException
	{
		List<Biling> bilings;
		Map<String, Object[]> paramsMap = buildQueryParamsMap(biling, timePeriod);
		if (paramsMap == null)
		{
			// brak parametr�w wyszukiwania
			bilings = DSApi.context().session().createQuery(
					"from " + Biling.class.getName() + " b order by b." + sortField + (ascending ? " asc" : " desc"))
					.setFirstResult(pageNumber)
					.setMaxResults(maxResults)
					.list();
		}
		else 
		{
			String where = createWhereClause(paramsMap);
			Query query = DSApi.context().session().createQuery(
					"from " + Biling.class.getName() + " b " + where + " order by b." + sortField + (ascending ? " asc" : " desc"));
			// ustawienie parametr�w zapytania
			for (Object[] param : paramsMap.values())
			{
				query.setParameter((String) param[0], param[1]);
			}
			
			query.setFirstResult(pageNumber);
			query.setMaxResults(maxResults);
			bilings = query.list();
		}
		
		return bilings;
	}
	
	/**
	 * Zwaraca list� sume kosztow
	 * 
	 * @param biling
	 * @param pageNumber
	 * @param maxResults
	 * @return
	 * @throws EdmException
	 */
	public static Double getBilingsSum(Biling biling, Pair<Date> timePeriod, int pageNumber, 
			int maxResults, String sortField, boolean ascending)
			throws EdmException
	{
		List<Biling> bilings;
		Map<String, Object[]> paramsMap = buildQueryParamsMap(biling, timePeriod);
		if (paramsMap == null)
		{
			// brak parametr�w wyszukiwania
			return (Double)  DSApi.context().session().createQuery("select SUM(b.netto) from " + Biling.class.getName() + " b ").uniqueResult();
		}
		else 
		{
			String where = createWhereClause(paramsMap);
			Query query = DSApi.context().session().createQuery("select SUM(b.netto) from " + Biling.class.getName() + " b " + where );
			// ustawienie parametr�w zapytania
			for (Object[] param : paramsMap.values())
			{
				query.setParameter((String) param[0], param[1]);
			}
			return (Double) query.uniqueResult();
		}
	}
	
	/**
	 * Zwraca ilo�� wynik�w zapytania
	 * 
	 * @param biling
	 * @throws EdmException
	 */
	public static int getCount(Biling biling) throws EdmException
	{
		return getCount(biling, null);
	}
	
	/**
	 * Zwraca ilosc polaczen z dla podanego numeru zestweinia 
	 * @throws SQLException 
	 */
	public static int getCallCount(String bilingNumber) throws EdmException, SQLException
	{
		PreparedStatement  ps= null;
		ResultSet rs = null;
		int result = 0;
		try
		{
			ps = DSApi.context().prepareStatement("select count(id) from DS_BILING where bilingNumber = ? ");
			ps.setString(1, bilingNumber);
			rs = ps.executeQuery();
			rs.next();
			result = rs.getInt(1);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
		return result;
	}
	
	
	/**
	 * Zwraca ilo�� wynik�w zapytania
	 * 
	 * @param biling
	 * @param timePeriod
	 * @throws EdmException
	 */
	public static int getCount(Biling biling, Pair<Date> timePeriod) throws EdmException
	{
		int result = 0;
		Map<String, Object[]> paramsMap = buildQueryParamsMap(biling, timePeriod);
		if (paramsMap == null)
		{
			// brak parametr�w wyszukiwania
			Long res = (Long) DSApi.context().session().createQuery(
					"select count(b.id) from " + Biling.class.getName() + " b")
					.uniqueResult();
			result = Integer.parseInt(res + "");
		}
		else
		{
			String where = createWhereClause(paramsMap);
			Query query = DSApi.context().session().createQuery(
					"select count(b.id) from " + Biling.class.getName() + " b " + where);
			// ustawienie parametr�w zapytania
			for (Object[] param : paramsMap.values())
			{
				query.setParameter((String) param[0], param[1]);
			}
			Long res = (Long) query.uniqueResult();
			result = Integer.parseInt(res + "");
		}
		
		return result;
	}
	
	/**
	 * Buduje map� parametr�w wyszukiwania
	 * 
	 * @param biling
	 * @return
	 */
	private static Map<String, Object[]> buildQueryParamsMap(Biling biling, Pair<Date> timePeriod)
	{
		if (biling == null)
			return null;
		
		Map<String, Object[]> paramsMap = new HashMap<String, Object[]>();
		
		// nazwa u�ytkownika
		if (StringUtils.isNotEmpty(biling.getUsername()))
			paramsMap.put("b.username like :USERNAME", new Object[] {"USERNAME", biling.getUsername() + "%"});
		// typ
		if (StringUtils.isNotEmpty(biling.getType()))
			paramsMap.put("b.type = :TYPE", new Object[] {"TYPE", biling.getType()});
		// numer telefonu
		if (StringUtils.isNotEmpty(biling.getPhoneNumberFrom()))
			paramsMap.put("b.phoneNumberFrom like :PHONENUMBER", 
					new Object[] {"PHONENUMBER", "%" + formatPhoneNumber(biling.getPhoneNumberFrom()) + "%"});
		// numer telefonu do
		if (StringUtils.isNotEmpty(biling.getPhoneNumberTo()))
			paramsMap.put("b.phoneNumberTo like :PHONENUMBERTO", 
					new Object[] {"PHONENUMBERTO", "%" + formatPhoneNumber(biling.getPhoneNumberTo()) + "%"});
		//numer zestawienia
		if (StringUtils.isNotEmpty(biling.getBilingNumber()))
			paramsMap.put("b.BilingNumber = :BILINGNUMBER", 
					new Object[] {"BILINGNUMBER", biling.getBilingNumber()});
		// Kierunek
		if (StringUtils.isNotEmpty(biling.getDirection()))
			paramsMap.put("b.direction like :DIRECTION", 
					new Object[] {"DIRECTION", StringUtils.trim(biling.getDirection()) + "%"});
		// zakres dat
		if (timePeriod != null)
		{
			if (timePeriod.getFirst() != null)
				paramsMap.put("b.dateTime > :STARTDATE", new Object[] {"STARTDATE", timePeriod.getFirst()});
			if (timePeriod.getSecond() != null)
			{
				// ustawienie czasu na koniec dnia, aby mo�liwe by�o zwr�cenie rozm�w z tego dnia
				Calendar calendar = GregorianCalendar.getInstance();
				calendar.setTime(timePeriod.getSecond());
				calendar.set(Calendar.HOUR_OF_DAY, calendar.getMaximum(Calendar.HOUR_OF_DAY));
				calendar.set(Calendar.MINUTE, calendar.getMaximum(Calendar.MINUTE));
				calendar.set(Calendar.SECOND, calendar.getMaximum(Calendar.SECOND));
				paramsMap.put("b.dateTime < :ENDDATE", new Object[] {"ENDDATE", calendar.getTime()});
			}
		}
		
		return paramsMap;
	}
	
	/**
	 * Tworzy klauzul� where zapytania
	 * 
	 * @param paramsMap
	 * @return
	 */
	private static String createWhereClause(Map<String, Object[]> paramsMap)
	{
		StringBuffer hql = new StringBuffer();
		Object[] keys = paramsMap.keySet().toArray();
		// p�tla buduj�ca ci�g zapytania
		for (int i = 0; i < keys.length; i++)
		{
			hql.append(keys[i]);
			if (i + 1 < keys.length)
				hql.append(" and ");
		}
		
		return (!hql.toString().equals("")) ? " where " + hql.toString() : "";
	}
	
	/**
	 * Formatuje numer telefonu
	 * 
	 * @param phoneNumber
	 * @return
	 */
	private static String formatPhoneNumber(String phoneNumber)
	{
		phoneNumber = StringUtils.trim(phoneNumber);
		if (phoneNumber.matches("[a-zA-Z]+"))
			// niestandardowy ci�g je�li zawiera litery
			return phoneNumber;
		
		// usuni�cie wszelkich znak�w nie b�d�cych cyfr�
		return phoneNumber.replaceAll("[^0-9]+", "");
	}

}
