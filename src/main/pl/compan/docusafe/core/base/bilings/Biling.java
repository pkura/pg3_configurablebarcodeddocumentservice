package pl.compan.docusafe.core.base.bilings;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

import jtpkg.cl;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.ilpol.LeasingObjectType;

/**
 * Klasa encji odwzorowuj�ca tabelk� DS_BILING
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class Biling implements Serializable 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Typy telefon�w
	 */
	public enum PhoneTypes {
		MOBILE ("kom�rkowy"),
		PRIVATE ("prywatny"),
		OFFICE ("s�u�bowy");
		
		private final String name;
		
		PhoneTypes(String name) {
			this.name = name;
		}
		
		public String getName() {
			return this.name;
		}
	}
	
	public String toString()
	{
		return phoneNumberFrom +" >> "+ phoneNumberTo + "("+timePeriod+"s)";
	}
	
	/**
	 * Identyfikator
	 */
	private Long id;
	
	/**
	 * Nazwa u�ytkownika
	 */
	private String username;
	
	/**
	 * Typ: s�u�bowy, kom�rkowy, prywatny
	 */
	private String type;
	
	/**
	 * Numer telefonu, z kt�rego dzwnoniono
	 */
	private String phoneNumberFrom;
	
	/**
	 * Numer telefonu, na kt�ry dzwoniono
	 */
	private String phoneNumberTo;
	
	/**
	 * Czas trwania rozmowy w sekundach
	 */
	private Integer timePeriod;
	
	/**
	 * Data i czas rozmowy
	 */
	private Date dateTime;
	
	/**
	 * Kwota netto
	 */
	private Double netto;
	
	/**
	 * Liczba impuls�w
	 */
	private Integer impulses;
	
	/**
	 * Kierunek
	 */
	private String direction;
	
	/**
	 * Ilo�� danych GPRS
	 */
	private Integer GPRS;
	/***
	 * Numer bilingu/faktury
	 */
	private String bilingNumber;
	
	/**
	 * Domy�lny konstruktor
	 */
	public Biling() 
	{
	}
	
	/**
	 * Konstruktor ustawiaj�cy pola
	 * 
	 * @param username
	 * @param type
	 * @param phoneNumberFrom
	 * @param phoneNumberTo
	 * @param timePeriod
	 * @param dateTime
	 * @param netto
	 * @param impulses
	 * @param direction
	 * @param gPRS
	 */
	public Biling(String username, String type, String phoneNumberFrom,
			String phoneNumberTo, int timePeriod, Date dateTime, double netto,
			int impulses, String direction, int gPRS) 
	{
		super();
		this.username = username;
		this.type = type;
		this.phoneNumberFrom = phoneNumberFrom;
		this.phoneNumberTo = phoneNumberTo;
		this.timePeriod = timePeriod;
		this.dateTime = dateTime;
		this.netto = netto;
		this.impulses = impulses;
		this.direction = direction;
		GPRS = gPRS;
	}

    public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmException("Blad usuniecia wpisu. "+e.getMessage());
        }
    }
    

	public void create() throws Exception, EdmException
	{
		duplicateCheck();
        DSApi.context().session().save(this);
        DSApi.context().session().flush();
	}
	
	private void duplicateCheck() throws EdmException
	{
        Criteria c = DSApi.context().session().createCriteria(Biling.class);
        c.add(Restrictions.eq("dateTime",this.getDateTime()));
        c.add(Restrictions.eq("direction",this.getDirection()));
        c.add(Restrictions.eq("netto",this.getNetto()));
        c.add(Restrictions.eq("phoneNumberFrom",this.getPhoneNumberFrom()));
        c.add(Restrictions.eq("phoneNumberTo",this.getPhoneNumberTo()));
        c.add(Restrictions.eq("timePeriod",this.getTimePeriod()));
        c.add(Restrictions.eq("username",this.getUsername()));
        c.add(Restrictions.eq("type",this.getType()));
        c.add(Restrictions.eq("impulses",this.getImpulses()));
        c.add(Restrictions.eq("bilingNumber",this.getBilingNumber()));
        c.add(Restrictions.eq("GPRS",this.getGPRS()));
        List<Biling> clist = (List<Biling>) c.list();
        if(clist != null && clist.size() > 0)
        	throw new EdmException("Istnieje juz taki wpis");
	}

	
	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public String getUsername() 
	{
		return username;
	}

	public void setUsername(String username) 
	{
		this.username = username;
	}

	public String getType() 
	{
		return type;
	}

	public void setType(String type) 
	{
		for (PhoneTypes phoneType : PhoneTypes.values())
		{
			if (phoneType.name().equals(type))
			{
				this.type = type;
				return;
			}
			else if (phoneType.getName().equals(type))
			{
				this.type = phoneType.getName();
				return;
			}
		}
		
		this.type = type;
	}

	public String getPhoneNumberFrom() 
	{
		return phoneNumberFrom;
	}

	public void setPhoneNumberFrom(String phoneNumberFrom) 
	{
		this.phoneNumberFrom = phoneNumberFrom;
	}

	public String getPhoneNumberTo() 
	{
		return phoneNumberTo;
	}

	public void setPhoneNumberTo(String phoneNumberTo) 
	{
		this.phoneNumberTo = phoneNumberTo;
	}

	public int getTimePeriod() 
	{
		return timePeriod;
	}

	public void setTimePeriod(int timePeriod) 
	{
		this.timePeriod = timePeriod;
	}

	public Date getDateTime() 
	{
		return dateTime;
	}

	public void setDateTime(Date dateTime) 
	{
		this.dateTime = dateTime;
	}

	public double getNetto() 
	{
		return netto;
	}

	public void setNetto(double netto) 
	{
		this.netto = netto;
	}

	public int getImpulses() 
	{
		return impulses;
	}

	public void setImpulses(int impulses) 
	{
		this.impulses = impulses;
	}

	public String getDirection() 
	{
		return direction;
	}

	public void setDirection(String direction) 
	{
		this.direction = direction;
	}

	public Integer getGPRS() 
	{
		return GPRS;
	}

	public void setGPRS(Integer gPRS) 
	{
		GPRS = gPRS;
	}

	public String getBilingNumber() {
		return bilingNumber;
	}

	public void setBilingNumber(String bilingNumber) {
		this.bilingNumber = bilingNumber;
	}
}
