package pl.compan.docusafe.core.base.bilings;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa encji odworowująca tabelkę DS_USER_PHONE
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class UserPhoneNumber implements Serializable
{
	private final static Logger log = LoggerFactory.getLogger(UserPhoneNumber.class);
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identyfikator
	 */
	private Long id;
	
	/**
	 * Numer telefonu
	 */
	private String phoneNumber;
	
	/**
	 * Nazwa użytkownika
	 */
	private String username;
	
	/**
	 * Domyślny konstruktor
	 */
	public UserPhoneNumber() 
	{
	}

	/**
	 * Konstruktor ustawiający pola
	 * 
	 * @param phoneNumber
	 * @param username
	 */
	public UserPhoneNumber(String phoneNumber, String username) 
	{
		super();
		this.phoneNumber = phoneNumber;
		this.username = username;
	}

    /**
     * Usuwa wpis 
     * @throws EdmException
     */
    public void delete() throws EdmException {
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia wpisu");
        }
    }


	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public String getPhoneNumber() 
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) 
	{
		this.phoneNumber = phoneNumber;
	}

	public String getUsername() 
	{
		return username;
	}

	public void setUsername(String username) 
	{
		this.username = username;
	}

	public static List<UserPhoneNumber> search(String username, String phoneNumber) throws EdmException
	{
        Criteria c = DSApi.context().session().createCriteria(UserPhoneNumber.class);
    	if(!StringUtils.isEmpty(username))
    		c.add(Restrictions.eq("username",username));
    	if(!StringUtils.isEmpty(phoneNumber))
    		c.add(Restrictions.eq("phoneNumber",phoneNumber));
        return c.list();
	}

	public static UserPhoneNumber find(Long id) throws EdmException
	{
		return DSApi.context().load(UserPhoneNumber.class, id);
	}
	
	
}
