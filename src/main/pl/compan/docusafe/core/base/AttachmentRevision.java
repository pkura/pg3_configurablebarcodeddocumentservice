package pl.compan.docusafe.core.base;

import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sun.media.jai.opimage.CodecRIFUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.IntegrityVerificationException;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.integration.DocumentConverter;
import pl.compan.docusafe.lucene.IndexStatus;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.StreamUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.viewserver.ViewerAction;

import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.codec.BmpImage;
import com.lowagie.text.pdf.codec.TiffImage;

import javax.persistence.NonUniqueResultException;

/**
 * Wersja za��cznika. Instancje tej klasy zawieraj� za��czniki
 * binarne.
 * <p>
 * Hibernate nie zarz�dza polem typu BLOB, w kt�rym trzymana jest
 * tre�� wersji za��cznika. Dane zapisywane s� w bazie w metodzie
 * {@link Attachment#createRevision(java.io.File)}. Jest to wymuszone faktem,
 * �e rozmaite bazy danych i ich sterowniki JDBC bardzo r�nie
 * obs�uguj� dane typu BLOB, niekt�re wymagaj� korzystania ze
 * swoich w�asnych API nie b�d�cych cz�ci� standardu JDBC (np.
 * PostgreSQL).
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AttachmentRevision.java,v 1.55 2010/06/22 11:02:07 tomekl Exp $
 */
public abstract class AttachmentRevision implements Lifecycle, Cloneable
{
    protected static final StringManager sm = StringManager.getManager(Constants.Package);
    protected static Logger log = LoggerFactory.getLogger(AttachmentRevision.class);
    
    public static final String JPEG = "jpeg";
    public static final String PNG = "png";
    public static final Integer FILE_ON_DISC = 1;
    public static final Integer FILE_IN_DATABASE = 0;

    protected Long id;
    protected Integer revision;
    protected Date ctime;
    protected String author;
    protected Attachment attachment;
    protected String originalFilename;
    protected Integer size;
    protected String mime;
    protected String lparam;
    protected Long wparam;
    protected Integer onDisc;
    /**
     * Identyfikator obiektu AttachmentRevision, z kt�rego powinna by�
     * pobrana tre�� za��cznika.
     */
    protected Long contentRefId;
    protected Boolean editable;
    protected String annotations;
    
    protected String verificationCode;

    private int indexStatusId = IndexStatus.NOT_INDEXED.id;


    public final AttachmentRevision cloneObject() throws EdmException
    {
        try
        {
            AttachmentRevision clone = (AttachmentRevision) super.clone();
            clone.duplicate();
            return clone;
        }
        catch (CloneNotSupportedException e)
        {
            throw new Error(e);
        }
    }
    
    public static String getPathToDecompressedImage(Long revisionId , String format, boolean fax, int page) throws EdmException, IOException
    {
        //przedostatnia czesc nazwy pliku (.0.) oznacza, ze jest to jpeg.
        //w przyszlosci moze bedziemy konwertowac tiffa do png czy innego formatu i trzeba bedzie
        //rozroznic np. trzecia strone Tiffa jako jpg i jako png. Zero oznacza jpg jedynka oznacza png
                    
        //ostatnia czesc nazwy pliku oznacza, czy jest to fax czy nie.
        //Czasami trzymamy zalaczniki na dysku, a nie w bazie danych, ale
        //tylko te z tabeli ds_attachment_revision. Jesli fax==true, to zalacznik jest
        //w tabeli ds_retained_attachment. Nie mozna wiec kierowac sie tylko nazwa z
        //Attachment.getPath(), bo ona zwraca nazwe ale wzgledem id z tabeli ds_attachment....
        //Skoro trzeba jakos rozrozniac to jesli na koncu jest 0 to nie jest to fax, a jak 1 to fax.
             
    	String path = ViewerAction.getDecompressedPath();
        AttachmentRevision ar = AttachmentRevision.find(revisionId);
        int len = path.length();
                    
        String pathElem;
        if(format.equals(JPEG))
            pathElem = ".0.";
        else if(format.equals(PNG))
            pathElem = ".1.";
        else throw new EdmException("unknown format"); 
                       
        path +="//"+ ar.getFileName()+"."+String.valueOf(page)+pathElem+((fax)?"1":"0");
        
        return path;
    }
    
    // TODO: nie uwzgl�dnia sytuacji, gdy za��cznik ma pole contentdata = NULL
    protected void duplicate() throws EdmException
    {
        Long oldId = id;
        
        File content = null;
        PreparedStatement ps = null;
        PreparedStatement pst = null;
        try
        {
            DSApi.context().session().save(this);
            // flush, aby w tabeli pojawi� si� wiersz z nowym id
            DSApi.context().session().flush();
            
            pst = DSApi.context().prepareStatement("select contentdata, onDisc from " + DSApi.getTableName(getClass()) + " where id = ?");
            pst.setLong(1, oldId);
            ResultSet rs = pst.executeQuery();
            if (rs.next())
            {
                int onDisc = rs.getInt(2);
                if (onDisc == FILE_ON_DISC)
                {
                    Attachment.copyFile(oldId, id);                    
                }
                else
                {
                    InputStream is = rs.getBinaryStream(1);
                    content = StreamUtils.toTempFile(is);
                    org.apache.commons.io.IOUtils.closeQuietly(is);


                    ps = DSApi.context().prepareStatement("update " + DSApi.getTableName(getClass()) + " set contentdata = ? where id = ?");
                    FileInputStream fis = new FileInputStream(content);
                    ps.setBinaryStream(1, fis, (int) content.length());
                    ps.setLong(2, id);
                    ps.executeUpdate();
                    DSApi.context().closeStatement(ps);
                    
                    org.apache.commons.io.IOUtils.closeQuietly(fis);                    
                }
            }
            

            /*
             // kopiowanie bloba
             final String sql = "update "+DSApi.getTableName(getClass())+
             " set contentdata = (select contentdata from "+
             DSApi.getTableName(getClass())+" where id = "+oldId+
             ") where id = "+id;

             st.executeUpdate(sql);
             */
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas kopiowania danych binarnych", e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(pst);
        	DSApi.context().closeStatement(ps);            
            if (content != null)
                content.delete();
        }
    }

    public static AttachmentRevision findByOrginalFileName(String fileName) throws EdmException {
        AttachmentRevision ar = null;
        try{
            //do poprawy tymczasowe rozwiazanie
            Criteria c = DSApi.context().session().createCriteria(AttachmentRevision.class);
            c.add(Restrictions.eq("originalFilename", fileName));
            ar =  (AttachmentRevision) c.uniqueResult();
        }catch (NonUniqueResultException e){
            log.error("", e);
        }

        return ar;
    }

    public static AttachmentRevision find(Long id) throws AttachmentRevisionNotFoundException, AccessDeniedException,
        EdmException
    {
        AttachmentRevision revision = DSApi.getObject(AttachmentRevision.class, id);
        
        //System.out.println(revision instanceof EncryptedAttachmentRevision);
        if (revision == null){
            throw new AttachmentRevisionNotFoundException(id);
        }
        
        Attachment attachment = revision.getAttachment();
        
        if (attachment == null)
            throw new EdmException("Nie znaleziono za��cznika");
        
        if (attachment.isDocumentType())
        {
            Document document = attachment.getDocument();

            if (document == null)
                throw new EdmException("Nie znaleziono dokumentu");
            
           // if (!document.canRead())
            //    throw new AccessDeniedException("Brak uprawnie� do odczytu dokumentu " + document.getId());
        }
        
        return revision;
    }

    public final void updateBinaryStream(InputStream stream, int fileSize) throws EdmException {
    	updateBinaryStreamRevisionSpecific(stream, fileSize);
    	DSApi.context().session().flush();
    	if(this.getAttachment().getDocument() != null && this.getAttachment().getDocument().getDocumentKind() != null) {
    		this.verificationCode = this.getAttachment().getDocument().getDocumentKind().logic().countAttachmentRevisionVerficationCode(this);
        }
    }
    
    /**
     * Pobiera strumie� zawieraj�cy dane z za��cznika binarnego.
     * Metoda powinna by� wywo�ywana tylko i wy��cznie w ramach
     * transakcji, w innym przypadku niekt�re sterowniki JDBC
     * b�d� pr�bowa�y wczyta� ca�� tre�� bloba do pami�ci.
     * <p>
     * Zwracany strumie� zawiera w sobie obiekt Statement u�yty
     * do odczytania BLOB-a, dlatego powinien by� zawsze zamykany
     * przy pomocy metody closeStream(). Metoda close() rzuca
     * wyj�tek.
     * @return
     * @throws EdmException
     */
    public abstract InputStream getBinaryStream() throws EdmException;
    
    public abstract String getAnnotations() throws DocumentNotFoundException, AccessDeniedException, EdmException, IOException;
    
    public abstract void updateAnnotations(String source) throws EdmException, IOException;

    public abstract void updateBinaryStreamRevisionSpecific(InputStream stream, int fileSize) throws EdmException;
    //   zastanowic sie nad zmiana
    public abstract long saveToFile(File file) throws EdmException, IOException;
    
    public abstract File saveToTempFile() throws EdmException, IOException;
    
    public abstract InputStream getStreamByPage(int page, String format, Boolean fax) throws EdmException;
    
    public abstract InputStream getAttachmentStream() throws EdmException;
    
    public abstract Integer getPageCount() throws EdmException;
    
    public abstract RenderedImage getPage(int page) throws EdmException;
    
    //public abstract InputStream getImageAsPDF() throws EdmException;
    
    protected Image getImage(int page) throws EdmException
    {
    	try
    	{
    		//System.out.println(mime);
	    	InputStream is = this.getAttachmentStream();
		    if (mime.endsWith("bmp"))
		    {
		        return BmpImage.getImage(is);
		    }
		    else if (mime.endsWith("gif") || mime.endsWith("jpg") || mime.endsWith("png") || mime.endsWith("jpeg"))
		    {
		    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    	IOUtils.copy(is, baos);
		        return Image.getInstance(baos.toByteArray());
		    }
		    else if (mime.endsWith("tif") || mime.endsWith("tiff"))
		    {
		        RandomAccessFileOrArray raf = new RandomAccessFileOrArray(is);
		        return TiffImage.getTiffImage(raf, page+1);
		    }

    	}
    	catch(Exception e)
    	{
    		log.error(e.getMessage(), e);
    		throw new EdmException("Nie uda�o si� odczyta� pliku!");
    	}
	    throw new EdmException("Nieznany rodzaj pliku graficznego");
    }
    
    public Image getItextImage(int page) throws EdmException
    {
    	return getImage(page);
    }

    protected InputStream getPdfFromLibreOffice() throws EdmException {
        String filename = getOriginalFilename();
        try {
            File file = FileUtils.createTempFile(filename, getAttachmentStream());
            File pdf = FileUtils.createTempFile("attachment.pdf");

            PdfUtils.documentToPdf(file, pdf);
            file.delete();
            pdf.deleteOnExit();

            return new FileInputStream(pdf);
        } catch(IOException e) {
            log.error("[getImageAsPDF] cannot create temp file with filename = {}", filename, e);
            throw new EdmException("Nie mo�na pobra� za��cznika", e);
        }
    }

    public InputStream getAttachmentAs(String targetExt) throws Exception {
        DocumentConverter converter = new DocumentConverter(new RestTemplate());

        String filename = getOriginalFilename();
        try {
            File file = FileUtils.createTempFile(filename, getAttachmentStream());

            File convertedFile = converter.convert(file, filename, FilenameUtils.getExtension(filename), targetExt);
            file.delete();

            return new FileInputStream(convertedFile);
        } catch(IOException e) {
            log.error("[getImageAsPDF] cannot create temp file with filename = {}", filename, e);
            throw new EdmException("Nie mo�na pobra� za��cznika", e);
        }

    }

    public InputStream getImageAsPDF() throws EdmException
	{
        if(!this.mime.equals("application/pdf") && AvailabilityManager.isAvailable("viewserver.rich-document-as-pdf")) {
            return getPdfFromLibreOffice();
        }

    	if (this.mime.endsWith("pdf"))
    	{
    		//TODO: ta metoda wykonuje za duzo kodu, nie potrzebnie konwertuje pdf do pdfa, ten if narazie zalatwia sprawe
    		LoggerFactory.getLogger("tomekl").debug("getImageAsPDF");
    		return this.getAttachmentStream();
    	}
    	
		try
		{
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			com.lowagie.text.Document pdfDoc = null;
	        PdfWriter writer = null;
	        PdfContentByte cb = null;
	
	        File fontDir = new File(Docusafe.getHome(), "fonts");
	        File arial = new File(fontDir, "arial.ttf");
	        BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(), 
	            BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
	        Font font = new Font(baseFont, 12);
	        font.setColor(200, 0, 0);
	        
	        
	        int pages = this.getPageCount();
	
	        for (int i=0; i < pages; i++)
	        {
	            if (this.mime.endsWith("pdf"))                
	            {
	                PdfReader reader = new PdfReader(getAttachmentStream());
	                
	                for (int j=0; j < reader.getNumberOfPages(); j++)
	                {
	                    int pageNumber = j+1;
	                    
	                    Rectangle pageSize = reader.getPageSizeWithRotation(pageNumber);
	                    if (pdfDoc == null)
	                    {
	                        pdfDoc = new com.lowagie.text.Document(pageSize);
	                        writer = PdfWriter.getInstance(pdfDoc, os);
	                        pdfDoc.open();
	                        cb = writer.getDirectContent();
	                    }
	                    else
	                    {
	                        pdfDoc.setPageSize(pageSize);
	                        pdfDoc.newPage();
	                    }
	                    
	                    PdfImportedPage page = writer.getImportedPage(reader, pageNumber);
	                    int rotation = reader.getPageRotation(pageNumber);
	                    if (rotation == 90 || rotation == 270) 
	                    {
	                        cb.addTemplate(page, 0, -1f, 1f, 0, 0, reader.getPageSizeWithRotation(pageNumber).height());
	                    }
	                    else 
	                    {
	                        cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
	                    }
	                }                    
	            }
	            else
	            {	                
	                Image image = this.getImage(i);
	                Rectangle pageSize = null;
	                //log.info("Rozdzielczosc dpi x/y: " + image.getDpiX() + "/" + image.getDpiY() + " z obrazka oo rozdzielczosci: " + image.plainWidth() + "/" + image.plainHeight() + " wynik dzielenia: " + image.plainWidth()/image.getDpiX());
	                
	                if(image.getDpiY() != image.getDpiX() && image.getDpiX() != 0 && image.getDpiY() != 0)
	                {
	                	pageSize = image.plainWidth()/(float)image.getDpiX() > image.plainHeight()/(float)image.getDpiY() ? PageSize.A3.rotate() : PageSize.A4;
	                }
	                else
	                {
	                	pageSize = image.plainWidth() > image.plainHeight() ? PageSize.A3.rotate() : PageSize.A4;
	                }
	                
	                if (pdfDoc == null)
	                {
	                    pdfDoc = new com.lowagie.text.Document(pageSize);
	                    writer = PdfWriter.getInstance(pdfDoc, os);
	                    pdfDoc.open();
	                    cb = writer.getDirectContent();
	                }
	                else
	                {
	                    pdfDoc.setPageSize(pageSize);
	                    pdfDoc.newPage();
	                }
	
                	//image.scalePercent(100);
                    log.info("Szerokosc strony do konw: " + pageSize.width() + " ,wysokosc strony do konw: " + pageSize.height() + " DPI X/Y " + image.getDpiX() + "/" + image.getDpiY() + "szerok/wysoko"  + image.plainWidth() + "/" + image.plainHeight() );

            		
            		if (image.getDpiX() != 0 && image.getDpiY() != 0 && image.getDpiX() != image.getDpiY())
            		{
                        float percentX = (pageSize.width() * 100) / (image.plainWidth()/ (image.getDpiX() / 100.0f));
                		float percentY = (pageSize.height() * 100) / (image.plainHeight()/ (image.getDpiY() / 100.0f));
                		
                		// byc moze zle i powinno sie dzielic przez proporcje Dpi w X i Y
            			percentX = percentX/(image.getDpiX() / 100f);
            			percentY = percentY/(image.getDpiY() / 100f);

                		log.info("Scalujemy procentowo: " + percentX + "/" + percentY);

                		log.info("Skalowana %szerokosc: " + percentX);
                		log.info("Skalowana %wysokosc: " + percentY + "\n");
            			image.scalePercent(percentX, percentY);
            		}
            		else 
            		{
            			//image.scalePercent(percentX < percentY ? percentX : percentY);

            			image.scaleToFit(pageSize.width(), pageSize.height());
            		}
            		//image.setWidthPercentage(0);
	            	
	                
	                //image.scaleToFit(pageSize.width(), pageSize.height());
	                image.setAbsolutePosition(0, 0);
	                cb.addImage(image);
	            }
	            
	           /* if(!getRemarksOnSeperatePage() && i == 0)
	            {
	                float pw = pdfDoc.getPageSize().width();
	                float ph = pdfDoc.getPageSize().height();
	
	                PdfLayer l1 = new PdfLayer("Uwagi", writer);
	                cb.beginLayer(l1);
	                Phrase p = new Phrase(layer, font);
	                ColumnText ct = new ColumnText(cb);
	                ct.setSimpleColumn(p, 20, 20, pw-20, ph-20, 15, Element.ALIGN_LEFT);
	                ct.go();
	                cb.endLayer();
	            }*/
	            
	        }
	        
	        if (pdfDoc != null)
	        {
	            pdfDoc.close();
	            writer.close();
	        }
	        ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray());
	        return is;
	        
		}
		catch (Exception e) 
		{
			throw new EdmException(e.getMessage());
		}
	}
    
    
    public String getFileName() throws EdmException
	{
		return new File(Attachment.getPath(getId())).getName();
	}
    
    protected static boolean getRemarksOnSeperatePage()
    {
        return DSApi.context().userPreferences().node("other").getBoolean("remarksOnSeperatePage", true);
    }
    
    /**
     * metoda zapisuje dane z podanego strumienia do pliku w formie as is. W przypadku zalacznikow 
     * szyfrowanych nalezy najpierw strumien zaszyfrowac.
     * @param source
     * @throws EdmException
     */
    protected void updateFile(InputStream source) throws EdmException
    {
        File dest = Attachment.getFile(this.getId());
        try
        {
            OutputStream out = new FileOutputStream(dest);
            
            byte[] buf = new byte[1024];
            int len;
            while ((len = source.read(buf)) > 0)
            {
                out.write(buf, 0, len);
            }
            source.close();
            out.close();
        }
        catch (IOException ioe)
        {
            throw new EdmException("B��d przy kopiowanu pliku", ioe);
        }
    }
    
    public AttachmentRevision()
    {
    }
    
    Object duplicateHibernateObject() throws EdmException
    {
    	PreparedStatement psu = null;
        try
        {
            final AttachmentRevision clone = (AttachmentRevision) super.clone();
            clone.id = null;
            // pole revision nie powinno by� ruszane, jest aktualizowane
            // w obiekcie Attachment, je�eli stamt�d wywo�ywana jest
            // bie��ca metoda
            
            DSApi.context().session().save(clone);
            // flush, aby w tabeli pojawi� si� wiersz z nowym id
            DSApi.context().session().flush();
                        
            
            if (Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM)
            {
                psu = DSApi.context().prepareStatement("update ds_attachment_revision set contentdata = null, onDisc=? where id = ?");
                psu.setLong(1, FILE_ON_DISC);
                psu.setLong(2, clone.id);
                int rows = psu.executeUpdate();
                DSApi.context().closeStatement(psu);
                
                if (rows != 1)
                    throw new EdmException("");
                //is = getBinaryStream();
                clone.updateFile(getBinaryStream());
            }
            else
            {
                psu = DSApi.context().prepareStatement("update ds_attachment_revision set contentdata = ?, onDisc=? where id = ?");
                psu.setLong(2, FILE_ON_DISC);
                psu.setLong(3, clone.id);
                InputStream is = getBinaryStream();
                File content;
                try
                {
                    content = StreamUtils.toTempFile(is);
                }
                catch (IOException e)
                {
                    throw new SQLException();
                }
                psu.setBinaryStream(1, getBinaryStream(), (int) content.length());
                psu.executeUpdate();                
                DSApi.context().closeStatement(psu);
            }
            return clone;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (CloneNotSupportedException e)
        {
            throw new Error(e.getMessage(), e);
        }
        finally
        {
        	DSApi.context().closeStatement(psu);
        }
    }
    
    public Long getId()
    {
        return id;
    }
    
    protected void setId(Long id)
    {
        this.id = id;
    }
    
    public Integer getRevision()
    {
        return revision;
    }
    
    void setRevision(Integer revision)
    {
        this.revision = revision;
    }
    
    public Date getCtime()
    {
        return ctime;
    }
    
    private void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }
    
    public String getAuthor()
    {
        return author;
    }
    
    @Deprecated
    public void setAuthor(String author)
    {
        this.author = author;
    }
    
    public Attachment getAttachment()
    {
        return attachment;
    }
    
    void setAttachment(Attachment attachment)
    {
        this.attachment = attachment;
    }
    
    public String getOriginalFilename()
    {
        return originalFilename;
    }
    
    public void setOriginalFilename(String originalFilename)
    {
        this.originalFilename = originalFilename;
    }
    
    public Integer getSize()
    {
        return size;
    }
    
    public void setSize(Integer size)
    {
        this.size = size;
    }
    
    public String getMime()
    {
        return mime;
    }
    
    public void setMime(String mime)
    {
        this.mime = mime;
    }
    
    public String getLparam()
    {
        return lparam;
    }
    
    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }
    
    public Long getWparam()
    {
        return wparam;
    }
    
    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }
    
    public Long getContentRefId()
    {
        return contentRefId;
    }
    
    public void setContentRefId(Long contentRefId)
    {
        this.contentRefId = contentRefId;
    }
    
    public void setEditable(Boolean editable)
    {
        this.editable = editable;
    }
    
    public Boolean getEditable()
    {
        if (editable == null)
            return Boolean.FALSE;
        return editable;
    }
    
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (!(o instanceof AttachmentRevision))
            return false;
        
        final AttachmentRevision attachmentRevision = (AttachmentRevision) o;
        
        if (id != null ? !id.equals(attachmentRevision.id) : attachmentRevision.id != null)
            return false;
        
        return true;
    }
    
    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }
    
    /**
     * Zwraca rewizj� jako Base64
     * @param revision
     * @return
     * @throws IOException
     * @throws EdmException
     */
    public static String asBase64(AttachmentRevision revision) throws IOException, EdmException
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();; 
		IOUtils.copy(revision.getAttachmentStream(), out);
		return Base64.encodeBase64String(out.toByteArray());
	}
    
    /* Lifecycle */

    public boolean onSave(Session session) throws CallbackException
    {
        if (author == null)
        {
            author = DSApi.context().getPrincipalName();
        }
        
        if (ctime == null)
        {
            ctime = new Date();
        }
        
        return false;
    }
    
    public boolean onUpdate(Session session) throws CallbackException
    {
        return false;
    }
    
    public boolean onDelete(Session session) throws CallbackException
    {
        // szukanie wszystkich obiekt�w AttachmentRevision, kt�re odwo�uj� si�
        // do tre�ci binarnej bie��cego za��cznika poprzez contentRefId
        // do pierwszego ze znalezionych obiekt�w przepisywana jest tre��
        // za��cznika, kolejnym zmieniana jest warto�� pola contentRefId tak,
        // by odwo�ywa�a si� do nowego po�o�enia za��cznika
        
        Long newRefId = null;
        PreparedStatement pst = null;
        PreparedStatement ps = null;
        InputStream is = null;
        try
        {
            pst = DSApi.context().prepareStatement("select id from ds_attachment_revision where contentrefid = ?");
            pst.setLong(1, id);            
            ResultSet rs = pst.executeQuery();
            while (rs.next())
            {
                AttachmentRevision rev = AttachmentRevision.find(rs.getLong(1));
                if (newRefId == null)
                {
                    if (Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM)
                    {
                        PreparedStatement psu = DSApi.context().prepareStatement(
                            "update ds_attachment_revision set contentdata = null, onDisc=1, contentrefid = null where id = ?");
                        psu.setLong(2, rev.getId());
                        int rows = psu.executeUpdate();
                        DSApi.context().closeStatement(psu);
                        
                        if (rows != 1)
                            throw new EdmException("");
                        is = getBinaryStream();
                        rev.updateFile(is);                        
                    }
                    else
                    {
                        
                        File tempFile = StreamUtils.toTempFile(getBinaryStream());
                        is = new FileInputStream(tempFile);
                        
                        PreparedStatement psu = DSApi.context().prepareStatement(
                            "update ds_attachment_revision set contentdata = ?, contentrefid = null where id = ?");
                        psu.setBinaryStream(1, is, (int) tempFile.length());
                        psu.setLong(2, rev.getId());
                        psu.executeUpdate();                        
                        DSApi.context().closeStatement(psu);                        
                        org.apache.commons.io.IOUtils.closeQuietly(is);                   
                        newRefId = rev.getId();
                    }
                }
                else
                {
                	ps = DSApi.context().prepareStatement("update ds_attachment_revision set contentrefid = ? where id = ?");
                    ps.setLong(1, newRefId);
                    ps.setLong(2, rev.getId());
                    ps.executeUpdate();
                    DSApi.context().closeStatement(ps);
                }
            }
        }
        catch (SQLException e)
        {
            throw new CallbackException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new CallbackException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            throw new CallbackException(e.getMessage(), e);
        }
        catch (EdmException e)
        {
            throw new CallbackException(e.getMessage(), e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
            DSApi.context().closeStatement(pst);            
            org.apache.commons.io.IOUtils.closeQuietly(is);
        }
        
        return false;
    }

    /**
     * Zwraca link do pobrania strony za��cznika (dla tiff�w), metoda jest tak�e odpowiedzialna
     * za sprawdzenie uprawnie� (lub zwr�cenie linka do strony, kt�ra te uprawnienia zwr�ci)
     *
     * metoda przeci��ana w klasach potomnych
     *
     * @param page numer strony
     * @return url
     * @throws EdmException
     */
    public String getPageLink(int page) throws EdmException{
        //download=true tylko dla test�w, nic to nie zmienia, b�dzie to usuni�te
        return "/viewserver/content.action?download=true&id=" + id + "&page=" + page ;
    }

    /**
     * Zwraca link do pobrania strony za��cznika (dla tiff�w), metoda jest tak�e odpowiedzialna
     * za sprawdzenie uprawnie� (lub zwr�cenie linka do strony, kt�ra te uprawnienia zwr�ci)
     *
     * metoda przeci��ana w klasach potomnych
     *
     * @param page numer strony
     * @param binderId id bindera, wykorzystywany przy sprawdzaniu uprawnie�, mo�e by� null
     * @return url
     * @throws EdmException
     */
    public String getPageLinkForBinder(int page, Long binderId) throws EdmException{
        return binderId == null
                ? getPageLink(page)
                : getPageLink(page)+ "&binderId=" + binderId;
    }

    /**
    * Metoda rzuca wyjatkiem gdy weryfikacja integralnosci zalacznika sie nie powiedzie
    * @throws IntegrityVerificationException
    */
    public void verifyIntegrity() throws IntegrityVerificationException
    {
    	String code = "";
    	try
    	{
    		if(this.getAttachment().getDocument() == null || this.getAttachment().getDocument().getDocumentKind() == null)
    			code = null;
    		else
    		{
	    		code = this.getAttachment().getDocument().getDocumentKind().logic().countAttachmentRevisionVerficationCode(this);
    		}
	    	if(this.verificationCode==null)
	    	{
	    		this.verificationCode = code;
	    	}
	    		
    	}
    	catch (Exception e) 
    	{
			throw new IntegrityVerificationException(e);
		}
    	
		if(code!=null && !code.equals(this.verificationCode))
			throw new IntegrityVerificationException("Integrity verification failed");
    }
    
    public void onLoad(Session session, Serializable serializable)
    {
    }
    
    public Integer getOnDisc()
    {
        return onDisc;
    }
    
    public void setOnDisc(Integer onDisc)
    {
        this.onDisc = onDisc;
    }

	public String getVerificationCode()
	{
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode)
	{
		this.verificationCode = verificationCode;
	}

    public IndexStatus getIndexStatus() {
        return IndexStatus.get(indexStatusId);
    }

    public void setIndexStatus(IndexStatus indexStatus) {
        this.indexStatusId = indexStatus.getId();
    }

    /**
     * <p>
     *     Tworzy tymczasowy plik z zawarto�ci� danego revisionu.
     *     Plik jest tworzony metod� <code>File.createTempFile()</code>.
     *     Rozszerzenie pliku jest zachowane.
     * </p>
     * @return
     * @throws EdmException
     * @throws IOException
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     */
    public File getAsTempFile() throws EdmException, IOException {
        String filename = getOriginalFilename();

        File file = FileUtils.createTempFile(filename, getAttachmentStream());

        return file;
    }


}
