package pl.compan.docusafe.core.base;

import java.io.Serializable;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * Klasa odwzorowuj�ca tabelk� DSO_USER_TO_JASPER_REPORT
 * 
 * @author marek.wojciechowski@docusafe.pl
 */
public class UserToJasperReports implements Serializable
{
	
	private Logger log = LoggerFactory.getLogger(UserToBriefcase.class);
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id
	 */
	private Long id;
	
	/**
	 * id raportu 
	 */
	private Long reportId;
	
	/**
	 * Nazwa raportu
	 */
	
	private String reportName;


	/**id u�ytkownika 
	 */
	private Long userId;
	
	/**
	 * Nazwa uzytkownika
	 */
	
	private String userName;
	
	/**
	 *Imi�
	 */
	private String userFirstname;
	
	/**
	 * nazwisko
	 */
	private String userLastname;

	/**
	 * nazwa zawnetrzna dla uzytkownika
	 */
	private String externalName;
	
	/**
	 * Domy�lny konstruktor
	 */
	public UserToJasperReports() 
	{
	}

	/**
	 * KOnstruktor ustawiaj�cy pola
	 * @param id
	 * @param reportId
	 * @param userId
	 * @param userName
	 * @param userFirstname
	 * @param userLastname
	 * @param reportName
	 */

	public UserToJasperReports(Long id, Long reportId, String reportName,
			Long userId, String userName, String userFirstname,
			String userLastname, String externalName) {
		super();
		this.id = id;
		this.reportId = reportId;
		this.userId = userId;
		this.userName = userName;
		this.userFirstname = userFirstname;
		this.userLastname = userLastname;
		this.reportName = reportName;
		this.externalName = externalName;
	}
	
	
	public void delete() throws Exception
    {
    	log.debug("deleting " + this);
        try
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
            if(AvailabilityManager.isAvailable("absence.createEventOnCreate"))
            {
            	CalendarFactory.getInstance().deleteEventsByRelatedAbsence(getId());
            }
        }
        catch (HibernateException e)
        {
            log.error("Blad usuniecia wpisu");
            throw new EdmException("Blad usuniecia wpisu. "+e.getMessage());
        }
    }
    

	public void create() throws Exception, EdmException
	{
		log.debug("create()");
		
        DSApi.context().session().save(this);
        DSApi.context().session().flush(); 
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long containerId) {
		this.reportId = containerId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserFirstname() {
		return userFirstname;
	}

	public void setUserFirstname(String userFirstname) {
		this.userFirstname = userFirstname;
	}

	public String getUserLastname() {
		return userLastname;
	}

	public void setUserLastname(String userLastname) {
		this.userLastname = userLastname;
	}
	
	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	
	public void setUserExternalName(String externalName) {
		this.externalName = externalName;
	}
	
	public String getUserExternalName() {
		return externalName;
	}
        
}
