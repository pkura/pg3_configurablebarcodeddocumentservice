package pl.compan.docusafe.core.base;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentLockMode.java,v 1.6 2009/02/28 09:22:45 wkuty Exp $
 */
public class DocumentLockMode implements java.io.Serializable, UserType
{
    public static final DocumentLockMode READ = new DocumentLockMode("READ");
    public static final DocumentLockMode WRITE = new DocumentLockMode("WRITE");

    private String mode;

    public DocumentLockMode()
    {
    }

    private DocumentLockMode(String mode)
    {
        this.mode = mode;
    }

    public java.io.Serializable disassemble(Object value) throws HibernateException
	{
		return (Integer) value;
	}
    
    public int hashCode(Object x) throws HibernateException
	{
		return 0;
	}
    
    public Object assemble(java.io.Serializable cached, Object owner) throws HibernateException
	{
		return cached;
	}
    
    public Object replace(Object original, Object target, Object owner) throws HibernateException
	{
		return original;
	}
    
    public String getMode()
    {
        return mode;
    }

    public String toString()
    {
        return mode;
    }

    private Object readResolve() throws ObjectStreamException
    {
        if (READ.toString().equals(mode)) return READ;
        else if (WRITE.toString().equals(mode)) return WRITE;
        else throw new InvalidObjectException("Nieznana warto�� pola mode: "+mode);
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof DocumentLockMode)) return false;

        final DocumentLockMode documentLockMode = (DocumentLockMode) o;

        if (mode != null ? !mode.equals(documentLockMode.mode) : documentLockMode.mode != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (mode != null ? mode.hashCode() : 0);
    }

    /* UserType */

    public int[] sqlTypes()
    {
        return new int[] { Types.VARCHAR };
    }

    public Class returnedClass()
    {
        return DocumentLockMode.class;
    }

    public boolean equals(Object o, Object o1) throws HibernateException
    {
        return o instanceof DocumentLockMode && o1 instanceof DocumentLockMode &&
            o.equals(o1);
    }

    public Object nullSafeGet(ResultSet resultSet, String[] strings, Object o) throws HibernateException, SQLException
    {
        String lockMode = resultSet.getString(strings[0]);
        if (READ.toString().equals(lockMode)) return READ;
        else if (WRITE.toString().equals(lockMode)) return WRITE;
        else throw new HibernateException("Niepoprawna warto�� pola lockMode: "+lockMode);
    }

    public void nullSafeSet(PreparedStatement preparedStatement, Object o, int i) throws HibernateException, SQLException
    {
        if (o == null)
        {
            preparedStatement.setObject(i, null);
        }
        else
        {
            preparedStatement.setString(i, o.toString());
        }
    }

    public Object deepCopy(Object o) throws HibernateException
    {
        return o;
    }

    public boolean isMutable()
    {
        return false;
    }
}
