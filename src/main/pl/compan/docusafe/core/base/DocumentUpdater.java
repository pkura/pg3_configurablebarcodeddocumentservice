package pl.compan.docusafe.core.base;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import java.util.Collections;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 *
 * Wymaga otwartego kontekstu
 */
public class DocumentUpdater {

    private boolean wasInTransaction;

    public DocumentUpdater() throws EdmException {
        wasInTransaction = DSApi.context().inTransaction();
        if (!wasInTransaction)
            DSApi.context().begin();
    }

    public void save() throws EdmException {
        //DSApi.context().session().flush();
        DSApi.context().commit();
        if (wasInTransaction)
            DSApi.context().begin();
    }


    public void update(Document document, Map<String, Object> changes) throws EdmException {
        document.getDocumentKind().setOnly(document.getId(), changes);
    }

    public void update(Document document, String cn, Object value) throws EdmException {
        document.getDocumentKind().setOnly(document.getId(), Collections.singletonMap(cn, value));
    }

    public static void updateValue(Document document, Map<String, Object> changes) throws EdmException {
        DocumentUpdater updater = new DocumentUpdater();
        updater.update(document, changes);
        updater.save();
    }

    public static void updateValue(Document document, String cn, Object value) throws EdmException {
        updateValue(document, Collections.singletonMap(cn, value));
    }
}
