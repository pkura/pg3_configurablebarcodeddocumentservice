package pl.compan.docusafe.core.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.jackrabbit.JackrabbitAttachmentRevision;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.regtype.RegistryEntry;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.MimetypesFileTypeMap;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.AttachmentsTabAction;

/**
 * Za��cznik dokumentu. W tej klasie nie jest przechowywana
 * tre�� za��cznika, jest ona tylko kontenerem dla klas
 * AttachmentRevision.
 *
 * @see Document
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Attachment.java,v 1.84 2009/10/16 14:57:17 pecet1 Exp $
 */
public class Attachment implements Lifecycle, Cloneable
{
    
    public static boolean ATTACHMENTS_IN_LOCAL_FILE_SYSTEM;
    public static boolean PATH_TO_ATTACHMENTS_WITH_DATE;
    public static boolean IMAGES_DECOMPRESSED_IN_LOCAL_FILE_SYSTEM;
    
    public static final Integer ATTACHMENT_TYPE_DOCUMENT = 1;
    public static final Integer ATTACHMENT_TYPE_REGISTRY_ENTRY = 2;
    public static final Integer ATTACHMENT_TYPE_UMOWA = 3;
    public static final Integer ATTACHMENT_TYPE_DICTIONARY = 4;
    
    private static final Log log = LogFactory.getLog(Attachment.class);
    static final StringManager sm = StringManager.getManager(Constants.Package);
    
    public final static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
    public final static SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    private Long id;
    private String title;
    private String description;
    private Date ctime;
    private String author;
    private Document document;
    private Long documentId;
    private Set<AttachmentRevision> revisions;
    private boolean deleted;
    private String barcode;
    private String lparam;
    private Long wparam;
    private String cn;
   
    private String fieldCn;
    
    private Integer type;
    private RegistryEntry registryEntry;
    private String uuid;

    private String fileExtension;
    private boolean encrypted = false;
	private boolean editable = true;

    static
    {
        ATTACHMENTS_IN_LOCAL_FILE_SYSTEM = Configuration.additionAvailable(Configuration.ADDITION_ATTACHMENTS_AS_FILES);
        PATH_TO_ATTACHMENTS_WITH_DATE = Configuration.additionAvailable(Configuration.PATH_TO_ATTACHMENTS_WITH_DATE);
        IMAGES_DECOMPRESSED_IN_LOCAL_FILE_SYSTEM = Configuration.isAdditionOn(Configuration.IMAGES_DECOMPRESSED_IN_LOCAL_FILE_SYSTEM);
    }

	/**
     * @todo - Nieco do kitu jest ta konstrukcja
     * Metoda zwraca File jesli tworzy, path jesli sciezke
     * Nie podoba mi sie ten pomysl albo cos zwraca plik, albo sciezke
     * Kiedys moze sie to poprawi
     * Poza tym powinno to byc w AttachmentRevision - bo tam ten kod pasuje
     * @param id
     * @param create
     * @return Sciezka i nazwa pliku z zalacznikami dla istniejacej rewizji zalacznika
     * @throws pl.compan.docusafe.core.EdmException
     */
    
    protected static Object getPathOrCreate(Long id, boolean create) throws EdmException
    {
        String s = Configuration.getPathToAttachments();
        if (s == null)
            return s;
        StringBuffer path = new StringBuffer(s);
        path.append("/");
        String filename = Long.toHexString(id);
        int len = filename.length();
        filename = ("00000000".concat(filename)).substring(len);
        if (PATH_TO_ATTACHMENTS_WITH_DATE)
        {           
            AttachmentRevision ar = AttachmentRevision.find(id);
            path.append(yearFormat.format(ar.getCtime()));
            path.append("/");
            path.append(dayFormat.format(ar.getCtime()));
        }
        else
        {
            path.append(filename.substring(0, 2));
            path.append("/"); 
            path.append(filename.substring(2, 5));
        }
        
        path.append("/");
        if (create)
        {
            (new File(path.toString())).mkdirs();
        }
        path.append(filename.toUpperCase());
        path.append(".DAT");
        if (create)
        {
            File file = new File(path.toString());
            try
            {
                if (file.exists())
                    file.delete();
                file.createNewFile();
            }
            catch (IOException ioe)
            {
                throw new EdmException("File problem-"+path.toString().toUpperCase());
            }            
            return file;
        }
        // Wywalilem to upperCase to jakas bryndza
        // Na unixach gubila sie nazwa zalacznika
        return path.toString();
    }
    
    /**
     * Zwraca sciezke dla zalacznika o podanym id
     * Jesli plik nie istnieje nie tworzy go
     */
    public static String getPath(Long id) throws EdmException
    {
        return (String) getPathOrCreate(id, false);
    }
    
    /**
     * Zwraca sciezke dla uwag zalacznika o podanym id
     * Jesli plik nie istnieje nie tworzy go
     */
    public static String getAnnotationsPath(Long id) throws EdmException
    {
        String filePath = (String) getPathOrCreate(id, false);
        String annotationsPath = filePath.substring(0, filePath.length()-3)+"ANN";
        return annotationsPath;
    }
    
    /**
     * Jesli nie istnieje zostanie utworzony
     * 
     * @param id revisionId
     * @return File - wskaznik na plik z wersja zalacznika 
     */
    public static File getFile(Long id) throws EdmException
    {
        return (File) getPathOrCreate(id, true);
    }
    
    /**
     * Usuwa fizycznie wersja zalacznikow podlegaj�ce podanemu obiektowi Attachment  (tylko te na dysku). 
     *
     */
    public void deleteRevisions()
    {
        for (AttachmentRevision ar : revisions)
        {
            if (ar.getOnDisc().equals(AttachmentRevision.FILE_ON_DISC))
            {
                try
                {
                    if (!(new File(getPath(ar.getId()))).delete())
                        throw new EdmException("File was found, but cannot be deleted.");
                }
                catch (EdmException edme)
                {
                    log.error("File cannot be deleted. Reason: " + edme.getMessage());
                }
            }
        }
    }
    
    public static void copyFile(Long from, Long to) throws EdmException
    {
        File dest = getFile(to);
        File source = new File(getPath(from));
        if (source == null)
        {
            throw new EdmException("Nie znaleziono pliku");
        }
        
        try
        {
            InputStream in = new FileInputStream(source);
            OutputStream out = new FileOutputStream(dest);
            
            byte[] buf = new byte[4096];
            int len;
            while ((len = in.read(buf)) > 0)
            {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
        catch (IOException ioe)
        {
            throw new EdmException("B��d przy kopiowanu pliku", ioe);
        }
    }
    
    /**
     * Konstruktor dla Hibernate.
     */
    Attachment()
    {
    }
    
    public Attachment(String title)
    {
        this.title = title;
        // nie mog� u�y� pustego napisu, bo Oracle konwertuje go
        // do NULL (kolumna description ma modyfikator NOT NULL).
        this.description = title;
    }
    
    public Attachment(String title, String description)
    {
        this.title = title;
        this.description = description;
    }
    
    public final Attachment cloneObject() throws EdmException
    {
        try
        {
            Attachment clone = (Attachment) super.clone();
            clone.duplicate();
            return clone;
        }
        catch (CloneNotSupportedException e)
        {
            throw new Error(e);
        }
    }
    
    protected void duplicate() throws EdmException
    {
        Set<AttachmentRevision> savedRevisions = this.revisions;
        this.revisions = null;
        
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        
        if (savedRevisions != null)
        {
            this.revisions = new HashSet<AttachmentRevision>(savedRevisions.size());
            for (AttachmentRevision revision : savedRevisions)
            {
                revision = revision.cloneObject();
                revision.setAttachment(this);
                this.revisions.add(revision);
            }
        }
    }
    /**
     * Pobiera za��cznik o wskazanym ID.  Wymaga uprawnienia do
     * odczytu dokumentu (Document.canRead()).
     */
    public static Attachment find(Long id) throws AttachmentNotFoundException, DocumentNotFoundException,
        AccessDeniedException, EdmException
    {
    	return find(id,true,null);
    }
    
    /**
     * Pobiera za��cznik o wskazanym ID.  Wymaga uprawnienia do
     * odczytu dokumentu (Document.canRead()) Je�li checkPermission = false nie sprawdza uprawnie� do dokumentu.
     */
    public static Attachment find(Long id,boolean checkPermission) throws AttachmentNotFoundException, DocumentNotFoundException,
        AccessDeniedException, EdmException
    {
    	return find(id,checkPermission,null);
    }
    
    /**
     * Pobiera za��cznik o wskazanym ID.  Wymaga uprawnienia do
     * odczytu dokumentu (Document.canRead()) Je�li checkPermission = false nie sprawdza uprawnie� do dokumentu.
     */
    public static Attachment find(Long id,boolean checkPermission,Long binderId) throws AttachmentNotFoundException, DocumentNotFoundException,
        AccessDeniedException, EdmException
    {
        Attachment attachment = (Attachment) DSApi.getObject(Attachment.class, id);
        
        if (attachment == null)
            throw new AttachmentNotFoundException(id);
        
        if (attachment.isDocumentType())
        {
            Document document = attachment.getDocument();
            if (document == null)
                throw new DocumentNotFoundException(id);
            
            if (checkPermission && !attachment.getDocument().canRead(binderId))
                throw new AccessDeniedException("Brak uprawnie� do odczytania za��cznik�w " + "w tym dokumencie");
        }
        
        return attachment;
    }
    
    /**
     *  Zwraca pierwszy znaleziony w bazie zalacznik. 
     * @param documentId
     * @return Attachment
     * @throws AttachmentNotFoundException
     * @throws pl.compan.docusafe.core.EdmException
     * @throws java.sql.SQLException
     */
    public static Attachment findByName(String name) throws AttachmentNotFoundException, EdmException, SQLException
    {
    	Attachment attachment = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement("select id from ds_attachment where title = ?");
    		ps.setString(1, name);
    		rs = ps.executeQuery();
               
                if(rs.getFetchSize() < 1)
                    return null;
                
    		if(rs.next())
    			attachment = (Attachment) DSApi.getObject(Attachment.class, rs.getLong(1));
    		rs.close();
    	}
    	finally
    	{
    		DSApi.context().closeStatement(ps);
                
    	}    
    	return attachment;
    }
    
    /**
     *  Zwraca pierwszy znaleziony w bazie zalacznik. 
     * @param documentId
     * @return Attachment
     * @throws AttachmentNotFoundException
     * @throws pl.compan.docusafe.core.EdmException
     * @throws java.sql.SQLException
     */
    public static Attachment findFirstAttachment(Long documentId) throws AttachmentNotFoundException, EdmException, SQLException
    {
    	Attachment attachment = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement("select id from ds_attachment where DOCUMENT_ID = ?  AND (DELETED <> 1 OR DELETED IS NULL)");
    		ps.setLong(1, documentId);
    		rs = ps.executeQuery();
               
                if(rs.getFetchSize() < 1)
                    return null;
                
    		if(rs.next())
    			attachment = (Attachment) DSApi.getObject(Attachment.class, rs.getLong(1));
    		rs.close();
    	}
    	finally
    	{
    		DSApi.context().closeStatement(ps);
                
    	}    
    	return attachment;
    }
    /**
     *  Zwraca wszystkie za�aczniki znalezione w bazie dla danego dokumentu. 
     *  zwracana lista mo�e byc pusta 
     * @param documentId
     * @return List Attachment
     * @throws 
     * @throws pl.compan.docusafe.core.EdmException
     * @throws java.sql.SQLException
     */
    public static List<Attachment> findAllAttachmentByDocumentId(Long documentId) throws AttachmentNotFoundException, EdmException, SQLException
    {
    	List<Attachment> attachment = new ArrayList<Attachment>();
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement("select id from ds_attachment where DOCUMENT_ID = ?  AND (DELETED =? OR DELETED IS NULL)");
    		ps.setLong(1, documentId);
    		ps.setBoolean(2, false);
    		rs = ps.executeQuery();
               
    		while(rs.next())
    			attachment.add((Attachment) DSApi.getObject(Attachment.class, rs.getLong(1)));
    		rs.close();
    	}
    	finally
    	{
    		DSApi.context().closeStatement(ps);
                
    	}    
    	return attachment;
    }
    
    public AttachmentRevision createRevision(File file) throws EdmException
    {
    	return createRevision(file,false);
    }
    
    public AttachmentRevision createRevision(File file, Boolean forceOfficeHisotry) throws EdmException
    {
		if(file == null || !file.isFile()){
			throw new IllegalArgumentException("Podany plik nie istnieje");
		}
        if (isDocumentType())
        {
            // createRevision(InputStream, ...) te� rzuca ten wyj�tek,
            // ale sprawdzenie uprawnie� w tym miejscu pozwala unikn��
            // niepotrzebnego otwierania pliku
            if (!getDocument().canModifyAttachments())
                throw new AccessDeniedException(sm
                    .getString("documentHelper.documentModificationDenied", getDocument().getId()));
            
            //document.changelog(DocumentChangelog.ADD_ATTACHMENT_REVISION);
            if (getRevisions().size() > 0)
            	DataMartManager.storeEvent(document, DataMartDefs.DOCUMENT_ATTACHMENT_REVISION_ADD,
            			DSApi.context().getPrincipalName(), getTitle(),forceOfficeHisotry);
        }
        
        FileInputStream fis = null;
        try
        {
            fis = new FileInputStream(file);
            return createRevision(fis, (int) file.length(), file.getName());
        }
        catch (IOException e)
        {
            throw new EdmException("B��d odczytu pliku", e);
        }
        finally
        {
        	IOUtils.closeQuietly(fis);
        }
    }
    
    public AttachmentRevision createRevision(InputStream stream, int fileSize, String originalFilename) throws EdmException
    {
        return createRevision(null, stream, fileSize, originalFilename);
    }
    
    /**
     * Utworzenie wersji za��cznika na podstawie istniej�cej wersji, kt�ra mo�e
     * pochodzi� z innego za��cznika.  W tym wypadku tre�� binarna wersji (blob)
     * nie b�dzie kopiowana, a jedynie w nowym za��czniku pole contentRefId b�dzie
     * zawiera�o identyfikator przekazanego tutaj za��cznika.
     */
    public AttachmentRevision createRevision(AttachmentRevision templateRevision) throws EdmException
    {
        return createRevision(templateRevision, null, 0, null);
    }
    
    private AttachmentRevision createRevision(AttachmentRevision templateRevision, InputStream stream, int fileSize,
        String originalFilename) throws EdmException
    {
        if (templateRevision == null && stream == null)
            throw new NullPointerException("templateRevision == null && stream == null");
        
        log.info("createRevision templateRevision=" + templateRevision + " stream=" + stream + " fileSize=" + fileSize);
        
        if (isDocumentType())
        {
            if (!getDocument().canModifyAttachments())
                throw new AccessDeniedException(sm
                    .getString("documentHelper.documentModificationDenied", getDocument().getId()));
            
            getDocument().fireAttachmentRevisionAdded(getTitle());
            getDocument().setMtime(new Date());
            
            /**
            if (revisions != null && revisions.size() > 0 && getDocument() instanceof OfficeDocument)
            {
            	
                ((OfficeDocument) getDocument()).addWorkHistoryEntry(
                    Audit.create("attachments", DSApi.context().getPrincipalName(), sm.getString("UtworzonoNowaWersjeZalacznika",getTitle()), "REVISION", id));
            }
            */
        }
        
        PreparedStatement ps = null;
        try
        {
            if (revisions == null)
                revisions = new HashSet<AttachmentRevision>();
            
            // szukam najwy�szego numeru rewizji za��cznika
            int recentRevision = 0;
            for (AttachmentRevision rev : revisions)
            {
                if (rev.getRevision().intValue() > recentRevision)
                    recentRevision = rev.getRevision().intValue();
            }
            
            // tworz� rewizj� z numerem o jeden wy�szym od znalezionego
            AttachmentRevision revision;
            if(getDocument() != null && getDocument().getDocumentKind()!=null) {
            	revision = getDocument().getDocumentKind().logic().createAttachmentRevision();
            } else {
                if(AvailabilityManager.isAvailable("jackrabbit.repositories")) {
                    revision = JackrabbitAttachmentRevision.create();
                } else {
            	    revision = new StandardAttachmentRevision();
                }
            }
            revision.setAttachment(this);
            revision.setRevision(recentRevision + 1);
            
            if (templateRevision == null)
            {
                revision.setSize(fileSize);
                revision.setOriginalFilename(originalFilename);
                
                if (revision.getOriginalFilename() != null)
                {
                    String mime = MimetypesFileTypeMap.getInstance().getContentType(
                        revision.getOriginalFilename().toLowerCase());
                    if (log.isDebugEnabled())
                        log.debug("MIME: " + revision.getOriginalFilename() + " -> " + mime);
                    revision.setMime(mime);
                }
                else
                {
                    revision.setMime(MimetypesFileTypeMap.DEFAULT_MIME_TYPE);
                }
                
                revisions.add(revision);
                DSApi.context().session().save(revision);
                DSApi.context().session().flush();
                DSApi.context().session().refresh(revision);
                //System.out.println(revision.getId());
                
                // aktualizuj� pole typu BLOB, dla niekt�rych baz danych
                // b�dzie to wygl�da�o inaczej (np. dla PostgreSQL nale�y
                // wykorzysta� LargeObject API).
                if(!(revision instanceof JackrabbitAttachmentRevision)){
                    if (ATTACHMENTS_IN_LOCAL_FILE_SYSTEM) {
                        revision.setOnDisc(AttachmentRevision.FILE_ON_DISC);
                    } else {
                        revision.setOnDisc(AttachmentRevision.FILE_IN_DATABASE);
                    }
                }
                revision.updateBinaryStream(stream, fileSize);
            }
            else //  (templateRevision != null)

            {
                revision.setSize(templateRevision.getSize());
                revision.setOriginalFilename(templateRevision.getOriginalFilename());
                revision.setMime(templateRevision.getMime());
                revision.setContentRefId(templateRevision.getId());
                revisions.add(revision);
                DSApi.context().session().save(revision);
            }
            if(getDocument() != null)
            	//TaskSnapshot.updateIsAnyImageByDocumentId(getDocument().getId(),getDocument().getStringType());
                //TaskSnapshot.updateByDocumentId(getDocument().getId(), InOfficeDocument.TYPE);
                TaskSnapshot.updateByDocument(getDocument());
            return revision;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);            
        }
    }
    
    /**
     * Oznacza za��cznik jako usuni�ty. U�ytkownik musi mie� prawo
     * do usuwania dokumentu zwi�zanego z za��cznikiem, inaczej rzucany
     * jest wyj�tek AccessDeniedException.
     * <p>
     * Usuwany za��cznik musi by� zwi�zany z dokumentem.
     * 
     * @throws pl.compan.docusafe.core.EdmException
     * @throws pl.compan.docusafe.core.security.AccessDeniedException
     */
    public void delete() throws EdmException, AccessDeniedException
    {
        delete(true);
    }
    
    /**
     * Oznacza za��cznik jako usuni�ty. U�ytkownik musi mie� prawo
     * do usuwania dokumentu zwi�zanego z za��cznikiem, inaczej rzucany
     * jest wyj�tek AccessDeniedException.
     * <p>
     * Usuwany za��cznik musi by� zwi�zany z dokumentem.
     * 
     * @param toHistory true <=> dodajemy wpis do historii pisma o usuni�ciu za��cznika
     *        Na razie ignorujemy dodajemy zawsze
     * 
     * @throws pl.compan.docusafe.core.EdmException
     * @throws pl.compan.docusafe.core.security.AccessDeniedException
     */
    public void delete(boolean toHistory) throws EdmException, AccessDeniedException
    {
        if (isDocumentType())
        {
            if (getDocument() == null)
                throw new NullPointerException(sm.getString("attachment.nullDocument", id));
            
            if (!getDocument().canModifyAttachments())
                throw new AccessDeniedException(sm.getString("attachment.deleteDenied"));
            /*
            if (toHistory && document instanceof OfficeDocument)
            {
                ((OfficeDocument) document).addWorkHistoryEntry(
                    Audit.create("attachments", DSApi.context().getPrincipalName(), sm.getString("UsunietoZalacznik",title), "DELETE",
                        getId()));
            }
            document.changelog(DocumentChangelog.DEL_ATTACHMENT);
            */
            AttachmentsTabAction tab = new AttachmentsTabAction() {
				
				@Override
				public String getDocumentType() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				protected List prepareTabs() {
					// TODO Auto-generated method stub
					return null;
				}
			};
            
            
			if(tab.getPowodUsuniecia() != null){
				DataMartManager.storeEvent(getDocument(), DataMartDefs.DOCUMENT_ATTACHMENT_DELETE, DSApi.context().getPrincipalName(), title + ", "+sm.getString("powodUsuniecia")+":" + tab.getPowodUsuniecia()  );                        

			}else{

				DataMartManager.storeEvent(getDocument(), DataMartDefs.DOCUMENT_ATTACHMENT_DELETE, DSApi.context().getPrincipalName(), title );                        
				getDocument().fireAttachmentDeleted(title);
        }
    }
        
        this.deleted = true;
    }
    
    /**
     * Zwraca najnowsz� wersj� za��cznika lub null.
     */
    public AttachmentRevision getMostRecentRevision()
    {
        if (revisions != null && revisions.size() > 0)
        {
            return (AttachmentRevision) revisions.toArray()[revisions.size() - 1];
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Zwraca przedostatni� wersj� za��cznika lub null.
     */
    public AttachmentRevision getBeforeMostRecentRevision()
    {
        if (revisions != null && revisions.size() > 1)
        {
            return (AttachmentRevision) revisions.toArray()[revisions.size() - 2];
        }
        else
        {
            return null;
        }
    }
    
    Object duplicateHibernateObject() throws EdmException
    {
        try
        {
            final Attachment clone = (Attachment) super.clone();
            clone.id = null;
            clone.revisions = null;
            // pole document nie powinno by� ruszane
            
            DSApi.context().session().save(clone);
            
            // klonowanie obiekt�w AttachmentRevision
            if (revisions != null)
            {
                clone.revisions = new HashSet<AttachmentRevision>(revisions.size());
                for (Iterator iter = revisions.iterator(); iter.hasNext();)
                {
                    final AttachmentRevision revision = (AttachmentRevision) ((AttachmentRevision) iter.next())
                        .duplicateHibernateObject();
                    revision.attachment = clone;
                    clone.revisions.add(revision);
                }
            }
            
            return clone;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (CloneNotSupportedException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    
    public Long getId()
    {
        return id;
    }
    
    private void setId(Long id)
    {
        this.id = id;
    }
    
    public String getTitle()
    {
        return title;
    }
    
    public void setTitle(String title) throws EdmException, AccessDeniedException
    {
        if (isDocumentType())
        {
            if (getDocument() == null)
                return;
            
            if (!getDocument().canModifyAttachments())
                throw new AccessDeniedException(Document.sm.getString("document.setPropertyDenied"));
        }
        this.title = title;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public void setDescription(String description) throws EdmException, AccessDeniedException
    {
        if (isDocumentType())
        {
            if (getDocument() == null)
                return;
            
            if (!getDocument().canModifyAttachments())
                throw new AccessDeniedException(Document.sm.getString("document.setPropertyDenied"));
        }
        
        this.description = description;
    }
    
    public Date getCtime()
    {
        return ctime;
    }
    
    private void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }
    
    public String getAuthor()
    {
        return author;
    }
    
    public void setAuthor(String author)
    {
        this.author = author;
    }

    
	public Document getDocument() throws EdmException
	{
    	if (documentId == null)
    		return document;
        if (document == null || !document.getId().equals(documentId))
        	document = Finder.find(Document.class, documentId);
        return document;
	}
    
    public void setDocument(Document document)
    {
    	this.document = document;
    	if(document != null)
    		this.documentId = document.getId();
    }
    
    /**
     * Zwraca zbi�r wersji za��cznika, zwracany zbi�r mo�e by�
     * pusty.
     * @return Zbi�r wersji (nigdy null) posortowany w rosn�cym
     *  porz�dku numer�w wersji.
     */
    public Set<AttachmentRevision> getRevisions()
    {
        if (revisions == null)
            revisions = new LinkedHashSet<AttachmentRevision>();
        return revisions;
    }
    
    private void setRevisions(Set<AttachmentRevision> revisions)
    {
        this.revisions = revisions;
    }
    
    public boolean isDeleted()
    {
        return deleted;
    }
    
    private void setDeleted(boolean deleted)
    {
        this.deleted = deleted;
    }
    
    public String getBarcode()
    {
        return barcode;
    }
    
    public void setBarcode(String barcode)
    {
        this.barcode = barcode;
    }
    
    public String getLparam()
    {
        return lparam;
    }
    
    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }
    
    public Long getWparam()
    {
        return wparam;
    }
    
    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }
    
    public String getCn()
    {
        return cn;
    }
    
    public void setCn(String cn)
    {
        this.cn = cn;
    }
    
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (!(o instanceof Attachment))
            return false;
        
        final Attachment attachment = (Attachment) o;
        
        if (id != null ? !id.equals(attachment.id) : attachment.id != null)
            return false;
        
        return true;
    }
    
    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }
    
    /* Lifecycle */

    public boolean onSave(Session session) throws CallbackException
    {
        if (author == null)
        {
            this.author = DSApi.context().getPrincipalName();
        }
        
        try
        {
        	if(this.documentId == null && getDocument() != null && getDocument().getId() == null)
        	{
        		DSApi.context().session().flush();
        		//DSApi.context().session().save(getDocument());
        	}
        	if(this.documentId == null)
        		this.documentId = getDocument().getId();
        }
        catch (Exception e) {
			log.error(e.getMessage(),e);
		}
        if (ctime == null)
        {
            this.ctime = new Date();
        }
        
        return false;
    }
    
    
    public boolean onUpdate(Session session) throws CallbackException
    {
        return false;
    }
    
    public boolean onDelete(Session session) throws CallbackException
    {
        return false;
    }
    
    public void onLoad(Session session, Serializable serializable)
    {
    }
    
    public String toString()
    {
        return getClass().getName() + "[id=" + id + (cn != null ? " cn=" + cn : "") + "]";
    }
    
    public void setType(Integer type)
    {
        this.type = type;
    }
    
    public Integer getType()
    {
        return type;
    }
    
    public void setRegistryEntry(RegistryEntry registryEntry)
    {
        this.registryEntry = registryEntry;
    }
    
    public RegistryEntry getRegistryEntry()
    {
        return registryEntry;
    }
    
    public boolean isDocumentType()
    {
        return ATTACHMENT_TYPE_DOCUMENT.equals(type);
    }
    
    public boolean isRegistryEntryType()
    {
        return ATTACHMENT_TYPE_REGISTRY_ENTRY.equals(type);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * 
     * @return Cn pola dockinda, z kt�rego zosta� zapisany za��cznik
     */
    public String getFieldCn() {
		return fieldCn;
	}

    /**
     * @param fieldCn Cn pola dockinda, z kt�rego zosta� zapisany za��cznik
     */
	public void setFieldCn(String fieldCn) {
		this.fieldCn = fieldCn;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

    /**
     * Zwraca rewizj� po numerze rewizji.
     * @param revision
     * @return
     */
    public AttachmentRevision getRevisionByNumber(Integer revision) {
        AttachmentRevision ar = null;

        for(AttachmentRevision rev : getRevisions()){
           if(rev.getRevision().equals(revision)){
               ar = rev;
           }
        }

        return ar;
    }

    public static class AttacgnebtComparatorByDate implements Comparator<Attachment>
    {
        public int compare(Attachment e1, Attachment e2)
        {
            return e1.getCtime().compareTo(e2.getCtime());
        }
    }

    public AttachmentRevision getNewestRevision(){
        AttachmentRevision ar = null;
        Date maxDate = new Date(Long.MIN_VALUE);
        for(AttachmentRevision rev : getRevisions()){
            if(rev.getCtime().after(maxDate)){
                maxDate = rev.getCtime();
                ar = rev;
            }
        }
        return ar;
    }
public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
		
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public boolean isEncrypted() {
		return encrypted;
	}

	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}
}


