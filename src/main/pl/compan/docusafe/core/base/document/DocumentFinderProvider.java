package pl.compan.docusafe.core.base.document;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.container.ContainerFinder;
import pl.compan.docusafe.core.office.container.OfficeCaseDbFinder;
import pl.compan.docusafe.core.office.container.OfficeCaseSolrFinder;
import pl.compan.docusafe.util.QueryForm;

public class DocumentFinderProvider {
    private static final String PROVIDE_DB = "db";
    private static final String PROVIDE_SOLR = "solr";

    private static DocumentFinderProvider instance;

    public static DocumentFinderProvider instance() {
        if(instance == null) {
            instance = new DocumentFinderProvider();
        }
        return instance;
    }

    public DocumentFinder provide(QueryForm queryForm) throws Exception {
    	return provide(queryForm, false);
    }
    
    public DocumentFinder provide(QueryForm queryForm, boolean searchByDBAnyway) throws Exception {
        String provide = AdditionManager.getPropertySafe("officeDocumentFinderProvider");
        if(searchByDBAnyway || provide.equals(PROVIDE_DB)) {
            return new DocumentDbFinder(queryForm);
        } else if(provide.equals(PROVIDE_SOLR)) {
            return new DocumentSolrFinder(queryForm);
        } else {
            throw new Exception("Unknown provider for "+provide);
        }
    }
}
