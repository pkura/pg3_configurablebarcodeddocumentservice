package pl.compan.docusafe.core.base.document;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DocumentDbFinder extends DocumentFinder {

    private static final Logger log = LoggerFactory.getLogger(DocumentDbFinder.class);

    private FromClause from;
    private TableAlias doc;
    private TableAlias perm;
    private TableAlias attachment;
    private WhereClause where;
    private TableAlias changelogTable;
    private OrderClause orderClause;
    private SelectClause selectId;
    private SelectClause selectCount;
    private SelectColumn idCol;
    private SelectColumn countCol;

    public DocumentDbFinder(QueryForm queryForm) {
        super(queryForm);
    }

    @Override
    public SearchResults find() throws Exception {
        prepareQuery();
        return getResults();
    }

    protected SearchResults getResults() throws EdmException {
        ResultSet rs = null;
        try
        {
            SelectQuery query = new SelectQuery(selectCount, from, where, null);

            rs = query.resultSet();
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby dokument�w.");

            int totalCount = rs.getInt(countCol.getPosition());

            rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            query = new SelectQuery(selectId, from, where, orderClause);
            query.setMaxResults(queryForm.getLimit());
            query.setFirstResult(queryForm.getOffset());

            //rs = ps.executeQuery();
            rs = query.resultSet();


            List<Document> results = new ArrayList<Document>(queryForm.getLimit());

            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(DSApi.context().load(Document.class, id));
            }

            results = filter(results);

            return new SearchResultsAdapter<Document>(results, queryForm.getOffset(), totalCount, Document.class);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            try {
                if(rs != null) {
                    DbUtils.closeQuietly(rs.getStatement());
                }
            } catch (SQLException e) {
                throw new EdmSQLException(e);
            } finally {
                DbUtils.closeQuietly(rs);
            }
        }
    }

    public void prepareQuery() throws EdmException
    {
        from = new FromClause();
        doc = from.createTable(DSApi.getTableName(Document.class), true);
        perm = null;
//        if (!DSApi.context().isAdmin())
//            perm = from.createTable(DSApi.getTableName(DocumentPermission.class), true);
        attachment = null;
        if (queryForm.hasProperty("barcode"))
            attachment = from.createTable(DSApi.getTableName(Attachment.class), true);

        where = new WhereClause();

        changelogTable = null;
        if (queryForm.hasProperty("accessedAs") || queryForm.hasProperty("accessedBy"))
        {
            changelogTable = from.createTable("ds_document_changelog", true);
            where.add(Expression.eqAttribute(doc.attribute("id"),
                    changelogTable.attribute("document_id")));
        }

        if (queryForm.hasProperty("doctypeIds"))
        {
            Long[] ids = (Long[]) queryForm.getProperty("doctypeIds");
            where.add(Expression.in(doc.attribute("doctype"), ids));
        }

        if (queryForm.hasProperty("documentKindIds"))
        {
            Long[] ids = (Long[]) queryForm.getProperty("documentKindIds");
            String dockindCol = DSApi.getColumnName(Document.class, "documentKind");
            where.add(Expression.in(doc.attribute(dockindCol), ids));
        }

        if (queryForm.hasProperty("title"))
        {
            String title = (String) queryForm.getProperty("title");
            String titleCol = DSApi.getColumnName(Document.class, "title");
            where.add(Expression.like(Function.upper(doc.attribute(titleCol)), title.toUpperCase()+"%"));
        }

        if (queryForm.hasProperty("description"))
        {
            String description = (String) queryForm.getProperty("description");
            String descriptionCol = DSApi.getColumnName(Document.class, "description");
            where.add(Expression.like(Function.upper(doc.attribute(descriptionCol)), description.toUpperCase()+"%"));
        }

        if (queryForm.hasProperty("startCtime"))
        {
            String ctimeCol = DSApi.getColumnName(Document.class, "ctime");
            where.add(Expression.ge(doc.attribute(ctimeCol), queryForm.getProperty("startCtime")));
        }

        if (queryForm.hasProperty("endCtime"))
        {
            String ctimeCol = DSApi.getColumnName(Document.class, "ctime");
            where.add(Expression.le(doc.attribute(ctimeCol), queryForm.getProperty("endCtime")));
        }

        if (queryForm.hasProperty("startMtime"))
        {
            String ctimeCol = DSApi.getColumnName(Document.class, "mtime");
            where.add(Expression.ge(doc.attribute(ctimeCol), queryForm.getProperty("startMtime")));
        }

        if (queryForm.hasProperty("endMtime"))
        {
            String ctimeCol = DSApi.getColumnName(Document.class, "mtime");
            where.add(Expression.le(doc.attribute(ctimeCol), queryForm.getProperty("endMtime")));
        }

        if (queryForm.hasProperty("author"))
        {
            String authorCol = DSApi.getColumnName(Document.class, "author");
            where.add(Expression.eq(doc.attribute(authorCol), queryForm.getProperty("author")));
        }

        if (queryForm.hasProperty("barcode"))
        {
            where.add(Expression.eqAttribute(attachment.attribute("document_id"),
                    doc.attribute("id")));
            where.add(Expression.eq(attachment.attribute("barcode"), queryForm.getProperty("barcode")));
        }

        if (queryForm.hasProperty("boxId"))
        {
            Long boxId = (Long) queryForm.getProperty("boxId");
            String col = DSApi.getColumnName(Document.class, "box");
            where.add(Expression.eq(doc.attribute(col), boxId));
        }
        if(queryForm.hasProperty("lastRemark")){
            String lastRemarkCol = DSApi.getColumnName(Document.class, "lastRemark");
            where.add(Expression.like(doc.attribute(lastRemarkCol),"%"+ queryForm.getProperty("lastRemark").toString()+"%"));
        }

        if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu")){
            where.add(Expression.eq(doc.attribute("czy_Aktualny"), Boolean.TRUE));
        }
        if(queryForm.hasProperty("isArchived")){
            String archived = DSApi.getColumnName(Document.class, "isArchived");
            where.add(Expression.eq(doc.attribute(archived), queryForm.getProperty("isArchived")));
        }
        if (queryForm.hasProperty("archDateTo"))
        {
            String ctimeCol = DSApi.getColumnName(Document.class, "archivedDate");
            where.add(Expression.le(doc.attribute(ctimeCol), queryForm.getProperty("archDateTo")));
        }
        if (queryForm.hasProperty("archDateTo"))
        {
            String ctimeCol = DSApi.getColumnName(Document.class, "archivedDate");
            where.add(Expression.ge(doc.attribute(ctimeCol), queryForm.getProperty("archDateFrom")));
        }
        if (!DSApi.context().isAdmin())
        {
            where.add(Expression.eq(doc.attribute("deleted"), Boolean.FALSE));
        }

        if (queryForm.hasProperty("accessedBy"))
        {
            where.add(Expression.eq(changelogTable.attribute("username"),
                    queryForm.getProperty("accessedBy")));
        }
        if (queryForm.hasProperty("documentBarcode"))
        {
            String documentBarcode = (String) queryForm.getProperty("documentBarcode");
            String documentBarcodeCol = DSApi.getColumnName(Document.class, "documentBarcode");
            where.add(Expression.like(Function.upper(doc.attribute(documentBarcodeCol)), documentBarcode.toUpperCase()+"%"));
        }
        String[] accessedAs = (String[]) queryForm.getProperty("accessedAs");

        if (accessedAs != null && accessedAs.length > 0)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < accessedAs.length; i++)
            {
                OR.add(Expression.eq(changelogTable.attribute("what"),
                        accessedAs[i]));
            }
            where.add(OR);
        }



        selectId = new SelectClause(true);
        selectCount = new SelectClause();

        idCol = selectId.add(doc, "id");
        countCol = selectCount.addSql("count(distinct "+doc.getAlias()+".id)");

        orderClause = new OrderClause();

        if (queryForm.hasSortFields())
        {
            for (int i=0; i < queryForm.getSortFields().size(); i++)
            {
                QueryForm.SortField field = (QueryForm.SortField) queryForm.getSortFields().get(i);
                orderClause.add(doc.attribute(DSApi.getColumnName(Document.class, field.getName())),
                        field.isAscending() ? OrderClause.ASC : OrderClause.DESC);
                // przy SELECT DISTINCT pola z ORDER BY musz� znale�� si� w SELECT
                selectId.add(doc, DSApi.getColumnName(Document.class, field.getName()));
            }
        }
    }


}
