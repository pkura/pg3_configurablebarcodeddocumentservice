package pl.compan.docusafe.core.base.document;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.rest.RestConfiguration;
import pl.compan.docusafe.rest.solr.DocumentSolrServer;
import pl.compan.docusafe.rest.solr.LuceneQuery;
import pl.compan.docusafe.rest.solr.LuceneQuery.PropertyType;
import pl.compan.docusafe.rest.solr.SolrDocumentFinder;
import pl.compan.docusafe.service.lucene.NewLuceneService;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;

import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.util.*;

public class DocumentSolrFinder extends DocumentFinder {

    private static final Logger log = LoggerFactory.getLogger(DocumentSolrFinder.class);
    
    public static final List<String> DEFAULT_FULL_TEXT_SEARCH_FIELDS = Arrays.asList("id", "authorName", "ctimeString", "documentDateString", "caseDocumentId", "barcode", "officeNumber", "currentAssignmentUsername", "description", "postalRegNumber", "content");
    public static final List<String> DEFAULT_SMALL_TEXT_SEARCH_FIELDS = Arrays.asList("content", "title", "description", "author", "lastRemark");
    
    private final SolrServer solr;
    
    protected List<String> fullTextHighlightedFields = DEFAULT_SMALL_TEXT_SEARCH_FIELDS;
    
    protected List<String> fullTextSearchFields = DEFAULT_SMALL_TEXT_SEARCH_FIELDS;

    public DocumentSolrFinder(QueryForm queryForm) throws MalformedURLException {
        super(queryForm);
        solr = getSolrServer();
        
        if(queryForm.hasProperty("highlightedFields")){
        	this.fullTextHighlightedFields = (List<String>) queryForm.getProperty("highlightedFields");
        }
        if(queryForm.hasProperty("fullTextSearchFields")){
        	this.fullTextSearchFields = (List<String>) queryForm.getProperty("fullTextSearchFields");
        }
        
        // convert to UTF-8
        if("ISO-8859-2".equals(queryForm.getEncoding())) {
            convertToUtf8(this.queryForm);
        }
    }

    protected void convertToUtf8(QueryForm queryForm) {
        Map<String, Object> properties = queryForm.getProperties();
        Set<Map.Entry<String, Object>> entries = properties.entrySet();
        for(Map.Entry<String, Object> entry: entries) {
            Object v = entry.getValue();
            if(v instanceof String) {
                properties.put(entry.getKey(), NewLuceneService.reverseMagic(((String) v).getBytes()));
            }
        }
    }

    @Override
    public SearchResults find() throws Exception {
        SolrQuery solrQuery = getSolrQuery();
        return getResults(solrQuery);
    }

    private SearchResultsAdapter<Document> getResults(SolrQuery solrQuery) throws SolrServerException, EdmException {
        QueryResponse response = solr.query(solrQuery);

        log.debug("[find] response = {}", response);

        Long numFound = response.getResults().getNumFound();
        log.debug("[getResults] numFound = {}", numFound);

        List<Document> results = getDocumentsFromQueryResponse(response);

        return new SearchResultsAdapter<Document>(results, queryForm.getOffset(), numFound.intValue(), Document.class);
    }

    private List<Document> getDocumentsFromQueryResponse(QueryResponse response) throws EdmException {
        Iterator<SolrDocument> it = response.getResults().iterator();

        ImmutableList<Long> ids = FluentIterable.from(response.getResults()).transform(new Function<SolrDocument, Long>() {
            @Nullable
            @Override
            public Long apply(@Nullable SolrDocument input) {
                return Long.valueOf((String) input.getFieldValue("id"));
            }
        }).toList();

        log.debug("[getDocumentsFromQueryResponse] ids = {}", ids);


        List<Document> results = new ArrayList<Document>();
        for(Long id: ids) {
            Document document = DSApi.context().load(Document.class, id);

            if(response.getHighlighting() != null){
            	Map<String, List<String>> hightlighting = response.getHighlighting().get(id.toString());
            	addHighlighting(document, hightlighting);
            }

            results.add(document);
        }
        results = filter(results);

        return results;
    }

    private SolrQuery getSolrQuery() {
        String luceneQuery = getLuceneQuery();

        //luceneQuery = NewLuceneService.reverseMagic(luceneQuery.getBytes());

        log.debug("[find] luceneQuery = {}", luceneQuery);

        SolrQuery query = new SolrQuery();

        if(queryForm.hasSortFields())
        {
            ImmutableList list = FluentIterable.from(queryForm.getSortFields()).transform(new com.google.common.base.Function<QueryForm.SortField, SolrQuery.SortClause>() {
                @Nullable
                @Override
                public SolrQuery.SortClause apply(@Nullable QueryForm.SortField sortField) {
                	String sortFieldName = sortField.getName();
                	
                	if(sortFieldName.equals("id")){
                		sortFieldName = "id_long";
                	}
                	else if(sortFieldName.equals("title")){
                		sortFieldName = "title_keyword";
                	}
                	else if(sortFieldName.equals("officeNumber")){
                		sortFieldName = "officeNumberInt";
                	}
                	
                    return sortField.isAscending() ? SolrQuery.SortClause.asc(sortFieldName) : SolrQuery.SortClause.desc(sortFieldName);
                }
            }).toList();

            query.setSorts(list);
        }

        query.setRows(queryForm.getLimit());
        query.setStart(queryForm.getOffset());

        query.setQuery(luceneQuery);
        
        if(!queryForm.hasProperty("disableSolrHighlight")){
        	query.setHighlight(true).setHighlightSnippets(2).setParam("hl.fl", StringUtils.join(fullTextHighlightedFields, ","));
        }

        return query;
    }

    private SolrServer getSolrServer() throws MalformedURLException {
        return new RestConfiguration().documentSolrServer();
    }

    private String getLuceneQuery() {
        LuceneQuery query = new LuceneQuery(LuceneQuery.Mode.PARANTHESES);

        if(queryForm.hasProperty("barcode")) {
        	String barcode = (String) queryForm.getProperty("barcode");
            query.stack(query.eqLike("barcode_keyword", barcode, LuceneQuery.PropertyType.STRING));
        }

        if(queryForm.hasProperty("accessedAs"))
        {
            String[] accessedAs = (String[]) queryForm.getProperty("accessedAs");
            query.stack(query.in("accessedAs", accessedAs, LuceneQuery.PropertyType.STRING));
        }

        if(queryForm.hasProperty("accessedBy"))
        {
            String accessedBy = (String) queryForm.getProperty("accessedBy");
            query.stack(query.eq("accessedBy", accessedBy, LuceneQuery.PropertyType.STRING));
        }

        if(queryForm.hasProperty("doctypeIds"))
        {
            throw new UnsupportedOperationException("doctypeIds is not supported");
        }

        if (queryForm.hasProperty("boxId"))
        {
            throw new UnsupportedOperationException("boxId is not supported");
        }


        if (queryForm.hasProperty("documentKindIds"))
        {
            Object documentKindIdsObj = queryForm.getProperty("documentKindIds");
            List<Long> documentKindIds;
            if(documentKindIdsObj instanceof List) {
                documentKindIds = (List<Long>) documentKindIdsObj;
            } else if(documentKindIdsObj instanceof Long[]) {
                documentKindIds = Arrays.asList((Long[])documentKindIdsObj);
            } else {
                documentKindIds = Arrays.asList((Long)documentKindIdsObj);
            }
            Collection<String> documentKindIdsStr = FluentIterable.from(documentKindIds)
                    .filter(new Predicate<Long>() {
                        @Override
                        public boolean apply(@Nullable Long input) {
                            return input != null;
                        }
                    })
                    .transform(new Function<Long, String>() {
                        @Nullable
                        @Override
                        public String apply(@Nullable Long input) {
                            return input == null ? "0" : input.toString();
                        }
                    }).toList();
            query.stack(query.in("documentKindId", documentKindIdsStr, LuceneQuery.PropertyType.INTEGER));
        }

        if (queryForm.hasProperty("title"))
        {
            String title = (String) queryForm.getProperty("title");
            query.stack(query.eqLike("title", title, LuceneQuery.PropertyType.STRING, false));
        }

        if (queryForm.hasProperty("description"))
        {
            String description = (String) queryForm.getProperty("description");
            query.stack(query.eqLike("description", description, LuceneQuery.PropertyType.STRING, false));
        }

        if (queryForm.hasProperty("startCtime"))
        {
            Date startCtime = (Date)queryForm.getProperty("startCtime");
            query.stack(query.gte("ctime", startCtime, LuceneQuery.PropertyType.DATETIME));
        }

        if (queryForm.hasProperty("endCtime"))
        {
            Date endCtime = (Date)queryForm.getProperty("endCtime");
            query.stack(query.lte("ctime", endCtime, LuceneQuery.PropertyType.DATETIME));
        }

        if (queryForm.hasProperty("startMtime"))
        {
            Date startMtime = (Date)queryForm.getProperty("startMtime");
            query.stack(query.gte("mtime", startMtime, LuceneQuery.PropertyType.DATETIME));
        }

        if (queryForm.hasProperty("endMtime"))
        {
            Date endMtime = (Date)queryForm.getProperty("endMtime");
            query.stack(query.lte("mtime", endMtime, LuceneQuery.PropertyType.DATETIME));
        }
        if (queryForm.hasProperty("author"))
        {
        	if(queryForm.getProperty("author") instanceof String[]){
        		LuceneQuery innerQuery = new LuceneQuery(LuceneQuery.Mode.QUOTE);
            	
                for(String author: (String[]) queryForm.getProperty("author")) {
                    innerQuery.stack(innerQuery.eq("author", author, LuceneQuery.PropertyType.STRING));
                }
                
                innerQuery.orStack();
                
                query.stack(innerQuery.toString());
        	}
        	else{
        		String author = (String) queryForm.getProperty("author");
        		query.stack(query.eq("author", author, LuceneQuery.PropertyType.STRING));
        	}
        }

        if(queryForm.hasProperty("lastRemark")){
            String lastRemark = (String) queryForm.getProperty("lastRemark");
            query.stack(query.eqLike("lastRemark", lastRemark, LuceneQuery.PropertyType.STRING, false));
        }

        if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu")){
            throw new UnsupportedOperationException("kancelaria.wlacz.wersjeDokumentu is not supported");
        }

        if(queryForm.hasProperty("isArchived")){
            Boolean isArchived = (Boolean) queryForm.getProperty("isArchived");
            query.stack(query.eq("isArchived", isArchived.toString(), LuceneQuery.PropertyType.BOOLEAN));
        }

        if (queryForm.hasProperty("archDateTo"))
        {
            Date archDateTo = (Date)queryForm.getProperty("archDateTo");
            query.stack(query.lte("archivedDate", archDateTo, LuceneQuery.PropertyType.DATETIME));
        }

        if (queryForm.hasProperty("archDateFrom"))
        {
            Date archDateFrom = (Date)queryForm.getProperty("archDateFrom");
            query.stack(query.gte("archivedDate", archDateFrom, LuceneQuery.PropertyType.DATETIME));
        }

        if(queryForm.hasProperty("content")) {

            LuceneQuery.Mode mode;
            if(queryForm.hasProperty("contentWordOrder") && ((Boolean)queryForm.getProperty("contentWordOrder")) == true) {
                mode = LuceneQuery.Mode.QUOTE;
            } else {
                mode = LuceneQuery.Mode.PARANTHESES;
            }

            LuceneQuery innerQuery = new LuceneQuery(mode);
            for(String fullTextField: fullTextSearchFields) {
            	if(mode == LuceneQuery.Mode.PARANTHESES){
            		innerQuery.stack(innerQuery.eqLike(fullTextField, (String)queryForm.getProperty("content"), LuceneQuery.PropertyType.STRING, false));
            	}
            	else{
            		innerQuery.stack(innerQuery.eq(fullTextField, (String)queryForm.getProperty("content"), LuceneQuery.PropertyType.STRING));
            	}
            }
            innerQuery.orStack();
            query.stack(innerQuery.toString());
        }
        
        //Pole full text search
        if(queryForm.hasProperty("full_text")){
        	String fullText = (String) queryForm.getProperty("full_text");
        	fullText = fullText.replaceAll(":", " ");
        	
        	LuceneQuery innerQuery = new LuceneQuery(LuceneQuery.Mode.PARANTHESES);
        	
            for(String fullTextSearchField: fullTextSearchFields) {
                innerQuery.stack(innerQuery.eqLike(fullTextSearchField, fullText, LuceneQuery.PropertyType.STRING, false));
            }
            
            innerQuery.orStack();
            
            query.stack(innerQuery.toString());
        }

        if (!DSApi.context().isAdmin())
        {
            query.stack(query.eq("deleted", "false", LuceneQuery.PropertyType.BOOLEAN));
        }

        query.andStack();

        return query.toString();
    }

    /**
     * TODO: highlighting powinno byc w oddzielnej strukturze danych, niezaleznie od wartosci.
     * @param document
     * @param highlights
     */
    protected void addHighlighting(Document document, Map<String, List<String>> highlights) {

        if(highlights == null) {
            return;
        }

        List<String> contentHighlight = highlights.get("content");
        List<String> titleHighlight = highlights.get("title");
        List<String> descriptionHighlight = highlights.get("description");
        List<String> lastRemarkHighlight = highlights.get("lastRemark");
//        List<String> authorHighlight = highlights.get("author");

        if(contentHighlight != null) {
            // super ugly hack
            // abstractDescription is used to store full text content data
            document.setAbstractDescription(StringUtils.join(contentHighlight, ", "));
        }

        if(lastRemarkHighlight != null) {
            document.setLastRemark(StringUtils.join(lastRemarkHighlight, ", "));
        }

//        if(authorHighlight != null) {
//            document.setAuthor(StringUtils.join(authorHighlight, ", "));
//        }

        if(descriptionHighlight != null) {
            document.setDescriptionNoCheck(StringUtils.join(descriptionHighlight, ", "));
        }

        if(titleHighlight != null) {
            document.setTitleNoCheck(StringUtils.join(titleHighlight, ", "));
        }
    }

}
