package pl.compan.docusafe.core.base.document;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import pl.compan.docusafe.rest.RestConfiguration;
import pl.compan.docusafe.rest.solr.ContainerSolrServer;
import pl.compan.docusafe.rest.solr.DocumentSolrServer;
import pl.compan.docusafe.rest.solr.LuceneQuery;
import pl.compan.docusafe.rest.views.DocumentView;
import pl.compan.docusafe.rest.views.RichDocumentView;

import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.util.*;

/**
 * <p>
 *     Bezpo?rednie query do Apache Solr
 * </p>
 */
public class DocumentSmartFinder {

    private static DocumentSmartFinder instance;

    public static DocumentSmartFinder instance() {
        if (instance == null) {
            instance = new DocumentSmartFinder();
        }
        return instance;
    }

    public List<RichDocumentView> find(String content, Integer limit, Integer offset) throws SolrServerException, MalformedURLException {
        SolrServer solrServer = getSolrServer();
        SolrQuery solrQuery = getSolrQuery(content, offset, limit);
        QueryResponse response = solrServer.query(solrQuery);

        Iterator<SolrDocument> it = response.getResults().iterator();

        Map<String, Map<String, List<String>>> highlightingMap = response.getHighlighting();

        List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

        while (it.hasNext()) {
            SolrDocument solrDocument = it.next();
            Map<String, Object> fieldValueMap = solrDocument.getFieldValueMap();
            Map<String, List<String>> hmap = highlightingMap.get(fieldValueMap.get("id").toString());
            fieldValueMap = addHighlighting(fieldValueMap, hmap);
            results.add(fieldValueMap);
        }

        ImmutableList<RichDocumentView> ret = FluentIterable.from(results).transform(new Function<Map<String, Object>, RichDocumentView>() {
            @Nullable
            @Override
            public RichDocumentView apply(@Nullable Map<String, Object> values) {
                return new RichDocumentView(values);
            }
        }).toList();

        return new ArrayList<RichDocumentView>(ret);
    }

    public SolrDocument find(Long id) throws SolrServerException, MalformedURLException {
        SolrServer solrServer = getSolrServer();
        LuceneQuery query = new LuceneQuery(LuceneQuery.Mode.PARANTHESES);
        query.stack(query.eq("id", id.toString(), LuceneQuery.PropertyType.ID));

        query.orStack();
        String queryString = query.toString();

        SolrQuery solrQuery = new SolrQuery();

        solrQuery.setRows(1);
        solrQuery.setStart(0);
        solrQuery.setQuery(queryString);

        QueryResponse response = solrServer.query(solrQuery);

        Iterator<SolrDocument> it = response.getResults().iterator();

        it.hasNext();
        return it.next();
    }

    private Map<String, Object> addHighlighting(Map<String, Object> fieldValueMap, Map<String, List<String>> highlighting) {
        Map<String, Object> ret = new HashMap<String, Object>();
        for(String key: fieldValueMap.keySet()) {
            List<String> h = highlighting.get(key);
            if(h != null) {
                ret.put(key, StringUtils.join(h, ", "));
            } else {
                // nie umieszczamy calego contentu, tylko highlighting
                if(! "content".equals(key)) {
                    ret.put(key, fieldValueMap.get(key));
                }
            }
        }
        return ret;
    }

    protected SolrQuery getSolrQuery(String content, Integer offset, Integer limit) {
        LuceneQuery query = new LuceneQuery(LuceneQuery.Mode.PARANTHESES);
        query.stack(query.eq("title", content, LuceneQuery.PropertyType.STRING));
        query.stack(query.eq("description", content, LuceneQuery.PropertyType.STRING));
        query.stack(query.eqLike("documentKind", content, LuceneQuery.PropertyType.STRING));
        query.stack(query.eqLike("content", content, LuceneQuery.PropertyType.STRING));
        query.stack(query.eqLike("author", content, LuceneQuery.PropertyType.STRING));
        query.orStack();
        String queryString = query.toString();

        SolrQuery solrQuery = new SolrQuery();

        solrQuery.setRows(limit);
        solrQuery.setStart(offset);
        solrQuery.addSort(SolrQuery.SortClause.desc("score"));
        solrQuery.addSort(SolrQuery.SortClause.desc("ctime"));
        solrQuery.addSort(SolrQuery.SortClause.desc("mtime"));
        solrQuery.setQuery(queryString);

        solrQuery.setHighlight(true).setHighlightSnippets(2).setParam("hl.fl", "title,description,content,documentKind");

        return solrQuery;
    }

    protected SolrServer getSolrServer() throws MalformedURLException {
        return new RestConfiguration().documentSolrServer();
    }
}

