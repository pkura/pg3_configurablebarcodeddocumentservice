package pl.compan.docusafe.core.base.document;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public abstract class DocumentFinder {

    private static final Logger log = LoggerFactory.getLogger(DocumentFinder.class);

    protected final QueryForm queryForm;

    public DocumentFinder(QueryForm queryForm) {
        this.queryForm = queryForm;
    }


    public abstract SearchResults find() throws Exception;

    /**
     * <p>
     *     Filter documents. Throw out documents that user doesn't have access to.
     * </p>
     * @param results
     * @return
     */
    protected List<Document> filter(List<Document> results) {
        return FluentIterable.from(results).filter(new Predicate<Document>() {
            @Override
            public boolean apply(@Nullable Document document) {
                try {
                    return PermissionManager.getInstance().canRead(document, null);
                } catch (EdmException ex) {
                    log.error("[filter] error filtering document doc.id = {}", document.getId(), ex);
                    return false;
                }
            }
        }).toList();
    }
}
