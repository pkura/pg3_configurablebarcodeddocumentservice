package pl.compan.docusafe.core.base;



import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * Klasa odwzorowuj�ca tabelk� DSO_USER_TO_BRIEFCASE
 * 
 * @author Grzegorz Filip
 */
public class UserToBriefcase implements Serializable
{
	
	private Logger log = LoggerFactory.getLogger(UserToBriefcase.class);
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id
	 */
	private Long id;
	
	/**
	 * id teczki 
	 */
	private Long containerId;
	
	/**
	 * nazawa teczki 
	 */
	private String containerOfficeId;
	
	/**id u�ytkownika 
	 */
	private Long userId;
	
	/**
	 * Nazwa uzytkownika
	 */
	
	
	private String userName;
	
	/**
	 *Imi�
	 */
	private String userFirstname;
	
	/**
	 * nazwisko
	 */
	private String userLastname;
	
	
	
	
	/**
	 * ustala czy mo�e wejs� do  tej teczki
	 */
	private Boolean canView;
	/**
	 *ustala czy mo�e tworzy� podteczki w tej teczce
	 */
	private Boolean canCreate;
	/**
	 * ustala czy mo�e modyfikowa� teczce
	 */
	private Boolean canModify;
	/**
	 * ustala czy mo�e wydrukowa� sprawy w tej teczce
	 */
	private Boolean canPrint;
	/**
	 * ustala czy mo�e podgl�da� sprawy w tej teczce
	 */
	private Boolean canViewCases;
	
	/**
	 * ustala czy mo�e dodawa� sprawy do tej teczki
	 */
	private Boolean canAddCasestoBriefcase;
	
	
	
	/**
	 * Domy�lny konstruktor
	 */
	public UserToBriefcase() 
	{
	}

	/**
	 * KOnstruktor ustawiaj�cy pola
	 * @param id
	 * @param containerId
	 * @param containerOfficeId
	 * @param userId
	 * @param userName
	 * @param userFirstname
	 * @param userLastname
	 * @param canViev
	 * @param canCreate
	 * @param canModify
	 * @param canPrint
	 * @param canViewCases
	 * @param canAddCasesToBriefcase
	 */

	public UserToBriefcase(Long id, Long containerId, String containerOfficeId,
			Long userId, String userName, String userFirstname,
			String userLastname, Boolean canViev, Boolean canCreate,
			Boolean canModify, Boolean canPrint, Boolean canViewCases,
			Boolean canAddCasesToBriefcase) {
		super();
		this.id = id;
		this.containerId = containerId;
		this.containerOfficeId = containerOfficeId;
		this.userId = userId;
		this.userName = userName;
		this.userFirstname = userFirstname;
		this.userLastname = userLastname;
		this.canView = canViev;
		this.canCreate = canCreate;
		this.canModify = canModify;
		this.canPrint = canPrint;
		this.canViewCases = canViewCases;
		this.canAddCasestoBriefcase = canAddCasestoBriefcase;
	}
	
	
	public void delete() throws Exception
    {
    	log.debug("deleting " + this);
        try
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
            if(AvailabilityManager.isAvailable("absence.createEventOnCreate"))
            {
            	CalendarFactory.getInstance().deleteEventsByRelatedAbsence(getId());
            }
        }
        catch (HibernateException e)
        {
            log.error("Blad usuniecia wpisu");
            throw new EdmException("Blad usuniecia wpisu. "+e.getMessage());
        }
    }
    

	public void create() throws Exception, EdmException
	{
		log.debug("create()");
		
        DSApi.context().session().save(this);
        DSApi.context().session().flush(); 
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getContainerId() {
		return containerId;
	}

	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}

	public String getContainerOfficeId() {
		return containerOfficeId;
	}

	public void setContainerOfficeId(String containerOfficeId) {
		this.containerOfficeId = containerOfficeId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserFirstname() {
		return userFirstname;
	}

	public void setUserFirstname(String userFirstname) {
		this.userFirstname = userFirstname;
	}

	public String getUserLastname() {
		return userLastname;
	}

	public void setUserLastname(String userLastname) {
		this.userLastname = userLastname;
	}

	public Boolean getCanView() {
		return canView;
	}

	public void setCanView(Boolean canViev) {
		this.canView = canViev;
	}

	public Boolean getCanCreate() {
		return canCreate;
	}

	public void setCanCreate(Boolean canCreate) {
		this.canCreate = canCreate;
	}

	public Boolean getCanModify() {
		return canModify;
	}

	public void setCanModify(Boolean canModify) {
		this.canModify = canModify;
	}

	public Boolean getCanPrint() {
		return canPrint;
	}

	public void setCanPrint(Boolean canPrint) {
		this.canPrint = canPrint;
	}

	public Boolean getCanViewCases() {
		return canViewCases;
	}

	public void setCanViewCases(Boolean canViewCases) {
		this.canViewCases = canViewCases;
	}

	public Boolean getCanAddCasestoBriefcase()
	{
		return canAddCasestoBriefcase;
	}

	
	public void setCanAddCasestoBriefcase(Boolean canAddCasestoBriefcase)
	{
		this.canAddCasestoBriefcase = canAddCasestoBriefcase;
	}
	
	/**
	 * Konstruktor ustawiaj�cy wszystkie pola
	 * 
	 * @param kPX
	 * @param company
	 * @param position
	 * @param department
	 * @param employmentStartDate
	 * @param employmentEndDate
	 * @param info
	 * @param user
	 */
/*	public UserToBriefcase(String kPX, String company, String position,
			String department, Date employmentStartDate,
			Date employmentEndDate, String info, UserImpl user) 
	{
		super();
		KPX = kPX;
		this.company = company;
		this.position = position;
		this.department = department;
		this.employmentStartDate = employmentStartDate;
		this.employmentEndDate = employmentEndDate;
		this.info = info;
		this.user = user;
	}

	*/
        
}
