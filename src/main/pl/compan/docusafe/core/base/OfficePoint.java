package pl.compan.docusafe.core.base;


import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * Klasa odwzorowująca tabelkę DSO_OFFICE_POINT
 *  określająca punkt kancelaryjny 
 * @author grzegorz.filip@docusafe.pl
 */
public class OfficePoint implements Serializable {

	
	private Logger log = LoggerFactory.getLogger(OfficePoint.class);
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id
	 */
	private Long id;
	
	/**
	 * prefix punktu 
	 */
	private String prefix;
	/**
	 * id prefix punktu 
	 */
	private Long prefixId;
	
	/**
	 * Nazwa folderu na maszynie ocr
	 */
	
	private String folderName;


	/**id dzialu na ktory ma byc dekretacja 
	 */
	private Long divisionId;
	
	private boolean deleted ;
	
	public OfficePoint() 
	{
	}


/**
 * Konstruktor ustawiajacy pola 
 * @param id
 * @param prefix
 * @param prefixId
 * @param folderName
 * @param divisionId
 * @param deleted
 */
	public OfficePoint(Long id, String prefix, Long prefixId, String folderName,
			Long divisionId, boolean deleted) {
		
		this.id = id;
		this.prefix = prefix;
		this.prefixId = prefixId;
		this.folderName = folderName;
		this.divisionId = divisionId;
		this.deleted = deleted;
	}



	public void delete() throws Exception
    {
    	log.debug("deleting " + this);
        try
        {
            this.deleted = true;
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            log.error("Blad usuniecia wpisu");
            throw new EdmException("Blad usuniecia wpisu. "+e.getMessage());
        }
    }
    

	


	public void create() throws Exception, EdmException
	{
		log.debug("create()");
		 this.deleted = false;
        DSApi.context().session().save(this);
        DSApi.context().session().flush(); 
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getPrefix() {
		return prefix;
	}


	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}


	public String getFolderName() {
		return folderName;
	}


	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}


	public Long getDivisionId() {
		return divisionId;
	}


	public void setDivisionId(Long divisionId) {
		this.divisionId = divisionId;
	}


	public boolean isDeleted() {
		return deleted;
	}


	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}


	public Long getPrefixId() {
		return prefixId;
	}


	public void setPrefixId(Long prefixId) {
		this.prefixId = prefixId;
	}


	public static OfficePoint findByPrefix(String filePrefix) throws EdmException {
		Criteria crit = DSApi.context().session().createCriteria(OfficePoint.class);
		crit.add(Restrictions.eq("prefix", filePrefix));
		crit.add(Restrictions.eq("deleted", false));
		if (!crit.list().isEmpty())
			return (OfficePoint) crit.list().get(0);
		else
			return null;
		
		// TODO Auto-generated method stub
		
	}





}
