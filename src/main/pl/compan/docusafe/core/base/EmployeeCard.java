package pl.compan.docusafe.core.base;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.general.hibernate.dao.DSDivisionFactory;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimesSummary;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeCard;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.admin.dictionaries.DockindDictionaries;
import pl.compan.docusafe.web.admin.employee.EmployeeCardListAction;

/**
 * Klasa odwzorowuj�ca tabelk� DS_EMPLOYEE_CARD
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EmployeeCard implements Serializable {
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory
			.getLogger(EmployeeCard.class);

	/**
	 * Identyfikator karty pracownika
	 */
	private Long id;

	/**
	 * Numer ewidencyjny
	 */
	private String KPX;

	/**
	 * Nazwa sp�ki, do kt�rej nale�y dana karta
	 */
	private String company;

	/**
	 * Id of position on dictionary
	 */
	private String position;

	/**
	 * Id of department
	 */
	private String department;

	/**
	 * Id of division
	 */
	private String division;

	/**
	 * Data, od kt�rej trwa zatrudnienie pracownika
	 */
	private Date employmentStartDate;

	/**
	 * Data zwolnienia pracownika
	 */
	private Date employmentEndDate;

	/**
	 * Uwagi dodatkowe do karty pracownika
	 */
	private String info;

	/**
	 * U�ytkownik, kt�ry jest zwi�zany z dan� kart� pracownika
	 */
	private UserImpl user;

	/**
	 * Urlopy pracownika
	 */
	private Set<Absence> absences;

	/**
	 * nadgodziny pracownik�w
	 */
	private Set<OvertimeCard> overtimes;

	/**
	 * Nazwa dzia�u do kt�rego przypisany jest u�ytkownika posiadaj�cy dan�
	 * kart� pracownika
	 */
	private String divisionName;

	/**
	 * ustala czy karta pracownika moze miec urlop z art. 188 Kp
	 */
	private Boolean T188KP;

	/**
	 * Maksymalna liczba dni urlopu T188 Kp w roku kalendarzowym
	 */
	private Integer t188kpDaysNum;

	/**
	 * ustala czy karta pracownika moze miec urlop ojcowski
	 */
	private Boolean TUJ;

	/**
	 * Maksymalna liczba dni urlopu UJ w roku kalendarzowym
	 */
	private Integer tujDaysNum;

	/**
	 * ustala czy karta pracownika moze miec urlop inwalidzki
	 */
	private Boolean TINW;

	/**
	 * Liczba dni urlopu inwalidzkiego dla danej karty pracownika
	 */
	private Integer invalidAbsenceDaysNum;

	/**
	 * U�ytkownik zewn�trzny (login uzytkownika)
	 */
	private String externalUser;

	private String firstName;

	private String lastName;

	/**
	 * Roczne podsumowanie nadgodzin pracownika, musi byc r�cznie ustawione,
	 * przyklady w raportach
	 */
	private EmployeeOvertimesSummary empYearsSummary;

	/**
	 * Domy�lny konstruktor
	 */
	public EmployeeCard() {
	}

	/**
	 * Konstruktor ustawiaj�cy wszystkie pola
	 * 
	 * @param kPX
	 * @param company
	 * @param position
	 * @param department
	 * @param employmentStartDate
	 * @param employmentEndDate
	 * @param info
	 * @param user
	 */
	public EmployeeCard(String kPX, String company, String position,
			String department, Date employmentStartDate,
			Date employmentEndDate, String info, UserImpl user) {
		super();
		KPX = kPX;
		this.company = company;
		this.position = position;
		this.department = department;
		this.employmentStartDate = employmentStartDate;
		this.employmentEndDate = employmentEndDate;
		this.info = info;
		this.user = user;
	}

	public void create() throws Exception, EdmException {
		DSApi.context().session().save(this);
		DSApi.context().session().flush();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKPX() {
		return KPX;
	}

	public void setKPX(String kPX) {
		KPX = kPX;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Date getEmploymentStartDate() {
		return employmentStartDate;
	}

	public void setEmploymentStartDate(Date employmentStartDate) {
		this.employmentStartDate = employmentStartDate;
	}

	public Date getEmploymentEndDate() {
		return employmentEndDate;
	}

	public void setEmploymentEndDate(Date employmentEndDate) {
		this.employmentEndDate = employmentEndDate;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public UserImpl getUser() {
		// to jest zly pomysl karta jest jakiegos uzytlkownika nawet jak go
		// usuniemy to on byla jego nie rozumiem tego rozwiazania
		// if (user == null || user.isDeleted())
		// return null;
		return user;
	}

	public void setUser(UserImpl user) {
		this.user = user;
	}

	public Set<Absence> getAbsences() {
		return absences;
	}

	public void setAbsences(Set<Absence> absences) {
		this.absences = absences;
	}

	public String getDivision() {
		return division;
	}

	/**
	 * Method returns object for Division. Remember use this method when context
	 * for db is open!!!
	 * 
	 * @return DivisionImpl object
	 * @throws EdmException
	 * @throws NumberFormatException
	 * @throws DivisionNotFoundException
	 */
	public String getDivisionNameByDivisionId() throws DivisionNotFoundException,
			NumberFormatException, EdmException {

		DSDivision divisionObject = null;
		String name = "";
		if (StringUtils.isNotEmpty(division)) {
			if (StringUtils.isNumeric(division)) {
				divisionObject = DSDivision.find(Integer.valueOf(division));
				name = divisionObject.getName();
			}
		} 
		return name;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public Boolean getT188KP() {
		return T188KP;
	}

	public void setT188KP(Boolean t188Kp) {
		T188KP = t188Kp;
	}

	public Boolean getTUJ() {
		return TUJ;
	}

	public void setTUJ(Boolean tUJ) {
		TUJ = tUJ;
	}

	public Boolean getTINW() {
		return TINW;
	}

	public void setTINW(Boolean tINW) {
		TINW = tINW;
	}

	public Integer getInvalidAbsenceDaysNum() {
		if (invalidAbsenceDaysNum == null)
			invalidAbsenceDaysNum = 0;
		return invalidAbsenceDaysNum;
	}

	public void setInvalidAbsenceDaysNum(Integer invalidAbsenceDaysNum) {
		this.invalidAbsenceDaysNum = invalidAbsenceDaysNum;
	}

	public Integer getT188kpDaysNum() {
		if (t188kpDaysNum == null)
			t188kpDaysNum = 0;
		return t188kpDaysNum;
	}

	public void setT188kpDaysNum(Integer t188kpDaysNum) {
		this.t188kpDaysNum = t188kpDaysNum;
	}

	public Integer getTujDaysNum() {
		if (tujDaysNum == null)
			tujDaysNum = 0;
		return tujDaysNum;
	}

	public void setTujDaysNum(Integer tujDaysNum) {
		this.tujDaysNum = tujDaysNum;
	}

	public String getDivisionName() throws EdmException {
		if (StringUtils.isNotEmpty(divisionName))
			return divisionName;

		if (user != null) {
			DSDivision[] divisions = user.getDivisions();
			if (divisions != null && divisions.length > 0)
				divisionName = divisions[0].getName();
		} else {
			divisionName = "Brak przypisania";
		}

		return divisionName;
	}

	public String getExternalUser() {
		return externalUser;
	}

	public void setExternalUser(String externalUser) {
		this.externalUser = externalUser;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	public Set<OvertimeCard> getOvertimes() {
		return overtimes;
	}

	public void setOvertimes(Set<OvertimeCard> overtimes) {
		this.overtimes = overtimes;
	}

	/**
	 * nalezy pobra� poza klasa i ustawi�
	 * 
	 * @return
	 */
	public EmployeeOvertimesSummary getEmpYearsSummary() {
		return empYearsSummary;
	}

	public void setEmpYearsSummary(EmployeeOvertimesSummary empYearsSummary) {
		this.empYearsSummary = empYearsSummary;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public String getDepartmentName() throws HibernateException, SQLException, NumberFormatException, EdmException {
		boolean contextOpened = false;
		String departmentName = "";
		if (StringUtils.isNotEmpty(department)) {
			if (StringUtils.isNumeric(department)) {
				try {
					contextOpened = DSApi.openContextIfNeeded();
					departmentName = DockindDictionaries.find(
							Integer.valueOf(department), "dsg_kp_departament")
							.getTitle();
				} catch (EdmException e) {
					LOG.error(
							"exception connected with open context for Dictionary class {}",
							e);
				} finally {
					DSApi.closeContextIfNeeded(contextOpened);
					LOG.info("close context for " + EmployeeCard.class
							+ " used on " + DockindDictionaries.class);
				}
			} else {
				departmentName = department;
			}
		}
		return departmentName;
	}

	public String getPositionName() throws HibernateException, SQLException, NumberFormatException, EdmException {
		boolean contextOpened = false;
		String positionName = "";
		if (StringUtils.isNotEmpty(position)) {
			if (StringUtils.isNumeric(position))
				try {
					contextOpened = DSApi.openContextIfNeeded();
					positionName = DockindDictionaries.find(
							Integer.valueOf(position), "dsg_kp_stanowisko")
							.getTitle();
				} catch (EdmException e) {
					LOG.error(
							"exception connected with open context for Dictionary class {}",
							e);
				} finally {
					DSApi.closeContextIfNeeded(contextOpened);
					LOG.info("close context for " + EmployeeCard.class
							+ " used on " + DockindDictionaries.class);
				}
			else {
				positionName = position;
			}
		}
		return positionName;
	}
}
