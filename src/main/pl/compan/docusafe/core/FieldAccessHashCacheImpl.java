package pl.compan.docusafe.core;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.*;

/**
 * Implementacja FieldAccess bazuj�ca na FieldsManagerze i cacheuj�ca modyfikacje
 * Klasa mocno powi�zana z FieldsManager
 *
 * Zawiera wszystkie informacje dotycz�ce warto�ci - obiekt enum w przypadku warto�ci s�ownikowych,
 * odpowiedni� klas� w przypadku warto�ci liczbowych (Long, Integer, Float, BigDecimal)
 *
 * Nie wspiera kluczy null, wspiera warto�ci null
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class FieldAccessHashCacheImpl extends FieldsAccessBase {
    private static final Logger LOG = LoggerFactory.getLogger(FieldAccessHashCacheImpl.class);

    private final FieldsManager fm;

    //Pola pobrane z FieldsManagera
    private final Map<String, Object> cache = Maps.newHashMap();

    //Zmienione pola - tylko te klucze  (cn p�l) b�d� zapisywane do bazy danych
    private final Set<String> changed = Sets.newHashSet();
    
    private Set<Entry<String, Object>> entrySet;

    public FieldAccessHashCacheImpl(FieldsManager fm) throws EdmException {
        this.fm = fm;
        fm.initializeValues();
    }

    public boolean containsKey(Object key) {
//        LOG.info("containsKey : {} -> {}", key, fm.containsField(key));
        return fm.containsField(key);
    }

    public boolean containsValue(Object value) {
        for(String cn: fm.getFieldsCns()){
            if(Objects.equal(value, get(cn))){
                return true;
            }
        }
        return false;
    }

    public Set<Entry<String, Object>> entrySet() {
        if(entrySet == null){
            entrySet = Sets.newHashSet();
            for(String cn: fm.getFieldsCns()){
                entrySet.add(new EntryImpl(cn));
            }
        }
        return entrySet;
    }

    /**
     * Znajduje warto�� w FieldsManager i zapisuje j� do mapy cache
     * @param cn
     * @throws NullPointerException je�eli key jest null
     */
    private void findValue(Object cn) throws NullPointerException, EdmException {
        Field field = fm.getField(cn.toString());
        cache.put(cn.toString(), field.asObject(fm.getKey(cn.toString())));
    }

    public Object get(Object key) throws NullPointerException {
        try {
            if(!cache.containsKey(key.toString())){
                findValue(key);
            }
//            LOG.info("return : {}", cache.get(key));
            return cache.get(key);
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean isEmpty() {
        return keySet().isEmpty();
    }

    public Set<String> keySet() {
        return fm.getFieldsCns();
    }

    /**
     * Pr�buje skonwertowa� obiekt value na typ obiektu previous
     * @param value
     * @param cn
     * @return
     */
    private Object tryConvert(Object value, String cn) throws EdmException {
        Field field = fm.getField(cn);
        return field.asObject(field.simpleCoerce(value));
    }

    public Object put(String key, Object value) {
        try {
            Preconditions.checkNotNull(key, "key cannot be null");
            
            Object prev = get(key);
            
            if(Objects.equal(prev, value)){
                return prev;
            }

            if(prev == null || (value != null && prev.getClass() != value.getClass())){
                //zak�adam, �e klasa zawsze jest taka sama
                value = tryConvert(value, key);
            }
            changed.add(key);
            return cache.put(key, value);
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void commitChanges() throws EdmException{
        LOG.info("commiting : " + changed);
        fm.getDocumentKind().setOnly(
                fm.getDocumentId(), 
                Maps.filterKeys(cache, Predicates.in(changed)));

    }

    public void putAll(Map<? extends String, ?> t) {
        for(Map.Entry<? extends String, ?> entry: t.entrySet()){
            put(entry.getKey(), entry.getValue());
        }
    }

    public int size() {
        return 0;
    }

    public Collection<Object> values() {
        return null;
    }

    private final class EntryImpl implements Map.Entry<String,Object>{
        private final String cn;

        private EntryImpl(String cn) {
            this.cn = cn;
        }

        public String getKey() {
            return cn;
        }

        public Object getValue() {
            return get(cn);
        }

        public Object setValue(Object value) {
            return put(cn, value);
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof EntryImpl
                && cn.equals(((EntryImpl)obj).getKey());
        }

        @Override
        public int hashCode() {
            return cn.hashCode();
        }
    }
}
