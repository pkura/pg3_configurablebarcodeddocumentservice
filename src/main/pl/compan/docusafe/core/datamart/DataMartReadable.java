package pl.compan.docusafe.core.datamart;

import java.util.Set;

import pl.compan.docusafe.core.EdmException;

public interface DataMartReadable<T> 
{
	public Set<T> read() throws EdmException;
}
