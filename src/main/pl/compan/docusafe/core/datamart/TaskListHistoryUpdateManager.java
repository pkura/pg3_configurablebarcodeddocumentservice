package pl.compan.docusafe.core.datamart;

import pl.compan.docusafe.boot.WatchDog;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.workflow.TaskHistoryEntry;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * Klasa odpowiedzialna za update informacji o historii listy zadan
 * Przeniesione w ramach porzadkow z OfficeDocument
 * @author wkutyla
 *
 */
public class TaskListHistoryUpdateManager 
{	
	
	private final static Logger log = LoggerFactory.getLogger(TaskListHistoryUpdateManager.class);
	/**
     * Dodaje nowy wpis do historii dekretacji, jednocze�nie wykonuj�c akcje zwi�zane z histori� listy zada�.
     * 
     * UWAGA: Wszystkie akcje zwi�zane z histori� listy zada� s� wykonywane w tej akcji i w przysz�o�ci
     * niezb�dne zmiany te� powinny by� tutaj dodawane (�eby si� w tym wszystkim nie pogubi�).
     * 
     * @param entry
     * @param activityKey  identyfikator zadania, dla kt�rego uaktualniamy histori� listy (mo�e by� r�wne null, w�wczas
     * wyznacznikiem zadania, kt�re mamy uaktualni� jest tylko identyfikator dokumentu - co w wyj�tkowych
     * sytuacjach mo�e spowodowa� pewne nie�cis�o�ci w historii)
     * @param overrideCurrent  true <=> je�li aktualnie dodawana do historii operacja jest "dekretacj� na u�ytkownika"
     * to zostanie nadpisany dotychczasowy wpis (gdy istnieje) zamiast tworzy� nowy - u�ywane g��wnie
     * podczas przyj�cia pisma i natychmiastowej dekretacji np. na koordynatora, aby u�ytkownik przyjmuj�cy pismo
     * nie mia� w historii listy �adnego wpisu
     * @throws EdmException
     */
	public static void updateTaskListHistory(Long documentId, AssignmentHistoryEntry entry, boolean overrideCurrent, String activityKey) throws EdmException
    {		
		if(AvailabilityManager.isAvailable("migrationWorkflow") || AvailabilityManager.isAvailable("updateTaskListHistory.disabled"))
			return;
		WatchDog dog = new WatchDog("update-task-list-history",DataMartManager.WATCH_DOG_ALERT_TIME);
		if (AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION.equals(entry.getProcessType()))
		{                       
			if (entry.getType() != null)
			{
				log.debug("TASK documentID {} type = {} activity = {} date = {}",documentId,entry.getType(),activityKey, new Date().getTime());
				TaskHistoryEntry the = TaskHistoryEntry.findCurrent(documentId, activityKey, Boolean.TRUE);
				log.debug("TASK time {} ", new Date().getTime());
				// ZEWN�TRZNY WORKFLOW (JBPM)
				if (AssignmentHistoryEntry.JBPM_END_TASK.equals(entry.getType()))
				{
					// koniec zadania
					finishHistoryEntry(the, entry, entry.getSourceUser());
				}
				else if (AssignmentHistoryEntry.JBPM_FINISH.equals(entry.getType()) ||
						AssignmentHistoryEntry.JBPM_CANCELLED.equals(entry.getType()))
				{
					// koniec procesu - konczymy wszystkie zadania
					List<TaskHistoryEntry> list = TaskHistoryEntry.findAllCurrent(documentId, Boolean.TRUE);
					for (TaskHistoryEntry t : list)
						finishHistoryEntry(t, entry, null);
               
					// jak przerwano proces, to zosta� utworzony nowy w obiegu r�cznym - zatem tworz� nowy wpis historii
					if (AssignmentHistoryEntry.JBPM_CANCELLED.equals(entry.getType()))
					{
						TaskHistoryEntry newThe = new TaskHistoryEntry(documentId, entry.getCdate(), entry.getCtime(), 
                           entry.getSourceUser(), DSDivision.ROOT_GUID, null, Boolean.FALSE);
						Persister.create(newThe);
					}
				}
				else if (AssignmentHistoryEntry.JBPM_INIT.equals(entry.getType()))
				{
					// inicjowanie nowego procesu - "ko�cz�" wszystkie wpisy z r�cznego obiegu
					log.debug("TASK JBPM_INIT in time {} ", new Date().getTime());
					List<TaskHistoryEntry> list = TaskHistoryEntry.findAllCurrent(documentId, Boolean.FALSE);
					for (TaskHistoryEntry t : list)
						finishHistoryEntry(t, entry, null);
					log.debug("TASK JBPM_INIT out time {} ", new Date().getTime());
				}
				else if (AssignmentHistoryEntry.JBPM_NORMAL_DIVISION.equals(entry.getType()))
				{
					// otrzymanie zadania przez dzia�
					if (the == null)
					{                        
						TaskHistoryEntry newThe = new TaskHistoryEntry(documentId, entry.getCdate(), entry.getCtime(), 
                           entry.getTargetUser(), entry.getTargetGuid(), activityKey, Boolean.TRUE);
						Persister.create(newThe);
					}
				}
				else if (AssignmentHistoryEntry.JBPM_FORWARD.equals(entry.getType()))
				{
					// przekazanie zadania (np. gdy ma miejsce "realizacja DOK po OPS")
					finishHistoryEntry(the, entry, null);
               
					TaskHistoryEntry newThe = new TaskHistoryEntry(documentId, entry.getCdate(), entry.getCtime(), 
                       entry.getTargetUser(), entry.getTargetGuid(), activityKey, Boolean.TRUE);
					Persister.create(newThe);
				}
				else if (AssignmentHistoryEntry.JBPM_NORMAL.equals(entry.getType()))
				{
					if (entry.isAccepted())
					{
						// akceptacja zadania
						if (the != null)
						{
							the.setUsername(entry.getTargetUser());
							the.setAcceptDate(entry.getCdate());
							the.setAcceptTime(entry.getCtime());
						}
					}
					else
					{
						// otrzymania zadania przez u�ytkownika
						if (the == null)
						{
							TaskHistoryEntry newThe = new TaskHistoryEntry(documentId, entry.getCdate(), entry.getCtime(), 
                               entry.getTargetUser(), entry.getTargetGuid(), activityKey, Boolean.TRUE);
							Persister.create(newThe);
						}
						else
						{
							the.setUsername(entry.getTargetUser());
							if (!"x".equals(entry.getTargetGuid()))
								the.setDivisionGuid(entry.getTargetGuid());
						}    
					}
				}                    
			}
			else
			{
				TaskHistoryEntry the = TaskHistoryEntry.findCurrent(documentId, activityKey, Boolean.FALSE);           
				// WEWN�TRZNY WORKFLOW (OBIEG RECZNY)            
				if (entry.isFinished())
				{
					// koniec zadania
					finishHistoryEntry(the, entry, entry.getSourceUser());
				}
				else if (entry.getTargetUser() != null)
				{
					if (entry.isAccepted())
					{
						// akceptacja zadania
						if (the != null)
						{
							the.setUsername(entry.getTargetUser());
							the.setAcceptDate(entry.getCdate());
							the.setAcceptTime(entry.getCtime());
						}
					}
					else
					{
						// dekretacja zadania do u�ytkownika
						if (overrideCurrent && the != null)
						{
							the.setReceiveDate(entry.getCdate());
							the.setReceiveTime(entry.getCtime());
							the.setUsername(entry.getTargetUser());
							the.setDivisionGuid(entry.getTargetGuid());
						}
						else
						{
							finishHistoryEntry(the, entry, entry.getSourceUser());
							TaskHistoryEntry newThe = new TaskHistoryEntry(documentId, entry.getCdate(), entry.getCtime(), 
                               entry.getTargetUser(), entry.getTargetGuid(), activityKey, Boolean.FALSE);
                       
							Persister.create(newThe);
						}
					}
				}
				else if (entry.getProcessName().equals("rejestracja pisma"))
				{
					// rejestracja pisma
					TaskHistoryEntry newThe = new TaskHistoryEntry(documentId, entry.getCdate(), entry.getCtime(), 
                       entry.getSourceUser(), DSDivision.ROOT_GUID, activityKey, Boolean.FALSE);
					Persister.create(newThe);
				}
				else
				{                
					// dekretacja na dzia�
					finishHistoryEntry(the, entry, entry.getSourceUser());
					TaskHistoryEntry newThe = new TaskHistoryEntry(documentId, entry.getCdate(), entry.getCtime(), 
                       null, entry.getTargetGuid(), activityKey, Boolean.FALSE);
					Persister.create(newThe);
				}
			}
		}
		log.debug("TASK OUT CLOSE time {} ", new Date().getTime());
		dog.tick();
    } 
	/**
     * Oznaczenie wpisu w historii listy zada� jako zako�czone (czyli przypisanie daty sko�czenia zadania) -
     * tak�e gdy brakuje wcze�niejszych danych pr�ba ich uzupe�nienia.
     */
    private static void finishHistoryEntry(TaskHistoryEntry the, AssignmentHistoryEntry entry, String username)
    {
        if (the != null)
        {
            if (the.getAcceptDate() == null)
            {
                the.setAcceptDate(entry.getCdate());
                the.setAcceptTime(entry.getCtime());
            }
            if (the.getUsername() == null)
                the.setUsername(username);
            the.setFinishDate(entry.getCdate());
            the.setFinishTime(entry.getCtime());
        }
    }
}
