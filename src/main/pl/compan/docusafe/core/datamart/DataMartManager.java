package pl.compan.docusafe.core.datamart;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.WatchDog;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * Klasa odpowiedzialna za
 * @author wkutyla
 *
 */
public class DataMartManager
{

	static final StringManager sm = StringManager.getManager(Constants.Package);
	private static final ThreadLocal<String> sessions = new ThreadLocal<String>();
	private static Properties eventToChangeLog = new Properties();
	public final static long WATCH_DOG_ALERT_TIME = 250l;
	public static Logger log = LoggerFactory.getLogger(DataMartManager.class);


	public static void init() throws Exception
	{
		eventToChangeLog = new Properties();
		eventToChangeLog.load(DataMartManager.class.getClassLoader().
            getResourceAsStream("event-mapping.properties"));
	}

	public static String getAuditProperty(String eventCode)
	{
		return eventToChangeLog.getProperty("audit.property."+eventCode);
	}

	public static String getAuditDescription(String eventCode)
	{
		return eventToChangeLog.getProperty("audit.description."+eventCode);
	}

	/**
	 * Otwiera sesje datamarta o podanym identyfikatorze
	 * @param id
	 * @throws Exception
	 */
	public static void openSession(Object id)
	{
		String s = sessions.get();
		if(s!=null)
			log.trace("Juz istnieje otwarta sesja w tym w�tku:"+s);
		sessions.set(id.toString());

	}

	/**
	 * Zamyka sesje datamarta
	 * @throws Exception
	 */
	public static void closeSession()
	{
		if(sessions.get()==null)
			log.trace("Nie istnieje sesja dla tego w�tku");
		sessions.set(null);
	}

	/**
	 * Pobiera aktualna sesje datmarta. W przypadku gdy sesja nie istnieje tworzy nowa sesje
	 * @return
	 */
	public static String getSession()
	{
		String sesId = sessions.get();
		if(sesId==null)
		{
			log.trace("Nie istnieje sesja dla tego w�tku");
			Long s = System.currentTimeMillis();
			sessions.set(s.toString());
			return s.toString();
		}
		else
		{
			return sesId;
		}

	}

	/**
	 * Metoda przeznaczona do rejestracji zdarzen gdy znamy obiekt dokument
	 * @param document
	 * @param eventCode
	 * @param username
	 * @param descriptionParams
	 * @throws DataMartException
	 */
	public static void storeEvent(Document document, String eventCode,  String username, String descriptionParams,boolean forceOfficeHisotry) throws DataMartException
	{
		storeEvent(document,null,eventCode,username,descriptionParams,forceOfficeHisotry);
	}

	/**
	 * Metoda przeznaczona do rejestracji zdarzen gdy znamy obiekt dokument
	 * @param document
	 * @param eventCode
	 * @param username
	 * @param descriptionParams
	 * @throws DataMartException
	 */
	public static void storeEvent(Document document, String eventCode,  String username, String descriptionParams) throws DataMartException
	{
		storeEvent(document,null,eventCode,username,descriptionParams,false);
	}
	/**
	 * Metoda przeznaczana do zapisu zdarzen w miejscach gdy nie posiadamy obiektu Document
	 * @param documentId
	 * @param eventCode
	 * @param username
	 * @param descriptionParams
	 * @throws DataMartException
	 */
	public static void storeEvent(Long documentId, String eventCode,  String username, String descriptionParams) throws DataMartException
	{
		storeEvent(null,documentId,eventCode,username,descriptionParams,true);
	}
	/**
	 * Metoda przeznaczona do zapisywania zdarzen zwiazanych z dokumetem
	 * Na razie jest "under development"
	 * W pierwszym kroku powolutku przenosimy tu elementy kodu z innych miejsc
	 * Jak juz to zrobimy bierzemy sie za przerobke
	 * Metoda ob
	 * @param document
	 * @param documentId
	 * @param eventCode
	 * @param username
	 * @param descriptionParams
	 * @param forceOfficeHisotry
	 * @throws DataMartException
	 */
	protected static void storeEvent(Document document, Long documentId, String eventCode,  String username, String descriptionParams,boolean forceOfficeHisotry) throws DataMartException
	{
		log.debug("Logowanie zdarzenia {}",eventCode);
		Long docId = null;
		if (document!=null)
			docId = document.getId();
		if (documentId!=null)
			docId = documentId;
		log.trace("document_id-->{}",docId);
		log.trace("forceOfficeHistory-->" + forceOfficeHisotry);
		boolean storeOfficeHistory = forceOfficeHisotry;
		if (!storeOfficeHistory)
		{
			if (document!=null && document instanceof OfficeDocument)
			{
				log.trace("officeDocument");
				storeOfficeHistory = true;
			}
		}
		try
		{
			DSApi.context().session().flush();
			String what = eventToChangeLog.getProperty(eventCode);
			if (what != null)
			{
				log.trace("documentChangeLog {}",what);
				storeDocumentChangelog(docId,username, DocumentChangelog.ADD_ATTACHMENT_REVISION);
			}
			if (storeOfficeHistory)
			{
				String property = eventToChangeLog.getProperty("audit.property."+eventCode);
				String descriptionKey = eventToChangeLog.getProperty("audit.description."+eventCode);
				log.trace("storeOfficeHistory {}-{}", property, descriptionKey);
				String description = sm.getString(descriptionKey,descriptionParams);
				Audit audit = Audit.create(property, username, description);
				addHistoryEntry(docId,audit);
			}
		}
		catch (Exception e)
		{
			log.error("Blad w czasie zapisu historii",e);
			throw new DataMartException(e);
		}
	}

	/**
	 * Metoda zapisuje zdarzenie
	 * @param event
	 * @throws DataMartException
	 */


	public static Long storeEvent(DataMartEvent event)
	{
		Long id = null;
		if(AvailabilityManager.isAvailable("dataMart"))
		{
			WatchDog wd = new WatchDog(200);
			wd.setMessageCode("Czas wstawiania zdarzenia do DataMarta");
			PreparedStatement ps = null;
			ResultSet rs = null;
			try
			{
				if(DSApi.isOracleServer())
				{
					ps = DSApi.context().prepareStatement("select ds_datamart_id.nextval from dual");
					rs = ps.executeQuery();
					if(rs.next())
						id = rs.getLong(1);
					rs.close();
					DSApi.context().closeStatement(ps);
					ps = DSApi.context().prepareStatement("insert into ds_datamart (document_id, process_code, event_code, event_date, change_field_cn, old_value, new_value, username, \"session\",object_id, event_id)" +
						" values(?,?,?,?,?,?,?,?,?,?,?)");
				}
				else if (DSApi.isFirebirdServer())
				{
					ps = DSApi.context().prepareStatement("select gen_id(ds_datamart_id,1) from RDB$DATABASE;");
					rs = ps.executeQuery();
					if(rs.next())
						id = rs.getLong(1);
					rs.close();
					DSApi.context().closeStatement(ps);
					ps = DSApi.context().prepareStatement("insert into ds_datamart (document_id, process_code, event_code, event_date, change_field_cn, old_value, new_value, username, session,object_id, event_id)" +
						" values(?,?,?,?,?,?,?,?,?,?,?)");
				}
				
				else if(DSApi.isPostgresServer()){
					ps = DSApi.context().prepareStatement("select nextval('ds_datamart_id')");
					rs = ps.executeQuery();
					if(rs.next())
						id = rs.getLong(1);
					rs.close();
					DSApi.context().closeStatement(ps);
					ps = DSApi.context().prepareStatement("insert into ds_datamart (document_id, process_code, event_code, event_date, change_field_cn, old_value, new_value, username, session,object_id, event_id)" +
						" values(?,?,?,?,?,?,?,?,?,?,?)");
				}
				else
				{
					ps = DSApi.context().prepareStatement("insert into ds_datamart (document_id, process_code, event_code, event_date, change_field_cn, old_value, new_value, username, session, object_id)" +
						" values(?,?,?,?,?,?,?,?,?,?)");
				}

				ps.setObject(1, event.getDocumentId());
				ps.setString(2, event.getProcessCode());
				ps.setString(3, event.getEventCode());
				ps.setTimestamp(4, new java.sql.Timestamp(event.getEventDate().getTime()));
				ps.setString(5, event.getChangeFieldCn());
				ps.setString(6, event.getBeforeEventValue());
				ps.setString(7, event.getAfterEventValue());
				ps.setString(8, event.getUserName());
				ps.setString(9, event.getSession());
				ps.setString(10, event.getObjectId());
				if(id != null)
					ps.setLong(11,id);
				ps.execute();
				DSApi.context().closeStatement(ps);
				ps = null;

				if(id == null)
				{
					ps = DSApi.context().prepareStatement("select @@IDENTITY as id");
					rs = ps.executeQuery();
					while(rs.next())
					{
						id = rs.getLong(1);
					}
					rs.close();
					DSApi.context().closeStatement(ps);
					ps = null;
				}
			}
			catch(Exception e)
			{
				log.error("error",e);
			}
			finally
			{
				
				DSApi.context().closeStatement(ps);
				wd.tick();
			}
		}
		if(log.isInfoEnabled())
			log.info(event.getAsString());
		return id;
	}


	/**
	 * Zapisuje historie dekretacji
	 * @TODO - zajac sie TaskHistoryEntry - wyniesc to z OfficeDocument
	 * @param document
	 * @param entry
	 */
	public static void storeAssignmentHistory(OfficeDocument document,AssignmentHistoryEntry entry,boolean overrideCurrent, String activityKey)
	{
		if(AvailabilityManager.isAvailable("migrationWorkflow"))
			return;
		log.trace("storeAssignmentHistory documentId={}",document.getId());
		WatchDog dog = new WatchDog("add-assignment-history",WATCH_DOG_ALERT_TIME);
		PreparedStatement ps = null;
		try
		{
			entry.setDocumentId(document.getId());
			DSApi.context().session().saveOrUpdate(entry);
			
			/*ps = DSApi.context().prepareStatement("select max(posn) as ile from dso_document_asgn_history "+DSApi.nolockString()+" where document_id = ?");
			ps.setLong(1, document.getId());
			ResultSet rs = ps.executeQuery();
			rs.next();
			long posn = 0;
			if (rs.getString("ile")!=null)
			{
				posn = rs.getLong("ile")+1;
			}
			rs.close();
			DSApi.context().closeStatement(ps);
			ps = DSApi.context().prepareStatement("insert into dso_document_asgn_history " +
					"(document_id,posn,ctime,sourceuser,targetuser,sourceguid,targetguid,objective,accepted," +
					"finished,type,status,processname,cdate,processType,substituteUser,substituteGuid)" +
					" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			ps.setLong(1, document.getId());
			ps.setLong(2, posn);
			ps.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
			ps.setString(4, entry.getSourceUser());
			ps.setString(5, entry.getTargetUser());
			ps.setString(6, entry.getSourceGuid());
			ps.setString(7, entry.getTargetGuid());
			ps.setString(8, entry.getObjective());
			ps.setBoolean(9, entry.isAccepted());
			ps.setBoolean(10, entry.isFinished());
			if (entry.getType()!=null)
			{
				ps.setInt(11, entry.getType().value);
			}
			else
			{
				ps.setObject(11, null);
			}
			ps.setString(12, entry.getStatus());
			ps.setString(13, entry.getProcessName());
			ps.setDate(14, new java.sql.Date(System.currentTimeMillis()));
			entry.setCtimeAndCdate(new java.util.Date());
			ps.setInt(15, entry.getProcessType());
			ps.setString(16, entry.getSubstituteUser());
			ps.setString(17, entry.getSubstituteGuid());
			ps.execute();*/

			if(!AvailabilityManager.isAvailable("migrationWorkflow"))
				TaskListHistoryUpdateManager.updateTaskListHistory(document.getId(), entry, overrideCurrent, activityKey);
		}
		catch (Exception e)
		{
			log.error("Blad w czasie skladowania informacji",e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		dog.tick();
	}
	/**
	 *
	 * @param document
	 * @return
	 */
	public static List<AssignmentHistoryEntry> getAssignmentHistory(OfficeDocument document)
	{
		try
		{
			return getAssignmentHistory(document.getId());
		}
		catch (Exception e)
		{
			log.error("Blad w czasie pobierania historii dekretacji",e);
			return new LinkedList<AssignmentHistoryEntry>();
		}
	}
	/**
	 * Zwraca historie dekretacji  ostatnia akcja jako pierwsza 
	 * @param document
	 * @return
	 */
	public static List<AssignmentHistoryEntry> getAssignmentHistoryDesc(OfficeDocument document)
	{
		try
		{
			return getAssignmentHistoryDesc(document.getId());
		}
		catch (Exception e)
		{
			log.error("Blad w czasie pobierania historii dekretacji",e);
			return new LinkedList<AssignmentHistoryEntry>();
		}
	}
	/**
	 *
	 * @param documentId
	 * @return
	 * @throws Exception
	 */
	protected static List<AssignmentHistoryEntry> getAssignmentHistory(Long documentId) throws Exception
	{
		return DSApi.context().session()
				.createCriteria(AssignmentHistoryEntry.class)
				.add(Restrictions.eq("documentId", documentId))
				.addOrder(Order.asc("ctime"))
				.list();
	}
	/**
	 *
	 * @param documentId
	 * @return
	 * @throws Exception
	 */
	protected static List<AssignmentHistoryEntry> getAssignmentHistoryDesc(Long documentId) throws Exception
	{
		return DSApi.context().session()
				.createCriteria(AssignmentHistoryEntry.class)
				.add(Restrictions.eq("documentId", documentId))
				.addOrder(Order.desc("ctime"))
				.list();
	}
	/**
	 * W tej czesci zaczynam grupowac funkcje wykonujace operacje na kluczowych tabelach
	 */
	/**
	 * Zwracamy historie dokumentu
	 * @param document
	 * @return
	 * Wyjatek nie jest rzucamy, gdy wystapi zwracana jest pusta lista
	 */
	public static List<Audit> getWorkHistory(Document document)
	{
		try
		{
			log.trace("getWorkHistory(document id={})",document.getId());
			return getWorkHistory(document.getId());
		}
		catch (Exception e)
		{
			log.error("Blad w czasie pobierania historii");
			return new Vector<Audit>();
		}

	}

	/**
	 * Metoda zwraca historie dokumentu
	 * Wprowadzamy aby zlikwidowac mozliwosc lockowania
	 * @param documentId
	 * @return
	 * @throws Exception
	 */
	protected static List<Audit> getWorkHistory(Long documentId) throws Exception
	{
		log.trace("getWorkHistory(id={})",documentId);
		List<Audit> resp = new Vector<Audit>();
		PreparedStatement ps = null;
		try
		{
			ps = DSApi.context().prepareStatement("select * from DSO_DOCUMENT_AUDIT where document_id = ? order by posn");
			ps.setLong(1, documentId);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				resp.add(getAuditFromRS(rs));
			}
			rs.close();
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return resp;
	}

	public static Audit getLastHistoryEntry(Long documentId) throws Exception
	{
		log.trace("getLastHistoryEntry(document id={})",documentId);
		Audit resp = null;
		PreparedStatement ps = null;
		try
		{
			ps = DSApi.context().prepareStatement("select * from DSO_DOCUMENT_AUDIT where document_id = ? order by posn desc");
			ps.setLong(1, documentId);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
			{
				resp = getAuditFromRS(rs);
			}
			rs.close();
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return resp;
	}

	protected static Audit getAuditFromRS(ResultSet rs) throws SQLException
	{
		Audit audit = new Audit();
		audit.setCtime(rs.getTimestamp("ctime"));
		audit.setProperty(rs.getString("property"));
		audit.setUsername(rs.getString("username"));
		audit.setDescription(rs.getString("description"));
		audit.setLparam(rs.getString("lparam"));
		audit.setWparam(rs.getLong("wparam"));
		audit.setPriority(rs.getInt("priority"));
		return audit;
	}
	public static void addHistoryEntry(Long documentId, Audit audit) throws EdmException
	{
		DSApi.context().session().flush();
		log.trace("addHistoryEntry documentId={})",documentId);
		PreparedStatement ps = null;
		try
		{
			/**
			 * @TODO - sposob nadawania identyfikatorow wymaga zmiany, ale nad tym popracujemy
			 */
			ps = DSApi.context().prepareStatement("select max(posn) as ile from DSO_DOCUMENT_AUDIT where document_id = ?");
			ps.setLong(1, documentId);
			ResultSet rs = ps.executeQuery();
			rs.next();
			long posn = 0;
			if (rs.getString("ile")!=null)
			{
				posn = rs.getLong("ile")+1;
			}
			rs.close();
			DSApi.context().closeStatement(ps);
			ps = DSApi.context().prepareStatement("insert into DSO_DOCUMENT_AUDIT (document_id,posn,ctime,property,username,description,lparam,wparam,priority) " +
					"values (?,?,?,?,?,?,?,?,?)");
			ps.setLong(1,documentId);
			ps.setLong(2, posn);
			if(audit.getCtime()!=null)ps.setTimestamp(3, new java.sql.Timestamp(audit.getCtime().getTime()));
			else ps.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
			ps.setString(4, audit.getProperty());
			ps.setString(5, audit.getUsername());
			ps.setString(6, audit.getDescription());
			ps.setString(7, audit.getLparam());
			if (audit.getWparam()!=null)
			{
				ps.setLong(8, audit.getWparam());
			}
			else
			{
				ps.setObject(8, null);
			}
			ps.setInt(9,audit.getPriority());
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new EdmException(e.getLocalizedMessage(),e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
	}

	/**
	 * Wykonuje wpis w tabeli DS_DOCUMENT_CHANGELOG
	 * @TODO - wpisy te powinny byc ujednolicone z innymi wpisami w historii
	 * @param document
	 * @param username
	 * @param what
	 * @throws Exception
	 */
	public static void storeDocumentChangelog(Long id,String username, DocumentChangelog what) throws EdmException
	{
		log.trace("storeDocumentChangeLog documentId={})",id);
		if (id == null)
            throw new EdmException("Nie mo�na wywo�a� metody changelog " +
                "dla nieistniej�cego dokumentu");

        List<String> usernames = new LinkedList<String>();
        // je�eli powiadomienie o dekretacji na u�ytkownika $bok, nale�y
        // je zamieni� na powiadomienia dotycz�ce konkretnych u�ytkownik�w
        if (what == DocumentChangelog.WF_ASSIGNED && username.equals("$bok"))
        {
            String guid = GlobalPreferences.getBokDivisionGuid();
            if (guid != null)
            {
                try
                {
                    DSUser[] users = DSDivision.find(guid).getUsers();
                    for (int j=0; j < users.length; j++)
                    {
                        usernames.add(users[j].getName());
                    }
                }
                catch (DivisionNotFoundException e)
                {
                    log.warn(e.getMessage(), e);
                }
            }
        }
        else
        {
            usernames.add(username);
        }
        // TODO: przetwarzanie dla zast�pstw (sortowanie, aby szuka� duplikat�w)
        // raczej lista, nie tablica, aby j� uzupe�nia�
        // Arrays.sort(usernames);

        PreparedStatement ps = null;
        try
        {
            for (Iterator<String> iter=usernames.iterator(); iter.hasNext(); )
            {
            	ps = DSApi.context().prepareStatement(
                        "insert into ds_document_changelog (document_id, username, ctime, what) " +
                            "values (?, ?, current_timestamp, ?)");

            	ps.setLong(1, id.longValue());
                ps.setString(3, what.getWhat());
                ps.setString(2, (String) iter.next());
                ps.execute();
                DSApi.context().closeStatement(ps);
            }
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
	}

	/**
	 * Pobiera liste zdarzen
	 * @param documentId
	 * @param changelog
	 * @return
	 * @throws EdmException
	 */
	public static List<Long> getChangelogEntries(Long documentId, DocumentChangelog changelog)
    throws EdmException
    {
		PreparedStatement ps = null;
        try
        {
        	log.trace("getChangeLogEntries documentId={},what={})",documentId,changelog.getWhat());
            ps = DSApi.context().prepareStatement("select ctime from ds_document_changelog where document_id = ? and what = ? order by ctime");
            ps.setLong(1, documentId);
            ps.setString(2, changelog.getWhat());

            ResultSet rs = ps.executeQuery();
            List<Long> results = new LinkedList<Long>();
            while (rs.next())
            {
                results.add(rs.getTimestamp(1).getTime());
            }
            rs.close();
            return results;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }
}
