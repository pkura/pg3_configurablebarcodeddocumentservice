package pl.compan.docusafe.core.datamart;

import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static pl.compan.docusafe.core.datamart.DataMartDefs.*;

/**
 * Klasa narz�dziowa do �atwiejszej obs�ugi event�w, najlepiej importowa� j� statycznie
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DataMartEventProcessors {

	public static Logger log = LoggerFactory.getLogger(DataMartManager.class);

	/**
	 * Eventy zwi�zane z fakturami
	 */
	public enum EventType {

		/*******************************
		 *		  INVOICE EVENTS
		 *******************************/
		
		/**
		 * Zmiana parametr�w centra kosztowego
		 */
		CENTRUM_KOSZTOW_CHANGE(INVOICE_DEFS_CENTRUM_KOSZTOW_CHANGE),
		CENTRUM_KOSZTOW_CREATE(INVOICE_DEFS_CENTRUM_KOSZTOW_CREATE),
		CENTRUM_KOSZTOW_DELETE(INVOICE_DEFS_CENTRUM_KOSZTOW_DELETE),

		ACCEPTANCE_CONDITION_CREATE(INVOICE_DEFS_ACCEPTANCE_CONDTION_CREATE),
		ACCEPTANCE_CONDITION_DELETE(INVOICE_DEFS_ACCEPTANCE_CONDTION_DELETE),

		/*******************************
		 *		  DIVISION EVENTS
		 *******************************/
		DIVISION_USER_ADD(DataMartDefs.DIVISION_USER_ADD),
		DIVISION_USER_REMOVE(DataMartDefs.DIVISION_USER_REMOVE),
		
		/*******************************
		 *		  USER EVENTS
		 *******************************/
		USER_CHANGE(DataMartDefs.USER_CHANGE),
		USER_ARCHIVE_ROLE_ADD(DataMartDefs.USER_ARCHIVE_ROLE_ADD),
		USER_ARCHIVE_ROLE_REMOVE(DataMartDefs.USER_ARCHIVE_ROLE_REMOVE),
		USER_OFFICE_ROLE_ADD(DataMartDefs.USER_OFFICE_ROLE_ADD),
		USER_OFFICE_ROLE_REMOVE(DataMartDefs.USER_OFFICE_ROLE_REMOVE),
		USER_DOCUMENT_VIEW(DataMartDefs.DOCUMENT_VIEW),

		/*******************************
		 *		OFFICCE WORKFLOW EVENTS
		 *******************************/
		COORDINATOR_CHANGE(DataMartDefs.COORDINATOR_CHANGE),
		COORDINATOR_CREATE(DataMartDefs.COORDINATOR_CREATE),

		/*******************************
		 *		  ROLE EVENTS
		 *******************************/
		ROLE_PERMISSION_ADD(DataMartDefs.ROLE_PERMISSION_ADD),
		ROLE_PERMISSION_REMOVE(DataMartDefs.ROLE_PERMISSION_REMOVE),
		
		/*******************************
		 *		  Dokumenty office
		 *******************************/
		DOCUMENT_FIELD_CHANGE(DataMartDefs.DOCUMENT_FIELD_CHANGE),
		BIP_CASE_INFO(DataMartDefs.BIP_CASE_INFO),
		BIP_DOCUMENT_INFO(DataMartDefs.BIP_DOCUMENT_INFO),

		/*******************************
		 *		  Dokumenty office
		 *******************************/
		NEWS_CHANGE(DataMartDefs.NEWS_CHANGE),
		NEWS_DELETED(DataMartDefs.NEWS_DELETED),

        /*******************************
         *      Full text search
         *******************************/
        FULL_TEXT_SEARCH(DataMartDefs.FULL_TEXT_SEARCH);
		
		String codeName;

		EventType(String codeName){
			this.codeName = codeName;
		}

		String getEventCode(){
			return codeName;
		}
	}

	public enum ProcessEventType {
		JBMP_PROCESS_INSTANCE_CREATE(JBPM_PROCESS_INSTANCE_CREATE);

		String eventCode;

		ProcessEventType(String eventCode){
			this.eventCode = eventCode;
		}

		String getEventCode(){
			return eventCode;
		}
	}

	public enum DictionaryEventType {
		ENTRY_CHANGE("entry.change"),
		ENTRY_CREATE("entry.create");

		String eventCode;

		DictionaryEventType(String eventCode){
			this.eventCode = eventCode;
		}

		String getEventCode(){
			return eventCode;
		}
	}

    final static ThreadLocal<DataMartEventBuilder> builder
		= new ThreadLocal<DataMartEventBuilder>(){
			@Override
			protected DataMartEventBuilder initialValue() {
				return new DataMartEventBuilder();
		}
	};

	/**
	 * Klasa pomocnicza u�atwiaj�ca tworzenie event�w dla datamart
	 */
	public static class DataMartEventBuilder{
		String eventSession;
		final LinkedList<DataMartEvent> events;

		String defaultObjectId = null;
		Long defaultDocumentId = null;

		private DataMartEventBuilder(){
			this.eventSession = DataMartEvent.generateSession();
			this.events = new LinkedList<DataMartEvent>();
		}

		public static DataMartEventBuilder create(){
			builder.set(new DataMartEventBuilder());
			return builder.get();
		}

		/**
		 * Pobiera Buildera z ThreadLocal
		 * @return
		 */
		public static DataMartEventBuilder get(){
			return builder.get();
		}

		/**
		 * Loguje wszystkie b��dy, kt�re zosta�y dodane za pomoc� DataMartEventBuildera.
		 */
		public static void logEvents() {
			try {
				builder.get().log();
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}

		/**
		 * Czy�ci wszystkie zalogowane b��dy.
		 */
		public static void clearEvents() {
			try {
				builder.get().clear();
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}

		/**
		 * Dodaje nowy event do kolejki event�w
		 * @param type
		 * @return
		 */
		public DataMartEventBuilder event(EventType type){
			addEventObject(type.getEventCode());
			return this;
		}

		/**
		 * Czy�ci eventy i generuje now� sesj�
		 * @return
		 */
		public DataMartEventBuilder clear(){
			events.clear();
			eventSession = DataMartEvent.generateSession();
			defaultDocumentId = null;
			defaultObjectId = null;
			return this;
		}

		public DataMartEventBuilder processEvent(ProcessEventType type, String processName){
			events.add(new DataMartEvent(false, defaultDocumentId, processName, type.getEventCode(), null, null, null, eventSession));
			events.getLast().setObjectId(defaultObjectId);
			return this;
		}

		public DataMartEventBuilder dictionaryEvent(DictionaryEventType type, String dictionaryName){
			events.add(new DataMartEvent(false, defaultDocumentId, null, "dictionary." + dictionaryName + "." + type.getEventCode(), null, null, null, eventSession));
			events.getLast().setObjectId(defaultObjectId);
			return this;
		}

		/**
		 * Ustawia domy�lny documentId dla nowych event�w,
		 * przydatne gdy na raz wykonywanych jest wiele czynno�ci na jednym dokumencie
		 * @param id
		 * @return
		 */
		public DataMartEventBuilder defaultDocumentId(Long id){
			defaultDocumentId = id;
			return this;
		}

		/**
		 * Ustawia domy�lny objectId dla nowych event�w,
		 * przydatne gdy na raz wykonywanych jest wiele czynno�ci na jednym obiekcie
		 * @param id
		 * @return
		 */
		public DataMartEventBuilder defaultObjectId(String id){
			defaultObjectId = id;
			return this;
		}

		public DataMartEventBuilder defaultObjectId(Object id){
			defaultObjectId = (id == null ? null : id.toString());
			return this;
		}

		public DataMartEventBuilder objectId(String objectId){
			events.getLast().setObjectId(objectId);
			return this;
		}
		
		public DataMartEventBuilder session(String session){
			events.getLast().setSession(session);
			return this;
		}

		public DataMartEventBuilder objectId(Object objectId){
			if(objectId == null){
				events.getLast().setObjectId(null);
			} else {
				events.getLast().setObjectId(objectId.toString());
			}
			return this;
		}


		/**
		 * Dodaje do eventu informacj� o zmianie warto�ci - lepiej u�ywa� checkValueChangeEvent
		 * @param fieldCn
		 * @param oldValue
		 * @param newValue
		 * @return
		 */
		public DataMartEventBuilder valueChange(String fieldCn, String oldValue, String newValue){
			events.getLast().setChangeFieldCn(fieldCn);
            events.getLast().setBeforeEventValue(oldValue);
			events.getLast().setAfterEventValue(newValue);

			return this;
		}


		/**
		 * Sprawdza czy warto�ci si� zmieni�y. Je�li tak - to tworzy nowy event i go uzupe�nia.
		 * Przyk�ad:
		 * <pre>
		 *	DataMartEventBuilder builder
		 *		= DataMartEventBuilder.create()
		 *			.defaultObjectId(user.getName()); //w tym przypadku trzeba korzysta� z warto�ci domy�lnych - nie wiemy ile event�w si� wykona i czy si� wykonaj�
		 *	builder.checkValueChangeEvent(EventType.USER_CHANGE, "phoneNum",user.getPhoneNum(), phoneNum);
		 * </pre>
		 * @param type
		 * @param fieldCn
		 * @param oldValue
		 * @param newValue
		 * @return
		 */
		public DataMartEventBuilder checkValueChangeEvent(EventType type, String fieldCn, String oldValue, String newValue){

			if(oldValue != null){
				if(!oldValue.equals(newValue)){
					event(type);
					valueChange(fieldCn, oldValue, newValue);
				}
			} else {
				if (null != newValue) {
					event(type);
					valueChange(fieldCn, oldValue, newValue);
				} //else -> oba s� null
			}

			return this;
		}

		public DataMartEventBuilder checkValueChangeEvent(DictionaryEventType type, String dictionaryName, String fieldCn, String oldValue, String newValue){

			if(oldValue != null){
				if(!oldValue.equals(newValue)){
					dictionaryEvent(type, dictionaryName);
					valueChange(fieldCn, oldValue, newValue);
				}
			} else {
				if (null != newValue) {
					dictionaryEvent(type, dictionaryName);
					valueChange(fieldCn, oldValue, newValue);
				} //else -> oba s� null
			}

			return this;
		}

		public DataMartEventBuilder documentId(long documentId){
			events.getLast().setDocumentId(documentId);
			return this;
		}

		/**
		 * Zapisuje wszystkie dodane eventy.
		 */
		public void log(){
			if (!events.isEmpty()) {
				log.debug("logging dataMart events [count = {}]", events.size());
				for (DataMartEvent event : events) {
					DataMartManager.storeEvent(event);
				}
			}
			clear();
		}

		private void addEventObject(String eventCode)
		{ 
			events.add(new DataMartEvent(false, defaultDocumentId, null, eventCode, null, null, null, eventSession));
			events.getLast().setObjectId(defaultObjectId);
		}
	}
}
