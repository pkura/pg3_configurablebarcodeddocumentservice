package pl.compan.docusafe.core.datamart;

/**
 * Klasa zawierajaca definicje stalych dla modulu DataMart
 * @author wkutyla
 *
 */
public class DataMartDefs 
{
	/**
	 * Proponuje przyjac konwencje zapisu zdarzen
	 * process.* - standardowe zdarzenia dotyczace dekretacji
	 *
	 */
	
	//Dekretacje
	public static final String PROCESS_START_EVENT = "process.start";
	public static final String PROCESS_FINISH = "process.finish";
	public static final String ASSIGNEMENT = "assignement";
	public static final String ASSIGNEMENT_ACKNOWLEDGE = "assignement.acknowledge";

	//Zdarzenia dotyczace dokuemntow
	public static final String DOCUMENT_CREATE = "document.create";
	public static final String DOCUMENT_VIEW = "document.view";
	public static final String DOCUMENT_VIEW_ATTRIBUTES = "document.view.attributes";
	public static final String DOCUMENT_VIEW_ATTACHMENT = "document.view.attachment";
	public static final String DOCUMENT_FIELD_CHANGE = "document.field.change";
	public static final String DOCUMENT_FIELD_MULTI_CHANGE = "document.field.multichange";
	public static final String DOCUMENT_DOCKIND_CHANGE = "document.dockind.change";
	public static final String DOCUMENT_DOCKIND_REMOVE = "document.dockind.remove";
	public static final String DOCUMENT_FLAG_ADD = "document.flag.add";
	public static final String DOCUMENT_FLAG_REMOVE = "document.flag.remove";
	public static final String DOCUMENT_LABEL_ADD = "document.label.add";
	public static final String DOCUMENT_LABEL_REMOVE = "document.label.remove";
	
	
	public static final String DOCUMENT_ATTACHMENT_ADD = "document.attachment.add";
	public static final String DOCUMENT_ATTACHMENT_DELETE = "document.attachment.delete";
	public static final String DOCUMENT_ATTACHMENT_REVISION_ADD = "document.attachment.revision.add";
	
	//Akceptacje
	public static final String ACCEPTANCE_GIVE = "acceptance.give";
	public static final String ACCEPTANCE_GIVE_CENTRUM = "acceptance.give.centrum";
	public static final String ACCEPTANCE_WITHDRAW = "acceptance.withdraw";
	public static final String ACCEPTANCE_WITHDRAW_CENTRUM = "acceptance.withdraw.centrum";
	
	//NEWS
	public static final String NEWS_DELETED = "news.deleted";
	public static final String NEWS_CHANGE = "news.change";
	/*
	 * S�OWNIKI - format nazwy eventu dla s�ownika "dictionary."+nazwaS�ownika+nazwaEventu
	 * patrz -> DictionaryEventType
	 */
	
	
	public static final String SEARCH = "search";
	public static final String OTHER_USER_TASKLIST_VIEW = "other.user.tasklist.view";

	public static final String DIVISION_USER_ADD = "organization.division.user.add";
	public static final String DIVISION_USER_REMOVE = "organization.division.user.remove";
	
	public static final String USER_CREATE = "user.create";
	public static final String USER_CHANGE = "user.change";
	public static final String USER_DELETE = "user.delete";
	public static final String USER_ARCHIVE_ROLE_ADD = "user.role.archive.add";
	public static final String USER_ARCHIVE_ROLE_REMOVE = "user.role.archive.remove";
	public static final String USER_OFFICE_ROLE_ADD = "user.role.office.add";
	public static final String USER_OFFICE_ROLE_REMOVE = "user.role.office.remove";
	public static final String USER_LOGIN = "user.login";
	public static final String USER_LOGOUT = "user.logout";

	public static final String COORDINATOR_CHANGE = "workflow.coordinator.change";
	public static final String COORDINATOR_CREATE = "workflow.coordinator.create";

	public static final String ROLE_PERMISSION_ADD = "role.permission.add";
	public static final String ROLE_PERMISSION_REMOVE = "role.permission.remove";

	public static final String INVOICE_DEFS_CENTRUM_KOSZTOW_CHANGE = "invoice.defs.centrum.change";
	public static final String INVOICE_DEFS_CENTRUM_KOSZTOW_CREATE = "invoice.defs.centrum.create";
	public static final String INVOICE_DEFS_CENTRUM_KOSZTOW_DELETE = "invoice.defs.centrum.delete";

	public static final String INVOICE_DEFS_ACCEPTANCE_CONDTION_CREATE = "invoice.defs.acceptance.cond.create";
	public static final String INVOICE_DEFS_ACCEPTANCE_CONDTION_DELETE = "invoice.defs.acceptance.cond.delete";

	/**
	 * Eventy dla r�nych proces�w
	 */
	public static final String JBPM_PROCESS_INSTANCE_CREATE = "jbpm.process.instance.create";
	public static final String JBPM_PROCESS_DEFINITION_LOAD = "jbpm.process.definition.load";
	
	
	public static final String BIP_DOCUMENT_INFO = "bip.document.info";
	public static final String BIP_CASE_INFO = "bip.case.info";
    public static final String FULL_TEXT_SEARCH = "fulltextSearch.search";
}
