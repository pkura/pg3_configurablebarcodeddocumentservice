package pl.compan.docusafe.core.datamart;

/**
 * Klasa reprezentujaca zdarzenie, ktore wystapilo dla danego procesu
 * @author wkutyla
 *
 */

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

public class DataMartEvent implements java.io.Serializable 
{
	static final long serialVersionUID = 2233232;
    
	/**
	 * Identyfikator zdarzenia
	 */
	private Long eventId;
	/**
	 * Dokument jakiego dotyczy zdarzeni
	 */
	private Long documentId; 
	/**
	 * Kod procesu (jezeli bedzie wykonywany wiecej niz jeden proces na dokumencie)
	 */
	private String processCode = "main";
	/**
	 * Sesja (sluzy do wiazania wielu wpisow w datamarcie)
	 */
	private String session;
	/**
	 * Data zdarzenia 
	 */
	private Date eventDate;
	/**
	 * Kod zdarzenia
	 */
	private String eventCode;
	/**
	 * Wykonawca
	 */
	private String userName;
	
	/**
	 * wskazue ktore pole dokumentu zostalo zmienione
	 */
	private String changeFieldCn;
	/**
	 * Wartosc istotna dla raportu przed zdarzeniem 
	 * np. osoba dekretujaca
	 */
	private String beforeEventValue;
	/**
	 * Wartosc istotna dla raportu po zdarzeniu 
	 * np. osoba do ktorej wykonana zostaje dekretacji 
	 */
	private String afterEventValue;

	/**
	 * Identyfikator jakiego� innego obiektu
	 */
	private String objectId;

	
	/**
	 * 
	 * @param store mowi czy od razu zapisac zdarzenie
	 * @param docId id dokumentu
	 * @param procCode kod procesu
	 * @param evCode kod zdarzenia (definiowane w DataMartDefs)
	 * @param fieldCn kod pola
	 * @param oldVal stara warto��
	 * @param newVal nowa wartosc
	 * @param session id sesji (mozna je pozyskac poprzez wywolanie createSession() na evencie). 
	 * 			Pozwala laczyc kilka zdarzen w jedna calosc (np pozycje importu, kryteria wyszukiwania,
	 * 			 pola zmienione za jednym razem)
	 */
	public DataMartEvent(boolean store, Long docId, String procCode, String evCode, String fieldCn, String oldVal, String newVal, String session)
	{
		try
		{
			this.userName = DSApi.context().getPrincipalName();
		}
		catch(IllegalStateException e)
		{
			this.userName = "test";
		}
		this.documentId = docId;
		this.processCode = procCode;
		this.eventCode = evCode;
		this.eventDate = new Date();
		this.changeFieldCn = fieldCn;
		this.beforeEventValue = oldVal;
		this.afterEventValue = newVal;
		this.session = session;
		if(store)
			this.eventId = DataMartManager.storeEvent(this);
	}
	public Long getEventId() {
		return eventId;
	}
	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	public String getProcessCode() {
		return processCode;
	}
	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventCode() {
		return eventCode;
	}
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getChangeFieldCn() {
		return changeFieldCn;
	}
	public void setChangeFieldCn(String changeFieldCn) {
		this.changeFieldCn = changeFieldCn;
	}
	public String getBeforeEventValue() {
		return beforeEventValue;
	}
	public void setBeforeEventValue(String beforeEventValue) {
		this.beforeEventValue = beforeEventValue;
	}
	public String getAfterEventValue() {
		return afterEventValue;
	}
	public void setAfterEventValue(String afterEventValue) {
		this.afterEventValue = afterEventValue;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getAsString()
	{
		String result = "";
		result+="ID:" + eventId;
		result+=" | DocID:" + documentId;
		result+=" | Data:" + eventDate.toLocaleString();
		result+=" | Kod:" + eventCode;
		result+=" | User:" + userName;
		result+=" | Field:" + changeFieldCn;
		result+=" | oldVal:" + beforeEventValue;
		result+=" | newVal:" + afterEventValue; 
		result+=" | session:" + session;
		result+=" | objectId:" + objectId;
		return result;
	}
	
	public String createSession() throws EdmException
	{
		String ret;
		try
		{
			MessageDigest md = MessageDigest.getInstance("md5");
			ret = new String(md.digest(Long.toString(System.currentTimeMillis()).getBytes()));
			this.session = ret;
			return ret;
		}
		catch(NoSuchAlgorithmException e)
		{
			throw new EdmException(e);
		}
	}

	public static String generateSession()
	{
		String ret;
		try
		{
			MessageDigest md = MessageDigest.getInstance("md5");
			ret = IOUtils.toString(Base64.encodeBase64(md.digest(Long.toString(System.currentTimeMillis()).getBytes())));
			return ret;
		}
		catch(Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * @return the objectId
	 */
	public String getObjectId() {
		return objectId;
	}

	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

}
