package pl.compan.docusafe.core.datamart;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * Klasa uproszczająca tworzenie kwerend dla tabeli ds_datamart. Obsługuje tylko proste zapytania.
 * @author Michał Sankowski <michal.sankowski@docusafe.pl>
 */
public class DataMartQuery {

	public static final String EVENT_CODE = "event_code";
	public static final String EVENT_ID = "event_id";
	public static final String OBJECT_ID = "object_id";
	public static final String EVENT_DATE = "event_date";
	public static final String DOCUMENT_ID = "document_id";

	/**
	 *  Wszystkie kolumny w tabeli
	 */
	private final static Set<String> COLUMNS
		= new LinkedHashSet<String>(Arrays.asList(
			EVENT_ID,
			DOCUMENT_ID,
			"process_code", 
			EVENT_CODE,
			"event_date",
            "change_field_cn", 
            "old_value",
            "new_value",
			"username",
			"\"session\"",
			OBJECT_ID));

	/**
	 * Przydatne - do wyświetlania - kolumny
	 */
	public final static Set<String> READABLE_COLUMNS
		= new LinkedHashSet<String>(Arrays.asList(
			"event_date",
			DOCUMENT_ID,
			"process_code",
			EVENT_CODE,
            "change_field_cn", 
            "old_value",
            "new_value",
			"username",
			"object_id"));

	public final static String DEFAULT_STATEMENT = getSelectStement(READABLE_COLUMNS);
	public final static String ALL_COLS_STATEMENT = getSelectStement(COLUMNS);

	private StringBuilder whereStatement = new StringBuilder("WHERE ");
	private List<Object> values = new ArrayList<Object>();

	private String selectStatement = DEFAULT_STATEMENT;

	//domyślnie bierze najnowsze eventy
	private String orderBy = EVENT_ID;
	private boolean desc = true;

	private int maxRows = 50;

	private static void likeAdd(DataMartQuery ret, String column, String eventCodePrefix) {
		checkColumn(column);
		ret.whereStatement.append(column+ " LIKE ?");
		ret.values.add(eventCodePrefix.replace("%", "%%") + "%");
	}

	private static void checkColumn(String columnName) throws IllegalArgumentException {
		if (!COLUMNS.contains(columnName)) {
			throw new IllegalArgumentException("Nieznana kolumna - " + columnName);
		}
	}

	private static void equalAdd(DataMartQuery ret, String columnName, Object value) {
		checkColumn(columnName);
		ret.whereStatement.append(columnName+ " = ? ");
		ret.values.add(value);
	}

	private void equalAddAnd(DataMartQuery ret, String columnName, Object value) {
		checkColumn(columnName);
		ret.whereStatement.append(" AND " + columnName+ " = ? ");
		ret.values.add(value);
	}
	
	private void gratherThenAddAnd(DataMartQuery ret, String columnName, Object value) {
		checkColumn(columnName);
		ret.whereStatement.append(" AND " + columnName+ " > ? ");
		ret.values.add(value);
	}
	
	private void eventDateAfter(DataMartQuery ret, String columnName, Object time)
	{
		checkColumn(columnName);
		ret.whereStatement.append(" AND " + columnName + " > ?");
		ret.values.add(time);
	}

	private static void multiEqualAdd(DataMartQuery ret,String columnName, Object[] values) {
		checkColumn(columnName);
		ret.whereStatement.append("(");
		for (Object value : values) {
			ret.whereStatement.append(columnName + " = ? OR ");
			ret.values.add(value);
		}
		ret.whereStatement.lastIndexOf("OR");
		ret.whereStatement.delete(ret.whereStatement.lastIndexOf("OR"), ret.whereStatement.length());
		ret.whereStatement.append(") ");
	}

	private static String getSelectStement(Set<String> columns) {
		StringBuilder ret = new StringBuilder("SELECT ");
		for(String column: columns){
			checkColumn(column);
			ret.append(column);
			ret.append(",");
		}
		ret.setCharAt(ret.lastIndexOf(","), ' '); //zamienia ostatni przeciek na spację
		ret.append(" FROM ds_datamart ");
		return ret.toString();
	}

	public static DataMartQuery forEventCode(String eventCode){
		DataMartQuery ret = new DataMartQuery();
		equalAdd(ret, EVENT_CODE, eventCode);
		return ret;
	}

	public static DataMartQuery forEventCodes(String... eventCodes){
		DataMartQuery ret = new DataMartQuery();
		multiEqualAdd(ret, EVENT_CODE, eventCodes);
		return ret;
	}

	public static DataMartQuery forEventCodePrefix(String eventCodePrefix){
		DataMartQuery ret = new DataMartQuery();
		likeAdd(ret, EVENT_CODE, eventCodePrefix);
		return ret;
	}

	DataMartQuery(){
	}

	public DataMartQuery eventCodeEqual(String code){
		equalAddAnd(this, EVENT_CODE, code);
		return this;
	}

	public DataMartQuery objectIdEqual(String objectId){
		equalAddAnd(this, OBJECT_ID, objectId);
		return this;
	}

	public DataMartQuery eventIdEqual(String eventId){
		equalAddAnd(this, EVENT_ID, eventId);
		return this;
	}
	
	public DataMartQuery eventIdGratherThen(String eventId)
	{
		gratherThenAddAnd(this, EVENT_ID, eventId);
		return this;
	}

	public DataMartQuery maxRows(int maxRows){
		this.maxRows = maxRows;
		return this;
	}
	
	public DataMartQuery orderBy(String columnName, boolean desc)
	{
		this.orderBy = columnName;
		this.desc = desc;
		return this;
	}
	
	public DataMartQuery eventDateAfter(Date time)
	{
		eventDateAfter(this, EVENT_DATE, time);
		return this;
	}

	public String getStatementString() {
		return selectStatement + whereStatement + " ORDER BY " + orderBy + (desc ? " DESC" : " ASC" );
	}
	
	public PreparedStatement prepareStatementBip() throws EdmException, SQLException{
		PreparedStatement ret = DSApi.context().prepareStatement("SELECT event_id,document_id,process_code,event_code,event_date,change_field_cn,old_value,new_value,username  FROM ds_datamart WHERE (event_code = ? OR event_code = ? )  AND event_date > ? AND event_id > ?  ORDER BY event_id DESC");

		int i = 1;
		for(Object value: values)
		{
			if (value instanceof Date)
			{
				Timestamp timestamp = new Timestamp(((Date) value).getTime());
				ret.setTimestamp(i++, timestamp);
			}
			if (i==4 &&DSApi.isPostgresServer())
			{
			
				ret.setLong(i, (Long.parseLong ((String) value)));
			}
			else
			{
				ret.setObject(i++, value);
			}
		}

		ret.setMaxRows(maxRows);

		return ret;
	}

	public PreparedStatement prepareStatement() throws EdmException, SQLException{
		PreparedStatement ret = DSApi.context().prepareStatement(getStatementString());

		int i = 1;
		for(Object value: values)
		{
			if (value instanceof Date)
			{
				Timestamp timestamp = new Timestamp(((Date) value).getTime());
				ret.setTimestamp(i++, timestamp);
			}
			else
			{
				ret.setObject(i++, value);
			}
		}

		ret.setMaxRows(maxRows);

		return ret;
	}

	public String getSelectStatement() {
		return selectStatement;
	}

	public void setSelectStatement(String selectStatement) {
		this.selectStatement = selectStatement;
	}
}
