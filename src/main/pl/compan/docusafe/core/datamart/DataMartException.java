package pl.compan.docusafe.core.datamart;

import pl.compan.docusafe.core.EdmException;

public class DataMartException extends EdmException 
{
	static final long serialVersionUID = 24324;
	
	public DataMartException(String message)
    {
        super(message);
    }
	
	public DataMartException(String message, Throwable cause)
    {
        super(message, cause);        
    }

    public DataMartException(Throwable cause)
    {
        super(cause);        
    }
}
