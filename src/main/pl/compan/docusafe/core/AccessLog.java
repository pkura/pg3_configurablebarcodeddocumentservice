package pl.compan.docusafe.core;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.Person;
import std.fun;
import std.lambda;

import java.util.List;

/**
 * Klasa loguj�ca dost�py do obiekt�w.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AccessLog.java,v 1.12 2009/04/22 05:48:35 wkuty Exp $
 */
public class AccessLog
{
    private static final Log access_log = LogFactory.getLog("access_log");

    /**
     * Metoda loguje odczyt dokumentu przez u�ytkownika. Powinna by�
     * wywo�ywana w ramach otwartego kontekstu DSContext.
     */
    public static void logAccess(Document document) throws EdmException
    {
        if (document == null)
            return;

        if (access_log.isInfoEnabled())
        {
            String username = DSApi.context().getPrincipalName();
            access_log.info("%READ% {\"username\":\""+username+"\"," +
                " \"documentid\":\""+document.getId()+"\"}");
            if(AvailabilityManager.isAvailable("accesLog.trace"))
            	access_log.info("%READ% {\"username\":\""+username+"\"," +
                " \"documentid\":\""+document.getId()+"\"}",new Exception("accessLog"));
        }
    }

/*
    public static void logSignon(String username, HttpServletRequest request, boolean failed)
    {
        if (access_log.isInfoEnabled())
        {
            access_log.info("%LOGON% {\"username\":\""+username+"\""+
                (failed ? " \"failed\":\"true\"" : "")+
                " \"ip\":\""+NetUtil.getClientAddr(request).getIp()+"\"}");
        }
    }
*/

/*
    public static void logSignoff(String username)
    {
        access_log.info("%LOGOUT% {\"username\":\""+username+"\"}");
    }
*/

    public static void logUserCreation(String username, String newUsername)
    {
        access_log.info("%USERCREATE% {\"username\":\""+username+"\",\"newuser\":\""+newUsername+"\"}");
    }

    public static void logPersonalDataInput(String username, Long documentId,
                                            List persons)
    {
        access_log.info("%PERSONALDATAINPUT% {\"username\":\""+username+"\"," +
            " \"documentid\":\""+documentId+"\","+
            " \"personaldata\":"+
                fun.map(persons, new lambda()
                {
                    public Object act(Object o)
                    {
                        return new JSONObject(((Person) o).toMap()).toString();
                    }
                })+"}");
    }

    public static void logPersonalDataAccess(String username, Long documentId,
                                             List persons)
    {
    	if (persons==null)
    	{              
    		return;
    	}
    	
        access_log.info("%PERSONALDATAACCESS% {\"username\":\""+username+"\"," +
            " \"documentid\":\""+documentId+"\","+
            " \"personaldata\":"+
                fun.map(persons, new lambda<Person, String>()
                {
                    public String act(Person person)
                    {
                        return new JSONObject((person).toMap()).toString();
                    }
                })+"}");
    }
}
