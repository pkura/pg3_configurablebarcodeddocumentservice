package pl.compan.docusafe.core;

/**
 * Created by brs on 24.12.13.
 */
public class ProfileNotAssignedException extends EdmException{
    public ProfileNotAssignedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProfileNotAssignedException(Throwable cause) {
        super(cause);
    }

    public ProfileNotAssignedException(String message) {
        super(message);
    }
}
