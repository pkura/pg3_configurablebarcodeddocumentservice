package pl.compan.docusafe.core;

import java.lang.reflect.Array;
import java.util.Collection;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SearchResultsAdapter.java,v 1.11 2006/02/20 15:42:16 lk Exp $
 */
public class SearchResultsAdapter<T> implements SearchResults<T>
{
    private Mapper<T> mapper;

    private T[] results;
    /**
     * Indeks bie��cej pozycji w tablicy wynik�w (bie��ca pozycja to
     * ta zwracana przez funkcj� {@link #next()}).
     */
    private int index;
    /**
     * Indeks pozycji znajduj�cej si� bezpo�rednio po ostatniej pozycji,
     * jak� mo�e zwr�cic funkcja {@link #next()}, nie musi istnie� w tablicy
     * results.
     */
    private int endIndex;
    /**
     * Ca�kowita liczba wynik�w, niekoniecznie wszystkie musz� si� znajdowa�
     * w tablicy results.
     */
    private int totalCount = -1;
    private boolean hasMore;
    /**
     * Liczba wynik�w, jakie zwr�c� kolejne wywo�ania funkcji {@link #next()}.
     */
    private int count;

    /**
     * Tworzy obiekt zawieraj�cy wszystkie wyniki wyszukiwania, ale zwracaj�cy
     * tylko niekt�re z nich.
     *
     * @param results Tablica wynik�w, kt�re b�d� zwracane przez metod� {@link #next()}.
     *  Zak�ada si�, �e w tablicy znajduj� si� wszystkie mo�liwe wyniki, natomiast
     *  parametry start i length ograniczaj� liczb� wynik�w zwracanych przez funkcj�
     *  {@link #next()}. Funkcja {@link #totalCount()} zwraca ca�kowit� d�ugo�� tablicy
     *  results.
     * @param start Indeks tablicy, od kt�rego zacznie si� zwracanie wynik�w.
     * @param length Liczba wynik�w, kt�re powinny by� zwr�cone. Warto�� nie mo�e
     *  by� wi�ksza ni� results.length - start.
     */
    public SearchResultsAdapter(T[] results, int start, int length)
    {
        if (results == null)
            throw new NullPointerException("results");
        if (start < 0)
            throw new IllegalArgumentException("start="+start);
        if (start > results.length)
            throw new IllegalArgumentException("start="+start+", results.length="+results.length);
        if (length > results.length - start)
            throw new IllegalArgumentException("length="+length+", results.length="+results.length+", start="+start);
        if (length < 0)
            throw new IllegalArgumentException("length="+length);

        this.results = results;
        this.index = start;
        this.endIndex = start + length; // nast�pny indeks po ostatnim dozwolonym
        this.totalCount = results.length;
        this.hasMore = start + length < results.length-1;
        this.count = length;
    }

    /**
     * Tworzy obiekt SearchResults, dla kt�rego nie da si� okre�li� ca�kowitej
     * liczby wynik�w, ale mo�na stwierdzi�, czy opr�cz wynik�w zwr�conych
     * pozosta�y jakie� jeszcze.
     */
    public SearchResultsAdapter(T[] results, int start, int length, boolean hasMore)
    {
        this(results, start, length);
        this.hasMore = hasMore;
    }

    /**
     * Tworzy zbi�r wynik�w zawieraj�cy wycinek wszystkich mo�liwych wynik�w
     * oraz informacj� o ich ca�kowitej liczbie oraz o po�o�eniu tego wycinka
     * w ramach ca�kowitego zbioru wynik�w.
     * @param results Wszystkie wyniki, jakie powinny by� zwr�cone przez ten obiekt.
     * @param offset Po�o�enie wynik�w zawartych w kolekcji results w ramach
     *  ca�kowitego zbioru wynik�w.
     * @param totalCount Rozmiar ca�kowitego zbioru wynik�w.
     */
    public SearchResultsAdapter(Collection<T> results, int offset, int totalCount,
                                Class<T> resultClass)
    {
        if (results == null)
            throw new NullPointerException("results");
        
        /*
        if (totalCount < results.size())
            throw new IllegalArgumentException("totalCount="+totalCount+", results.size()="+
                results.size());
        */
        
        // ten b��d nie jest raportowany, poniewa� warto�� offsetu pochodzi
        // od u�ytkownika
//        if (offset + results.size() > totalCount)
//            throw new IllegalArgumentException("offset="+offset+", results.size()="+
//                results.size()+", totalCount="+totalCount);

        this.results = results.toArray((T[]) Array.newInstance(resultClass, results.size()));
        this.index = 0;
        this.endIndex = this.results.length; // nast�pny indeks po ostatnim dozwolonym
        this.totalCount = totalCount;
        this.hasMore = offset + this.results.length < totalCount;
        this.count = this.results.length;
    }

    public boolean hasNext()
    {
        return index < endIndex;
    }

    public T next()
    {
        return mapper != null ? mapper.map(results[index++]) : results[index++];
    }

    /**
     * Rzuca wyj�tek UnsupportedOperationException.
     */
    public void remove()
    {
        throw new UnsupportedOperationException();
    }

    public boolean hasMore()
    {
        return hasMore;
    }

    public int count()
    {
        return count;
    }

    public int totalCount()
    {
        return totalCount;
    }

    public T[] results()
    {
        return results;
    }

    public void setMapper(Mapper<T> mapper)
    {
        this.mapper = mapper;
    }

    // TODO: ma�o eleganckie, wymy�li� spos�b na wywo�ywanie tej metody bez parametru Class
    public static <E> SearchResults<E> emptyResults(final Class<E> c)
    {
        return new SearchResults<E>()
        {
            public boolean hasMore() { return false; }
            public int count() { return 0; }
            public int totalCount() { return 0; }
            public void remove() { }
            public boolean hasNext() { return false; }
            public E next() { return null; }
            public E[] results() { return (E[]) Array.newInstance(c.getClass(), 0); };
            public void setMapper(Mapper<E> mapper) { }
        };
    }
}
