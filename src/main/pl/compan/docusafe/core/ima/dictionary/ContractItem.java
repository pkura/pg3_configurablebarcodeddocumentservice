package pl.compan.docusafe.core.ima.dictionary;

import java.io.Serializable;
import java.util.List;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

public class ContractItem implements Lifecycle{
	
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(DocInCaseType.class.getPackage().getName(),null);
	
	private Integer id;
    private String cn;
    private String title;
    
    ContractItem(){
    	
    }
    
    public static List<ContractItem> list() throws EdmException{
    	return DSApi.context().session().createCriteria(ContractItem.class)
        .addOrder(Order.asc("title")).list();
    }
    
    public static ContractItem find(Integer id) throws EdmException
    {
    	ContractItem status = (ContractItem) DSApi.getObject(ContractItem.class, id);

        if (status == null)
            throw new EntityNotFoundException(ContractItem.class, id);

        return status;
    }
    
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Box)) return false;

        final ContractItem doc = (ContractItem) o;

        if (id != null ? !(id.equals(doc.id)) : doc.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public boolean onSave(Session s) throws CallbackException
    {      
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }
    
    public void onLoad(Session s, Serializable id)
    {
    }

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
}
