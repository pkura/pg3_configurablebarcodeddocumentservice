package pl.compan.docusafe.core.ima.dictionary;

import java.io.Serializable;
import java.util.List;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

public class Questionnaire  implements Lifecycle
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(Questionnaire.class.getPackage().getName(),null);
   
	private Integer id;
	private Integer iddoccase;
	private Integer idmediator;
	private String title;
	private Integer idquestions;
	private Long document_id;

	   
    
    Questionnaire()
    {
    }
   
    public static List<Questionnaire> findByTitle(String title) throws EdmException
    {    	
        return DSApi.context().session().createCriteria(Questionnaire.class).
        add(Restrictions.like("title", "%"+title+"%").ignoreCase()).list();
    }
    
    public static List<Questionnaire> list() throws EdmException
    {
    	  return DSApi.context().session().createCriteria(Questionnaire.class)
          .addOrder(Order.asc("title"))
			.list();	
    	
    }
    
    public static Questionnaire findByCaseAndMediator(Integer iddoccase, Integer idmediator)throws EdmException
    {
    	List<Questionnaire> questionnaires = DSApi.context().session().createCriteria(Questionnaire.class).add(Restrictions.eq("iddoccase", iddoccase).ignoreCase()).add(Restrictions.eq("idmediator", idmediator)).list();
    	if (questionnaires!=null && questionnaires.size()>0) return questionnaires.get(0);
    	else
    		return null;
    	
    }
    
    
    public static Questionnaire findByDocumentId(Long documentId) throws EdmException
    {
    	List<Questionnaire> questionnaires = DSApi.context().session().createCriteria(Questionnaire.class).add(Restrictions.eq("document_id", documentId)).list();
    	
    	if (questionnaires!=null && questionnaires.size()>0)
    		return questionnaires.get(0);
    	else return null;
    }
    
    public static Questionnaire find(Integer id) throws EdmException
    {
    	Questionnaire status = (Questionnaire) DSApi.getObject(Questionnaire.class, id);

        if (status == null)
            throw new EntityNotFoundException(Questionnaire.class, id);

        return status;
    }

    
    
    public Questionnaire(String title)
    {
        if (title == null)
            throw new NullPointerException("title");
        this.title = title.toUpperCase();
    }
  
    
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Box)) return false;

        final Questionnaire doc = (Questionnaire) o;

        if (id != null ? !(id.equals(doc.id)) : doc.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public boolean onSave(Session s) throws CallbackException
    {      
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }
    
    public String toString() {
    	return ""+id+" "+title;
    }

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getIddoccase() {
		return iddoccase;
	}

	public void setIddoccase(Integer iddoccase) {
		this.iddoccase = iddoccase;
	}

	public Integer getIdmediator() {
		return idmediator;
	}

	public void setIdmediator(Integer idmediator) {
		this.idmediator = idmediator;
	}

	public Integer getIdquestions() {
		return idquestions;
	}

	public void setIdquestions(Integer idquestions) {
		this.idquestions = idquestions;
	}

	public Long getDocument_id() {
		return document_id;
	}

	public void setDocument_id(Long documentId) {
		document_id = documentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	

}
