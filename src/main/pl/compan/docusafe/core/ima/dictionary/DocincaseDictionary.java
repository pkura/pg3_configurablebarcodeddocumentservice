package pl.compan.docusafe.core.ima.dictionary;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

@Entity
@Table(name = "ds_docincase_dictionary")
public class DocincaseDictionary {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private Long id;

	/** Id powiazanego dokumentu */
	@Column(name = "DOCUMENT_ID")
	private Long document_id;

	/** FieldCN */
	@Column(name = "FIELD_CN", nullable = false, length = 50)
	private String field_cn;

	/** FieldVal */
	@Column(name = "FIELD_VAL", nullable = false, length = 50)
	private String field_val;

	@Deprecated
	// Dla Hibernate
	public DocincaseDictionary() {
	}

	public DocincaseDictionary(String fieldCn, Long documentId, String fieldVal) {
		this.field_cn = fieldCn;
		this.document_id = documentId;
		this.field_val = fieldVal;
	}

	public Long getId() {
		return id;
	}

	
	public void setId(Long id) {
		this.id = id;
	}

	

	public Long getDocument_id() {
		return document_id;
	}

	public void setDocument_id(Long documentId) {
		document_id = documentId;
	}

	public String getField_cn() {
		return field_cn;
	}

	public void setField_cn(String fieldCn) {
		field_cn = fieldCn;
	}

	public String getField_val() {
		return field_val;
	}

	public void setField_val(String fieldVal) {
		field_val = fieldVal;
	}

	public boolean save() throws EdmException {

		try {
			DSApi.context().session().saveOrUpdate(this);
			return true;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static DocincaseDictionary find(Integer id) throws EdmException {
		DocincaseDictionary status = (DocincaseDictionary) DSApi.getObject(
				DocincaseDictionary.class, id);

		if (status == null)
			throw new EntityNotFoundException(DocincaseDictionary.class, id);

		return status;
	}

	/** 
	 * Metoda wyszukuje wszystkie powiazania pomiedzy 
	 * @param documentId
	 * @return
	 * @throws EdmException
	 */
	public static List<DocincaseDictionary> findByDocumentId(Long documentId)
			throws EdmException {
		return  (DSApi.context().session().createCriteria(
				DocincaseDictionary.class).add(
				Restrictions.eq("document_id", documentId)).list());
	}

	public static DocincaseDictionary findByCaseAndDocincase(Long caseId,
			Long docincaseId) throws EdmException {
		List list = (List) (DSApi.context().session().createCriteria(
				DocincaseDictionary.class).add(
				Restrictions.eq("document_id", docincaseId)).add(
				Restrictions.eq("field_val", caseId.toString())).add(
				Restrictions.eq("field_cn", "DS_DOCCASE")).list());
		if (list != null && list.size() > 0)
			return (DocincaseDictionary) list.get(0);
		else
			return null;
	}

	public void create() throws EdmException {
		try {
			DSApi.context().session().save(this);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmException("Blad dodania slownika" + e.getMessage());
		}
	}

}
