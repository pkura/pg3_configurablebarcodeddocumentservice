package pl.compan.docusafe.core.ima.dictionary;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

@Entity
@Table(name = "ds_docincase")
public class Docincase  implements Lifecycle
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(Docincase.class.getPackage().getName(),null);
   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private Integer id;
	
	@Column(name = "DOCUMENT_ID", nullable=false)
	private Long document_id;
	
	@Column(name = "deadline_time", nullable = true)
	private Date deadline_time;

	@Column(name = "doctype", nullable = true)
	private String doctype;
	
	@Column(name = "docstatus", nullable = true)
	private String docstatus;
	
	@Column(name = "name", nullable = true)
	private String name;
	
	@Column(name = "dsuser", nullable = true)
	private String dsuser;
	
	@Column(name = "taskdescription", nullable = true)
	private String taskdescription;

	@Column(name = "iddoccase", nullable = true)
	private String iddoccase;
	
	
	Docincase()
    {
    }
          
    public static Docincase find(Integer id) throws EdmException
    {
    	Docincase status = (Docincase) DSApi.getObject(Docincase.class, id);

        if (status == null)
            throw new EntityNotFoundException(Docincase.class, id);

        return status;
    }
    
    public static List<Docincase> findByRelatedDocumentId(String documentId)throws EdmException
    {
    	return DSApi.context().session().createCriteria(Docincase.class).add(Restrictions.eq("iddoccase", documentId)).list();
    }
    
    
    public static Docincase findByDocumentId(Long documentId) throws EdmException{
    	return (Docincase)(DSApi.context().session().createCriteria(Docincase.class).add(Restrictions.eq("document_id", documentId)).list().get(0));
    }
    
  
    
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Box)) return false;

        final Docincase doc = (Docincase) o;

        if (id != null ? !(id.equals(doc.id)) : doc.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public boolean onSave(Session s) throws CallbackException
    {      
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }
    
    
    public String toString() {
    	return ""+id;
    }

   

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	
	public Date getDeadline_time() {
		return deadline_time;
	}


	public void setDeadline_time(Date deadlineTime) {
		deadline_time = deadlineTime;
	}


	public String getDoctype() {
		return doctype;
	}


	public void setDoctype(String doctype) {
		this.doctype = doctype;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDsuser() {
		return dsuser;
	}


	public void setDsuser(String dsuser) {
		this.dsuser = dsuser;
	}


	public String getTaskdescription() {
		return taskdescription;
	}


	public void setTaskdescription(String taskdescription) {
		this.taskdescription = taskdescription;
	}


	public String getIddoccase() {
		return iddoccase;
	}


	public void setIddoccase(String iddoccase) {
		this.iddoccase = iddoccase;
	}


	public String getDocstatus() {
		return docstatus;
	}


	public void setDocstatus(String docstatus) {
		this.docstatus = docstatus;
	}

	
    
    
	public Long getDocument_id() {
		return document_id;
	}

	public void setDocument_id(Long documentId) {
		document_id = documentId;
	}

}
