package pl.compan.docusafe.core.ima.dictionary;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

@Entity
@Table(name = "ds_doccase")
public class Doccase  implements Lifecycle
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(Doccase.class.getPackage().getName(),null);
   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private Integer id;
	
	@Column(name = "DOCUMENT_ID", nullable=false)
	private Long document_id;
	
	@Column(name = "casetype", nullable = true)
	private String casetype;
	
	@Column(name = "casestatus", nullable = true)
	private String casestatus;
	
	@Column(name = "title", nullable = true)
	private String title;
	
	@Column(name = "number", nullable = true)
	private String number;
    
    
	Doccase()
    {
    }
          
    public static Doccase find(Integer id) throws EdmException
    {
    	Doccase status = (Doccase) DSApi.getObject(Doccase.class, id);

        if (status == null)
            throw new EntityNotFoundException(Doccase.class, id);

        return status;
    }
    
    
    public static Doccase findByDocumentId(Long documentId) throws EdmException{
    	return (Doccase)(DSApi.context().session().createCriteria(Doccase.class).add(Restrictions.eq("document_id", documentId)).list().get(0));
    }
    
  
    
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Box)) return false;

        final Doccase doc = (Doccase) o;

        if (id != null ? !(id.equals(doc.id)) : doc.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public boolean onSave(Session s) throws CallbackException
    {      
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }
        
    public String toString() {
    	return ""+id;
    }
 
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	public String getCasetype() {
		return casetype;
	}

	public void setCasetype(String casetype) {
		this.casetype = casetype;
	}

	public String getCasestatus() {
		return casestatus;
	}

	public void setCasestatus(String casestatus) {
		this.casestatus = casestatus;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Long getDocument_id() {
		return document_id;
	}

	public void setDocument_id(Long documentId) {
		document_id = documentId;
	}


}
