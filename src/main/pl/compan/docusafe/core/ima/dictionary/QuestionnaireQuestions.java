package pl.compan.docusafe.core.ima.dictionary;

import java.io.Serializable;
import java.util.List;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

public class QuestionnaireQuestions  implements Lifecycle
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(QuestionnaireQuestions.class.getPackage().getName(),null);
   
	private Integer id;
	private Integer q1;
	private Integer q2;
	private Integer q3;
	private Integer q4;
	private Integer q5;
	private Integer q6;
	private Long document_id;

	   
    
    QuestionnaireQuestions()
    {
    }
         
    
    public static QuestionnaireQuestions find(Integer id) throws EdmException
    {
    	QuestionnaireQuestions status = (QuestionnaireQuestions) DSApi.getObject(QuestionnaireQuestions.class, id);

        if (status == null)
            throw new EntityNotFoundException(QuestionnaireQuestions.class, id);

        return status;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Box)) return false;

        final QuestionnaireQuestions doc = (QuestionnaireQuestions) o;

        if (id != null ? !(id.equals(doc.id)) : doc.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public boolean onSave(Session s) throws CallbackException
    {      
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    	
    }  

  

	public Integer getQ1() {
		return q1;
	}


	public void setQ1(Integer q1) {
		this.q1 = q1;
	}


	public Integer getQ2() {
		return q2;
	}


	public void setQ2(Integer q2) {
		this.q2 = q2;
	}


	public Integer getQ3() {
		return q3;
	}


	public void setQ3(Integer q3) {
		this.q3 = q3;
	}


	public Integer getQ4() {
		return q4;
	}


	public void setQ4(Integer q4) {
		this.q4 = q4;
	}


	public Integer getQ5() {
		return q5;
	}


	public void setQ5(Integer q5) {
		this.q5 = q5;
	}


	public Integer getQ6() {
		return q6;
	}


	public void setQ6(Integer q6) {
		this.q6 = q6;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getDocument_id() {
		return document_id;
	}

	public void setDocument_id(Long documentId) {
		document_id = documentId;
	}
	


	public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	

}
