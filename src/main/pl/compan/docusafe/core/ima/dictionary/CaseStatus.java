package pl.compan.docusafe.core.ima.dictionary;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

@Entity
@Table(name = "ds_ima_case_status")
public class CaseStatus  implements Lifecycle
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(CaseStatus.class.getPackage().getName(),null);
   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private Integer id;
	
	@Column(name = "cn", nullable = true)
    private String cn;
	
	@Column(name = "title", nullable = true)
    private String title;
	
	@Column(name = "centrum", nullable = true)
    private int centrum;
	
	@Column(name = "refValue", nullable = true)
    private String refValue;
	
	@Column(name = "available", nullable = true)
    private boolean available;  
	
	@Column(name = "language", nullable = true)
	private String language;
    
    

	CaseStatus()
    {
    }
   
    public static List<CaseStatus> findByTitle(String title) throws EdmException
    {    	
        return DSApi.context().session().createCriteria(CaseStatus.class).
        add(Restrictions.like("title", "%"+title+"%").ignoreCase()).list();
    }
    
    /**
     * Metoda zwraca Status Sprawy po CN oraz Jezyku
     * @param cn
     * @param language
     * @return
     * @throws EdmException
     */
    public static CaseStatus findByCnAndLanguage(String cn, String language) throws EdmException
    {    	
        
        return (CaseStatus)DSApi.context().session().createCriteria(CaseStatus.class).add(Restrictions.eq("cn", cn).ignoreCase()).
        add(Restrictions.eq("language", language).ignoreCase()).list().get(0);  
        
    }
    
    public static List<CaseStatus> list() throws EdmException
    {
    	  return DSApi.context().session().createCriteria(CaseStatus.class)
          .addOrder(Order.asc("title"))
			.list();	
    	
    }
    
    public static CaseStatus find(Integer id) throws EdmException
    {
    	CaseStatus status = (CaseStatus) DSApi.getObject(CaseStatus.class, id);

        if (status == null)
            throw new EntityNotFoundException(CaseStatus.class, id);

        return status;
    }

    public CaseStatus(String title)
    {
        if (title == null)
            throw new NullPointerException("title");
        this.title = title.toUpperCase();
    }
  
    
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Box)) return false;

        final CaseStatus doc = (CaseStatus) o;

        if (id != null ? !(id.equals(doc.id)) : doc.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public boolean onSave(Session s) throws CallbackException
    {      
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }
    
    public String toString() {
    	return ""+id+" "+title;
    }

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCentrum() {
		return centrum;
	}

	public void setCentrum(int centrum) {
		this.centrum = centrum;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
