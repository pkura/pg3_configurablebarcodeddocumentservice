package pl.compan.docusafe.core.ima.dictionary;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

@Entity
@Table(name = "ds_ima_mediator")
public class Mediator  implements Lifecycle
{	   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private Integer id;
	
	@Column(name = "firstname", nullable = true)
	private String firstname;	

	@Column(name = "lastname", nullable = true)
	private String lastname;	

	@Column(name = "email", nullable = true)
	private String email;	

	@Column(name = "domain", nullable = true)
	private String domain;	

	@Column(name = "rq1", nullable = true)
	private Integer rq1;
	
	@Column(name = "rq2", nullable = true)
	private Integer rq2;
	
	@Column(name = "rq3", nullable = true)
	private Integer rq3;
	
	@Column(name = "rq4", nullable = true)
	private Integer rq4;
	
	@Column(name = "rq5", nullable = true)
	private Integer rq5;
	
	@Column(name = "rq6", nullable = true)
	private Integer rq6;
	
	@Column(name = "note", nullable = true)
	private Integer note;
	
	@Column(name = "iddsuser", nullable=true)
	private Long iddsuser;
	
	@Column(name = "document_id", nullable=false)
	private Long document_id;
	
	@Column(name = "iban", nullable = true)
	private String iban;	
	
	@Column(name = "bic", nullable = true)
	private String bic;	
		
	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	

	public Integer getRq1() {
		return rq1;
	}

	public void setRq1(Integer rq1) {
		this.rq1 = rq1;
	}

	public Integer getRq2() {
		return rq2;
	}

	public void setRq2(Integer rq2) {
		this.rq2 = rq2;
	}

	public Integer getRq3() {
		return rq3;
	}

	public void setRq3(Integer rq3) {
		this.rq3 = rq3;
	}

	public Integer getRq4() {
		return rq4;
	}

	public void setRq4(Integer rq4) {
		this.rq4 = rq4;
	}

	public Integer getRq5() {
		return rq5;
	}

	public void setRq5(Integer rq5) {
		this.rq5 = rq5;
	}

	public Integer getRq6() {
		return rq6;
	}

	public void setRq6(Integer rq6) {
		this.rq6 = rq6;
	}
    
    
	public Long getDocument_id() {
		return document_id;
	}

	public void setDocument_id(Long documentId) {
		document_id = documentId;
	}

	public Long getIddsuser() {
		return iddsuser;
	}

	public void setIddsuser(Long iddsuser) {
		this.iddsuser = iddsuser;
	}

	Mediator()
    {
    }
    
    public static List<Mediator> findByFirstname(String firstname) throws EdmException
    {
    	return DSApi.context().session().createCriteria(Mediator.class).add(Restrictions.like("firstname","%"+firstname+"%").ignoreCase()).list();
    }
       
    public static List<Mediator> list() throws EdmException
    {
    	  return DSApi.context().session().createCriteria(Mediator.class)
          .addOrder(Order.asc("lastname")).addOrder(Order.asc("firstname"))
			.list();	
    	
    }
    
    public static Mediator find(Integer id) throws EdmException
    {
    	Mediator status = (Mediator) DSApi.getObject(Mediator.class, id);

        if (status == null)
            throw new EntityNotFoundException(Mediator.class, id);

        return status;
    }
    
    public static List<Mediator> findByDSUserId(Long idDsUser)throws EdmException{
    	return DSApi.context().session().createCriteria(Mediator.class).add(Restrictions.eq("iddsuser", idDsUser)).list();
    }
    
    public static Mediator findByDocumentId(Long documentId) throws EdmException{
    	return (Mediator)(DSApi.context().session().createCriteria(Mediator.class).add(Restrictions.eq("document_id", documentId)).list().get(0));
    }
    

    /*public ImaMediator(String title)
    {
        if (title == null)
            throw new NullPointerException("title");
        this.title = title.toUpperCase();
    }*/
  
    
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Box)) return false;

        final Mediator doc = (Mediator) o;

        if (id != null ? !(id.equals(doc.id)) : doc.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public boolean onSave(Session s) throws CallbackException
    {      
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }
    
    
    public String toString() {
    	return ""+id+" "+lastname+" "+firstname;
    }

   

	public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	public Integer getNote() {
		return note;
	}

	public void setNote(Integer note) {
		this.note = note;
	}

}
