package pl.compan.docusafe.core.ima.dictionary;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.util.StringManager;

public class DocInCaseStatus  implements Lifecycle
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(DocInCaseStatus.class.getPackage().getName(),null);
   
	private Integer id;
    private String cn;
    private String title;
    private int centrum;
    private String refValue;
    private boolean available;  
    
    DocInCaseStatus()
    {
    }
   
    public static List<DocInCaseStatus> findByTitle(String title) throws EdmException
    {    	
        return DSApi.context().session().createCriteria(DocInCaseStatus.class).
        add(Restrictions.like("title", "%"+title+"%").ignoreCase()).list();
    }
    
    public static List<DocInCaseStatus> list() throws EdmException
    {
    	  return DSApi.context().session().createCriteria(DocInCaseStatus.class)
          .addOrder(Order.asc("title"))
			.list();	
    	
    }
    
    public static DocInCaseStatus find(Integer id) throws EdmException
    {
    	DocInCaseStatus status = (DocInCaseStatus) DSApi.getObject(DocInCaseStatus.class, id);

        if (status == null)
            throw new EntityNotFoundException(DocInCaseStatus.class, id);

        return status;
    }

    public DocInCaseStatus(String title)
    {
        if (title == null)
            throw new NullPointerException("title");
        this.title = title.toUpperCase();
    }
  
    
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Box)) return false;

        final DocInCaseStatus doc = (DocInCaseStatus) o;

        if (id != null ? !(id.equals(doc.id)) : doc.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public boolean onSave(Session s) throws CallbackException
    {      
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }
    
    public String toString() {
    	return ""+id+" "+title;
    }

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCentrum() {
		return centrum;
	}

	public void setCentrum(int centrum) {
		this.centrum = centrum;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	

}
