package pl.compan.docusafe.core.ima.dictionary;

import java.io.Serializable;
import java.util.List;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

public class MediatorNotes  implements Lifecycle
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(MediatorNotes.class.getPackage().getName(),null);
   
	private Integer idmediator;
	private Integer rq1;
	private Integer rq2;
	private Integer rq3;
	private Integer rq4;
	private Integer rq5;
	private Integer rq6;
	   
    
    public MediatorNotes()
    {
    }
         
    
    public static MediatorNotes findByMediatorId(Integer idmediator) throws EdmException
    {
    	List<MediatorNotes> notes = DSApi.context().session().createCriteria(MediatorNotes.class).add(Restrictions.eq("idmediator", idmediator)).list();
    	
    	if (notes!=null && notes.size()>0)
    		return notes.get(0);
    	else return null;
    }
    
    public static MediatorNotes find(Integer idmediator) throws EdmException
    {
    	MediatorNotes status = (MediatorNotes) DSApi.getObject(MediatorNotes.class, idmediator);

        if (status == null)
            throw new EntityNotFoundException(MediatorNotes.class, idmediator);

        return status;
    }

   
    public boolean onSave(Session s) throws CallbackException
    {      
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    	
    }
    

	public Integer getRq1() {
		return rq1;
	}


	public void setRq1(Integer rq1) {
		this.rq1 = rq1;
	}


	public Integer getRq2() {
		return rq2;
	}


	public void setRq2(Integer rq2) {
		this.rq2 = rq2;
	}


	public Integer getRq3() {
		return rq3;
	}


	public void setRq3(Integer rq3) {
		this.rq3 = rq3;
	}


	public Integer getRq4() {
		return rq4;
	}


	public void setRq4(Integer rq4) {
		this.rq4 = rq4;
	}


	public Integer getRq5() {
		return rq5;
	}


	public void setRq5(Integer rq5) {
		this.rq5 = rq5;
	}


	public Integer getRq6() {
		return rq6;
	}


	public void setRq6(Integer rq6) {
		this.rq6 = rq6;
	}


	public Integer getIdmediator() {
		return idmediator;
	}


	public void setIdmediator(Integer idmediator) {
		this.idmediator = idmediator;
	}


	public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	

}
