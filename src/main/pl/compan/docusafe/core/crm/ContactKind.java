package pl.compan.docusafe.core.crm;

import java.util.List;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

/**
 * Rodzaj kontaktu crm
 * @author Mariusz Kilja�czyk
 *
 */

public class ContactKind
{
    private static final Logger log = LoggerFactory.getLogger(ContactKind.class);
    
    private Long   id;
    private String name;
    private String cn;
    
    
    StringManager sm = 
        GlobalPreferences.loadPropertiesFile(ContactKind.class.getPackage().getName(),null);
    
    public ContactKind()
    {
    }
    
    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania ContactKind "+e.getMessage());
            throw new EdmException("Blad dodania ContactKind"+e.getMessage());
        }
    }

    public static ContactKind find(Long id) throws EdmException
    {
        StringManager smL = GlobalPreferences.loadPropertiesFile(ContactKind.class.getPackage().getName(),null);
        ContactKind inst = DSApi.getObject(ContactKind.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono ContactKind "+id);
        else 
            return inst;
    }
    
    public static ContactKind findByName(String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(ContactKind.class);
            c.add(Restrictions.eq("name",name).ignoreCase());
            List<ContactKind> clist = (List<ContactKind>) c.list();
            if(clist.size() > 0)
            	return clist.get(0);
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania ContactKind o nazwie "+ name +" " + e);
		}
        return null;
    }
    
	public static ContactKind findByCn(String cn)
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(ContactKind.class);

            c.add(Expression.like("cn",cn).ignoreCase());

            List<ContactKind> clist = (List<ContactKind>) c.list();
            if(clist.size() > 0)
            	return clist.get(0);
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania ContactKind o cn "+ cn +" " + e);
		}
        return null;
	}
    
    public static List<ContactKind> contactKindList() throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(ContactKind.class);
        return (List<ContactKind>) c.list();

    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String nazwa)
    {
        this.name = nazwa;
    }

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}
}