package pl.compan.docusafe.core.crm;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * Rodzaj konkurencji crm
 * @author Mariusz Kilja�czyk
 *
 */

public class CompetitionKind
{
    private static final Logger log = LoggerFactory.getLogger(CompetitionKind.class);
    
    private Long   id;
    private String name;
    private String cn;
    
    
    StringManager sm = 
        GlobalPreferences.loadPropertiesFile(CompetitionKind.class.getPackage().getName(),null);
    
    public CompetitionKind()
    {
    }
    
    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania CompetitionKind "+e.getMessage());
            throw new EdmException("Blad dodania CompetitionKind"+e.getMessage());
        }
    }

    public static CompetitionKind find(Long id) throws EdmException
    {
        StringManager smL = GlobalPreferences.loadPropertiesFile(CompetitionKind.class.getPackage().getName(),null);
        CompetitionKind inst = DSApi.getObject(CompetitionKind.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono CompetitionKind "+id);
        else 
            return inst;
    }
    
    public static CompetitionKind findByName(String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(CompetitionKind.class);
            c.add(Expression.like("name",name).ignoreCase());
            List<CompetitionKind> clist = (List<CompetitionKind>) c.list();
            if(clist.size() > 0)
            	return clist.get(0);
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania CompetitionKind o nazwie "+ name +" " + e);
		}
        return null;
    }
    
	public static CompetitionKind findByCn(String cn)
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(CompetitionKind.class);

            c.add(Expression.like("cn",cn).ignoreCase());

            List<CompetitionKind> clist = (List<CompetitionKind>) c.list();
            if(clist.size() > 0)
            	return clist.get(0);
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania CompetitionKind o cn "+ cn +" " + e);
		}
        return null;
	}
    
    public static List<CompetitionKind> list() throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(CompetitionKind.class);
        return (List<CompetitionKind>) c.list();

    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String nazwa)
    {
        this.name = nazwa;
    }

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}
}