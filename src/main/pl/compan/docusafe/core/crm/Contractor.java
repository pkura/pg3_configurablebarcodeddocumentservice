package pl.compan.docusafe.core.crm;


import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.ilpol.CrmVindicationLogic;
import pl.compan.docusafe.parametrization.ilpol.DLBinder;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.QueryForm.SortField;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

/**
 * S�ownik klient�w dla dokument�w leasingowych
 * oraz kontrahentow dla crm
 * @author Mariusz Kilja�czyk
 *
 */

public class Contractor extends AbstractDocumentKindDictionary
{
    private static final Logger log = LoggerFactory.getLogger("kontrahent_log");
    
    private Long   id;
    private String name;
    private String fullName;
    private String nip;
    private String regon;
    private String krs;
    private String street;
    private String code;
    private String city;
    private String country;
    private String remarks;
    private String contractorNo;
    private String phoneNumber;
    private String phoneNumber2;
    private String fax;
    private String email;
    private String www;
    private Region region;
    private Set<Marka> marka;
    private Set<Machine> machine;
    private Set<Role> role;
    private List<Competition> competitions;
    private List<CooperationTerm> cooperationTerms;
    private Set<Patron> patrons;
    private Double obrot;
    private Long defaultWorkerId; 
    private Date ctime;


    StringManager sm = GlobalPreferences.loadPropertiesFile(Contractor.class.getPackage().getName(),null);
    
    public Contractor()
    {
    	role = new HashSet<Role>();
    }
    
    public String dictionaryAction()
    {
        return "/office/common/contractor.action";
    }
    public Map<String,String> dictionaryAttributes()
    {
        StringManager smL = GlobalPreferences.loadPropertiesFile(Contractor.class.getPackage().getName(),null);
        Map<String,String> map = new LinkedHashMap<String,String>();
        map.put("name", smL.getString("Nazwa"));
        map.put("phoneNumber", smL.getString("Telefon"));

        return map;
    }

    
	public Integer getHeight()
	{
		return 770;
	}
	
	public Integer getWidth()
	{
		return 700;
	}
    
    /**
     * Zwraca stringa zawieraj�cego nazwe funkcji (najlepiej umieszczonej w pliku dockind.js)
     *  kt�ra wykona si� w funkcji accept_cn
     * 
     */
    public String dictionaryAccept()
    {
        return "accept_contractor(map);";              
    }
    
    
    public String getDictionaryDescription()
    {
        return name;
    }
    
	public void save() throws EdmException
    {
		DSApi.context().session().update(this);
        DSApi.context().session().flush();
		log.debug("U�ytkownik "+DSApi.context().getPrincipalName()+
        		" ZAPISA� 'Dane podstawowe'. Dane : \n" +this.toString());
    }
    /**
     * Zapisuje strone w sesji hibernate, sprawdza cxzy nie ma kontrahenta o danym numerze nip
     * @throws EdmException
     */
    public void create() throws EdmException
    {
    	create(false);
    }
	
    /**
     * Zapisuje strone w sesji hibernate, jesli duplicate = true nie sprawdza czy duplikat
     * @throws EdmException
     */
    public void create(boolean duplicate) throws EdmException
    {
        try 
        {
        	boolean isNew = this.id == null;
        	if(!duplicate && isNew && nip != null && nip.length() > 0)
        	{
        		List<Contractor> list = findByNIP(nip);
        		if(list != null && list.size() > 0)
        		{
        			throw new DSException("Kontrahent o numerze "+nip+
        					" ju� istnieje "+
        					(list.get(0).getPatrons() != null ?
        					"i nale�y do "+getPatronsNames()
        					: ""));
        		}
        	}
        	if(ctime == null)
        	{
        		this.ctime = new java.util.Date();
        	}
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
            log.debug("U�ytkownik "+DSApi.context().getPrincipalName()+
            		(isNew? " DODA� " : " ZAPISA� ")+" 'Dane podstawowe'. Dane : \n" +this.toString());
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania klienta dokumentu leasingowego. "+e.getMessage());
            throw new EdmException(sm.getString("BladDodaniaKlientaDokumentuLeasingowego")+e.getMessage(),e);
        }
    }
    
    public String getPatronsNames() throws EdmException {
		StringBuffer sb = new StringBuffer();
		for (Patron p : getPatrons()) 
		{
			sb.append(DSUser.findByUsername(p.getUsername()).asFirstnameLastname());
			sb.append(", ");
		}
		return sb.toString();
	}

	public String toString()
    {
    	return "id = "+id+"\n"+
    	"name = "+name+"\n"+
    	"fullName = "+fullName+"\n"+
    	"nip = "+nip+"\n"+
    	"regon = "+regon+"\n"+
    	"krs = "+krs+"\n"+
    	"street = "+street+"\n"+
    	"code = "+code+"\n"+
    	"city = "+city+"\n"+
    	"country = "+country+"\n"+
    	"remarks = "+remarks+"\n"+
    	"contractorNo = "+contractorNo+"\n"+
    	"phoneNumber = "+phoneNumber+"\n"+
    	"phoneNumber2 = "+phoneNumber2+"\n"+
    	"fax = "+fax+"\n"+
    	"email = "+email+"\n"+
    	"www = "+www+"\n"+
    	"region = "+(region != null ? region.getName():"")+"\n"+
    	"ctime = "+ctime+"\n";
    }

    public Contractor find(Long id) throws EdmException
    {
        StringManager smL = GlobalPreferences.loadPropertiesFile(Contractor.class.getPackage().getName(),null);
        Contractor inst = DSApi.getObject(Contractor.class, id);
        if (inst == null) 
            throw new EdmException(smL.getString("NieZnalezionoKlientaOnr",id));
        else 
            return inst;
    }

    public static Contractor findById(Long id) throws EdmException
    {
        StringManager smL = GlobalPreferences.loadPropertiesFile(Contractor.class.getPackage().getName(),null);
        Contractor inst = DSApi.getObject(Contractor.class, id);
        if (inst == null) 
            throw new EdmException(smL.getString("NieZnalezionoKlientaOnr",id));
        else 
            return inst;
    }
    
    public void addPatron(String username,String own)
    {

    	if(patrons == null)
    		patrons = new HashSet<Patron>();
		patrons.add(Patron.findOrCreate(username, this,own));
    }
    
    public void delete() throws EdmException
    {
        try 
        {
            DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
            DockindQuery dockindQuery = new DockindQuery(0,0);
            dockindQuery.setDocumentKind(kind);
            
            Field f = kind.getFieldByCn(DlLogic.KLIENT_CN);
            dockindQuery.field(f,id);
            SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
            if (!(searchResults == null || searchResults.totalCount()<1))
                throw new EdmException(sm.getString("NieMoznaUsunacTejInstytucjiKontrahentaPoniewazSaDoNiejPrzypisaneDokumenty"));
            
            QueryForm form = new QueryForm(0, 100);
            form.addProperty("idKlienta", new Integer(id.toString()));
            SearchResults<? extends DlContractDictionary> results = DlContractDictionary.search(form);
            if (!(results == null || results.totalCount() == 0))
                throw new EdmException(sm.getString("NieMoznaUsunacTejInstytucjiKontrahentaPoniewazSaDoNiejPrzypisaneDokumenty"));
           
            form = new QueryForm(0, 100);
            form.addProperty("idKlienta", new Integer(id.toString()));
            SearchResults<? extends DlApplicationDictionary> results2 = DlApplicationDictionary.search(form);

            if (!(results2 == null || results2.totalCount() == 0))
                throw new EdmException(sm.getString("NieMoznaUsunacTejInstytucjiKontrahentaPoniewazSaDoNiejPrzypisaneDokumenty"));
            
            kind = DocumentKind.findByCn(DocumentLogicLoader.DL_BINDER);
            dockindQuery = new DockindQuery(0,0);
            dockindQuery.setDocumentKind(kind);
            f = kind.getFieldByCn(DLBinder.KLIENT_FIELD_CN);
            dockindQuery.field(f,id);
            searchResults = DocumentKindsManager.search(dockindQuery);
            if (!(searchResults == null || searchResults.totalCount()<1))
                throw new EdmException(sm.getString("NieMoznaUsunacTejInstytucjiKontrahentaPoniewazSaDoNiejPrzypisaneDokumenty"));
            
            dockindQuery = new DockindQuery(0,0);
            dockindQuery.setDocumentKind(kind);
            f = kind.getFieldByCn(DLBinder.DOSTAWCA_FIELD_CN);
            dockindQuery.field(f,id);
            searchResults = DocumentKindsManager.search(dockindQuery);
            if (!(searchResults == null || searchResults.totalCount()<1))
                throw new EdmException(sm.getString("NieMoznaUsunacTejInstytucjiKontrahentaPoniewazSaDoNiejPrzypisaneDokumenty"));
            
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();  
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia klienta dokumentu leasingowego");
            throw new EdmException(sm.getString("BladUsunieciaKlientaDokumentuLeasingowego")+e.getMessage());
        }
        
    }
    /** Zwraca mape zawieraj�c� kolekcje dokument�w zwi�zanych z kontrahentem */
    public static Map<String,SearchResults> findDocument(Long idKilenta) throws EdmException
    {
    	Map<String, SearchResults> map = new TreeMap<String, SearchResults>();
    	DockindQuery dockindQuery = null; 
    	DocumentKind kind = null;
    	Field f = null;
    	
    	kind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
    	dockindQuery = new DockindQuery(0,0);
        dockindQuery.setDocumentKind(kind);        
        f = kind.getFieldByCn(DlLogic.KLIENT_CN);
        dockindQuery.field(f,idKilenta);
        SearchResults<Document> dlDoc = DocumentKindsManager.search(dockindQuery);
        
        kind = DocumentKind.findByCn(DocumentLogicLoader.CRM_VINDICATION_KIND);
        dockindQuery = new DockindQuery(0,0);
        dockindQuery.setDocumentKind(kind); 
        f = kind.getFieldByCn(CrmVindicationLogic.NUMER_KONTRAHENTA_FIELD_CN);
        dockindQuery.field(f,idKilenta);
        SearchResults<Document> crmDoc = DocumentKindsManager.search(dockindQuery);
        
        
        QueryForm form = new QueryForm(0,0);
        form.addProperty("idKlienta", idKilenta.intValue());
        SearchResults<? extends DlContractDictionary> contractResults = DlContractDictionary.search(form);

       
        form = new QueryForm(0, 0);
        form.addProperty("idKlienta", idKilenta.intValue());
        SearchResults<? extends DlApplicationDictionary> applicationResults = DlApplicationDictionary.search(form);        
        
        
        
        map.put(DocumentLogicLoader.DL_KIND, dlDoc);
        map.put(DocumentLogicLoader.CRM_VINDICATION_KIND, crmDoc);
        map.put("application", applicationResults);
        map.put("contract", contractResults);
        
		return map;

    	
    }
    
    public static List<Contractor> findByNIP(String nip)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Contractor.class);
            if(nip != null)
            	c.add(Restrictions.eq("nip", nip));
            else
            	c.add(Restrictions.isNull("nip"));

            return (List<Contractor>) c.list();
        }
        catch (Exception e) 
		{
			log.error("B��d wyszukania kontrahenta po numerze contractorNo " + e);			
		}
        return null;
    }
    
    public static List<Contractor> findByEmail(String email)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Contractor.class);
            if(email != null)
            	c.add(Restrictions.eq("email", email));
            else
            	c.add(Restrictions.isNull("email"));

            return (List<Contractor>) c.list();
        }
        catch (Exception e) 
		{
			log.error("B��d wyszukania kontrahenta po numerze contractorNo " + e);			
		}
        return null;
    }

    public static List<Contractor> findByFaxNumber(String faxNumber)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Contractor.class);
            if(faxNumber != null)
                c.add(Restrictions.eq("fax", faxNumber));
            else
                c.add(Restrictions.isNull("fax"));

            return (List<Contractor>) c.list();
        }
        catch (Exception e)
        {
            log.error("B��d wyszukania kontrahenta po numerze contractorNo " + e);
        }
        return null;
    }

    public static List<Contractor> findByDefaultWorkerId(Long defaultWorkerId)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Contractor.class);
            if(defaultWorkerId != null)
            	c.add(Restrictions.eq("defaultWorkerId", defaultWorkerId));
            else
            	c.add(Restrictions.isNull("defaultWorkerId"));

            return (List<Contractor>) c.list();
        }
        catch (Exception e) 
		{
			log.error("B��d wyszukania kontrahenta po numerze defaultWorkerId ",e);			
		}
        return null;
    }
    
    public static List<Contractor> findByNameCity(String name,String city)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Contractor.class);
            if(name != null)
            	c.add(Restrictions.eq("name", name).ignoreCase());
            if(city != null)
            	c.add(Restrictions.eq("city", city).ignoreCase());

            return (List<Contractor>) c.list();
        }
        catch (Exception e) 
		{
			log.error("B��d wyszukania kontrahenta po numerze defaultWorkerId ",e);			
		}
        return null;
    }

    public static SearchResults<Contractor> search(QueryForm form)
    {
        try
        {        
        	FromClause from = new FromClause();
            final TableAlias tabContractor = from.createTable("DSC_CONTRACTOR");
            
            final WhereClause where = new WhereClause();
            
            if (form.hasProperty("name"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("name"),"%"+(String)form.getProperty("name")+"%",true));
            }
            if (form.hasProperty("fullName"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("fullName"),"%"+(String)form.getProperty("fullName")+"%",true));
            }
            if (form.hasProperty("nip"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("nip"),"%"+(String)form.getProperty("nip")+"%"));
            }
            if (form.hasProperty("regon"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("regon"),"%"+(String)form.getProperty("regon")+"%"));
            }   
            if (form.hasProperty("krs"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("krs"),"%"+(String)form.getProperty("krs")+"%"));
            }   
            if (form.hasProperty("street"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("street"),"%"+(String)form.getProperty("street")+"%",true));
            }
            if (form.hasProperty("code"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("code"),"%"+(String)form.getProperty("code")+"%"));
            }
            if (form.hasProperty("city"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("city"),"%"+(String)form.getProperty("city")+"%",true));
            }
            if (form.hasProperty("country"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("country"),"%"+(String)form.getProperty("country")+"%",true));
            }
            if (form.hasProperty("remarks"))
            {
            	where.add(Expression.like(tabContractor.attribute("remarks"),"%"+(String)form.getProperty("remarks")+"%",true));
            }
            if (form.hasProperty("contractorDictionaryId"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("contractorDictionaryId"),"%"+(String)form.getProperty("contractorDictionaryId")+"%"));
            }
            if (form.hasProperty("contractorNo"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("contractorNo"),"%"+(String)form.getProperty("contractorNo")+"%",true));
            }     
            if (form.hasProperty("fax"))
            {
            	where.add(Expression.like(tabContractor.attribute("fax"),"%"+(String)form.getProperty("fax")+"%"));
            } 
            if (form.hasProperty("phoneNumber"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("phoneNumber"),"%"+(String)form.getProperty("phoneNumber")+"%"));
            } 
            if (form.hasProperty("phoneNumber2"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("phoneNumber2"),"%"+(String)form.getProperty("phoneNumber2")+"%"));
            } 
            if (form.hasProperty("email"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("email"),"%"+(String)form.getProperty("email")+"%",true));
            } 
            if (form.hasProperty("www"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("www"),"%"+(String)form.getProperty("www")+"%",true));
            } 
            if (form.hasProperty("ctime"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("ctime"),"%"+(String)form.getProperty("ctime")+"%",true));
            } 
            if (form.hasProperty("region"))
            {
            	where.add(Expression.eq(tabContractor.attribute("region"),form.getProperty("region")));
            }
            if (form.hasProperty("roles"))
            {
            	final TableAlias tabRef = from.createTable("DS_CONTRACTOR_TO_ROLE");
            	where.add(Expression.eqAttribute(
            			tabRef.attribute("CONTRACTOR_ID"), tabContractor.attribute("ID")));  
            	
            	Junction OR = Expression.disjunction();

            	String[] checkRoles = (String[]) form.getProperty("roles");
            	for (int i = 0; i < checkRoles.length; i++) 
            	{
            		OR.add(Expression.eq(
            				tabRef.attribute("ROLE_ID"),checkRoles[i]));
				}         
                where.add(OR);
            }
            if (form.hasProperty("obrot"))
            {
            	where.add(Expression.eq(
            			tabContractor.attribute("obrot"),form.getProperty("obrot")));
            }
            if (form.hasProperty("username"))
            {
            	where.add(Expression.like(
            			tabContractor.attribute("dsuser"),"%"+(String)form.getProperty("username")+"%"));
            }
            if (form.hasProperty("marka"))
            {
            	
            	final TableAlias tabRef = from.createTable("DS_CONTRACTOR_TO_MARKA");
            	where.add(Expression.eqAttribute(
            			tabRef.attribute("CONTRACTOR_ID"), tabContractor.attribute("ID")));  
            	
            	Junction OR = Expression.disjunction();

            	String[] checkRoles = (String[]) form.getProperty("marka");
            	for (int i = 0; i < checkRoles.length; i++) 
            	{
            		OR.add(Expression.eq(
            				tabRef.attribute("MARKA_ID"),checkRoles[i]));
				}         
                where.add(OR);
            }
            if (form.hasProperty("machine"))
            {
            	
            	final TableAlias tabRef = from.createTable("DS_CONTRACTOR_TO_machine");
            	where.add(Expression.eqAttribute(
            			tabRef.attribute("CONTRACTOR_ID"), tabContractor.attribute("ID")));  
            	
            	Junction OR = Expression.disjunction();

            	String[] checkRoles = (String[]) form.getProperty("machine");
            	for (int i = 0; i < checkRoles.length; i++) 
            	{
            		OR.add(Expression.eq(
            				tabRef.attribute("machine_ID"),checkRoles[i]));
				}         
                where.add(OR);
            }
            //Uprawnienia 
            System.out.println("SZUKA");
            if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_ALL))
            {
            	if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_REGION))
            	{
            		if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_USER))
            		{
            			return null;
            		}
            		else
            		{
            			final TableAlias tabRef = from.createTable("dsc_patron");
                    	where.add(Expression.eqAttribute(tabRef.attribute("CONTRACTORID"), tabContractor.attribute("ID")));
                    	where.add(Expression.eq(tabRef.attribute("USERNAME"), DSApi.context().getPrincipalName()));  
//            			where.add(Expression.eq(tabContractor.attribute("dsuser"),DSApi.context().getPrincipalName()));
            		}
            	}
            	else
            	{               
            		List<String> users = new ArrayList<String>();
            		for (DSDivision division : DSApi.context().getDSUser().getDivisions()) 
                    {
            			for (DSUser user : division.getUsers())
            			{
            				users.add(user.getName());
						}
            		}
            		
            		final TableAlias tabRef = from.createTable("dsc_patron");
                	where.add(Expression.eqAttribute(tabRef.attribute("CONTRACTORID"), tabContractor.attribute("ID")));
                	where.add(Expression.in(tabRef.attribute("USERNAME"),users.toArray()));  
//            		log.error("MA "+DSPermission.CONTRACTOR_REGION);
//                	final TableAlias regionTable = from.createJoinedTable("dsc_region", true, tabContractor, FromClause.LEFT_OUTER,"$l.region = $r.id" );
//                	
//                	Junction OR = pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();
//                	for (DSDivision division : DSApi.context().getDSUser().getDivisions()) 
//                	{
//                		//grupy uprawnie� s� tworzone na podtswie cn regionu (DV_+CN_REGIONU) i dlatego mozemy to tutaj tak upro�ci� 
//                		//dodanie niepotrzebnych grup nie zaszkodzi
//                		OR.add(Expression.eq(regionTable.attribute("CN"),division.getGuid().substring(3)));
//    				}         
////        			OR.add(Expression.eq(tabContractor.attribute("dsuser"),DSApi.context().getPrincipalName()));
//                    where.add(OR);
            	}
            }
            
            SelectClause selectId = new SelectClause(true);
            SelectColumn idCol = selectId.add(tabContractor, "id");
            OrderClause order = new OrderClause();
            List<SortField> l = form.getSortFields();
            for (SortField sortField : l) 
            {
            	order.add(tabContractor.attribute(sortField.getName()), sortField.isAscending());
            	selectId.add(tabContractor, sortField.getName());
			}
            
            SelectClause selectCount = new SelectClause();
            SelectColumn countCol = selectCount.addSql("count(distinct "+tabContractor.getAlias()+".id)");
            SelectQuery selectQuery;
            

            selectQuery = new SelectQuery(selectCount, from, where, null);
            ResultSet rs = selectQuery.resultSet();
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby zada�.");
            int totalCount = rs.getInt(countCol.getPosition());
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());
            
            selectQuery = new SelectQuery(selectId, from, where, order);
            if (form.getLimit() > 0)
            {
                selectQuery.setMaxResults(form.getLimit());
                selectQuery.setFirstResult(form.getOffset());
            }
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            List<Contractor> results = new ArrayList<Contractor>(form.getLimit());
            Contractor temp;
            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                temp = DSApi.context().load(Contractor.class, id);
                results.add(temp);
            }
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            return new SearchResultsAdapter<Contractor>(results, form.getOffset(), totalCount,
            		Contractor.class);
        }

        catch (Exception e)
        {
        	log.error("", e);
        }
        return null;
    }
    
    private void updateDocumentPermission(Long contractorId,String region) throws EdmException 
    {
    	try
    	{
	    	DSApi.context().session().flush();
			List<Long> docs = DlLogic.getDocumentsByContractorId(contractorId);
			Integer i = 0;
			log.error("Rozpoczynam updatePermission dla kontrahenat "+contractorId + " ilo�� "+docs.size());
			for (Long docId : docs) 
			{
				i++;
				Document document = Document.find(docId,false);
				java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
				document.getDocumentKind().logic().documentPermissions(document);
			}
			log.error("Zako�czy�");
    	}
    	catch (Exception e) 
    	{
			log.error("",e);
		}
	}

	public void setRegion(Region region) throws EdmException
	{
		if( ( this.id != null  && this.region == null && region != null ) || 
			(this.id != null && this.region != null && ((region != null && !region.getId().equals(this.region.getId()))))
		  )
		{
			updateDocumentPermission(this.getId(),region.getCn());		
		}
		this.region = region;
	}

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String daneKontaktowe)
    {
        this.remarks = daneKontaktowe;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String kod)
    {
        this.code = kod;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String kraj)
    {
        this.country = kraj;
    }

    public String getKrs()
    {
        return krs;
    }

    public void setKrs(String krs)
    {
        this.krs = krs;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String miasto)
    {
        this.city = miasto;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String nazwa)
    {
        this.name = nazwa;
    }

    public String getNip()
    {
        return nip;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String pelnaNazwa)
    {
        this.fullName = pelnaNazwa;
    }

    public String getRegon()
    {
        return regon;
    }

    public void setRegon(String regon)
    {
        this.regon = regon;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String ulica)
    {
        this.street = ulica;
    }

    public String getContractorNo()
    {
        return contractorNo;
    }

    public void setContractorNo(String contractorNo)
    {
        this.contractorNo = contractorNo;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String emial)
    {
        this.email = emial;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber2()
    {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2)
    {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getWww()
    {
        return www;
    }

    public void setWww(String www)
    {
        this.www = www;
    }
    
    public String toStringInLine()
    {
    	return "Kontrahnet: "+id+";"+
        name+";"+
        fullName+";"+
        nip+";"+
        regon+";"+
        krs+";"+
        street+";"+
        code+";"+
        city+";"+
        country+";"+
        remarks+";"+
        contractorNo+";"+
        phoneNumber+";"+
        phoneNumber2+";"+
        fax+";"+
        email+";"+
        www+";"+
        ctime+";"+
        (region != null ? region.getName():"brak")+"/n";
    }

	public Region getRegion() {
		return region;
	}

	public Set<Role> getRole() {
		return role;
	}

	public void setRole(Set<Role> role) {
		this.role = role;
	}

	public void addRole(Role role) throws EdmException 
	{
		this.role.add(role);
	}

	public List<Competition> getCompetitions() {
		return competitions;
	}

	public void setCompetitions(List<Competition> competitions) {
		this.competitions = competitions;
	}

	public void setCooperationTerms(List<CooperationTerm> cooperationTerms) {
		this.cooperationTerms = cooperationTerms;
	}

	public List<CooperationTerm> getCooperationTerms() {
		return cooperationTerms;
	}
//
//	public String getDsUser() {
//		return dsUser;
//	}
//
//	public void setDsUser(String dsUser) {
//		this.dsUser = dsUser;
//	}

	public void setObrot(Double obrot) {
		this.obrot = obrot;
	}

	public Double getObrot() {
		return obrot;
	}

	public void setDefaultWorkerId(Long defaultWorkerId) {
		this.defaultWorkerId = defaultWorkerId;
	}

	public Long getDefaultWorkerId() {
		return defaultWorkerId;
	}

	public List<String> additionalSearchParam() {
		return null;
	}

	public boolean isAjaxAvailable() 
	{
		return false;
	}

	public List<Map<String, String>> search(Object[] args) throws EdmException {
		return null;
	}

	public String getBoxDescription() 
	{
		return 
		("<b>Nazwa:</b>"+(name != null ? name : "")+
		"<br><b>Pelna nazwa:</b>"+(fullName != null ? fullName : "")+
		"<br><b>NIP:</b>"+(nip != null ? nip : "")+
		"<br><b>Regon:</b>"+(regon != null ? regon : "")+
		"<br><b>KRS:</b>"+(krs != null ? krs : "")+
		"<br><b>Ulica:</b>"+(street != null ? street : "")+
		"<br><b>Kod:</b>"+(code != null ? code : "")+
		"<br><b>Miasto:</b>"+(city != null ? city : "")+
		"<br><b>Kraj:</b>"+(country != null ? country : "")+
		"<br><b>Opis:</b>"+(remarks != null ? remarks : "")+
		"<br><b>Numer Kontrahenta:</b>"+(contractorNo != null ? contractorNo : "")+
		"<br><b>Numer telefonu:</b>"+(phoneNumber != null ? phoneNumber : "")+
		"<br><b>Numer telefonu:</b>"+(phoneNumber2 != null ? phoneNumber2 : "")+
		"<br><b>Faks:</b>"+(fax != null ? fax : "")+
		"<br><b>E-mail:</b>"+(email != null ? email : "")+
		"<br><b>www:</b>"+(www != null ? www : "")+
		"<br><b>Data rejestracji:</b>"+(ctime != null ? ctime : "")+
		"<br><b>Region:</b>"+(region != null ? region.getName():"brak")).replaceAll("\"", "");
	}

	public boolean isBoxDescriptionAvailable() {
		return true;
	}

	public Set<Marka> getMarka() {
		return marka;
	}

	public void setMarka(Set<Marka> marka) {
		this.marka = marka;
	}

	public Set<Machine> getMachine() {
		return machine;
	}

	public void setMachine(Set<Machine> machine) {
		this.machine = machine;
	}

    public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	//@overriding
	public Map<String, String> popUpDictionaryAttributes()
	{
		return null;
	}

	

	public Boolean patronsContainsUser(String username)
	{
		for (Patron p : getPatrons()) 
		{
			if(username.equalsIgnoreCase(p.getUsername()))
				return true;
		}
		return false;
	}

	public Set<Patron> getPatrons() {
		return patrons;
	}

	public void setPatrons(Set<Patron> patrons) {
		this.patrons = patrons;
	}
}