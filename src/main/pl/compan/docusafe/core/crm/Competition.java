package pl.compan.docusafe.core.crm;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import org.apache.axis.components.logger.LogFactory;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;


/**
 * Klasa reprezentujaca konkurencje w CRM
 *
 * @author Mariusz Kilja�czyk
 *
 */
public class Competition 
{
	private Long id;
	private CompetitionName competitionName;
	private CompetitionKind competitionKind;
	private Boolean oficjalna;
	private Double marzaOd;
	private Double marzaDo;
	private Double prowizjaSalon;
	private Double prowizjaHandlowiec;
	private Double nagrodaHandlowiec;
	private Double wysokoscSprzedazy;
	private Integer okres;
	private String opis;
	private Contractor contractor;
	private Date dataDodania;
	public static Map<Integer,String> okresy;

	static
	{
		okresy = new TreeMap<Integer, String>();
		okresy.put(0, "miesi�c");
		okresy.put(1, "tydzie�");
		okresy.put(2, "rok");
		
	}
	
	public void save() throws EdmException
    {
		LogFactory.getLog("kontrahent_log").debug("U�ytkownik "+DSApi.context().getPrincipalName()+
        		" ZAPISA� 'Market Intelligence'. Dane : \n" +this.toString());
    }

    public void create() throws EdmException
    {
        try 
        {
        	boolean isNew = this.id == null;
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
            LogFactory.getLog("kontrahent_log").debug("U�ytkownik "+DSApi.context().getPrincipalName()+
            		(isNew? " DODA� " : " ZAPISA� ")+" 'Market Intelligence'. Dane : \n" +this.toString());
       
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }
    public String toString()
    {
    	return "id = "+id+"\n"+
    	"competitionName = "+(competitionName != null ? competitionName.getName():"")+"\n"+
    	"competitionKind = "+(competitionKind != null ? competitionKind.getName():"")+"\n"+
    	"oficjalna = "+oficjalna+"\n"+
    	"marzaOd = "+marzaOd+"\n"+
    	"marzaDo = "+marzaDo+"\n"+
    	"prowizjaSalon = "+prowizjaSalon+"\n"+
    	"prowizjaHandlowiec = "+prowizjaHandlowiec+"\n"+
    	"nagrodaHandlowiec = "+nagrodaHandlowiec+"\n"+
    	"wysokoscSprzedazy = "+wysokoscSprzedazy+"\n"+
    	"okres = "+(okres != null ? this.okresy.get(okres):"")+"\n"+
    	"opis = "+opis+"\n"+
    	"contractor = "+(contractor != null ? contractor.getNip():"")+"\n"+
    	"dataDodania = "+dataDodania+"\n";
    }

    public void delete() throws EdmException 
    {
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Competition find(Long id) throws EdmException 
    {
        Competition worker = (Competition) DSApi.getObject(Competition.class,id);

        if (worker == null) 
        {
            throw new EntityNotFoundException(Competition.class, id);
        }

        return worker;
    }
    public String getDescription()
    {
    	return (competitionName != null ? competitionName.getName():"brak" );
    }

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public CompetitionName getCompetitionName() {
		return competitionName;
	}

	public void setCompetitionName(CompetitionName competitionName) {
		this.competitionName = competitionName;
	}

	public CompetitionKind getCompetitionKind() {
		return competitionKind;
	}

	public void setCompetitionKind(CompetitionKind competitionKind) {
		this.competitionKind = competitionKind;
	}

	public Boolean getOficjalna() {
		return oficjalna;
	}

	public void setOficjalna(Boolean oficjalna) {
		this.oficjalna = oficjalna;
	}

	public Double getMarzaOd() {
		return marzaOd;
	}

	public void setMarzaOd(Double marzaOd) {
		this.marzaOd = marzaOd;
	}

	public Double getMarzaDo() {
		return marzaDo;
	}

	public void setMarzaDo(Double marzaDo) {
		this.marzaDo = marzaDo;
	}

	public Double getProwizjaSalon() {
		return prowizjaSalon;
	}

	public void setProwizjaSalon(Double prowizjaSalon) {
		this.prowizjaSalon = prowizjaSalon;
	}

	public Double getProwizjaHandlowiec() {
		return prowizjaHandlowiec;
	}

	public void setProwizjaHandlowiec(Double prowizjaHandlowiec) {
		this.prowizjaHandlowiec = prowizjaHandlowiec;
	}

	public Double getNagrodaHandlowiec() {
		return nagrodaHandlowiec;
	}

	public void setNagrodaHandlowiec(Double nagrodaHandlowiec) {
		this.nagrodaHandlowiec = nagrodaHandlowiec;
	}

	public Double getWysokoscSprzedazy() {
		return wysokoscSprzedazy;
	}

	public void setWysokoscSprzedazy(Double wysokoscSprzedazy) {
		this.wysokoscSprzedazy = wysokoscSprzedazy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getOkres() {
		return okres;
	}

	public void setOkres(Integer okres) {
		this.okres = okres;
	}
	
	public static Map<Integer, String> getOkresy() {
		return okresy;
	}

	public static void setOkresy(Map<Integer, String> okresy) {
		Competition.okresy = okresy;
	}
	
	public Contractor getContractor() {
		return contractor;
	}

	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}

	public Date getDataDodania() {
		return dataDodania;
	}

	public void setDataDodania(Date dataDodania) {
		this.dataDodania = dataDodania;
	}	
}
