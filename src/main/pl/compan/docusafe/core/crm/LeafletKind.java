package pl.compan.docusafe.core.crm;

import java.util.List;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

/**
 * Rodzaje materia��w marketingowych crm
 * @author Mariusz Kilja�czyk
 *
 */

public class LeafletKind
{
    private static final Logger log = LoggerFactory.getLogger(LeafletKind.class);
    
    private Long   id;
    private String name;
    private String cn;
    
    
    StringManager sm = 
        GlobalPreferences.loadPropertiesFile(LeafletKind.class.getPackage().getName(),null);
    
    public LeafletKind()
    {
    }

    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania LeafletKind  "+e.getMessage());
            throw new EdmException("Blad dodania LeafletKind "+e.getMessage());
        }
    }

    public static LeafletKind find(Long id) throws EdmException
    {
        StringManager smL = GlobalPreferences.loadPropertiesFile(LeafletKind.class.getPackage().getName(),null);
        LeafletKind inst = DSApi.getObject(LeafletKind.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono LeafletKind "+id);
        else 
            return inst;
    }
    
    public static LeafletKind findByName(String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(LeafletKind.class);

            c.add(Expression.like("name",name).ignoreCase());

            List<LeafletKind> clist = (List<LeafletKind>) c.list();
            if(clist.size() > 0)
            	return clist.get(0);
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania materia�u o nazwie "+ name +" " + e);
		}
        return null;
    }

	public static LeafletKind findByCn(String cn) throws EdmException 
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(LeafletKind.class);

            c.add(Expression.like("cn",cn).ignoreCase());

            List<LeafletKind> clist = (List<LeafletKind>) c.list();
            if(clist.size() > 0)
            	return clist.get(0);
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania materia�u o nazwie "+ cn +" " + e);
			throw new EdmException("B��d wyszukania materia�u reklamowego o cn = "+ cn);
		}
	}
    
    public static List<LeafletKind> leafletKindlist() throws EdmException
    {

        Criteria c = DSApi.context().session().createCriteria(LeafletKind.class);

        return (List<LeafletKind>) c.list();
    }

    

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String nazwa)
    {
        this.name = nazwa;
    }

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

}