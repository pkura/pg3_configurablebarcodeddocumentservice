package pl.compan.docusafe.core.crm;

import java.util.List;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

/**
 * Marka kontrahenta crm
 * @author Mariusz Kilja�czyk
 *
 */

public class Machine
{
    private static final Logger log = LoggerFactory.getLogger(Machine.class);
    
    private Long   id;
    private String name;
    private String cn;
    
    
    StringManager sm = 
        GlobalPreferences.loadPropertiesFile(Machine.class.getPackage().getName(),null);
    
    public Machine()
    {
    }

     
    
    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania regionu "+e.getMessage());
            throw new EdmException("Blad dodania regionu"+e.getMessage());
        }
    }

    public static Machine find(Long id) throws EdmException
    {
        StringManager smL = GlobalPreferences.loadPropertiesFile(Machine.class.getPackage().getName(),null);
        Machine inst = DSApi.getObject(Machine.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono marki "+id);
        else 
            return inst;
    }
    
    public static List<Machine> findName(String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Machine.class);

            c.add(Expression.like("name",name).ignoreCase());

            return (List<Machine>) c.list();
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania marki o nazwie "+ name +" " + e);
		}
        return null;
    }
    
    public static Machine findByName(String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Machine.class);

            c.add(Expression.like("name",name).ignoreCase());

            List<Machine> clist = (List<Machine>) c.list();
            if(clist.size() > 0)
            	return clist.get(0);
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.error("B��d wyszukania marki o nazwie "+ name +" " + e);
		}
        return null;
    }
    
    public static List<Machine> list()
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Machine.class);
            c.addOrder(Order.asc("name"));
            return (List<Machine>) c.list();
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania marki o nazwie" + e);
		}
        return null;
    }

    

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String nazwa)
    {
        this.name = nazwa;
    }

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}
}