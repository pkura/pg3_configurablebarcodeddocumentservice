package pl.compan.docusafe.core.crm;

import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DockindQuery.JoinInfo;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.reports.tools.SimpleSQLHelper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa reprezentujaca kontakt w CRM
 *
 * @author Mariusz Kilja�czyk
 *
 */
public class Contact extends AbstractDocumentKindDictionary
{
	private static Logger log = LoggerFactory.getLogger(Contact.class);
	private Long id;
	private ContactKind rodzajkontaktu;
	private Date dataKontaktu;
	private String opis;
	private Integer osobaKontaktowa;
	private Set<LeafletKind> leaflet;
	private Date dataNastepnegoKontaktu;
	private String celKontaktu;
	private String createUser;
	private ContactStatus status;
	private ContactKind rodzajNastepnegoKontaktu;
	
	/**
	 * Sprawdza czy slownik nadpisuje utworzenie zaptania dla tego pola
	 */
    public boolean isOverloadQuery(String property)
    {
    	return true;
    }
    
    private static String TABLE = "DSC_Contact";
    /**
     * Dodaje warunek do zapytania 
     */
    public void addOverloadQuery(String property, String value, DockindQuery query)
    {
    	if("dataKontaktu".equals(property))
    	{
    		try
    		{
    			SimpleSQLHelper helper = new SimpleSQLHelper();
    			java.sql.Date dateFrom = new java.sql.Date(DateUtils.parseDateAnyFormat(value).getTime());
    			java.sql.Date dateTo = helper.addDayDate(dateFrom, 1);
    			query.addGe(TABLE + query.ATTRIBUTE_SEPARATOR + "dataKontaktu", dateFrom);
    			query.addLe(TABLE + query.ATTRIBUTE_SEPARATOR + "dataKontaktu", dateTo);
    		}
    		catch (ParseException e) 
    		{
				log.error(e.getMessage(),e);
			}
    	}
    	else if("nazwaRodzajkontaktu".equals(property))
    	{
    		ContactKind ck =ContactKind.findByName(value);
    		if(ck == null)
    			return;
    		query.addEq(TABLE + query.ATTRIBUTE_SEPARATOR + "Rodzajkontaktu", ck.getId());
    	}
    	else if("dataNastepnegoKontaktuAsString".equals(property))
    	{
    		try
    		{
    			SimpleSQLHelper helper = new SimpleSQLHelper();
    			java.sql.Date dateFrom = new java.sql.Date(DateUtils.parseDateAnyFormat(value).getTime());
    			java.sql.Date dateTo = helper.addDayDate(dateFrom, 1);
    			query.addGe(TABLE + query.ATTRIBUTE_SEPARATOR + "dataNastepnegoKontaktu", dateFrom);
    			query.addLe(TABLE + query.ATTRIBUTE_SEPARATOR + "dataNastepnegoKontaktu", dateTo);
    		}
    		catch (ParseException e) 
    		{
				log.error(e.getMessage(),e);
			}
    	}
    }
	
    public void create() throws EdmException
    {
        try 
        {
        	this.createUser = DSApi.context().getPrincipalName();
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static class ContactComparatorById implements Comparator<Contact>
    {
        public int compare(Contact e1, Contact e2)
        {
            return e1.getId().compareTo(e2.getId());
        }
    }
    
    public void delete() throws EdmException 
    {
        try 
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Zwraca stringa zawieraj�cego kod javascript kt�ry wykona si� w funkcji accept_cn
     * 
     */
    public String dictionaryAccept()
    {
        return "accept_contact(map)"; 
    }
    
    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        map.put("dataKontaktu", "Data kontaktu");
        map.put("nazwaRodzajkontaktu", "Rodzaj kontaktu");
        map.put("dataNastepnegoKontaktuAsString", "Data nast�pnego kontaktu");
        return map;
    }
    
	public String getDataNastepnegoKontaktuAsString()
	{
		return (dataNastepnegoKontaktu != null ? DateUtils.formatJsDateTime(dataNastepnegoKontaktu):"");
	}
    
    public String dictionaryAction()
    {
        return "/office/common/crmContactDictionary.action";
    }
    
    public String getDictionaryDescription()
    {
        return (dataKontaktu != null ? DateUtils.formatJsDate(dataKontaktu):"") +" "+ (rodzajkontaktu != null ? rodzajkontaktu.getName() : " - ");
    }

    public Contact find(Long id) throws EdmException 
    {
        Contact worker = (Contact) DSApi.getObject(Contact.class,id);

        if (worker == null) 
        {
            throw new EntityNotFoundException(Contact.class, id);
        }

        return worker;
    }
    
    public static Contact findById(Long id) throws EdmException 
    {
        Contact worker = (Contact) DSApi.getObject(Contact.class,id);

        if (worker == null) 
        {
            throw new EntityNotFoundException(Contact.class, id);
        }

        return worker;
    }
    

	public ContactKind getRodzajkontaktu() {
		return rodzajkontaktu;
	}

	public void setRodzajkontaktu(ContactKind rodzajkontaktu) {
		this.rodzajkontaktu = rodzajkontaktu;
	}

	public Date getDataKontaktu() {
		return dataKontaktu;
	}

	public void setDataKontaktu(Date dataKontaktu) {
		this.dataKontaktu = dataKontaktu;
	}

	public Integer getOsobaKontaktowa() {
		return osobaKontaktowa;
	}

	public void setOsobaKontaktowa(Integer osobaKontaktowa) {
		this.osobaKontaktowa = osobaKontaktowa;
	}

	public Set<LeafletKind> getLeaflet() {
		return leaflet;
	}

	public void setLeaflet(Set<LeafletKind> leaflet) {
		this.leaflet = leaflet;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setDataNastepnegoKontaktu(Date dataNastepnegoKontaktu) {
		this.dataNastepnegoKontaktu = dataNastepnegoKontaktu;
	}

	public Date getDataNastepnegoKontaktu() {
		return dataNastepnegoKontaktu;
	}

	public void setCelKontaktu(String celKontaktu) {
		this.celKontaktu = celKontaktu;
	}

	public String getCelKontaktu() {
		return celKontaktu;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateUser() {
		return createUser;
	}

	public String getNazwaRodzajkontaktu()
	{
		if(rodzajkontaktu != null)
			return rodzajkontaktu.getName();
		else
			return "brak";
	}
	
	public String getNazwaStatusukontaktu()
	{
		if(status != null)
			return status.getName();
		else
			return "brak";
	}

	public boolean isAjaxAvailable() {
		return false;
	}
	
	public String getBoxDescription() 
	{		
		return 
		"<br><div><b>Opis:&nbsp;</b>"+(opis != null ? opis : "")+ "</div>" +
		"<div><b>Rodzaj kontaktu:&nbsp;</b>"+(rodzajkontaktu != null ? rodzajkontaktu.getName() : "")+ "</div>" +
		"<br><div><b>Data kontaktu:&nbsp;</b>"+(dataKontaktu != null ? dataKontaktu : "")+ "</div>" +
		"<br><div><b>Data nast�pnego kontaktu:&nbsp;</b>"+(dataNastepnegoKontaktu != null ? dataNastepnegoKontaktu : "")+ "</div>" +
		"<br><div><b>Cel kontaktu:&nbsp;</b>"+(celKontaktu != null ? celKontaktu : "") + "</div>"+
		"<br><div><b>Status kontaktu:&nbsp;</b>"+(status != null ? status.getName() : "") + "</div>";
	}

	public boolean isBoxDescriptionAvailable() 
	{
		return true;
	}

	public ContactStatus getStatus() {
		return status;
	}

	public void setStatus(ContactStatus status) {
		this.status = status;
	}

	public ContactKind getRodzajNastepnegoKontaktu() {
		return rodzajNastepnegoKontaktu;
	}

	public void setRodzajNastepnegoKontaktu(ContactKind rodzajNastepnegoKontaktu) {
		this.rodzajNastepnegoKontaktu = rodzajNastepnegoKontaktu;
	}

	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		
		return null;
	}
}
