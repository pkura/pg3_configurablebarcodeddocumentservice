package pl.compan.docusafe.core.crm;

import java.util.List;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

/**
 * Status kontaktu crm
 * @author Mariusz Kilja�czyk
 *
 */

public class ContactStatus
{
    private static final Logger log = LoggerFactory.getLogger(ContactStatus.class);
    
    private Long   id;
    private String name;
    private String cn;
    
    
    StringManager sm = 
        GlobalPreferences.loadPropertiesFile(ContactStatus.class.getPackage().getName(),null);
    
    public ContactStatus()
    {
    }
    
    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania ContactStatus "+e.getMessage());
            throw new EdmException("Blad dodania ContactStatus"+e.getMessage());
        }
    }

    public static ContactStatus find(Long id) throws EdmException
    {
        StringManager smL = GlobalPreferences.loadPropertiesFile(ContactStatus.class.getPackage().getName(),null);
        ContactStatus inst = DSApi.getObject(ContactStatus.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono ContactStatus "+id);
        else 
            return inst;
    }
    
    public static ContactStatus findByName(String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(ContactStatus.class);
            c.add(Restrictions.like("name",name).ignoreCase());
            List<ContactStatus> clist = (List<ContactStatus>) c.list();
            if(clist.size() > 0)
            	return clist.get(0);
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania ContactStatus o nazwie "+ name +" " + e);
		}
        return null;
    }
    
	public static ContactStatus findByCn(String cn)
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(ContactStatus.class);

            c.add(Expression.like("cn",cn).ignoreCase());

            List<ContactStatus> clist = (List<ContactStatus>) c.list();
            if(clist.size() > 0)
            	return clist.get(0);
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania ContactStatus o cn "+ cn +" " + e);
		}
        return null;
	}
    
    public static List<ContactStatus> statusKindList() throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(ContactStatus.class);
        return (List<ContactStatus>) c.list();

    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String nazwa)
    {
        this.name = nazwa;
    }

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}
}