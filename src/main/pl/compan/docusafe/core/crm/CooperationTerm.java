package pl.compan.docusafe.core.crm;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import org.apache.axis.components.logger.LogFactory;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;


/**
 * Klasa reprezentujaca oferte handlowa w CRM
 *
 * @author Mariusz Kilja�czyk
 *
 */
public class CooperationTerm 
{
	private Long id;
	private Double marzaDo;
	private Double marzaOd;
	private Double prowizjaSalonOd;
	private Double prowizjaSalonDo;
	private Double prowizjaHandlowiec;
	private Double nagrodaHandlowiec;
	private String opis;
	private Date dataDodania;
	private Contractor contractor;

	

	public void save() throws EdmException
    {
		LogFactory.getLog("kontrahent_log").debug("U�ytkownik "+DSApi.context().getPrincipalName()+
        		" ZAPISA� 'Warunmi wsp�pracy'. Dane : /n" +this.toString());
    }
	
    public void create() throws EdmException
    {
        try 
        {
        	boolean isNew = this.id == null;
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
            LogFactory.getLog("kontrahent_log").debug("U�ytkownik "+DSApi.context().getPrincipalName()+
            		(isNew? " DODA� " : " ZAPISA� ")+" 'Warunmi wsp�pracy'. Dane : /n" +this.toString());
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public String toString()
    {
    	return "id = "+id+"\n"+
    	"marzaDo = "+marzaDo+"\n"+
    	"marzaOd = "+marzaOd+"\n"+
    	"prowizjaSalonOd = "+prowizjaSalonOd+"\n"+
    	"prowizjaSalonDo = "+prowizjaSalonDo+"\n"+
    	"prowizjaHandlowiec = "+prowizjaHandlowiec+"\n"+
    	"nagrodaHandlowiec = "+nagrodaHandlowiec+"\n"+
    	"opis = "+opis+"\n"+
    	"contractor = "+(contractor != null ? contractor.getNip():"")+"\n"+
    	"dataDodania = "+dataDodania+"\n";
    }

    public void delete() throws EdmException 
    {
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }

    public static CooperationTerm find(Long id) throws EdmException 
    {
        CooperationTerm worker = (CooperationTerm) DSApi.getObject(CooperationTerm.class,id);

        if (worker == null) 
        {
            throw new EntityNotFoundException(CooperationTerm.class, id);
        }

        return worker;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getMarzaDo() {
		return marzaDo;
	}

	public void setMarzaDo(Double marzaDo) {
		this.marzaDo = marzaDo;
	}

	public Double getMarzaOd() {
		return marzaOd;
	}

	public void setMarzaOd(Double marzaOd) {
		this.marzaOd = marzaOd;
	}

	public Double getProwizjaSalonOd() {
		return prowizjaSalonOd;
	}

	public void setProwizjaSalonOd(Double prowizjaSalonOd) {
		this.prowizjaSalonOd = prowizjaSalonOd;
	}

	public Double getProwizjaSalonDo() {
		return prowizjaSalonDo;
	}

	public void setProwizjaSalonDo(Double prowizjaSalonDo) {
		this.prowizjaSalonDo = prowizjaSalonDo;
	}

	public Double getProwizjaHandlowiec() {
		return prowizjaHandlowiec;
	}

	public void setProwizjaHandlowiec(Double prowizjaHandlowiec) {
		this.prowizjaHandlowiec = prowizjaHandlowiec;
	}

	public Double getNagrodaHandlowiec() {
		return nagrodaHandlowiec;
	}

	public void setNagrodaHandlowiec(Double nagrodaHandlowiec) {
		this.nagrodaHandlowiec = nagrodaHandlowiec;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Date getDataDodania() {
		return dataDodania;
	}

	public void setDataDodania(Date dataDodania) {
		this.dataDodania = dataDodania;
	}

	public Contractor getContractor() {
		return contractor;
	}

	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}
}
