package pl.compan.docusafe.core.crm;

import java.util.List;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

/**
 * Role kontrahenta crm
 * @author Mariusz Kilja�czyk
 *
 */

public class Role
{
    private static final Logger log = LoggerFactory.getLogger(Role.class);
    
    private Long   id;
    private String name;
    private String cn;
    private Integer leoId;
    
    
    StringManager sm = 
        GlobalPreferences.loadPropertiesFile(Role.class.getPackage().getName(),null);
    
    public Role()
    {
    }

    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania regionu "+e.getMessage());
            throw new EdmException("Blad dodania regionu"+e.getMessage());
        }
    }

    public static Role find(Long id) throws EdmException
    {
        Role inst = DSApi.getObject(Role.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono roli "+id);
        else 
            return inst;
    }
    
    public static List<Role> findName(String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Role.class);

            c.add(Expression.like("name",name).ignoreCase());

            return (List<Role>) c.list();
        }
        catch (Exception e) 
		{
			log.error("B��d wyszukania roli o nazwie "+ name +" " + e);
		}
        return null;
    }
    
    public static Role findByLeoId(Integer leoId)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Role.class);

            c.add(Restrictions.eq("leoId", leoId));

            List<Role> clist = (List<Role>) c.list();
            if(clist.size() > 0)
            {
            	return clist.get(0);
            }
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.error("B��d wyszukania regionu o leoid "+ leoId +" " + e);
		}
        return null;
    }
    
    public static Role findByName(String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Role.class);

            c.add(Expression.eq("name",name).ignoreCase());

            List<Role> clist = (List<Role>) c.list();
            if(clist.size() > 0)
            {
            	return clist.get(0);
            }
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania regionu o nazwie "+ name +" " + e);
		}
        return null;
    }
    
    public static List<Role> roleList()
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Role.class);  

            return (List<Role>) c.list();
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania regionu o nazwie" + e);
		}
        return null;
    }

    

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String nazwa)
    {
        this.name = nazwa;
    }

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}



	public void setLeoId(Integer leoId) {
		this.leoId = leoId;
	}



	public Integer getLeoId() {
		return leoId;
	}
}