package pl.compan.docusafe.core.crm;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.StringManager;

/**
 * Region kontrahenta crm
 * @author Mariusz Kilja�czyk
 *
 */

public class Region
{

	private static StringManager sm = GlobalPreferences.loadPropertiesFile(Region.class.getPackage().getName(), null);
    private static final Log log = LogFactory.getLog(Region.class);

	private Long id;
	private String name;
	private String cn;
	private Integer leoId;

    
    public Region()
    {
    }     
    
    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
        	String guid  = "DV_"+leoId.toString();
			try
			{
				log.error("Szuka dzia�u "+guid);
				DSDivision.find(guid);
			}
			catch (DivisionNotFoundException e) 
			{
				log.error("Nie nzlazl dodaje dzial "+guid);
				DSDivision div  = DSDivision.find("DV_ROOT");
				log.error("DODAJE DZIAL GUID = "+guid);
				div.createGroup(name,guid);
				
			}
			System.out.println("SAVE");
			DSApi.context().session().save(this);
			System.out.println("I JUZ ");
			DSApi.context().session().flush();
			
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania regionu "+e.getMessage());
            throw new EdmException("Blad dodania regionu"+e.getMessage());
        }
    }

    public static Region find(Long id) throws EdmException
    {
        Region inst = DSApi.getObject(Region.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono regionu "+id);
        else 
            return inst;
    }
    
    public static List<Region> findName(String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Region.class);

            c.add(Restrictions.like("name",name).ignoreCase());

            return (List<Region>) c.list();
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania regionu o nazwie "+ name +" " + e);
		}
        return null;
    }
    
    public static Region findByLEOID(Integer leoId)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Region.class);
            c.add(Restrictions.eq("leoId",leoId));
            List<Region> list = (List<Region>) c.list();
            if(list != null && list.size() > 0)
            	return list.get(0);
        }
        catch (Exception e) 
		{
			log.error("B��d wyszukania regionu o leoId "+ leoId +" " + e);
		}
        return null;
    }
    
    public static Region findByLEOIDandName(Integer leoId,String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Region.class);
            c.add(Restrictions.eq("leoId",leoId));
            c.add(Restrictions.like("name",name).ignoreCase());
            List<Region> list = (List<Region>) c.list();
            if(list != null && list.size() > 0)
            	return list.get(0);
        }
        catch (Exception e) 
		{
			log.error("B��d wyszukania regionu o leoId i name "+ leoId +" "+name+" " + e);
		}
        return null;
    }
    
    public static Region findByName(String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Region.class);
            c.add(Restrictions.like("name",name).ignoreCase());
            List<Region> clist = (List<Region>) c.list();
            if(clist.size() > 0)
            	return clist.get(0);
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania regionu o nazwie "+ name +" " + e);
		}
        return null;
    }
    
    public static List<Region> regionList()
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Region.class).addOrder(Order.asc("name"));
            return (List<Region>) c.list();
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania regionu o nazwie" + e);
		}
        return null;
    }
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String nazwa)
    {
        this.name = nazwa;
    }

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}


	public void setLeoId(Integer leoId)
	{
		this.leoId = leoId;
	}


	public Integer getLeoId()
	{
		return leoId;
	}
}