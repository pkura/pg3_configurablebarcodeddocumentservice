package pl.compan.docusafe.core.crm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.parametrization.ilpol.CrmVindicationLogic;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.util.StringManager;


/**
 * Klasa reprezentujaca osoby w CRM
 *
 * @author Mariusz Kilja�czyk
 *
 */
public class ContractorWorker extends AbstractDocumentKindDictionary{
    private Long id;
    private String title;
    private String firstname;
    private String lastname;
    private String street;
    private String zip;
    private String location;
    private String country;
    private String pesel;
    private String nip;
    private String email;
    private String fax;
    private String remarks;
    private String phoneNumber;
    private String phoneNumber2;
    private Long contractorId;
	private static ContractorWorker instance;
	public static synchronized ContractorWorker getInstance()
	{
		if (instance == null)
			instance = new ContractorWorker();
		return instance;
	}
    public void create() throws EdmException {
        try 
        {
        	if(firstname == null || lastname == null)
        		throw new EdmException("Nie mozna utworzy� pracownika. Brak podstawowych danych");
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException 
    {
        try 
        {
        	if(!DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE))
        		throw new EdmException("Brak uprawnie� do usuwania pracownika");
        	
        	List<Contractor>  contractors = Contractor.findByDefaultWorkerId(id);
        	if(contractors != null && contractors.size() > 0)
        		throw new EdmException("Nie mo�na usun�� pracownika poniewa� jest ustawiony jako osoba domy�lna");
        	
        	 DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.CRM_VINDICATION_KIND);
             DockindQuery dockindQuery = new DockindQuery(0,0);
             dockindQuery.setDocumentKind(kind);
             
             Field f = kind.getFieldByCn(CrmVindicationLogic.NUMER_PRACOWNIKA_FIELD_CN);
             dockindQuery.field(f,this.id);
             SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
             if (!(searchResults == null || searchResults.totalCount()<1))
                 throw new EdmException("Nie mo�na usun�� pracownika ");
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Zwraca stringa zawieraj�cego kod javascript kt�ry wykona si� w funkcji accept_cn
     * 
     */
    public String dictionaryAccept()
    {
        return "accept_contractor_w(map);";
    }
    
    public Map<String,String> dictionaryAttributes()
    {
        StringManager smL = 
            GlobalPreferences.loadPropertiesFile(DlApplicationDictionary.class.getPackage().getName(),null);
        Map<String,String> map = new LinkedHashMap<String,String>();
        map.put("imieNazwisko", smL.getString("Imie")+" "+smL.getString("Nazwisko"));
        map.put("phoneNumber", smL.getString("Telefon"));
        return map;
    }
    
    public String dictionaryAction()
    {
        return "/crm/contractor-worker-tab.action?dictionaryAction=true";
    }
    
    public String getDictionaryDescription()
    {
        return firstname +" "+ lastname;
    }
    
    public static List<ContractorWorker> findByContractorId(Long contractorId) throws EdmException
    {
            Criteria c = DSApi.context().session().createCriteria(ContractorWorker.class);
            c.add(Expression.eq("contractorId", contractorId));
            return c.list();
    }

    /**
     * Zapisuje w bazie danych bie��cy obiekt, je�eli nie istnieje
     * inny o takich samych warto�ciach p�l.
     * @return
     * @throws EdmException
     */
    public boolean createIfNew() throws EdmException {
        try {
            Criteria crit = DSApi.context().session()
                                 .createCriteria(ContractorWorker.class);

            if (title != null) {
                crit.add(Expression.like("title","%"+ title +"%"));
            }

            if (firstname != null) {
                crit.add(Expression.like("firstname","%"+ firstname +"%"));
            }

            if (lastname != null) {
                crit.add(Expression.like("lastname","%"+ lastname +"%"));
            }

            if (street != null) {
                crit.add(Expression.like("street","%"+ street +"%"));
            }

            if (zip != null) {
                crit.add(Expression.eq("zip", zip));
            }

            if (location != null) {
                crit.add(Expression.like("location","%"+ location +"%"));
            }

            if (country != null) {
                crit.add(Expression.eq("country", country));
            }

            if (pesel != null) {
                crit.add(Expression.eq("pesel", pesel));
            }

            if (nip != null) {
                crit.add(Expression.eq("nip", nip));
            }

            if (email != null) {
                crit.add(Expression.eq("email", email));
            }

            if (fax != null) {
                crit.add(Expression.eq("fax", fax));
            }

            if (phoneNumber != null) {
                crit.add(Expression.eq("phoneNumber", phoneNumber));
            }
            
            if (phoneNumber2 != null) {
                crit.add(Expression.eq("phoneNumber2", phoneNumber2));
            }

            if (crit.list().size() == 0) {
                DSApi.context().session().save(this);

                return true;
            }

            return false;
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public ContractorWorker find(Long id) throws EdmException {
        ContractorWorker worker = (ContractorWorker) DSApi.getObject(ContractorWorker.class,
                id);

        if (worker == null) {
            throw new EntityNotFoundException(ContractorWorker.class, id);
        }

        return worker;
    }

    /**
     * Znajduje obiekty ContractorWorker na podstawie formularza wyszukiwania.
     * @param form
     * @return
     * @throws EdmException
     */
    @SuppressWarnings("unchecked")
    public static SearchResults<?extends ContractorWorker> search(
        ContractorWorkerQuery query) throws EdmException {
        try {
            Criteria c = DSApi.context().session()
                              .createCriteria(ContractorWorker.class);

            if (query.title != null) {
                c.add(Expression.like("title ",
                        "%" + query.title + "%").ignoreCase());
            }

            if (query.firstname != null) {
                c.add(Expression.like("firstname",
                        "%" +  query.firstname +
                        "%").ignoreCase());
            }

            if (query.lastname != null) {
                c.add(Expression.like("lastname",
                        "%" +query.lastname +
                        "%").ignoreCase());
            }

            if (query.street != null) {
                c.add(Expression.like("street",
                        "%" +  query.street +
                        "%").ignoreCase());
            }

            if (query.zip != null) {
                c.add(Expression.like("zip",
                        "%" + query.zip + "%").ignoreCase());
            }

            if (query.location != null) {
                c.add(Expression.like("location",
                        "%" + query.location +
                        "%").ignoreCase());
            }

            if (query.pesel != null) {
                c.add(Expression.eq("pesel ", query.pesel));
            }

            if (query.country != null) {
                c.add(Expression.eq("country ", query.country).ignoreCase());
            }

            if (query.pesel != null) {
                c.add(Expression.eq("pesel", query.pesel));
            }

            if (query.nip != null) {
                c.add(Expression.eq("nip", query.nip));
            }

            if (query.email != null) {
                c.add(Expression.eq("email", query.email).ignoreCase());
            }

            if (query.fax != null) {
                c.add(Expression.eq("fax", query.fax));
            }

            if (query.remarks != null) {
                c.add(Expression.like("remarks", "%" +query.remarks + "%").ignoreCase());
            }

            if (query.phoneNumber != null) {
                c.add(Expression.eq("phoneNumber", query.phoneNumber));
            }
            
            if (query.phoneNumber2 != null) {
                c.add(Expression.eq("phoneNumber2", query.phoneNumber2));
            }

            if (query.contractorId != null) {
                c.add(Expression.eq("contractorId", query.contractorId));
            }

            for (Iterator iter = query.getSortFields().iterator();
                    iter.hasNext();) {
                ContractorWorkerQuery.SortField field = (ContractorWorkerQuery.SortField) iter.next();

                if (field.isAscending()) {
                    c.addOrder(Order.asc(field.getName()));
                } else {
                    c.addOrder(Order.desc(field.getName()));
                }
            }

            List<ContractorWorker> list = (List<ContractorWorker>) c.list();

            if (list.size() == 0) {
                return SearchResultsAdapter.emptyResults(ContractorWorker.class);
            } else {
                int toIndex;
                int fromIndex = (query.getOffset() < list.size())
                    ? query.getOffset() : (list.size() - 1);

                if (query.getLimit() == 0) {
                    toIndex = list.size();
                } else {
                    toIndex = ((query.getOffset() + query.getLimit()) <= list.size())
                        ? (query.getOffset() + query.getLimit()) : list.size();
                }

                return new SearchResultsAdapter<ContractorWorker>(list.subList(
                        fromIndex, toIndex), query.getOffset(), list.size(),
                    ContractorWorker.class);
            }
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public String getAddress() {
        return getAddress(street, zip, location);
    }

    public static String getAddress(String street, String zip, String location) {
        StringBuilder address = new StringBuilder();

        if (!StringUtils.isEmpty(street)) {
            address.append(street);
        }

        if (!StringUtils.isEmpty(location)) {
            if (address.length() > 0) {
                address.append(' ');
            }

            address.append(location);
        }

        return address.toString();
    }

    public String toString() {
        return getClass().getName() + "[title=" + title + " firstname=" +
        firstname + " lastname=" + lastname +"conID "+ contractorId;
    }

    public Long getId() {
        return id;
    }

    private void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getContractorId() {
        return contractorId;
    }

    public void setContractorId(Long contarctorId) {
        this.contractorId = contarctorId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public static class ContractorWorkerQuery {
        private Integer limit;
        private Integer offset;
        private String title;
        private String firstname;
        private String lastname;
        private String street;
        private String zip;
        private String location;
        private String country;
        private String pesel;
        private String nip;
        private String email;
        private String fax;
        private String remarks;
        private String phoneNumber;
        private String phoneNumber2;
        private Long contractorId;
        private List<SortField> sortFields = new ArrayList<SortField>();

        public ContractorWorkerQuery() {
        }

        public List<SortField> getSortFields() {
            return sortFields;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public void setSortFields(List<SortField> sortFields) {
            this.sortFields = sortFields;
        }

        public Long getContractorId() {
            return contractorId;
        }

        public void setContractorId(Long contarctorId) {
            this.contractorId = contarctorId;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFax() {
            return fax;
        }

        public void setFax(String fax) {
            this.fax = fax;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getNip() {
            return nip;
        }

        public void setNip(String nip) {
            this.nip = nip;
        }

        public String getPesel() {
            return pesel;
        }

        public void setPesel(String pesel) {
            this.pesel = pesel;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public void addDesc(String name)
        {
            sortFields.add(SortField.desc(name));
        }
        public void addAsc(String name)
        {
            sortFields.add(SortField.asc(name));
        }
        public static class SortField {
            private String name;
            private boolean ascending;

            private SortField(String name, boolean ascending) {
                this.name = name;
                this.ascending = ascending;
            }

            public String getName() {
                return name;
            }

            public boolean isAscending() {
                return ascending;
            }

            private static SortField asc(String name) {
                return new SortField(name, true);
            }

            private static SortField desc(String name) {
                return new SortField(name, false);
            }
        }
        public String getPhoneNumber2()
        {
            return phoneNumber2;
        }

        public void setPhoneNumber2(String phoneNumber2)
        {
            this.phoneNumber2 = phoneNumber2;
        }
    }

    public String getPhoneNumber2()
    {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2)
    {
        this.phoneNumber2 = phoneNumber2;
    }
    
    public String getImieNazwisko()
    {
    	return firstname+" "+lastname;
    }
	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
}
