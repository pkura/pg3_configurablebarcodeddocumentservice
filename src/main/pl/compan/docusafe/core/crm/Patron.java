package pl.compan.docusafe.core.crm;

import java.util.List;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

/**
 * Rodzaj kontaktu crm
 * @author Mariusz Kilja�czyk
 *
 */

public class Patron
{
    private static final Logger log = LoggerFactory.getLogger(Patron.class);
    
    private Long id;
    private Contractor  contractor;
    private String username;
    private String own;
    
    
    StringManager sm = GlobalPreferences.loadPropertiesFile(Patron.class.getPackage().getName(),null);
    
    public Patron()
    {
    }
    
    public void delete() throws EdmException 
    {
        try 
        {
            DSApi.context().session().delete(this);
//            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }
    
    
    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania Opiekuna "+e.getMessage());
            throw new EdmException("Blad dodania Opiekuna"+e.getMessage());
        }
    }
    
    
    public static Patron find(Long id) throws EdmException 
    {
    	Patron worker = (Patron) DSApi.getObject(Patron.class,id);

        if (worker == null) 
        {
            throw new EntityNotFoundException(Patron.class, id);
        }

        return worker;
    }
    
    public static Patron findOrCreate(String username,Contractor contractor)
    {
    	return findOrCreate(username, contractor,"DS");
    }
    
    public static Patron findOrCreate(String username,Contractor contractor,String own)
    {
    	Patron p = null;
    	 try
         {
             Criteria c = DSApi.context().session().createCriteria(Patron.class);
             c.add(Restrictions.eq("contractor",contractor));
             c.add(Restrictions.eq("username",username).ignoreCase());
             c.add(Restrictions.eq("own",own).ignoreCase());
             List<Patron> list = c.list();
             if(list == null || list.size() < 1 )
             {
            	 p = new Patron();
            	 p.setContractor(contractor);
            	 p.setUsername(username);
            	 p.setOwn(own);
            	 p.create();
             }
             else
            	 p = list.get(0);
         }
         catch (Exception e) 
 		{
 			log.error("B��d wyszukania Opiekuna dla kontrahenta "+ contractor +" " + e);
 		}
    	 log.trace(""+p);
         return p;
    }
    
    public static List<Patron> findByName(Long contractorId)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Patron.class);
            c.add(Restrictions.eq("contractorId",contractorId));
            List<Patron> clist = (List<Patron>) c.list();
            return clist;
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania Opiekuna dla kontrahenta "+ contractorId +" " + e);
		}
        return null;
    }


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOwn() {
		return own;
	}

	public void setOwn(String own) {
		this.own = own;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Contractor getContractor() {
		return contractor;
	}

	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}
    

   
}