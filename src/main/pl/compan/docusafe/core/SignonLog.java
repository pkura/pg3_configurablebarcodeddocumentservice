package pl.compan.docusafe.core;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/* User: Administrator, Date: 2005-11-18 15:29:48 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SignonLog.java,v 1.3 2009/09/24 11:17:57 pecet1 Exp $
 */
public class SignonLog
{
    private static final Log log = LogFactory.getLog("signon_log");
	private final static Logger accessLog = LoggerFactory.getLogger("access_log");

    public static void logSignon(String username)
    {
        if (log.isInfoEnabled())
        {
            log.info("%SIGNON% {\"username\":\""+username+"\"}");
        }
		accessLog.info("%LOGIN% {\"username\":\""+username+"\"}");
    }

    public static void logEmergencySignon(String username)
    {
        if (log.isInfoEnabled())
        {
            log.info("%SIGNON% {\"username\":\""+username+"\", \"emergency\": \"true\"}");
        }
		accessLog.info("%LOGIN% {\"username\":\""+username+"\"}");
    }

    public static void logFailedSignon(String username)
    {
        logFailedSignon(username, null);
    }

    public static void logFailedSignon(String username, String remark)
    {
        if (log.isInfoEnabled())
        {
            log.info("%FAILEDSIGNON% {\"username\":\""+username+"\""+
                (remark != null ? ",\"remark\":\""+remark+"\"" : "") + "}");
        }
    }

    public static void logSignoff(String username)
    {
        if (log.isInfoEnabled())
        {
            log.info("%SIGNOFF% {\"username\":\""+username+"\"}");
        }
		accessLog.info("%LOGOUT% {\"username\":\""+username+"\"}");
    }
}
