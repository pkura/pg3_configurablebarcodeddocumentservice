package pl.compan.docusafe.core.cfg.part;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import pl.compan.docusafe.web.part.TagSurroundingView;
import pl.compan.docusafe.web.part.PartView;
import org.dom4j.Element;
import pl.compan.docusafe.web.part.CompositePartView;
import pl.compan.docusafe.web.part.ContentPartView;
import pl.compan.docusafe.web.part.DefaultPartView;
import pl.compan.docusafe.web.part.Labels;

/**
 * Klasa s�u��ca wczytywaniu/zapisywaniu PartView, zmiana nazwy spowoduje konieczno�� poprawiania wszystkich XML'�w
 * nazwa enuma = typ.toUpperCase
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
enum PartViewIO {
	DEFAULT{
		public PartView fromViewElement(Element el){
			return DefaultPartView.INSTANCE;
		}
	},
	COMPOSITE{
		public PartView fromViewElement(Element el){
			return new CompositePartView(fromXmlElements(el.elements("view")));
		}
	},
	RAW{
		public PartView fromViewElement(Element el){
			return new ContentPartView(Labels.unescapedLabel(el.getTextTrim()));
		}
	},
	DIV{
		public PartView fromViewElement(Element el){
			return TagSurroundingView.DIV;
		}
	},
	NULL{
		public PartView fromViewElement(Element el){
			return PartView.NULL;
		}
	};

	abstract PartView fromViewElement(Element el);


	public static Collection<PartView> fromXmlElements(List<Element> els){
		ArrayList<PartView> ret = new ArrayList<PartView>(els.size());
		for(Element el:els){
			ret.add(fromXmlElement(el));
		}
		return ret;
	}

	public static PartView fromXmlElement(Element el){
		String type = el.attributeValue("type");
		try {
			return PartViewIO.valueOf(type.toUpperCase()).fromViewElement(el);
		} catch (IllegalArgumentException e) {
			DSPartsConfig.LOG.error(e.getMessage(), e);
			DSPartsConfig.LOG.error("using default");
			return DefaultPartView.INSTANCE;
		}
	}
}
