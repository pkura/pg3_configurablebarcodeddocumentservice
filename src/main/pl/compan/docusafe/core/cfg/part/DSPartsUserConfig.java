package pl.compan.docusafe.core.cfg.part;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DSPartsUserConfig extends DSPartsConfig{

	public DSPartsUserConfig(DSPartsConfig config) {
		super(config.provideManager(null), config.getConfig());
	}
}
