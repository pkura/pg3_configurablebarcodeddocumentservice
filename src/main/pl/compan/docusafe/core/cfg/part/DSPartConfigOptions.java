package pl.compan.docusafe.core.cfg.part;

import pl.compan.docusafe.web.part.PartView;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

/**
 * Dost�pne opcje konfiguracyjne jednego part'a
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DSPartConfigOptions {

	private PartView defaultView = null;

	private Map<String, PartView> partViewOptions = new HashMap<String, PartView>();

	public PartView getDefaultView(){
		return defaultView;
	}

	void setDefaultView(PartView view){
		defaultView = view;
	}

	void addOption(String name, PartView view){
		if(defaultView == null) {
			defaultView = view;
		}
		if(!StringUtils.isBlank(name)){
			partViewOptions.put(name, view);
		} else {
			throw new IllegalArgumentException("name cannot be blank");
		}
	}
}
