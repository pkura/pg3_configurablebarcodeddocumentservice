package pl.compan.docusafe.core.cfg.part;

/**
 * Obiekt udost�pniaj�cy wymagane obiekty dla PartView
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface RenderingContext {

	/**
	 * Identyfikator generowanego parta, na nim powinny opiera� si� generowane id, name etc.
	 * @return
	 */
	String getPartName();

	/**
	 * Zawarto�� danego ds:parta
	 * @return
	 */
	Appendable getContents();
}
