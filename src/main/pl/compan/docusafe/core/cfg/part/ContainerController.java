package pl.compan.docusafe.core.cfg.part;

/**
 * 
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface ContainerController{

	Iterable<String> provideOrder(Iterable<String> partNames);

}
