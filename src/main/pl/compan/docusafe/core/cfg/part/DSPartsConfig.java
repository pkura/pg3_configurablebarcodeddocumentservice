package pl.compan.docusafe.core.cfg.part;

import pl.compan.docusafe.web.part.DefaultPartView;
import pl.compan.docusafe.web.part.PartView;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Maps;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.cfg.ActionContext;
import pl.compan.docusafe.core.cfg.ActionContext.Action;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DSPartsConfig {

    static final Logger LOG = LoggerFactory.getLogger(DSPartsConfig.class);
	private static final String CONFIGURABLE = "configurable";

	public static final DSPartsConfig DEFAULT = parseDefault();

	

	private final DSPartsConfigConfig config;

	/**
	 * Zawiera wszystkich manager�w, mapa po wype�nieniu przestaje by� modyfikowalna
	 */
	private Map<Predicate<ActionContext>, DSPartsManager> managers
			= Maps.newLinkedHashMap();
	private Map<ActionContext, DSPartsManager> managersCache
			= new ConcurrentHashMap<ActionContext, DSPartsManager>();

	/**
	 * Na razie tylko jeden, do zmiany w przysz�o�ci
	 */
	private DSPartsManager defaultManager;

	private static DSPartsConfig parseDefault(){
		DSPartsConfig ret = null;
		
		try {
			Document document
				= new SAXReader()
					.read(Docusafe.getServletContext()
						.getResource("/WEB-INF/classes/docusafe-parts-default.xml"));
			ret = parse(document);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} 

		return ret;
	}

	private static DSPartsConfig parse(Document xml){
		DSPartsConfig ret = new DSPartsConfig();
		for(Element element: (List<Element>)xml.getRootElement().elements("part-config")){
			parseConfig(element, ret);
		}
		ret.managers = Collections.unmodifiableMap(ret.managers);
		return ret;
	}

	private static void parseConfig(Element config, DSPartsConfig ret) {
		//Na razie parsuje tylko jedn� konfiguracj� - do uzupe�nienia

		Map<String, PartView> views = new HashMap<String, PartView>();
		Map<String, String> prefixes = new HashMap<String, String>();
		
		ContainerStructure structure = new ContainerStructure();
		
		String configName = config.attributeValue("name");

		Predicate<ActionContext> predicate = parsePredicate(config.element("predicate"));

		PartView defaultView = DefaultPartView.INSTANCE;
		if(config.element("default") != null){
			DSPartConfigOptions defaultOption = parsePart(config.element("default"), prefixes);
			defaultView = defaultOption.getDefaultView();
		}
		for (Element containerElement : (List<Element>) config.elements("container")) {
			ContainerStructure.Container container
					= new ContainerStructure.Container(containerElement.attributeValue("name"));
			structure.addContainer(container);

			for (Element element : (List<Element>) containerElement.elements()) {
				if (element.getName().equals("group")) {
					ContainerStructure.Group group = new ContainerStructure.Group(element.attributeValue("name"));
					container.addGroup(group);

					for (Element partElement : (List<Element>) element.elements("part")) {
						String partName = partElement.attributeValue("name");
						group.addPart(partName);
						DSPartConfigOptions configOptions = parsePart(partElement, prefixes);
						views.put(partName, configOptions.getDefaultView());
						LOG.trace("partName = " + partName + "  defaultView = " + configOptions.getDefaultView());
					}
				} else if(element.getName().equals("part")) {
					String partName = element.attributeValue("name");
					ContainerStructure.Group group
						= new ContainerStructure.Group(partName);
					group.addPart(partName);
					DSPartConfigOptions configOptions = parsePart(element, prefixes);
					views.put(partName, configOptions.getDefaultView());
				}
			}
		}
		DSPartsManager manager = new DSPartsManager(configName, views, prefixes, defaultView, structure);
		if(configName.equals("default")){
			ret.defaultManager = manager;
		}
		ret.managers.put(predicate, manager);
	}

	private static Predicate<ActionContext> parsePredicate(Element predicateElement) {
		Predicate<ActionContext> predicate = Predicates.alwaysTrue();
		if(predicateElement != null){
			List<Element> actionsElements = predicateElement.elements("action");
			Set<Action> actions = EnumSet.noneOf(Action.class);
			for(Element action: actionsElements){
				actions.add(Action.valueOf(action.getTextTrim().replace("-", "_").toUpperCase()));
			}
			LOG.trace("predicate - in " + actions);
			predicate = new ActionPredicate(Predicates.in(actions));

			List<Element> documentTypeElements = predicateElement.elements("document-type");
			if (documentTypeElements != null) {
				Set<DocumentType> dtypes = EnumSet.noneOf(DocumentType.class);
				for (Element el : documentTypeElements) {
					dtypes.add(DocumentType.valueOf(el.getTextTrim().replace("-", "_").toUpperCase()));
				}
				predicate = Predicates.and(predicate, new DocumentTypePredicate(Predicates.in(dtypes)));
			}
		}
		return predicate;
	}

	private static class DocumentTypePredicate implements Predicate<ActionContext> {
		private final Predicate<DocumentType> documentTypePredicate;

		public DocumentTypePredicate(Predicate<DocumentType> documentTypePredicate) {
			this.documentTypePredicate = documentTypePredicate;
		}

		public boolean apply(ActionContext arg) {
			return documentTypePredicate.apply(arg.getDocumentType());
		}
	}

	private static class ActionPredicate implements Predicate<ActionContext>{
		private final Predicate<Action> actionPredicate;

		public ActionPredicate(Predicate<Action> actionPredicate) {
			this.actionPredicate = actionPredicate;
		}

		public boolean apply(ActionContext arg) {
			return actionPredicate.apply(arg.getType());
		}
	}

	private static DSPartConfigOptions parsePart(Element element, Map<String, String> prefixes) {
	/***************************************************************
	 * Obs�ug� nowych rodzaj�w parts�w dodawa� do PartViewIO
	 ***************************************************************/

		DSPartConfigOptions options = new DSPartConfigOptions();
		for(Element option: (List<Element>)element.elements("view")){
			String name = option.attributeValue("name");
			String type = option.attributeValue("type");
			String prefix = option.attributeValue("prefix");
			
			PartView view;
			if(StringUtils.isBlank(type)){
				LOG.error("blank option type - using default");
				options.addOption("default", view = DefaultPartView.INSTANCE);
			} else {
				if(StringUtils.isBlank(name)) { name = type; }
				options.addOption(name, view = PartViewIO.fromXmlElement(option));
				LOG.trace("tag name " + name);
			}
			if(isTrue(option.attributeValue("default"))){
				options.setDefaultView(view);
				LOG.trace("default option = " + view.toString());
			}

			if(!StringUtils.isBlank(prefix)){
				prefixes.put(prefix, element.attributeValue("name"));
			}
		}

		return options;
	}

	public Map<String, String> getLabeledConfigurationNames(){
		return config.getLabeledConfigurations();
	}

	public Map<String, String> getLabeledPartNames(String configurationName){
		return config.getLabeledPartNames(configurationName);
	}

	public Set<String> getConfigurableValues(String configurationName,
			String partName){

		return config.getConfigurableValues(configurationName, partName);
	}

	private static boolean isTrue(String string){
		return string != null && Boolean.TRUE.toString().equals(string.trim());
	}

	private DSPartsConfig(){
		config = new DSPartsConfigConfig();
	}

	DSPartsConfig(DSPartsManager manager, DSPartsConfigConfig config){
		this.defaultManager = manager;
		this.config = config;
	}

	/**
	 * Zwraca konfiguracj� dla aktualnego u�ytkownika
	 * @return
	 */
	public static DSPartsConfig get(){
		return DEFAULT;
	}

	public DSPartsManager provideManager(ActionContext context){
		DSPartsManager ret = provideStoredManager(context);
		LOG.info("stored manager for {" + context+ "}: " + ret.getName());
		return ret == null ? defaultManager : ret;
	}

	private DSPartsManager provideStoredManager(ActionContext context){
		DSPartsManager ret = null;
		ret = managersCache.get(context);
		if(ret == null){
			for(Entry<Predicate<ActionContext>, DSPartsManager> entry: managers.entrySet()){
				if(entry.getKey().apply(context)){
					ret = entry.getValue();
					managersCache.put(context, ret);
					break;
				}
			}
		}
		return ret;
	}

	DSPartsConfigConfig getConfig() {
		return config;
	}
}
