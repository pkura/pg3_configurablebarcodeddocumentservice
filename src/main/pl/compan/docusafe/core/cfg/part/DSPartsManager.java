package pl.compan.docusafe.core.cfg.part;

import pl.compan.docusafe.web.part.PartView;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DSPartsManager {

	private static final Logger LOG = LoggerFactory.getLogger(DSPartsManager.class);

	private final String name;

	private final ContainerStructure structure;
	private final Map<String, PartView> partViews;
	private final Map<String, String> prefixes;
	
	private final PartView defaultView;

	/**
	 * Tworzy nowego DSPartsManagera
	 * @param containerController
	 * @param partViews - lista widok�w, je�li DSPartsManager b�dzie poproszony
	 * o widok nieobecny w mapie zwr�ci domy�lny widok DefaultPartView
	 */
	public DSPartsManager(String name, Map<String, PartView> partViews, Map<String, String> prefixes, PartView defaultPartView, ContainerStructure structure) {
		this.name = name;
		this.defaultView = defaultPartView;
		this.partViews = partViews;
		this.structure = structure;
		this.prefixes = prefixes;
	}

	/**
	 * Zwraca PartView dla danej nazwy,
	 * @param partName
	 * @return nigdy nie zwraca null
	 */
	public PartView getPartView(String partName){
		PartView ret = partViews.get(partName);
		if(ret == null) {
			LOG.warn("brak zapisanego ds-part {}, posiadane ds-parts{}", partName, partViews.keySet());
			ret = defaultView;
		}
		return ret;
	}

	/**
	 * @return the structure
	 */
	public ContainerStructure getStructure() {
		return structure;
	}

	public String getName(){return name;}

	public Map<String, String> getPrefixes() {
		return prefixes;
	}
}
