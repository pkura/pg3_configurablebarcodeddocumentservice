package pl.compan.docusafe.core.cfg.part;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
class DSPartsConfigConfig {

	//<config-name, <part-name, configurable-values/atributes>>>
	private final Map<String, Map<String, Set<String>>> configurableValues
			= new HashMap<String, Map<String, Set<String>>>();

	private Map<String, String> labeledConfigurations = null;
	private Map<String, Map<String, String>> labeledPartNames = null;

	Set<String> configuration(String configName, String partName){
		if(!configurableValues.containsKey(configName)){
			labeledConfigurations = null;
			configurableValues.put(configName, new HashMap<String, Set<String>>());
		}
		if(!configurableValues.get(configName).containsKey(partName)){
			configurableValues.get(configName).put(partName, new HashSet<String>());
		}
		return configurableValues.get(configName).get(partName);
	}

	Map<String, String> getLabeledConfigurations(){
		if(labeledConfigurations == null){
			labeledConfigurations = new LinkedHashMap<String, String>();
			for(String name:configurableValues.keySet()){
				labeledConfigurations.put(name, name);
			}
		}
		return labeledConfigurations;
	}

	Map<String, String> getLabeledPartNames(String configurationName) {
		if (labeledPartNames == null) {
			labeledPartNames = new LinkedHashMap<String, Map<String, String>>();
			for (String configName : configurableValues.keySet()) {
				labeledPartNames.put(configName, new LinkedHashMap<String, String>());
				for (String partName : configurableValues.get(configName).keySet()) {
					labeledPartNames.get(configName).put(partName, partName);
				}
			}
		}
		return labeledPartNames.get(configurationName);
	}

	Set<String> getConfigurableValues(String configurationName, String partName) {
		return configurableValues.get(configurationName).get(partName);
	}

}
