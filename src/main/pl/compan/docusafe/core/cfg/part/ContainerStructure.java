package pl.compan.docusafe.core.cfg.part;

import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ContainerStructure {
	@Expose List<Container> containers = new ArrayList<Container>();

	public void addContainer(Container container){
		containers.add(container);
	}

	public final static class Container {
		@Expose String name = "";
		@Expose List<Group> groups = new ArrayList<Group>();

		public Container(String name){
			this.name = name;
		}

		public void addGroup(Group group){
			groups.add(group);
		}
	}

	public final static class Group {
		@Expose String name = "";
		@Expose List<String> parts = new ArrayList<String>();

		public Group(String name){
			this.name = name;
		}

		public void addPart(String name){
			parts.add(name);
		}
	}
}
