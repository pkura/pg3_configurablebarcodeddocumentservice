package pl.compan.docusafe.core.cfg.part;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DefaultContainerController implements ContainerController{

	public Iterable<String> provideOrder(Iterable<String> partNames) {
		return partNames;
	}
}
