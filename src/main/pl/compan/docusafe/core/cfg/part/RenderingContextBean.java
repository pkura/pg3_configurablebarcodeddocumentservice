package pl.compan.docusafe.core.cfg.part;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class RenderingContextBean implements RenderingContext{
	private String partName;
	private Appendable contents;

	public String getPartName() {
		return partName;
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public Appendable getContents() {
		return contents;
	}

	public void setContents(Appendable contents) {
		this.contents = contents;
	}
}
