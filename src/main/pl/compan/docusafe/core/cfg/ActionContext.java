package pl.compan.docusafe.core.cfg;

import pl.compan.docusafe.core.base.DocumentType;

/**
 * Kontekst akcji, okre�la do czego dok�adnie odnosi si� konfiguracja
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ActionContext {

	public enum Action {
		NEW_DOCUMENT,
		DOCUMENT_SUMMARY
	}

	private final Action type;
	private DocumentType documentType;

	public static ActionContext action(Action type) {
		return new ActionContext(type);
	}

	public ActionContext type(DocumentType type) {
		this.documentType = type;
		return this;
	}

	private ActionContext(Action type) {
		this.type = type;
	}

	/**
	 * Zwraca rodzaj akcji
	 */
	public Action getType() {
		return type;
	}

	public DocumentType getDocumentType(){
		return documentType;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ActionContext other = (ActionContext) obj;
		if (this.type != other.type) {
			return false;
		}
		if (this.documentType != other.documentType) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 41 * hash + this.type.hashCode();
		hash = 41 * hash + (this.documentType == null ? 0 : this.documentType.hashCode());
		return hash;
	}

	@Override
	public String toString() {
		return "TYPE - " + type.toString() + "; DOCTYPE - " + documentType.toString();
	}
}
