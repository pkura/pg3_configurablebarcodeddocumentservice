package pl.compan.docusafe.core.cfg;

/**
 * Klasa enumeracyjna zawieraj�ca nazwy dost�pnych szablon�w list�w.
 * Ka�de pole typu Mail odpowiada plikowi w katalogu HOME_DIRECTORY/mail.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Mail.java,v 1.13 2010/06/11 17:16:15 mariuszk Exp $
 */
public class Mail
{
    // po dodaniu ka�dego nowego pola nale�y doda� je do tablicy mails
    
    public static final Mail DOCUMENT_UNLOCK_REQUEST = new Mail("document-unlock-request.txt");
    public static final Mail NEW_USER = new Mail("new-user.txt");
    public static final Mail PASSWORD_RECOVERY = new Mail("password-recovery.txt");
    public static final Mail WATCH_ATTACHMENT_DELETED = new Mail("watch-attachment-deleted.txt");
    public static final Mail WATCH_ATTACHMENT_ADDED = new Mail("watch-attachment-added.txt");
    public static final Mail WATCH_ATTACHMENT_REVISION_ADDED = new Mail("watch-attachment-revision-added.txt");
    public static final Mail WATCH_DOCUMENT_MODIFIED = new Mail("watch-document-modified.txt");
    public static final Mail SEND_DOCUMENT_LINK = new Mail("send-document-link.txt");
    public static final Mail TASK_LIST_MAILER = new Mail("mailer.txt");
    public static final Mail SUBSTITUTION_MAIL = new Mail("user-substituted.txt");
    public static final Mail OFFICE_MAILER = new Mail("officeMailer.txt");
    public static final Mail REPORT_TASK_MAILER = new Mail("reportTaskMailer.txt");
    public static final Mail TASK_LIST_MAILER_LABEL = new Mail("mailerLabel.txt");
    public static final Mail NEW_DRK_MAIL = new Mail("new-drk.txt");
    public static final Mail ORDERS_REMINDER = new Mail("orders-reminder.txt");
    public static final Mail BARCODES_NOTIFIER = new Mail("barcodes-notifier.txt");
    public static final Mail CALENDAR_NEW_EVENT_NOTIFIER = new Mail("calendar-notifier.txt");
    public static final Mail CALENDAR_EVENT_UPDATE_NOTIFIER = new Mail("calendar-notifier-update.txt");
    public static final Mail CHANGE_ORDER_STATUS = new Mail("change-order-status.txt");
    public static final Mail NEW_ORDER = new Mail("new-order.txt");
    public static final Mail ORDERS_IM = new Mail("orders-IM.txt");
    public static final Mail ACCEPTANCE_NOTIFIER = new Mail("acceptance-notifier.txt");
    public static final Mail ORDER_DOCUMENT = new Mail("order-document.txt");
    public static final Mail ORDER_BOX = new Mail("order-box.txt");
    public static final Mail TASK_REMINDER = new Mail("task-reminder-mail-template.txt");
    public static final Mail INTERNAL_DEMAND_NOTIFIER = new Mail("internal-demand-notifier.txt");
    public static final Mail INVOICE_MAILER = new Mail("invoice-mailer.txt");
    
    /**
     * Wszystkie warto�ci enumeracji.
     */
    public static final Mail[] mails = new Mail[] {
        DOCUMENT_UNLOCK_REQUEST, NEW_USER, PASSWORD_RECOVERY,
        WATCH_ATTACHMENT_DELETED, WATCH_DOCUMENT_MODIFIED,
        WATCH_ATTACHMENT_ADDED, WATCH_ATTACHMENT_REVISION_ADDED,
        SEND_DOCUMENT_LINK, NEW_DRK_MAIL, TASK_LIST_MAILER, 
        TASK_LIST_MAILER_LABEL,ORDERS_REMINDER,ORDER_DOCUMENT,ORDER_BOX, SUBSTITUTION_MAIL
    };

    private final String name; // for debug only

    private Mail(String name)
    {
        this.name = name;
    }
    
    /**
     * Zwraca instancj� maila z podan� nazw�
     * @param name
     * @return
     */
    public static Mail getInstance(String name)
    {
    	return new Mail(name);
    }
    
    public String getName()
    {
        return name;
    }

    public String toString()
    {
        return getClass().getName()+"[name="+name+"]";
    }
    
    public static Mail findMailByFileName(String fileName)
    {
    	for(Mail ml : mails)
    	{
    		if(ml.name.equalsIgnoreCase(fileName))
    			return ml;
    	}
    	return null;
    }
}
