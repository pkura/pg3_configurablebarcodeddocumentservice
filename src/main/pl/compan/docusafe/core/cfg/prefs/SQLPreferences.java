package pl.compan.docusafe.core.cfg.prefs;

import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;

/**
 * Nowa ga��� jest tworzona przy wywo�aniu konstruktora.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SQLPreferences.java,v 1.20 2009/07/14 14:21:21 mariuszk Exp $
 */
public class SQLPreferences extends AbstractPreferences
{
    private static final Log log = LogFactory.getLog(SQLPreferences.class);

    private String nodeTable;
    private String valueTable;
    private String generator;

    /**
     * Identyfikator tej ga��zi w bazie danych.
     */
    private Integer id;
    /**
     * W�asno�ci zwi�zane z t� ga��zi�.
     */
    private Map<String, String> properties = new HashMap<String, String>();
    /**
     * Flaga ustawiana w konstruktorze, gdy nie uda si� zapisa� ga��zi
     * w bazie danych lub odczyta� z bazy jej identyfikatora.
     */
    private boolean sqlError;

    private boolean flushNeeded;

    private String username;

    /**
     * Domy�lna implementacja wywo�uje {@link SQLPreferences#userRoot()},
     * co mo�e niekorzystnie wp�ywa� na wydajno�� i niepotrzebnie tworzy�
     * g��wn� ga��� Preferences dla zalogowanego u�ytkownika. B��dy mog�
     * powsta� tak�e wtedy, gdy nie ma zalogowanego u�ytkownika.
     * @return true, je�eli s� to preferencje u�ytkownika.
     */
    public boolean isUserNode()
    {
        return username != null;
    }

    /**
     * Tworzenie nowej ga��zi. Zastosowania: <ul>
     * <li> Tworzenie (odczyt) ga��zi g��wnej. Je�eli uda si� odczyta�/utworzy�,
     *   nadana zostaje warto�� atrybutowi id. Je�eli wyst�pi b��d, ustawiona zostanie
     *   flaga sqlError (�adnych wyj�tk�w) i przy flush() pr�ba tworzenia
     *   zostanie ponowiona.
     * <li> Tworzenie (odczyt) ga��zi podrz�dnej. Je�eli uda si� odczyta�/utworzy�,
     *   nadana zostaje warto�� atrybutowi id. Je�eli wyst�pi b��d, ustawiona zostanie
     *   flaga sqlError (�adnych wyj�tk�w) i przy flush() pr�ba tworzenia
     *   zostanie ponowiona.
     *   Je�eli ga��� nadrz�dna nie ma id, tworzenie jest niemo�liwe, ponowna pr�ba
     *   zostanie podj�ta przy flush(). Ustawiona zostaje flaga sqlError.
     *
     * Je�eli wyst�pi b��d komunikacji z baz� danych, nie jest rzucany wyj�tek,
     * za� pr�ba utworzenia ga��zi jest ponawiana w {@link #flush()}.
     *
     * TODO: ga��� nie powinna by� tworzona, je�eli z w�tkiem nie jest zwi�zana transakcja,
     * nale�y rzuci� wyj�tek.
     * @param parent
     * @param name
     */
    SQLPreferences(SQLPreferences parent, String name, String username)
    {
        super(parent, name);
        this.username = username;

        if (isUserNode())
        {
            nodeTable = SQLPreferencesFactory.USER_NODE_TABLE;
            valueTable = SQLPreferencesFactory.USER_VALUE_TABLE;
            generator = SQLPreferencesFactory.USER_GENERATOR;
        }
        else
        {
            nodeTable = SQLPreferencesFactory.SYSTEM_NODE_TABLE;
            valueTable = SQLPreferencesFactory.SYSTEM_VALUE_TABLE;
            generator = SQLPreferencesFactory.SYSTEM_GENERATOR;
        }

        // je�eli istnieje ga��� nadrz�dna, ale jej tworzenie w bazie
        // nie powiod�o si�, bie��ca ga��� te� nie jest na razie tworzona
        // w bazie - odb�dzie si� to w flush()
        if (parent != null && parent.sqlError)
        {
            sqlError = true;
            return;
        }

        PreparedStatement ps = null;
        //Statement st = null;
        try
        {
            // pr�ba odczytania ga��zi z bazy danych            
            int parameterIndex = 1;
            if(DSApi.isOracleServer())
            {
                ps = SQLPreferencesFactory.connection().prepareStatement(
                    "SELECT ID FROM "+nodeTable+
                    " WHERE"+((name != null && name!="") ? " NAME = ? " : " NAME IS NULL ")
                    +"AND " +
                    (isUserNode() ? " USERNAME = ? " : " USERNAME IS NULL ") +
                    " AND " +
                    (parent != null ? " PARENT_ID = ? " : " PARENT_ID IS NULL "));
               
                if(name != null && name!="")
                {
                    ps.setString(parameterIndex++, name);
                }
            }
            else //if(DSApi.getDialect().equals(Docusafe.DB_FIREBIRD_1_5))
            {
                ps = SQLPreferencesFactory.connection().prepareStatement(
                    "SELECT ID FROM "+nodeTable+
                    " WHERE NAME = ? AND " +
                    (isUserNode() ? " USERNAME = ? " : " USERNAME IS NULL ") +
                    " AND " +
                    (parent != null ? " PARENT_ID = ? " : " PARENT_ID IS NULL "));
                
                if(name != null)
                {
                    ps.setString(parameterIndex++, name);
                }
                
            }
           
            if (isUserNode()) ps.setString(parameterIndex++, username);
            if (parent != null) ps.setInt(parameterIndex++, parent.id.intValue());

            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                id = new Integer(rs.getInt(1));
            }

            rs.close();
            ps.close();

            // ga��� istnieje w bazie danych i jest odczytywana
            if (id != null)
            {
                ps = SQLPreferencesFactory.connection().prepareStatement(
                    "SELECT PREF_KEY, PREF_VALUE FROM "+valueTable+
                    " WHERE NODE_ID = ? AND " +
                    (isUserNode() ? " USERNAME = ? " : " USERNAME IS NULL"));
                parameterIndex = 1;
                ps.setInt(parameterIndex++, id.intValue());
                if (isUserNode()) ps.setString(parameterIndex++, username);

                ResultSet rsp = ps.executeQuery();
                while (rsp.next())
                {
                    String key = rsp.getString(1);
                    Reader stream = rsp.getCharacterStream(2);
                    if (stream == null) continue;
                    StringBuilder value = new StringBuilder(128);
                    char[] buffer = new char[128];
                    int count;
                    while ((count = stream.read(buffer)) > 0) value.append(buffer, 0, count);
                    properties.put(key, value.toString());
                }
                rsp.close();
                ps.close();
            }
            // tworzenie ga��zi
            else
            {
                id = insertNode(SQLPreferencesFactory.connection(), parent, name,
                    isUserNode() ? username : null);

                newNode = true;
            }

            if (log.isTraceEnabled()){
                log.trace("ctor ("+absolutePath()+") properties="+properties);
            }
        }
        catch (Exception e)
        {
            sqlError = true;
            id = null;
            properties.clear();
            log.error("ctor ("+absolutePath()+"): "+e.getMessage(), e);
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
        }
    }

    protected AbstractPreferences childSpi(String name)
    {
        return new SQLPreferences(this, name, username);
    }

    protected String getSpi(String key)
    {
        return (String) properties.get(key);
    }

    protected void putSpi(String key, String value)
    {
        flushNeeded = true;
        properties.put(key, value);
    }

    protected void removeSpi(String key)
    {
        flushNeeded = true;
        properties.remove(key);
    }


    /* Metody mog�ce rzuci� wyj�tek */


    /**
     * TODO: nic nie robi�, je�eli z w�tkiem nie jest zwi�zana transakcja
     */
    protected void flushSpi() throws BackingStoreException
    {
        if (log.isDebugEnabled())
            log.debug("flushSpi(): sqlError="+sqlError);
        // Ga��� powinna by�a zosta� utworzona w konstruktorze, je�eli
        // jednak si� tak nie sta�o, tutaj nast�puje kolejna pr�ba.
        // Je�eli te� si� nie powiedzie, zostanie rzucony wyj�tek.

        // je�eli ga��� nie zosta�a odczytana lub utworzona w konstruktorze,
        // jest to robione teraz
        if (sqlError)
        {
            if (log.isDebugEnabled()){
                log.debug("flushSpi(): ("+absolutePath()+") sqlError="+sqlError+
                    " isRemoved="+isRemoved());
            }

            if (parent() != null && ((SQLPreferences) parent()).sqlError)
                throw new BackingStoreException("Ga��� nadrz�dna ("+
                    parent().absolutePath()+") nie zosta�a prawid�owo odczytana " +
                    "z bazy, sqlError=true");

            // je�eli parent IS NULL - tworzenie z parent_id = null, id = :next
            
            
            PreparedStatement ps = null;
            PreparedStatement psc = null;
            try
            {
                // sprawdzam, czy taka ga��� ju� istnieje
                psc = SQLPreferencesFactory.connection().prepareStatement(
                    "SELECT ID FROM "+nodeTable+" WHERE NAME = ? AND "+
                    (isUserNode() ? " USERNAME = ? " : " USERNAME IS NULL ") +
                    " AND " +
                    (parent() != null ? " PARENT_ID = ? " : " PARENT_ID IS NULL "));
                int parameterIndex = 1;
                if(name()!=null)psc.setString(parameterIndex++, name());
                if (isUserNode()) psc.setString(parameterIndex++, username);
                if (parent() != null) psc.setInt(parameterIndex++, ((SQLPreferences) parent()).id.intValue());

                ResultSet rsc = psc.executeQuery();

                // je�eli ga��� istnia�a ju� w bazie danych, pobieram tylko id
                if (rsc.next())
                {
                    id = new Integer(rsc.getInt(1));
                }
                // tworzenie ga��zi (o ile nie zosta�a wcze�niej usuni�ta)
                else if (!isRemoved())
                {

                	id = insertNode(SQLPreferencesFactory.connection(), 
                        (SQLPreferences) parent(), name(),
                        isUserNode() ? username : null);
                }
            }
            catch (SQLException e)
            {
                throw new BackingStoreException(new EdmSQLException(e));
            }
            catch (HibernateException e)
            {
                throw new BackingStoreException(new EdmHibernateException(e));
            }
            catch (EdmException e)
            {
                throw new BackingStoreException(e);
            }
            finally
            {
                if (ps != null) try { ps.close(); } catch (Exception e) { }
                if (psc != null) try { psc.close(); } catch (Exception e) { }
                
            }

            sqlError = false;
        }

        if (!flushNeeded)
        {
            if (log.isDebugEnabled())
                log.debug("flushSpi(): ("+absolutePath()+"): flushNeeded=false, nic nie robi�");
            return;
        }

        //Statement st = null;
        PreparedStatement ps = null;

        try
        {
            if (isRemoved())
            {
                if (log.isDebugEnabled())
                    log.debug("flushSpi(): ("+absolutePath()+"): usuwanie ga��zi");

                ps = SQLPreferencesFactory.connection().prepareStatement(
                    "DELETE FROM "+nodeTable+" WHERE NAME = ? AND "+
                    (isUserNode() ? " USERNAME = ? " : " USERNAME IS NULL ") +
                    " AND " +
                    (parent() != null ? "PARENT_ID = ? " : "PARENT_ID IS NULL"));
                int parameterIndex = 1;
                ps.setString(parameterIndex++, name());
                if (isUserNode()) ps.setString(parameterIndex++, username);
                if (parent() != null) ps.setInt(parameterIndex++, ((SQLPreferences) parent()).id.intValue());
                ps.executeUpdate();
            }
            else
            {
                if (log.isDebugEnabled())
                    log.debug("flushSpi(): ("+absolutePath()+"): aktualizacja warto�ci ("+
                        properties.size()+": "+properties+")");

                ps = SQLPreferencesFactory.connection().prepareStatement(
                    "DELETE FROM "+valueTable+
                    " WHERE NODE_ID = ? AND " +
                    (isUserNode() ? " USERNAME = ? " : " USERNAME IS NULL "));
                int parameterIndex = 1;
                ps.setInt(parameterIndex++, id.intValue());
                if (isUserNode()) ps.setString(parameterIndex++, username);
                ps.executeUpdate();
                ps.close();

                if (properties.size() > 0)
                {
                    ps = SQLPreferencesFactory.connection().prepareStatement(
                        "INSERT INTO "+valueTable+" (NODE_ID, PREF_KEY, PREF_VALUE, USERNAME) " +
                        "VALUES (?, ?, ?, ?)");

                    for (Iterator iter=properties.entrySet().iterator(); iter.hasNext(); )
                    {
                        Map.Entry entry = (Map.Entry) iter.next();
                        ps.setInt(1, id.intValue());
                        ps.setString(2, (String) entry.getKey());
                        String value = (String) entry.getValue();
                        ps.setCharacterStream(3, new StringReader(value), value.length());
                        //ps.setString(3, (String) entry.getValue());
                        if (isUserNode())
                            ps.setString(4, username);
                        else
                            ps.setString(4, null);
                        ps.executeUpdate();
                    }
                }
            }
        }
        catch (SQLException e)
        {
            throw new BackingStoreException(new EdmSQLException(e));
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
            
        }

        flushNeeded = false;
    }

    protected void removeNodeSpi() throws BackingStoreException
    {
        // nie trzeba ustawia� flagi flushNeeded, bo je�eli
        // parent.sqlError = true, flushSpi() wykona modyfikacje
        // bazy danych, a je�eli parent.sqlError = false,
        // modyfikacje zostan� wykonane od razu

        // je�eli podczas odczytu ga��zi nadrz�dnej wyst�pi� b��d,
        // bie��ca ga��� b�dzie usuwana dopiero w flush()
        // XXX: b��d? flush() dzia�a top-down, remove() dzia�a bottom-top
    	  
        if (parent() != null && ((SQLPreferences) parent()).sqlError)
        {
            properties.clear();
            return;
        }

        PreparedStatement ps = null;

        try
        {
            ps = SQLPreferencesFactory.connection().prepareStatement(
                "DELETE FROM "+nodeTable+" WHERE NAME = ? AND "+
                (isUserNode() ? " USERNAME = ? " : " USERNAME IS NULL ") +
                " AND " +
                (parent() != null ? "PARENT_ID = ?" : "PARENT_ID IS NULL"));
            int parameterIndex = 1;
            ps.setString(parameterIndex++, name());
            if (isUserNode()) ps.setString(parameterIndex++, username);
            if (parent() != null) ps.setInt(parameterIndex++, ((SQLPreferences) parent()).id.intValue());

            ps.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new BackingStoreException(new EdmSQLException(e));
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
        }
    }

    /**
     * Chwilowo niezaimplementowane.
     * @throws BackingStoreException
     */
    protected void syncSpi() throws BackingStoreException
    {
/*
        // je�eli ga��� nie zosta�a odczytana z bazy danych, nic tu nie robi�
        if (sqlError)
        {
            log.warn("sync ("+absolutePath()+"): sqlError=true, nic nie robi�");
            return;
        }

        Map tempProperties = new HashMap();

        PreparedStatement ps = null;
        try
        {
            ps = SQLPreferencesFactory.connection().prepareStatement(
                "SELECT PREF_KEY, PREF_VALUE " +
                "FROM "+valueTable+
                " WHERE NODE_ID = ? AND "+
                (isUserNode() ? " USERNAME = ? " : " USERNAME IS NULL "));
            int parameterIndex = 1;
            ps.setInt(parameterIndex++, id.intValue());
            if (isUserNode()) ps.setString(parameterIndex++, SQLPreferencesFactory.username());

            ResultSet rsp = ps.executeQuery();

            while (rsp.next())
            {
                tempProperties.put(rsp.getString(1), rsp.getString(2));
            }
            rsp.close();
            ps.close();

            properties = tempProperties;
        }
        catch (SQLException e)
        {
            throw new BackingStoreException(new EdmSQLException(e));
        }
        catch (EdmException e)
        {
            throw new BackingStoreException(e);
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
        }
*/
    }

    protected String[] childrenNamesSpi() throws BackingStoreException
    {
        if (sqlError)
        {
            throw new BackingStoreException("Ga��� nie zosta�a prawid�owo odczytana " +
                "z bazy danych, nie mo�na pobra� nazw ga��zi podrz�dnych");
        }

        PreparedStatement ps = null;
        try
        {
            ps = SQLPreferencesFactory.connection().prepareStatement(
                "SELECT NAME FROM "+nodeTable+
                " WHERE PARENT_ID = ? AND " +
                (isUserNode() ? " USERNAME = ? " : " USERNAME IS NULL "));
            int parameterIndex = 1;
            ps.setInt(parameterIndex++, id.intValue());
            if (isUserNode()) ps.setString(parameterIndex++, username);

            ResultSet rs = ps.executeQuery();

            List<String> results = new ArrayList<String>(16);
            while (rs.next())
            {
                results.add(rs.getString(1));
            }
            rs.close();

            return (String[]) results.toArray(new String[results.size()]);
        }
        catch (SQLException e)
        {
            throw new BackingStoreException(new EdmSQLException(e));
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
        }
    }

    // TODO: zwraca� list� kluczy z kolekcji properties
    protected String[] keysSpi() throws BackingStoreException
    {
        if (sqlError)
        {
            throw new BackingStoreException("Ga��� nie zosta�a prawid�owo odczytana " +
                "z bazy danych, nie mo�na pobra� nazw kluczy");
        }

        PreparedStatement ps = null;
        try
        {
            ps = SQLPreferencesFactory.connection().prepareStatement(
                "SELECT PREF_KEY FROM "+valueTable+
                " WHERE NODE_ID = ? AND "+
                (isUserNode() ? " USERNAME = ? " : " USERNAME IS NULL "));
            int parameterIndex = 1;
            ps.setInt(parameterIndex++, id.intValue());
            if (isUserNode()) ps.setString(parameterIndex++, username);

            ResultSet rs = ps.executeQuery();

            List<String> results = new ArrayList<String>(16);
            while (rs.next())
            {
                results.add(rs.getString(1));
            }
            rs.close();

            return results.toArray(new String[results.size()]);
        }
        catch (SQLException e)
        {
            throw new BackingStoreException(new EdmSQLException(e));
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
        }
    }

    private Integer insertNode(Connection conn, SQLPreferences parent, String name, String username)
        throws SQLException, HibernateException, EdmException
    {
        Integer nodeId = null;

        boolean sequences = DSApi.getDialect().supportsSequences();

        if (sequences)
        {
            Statement st = conn.createStatement();
            ResultSet rsid = st.executeQuery(DSApi.getDialect().getSequenceNextValString(generator));
            if (!rsid.next())
                throw new EdmException("Nie mo�na pobra� warto�ci z sekwencji "+generator);

            nodeId = new Integer(rsid.getInt(1));

            rsid.close();
            st.close();
        }

        // je�eli dialekt wspiera sekwencje, id jest ju� pobrane i wstawiam
        // je tu do tabeli; je�eli nie, pobior� je po wstawieniu wiersza
        PreparedStatement ps = SQLPreferencesFactory.connection().prepareStatement(
            "INSERT INTO "+nodeTable+" (PARENT_ID, NAME, USERNAME"+
                (sequences ? ", ID" : "") +
            ") VALUES " +
            "(?, ?, ?"+(sequences ? ", ?" : "")+")");

        if (parent != null)
        {
            ps.setInt(1, parent.id.intValue());
        }
        else
        {
            ps.setObject(1, null);
        }
        ps.setString(2, name);
        if (username != null)
        {
            ps.setString(3, username);
        }
        else
        {
            ps.setObject(3, null);
        }

        if (sequences)
        {
            ps.setInt(4, nodeId.intValue());
        }

        ps.executeUpdate();
        ps.close();

        if (!sequences)
        {        	
            Statement st = SQLPreferencesFactory.connection().createStatement();
            ResultSet rs = st.executeQuery(DSApi.getDialect().getIdentitySelectString(nodeTable, "id", Types.BIGINT));
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� warto�ci z sekwencji "+
                    DSApi.getDialect().getIdentitySelectString(nodeTable, "id", Types.BIGINT));
            nodeId = new Integer(rs.getInt(1));
            rs.close();
            st.close();
            
        	//throw new EdmException("TA FUNKCJONALNOSC JESZCZE NIE PRZEROBIONA NA Hibernate3");
        }

        return nodeId;
    }
}
