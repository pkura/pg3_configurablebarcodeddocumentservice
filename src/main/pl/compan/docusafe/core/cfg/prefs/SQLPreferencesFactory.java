package pl.compan.docusafe.core.cfg.prefs;

import org.hibernate.HibernateException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SQLPreferencesFactory.java,v 1.13 2008/10/06 11:15:15 pecet4 Exp $
 */
public class SQLPreferencesFactory implements PreferencesFactory
{
    private static final Log log = LogFactory.getLog(SQLPreferencesFactory.class);

    static final String SYSTEM_NODE_TABLE = "DS_PREFS_NODE";
    static final String SYSTEM_VALUE_TABLE = "DS_PREFS_VALUE";
    static final String SYSTEM_GENERATOR = "DS_PREFS_NODE_ID";

    static final String USER_NODE_TABLE = "DS_PREFS_NODE";
    static final String USER_VALUE_TABLE = "DS_PREFS_VALUE";
    static final String USER_GENERATOR = "DS_PREFS_NODE_ID";

    /**
     * Zwraca g��wn� ga��� preferencji systemowych.  Metoda musi
     * by� wywo�ywana w ramach otwartej sesji (DSContext). Ka�de
     * wywo�anie zwraca now� instancj�.
     */
    public Preferences systemRoot()
    {
        return new SQLPreferences(null, "", null);
    }

    /**
     * Zwraca g��wn� ga��� preferencji zalogowanego u�ytkownika.  Metoda musi
     * by� wywo�ywana w ramach otwartej sesji (DSContext). Ka�de wywo�anie
     * zwraca now� instancj�.
     */
    public Preferences userRoot()
    {
        return userRoot(DSApi.context().getPrincipalName());
    }

    /**
     * Zwraca g��wn� ga��� preferencji dowolnego u�ytkownika.
     */
    public Preferences userRoot(String username)
    {
        return new SQLPreferences(null, "", username);
    }

    static Connection connection() throws SQLException
    {
        try
        {
            return DSApi.context().session().connection();
        }
        catch (IllegalStateException e)
        {
            log.error(e.getMessage(), e);
            throw new SQLException("Brak po��czenia");
        }
        catch (EdmException e)
        {
            log.error(e.getMessage(), e);
            throw new SQLException("Brak po��czenia ("+e.getMessage()+")");
        }
        catch (HibernateException e)
        {
            log.error(e.getMessage(), e);
            throw new SQLException("Brak po��czenia ("+e.getMessage()+")");
        }
    }

/*
    static String username() throws EdmException
    {
        return DSApi.context().getPrincipalName();
    }
*/

    public static void testRead()
    {

        SQLPreferencesFactory factory = new SQLPreferencesFactory();
        try
        {
            DSApi.openAdmin();

            Preferences prefs = factory.systemRoot();

            Preferences fax = prefs.node("modules/fax");

        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            LogFactory.getLog("eprint").debug("", e);
        }
        finally
        {
            try { DSApi.close(); } catch (Exception e) { }
        }
    }

    static void print(Preferences root)
    {
    }

    public static void test()
    {

        // XXX: kod jest nieprawid�owy, obiekt Preferences powinien
        // by� zapami�tywany, bo factory.*root zwraca zawsze now� instancj�
        SQLPreferencesFactory factory = new SQLPreferencesFactory();
        try
        {
            DSApi.openAdmin();
            DSApi.context().begin();

            Preferences prefs = factory.systemRoot();
            prefs.putBoolean("bool", true);
            prefs.node("modules/faks").put("route.1", "(22) 343434");
            prefs.node("modules/fax2").remove("route.1");

            prefs.node("xix").put("a", "b");

            prefs.sync();


            prefs.node("modules/fax").removeNode();
            prefs.node("modules/fax_XX").removeNode();

            print(prefs);

            prefs.flush();

            print(prefs);

            //prefs.node("modules").node("fax").removeNode();

            print(prefs);

            Preferences u = factory.userRoot();

            //u.node("users2").putBoolean("xxx", true);
            u.node("users2").removeNode();

            u.flush();

            DSApi.context().commit();
        }
        catch (Exception e)
        {
            DSApi.context().setRollbackOnly();
            log.error(e.getMessage(), e);
            LogFactory.getLog("eprint").debug("", e);
        }
        finally
        {
            try { DSApi.close(); } catch (Exception e) { }
        }
    }
}
