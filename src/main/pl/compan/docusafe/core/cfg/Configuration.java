package pl.compan.docusafe.core.cfg;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.common.license.License;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.StringManager;

import com.google.common.collect.Maps;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Configuration.java,v 1.128 2010/08/16 09:30:43 pecet1 Exp $
 */
public class Configuration
{ 
    private static final Log log = LogFactory.getLog(Configuration.class);
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public static final String SUBDIR_MAIL = "mail";
    public static final String SUBDIR_LOGS = "logs";
    public static final String SUBDIR_TEMPLATES = "templates";

    public static final Charset MAIL_CHARSET = Charset.forName(Docusafe.getAdditionProperty("mail.charset"));

    public static final String ADDITION_ORDERS = "orders";
    public static final String ADDITION_EDITOR = "editor";
    public static final String ADDITION_MAIL = "mail";
    public static final String ADDITION_CALENDAR = "calendar";
    public static final String ADDITION_JBPM = "jbpm";
    public static final String ADDITION_INTERNAL = "internal";
    public static final String ADDITION_BIP_SERVICE = "bip.service";
    /**
     * zmienna ukrywaj�ca List� kontakt�w
     */
    public static final String ADDITION_ADDRESS_BOOK = "addressbook"; 
    public static final String ADDITION_REGISTRIES = "registries";
    public static final String ADDITION_MULTITIFF = "multiTiff";
    public static final String ADDITION_T_C_LAYOUT = "tcLayout";
    public static final String ADDITION_ROLL_MENU = "rollMenu";
    public static final String ADDITION_ARCHIVE_ACCEPTANCES_RIGHT = "archiveAcceptancesRight";
    
    public static final String IMAGES_DECOMPRESSED_IN_LOCAL_FILE_SYSTEM = "imagesDecompressedAsFiles";
    public static final String ADDITION_ATTACHMENTS_AS_FILES = "attachmentsAsFiles";
    public static final String PATH_TO_ATTACHMENTS_WITH_DATE = "pathToAttachmentsWithDate";
    public static final String ADDITION_CLEAN_TASKLIST_ON_BOOT = "cleanTasklistOnBoot";
    public static final String ADDITION_RELOAD_TASKLIST_ON_BOOT = "reloadTasklistOnBoot";
    public static final String ADDITION_COUNT_REPLIES_ON_TASKLIST = "countRepliesOnTasklist";  
    public static final String ADDITION_IMPORT_ENCODING = "importEncoding";
    public static final String ADDITION_MULTI_REALIZATION_ASSIGNMENT = "multiRealizationAssignment";
    public static final String ADDITION_GENERATE_XLS_REPORT_FROM_SEARCH_DOCKIND = "generateXlsReportFromSearchDockind";
    public static final String ADDITION_GENERATE_CSV_REPORT_FROM_SEARCH_DOCKIND = "generateCsvReportFromSearchDockind";
    public static final String ADDITION_GENERATE_XML_REPORT_FROM_SEARCH_DOCKIND = "generateXmlReportFromSearchDockind";
    public static final String ADDITION_DO_NOT_REQUIRE_RESPOND = "doNotRequireRespond";
    public static final String ADDITION_DISPATCH_DIVISION ="uproszczonaWysylka";
    public static final String ADDITION_EXTENDED_TREE="extendedTree";
    public static final String ADDITION_JBPM_ACCEPTANCES="jbpmAcceptances";
	public static final String ADDITION_WAITING_REPLIES="waitingReplies";
    public static final String FILTER_LATENCY = "filterLatency";

    /**
     * Nazwa parametru kontekstowego aplikacji, pod kt�r� nale�y szuka�
     * nazwy katalogu domowego. Parametr jest zazwyczaj definiowany w
     * pliku konfiguracyjnym serwera aplikacyjnego (Tomcat - conf/server.xml).
     */
    public static final String HOME = "homeDirectory";

    private static boolean applicationInitialized = false;

    @Deprecated
    private static String ldapRoot;

    private static DataSource dataSource;

    private static File homeDirectory;

    private static final Properties properties = new Properties();

    private static ServletContext servletContext;

    //private static boolean evaluation;
    /**
     * Lista nazw dost�pnych modu��w pobrana z pliku licencji.
     * Tablica po pobraniu powinna zosta� posortowana.
     */
    //private static String[] availableModules;
    /**
     * Moment w czasie, po osi�gni�ciu kt�rego aplikacja powinna
     * przesta� dzia�a�. Je�eli 0, nie ma znaczenia.
     */
    //private static long expiration;
    /**
     * Maksymalna liczba u�ytkownik�w, kt�rych mo�na utworzy�
     * i u�ywa� w aplikacji.
     */
    //private static int maxUsers;
    /**
     * Numer wersji aplikacji.
     */
    private static String versionString;
    private static int majorVersion;
    private static int minorVersion;
    private static int distNumber;
    private static String buildId;

    //private static Sitemap sitemap;
    //private static SiteMap sitemap;

    private static String mailFromEmail;
    private static String mailFromPersonal;
    private static String baseUrl;
    private static String userFactory;

    private static String databaseType;

    private static final FeatureSet moduleSet = new FeatureSet();
    private static final FeatureSet extraSet = new FeatureSet();
    //private static Set extras = new HashSet();
    public static final String DB_FIREBIRD_1_0 = "firebird_1.0";
    public static final String DB_FIREBIRD_1_5 = "firebird_1.5";
    public static final String ORACLE_9 = "oracle_9";
    public static final String DB_INTERBASE_7_5 = "interbase_7.5";
    public static final String DB_SQLSERVER2000 = "sqlserver_2000";

    /** Mapa zawieraj�ca nazwy klas Driver dla poszczeg�lnych baz danych */
    private static final Map<String, String> dbDrivers;
    static {
        Map<String, String> _dbDrivers = Maps.newHashMap();
        _dbDrivers.put(Configuration.DB_FIREBIRD_1_0, "org.firebirdsql.jdbc.FBDriver");
        _dbDrivers.put(Configuration.DB_FIREBIRD_1_5, "org.firebirdsql.jdbc.FBDriver");
        _dbDrivers.put(Configuration.ORACLE_9, "oracle.jdbc.driver.OracleDriver");
        _dbDrivers.put(Configuration.DB_INTERBASE_7_5, "interbase.interclient.Driver");
        //dbDrivers.put(Configuration.DB_SQLSERVER2000, "com.microsoft.jdbc.sqlserver.SQLServerDriver");
        _dbDrivers.put(Configuration.DB_SQLSERVER2000, "net.sourceforge.jtds.jdbc.Driver");
        dbDrivers = Collections.unmodifiableMap(_dbDrivers);
    }

    public static String getDriverClass(String databaseType)
    {
        return (String) dbDrivers.get(databaseType);
    }

    /**
     * Zwraca true, je�eli aplikacja zosta�a poprawnie skonfigurowana na
     * podstawie plik�w w katalogu domowym.
     * @see #homeInit()
     * @deprecated Tylko w nieu�ywanych filtrach
     */
    public static boolean isConfigured()
    {
        return applicationInitialized;
    }

    /**
     * Zwraca obiekt File wskazuj�cy na katalog domowy aplikacji.
     * @deprecated
     * @see Docusafe#getHome()
     */
    public static File getHome()
    {
        // return homeDirectory;
        return Docusafe.getHome();
    }

    /**
     * @deprecated
     * @see pl.compan.docusafe.boot.Docusafe#getMailFromEmail()
     */
    public static String getMailFromEmail()
    {
        // return mailFromEmail;
        return Docusafe.getMailFromEmail();
    }

    /**
     * @deprecated
     * @see pl.compan.docusafe.boot.Docusafe#getMailFromPersonal()
     */
    public static String getMailFromPersonal()
    {
        //return mailFromPersonal;
        return Docusafe.getMailFromPersonal();
    }

    public static String getBaseUrl()
    {
        //return baseUrl;
        return Docusafe.getBaseUrl();
    }

    /**
     * @deprecated
     */
    public static String getVersionString()
    {
        //return versionString;
        return Docusafe.getVersion();
    }

    /**
     * @deprecated
     */
    public static int getDistNumber()
    {
        //return distNumber;
        return Docusafe.getDistNumber();
    }

    /**
     * @deprecated
     */
    public static String getBuildId()
    {
        //return buildId;
        return Docusafe.getBuildId();
    }

    /**
     * Zwraca parametr aplikacji o danej nazwie, je�eli parametr nie istnieje,
     * zwracana jest warto�� null. Parametry pochodz� z plik�w docusafe.properties
     * (w pliku war) oraz workflow.config (w katalogu domowym aplikacji).
     */
    public static String getProperty(String name)
    {
        //return properties.getProperty(name);
        return Docusafe.getProperty(name);
    }

    /**
     * Sprawdza czy jest dostepna dodatkowa funkcjonalnosc o nazwie addition
     */
    public static boolean additionAvailable(String addition) 
    {
        String property = Docusafe.getAdditionProperty("addition."+addition+".available");
        if ("true".equals(property))
            return true;

        return false;
    }

    /**
     * Zwraca list� dost�pnych dodatk�w.
     */
    public static String[] getAvailableAdditions()
    {
        List<String> additions = new ArrayList<String>();
        if (additionAvailable(ADDITION_EDITOR))
            additions.add(ADDITION_EDITOR);
        if (additionAvailable(ADDITION_ORDERS))
            additions.add(ADDITION_ORDERS);
        if (additionAvailable(ADDITION_MAIL))
            additions.add(ADDITION_MAIL);
        if (additionAvailable(ADDITION_CALENDAR))
            additions.add(ADDITION_CALENDAR);
        if (additionAvailable(ADDITION_ADDRESS_BOOK))
            additions.add(ADDITION_ADDRESS_BOOK);
        if(additionAvailable(ADDITION_ATTACHMENTS_AS_FILES))
            additions.add(ADDITION_ATTACHMENTS_AS_FILES);
        if(additionAvailable(PATH_TO_ATTACHMENTS_WITH_DATE))
            additions.add(PATH_TO_ATTACHMENTS_WITH_DATE);
        if(additionAvailable(IMAGES_DECOMPRESSED_IN_LOCAL_FILE_SYSTEM) && additionAvailable(ADDITION_ATTACHMENTS_AS_FILES))
            additions.add(IMAGES_DECOMPRESSED_IN_LOCAL_FILE_SYSTEM);
        if(additionAvailable(ADDITION_T_C_LAYOUT)){
        	additions.add(ADDITION_T_C_LAYOUT);
        }
        if(additionAvailable(ADDITION_ROLL_MENU))
        	additions.add(ADDITION_ROLL_MENU);
        if(additionAvailable(ADDITION_ARCHIVE_ACCEPTANCES_RIGHT))
        	additions.add(ADDITION_ARCHIVE_ACCEPTANCES_RIGHT);
        if(additionAvailable(ADDITION_JBPM))
        	additions.add(ADDITION_JBPM);
        if(additionAvailable(ADDITION_INTERNAL))
        	additions.add(ADDITION_INTERNAL);
        if(additionAvailable(ADDITION_DISPATCH_DIVISION)){
        	additions.add(ADDITION_DISPATCH_DIVISION);
        }
        if(additionAvailable(ADDITION_EXTENDED_TREE)){
        	additions.add(ADDITION_EXTENDED_TREE);
        }
		if(additionAvailable(ADDITION_WAITING_REPLIES)){
			additions.add(ADDITION_WAITING_REPLIES);
		}
        if(additionAvailable(ADDITION_JBPM_ACCEPTANCES)){
        	additions.add(ADDITION_JBPM_ACCEPTANCES);
        }
        
        
        return (String[]) additions.toArray(new String[additions.size()]);
    }
    
    
    /**
     * Zwraca sciezke do zalacznikow (jesli sa trzymane jako pliki a nie w bazie danych)
     */
    public static String getPathToAttachments()
    {
        return Docusafe.getAdditionProperty("addition.pathToAttachments");
    }

    /** Zwraca true <=> w�asno�� ma ustawion� warto�� 'true' */
    public static boolean isAdditionOn(String property)
    {
        return "true".equals(Docusafe.getAdditionProperty(property));
    }
    
    /**
     * @deprecated
     * @see pl.compan.docusafe.boot.Docusafe#getServletContext()
     */
    public static ServletContext getServletContext()
    {
        //return servletContext;
        return Docusafe.getServletContext();
    }

    /**
     * @deprecated
     */
    public static boolean moduleAvailable(String moduleName)
    {
        return Docusafe.moduleAvailable(moduleName);
    }

    /**
     * True, je�eli dost�pny jest modu� kancelarii.
     */
    public static boolean officeAvailable()
    {
        return Docusafe.moduleAvailable(Modules.MODULE_OFFICE);
    }

    public static boolean coreOfficeAvailable()
    {
        return Docusafe.moduleAvailable(Modules.MODULE_COREOFFICE);
    }

    public static boolean faxAvailable()
    {
        return Docusafe.moduleAvailable(Modules.MODULE_FAX);
    }

    /**
     * True, je�eli dost�pny jest modu� workflow.
     */
    @Deprecated
    public static boolean workflowAvailable()
    {
        return Docusafe.moduleAvailable(Modules.MODULE_WORKFLOW);
    }

    public static boolean simpleOfficeAvailable()
    {
        return Docusafe.moduleAvailable(Modules.MODULE_SIMPLEOFFICE);
    }

    public static Reader getMail(Mail mail) throws IOException
    {
        return new InputStreamReader(
            new FileInputStream(
                new File(new File(Docusafe.getHome(), SUBDIR_MAIL), mail.getName())),
            MAIL_CHARSET);
    }
    
    public static Reader getTemplate(String name) throws IOException
    {
        return new InputStreamReader(
            new FileInputStream(
                new File(new File(Docusafe.getHome(), SUBDIR_TEMPLATES), name)),
            MAIL_CHARSET);
    }

    public static Reader getMail(String template) throws IOException
    {
        return new InputStreamReader(
            new FileInputStream(
                new File(new File(Docusafe.getHome(), SUBDIR_MAIL), template)),
            MAIL_CHARSET);
    }
    public static String getIdString()
    {
        return Docusafe.getIdString();
    }

    public static String getJVMString()
    {
        return Docusafe.getJVMString();
    }

    private static boolean warInitCompleted = false;

    static class FeatureSet
    {
        private Set features = new HashSet();
        private String[] array;
        private Map featureMap;

        public synchronized void add(String feature) { features.add(feature); array = null; featureMap = null; }
        public synchronized boolean contains(String feature) { return features.contains(feature); }
        public synchronized void clear() { features.clear(); array = null; featureMap = null; }
        public synchronized String[] asArray()
        {
            if (array == null)
            {
                array = (String[]) features.toArray(new String[features.size()]);
            }
            return array;
        }
        public synchronized Map asFeatureMap()
        {
            if (featureMap == null)
            {
                featureMap = new HashMap(features.size());
                for (Iterator iter=features.iterator(); iter.hasNext(); )
                    featureMap.put(iter.next(), Boolean.TRUE);
            }
            return featureMap;
        }
        public String toString() { return features.toString(); }
    }

    private static License license;

    public static void licenseInit(License license) throws EdmException
    {
        if (license == null)
            throw new NullPointerException("license");

        Configuration.license = license;

        moduleSet.clear();
        
        String[] moduleNames = license.getModules();
        for (int i=0; i < moduleNames.length; i++)
        {
            moduleSet.add(moduleNames[i]);
        }

        // zale�no�ci pomi�dzy modu�ami
        if (moduleSet.contains(Modules.MODULE_INVOICES) ||
            moduleSet.contains(Modules.MODULE_CONTRACTS))
        {
            moduleSet.add(Modules.MODULE_REGISTRIES);
        }

        if (moduleSet.contains(Modules.MODULE_SIMPLEOFFICE) || moduleSet.contains(Modules.MODULE_OFFICE) ||
            moduleSet.contains(Modules.MODULE_REGISTRIES) || moduleSet.contains(Modules.MODULE_FAX))
        {
            moduleSet.add(Modules.MODULE_COREOFFICE);
        }

        extraSet.clear();
        String[] extra = license.getExtras();
        if (extra != null)
        {
            for (int i=0; i < extra.length; i++)
            {
                extraSet.add(extra[i]);
            }
        }

        statusMessage("Licencja dla: "+license.getCustomer()+//", nr: "+license.getLicenseNo()+
            ", modu�y: "+license.getModulesAsString(", ")+", "+moduleSet+
            ", extras: "+extraSet);

        DSPermission.init();

        if (log.isDebugEnabled())
            log.debug("licenseInit(): moduleSet="+moduleSet+" extras="+extraSet);
    }


    /**
     * @deprecated Tylko w EdmListener
     */
    public static String getLicenseString()
    {
        if (license != null)
        {
            return license.getCustomer()+" ["+license.getModulesAsString(" ")+"] "+
                "["+license.getExtrasAsString(" ")+"]";
        }
        return "[brak licencji]";
    }

    public static boolean hasExtra(String extra)
    {
        //return extraSet.contains(extra);
        return Docusafe.hasExtra(extra);
    }

    public static void statusMessage(String message)
    {
    }

    /**
     * Odczytuje plik z w�asno�ciami w formacie Properties i do��cza odczytane
     * w�asno�ci do przekazanego obiektu Properties sprawdzaj�c, czy nie wyst�puje
     * konflikt nazw w�asno�ci. Je�eli taki konflikt wyst�pi, rzucany jest wyj�tek
     * EdmException.
     */
    private static void appendProperties(Properties properties, File file)
        throws EdmException
    {
        FileInputStream is = null;
        try
        {
            is = new FileInputStream(file);

            Properties newProperties = new Properties();
            newProperties.load(is);

            Enumeration propertiesEnum = newProperties.propertyNames();
            while (propertiesEnum.hasMoreElements())
            {
                String property = (String) propertiesEnum.nextElement();
                if (properties.getProperty(property) != null)
                    throw new EdmException("Konflikt nazw parametr�w: w pliku "+file+
                        " okre�lono parametr "+property+", kt�ry znajduje si� ju� w innym " +
                        "pliku konfiguracyjnym.");
                properties.setProperty(property, newProperties.getProperty(property));
            }
        }
        catch (IOException e)
        {
            throw new EdmException(e.getMessage(), e);
        }
        finally
        {
        	org.apache.commons.io.IOUtils.closeQuietly(is);
        }
    }
}
