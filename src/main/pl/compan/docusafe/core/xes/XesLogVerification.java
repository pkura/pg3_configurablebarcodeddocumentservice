package pl.compan.docusafe.core.xes;

import java.util.concurrent.ExecutionException;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

import com.opensymphony.webwork.ServletActionContext;


/**
 *     
 * @author Grzegorz Filip
 * Klasa weryfikująca czy dana akcja jest logowana do xesa 
 */
public class XesLogVerification implements ActionListener {
private Logger log = LoggerFactory.getLogger(XesLogVerification.class);

private static boolean xesApiAvailable = AvailabilityManager.isAvailable("XesLogApi");
    @Override
    public void actionPerformed(ActionEvent event) {
    	  try
		{
    	 if(xesApiAvailable){
			XesLog.isXesLogForThisAction(event, ServletActionContext.getRequest().getRequestURI());
    	 
    	 }
		} catch (ExecutionException e)
		{
			log.error("", e);
		} catch (EdmException e)
		{
			log.error("", e);
		}
    }
}