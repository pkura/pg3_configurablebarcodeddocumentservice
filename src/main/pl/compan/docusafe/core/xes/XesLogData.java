package pl.compan.docusafe.core.xes;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.beust.jcommander.internal.Lists;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import org.springframework.util.CollectionUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;



/**
 * Klasa odwzorowuj�ca tabelk� DS_XES_LOG_DATA
 * 
 * @author Grzegorz Filip
 */

public class XesLogData implements Serializable
{


	public XesLogData(Long dockindId, Long userId, Long documentId, Date actionTime, String actionName, String activitiId, Long actionDataId, Long versionId,
			XesLog xesLog, Date creatingTime, Integer xesLogType)
	{
		super();
		this.dockindId = dockindId;
		this.userId = userId;
		this.documentId = documentId;
		this.actionTime = actionTime;
		this.actionName = actionName;
		this.activitiId = activitiId;
		this.actionDataId = actionDataId;
		this.versionId = versionId;
		this.xesLog = xesLog;
		this.creatingTime=creatingTime;
		this.xesLogType = xesLogType;
	}

	private Logger log = LoggerFactory.getLogger(XesLogData.class);
	
	
	
	/**
	 * Id Pozycji  
	 */
	private Long id;
	/**
	 * Id Dokinda 
	 */
	private Long dockindId;

	/**
	 * Id uzytkownika DSUser wykonuj�cego akcje 
	 */
	private Long userId;

	/**
	 * Id dokumentu na kt�rym wykonano akcj�
	 */
	private Long documentId;
	/**
	 * Id Sprawy na kt�rym wykonano akcj�
	 */
	private Long caseId;
	/**
	 * Id Teczki na kt�rym wykonano akcj�
	 */
	private Long officeFolderId;
	
	/**
	 * Czas wykonania czynnosci
	 */
	private Date actionTime;
	/**
	 * nazwa wykonywanej czynno�ci
	 */
	private String actionName;
	/**
	 * activitiId
	 */
	private String activitiId;

	/**
	 * Id pozycji  z Tabeli Powi�zanej zawierajac� dodatkowe  dane zdazenia DS_XES_LOG_DATA
	 */
	private Long actionDataId;
	/**
	 * Id versji 
	 */
	private Long versionId;
	
	/**
	 * XesXml Zapisany Do Bazy
	 */
	private byte[] xesXmlData;
	/**
	 * Id z tabeli DS_XES_LOG 
	 */
	private XesLog xesLog;
	/**
	 * nazwa klasy z kt�rej wykonywano akcj�(czynno�ci)
	 */
	private String actionClass; 
	
	/**
	 * Uri akcji
	 */
	private String actionUri;
	/**
	 * Domy�lny konstruktor
	 */
	/** ENUM
	 * Typ Logu czy dotyczy  usera czy  dokumentu 
	 * 1 = user 2 = document 3 - niezdefiniowany
	 */
	  private Integer xesLogType;

	  /**
		 * Data utworzenia loga 
		 */
		private Date creatingTime;
		
	public XesLogData()
	{
	}

	public void delete() throws Exception
	{
		log.debug("deleting " + this);
		try
		{
			DSApi.context().session().delete(this);
			DSApi.context().session().flush();
		} catch (HibernateException e)
		{
			log.error("Blad usuniecia wpisu");
			throw new EdmException("Blad usuniecia wpisu. " + e.getMessage());
		}
	}


	public void create() throws Exception, EdmException
	{
		log.debug("create()");

		DSApi.context().session().save(this);
		DSApi.context().session().flush();
	}

/**
 * Zwraca obiekt  XesLogData o podanym id  lub null je�li nie znaleziono 
 * @param actionDataId
 * @return XesLogData object
 * @throws EdmException
 */
	public static XesLogData findById(long actionDataId) throws EdmException
	{
		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq("id", actionDataId));
		if (criteria.list() != null)
			return (XesLogData) criteria.list().get(0);
		else
			return null;
	}
	/**
	 * Zwraca liste obiekt�w znalezionych dla podanego id dokumentu lista mo�e by� pusta 
	 * @param documentId
	 * @return List XesLogData
	 * @throws EdmException
	 */
	public static List<XesLogData> findAllByDocumentId(long documentId) throws EdmException
	{
		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq("documentId", documentId));
	    return  criteria.list();
	}

    /**
     * Zwraca liste obiekt�w znalezionych dla podanego u�ytkownika
     * @param userId
     * @return
     * @throws EdmException
     */
    public static List<XesLogData> findAllByUserId(long userId) throws EdmException{
        return getCriteria().add(Restrictions.eq("userId", userId)).list();
    }

    private static Criteria getCriteria() throws EdmException {
        return DSApi.context().session().createCriteria(XesLogData.class);
    }

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Long getDockindId()
	{
		return dockindId;
	}


	public void setDockindId(Long dockindId)
	{
		this.dockindId = dockindId;
	}


	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public Long getDocumentId()
	{
		return documentId;
	}

	public void setDocumentId(Long documentId)
	{
		this.documentId = documentId;
	}

	public Date getActionTime()
	{
		return actionTime;
	}


	public void setActionTime(Date actionTime)
	{
		this.actionTime = actionTime;
	}

	public String getActionName()
	{
		return actionName;
	}

	public void setActionName(String actionName)
	{
		this.actionName = actionName;
	}

	

	public Long getActionDataId()
	{
		return actionDataId;
	}

	public void setActionDataId(Long actionDataId)
	{
		this.actionDataId = actionDataId;
	}


	public Long getVersionId()
	{
		return versionId;
	}

	public void setVersionId(Long versionId)
	{
		this.versionId = versionId;
	}


	

	public void setXesLog(XesLog xesLog)
	{
		this.xesLog = xesLog;
	}

	
	public XesLog getXesLog()
	{
		return xesLog;
	}

	

	
	public Integer getXesLogType()
	{
		return xesLogType;
	}

	
	public void setXesLogType(Integer xesLogType)
	{
		this.xesLogType = xesLogType;
	}

	
	public Date getCreatingTime()
	{
		return creatingTime;
	}

	
	public void setCreatingTime(Date creatingTime)
	{
		this.creatingTime = creatingTime;
	}

	
	public String getActivitiId()
	{
		return activitiId;
	}

	
	public void setActivitiId(String activitiId)
	{
		this.activitiId = activitiId;
	}

	
	public String getActionClass()
	{
		return actionClass;
	}

	
	public void setActionClass(String actionClass)
	{
		this.actionClass = actionClass;
	}

	
	public Long getCaseId()
	{
		return caseId;
	}

	
	public void setCaseId(Long caseId)
	{
		this.caseId = caseId;
	}

	
	public Long getOfficeFolderId()
	{
		return officeFolderId;
	}

	
	public void setOfficeFolderId(Long officeFolderId)
	{
		this.officeFolderId = officeFolderId;
	}

	
	public byte[] getXesXmlData()
	{
		return xesXmlData;
	}

	
	public void setXesXmlData(byte[] xesXmlData)
	{
		this.xesXmlData = xesXmlData;
	}

	
	public String getActionUri()
	{
		return actionUri;
	}

	
	public void setActionUri(String actionUri)
	{
		this.actionUri = actionUri;
	}

	
}
