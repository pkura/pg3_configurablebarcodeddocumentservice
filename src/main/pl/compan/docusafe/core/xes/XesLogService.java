package pl.compan.docusafe.core.xes;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.xes.XesLog.XesLogType;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *    
 * @author Grzegorz Filip
 * serwis odpowiedzialny za  tworzenie wpis�w logw xesa do XES_LOG  XES_LOG_DATA
 */
public class XesLogService extends ServiceDriver
{
	
	public static final Logger log = LoggerFactory.getLogger(XesLogService.class);
	private static Queue<XesLog> xesEntries = new LinkedList<XesLog>();
	
	public class XesLogTimer extends TimerTask
	{

		@Override
		public void run()
		{
			try
			{
			//	DSApi.openAdmin();
			//	DSApi.context().begin(); 
			log.info("XesLogService Run");
			while (xesEntries.peek()!=null){
				//jest tylk otymczasowym obiektem na podstawie kt�rego powstan� nowe logi badz zaktulizowane zostana istniejace traktowane jest jako obiekt parametr�w 
				XesLog tempLog = xesEntries.poll();

				DSApi.openAdmin();
				DSApi.context().begin(); 
				try
				{ 	tempLog.createOrUpdate();
					//po tej akji sa zbudwane juz prawidlowe obiekty wiec temp nam nie potrzebny 
				if (tempLog.getVersionId()==null)
					DSApi.context().session().delete(tempLog);
				else{
					DSApi.context().session().save(tempLog);	
				}
				
					DSApi.context().session().flush();
				} catch (Exception e)
				{
					log.error("XesLogService - error durnin storing XesLog :"+tempLog.getId(), e);
					//xesEntries.add(xl);
					continue;
					
				}
				DSApi.context().commit();
			}
		//	DSApi.context().commit();
			DSApi.close();
			} catch (EdmException e)
			{
				log.error("", e);
			}

		}

	}

	private Timer timer;
	

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		int delay = 30;
		int period = 2;
		timer.schedule(new XesLogTimer(), delay * DateUtils.SECOND, period  * DateUtils.SECOND );
		log.info("Xes lOG :" + period+ " s.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if (timer != null) {
			timer.cancel();
		}
		
	}

	@Override
	protected boolean canStop()
	{
		// TODO Auto-generated method stub
		return false;
	}

	
	protected static  void addXesLog(XesLog xesLog)
	{
		xesEntries.add(xesLog);
	}

}
