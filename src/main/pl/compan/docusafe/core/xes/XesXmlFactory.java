package pl.compan.docusafe.core.xes;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javassist.bytecode.Descriptor.Iterator;

import javax.persistence.Access;
import javax.servlet.http.HttpServletResponse;

import net.fortuna.ical4j.model.property.Clazz;

import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.io.IOUtils;
import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.extension.XExtensionManager;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.out.XSerializer;
import org.deckfour.xes.out.XesXmlSerializer;
import org.deckfour.xes.xstream.XesXStreamPersistency;
import org.hibernate.HibernateException;
import org.hibernate.type.ByteArrayBlobType;
import org.jfree.util.Log;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.xes.XesLog.XesLogType;
import pl.compan.docusafe.general.mapers.utils.strings.PolishStringUtils;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.common.tag.XmlLink.Link;

import com.bea.xml.stream.reader.XmlReader;
import com.opensymphony.webwork.ServletActionContext;
import com.thoughtworks.xstream.XStream;

/**
 *    
 * @author Grzegorz Filip
 *  Fabryka do tworzeniea reprezntacji xmlowej danej akcji a takze do generowania calego loga 
 */
public class XesXmlFactory
{
	public static XFactory factory = XFactoryRegistry.instance().currentDefault();
	
	public static final char CHARACTERS[] = new char[] {
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
		'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
		'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
	};
	
	private Logger logger = LoggerFactory.getLogger(XesXmlFactory.class);
	/**
	 *  mapa z dwoma kluczami bo wyst�puja w systemie akcje o tej samej nazwie np create  gdzie:
	 * @pierwszym kluczem jest nazwa akcji 
	 * @drugim kluczem jest Uri akcji 
	 * @wartosc jest nazwa metody do wyswietlenia w xesLogu
	 * @return
	 * @throws EdmException
	 */
	private static MultiKeyMap namesMap =  new MultiKeyMap();
	
	private   Map <Integer,Object> iterateMapValues = new HashMap<Integer, Object>();
	private   Map <Integer,String> iterateMapNames = new HashMap<Integer, String>();
	
	/**
	 *  mape z dwoma kluczami bo wyst�puja w systemie akcje o tej samej nazwie np create  gdzie:
	 * @pierwszym kluczem jest nazwa akcji 
	 * @drugim kluczem jest Uri akcji 
	 * @wartosc jest nazwa metody do wykonania dla tej akcji 
	 * @return
	 * @throws EdmException
	 */
	private static  MultiKeyMap mapActionToMethods = new MultiKeyMap();
	
	private final  static String ConName = "concept:name";
	private final static String ConTimeStamp = "time:timestamp";
	private final static  String ConTransition = " lifecycle:transition";

	private static final String XesLogData = null;
	
	private  XAttribute createBooleanXesXmlAttrbute(String name, Boolean value){
		XAttribute attribute;
		attribute = factory.createAttributeBoolean(name, value, null);
		return attribute;
	}
	private  XAttribute createBooleanXesXmlXAttrbute(String name, Boolean value ,XExtension extension){
		XAttribute attribute;
		attribute = factory.createAttributeBoolean(name, value, extension);
		return attribute;
	}
	
	private  XAttribute createDoubleXesXmlXAttrbute(String name, Double value){
		XAttribute attribute;
		attribute = factory.createAttributeContinuous(name, value, null);
		return attribute;
	}
	private  XAttribute createDoubleXesXmlXAttrbute(String name, Double value ,XExtension extension){
		XAttribute attribute;
		attribute = factory.createAttributeContinuous(name, value, extension);
		return attribute;
	}
	
	private  XAttribute createLongXesXmlXAttrbute(String name, Long value){
		XAttribute attribute;
		attribute = factory.createAttributeDiscrete(name, value, null);
		return attribute;
	}
	private  XAttribute createStringXesXmlXAttrbute(String name, String value ){
		XAttribute attribute;
		if(value.isEmpty())
			value ="brak";
		attribute = factory.createAttributeLiteral(name, value, null);
		return attribute;
	}
	
	private  XAttribute createStringXesXmlXAttrbute(String name, String value ,XExtension extension){
		XAttribute attribute;
		attribute = factory.createAttributeLiteral(name, value, extension);
		return attribute;
	}
	private  XAttribute createTimeStampXesXmlXAttrbute(String name, Date date ){
		XAttribute attribute;
		attribute = factory.createAttributeTimestamp(name, date, null);
		return attribute;
	}
	private  XAttribute createTimeStampXesXmlXAttrbute(String name, Date date ,XExtension extension ){
		XAttribute attribute;
		attribute = factory.createAttributeTimestamp(name, date, extension);
		return attribute;
		
	}
	/**
	 * metoda tworzy eventa do usupenienia parametrami
	 * @return
	 */
	public static XEvent createXesXmlEvent() {
		XEvent event = factory.createEvent();
		return event;
	}
	public static XTrace createXesXmlTrace() {
	
		XTrace trace = factory.createTrace();
		return trace;
	}
	public static XLog createXesXmlLog(){
	XLog log = factory.createLog();
	return log;
	}
	
	 /**
	 * METODA ODPOWIEDZIALNA ZA WYGENEROWANIE  CALEGO LOGA AKCJI
	 * Metoda  generuje XesXml-a  z przekazanego obiektu XesLog-a  lub  Id  XesLog-a w zaleznosci od parametru serialize(true/false) serialuzej obiekt i zwraca jako nazwa.xes lub 
	 * nie serializuje i zwraca jako nazwa.xml;
	 * @param xesLog
	 * @param xesLogId
	 * @param serialize
	 * @param isRestRequest 
	 * @throws HibernateException
	 * @throws EdmException
	 * @throws XmlPullParserException
	 * @throws URISyntaxException
	 * @throws FileNotFoundException 
	 */
	public File generateXesXml(XesLog xesLog, Long xesLogId, boolean serialize, boolean isRestRequest) throws HibernateException, EdmException, XmlPullParserException,
			URISyntaxException, FileNotFoundException
	{
		XesLog xl = null;

		if (xesLog != null)
		{
			xl = xesLog;
		} else
		{
			xl = XesLog.findById(xesLogId);
		}
		Set<pl.compan.docusafe.core.xes.XesLogData> xesLogDatasSet = xl.getXesLogsDatas();
		List<XTrace> xTracesList = new ArrayList<XTrace>();
		for (XesLogData xesLogData : xesLogDatasSet)
		{
			if (xesLogData.getXesXmlData() != null)
				xTracesList.add(getXtraceFromXesLogData(xesLogData));
		}

		XLog xLog = createXesXmlLog();
		addDefaultExcenstionsToXLog(xLog);

		for (XTrace xTrace : xTracesList)
			xLog.add(xTrace);

		if (serialize)
			return serializeXesLog(xLog, xesLog, isRestRequest, null);
		else
			return createWitoutSerialize(xLog, xesLog ,null, isRestRequest);
		

	}
	/**
	 *  METODA ODPOWIEDIZALAN ZA UTWORZENIE CALEGO OBIKTU LOGA POSZEGOLNEJ AKCJI
	 * metoda na podstawie obiektu {@link pl.compan.docusafe.core.xes.XesLogData} buduje  Xtrace z  pojedynczej akcji do XesowegoXmla i wrzuca go do bazy.
	 * Struktura xmla jest uzalezniona od okreslonej akcji na obiekcie.
	 *dla podanej akcji wykonuje odpowiednia metod� odpowiadajaca za strukture xml-a  akcji  okreslonej na obiekcie
	 * @param xesLogData
	 */
public  void createXmlData(XesLogData xesLogData) throws EdmException
	{
		namesMap = XesLoggedActionsTypes.getMapedNames();
		preparemethodMap();
		
		if(mapActionToMethods.containsKey(xesLogData.getActionName(), xesLogData.getActionUri())){
		
		try
		{
			Method method = this.getClass().getMethod((String)mapActionToMethods.get(xesLogData.getActionName(),xesLogData.getActionUri()), XesLogData.class);
			method.invoke(this, xesLogData);
			
		} catch (SecurityException e)
		{
			logger.debug("", e);
		} catch (NoSuchMethodException e)
		{
			logger.debug("", e);
		} catch (IllegalArgumentException e)
		{
			
			logger.debug("", e);
		} catch (IllegalAccessException e)
		{
			
			logger.debug("", e);
		} catch (InvocationTargetException e)
		{
			
			logger.debug("", e);
		}
		}
		
	}
 /**
  * metoda tworzy obiekt Xtracea dla akcji logowania urzytkownika do  systemu 
  * @param xesLogData
  * @throws HibernateException
  * @throws EdmException
  * @throws XmlPullParserException
  */
public void createLoginXesXmlData(XesLogData xesLogData) throws HibernateException, EdmException, XmlPullParserException
{

	XTrace xTace = createBasicXtraceElement(xesLogData);
	XEvent xEvent = createXesXmlEvent();
	addDefaultXesValues(xesLogData, xEvent);
	addDefaultXattribute(xEvent, xesLogData);
	xTace.add(xEvent);
	storeXmlData(xTace, xesLogData);
}

 /**
  * metoda tworzy obiekt Xtracea dla akcji wylogowania urzytkownika z  systemu 
  * @param xesLogData
  * @throws HibernateException
  * @throws EdmException
  * @throws XmlPullParserException
  */

public  void createLogoutXesXmlData (XesLogData xesLogData) throws HibernateException, EdmException, XmlPullParserException{
	 createLoginXesXmlData(xesLogData);
 }
/**
 * metoda tworzy obiekt Xtracea dla akcji dekretacji dokumentu  
 * @param xesLogData
 * @throws HibernateException
 * @throws EdmException
 * @throws XmlPullParserException
 */
public void createXesXmlForAssignemntDocument(XesLogData xesLogData) throws HibernateException, EdmException, XmlPullParserException
{
	XTrace xTace = createBasicXtraceElement(xesLogData);
	XEvent xEvent = createXesXmlEvent();
	addDefaultXesValues(xesLogData, xEvent);
	addDecretationAtributes(xEvent, xesLogData);
	addDefaultXattribute(xEvent, xesLogData);
	xTace.add(xEvent);
	storeXmlData(xTace, xesLogData);
}
/**
 * metoda tworzy obiekt Xtracea dla akcji odebrania (akceptacji )dekretacji dokumentu  
 * @param xesLogData
 * @throws HibernateException
 * @throws EdmException
 * @throws XmlPullParserException
 */
public void createXesXmlForRecivingDecretation(XesLogData xesLogData) throws HibernateException, EdmException, XmlPullParserException
{
	XTrace xTace = createBasicXtraceElement(xesLogData);
	XEvent xEvent = createXesXmlEvent();
	addDefaultXesValues(xesLogData, xEvent);
	addDecretationRecivedAtributes(xEvent, xesLogData);
	//addDecretationAtributes(xEvent, xesLogData);
	addDefaultXattribute(xEvent, xesLogData);
	xTace.add(xEvent);
	storeXmlData(xTace, xesLogData);
}

public void createXesXmlForUpdateDocument(XesLogData xesLogData) throws HibernateException, EdmException, XmlPullParserException
{
	createXesXmlForCreatingDocument(xesLogData);
}
public void createXesXmlForCreatingDocument(XesLogData xesLogData) throws HibernateException, EdmException, XmlPullParserException
{
	XTrace xTace = createBasicXtraceElement(xesLogData);
	XEvent xEvent = createXesXmlEvent();
	addDefaultXesValues(xesLogData, xEvent);
	addValuesFromDockind(xesLogData, xEvent);
	addDefaultXattribute(xEvent, xesLogData);
	xTace.add(xEvent);
	storeXmlData(xTace, xesLogData);

}

public void createXesXmlForAddAttachment(XesLogData xesLogData) throws HibernateException, EdmException, XmlPullParserException
{

	XTrace xTace = createBasicXtraceElement(xesLogData);
	XEvent xEvent = createXesXmlEvent();
	addDefaultXesValues(xesLogData, xEvent);
	//addValuesFromDockind(xesLogData, xEvent);
	addDefaultXattribute(xEvent, xesLogData);
	xTace.add(xEvent);
	storeXmlData(xTace, xesLogData);

}
public void createXesXmlForDeleteAttachment(XesLogData xesLogData) throws HibernateException, EdmException, XmlPullParserException
{
	XTrace xTace = createBasicXtraceElement(xesLogData);
	XEvent xEvent = createXesXmlEvent();
	addDefaultXesValues(xesLogData, xEvent);
	//addValuesFromDockind(xesLogData, xEvent);
	addDefaultXattribute(xEvent, xesLogData);
	xTace.add(xEvent);
	storeXmlData(xTace, xesLogData);
}
public void createXesXmlForAddToCaseDocument(XesLogData xesLogData) throws HibernateException, EdmException, XmlPullParserException
{
	XTrace xTace = createBasicXtraceElement(xesLogData);
	XEvent xEvent = createXesXmlEvent();
	addDefaultXesValues(xesLogData, xEvent);
	addValuesFromCase(xesLogData, xEvent);
	//addValuesFromDockind(xesLogData, xEvent);
	addDefaultXattribute(xEvent, xesLogData);
	xTace.add(xEvent);
	storeXmlData(xTace, xesLogData);
}
public void createXesXmlForRemoveFromCaseDocument(XesLogData xesLogData) throws HibernateException, EdmException, XmlPullParserException
{
	XTrace xTace = createBasicXtraceElement(xesLogData);
	XEvent xEvent = createXesXmlEvent();
	addDefaultXesValues(xesLogData, xEvent);
	addValuesFromCase(xesLogData, xEvent);
	addDefaultXattribute(xEvent, xesLogData);
	//addValuesFromDockind(xesLogData, xEvent);

	xTace.add(xEvent);
	storeXmlData(xTace, xesLogData);
}

    public File generateXesXmlByDocumentId(Long documentId, boolean isRestRequest) throws EdmException, FileNotFoundException, XmlPullParserException, URISyntaxException {
        XesLog xesLog = XesLog.findByDocumentId(documentId);
        return generateXesXml(xesLog,xesLog.getId(), true, isRestRequest);
    }

    public File generateXesXmlByUserAndDate(Long userId, Date time, boolean isRestRequest) throws EdmException, FileNotFoundException, XmlPullParserException, URISyntaxException {
        XesLog xesLog = XesLog.findByUserIdAndDate(userId, time);
        return generateXesXml(xesLog,xesLog.getId(), true, isRestRequest);
    }

    public File generateXesXmlByUserAndDate(Long userId, Date starttime, Date endtime, boolean isRestRequest) throws EdmException, FileNotFoundException, XmlPullParserException, URISyntaxException {
        return XesLog.findByUserIdAndDate(userId, starttime, endtime, isRestRequest);
    }

    /**
	 * Metoda serializuje utworony Log  zapisuje do katalogu home Aplikacji  oraz streamuje do uzytkownika 
	 * @param log
	 * @param xesLog
     * @param isRestRequest 
     * @param opcjonalnie nazwa pliku
	 */
	protected File serializeXesLog(XLog xLog, XesLog xesLog, boolean isRestRequest , String fileName)
	{

		XStream xstream = new XStream();
		XesXStreamPersistency.register(xstream);

		
		File sFile = new File(prepareFilePathToXesLog(xesLog , true ,fileName));
		if (sFile.exists())
		{
			sFile.delete();
		}
		try
		{
			sFile.createNewFile();
		} catch (IOException e1)
		{
			 logger.error("", e1);
		}
		try
		{
			OutputStream oStream = new BufferedOutputStream(new FileOutputStream(sFile));
			
			XSerializer logSerializer = new XesXmlSerializer();
			logSerializer.serialize(xLog, oStream);
			oStream.close();
		} catch (IOException e)
		{
			logger.error("", e);
		}

		if(!isRestRequest)
		streamFile(sFile);

        return sFile;
	}

	private void streamFile(File sFile)
	{
		  try
          {
              ServletUtils.streamFile(ServletActionContext.getResponse(), sFile, "application/xml", "Content-Disposition: attachment; filename=\""+sFile.getName()+"\"");
          }
          catch (IOException e)
          {
              logger.error("", e);
          }
		
	}
	/**
	 * Metoda przygotowuje  plik xesLoga  ktoru zostanie zapisany w katalogu home/xes/ 
	 * @param serializeObject 
	 * @param fileName 
	 * 
	 * @return
	 */
	private String prepareFilePathToXesLog(XesLog xl, boolean serializeObject, String fileName)

	{
		StringBuilder path = new StringBuilder();
		path.append(Docusafe.getHome().getAbsolutePath() + "/xes/");
		new File(path.toString()).mkdirs();

		if(xl == null && fileName !=null && !fileName.isEmpty())
		{
			path.append(fileName);
		}
		else if (xl == null ){
			path.append("XesLog-random-"+getRandomName());
		}
		
		else if (xl.getXesLogType().equals(XesLogType.DOCUMENT))
		{
			path.append(xl.getXesLogType().name());
			path.append("_ID_" + xl.getDocumentId());
		}

		else if (xl.getXesLogType().equals(XesLogType.USER))
		{
			path.append(xl.getXesLogType().name());
			path.append("_DATA_" + new DateUtils().formatCommonDateTimeWithoutWhiteSpaces(xl.getCreatingTime()));
		}
		
		if (serializeObject)
			path.append(".xes");
		else
			path.append(".xml");
		return path.toString();



	}

	
/**
 * Metoda NIE SERIALIZUJE  utworonego Log-a -  zapisuje do katalogu home Aplikacji oraz streamuje do uzytkownika 
 * @param xLog 
 * @param xesLog
 * @param fileName 
 */
	private File createWitoutSerialize(XLog xLog, XesLog xesLog, String fileName, boolean isRestRequest)
	{
		File sFile = new File(prepareFilePathToXesLog(xesLog, false, fileName));
		if (sFile.exists())
		{
			sFile.delete();
		}
		try
		{
			sFile.createNewFile();
		} catch (IOException e1)
		{
			logger.error("", e1);
		}
		try
		{
			OutputStream oStream;
			oStream = new BufferedOutputStream(new FileOutputStream(sFile));
			XStream xstream = new XStream();
			XesXStreamPersistency.register(xstream);
			xstream.toXML(xLog, oStream);

			
		} catch (FileNotFoundException e)
		{
			logger.error("", e);
		}

        if(!isRestRequest) {
            streamFile(sFile);
        }

        return sFile;

	}
	
	private String getRandomName()
	{
	char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
	StringBuilder sb = new StringBuilder();
	Random random = new Random();
	for (int i = 0; i < 20; i++) {
	    char c = chars[random.nextInt(chars.length)];
	    sb.append(c);
	}
	return sb.toString();
	}
	/**
	 * Metoda zwraca zapisanego Xtraca z obiektu  XesLogData
	 * @param xesLogData
	 * @return
	 * @throws HibernateException
	 * @throws EdmException
	 */
	protected XTrace getXtraceFromXesLogData(XesLogData xesLogData) throws HibernateException, EdmException
	{

		XStream xstream = new XStream();
		XesXStreamPersistency.register(xstream);
		ByteArrayInputStream bis = new ByteArrayInputStream(xesLogData.getXesXmlData());

		XTrace xTrace = (XTrace) xstream.fromXML(bis);

		return xTrace;

	}
	
	protected  XExtension getExtLifcycle() throws URISyntaxException{
		URI uri = new URI("http://www.xes-standard.org/lifecycle.xesext");
		return XExtensionManager.instance().getByUri(uri);
		
	
	}
	protected  XExtension getExtOrganizational() throws URISyntaxException{
		URI uri = new URI("http://www.xes-standard.org/org.xesext");
		return XExtensionManager.instance().getByUri(uri);
		
	
	}
	 protected  XExtension getExtTime() throws URISyntaxException{
		URI uri = new URI("http://www.xes-standard.org/time.xesext");
		return XExtensionManager.instance().getByUri(uri);
		
	
	}
	 protected  XExtension getExtConcept() throws URISyntaxException{
		URI uri = new URI("http://www.xes-standard.org/concept.xesext");
		return XExtensionManager.instance().getByUri(uri);
		
	
	}
	 protected  XExtension getExtSemantic() throws URISyntaxException{
		URI uri = new URI("http://www.xes-standard.org/semantic.xesext");
		return XExtensionManager.instance().getByUri(uri);
		
	
	}
	 protected void addDefaultExcenstionsToXLog(XLog log) throws URISyntaxException{
		
		log.getExtensions().add(getExtConcept());
		log.getExtensions().add(getExtLifcycle());
		log.getExtensions().add(getExtSemantic());
		log.getExtensions().add(getExtOrganizational());
		log.getExtensions().add(getExtTime());
	}
	/**
	 * Mtoda zapisuje do bazy wygenerowanego Xtraca z Akcji
	 * i dodaje go do obeiktu XesLogData
	 * @param trace
	 * @param xesLogData
	 * @throws HibernateException
	 * @throws EdmException
	 * @throws XmlPullParserException
	 */
	 private static void storeXmlData(XTrace xTrace, XesLogData xesLogData) throws HibernateException, EdmException, XmlPullParserException
	{
		XStream xstream = new XStream();
		//XmlPullParserFactory parser = XmlPullParserFactory.newInstance();

		XesXStreamPersistency.register(xstream);
		byte[] bytes = xstream.toXML(xTrace).getBytes();
		xesLogData.setXesXmlData(bytes);
		DSApi.context().session().save(xesLogData);
	}
	
	
	/**
	 * jezeli log tyczy sie dokumentu   metoda dodaje wartosci z dokinda  do logu
	 * @param xesLogData
	 * @param xEvent
	 * @throws EdmException
	 * @throws DocumentNotFoundException
	 */
	private void addValuesFromDockind(XesLogData xesLogData, XEvent xEvent) throws EdmException, DocumentNotFoundException
	{
		OfficeDocument doc;
		if (xesLogData.getXesLogType().equals(XesLogType.DOCUMENT.ordinal()))
		{
			doc = OfficeDocument.findOfficeDocument(xesLogData.getDocumentId(), false);
			addDokindValuesFromDocument(xEvent, doc);
		}
	}
	/**
	 * Metoda tworzy nowy obiekt xtraca  o nazwie wykoanej akcji w kt�rym bedzie sie znajdowac event z parametrami  akcji 
	 * @param xesLogData
	 * @return
	 */
	private XTrace createBasicXtraceElement(XesLogData xesLogData)
	{
		XTrace xTace = createXesXmlTrace();
		XAttribute name = createStringXesXmlXAttrbute(ConName, (String)namesMap.get(xesLogData.getActionName(),xesLogData.getActionUri()));
		xTace.getAttributes().put(name.getKey(), name);
		return xTace;
	}
	/**
	 * Metoda dodaje parametry do loga o dekretacji : osobe na kt�ra jest dekretacja i jej externalname lub w przypadku dekretacji na dzial
	 *  nazwe dzialu
	 * @param xEvent
	 * @param xesLogData
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
		private void addDecretationAtributes(XEvent xEvent, XesLogData xesLogData) throws UserNotFoundException, EdmException
		{
			XAttribute childAtt = null;

			OfficeDocument doc;
				doc = OfficeDocument.findOfficeDocument(xesLogData.getDocumentId(), false);
				if (doc != null && doc.getCurrentAssignmentUsername() != null)
				{
					DSUser candidate = DSUser.findByUsername(doc.getCurrentAssignmentUsername());
					DSUser sender = DSUser.findById(xesLogData.getUserId());
					childAtt = createStringXesXmlXAttrbute("Dekretacja na", replaceAllPoolishSymbols(candidate.getLastnameFirstnameWithOptionalIdentifier()));
					xEvent.getAttributes().put(childAtt.getKey(), childAtt);
					childAtt = createStringXesXmlXAttrbute("sender-user-firstname", replaceAllPoolishSymbols(sender.getFirstname()));
					xEvent.getAttributes().put(childAtt.getKey(), childAtt);
					childAtt = createStringXesXmlXAttrbute("sender-user-lastname", replaceAllPoolishSymbols(sender.getLastname()));
					xEvent.getAttributes().put(childAtt.getKey(), childAtt);
					childAtt = createStringXesXmlXAttrbute("sender-user-external-name", replaceAllPoolishSymbols(sender.getExternalName()));
					xEvent.getAttributes().put(childAtt.getKey(), childAtt);
					childAtt = createStringXesXmlXAttrbute("recipient-candidate-firstname", replaceAllPoolishSymbols(candidate.getFirstname()));
					xEvent.getAttributes().put(childAtt.getKey(), childAtt);
					childAtt = createStringXesXmlXAttrbute("recipient-candidate-lastname", replaceAllPoolishSymbols(candidate.getLastname()));
					xEvent.getAttributes().put(childAtt.getKey(), childAtt);
					childAtt = createStringXesXmlXAttrbute("recipient-candidate-external-name", replaceAllPoolishSymbols(candidate.getExternalName()));
					xEvent.getAttributes().put(childAtt.getKey(), childAtt);
				} else if (doc != null && doc.getCurrentAssignmentGuid() != null)
				{
					childAtt = createStringXesXmlXAttrbute("Dekretacja na dzial", replaceAllPoolishSymbols(DSDivision.find(doc.getCurrentAssignmentGuid()).getName()));
				}
			
		}
		
	/**
	 * Metoda dodaje parametry do loga o odbiorze dekretacji przez osobe na kt�ra jest dekretacja i jej externalname
	 * @param xEvent
	 * @param xesLogData
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	private void addDecretationRecivedAtributes(XEvent xEvent, XesLogData xesLogData) throws UserNotFoundException, EdmException
	{

		XAttribute childAtt = null;

		OfficeDocument doc;

		doc = OfficeDocument.findOfficeDocument(xesLogData.getDocumentId(), false);
		if (doc != null && doc.getCurrentAssignmentUsername() != null)
		{
			DSUser sender = DSUser.findByUsername(doc.getAssignmentHistoryDesc().get(0).getSourceUser());
			DSUser recipient = DSUser.findByUsername(doc.getCurrentAssignmentUsername());
			childAtt = createStringXesXmlXAttrbute("Dekretacja od:", replaceAllPoolishSymbols(sender.getLastnameFirstnameWithOptionalIdentifier()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
			childAtt = createStringXesXmlXAttrbute("sender-user-firstname", replaceAllPoolishSymbols(sender.getFirstname()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
			childAtt = createStringXesXmlXAttrbute("sender-user-lastname", replaceAllPoolishSymbols(sender.getLastname()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
			childAtt = createStringXesXmlXAttrbute("sender-user-external-name", replaceAllPoolishSymbols(sender.getExternalName()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
			childAtt = createStringXesXmlXAttrbute("Zostala odebrana przez", replaceAllPoolishSymbols(recipient.getFirstnameLastnameName()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
			childAtt = createStringXesXmlXAttrbute("recipient-candidate-firstname", replaceAllPoolishSymbols(recipient.getFirstname()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
			childAtt = createStringXesXmlXAttrbute("recipient-candidate-lastname", replaceAllPoolishSymbols(recipient.getLastname()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
			childAtt = createStringXesXmlXAttrbute("recipient-candidate-external-name", replaceAllPoolishSymbols(recipient.getExternalName()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
		}

	}
	/**
	 * metoda dodaje domyslne wartosci wystepujace na kazdym typie xes loga takie jak :
	 * data wykonania / nazwa akcji / id dokumentu / osoba wykonujaca i jej external name  
	 * @param xesLogData
	 * @param xEvent
	 * @throws EdmException
	 * @throws UserNotFoundException
	 */
	private void addDefaultXesValues(XesLogData xesLogData, XEvent xEvent) throws EdmException, UserNotFoundException
	{
		iterateMapValues = new HashMap<Integer, Object>();
		iterateMapNames = new HashMap<Integer, String>();
		iterateMapNames.put(1, "Data wykonania");
		iterateMapValues.put(1, xesLogData.getActionTime());
		iterateMapNames.put(2, "Nazwa akcji");
		iterateMapValues.put(2, xesLogData.getActionName());

		if (xesLogData.getDocumentId() != null)
		{
			iterateMapValues.put(3, xesLogData.getDocumentId());
			iterateMapNames.put(3, "Document-ID");
		}

		if (xesLogData.getXesLogType().equals(XesLogType.DOCUMENT.ordinal()))
		{ DSUser user = DSUser.findById(xesLogData.getUserId());
			iterateMapNames.put(4, "user-firstname ");
			iterateMapValues.put(4, replaceAllPoolishSymbols(user.getFirstname()));
			iterateMapNames.put(5, "user-lastname");
			iterateMapValues.put(5, replaceAllPoolishSymbols(user.getLastname()));
			iterateMapNames.put(6, "user-external-name");
			iterateMapValues.put(6, replaceAllPoolishSymbols(user.getExternalName()));

		}

		for (Integer i : iterateMapValues.keySet())
		{
			Object o = iterateMapValues.get(i);
			XAttribute childAtt = null;

			if (o instanceof Date)
				childAtt = createTimeStampXesXmlXAttrbute(iterateMapNames.get(i), (Date) o);
			else if (o instanceof String)
				childAtt = createStringXesXmlXAttrbute(iterateMapNames.get(i), (String) o);
			else if (o instanceof Long)
				childAtt = createLongXesXmlXAttrbute(iterateMapNames.get(i), (Long) o);

			xEvent.getAttributes().put(childAtt.getKey(), childAtt);

		}
	}
	/**
	 * metoda dodaje  wartosci ze sprawy takie jak : symbol / nazwa / opis / opis rwa teczka /  dzial teczki
	 * @param xesLogData
	 * @param xEvent
	 * @throws EdmException
	 */
	private void addValuesFromCase(XesLogData xesLogData, XEvent xEvent) throws EdmException
	{
		if (xesLogData.getCaseId() != null)
		{
			OfficeCase c = OfficeCase.find(xesLogData.getCaseId());
			XAttribute childAtt = null;
			childAtt = createStringXesXmlXAttrbute("Symbol sprawy", c.getOfficeId());
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
			childAtt = createStringXesXmlXAttrbute("Nazwa sprawy", replaceAllPoolishSymbols(c.getTitle()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
			childAtt = createStringXesXmlXAttrbute("Opis sprawy", replaceAllPoolishSymbols(c.getDescription()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
			childAtt = createStringXesXmlXAttrbute("Opis teczki", replaceAllPoolishSymbols(c.getRwa().getDescription()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
			childAtt = createStringXesXmlXAttrbute("Dzial teczki ", replaceAllPoolishSymbols(DSDivision.find(c.getDivisionGuid()).getName()));
			xEvent.getAttributes().put(childAtt.getKey(), childAtt);
		}
	}
/**
 * metoda wrzuca do loga wartosci z dokinda 
 * @param xEvent
 * @param doc
 * @throws EdmException
 */
	protected void addDokindValuesFromDocument(XEvent xEvent, OfficeDocument doc) throws EdmException
	{
		Map<String, Object> mapFieldsValues = new HashMap<String, Object>();
		Map<String, Field> mapFields = doc.getDocumentKind().getFieldsMap();
		Map<String, String> mapFieldsNAmes = new HashMap<String, String>();
		boolean supported = true;

		for (String key : mapFields.keySet())
		{
			mapFieldsNAmes.put(mapFields.get(key).getCn(), mapFields.get(key).getName());
		}
		mapFieldsValues = doc.getFieldsManager().getFieldValues();
		for (String key : mapFieldsValues.keySet())
		{ 
			supported = true;
			XAttribute childAtt = null;
			Object o = mapFieldsValues.get(key);
			if (o == null)
				supported = false;
			else if (o instanceof Date)
				childAtt = createTimeStampXesXmlXAttrbute(replaceAllPoolishSymbols(mapFieldsNAmes.get(key)), (Date) o);
			else if (o instanceof String)
				childAtt = createStringXesXmlXAttrbute(replaceAllPoolishSymbols(mapFieldsNAmes.get(key)), (String) o);
			else if (o instanceof Long)
				childAtt = createLongXesXmlXAttrbute(replaceAllPoolishSymbols(mapFieldsNAmes.get(key)), (Long) o);
			else if (o instanceof Double)
				childAtt = createDoubleXesXmlXAttrbute(replaceAllPoolishSymbols(mapFieldsNAmes.get(key)), (Double) o);
			else if (o instanceof Boolean)
				childAtt = createBooleanXesXmlAttrbute(replaceAllPoolishSymbols(mapFieldsNAmes.get(key)), (Boolean) o);
			
			else
				supported = false;
			if (supported)
				xEvent.getAttributes().put(childAtt.getKey(), childAtt);
		}
		
	}

	
	private String  replaceAllPoolishSymbols(String name){
	PolishStringUtils psu = new PolishStringUtils();
	return	psu.zamienPolskieZnaki(name);
		
		
	}
	/**
	 * metoda tworzy defaultowy komunikat o sukciesie wykonanej akcji
	 * @param xEvent
	 */
	private void addDefaultXattribute(XEvent xEvent , XesLogData xesLogData)
	{
		XAttribute childAtt = null;
		
		childAtt = createStringXesXmlXAttrbute(ConTransition, "complete");
		xEvent.getAttributes().put(childAtt.getKey(), childAtt);
		XAttribute att = createTimeStampXesXmlXAttrbute(ConTimeStamp, xesLogData.getActionTime());
		xEvent.getAttributes().put(att.getKey(), att);
		
	}
	

	protected static  void preparemethodMap() throws EdmException
	{
		mapActionToMethods =  XesLoggedActionsTypes.getMultiKeyMap();
		
	}


    public String getXesLogBase64Document(Long docId, boolean isRestRequest) throws EdmException{
        try{
            File f = generateXesXmlByDocumentId(docId, isRestRequest);
            return FileUtils.encodeByBase64(f);
        }catch(Exception e){
            logger.error("", e);
            throw new EdmException(e);
        }
    }

    public String getXesLogBase64User(Long userId, Date date, boolean isRestRequest) throws EdmException{
        try{
            File f = generateXesXmlByUserAndDate(userId, date, isRestRequest);
            return FileUtils.encodeByBase64(f);
        }catch(Exception e){
            logger.error("", e);
            throw new EdmException(e);
        }
    }

    public String getXesLogBase64User(Long userId, Date startdate, Date enddate, boolean isRestRequest) throws EdmException{
        try{
            File f = XesLog.findByUserIdAndDate(userId, startdate, enddate, isRestRequest);
            return FileUtils.encodeByBase64(f);
        }catch(Exception e){
            logger.error("", e);
            throw new EdmException(e.getMessage());
        }
    }

	public File generateXesXmlFromResults(List<XesLog> xesLogSesult, boolean serialize, String fileName, Boolean isRequest) throws URISyntaxException, HibernateException,
			EdmException
	{
		XLog xLog = createXesXmlLog();
		addDefaultExcenstionsToXLog(xLog);

		for (XesLog xl : xesLogSesult)
		{

			Set<pl.compan.docusafe.core.xes.XesLogData> xesLogDatasSet = xl.getXesLogsDatas();
			List<XTrace> xTracesList = new ArrayList<XTrace>();
			for (XesLogData xesLogData : xesLogDatasSet)
			{
				if (xesLogData.getXesXmlData() != null)
					xTracesList.add(getXtraceFromXesLogData(xesLogData));

			}

			for (XTrace xTrace : xTracesList)
				xLog.add(xTrace);
		}



		if (serialize)
			return serializeXesLog(xLog, null, isRequest, fileName);
		else
			return createWitoutSerialize(xLog, null, fileName, isRequest);
	}
}
