package pl.compan.docusafe.core.xes;


import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import nu.hoohoo.mine.tabs.Tab;
import nu.hoohoo.mine.tabs.TabsTag;







//import org.activiti.engine.repository.ProcessDefinition;
//import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.xmlpull.v1.XmlPullParserException;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.xes.XesLog.XesLogType;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;


/**
 *    
 * @author Grzegorz Filip
 *  Klasa obslugujaca zdazenia na zakladce administracja xesLog  
 */
public class XesLogAction extends EventActionSupport
{
	private StringManager sm = GlobalPreferences.loadPropertiesFile("", null);
	private Logger logger = LoggerFactory.getLogger(XesLogAction.class);
	private static final String FORWARD = "/admin/xes-log.action";
	private int fromIndex;
	private int toIndex;
	private int LIMIT = 20;
	int offset;
	private Pager pager;
	private String cdateFrom;
	private String cdateTo;
	private Integer kind;
	private Map<Integer, String> kinds;
	private String docKind;
	private Map<String, String> docKinds;
	private String[] selectedUsers;
	private String[] assignedUser;
	private Long xesLogId;
	private boolean isSearchResults;
	private boolean serialize;
	
	// @SETINGS
	private Map<String, Boolean> setingsMap = new HashMap<String, Boolean>();
	private boolean doAddToNextCase ;
	private boolean doAddToCase;
	private boolean doWyslijDoEpuap;
	private boolean doSaveAndAssignOfficeNumber;
	private boolean doUpdateDocument;
	private boolean doCreateCase;
	private boolean doRemoveFromCase;
	private boolean doDeleteAttachment;
	private boolean doAddAttachment;
	private boolean logout;
	private boolean login;
	private boolean doAcceptDecretation;
	private boolean doCreateDocument;
	private boolean doAssign;
	
		
	private List<DSUser> users;


	List<DynaBean> beans;

	@Override
	protected void setup()
	{
		FillForm fillForm = new FillForm();


		registerListener(DEFAULT_ACTION)
		.append(OpenHibernateSession.INSTANCE)
		.append(new IsSearchResults())
		.append(new FillForm())
		.appendFinally(CloseHibernateSession.INSTANCE);
		
		
		registerListener("doSearch")
		.append(OpenHibernateSession.INSTANCE)
		.append(new Search())
		.append(new FillForm())
		.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSaveSetings")
		.append(OpenHibernateSession.INSTANCE)
		.append(new SaveSetings())
		.append(new FillForm())
		.appendFinally(CloseHibernateSession.INSTANCE);

		
		registerListener("doGetXesLog")
		.append(OpenHibernateSession.INSTANCE)
		.append(new GetXesLog())
		.append(new Search())
		.append(new FillForm())
		.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doGeneretefromToDateforUser")
		.append(OpenHibernateSession.INSTANCE)
		.append(new GetXesLogFromDates())
		.append(new Search())
		.append(new FillForm())
		.appendFinally(CloseHibernateSession.INSTANCE);
		
	}



	private class FillForm implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{

			try
			{
				
				boolean opened = DSApi.openContextIfNeeded();
				
				kinds  = new LinkedHashMap<Integer, String>(2);
				kinds.put(XesLogType.DOCUMENT.ordinal(), "Dokument");
				kinds.put(XesLogType.USER.ordinal(), "Uzytkownik");
				users = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);

				if (!isSearchResults)
				{
					List<XesLog> xesLogList = getXesLogsFromToday();

					beans = new ArrayList<DynaBean>();
					beans.addAll(generateBeans(xesLogList));
					ServletActionContext.getRequest().setAttribute("beans", beans);
					
				}		
				DSApi.closeContextIfNeeded(opened);
			} catch (EdmException e)
			{
				logger.error("", e);
			}

			// zak�adki (XesLog, UStawienia)

			    Tab[] tabs = null;


			 	tabs = new Tab[2];
			 	tabs[0] = new Tab("xesloglist", ServletActionContext.getRequest().getContextPath()+FORWARD+"?&tabId=xesloglist","Xes Log");
			 	tabs[1] = new Tab("xessetings", ServletActionContext.getRequest().getContextPath()+FORWARD+"?&tabId=xessetings","Ustawienia");
		     

			  ServletActionContext.getRequest().setAttribute(TabsTag.KEY, tabs);
			
			  setupSetings();
			 
		}

	}
	private class GetXesLog implements ActionListener {
		public void actionPerformed( ActionEvent event) {
			try{
			
			if(xesLogId !=null){
				
				generateXmlbyXesLogId(xesLogId,serialize);
			}
			else {
				addActionError("Nie znaleziono  XesLog id");
			}
			
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			} catch (HibernateException e)
			{
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			} catch (XmlPullParserException e)
			{
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			} catch (URISyntaxException e)
			{
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			} catch (FileNotFoundException e)
			{
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			}
		}
	}
	private class GetXesLogFromDates implements ActionListener {
		public void actionPerformed( ActionEvent event) {
			try{
				
				generateXmlFromDates(event, cdateFrom,cdateTo,serialize);
			
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			} catch (HibernateException e)
			{
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			} catch (XmlPullParserException e)
			{
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			} catch (URISyntaxException e)
			{
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			} catch (FileNotFoundException e)
			{
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			} catch (Exception e)
			{
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			}
		}
	}
	private class SaveSetings implements ActionListener
	{

		public void actionPerformed(final ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				boolean opened = DSApi.openContextIfNeeded();
				updateSetings();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(opened);
			} catch (EdmException e)
			{
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			} catch (Exception e)
			{
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			}
		}
	}
	
	private class Search implements ActionListener {
		public void actionPerformed(final ActionEvent event) {

			try
			{	
				boolean opened = DSApi.openContextIfNeeded();

				Criteria criteria = DSApi.context().session().createCriteria(XesLog.class);
				if (kind != null)
					criteria.add(Restrictions.eq("xesLogType", kind));
				if (cdateFrom != null && !cdateFrom.isEmpty())
				{
					criteria.add(Restrictions.ge("creatingTime", DateUtils.nullSafeParseJsDate(cdateFrom)));
				}
				if (cdateTo != null && !cdateTo.isEmpty())
					criteria.add(Restrictions.le("creatingTime", DateUtils.nullSafeParseJsDate(cdateTo)));
				if (selectedUsers!=null && selectedUsers.length > 0)
				{
					Disjunction or = Restrictions.disjunction();
					for (String userSelected : selectedUsers)
					{
						or.add(Restrictions.eq("userId",DSUser.findByUsername(userSelected).getId()));
					}
					criteria.add(or);
				}
				criteria.addOrder(Order.desc("id"));

				List<XesLog> xesLogSesult = criteria.list();
				List<XesLog> xesLogs = new ArrayList<XesLog>();

				if (xesLogs.size() > LIMIT)

					xesLogs = xesLogSesult.subList(offset, toIndex);

				if (offset + LIMIT > xesLogs.size())
					xesLogs = xesLogSesult.subList(offset, xesLogSesult.size());

				else
					xesLogs = xesLogSesult.subList(offset, offset + LIMIT);
				
				
	               /*Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
	                 {
							public String getLink(int offset) {
								return HttpUtils.makeUrl(
										"/admin/xes-log.action",
										new Object[] { "doSearch", "true",
												 "kind", kind,
												"title", title, "archDateFrom",
												archDateFrom, "archDateTo",
												archDateTo, "cdateFrom", cdateFrom,
												"cdateTo", cdateTo, "selectedUsers",
												selectedUsers,  "sortField",
												sortField,
												"ascending",
												String.valueOf(ascending), "popup",
												popup ? "true" : "false", "offset",
												String.valueOf(offset) });
							}
						};
	                    pager = new Pager(linkVisitor, offset, LIMIT, xesLogs.size(), 10);
	                */
	              if(!xesLogs.isEmpty()){
	    					
	    			beans = new ArrayList<DynaBean>();
	    			beans.addAll(generateBeans(xesLogs));
	    			ServletActionContext.getRequest().setAttribute("beans", beans);
	              }   
	              isSearchResults = true;
	              DSApi.closeContextIfNeeded(opened);
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
				addActionError(e.getMessage());
			}

		}
	}
	private class IsSearchResults implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event)
		{
			XesLogAction.this.isSearchResults = false;
		}
	}
	/**
	 * Metoda aktualizuje  ustawienia logowania  dla akcji okrezlonych przez uzytkownika 
	 */
	private void updateSetings()
	{

		try
		{
			putSelectedValuesToMap();
			List<XesLoggedActionsTypes> list = getListSetingsFromBase();
			for (XesLoggedActionsTypes xlat : list)
			{
				xlat.setLogThisAction(this.setingsMap.get(xlat.getSettingName()));
				DSApi.context().session().update(xlat);
			}

		} catch (EdmException e)
		{
			logger.debug(e);
		}
	}
	/**
	 * metoda Uzupelnia mape logowania akcji  wedlug okreslen przez uzytkownika
	 */
	private void putSelectedValuesToMap()
{
		setingsMap.clear();
		setingsMap.put("doAddToNextCase", this.doAddToNextCase);
		setingsMap.put("doAddToCase", this.doAddToCase);
		setingsMap.put("doWyslijDoEpuap", this.doWyslijDoEpuap);
		setingsMap.put("doSaveAndAssignOfficeNumber", this.doSaveAndAssignOfficeNumber);
		setingsMap.put("doUpdateDocument", this.doUpdateDocument);
		setingsMap.put("doCreateCase", this.doCreateCase);
		setingsMap.put("doRemoveFromCase", this.doRemoveFromCase);
		setingsMap.put("doDeleteAttachment", this.doDeleteAttachment);
		setingsMap.put("doAddAttachment", this.doAddAttachment);
		setingsMap.put("logout", this.logout);
		setingsMap.put("login", this.login);
		setingsMap.put("doAcceptDecretation", this.doAcceptDecretation);
		setingsMap.put("doCreateDocument", this.doCreateDocument);
		setingsMap.put("doAssign", this.doAssign);
		
}
	/**
	 * Metoda uzupelnia zakladke ustawienia danymi 
	 */
	private void setupSetings()
	{
		
		try
		{
			List<XesLoggedActionsTypes> list = getListSetingsFromBase();
		if (!list.isEmpty())
			for (XesLoggedActionsTypes xlat : list){
				setingsMap.put(xlat.getSettingName(), xlat.getLogThisAction());
			}
		} catch (EdmException e)
		{
			logger.debug(e);
		}
		this.doAddToNextCase = setingsMap.get("doAddToNextCase");
		this.doAddToCase =   setingsMap.get("doAddToCase");
		this.doWyslijDoEpuap =   setingsMap.get("doWyslijDoEpuap");
		this.doSaveAndAssignOfficeNumber =  setingsMap.get("doSaveAndAssignOfficeNumber");
		this.doUpdateDocument =   setingsMap.get("doUpdateDocument");
		this.doCreateCase =   setingsMap.get("doCreateCase");
		this.doRemoveFromCase =   setingsMap.get("doRemoveFromCase");
		this.doDeleteAttachment =   setingsMap.get("doDeleteAttachment");
		this.doAddAttachment =   setingsMap.get("doAddAttachment");
		this.logout =  setingsMap.get("logout");
		this.login =  setingsMap.get("login");
		this.doAcceptDecretation =  setingsMap.get("doAcceptDecretation");
		this.doCreateDocument =  setingsMap.get("doCreateDocument");
		this.doAssign = setingsMap.get("doAssign");
	}

	private List<XesLoggedActionsTypes> getListSetingsFromBase() throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(XesLoggedActionsTypes.class);

		criteria.add(Restrictions.isNotNull("setingName"));
		criteria.addOrder(Order.desc("id"));
		List<XesLoggedActionsTypes> list = criteria.list();
		return list;
	}
	
	private List<XesLog> getXesLogsFromToday() throws EdmException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd");
		sdf.format(new Date());
		Date dateFrom = DateUtils.parseSlashDateFormatShort(sdf.format(new Date()));
		Date dateTo = new Date();
		dateTo.setHours(23);
		dateTo.setMinutes(59);
		return getXesLogByDateFromTo(dateFrom, dateTo);
	}


	
	public DynaClass xesLog = new BasicDynaClass("xesLog", null, new DynaProperty[] {
			//Dane z case To Case
			new DynaProperty("id", Long.class),
			new DynaProperty("userId", Long.class),
			new DynaProperty("actionDataId", Long.class),
			new DynaProperty("documentId", Long.class),
			new DynaProperty("xesLogType", String.class),
			new DynaProperty("actionTime", Date.class),
			new DynaProperty("creatingTime", Date.class),
			new DynaProperty("userFirstnameLastname", String.class),
			new DynaProperty("actionName", String.class),
			new DynaProperty("activityId", String.class),
			new DynaProperty("versionId", Long.class),
			new DynaProperty("linkToDownload", String.class) 
			});



	public List<XesLog> getAllXesLog() throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(XesLog.class);

		return criteria.list();
	}

	public DynaBean geatXesLogBeanByXesLogId(Long id) throws EdmException
	{
		DynaBean bean = null;
		Criteria criteria = DSApi.context().session().createCriteria(XesLog.class);
		criteria.add(Restrictions.eq("id", id));
		List<XesLog> list = criteria.list();
		for (XesLog xl : list)
		{
			bean = getBean(xl);
		}

		return bean;
	}

	/**
	 *  Wykonuje  query wedlug zadanych kryteriow domysnie posortowanych po id wpisu desc 
	 *  zwraca liste obiektow XesLog
	 * @param fromInxed
	 * @param toIndex
	 * @return
	 * @throws EdmException
	 */
	public List<XesLog> getXesLogsFromToPosistions(int fromInxed, int toIndex) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(XesLog.class);
		criteria.setFirstResult(fromInxed);
		criteria.setMaxResults(toIndex);
		criteria.addOrder(Order.desc("id"));
		return criteria.list();
	}

	/**
	 * Wykonuje  query wedlug zadanych kryteriow
	 * w przyapdku braku descBy ustawia  order by id  desc 
	 * zwraca liste obiektow XesLog
	 * @param fromInxed  
	 * @param toIndex 
	 * @param descBy
	 * @return
	 * @throws EdmException
	 */
	public List<XesLog> getXesLogsFromToPosistionsOrderDescBy(int fromInxed, int toIndex, String descBy) throws EdmException
	{
		if (descBy == null || descBy.isEmpty())
		{
			descBy = "id";
		}
		Criteria criteria = DSApi.context().session().createCriteria(XesLog.class);
		criteria.setFirstResult(fromInxed);
		criteria.setMaxResults(toIndex);
		criteria.addOrder(Order.desc(descBy));
		return criteria.list();
	}


	public XesLog getXesLogById(Long xesLogId) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(XesLog.class);
		criteria.add(Restrictions.eq("id", xesLogId));
		if(!criteria.list().isEmpty())
			return (XesLog) criteria.list().get(0);
			else return null;
		
	}

	/**
	 * Tworzy pojedynczy obiekt dynaBean z  obiektu XesLog-a
	 * @param XesLog
	 * @return DynaBean(xesLog)
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	private DynaBean getBean(XesLog xl) throws UserNotFoundException, EdmException
	{
		DynaBean bean = DynaBeans.bean(xesLog);
		DSUser u= null;
		bean.set("id", xl.getId());
		
		bean.set("userId", xl.getUserId()!=null?xl.getUserId() : 0l );
		try
		{
			 u = DSUser.findById(xl.getUserId());
		} catch (UserNotFoundException e)
		{
			logger.debug("XesLogActionFIndUsers", e);
		}
		bean.set("userFirstnameLastname", (u!=null ? u.getLastnameFirstnameWithOptionalIdentifier() : ""));
		bean.set("actionDataId", xl.getActionDataId());
		bean.set("documentId", (xl.getDocumentId()!=null? xl.getDocumentId() : -1l));
		bean.set("actionTime", xl.getActionTime());
		bean.set("creatingTime", xl.getCreatingTime());
		bean.set("actionName", (xl.getActionName() != null ? xl.getActionName() : ""));
		bean.set("activityId", (xl.getActivitiId() != null ? xl.getActivitiId() : ""));
		bean.set("versionId", (xl.getVersionId()));
		bean.set("xesLogType", xl.getXesLogType().name());
		
		return bean;
	}

	/**
	 * metoda tworzy gotowa liste beanow xesLogsBeans
	 * z przekazanej  listy  XesLogow
	 *  
	 * @param fromInxed
	 * @param toIndex
	 * @return xesLogBeans
	 * @throws EdmException
	 */
	public List<DynaBean> generateBeans(List<XesLog> listXesLogs) throws EdmException
	{
		List xesLogBeans = new ArrayList(listXesLogs.size());

		for (XesLog xl : listXesLogs)
		{
			xesLogBeans.add(getBean(xl));
		}

		return xesLogBeans;

	}

	/**
	 * @throws FileNotFoundException 
	 * Genereuje Xmla wed�ug standardu xes z wybranego  pojedynczego obiektu 
	 * @param xesLogId
	 * @throws URISyntaxException 
	 * @throws XmlPullParserException 
	 * @throws EdmException 
	 * @throws HibernateException 
	 * 
	 */
	public File generateXmlbyXesLogId(Long xesLogId, boolean serialize) throws HibernateException, EdmException, XmlPullParserException, URISyntaxException, FileNotFoundException
	{
		XesXmlFactory xesFactory = new XesXmlFactory();

		XesLog xld = getXesLogById(xesLogId);
		
		return xesFactory.generateXesXml(xld, xesLogId, serialize, false);
	}

	/**
	 * @param event 
	 * @param xesLogId
	 * @throws Exception 
	 * 
	 */
	public File generateXmlFromDates(ActionEvent event, String from , String to, boolean serialize) throws Exception
	{
		checkCorrectValues(event, from,to);
		
		Criteria criteria = DSApi.context().session().createCriteria(XesLog.class);
			criteria.add(Restrictions.eq("xesLogType", kind));
			criteria.add(Restrictions.ge("creatingTime", DateUtils.nullSafeParseJsDate(cdateFrom)));
			criteria.add(Restrictions.le("creatingTime", DateUtils.nullSafeParseJsDate(cdateTo)));
		if (selectedUsers!=null && selectedUsers.length > 0)
		{
			for (String userSelected : selectedUsers)
			{
				criteria.add(Restrictions.eq("userId",DSUser.findByUsername(userSelected).getId()));
			}
		}
		criteria.addOrder(Order.asc("id"));

		List<XesLog> xesLogSesult = criteria.list();
		XesXmlFactory xesFactory = new XesXmlFactory();
		
		return xesFactory.generateXesXmlFromResults(xesLogSesult, serialize , prepareFileNameToXesLog(from, to), false);
	}
	
	/**
	 * Metoda przygotowuje nazw� pliku xesLoga dla zakresu dat  ktoru zostanie zapisany w katalogu home/xes/ 
	 * @param serializeObject 
	 * 
	 * @return
	 */
	private String prepareFileNameToXesLog(String dateFrom , String dateTo)

	{
		StringBuilder path = new StringBuilder();
		path.append(XesLogType.USER);
		path.append("_DATA_OD_"+dateFrom+"_DATA_DO_"+dateTo);
		return path.toString();
	}
	
	private void checkCorrectValues(ActionEvent event, String from, String to) throws EdmException
	{
		if ( this.kind != XesLogType.USER.ordinal())
		{
			throw new EdmException(sm.getString("xesLog.OnlyForUserType"));
		}	
		else if( cdateFrom == null ||  cdateFrom.isEmpty() ||  cdateTo == null || cdateTo.isEmpty() )
		{
			throw new EdmException(sm.getString("xesLog.wrongDates"));
		}
		else if( cdateFrom != null ||  !cdateFrom.isEmpty() ||  cdateTo != null || !cdateTo.isEmpty() )
		{
			if( DateUtils.nullSafeParseJsDate(cdateTo).before( DateUtils.nullSafeParseJsDate(cdateFrom)))
			throw new EdmException(sm.getString("xesLog.dateAfterbeforeFrom"));
		}	
		else if(this.selectedUsers == null ||  this.selectedUsers.length < 0  ||  this.selectedUsers.length != 1)
		{
			throw new EdmException(sm.getString("xesLog.OnlyForOneUser"));
			
		}
		
	}
	/**
	 * Zwraca pojedynczy obiekt  XesLog lub null 
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public XesLog findById(Long id) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(XesLog.class);
		criteria.add(Restrictions.eq("id", id));
		if (criteria.list().isEmpty())
			return null;
		else
			return (XesLog) criteria.list().get(0);
	}

	/**
	 * Zwraca obiekt xesLogData lub null   z przekazanego idka wpisu z Logiem
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public XesLogData getXesLogDataByXesLogId(Long id) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(XesLog.class);
		criteria.add(Restrictions.eq("id", id));

		if (!criteria.list().isEmpty())
		{
			XesLog xl = (XesLog) criteria.list().get(0);
			XesLogData xld = XesLogData.findById(xl.getActionDataId());
			return xld;
		}


		return null;
	}


	/**
	 * Zwraca liste obiekt�w XesLog lub null w zadanych datach wykonania czynnosci od  do 
	 * @param dateFrom
	 * @param dateTo
	 * @return
	 * @throws EdmException
	 */
	public List getXesLogByDateFromTo(Date dateFrom, Date dateTo) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(XesLog.class);

		if (dateFrom != null && dateTo != null)
		{
			criteria.add(Restrictions.between("actionTime", dateFrom, dateTo));
		} else
			return null;

		return criteria.list();
	}

	public List getXesLogByDate(Date date) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(XesLog.class);

		if (date != null)
		{
			criteria.add(Restrictions.eq("actionTime", date));
		} else
			return null;

		return criteria.list();
	}

	public String getBaseLink()
	{
		return "/office/tasklist/user-task-list.action";
	}


	public String getCdateFrom()
	{
		return cdateFrom;
	}




	public void setCdateFrom(String cdateFrom)
	{
		this.cdateFrom = cdateFrom;
	}




	public String getCdateTo()
	{
		return cdateTo;
	}




	public void setCdateTo(String cdateTo)
	{
		this.cdateTo = cdateTo;
	}




	public Integer getKind()
	{
		return kind;
	}




	public void setKind(Integer kind)
	{
		this.kind = kind;
	}


	public boolean isSerialize()
	{
		return serialize;
	}




	public void setSerialize(boolean serialize)
	{
		this.serialize = serialize;
	}




	public List<DynaBean> getBeans()
	{
		return beans;
	}




	public void setBeans(List<DynaBean> beans)
	{
		this.beans = beans;
	}


	
/*	public boolean isAscending()
	{
		return ascending;
	}


	
	public void setAscending(boolean ascending)
	{
		this.ascending = ascending;
	}*/


	
	public String[] getSelectedUsers()
	{
		return selectedUsers;
	}


	
	public void setSelectedUsers(String[] selectedUsers)
	{
		this.selectedUsers = selectedUsers;
	}


	
	public boolean isSearchResults()
	{
		return isSearchResults;
	}


	
	public void setSearchResults(boolean isSearchResults)
	{
		this.isSearchResults = isSearchResults;
	}

	
	public int getFromIndex()
	{
		return fromIndex;
	}
	
	public void setFromIndex(int fromIndex)
	{
		this.fromIndex = fromIndex;
	}
	
	public int getToIndex()
	{
		return toIndex;
	}
	
	public void setToIndex(int toIndex)
	{
		this.toIndex = toIndex;
	}
	
	public int getLIMIT()
	{
		return LIMIT;
	}
	
	public void setLIMIT(int lIMIT)
	{
		LIMIT = lIMIT;
	}
	
	public List<DSUser> getUsers()
	{
		return users;
	}
	
	public void setUsers(List<DSUser> users)
	{
		this.users = users;
	}
	
	public static String getForward()
	{
		return FORWARD;
	}

	
	public Long getXesLogId()
	{
		return xesLogId;
	}

	
	public void setXesLogId(Long xesLogId)
	{
		this.xesLogId = xesLogId;
	}

	
	public Logger getLogger()
	{
		return logger;
	}

	
	public void setLogger(Logger logger)
	{
		this.logger = logger;
	}

	
	public int getOffset()
	{
		return offset;
	}

	
	public void setOffset(int offset)
	{
		this.offset = offset;
	}

	
	public Pager getPager()
	{
		return pager;
	}

	
	public void setPager(Pager pager)
	{
		this.pager = pager;
	}

	
	
	
	public Map<Integer, String> getKinds()
	{
		kinds  = new LinkedHashMap<Integer, String>(3);
		kinds.put(XesLogType.DOCUMENT.ordinal(), "Dokument");
		kinds.put(XesLogType.USER.ordinal(), "User");
		kinds.put(null, "");
		return kinds;
	}

	
	public void setKinds(Map<Integer, String> kinds)
	{
		this.kinds = kinds;
	}

	
	public String getDocKind()
	{
		return docKind;
	}

	
	public void setDocKind(String docKind)
	{
		this.docKind = docKind;
	}

	
	public Map<String, String> getDocKinds()
	{
		return docKinds;
	}

	
	public void setDocKinds(Map<String, String> docKinds)
	{
		this.docKinds = docKinds;
	}

	
	public String[] getAssignedUser()
	{
		return assignedUser;
	}

	
	public void setAssignedUser(String[] assignedUser)
	{
		this.assignedUser = assignedUser;
	}

		
	public DynaClass getXesLog()
	{
		return xesLog;
	}

	
	public void setXesLog(DynaClass xesLog)
	{
		this.xesLog = xesLog;
	}

	
	public boolean isDoAddToNextCase()
	{
		return doAddToNextCase;
	}

	
	public void setDoAddToNextCase(boolean doAddToNextCase)
	{
		this.doAddToNextCase = doAddToNextCase;
	}

	
	public boolean isDoAddToCase()
	{
		return doAddToCase;
	}

	
	public void setDoAddToCase(boolean doAddToCase)
	{
		this.doAddToCase = doAddToCase;
	}

	
	public boolean isDoWyslijDoEpuap()
	{
		return doWyslijDoEpuap;
	}

	
	public void setDoWyslijDoEpuap(boolean doWyslijDoEpuap)
	{
		this.doWyslijDoEpuap = doWyslijDoEpuap;
	}

	
	public boolean isDoSaveAndAssignOfficeNumber()
	{
		return doSaveAndAssignOfficeNumber;
	}

	
	public void setDoSaveAndAssignOfficeNumber(boolean doSaveAndAssignOfficeNumber)
	{
		this.doSaveAndAssignOfficeNumber = doSaveAndAssignOfficeNumber;
	}

	
	public boolean isDoUpdateDocument()
	{
		return doUpdateDocument;
	}

	
	public void setDoUpdateDocument(boolean doUpdateDocument)
	{
		this.doUpdateDocument = doUpdateDocument;
	}

	
	public boolean isDoCreateCase()
	{
		return doCreateCase;
	}

	
	public void setDoCreateCase(boolean doCreateCase)
	{
		this.doCreateCase = doCreateCase;
	}

	
	public boolean isDoRemoveFromCase()
	{
		return doRemoveFromCase;
	}

	
	public void setDoRemoveFromCase(boolean doRemoveFromCase)
	{
		this.doRemoveFromCase = doRemoveFromCase;
	}

	
	public boolean isDoDeleteAttachment()
	{
		return doDeleteAttachment;
	}

	
	public void setDoDeleteAttachment(boolean doDeleteAttachment)
	{
		this.doDeleteAttachment = doDeleteAttachment;
	}

	
	public boolean isDoAddAttachment()
	{
		return doAddAttachment;
	}

	
	public void setDoAddAttachment(boolean doAddAttachment)
	{
		this.doAddAttachment = doAddAttachment;
	}

	
	public boolean isLogout()
	{
		return logout;
	}

	
	public void setLogout(boolean logout)
	{
		this.logout = logout;
	}

	
	public boolean isLogin()
	{
		return login;
	}

	
	public void setLogin(boolean login)
	{
		this.login = login;
	}

	
	public boolean isDoAcceptDecretation()
	{
		return doAcceptDecretation;
	}

	
	public void setDoAcceptDecretation(boolean doAcceptDecretation)
	{
		this.doAcceptDecretation = doAcceptDecretation;
	}

	
	public boolean isDoCreateDocument()
	{
		return doCreateDocument;
	}

	
	public void setDoCreateDocument(boolean doCreateDocument)
	{
		this.doCreateDocument = doCreateDocument;
	}

	
	public boolean isDoAssign()
	{
		return doAssign;
	}

	
	public void setDoAssign(boolean doAssign)
	{
		this.doAssign = doAssign;
	}
}
