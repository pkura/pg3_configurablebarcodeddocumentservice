package pl.compan.docusafe.core.xes;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;






import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;


/**
 * Klasa odwzorowuj�ca tabelk� DS_XES_LOG tabela zawiera podstawowe dane zawierajace wykoan� czynno�� na dokumencie
 * 
 * @author Grzegorz Filip
 */

public class XesLog implements Serializable
{

	/**
	 * 
	 * Rodzaj XesLoga 
	 * @parram 0 niezdefiniowany
	 * @parram  1- user
	 * @parram 2 - document
	 * @parram 3 - case
	 */
	public static enum XesLogType {
		UNDEFINED(0), USER(1), DOCUMENT(2) , CASE(3);
;

		private Integer xesLogType;

		XesLogType(Integer xesLogType)
		{
			this.xesLogType = xesLogType;
		}
	};
	
	//protected static Map<String,Boolean> LoggedActioinMap;
	protected static String generateXlogData = "xesLog";
	protected static Long NOTEXIST = 1l;
	protected static String URI = "actionUri";
	private Logger logger = LoggerFactory.getLogger(XesLog.class);

	/** ENUM
	 * Typ Logu czy dotyczy  usera czy  dokumentu 
	 * 1 = user 2 = document  3 ' case 0 - niezdefiniowany
	 */
	private Integer xesLogType;

	private Set<XesLogData> xesLogsDatas;
	/**
	 * Id Pozycji
	 */
	private Long id;
	/**
	 * Id uzytkownika DSUser wykonuj�cego akcje 
	 */
	private Long userId;

	/**
	 * Id dokumentu na kt�rym wykonano akcj�
	 */
	private Long documentId;
	/**
	 * Id Sprawy na kt�rym wykonano akcj�
	 */
	private Long caseId;
	/**
	 * Id Teczki na kt�rym wykonano akcj�
	 */
	private Long officeFolderId;
	/**
	 * Czas wykonania czynnosci
	 */
	private Date actionTime;
	/**
	 * Data utworzenia loga 
	 */
	private Date creatingTime;
	/**
	 * nazwa wykonywanej czynno�ci
	 */
	private String actionName;
	/**
	 * activitiId
	 */
	private String activitiId;
	/**
	 * Uri akcji
	 */
	private String actionUri;
	/**
	 * Czy jest aktywny dany log czyli jesli log ma active to go aktualizujemy w XesLogu a nie tworzymy nowego 
	 */
	private boolean activeLog;
	/**
	 * Id pozycji  z Tabeli Powi�zanej zawierajac� dodatkowe  dane zdazenia DS_XES_LOG_DATA
	 */
	private Long actionDataId;
	/**
	 * Id versji 
	 */
	private Long versionId;

	/**
	 * nazwa klasy z kt�rej wykonywano akcj�(czynno�ci)
	 */
	private String actionClass;
	
	private static boolean xesApiAvailable = AvailabilityManager.isAvailable("XesLogApi");
	/**
	 * Domy�lny konstruktor
	 */
	public XesLog()
	{

	}
	
	/**
	 * Metoda sprawdza czy dla wykonywanej akcji jest ustawione logowanie do xesa 
	 * jesli tak to ustawia zmienna na evencie na true i wrzuca do kolejki na serwis XesLogService
	 *  XesLoga  
	 *  
	 * @param eventActionSupport 
	 * 
	 * @param map 
	 * @throws EdmException 
	 */
	
	public static void doXesLogEvent(String actionName , EventActionSupport eventActionSupport, ActionEvent event) throws Exception{
		
		if(xesApiAvailable && event.getAttribute(generateXlogData)!=null && (Boolean)event.getAttribute(generateXlogData) /*|| isXesLogForThisAction(eventActionSupport))*/){
			 createXesLogEvent(actionName , eventActionSupport,  event) ;
			 event.setAttribute(generateXlogData, false);
		}
		else {
		
			if(AvailabilityManager.isAvailable("createNewAction")){
				createNewActionTypes(actionName, eventActionSupport);
			}
			
		}
	
	}
	/**
	 *  Metoda sprawdza czy dla wykonywanej akcji jest ustawione logowanie do xesa 
	 * jesli tak to ustawia zmienna na evencie na true i wrzuca do kolejki na serwis XesLogService
	 *  XesLoga  
	 * @param actionListener
	 * @param eventProcesingAction
	 * @param event
	 * @throws Exception
	 */
	public static void doXesLogEventCommon(ActionListener actionListener, EventProcessingAction eventProcesingAction,
			pl.compan.docusafe.web.common.event.ActionEvent event) throws Exception
	{

		String uri = event.getRequest().getRequestURI();
		String actionName = actionListener.toString();
		if (actionName.contains("@"))
			actionName = actionName.substring(0, actionName.lastIndexOf("@"));
		if (actionName.contains("$"))
			actionName = actionName.substring(actionName.indexOf("$")+1);

		String actionClass = actionListener.toString();
		if (actionClass.contains("$"))
			actionClass = actionClass.substring(0, actionClass.lastIndexOf("$"));

		if (xesApiAvailable && event.getAttribute(generateXlogData) != null
				&& ((Boolean) event.getAttribute(generateXlogData) || XesLoggedActionsTypes.isActionLoggingEnableByUriAndAction(uri, actionName, actionClass)))
		{
			event.setAttribute(URI, uri);
			createXesLogEventFromCommonEvent(actionName, actionClass, eventProcesingAction, event);
			event.setAttribute(generateXlogData, false);
			
		} /*else if (AvailabilityManager.isAvailable("createNewAction"))
		{
			//boolean opened = DSApi.openContextIfNeeded();
			Criteria criteria = DSApi.context().session().createCriteria(XesLoggedActionsTypes.class);
			criteria.add(Restrictions.eq("actionClass", actionClass));
			if(actionName!=null && !actionName.isEmpty())
			criteria.add(Restrictions.eq("name", actionName));
			else
			actionName = "brak";
			List<XesLoggedActionsTypes> list = criteria.list();
			if (list.isEmpty())
			{
			XesLoggedActionsTypes xlat = new XesLoggedActionsTypes();
			xlat.setActionClass(actionClass);
			xlat.setActionName(actionName);
			xlat.setLogThisAction(false);
			xlat.setShowingName("nn");
			xlat.setUri(uri);
			DSApi.context().session().save(xlat);
			DSApi.context().session().flush();
			
				
		}
		//	DSApi.closeContextIfNeeded(opened);
		}*/


	}



	private static OfficeDocument getDocumentFromActionCommon(EventProcessingAction eventProcesingAction, pl.compan.docusafe.web.common.event.ActionEvent event)
			throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, EdmException
	{
		Method method = null;
		OfficeDocument doc = null;
		try
		{
			method = eventProcesingAction.getClass().getMethod("getDocument");
			if (method != null)
			{
				doc = (OfficeDocument) method.invoke(eventProcesingAction);
			} else
			{
				method = eventProcesingAction.getClass().getMethod("getDocumentId");
				if (method != null)
				{
					Long id = (Long) method.invoke(eventProcesingAction);
					if (id == null)
					{
						return null;
					} else

						doc = OfficeDocument.findOfficeDocument(id, false);
				}
			}
		} catch (NoSuchMethodException e)
		{
			;//nic nie robimy bo po prostu niema ;
		}
		if (doc == null && event.getAttribute("documentId") != null)
		{
			try
			{
				doc = OfficeDocument.findOfficeDocument((Long) event.getAttribute("documentId"), false);
			} catch (Exception e)
			{
				;//po prostu nie istnieje taki document nic nie robimy 
			}
		}
		return doc;
	}

	private static void createXesLogEvent(String actionName, EventActionSupport eventActionSupport, ActionEvent event) throws Exception
	{

		String actionClass = eventActionSupport.toString();
		if (actionClass.contains("@"))
			actionClass = actionClass.substring(0, actionClass.lastIndexOf("@"));
		
		OfficeDocument doc = getDocumentFromAction(eventActionSupport, event);
		OfficeCase oficeCase = getCaseFromAction(eventActionSupport, event);
		OfficeFolder officeFolder = getOfficeFolderFromAction(eventActionSupport, event);
		String uri = (String) event.getAttribute(URI);
		createXesLogAddToService(actionName, actionClass, uri ,doc, oficeCase, officeFolder );
	}

	private static void createXesLogAddToService(String actionName, String actionClass, String uri , OfficeDocument doc, OfficeCase oficeCase,
			OfficeFolder officeFolder) throws EdmException, UserNotFoundException
	{
		XesLog xLog = new XesLog();
		
		xLog.setXesLogType(XesLogType.USER);
		xLog.setActionClass(actionClass);
		xLog.setActionName(actionName);
		xLog.setUserId(DSApi.context().getDSUser().getId());
		xLog.setActionTime(new Date());
		xLog.setCreatingTime(new Date());
		xLog.setActivitiId("brak");
		xLog.setActionDataId(NOTEXIST);
		xLog.setActionUri(uri);
		if (doc != null){
			xLog.setXesLogType(XesLogType.DOCUMENT);
			xLog.setDocumentId(doc.getId());
		}
		if (oficeCase != null){
			xLog.setCaseId(oficeCase.getId());
		}
		if (officeFolder != null)
			xLog.setOfficeFolderId(officeFolder.getId());
		XesLogService.addXesLog(xLog);
	}
	
	private static void createXesLogEventFromCommonEvent(String actionName,String actionClass, EventProcessingAction eventProcesingAction,
			pl.compan.docusafe.web.common.event.ActionEvent event) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, EdmException
	{
		OfficeDocument doc = getDocumentFromActionCommon(eventProcesingAction, event);
		OfficeCase oficeCase = getCaseFromActionCommon(eventProcesingAction, event);
		OfficeFolder officeFolder = getOfficeFolderFromCommonAction(eventProcesingAction, event);
		String uri = (String) event.getAttribute(URI);
		createXesLogAddToService(actionName, actionClass, uri,doc, oficeCase, officeFolder);

	}
		
	private static OfficeFolder getOfficeFolderFromAction(EventActionSupport eventActionSupport, ActionEvent event) throws SecurityException,
			 IllegalArgumentException, IllegalAccessException, InvocationTargetException
	{
		
		OfficeFolder of = null;
		Method method = null;
		try{
		method = eventActionSupport.getClass().getMethod("getPortfolioId");
		if (method != null)
		{
			Long ofId = (Long) method.invoke(eventActionSupport);
			try
			{if(of!=null)
				of = OfficeFolder.find(ofId);
			} catch (EdmException e)
			{
				return null;

			}
		}
		}catch (NoSuchMethodException e){
			;//nic nie robimy;
		}

		return of;
	}
	private static OfficeFolder getOfficeFolderFromCommonAction(EventProcessingAction eventProcesingAction,
			pl.compan.docusafe.web.common.event.ActionEvent event) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
	{
		OfficeFolder of = null;
		Method method = null;
		try{
		method = eventProcesingAction.getClass().getMethod("getPortfolioId");
		if (method != null)
		{
			Long ofId = (Long) method.invoke(eventProcesingAction);
			try
			{
				of = OfficeFolder.find(ofId);
			} catch (EdmException e)
			{
				return null;

			}
		}
		}catch (NoSuchMethodException e){
			;//nic nie robimy;
		}
		
		

		return of;
		
	}
	
	
	private static OfficeCase getCaseFromAction(EventActionSupport eventActionSupport, ActionEvent event) throws SecurityException, 
			IllegalArgumentException, IllegalAccessException, InvocationTargetException, EdmException
	{
		OfficeCase c = null;
		Method method = null;
		try{
		method = eventActionSupport.getClass().getMethod("getCaseId");
		
		if (method != null)
		{
			Long caseId = (Long) method.invoke(eventActionSupport);
			if (caseId != null)
				c = OfficeCase.find(caseId);
		} 
		
		if (c==null){
			method = eventActionSupport.getClass().getMethod("getOfficeCase");
			if (method != null)
			{
				c = (OfficeCase) method.invoke(eventActionSupport);
			}
		}
		
		}catch (NoSuchMethodException e){
			;//nic nie robimy bo po prostu niema ;
		}
		return c;
	}

	private static OfficeCase getCaseFromActionCommon(EventProcessingAction eventProcesingAction, pl.compan.docusafe.web.common.event.ActionEvent event) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, EdmException
	{
		OfficeCase c = null;
		Method method = null;
		try{
		method = eventProcesingAction.getClass().getMethod("getCaseId");
		
		if (method != null)
		{
			Long caseId = (Long) method.invoke(eventProcesingAction);
			if (caseId != null)
				c = OfficeCase.find(caseId);
		} else
		{
			method = eventProcesingAction.getClass().getMethod("getOfficeCase");
			if (method != null)
			{
				c = (OfficeCase) method.invoke(eventProcesingAction);
			}
		}
		}catch (NoSuchMethodException e){
			;//nic nie robimy bo po prostu niema ;
		}
		return c;
	}
	
/**
 * Sprawdza czy w akcji uczestniczy dokument i prubuje wywola� na klasie metody getDocument lub get document id 
 * jesli clasa akcji posiada takie metody pobiera dokument
 *
 * @param doc
 * @param eventActionSupport
 * @param event
 * @return  dokument  lub null 
 * @throws SecurityException
 * @throws NoSuchMethodException
 * @throws IllegalArgumentException
 * @throws IllegalAccessException
 * @throws InvocationTargetException
 * @throws EdmException
 */

	private static OfficeDocument getDocumentFromAction( EventActionSupport eventActionSupport, ActionEvent event) throws SecurityException,
			NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, EdmException
	{
		Method method = null;
		OfficeDocument doc = null;
		try{
		method = eventActionSupport.getClass().getMethod("getDocument");
		if (method != null)
		{
			doc = (OfficeDocument) method.invoke(eventActionSupport);
		} else
		{
			method = eventActionSupport.getClass().getMethod("getDocumentId");
			if (method != null)
			{
				Long id = (Long) method.invoke(eventActionSupport);
				if (id == null)
				{
					return null;
				} else

					doc = OfficeDocument.findOfficeDocument(id, false);
			}
		}
		}catch (NoSuchMethodException e){
			;//nic nie robimy bo po prostu niema ;
		}
		if(doc==null && event.getAttribute("documentId")!=null){
			try{
			doc = OfficeDocument.findOfficeDocument((Long)event.getAttribute("documentId"), false);
			}
			catch (Exception e) {
				;//po prostu nie istnieje taki document nic nie robimy 
			}
		}
		return doc;
	}
	private static void createNewActionTypes(String actionname, EventActionSupport eventActionSupport) throws HibernateException, EdmException
	{
		
		
		String actionClass = eventActionSupport.toString();
		if (actionClass.contains("@"))
			actionClass = actionClass.substring(0,actionClass.lastIndexOf("@"));
		
		
		Criteria criteria = DSApi.context().session().createCriteria(XesLoggedActionsTypes.class);
		criteria.add(Restrictions.eq("actionClass", actionClass));
		if(actionname!=null && !actionname.isEmpty())
		criteria.add(Restrictions.eq("actionName", actionname));
		else
		actionname = "brak";
		List<XesLoggedActionsTypes> list = criteria.list();
		if (list.isEmpty())
		{

			XesLoggedActionsTypes xlat = new XesLoggedActionsTypes();
			xlat.setActionClass(actionClass);
			xlat.setActionName(actionname);
			xlat.setLogThisAction(false);
			xlat.setShowingName("nn");
			xlat.setUri("nn");
			DSApi.context().session().save(xlat);
			DSApi.context().session().flush();
			
		}
		
		
	}

	/**
	 * 
	 * @param xesLogType
	 * @param creatingTime can be null
	 * @param userId
	 * @param documentId
	 * @param actionTime
	 * @param actionName
	 * @param activitiId
	 * @param actionDataId
	 * @param versionId
	 * @throws EdmException 
	 * @throws UserNotFoundException 
	 */
	public XesLog(Integer xesLogType, Date creatingTime, Long userId, Long documentId, Date actionTime, String actionName, String activitiId,
			long actionDataId, Long versionId) throws UserNotFoundException, EdmException
	{
		super();
		this.xesLogType = xesLogType;
		if (userId == null)
			userId = DSApi.context().getDSUser().getId();
		this.userId = userId;
		this.documentId = documentId;
		this.actionTime = actionTime;
		this.actionName = actionName;
		this.activitiId = activitiId;
		this.actionDataId = actionDataId;
		this.versionId = versionId;
		this.creatingTime = creatingTime;

	}

	public XesLog(Integer xesLogType, Long userId, Long documentId, Date actionTime, String actionName, String activitiId, long actionDataId, Long versionId)
			throws UserNotFoundException, EdmException
	{
		super();
		this.xesLogType = xesLogType;
		if (userId == null)
			userId = DSApi.context().getDSUser().getId();
		this.userId = userId;
		this.documentId = documentId;
		this.actionTime = actionTime;
		this.actionName = actionName;
		this.activitiId = activitiId;
		this.actionDataId = actionDataId;
		this.versionId = versionId;
		this.creatingTime = new Date();

	}

	public void delete() throws Exception
	{
		logger.debug("deleting " + this);
		try
		{
			XesLogData xld = XesLogData.findById(this.getDocumentId());
			if (xld != null)
				DSApi.context().session().delete(this);
			DSApi.context().session().flush();
		} catch (HibernateException e)
		{
			logger.error("Blad usuniecia wpisu");
			throw new EdmException("Blad usuniecia wpisu. " + e.getMessage());
		}
	}

	/**
	 * Tworzy nowy obiekt XesLog dla documentu oraz nowy obiekt XesLogData
	 * w przypadku gdy obiekt nie istnieje tworzy nowy obiekty z wersja nr 1  
	 * w przypadku gdy taki obiekt ju� istnieje - sprawdza czy nie wystarczy zakualizowa� bie�acego obiektu por�wnuj�c wersje logu
	 * je�eli podano now� wersj� sprawdza czy jest to odpowiednia wersja w bazie i j� aktualizuje 
	 * @throws Exception
	 * @throws EdmException
	 */
	public void createOrUpdate() throws Exception
	{
		if (this.getCaseId()!=null){
			prepareForCase(this);
		}
				if (this.getXesLogType().equals(XesLogType.DOCUMENT))
			prepareForDocument(this);

		else if (this.getXesLogType().equals(XesLogType.USER))
			prepareForUser(this);
		else if (this.getXesLogType().equals(XesLogType.UNDEFINED))
		{
			logger.debug("niezdefiniowany ");
		}
		else if (this.getXesLogType().equals(XesLogType.CASE))
		{
			logger.debug("niezdefiniowany ");
		}

	}

	public void update() throws Exception, EdmException
	{
		XesLog existedLogDocument = getXesLogByDocumentId(getDocumentId());
		XesLog existedLogUser = getXesLogByUserId(this.getUserId());
		if (this.getXesLogType().equals(XesLogType.DOCUMENT))
		{
			if (existedLogDocument != null)
			{
				updateDocument(existedLogDocument, this);
				if (existedLogUser != null)
				{
					updateUser(existedLogUser, this);
				} else
					createForUser(this);

			}
		} else if (this.getXesLogType().equals(XesLogType.USER))
		{
			existedLogUser = getXesLogByUserId(this.getUserId());
			if (existedLogUser != null)
			{
				updateUser(existedLogUser, this);
			}

		} else
		{
			throw new EdmException("nie zdefiniowany TYP XESLOGA");
		}
	}

	/**
	 * zwraca obiekt  xes logu dla uzytkownika  z dnia biezacego
	 * @param userId
	 * @return
	 * @throws EdmException
	 */
	public XesLog getXesLogByUserId(long userId) throws EdmException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd");
		sdf.format(new Date());
		Date data = DateUtils.parseSlashDateFormatShort(sdf.format(new Date()));
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("userId", userId));
		criteria.add(Restrictions.ge("creatingTime", data));
		criteria.add(Restrictions.eq("xesLogType", XesLogType.USER.ordinal()));
		criteria.add(Restrictions.eq("activeLog", true));
		criteria.addOrder(Order.desc("id"));
		if (!criteria.list().isEmpty())
			return (XesLog) criteria.list().get(0);
		else
			return null;
	}
	/**
	 * zwraca obiekt  xes logu dla sprawy  po xesLogId
	 * @param userId
	 * @return
	 * @throws EdmException
	 */
	public static XesLog getXesLogCaseById(long xesLogId) throws EdmException
	{
			
        Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq("xesLogType", XesLogType.CASE.ordinal()));
		criteria.add(Restrictions.eq("activeLog", true));
		criteria.add(Restrictions.eq("id", xesLogId));
		if (!criteria.list().isEmpty())
			return (XesLog) criteria.list().get(0);
		else
			return null;
	}
	/**
	 * zwraca obiekt  xes logu dla sprawy  po idSprawy
	 * @param userId
	 * @return
	 * @throws EdmException
	 */
	public static XesLog getXesLogCaseByCaseId(Long caseId) throws EdmException
	{
			
        Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq("xesLogType", XesLogType.CASE.ordinal()));
		criteria.add(Restrictions.eq("activeLog", true));
		criteria.add(Restrictions.eq("caseId", caseId));
		criteria.addOrder(Order.desc("id"));
		if (!criteria.list().isEmpty())
			return (XesLog) criteria.list().get(0);
		else
			return null;
	}
	private void createForUser(XesLog xesLog) throws Exception
	{
		XesLog xl = new XesLog();
		xl.setVersionId(1l);
		xl.setActiveLog(true);
		xl.setXesLogType(XesLogType.USER.ordinal());
		xl.setActionDataId(xesLog.getActionDataId());
		xl.setActionName(xesLog.getActionName());
		xl.setUserId(xesLog.getUserId());
		xl.setActionTime(xesLog.getActionTime());
		xl.setActivitiId(xesLog.getActivitiId());
		xl.setActionUri(xesLog.getActionUri());
		xl.setCreatingTime(new Date());
		xl.setActionClass(xesLog.getActionClass());
		
		if(xesLog.getCaseId()!=null)
			xl.setCaseId(xesLog.getCaseId()) ;	
		
		if(xesLog.getOfficeFolderId()!=null)
			xl.setOfficeFolderId(xesLog.getOfficeFolderId()) ;	
		
		if (xesLog.getDocumentId() != null)
			xl.setDocumentId(xesLog.getDocumentId());
		
		DSApi.context().session().save(xl);
		
		createXesLogDataItemForUser(xl);
		
	}

	/**
	 * Wykonuje query wedlug zadanych kryteriow
	 * ustawia  order by id  desc 
	 * zwraca liste obiektow XesLoga powiazanych z jednym dokumentem 
	 * zawsze bedzie jeden element albo pusty nie moze byc wiecej niz jeden  log dla dokumentu
	 * @param fromInxed  
	 * @param toIndex 
	 * @param descBy
	 * @return
	 * @throws EdmException
	 */
	public static  XesLog getXesLogByDocumentId(long documentId) throws EdmException
	{
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("documentId", documentId));
		criteria.add(Restrictions.eq("xesLogType", XesLogType.DOCUMENT.ordinal()));
		criteria.add(Restrictions.eq("activeLog", true));
		criteria.addOrder(Order.desc("id"));
		if (!criteria.list().isEmpty())
			return (XesLog) criteria.list().get(0);
		else
			return null;

	}

	private void createForDocument(XesLog xesLog) throws Exception
	{
		this.setActiveLog(true);
		this.setVersionId(1l);
		DSApi.context().session().save(this);
		createXesLogDataItemForDocument(this);

	}

	private void prepareForUser(XesLog xesLog) throws Exception
	{
		XesLog existedLogUser = getXesLogByUserId(xesLog.getUserId());
		if (existedLogUser != null)
		{
			updateUser(existedLogUser, this);
		} else
		{
			createForUser(this);
		}

	}

	private void prepareForDocument(XesLog xesLog) throws Exception
	{
		XesLog existedLogDocument = getXesLogByDocumentId(xesLog.getDocumentId());
		XesLog existedLogUser = getXesLogByUserId(xesLog.getUserId());
		if (existedLogDocument != null)
		{
			updateDocument(existedLogDocument, this);
			if (existedLogUser != null)
			{
				updateUser(existedLogUser, this);
			} else
				createForUser(this);

		} else
		{
			createForDocument(this);

		
			if (existedLogUser != null)
			{
				updateUser(existedLogUser, this);
			} else
				createForUser(this);

		}

	}

	private void prepareForCase(XesLog xesLog) throws Exception
	{
		XesLog existedLogCase = getXesLogCaseByCaseId(xesLog.getCaseId());

		if (existedLogCase != null)
		{
			updateCase(existedLogCase, this);

		} else
			createForCase(this);



	}

	
	private void createForCase(XesLog xesLog) throws Exception
	{
		XesLog xl = new XesLog();
		xl.setVersionId(1l);
		xl.setActiveLog(true);
		xl.setXesLogType(XesLogType.CASE.ordinal());
		xl.setActionDataId(xesLog.getActionDataId());
		xl.setActionName(xesLog.getActionName());
		xl.setUserId(xesLog.getUserId());
		xl.setActionTime(xesLog.getActionTime());
		xl.setActivitiId(xesLog.getActivitiId());
		xl.setActionUri(xesLog.getActionUri());
		xl.setCreatingTime(new Date());
		xl.setActionClass(xesLog.getActionClass());
		
		if(xesLog.getCaseId()!=null)
			xl.setCaseId(xesLog.getCaseId()) ;	
		
		if(xesLog.getOfficeFolderId()!=null)
			xl.setOfficeFolderId(xesLog.getOfficeFolderId()) ;	
		
		if (xesLog.getDocumentId() != null)
			xl.setDocumentId(xesLog.getDocumentId());
		
		DSApi.context().session().save(xl);
		
		createXesLogDataItemForCase(xl);
		
	}

	private void updateCase(XesLog existedLogCase, XesLog xesLog) throws Exception
	{
		Long newVersionId = existedLogCase.getVersionId() + 1l;
		setDefaultParameters(existedLogCase, xesLog, newVersionId);
		DSApi.context().session().save(existedLogCase);
		//	DSApi.context().session().flush();
			createXesLogDataItemForCase(existedLogCase);
	}

	/**
	 * Wykonuje update obiektu XesLog aktualizuj�c informacje o obiekcie do najnowszej wersji.
	 * Podczas updata tworzy nowwy obiekt XesLogData zawieraj�cy wykonan� akcj� i ustawiajac mu kolejna werjs� zgodna rodzicem XesLog
	 * @throws Exception
	 */


	private void updateUser(XesLog existedLogUser, XesLog xesLog) throws Exception

	{
		Long newVersionId = existedLogUser.getVersionId() + 1l;
		setDefaultParameters(existedLogUser, xesLog, newVersionId);

		DSApi.context().session().save(existedLogUser);
	//	DSApi.context().session().flush();
		createXesLogDataItemForUser(existedLogUser);
	}
	public static boolean isXesLogForThisAction(EventActionSupport eventActionSupport) throws EdmException, ExecutionException
	{
		if(xesApiAvailable && eventActionSupport != null){
			//throw new NullPointerException("Event Action Support is null");
		String actionName = eventActionSupport.toString();
		if (actionName.contains("@"))
			actionName = actionName.substring(0,actionName.lastIndexOf("@"));
		
		return XesLoggedActionsTypes.checIsLogingEnable(actionName);
		}
		return false;
	}

	public static boolean isXesLogForThisAction(ActionEvent event, String requestURI) throws ExecutionException, EdmException
	{
		String actionName = null;

		if (xesApiAvailable && requestURI != null)
		{
			if (event != null && event.getAttribute("executedListener") != null)
				actionName = (String) event.getAttribute("executedListener");
			if(!requestURI.isEmpty() && !requestURI.startsWith("/docusafe"))
				requestURI = "/docusafe"+requestURI;
		
		//throw new NullPointerException("Event is null");
		if (XesLoggedActionsTypes.isActionLoggingEnableByUriAndAction(requestURI, actionName, null))
		{
			if (event != null){
				event.setAttribute(generateXlogData, true);
				event.setAttribute(URI, requestURI);
			}
			return true;
		}
		}
		return false;


	}

	/**
	 * UStawia domyslne  parametry  dla istniej�cego  XesLoga 
	 * @param existedLog
	 * @param xesLog
	 * @param newVersionId
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	private void setDefaultParameters(XesLog existedLog, XesLog xesLog, Long newVersionId) throws UserNotFoundException, EdmException
	{
		existedLog.setActionDataId(xesLog.getActionDataId());
		existedLog.setActionName(xesLog.getActionName());
		existedLog.setActionTime(xesLog.getActionTime());
		existedLog.setActionClass(xesLog.getActionClass());
		existedLog.setActionUri(xesLog.getActionUri());
		if(xesLog.getCaseId()!=null)
			 existedLog.setCaseId(xesLog.getCaseId()) ;	
		
		if(xesLog.getOfficeFolderId()!=null)
			 existedLog.setOfficeFolderId(xesLog.getOfficeFolderId()) ;	
		
		if (xesLog.getDocumentId() != null)
			existedLog.setDocumentId(xesLog.getDocumentId());
		
		existedLog.setActivitiId(xesLog.getActivitiId());
		existedLog.setVersionId(newVersionId);
		existedLog.setUserId(xesLog.getUserId());
	}


	/**
	 *Wykonuje Update XesLoga Dla usera i tworzy nowy obiekt xesLogData - dla dokumentu
	 * @param existedLogDocument
	 * @param xesLog
	 * @throws Exception
	 */

	private void updateDocument(XesLog existedLogDocument, XesLog xesLog) throws Exception
	{
		Long newVersionId = existedLogDocument.getVersionId() + 1l;
		setDefaultParameters(existedLogDocument, xesLog, newVersionId);

		DSApi.context().session().save(existedLogDocument);
		//DSApi.context().session().flush();
		createXesLogDataItemForDocument(existedLogDocument);
	}

	/**
	 * Tworzy nowy obiekt XesLogData dla dokuemntu z przekazanego XesLoga i dodaje powiazania
	 * @param xesLogDocument
	 * @throws Exception
	 */

	private void createXesLogDataItemForDocument(XesLog xesLogDocument) throws Exception
	{
		OfficeDocument doc = (OfficeDocument) Document.find(xesLogDocument.getDocumentId(), false);

		XesLogData xld = new XesLogData();

		xld.setXesLogType(XesLogType.DOCUMENT.ordinal());
		xld.setDocumentId(xesLogDocument.getDocumentId());
		setXesLogDataDefaultParameters(xesLogDocument, xld);
		xld.setDockindId(doc.getDocumentKind().getId());
		XesXmlFactory factory = new XesXmlFactory();
		factory.createXmlData(xld);
	//	factory.generateXesXml(xesLogDocument, null, true);
		DSApi.context().session().save(xld);

		addXesLogData(xesLogDocument, xld);

		

	}

	/**
	 *  dodaje powiazania pomiedzy XesLog-a(Usera lub Dokumentu) a XesLogData  
	 * @param xesLog
	 * @param xesLogData
	 * @throws EdmException 
	 * @throws HibernateException 
	 */
	private static void addXesLogData(XesLog xesLog, XesLogData xesLogData) throws HibernateException, EdmException
	{
		
		xesLog.addXesLogData(xesLogData);
		xesLogData.setXesLog(xesLog);
		DSApi.context().session().update(xesLog);	
		DSApi.context().session().update(xesLogData);	
	}

	/**
	 * Tworzy nowy obiekt XesLogData dla Usera z przekazanego XesLoga i dodaje powiazanie go z XesLogiem Usera
	 * @param xesLogDocument
	 * @throws Exception
	 */
	private void createXesLogDataItemForUser(XesLog xesLogUser) throws Exception
	{

		XesLogData xld = new XesLogData();
		xld.setXesLogType(XesLogType.USER.ordinal());
		setXesLogDataDefaultParameters(xesLogUser, xld);
		XesXmlFactory factory = new XesXmlFactory();
		
		factory.createXmlData(xld);
		//factory.generateXesXml(xesLogUser, null, true);
		
		
		DSApi.context().session().save(xld);
		addXesLogData(xesLogUser, xld);

	}
	/**
	 * Tworzy nowy obiekt XesLogData dla Sprawz z przekazanego XesLoga
	 * @param xesLogCase
	 * @throws Exception
	 */
	private void createXesLogDataItemForCase(XesLog xesLogCase) throws Exception
	{

		XesLogData xld = new XesLogData();
		xld.setXesLogType(XesLogType.CASE.ordinal());
		setXesLogDataDefaultParameters(xesLogCase, xld);
		XesXmlFactory factory = new XesXmlFactory();
		
		factory.createXmlData(xld);
		//factory.generateXesXml(xesLogUser, null, true);
		
		
		DSApi.context().session().save(xld);
		addXesLogData(xesLogCase, xld);

	}

	/**
	 * Ustawia domyslne parametry dla xesLogData i dodaje powiazanie go z xesLogiemDokumentu lub xesLogiemUsera
	 * @param xesLog
	 * @param xld
	 * @throws EdmException 
	 * @throws UserNotFoundException 
	 */
	private void setXesLogDataDefaultParameters(XesLog xesLog, XesLogData xld) throws UserNotFoundException, EdmException
	{
		xld.setCreatingTime(xesLog.getCreatingTime());
		xld.setActionDataId(xesLog.getActionDataId());
		xld.setActionName(xesLog.getActionName());
		xld.setActionTime(xesLog.getActionTime());
		xld.setActivitiId(xesLog.getActivitiId());
		xld.setActionClass(xesLog.getActionClass());
		xld.setActionUri(xesLog.getActionUri());
		if(xesLog.getCaseId()!=null)
			xld.setCaseId(xesLog.getCaseId()) ;	
		
		if(xesLog.getOfficeFolderId()!=null)
			xld.setOfficeFolderId(xesLog.getOfficeFolderId());	
		
		if (xesLog.getDocumentId() != null)
			xld.setDocumentId(xesLog.getDocumentId());
		
		xld.setVersionId(xesLog.getVersionId());
		xld.setUserId(xesLog.getUserId());
	}

	public static  void createLoginLogoutEvent(DSUser user,String actionName, String clasToString, String uri ,Boolean validate)
	{
		try{
		String actionClass = clasToString;
		if (actionClass.contains("@"))
			actionClass = actionClass.substring(0,actionClass.lastIndexOf("@"));
		XesLog xl = new XesLog();
		xl.setXesLogType(XesLogType.USER);
		xl.setActionClass(actionClass);
		xl.setActionName(actionName);
		xl.setActionUri(uri);
		xl.setUserId(user.getId());
		xl.setActionTime(new Date());
		xl.setCreatingTime(new Date());
		xl.setActivitiId("brak");
		xl.setActionDataId(NOTEXIST);
		boolean createdLogout = false;
		if(validate)
		 createdLogout = validateCreateLogutExist(user,actionName ,xl, uri,clasToString);
		
		if(createdLogout){
			createLoginEvent(user,actionName,clasToString,uri);
		}
	
	
		}
		catch (Exception e) {
			String a = "";
				}
	}
private static void createLoginEvent(DSUser user, String actionName, String clasToString, String uri)
	{
	
		String actionClass = clasToString;
		if (actionClass.contains("@"))
			actionClass = actionClass.substring(0,actionClass.lastIndexOf("@"));
		XesLog xl = new XesLog();
		xl.setXesLogType(XesLogType.USER);
		xl.setActionClass(actionClass);
		xl.setActionName(actionName);
		xl.setActionUri(uri);
		xl.setUserId(user.getId());
		xl.setActionTime(new Date());
		xl.setCreatingTime(new Date());
		xl.setActivitiId("brak");
		xl.setActionDataId(NOTEXIST);
		XesLogService.addXesLog(xl);
	}

/**
 * Metoda sprawdza czy istnieje log wylogowania dla uzytkownika przed zalogowaniem jesli nie to go tworzy
 * @param user
 * @param actionName
 * @param xl
 * @param uri
 * @param clasToString
 * @throws Exception 
 */
	private static boolean validateCreateLogutExist(DSUser user, String actionName, XesLog xl, String uri, String clasToString) throws Exception
	{

		SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd");
		sdf.format(new Date());
		Date data = DateUtils.parseSlashDateFormatShort(sdf.format(new Date()));
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("userId", user.getId()));
		criteria.add(Restrictions.between("creatingTime", data, new Date()));
		criteria.add(Restrictions.eq("xesLogType", XesLogType.USER.ordinal()));
		criteria.addOrder(Order.desc("id"));
		if (!criteria.list().isEmpty())
		{
			XesLog last = (XesLog) criteria.list().get(0);

			//tu sprawdzamy z bie�acego dnia 
			if (!"logout".equals(last.getActionName()))
			{
				Date newDate = new Date();
				newDate = xl.getActionTime();
				newDate.setSeconds(xl.getActionTime().getSeconds() - 5);
				xl.setActionTime(newDate);
				createLogutParameters(xl);
				XesLogService.addXesLog(xl);
				return true;
			} else
			{
				XesLogService.addXesLog(xl);
				return false;
			}

		}
		//tu sprawdzamy we wczesniejszych dnia czy w ostatniej akcji  jest wylogowanie je�li nei to tworzymy z data wpisu 
		else
		{
			criteria = DSApi.context().session().createCriteria(XesLog.class);
			criteria.add(Restrictions.eq("userId", user.getId()));
			criteria.add(Restrictions.eq("xesLogType", XesLogType.USER.ordinal()));
			criteria.addOrder(Order.desc("id"));
			if (!criteria.list().isEmpty())
			{
				XesLog last = (XesLog) criteria.list().get(0);
				if (!"logout".equals(last.getActionName()))
				{
					createLogutParameters(xl);
					Date dateAction = last.getActionTime();
					Date dateCreated = last.getCreatingTime();
					Date dateActionNew = createNewDate(dateAction);

					if (dateAction.before(dateActionNew))
					{
						xl.setActionTime(dateActionNew);
						xl.setCreatingTime(dateCreated);
					} else // tutaj jesli ktos pracowal  pozniej niz do szesnatej to minute po ostaniej akcji wylogowanie
					{
						dateActionNew.setMinutes(dateAction.getMinutes() + 1);
						xl.setActionTime(dateActionNew);
						xl.setCreatingTime(dateCreated);
					}
					XesLogService.addXesLog(xl);
					return true;
				}
				else
				{
					XesLogService.addXesLog(xl);
					return false;
				}
			}
			//jesl iw og�le nie isnieje to tworzymy 
			else{
				XesLogService.addXesLog(xl);
			}

		}
		return false;

	}

private static void createLogutParameters(XesLog xl)
{
	xl.setActionName("logout");
	xl.setActionUri("/docusafe/logout.do");
	xl.setActionClass("pl.compan.docusafe.web.common.LogoutAction");
}

private static Date createNewDate(Date dateAction)
{
	Date dateActionNew = new Date();
	
	dateActionNew.setTime(dateAction.getTime());
	dateActionNew.setMonth(dateAction.getMonth());
	dateActionNew.setYear(dateAction.getYear());
	dateActionNew.setHours(15);
	dateActionNew.setMinutes(59);
	dateActionNew.setSeconds(59);
	return dateActionNew;
}

	public static XesLog findById(Long xesLogId) throws EdmException
	{
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("id", xesLogId));
		
		if (!criteria.list().isEmpty())
		return (XesLog) criteria.list().get(0);
		else
		return null;
	}

    /**
     * Wykonuje  query wedlug zadanych kryteriow
     * ustawia  order by id  desc
     * zwraca liste obiektow XesLoga powiazanych z jednym dokumentem
     * @param fromInxed
     * @param toIndex
     * @param descBy
     * @return
     * @throws EdmException
     */
    public static XesLog findByDocumentId(long documentId) throws EdmException
    {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("documentId", documentId));
        criteria.addOrder(Order.desc("id"));
        List<XesLog> l = criteria.list();
        if(!l.isEmpty())
            return (XesLog) l.get(0);
        else return null;
    }

    public static XesLog findByUserIdAndDate(long userId, Date time) throws EdmException
    {
		Date data = DateUtils.startOfDay(time);
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("userId", userId));
        criteria.add(Restrictions.eq("creatingTime", data));
        criteria.addOrder(Order.desc("id"));
        List<XesLog> l = criteria.list();
        if(!l.isEmpty())
            return (XesLog) l.get(0);
        else return null;
    }

    public static File findByUserIdAndDate(long userId, Date starttime, Date endtime, boolean isRequest) throws EdmException, URISyntaxException {
        if(!DateUtils.isGreaterDate(starttime, endtime)) {

            starttime = DateUtils.startOfDay(starttime);
            endtime = DateUtils.endOfDay(endtime);

            Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq("userId", userId));
            criteria.add(Restrictions.ge("creatingTime", starttime));
            criteria.add(Restrictions.le("creatingTime", endtime));
            criteria.addOrder(Order.desc("id"));
            List<XesLog> xesLogSesult = criteria.list();
            if (!xesLogSesult.isEmpty()) {
                XesXmlFactory xesFactory = new XesXmlFactory();

                StringBuilder path = new StringBuilder();
                path.append(XesLogType.USER);
                path.append("_DATA_OD_"+DateUtils.formatSqlDate(starttime)+"_DATA_DO_"+DateUtils.formatSqlDate(starttime));
                String xesFileName = path.toString();

                return xesFactory.generateXesXmlFromResults(xesLogSesult, false, xesFileName, isRequest);
            } else {
            	throw new EdmException("Nie znale�ono u�ytkownika o podanym id " + userId);
            }
        } else {
        	throw new EdmException("Data rozpocz�cia jest po�niejsza od daty zako�czenia!");
        }
    }

    private static Criteria getCriteria() throws EdmException {
        return DSApi.context().session().createCriteria(XesLog.class);
    }


    public Long getVersionId()
	{
		return versionId;
	}


	public void setVersionId(Long versionId)
	{
		this.versionId = versionId;
	}

	
	public Set<XesLogData> getXesLogsDatas()
	{
		if (xesLogsDatas == null)
			xesLogsDatas = new LinkedHashSet<XesLogData>();
		return xesLogsDatas;

	}

	public void addXesLogData(XesLogData xld)
	{
		if (xld != null)
		{ getXesLogsDatas();
			xesLogsDatas.add(xld);

		}

	}


	public void setXesLogsDatas(Set<XesLogData> xesLogsDatas)
	{
		this.xesLogsDatas = xesLogsDatas;
	}


	public String getActivitiId()
	{
		return activitiId;
	}


	public void setActivitiId(String activitiId)
	{
		this.activitiId = activitiId;
	}


	public Date getCreatingTime()
	{
		return creatingTime;
	}


	public void setCreatingTime(Date creatingTime)
	{
		this.creatingTime = creatingTime;
	}

	public XesLogType getXesLogType()
	{
		if (xesLogType != null && xesLogType >= 0 && xesLogType < XesLogType.values().length)
			return XesLogType.values()[xesLogType];
		else
			return XesLogType.UNDEFINED;
	}

	public Integer getXesLogTypeInt()
	{
		return xesLogType;
	}

	void setXesLogType(Integer xesLogType)
	{
		this.xesLogType = xesLogType;
	}

	public void setXesLogType(XesLogType xesLogType)
	{
		this.xesLogType = xesLogType.ordinal();
	}


	public boolean isActiveLog()
	{
		return activeLog;
	}


	public void setActiveLog(boolean activeLog)
	{
		this.activeLog = activeLog;
	}


	public void setVersionId(long versionId)
	{
		this.versionId = versionId;
	}

	public Long getId()
	{
		return id;
	}


	public void setId(Long id)
	{
		this.id = id;
	}


	public Long getUserId()
	{
		return userId;
	}


	public void setUserId(Long userId)
	{
		this.userId = userId;
	}


	public Long getDocumentId()
	{
		return documentId;
	}


	public void setDocumentId(Long documentId)
	{
		this.documentId = documentId;
	}


	public Date getActionTime()
	{
		return actionTime;
	}


	public void setActionTime(Date actionTime)
	{
		this.actionTime = actionTime;
	}


	public String getActionName()
	{
		return actionName;
	}


	public void setActionName(String actionName)
	{
		this.actionName = actionName;
	}


	public Long getActionDataId()
	{
		return actionDataId;
	}


	public void setActionDataId(Long actionDataId)
	{
		this.actionDataId = actionDataId;
	}


	public String getActionClass()
	{
		return actionClass;
	}


	public void setActionClass(String actionClass)
	{
		this.actionClass = actionClass;
	}

	
	public Long getCaseId()
	{
		return caseId;
	}

	
	public void setCaseId(Long caseId)
	{
		this.caseId = caseId;
	}

	
	public Long getOfficeFolderId()
	{
		return officeFolderId;
	}

	
	public void setOfficeFolderId(Long officeFolderId)
	{
		this.officeFolderId = officeFolderId;
	}

	
	public String getActionUri()
	{
		return actionUri;
	}

	
	public void setActionUri(String actionUri)
	{
		this.actionUri = actionUri;
	}



	public static void createAcceptedDecretation(OfficeDocument document, String previousAssignee, String currentAssignee) throws UserNotFoundException, EdmException
	{
		XesLog xl = new XesLog();
		xl.setActionClass("pl.compan.docusafe.web.office.AcceptWfAssignmentAction");
		xl.setActionDataId(NOTEXIST);
		xl.setActionName("doAccept");
		xl.setActionTime(new Date());
		xl.setActionUri("/docusafe/office/accept-wf-assignment.action");
		xl.setUserId(DSApi.context().getDSUser().getId());
		xl.setActivitiId("Brak");
		xl.setXesLogType(XesLogType.DOCUMENT);
		xl.setDocumentId(document.getId());
		
		XesLogService.addXesLog(xl);
		
	}


}
