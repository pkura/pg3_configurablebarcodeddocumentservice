package pl.compan.docusafe.core.xes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.collections.map.MultiKeyMap;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cache.XesLoggedAcionCacheBuilder;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;


public class XesLoggedActionsTypes
{

	public static final Logger log = LoggerFactory.getLogger(XesLoggedActionsTypes.class);
	private static  boolean casEnabled = AvailabilityManager.isAvailable("cas.enabled");
	private Long id;
	private String actionClass;
	private boolean logThisAction;
	private String actionName;
	private String showingName;
	private String uri;
	private String methodName;
	private String setingName;
	private static MultiKeyMap mapedNames =  new MultiKeyMap();
	protected static String generateXlogData = "xesLog";
	private static  MultiKeyMap multikeyMap = new MultiKeyMap();
	
	private static boolean xesLogApiAvailable = AvailabilityManager.isAvailable("XesLogApi");
	
	/**
	 * metoda zwrawa mape z dwoma kluczami bo wyst�puja w systemie akcje o tej samej nazwie np create  gdzie:
	 * @pierwszym kluczem jest nazwa akcji 
	 * @drugim kluczem jest Uri akcji 
	 * @wartosc jest nazwa metody do wyswietlenia w xesLogu
	 * @return
	 * @throws EdmException
	 */
	public static MultiKeyMap getMapedNames() throws EdmException
	{
		
		Criteria criteria = DSApi.context().session().createCriteria(XesLoggedActionsTypes.class);
		criteria.add(Restrictions.eq("logThisAction", true));
		List<XesLoggedActionsTypes> list = criteria.list();
		if (!list.isEmpty())
			for (XesLoggedActionsTypes xlat : list)
					mapedNames.put(xlat.getActionName() , xlat.getUri(),xlat.getShowingName());
		return mapedNames;

	}
	
	/**
	 * metoda zwrawa mape z dwoma kluczami bo wyst�puja w systemie akcje o tej samej nazwie np create  gdzie:
	 * @pierwszym kluczem jest nazwa akcji 
	 * @drugim kluczem jest Uri akcji 
	 * @wartosc jest nazwa metody do wykonania dla tej akcji 
	 * @return
	 * @throws EdmException
	 */
	public static MultiKeyMap getMultiKeyMap() throws EdmException
	{
		multikeyMap = new MultiKeyMap();
		Criteria criteria = DSApi.context().session().createCriteria(XesLoggedActionsTypes.class);
		criteria.add(Restrictions.eq("logThisAction", true));
		List<XesLoggedActionsTypes> list = criteria.list();
		if (!list.isEmpty())
			for (XesLoggedActionsTypes xlat : list)
				multikeyMap.put(xlat.getActionName(),xlat.getUri(),xlat.getMethodName());
		return multikeyMap;

	}
	/**@
	 * Cache-owana Mapa (String,Boolean) Nazwa AKcji -> true/false 
	 * zawieraj�ca akcje logowane do xesLoga po parametrze  event Action Support
	 */	
	public static Cache<String, Boolean> LoggedActioinMap = CacheBuilder.newBuilder().expireAfterWrite(2, TimeUnit.MINUTES).build();
	
	/**@
	 * Cache-owana Mapa (String,Boolean) Nazwa AKcji -> true/false 
	 * zawieraj�ca akcje logowane do xesLoga po parametrze uri akcji
	 */
	public static Cache<String, Boolean> LoggedActioinMapUri = CacheBuilder.newBuilder().expireAfterWrite(2, TimeUnit.MINUTES).build();


	public XesLoggedActionsTypes()
	{

	}
	public XesLoggedActionsTypes(Long id, String actionClass, boolean logThisAction, String actionName,String methodName, String uri, String showingName)
	{
		super();
		this.id = id;
		this.actionClass = actionClass;
		this.logThisAction = logThisAction;
		this.actionName = actionName;
		this.uri = uri;
		this.methodName = methodName;
		this.showingName = showingName;
	}

	public void create() throws HibernateException, EdmException
	{
		log.debug("create()");

		DSApi.context().session().save(this);
		DSApi.context().session().flush();
	}

	public void update() throws HibernateException, EdmException
	{
		log.debug("update()");

		DSApi.context().session().update(this);
		DSApi.context().session().flush();
	}

	public void delete() throws HibernateException, EdmException
	{
		log.debug("delete()");

		DSApi.context().session().delete(this);
		DSApi.context().session().flush();
	}



	public static boolean isActionLoggingEnableByActionClass(String actionClass) throws EdmException
	{
		if(xesLogApiAvailable){
		Criteria criteria = DSApi.context().session().createCriteria(XesLoggedActionsTypes.class);
		criteria.add(Restrictions.eq("actionClass", actionClass));
		List<XesLoggedActionsTypes> list = criteria.list();
		if (!list.isEmpty())
		{
			return (Boolean) list.get(0).getLogThisAction();
		}
		}
		return false;

	}
	
	public static Boolean isActionLoggingEnableByUri(String requestURI) throws EdmException
	{
		if(xesLogApiAvailable){
			requestURI = revaluateUriCas(requestURI);
		Criteria criteria = DSApi.context().session().createCriteria(XesLoggedActionsTypes.class);
		criteria.add(Restrictions.eq("uri", requestURI));
		List<XesLoggedActionsTypes> list = criteria.list();
		if (!list.isEmpty())
		{
			return (Boolean) list.get(0).getLogThisAction();
		}else {
			return false;
		}
		}
		return false;
		
	}

	private static String revaluateUriCas(String requestURI)
	{
		if (casEnabled) {
			if(requestURI!=null && !requestURI.isEmpty() && !requestURI.startsWith("/docusafe"))
			requestURI = "/docusafe"+requestURI;
		}
		else {
			if(requestURI!=null && !requestURI.isEmpty() && !requestURI.startsWith("/docusafe")){
				requestURI = "/docusafe"+requestURI;
			}
		}
		return requestURI;
	}
    public static Boolean isActionLoggingEnableByUriAndAction(String action ,String actionClass) throws EdmException
    {
        Boolean b = false;
        try{
        	 String uri=  (ServletActionContext.getRequest()!=null? ServletActionContext.getRequest().getRequestURI() : "" );
            b =  isActionLoggingEnableByUriAndAction(uri ,action ,actionClass);
        }catch(Exception e){
            log.error("", e);
            return b;
            //zrobi� co� podczas b��du, wyst�puje b��d springowy
        }

        return b;
    }

	public static Boolean isActionLoggingEnableByUriAndAction(String requestURI , String action ,String actionClass) throws EdmException
	{
		if(xesLogApiAvailable){
			requestURI = revaluateUriCas(requestURI);
			 log.error("request", requestURI);
       	
		Criteria criteria = DSApi.context().session().createCriteria(XesLoggedActionsTypes.class);
		if(requestURI.isEmpty() && ("login".equals(action) || "logout".equals(action))){
		//dla tych akcji  bez uri pozwalamy na logowanie xesa jesli jest mozliwe
		}
		else 
			criteria.add(Restrictions.eq("uri", requestURI));
		
		
		Disjunction or = Restrictions.disjunction();
		if(action!=null)
			or.add(Restrictions.eq("actionName",action));
			if(actionClass!=null)
			or.add(Restrictions.eq("actionClass",actionClass));
		criteria.add(or);
		
		List<XesLoggedActionsTypes> list = criteria.list();
		if (!list.isEmpty())
		{
			return (Boolean) list.get(0).getLogThisAction();
		}else {
			return false;
		}
		}
		return false;
		
	}
	protected static Boolean getValueUri(String requestURI) throws EdmException
	{
		return isActionLoggingEnableByUri(requestURI);
	}

	static boolean checIsLogingEnable(final String actionName) throws ExecutionException
	{
		return LoggedActioinMap.get(actionName, new Callable<Boolean>() {

			@Override
			public Boolean call() throws Exception
			{
				return getValue(actionName);
			}
		});
	}

	
	public static boolean isXesLogForThisActionUri(final String requestURI) throws ExecutionException
	{
		
		return LoggedActioinMapUri.get(requestURI, new Callable<Boolean>() {

			@Override
			public Boolean call() throws Exception
			{
				return getValueUri(requestURI);
			}
		});

	}
	protected static Boolean getValue(String actionClass) throws EdmException
	{
		return isActionLoggingEnableByActionClass(actionClass);
	}
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getActionClass()
	{
		return actionClass;
	}

	public void setActionClass(String actionClass)
	{
		this.actionClass = actionClass;
	}

	public boolean getLogThisAction()
	{
		return logThisAction;
	}

	public void setLogThisAction(boolean logThisAction)
	{
		this.logThisAction = logThisAction;
	}

	public String getActionName()
	{
		return actionName;
	}

	public void setActionName(String actionName)
	{
		this.actionName = actionName;
	}

	public String getShowingName()
	{
		return showingName;
	}

	public void setShowingName(String showingName)
	{
		this.showingName = showingName;
	}
	public String getUri()
	{
		return uri;
	}

	public void setUri(String uri)
	{
		this.uri = uri;
	}

	
	public String getMethodName()
	{
		return methodName;
	}

	
	public void setMethodName(String methodName)
	{
		this.methodName = methodName;
	}

	
	public static MultiKeyMap getMultikeyMap()
	{
		return multikeyMap;
	}

	
	public static void setMultikeyMap(MultiKeyMap multikeyMap)
	{
		XesLoggedActionsTypes.multikeyMap = multikeyMap;
	}

	
	public static void setMapedNames(MultiKeyMap mapedNames)
	{
		XesLoggedActionsTypes.mapedNames = mapedNames;
	}

	
	public String getSettingName()
	{
		return setingName;
	}

	
	public void setSettingName(String settingsName)
	{
		this.setingName = settingsName;
	}


}
