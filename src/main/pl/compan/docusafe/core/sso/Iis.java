package pl.compan.docusafe.core.sso;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.ima.dictionary.Mediator;

@Entity
@Table(name = "sso")
public class Iis implements Lifecycle {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idsso", nullable = false)
	private Integer idsso;

	@Column(name = "login_ad", nullable = true)
	private String login_ad;

	@Column(name = "guid_source", nullable = true)
	private String guid_source;

	@Column(name = "guid_sso", nullable = true)
	private String guid_sso;

	public Integer getIdsso() {
		return idsso;
	}

	public void setIdsso(Integer idsso) {
		this.idsso = idsso;
	}

	public String getLogin_ad() {
		return login_ad;
	}

	public void setLogin_ad(String login_ad) {
		this.login_ad = login_ad;
	}

	public String getGuid_source() {
		return guid_source;
	}

	public void setGuid_source(String guid_source) {
		this.guid_source = guid_source;
	}

	public String getGuid_sso() {
		return guid_sso;
	}

	public void setGuid_sso(String guid_sso) {
		this.guid_sso = guid_sso;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Iis))
			return false;

		final Iis doc = (Iis) o;

		if (idsso != null ? !(idsso.equals(doc.idsso)) : doc.idsso != null)
			return false;

		return true;
	}

	public int hashCode() {
		return (idsso != null ? idsso.hashCode() : 0);
	}

	public boolean onSave(Session s) throws CallbackException {
		return false;
	}

	public boolean onUpdate(Session s) throws CallbackException {
		return false;
	}

	public boolean onDelete(Session s) throws CallbackException {
		return false;
	}

	public void onLoad(Session s, Serializable id) {
	}

	public String toString() {
		return idsso.toString();
	}

	public final void create() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	/**
	 * Metoda wyszukuje obiekt Sso po parametrze Guid zwr�conym przez SSO
	 * @param _guid_sso
	 * @return
	 * @throws EdmException	
	 */
	public static Iis findByGuidSso(String _guid_sso) throws EdmException {
				
		Iis _iis =  (Iis) (DSApi.context().session().createCriteria(Iis.class)
				.add(Restrictions.eq("guid_sso", _guid_sso)).list().get(0));
							
		return _iis;
	}
}
