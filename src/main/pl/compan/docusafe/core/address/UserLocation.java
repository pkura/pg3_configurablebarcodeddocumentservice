package pl.compan.docusafe.core.address;

import java.io.Serializable;

/**
 * Klasa encji odwzorowuj�ca tabele DS_USER_LOCATION
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class UserLocation implements Serializable 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identyfikator
	 */
	private Long id;
	
	/**
	 * Parametr X lokalizacji u�ytkownika na obrazie pi�tra
	 */
	private int paramX;
	
	/**
	 * Parametr Y lokalizacji u�ytkownika na obrazie pi�tra
	 */
	private int paramY;
	
	/**
	 * Pi�tro, na kt�rym znajduje si� u�ytkownik
	 */
	private Floor floor;
	
	/**
	 * Domy�lny konstruktor
	 */
	public UserLocation() 
	{
	}
	
	/**
	 * Konstruktor ustawiaj�cy wszystkie pola opr�cz identyfikatora
	 * 
	 * @param paramX
	 * @param paramY
	 * @param floor
	 */
	public UserLocation(int paramX, int paramY, Floor floor) 
	{
		super();
		this.paramX = paramX;
		this.paramY = paramY;
		this.floor = floor;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� id
	 * 
	 * @return
	 */
	public Long getId() 
	{
		return id;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� id
	 * 
	 * @param id
	 */
	public void setId(Long id) 
	{
		this.id = id;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� paramX
	 * 
	 * @return
	 */
	public int getParamX() 
	{
		return paramX;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� paramX
	 * 
	 * @param paramX
	 */
	public void setParamX(int paramX) 
	{
		this.paramX = paramX;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� paramY
	 * 
	 * @return
	 */
	public int getParamY() 
	{
		return paramY;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� paramY
	 * 
	 * @param paramY
	 */
	public void setParamY(int paramY) 
	{
		this.paramY = paramY;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� floor
	 * 
	 * @return
	 */
	public Floor getFloor() 
	{
		return floor;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� floor
	 * 
	 * @param floor
	 */
	public void setFloor(Floor floor) 
	{
		this.floor = floor;
	}
}
