package pl.compan.docusafe.core.address;

import java.io.Serializable;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Image;

public class Floor implements Serializable 
{

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identyfikator pi�tra
	 */
	private Long id;
	
	/**
	 * Nazwa danego pi�tra
	 */
	private String name;
	
	/**
	 * Kr�tki opis danego pi�tra
	 */
	private String description;
	
	/**
	 * Adres, z kt�rym powi�zane jest pi�tro
	 */
	private Address address;
	
	/**
	 * Obraz danego pi�tra
	 */
	private Image image;
	
	/**
	 * Domy�lny konstruktor
	 */
	public Floor() 
	{
	}

	/**
	 * Konstruktor ustawiaj�cy wszystkie pola
	 * 
	 * @param name
	 * @param description
	 * @param address
	 */
	public Floor(String name, String description, Address address) 
	{
		super();
		this.name = name;
		this.description = description;
		this.address = address;
	}

	/**
	 * Metoda usuwaj�ca dany obiekt z bazy danych
	 * 
	 * @throws EdmException
	 */
	public void delete() throws EdmException
    {
        Persister.delete(this);
    }

	/**
	 * Metoda zwracaj�ca sk�adow� id
	 * 
	 * @return
	 */
	public Long getId() 
	{
		return id;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� id
	 * 
	 * @param id
	 */
	public void setId(Long id) 
	{
		this.id = id;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� name
	 * 
	 * @return
	 */
	public String getName() 
	{
		return name;
	}

	/**
	 * Metoda ustawiaj�ca sk�adowa name
	 * 
	 * @param name
	 */
	public void setName(String name) 
	{
		this.name = name;
	}
	
	/**
	 * Metoda zwracaj�ca sk�adow� description
	 * 
	 * @return
	 */
	public String getDescription() 
	{
		return description;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� description
	 * 
	 * @param description
	 */
	public void setDescription(String description) 
	{
		this.description = description;
	}

	/**
	 * Zwraca sk�adow� address
	 * 
	 * @return
	 */
	public Address getAddress() 
	{
		return address;
	}

	/**
	 * Ustawia sk�adow� address
	 * 
	 * @param address
	 */
	public void setAddress(Address address) 
	{
		this.address = address;
	}
	
	/**
	 * Metoda zwracaj�ca sk�adow� image
	 * 
	 * @return
	 */
	public Image getImage() 
	{
		return image;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� image
	 * 
	 * @param image
	 */
	public void setImage(Image image) 
	{
		this.image = image;
	}

	/**
	 * Metoda statyczna wyszukuj�ca pi�tro po podanym identyfikatorze
	 * 
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public static Floor find(Long id)
		throws EdmException
	{
		return (Floor) Finder.find(Floor.class, id);
	}
}
