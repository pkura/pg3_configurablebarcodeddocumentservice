package pl.compan.docusafe.core.address;

import java.io.Serializable;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;

/**
 * Klasa reprezentująca encję DS_PHONE
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class Phone implements Serializable 
{

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identyfikator numeru
	 */
	private Long id;
	
	/**
	 * Nazwa numeru
	 */
	private String name;
	
	/**
	 * Typ numeru
	 */
	private String type;
	
	/**
	 * Numer telofonu, faxu
	 */
	private String number;
	
	/**
	 * Adres organizacji z którą dany numer jest powiązany
	 */
	private Address address;
	
	/**
	 * Domyślny konstruktor.
	 */
	public Phone() 
	{
	}
	
	/**
	 * Konstruktor ustawiający wszystkie pola.
	 * 
	 * @param name
	 * @param type
	 * @param number
	 * @param address
	 */
	public Phone(String name, String type, String number, Address address) 
	{
		super();
		this.name = name;
		this.type = type;
		this.number = number;
		this.address = address;
	}
	
	/**
	 * Metoda usuwająca dany obiekt z bazy danych
	 * 
	 * @throws EdmException
	 */
	public void delete() throws EdmException
    {
        Persister.delete(this);
    }

	/**
	 * Metoda zwracająca składową id
	 * 
	 * @return
	 */
	public Long getId() 
	{
		return id;
	}

	/**
	 * Metoda ustawiająca składową id
	 * 
	 * @param id
	 */
	public void setId(Long id) 
	{
		this.id = id;
	}

	/**
	 * Metoda zwracająca składową name
	 * 
	 * @return
	 */
	public String getName() 
	{
		return name;
	}

	/**
	 * Metoda ustawiająca składową name
	 * 
	 * @param name
	 */
	public void setName(String name) 
	{
		this.name = name;
	}

	/**
	 * Metoda ustawiająca składową type
	 * 
	 * @return
	 */
	public String getType() 
	{
		return type;
	}

	/**
	 * Metoda ustawiająca składową type
	 * 
	 * @param type
	 */
	public void setType(String type) 
	{
		this.type = type;
	}

	/**
	 * Metoda zwracająca składową number
	 * 
	 * @return
	 */
	public String getNumber() 
	{
		return number;
	}

	/**
	 * Metoda ustawiająca składową number
	 * 
	 * @param number
	 */
	public void setNumber(String number) 
	{
		this.number = number;
	}

	/**
	 * Metoda zwracająca składową address
	 * 
	 * @return
	 */
	public Address getAddress() 
	{
		return address;
	}

	/**
	 * Metoda ustawiająca składową address
	 * 
	 * @param address
	 */
	public void setAddress(Address address) 
	{
		this.address = address;
	}
	
	/**
	 * Metoda statyczna wyszukująca telefon po podanym identyfikatorze
	 * 
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public static Phone find(Long id)
		throws EdmException
	{
		return (Phone) Finder.find(Phone.class, id);
	}
}
