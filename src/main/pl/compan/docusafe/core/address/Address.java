package pl.compan.docusafe.core.address;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Image;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa reprezentuj�ca encj� DS_ADDRESS
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class Address implements Serializable 
{
	@SuppressWarnings("unused")
	private static Logger log = LoggerFactory.getLogger(Address.class);
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identyfiaktor adresu organizacji
	 */
	private Long id;
	
	/**
	 * Nazwa kodowa adresu organizacji
	 */
	private String name;
	
	/**
	 * Nazwa organizacji
	 */
	private String organization;
	
	/**
	 * Nazwa ulicy, na kt�rej mie�ci si� organizacja
	 */
	private String street;
	
	/**
	 * Kod pocztowy
	 */
	private String zipCode;
	
	/**
	 * Miasto, w kt�rym mie�ci si� organizacja
	 */
	private String city;
	
	/**
	 * Region, w kt�rym mie�ci si� organizacja - wojew�dztwo
	 */
	private String region;
	
	/**
	 * Zdj�cie, mapka dojazdu do danej lokalizacji
	 */
	private Image image;
	
	/**
	 * Lista telefon�w do organizacji
	 */
	private List<Phone> phones;
	
	/**
	 * Lista z pi�trami danej organizacji
	 */
	private List<Floor> floors;
	
	/**
	 * Czy dany adres jest central�
	 */
	private boolean headOffice;
	
	/**
	 * Konstruktor domy�lny
	 */
	public Address() 
	{
	}
	
	/**
	 * Konstruktor ustawiaj�cy wszystkie pola, opr�cz listy numer�w
	 * 
	 * @param name
	 * @param organization
	 * @param street
	 * @param zipCode
	 * @param city
	 * @param region
	 */
	public Address(String name, String organization, String street,
			String zipCode, String city, String region) 
	{
		super();
		this.name = name;
		this.organization = organization;
		this.street = street;
		this.zipCode = zipCode;
		this.city = city;
		this.region = region;
	}
	
	/**
	 * Metoda usuwaj�ca dany obiekt z bazy danych
	 * 
	 * @throws EdmException
	 */
	public void delete() throws EdmException
    {
        Persister.delete(this);
    }
	
	/**
	 * Metoda zwracaj�ca sk�adow� id
	 * 
	 * @return
	 */
	public Long getId()
	{
		return id;
	}
	
	/**
	 * Metoda ustawiaj�ca sk�adow� id
	 * 
	 * @param id
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� name
	 * 
	 * @return
	 */
	public String getName() 
	{
		return name;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� name
	 * 
	 * @param name
	 */
	public void setName(String name) 
	{
		this.name = name;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� organization
	 * 
	 * @return
	 */
	public String getOrganization() 
	{
		return organization;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� organization
	 * 
	 * @param organization
	 */
	public void setOrganization(String organization) 
	{
		this.organization = organization;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� street
	 * 
	 * @return
	 */
	public String getStreet() 
	{
		return street;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� street
	 * 
	 * @param street
	 */
	public void setStreet(String street) 
	{
		this.street = street;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� zipCode
	 * 
	 * @return
	 */
	public String getZipCode() 
	{
		return zipCode;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� zipCode
	 * 
	 * @param zipCode
	 */
	public void setZipCode(String zipCode) 
	{
		this.zipCode = zipCode;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� city
	 * 
	 * @return
	 */
	public String getCity() 
	{
		return city;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� city
	 * 
	 * @param city
	 */
	public void setCity(String city) 
	{
		this.city = city;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� region
	 * 
	 * @return
	 */
	public String getRegion() 
	{
		return region;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� region.
	 * 
	 * @param region
	 */
	public void setRegion(String region) 
	{
		this.region = region;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� image
	 * 
	 * @return
	 */
	public Image getImage() 
	{
		return image;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� image
	 * 
	 * @param image
	 */
	public void setImage(Image image) 
	{
		this.image = image;
	}

	/**
	 * Metoda zwracaj�ca list� telefon�w organizacji
	 * 
	 * @return
	 */
	public List<Phone> getPhones() 
	{
		if (phones == null)
			phones = new ArrayList<Phone>();
		return phones;
	}
	
	/**
	 * Metoda ustawiaj�ca list� telefon�w do organizacji
	 * 
	 * @param phones
	 */
	public void setPhones(List<Phone> phones) 
	{
		this.phones = phones;
	}
	
	/**
	 * Metoda zwracaj�ca sk�adow� floors
	 * 
	 * @return
	 */
	public List<Floor> getFloors() 
	{
		if (floors == null)
			floors = new ArrayList<Floor>();
		return floors;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� floors
	 * 
	 * @param floors
	 */
	public void setFloors(List<Floor> floors) 
	{
		this.floors = floors;
	}
	
	/**
	 * Metoda zwracaj�ca sk�adow� headOffice
	 * 
	 * @return
	 */
	public boolean isHeadOffice() 
	{
		return headOffice;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� headOffice
	 * 
	 * @param headOffice
	 */
	public void setHeadOffice(boolean headOffice) 
	{
		this.headOffice = headOffice;
	}
	
	
	/**
	 * Metoda pobieraj�ca list� numer�w telefon�w
	 * 
	 * @throws EdmException
	 */
	public void loadPhonesList() throws EdmException
	{
		if (id == null || id <= 0) 
			return;
		
		String sql = "from " + Phone.class.getName() + " p " +
		"where p.address = ?";
		Query query = DSApi.context().session().createQuery(sql);
		query.setParameter(0, this);
		phones = query.list();
	}
	
	/**
	 * Metoda pobieraj�ca list� pi�ter
	 * 
	 * @throws EdmException
	 */
	public void loadFloorsList() throws EdmException
	{
		if (id == null || id <= 0) 
			return;
		
		String sql = "from " + Floor.class.getName() + " f " +
		"where f.address = ?";
		Query query = DSApi.context().session().createQuery(sql);
		query.setParameter(0, this);
		floors = query.list();
	}
	
	/**
	 * Metoda statyczna wyszukuj�ca adres po podanym identyfikatorze
	 * 
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public static Address find(Long id)
		throws EdmException
	{
		return (Address) Finder.find(Address.class, id);
	}
	
	/**
	 * Metoda statyczna zwracaj�ca list� wszystkich zapisanych adres�w
	 * 
	 * @return
	 * @throws EdmException
	 */
	public static List<Address> getAll()
		throws EdmException
	{
		String sql = "select a from a in class " + Address.class.getName();
		Query query = DSApi.context().session().createQuery(sql);
		return query.list();
	}
}
