package pl.compan.docusafe.core;

import pl.compan.docusafe.util.StringManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * Abstrakcyjna klasa z implementacjami dla GIODO i dla normalnych ludzi.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PasswordPolicy.java,v 1.15 2009/03/06 12:45:56 pecet3 Exp $
 */
public abstract class PasswordPolicy
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public static final String NODE_PASSWORD_POLICY = "modules/coreoffice/passwordpolicy";

    public static final String KEY_PASSWORD_EXPIRES = "passwordExpires";
    public static final String KEY_DAYS_TO_EXPIRE = "daysToExpire";
    public static final String KEY_MIN_PASSWORD_LENGTH = "minPasswordLength";
    public static final String KEY_MUST_HAVE_DIGITS = "mustHaveDigits";
    public static final String KEY_HOW_MANY_DIGITS = "howManyDigits";
    public static final String KEY_MUST_HAVE_LETTERS = "mustHaveLetters";
    public static final String KEY_HOW_MANY_LETTERS = "howManyLetters";
    public static final String KEY_MUST_HAVE_SMALL_LETTERS = "mustHaveSmallLetters";
    public static final String KEY_HOW_MANY_SMALL_LETTERS = "howManySmallLetters";
    public static final String KEY_MUST_HAVE_SPECIALS = "mustHaveSpecials";
    public static final String KEY_HOW_MANY_SPECIALS = "howManySpecials";
    public static final String KEY_LOCKING_PASSWORDS = "lockingPasswords";
    public static final String KEY_HOW_MANY_INCORRECT_LOGINS = "howManyIncorrectLogins";
    public static final String KEY_MUST_CHANGE_PASSWORD_AT_FIRST_LOGON = "changePasswordAtFirstLogon";

    public static final int KEY_MIN_PASSWORD_LENGTH_DEFAULT = 6;

    public static final int MAX_PASSWORD_LENGTH = 30;

    /**
     * Klucz, pod kt�rym w ServletContext trzymany jest obiekt java.util.Properties
     * utworzony przez metod� {@link #getProperties(java.util.prefs.Preferences)}.
     */
    public static final String PROPERTIES_KEY = PasswordPolicy.class.getName()+".PROPERTIES_KEY";

    /**
     * Klucz, pod kt�rym w sesji u�ytkownika trzymana jest informacja o liczbie
     * dni, jakie pozosta�y mu do wyga�ni�cia has�a (Integer).
     */
    public static final String DAYS_LEFT_KEY = PasswordPolicy.class.getName()+".DAYS_LEFT";

    public static java.util.Properties getProperties(Preferences prefs)
    {
        java.util.Properties props = new java.util.Properties();

        props.put(KEY_PASSWORD_EXPIRES, new Boolean(prefs.getBoolean(KEY_PASSWORD_EXPIRES, false)));
        props.put(KEY_DAYS_TO_EXPIRE, new Integer(prefs.getInt(KEY_DAYS_TO_EXPIRE, 0)));
        props.put(KEY_MIN_PASSWORD_LENGTH, new Integer(prefs.getInt(KEY_MIN_PASSWORD_LENGTH, KEY_MIN_PASSWORD_LENGTH_DEFAULT)));
        props.put(KEY_MUST_HAVE_DIGITS, new Boolean(prefs.getBoolean(KEY_MUST_HAVE_DIGITS, false)));
        props.put(KEY_HOW_MANY_DIGITS, new Integer(prefs.getInt(KEY_HOW_MANY_DIGITS, 0)));
        props.put(KEY_MUST_HAVE_LETTERS, new Boolean(prefs.getBoolean(KEY_MUST_HAVE_LETTERS, false)));
        props.put(KEY_HOW_MANY_LETTERS, new Integer(prefs.getInt(KEY_HOW_MANY_LETTERS, 0)));
        props.put(KEY_MUST_HAVE_SMALL_LETTERS, new Boolean(prefs.getBoolean(KEY_MUST_HAVE_SMALL_LETTERS, false)));
        props.put(KEY_HOW_MANY_SMALL_LETTERS, new Integer(prefs.getInt(KEY_HOW_MANY_SMALL_LETTERS, 0)));
        props.put(KEY_MUST_HAVE_SPECIALS, new Boolean(prefs.getBoolean(KEY_MUST_HAVE_SPECIALS, false)));
        props.put(KEY_HOW_MANY_SPECIALS, new Integer(prefs.getInt(KEY_HOW_MANY_SPECIALS, 0)));
        props.put(KEY_LOCKING_PASSWORDS, new Boolean(prefs.getBoolean(KEY_LOCKING_PASSWORDS, false)));
        props.put(KEY_HOW_MANY_INCORRECT_LOGINS, new Integer(prefs.getInt(KEY_HOW_MANY_INCORRECT_LOGINS, 0)));
        props.put(KEY_MUST_CHANGE_PASSWORD_AT_FIRST_LOGON, new Boolean(prefs.getBoolean(KEY_MUST_CHANGE_PASSWORD_AT_FIRST_LOGON, true)));
        return props;
    }

    public static void validate(String username, String password, Preferences prefs)
        throws IllegalPasswordException
    {
        if (username == null)
            throw new NullPointerException("username");
        if (password == null)
            throw new NullPointerException("password");

        int minLength = prefs.getInt(KEY_MIN_PASSWORD_LENGTH, KEY_MIN_PASSWORD_LENGTH_DEFAULT);
        boolean mustHaveDigits = prefs.getBoolean(KEY_MUST_HAVE_DIGITS, false);
        int howManyDigits = prefs.getInt(KEY_HOW_MANY_DIGITS, 0);
        boolean mustHaveLetters =  prefs.getBoolean(KEY_MUST_HAVE_LETTERS, false);
        int howManyLetters = prefs.getInt(KEY_HOW_MANY_LETTERS, 0);
        boolean mustHaveSmallLetters =  prefs.getBoolean(KEY_MUST_HAVE_SMALL_LETTERS, false);
        int howManySmallLetters = prefs.getInt(KEY_HOW_MANY_SMALL_LETTERS, 0);
        boolean mustHaveSpecials =  prefs.getBoolean(KEY_MUST_HAVE_SPECIALS, false);
        int howManySpecials = prefs.getInt(KEY_HOW_MANY_SPECIALS, 0);

        List<String> messages = new ArrayList<String>(6);

        if (password.length() < minLength)
        {
            messages.add("Has�o musi mie� przynajmniej "+minLength+" znak�w");
        }
        
        String smallPassword = password.toLowerCase();
        String smallUsername = username.toLowerCase();
        
        if (smallPassword.indexOf(smallUsername) >= 0)
        {
            messages.add("Has�o nie mo�e zawiera� w sobie nazwy u�ytkownika");
        }

        if (mustHaveDigits && howManyDigits > 0)
        {
            int countDigits = 0;
            for (int i=0; i < password.length(); i++)
            {
                if (Character.isDigit(password.charAt(i)))
                    countDigits++;
            }

            if (countDigits < howManyDigits)
                messages.add("W ha�le " + ((howManyDigits==1||howManyDigits>4)?"musi":"musz�") + " si� znale�� co najmniej "+howManyDigits+
                    (howManyDigits == 1 ? " cyfra" : (howManyDigits > 4 ? " cyfr" : " cyfry")));
        }
        
        if (mustHaveLetters && howManyLetters > 0)
        {
            int countLetters = 0;
            for (int i=0; i < password.length(); i++)
            {
                if (Character.isLetter(password.charAt(i)) && (!Character.isLowerCase(password.charAt(i))))
                    countLetters++;
            }

            if (countLetters < howManyLetters)
                messages.add("W ha�le musi si� znale�� co najmniej "+howManyLetters+
                    (howManyLetters == 1 ? " du�a litera" : (howManyLetters > 4 ? " du�ych liter" : " du�e litery")));
        }
        
        if (mustHaveSmallLetters && howManySmallLetters > 0)
        {
            int countLetters = 0;
            for (int i=0; i < password.length(); i++)
            {
                if (Character.isLetter(password.charAt(i)) && Character.isLowerCase(password.charAt(i)))
                    countLetters++;
            }

            if (countLetters < howManySmallLetters)
                messages.add("W ha�le musi si� znale�� co najmniej "+howManySmallLetters+
                    (howManySmallLetters == 1 ? " ma�a litera" : (howManySmallLetters > 4 ? " ma�ych liter" : " ma�e litery")));
        }
        
        if (mustHaveSpecials && howManySpecials > 0)
        {
            int countSpecials = 0;
            for (int i=0; i < password.length(); i++)
            {
                if (!(Character.isLetter(password.charAt(i)) || Character.isDigit(password.charAt(i))))
                    countSpecials++;
            }

            if (countSpecials < howManySpecials)
                messages.add("W ha�le musi si� znale�� co najmniej "+howManySpecials+
                    (howManySpecials == 1 ? " znak specjalny" : (howManySpecials > 4 ? " znak�w specjalnych" : " znaki specjalne")));
        }

        if (messages.size() > 0)
        {
            throw new IllegalPasswordException((String) messages.get(0),
                messages.size() > 1 ? messages.subList(1, messages.size()) : null);
        }
    }

    private static PasswordPolicy policy;

/*
    public static PasswordPolicy getInstance()
    {
        if (policy == null)
        {
            String clazz = Configuration.getProperty("password.policy");
            if (clazz == null)
                clazz = DEFAULT_CLASS;

            try
            {
                policy = (PasswordPolicy) Class.forName(clazz).newInstance();
            }
            catch (ClassNotFoundException e)
            {
                throw new RuntimeException("Nie znaleziono klasy obs�uguj�cej zarz�dzanie" +
                    " has�ami: "+clazz);
            }
            catch (Exception e)
            {
                throw new RuntimeException("B��d podczas uruchamiania klasy obs�uguj�cej " +
                    "zarz�dzanie has�ami");
            }
        }

        return policy;
    }
*/

    /**
     * Walidacja nowego has�a.
     * @param username
     * @param password
     * @throws IllegalPasswordException
     */
    public abstract void validatePassword(String username, String password)
        throws IllegalPasswordException;

    /**
     * Liczba dozwolonych logowa� po up�ywie wa�no�ci has�a.
     */
    public abstract int graceLogins();

    public abstract boolean passwordExpired(Date lastPasswordChange);

    public abstract int daysToExpire(Date lastPasswordChange);

    public abstract boolean passwordExpires();
}
