package pl.compan.docusafe.core.regtype;

import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.regtype.RegistryPrintout.ColumnInfo;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.PdfGenerator;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.select.*;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class RegistriesManager
{
    private StringManager sm=
        GlobalPreferences.loadPropertiesFile(RegistriesManager.class.getPackage().getName(),null);
    private static final Log log = LogFactory.getLog(RegistriesManager.class);

    /**
     * Wyszukiwanie wpis�w wed�ug zapytania {@link RegistryQuery}, kt�re okre�la kryteria wyszukiwania.
     * 
     * @param query
     * @return
     * @throws EdmException
     */
    public static SearchResults<RegistryEntry> search(final RegistryQuery query) throws EdmException
    {
        Registry registry = query.getRegistry();

        // ustalanie tabel, kt�re b�dziemy u�ywali
        FromClause from = new FromClause();
        Map<String,TableAlias> tables = new HashMap<String,TableAlias>();

        TableAlias registryTable = from.createTable(registry.getTablename(), true);
        tables.put(registry.getTablename(), registryTable);

        TableAlias regEntryTable = from.createTable("dsr_registry_entry", true);
        tables.put("dsr_registry_entry", regEntryTable);

        /*
        TableAlias perm = null;
        if (!DSApi.context().isAdmin() && query.isCheckPermissions())
        {
            perm = from.createTable(DSApi.getTableName(DocumentPermission.class), true);
            tables.put(DSApi.getTableName(DocumentPermission.class), perm);
        }
        */

        Map<String,RegistryQuery.JoinInfo> map = query.getTables();
        for (String key : map.keySet())
        {
            RegistryQuery.JoinInfo joinInfo = map.get(key);
            TableAlias alias;
            if (joinInfo.isMultiple())
            {
                String[] parts = key.split("#");
                alias = from.createTable(parts[0], true);
            }
            else
                alias = from.createTable(key, true);            
            tables.put(key, alias);
        }

        WhereClause where = new WhereClause();

        // dodawanie warunk�w do zapytania, aby poprawnie z��czy� tabele
        where.add(Expression.eqAttribute(regEntryTable.attribute("id"), registryTable.attribute("entry_id")));
        /*
        if (!DSApi.context().isAdmin() && query.isCheckPermissions())
        {
            where.add(Expression.eqAttribute(regEntryTable.attribute("id"), perm.attribute("entry_id")));
        }
        */

        for (String key : map.keySet())
        {
            RegistryQuery.JoinInfo joinInfo = map.get(key);
            if (joinInfo.isMultiple())
            {                
                String[] parts = key.split("#");
                where.add(Expression.eqAttribute(tables.get(key).attribute("entry_id"),
                    regEntryTable.attribute("id")));
                where.add(Expression.eq(tables.get(key).attribute("field_cn"),
                    parts[1]));
            }
            else
                where.add(Expression.eqAttribute(tables.get(key).attribute(joinInfo.getTableKeyColumn()),
                    tables.get(joinInfo.getJoinTableName()).attribute(joinInfo.getJoinTableColumn())));
        }

        // tworzenie fragmentu zapytania WHERE na podstawie warto�ci z formularza
        // wyszukiwania wype�nionego przez u�ytkownika
        query.visitQuery(new RegistryQueryVisitor(tables, where));

        // tworzenie pozosta�ego fragmentu zapytania WHERE niezwi�zanego z polami formularza
      /*  if (!DSApi.context().isAdmin())
        {
            where.add(Expression.eq(regEntryTable.attribute("deleted"), Boolean.FALSE));
        } */

        /*
        if (!DSApi.context().isAdmin() && query.isCheckPermissions())
        {
            where.add(Expression.eq(perm.attribute("name"), ObjectPermission.READ));
            where.add(Expression.eq(perm.attribute("negative"), Boolean.FALSE));
            // and ( perm.subjecttpe = Permission.SUBJECT_ANY

            Junction OR = Expression.disjunction().
                add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.ANY)).
                add(Expression.conjunction().
                    add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.USER)).
                    add(Expression.eq(perm.attribute("subject"), DSApi.context().getPrincipalName())));

            DSDivision[] divisions = DSApi.context().getDSUser().getDivisions();

            for (DSDivision division : divisions)
            {
                // tylko grupy mog� mie� uprawnienia
                if (!division.isGroup())
                    continue;
                OR.add(Expression.conjunction().
                    add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.GROUP)).
                    add(Expression.eq(perm.attribute("subject"), division.getGuid())));
            }

            where.add(OR);
        }
        */


        // tworzenie cz�� ORDER BY zapytania
        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(registryTable, "entry_id");

        OrderClause order = new OrderClause();

        FormQuery.OrderBy[] orderBy = query.getOrder();
        for (int i=0; i < orderBy.length; i++)
        {
            if ("id".equals(orderBy[i].getAttribute()))
            {
                order.add(regEntryTable.attribute("id"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(regEntryTable, "id");
            }
            else if ("ctime".equals(orderBy[i].getAttribute()))
            {
                order.add(regEntryTable.attribute("ctime"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(regEntryTable, "ctime");
            }
            else if ("sequenceNumber".equals(orderBy[i].getAttribute()))
            {
                order.add(regEntryTable.attribute("sequenceNumber"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(regEntryTable, "sequenceNumber");
            }
            /* else if ("mtime".equals(orderBy[i].getAttribute()))
            {
                order.add(regEntryTable.attribute("mtime"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(regEntryTable, "mtime");
            }
            */
            else if (orderBy[i].getAttribute().startsWith("registry_"))
            {
                try
                {
                    String fieldCn = orderBy[i].getAttribute().substring("registry_".length());
                    Field field = registry.getFieldByCn(fieldCn);
                    if (Field.CLASS.equals(field.getType()))
                    {
                        try
                        {
                            Class c = Class.forName(field.getClassname());       
                            String tableName = DSApi.getTableName(c);
                            String column = field.getDictionarySortColumn();
                            order.add(tables.get(tableName).attribute(column), Boolean.valueOf(orderBy[i].isAscending()));
                            selectId.add(tables.get(tableName), column);
                        }
                        catch (ClassNotFoundException e)
                        {
                            throw new EdmException("Nie znaleziono klasy: "+field.getClassname());
                        }
                    }
                    else
                    {
                        String column = field.getColumn();
                        order.add(registryTable.attribute(column), Boolean.valueOf(orderBy[i].isAscending()));
                        selectId.add(registryTable, column);
                    }
                }
                catch (Exception e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }


        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(distinct "+regEntryTable.getAlias()+".id)");

        SelectQuery selectQuery;

        int totalCount;
        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);

            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby wpis�w.");

            totalCount = rs.getInt(countCol.getPosition());

            rs.close();

            selectQuery = new SelectQuery(selectId, from, where, order);
            if (query.getLimit() > 0) {
                selectQuery.setMaxResults(query.getLimit());
                selectQuery.setFirstResult(query.getOffset());
            }
            //ps = selectQuery.createStatement(DSApi.context().session().connection());

            rs = selectQuery.resultSet(DSApi.context().session().connection());

            List<RegistryEntry> results = new ArrayList<RegistryEntry>(query.getLimit());

            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(DSApi.context().load(RegistryEntry.class, id));
            }

            rs.close();

            return new SearchResultsAdapter<RegistryEntry>(results, query.getOffset(), totalCount,
                RegistryEntry.class);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }                
    }
    
    /**
     * Tworzy nowy wpis w rejestrze.
     * 
     * @param registryId   id rejestru, do kt�rego wpisa� tworzony wpis
     * @param caseId   id sprawy, z kt�r� powi�za� nowo tworzony wpis (mo�e by� null)
     * @param inDocumentId   id pisma przychodz�cego, z kt�rym powi�za� nowo tworzony wpis (mo�e by� null)
     * @param values
     * @return
     */
    public static RegistryEntry createRegistryEntry(Long registryId, Long caseId, Long inDocumentId, Map<String, Object> values) throws EdmException
    {
        StringManager smL = GlobalPreferences.loadPropertiesFile(RegistriesManager.class.getPackage().getName(),null);
        Registry registry = Registry.find(registryId);
        
        RegistryEntry entry = new RegistryEntry();        
        entry.setRegistry(registry);
        entry.setYear(GlobalPreferences.getCurrentYear());                
        
        entry.create();
        
        String description = null;// = "Utworzono wpis w rejestrze " + registry.getName();
        
        if (caseId != null)
        {
            OfficeCase oc = OfficeCase.find(caseId);
            entry.getCases().add(oc);
            oc.getRegistryEntries().add(entry);
            description = smL.getString("UtworzonoWpisWrejestrzeDlaSprawy",oc.getOfficeId());//" dla sprawy " + oc.getOfficeId(); 
        }
        
        if (inDocumentId != null)
        {
            InOfficeDocument inDoc = InOfficeDocument.findInOfficeDocument(inDocumentId);
            entry.setInDocument(inDoc);
            description = smL.getString("UtworzonoWpisWrejestrzeDlaPismaPrzychodzacegoOnumerzeKO",inDoc.getOfficeNumber());//" dla pisma przychodz�cego o numerze KO " + inDoc.getOfficeNumber(); 
        }
        
        entry.getWorkHistory().add(Audit.create("create", DSApi.context().getPrincipalName(), description));
        
        registry.set(entry.getId(), values);   
        return entry;
    }
    
    /**
     * Tworzy nowy wpis w rejestrze.
     * 
     * @param registryId   id rejestru, do kt�rego wpisa� tworzony wpis
     * @param caseId   id sprawy, z kt�r� powi�za� nowo tworzony wpis (mo�e by� null)
     * @param values
     * @return
     */
    public static void updateRegistryEntry(RegistryEntry entry, Map<String, Object> values) throws EdmException
    {
        Registry registry = entry.getRegistry();
        
        registry.setWithHistory(entry.getId(), values);
    }
    
    /**
     * Generuje wydruk (zestawienie) dla danego rejestru (wstawiaj�c tylko wpisy z podanej tablicy
     * i zachowuj�c kolejno��).
     * 
     */
    public static File generatePrintout(Registry registry, String printoutCn, RegistryEntry[] entries) throws EdmException
    {
        RegistryPrintout printout = registry.getPrintout(printoutCn);
        List<String> columns = new ArrayList<String>();
        List<List<Object>> content = new ArrayList<List<Object>>();
        int[] widths = new int[printout.getColumns().size()];       
        for (int i=0; i < printout.getColumns().size(); i++)
        {
            ColumnInfo columnInfo = printout.getColumns().get(i);
            columns.add(columnInfo.getName());
            widths[i] = columnInfo.getSize();
        }
        
        for (RegistryEntry entry : entries)
        {
            List<Object> row = new ArrayList<Object>(printout.getColumns().size());
            RegistryEntryManager rm = registry.getRegistryEntryManager(entry.getId());
            for (ColumnInfo columnInfo : printout.getColumns())
            {
                if (columnInfo.isCommon())
                {
                    row.add(getCommonFieldValue(entry, columnInfo.getFieldCn()));
                }
                else
                {
                    row.add(Field.DATE.equals(rm.getField(columnInfo.getFieldCn()).getType()) ? 
                            (rm.getKey(columnInfo.getFieldCn()) != null ? DateUtils.formatJsDateYY((Date) rm.getKey(columnInfo.getFieldCn())) : "") : 
                            rm.getDescription(columnInfo.getFieldCn()));
                }
            }
            content.add(row);
        }
        
        return PdfGenerator.generate(printout.getName(), widths, columns, content, printout.getOrientation());        
    }       
    
    /**
     * Dla danego wpisu w rejestrze przekazuje warto�� pola (wsp�lnego dla wszystkich rejestr�w) 
     * identyfikowanego przez 'commonFieldCn'. 
     * 
     * @param commonFieldCn
     */
    public static Object getCommonFieldValue(RegistryEntry entry, String commonFieldCn) throws EdmException
    {
        if (commonFieldCn == null)
            return null;
        else if ("registryNo".equals(commonFieldCn))
            return entry.getSequenceNumber();
        else if ("ctime".equals(commonFieldCn))
            return DateUtils.formatJsDateYY(entry.getCtime());
        else if ("creator".equals(commonFieldCn))
            return DSUser.safeToFirstnameLastname(entry.getCreator());
        else if ("sender".equals(commonFieldCn))
            return (entry.getInDocument() != null && entry.getInDocument().getSender() != null) ? 
                    entry.getInDocument().getSender().getShortSummary() : null;
        else if ("address".equals(commonFieldCn))
            return (entry.getInDocument() != null && entry.getInDocument().getSender() != null) ? 
                    entry.getInDocument().getSender().getAddress() : null;                    
        else if ("case".equals(commonFieldCn))
            return entry.getCasesDescription();
        else
            throw new EdmException("Napotkano nieznan� nazw� pola w wydruku: "+commonFieldCn);
    }
}
