package pl.compan.docusafe.core.regtype;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FieldValueCoerceException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.lang.reflect.Method;
/* User: Administrator, Date: 2007-02-16 11:20:27 */

/**
 * Klasa reprezentuj�ca jedno pole(atrybut) z danego rodzaju dokumentu.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class Field
{
    public static final String DATE = "date";
    public static final String STRING = "string";
    public static final String BOOL = "bool";
    public static final String ENUM = "enum";
    public static final String FLOAT = "float";
    public static final String INTEGER = "integer";
    public static final String LONG = "long";
    public static final String CLASS = "class";

    public static final String MATCH_EXACT = "exact"; 

    private Integer id;
    private String cn;
    private String column;
    private String name;
    private String type;
    /** Okre�la czy to pole mo�e mie� wiele warto�ci */
    private boolean multiple;
    /** okre�la spos�b w jaki ma by� przedstawione pola "referencyjne" (ENUM_REF)*/
    private Integer length;
    private String classname;
    private String match;
    private boolean hidden;
    private boolean readOnly;
    /** Okre�la czy to pole jest obowi�zkowe. */
    private boolean required;
    /**
     * Okre�la czy wyszukiwanie normalne czy po zakresie - ma sens tylko dla p�l liczbowych i tekstowych.
     * Zatem dla p�l innych ni� LONG, INTEGER, FLOAT, STRING nie powinno si� tej flagi ustawia�.
     */
    private boolean searchByRange;
    /** 
     * true <=> to pole jest wy��czone z wyszukiwarki - nie mo�na po nim wyszukiwa� i nie pojawia si�
     * na wynikach wyszukiwanie
     */
    private boolean searchHidden;  
    
    public Field(Integer id, String cn, String column, String name, String type, 
                 Integer length, String classname, boolean required)
            throws EdmException
    {
        if (id == null)
            throw new NullPointerException("id");
        // atrybuty wielowarto�ciowe nie musz� miec podanej kolumny
       // if (column == null)
       //     throw new NullPointerException("column");
        if (name == null)
            throw new NullPointerException("name");
        if (type == null)
            throw new NullPointerException("type");

        if (!STRING.equals(type) && !DATE.equals(type) && !BOOL.equals(type) &&
            !ENUM.equals(type) && !INTEGER.equals(type) && !LONG.equals(type) &&
            !CLASS.equals(type) && !FLOAT.equals(type))
            throw new EdmException("Nieznany typ pola: "+type);

        if (STRING.equals(type) && length == null)
            throw new EdmException("Pole napisowe musi mie� okre�lon� d�ugo��");
        if (!STRING.equals(type) && length != null)
            throw new EdmException("Tylko pole napisowe mo�e mie� okre�lon� d�ugo��");

        if (CLASS.equals(type) && classname == null)
            throw new EdmException("Pole typu 'class' musi mie� podan� nazw� klasy");
        if (!CLASS.equals(type) && classname != null)
            throw new EdmException("Tylko pole typu 'class' mo�e mie� podan� nazw� klasy");

        this.id = id;
        this.cn = cn;
        this.column = column;
        this.name = name;
        this.type = type;
        this.length = length;
        this.classname = classname;
        this.required = required;
    }

    void set(PreparedStatement ps, int index, Object value)
                    throws FieldValueCoerceException, SQLException
    {
        Object coerced = simpleCoerce(value);

        if (coerced == null)
        {
        	ps.setObject(index, null);
        	return;
        }
        if (STRING.equals(type))
        {
                ps.setString(index, (String) coerced);
        }
        else if (DATE.equals(type))
        {
                ps.setDate(index, new java.sql.Date(((Date) coerced).getTime()));
        }
        else if (BOOL.equals(type))
        {
                ps.setBoolean(index, ((Boolean) coerced).booleanValue());
        }
        else if (ENUM.equals(type))
        {
                ps.setInt(index, ((Integer) coerced).intValue());
        }
        else if (INTEGER.equals(type))
        {
                ps.setInt(index, ((Integer) coerced).intValue());
        }
        else if (FLOAT.equals(type))
        {
                ps.setFloat(index, ((Float) coerced).floatValue());
        }
        else if (LONG.equals(type))
        {
                ps.setLong(index, ((Long) coerced).longValue());
        }
        else if (CLASS.equals(type))
        {
                ps.setLong(index, ((Long) coerced).longValue());
        }
        // nie sprawdzam, czy pole jest nieznanego typu, bo robi to wywo�anie coerce()
    }

    /**
     * Konwertuje przekazany obiekt do typu reprezentowanego przez
     * to pole.  Je�eli obiekt jest ju� ��danej klasy, nie s� w nim
     * dokonywane �adne zmiany z wyj�tkiem sytuacji, gdy jest to
     * napis przekraczaj�cy d�ugo�� pola - w�wczas warto�� jest skracana.
     * <p>
     * Dla pola typu ENUM lub ENUM_REF klas� jest java.lang.Integer, a dla typu CLASS java.lang.Long.
     * <p>
     * Je�eli warto�� nie mo�e by� bezb��dnie skonwertowana (np.
     * niepoprawna tekstowa posta� daty), rzucany jest wyj�tek
     * FieldValueCoerceException.
     * @param value
     * @return
     * @throws FieldValueCoerceException
     */
    public Object coerce(Object value) throws FieldValueCoerceException
    {
        if (isMultiple())
        {
            List<Object> list = new ArrayList<Object>();
            if (value instanceof Object[])
            {                
                for (Object object : (Object[]) value)
                {
                    Object o = simpleCoerce(object);
                    if (o != null)
                        list.add(o);
                }
            }
            else
            {
                list.add(simpleCoerce(value));
            }
            return list;
        }
        else
            return simpleCoerce(value);        
    }  
    
    /**
     * Konwertuje przekazany obiekt do typu reprezentowanego przez
     * dane pole. Dzia�a nie zwa�aj�c na pola wielokrotne, dlatego te�
     * powinno si� zawsze wywo�ywa� funkcj� {@link #coerce}, a ta funkcja powinna by� wo�ana tylko
     * wewn�trz {@link #coerce} lub w�wczas, gdy �wiadomie chcemy przekonwertowa� pojedy�cz� warto��. 
     */
    public Object simpleCoerce(Object value) throws FieldValueCoerceException
    {
        if (STRING.equals(type))
        {
            if (value == null)
                return null;
            String s = String.valueOf(value);
            s = TextUtils.trimmedStringOrNull(s);
            if (s == null)
                return null;
            if (s.length() > length.intValue())
                s = s.substring(0, length.intValue());
            return s;
        }
        else if (DATE.equals(type))
        {
            if (value == null)
                return null;

            Date date;
            if (!(value instanceof Date))
            {
                try
                {
                    date = DateUtils.parseJsDate(String.valueOf(value));
                    if (!DSApi.isLegalDbDate(date))
                        throw new FieldValueCoerceException("Przekazana data nie znajduje si� " +
                            "w zakresie wymaganym przez baz� danych: "+DateUtils.formatJsDate(date));
                }
                catch (FieldValueCoerceException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    //log.error(e.getMessage(), e);
                    throw new FieldValueCoerceException("Nie mo�na skonwertowa� warto�ci '"+value+"' do daty");
                }
            }
            else
            {
                date = (Date) value;
                if (!DSApi.isLegalDbDate(date))
                    throw new FieldValueCoerceException("Przekazana data nie znajduje si� " +
                        "w zakresie wymaganym przez baz� danych: "+DateUtils.formatJsDate(date));
            }
            return date;
        }
        else if (BOOL.equals(type))
        {
            if (value == null)
                return null;

            return Boolean.valueOf(String.valueOf(value));
        }
        else if (ENUM.equals(type))
        {
            // TODO: moze dodac sprawdzenie czy poprawny identyfikator z enumItems??
            if (value == null)
                return null;

            if (value instanceof String &&
                TextUtils.trimmedStringOrNull((String) value) == null)
                return null;

            if (value instanceof Integer)
                return value;

            try
            {
                return new Integer(String.valueOf(value));
            }
            catch (NumberFormatException e)
            {
                throw new FieldValueCoerceException("Nie mo�na skonwertowa� warto�ci "+value+" do liczby ca�kowitej");
            }
        }
        else if (INTEGER.equals(type))
        {
            if (value == null)
                return null;

            if (value instanceof Integer)
                return value;

            try
            {
                return new Integer(Integer.parseInt(value.toString()));
            }
            catch (NumberFormatException e)
            {
                throw new FieldValueCoerceException("Nie mo�na skonwertowa� warto�ci "+value+" do liczby ca�kowitej");
            }
        }
        else if (FLOAT.equals(type))
        {
            if (value == null)
                return null;

            if (value instanceof Float)
                return value;

            try
            {
                String s = value.toString().replace(",", ".");
                return new Float(Float.parseFloat(s));
            }
            catch (NumberFormatException e)
            {
                throw new FieldValueCoerceException("Nie mo�na skonwertowa� warto�ci "+value+" do liczby rzeczywistej");
            }
        }
        else if (LONG.equals(type) || CLASS.equals(type))
        {
            if (value == null)
                return null;

            if (value instanceof Long)
                return value;

            try
            {
                return new Long(Long.parseLong(value.toString()));
            }
            catch (NumberFormatException e)
            {
                throw new FieldValueCoerceException("Nie mo�na skonwertowa� warto�ci "+value+" do liczby ca�kowitej");
            }
        }
        else
        {
            throw new IllegalStateException("Nieznany typ pola: "+type);
        }
    }

    /**
     * Wywo�anie metody remoteMethod na klasie zwi�zanej z tym polem. Ma sens tylko dla p�l
     * typu CLASS.
     * @param thisMethod nazwa metody lokalnej, kt�ra zosta�a wywo�ana
     * @param remoteMethod nazwa metody zdalnej
     * @param object obiekt, na kt�rym wywo�a� metod� - gdy null wo�ana b�dzie metoda statyczna
     */
    Object invokeMethod(String thisMethod, String remoteMethod, Object object)
    {
        if (!CLASS.equals(type))
            throw new IllegalStateException("Metoda '"+thisMethod+"' mo�e by� wo�ana tylko dla typu CLASS");
        try
        {
            Class c = Class.forName(getClassname());
            Method method = c.getDeclaredMethod(remoteMethod, new Class[]{});
            return method.invoke(object, new Object[]{});
        }
        catch (ClassNotFoundException e)
        {
            throw new IllegalStateException("Pole ma okre�lon� nierozpoznan� klas�: "+getClassname());
        }
        catch (NoSuchMethodException e)
        {
            throw new IllegalStateException("Nie istnieje metoda '"+remoteMethod+"' w klasie: "+getClassname());
        }
        catch (Exception e)
        {
            throw new IllegalStateException("B��d podczas wo�ania metody '"+remoteMethod+"' na klasie: "+getClassname());
        }
    }

    /**
     * Zwraca 'akcj� s�ownikow�' dla klasy, z kt�r� zwi�zane jest to pole. Ma sens tylko dla p�l
     * typu CLASS.
     */
    public String getDictionaryAction()
    {
        Object object = invokeMethod("getDictionaryAction", "dictionaryAction", null);
        if (object instanceof String)
            return (String) object;
        else
            throw new IllegalStateException("Wynik funkcji 'dictionaryAction' jest innego typu ni� 'String'");
    }

    /**
     * Zwraca map� atrybut�w klasy zwi�zanej z tym polem, kt�re maj� by� wy�wietlane na formatce
     * obok przycisku wo�aj�cego 'akcj� s�ownikow�'. Ma sens tylko dla p�l typu CLASS.
     */
    public Map<String,String> getDictionaryAttributes()
    {
        Object object = invokeMethod("getDictionaryAttributes", "dictionaryAttributes", null);
        if (object instanceof Map)
            return (Map<String,String>) object;
        else
            throw new IllegalStateException("Wynik funkcji 'dictionaryAttributes' jest innego typu ni� 'Map<String,String>'");
    }

    /**
     * Zwraca nazw� kolumny w bazie, po kt�rej maj� by� sortowane wyniki wyszukiwania rodzaj� dokument�w,
     * gdy u�ytkownik wybierze sortowanie po obiektach tej klasy. Ma sens tylko dla p�l typu CLASS.
     */
    public String getDictionarySortColumn()
    {
        Object object = invokeMethod("getDictionarySortColumn", "dictionarySortColumn", null);
        if (object instanceof String)
            return (String) object;
        else
            throw new IllegalStateException("Wynik funkcji 'dictionarySortColumn' jest innego typu ni� 'String'");
    }    
    
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public String getColumn()
    {
        return column;
    }

    public void setColumn(String column)
    {
        this.column = column;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Integer getLength()
    {
        return length;
    }

    public void setLength(Integer length)
    {
        this.length = length;
    }

    public String getClassname()
    {
        return classname;
    }

    public void setClassname(String classname)
    {
        this.classname = classname;
    }

    public List<EnumItem> getEnumItems()
    {
        throw new IllegalStateException("Wywo�anie getEnumItems() ma sens tylko dla pola typu "+ENUM);
    }

    public List<EnumItem> getSortedEnumItems()
    {
        throw new IllegalStateException("Wywo�anie getEnumItems() ma sens tylko dla pola typu "+ENUM);
    }
    
  /*  public List<EnumItem> getEnumItems(Object discriminator)
    {
        throw new IllegalStateException("Wywo�anie getEnumItems(discriminator) ma sens tylko dla pola typu "+ENUM_REF);
    }*/

    public EnumItem getEnumItem(Integer id) throws EntityNotFoundException
    {
        throw new IllegalStateException("Wywo�anie getEnumItem(id) ma sens tylko dla pola typu "+ENUM);
    }

    public EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException
    {
        throw new IllegalStateException("Wywo�anie getEnumItemByCn(cn) ma sens tylko dla pola typu "+ENUM);
    }
    
    public EnumItem getEnumItemByTitle(String cn) throws EntityNotFoundException
    {
        throw new IllegalStateException("Wywo�anie getEnumItemByCn(cn) ma sens tylko dla pola typu "+ENUM);
    }
    
    public String getMatch()
    {
        return match;
    }

    public void setMatch(String match)
    {
        this.match = match;
    }

    public boolean isHidden()
    {
        return hidden;
    }

    public void setHidden(boolean hidden)
    {
        this.hidden = hidden;
    }

    public boolean isReadOnly()
    {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly)
    {
        this.readOnly = readOnly;
    }

    public boolean isMultiple()
    {
        return multiple;
    }

    public void setMultiple(boolean multiple)
    {
        this.multiple = multiple;
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }
   
    public boolean isSearchByRange()
    {
        return searchByRange;
    }

    public void setSearchByRange(boolean searchByRange)
    {
        this.searchByRange = searchByRange;
    }
    
    public boolean isSearchHidden()
    {
        return searchHidden;
    }

    public void setSearchHidden(boolean searchHidden)
    {
        this.searchHidden = searchHidden;
    }    
        
    public static class ValidationRule
    {
        private String fieldCn;
        private String type;        
        private String value;

        public String getFieldCn()
        {
            return fieldCn;
        }

        public void setFieldCn(String fieldCn)
        {
            this.fieldCn = fieldCn;
        }

        public String getType()
        {
            return type;
        }

        public void setType(String type)
        {
            this.type = type;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }              
    }
}
