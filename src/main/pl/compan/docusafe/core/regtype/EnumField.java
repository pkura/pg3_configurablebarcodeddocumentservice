package pl.compan.docusafe.core.regtype;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.field.EnumerationField;

import java.util.*;

/**
 * Klasa raprezentująca pole wyliczeniowe z danego rodzaju dokumentu.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class EnumField extends Field
{
    private Map<Integer, EnumItem> enumItems;

    public EnumField(Integer id, String cn, String column, String name,
                     boolean required) throws EdmException
    {
        super(id, cn, column, name, ENUM, null, null, required);
    }

    public void addEnum(Integer id, String cn, String title, String[] args)
    {
        if (id == null)
            throw new NullPointerException("id");
        if (title == null)
            throw new NullPointerException("title");

        if (enumItems == null)
            enumItems = new LinkedHashMap<Integer, EnumItem>();

        enumItems.put(id, new EnumItem(id, cn, title, args));
    }

    public List<EnumItem> getEnumItems()
    {
        return enumItems != null ? new ArrayList<EnumItem>(enumItems.values()) : Collections.EMPTY_LIST;
    }
    
    public List<EnumItem> getSortedEnumItems()
    {
        List<EnumItem> list = getEnumItems();
        Collections.sort(list, new EnumItem.EnumItemComparator());
        return list;
    }

    public EnumItem getEnumItem(Integer id) throws EntityNotFoundException
    {
        EnumItem item = enumItems != null ? (EnumItem) enumItems.get(id) : null;
        if (item == null)
            throw new EntityNotFoundException("Nie znaleziono wartości wyliczeniowej o identyfikatorze = "+id);
        return item;
    }
    
    public EnumItem getEnumItemByCn(String cn) throws EntityNotFoundException
    {
        if (enumItems != null)
        {
            for (EnumItem i : enumItems.values())
            {
                if (cn.equals(i.getCn()))
                    return i;
            }
        }
        throw new EntityNotFoundException("Nie znaleziono wartości wyliczeniowej o cn = "+cn);
    }

    public EnumItem getEnumItemByTitle(String title) throws EntityNotFoundException
    {
        if (enumItems != null)
        {
            for (EnumItem i : enumItems.values())
            {
                if (title.equals(i.getTitle()))
                    return i;
            }
        }
        throw new EntityNotFoundException("Nie znaleziono wartości wyliczeniowej o title = "+title);
    }
}
