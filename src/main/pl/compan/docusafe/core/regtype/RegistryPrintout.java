package pl.compan.docusafe.core.regtype;

import java.util.List;

/**
 * Definiuje pewien wydruk dla rejestru.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class RegistryPrintout
{
    private String cn;
    private String name;    
    private List<ColumnInfo> columns;  
    private String orientation;

    public List<ColumnInfo> getColumns()
    {
        return columns;
    }

    public void setColumns(List<ColumnInfo> columns)
    {
        this.columns = columns;
    }
    
    public String getCn()
    {
        return cn;
    }
    
    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getOrientation()
    {
        return orientation;
    }

    public void setOrientation(String orientation)
    {
        this.orientation = orientation;
    }
    
    public static class ColumnInfo
    {
        private String name;
        private String fieldCn;        
        private boolean common;
        private int size;

        public boolean isCommon()
        {
            return common;
        }

        public void setCommon(boolean common)
        {
            this.common = common;
        }

        public String getFieldCn()
        {
            return fieldCn;
        }

        public void setFieldCn(String fieldCn)
        {
            this.fieldCn = fieldCn;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public int getSize()
        {
            return size;
        }

        public void setSize(int size)
        {
            this.size = size;
        }                
    }
}
