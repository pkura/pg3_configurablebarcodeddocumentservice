package pl.compan.docusafe.core.regtype;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Expression;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentNotFoundException;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.StringManager;

/**
 * Klasa reprezentuj�ca wpis do danego rejestru. Zawiera warto�ci wsp�lnych cech rejestr�w.
 * Wszystkie specyficzne atrybuty s� dost�pne poprzez klas� {@link RegistryEntryManager}.
 * 
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
 */

public class RegistryEntry
{
    static final StringManager sm =
        StringManager.getManager(Constants.Package);

    private Long id;
    private Integer sequenceNumber;
    private Integer year;
    private Registry registry;
    private String creator;
    private Date ctime;
    private Integer hversion;
    private Set<OfficeCase> cases;
    private InOfficeDocument inDocument;
    private List<Audit> workHistory;
    private List<Attachment> attachments;
    
    public synchronized void create() throws EdmException
    {
        try
        {
            // przydzielam numer w rejestrze (w obr�bie danego roku)
            Query query = DSApi.context().session().createQuery("select max(r.sequenceNumber) from r in class "+
                RegistryEntry.class.getName()+" where r.year = ?");
            query.setInteger(0, year);       
            List max = query.list();

            if (max != null && max.size() > 0 )
            {
            	try
            	{
            		this.sequenceNumber = new Integer( max.get(0).toString()) + 1;
            	}
            	catch (Exception e) {
            		 this.sequenceNumber = new Integer(1);
				}
            }
            else
            {
                this.sequenceNumber = new Integer(1);
            }
            
            // ustawiam dat� wpisu i osob� tworz�ca wpis
            creator = DSApi.context().getPrincipalName();
            ctime = new Date();
            
            DSApi.context().session().save(this);  
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    private void setId(Long id)
    {
        this.id = id;
    }
    
    public Long getId()
    {
        return id;
    }

    public void setSequenceNumber(Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Integer getSequenceNumber()
    {
        return sequenceNumber;
    }

    public void setYear(Integer year)
    {
        this.year = year;
    }

    public Integer getYear()
    {
        return year;
    }

    public void setRegistry(Registry registry)
    {
        this.registry = registry;
    }

    public Registry getRegistry()
    {
        return registry;
    }
     
    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getCreator()
    {
        return creator;
    }

    public void setCases(Set<OfficeCase> cases)
    {
        this.cases = cases;
    }

    public Set<OfficeCase> getCases()
    {
        if (cases == null)
            cases = new LinkedHashSet<OfficeCase>();
        return cases;
    }
    
    public String getCasesDescription()
    {
        String cases = null;
        for (OfficeCase oc : this.getCases())
        {
            if (cases == null)
                cases = "";
            else
                cases += ", ";
            cases += oc.getOfficeId();                
        }
        return cases;
    }

    public static RegistryEntry find(Long id) throws EdmException
    {
        try
        {
            return (RegistryEntry) DSApi.context().session().load(RegistryEntry.class, id);
        }
        catch (Exception e)
        {
            throw new EdmException("Nie mo�na odnale�� wpisu o id " + id);            
        }
    }
    
    @SuppressWarnings("unchecked")
    public static RegistryEntry find(Long registryId, Long inDocumentId) throws EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(RegistryEntry.class);
            criteria.createCriteria("registry").add(Expression.idEq(registryId));
            criteria.createCriteria("inDocument").add(Expression.idEq(inDocumentId));
            
            List<RegistryEntry> list = criteria.list();
            if (list.size() > 0)
                return list.get(0);
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public List<Audit> getWorkHistory()
    {
        if (workHistory == null)
            workHistory = new LinkedList<Audit>();
        return workHistory;
    }

    public void setWorkHistory(List<Audit> workHistory)
    {
        this.workHistory = workHistory;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }
    
    public InOfficeDocument getInDocument()
    {
        return inDocument;
    }

    public void setInDocument(InOfficeDocument inDocument)
    {
        this.inDocument = inDocument;
    }

    /**
     * Dodaje do wpisu za��cznik. Za��cznik jest zapisywany w sesji
     * (Session.saveOrUpdate()).
     * 
     * @param attachment
     * @throws EdmException
     * @throws AccessDeniedException
     */
    public void createAttachment(Attachment attachment)
        throws EdmException, AccessDeniedException
    {
        createAttachment(attachment, false);
    }
    
    /**
     * Dodaje do wpisu za��cznik.
     * Za��cznik jest zapisywany w sesji (Session.saveOrUpdate()).
     * 
     * @param attachment
     * @param okre�la, czy dodawany za��cznik powsta� w wyniku kompilacji plik�w sprawy;
     * 
     * @throws EdmException
     * @throws AccessDeniedException
     */
    public void createAttachment(Attachment attachment, boolean compiled)
        throws EdmException, AccessDeniedException
    {
        //Obs�u�y� uprawnienia dla rejestr�w
        //if (!canModifyAttachments())
        //  throw new AccessDeniedException(sm.getString("document.addAttachmentDenied", id));

        // sprawdzenie, czy za��cznik jest zwi�zany z innym dokumentem
        if (attachment.getDocument() != null &&
            !attachment.getDocument().equals(this))
            throw new IllegalArgumentException(
                sm.getString("document.addingForeignAttachment", id));

        if (attachments == null)
            attachments = new LinkedList<Attachment>();

        attachment.setRegistryEntry(this);
        attachment.setType(Attachment.ATTACHMENT_TYPE_REGISTRY_ENTRY);

        attachments.add(attachment);

        try
        {
            DSApi.context().session().saveOrUpdate(this);
            DSApi.context().session().saveOrUpdate(attachment);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Pobiera za��cznik o konkretnym ID, powi�zany z tym wpisem.
     * Je�eli nie istnieje za��cznik o danym ID, lub istnieje, ale jest
     * oznaczony jako usuni�ty, zwracana jest warto�� null.
     */
    public Attachment getAttachment(Long id)
        throws AttachmentNotFoundException
    {
        if (id == null)
            throw new NullPointerException("id");

        if (attachments != null)
        {
            for (Iterator iter = attachments.iterator(); iter.hasNext(); )
            {
                Attachment attachment = (Attachment) iter.next();
                if (!attachment.isDeleted() && attachment.getId().equals(id))
                    return attachment;
            }
        }

        throw new AttachmentNotFoundException(id);
    }

    public List<Attachment> getAttachments()
    {
        if (attachments == null)
            attachments = new LinkedList<Attachment>();

        List<Attachment> filtered = new LinkedList<Attachment>(attachments);
        for (Iterator iter=filtered.iterator(); iter.hasNext(); )
        {
            Attachment a = (Attachment) iter.next();
            if (a.isDeleted()) iter.remove();
        }

        return filtered;
    }

    public void setAttachments(List<Attachment> attachments)
    {
        this.attachments = attachments;
    }

}
