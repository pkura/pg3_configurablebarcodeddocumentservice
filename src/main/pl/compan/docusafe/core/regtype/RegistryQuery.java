package pl.compan.docusafe.core.regtype;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.FormQuery;

import java.util.*;

/**
 * Klasa s�u��ca do budowania zapyta� dla wyszukiwania wpis�w w okreslonym rejestrze.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class RegistryQuery extends FormQuery
{
    public static String ATTRIBUTE_SEPARATOR = ",";

    private Registry registry;
    private Map<String,JoinInfo> tables = new HashMap<String,JoinInfo>();
    /**
     * okre�la czy maj� by� sprawdzane uprawnienia do rejestr�w podczas wyszukiwania
     */
    private boolean checkPermissions = true;
    /**
     * Zbi�r 'cn' p�l napisowych, kt�re chcemy �eby by�y wyszukiwane dok�adnie (czyli przez eq, a nie like)
     * - czyli zignorujemy MATCH_EXACT z danego pola.
     * Te pola musz� by� dodane tutaj ZANIM zostan� wywo�ane odpowiednie metody stringField, bo wpp
     * forceSearchEq nie b�dzie mia� wp�ywu na operatory wyszukiwania.
     */
    private Set<String> forceSearchEq = new HashSet<String>();

  //  private static JoinInfo dso_container = new JoinInfo("id", "dsr_registry_entry", "case_id");
    
    private RegistryQuery(Registry registry)
    {
        this.registry = registry;
    }

    public RegistryQuery(int offset, int limit)
    {
        super(offset, limit);
    }

    private void notifyTable(String tableName, JoinInfo join)
    {
        if (tableName == null)
            throw new NullPointerException("tableName");
        if (tables.get(tableName) == null)
        {
            tables.put(tableName, join);
        }
    }

    public Registry getRegistry()
    {
        return registry;
    }

    public void setRegistry(Registry registry)
    {
        this.registry = registry;
    }

    public Map<String, JoinInfo> getTables()
    {
        return tables;
    }

    public void ctime(Date ctimeFrom, Date ctimeTo)
    {
        if (ctimeFrom != null)
            addGe("dsr_registry_entry"+ATTRIBUTE_SEPARATOR+"ctime", ctimeFrom);
        if (ctimeTo != null)
            addLe("dsr_registry_entry"+ATTRIBUTE_SEPARATOR+"ctime", ctimeTo);                
    }
    /*
    public void mtime(Date mtimeFrom, Date mtimeTo)
    {
        if (mtimeFrom != null)
            addGe("dsr_registry_entry"+ATTRIBUTE_SEPARATOR+"mtime", mtimeFrom);
        if (mtimeTo != null)
            addLe("dsr_registry_entry"+ATTRIBUTE_SEPARATOR+"mtime", mtimeTo);                
    }
    */
    
    public void creatingUser(String[] users)
    {
        if (users == null)
            return;

        if (users.length == 0)
            return;

        int added = 0;
        RegistryQuery or = new RegistryQuery(registry);
        for (int i=0; i < users.length; i++)
        {
            if (users[i] != null && users[i].length() > 0)
            {
                or.creatingUser(users[i]);
                added++;
            }
        }

        if (added > 0)
        {
            addDisjunction(or);            
        }
    }
    
    private void creatingUser(String user)
    {
        addEq("dsr_registry_entry"+ATTRIBUTE_SEPARATOR+"creator", user);
    }
    
    public void sequenceNumber(Integer seqNoFrom, Integer seqNoTo)
    {
        if (seqNoFrom != null)
            addGe("dsr_registry_entry"+ATTRIBUTE_SEPARATOR+"sequenceNumber", seqNoFrom);
        if (seqNoTo != null)
            addLe("dsr_registry_entry"+ATTRIBUTE_SEPARATOR+"sequenceNumber", seqNoTo);  
    }
    
    /**
     * Dodaje warunek Eq dla podanego pola i wskazanej warto�ci.
     */
    public void field(Field field, Object value)
    {
        if (field == null)
            throw new NullPointerException("field");
        if (value != null)
            addEq(registry.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), value);
    }

    /**
     * Dodaje warunek dla podanego pola (zak�adaj�c si� jest typu bool) i wskazanej warto�ci.
     */
    public void boolField(Field field, Boolean value)
    {
        if (field == null)
            throw new NullPointerException("field");
        if (value == null)
            return;
        if (value.booleanValue())
        {
            addEq(registry.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), Boolean.TRUE);
        }
        else
        {
            RegistryQuery or = new RegistryQuery(registry);
            or.addEq(registry.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), Boolean.FALSE);
            or.addEq(registry.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), null);
            addDisjunction(or);
        }
    }    
    
    public void stringField(Field field, String value)
    {
        if (field == null)
            throw new NullPointerException("field");
        if (value == null)
            return;

        String attribute;
        if (field.isMultiple())
        {
            attribute = registry.getMultipleTableName()+"#"+field.getCn()+ATTRIBUTE_SEPARATOR+"field_val";
            notifyTable(registry.getMultipleTableName()+"#"+field.getCn(), new JoinInfo(registry.getTablename(), field.getColumn()));
        }
        else
            attribute = registry.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn();
        if (Field.MATCH_EXACT.equals(field.getMatch()) || forceSearchEq.contains(field.getCn()))
            addEq(attribute, value);
        else
            addLike(attribute, value+"%");
    }

    public void rangeField(Field field, Object from, Object to)
    {
        if (field == null)
            throw new NullPointerException("field");
        if (from != null)
            addGe(registry.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), from);
        if (to != null)
            addLe(registry.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), to);
    }

    public void enumField(Field field, Integer[] values)
    {
        if (field == null)
            throw new NullPointerException("field");
        if (values == null)
            throw new NullPointerException("values");
        if (values.length == 0)
            return;

        RegistryQuery or = new RegistryQuery(registry);
        for (int i=0; i < values.length; i++)
        {
            if (values[i] != null)
                or.field(field, values[i]);
        }

        addDisjunction(or);
    }

    public void otherClassField(Field field, String property, String value) throws EdmException
    {
        if (field == null)
            throw new NullPointerException("field");
        if (value == null)
            return;
        try
        {
            Class c = Class.forName(field.getClassname());       
            String tableName = DSApi.getTableName(c);
            String columnName = DSApi.getColumnName(c, property);
            
            addLike(tableName+ATTRIBUTE_SEPARATOR+columnName, value+"%");
            
            notifyTable(tableName, new JoinInfo("id", registry.getTablename(), field.getColumn()));
        }
        catch (ClassNotFoundException e)
        {
            throw new EdmException("Nie znaleziono klasy: "+field.getClassname());
        }
    }
    
    public boolean isCheckPermissions()
    {
        return checkPermissions;
    }

    public void setCheckPermissions(boolean checkPermissions)
    {
        this.checkPermissions = checkPermissions;
    }

    public void addForceSearchEq(String fieldCn)
    {
        forceSearchEq.add(fieldCn);
    }
    
    public void orderAsc(String attribute) throws EdmException
    {
        super.orderAsc(attribute);
        checkSortField(attribute);
    }

    public void orderDesc(String attribute) throws EdmException
    {
        super.orderDesc(attribute);
        checkSortField(attribute);
    }      
    
    /**
     * Sprawdza czy konieczne jest dodania tabeli s�ownikowej do zapytania ze wzgl�du na sortowanie po danym polu.
     */
    private void checkSortField(String attribute) throws EdmException
    {
        if (attribute.startsWith("registry_"))
        {
            String fieldCn = attribute.substring("registry_".length());
            Field field = registry.getFieldByCn(fieldCn);
            if (Field.CLASS.equals(field.getType()))
            {
                try
                {
                    Class c = Class.forName(field.getClassname());       
                    String tableName = DSApi.getTableName(c);                                       
                    
                    notifyTable(tableName, new JoinInfo("id", registry.getTablename(), field.getColumn()));
                }
                catch (ClassNotFoundException e)
                {
                    throw new EdmException("Nie znaleziono klasy: "+field.getClassname());
                }               
            }
        }
    }
    
    /**
     * Przechowuje informacje o z��czaniu danej tabeli
     */
    public static class JoinInfo
    {
        /**
         * nazwa g��wnej kolumny w tabeli, kt�rej ten obiekt dotyczy
         */
        private String tableKeyColumn;
        /**
         * nazwa tabeli, z kt�r� "ta tabela" ma zosta� z��czona
         */
        private String joinTableName;
        /**
         * nazwa g��wnej kolumny w tabeli, z kt�r� ten obiekt ma by� po��czony
         */
        private String joinTableColumn;
        /**
         * true <=> oznacza tabel� z warto�ciami wielokrotnymi (zamiast tableKeyColumn ��czenie po entry_id,field_cn)
         */
        private boolean multiple;
        
        /**
         * Z��czenie ze zwyk�� tabel�.
         */
        public JoinInfo(String tableKeyColumn, String joinTableName, String joinTableColumn)
        {
            this.tableKeyColumn = tableKeyColumn;
            this.joinTableName = joinTableName;
            this.joinTableColumn = joinTableColumn;
        }

        /**
         * Z��czenie z tabel� trzymaj�ca warto�ci wielokrotne dla rejestr�w.
         */
        public JoinInfo(String joinTableName, String joinTableColumn)
        {
            this.multiple = true;
            this.joinTableName = joinTableName;
            this.joinTableColumn = joinTableColumn;
        }
        
        public String getTableKeyColumn()
        {
            return tableKeyColumn;
        }

        public void setTableKeyColumn(String tableKeyColumn)
        {
            this.tableKeyColumn = tableKeyColumn;
        }

        public String getJoinTableName()
        {
            return joinTableName;
        }

        public void setJoinTableName(String joinTableName)
        {
            this.joinTableName = joinTableName;
        }

        public String getJoinTableColumn()
        {
            return joinTableColumn;
        }

        public void setJoinTableColumn(String joinTableColumn)
        {
            this.joinTableColumn = joinTableColumn;
        }

        public boolean isMultiple()
        {
            return multiple;
        }

        public void setMultiple(boolean multiple)
        {
            this.multiple = multiple;
        }
        
    }
}
