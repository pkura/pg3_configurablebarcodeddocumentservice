package pl.compan.docusafe.core.regtype;

import java.util.Comparator;
/* User: Administrator, Date: 2007-02-16 11:48:24 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class EnumItem
{
    private Integer id;
    private String cn;
    private String title;
    private String[] args;
    /**
     * pomocniczy atrybut (niewykorzystywany podczas wczytywania danych z XMLa) - u�ywany w pewnych
     * sytuacjach, mo�e by� wykorzystany w dowolny spos�b przez u�ytkownika
     */
    private Object tmp;

    public EnumItem(Integer id, String cn, String title, String[] args)
    {
        this.id = id;
        this.cn = cn;
        this.title = title;
        this.args = args;
    }

    public Integer getId()
    {
        return id;
    }
    
    public String getCn()
    {
        return cn;
    }

    public String getTitle()
    {
        return title;
    }

    public Object getTmp()
    {
        return tmp;
    }

    public void setTmp(Object tmp)
    {
        this.tmp = tmp;
    }

    /**
     * Zwraca warto�� argumentu zwi�zanego z warto�ci�.
     * @param position Pozycja argumentu liczona od 1.
     * @return Warto�� argumentu lub null.
     */
    public String getArg(int position)
    {
        if (position > 0 && (position-1) < args.length)
            return args[position-1];
        return null;
    }
    
    public static class EnumItemComparator implements Comparator<EnumItem>
    {
        public int compare(EnumItem e1, EnumItem e2)
        {
            return e1.getTitle().compareTo(e2.getTitle());
        }
    }
}
