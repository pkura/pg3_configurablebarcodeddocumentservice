package pl.compan.docusafe.core.regtype;

import java.util.List;


/**
 * Obiekt wspomagaj�cy tworzenie dynamicznego menu dla rejestr�w. Przechowuje odpowiednie informacje,
 * kt�re przez strone JSP s� u�ywane do wygenerowania odpowiedniego menu.
 * 
 * UWAGA: Obiekt tej klasy musi by� tworzony (poprzez metod� {@link Registry#getRegistriesMenuInfo(Long)} 
 * i zapisywany na zmiennej o nazwie 'menuInfo' w ka�dej akcji, kt�ra wymaga wy�wietlenia po lewej
 * stronie menu rejestr�w.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class RegistriesMenuInfo
{
    private List<Registry> registries;
    
    private Long selectedRegistryId;
    
    RegistriesMenuInfo(List<Registry> registries, Long selectedRegistryId)
    {
        this.registries = registries;
        this.selectedRegistryId = selectedRegistryId;
    }

    public List<Registry> getRegistries()
    {
        return registries;
    }

    public Long getSelectedRegistryId()
    {
        return selectedRegistryId;
    }        
}
