package pl.compan.docusafe.core.regtype;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.regtype.Field.ValidationRule;
import pl.compan.docusafe.core.regtype.RegistryPrintout.ColumnInfo;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;

/**
 * Klasa opisuj�ca rodzaj rejestru. Powsta�a na bazie {@link pl.compan.docusafe.web.office.common.DocumentMainTabAction}.
 * Wszystkie czynno�ci zwi�zane z typami rejestr�w s� wykonywane analogicznie
 * do czynno�ci dotycz�cych rodzaj�w dokument�w.
 * 
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
 * @version $Id$
 */
public class Registry
{
    private static final Log log = LogFactory.getLog(pl.compan.docusafe.core.dockinds.DocumentKind.class);
    static final StringManager sm = StringManager.getManager(Constants.Package);
    
    private Long id;
    private String name;
    private String shortName;
    private String cn;
    private String tablename;
    /** tabela trzymaj�ca warto�ci atrybut�w wielowarto�ciowych tego rodzaju rejestru */
    private String multipleTableName;
    private boolean enabled;
   // private boolean caseRequired;
    private String regType;
    private String requiredRwa;
    private Integer hversion;
    private List<Field> fields;
    /**
     * regu�y walidacji
     */
    private List<ValidationRule> validationRules;
    /**
     * dodatkowe w�a�ciwo�ci dla danego rodzaju rejestru
     */
    private Map<String,String> properties;
    /**
     * Uprawnienia do tworzenia/modyfikacji wpis�w w tym rejestrze postaci listy "guid-�w" 
     * grup/dzia��w maj�cych te uprawnienia.
     */
    private List<String> writePermissions;
    /**
     * Uprawnienia do odczytu wpis�w w tym rejestrze w postaci listy "guid-�w" grup/dzia��w maj�cych 
     * te uprawnienia.
     */
    private List<String> readPermissions;
    private Map<String,RegistryPrintout> printouts;
    private org.dom4j.Document xmlDocument;

    // typy rejestr�w
    public static final String CASE = "case";
    public static final String INDOCUMENT = "indocument";
    public static final String OTHER = "other";
    
    //klucze dla mapy properties
    
   
    public Registry()
    {
    }

    public static List<Registry> list() throws EdmException
    {
        return Finder.list(Registry.class, "name");
    }    
    
    @SuppressWarnings("unchecked")
    public static List<Registry> list(boolean onlyEnabled) throws EdmException
    {
        if (onlyEnabled)
        {
            Criteria criteria = DSApi.context().session().createCriteria(Registry.class);
            criteria.add(Expression.eq("enabled", Boolean.TRUE));
            try
            {
                return (List<Registry>) criteria.list();
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
        }
        else
        {
            return list();
        }
    }

    public static Registry find(Long id) throws EdmException
    {
        return (Registry) Finder.find(Registry.class, id);
    }

    @SuppressWarnings("unchecked")
    public static Registry findByCn(String cn) throws EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(Registry.class);
            criteria.add(Expression.eq("cn", cn));

            List<Registry> list = criteria.list();
            if (list.size() > 0)
            {
                return (Registry) list.get(0);
            }

            return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Zwraca informacje pomocno do tworzenia menu dla rejestr�w
     * 
     * @param selectedRegistryId identyfikator rejestru, kt�ry akutalnie jest wybrany
     */
    public static RegistriesMenuInfo getRegistriesMenuInfo(Long selectedRegistryId) throws EdmException
    {
        return new RegistriesMenuInfo(list(true), selectedRegistryId);
    }
    
    public RegistryEntryManager getRegistryEntryManager(Long entryId)
    {
        return new RegistryEntryManager(entryId, this);
    }

    public void initialize() throws EdmException
    {
        if (fields == null)
            loadFields();
    }

    public List<Field> getFields() throws EdmException
    {
        if (fields == null)
            loadFields();
        return fields;
    }

    // TODO: przerobic moze zeby zamiast listy byla mapa i wowczas wystarczy tu zrobic get (ale wtedy pewnie bylaby inna kolejnosc w getFields()??)
    public Field getFieldByCn(String cn) throws EdmException
    {
        List<Field> fs = getFields();
        for (int i=0; i < fs.size(); i++)
        {
            if (fs.get(i).getCn() != null && fs.get(i).getCn().equals(cn))
                return fs.get(i);
        }
        return null;
    }

    /**
     * Wczytuje z XML zapisanego w bazie informacje o polach tego rodzaju rejestru.
     * Jest ono wykonywane tylko przy pierwszym odwo�aniu.
     *
     * @throws EdmException
     */
    private void loadFields() throws EdmException
    {
        if (fields != null)
            return;

        org.dom4j.Document doc = getXML();

     /**/   org.dom4j.Element mField = doc.getRootElement().element("cn");                  
        cn = mField != null ? mField.attributeValue("value") : null;
        if (cn == null)
            throw new EdmException("Brakuje w XML nazwy kodowej rejestru (cn)");
        mField = doc.getRootElement().element("name");
        name = mField != null ? mField.attributeValue("value") : null;
        if (name == null)
            throw new EdmException("Brakuje w XML nazwy rejestru (cn)");
        mField = doc.getRootElement().element("shortName");
        shortName = mField != null ? mField.attributeValue("value") : null;
        if (shortName == null)
            throw new EdmException("Brakuje w XML nazwy skr�conej rejestru (cn)");/**/
        org.dom4j.Element elFields = doc.getRootElement().element("fields");
        if (elFields != null)
        {
           // org.dom4j.Element elMainField = elFields.element("main-field");
           // if (elMainField != null)
           //     mainFieldCn = elMainField.attributeValue("cn");
            List fieldTags = elFields.elements("field");
            if (fieldTags != null)
            {
                fields = new ArrayList<Field>(fieldTags.size());
                for (Iterator iter=fieldTags.iterator(); iter.hasNext(); )
                {
                    org.dom4j.Element el = (Element) iter.next();
                    org.dom4j.Element elType = el.element("type");
                    String type = elType.attributeValue("name");
                    //String refStyle = elType.attributeValue("ref-style");
                    boolean required = "true".equals(el.attributeValue("required"));
                    //if (refStyle == null)
                    //    refStyle = Field.STYLE_NORMAL;

                 /*   org.dom4j.Element elAvailable = el.element("available");
                    List<Field.Condition> availableWhen = elAvailable != null ? readWhenAvailable(elAvailable) : null;
                    org.dom4j.Element elRequired = el.element("required");
                    List<Field.Condition> requiredWhen = elRequired != null ? readWhenRequired(elRequired) : null;*/

                    Field field = null;

                    if (Field.ENUM.equals(type))
                    {
                        EnumField enumField = new EnumField(
                         //   refStyle,
                           // isRef,
                            Integer.valueOf(el.attributeValue("id")),
                            el.attributeValue("cn"),
                            el.attributeValue("column"),
                            el.attributeValue("name"),
                            required/*,
                            requiredWhen,
                            availableWhen*/);

                        List elItems = elType.elements("enum-item");
                        if (elItems != null && elItems.size() > 0)
                        {
                            for (Iterator itemIter=elItems.iterator(); itemIter.hasNext(); )
                            {
                                Element elItem = (Element) itemIter.next();

                                List<String> args = new ArrayList<String>(6);
                                for (int i=1; i <= 5; i++)
                                {
                                    String attrName = "arg"+i;
                                    if (elItem.attributeValue(attrName) != null)
                                    {
                                        args.add(elItem.attributeValue(attrName));
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                                enumField.addEnum(
                                    new Integer(elItem.attributeValue("id")),
                                    elItem.attributeValue("cn"),
                                    elItem.attributeValue("title"),
                                    (String[]) args.toArray(new String[args.size()]));
                            }
                        }
                        field = enumField;
                    }
                    else
                    {
                        // inne typy ni� Field.ENUM
                        String classname = null;
                        if (elType.attribute("class") != null && Field.CLASS.equals(type))
                            classname = elType.attributeValue("class");

                        Integer length = null;
                        if (elType.attribute("length") != null && Field.STRING.equals(type))
                            length = new Integer(elType.attributeValue("length"));

                        field = new Field(
                            Integer.valueOf(el.attributeValue("id")),
                            el.attributeValue("cn"),
                            el.attributeValue("column"),
                            el.attributeValue("name"),
                            type,
                            length,
                            classname,
                            required);
                    }

                    // dodatkowe w�asciwo�ci pola
                    if (elType.attribute("match") != null && Field.STRING.equals(type))
                        field.setMatch(elType.attributeValue("match"));
                    field.setMultiple("true".equals(elType.attributeValue("multiple")));
                    if (el.attribute("hidden") != null)
                        field.setHidden(Boolean.valueOf(el.attributeValue("hidden")).booleanValue());
                    if (el.attribute("readonly") != null)
                        field.setReadOnly(Boolean.valueOf(el.attributeValue("readonly")).booleanValue());
                    if (elType.attribute("searchByRange") != null)
                        field.setSearchByRange(Boolean.valueOf(elType.attributeValue("searchByRange")).booleanValue());
                    field.setSearchHidden("true".equals(el.attributeValue("search-hidden")));
                    
                    fields.add(field);
                }
            }
        }

        if (fields == null)
            fields = new LinkedList<Field>();
        
        // wydruki
        readPrintouts(doc.getRootElement().element("printouts"));
        
        // uprawnienia
        org.dom4j.Element elPermissions = doc.getRootElement().element("permissions");
        if (elPermissions != null)
        {
            readPermissions = readPermissions(elPermissions.element("read"));
            writePermissions = readPermissions(elPermissions.element("write"));
        }
        
        // pozosta�e w�a�ciwo�ci
        readOther(doc.getRootElement().element("other"));
    }

    /**
     * Wczytuje z elementu z XMLa informacje o dost�pnych wydrukach dla danego rejestru.
     */
    private void readPrintouts(Element elPrintouts)
    {
        if (elPrintouts != null)
        {
            List printoutTags = elPrintouts.elements("printout");
            if (printoutTags != null)
            {
                printouts = new LinkedHashMap<String,RegistryPrintout>();
                for (Iterator iter=printoutTags.iterator(); iter.hasNext(); )
                {
                    org.dom4j.Element elPrintout = (Element) iter.next();
                    RegistryPrintout registryPrintout = new RegistryPrintout();
                    if (elPrintout.attributeValue("name") != null && elPrintout.attributeValue("cn") != null)
                    {
                        registryPrintout.setName(elPrintout.attributeValue("name"));
                        registryPrintout.setCn(elPrintout.attributeValue("cn"));
                        registryPrintout.setOrientation(elPrintout.attributeValue("orientation"));
                        List columnTags = elPrintout.elements("column");
                        List<ColumnInfo> columns = new ArrayList<ColumnInfo>();
                        for (Iterator columnIter=columnTags.iterator(); columnIter.hasNext();)
                        {
                            org.dom4j.Element elColumn = (Element) columnIter.next();
                            ColumnInfo columnInfo = new ColumnInfo();
                            columnInfo.setName(elColumn.attributeValue("name"));     
                            columnInfo.setFieldCn(elColumn.attributeValue("fieldCn"));
                            columnInfo.setCommon("true".equals(elColumn.attributeValue("common")));
                            if (elColumn.attributeValue("size") != null)
                            {
                                try
                                {
                                    columnInfo.setSize(Integer.parseInt(elColumn.attributeValue("size")));
                                }
                                catch (NumberFormatException e)
                                {
                                    log.error("Podano niepoprawny rozmiar kolumny dla wydruku w XML opisuj�cym rejestr", e);                           
                                    columnInfo.setSize(1);
                                }
                            }
                            else
                                columnInfo.setSize(1);
                            columns.add(columnInfo);
                        }
                        registryPrintout.setColumns(columns);
                    }
                    printouts.put(registryPrintout.getCn(), registryPrintout);
                }
            }
        }
    }
    
    /**
     * wczytuje z elementu z XMLa pozosta�� cz�� parametryzacji rejestru (znajduj�c�
     * si� wewn�trz tag-u "other")
     */
    private void readOther(Element elOther) throws EdmException
    {
        if (elOther != null)
        {
     /**/       // dane o tabeli trzymaj�cej warto�ci specyficznych atrybut�w rejestru 
            org.dom4j.Element elTable = elOther.element("table");
            if (elTable != null)
                tablename = elTable.attributeValue("name");
            else
                throw new EdmException("Nie podano nazwy tabeli dla rejestru"); /**/
            // dane o tabeli trzymaj�cej warto�ci atrybut�w wielowarto�ciowych
            org.dom4j.Element elMultipleTable = elOther.element("multiple-table");
            if (elMultipleTable != null)
                multipleTableName = elMultipleTable.attributeValue("name");
            // dane o sprawie
            org.dom4j.Element elRegtype = elOther.element("regtype");
            if (elRegtype != null)
            {
                regType = elRegtype.attributeValue("name");
                requiredRwa = elRegtype.attributeValue("rwa");
            }  /**/
            // walidacja
            org.dom4j.Element elValidation = elOther.element("validation");
            if (elValidation != null)
            {                          
                List ruleTags = elValidation.elements("rule");
                if (ruleTags != null)
                {
                    validationRules = new ArrayList<ValidationRule>(ruleTags.size());                 
                    for (Iterator iter=ruleTags.iterator(); iter.hasNext(); )
                    {
                        org.dom4j.Element elRule = (Element) iter.next();
                        ValidationRule rule = new ValidationRule();
                        if (elRule.attributeValue("cn") != null && elRule.attributeValue("type") != null)
                        {
                            rule.setFieldCn(elRule.attributeValue("cn"));
                            rule.setType(elRule.attributeValue("type"));
                            rule.setValue(elRule.attributeValue("value"));
                            validationRules.add(rule);
                        }
                    }
                }
            }
            // pozosta�e w�a�ciwo�ci rodzaju rejestru
            org.dom4j.Element elProperties = elOther.element("properties");
            if (elProperties != null)
            { 
                List propTags = elProperties.elements("property");
                if (propTags != null)
                {
                    properties = new LinkedHashMap<String, String>();                
                    for (Iterator iter=propTags.iterator(); iter.hasNext(); )
                    {
                        org.dom4j.Element elProp = (Element) iter.next();
                        if (elProp.attributeValue("name") != null && elProp.attributeValue("value") != null)
                            properties.put(elProp.attributeValue("name"), elProp.attributeValue("value"));
                    }
                }
            }
        }
    }
    
    /**
     * Czyta z dokumentu DOM uprawnienia do danego rejestru (mog� to by� uprawnienia do odczytu albo do zapisu).
     * 
     * @param element w�ze� zawieraj�cy dane uprawnienia
     */
    private static List<String> readPermissions(Element element)
    {
        if (element == null)
            return null;
        
        List permList = element.elements("division");
        List<String> editPermissions = new ArrayList<String>();
        for (Iterator permIter=permList.iterator(); permIter.hasNext(); )
        {
            Element elPerm = (Element) permIter.next();
            String guid = TextUtils.trimmedStringOrNull(elPerm.attributeValue("guid"));
            if (guid != null)
                editPermissions.add(guid); 
        }
        return editPermissions;
    }        

    /**
     * R�cznie przypisuje zawarto�� dokumentu XML z podnaego pliku - ta funkcja powinna by� u�ywana TYLKO  
     * podczas tworzenia nowego (aktualizacji istniej�cego) rejestru w celu przypisania odpowiednich atrybut�w
     * takicj jak nazwa tbaeli, cn i nazwa rejestru, aby nie trzeba by�o zawsze parsowa� XML-a gdy potrzebujemy
     * tylko podstawowe informacje.
     * 
     * @param file
     * @throws EdmException
     */
    public void setXML(File file) throws EdmException
    {
        try
        {
            InputStream is = new FileInputStream(file);
            SAXReader reader = new SAXReader();
            xmlDocument = reader.read(is);
            org.apache.commons.io.IOUtils.closeQuietly(is);
        }
        catch (Exception e)
        {
            throw new EdmException("B��d odczytu XML", e);
        }
    }
    
    /**
     * Zwraca XML opisuj�cy typ rejestru. Ponowne wywo�ania
     * nie odwo�uj� si� do bazy, tylko zwracaj� dokument odczytany
     * za pierwszym razem.
     * <p>
     * Je�eli w bazie nie ma XML opisuj�cego typ rejestru, zwracany jest
     * utworzony ad hoc dokument zawieraj�cy tylko element g��wny.
     */
    public org.dom4j.Document getXML() throws EdmException
    {
        if (xmlDocument != null)
            return xmlDocument;

        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select content from " +
                DSApi.getTableName(Registry.class)+" where id = ?");
            pst.setLong(1, getId());
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
                throw new EdmException("Nie mo�na odczyta� obiektu RegistryType#"+getId());

            Blob blob = rs.getBlob(1);

            // je�eli w bazie by� XML, odczytuj� go, w przeciwnym razie
            // tworz� pusty dokument (tylko g��wny element)
            if (blob != null)
            {
                InputStream is = blob.getBinaryStream();
                SAXReader reader = new SAXReader();
                xmlDocument = reader.read(is);
                org.apache.commons.io.IOUtils.closeQuietly(is);
            }
            else
            {
                org.dom4j.DocumentFactory factory = new org.dom4j.DocumentFactory();
                xmlDocument = factory.createDocument(factory.createElement("regtype"));
            }

            return xmlDocument;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "id="+id);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (Exception e)
        {
            throw new EdmException("B��d odczytu XML", e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }
    
    /**
     * Zapisuje do bazy zawartosc pliku XML opisuj�cego dany rejestr.
     * 
     * @param file plik opisuj�cy rejestr
     * @throws EdmException
     */
    public void saveContent(File file) throws EdmException
    {
        FileInputStream stream = null;
        try 
        {
            stream = new FileInputStream(file);
            int fileSize = (int) file.length();
            PreparedStatement ps = null;
            
            try 
            {
                DSApi.context().session().save(this);
                DSApi.context().session().flush();
                
                int rows;
                if (DSApi.isOracleServer())
                {
                    ps = DSApi.context().prepareStatement(
                        "select content from "+DSApi.getTableName(Registry.class)+
                        " where id = ? for update");
                    ps.setLong(1, this.getId().longValue());
    
                    ResultSet rs = ps.executeQuery();
                    if (!rs.next())
                        throw new EdmException("Nie mo�na odczyta� pliku opisuj�cego rejestr "+this.getId());
    
                    rows = 1;
    
                    Blob blob = rs.getBlob(1);
                    if (!(blob instanceof oracle.sql.BLOB))
                        throw new EdmException("Spodziewano si� obiektu oracle.sql.BLOB");
    
                    OutputStream out = ((oracle.sql.BLOB) blob).getBinaryOutputStream();
                    byte[] buf = new byte[2048];
                    int count;
                    while ((count = stream.read(buf)) > 0)
                    {
                        out.write(buf, 0, count);
                    }
                    out.close();
                    stream.close();
                }
                else
                {
                    ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(Registry.class)+
                        " set content = ? where id = ?");
                    ps.setBinaryStream(1, stream, fileSize);
                    ps.setLong(2, this.getId().longValue());
                    rows = ps.executeUpdate();
                }
                
                if (rows != 1)
                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
            }
            catch (IOException e)
            {
                throw new EdmException("B��d podczas zapisu danych", e);
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }
            finally
            {
            	DSApi.context().closeStatement(ps);                
            }
        }
        catch (IOException e)
        {
            throw new EdmException("B��d odczytu pliku", e);
        }
        finally
        {
            if (stream != null) try { stream.close(); } catch (Exception e) { }
        }
    }
 
    /**
     * Zwraca true <=> obiekt o1 jest r�ny od obiektu o2.
     */
    private static boolean notEqual(Object o1, Object o2)
    {
        return ((o1 != null && !o1.equals(o2)) || (o1 == null && o2 != null)); 
    }
    
    /**
     * Zapisuje do tabeli dotyczacej danej typu rejestru wartosci wpisu o id 'entryId', dodatkowo
     * sprawdzaj�c czy dane nie uleg�y zmianie od ostaniego zapisu - je�li uleg�y to dodawany jest
     * odpowiedni wpis do historii danego obiektu RegistryEntry. 
     */
    public void setWithHistory(Long entryId, Map<String,Object> fieldValues) throws EdmException
    {
        // pobieramy stare warto�ci
        RegistryEntryManager oldRm = this.getRegistryEntryManager(entryId);
        oldRm.initialize();
        
        // zapisujemy zmiany
        set(entryId, fieldValues);
        
        // pobieramy nowe warto�ci
        RegistryEntryManager newRm = this.getRegistryEntryManager(entryId);
        newRm.initialize();
        
        // por�wnujemy stare warto�ci z nowymi przechodz�c kolejno po wszystkich polach
        StringBuilder historyBuilder = null;
        for (Field field : getFields())
        {
            boolean hasChanged = false;
            if (field.isMultiple())
            {
                // por�wnujemy listy (ale troche "niedok�adnie", w zwi�zku z tym zgadzamy si� z tym,
                // �e czasem b�dzie informacja o zmianie mimo, �e takiej zmiany nie by�o)
                List<Object> oldValue = (List<Object>) oldRm.getKey(field.getCn());
                List<Object> newValue = (List<Object>) newRm.getKey(field.getCn());
                
                if ((oldValue == null && newValue != null) || (oldValue != null && newValue == null))
                    hasChanged = true;
                else if (oldValue != null && newValue != null)
                {
                    if (oldValue.size() != newValue.size())                    
                        hasChanged = true;                 
                    else
                    {
                        boolean listsEqual = true;
                        for (int i=0; i < oldValue.size(); i++)
                        {
                            if (notEqual(oldValue.get(i), newValue.get(i)))                                
                                listsEqual = false;                                    
                        }
                        hasChanged = !listsEqual;
                    }                        
                }
            }
            else       
            {
                hasChanged = notEqual(oldRm.getKey(field.getCn()), newRm.getKey(field.getCn()));
            }
            
            if (hasChanged)
            {
                // warto�� danego pola uleg�a zmianie
                Object oldDesc = oldRm.getDescription(field.getCn());
                Object newDesc = newRm.getDescription(field.getCn());
                String change = null;
                if (oldDesc == null)
                    change = field.getName() + " z warto�ci pustej na '" + newDesc + "'";
                else if (newDesc == null)
                    change = field.getName() + " z '" + oldDesc + " na warto�� pust�";
                else
                    change = field.getName() + " z '" + oldDesc + "' na '" + newDesc + "'";
                if (historyBuilder == null)
                    historyBuilder = new StringBuilder("Zmieniono warto�ci p�l: ").append(change);
                else
                    historyBuilder.append(", ").append(change);
            }
        }
        
        if (historyBuilder != null)
        {
            // dokonano jakich� zmian - dodajemy wpis do historii
            historyBuilder.append(".");
            RegistryEntry entry = RegistryEntry.find(entryId);
            entry.getWorkHistory().add(Audit.create("changeFields", DSApi.context().getPrincipalName(),
                historyBuilder.toString()));
        }
    }
    
    /**
     * Zapisuje do tabeli dotyczacej danej typu rejestru wartosci wpisu o id 'entryId'. Gdy juz taki
     * wiersz istnia� wykonywany jest update, wpp wstawiany nowy wiersz.
     * Gdy nie ma jakiego� pola w 'fieldValues' to w�wczas jest mu przypisywany null!
     * 
     * @param entryId
     * @param fieldValues
     * @throws EdmException
     */
    public void set(Long entryId, Map<String,Object> fieldValues) throws EdmException
    {
        PreparedStatement ps = null;
        PreparedStatement pst = null;
        try
        {
            StringBuilder builder = null;
            pst = DSApi.context().prepareStatement("select entry_id from "+getTablename()+" where entry_id = ?");
            pst.setLong(1, entryId);
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
            {
                // wstawiamy nowy wiersz
                boolean first = true;
                builder = new StringBuilder("insert into ");
                builder.append(getTablename()).append(" (");
                for (Field field : getFields())
                    // wielowartosciowe trzymane w innej tabeli
                    if (!field.isMultiple())
                    {
                        if (!first)
                            builder.append(", ");
                        else
                            first = false;
                        builder.append(field.getColumn());
                    }
                if (!first)
                    builder.append(", ");
                builder.append("entry_id) values (");
                first = true;
                for (int i=0; i < getFields().size()+1; i++)
                    // wielowartosciowe trzymane w innej tabeli
                    if (i == getFields().size() || !getFields().get(i).isMultiple())
                    {
                        if (!first)
                            builder.append(", ");
                        else
                            first = false;
                        builder.append("?");
                    }
                builder.append(")");
            }
            else
            {
                // aktualizujemy istniejacy wiersz
                if (getFields().size() == 0)
                {
                    // jak nie ma zadnych pol w typie to nie ma czego aktualizowac
                    return;
                }
                boolean first = true;
                builder = new StringBuilder("update ");
                builder.append(getTablename()).append(" set ");                                    
                
                for (Field field : getFields())
                    // wielowartosciowe trzymane w innej tabeli
                    if (!field.isMultiple())
                    {
                        if (!first)
                            builder.append(", ");
                        else
                            first = false;
                        builder.append(field.getColumn());
                        builder.append(" = ?");
                        
                    }
                builder.append(" where entry_id = ?");                
            }

            ps = DSApi.context().prepareStatement(builder.toString());
            // ustalamy parametry zapytania
            int count = 0;
            for (Field field : getFields())
            {                                
                if (field.isMultiple())
                {
                    saveMultipleData(entryId, field, (List<Object>) field.coerce(fieldValues.get(field.getCn())));
                    //field.set(ps, count, null);
                }
                else
                {
                    count++;
                    field.set(ps, count, fieldValues.get(field.getCn()));
                }
            }
            ps.setLong(count+1, entryId);
            ps.executeUpdate();                                                
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        	DSApi.context().closeStatement(pst);
        }
    }

    /**
     * Zapisuje do tabeli dotyczacej danego rejestru wartosci wpisu o id 'entryId'. Gdy juz taki
     * wiersz istnia� wykonywany jest update, wpp wstawiany nowy wiersz.
     * Tylko uaktualniane te pola, kt�re podane w 'fieldValues', zatem je�li kt�rego� pola
     * tam nie ma to jego warto�� w bazie nie ulegnie zmianie.
     * 
     * @param entryId
     * @param fieldValues
     * @throws EdmException
     */
    public void setOnly(Long entryId, Map<String,Object> fieldValues) throws EdmException
    {
        PreparedStatement ps = null;
        PreparedStatement pst = null;
        try
        {
            Set<String> fieldCns = fieldValues.keySet();
            StringBuilder builder = null;
            pst = DSApi.context().prepareStatement("select entry_id from "+getTablename()+" where entry_id = ?");
            pst.setLong(1, entryId);
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
            {
                // wstawiamy nowy wiersz
                boolean first = true;
                builder = new StringBuilder("insert into ");
                builder.append(getTablename()).append(" (");
                for (String fieldCn : fieldCns)
                {
                    Field field = getFieldByCn(fieldCn);
                    // wielowartosciowe trzymane w innej tabeli
                    if (!field.isMultiple())
                    {
                        if (!first)
                            builder.append(", ");
                        else
                            first = false;
                        builder.append(field.getColumn());
                    }
                }
                if (!first)
                    builder.append(", ");
                builder.append("entry_id) values (");
                first = true;
                //for (int i=0; i < fieldCns.size()+1; i++)
                for (String fieldCn : fieldCns)
                    // wielowartosciowe trzymane w innej tabeli
                    if (!getFieldByCn(fieldCn).isMultiple())   
                    {
                        if (!first)
                            builder.append(", ");
                        else
                            first = false;
                        builder.append("?");
                    }
                if (!first)
                    builder.append(", ");
                builder.append("?)");
            }
            else
            {
                // aktualizujemy istniejacy wiersz
                if (fieldValues.keySet().size() == 0)
                {
                    return;
                }

             /*   StringBuilder historyBuilder = new StringBuilder("Zmieniono warto�ci p�l: ");
                boolean anyChange = false;
                RegistryEntryManager rem = new RegistryEntryManager(entryId, this);*/      
                
                boolean first = true;
                builder = new StringBuilder("update ");
                builder.append(getTablename()).append(" set ");
                
                for (String fieldCn : fieldCns)
                {
                    Field field = getFieldByCn(fieldCn);
                    // wielowartosciowe trzymane w innej tabeli
                    if (!field.isMultiple())
                    {
                        if (!first)
                            builder.append(", ");
                        else
                            first = false;
                        builder.append(field.getColumn());
                        builder.append(" = ?");
                        
                  /*      // sprawdzanie czy warto�� uleg�a zmianie (na potrzeby historii)
                        Object oldValue = rem.getKey(field.getCn()); 
                        Object newValue = fieldValues.get(field.getCn());                        
                        boolean fieldChanged = (oldValue != null && !oldValue.equals(newValue)) ||
                            (oldValue == null && newValue != null);
                                                
                        if (fieldChanged)
                        {
                            if (anyChange)
                                historyBuilder.append(", ");
                            else
                                anyChange = true;
                            historyBuilder.append(field.getName() + " na " + newValue.toString());
                        } */
                    } 
                }
                builder.append(" where entry_id = ?");
                
                // dodajemy wpis o zmianach do historii
              /*  if (anyChange)
                {
                    RegistryEntry entry = RegistryEntry.find(entryId);
                    entry.getWorkHistory().add(Audit.create("changeFields", DSApi.context().getPrincipalName(),
                        historyBuilder.toString()));
                } */
            }

            ps = DSApi.context().prepareStatement(builder.toString());
            // ustalamy parametry zapytania
            int count = 0;
            for (String fieldCn : fieldCns)
            {
                Field field = getFieldByCn(fieldCn);                               
                if (field.isMultiple())
                {
                    saveMultipleData(entryId, field, (List<Object>) field.coerce(fieldValues.get(field.getCn())));
                    //field.set(ps, count, null);
                }
                else
                {
                    count++; 
                    field.set(ps, count, fieldValues.get(field.getCn()));
                }
            }
            ps.setLong(count+1, entryId);

            ps.executeUpdate();

            
                        
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        	DSApi.context().closeStatement(pst);            
        }
    }
    
    /**
     * Zapisuje do bazy warto�ci atrybutu wielokrotnego.     
     */
    private void saveMultipleData(Long entryId, Field field, List<Object> values) throws EdmException    
    {
        PreparedStatement ps = null;
        try
        {
            String del = "delete from "+multipleTableName+" where entry_id=? and field_cn=?";
            ps = DSApi.context().prepareStatement(del);
            ps.setLong(1, entryId);
            ps.setString(2, field.getCn());
            ps.execute();
            DSApi.context().closeStatement(ps);
            
            if (values != null)
            {
                String ins = "insert into "+multipleTableName+" (entry_id,field_cn,field_val) values (?,?,?)";            
                for (Object object : values)
                {
                    if (object != null)
                    {
                        ps = DSApi.context().prepareStatement(ins);
                        ps.setLong(1, entryId);
                        ps.setString(2, field.getCn());
                        ps.setString(3, object.toString());
                        ps.execute();
                        DSApi.context().closeStatement(ps);
                    }
                }
            }
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);            
        }
    }
    
    /**
     * Sprawdza czy aktualnie zalogowany u�ytkownik ma prawo do odczytywanie/przegl�dania danego rejestru.
     */
    public boolean canRead() throws EdmException
    {        
        if (DSApi.context().isAdmin())
            return true;
        DSUser user = DSApi.context().getDSUser();
        List<String> guids = getReadPermissions();
        // jak nie podano �adnych dzia��w to znaczy �e ka�dy ma uprawnienie
        if (guids == null || guids.size() == 0)
            return true;
        for (String guid : guids)
        {
            try
            {
                DSDivision division = DSDivision.find(guid);
                if (user.inDivision(division))
                    return true;
            }
            catch (DivisionNotFoundException e)
            {
                log.error(e.getMessage(), e);
            }
        }
        return false;
    }
    
    /**
     * Sprawdza czy aktualnie zalogowany u�ytkownik ma prawo do tworzenia/edycji wpis�w w danym rejestrze.
     */
    public boolean canWrite() throws EdmException
    {        
        if (DSApi.context().isAdmin())
            return true;
        DSUser user = DSApi.context().getDSUser();
        List<String> guids = getWritePermissions();
        // jak nie podano �adnych dzia��w to znaczy �e ka�dy ma uprawnienie
        if (guids == null || guids.size() == 0)
            return true;
        for (String guid : guids)
        {
            try
            {
                DSDivision division = DSDivision.find(guid);
                if (user.inDivision(division))
                    return true;
            }
            catch (DivisionNotFoundException e)
            {
                log.error(e.getMessage(), e);
            }
        }
        return false;
    }
    
    /**
     * usuwa z tabeli dotyczacej tego rejestru wiersz z wartosciami dla wpisu o id 'entryId'
     */
    public void remove(Long entryId) throws EdmException
    {
        if (entryId == null)
            throw new NullPointerException("entryId");

        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("delete from "+getTablename()+" where id=?");
            pst.setLong(1, entryId);
            pst.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Registry)) return false;

        final Registry registryType = (Registry) o;

        if (id != null ? !id.equals(registryType.getId()) : registryType.getId() != null) return false;

        return true;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public String getTablename()
    {
        return tablename;
    }

    public void setTablename(String tablename)
    {
        this.tablename = tablename;
    }

    public String getMultipleTableName()
    {
        return multipleTableName;
    }

    public void setMultipleTableName(String multipleTableName)
    {
        this.multipleTableName = multipleTableName;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

 /*   public boolean isCaseRequired()
    {
        return caseRequired;
    }

    public void setCaseRequired(boolean caseRequired)
    {
        this.caseRequired = caseRequired;
    }*/        

    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public List<ValidationRule> getValidationRules()
    {
        return validationRules;
    }

    public Map<String, String> getProperties()
    {
        if (properties == null)
            properties = new LinkedHashMap<String, String>();
        return properties;
    }

    public void setRequiredRwa(String requiredRwa)
    {
        this.requiredRwa = requiredRwa;
    }

    public String getRequiredRwa()
    {
        return requiredRwa;
    }

    public Map<String,RegistryPrintout> getPrintouts()
    {
        return printouts;
    }
    
    public RegistryPrintout getPrintout(String cn)
    {
        return printouts.get(cn);
    }

    private List<String> getReadPermissions() throws EdmException
    {
        initialize();
        return readPermissions;
    }

    private List<String> getWritePermissions() throws EdmException
    {
        initialize();
        return writePermissions;
    }

    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    public String getShortName()
    {
        return shortName;
    }

    public String getRegType()
    {
        return regType;
    }

    public void setRegType(String regType)
    {
        this.regType = regType;
    }
}
