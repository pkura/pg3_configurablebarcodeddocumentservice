package pl.compan.docusafe.core.regtype;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.HibernateException;

/**
 * Klasa daj�ca prosty dost�p do specyficznych p�l i ich warto�ci z danego wpisu do rejestru. Zanim si� u�yje
 * obiektu tej klasy na stronie JSP, powinien on zosta� zainicjowany (wczytany z bazy). Inicjowanie
 * nast�pi poprzez odwo�anie do danego pola/warto�ci, ale tak�e mo�na zrobi� to r�cznie poprzez funkcje
 * 'initialize()'.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class RegistryEntryManager
{
    private Registry registry;
    private Long registryEntryId;
    private Map<String, FieldValue> values;

    RegistryEntryManager(Long registryEntryId, Registry registry)
    {
        this.registryEntryId = registryEntryId;
        this.registry = registry;
    }

    private Map<String, FieldValue> getValues() throws EdmException
    {
        if (values == null)
            loadData();
        return values;
    }

    public void initialize() throws EdmException
    {
        registry.initialize();
        if (values == null)
            loadData();
    }

    public List<Field> getFields() throws EdmException
    {
        return registry.getFields();
    }

    public Field getField(String cn) throws EdmException
    {
        return registry.getFieldByCn(cn);//getValues().get(cn).getField();
    }

    /**
     * Zwraca warto�� danego pola przekonwertowan� do czytelnej postaci. Dla cz�sci p�l
     * getValue(cn) == getKey(cn), ale np. dla pola typu CLASS getKey zwraca id obiektu
     * a getValue ten obiekt, oraz dla type ENUM getKey zwraca id warto�ci wyliczeniowej,
     * a getValue nazw� tej warto�ci wyliczeniowej (wy�wietlan� u�ytkownikowi).
     * @param cn
     * @return
     * @throws EdmException
     */
    public Object getValue(String cn) throws EdmException
    {
        if (getValues().get(cn) == null)
            return null;
        else
            return getValues().get(cn).getValue();
    }

    /**
     * Zwraca warto�� danego pola, kt�r� mo�na wy�wietli� u�ytkownikowi. R�ni si� od getValue(cn)
     * tylko dla p�l typu CLASS - zwraca opis danego obiektu, zamiast rzeczywistego obiektu.
     * 
     * @param cn
     * @return
     * @throws EdmException
     */
    public Object getDescription(String cn) throws EdmException
    {
        if (getValues().get(cn) == null)
            return null;
        else
            return getValues().get(cn).getDescription();
    }
    
    /**
     * Zwraca rzeczywist� warto�� danego pola (czyli tak� jaka jest w tabeli w bazie zapisana).
     * @param cn
     * @return
     * @throws EdmException
     */
    public Object getKey(String cn) throws EdmException
    {
        if (getValues().get(cn) == null)
            return null;
        else
            return getValues().get(cn).getKey();
    }

    /**
     * Zwraca warto�� pola w postaci obiektu EnumItem (ma sens tylko dla p�l ENUM)
     */
    public EnumItem getEnumItem(String cn) throws EdmException
    {
        Field field = getField(cn);
        if (!Field.ENUM.equals(field.getType()))
            throw new IllegalStateException("Funkcja rm.getItem(cn) mo�e by� wo�ana tylko dla p�l o typie wyliczeniowym");
        Object key = getKey(cn);
        if (key == null)
            return null;

        return field.getEnumItem((Integer) key);
    }

    /**
     * Zwraca warto�� pola w postaci nazwy kodowej (cn) obiektu EnumItem (ma sens tylko dla p�l ENUM)
     */
    public String getEnumItemCn(String cn) throws EdmException
    {
        EnumItem item = getEnumItem(cn);
        if (item == null)
            return null;
        else
            return item.getCn();
    }

    /**
     * Wczytuje z bazy warto�ci poszczeg�lnych p�l. Je�li registryEntryId r�wny null, to oznacza �e
     * ten FieldsManager zosta� pobrany dla nowo tworzonego dokumentu, zatem wszystkie warto�ci
     * b�d� null-ami.
     *
     * @throws EdmException
     */
    private void loadData() throws EdmException
    {
        if (registryEntryId == null)
            values = new HashMap<String, FieldValue>();

        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select * from "+registry.getTablename()+" where entry_id = ?");
            pst.setLong(1, registryEntryId);
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
            {
                values = new HashMap<String, FieldValue>();
            }
            else
            {
                values = new HashMap<String, FieldValue>();
                List<Field> fields = registry.getFields();
                for (int i=0; i < fields.size(); i++)
                {
                    values.put(fields.get(i).getCn(), getFieldValue(rs, fields.get(i), fields.get(i).getColumn()));
                }
            }
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }

    /**
     * Zmienia/uaktualnia warto�ci p�l (tylko w pami�ci, nie w bazie) przekazanymi warto�ciami 'values'
     */
    public void reloadValues(Map<String,Object> values) throws EdmException
    {
        for (String fieldCn : values.keySet())
        {
            try
            {
                Field field = getField(fieldCn);
                Object key = field.coerce(values.get(fieldCn));
                getValues().put(fieldCn, new FieldValue(field, key));
            }
            catch (Exception e)
            {
                // reloadValues odbywa sie przewaznie jak byl blad w celu przywrocenia stanu ktory
                // wpisal uzytkownik, wiec jesli blad byl spowodowany wlasnie tym, ze nie mozna
                // rozpoznac poprawnych wartosci, to tu tez ten blad wyskoczy (zatem te bledy tu olewam)
            }
        }
    }

    /**
     * Na podstawie wyniku otrzymanego z zapytania do bazy (pobieranego z 'rs') tworzony jest obiekt FieldValue
     * odpowiadaj�cy za warto�� pola 'field'.
     * @param rs
     * @param field
     * @param column nazwa kolumny w bazie, w kt�rej znajduje si� warto�� dla pola 'field'
     * @return
     * @throws SQLException
     * @throws EdmException
     */
    private FieldValue getFieldValue(ResultSet rs, Field field, String column)
        throws SQLException, EdmException
    {
        if (field.isMultiple())
        {
            // atrybut wielowarto�ciowy
            StringBuilder sql = new StringBuilder("select field_val from ").append(registry.getMultipleTableName()).append(" where field_cn=? and entry_id=?");
            PreparedStatement ps = null;
            try
            {
                ps = DSApi.context().prepareStatement(sql.toString());
                ps.setString(1, field.getCn());
                ps.setLong(2, registryEntryId);
                ResultSet vrs = ps.executeQuery();
                List<Object> values = new ArrayList<Object>();
                while (vrs.next())
                {
                    String v = vrs.getString(1);
                    Object value = field.simpleCoerce(v);
                  
                    if (value != null)
                        values.add(value);
                }                                          
                return new FieldValue(field, values);
            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }
            finally
            {
            	DSApi.context().closeStatement(ps);
            }                
        }
        else
        {
            if (Field.STRING.equals(field.getType()))
            {
                String value = rs.getString(column);
                return new FieldValue(field, value, value);
            }
            else if (Field.DATE.equals(field.getType()))
            {
                java.sql.Date date = rs.getDate(column);
                java.util.Date value = date != null ? new java.util.Date(date.getTime()) : null;
                return new FieldValue(field, value, value);
            }
            else if (Field.BOOL.equals(field.getType()))
            {
                boolean bool = rs.getBoolean(column);
                Boolean value = rs.wasNull() ? null : Boolean.valueOf(bool);
                return new FieldValue(field, value, (value == null || !value) ? "nie" : "tak");
            }
            else if (Field.ENUM.equals(field.getType()))
            {
                int i = rs.getInt(column);
                Integer value = rs.wasNull() ? null : new Integer(i);
                return new FieldValue(field, value);
            }
            else if (Field.FLOAT.equals(field.getType()))
            {
                float f = rs.getFloat(column);
                Float value = rs.wasNull() ? null : new Float(f);
                return new FieldValue(field, value, value);
            }
            else if (Field.INTEGER.equals(field.getType()))
            {
                int i = rs.getInt(column);
                Integer value = rs.wasNull() ? null : new Integer(i);
                return new FieldValue(field, value, value);
            }
            else if (Field.LONG.equals(field.getType()))
            {
                long l = rs.getLong(column);
                Long value = rs.wasNull() ? null : new Long(l);
                return new FieldValue(field, value, value);
            }
             else if (Field.CLASS.equals(field.getType()))
            {
                long l = rs.getLong(column);
                Long value = rs.wasNull() ? null : new Long(l);
                return new FieldValue(field, value);
            }
            else
            {
                throw new EdmException("Nieznany typ pola: "+field.getType());
            }
        }
    }

    /**
     * Reprezentuje warto�� danego pola. Pole 'key' oznacza warto��(klucz) zapisywan� do bazy, a 'value' warto��
     * odpowiadaj�c� temu kluczowi. Dla wi�kszo�ci p�l 'key' == 'value', a jedynie dla p�l typu Field.ENUM
     * i Field.CLASS konieczne jest dodatkowe wyliczenie warto�ci dla klucza.
     */
    public static class FieldValue
    {
        private Field field;
        private Object key;
        private Object value;

        /**
         * Konstruktor.
         *
         * @param field
         * @param key
         * @param value
         */
        public FieldValue(Field field, Object key, Object value)
        {
            this.field = field;
            this.key = key;
            this.value = value;
        }

        /**
         * Konstruktor dla warto�ci typ�w wymagaj�cych dodatkowego wyliczenia warto�ci 'value' na podstawie 'key'.
         * @param field
         * @param key
         * @throws EdmException
         */
        @SuppressWarnings("unchecked")
        FieldValue(Field field, Object key) throws EdmException
        {
            this.field = field;
            this.key = key;

            // inicjalizujemy wartosc (o ile 'key' r�ne od null)
            if (this.key != null)
            {
                if (field.isMultiple())
                {
                    // atrybut wielowarto�ciowy
                    List<Object> keys = (List<Object>) key;
                    List<Object> values = new ArrayList<Object>();
                    for (Object o : keys)
                    {
                        values.add(calculateValue(o));
                    }
                    this.value = values;
                }
                else
                {
                    // atrybut jednowarto�ciowy
                    this.value = calculateValue(this.key);
                }
            }
        }

        /** 
         * na podstawie klucza wylicza pojedy�cz� warto�c dla tego pola 
         */
        @SuppressWarnings("unchecked")
        private Object calculateValue(Object key) throws EdmException
        {
            if (Field.CLASS.equals(field.getType()))
            {
                try
                {
                    Class c = Class.forName(field.getClassname());
                    return Finder.find(c, (Long) key);
                }
                catch (ClassNotFoundException e)
                {
                    throw new EdmException("Nie znaleziono definicji klasy "+field.getClassname(), e);
                }
            }
            else if (Field.ENUM.equals(field.getType()))
            {
                return field.getEnumItem((Integer) key).getTitle();
            }
            else
            {
                // nie potrzeba dodatkowego wyliczenia dla innych typ�w - warto�� taka sama jak klucz
                return key;                        
            }
        }
        
        /**
         * na podstawie warto�ci wylicza pojedy�czy opis dla tego pola
         */
        private Object calculateDescription(Object value)
        {
            if (value == null || !Field.CLASS.equals(field.getType()))
            {
                if (value instanceof Date)
                    return DateUtils.formatJsDate((Date) value);
                else
                    return value;
            }
            Object object = field.invokeMethod("getDescription", "getDictionaryDescription", value);
            if (object instanceof String)
                return (String) object;
            else
                throw new IllegalStateException("Wynik funkcji 'getDictionaryDescription' jest innego typu ni� 'String'");
        }
        
        public Object getKey()
        {
            return key;
        }

        public Object getValue()
        {
            return value;
        }
        
        /**
         * Zwraca warto�� pola w postaci opisu (zamiast rzeczywistego obiektu) - ma sens tylko dla p�l typu CLASS,
         * bo dla pozosta�ych zwracane to samo co przez funkcje {@link this#getValue()}. 
         */
        @SuppressWarnings("unchecked")
        public Object getDescription()
        {
            if (field.isMultiple())
            {
                List<Object> values = (List<Object>) value;
                String description = null;
                for (int i=0; i < values.size(); i++)
                {                    
                    if (i == 0)
                        description = calculateDescription(values.get(i)).toString();
                    else
                        description += ", " + calculateDescription(values.get(i)).toString();
                }
                return description;
            }
            else
                return calculateDescription(this.value);
        }

        public Field getField()
        {
            return field;
        }

        public void setKey(Object key)
        {
            this.key = key;
        }

        public void setValue(Object value)
        {
            this.value = value;
        }
    }

    public Registry getRegistry()
    {
        return registry;
    }       
}
