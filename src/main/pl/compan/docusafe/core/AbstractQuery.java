package pl.compan.docusafe.core;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AbstractQuery.java,v 1.4 2006/02/20 15:42:16 lk Exp $
 */
public abstract class AbstractQuery
{
    /**
     * <p>
     *     Pole do wyszukiwania pełnotekstowego.
     * </p>
     */
    protected String content;
    protected boolean contentWordOrder;

    private List<SortField> sortFields = new LinkedList<SortField>();

    public void safeSortBy(String field, String fallbackField, boolean ascending)
    {
        if (!validSortField(fallbackField))
            throw new IllegalArgumentException("Niepoprawne pole sortowania: "+fallbackField);

        if (!validSortField(field))
            field = fallbackField;

        sortFields.add(new SortField(field, Boolean.valueOf(ascending)));
    }

    public List sortFields()
    {
        return Collections.unmodifiableList(sortFields);
    }

    protected abstract boolean validSortField(String field);

    public void setContentWordOrder(boolean contentWordOrder) {
        this.contentWordOrder = contentWordOrder;
    }

    public boolean isContentWordOrder() {
        return contentWordOrder;
    }

    public static class SortField
    {

        private String name;
        private Boolean direction;
        SortField(String name, Boolean direction)
        {
            if (name == null)
                throw new NullPointerException("name");
            if (direction == null)
                throw new NullPointerException("direction");

            this.name = name;
            this.direction = direction;
        }

        public String getName()
        {
            return name;
        }

        public Boolean getDirection()
        {
            return direction;
        }

    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
