package pl.compan.docusafe.core.templating;

import org.antlr.stringtemplate.AttributeRenderer;
import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class RtfTemplating implements TemplateEngine{
    private final static Logger LOG = LoggerFactory.getLogger(RtfTemplating.class);

    private final Charset charset;

    public RtfTemplating(){
        charset = Charset.forName("US-ASCII");
    }

    public void process(InputStream template, OutputStream result, Map<String, ?> values) throws IOException {
        String templateString = IOUtils.toString(new InputStreamReader(template, charset));
        StringTemplate st = new StringTemplate(templateString);
        st.registerRenderer(Object.class, new RtfRenderer());
        st.registerRenderer(String.class, new RtfRenderer());
        st.setAttributes(values);
//        LOG.info("template = '{}', result = '{}'", templateString, st.toString());
        Writer writer = new OutputStreamWriter(result, charset);
        IOUtils.write(st.toString(), writer);
        writer.flush();
    }

    private static class RtfRenderer implements AttributeRenderer {

        public String toString(Object o) {
            if(o == null) return "";
            String string = o.toString();
            LOG.info("before {} after {}", string, escapeRtf(string));
            return escapeRtf(string);
        }

        private String escapeRtf(String string){
            if(StringUtils.isAsciiPrintable(string) && !string.contains("\\")){
                return string;
            } else {
                StringBuilder ret = new StringBuilder(string.length() + 2);
                for(char c : string.toCharArray()){
                    if(c == '\\'){
                        ret.append("\\\\"); // == \\
                    } else if (c <= 0x7f) {
                        ret.append(c);
                    } else {
                        ret.append("\\u");
                        ret.append((int)c);
                        ret.append("?");
                    }
                }
                return ret.toString();
            }
        }

        public String toString(Object o, String s) {
            return toString(o);
        }
    }
}
