package pl.compan.docusafe.core.templating;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

/**
 * Interfejs mechanizmu umo�liwiaj�cego wykorzystanie szablon�w
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface TemplateEngine {

    /**
     * Wykorzystuje szablon do wygenerowania gotowego dokumentu
     * @param template strumie� danych zawieraj�cy szablon
     * @param result strumie� do kt�rego zapisany zostanie wynik
     * @param values warto�ci, kt�re b�d� u�yte w szablonie
     */
    void process(InputStream template, OutputStream result, Map<String, ?> values) throws IOException, Exception;

}
