package pl.compan.docusafe.core.templating;

import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class TextTemplating implements TemplateEngine {
    private final static Logger LOG = LoggerFactory.getLogger(TemplateEngine.class);

    private final Charset charset;

    public TextTemplating(String charsetName){
        charset = Charset.forName(charsetName);
    }

    public void process(InputStream template, OutputStream result, Map<String, ?> values) throws IOException {
        String templateString = IOUtils.toString(new InputStreamReader(template, charset));
        StringTemplate st = new StringTemplate(templateString);
        st.setAttributes(values);
        LOG.info("template = '{}', result = '{}'", templateString, st.toString());
        Writer writer = new OutputStreamWriter(result, charset);
        IOUtils.write(st.toString(), writer);
        writer.flush();
    }
}
