package pl.compan.docusafe.core.templating;

import net.sourceforge.rtf.IRTFDocumentTransformer;
import net.sourceforge.rtf.RTFTemplate;
import net.sourceforge.rtf.handler.RTFDocumentHandler;
import net.sourceforge.rtf.template.velocity.RTFVelocityTransformerImpl;
import net.sourceforge.rtf.template.velocity.VelocityTemplateEngineImpl;
import org.apache.velocity.app.VelocityEngine;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.text.Format;
import java.util.Map;

public class RtfVelocityTemplate implements TemplateEngine  
{
	
    private final static Logger log = LoggerFactory.getLogger(RtfVelocityTemplate.class);
    private final static Charset charset = Charset.forName("Cp1250");
    private Map<Class, Format> defaultFormatters;
	//String newString = new String(res.getString().getBytes(), "IS08859-2");

    

    public void process(InputStream template, OutputStream result, Map<String, ?> values) throws IOException, Exception {
        
  
        RTFTemplate rtfTemplate  = new RTFTemplate();  
        
        RTFDocumentHandler parser = new RTFDocumentHandler();
        rtfTemplate.setParser(parser);

        IRTFDocumentTransformer transformer = new RTFVelocityTransformerImpl();
        rtfTemplate.setTransformer(transformer);

        VelocityTemplateEngineImpl templateEngine = new VelocityTemplateEngineImpl();

        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty("input.encoding ", charset);
        velocityEngine.setProperty("output.encoding", charset);
        velocityEngine.setProperty ("response.encoding", charset);
        
        templateEngine.setVelocityEngine(velocityEngine);
        rtfTemplate.setTemplateEngine(templateEngine);        
        
        rtfTemplate.setTemplate(template);          
        
        for(String key : values.keySet())
        {
        	log.info("Klucz do odwolania: " + key + " wartosc: " + values.get(key));
            rtfTemplate.put(key, values.get(key));
        }
        Writer writer = new OutputStreamWriter(result, charset);

        if (defaultFormatters != null && !defaultFormatters.isEmpty()) {
            for (Class clazz : defaultFormatters.keySet()) {
                rtfTemplate.setDefaultFormat(clazz, defaultFormatters.get(clazz));
            }
        }

        log.info("maxMEmery: {}, totalMemeory: {}, freeMemory: {}", Runtime.getRuntime().maxMemory(), Runtime.getRuntime().totalMemory(), Runtime.getRuntime().freeMemory());
        rtfTemplate.merge(writer);
        writer.flush(); 
        writer.close();
        result.flush();
        result.close();
        log.info("Dokument zostal wygenerowany rtf velocity");
    }

    public Map<Class, Format> getDefaultFormatters() {
        return defaultFormatters;
    }

    public void setDefaultFormatters(Map<Class, Format> defaultFormatters) {
        this.defaultFormatters = defaultFormatters;
    }
}
