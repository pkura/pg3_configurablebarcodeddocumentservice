package pl.compan.docusafe.core.templating;

import com.google.gson.*;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.*;

public class DocumentSnapshotSerializer {

    private static final Logger log = LoggerFactory.getLogger(DocumentSnapshotSerializer.class);

    private static FieldsManagerUtils.FieldValuesGetter getter = new FieldsManagerUtils.DateEnumValuesGetter(false);

    public static Map<String, Object> getMapFromDocument(Document document) throws EdmException {

        final Map<String, Object> ret = new HashMap<String, Object>();

        final FieldsManager fm = document.getFieldsManager();

        for(Field field : fm.getFields()) {

            if(field.isHidden()) {
                continue;
            }

            // klucz, kt�ry dodajemy do mapy ret
            final String cn = field.getCn();

            // warto��, kt�r� dodajemy jako warto�� mapy ret
            Object value = null;

            // hidden
            // dictionary / multi dictionary
            if(field instanceof AbstractDictionaryField) {
                // dictionary
                if(! field.isMultiple()) {
                    value = getStringValueFromDictionary(field, fm);
                }
                // multi dictionary
                else {
                    value = getValueFromMultiDictionary(field, fm);
                }
            }
            // simple field
            else {
                value = getValueFromSimpleField(field, fm);
            }
            ret.put(cn, value);
        }

        return ret;
    }

    public static Map<String, Object> getMapFromDocumentNew(Document document) throws EdmException {

        final Map<String, Object> ret = new HashMap<String, Object>();

        final FieldsManager fm = document.getFieldsManager();

        for(Field field : fm.getFields()) {

            if(field.isHidden()) {
                continue;
            }

            // klucz, kt�ry dodajemy do mapy ret
            final String cn = field.getCn();

            // warto��, kt�r� dodajemy jako warto�� mapy ret
            Object value = null;

            // hidden
            // dictionary / multi dictionary
            if(field instanceof AbstractDictionaryField) {
                // dictionary
                if(! field.isMultiple()) {
                    value = getValueFromDictionary(field, fm);

                }
                // multi dictionary
                else {
                    value = getValueFromMultiDictionary(field, fm);
                }
            }
            // simple field
            else {
                value = getValueFromSimpleField(field, fm);
            }
            ret.put(cn, value);
        }

        return ret;
    }

    static String getValueFromSimpleField(Field f, FieldsManager fm) throws EdmException {
        String cn = f.getCn();
        return fm.getStringValue(cn);
    }

    static Map<String, Object> getStringValueFromDictionary(Field f, FieldsManager fm) throws EdmException {
        final Map<String, Object> map = new HashMap<String, Object>();

        log.debug("[getStringValueFromDictionary] cn = {}", f.getCn());

//        final List<Field> fields = f.getFields();
//
//        if(fields == null) {
//            log.error("[getStringValueFromDictionary] fields == null");
//            return map;
//        }

        String cn = f.getCn();
//        Map<String, Object> dictTemplateValues = FieldsManagerUtils.getDictionaryExtractedValues(fm, cn, getter);
        Map<String, Object> dictTemplateValues = (Map<String, Object>)fm.getValue(cn);


        // TODO
        log.info("[getStringValueFromDictionary] dictTemplateValues = {}", dictTemplateValues);
//        log.info("[getValueFromSimpleField] dictTemplateValues = {}", dictTemplateValues);


//        for(Field field: fields) {
//
//            // By� mo�e warto�ci b�d� b��dne (zak�adamy, �e dostaniemy tutaj string)
//            String cn = field.getCn();
//            List<Map<String, Object>> dictTemplateValues = FieldsManagerUtils.getMultiDictionaryExtractedValues(fm, cn, getter);
//
//            log.info("[getValueFromSimpleField] dictTemplateValues = {}", dictTemplateValues);
//
//        }

        return dictTemplateValues;
    }

    static List<Map<String, Object>> getValueFromMultiDictionary(Field f, FieldsManager fm) throws EdmException {
        log.trace("[getValueFromMultiDictionary] cn = {}", f.getCn());
        String cn = f.getCn();
        List<Map<String, Object>> dictTemplateValues = FieldsManagerUtils.getMultiDictionaryExtractedValues(fm, cn, getter);

        // TODO
        log.info("[getValueFromMultiDictionary] dictTemplateValues = {}", dictTemplateValues);

        return dictTemplateValues;
    }

    static Map<String, Object> getValueFromDictionary(Field f, FieldsManager fm) throws EdmException {
        log.trace("[getValueFromMultiDictionary] cn = {}", f.getCn());
        String cn = f.getCn();

        Map<String, Object> dictTemplateValues = FieldsManagerUtils.getDictionaryExtractedValues(fm, cn, fm.getLongValue(f.getCn()), getter);

        // TODO
        log.info("[getValueFromMultiDictionary] dictTemplateValues = {}", dictTemplateValues);

        return dictTemplateValues;
    }

    static JsonArray getJsonFromMap(Map<String, Object> map, FieldsManager fm) throws Exception {
        final JsonArray ret = new JsonArray();

        for(Map.Entry<String, Object> entry: map.entrySet()) {
            final Field field = fm.getField(entry.getKey());
            JsonObject json = getJsonFromEntry(entry, field);
            if(json != null) {
                ret.add(json);
            }
        }

        return ret;
    }

    static JsonObject getJsonFromEntry(Map.Entry<String, Object> entry, Field field) throws Exception {
        final JsonObject ret = new JsonObject();

        final String cn = entry.getKey();
        final Object value = entry.getValue();

//        final Field field = fm.getField(cn);

//        if(field == null) {
//            log.warn("[getJsonFromEntry] field is null, entry = {}", entry);
//            return null;
//        }

        log.debug("[getJsonFromEntry] entry = {}, cn = {}", entry, cn);

        ret.addProperty("cn", cn);
        ret.addProperty("name", field != null ? field.getName() : cn);

        // warto��, kt�r� dodamy jako 'value' do `ret`
        JsonElement jsonValue;

        // null
        if(value == null) {
            jsonValue = new JsonNull();
        }
        // dictionary
        else if(value instanceof Map) {
//            Map<String,Field> fieldMap = fieldListToMap(field.getFields());
            jsonValue = getJsonValueFromDictionary((Map)value);
        }
        // multi dictionary
        else if(value instanceof List) {
//            Map<String,Field> fieldMap = fieldListToMap(field.getFields());
            jsonValue = getJsonValueFromMultiDictionary((List)value);
        }
        // simple field
        else if(value instanceof String) {
            jsonValue = getJsonValueFromString((String)value);
        }
        // stringable type
        else if(value instanceof Number || value instanceof Date) {
            jsonValue = new JsonPrimitive(value.toString());
        }
        // unknown type
        else {
            log.warn("[getJsonFromEntry] Unknown value type, value.class = " + value.getClass() + ", value = " + value + ", ignoring");
            jsonValue = new JsonNull();
        }

        ret.add("value", jsonValue);

        return ret;
    }

    static JsonElement getJsonValueFromString(String value) {
        return new JsonPrimitive(value);
    }

    /**
     *
     * @param dictionary (cn => value), value traktujemy jako string
     * @return
     */
    static JsonElement getJsonValueFromDictionary(Object dictionary) throws Exception {
        JsonArray ret = new JsonArray();
        if (dictionary instanceof Map)
        {
            for(Map.Entry<String, Object> entry: ((Map<String, Object>)dictionary).entrySet()) {
                //            Field field = fieldMap.get(entry.getKey());
                JsonObject json = getJsonFromEntry(entry, null);
                if(json != null) {
                    ret.add(json);
                }
            }
        }


        return ret;
    }

    static JsonElement getJsonValueFromMultiDictionary(List<Map<String, Object>> list) throws Exception {
        log.debug("[getJsonValueFromMultiDictionary] list = {}", list);
        JsonArray ret = new JsonArray();

        for(Object map: list) {
            ret.add(getJsonValueFromDictionary(map));
        }

        return ret;
    }

    private static Map<String, Field> fieldListToMap(List<Field> fields) {
        Map<String, Field> map = new HashMap<String, Field>();

        if(fields == null) {
            log.warn("[fieldListToMap] fields are null");
            return map;
        }

        for(Field field: fields) {
            map.put(field.getCn(), field);
        }

        return map;
    }

    /**
     * <code>getJsonFromMap(getMapFromDocument(document))</code>
     * @param document
     * @return
     */
    public static JsonArray getJsonFromDocument(Document document) throws Exception {
        return getJsonFromMap(getMapFromDocument(document), document.getFieldsManager());
    }

    static byte[] getBinaryJsonFromDocument(Document document) throws Exception {
        return serializeString(getJsonFromDocument(document).toString());
    }

    static byte[] serializeString(String str) {
        return str.getBytes();
    }
    static String unserializeString(byte[] binary) {
        return new String(binary);
    }

    static JsonElement getJsonFromString(String str) throws Exception {
        final JsonParser parser = new JsonParser();

        JsonElement obj;
        try
        {
            obj = parser.parse(str);
        }
        catch(Exception ex)
        {
            throw new Exception("[getJsonFromString] cannot parse json: "+str, ex);
        }

        return obj;
    }

    public static Map<String, Object> getMapFromJson(JsonArray json) throws Exception {

        Map<String, Object> ret = new HashMap<String, Object>();

        for(JsonElement elem: json) {

            if(! elem.isJsonObject()) {
                throw new Exception("elem is not object, elem = "+elem);
            }

            JsonObject obj = elem.getAsJsonObject();

            // key (ret)
            final String cn = obj.get("cn").getAsString();

            // value (ret)
            Object value;

            final JsonElement jsonValue = obj.get("value");

            // dictionary / multi dictionary
            /** Bugfix nullpointer */
            if (jsonValue == null)
            {
            	value = null;
            }
            else
            {
	            if(jsonValue.isJsonArray()) { 
	
	                JsonArray arr = jsonValue.getAsJsonArray();
	
	                // niezdeterminowana warto�� (dictionary vs multi dictionary)
	                if(arr.size() == 0) {
	                    value = null;
	                }
	                // dictionary
	                else if(! arr.get(0).isJsonArray()) {
	                    value = getMapFromJson(arr);
	                }
	                // multi dictionary
	                else {
	                    value = getListFromJson(arr);
	                }
	
	            }
	            // simple field
	            else if(jsonValue.isJsonPrimitive()) {
	                value = jsonValue.getAsString();
	            }
	            // null
	            else if(jsonValue.isJsonNull()) {
	                value = null;
	            }
	            // unknown
	            else {
	                throw new Exception("unknown type of value, elem = "+elem+", cn = "+cn+", value.class = "+jsonValue.getClass()+", value = "+jsonValue);
	            }
        	}

            ret.put(cn, value);
        }

        return ret;
    }

    private static Object getListFromJson(JsonArray arr) throws Exception {
        List<Object> list = new ArrayList<Object>();

        for(JsonElement elem: arr) {
            final JsonArray innerArr = elem.getAsJsonArray();
            list.add(getMapFromJson(innerArr));
        }

        return list;
    }
}
