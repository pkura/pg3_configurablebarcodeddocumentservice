package pl.compan.docusafe.core.templating;

import com.google.common.base.Optional;
import com.google.gson.*;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Snapshot dokumentu w formie mapy Key=>Value, bez przechowywania informacji o typach p�l.
 * Oznacza to, �e mapa przechowuje tylko warto�ci tekstowe.
 *
 * @author Jan �wi�cki <a href="mailto:jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
@Entity
@Table(name = "ds_document_snapshot")
public class DocumentSnapshot {

    private static final Logger log = LoggerFactory.getLogger(DocumentSnapshot.class);

    @Id
    @SequenceGenerator(name = "ds_document_snapshot_seq", sequenceName = "ds_document_snapshot_seq")
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "ds_document_snapshot_seq")
    private Long id;

    /**
     * U�ytkownik, kt�ry utworzy� snapshot dokumentu
     */
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private UserImpl user;

    @ManyToOne
    @JoinColumn(name = "document_id", referencedColumnName =  "id")
    private Document document;

    /**;
     * Data utworzenia
     */
    @Column(name = "ctime")
    private Date ctime;

    /**
     * Data modyfikacji
     */
    @Column(name = "mtime")
    private Date mtime;

    @Column(name = "title", length = 64)
    private String title;

    @Column(name = "content")
    //@Lob - niepoprawnie dzia�a z postgresql
    private byte[] content;

    /**
     * Mapa warto�ci, wgrywana z pola {@code content}.
     */
//    @Transient
//    private List<Map<String, String>> values;

    /**
     * Mapa warto�ci z pola {@code content} w formacie json.
     */
    @Transient
    private JsonArray jsonValues;

    @Transient
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public DocumentSnapshot() {

    }

    public DocumentSnapshot(Document document, UserImpl user) throws Exception {
        setCtime(new Date());
        mtime = ctime;
        this.user = user;
        this.document = document;

        this.content = DocumentSnapshotSerializer.getBinaryJsonFromDocument(document);
    }

//    private static JsonArray getJsonFromDocument(Document document) throws EdmException {
//
//        JsonArray fields = new JsonArray();
//
//        final FieldsManager fm = document.getFieldsManager();
//
//        log.info("[getJsonFromDocument] saving fields");
//
//        for (Field field: fm.getFields()) {
//            log.info("[getJsonFromDocument]     field.cn = {}, field.classname = {}", field.getCn(), field.getClass().getSimpleName());
//
//            // dictionary / multi dictionary
//            if(field instanceof AbstractDictionaryField) {
//
//                // dictionary
//                if(! field.isMultiple()) {
//
//                    final List<Field> innerFields = field.getFields();
//
//                    if(innerFields == null) {
//                        log.warn("[getJsonFromDocument] dictionary, fields are null, cn = {}", field.getCn());
//                    } else {
//                        JsonArray innerJsonFields = new JsonArray();
//
//                        for(Field innerField: innerFields) {
//                            // TODO: nie zwraca poprawnie warto�ci, bo fieldsManager nie linkuje cn�w z p�l dictionary
//                            final JsonObject jf = createJsonFieldFromSimpleField(innerField, fm);
//                            innerJsonFields.add(jf);
//                        }
//
//                        final JsonObject jsonField = createJsonField(field.getCn(), field.getName(), innerJsonFields);
//                        fields.add(jsonField);
//                    }
//                }
//                // multi dictionary
//                else {
//
////                    FieldsManagerUtils.FieldValuesGetter getter = new FieldsManagerUtils.DateEnumValuesGetter(false);
////                    List<Map<String, Object>> dictTemplateValues = FieldsManagerUtils.getMultiDictionaryExtractedValues(fm, field.getCn(), getter);
////
////                    JsonArray innerJsonFields = new JsonArray();
////
////                    for(Map<String, Object> map: dictTemplateValues) {
////
////
////
////                        JsonArray fs = new JsonArray();
////
////                    }
////
////                    final JsonObject jsonField = createJsonField(field.getCn(), field.getName(), innerJsonFields);
////                    fields.add(jsonField);
//
//                }
//            }
//            // simple field
//            else {
//                JsonObject jsonField = createJsonFieldFromSimpleField(field, fm);
//                fields.add(jsonField);
//            }
//
////            final Object value = fieldsManager.getValue(field.getCn());
//
////            List<Field> fields = field.getFields();
////            if(fields != null) {
////
////                for (Field f: fields) {
////                    Map<String, String> m = new HashMap<String, String>();
////                    m.put("cn", field.getCn()+"_"+f.getCn());
////                    m.put("name", field.getName()+": "+f.getName());
////                    m.put("value", fieldsManager.getStringValue(f.getCn()));
////                    vs.add(m);
////                }
////
////            } else {
////                Map<String, String> map = new HashMap<String, String>();
////
////                map.put("cn", field.getCn());
////                map.put("name", field.getName());
////                map.put("value", fieldsManager.getStringValue(field.getCn()));
////
////                vs.add(map);
////            }
//        }
//
//        return fields;
//    }

//    private static JsonObject createJsonField(String cn, String name, JsonElement value) {
//        JsonObject obj = new JsonObject();
//        obj.addProperty("cn", cn);
//        obj.addProperty("name", name);
//        obj.add("value", value);
//        return obj;
//    }

//    private static JsonObject createJsonFieldFromSimpleField(Field field, FieldsManager fm) throws EdmException {
//        String value = fm.getStringValue(field.getCn());
//
//        JsonElement v;
//        if(value == null) {
//            v = new JsonNull();
//        } else {
//            v = new JsonPrimitive(value);
//        }
//
//        JsonObject jsonField = createJsonField(field.getCn(), field.getName(), v);
//        return jsonField;
//    }

    /**
     * Konwertuje {@code content}, na map� String=>String.
     * {@code content} musi by� w formacie json.
     *
     * @return
     */
//    public static List<Map<String, String>> unserializeValues(byte[] content) throws Exception {
//        JsonArray obj = unserializeValuesAsJson(content);
//
//        List<Map<String, String>> ret = jsonToMap(obj);
//
//        return ret;
//    }

    /**
     * Konwertuje obiekt json do mapy String=>String.
     *
     * @param obj
     * @return map from json object
     */
//    private static List<Map<String, String>> jsonToMap(JsonArray obj) {
//        final List<Map<String, String>> ret = new ArrayList<Map<String, String>>();
//
////        final Set<Map.Entry<String,JsonElement>> entries = obj.entrySet();
//
//        for(JsonElement value: obj)
//        {
//            if(value.isJsonObject()) {
//                JsonObject innerObj = value.getAsJsonObject();
//
//                final Set<Map.Entry<String, JsonElement>> innerEntries = innerObj.entrySet();
//
//                Map<String, String> innerMap = new HashMap<String, String>();
//
//                for(Map.Entry<String, JsonElement> innerEntry: innerEntries) {
//                    JsonElement v = innerEntry.getValue();
//                    innerMap.put(innerEntry.getKey(), v.isJsonPrimitive() ? v.getAsString() : "");
//                }
//
//                ret.add(innerMap);
//            }
//        }
//
//        return ret;
//    }

    /**
     * Konwertuje {@code content} na {@code JsonObject}.
     * {@code content} musi by� w formacie json.
     *
     * @param content
     * @return
     * @throws EdmException
     */
//    private static JsonArray unserializeValuesAsJson(byte[] content) throws Exception {
//        final String json = new String(content);
//
//        final JsonParser parser = new JsonParser();
//
//        JsonArray obj;
//        try
//        {
//           obj = (JsonArray)parser.parse(json);
//        }
//        catch(Exception ex)
//        {
//            throw new Exception("[unserializeValuesAsJson] cannot parse json: "+json, ex);
//        }
//
//        return obj;
//    }

//    private static byte[] getSerializedValues(List<Map<String, String>> values) throws Exception {
//        log.debug("[getSerializedValues] values = {}", values);
//
//        if(values == null)
//        {
//            throw new Exception("values cannot be null");
//        }
//
//        // new Gson().toJson(values) omija�o warto�ci null
//        // final String json = new Gson().toJson(values);
//
//        JsonArray obj = new JsonArray();
//
//        for (Map<String, String> entry : values) {
//            JsonObject innerJsonObject = new JsonObject();
//            for(Map.Entry<String, String> innerEntry: entry.entrySet()) {
//                innerJsonObject.addProperty(innerEntry.getKey(), innerEntry.getValue());
//            }
//            obj.add(innerJsonObject);
//        }
//
//        String json = obj.toString();
//
//        log.debug("[getSerializedValues] json = {}", json);
//        final byte[] ret = json.getBytes();
//
//        return ret;
//    }

//    private static byte[] serializeValuesFromJson(JsonArray values) throws Exception {
//        return getSerializedValues(jsonToMap(values));
//    }

    public static Collection<DocumentSnapshot> findByDocumentId(Long id) throws EdmException {

        Document document = Document.find(id);

        final Criteria criteria = DSApi.context().session().createCriteria(DocumentSnapshot.class);

        criteria.add(Restrictions.eq("document", document));
        criteria.addOrder(Order.asc("ctime"));

        return criteria.list();
    }

    public static Optional<DocumentSnapshot> find(Long id) {
        final Criteria criteria;
        try {
            criteria = DSApi.context().session().createCriteria(DocumentSnapshot.class);
            criteria.add(Restrictions.eq("id", id));

            final List<DocumentSnapshot> list = criteria.list();

            if(list.size() == 0)
            {
                return Optional.absent();
            }
            else
            {
                return Optional.of(list.get(0));
            }

        } catch (EdmException e) {
            log.error("", e);
            return Optional.absent();
        }
    }

    public static Optional<DocumentSnapshot> findCheckAccess(Long id) throws EdmException {
        Optional<DocumentSnapshot> opt = find(id);

        if(opt.isPresent())
        {
            if(! opt.get().hasAccess()) {
                throw new SecurityException("Brak uprawnien do snapshotu "+id);
            } else {
                return opt;
            }
        } else {
            return opt;
        }
    }

    public static boolean existsCheckAccess(Long id) throws EdmException {
        return findCheckAccess(id).isPresent();
    }

    public static void existsCheckAccessOrError(Long id) throws EdmException {
        if(! existsCheckAccess(id)) {
            throw new EdmException("Brak snapshotu o id "+id);
        }
    }

    public DSUser getUser() {
        return user;
    }

    public void setUser(UserImpl user) {
        this.user = user;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = new Date(ctime.getTime());
    }

    public Date getMtime() {
        return mtime;
    }

    public void setMtime(Date mtime) {
        this.mtime = new Date(mtime.getTime());
    }

    public Long getId() {
        return id;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Document getDocument() {
        return document;
    }

    public JsonArray getJson() throws Exception {
        String json = DocumentSnapshotSerializer.unserializeString(content);
        return (JsonArray)DocumentSnapshotSerializer.getJsonFromString(json);
    }

//    public List<Map<String, String>> getValues() throws Exception {
//        if(values == null)
//        {
//            values = unserializeValues(content);
//        }
//        return values;
//    }

//    public Map<String, Object> getValuesForTemplate()

    /**
     * Zwraca map� cn=>value z warto�ci.
     * @return
     * @throws Exception
     */
//    public Map<String, Object> getValuesForTemplate() throws Exception {
//        if(jsonValues == null) {
//            jsonValues = unserializeValuesAsJson(content);
//        }
//
//        Map<String, Object> ret = new HashMap<String, Object>();
//
//        for(JsonElement elem: jsonValues) {
//
//            if(elem.isJsonObject()) {
//                JsonObject elemObj = elem.getAsJsonObject();
//
//                JsonElement jsonCn = elemObj.get("cn");
//                JsonElement jsonValue = elemObj.get("value");
//
//                String cn = jsonCn.getAsString();
//                Optional<Object> value = getTemplateValue(jsonValue);
//
//                if(value.isPresent()) {
//                    ret.put(cn, value.get());
//                }
//
//            } else {
//                log.error("[getValuesForTemplate] elem is not object: {}", elem);
//
//
//            }
//        }
//
//        return ret;
//    }

    /**
     * <p>Metoda mapuje warto�� <code>jsonValue</code> na javowy odpowiednik Apache Velocity</p>
     *
     * <p>Obs�ugiwane s�:
     *
     * <ul>
     *     <li>warto�ci prymitywne (wraz z nullami), wtedy zwracamy <b>String</b></li>
     *     <li>warto�ci dictionary, wtedy zwracamy <b>Map&lt;String, String&gt;</b></li>
     *     <li>warto�ci multi dictionary, wtedy zwracamy <b>List&lt;Map&lt;String, String&gt;&gt;</b></li>
     * </ul>
     *
     * </p>
     *
     * @param jsonValue
     * @return Optional(Object)
     */
//    private static Optional<Object> getTemplateValue(JsonElement jsonValue) {
//        // simple value
//        if(jsonValue.isJsonPrimitive() || jsonValue.isJsonNull()) {
//            return Optional.of((Object) jsonValue.getAsString());
//        }
//        // dictionary / multi dictionary
//        else if(jsonValue.isJsonArray()) {
//
//            JsonArray jsonArray = jsonValue.getAsJsonArray();
//
//            if(jsonArray.size() == 0) {
//                log.warn("[getTemplateValue] empty array, jsonValue = {}", jsonValue);
//                return Optional.absent();
//            } else {
//
//                // dictionary
//                // return Map<String, String>
//                if(! jsonArray.get(0).isJsonArray()) {
//                    try {
//                        return Optional.of((Object)getMapFromJsonArrayOfFields(jsonArray));
//                    } catch (Exception e) {
//                        log.error("[getTemplateValue] cannot get map from dictionary, jsonValue = {}", jsonValue, e);
//                        return Optional.absent();
//                    }
//                }
//                // multi dictionary
//                else {
//
//                    List<Map<String, String>> list = new ArrayList<Map<String, String>>();
//
//                    for(JsonElement elem: jsonArray) {
//
//                        JsonArray innerArray = elem.getAsJsonArray();
//
//                        try {
//                            Map<String, String> map = getMapFromJsonArrayOfFields(innerArray);
//                            list.add(map);
//                        } catch (Exception e) {
//                            log.error("[getTemplateValue] cannot get map from multi dictionary element, jsonValue = {}, elem = {}", jsonValue, innerArray, e);
//                        }
//                    }
//
//                    return Optional.of((Object)list);
//                }
//            }
//        }
//        else {
//            log.error("[getTemplateValue] unknown jsonValue type, jsonValue = {}, jsonValue.class = {}", jsonValue, jsonValue.getClass());
//            return Optional.absent();
//        }
//    }
//
//
//    /**
//     * <p>Konwertuje obiekt json na map�.</p>
//     *
//     * <p>Zak�adamy, �e <code>obj</code> jest obiektem json z warto�ciami tekstowymi
//     * (warto�ci s� wrzucane do mapy poprzez metod� <code>getAsString</code>.</p>
//     *
//     * @param obj
//     * @return
//     */
//    private static Map<String, String> jsonFlatObjectToMap(JsonObject obj) {
//        Map<String, String> map = new HashMap<String, String>();
//
//        for (Map.Entry<String, JsonElement> entry : obj.getAsJsonObject().entrySet()) {
//            map.put(entry.getKey(), entry.getValue().getAsString());
//        }
//
//        return map;
//    }

    /**
     * <p>Konwertuje json array zawieraj�ce list� p�l na map� cn=>value.</p>
     *
     * <p>Zak�adamy format elemnt�w <code>array</code> jako: <code>{
     *     "cn": ...,
     *     "name": ...,
     *     "value": ...
     * }</code>, gdzie <code>cn</code>, <code>name</code> oraz <code>value</code> powinny by�
     * stringami (w og�lno�ci nie jest to spe�nione dla pola <code>value</code>)</p>
     *
     * @param array
     * @return
     */
//    private static Map<String, String> getMapFromJsonArrayOfFields(JsonArray array) throws Exception {
//        Map<String, String> map = new HashMap<String, String>();
//
//        for(JsonElement elem: array) {
//            if(! elem.isJsonObject()) {
//                throw new Exception("elem is not JsonObject, elem = "+elem+", array = "+array);
//            } else {
//                final JsonObject elemObject = elem.getAsJsonObject();
//
//                map.put(elemObject.get("cn").getAsString(), elemObject.get("value").getAsString());
//            }
//        }
//
//        return map;
//    }

//    private void setValues(List<Map<String, String>> values) throws Exception {
//        this.values = values;
//        this.content = getSerializedValues(values);
//    }

//    public JsonArray getJsonValues() throws Exception {
//        if(jsonValues == null)
//        {
//            jsonValues = unserializeValuesAsJson(content);
//        }
//        return jsonValues;
//    }

//    public void setJsonValues(JsonArray values) throws Exception {
//        jsonValues = values;
//        content = serializeValuesFromJson(jsonValues);
//    }

    public JsonObject getJsonView() throws Exception {
        JsonArray vs = getJson();

        JsonObject ret = new JsonObject();

        ret.addProperty("id", id);
        ret.addProperty("title", title);
        ret.addProperty("documentId", document.getId());
        ret.addProperty("dockindCn", document.getDocumentKind().getCn());
        ret.addProperty("ctime", dateFormat.format(ctime));
        ret.addProperty("mtime", dateFormat.format(mtime));
        ret.addProperty("user", user.getFirstnameLastnameName());
        ret.add("fields", vs);

        log.debug("[getJsonView] fields = {}", vs.toString());

        return ret;
    }

    public JsonObject getModifiedJsonView(Map<String, String> map) throws Exception {
        final JsonObject jsonView = getJsonView();

        final Set<Map.Entry<String, String>> entries = map.entrySet();

        for(Map.Entry<String, String> entry : entries) {
           jsonView.addProperty(entry.getKey(), entry.getValue());
        }

        return jsonView;
    }

    public void setPlainJsonValues(String values) {
        content = values.getBytes();
    }

    public void updateMtime() {
        this.mtime = new Date();
    }

    public static void filterByPermission(DSUser dsUser, Collection<DocumentSnapshot> snapshots, List<DocumentSnapshot> filteredSnapshots) throws EdmException {

        Iterator<DocumentSnapshot> it = snapshots.iterator();
        
        while(it.hasNext())
        {
            final DocumentSnapshot snapshot = it.next();

            boolean hasAccess = snapshot.hasAccess(dsUser);

            if(hasAccess) {
                filteredSnapshots.add(snapshot);
            }
        }

    }

    public boolean hasAccess(DSUser currentUser) throws EdmException {
        final boolean hasTemplatesPermission = DSApi.context().hasPermission(currentUser, DSPermission.TEMPLATES);
        final boolean isOwner = this.getUser().getId().equals(currentUser.getId());
        final boolean isAdmin = DSApi.context().isAdmin();

        boolean hasAccess = isOwner || hasTemplatesPermission || isAdmin;

        return hasAccess;
    }

    public boolean hasAccess() throws EdmException {
        DSUser currentUser = DSApi.context().getDSUser();

        boolean hasAccess = hasAccess(currentUser);

        return hasAccess;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
