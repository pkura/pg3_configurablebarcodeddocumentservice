package pl.compan.docusafe.core.templating;


import pl.compan.docusafe.core.repository.Repository;
import pl.compan.docusafe.core.repository.RepositoryProvider;
import pl.compan.docusafe.core.repository.SimpleFileSystemRepository;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.text.Format;
import java.util.Map;

/**
 * Klasa narz�dziowa do korzystania z szablon�w
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class Templating {
    private Templating(){}

    //klasa ThreadSafe
    private final static RtfVelocityTemplate rtfVelocityTemplating = new RtfVelocityTemplate();
    private final static RtfTemplating rtfTemplating = new RtfTemplating();
    private final static Logger log = LoggerFactory.getLogger(Templating.class);
    public static void rtfTemplate(String templateFileName, Map<String,?> params, OutputStream os, String special) throws Exception
    {
        rtfTemplate(templateFileName, params, os, null, special);
    }

    public static void rtfTemplate(String templateFileName, Map<String, ?> params, OutputStream os, Map<Class, Format> defaultFormatters, String special) throws Exception {
        Repository repo = RepositoryProvider.getTemplateRepository();

        rtfTemplate(repo, templateFileName, params, os, defaultFormatters, special);
    }

    public static void rtfTemplate(Repository repository, String templateFileName, Map<String, ?> params, OutputStream os, String special) throws Exception {
        rtfTemplate(repository, templateFileName, params, os, null, special);
    }

    public static void rtfTemplate(Repository repository, String templateFileName, Map<String, ?> params, OutputStream os, Map<Class, Format> defaultFormatters, String special) throws Exception {
        // wymuszenie generowania tempalteow w ifpanie spowodowane inna werjsa procesow dla starcyh dokumentow
        if(special.equals("true") || templateFileName.equals("Faktura_ZestawienieFaktura.rtf") || templateFileName.equals("Delegacja_InstrukcjaWyjazdowa.rtf"))
        {
            log.info("Generate template with special");
            rtfVelocityTemplating.setDefaultFormatters(defaultFormatters);
            rtfVelocityTemplating.process(
                    repository.get(templateFileName),
                    os,
                    params);
        }
        else
        {
            log.info("Generate template");
            rtfTemplating.process(
                    repository.get(templateFileName),
                    os,
                    params);
        }

    }
}
