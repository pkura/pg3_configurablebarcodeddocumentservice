package pl.compan.docusafe.core.record.construction;

import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.type.Type;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
/* User: Administrator, Date: 2005-11-29 10:25:43 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ConstructionDecision.java,v 1.13 2008/11/24 21:47:12 wkuty Exp $
 */
public class ConstructionDecision
{
    public static final String RES_ACCEPT_TEMPLATE = "record.construction.ConstructionDecision.accept";
    public static final String RES_UNACCEPT_TEMPLATE = "record.construction.ConstructionDecision.unaccept";

    private Long id;
    private Integer hversion;
    private Date ctime;
    private String author;
    /**
     * True, je�eli decyzja pozytywna.
     */
    private Boolean accepted;
    private Date date;
    /**
     * Numer subrejestru.  Warto�� identyczna jak w powi�zanym wniosku.
     */
    private String registryNo;
    private Integer decisionNoInRegistry;
    private Integer sequenceId;
    private String decisionNo;
    private ConstructionApplication constructionApplication;
    private String remarks;
    /** za�atwiaj�cy spraw� */
    private String operator;

    public void create() throws EdmException
    {
        if (constructionApplication == null)
            throw new NullPointerException("constructionApplication");
        if (registryNo == null)
            throw new NullPointerException("registryNo");
        if (sequenceId == null)
            throw new NullPointerException("sequenceId");
        if (decisionNo == null)
            throw new NullPointerException("decisionNo");
        if (date == null)
            throw new NullPointerException("date");
        if (accepted == null)
            throw new NullPointerException("accepted");

        this.author = DSApi.context().getPrincipalName();
        this.ctime = new Date();

        try
        {
            List count = DSApi.context().classicSession().find(
                "select count(cd) from cd in class "+ConstructionDecision.class.getName()+
                    " where cd.registryNo = ? and cd.sequenceId = ?",
                new Object[] { registryNo, sequenceId },
                new Type[] { Hibernate.STRING, Hibernate.INTEGER });
            if (count.get(0) instanceof Long &&
                    (Long) count.get(0) > 0)
                throw new EdmException("Istnieje ju� wniosek nr "+sequenceId+
                " w subrejestrze "+registryNo);

            constructionApplication.setConstructionDecision(this);
            constructionApplication.setDecisionNo(decisionNo);
            constructionApplication.setDecisionNoInRegistry(decisionNoInRegistry);
            constructionApplication.setDecisionDate(date);
            constructionApplication.setAccepted(accepted);

            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    public static ConstructionDecision findByApplicationId(Long id) throws EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(ConstructionDecision.class);
            criteria.add(Expression.eq("constructionApplication.id", id));
            List decisions = criteria.list();
            if (decisions.size() > 1)
                throw new EdmException("Znaleziono dwie decyzja zezwolania na budow� zwi�zane " +
                    "z wnioskiem nr "+id);
            if (decisions.size() > 0)
                return (ConstructionDecision) decisions.get(0);

            throw new EntityNotFoundException("Nie znaleziono decyzji zezwoleniu na budow� " +
                "zwi�zanego z wnioskiem nr "+id);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Integer suggestSequenceId(String registryNo) throws EdmException
    {
        try
        {
            List max = DSApi.context().classicSession().find(
                "select max(cd.sequenceId) from cd in class "+ConstructionDecision.class.getName()+
                    " where cd.registryNo = ?",
                registryNo != null ? registryNo.toUpperCase() : null, Hibernate.STRING);
            if (max.size() > 0 && max.get(0) instanceof Integer)
                return (Integer) max.get(0) + 1;
            else
                return 1;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException{
        
        Persister.delete(this);
    }

    private final static NumberFormat y2 = NumberFormat.getInstance();
    {
        y2.setMinimumIntegerDigits(2);
        y2.setMaximumIntegerDigits(2);
    }
    
    public static Map<String,Object> suggestDecisionNo(ConstructionApplication ca, Integer sequenceId) throws EdmException
    {
      /*  return ca.getRegistryNo() + "." +
            ca.getOfficeCase().getRwa().getRwa() + "-" +
            sequenceId + "/" +
            y2.format(ca.getOfficeCase().getYear() % 100);*/

        String registryNo = ca.getRegistryNo();
        Integer year = new Integer(GlobalPreferences.getCurrentYear());
        
        ConstructionDecisionCounter counter = ConstructionDecisionCounter.find(year, registryNo);
        if (counter == null) 
        {
            /* nie bylo jeszcze decyzji o danym prefiksie sprawy */
            counter = new ConstructionDecisionCounter();
            
            counter.setYear(year);
            counter.setRegistryNo(registryNo);
            
            counter.create();
        }
        
        Map<String,Object> result = new HashMap<String,Object>();
        
        //String prefix = Configuration.getProperty("construction.decision.prefix");
        String decNo = /*prefix + "." +*/ registryNo + "-" +   
            (counter.getNextNumber() < 10?"0":"") + 
            counter.getNextNumber() + "/" +
            y2.format(ca.getOfficeCase().getYear() % 100);
        result.put("decisionNo", decNo);
        result.put("decisionNoInRegistry", counter.getNextNumber());
        counter.setNextNumber(counter.getNextNumber() + 1);
        
        return result;
    }

    public static ConstructionDecision find(Long id) throws EdmException
    {
        return Finder.find(ConstructionDecision.class, id);
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public Boolean getAccepted()
    {
        return accepted;
    }

    public void setAccepted(Boolean accepted)
    {
        this.accepted = accepted;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getRegistryNo()
    {
        return registryNo;
    }

    public void setRegistryNo(String registryNo)
    {
        this.registryNo = registryNo != null ? registryNo.toUpperCase() : null;
    }

    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public Integer getDecisionNoInRegistry()
    {
        return decisionNoInRegistry;
    }

    public void setDecisionNoInRegistry(Integer decisionNoInRegistry)
    {
        this.decisionNoInRegistry = decisionNoInRegistry;
    }
    
    public String getDecisionNo()
    {
        return decisionNo;
    }

    public void setDecisionNo(String decisionNo)
    {
        this.decisionNo = decisionNo;
    }

    public ConstructionApplication getConstructionApplication()
    {
        return constructionApplication;
    }

    public void setConstructionApplication(ConstructionApplication constructionApplication)
    {
        this.constructionApplication = constructionApplication;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }
    
    public String getOperator()
    {
        return operator;
    }
    
    public void setOperator(String operator)
    {
        this.operator = operator;
    }
}
