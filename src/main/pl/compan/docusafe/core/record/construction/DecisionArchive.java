package pl.compan.docusafe.core.record.construction;


//import java.sql.Date;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.FormQuery;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.QueryForm.SortField;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class DecisionArchive {
	private long id;
	private String name;
	private String address;
	private String object_kind;
	private String proposition_no;
	private Date proposition_in_date;
	private String decision_no;
	private Date decision_date;
	private String reference;
	private String decision_after;
	private String notice;
	
	private static final Logger log = LoggerFactory.getLogger(DecisionArchive.class);
	StringManager sm = GlobalPreferences.loadPropertiesFile("",null);
	public boolean create() throws EdmException
    {
		boolean created;
        /*try 
        {
            DSApi.context().session().save(this);
            created = true;
        }
        catch (HibernateException e) 
        {
            created = false;
            log.error("Blad dodania Decyzji"+e.getMessage());
            throw new EdmException(sm.getString("BladDodaniaDecyzji")+e.getMessage());
        }*/
		try
        {
            Criteria crit = DSApi.context().session().createCriteria(DecisionArchive.class);
            if (decision_no != null) crit.add(Expression.eq("decision_no", decision_no).ignoreCase());

            if (crit.list().size() == 0)
            {
                DSApi.context().session().save(this);
                return true;
            }

            return false;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	
	public static class DAQuery extends FormQuery{
		private String name;
		private String address;
		private String object_kind;
		private String proposition_no;
		private String proposition_no_from;
		private String proposition_no_to;
		private Date proposition_in_date;
		private String decision_no;
		private String decision_no_from;
		private String decision_no_to;
		private Date decision_date;
		private String reference;
		private String decision_after;
		private String notice;
        private Integer limit;
        private Integer offset;
        private SortField sortFiled;
        
        public DAQuery(){
        	super();
        }
        
		public DAQuery(Integer limit, Integer offset, String name, String address, String object_kind,
				String proposition_no,
				String decision_no) {
			this.name = name;
			this.address = address;
			this.object_kind = object_kind;
			this.proposition_no = proposition_no;
			this.decision_no = decision_no;
			this.limit = limit;
			this.offset = offset;
		}
		
		public DAQuery(Integer limit, Integer offset) {
			this.limit = limit;
			this.offset = offset;
		}
		
		private Date proposition_in_date_from;
		private Date proposition_in_date_to;
		private Date decision_date_from;
		private Date decision_date_to;

		public Date getProposition_in_date_from() {
			return proposition_in_date_from;
		}

		public void setProposition_in_date_from(Date proposition_in_date_from) {
			this.proposition_in_date_from = proposition_in_date_from;
		}

		public Date getProposition_in_date_to() {
			return proposition_in_date_to;
		}

		public void setProposition_in_date_to(Date proposition_in_date_to) {
			this.proposition_in_date_to = proposition_in_date_to;
		}

		public Date getDecision_date_from() {
			return decision_date_from;
		}

		public void setDecision_date_from(Date decision_date_from) {
			this.decision_date_from = decision_date_from;
		}

		public Date getDecision_date_to() {
			return decision_date_to;
		}

		public void setDecision_date_to(Date decision_date_to) {
			this.decision_date_to = decision_date_to;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getObject_kind() {
			return object_kind;
		}
		public void setObject_kind(String object_kind) {
			this.object_kind = object_kind;
		}
		public String getProposition_no() {
			return proposition_no;
		}
		public void setProposition_no(String proposition_no) {
			this.proposition_no = proposition_no;
		}
		public Date getProposition_in_date() {
			return proposition_in_date;
		}
		public void setProposition_in_date(Date proposition_in_date) {
			this.proposition_in_date = proposition_in_date;
		}
		public String getDecision_no() {
			return decision_no;
		}
		public void setDecision_no(String decision_no) {
			this.decision_no = decision_no;
		}
		public Date getDecision_date() {
			return decision_date;
		}
		public void setDecision_date(Date decision_date) {
			this.decision_date = decision_date;
		}
		public String getReference() {
			return reference;
		}
		public void setReference(String reference) {
			this.reference = reference;
		}
		public String getDecision_after() {
			return decision_after;
		}
		public void setDecision_after(String decision_after) {
			this.decision_after = decision_after;
		}
		public String getNotice() {
			return notice;
		}
		public void setNotice(String notice) {
			this.notice = notice;
		}
		public void setLimit(Integer limit) {
			this.limit = limit;
		}
		public void setOffset(Integer offset) {
			this.offset = offset;
		}
		public SortField getSortFiled() {
			return sortFiled;
		}
		public void setSortFiled(SortField sortFiled) {
			this.sortFiled = sortFiled;
		}

		public String getProposition_no_from() {
			return proposition_no_from;
		}

		public void setProposition_no_from(String proposition_no_from) {
			this.proposition_no_from = proposition_no_from;
		}

		public String getProposition_no_to() {
			return proposition_no_to;
		}

		public void setProposition_no_to(String proposition_no_to) {
			this.proposition_no_to = proposition_no_to;
		}

		public String getDecision_no_from() {
			return decision_no_from;
		}

		public void setDecision_no_from(String decision_no_from) {
			this.decision_no_from = decision_no_from;
		}

		public String getDecision_no_to() {
			return decision_no_to;
		}

		public void setDecision_no_to(String decision_no_to) {
			this.decision_no_to = decision_no_to;
		}

		public int getLimit() {
			return limit;
		}
	}
	
	public static SearchResults<? extends DecisionArchive> search(DAQuery query) throws EdmException{
		try
        {
            Criteria c = DSApi.context().session().createCriteria(DecisionArchive.class);
            final WhereClause where = new WhereClause();
            FromClause from = new FromClause();
            final TableAlias appTable = from.createTable(DSApi.getTableName(ConstructionApplication.class));
            
            if (query.address != null)
            {//.ignoreCase()
                c.add(Expression.like("address",query.address));
            }
            if (query.decision_after != null)
            {
                c.add(Expression.eq("decision_after", query.decision_after));
            }
            if (query.decision_date != null)
            {
                c.add(Expression.eq("decision_date",query.decision_date));
            }   
            if (query.decision_no != null)
            {
                c.add(Expression.like("decision_no",query.decision_no));
            }
            if (query.name != null)
            {//.ignoreCase()
                c.add(Expression.like("name",query.name));
            }
            if (query.notice != null)
            {
                c.add(Expression.like("notice", query.notice));
            }
            if (query.object_kind != null)
            {
                c.add(Expression.like("object_kind",query.object_kind));
            }   
            if (query.proposition_in_date != null)
            {
                c.add(Expression.eq("proposition_in_date",query.proposition_in_date));
            }
            if (query.proposition_no != null)
            {//.ignoreCase()
                c.add(Expression.like("proposition_no",query.proposition_no));
            }
            if (query.reference != null)
            {
                c.add(Expression.like("reference", query.reference));
            }
            if(query.proposition_in_date_from != null)
            	c.add(Expression.ge("proposition_in_date", query.proposition_in_date_from));
            if(query.proposition_in_date_to != null)
            	c.add(Expression.le("proposition_in_date", query.proposition_in_date_to));
            if(query.decision_date_from != null)
            	c.add(Expression.ge("decision_date", query.decision_date_from));
            if(query.decision_date_to != null)
            	c.add(Expression.le("decision_date", query.decision_date_to));
            if(query.decision_no_from != null)
            	c.add(Expression.ge("decision_no", query.decision_no_from));
            if(query.decision_no_to != null)
            	c.add(Expression.le("decision_no", query.decision_no_to));
            

            QueryForm.SortField field = query.getSortFiled();
            
            if (field.isAscending())
                c.addOrder(Order.asc(field.getName()));
            else
                c.addOrder(Order.desc(field.getName()));
            
            List <DecisionArchive> list = (List <DecisionArchive>)c.list();
            
            if (list.size() == 0)
            {
                return SearchResultsAdapter.emptyResults(DecisionArchive.class);
            }
            else
            {
                int toIndex;
                int fromIndex = query.getOffset() < list.size() ? query.getOffset() : list.size()-1;
                if(query.getLimit()== 0)
                    toIndex = list.size(); 
                else
                    toIndex = (query.getOffset()+query.getLimit() <= list.size()) ? query.getOffset()+query.getLimit() : list.size();
                return new SearchResultsAdapter<DecisionArchive>(list.subList(fromIndex, toIndex), query.getOffset(), list.size(), DecisionArchive.class);
            }
        }
        catch (HibernateException e)
        {
            try
            {
            	LogFactory.getLog("eprint").debug("", e);
                throw new EdmHibernateException(e);
                
            }
            catch (EdmHibernateException e1)
            {
            	LogFactory.getLog("eprint").debug("", e1);
            }
        }
        catch (EdmException e)
        {
        	LogFactory.getLog("eprint").debug("", e);
        }
        return null;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getObject_kind() {
		return object_kind;
	}
	public void setObject_kind(String object_kind) {
		this.object_kind = object_kind;
	}
	public String getProposition_no() {
		return proposition_no;
	}
	public void setProposition_no(String proposition_no) {
		this.proposition_no = proposition_no;
	}
	public Date getProposition_in_date() {
		return proposition_in_date;
	}
	public void setProposition_in_date(Date date) {
		this.proposition_in_date = date;
	}
	public String getDecision_no() {
		return decision_no;
	}
	public void setDecision_no(String decision_no) {
		this.decision_no = decision_no;
	}
	public Date getDecision_date() {
		return decision_date;
	}
	public void setDecision_date(Date decision_date) {
		this.decision_date = decision_date;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getDecision_after() {
		return decision_after;
	}
	public void setDecision_after(String decision_after) {
		this.decision_after = decision_after;
	}
	public String getNotice() {
		return notice;
	}
	public void setNotice(String notice) {
		this.notice = notice;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
