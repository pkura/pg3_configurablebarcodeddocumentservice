package pl.compan.docusafe.core.record.construction;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.type.Type;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.record.Records;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.*;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
/* User: Administrator, Date: 2005-11-25 17:02:49 */

/**
 * Wniosek o pozwolenie na budow�.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ConstructionApplication.java,v 1.25 2010/04/21 11:31:16 mariuszk Exp $
 */
public class ConstructionApplication
{
    private Long id;
    private Integer hversion;
    /**
     * Numer subrejestru.
     */
    private String registryNo;
    /**
     * Numer wniosku w ramach subrejestru.
     */
    private Integer sequenceId;
    private Date ctime;
    private String author;
    private String clerk;
    /**
     * Data wp�yni�cia wniosku.
     */
    private Date submitDate;
    /**
     * Znak sprawy dla wniosku.
     */
    private String applicationNo;
    /**
     * Inwestor.
     */
    //private String developer;
    private String developerName;
    private String developerStreet;
    private String developerZip;
    private String developerLocation;
    private String summary;
//    private String decisionNo;
//    private Date decisionDate;
    private String remarks;

    private OfficeCase officeCase;

    private ConstructionDecision constructionDecision;
   // private Set constructionDecisions;


    private String decisionNo;
    private Integer decisionNoInRegistry;
    private Date decisionDate;
    private Boolean accepted;
    private Long inDocumentId;
    private Boolean withdrawn;

    private String usableArea;
    private String buildArea;
    private String volume;
    private String rooms;
    private String flats;
    private BigDecimal fee;

    static final Logger log = LoggerFactory.getLogger(ConstructionApplication.class);

    public ConstructionApplication()
    {
    }

    public void create(OfficeCase officeCase) throws EdmException
    {
        if (registryNo == null)
            throw new NullPointerException("Nie podano symbolu rejestru");

        try
        {
            List count = DSApi.context().classicSession().find(
                "select count(ca) from ca in class "+ConstructionApplication.class.getName()+
                    " where ca.registryNo = ? and ca.sequenceId = ?",
                new Object[] { registryNo, sequenceId },
                new Type[] { Hibernate.STRING, Hibernate.INTEGER });

            if (count.get(0) instanceof Long &&
                ((Long) count.get(0)).intValue() > 0)
                throw new EdmException("Istnieje ju� wniosek nr "+sequenceId+
                " w subrejestrze "+registryNo);

            this.ctime = new Date();
            this.author = DSApi.context().getPrincipalName();
            this.clerk = DSApi.context().getPrincipalName();

            this.officeCase = officeCase;
            officeCase.putInRegistry(Records.CONSTRUCTION);

            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static ConstructionApplication find(Long id) throws EdmException
    {
        return (ConstructionApplication) Finder.find(ConstructionApplication.class, id);
    }

    public static ConstructionApplication findByOfficeCase(OfficeCase officeCase)
        throws EdmException
    {
        return findByOfficeCaseId(officeCase.getId());
    }

    public void delete() throws EdmException
    {
         Persister.delete(this);
    }

    public static ConstructionApplication findByOfficeCaseId(Long id)
        throws EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(ConstructionApplication.class);
            criteria.add(org.hibernate.criterion.Expression.eq("officeCase.id", id));
            List applications = criteria.list();
            
            if (applications.size() > 1)
                throw new EdmException("Znaleziono dwa wnioski o zezwolenie na budow� zwi�zane " +
                    "ze spraw� nr "+id);
            
            if (applications.size() > 0)
                return (ConstructionApplication) applications.get(0);

            throw new EntityNotFoundException("Nie znaleziono wniosku o zezwolenie na budow� " +
                "zwi�zanego ze spraw� nr "+id);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }



    public static String suggestApplicationNo(OfficeCase officeCase)
        throws EdmException
    {
        return officeCase.getOfficeId();
/*
        return officeCase.getOfficeIdPrefix() + "." +
            registryNo + "." + sequenceId;
*/
    }

    public static Integer suggestSequenceId(String registryNo) throws EdmException
    {
        try
        {
            List max = DSApi.context().classicSession().find(
                "select max(a.sequenceId) from a in class "+ConstructionApplication.class.getName()+
                    " where upper(a.registryNo) = ?",
                registryNo != null ? registryNo.toUpperCase() : null, Hibernate.STRING);
            if (max.size() > 0 && max.get(0) != null )
            	try
            	{
            		return new Integer(max.get(0).toString())+1;
            	}
            	catch (Exception e) {
            		return new Integer(1);
            	}
            else
                return new Integer(1);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static SearchResults<ConstructionApplication> search(Query query) throws EdmException
    {
        FromClause from = new FromClause();
        final TableAlias appTable = from.createTable(DSApi.getTableName(ConstructionApplication.class));
        final WhereClause where = new WhereClause();
/*
        TableAlias decTable = null;
        if (query.isAnyDecision() || query.getDecision() != null)
        {
            decTable = from.createTable(DSApi.getTableName(ConstructionDecision.class));
            where.add(Expression.eqAttribute(
                appTable.attribute("id"), decTable.attribute("application_id")));

            if (query.getDecision() != null)
                where.add(Expression.eq(decTable.attribute("accepted"), query.getDecision()));
        }
        final TableAlias _decTable = decTable;
*/

        if (query.isAnyDecision())
        {
            where.add(Expression.ne(appTable.attribute("decisionNo"), null));
        }
        else if (query.isNoDecision())
        {
            where.add(Expression.eq(appTable.attribute("decisionNo"), null));
        }

        if (query.getDecision() != null)
            where.add(Expression.eq(appTable.attribute("accepted"), query.getDecision()));

        if (query.author != null)
            where.add(Expression.eq(appTable.attribute("author"), query.author));
        
        query.visitQuery(new FormQuery.QueryVisitorAdapter() {
            public void visitGe(FormQuery.Ge expr) throws EdmException
            {
                if (expr.getAttribute().equals("submitDate"))
                {
                    where.add(Expression.ge(appTable.attribute("submitDate"), expr.getValue()));
                }
                else if (expr.getAttribute().equals("decisionDate"))
                {
                    where.add(Expression.ge(appTable.attribute("decisionDate"), expr.getValue()));
                }
            }

            public void visitLe(FormQuery.Le expr) throws EdmException
            {
                if (expr.getAttribute().equals("submitDate"))
                {
                    where.add(Expression.le(appTable.attribute("submitDate"), expr.getValue()));
                }
                else if (expr.getAttribute().equals("decisionDate"))
                {
                    where.add(Expression.le(appTable.attribute("decisionDate"), expr.getValue()));
                }
            }

            public void visitEq(FormQuery.Eq expr) throws EdmException
            {
                if ("applicationNo".equals(expr.getAttribute()))
                {
                    where.add(Expression.eq(appTable.attribute("applicationNo"), expr.getValue()));
                }
                else if ("decisionNo".equals(expr.getAttribute()))
                {
                    where.add(Expression.eq(appTable.attribute("decisionNo"), expr.getValue()));
                }
                else if ("registryNo".equals(expr.getAttribute()))
                {
                    where.add(Expression.eq(appTable.attribute("registryNo"), expr.getValue()));
                }
            }
        });

        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(appTable, "id");

        OrderClause order = new OrderClause();
        if (query.defaultDecisionSort) {
            /* sortowanie po decyzjach wedlug "default-owych" regul */
            order.add(appTable.attribute("registryNo"), true);
            order.add(appTable.attribute("decisionNoInRegistry"), true);
            order.add(appTable.attribute("decisionNo"), true);
        }
            
        if (query.sortField != null)
            order.add(appTable.attribute(query.sortField), query.ascending);
        order.add(appTable.attribute("ctime"), Boolean.TRUE);
        selectId.add(appTable, "ctime");

        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(distinct "+appTable.getAlias()+".id)");

        int totalCount;
        SelectQuery selectQuery;

        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby certyfikat�w.");
            totalCount = rs.getInt(countCol.getPosition());
            rs.close();

            selectQuery = new SelectQuery(selectId, from, where, order);
            if (query.getLimit() > 0)
            {
                selectQuery.setMaxResults(query.getLimit());
                selectQuery.setFirstResult(query.getOffset());
            }
            //ps = selectQuery.createStatement(DSApi.context().session().connection());
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            List<ConstructionApplication> results = new ArrayList<ConstructionApplication>(query.getLimit());
            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(DSApi.context().load(ConstructionApplication.class, id));
            }
            rs.close();

            return new SearchResultsAdapter<ConstructionApplication>(results, query.getOffset(), totalCount,
                ConstructionApplication.class);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static class Query extends FormQuery
    {
        private boolean noDecision;
        private boolean anyDecision;
        private Boolean decision;
        private String author;
        private String sortField;
        private boolean ascending;
        private boolean defaultDecisionSort;

        public Query()
        {
        }

        public Query(int offset, int limit)
        {
            super(offset, limit);
        }

        public void decision(boolean accepted)
        {
            decision = Boolean.valueOf(accepted);
        }

        public void anyDecision()
        {
            anyDecision = true;
        }

        public void noDecision()
        {
            noDecision = true;
        }

        public void submitDate(Date from, Date to)
        {
            if (from != null)
                addGe("submitDate", from);
            if (to != null)
                addLe("submitDate", to);
        }

        public void decisionDate(Date from, Date to)
        {
            if (from != null)
                addGe("decisionDate", from);
            if (to != null)
                addLe("decisionDate", to);
        }

        public void applicationNo(String applicationNo)
        {
            if (applicationNo != null)
                addEq("applicationNo", applicationNo);
        }

        public void decisionNo(String decisionNo)
        {
            if (decisionNo != null)
                addEq("decisionNo", decisionNo);
        }

        public void registryNo(String registryNo)
        {
            if (registryNo != null)
                addEq("registryNo", registryNo);
        }

        public void setAuthor(String author)
        {
            this.author = author;
        }
        
        public void setSortField(String sortField)
        {
            this.sortField = sortField;
        }
        
        public void setAscending(boolean ascending)
        {
            this.ascending = ascending;
        }
        
        boolean isAnyDecision()
        {
            return anyDecision;
        }

        Boolean getDecision()
        {
            return decision;
        }

        boolean isNoDecision()
        {
            return noDecision;
        }
        
        public void setDefaultDecisionSort(boolean defaultDecisionSort)
        {
            this.defaultDecisionSort = defaultDecisionSort;
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public String getRegistryNo()
    {
        return registryNo;
    }

    public void setRegistryNo(String registryNo)
    {
        this.registryNo = registryNo != null ? registryNo.toUpperCase() : null;
    }

    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getClerk()
    {
        return clerk;
    }

    public void setClerk(String clerk)
    {
        this.clerk = clerk;
    }
    
    public Date getSubmitDate()
    {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate)
    {
        this.submitDate = submitDate;
    }

    public String getApplicationNo()
    {
        return applicationNo;
    }

    public void setApplicationNo(String applicationNo)
    {
        this.applicationNo = applicationNo;
    }

    public String getDeveloperName()
    {
        return developerName;
    }

    public void setDeveloperName(String developerName)
    {
        this.developerName = developerName;
    }

    public String getDeveloperStreet()
    {
        return developerStreet;
    }

    public void setDeveloperStreet(String developerStreet)
    {
        this.developerStreet = developerStreet;
    }

    public String getDeveloperZip()
    {
        return developerZip;
    }

    public void setDeveloperZip(String developerZip)
    {
        this.developerZip = developerZip;
    }

    public String getDeveloperLocation()
    {
        return developerLocation;
    }

    public void setDeveloperLocation(String developerLocation)
    {
        this.developerLocation = developerLocation;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }


    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public OfficeCase getOfficeCase()
    {
        return officeCase;
    }

    public void setOfficeCase(OfficeCase officeCase)
    {
        this.officeCase = officeCase;
    }

    public ConstructionDecision getConstructionDecision() {
        return constructionDecision;
    }

    public void setConstructionDecision(ConstructionDecision constructionDecision) {
        this.constructionDecision = constructionDecision;
    }

    public Boolean getAccepted()
    {
        return accepted;
    }

    public void setAccepted(Boolean accepted)
    {
        this.accepted = accepted;
    }

    public String getDecisionNo()
    {
        return decisionNo;
    }

    public void setDecisionNo(String decisionNo)
    {
        this.decisionNo = decisionNo;
    }

    public Integer getDecisionNoInRegistry()
    {
        return decisionNoInRegistry;
    }

    public void setDecisionNoInRegistry(Integer decisionNoInRegistry)
    {
        this.decisionNoInRegistry = decisionNoInRegistry;
    }
    
    public Date getDecisionDate()
    {
        return decisionDate;
    }

    public void setDecisionDate(Date decisionDate)
    {
        this.decisionDate = decisionDate;
    }

    public Long getInDocumentId()
    {
        return inDocumentId;
    }

    public void setInDocumentId(Long inDocumentId)
    {
        this.inDocumentId = inDocumentId;
    }

    public Boolean getWithdrawn()
    {
        return withdrawn;
    }

    public void setWithdrawn(Boolean withdrawn)
    {
        this.withdrawn = withdrawn;
    }

    public String getUsableArea()
    {
        return usableArea;
    }

    public void setUsableArea(String usableArea)
    {
        this.usableArea = usableArea;
    }

    public String getBuildArea()
    {
        return buildArea;
    }

    public void setBuildArea(String buildArea)
    {
        this.buildArea = buildArea;
    }

    public String getVolume()
    {
        return volume;
    }

    public void setVolume(String volume)
    {
        this.volume = volume;
    }

    public String getRooms()
    {
        return rooms;
    }

    public void setRooms(String rooms)
    {
        this.rooms = rooms;
    }

    public String getFlats()
    {
        return flats;
    }

    public void setFlats(String flats)
    {
        this.flats = flats;
    }

    public BigDecimal getFee()
    {
        return fee;
    }

    public void setFee(BigDecimal fee)
    {
        this.fee = fee;
    }
}
