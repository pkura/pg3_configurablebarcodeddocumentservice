package pl.compan.docusafe.core.record.construction;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

/**
 * User: Michal Manski
 * Date: 2006-04-25
 */
public class ConstructionDecisionCounter
{
    private Long id;
    private String registryNo;
    private Integer nextNumber;
    private Integer year;
    
    public void create() throws EdmException
    {
        if (registryNo == null)
            throw new NullPointerException("regisrtyNo");
        if (year == null)
            throw new NullPointerException("year");
        nextNumber = 1;
        
        try 
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static ConstructionDecisionCounter find(Integer year, String registryNo) throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(ConstructionDecisionCounter.class);
            crit.add(org.hibernate.criterion.Expression.eq("year", year));
            crit.add(org.hibernate.criterion.Expression.eq("registryNo", registryNo));
            List counters = crit.list();

            if (counters.size() > 0)
            {
                return (ConstructionDecisionCounter) counters.get(0);
            }

            return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
    
    public String getRegistryNo()
    {
        return registryNo;
    }

    public void setRegistryNo(String registryNo)
    {
        this.registryNo = registryNo;
    }
    
    public Integer getNextNumber()
    {
        return nextNumber;
    }

    public void setNextNumber(Integer nextNumber)
    {
        this.nextNumber = nextNumber;
    }
    
    public Integer getYear()
    {
        return year;
    }

    public void setYear(Integer year)
    {
        this.year = year;
    }
}
