package pl.compan.docusafe.core.record.contracts;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;

public class ContractHistory {
	
	private Long id;
	
	/** Id umowy */
	private Long contractId;
	
	/** Data wpisy */
	private Date modificationDate;
	
	/** Opis wprowadzonych zmian */
	private String description;
	
	private Long userId;
	
	private String userFirstnameLastname;
	
	public ContractHistory() { };	
	
	public ContractHistory(Long contractId, String description) throws UserNotFoundException, EdmException {
		this.contractId = contractId;
		this.description = description;
		this.userId = DSApi.context().getDSUser().getId();
		this.modificationDate = new Date();		
	}
	
    public static List <ContractHistory> findByContractId(Long contract_id) throws EdmException
    {
		Criteria c = DSApi.context().session().createCriteria(ContractHistory.class);
		c.add(Restrictions.eq("contractId", contract_id));
		return (List<ContractHistory>) c.list();
    }
    
    public void create() throws EdmException {
    	DSApi.context().session().save(this);
    }
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @return the contractId
	 */
	public Long getContractId() {
		return contractId;
	}
	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	public String getUserFirstnameLastname() {
		return userFirstnameLastname;
	}

	public void setUserFirstnameLastname(String userFirstnameLastname) {
		this.userFirstnameLastname = userFirstnameLastname;
	}

}
