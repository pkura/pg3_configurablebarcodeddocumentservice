package pl.compan.docusafe.core.record.contracts;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.*;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Contract.java,v 1.30 2009/10/09 12:51:34 rafalp Exp $
 */
public class Contract implements Cloneable
{
    public static final String KIND_UMOWA = "UMOWA";
    public static final String KIND_UMOWA_ZLECENIE = "UMOWA_ZLECENIE";
    public static final String KIND_ZLECENIE = "ZLECENIE";
    public static final String KIND_UMOWA_DZIELO = "UMOWA_DZIELO";
    public static final String KIND_INNA = "INNA";
    public static final String KIND_UBEZPIECZENIE = "UMOWA_UBEZPIECZENIA";
    public static final String KIND_POROZUMIENIE = "POROZUMIENIE";
    public static final String KIND_ANEKS = "ANEKS";
    public static final String KIND_UMOWA_NAJMU = "UMOWA_NAJMU";
    public static final String KIND_UMOWA_DZIERZAWY = "UMOWA_DZIERZAWY"; 


    public static final String STATUS_AKTUALNA = "AKTUALNA";
    public static final String STATUS_WYGASLA = "WYGASLA";
    public static final String STATUS_ROZWIAZANA = "ROZWIAZANA";

    private Long id;
    private Integer hversion;
    private Integer sequenceId;
    private Date contractDate;
    private String voucherNo;
    private String contractKind;
    private String contractNo;
    private String description;
    private BigDecimal gross;
    private Date startTerm;
    private Date endTerm;
    private Date periodfFrom;
    private Date periodfTo;
    private boolean deleted;
    private String remarks;
    private Vendor vendor;
    private List audit;
    private String lparam;
    private Long wparam;
    private Integer year;
    private String author;

    private String contractStatus;
    private Document document;

    public synchronized void create() throws EdmException
    {
        try
        {
            List max = DSApi.context().classicSession().find("select max(i.sequenceId) from " +
                "i in class "+Contract.class.getName()+" where i.year = ?",
                year, Hibernate.INTEGER);

            if (max != null && max.size() > 0 && (max.get(0) instanceof Long || max.get(0) instanceof Integer))
            {
				//TODO poprawi� toString
                this.sequenceId = new Integer(max.get(0).toString()) + 1;
            }
            else
            {
                this.sequenceId = new Integer(1);
            }

            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public final Contract cloneContract() throws EdmException
    {
        try {
        	Contract clone = (Contract) super.clone();
            return clone;
        } catch (CloneNotSupportedException ex) {
            throw new EdmException(ex);
        }
    }

    public List <ContractHistory> getHistory() throws EdmException {
    	return id == null ? null : ContractHistory.findByContractId(id);
    }

    public void setHistoryEntry(String description) throws EdmException {
    	if (id == null)
    		throw new EdmException("Nie mozna utworzyc wpisu dla nieistniejacej umowy.");
    	ContractHistory ch = new ContractHistory(id, description);
    	
    	ch.create();
    }

    public static Contract find(Long id) throws EdmException
    {
        return (Contract) Finder.find(Contract.class, id);
    }
    
    public static Contract findByDocumentId(Long id) throws EdmException
    {
        
    	InOfficeDocument inDoc = (InOfficeDocument) InOfficeDocument.find(id);
    	if (inDoc.getContractId()!=null)
    	return (Contract) Finder.find(Contract.class, inDoc.getContractId());
    	else 
    		return null;
        
    }

    public static SearchResults<Contract> search(Query query) throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias invTable = from.createTable(DSApi.getTableName(Contract.class));
        TableAlias vendorTable = null;
        if (query.sequenceId == null && (query.vendor != null || query.nip != null))
            vendorTable = from.createTable(DSApi.getTableName(pl.compan.docusafe.core.record.contracts.Vendor.class));

        WhereClause where = new WhereClause();

        if (query.fromUserDivisions != null && query.fromUserDivisions) 
        {
            /* tylko faktury z dzialow do ktorych nalezy uzytkownik */
            TableAlias userTable = from.createTable(DSApi.getTableName(pl.compan.docusafe.core.users.sql.UserImpl.class));
            TableAlias linkUserTable = from.createTable("ds_user_to_division");
            TableAlias authorTable = from.createTable(DSApi.getTableName(pl.compan.docusafe.core.users.sql.UserImpl.class));
            TableAlias linkAuthorTable = from.createTable("ds_user_to_division");
            
            //Junction OR = Expression.disjunction();
            //OR.add(Expression.eq(invTable.attribute("author"), null));
            
            where.add(Expression.eqAttribute(invTable.attribute("author"), authorTable.attribute("name")));
            where.add(Expression.eq(userTable.attribute("name"), DSApi.context().getDSUser().getName()));
            
            where.add(Expression.eqAttribute(linkUserTable.attribute("user_id"), userTable.attribute("id")));
            where.add(Expression.eqAttribute(linkAuthorTable.attribute("user_id"), authorTable.attribute("id")));
            
            where.add(Expression.eqAttribute(linkAuthorTable.attribute("division_id"), linkUserTable.attribute("division_id")));
        }
        
        where.add(Expression.eq(invTable.attribute("deleted"), Boolean.FALSE));

        if (query.sequenceId != null)
        {
            where.add(Expression.eq(invTable.attribute("sequenceid"), query.sequenceId));
        }
        if (query.year != null)
        {
            where.add(Expression.like(invTable.attribute("CONTRACTDATE"), query.year.toString()+"%"));
        }

        if (query.contractNo != null)
        {
            where.add(Expression.like(
                Function.upper(invTable.attribute("contractno")),
                StringUtils.substring(query.contractNo.toUpperCase(), 0, 49)+"%"));
        }

        if (query.contractDateFrom != null)
        {
            where.add(Expression.ge(invTable.attribute("contractdate"),
                DateUtils.midnight(query.contractDateFrom, 0)));
        }

        if (query.contractDateTo != null)
        {
            where.add(Expression.lt(invTable.attribute("contractdate"),
                DateUtils.midnight(query.contractDateTo, 1)));
        }

        if (query.termFrom != null)
        {
            where.add(Expression.ge(invTable.attribute("term"),
                DateUtils.midnight(query.termFrom, 0)));
        }

        if (query.termTo != null)
        {
            where.add(Expression.lt(invTable.attribute("term"),
                DateUtils.midnight(query.termTo, 1)));
        }
        
        if (query.periodfFrom != null)
        {
            where.add(Expression.ge(invTable.attribute("periodf_From"),
                DateUtils.midnight(query.periodfFrom, 0)));
        }

        if (query.periodfTo != null)
        {
            where.add(Expression.lt(invTable.attribute("periodf_To"),
                DateUtils.midnight(query.periodfTo, 1)));
        }

        if (query.grossFrom != null)
        {
            where.add(Expression.ge(invTable.attribute("gross"), query.grossFrom));
        }

        if (query.grossTo != null)
        {
            where.add(Expression.le(invTable.attribute("gross"), query.grossTo));
        }

        if (query.description != null)
        {
            where.add(Expression.like(Function.upper(invTable.attribute("description")),
                StringUtils.substring(query.description.toUpperCase(), 0, 159)+"%"));
        }

        if (query.contractKind != null)
        {
            where.add(Expression.eq(invTable.attribute("contractkind"), query.contractKind));
        }

        if (query.voucherNo != null)
        {
            where.add(Expression.like(Function.upper(invTable.attribute("voucherno")),
                StringUtils.substring(query.voucherNo.toUpperCase(), 0, 49)+"%"));
        }

        if (query.contractStatus != null) {
            where.add(Expression.eq(invTable.attribute("contractstatus"), query.contractStatus));

        }

        if (query.author != null) 
        {
            Junction OR = Expression.disjunction();
            OR.add(Expression.eq(invTable.attribute("author"), query.author));
            OR.add(Expression.eq(invTable.attribute("author"), null));
            where.add(OR);
        }

        if (query.creatingUser != null) 
        {
            where.add(Expression.eq(invTable.attribute("author"), query.creatingUser));
        }
        
        if (vendorTable != null && (query.vendor != null || query.nip != null))
        {
            where.add(Expression.eqAttribute(invTable.attribute("vendor_id"), vendorTable.attribute("id")));
       
            if (query.vendor != null)
            {
                Junction OR = Expression.disjunction();

                String str = StringUtils.substring(query.vendor,0,query.vendor.indexOf("/")-1);
                OR.add(Expression.like(Function.upper(vendorTable.attribute("name")),
                    StringUtils.substring(str.toUpperCase(), 0, 159)+"%"));
                OR.add(Expression.like(Function.upper(vendorTable.attribute("street")),
                    StringUtils.substring(str.toUpperCase(), 0, 49)+"%"));
                OR.add(Expression.like(Function.upper(vendorTable.attribute("location")),
                    StringUtils.substring(str.toUpperCase(), 0, 49)+"%"));

                where.add(OR);
            }

            if (query.nip != null)
            {
                where.add(Expression.eq(vendorTable.attribute("nip"), query.nip));
            }
        }

        

        // kolumny, po kt�rych wykonywane jest sortowanie musz� si�
        // znale�� w SELECT
        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(invTable, "id");

        OrderClause order = new OrderClause();

        for (Iterator iter=query.sortFields().iterator(); iter.hasNext(); )
        {
            AbstractQuery.SortField field = (AbstractQuery.SortField) iter.next();
            // je�eli dodana b�dzie mo�liwo�� sortowania po innych polach,
            // nale�y uaktualni� metod� Query.validSortField()
            if (field.getName().equals("contractNo"))
            {
                order.add(invTable.attribute("contractNo"), field.getDirection());
                selectId.add(invTable, "contractNo");
            }
            else if (field.getName().equals("contractDate"))
            {
                order.add(invTable.attribute("contractDate"), field.getDirection());
                selectId.add(invTable, "contractDate");
            }
            else if (field.getName().equals("sequenceId"))
            {
                order.add(invTable.attribute("sequenceId"), field.getDirection());
                selectId.add(invTable, "sequenceId");
            }
        }

        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(DISTINCT "+invTable.column("id")+")");

        SelectQuery selectQuery;

        int totalCount;
        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby faktur.");

            totalCount = rs.getInt(countCol.getPosition());

            rs.close();

            selectQuery = new SelectQuery(selectId, from, where, order);
            if (query.limit > 0) 
            {
                selectQuery.setMaxResults(query.limit);
                selectQuery.setFirstResult(query.offset);
            }
            rs = selectQuery.resultSet(DSApi.context().session().connection());

            List<Contract> results = new ArrayList<Contract>(query.limit);

            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(DSApi.context().load(Contract.class, id));
            }

            rs.close();

            return new SearchResultsAdapter<Contract>(results, query.offset, totalCount, Contract.class);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

    public static class Query extends AbstractQuery
    {
        private int offset;
        private int limit;

        private Integer sequenceId;
        private Date contractDateFrom;
        private Date contractDateTo;
        private String contractKind;
        private String contractNo;
        private String description;
        private Date termFrom;
        private Date termTo;
        private Date periodfFrom;
        private Date periodfTo;
        private BigDecimal grossFrom;
        private BigDecimal grossTo;
        private String voucherNo;
        private String vendor;
        private String nip;
        private String contractStatus;
        private String author; // mamy uprawnienie do wyszukiwania tylko swoich
        private String creatingUser; // chcemy wyszukac umowy tylko danego authora
        private Boolean fromUserDivisions;
        private Integer year;

        public Query(int offset, int limit)
        {
            this.offset = offset;
            this.limit = limit;
        }

        protected boolean validSortField(String field)
        {
            return "contractNo".equals(field) || "contractDate".equals(field) ||
                "sequenceId".equals(field);
        }

        public void setSequenceId(Integer sequenceId)
        {
            this.sequenceId = sequenceId;
        }

        public void setContractDateFrom(Date contractDateFrom)
        {
            this.contractDateFrom = contractDateFrom;
        }

        public void setContractDateTo(Date contractDateTo)
        {
            this.contractDateTo = contractDateTo;
        }

        public void setContractKind(String contractKind)
        {
            this.contractKind = contractKind;
        }

        public void setContractNo(String contractNo)
        {
            this.contractNo = contractNo;
        }

        public void setDescription(String description)
        {
            this.description = description;
        }

        public void setTermFrom(Date termFrom)
        {
            this.termFrom = termFrom;
        }

        public void setTermTo(Date termTo)
        {
            this.termTo = termTo;
        }

        public void setGrossFrom(BigDecimal grossFrom)
        {
            this.grossFrom = grossFrom;
        }

        public void setGrossTo(BigDecimal grossTo)
        {
            this.grossTo = grossTo;
        }

        public void setVoucherNo(String voucherNo)
        {
            this.voucherNo = voucherNo;
        }

        public void setVendor(String vendor)
        {
            this.vendor = vendor;
        }

        public void setNip(String nip)
        {
            this.nip = nip;
        }

        public String getContractStatus() {
            return contractStatus;
        }

        public void setContractStatus(String contractStatus) {
            this.contractStatus = contractStatus;
        }
        
        public String getAuthor() {
            return author;
        }
        
        public void setAuthor(String author) {
            this.author = author;
        }
        
        public String getCreatingUser() {
            return creatingUser;
        }
        
        public void setCreatingUser(String creatingUser) {
            this.creatingUser = creatingUser;
        }
        
        public Boolean fromUserDivisions() {
            return fromUserDivisions;
        }
        
        public void setFromUserDivisions(Boolean fromUserDivisions) {
            this.fromUserDivisions = fromUserDivisions;
        }

        public void setYear(Integer year)
        {
            this.year = year;
        }

		public Date getPeriodfFrom() {
			return periodfFrom;
		}

		public void setPeriodfFrom(Date periodfFrom) {
			this.periodfFrom = periodfFrom;
		}

		public Date getPeriodfTo() {
			return periodfTo;
		}

		public void setPeriodfTo(Date periodfTo) {
			this.periodfTo = periodfTo;
		}
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Contract)) return false;

        final Contract contract = (Contract) o;

        if (id != null ? !id.equals(contract.id) : contract.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public Date getContractDate()
    {
        return contractDate;
    }

    public void setContractDate(Date contractDate)
    {
        this.contractDate = contractDate;
    }

    public String getVoucherNo()
    {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo)
    {
        this.voucherNo = voucherNo;
    }

    public String getContractKind()
    {
        return contractKind;
    }

    public void setContractKind(String contractKind)
    {
        this.contractKind = contractKind;
    }

    public String getContractNo()
    {
        return contractNo;
    }

    public void setContractNo(String contractNo)
    {
        this.contractNo = contractNo;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public BigDecimal getGross()
    {
        return gross;
    }

    public void setGross(BigDecimal gross)
    {
        this.gross = gross;
    }

    public Date getStartTerm()
    {
        return startTerm;
    }

    public void setStartTerm(Date startTerm)
    {
        this.startTerm = startTerm;
    }

    public Date getEndTerm()
    {
        return endTerm;
    }

    public void setEndTerm(Date endTerm)
    {
        this.endTerm = endTerm;
    }

    public boolean isDeleted()
    {
        return deleted;
    }

    public void setDeleted(boolean deleted)
    {
        this.deleted = deleted;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public Vendor getVendor()
    {
        return vendor;
    }

    public void setVendor(Vendor vendor)
    {
        this.vendor = vendor;
    }

    public List getAudit()
    {
        return audit;
    }

    public void setAudit(List audit)
    {
        this.audit = audit;
    }

    public String getLparam()
    {
        return lparam;
    }

    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }

    public Long getWparam()
    {
        return wparam;
    }

    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }

    public Integer getYear()
    {
        return year;
    }

    public void setYear(Integer year)
    {
        this.year = year;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }
    
    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

	public Date getPeriodfFrom() {
		return periodfFrom;
	}

	public void setPeriodfFrom(Date periodfFrom) {
		this.periodfFrom = periodfFrom;
	}

	public Date getPeriodfTo() {
		return periodfTo;
	}

	public void setPeriodfTo(Date periodfTo) {
		this.periodfTo = periodfTo;
	}
}
