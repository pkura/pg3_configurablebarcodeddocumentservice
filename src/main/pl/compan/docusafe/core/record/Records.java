package pl.compan.docusafe.core.record;
/* User: Administrator, Date: 2005-10-05 12:34:29 */

/**
 * Lista rejestr�w, kt�rych elementy wi�zane s� ze sprawami lub
 * z pismami.  Sta�e zdefiniowane w tej klasie powinny by� u�ywane
 * jako warto�ci p�l bitowych identyfikuj�cych rejestry, w kt�rych
 * znajduje si� pismo lub sprawa.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Records.java,v 1.3 2006/02/14 16:00:04 lk Exp $
 */
public class Records
{
    /**
     * Sta�a okre�laj�ca rodzaj rejestru w polach bitowych.
     */
    public static final int GRANTS = 1;
    public static final int CONSTRUCTION = 1 << 1;
}
