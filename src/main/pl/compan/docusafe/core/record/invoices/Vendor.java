package pl.compan.docusafe.core.record.invoices;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.TextUtils;

/**
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Vendor.java,v 1.5 2006/12/07 13:53:24 mmanski Exp $
 */
public class Vendor implements Cloneable
{
    private Long id;
    private String name;
    private String street;
    private String zip;
    private String location;
    private String country;
    private String nip;
    private String regon;
    private String phone;
    private String fax;
    private String lparam;
    private Long wparam;

    public String getSummary()
    {
        return name;
    }

    public void normalizeFields()
    {
        name = TextUtils.trimmedStringOrNull(name, 160);
        street = TextUtils.trimmedStringOrNull(street, 50);
        zip = TextUtils.trimmedStringOrNull(zip, 14);
        location = TextUtils.trimmedStringOrNull(location, 50);
        country = TextUtils.trimmedStringOrNull(country, 3);
        nip = TextUtils.trimmedStringOrNull(nip, 15);
        regon = TextUtils.trimmedStringOrNull(regon, 15);
        phone = TextUtils.trimmedStringOrNull(phone, 30);
        fax = TextUtils.trimmedStringOrNull(fax, 30);
    }

    public final Vendor cloneObject() throws EdmException
    {
        try
        {
            Vendor clone = (Vendor) super.clone();
            clone.duplicate();
            return clone;
        }
        catch (CloneNotSupportedException e)
        {
            throw new Error(e);
        }
    }

    protected void duplicate() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }


    public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Vendor find(Long id) throws EdmException
    {
        Vendor person = (Vendor) DSApi.getObject(Vendor.class, id);

        if (person == null)
            throw new EntityNotFoundException(Vendor.class, id);

        return person;
    }

    public String toString()
    {
        return getClass().getName()+"[id="+id+" name="+name+"]";
    }

    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }


    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getNip()
    {
        return nip;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }


    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public String getRegon()
    {
        return regon;
    }

    public void setRegon(String regon)
    {
        this.regon = regon;
    }

    public String getLparam()
    {
        return lparam;
    }

    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }

    public Long getWparam()
    {
        return wparam;
    }

    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }

	public String getAddress() {
		return this.street + " " + this.zip + " " + this.location;
	}

}
