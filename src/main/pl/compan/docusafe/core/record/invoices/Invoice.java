package pl.compan.docusafe.core.record.invoices;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;

import pl.compan.docusafe.core.AbstractQuery;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InvoicesKind;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import std.lambda;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Invoice.java,v 1.53 2009/10/21 13:40:40 mariuszk Exp $
 */
public class Invoice implements Lifecycle
{
    public static final String TERMS_CASH = "CASH";
    public static final String TERMS_TRANSFER = "TRANSFER";
    public static final String TERMS_CC = "CC";
    public static final String TERMS_SUBSCRIPTION = "SUBSCRIPTION";
   
    private Long id;
    private Integer hversion;
    private Integer sequenceId;
    private Date ctime;
    private String author;
    private Date invoiceDate;
    private Date paymentDate;
    private Date saleDate;
    private String termsOfPayment;
    private String invoiceNo;
    private BigDecimal gross;
    private BigDecimal net;
    private Integer vat;
    private String description;
    private Vendor vendor;
    private String contractNo;
    private Long contractId;
    private Boolean cancelled;

    private String acceptingUser;
    private String decretation;
    private String cpv;
    private String budgetaryDescription;
    private String relayedTo;
    private Date budgetaryRegistryDate;
    private Date acceptDate;
    private Date acceptReturnDate;
    private String remarks;
    private Integer year;

    private boolean deleted;

    private List<Audit> audit;

    private String lparam;
    private Long wparam;


    private Document document;
    private String divisionGuid;
    private String acceptingUserdivisionGuid;
    private InvoicesKind kind;
    private Boolean archive;

    //public SearchResults search

    public synchronized void create() throws EdmException
    {
        try
        {
            List max = DSApi.context().classicSession().find("select max(i.sequenceId) from " +
                "i in class "+Invoice.class.getName() + " where i.year = ?",
                year, Hibernate.INTEGER);

            if (max != null && max.size() > 0 && max.get(0) instanceof Long || max.get(0) instanceof Integer)
            {
                this.sequenceId = new Integer(max.get(0).toString()) + 1;
            }
            else
            {
                this.sequenceId = new Integer(1);
            }

            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Invoice find(Long id) throws EdmException
    {
        return (Invoice) Finder.find(Invoice.class, id);
    }

    public static List<Invoice> find(Collection<Long> ids) throws EdmException {

    	Criteria criteria = DSApi.context().session().createCriteria(Invoice.class);
    	criteria.add(org.hibernate.criterion.Expression.in("id", ids));

    	List<Invoice> docs = criteria.list();

    	if (docs == null)
    		throw new EdmException("Invoice not found");

    	return docs;
    }
    
    public static List<Invoice> findByContractId(Long contractId)
    {
        try 
        {
            List<Invoice> inv = (List<Invoice>) DSApi.context().classicSession().find(
                    "select d from d in class "+Invoice.class.getName()+" where d.contractId="+contractId);           
            return inv;
        }
        catch (EdmException e) 
        {
           
        }
        catch (HibernateException e) 
        {
            
        }
        return new ArrayList<Invoice>();
    }
    
    public static List<Invoice> findByContractNo(String contractNo)
    {
        try 
        {
            List<Invoice> inv = (List<Invoice>) DSApi.context().classicSession().find(
                    "select d from d in class "+Invoice.class.getName()+" where d.contractId is null and d.contractNo="+contractNo);
            return inv;
        }
        catch (EdmException e) 
        {
        }
        catch (HibernateException e) 
        {
        } 
        return new ArrayList<Invoice>();
    }
    

    private static final lambda<Invoice, Invoice> identityMapper = new lambda<Invoice, Invoice>()
    {
        public Invoice act(Invoice invoice)
        {
            return invoice;
        }
    };

    public static SearchResults<Invoice> search(Query query) throws EdmException
    {
        return search(query, identityMapper, Invoice.class);
    }

    public static <T> SearchResults<T> search(Query query, lambda<Invoice, T> mapper,
                                              Class<T> mappedClass) throws EdmException
    {
        if (mapper == null)
            throw new NullPointerException("mapper");

        FromClause from = new FromClause();
        TableAlias invTable = from.createTable(DSApi.getTableName(Invoice.class));
        TableAlias vendorTable = null;
        if (query.sequenceId == null &&
            (query.nip != null || query.vendor != null || query.regon != null))
            vendorTable = from.createTable(DSApi.getTableName(Vendor.class));
        
        WhereClause where = new WhereClause();
        
        /**Uprawnienia*/
        if(true && !DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE))
        {        
        	Boolean canFindByAuthor = true;
        	Boolean canFindByAcceptingUser = true;
        	String[] guids = null;
        	Junction divisionOR = Expression.disjunction();
        	if (DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_Z_DZIALU))
            {    		
        		guids = DSApi.context().getDSUser().getDivisionsGuid();
        		if(guids.length > 0)
        		{
        			divisionOR.add(Expression.in(invTable.attribute("division_guid"), guids));
        		}
            }
        	else if(DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_SWOICH))
        	{
        		divisionOR.add(Expression.eq(invTable.attribute("author"), DSApi.context().getDSUser().getName()));
        	}
        	else
        	{
        		canFindByAuthor = false;
        	}
        	if (DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_Z_DZIALU_DO_AKCEPTACJI))
        	{
        		if(guids == null)
        			guids = DSApi.context().getDSUser().getDivisionsGuid();
        		if(guids.length > 0)
        		{
        			divisionOR.add(Expression.in(invTable.attribute("acceptingUserdivisionGuid"), guids));
        		}
            }
        	else if(DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_SWOICH_DO_AKCEPTACJI))
        	{
        		divisionOR.add(Expression.eq(invTable.attribute("acceptingUser"), DSApi.context().getDSUser().getName()));
        	}
        	else
        	{
        		canFindByAcceptingUser = false;
        	}
        	if(!canFindByAcceptingUser && !canFindByAuthor)
        		throw new EdmException("Nie masz uprawnie� do wyszukiwania faktur");
        	where.add(divisionOR);
        }
        
        where.add(Expression.eq(invTable.attribute("deleted"), Boolean.FALSE));

        if (query.sequenceId != null)
        {
            where.add(Expression.eq(invTable.attribute("sequenceid"), query.sequenceId));
            if (query.year == null){
             		where.add(Expression.eq(invTable.attribute("create_year"), new DateUtils().getCurrentYear()));
             	} else {
             		where.add(Expression.eq(invTable.attribute("create_year"), query.year));
             	}
           
        }
        else
        {
            if (query.year != null)
            {
                if (DSApi.isOracleServer()) {
            		where.add(Expression.eq_string("EXTRACT(YEAR FROM " + invTable.column("INVOICEDATE") + ")", query.year.toString()));
            	} else {
            		where.add(Expression.like(invTable.attribute("INVOICEDATE"), query.year.toString()+"%"));
            	}
            }else if (query.year == null){
            	where.add(Expression.eq(invTable.attribute("create_year"), new DateUtils().getCurrentYear()));
            }

            if (query.sequenceIdFrom != null)
            {
                where.add(Expression.ge(invTable.attribute("sequenceid"), query.sequenceIdFrom));
            }

            if (query.sequenceIdTo != null)
            {
                where.add(Expression.le(invTable.attribute("sequenceid"), query.sequenceIdTo));
            }

            if (query.invoiceNo != null)
            {
                where.add(Expression.like(
                    Function.upper(invTable.attribute("invoiceno")),
                    StringUtils.substring(query.invoiceNo.toUpperCase(), 0, 49)+"%"));
            }

            Junction contractOR = Expression.disjunction();
            
            if (query.contractNo != null)
            {
                contractOR.add(Expression.like(
                    Function.upper(invTable.attribute("contractno")),
                    StringUtils.substring(query.contractNo.toUpperCase(), 0, 50)+"%"));
            }

            if (query.contractIds != null)
            {
                for (int i=0; i < query.contractIds.size(); i++) 
                {
                    contractOR.add(Expression.eq(invTable.attribute("contractid"), query.contractIds.get(i)));
                }
                
            }
            
            if (!contractOR.isEmpty())
                where.add(contractOR);
            
            if(query.ctimeDateFrom != null) {
                where.add(Expression.ge(invTable.attribute("ctime"),DateUtils.midnight(query.ctimeDateFrom, 0)));
            }
            if(query.ctimeDateTo != null) {
                where.add(Expression.lt(invTable.attribute("ctime"),DateUtils.midnight(query.ctimeDateTo, 1)));
            }

            if (query.invoiceDateFrom != null)
            {
                where.add(Expression.ge(invTable.attribute("invoicedate"),
                    DateUtils.midnight(query.invoiceDateFrom, 0)));
            }

            if (query.invoiceDateTo != null)
            {
                where.add(Expression.lt(invTable.attribute("invoicedate"),
                    DateUtils.midnight(query.invoiceDateTo, 1)));
            }

            if (query.paymentDateFrom != null)
            {
                where.add(Expression.ge(invTable.attribute("paymentdate"),
                    DateUtils.midnight(query.paymentDateFrom, 0)));
            }

            if (query.paymentDateTo != null)
            {
                where.add(Expression.lt(invTable.attribute("paymentdate"),
                    DateUtils.midnight(query.paymentDateTo, 1)));
            }

            if (query.grossFrom != null)
            {
                where.add(Expression.ge(invTable.attribute("gross"), query.grossFrom));
            }

            if (query.grossTo != null)
            {
                where.add(Expression.le(invTable.attribute("gross"), query.grossTo));
            }

            if (query.acceptingUser != null)
            {
                where.add(Expression.eq(invTable.attribute("acceptinguser"), query.acceptingUser));
            }
            
            if (query.decretation != null)
            {
                where.add(Expression.eq(invTable.attribute("decretation"), query.decretation));
            }
            
            if (query.cpv != null)
            {
                where.add(Expression.eq(invTable.attribute("cpv"), query.cpv));
            }

            if (query.relayedTo != null)
            {
                where.add(Expression.like(Function.upper(invTable.attribute("relayedto")),
                    StringUtils.substring(query.relayedTo.toUpperCase(), 0, 79)+"%"));
            }

            if (query.budgetaryRegistryDateFrom != null)
            {
                where.add(Expression.ge(invTable.attribute("RB_DATE"),
                    DateUtils.midnight(query.budgetaryRegistryDateFrom, 0)));
            }

            if (query.budgetaryRegistryDateTo != null)
            {
                where.add(Expression.lt(invTable.attribute("RB_DATE"),
                    DateUtils.midnight(query.budgetaryRegistryDateTo, 1)));
            }

            if (query.description != null)
            {
                where.add(Expression.like(Function.upper(invTable.attribute("description")),
                    StringUtils.substring(query.description.toUpperCase(), 0, 159)+"%"));
            }

            if (query.kind != null)
            {
                where.add(Expression.eq(invTable.attribute("kind"), query.kind));
            }

            if (query.termsOfPayment != null)
            {
                where.add(Expression.eq(invTable.attribute("termsofpayment"), query.termsOfPayment));
            }

            if (query.cancelled != null )
            {
            	 Junction OR = Expression.disjunction();
                 OR.add(Expression.eq(invTable.attribute("cancelled"), query.cancelled));
                 OR.add(Expression.eq(invTable.attribute("cancelled"), null));
                 where.add(OR);
            }
            
            if(query.divisionGuid != null) 
            {
                where.add(Expression.eq(invTable.attribute("divisionGuid"), query.divisionGuid));
            }

            if (query.author != null) 
            {
                where.add(Expression.eq(invTable.attribute("author"), query.author));
            }
            
            if (query.creatingUser != null) 
            {
                where.add(Expression.eq(invTable.attribute("author"), query.creatingUser));
            }

            if (query.archive != null)
            {
            	
                 if(query.archive)
                 {
                     where.add(Expression.eq(invTable.attribute("archive"), query.archive));
                 }
                 else
                 {
                	 Junction OR = Expression.disjunction();
                     OR.add(Expression.eq(invTable.attribute("archive"), query.archive));
                	 OR.add(Expression.eq(invTable.attribute("archive"), null));
                	 where.add(OR);
                 }
                 
            }

            if (vendorTable != null &&
                (query.vendor != null || query.nip != null || query.regon != null))
            {
                where.add(Expression.eqAttribute(invTable.attribute("vendor_id"), vendorTable.attribute("id")));

                Junction OR = Expression.disjunction();
                        
                if (query.vendor != null)
                {
                    String str = StringUtils.substring(query.vendor,0,query.vendor.indexOf("/")-1);
                    OR.add(Expression.like(Function.upper(vendorTable.attribute("name")),
                        StringUtils.substring(str.toUpperCase(), 0, 159)+"%"));
                    OR.add(Expression.like(Function.upper(vendorTable.attribute("street")),
                        StringUtils.substring(str.toUpperCase(), 0, 49)+"%"));
                    OR.add(Expression.like(Function.upper(vendorTable.attribute("location")),
                        StringUtils.substring(str.toUpperCase(), 0, 49)+"%"));
                }

                if (query.nip != null)
                {
                    OR.add(Expression.eq(vendorTable.attribute("nip"), query.nip));
                }

                if (query.regon != null)
                {
                    OR.add(Expression.eq(vendorTable.attribute("regon"), query.regon));
                }

                where.add(OR);
            }
        }

        // kolumny, po kt�rych wykonywane jest sortowanie musz� si�
        // znale�� w SELECT
        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(invTable, "id");

        OrderClause order = new OrderClause();

        for (Iterator iter=query.sortFields().iterator(); iter.hasNext(); )
        {
            AbstractQuery.SortField field = (AbstractQuery.SortField) iter.next();
            // je�eli dodana b�dzie mo�liwo�� sortowania po innych polach,
            // nale�y uaktualni� metod� Query.validSortField()
            if (field.getName().equals("ctime"))
            {
                order.add(invTable.attribute("ctime"), field.getDirection());
                selectId.add(invTable, "ctime");
            }
            else if (field.getName().equals("invoiceNo"))
            {
                order.add(invTable.attribute("invoiceNo"), field.getDirection());
                selectId.add(invTable, "invoiceNo");
            }
            else if (field.getName().equals("invoiceDate"))
            {
                order.add(invTable.attribute("invoiceDate"), field.getDirection());
                selectId.add(invTable, "invoiceDate");
            }
            else if (field.getName().equals("paymentDate"))
            {
                order.add(invTable.attribute("paymentDate"), field.getDirection());
                selectId.add(invTable, "paymentDate");
            }
            else if (field.getName().equals("sequenceId"))
            {
                order.add(invTable.attribute("sequenceId"), field.getDirection());
                selectId.add(invTable, "sequenceId");
            }
            else if (field.getName().equals("cpv"))
            {
                order.add(invTable.attribute("cpv"), field.getDirection());
                selectId.add(invTable, "cpv");
            }
            else if (field.getName().equals("decretation"))
            {
                order.add(invTable.attribute("decretation"), field.getDirection());
                selectId.add(invTable, "decretation");
            }
            else if (field.getName().equals("gross"))
            {
                order.add(invTable.attribute("gross"), field.getDirection());
                selectId.add(invTable, "gross");
            }
            else if (field.getName().equals("acceptingUser"))
            {
                order.add(invTable.attribute("acceptingUser"), field.getDirection());
                selectId.add(invTable, "acceptingUser");
            }
        }

        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(DISTINCT "+invTable.column("id")+")");

        SelectQuery selectQuery;

        int totalCount;
        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby faktur.");

            totalCount = rs.getInt(countCol.getPosition());

            rs.close();

            selectQuery = new SelectQuery(selectId, from, where, order);
            if (query.limit > 0) 
            {
                selectQuery.setMaxResults(query.limit);
                selectQuery.setFirstResult(query.offset);
            }
            rs = selectQuery.resultSet(DSApi.context().session().connection());

            List<T> results = new ArrayList<T>(query.limit);
            
            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(mapper.act(DSApi.context().load(Invoice.class, id)));
            }
            
            rs.close();

            return new SearchResultsAdapter<T>(results, query.offset, totalCount, mappedClass);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Invoice)) return false;

        final Invoice invoice = (Invoice) o;

        if (id != null ? !id.equals(invoice.id) : invoice.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public static class Query extends AbstractQuery
    {
        private int offset;
        private int limit;

        private Integer sequenceId;
        private Integer sequenceIdFrom;
        private Integer sequenceIdTo;
        private String invoiceNo;
        private Date ctimeDateFrom;
        private Date ctimeDateTo;
        private Date invoiceDateFrom;
        private Date invoiceDateTo;
        private Date paymentDateFrom;
        private Date paymentDateTo;
        private String termsOfPayment;
        private Integer kind;
        private BigDecimal grossFrom;
        private BigDecimal grossTo;
        private BigDecimal gross;
        private String description;
        private String contractNo;
        private List<Long> contractIds;
        private String vendor;
        private String acceptingUser;
        private String decretation;
        private String cpv;
        private Date budgetaryRegistryDateFrom;
        private Date budgetaryRegistryDateTo;
        private String relayedTo;
        private String nip;
        private String regon;
        private Boolean cancelled;
        private String divisionGuid;
        private String author; // mamy uprawnienie do wyszukiwania tylko swoich
        private String creatingUser; // chcemy wyszukac faktury tylko danego authora
        private Boolean fromUserDivisions;
        private Boolean fromUserDivisionsToAccept;
        private Boolean fromMyAccept;
        private Integer year;
        private Boolean archive;

        public Query(int offset, int limit)
        {
            this.offset = offset;
            this.limit = limit;
        }

        protected boolean validSortField(String field)
        {
        	 return "ctime".equals(field) || "invoiceNo".equals(field) || "invoiceDate".equals(field) ||
                "paymentDate".equals(field) || "sequenceId".equals(field) ||
                "decretation".equals(field) || "cpv".equals(field) || "gross".equals(field) || "acceptingUser".equals(field) || "ctime".equals(field);
        }

        public Integer getKind()
        {
            return kind;
        }

        public void setKind(Integer kind)
        {
            this.kind = kind;
        }

        public void setSequenceId(Integer sequenceId)
        {
            this.sequenceId = sequenceId;
        }

        public void setSequenceIdFrom(Integer sequenceIdFrom)
        {
            this.sequenceIdFrom = sequenceIdFrom;
        }

        public void setSequenceIdTo(Integer sequenceIdTo)
        {
            this.sequenceIdTo = sequenceIdTo;
        }

        public void setInvoiceNo(String invoiceNo)
        {
            this.invoiceNo = invoiceNo;
        }

        public void setInvoiceDateFrom(Date invoiceDateFrom)
        {
            this.invoiceDateFrom = invoiceDateFrom;
        }

        public void setInvoiceDateTo(Date invoiceDateTo)
        {
            this.invoiceDateTo = invoiceDateTo;
        }

        public void setTermsOfPayment(String termsOfPayment)
        {
            this.termsOfPayment = termsOfPayment;
        }

        public void setGrossFrom(BigDecimal grossFrom)
        {
            this.grossFrom = grossFrom;
        }

        public void setGrossTo(BigDecimal grossTo)
        {
            this.grossTo = grossTo;
        }

        public void setDescription(String description)
        {
            this.description = description;
        }

        public void setContractNo(String contractNo)
        {
            this.contractNo = contractNo;
        }

        public void setContractId(Long contractId)
        {
            this.contractIds = new ArrayList<Long>();
            contractIds.add(contractId);
        }
        
        public void setContractIds(List<Long> contractIds)
        {
            this.contractIds = contractIds;
        }
        
        public void setVendor(String vendor)
        {
            this.vendor = vendor;
        }

        public void setAcceptingUser(String acceptingUser)
        {
            this.acceptingUser = acceptingUser;
        }
        
    	public void setDecretation(String decretation) {
    		this.decretation = decretation;
    	}
    	
    	public void setCpv(String cpv) {
    		this.cpv = cpv;
    	}

        public void setBudgetaryRegistryDateFrom(Date budgetaryRegistryDateFrom)
        {
            this.budgetaryRegistryDateFrom = budgetaryRegistryDateFrom;
        }

        public void setBudgetaryRegistryDateTo(Date budgetaryRegistryDateTo)
        {
            this.budgetaryRegistryDateTo = budgetaryRegistryDateTo;
        }

        public void setNip(String nip)
        {
            this.nip = nip;
        }

        public void setRegon(String regon)
        {
            this.regon = regon;
        }

        public void setRelayedTo(String relayedTo)
        {
            this.relayedTo = relayedTo;
        }

        public void setPaymentDateFrom(Date paymentDateFrom)
        {
            this.paymentDateFrom = paymentDateFrom;
        }

        public void setPaymentDateTo(Date paymentDateTo)
        {
            this.paymentDateTo = paymentDateTo;
        }

        public Date getCtimeDateFrom() {
            return ctimeDateFrom;
        }

        public void setCtimeDateFrom(Date ctimeDateFrom) {
            this.ctimeDateFrom = ctimeDateFrom;
        }

        public Date getCtimeDateTo() {
            return ctimeDateTo;
        }

        public void setCtimeDateTo(Date ctimeDateTo) {
            this.ctimeDateTo = ctimeDateTo;
        }
        
        public String getAuthor() {
            return author;
        }
        
        public void setAuthor(String author) {
            this.author = author;
        }
        
        public String getCreatingUser() {
            return creatingUser;
        }
        
        public void setCreatingUser(String creatingUser) {
            this.creatingUser = creatingUser;
        }
        
        public Boolean getFromUserDivisions() {
            return fromUserDivisions;
        }
        
        public void setFromUserDivisions(Boolean fromUserDivisions) {
            this.fromUserDivisions = fromUserDivisions;
        }
        
        public Boolean getCancelled() {
            return cancelled;
        }
        
        public void setCancelled(Boolean cancelled)
        {
            this.cancelled = cancelled;
        }

        public void setYear(Integer year)
        {
            this.year = year;
        }

		/**
		 * @return the archive
		 */
		public Boolean getArchive() {
			return archive;
		}

		/**
		 * @param archive the archive to set
		 */
		public void setArchive(Boolean archive) {
			this.archive = archive;
		}

		public Boolean getFromUserDivisionsToAccept() {
			return fromUserDivisionsToAccept;
		}

		public void setFromUserDivisionsToAccept(Boolean fromUserDivisionsToAccept) {
			this.fromUserDivisionsToAccept = fromUserDivisionsToAccept;
		}

		public Boolean getFromMyAccept() {
			return fromMyAccept;
		}

		public void setFromMyAccept(Boolean fromMyAccept) {
			this.fromMyAccept = fromMyAccept;
		}
    }

    /* getters & setters */

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }


    public Date getSaleDate()
    {
        return saleDate;
    }

    public void setSaleDate(Date saleDate)
    {
        this.saleDate = saleDate;
    }

    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public Date getInvoiceDate()
    {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate)
    {
        this.invoiceDate = invoiceDate;
    }

    public Date getPaymentDate()
    {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate)
    {
        this.paymentDate = paymentDate;
    }

    public String getTermsOfPayment()
    {
        return termsOfPayment;
    }

    public void setTermsOfPayment(String termsOfPayment)
    {
        this.termsOfPayment = termsOfPayment;
    }

    public String getInvoiceNo()
    {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo)
    {
        this.invoiceNo = invoiceNo;
    }

    public BigDecimal getGross()
    {
        return gross;
    }

    public void setGross(BigDecimal gross)
    {
        this.gross = gross;
    }

    public BigDecimal getNet()
    {
        return net;
    }

    public void setNet(BigDecimal net)
    {
        this.net = net;
    }

    public Integer getVat()
    {
        return vat;
    }

    public void setVat(Integer vat)
    {
        this.vat = vat;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getContractNo()
    {
        return contractNo;
    }

    public void setContractNo(String contractNo)
    {
        this.contractNo = contractNo;
    }

    public Long getContractId()
    {
        return contractId;
    }

    public void setContractId(Long contractId)
    {
        this.contractId = contractId;
    }

    public String getAcceptingUser()
    {
        return acceptingUser;
    }

    public void setAcceptingUser(DSUser acceptingUser) throws EdmException
    {
    	if(acceptingUser != null)
    	{
	        this.acceptingUser = acceptingUser.getName();
	        DSDivision [] divs = acceptingUser.getDivisionsWithoutGroup();
	        if(divs != null && divs.length > 0)
	        this.acceptingUserdivisionGuid = divs[0].getGuid();
    	}
    	else
    	{
    		this.acceptingUserdivisionGuid = null;
    		this.acceptingUser = null;
    	}
    }
    
    public String getDecretation() 
    {
		return decretation;
	}
    
    public void setDecretation(String decretation) 
    {
		this.decretation = decretation;
	}
    
    public String getCpv() 
    {
		return cpv;
	}
    
    public void setCpv(String cpv) 
    {
		this.cpv = cpv;
	}

    public String getBudgetaryDescription()
    {
        return budgetaryDescription;
    }

    public void setBudgetaryDescription(String budgetaryDescription)
    {
        this.budgetaryDescription = budgetaryDescription;
    }

    public String getRelayedTo()
    {
        return relayedTo;
    }

    public void setRelayedTo(String relayedTo)
    {
        this.relayedTo = relayedTo;
    }

    public Date getBudgetaryRegistryDate()
    {
        return budgetaryRegistryDate;
    }

    public void setBudgetaryRegistryDate(Date budgetaryRegistryDate)
    {
        this.budgetaryRegistryDate = budgetaryRegistryDate;
    }

    public Vendor getVendor()
    {
        return vendor;
    }

    public void setVendor(Vendor vendor)
    {
        this.vendor = vendor;
    }

    public List<Audit> getAudit()
    {
        return audit;
    }

    private void setAudit(List<Audit> audit)
    {
        this.audit = audit;
    }

    public String getLparam()
    {
        return lparam;
    }

    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }

    public Long getWparam()
    {
        return wparam;
    }

    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }

    public boolean isDeleted()
    {
        return deleted;
    }

    public void setDeleted(boolean deleted)
    {
        this.deleted = deleted;
    }

    public Date getAcceptDate()
    {
        return acceptDate;
    }

    public void setAcceptDate(Date acceptDate)
    {
        this.acceptDate = acceptDate;
    }

    public Date getAcceptReturnDate()
    {
        return acceptReturnDate;
    }

    public void setAcceptReturnDate(Date acceptReturnDate)
    {
        this.acceptReturnDate = acceptReturnDate;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public Integer getYear()
    {
        return year;
    }

    public void setYear(Integer year)
    {
        this.year = year;
    }

    public Document getDocument()
    {
        return document;
    }

    public void setDocument(Document document)
    {
        this.document = document;
    }

    /* Lifecycle */

    public boolean onSave(Session s) throws CallbackException
    {
        if (ctime == null) ctime = new Date();
        if (author == null) author = DSApi.context().getPrincipalName();
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }

    public String getDivisionGuid() {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid) {
        this.divisionGuid = divisionGuid;
    }
    
    public Boolean getCancelled() {
        return cancelled;
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    public InvoicesKind getKind()
    {
        return kind;
    }

    public void setKind(InvoicesKind kind)
    {
        this.kind = kind;
    }

	/**
	 * @return the archive
	 */
	public Boolean getArchive() {
		return archive;
	}

	/**
	 * @param archive the archive to set
	 */
	public void setArchive(Boolean archive) {
		this.archive = archive;
	}
    
}
