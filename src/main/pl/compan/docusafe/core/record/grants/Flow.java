package pl.compan.docusafe.core.record.grants;

import pl.compan.docusafe.core.EdmException;

import java.util.HashMap;
import java.util.Map;
/* User: Administrator, Date: 2005-10-06 11:26:42 */

/**
 * TODO: to powinna by� klasa bazowa
 * GrantRequest i Beneficiary powinny mie� w�asne implementacje Flow.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Flow.java,v 1.8 2006/02/20 15:42:21 lk Exp $
 */
public abstract class Flow
{
    private final Map stepsByName = new HashMap();

    protected void register(Step step)
    {
        stepsByName.put(step.getName(), step);
    }

    protected Step getStep(String name)
    {
        return (Step) stepsByName.get(name);
    }

    /**
     * Rozpoczyna proces nadaj�c swojemu obiektowi bazodanowemu
     *
     * @throws EdmException
     */
    public abstract void start() throws EdmException;

    /**
     * Rejestruje wynik bie��cego kroku procesu i ewentualnie popycha
     * proces do przodu.
     * @param result Wynik bie��cego kroku procesu.
     */
    public abstract Step push(String result) throws EdmException;

    public abstract boolean isTerminated();

    public abstract Step getStep();

    public abstract String suggestResult();
}
