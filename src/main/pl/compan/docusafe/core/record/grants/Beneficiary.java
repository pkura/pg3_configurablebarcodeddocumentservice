package pl.compan.docusafe.core.record.grants;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;

import java.util.Date;
/* User: Administrator, Date: 2005-10-05 12:41:13 */

/**
 * Beneficjent stypendium.  Z obiektem GrantRequest mo�e by�
 * zwi�zanych kilka obiekt�w Beneficiary, dla ka�dego z nich
 * status przyznania stypendium mo�e by� inny.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Beneficiary.java,v 1.5 2006/09/12 13:02:09 lk Exp $
 */
public class Beneficiary
{
    private String firstname;
    private String lastname;
    private Date birthDate;
    private String fathersName;
    private String mothersName;
    private String schoolName;
    private String schoolStreet;
    private String schoolZip;
    private String schoolLocation;
    private String bankAccount;
    private String helpRequested;
    private String helpGranted;
    private String justification;
    private String step;
    private String lastStep;
    private String lastResult;
    private Boolean rejected;

    public void create() throws EdmException
    {
        new BeneficiaryFlow(this).start();
        Persister.create(this);
    }

    public String getSchoolAddress()
    {
    	String address = "";
        if (schoolName != null)
            address += schoolName;
        if (schoolStreet != null)
            address += " " + schoolStreet;
    	if (schoolZip != null)
    		address += " " + schoolZip;
    	if (schoolLocation != null)
    		address += " " + schoolLocation;
    	return address;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public Date getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getFathersName()
    {
        return fathersName;
    }

    public void setFathersName(String fathersName)
    {
        this.fathersName = fathersName;
    }

    public String getMothersName()
    {
        return mothersName;
    }

    public void setMothersName(String mothersName)
    {
        this.mothersName = mothersName;
    }

    public String getSchoolName()
    {
        return schoolName;
    }

    public void setSchoolName(String schoolName)
    {
        this.schoolName = schoolName;
    }

    public String getSchoolStreet()
    {
        return schoolStreet;
    }

    public void setSchoolStreet(String schoolStreet)
    {
        this.schoolStreet = schoolStreet;
    }

    public String getSchoolZip()
    {
        return schoolZip;
    }

    public void setSchoolZip(String schoolZip)
    {
        this.schoolZip = schoolZip;
    }

    public String getSchoolLocation()
    {
        return schoolLocation;
    }

    public void setSchoolLocation(String schoolLocation)
    {
        this.schoolLocation = schoolLocation;
    }

    public String getBankAccount()
    {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount)
    {
        this.bankAccount = bankAccount;
    }

    public String getHelpRequested()
    {
        return helpRequested;
    }

    public void setHelpRequested(String helpRequested) throws EdmException
    {
        if (helpRequested != null &&
            !GrantRequest.STYPENDIUM.equals(helpRequested) &&
            !GrantRequest.ZASILEK.equals(helpRequested))
            throw new EdmException("Nieprawid�owy typ ��danej pomocy: "+helpRequested);

        this.helpRequested = helpRequested;
    }

    public String getJustification()
    {
        return justification;
    }

    public void setJustification(String justification)
    {
        this.justification = justification;
    }

    public String getStep()
    {
        return step;
    }

    public void setStep(String step)
    {
        this.step = step;
    }

    public String getLastStep()
    {
        return lastStep;
    }

    public void setLastStep(String lastStep)
    {
        this.lastStep = lastStep;
    }

    public String getLastResult()
    {
        return lastResult;
    }

    public void setLastResult(String lastResult)
    {
        this.lastResult = lastResult;
    }

    public String getHelpGranted()
    {
        return helpGranted;
    }

    public void setHelpGranted(String helpGranted) throws EdmException
    {
        if (helpGranted != null &&
            !GrantRequest.STYPENDIUM_P.equals(helpGranted) &&
            !GrantRequest.STYPENDIUM_R.equals(helpGranted) &&
            !GrantRequest.ZASILEK_P.equals(helpGranted) &&
            !GrantRequest.ZASILEK_R.equals(helpGranted))
            throw new EdmException("Nieprawid�owy rodzaj przyznanej pomocy: "+helpGranted);

        this.helpGranted = helpGranted;
    }

    public Boolean getRejected()
    {
        return rejected;
    }

    public void setRejected(Boolean rejected)
    {
        this.rejected = rejected;
    }
}
