package pl.compan.docusafe.core.record.grants;

import pl.compan.docusafe.core.EdmException;

import java.util.Iterator;
import java.util.List;
/* User: Administrator, Date: 2005-10-20 13:31:01 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: GrantRequestFlow.java,v 1.5 2006/02/20 15:42:21 lk Exp $
 */
public class GrantRequestFlow extends Flow
{
    /**
     * Pierwszy krok - wniosek musi by� kompletny.
     */
    public static final Step WYPELNIANIE =
        new Step("WYPELNIANIE", "Wype�nianie danych wnioskodawcy i benicjent�w",
            null);
    public static final Step ORGAN =
        new Step("ORGAN",
            "Czy organ, do kt�rego wniesiono wniosek jest w�a�ciwy w sprawie?",
            new String[] { "true", "Tak", "false", "Nie" });
    public static final Step ORGAN_NIEWLASCIWY =
        new Step("ORGAN_NIEWLASCIWY",
            "Organ nie jest w�a�ciwy w sprawie, wniosek nale�y odes�a�.  Proces zosta� zako�czony.",
            true);
    public static final Step KOMPLETNY =
        new Step("KOMPLETNY",
            "Czy wniosek jest kompletny?",
            new String[] { "true", "Tak", "false", "Nie" });
    public static final Step NIEKOMPLETNY =
        new Step("NIEKOMPLETNY",
            "Wniosek jest niekompletny, nale�y wezwa� do usuni�cia brak�w.  Proces zako�czony.",
            true);
    public static final Step PLACOWKA =
        new Step("PLACOWKA",
            "Czy plac�wka jest w�a�ciwa?",
            new String[] { "true", "Tak", "false", "Nie" });
    public static final Step PLACOWKA_NIEWLASCIWA =
        new Step("PLACOWKA_NIEWLASCIWA", "Plac�wka jest niew�a�ciwa, sugerowana decyzja odmowna", true);
//    public static final Step WIEK =
//        new Step("WIEK",
//            "Czy uprawniony spe�nia kryterium wiekowe (7-24 lat)?",
//            new String[] { "true", "Tak", "false", "Nie" });
//    public static final Step WIEK_NIEWLASCIWY =
//        new Step("WIEK_NIEWLASCIWY", "Kryterium wiekowe (7-24 lat) nie jest spe�nione, sugerowana decyzja odmowna", true);
    public static final Step DOCHODY =
        new Step("DOCHODY",
            "Czy uprawniony spe�nia kryterium dochodowe?",
            new String[] { "true", "Tak", "false", "Nie" });
    public static final Step NIEWLASCIWE_DOCHODY =
        new Step("NIEWLASCIWE_DOCHODY", "Niespe�nione kryterium dochodowe, sugerowana decyzja odmowna", true);
//    public static final Step ODMOWA =
//        new Step("ODMOWA", "Decyzja odmowna.  Proces zako�czony", true);
    public static final Step CZEKAJ_BENEFICJENCI =
        new Step("CZEKAJ_BENEFICJENCI", "Kroki procesu dotycz�ce " +
            "beneficjent�w", false);
//    public static final Step FORMA_POMOCY =
//        new Step("FORMA_POMOCY", "Wyb�r formy pomocy", false);
    public static final Step KONIEC =
        new Step("KONIEC", "Zako�czono", true);

    private GrantRequest request;

    public GrantRequestFlow(GrantRequest request)
    {
        if (request == null)
            throw new NullPointerException("request");
        this.request = request;

        register(WYPELNIANIE);
        register(ORGAN);
        register(ORGAN_NIEWLASCIWY);
        register(KOMPLETNY);
        register(NIEKOMPLETNY);
        register(PLACOWKA);
        register(PLACOWKA_NIEWLASCIWA);
        //register(ODMOWA);
//        register(WIEK);
//        register(WIEK_NIEWLASCIWY);
        register(DOCHODY);
        register(NIEWLASCIWE_DOCHODY);
        register(CZEKAJ_BENEFICJENCI);
//        register(FORMA_POMOCY);
        register(KONIEC);
    }

    public void start() throws EdmException
    {
        if (request.getStep() != null)
            throw new EdmException("Proces zosta� ju� uruchomiony");

        request.setStep(WYPELNIANIE.getName());
    }

    public Step push(String result) throws EdmException
    {
        if (request.getStep() == null)
            throw new EdmException("Proces nie zosta� uruchomiony");

        Step currentStep = getStep(request.getStep());

        if (currentStep.isTerminal())
            throw new EdmException("Proces zosta� zako�czony");

        if (currentStep.getResults().size() > 0 && result == null)
            throw new EdmException("Nie podano wyniku");

        if (currentStep.getResults().size() > 0 && result != null &&
            !currentStep.getResults().containsKey(result))
            throw new EdmException("Podany wynik jest nieprawid�owy (nie jest to" +
                " jeden z mo�liwych wynik�w");

        Step lastStep = currentStep;
        String lastResult = result;

        Step step;

        if (WYPELNIANIE.equals(currentStep))
        {
            if (request.getDocumentDate() == null ||
                request.getLastname() == null ||
                request.getStreet() == null ||
                request.getLocation() ==null ||
                request.getIncome() == null)
                throw new EdmException("Nie wype�niono wszystkich wymaganych p�l wniosku");

//            if (request.getBeneficiaries().size() == 0)
//                throw new EdmException("Nie podano danych beneficjent�w");

            step = ORGAN;
        }
        else if (ORGAN.equals(currentStep))
        {
            if ("false".equals(result))
            {
                step = ORGAN_NIEWLASCIWY;
            }
            else
            {
                step = KOMPLETNY;
            }
        }
        else if (KOMPLETNY.equals(currentStep))
        {
            if ("false".equals(result))
            {
                step = NIEKOMPLETNY;
            }
            else
            {
                step = PLACOWKA;
            }
        }
        else if (PLACOWKA.equals(currentStep))
        {
            if ("false".equals(result))
            {
                step = PLACOWKA_NIEWLASCIWA;
            }
            else
            {
                step = CZEKAJ_BENEFICJENCI;
            }
        }
        else if (CZEKAJ_BENEFICJENCI.equals(currentStep))
        {
            // proces zostanie popchni�ty dopiero wtedy, gdy zako�cz�
            // si� procesy zwi�zane ze wszystkimi beneficjentami

            List beneficiaries = request.getBeneficiaries();

            if (beneficiaries == null || beneficiaries.size() == 0)
                throw new EdmException("Nie wpisano beneficjent�w wniosku");

            // je�eli kt�rykolwiek z proces�w beneficjent�w nie
            // zosta� zako�czony, powr�t bez zmian w GrantRequest
            for (Iterator iter=beneficiaries.iterator(); iter.hasNext(); )
            {
                Beneficiary beneficiary = (Beneficiary) iter.next();
                Flow bflow = new BeneficiaryFlow(beneficiary);
                if (!bflow.isTerminated())
                    throw new EdmException("Wykonaj kroki procesu dotycz�ce " +
                        "beneficjent�w");
            }

            step = KONIEC;
        }
        else
        {
            throw new EdmException("Nieprawid�owy krok procesu: "+currentStep.getName());
        }

        request.setStep(step.getName());
        request.setLastStep(lastStep.getName());
        request.setLastResult(lastResult);

        return step;
    }

    public boolean isTerminated()
    {
        return request.getStep() != null && getStep(request.getStep()).isTerminal();
    }

    public Step getStep()
    {
        return getStep(request.getStep());
    }

    public String suggestResult()
    {
        return null;
    }
}
