package pl.compan.docusafe.core.record.grants;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
/* User: Administrator, Date: 2005-10-12 15:05:10 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Step.java,v 1.4 2006/02/20 15:42:22 lk Exp $
 */
public class Step
{
    private String name;
    private String description;
    private boolean terminal;
    private Map results;

    public Step(String name, String description, boolean terminal)
    {
        this.name = name;
        this.description = description;
        this.terminal = terminal;
        this.results = Collections.EMPTY_MAP;
    }

    public Step(String name, String description, Object[] results)
    {
        this.name = name;
        this.description = description;
        if (results != null && results.length % 2 != 0)
            throw new IllegalArgumentException("D�ugo�� tablicy results " +
                "musi by� parzysta");

        if (results != null)
        {
            Map tmp = new LinkedHashMap();

            for (int i=0; i < results.length; i+=2)
            {
                if (results[i] == null || results[i+1] == null ||
                    !(results[i] instanceof String) ||
                    !(results[i+1] instanceof String))
                    throw new IllegalArgumentException("Argumenty musz� " +
                        "by� instancjami java.lang.String");

                tmp.put(results[i], results[i+1]);
            }

            this.results = Collections.unmodifiableMap(tmp);
        }
        else
        {
            this.results = Collections.EMPTY_MAP;
        }
    }

    /**
     * Nazwa kodowa kroku.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Opis kroku.
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Niemodyfikowalna tablica mo�liwych wynik�w kroku.  Kluczami
     * tablicy s� warto�ci krok�w, warto�ciami tablicy opisy krok�w.
     */
    public Map getResults()
    {
        return results;
    }

    public boolean isTerminal()
    {
        return terminal;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Step step = (Step) o;
        if (name != null ? !name.equals(step.name) : step.name != null) return false;
        return true;
    }

    public int hashCode()
    {
        return (name != null ? name.hashCode() : 0);
    }
}
