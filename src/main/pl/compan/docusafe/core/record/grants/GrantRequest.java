package pl.compan.docusafe.core.record.grants;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.text.StyledDocument;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import pl.compan.docusafe.common.rtf.RTF;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.FormQuery;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.record.Records;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.web.record.grants.GrantsSearchAction;
/* User: Administrator, Date: 2005-10-05 12:39:54 */

/**
 create table dsr_gra_request (
    id                  NUMERIC(18,0)    NOT NULL,
    hversion            integer not null,
    documentdate        date,
    sequenceid          integer not null,
    firstname           varchar(50) collate pxw_plk,
    lastname            varchar(50) collate pxw_plk,
    street              varchar(50) collate pxw_plk,
    zip                 varchar(14),
    location            varchar(50) collate pxw_plk,
    income              numeric(6,2),
    step                varchar(30),
    laststep            varchar(30),
    lastresult          varchar(30),
    case_id             numeric(18,0) NOT NULL
 );
 create generator dsr_gra_request_id;

 create table dsr_gra_beneficiary (
 request_id          NUMERIC(18,0) NOT NULL,
 posn                INTEGER NOT NULL,
 firstname           varchar(50) collate pxw_plk,
 lastname            varchar(50) collate pxw_plk,
 birthdate           date not null,
 fathersname         varchar(50) collate pxw_plk,
 mothersname         varchar(50) collate pxw_plk,
 schoolname          varchar(50) NOT NULL collate pxw_plk,
 schoolstreet        varchar(50) collate pxw_plk,
 schoolzip           varchar(14),
 schoollocation      varchar(50) collate pxw_plk,
 bankaccount         varchar(40),
 helprequested       varchar(30) NOT NULL,
 helpgranted         varchar(30),
 rejected            smallint,
 justification       varchar(200) collate pxw_plk,
 step                varchar(30),
 laststep            varchar(30),
 lastresult          varchar(30)
 );

 * Pro�ba o przyznanie stypendium.
 *
 * TODO: czy da si� usun�� zale�no�� od sprawy?
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: GrantRequest.java,v 1.16 2008/11/24 21:47:10 wkuty Exp $
 */
public class GrantRequest
{
	public static final String RES_STYPENDIUM_PIENIEZNE_TEMPLATE = "record.grants.GrantRequest.stypendium_pienezne";
	public static final String RES_STYPENDIUM_RZECZOWE_TEMPLATE = "record.grants.GrantRequest.stypendium_rzeczowe";
	public static final String RES_ZASILEK_PIENIEZNY_TEMPLATE = "record.grants.GrantRequest.zasilek_pieniezny";
	public static final String RES_ZASILEK_RZECZOWY_TEMPLATE = "record.grants.GrantRequest.zasilek_rzeczowy";
	public static final String RES_NIEKOMPLETNY_TEMPLATE = "record.grants.GrantRequest.niekompletny";
    public static final String RES_ORGAN_NIEWLASCIWY_TEMPLATE = "record.grants.GrantRequest.organ_niewlasciwy";
    public static final String RES_PLACOWKA_NIEWLASCIWA_TEMPLATE = "record.grants.GrantRequest.placowka_niewlasciwa";
    public static final String RES_NIEWLASCIWE_DOCHODY_TEMPLATE = "record.grants.GrantRequest.niewlasciwe_dochody";
    public static final String RES_ZA_WYSOKI_WIEK_TEMPLATE = "record.grants.GrantRequest.nieodpowiedni_wiek";
    public static final String RES_ZA_NISKI_WIEK_TEMPLATE = "record.grants.GrantRequest.za_niski_wiek";

    public static final Integer MAKSYMALNY_DOCHOD = 300;

    /**
     * Wnioskowany rodzaj pomocy - stypendium.
     */
    public static final String STYPENDIUM = "stypendium";
    /**
     * Wnioskowany rodzaj pomocy - zasi�ek.
     */
    public static final String ZASILEK = "zasilek";
    /**
     * Przyznana pomoc. Stypendium - pomoc pieni�na.
     */
    public static final String STYPENDIUM_P = "stypendium_p";
    /**
     * Przyznana pomoc. Zasi�ek - pomoc pieni�na.
     */
    public static final String ZASILEK_P = "zasilek_p";
    /**
     * Przyznana pomoc. Stypendium - pomoc rzeczowa.
     */
    public static final String STYPENDIUM_R = "stypendium_r";
    /**
     * Przyznana pomoc. Zasi�ek - pomoc rzeczowa.
     */
    public static final String ZASILEK_R = "zasilek_r";

    private Long id;
    private Integer sequenceId;
    private Integer hversion;
    private OfficeCase officeCase;
    private String officeId;       /* znak sprawy kt�rej dotyczy ten wniosek */
    private Date documentDate;
    private Long inDocumentId;
    private Integer printed;       /* printed>0 <=> byl juz uzyty w jakiejs liscie wyplat do kasy lub banku */

    // dane wnioskodawcy
    private String firstname;
    private String lastname;
    private String street;
    private String zip;
    private String location;
    private BigDecimal income;

    private List beneficiaries;

    private String step;
    private String lastStep;
    private String lastResult;

    public void create(OfficeCase officeCase, InOfficeDocument document) throws EdmException
    {
        try
        {
            List max = DSApi.context().classicSession().find(
                "select max(r.sequenceId) from " +
                "r in class "+GrantRequest.class.getName());
            if (max.size() > 0 && max.get(0) instanceof Long)
            {
                this.sequenceId = new Integer(((Long) max.get(0)).intValue()+1);
            }
            else
            {
                this.sequenceId = new Integer(1);
            }
            this.officeCase = officeCase;
            this.officeId = officeCase.getOfficeId();
            officeCase.putInRegistry(Records.GRANTS);

            new GrantRequestFlow(this).start();

            //List documents = officeCase.getDocuments(InOfficeDocument.class);
            //if (documents != null && documents.size() == 1)     
            //{
            //    InOfficeDocument document = (InOfficeDocument) documents.get(0);
            this.documentDate = document.getDocumentDate();
            this.inDocumentId = document.getId();
            if (document.getSender() != null && !document.getSender().isAnonymous())
            {
                this.firstname = document.getSender().getFirstname();
                this.lastname = document.getSender().getLastname();
                this.street = document.getSender().getStreet();
                this.zip = document.getSender().getZip();
                this.location = document.getSender().getLocation();
            }
            //}

            Persister.create(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static GrantRequest findByOfficeCase(OfficeCase officeCase)
        throws EdmException
    {
        return findByOfficeCaseId(officeCase.getId());
    }

    public static GrantRequest findByOfficeCaseId(Long id)
        throws EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(GrantRequest.class);
            criteria.add(org.hibernate.criterion.Expression.eq("officeCase.id", id));
            List grants = criteria.list();
            if (grants.size() > 1)
                throw new EdmException("Znaleziono dwa wnioski o stypendium zwi�zane " +
                    "ze spraw� nr "+id);
            if (grants.size() > 0)
                return (GrantRequest) grants.get(0);

            throw new EntityNotFoundException("Nie znaleziono wniosku stypendialnego " +
                "zwi�zanego ze spraw� nr "+id);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static GrantRequest find(Long id) throws EdmException
    {
        return (GrantRequest) Finder.find(GrantRequest.class, id);
    }

    
    public static SearchResults<GrantRequest> search(Query query) throws EdmException
    {
        FromClause from = new FromClause();
        final TableAlias grantTable = from.createTable(DSApi.getTableName(GrantRequest.class));
        final TableAlias beneficiaryTable = from.createTable("dsr_gra_beneficiary");
        final WhereClause where = new WhereClause();

        where.add(Expression.eqAttribute(grantTable.attribute("id"), beneficiaryTable.attribute("request_id")));

        if (query.sequenceId != null)
            where.add(Expression.eq(grantTable.attribute("sequenceid"), query.sequenceId));

        if (query.sequenceIdFrom != null)
        {
            where.add(Expression.ge(grantTable.attribute("sequenceid"), query.sequenceIdFrom));
        }

        if (query.sequenceIdTo != null)
        {
            where.add(Expression.le(grantTable.attribute("sequenceid"), query.sequenceIdTo));
        }

        if (query.firstname != null)
        	where.add(Expression.like(Function.upper(grantTable.attribute("firstname")),
                    StringUtils.substring(query.firstname.toUpperCase(), 0, 159)+"%"));
        
        if (query.lastname != null)
        	where.add(Expression.like(Function.upper(grantTable.attribute("lastname")),
                    StringUtils.substring(query.lastname.toUpperCase(), 0, 159)+"%"));

     /*   if (query.printed != null)
            if (query.printed > 0)
                where.add(Expression.eq(grantTable.attribute("printed"), query.printed));
            else
            {
                Junction OR = Expression.disjunction();
                OR.add(Expression.eq(grantTable.attribute("printed"), null));
                OR.add(Expression.eq(grantTable.attribute("printed"), Integer.valueOf(0)));
                where.add(OR);
            } */

        if (query.withBank != null)
            if (query.withBank)
                where.add(Expression.ne(beneficiaryTable.attribute("bankAccount"), null));
            else
                where.add(Expression.eq(beneficiaryTable.attribute("bankAccount"), null));

        if (query.schoolName != null)
        	where.add(Expression.like(Function.upper(beneficiaryTable.attribute("schoolName")),
                    StringUtils.substring(query.schoolName.toUpperCase(), 0, 159)+"%"));

        if (query.schoolZip != null)
        	where.add(Expression.like(Function.upper(beneficiaryTable.attribute("schoolZip")),
                    StringUtils.substring(query.schoolZip.toUpperCase(), 0, 159)+"%"));

        if (query.schoolLocation != null)
        	where.add(Expression.like(Function.upper(beneficiaryTable.attribute("schoolLocation")),
                    StringUtils.substring(query.schoolLocation.toUpperCase(), 0, 159)+"%"));

        if (query.schoolStreet != null)
        	where.add(Expression.like(Function.upper(beneficiaryTable.attribute("schoolStreet")),
                    StringUtils.substring(query.schoolStreet.toUpperCase(), 0, 159)+"%"));

        if (query.justification != null)
        {
            where.add(Expression.like(Function.upper(beneficiaryTable.attribute("justification")),
                    StringUtils.substring(query.justification.toUpperCase(), 0, 159)+"%"));
        }

        if (query.helpGranted != null)
        {
            Junction OR = Expression.disjunction();
            if (GrantsSearchAction.POMOC_PIENIEZNA.equals(query.helpGranted))
            {
                OR.add(Expression.eq(beneficiaryTable.attribute("helpGranted"),
                    GrantRequest.STYPENDIUM_P));
                OR.add(Expression.eq(beneficiaryTable.attribute("helpGranted"),
                    GrantRequest.ZASILEK_P));
            }
            else if (GrantsSearchAction.POMOC_RZECZOWA.equals(query.helpGranted))
            {
                OR.add(Expression.eq(beneficiaryTable.attribute("helpGranted"),
                    GrantRequest.STYPENDIUM_R));
                OR.add(Expression.eq(beneficiaryTable.attribute("helpGranted"),
                    GrantRequest.ZASILEK_R));
            }

            where.add(OR);
        }

        if (query.decision != null)
        {
            if (BeneficiaryFlow.KONIEC.getName().equals(query.decision))
            {
                where.add(Expression.eq(grantTable.attribute("step"),
                    GrantRequestFlow.KONIEC.getName()));
                where.add(Expression.eq(beneficiaryTable.attribute("step"),
                    BeneficiaryFlow.KONIEC.getName()));
            }
            else if (BeneficiaryFlow.ODMOWA.getName().equals(query.decision))
            {
                Junction OR = Expression.disjunction();
                OR.add(Expression.eq(grantTable.attribute("step"),
                    GrantRequestFlow.NIEKOMPLETNY.getName()));
                OR.add(Expression.eq(grantTable.attribute("step"),
                    GrantRequestFlow.ORGAN_NIEWLASCIWY.getName()));
                OR.add(Expression.eq(grantTable.attribute("step"),
                    GrantRequestFlow.PLACOWKA_NIEWLASCIWA.getName()));

                Junction AND = Expression.conjunction();
                AND.add(Expression.eq(grantTable.attribute("step"),
                    GrantRequestFlow.KONIEC.getName()));
                AND.add(Expression.eq(beneficiaryTable.attribute("step"),
                    BeneficiaryFlow.ODMOWA.getName()));
                OR.add(AND);

                where.add(OR);
            }
            else if (GrantsSearchAction.BRAK_DECYZJI.equals(query.decision))
            {
                Junction OR = Expression.disjunction();
                OR.add(Expression.eq(grantTable.attribute("step"),
                    GrantRequestFlow.WYPELNIANIE.getName()));
                OR.add(Expression.eq(grantTable.attribute("step"),
                    GrantRequestFlow.KOMPLETNY.getName()));
                OR.add(Expression.eq(grantTable.attribute("step"),
                    GrantRequestFlow.ORGAN.getName()));
                OR.add(Expression.eq(grantTable.attribute("step"),
                    GrantRequestFlow.PLACOWKA.getName()));
                OR.add(Expression.eq(grantTable.attribute("step"),
                    GrantRequestFlow.CZEKAJ_BENEFICJENCI.getName()));

                where.add(OR);
            }
        }

        if (query.finished)
        {
            Junction OR = Expression.disjunction();
            OR.add(Expression.eq(grantTable.attribute("step"),
                GrantRequestFlow.NIEKOMPLETNY.getName()));
            OR.add(Expression.eq(grantTable.attribute("step"),
                GrantRequestFlow.ORGAN_NIEWLASCIWY.getName()));
            OR.add(Expression.eq(grantTable.attribute("step"),
                GrantRequestFlow.PLACOWKA_NIEWLASCIWA.getName()));
            OR.add(Expression.eq(grantTable.attribute("step"),
                GrantRequestFlow.KONIEC.getName()));

            where.add(OR);
        }

        if (query.zip != null)
        	where.add(Expression.like(Function.upper(grantTable.attribute("zip")),
                    StringUtils.substring(query.zip.toUpperCase(), 0, 159)+"%"));
        
        if (query.location != null)
        	where.add(Expression.like(Function.upper(grantTable.attribute("location")),
                    StringUtils.substring(query.location.toUpperCase(), 0, 159)+"%"));
        
        if (query.street != null)
        	where.add(Expression.like(Function.upper(grantTable.attribute("street")),
                    StringUtils.substring(query.street.toUpperCase(), 0, 159)+"%"));
            		
        if (query.startDocumentDate != null)
        {
            where.add(Expression.ge(grantTable.attribute("documentdate"),
                DateUtils.midnight(query.startDocumentDate, 0)));
        }

        if (query.endDocumentDate != null)
        {
            where.add(Expression.lt(grantTable.attribute("documentdate"),
                DateUtils.midnight(query.endDocumentDate, 1)));
        }

        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(grantTable, "id");

        OrderClause order = new OrderClause();
            
        if (query.sortField != null) {
            order.add(grantTable.attribute(query.sortField), query.ascending);
            selectId.add(grantTable, query.sortField);
        }
        else {
        	order.add(grantTable.attribute("sequenceid"), Boolean.FALSE);
        	selectId.add(grantTable, "sequenceid");
        }

        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(distinct "+grantTable.getAlias()+".id)");

        int totalCount;
        SelectQuery selectQuery;

        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby wniosk�w.");
            totalCount = rs.getInt(countCol.getPosition());
            rs.close();

            selectQuery = new SelectQuery(selectId, from, where, order);
            if (query.getLimit() > 0)
            {
                selectQuery.setMaxResults(query.getLimit());
                selectQuery.setFirstResult(query.getOffset());
            }
            //ps = selectQuery.createStatement(DSApi.context().session().connection());
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            List<GrantRequest> results = new ArrayList<GrantRequest>(query.getLimit());
            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(DSApi.context().load(GrantRequest.class, id));
            }
            rs.close();

            return new SearchResultsAdapter<GrantRequest>(results, query.getOffset(), totalCount,
                GrantRequest.class);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static class Query extends FormQuery
    {
        private Integer sequenceId;
        private Integer sequenceIdFrom;
        private Integer sequenceIdTo;
        private Date startDocumentDate;
        private Date endDocumentDate;
        private Integer printed;
        private Boolean withBank;
        private String zip;
        private String location;
        private String street;
        private String firstname;
        private String lastname;
        private String sortField;
        private boolean ascending;
        private String schoolZip;
        private String schoolLocation;
        private String schoolStreet;
        private String schoolName;
        private String helpGranted;    /* forma pomocy: pieni�na lub rzeczowa */
        private String justification;  /* przyczyna odmowy */
        private String decision;       /* wnioski z jaka decyzja */
        private boolean finished = false; /* gdy wyszukujemy wniosku z udzielona juz decyzja */

        public Query()
        {
        }

        public Query(int offset, int limit)
        {
            super(offset, limit);
        }

        public void setZip(String zip)
        {
            this.zip = zip;
        }

        public void setLocation(String location)
        {
            this.location = location;
        }
        
        public void setStreet(String street)
        {
            this.street = street;
        }
        
        public void setFirstname(String firstname)
        {
            this.firstname = firstname;
        }
        
        public void setLastname(String lastname)
        {
            this.lastname = lastname;
        }
        
        public void setSortField(String sortField)
        {
            this.sortField = sortField;
        }
        
        public void setAscending(boolean ascending)
        {
            this.ascending = ascending;
        }
        
        public void setStartDocumentDate(Date startDocumentDate)
        {
        	this.startDocumentDate = startDocumentDate;
        }
        
        public void setEndDocumentDate(Date endDocumentDate)
        {
        	this.endDocumentDate = endDocumentDate;
        }

        public void setPrinted(Integer printed)
        {
            this.printed = printed;
        }

        public void setWithBank(boolean withBank)
        {
            this.withBank = withBank;
        }

        public void setSequenceId(Integer sequenceId)
        {
        	this.sequenceId = sequenceId;
        }

        public void setSequenceIdFrom(Integer sequenceIdFrom)
        {
        	this.sequenceIdFrom = sequenceIdFrom;
        }

        public void setSequenceIdTo(Integer sequenceIdTo)
        {
        	this.sequenceIdTo = sequenceIdTo;
        }

        public void setSchoolZip(String schoolZip)
        {
            this.schoolZip = schoolZip;
        }

        public void setSchoolLocation(String schoolLocation)
        {
            this.schoolLocation = schoolLocation;
        }

        public void setSchoolStreet(String schoolStreet)
        {
            this.schoolStreet = schoolStreet;
        }

        public void setSchoolName(String schoolName)
        {
            this.schoolName = schoolName;
        }

        public void setHelpGranted(String helpGranted)
        {
            this.helpGranted = helpGranted;
        }

        public void setDecision(String decision)
        {
            this.decision = decision;
        }

        public void setJustification(String justification)
        {
            this.justification = justification;
        }

        public void setFinished(boolean finished)
        {
            this.finished = finished;
        }
    }

    /**
     * Podaje opis udzielonej decyzji dla tego wniosku. Zak�adam ze jest conajmniej
     * jeden beneficjent.
     *
     * @return opis decyzji
     */
    public String getDecisionDescription()
    {
        if (GrantRequestFlow.KONIEC.getName().equals(step))
        {
            Beneficiary beneficiary = (Beneficiary) beneficiaries.get(0);

            if (BeneficiaryFlow.KONIEC.getName().equals(beneficiary.getStep()))
            {
                String description = "pozytywna, ";
                description += (MAKSYMALNY_DOCHOD.intValue() - income.intValue()) + "z�., ";
                if (STYPENDIUM_P.equals(beneficiary.getHelpGranted()) ||
                    ZASILEK_P.equals(beneficiary.getHelpGranted()))
                {
                    description += "forma pieni�na, ";
                    if (beneficiary.getBankAccount() != null)
                        description += "przelew";
                    else
                        description += "kasa";
                }
                else if (STYPENDIUM_R.equals(beneficiary.getHelpGranted()) ||
                    ZASILEK_R.equals(beneficiary.getHelpGranted()))
                    description += "forma rzeczowa";

                return description;
            }
            else if (BeneficiaryFlow.ODMOWA.getName().equals(beneficiary.getStep()))
            {
                String description = "odmowna, ";
                if (BeneficiaryFlow.NIEODPOWIEDNIE_DOCHODY_WYJASNIENIE.equals(beneficiary.getJustification()))
                    description += "przekroczony doch�d";
                else if (BeneficiaryFlow.NIEODPOWIEDNI_WIEK_WYJASNIENIE.equals(beneficiary.getJustification()))
                    description += "nieodpowiedni wiek";

                return description;
            }
        }
        else if (GrantRequestFlow.NIEKOMPLETNY.getName().equals(step))
        {
            return "odmowna, wniosek niekompletny";
        }
        else if (GrantRequestFlow.PLACOWKA_NIEWLASCIWA.getName().equals(step))
        {
            return "odmowna, plac�wka niew�a�ciwa";
        }
        else if (GrantRequestFlow.ORGAN_NIEWLASCIWY.getName().equals(step))
        {
            return "odmowna, organ niew�a�ciwy";
        }

        return null;
    }
    
    public static void fillTemplateRTF(StyledDocument document, GrantRequest grantRequest, int beneficiaryNo) throws EdmException
    {
    	final int BENEFICIARY_MAX_COUNT = 4;
    	
    	Map<String,String> context = new HashMap<String, String>();
        context.put("data_utworzenia", DateUtils.formatCommonDate(new Date()));
        context.put("adres", grantRequest.getAddress());
        context.put("adres_ulica", grantRequest.getStreet());
        context.put("adres_miejscowosc", grantRequest.getZip()+" "+grantRequest.getLocation());
        context.put("wnioskodawca", grantRequest.getFirstname()+" "+grantRequest.getLastname());
        context.put("data_wniosku", DateUtils.formatCommonDate(grantRequest.getDocumentDate()));
        context.put("numer_sprawy", grantRequest.getOfficeId());
        context.put("dochod", grantRequest.getIncome().toString());
        context.put("data", DateUtils.formatCommonDate(new Date()));
        context.put("lista_uczniow", grantRequest.getBeneficiary(beneficiaryNo).getFirstname()+" "+grantRequest.getBeneficiary(beneficiaryNo).getLastname());
        for (int i=0; i < BENEFICIARY_MAX_COUNT; i++)
        {
        	if (i == 0)
        	{
        		Beneficiary beneficiary = grantRequest.getBeneficiary(beneficiaryNo);
        		context.put("uczen_"+i, beneficiary.getFirstname()+" "+beneficiary.getLastname());
        		context.put("data_ur_"+i, DateUtils.formatCommonDate(beneficiary.getBirthDate()));
        	}
        	else
        	{
        		context.put("uczen_"+i, "....................................");
        		context.put("data_ur_"+i, "....................");
        	}
        }
        context.put("kwota", ((new BigDecimal(300)).subtract(grantRequest.getIncome())).toString());
        context.put("numer_konta", grantRequest.getBeneficiary(beneficiaryNo).getBankAccount());
        RTF.substitute(document, context, "{", "}");
    }
    
    public static String getRequestedHelpDescription(String helpRequested)
    {
        if (STYPENDIUM.equals(helpRequested))
            return "Stypendium";
        else if (ZASILEK.equals(helpRequested))
            return "Zasi�ek";
        else
            return null;
    }

    public static String getGrantedHelpDescription(String helpGranted)
    {
        if (STYPENDIUM_P.equals(helpGranted))
            return "Stypendium pieni�ne";
        else if (STYPENDIUM_R.equals(helpGranted))
            return "Stypendium rzeczowe";
        else if (ZASILEK_P.equals(helpGranted))
            return "Zasi�ek pieni�ny";
        else if (ZASILEK_R.equals(helpGranted))
            return "Zasi�ek rzeczowy";
        else
            return null;
    }

    public String getAddress()
    {
    	String address = street;
    	if (zip != null)
    		address += " " + zip;
    	if (location != null)
    		address += " " + location;
    	return address;
    }

    public String getBeneficiarySchoolAddress()
    {
        if (beneficiaries.size() > 0)
            return ((Beneficiary) beneficiaries.get(0)).getSchoolAddress();
        else
            return null;
    }

    public String getBeneficiaryName()
    {
        if (beneficiaries.size() > 0)
            return ((Beneficiary) beneficiaries.get(0)).getFirstname()+" "+((Beneficiary) beneficiaries.get(0)).getLastname();
        else
            return null;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public OfficeCase getOfficeCase()
    {
        return officeCase;
    }

    public void setOfficeCase(OfficeCase officeCase)
    {
        this.officeCase = officeCase;
    }

    public String getOfficeId()
    {
    	return officeId;
    }
    
    public void setOfficeId(String officeId)
    {
    	this.officeId = officeId;
    }
    
    public Date getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(Date documentDate)
    {
        this.documentDate = documentDate;
    }

    public Long getInDocumentId()
    {
        return inDocumentId;
    }

    public void setInDocumentId(Long inDocumentId)
    {
        this.inDocumentId = inDocumentId;
    }
    
    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getTemplateName(int beneficiaryNo) throws EdmException
    {
    	if (step.equals(GrantRequestFlow.NIEKOMPLETNY.getName()))
    		return RES_NIEKOMPLETNY_TEMPLATE;
    	else if (step.equals(GrantRequestFlow.ORGAN_NIEWLASCIWY.getName()))
    		return RES_ORGAN_NIEWLASCIWY_TEMPLATE;
    	else if (step.equals(GrantRequestFlow.PLACOWKA_NIEWLASCIWA.getName()))
    		return RES_PLACOWKA_NIEWLASCIWA_TEMPLATE;
    	else if (step.equals(GrantRequestFlow.KONIEC.getName()))
    	{
    		Beneficiary beneficiary = getBeneficiary(beneficiaryNo);
    		if (beneficiary.getStep().equals(BeneficiaryFlow.ODMOWA.getName()))
    		{
    			if (BeneficiaryFlow.NIEODPOWIEDNI_WIEK_WYJASNIENIE.equals(beneficiary.getJustification()))
                {

                    int difference = (new Date()).getYear() - beneficiary.getBirthDate().getYear();
                    if (difference > 23)
                        return RES_ZA_WYSOKI_WIEK_TEMPLATE;
                    else
                        return RES_ZA_NISKI_WIEK_TEMPLATE;
                }
                else if (BeneficiaryFlow.NIEODPOWIEDNIE_DOCHODY_WYJASNIENIE.equals(beneficiary.getJustification()))
    				return RES_NIEWLASCIWE_DOCHODY_TEMPLATE;
    		}
    		else if (beneficiary.getStep().equals(BeneficiaryFlow.KONIEC.getName()))
    		{
    			if (GrantRequest.STYPENDIUM_P.equals(beneficiary.getHelpGranted()))
    				return RES_STYPENDIUM_PIENIEZNE_TEMPLATE;
    			else if (GrantRequest.STYPENDIUM_R.equals(beneficiary.getHelpGranted()))
    				return RES_STYPENDIUM_RZECZOWE_TEMPLATE;
    			else if (GrantRequest.ZASILEK_P.equals(beneficiary.getHelpGranted()))
    				return RES_ZASILEK_PIENIEZNY_TEMPLATE;
    			else if (GrantRequest.ZASILEK_R.equals(beneficiary.getHelpGranted()))
    				return RES_ZASILEK_RZECZOWY_TEMPLATE;	
    		}    			
    	}
    	
    	throw new EdmException("Napotkany niezidentyfikowany przypadek podczas tworzenia szablonu rtf");
    }
    
    public List getBeneficiaries()
    {
        if (beneficiaries == null)
            beneficiaries = new LinkedList();
        return beneficiaries;
    }

    public Beneficiary getBeneficiary(int posn) throws EdmException
    {
        if (beneficiaries == null || beneficiaries.size() <= posn)
            throw new EdmException("Nie znaleziono beneficjenta numer "+posn);
        return (Beneficiary) beneficiaries.get(posn);
    }

    public void addBeneficiary(Beneficiary beneficiary) throws EdmException
    {
        if (beneficiaries == null)
            beneficiaries = new LinkedList();

        if (beneficiary.getStep() == null)
            new BeneficiaryFlow(beneficiary).start();

        beneficiaries.add(beneficiary);
    }

    public void deleteBeneficiary(int posn) throws EdmException
    {
        if (beneficiaries == null || beneficiaries.size() <= posn)
            return;

        beneficiaries.remove(posn);
    }

    void setBeneficiaries(List beneficiaries)
    {
        this.beneficiaries = beneficiaries;
    }

    public String getStep()
    {
        return step;
    }

    void setStep(String step)
    {
        this.step = step;
    }

    public String getLastStep()
    {
        return lastStep;
    }

    void setLastStep(String lastStep)
    {
        this.lastStep = lastStep;
    }

    public String getLastResult()
    {
        return lastResult;
    }

    void setLastResult(String lastResult)
    {
        this.lastResult = lastResult;
    }

    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public BigDecimal getIncome()
    {
        return income;
    }

    public void setIncome(BigDecimal income)
    {
        this.income = income;
    }

    public void setPrinted(Integer printed)
    {
        this.printed = printed;
    }

    public Integer getPrinted()
    {
        if (printed == null)
            return Integer.valueOf(0);
        else
            return printed;
    }
}
