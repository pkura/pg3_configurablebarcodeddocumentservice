package pl.compan.docusafe.core.record.grants;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.DateUtils;

import java.util.Date;
/* User: Administrator, Date: 2005-10-20 14:22:34 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: BeneficiaryFlow.java,v 1.5 2006/07/28 14:07:44 mmanski Exp $
 */
public class BeneficiaryFlow extends Flow
{
    public static final Step ODPOWIEDNI_WIEK =
        new Step("ODPOWIEDNI_WIEK", "Czy spe�nia przedzia� wiekowy (7-24 lat)",
            new Object[] { "true", "Tak", "false", "Nie" } );
    public static final Step ODPOWIEDNIE_DOCHODY =
        new Step("ODPOWIEDNIE_DOCHODY", "Czy spe�nia kryterium dochodowe",
            new Object[] { "true", "Tak", "false", "Nie" } );
    public static final Step FORMA_POMOCY =
        new Step("FORMA_POMOCY", "Wyb�r formy pomocy", false);
/*
    public static final Step FORMA_POMOCY =
        new Step(
            "FORMA_POMOCY",
            "Jaka ma by� forma pomocy?",
            new Object[] { GrantRequest.STYPENDIUM, "Stypendium szkolne",
                           GrantRequest.ZASILEK, "Zasi�ek szkolny" });
*/
    public static final Step ODMOWA =
        new Step("ODMOWA", "Odmowa pomocy", true);
    public static final Step KONIEC =
        new Step("KONIEC", "Przyznanie pomocy jest mo�liwe.", true);

    public static final String NIEODPOWIEDNIE_DOCHODY_WYJASNIENIE = "Nie spe�nia kryterium dochodowego";
    public static final String NIEODPOWIEDNI_WIEK_WYJASNIENIE = "Nie spe�nia kryterium wiekowego";
    
    private Beneficiary beneficiary;

    public BeneficiaryFlow(Beneficiary beneficiary)
    {
        this.beneficiary = beneficiary;

        register(ODPOWIEDNI_WIEK);
        register(ODPOWIEDNIE_DOCHODY);
        register(FORMA_POMOCY);
        register(ODMOWA);
        register(KONIEC);
    }

    public void start() throws EdmException
    {
        if (beneficiary.getStep() != null)
            throw new EdmException("Proces zosta� ju� uruchomiony");

        beneficiary.setStep(ODPOWIEDNI_WIEK.getName());
    }

    public Step push(String result) throws EdmException
    {
        if (beneficiary.getStep() == null)
            throw new EdmException("Proces nie zosta� uruchomiony");

        Step currentStep = getStep(beneficiary.getStep());

        if (currentStep.isTerminal())
            throw new EdmException("Proces zosta� zako�czony");

        if (currentStep.getResults().size() > 0 && result == null)
            throw new EdmException("Nie podano wyniku");

        if (currentStep.getResults().size() > 0 && result != null &&
            !currentStep.getResults().containsKey(result))
            throw new EdmException("Podany wynik jest nieprawid�owy (nie jest to" +
                " jeden z mo�liwych wynik�w");

        Step lastStep = currentStep;
        String lastResult = result;

        Step step;

        if (ODPOWIEDNI_WIEK.equals(currentStep))
        {
            if ("false".equals(result))
            {
                step = ODMOWA;
                beneficiary.setRejected(Boolean.TRUE);
                beneficiary.setJustification(NIEODPOWIEDNI_WIEK_WYJASNIENIE);
            }
            else
            {
                step = ODPOWIEDNIE_DOCHODY;
            }
        }
        else if (ODPOWIEDNIE_DOCHODY.equals(currentStep))
        {
            if ("false".equals(result))
            {
                beneficiary.setRejected(Boolean.TRUE);
                beneficiary.setJustification(NIEODPOWIEDNIE_DOCHODY_WYJASNIENIE);
                step = ODMOWA;
            }
            else
            {
                step = FORMA_POMOCY;
            }
        }
        else if (FORMA_POMOCY.equals(currentStep))
        {
            if (beneficiary.getHelpGranted() == null)
                throw new EdmException("Nie okre�lono formy pomocy");
            // sprawdzenie formy - pieni�nej/rzeczowej
            // nadanie odpowiednich warto�ci polom
            //beneficiary.set
            step = KONIEC;
        }
        else
        {
            throw new EdmException("Nieprawid�owy krok procesu: "+currentStep.getName());
        }

        beneficiary.setStep(step.getName());
        beneficiary.setLastStep(lastStep.getName());
        beneficiary.setLastResult(lastResult);

        return step;
    }

    public boolean isTerminated()
    {
        return beneficiary.getStep() != null && getStep(beneficiary.getStep()).isTerminal();
    }

    public Step getStep()
    {
        return getStep(beneficiary.getStep());
    }

    public String suggestResult()
    {
        Step step = getStep();

        if (step == ODPOWIEDNI_WIEK && beneficiary.getBirthDate() != null)
        {
            int days = DateUtils.difference(new Date(), beneficiary.getBirthDate());
            if (days >= 7*365 && days <= 24*365)
                return "true";
            else
                return "false";
        }

        return null;
    }
}
