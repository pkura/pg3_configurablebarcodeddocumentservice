package pl.compan.docusafe.core.record.projects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class AccountingCostKind {

	private Integer id;
	private String title;
	private String cn;
	private Integer centrum;
	private String refValue;
	private Integer available;

	public AccountingCostKind() {
	}

	public AccountingCostKind(String string, String refValue, String cn) {
		this.title = string;
		this.cn = cn;
		this.refValue = refValue;
		centrum = 0;
		available = 1;
	}

	/**
	 * Zwraca list� wszystkich obiekt�w CostKind znajduj�cych si� w
	 * bazie posortowanych po polu posn (zainicjalizowane proxy).
	 * 
	 * @return
	 * 
	 * @throws EdmException
	 */
	public static List<AccountingCostKind> list() throws EdmException {
		List<AccountingCostKind> list = Finder.list(AccountingCostKind.class,
				"title");
		for (AccountingCostKind iter: list) {
			iter.getCn();
			iter.getTitle();
			DSApi.initializeProxy(iter);
		}
		return list;
	}

	/**
	 * 
	 * @param id
	 * @return Zwraca rodzaj kosztu projektu o podanym id
	 * @throws EdmException
	 */
	public static AccountingCostKind find(Integer id) throws EdmException {
		return (AccountingCostKind) Finder.find(AccountingCostKind.class, id);
	}

	/**
	 * @return Sprawdza czy mo�na usun�� dany typ kosztu
	 * @throws EdmException
	 */
	public boolean canDelete() throws EdmException {
		FromClause from = new FromClause();
		TableAlias docTable = from.createTable(DSApi
				.getTableName(ProjectEntry.class));

		WhereClause where = new WhereClause();
		where.add(Expression.eq(docTable.attribute("costKindId"), getId()));

		SelectClause selectCount = new SelectClause();
		selectCount.addSql("count(*)");

		SelectQuery selectQuery;

		try {
			selectQuery = new SelectQuery(selectCount, from, where, null);

			ResultSet rs = selectQuery.resultSet(DSApi.context().session()
					.connection());
			boolean unable = false;
			int count = -1;
			if (rs.next()) {
				count = rs.getInt(1);
			} else {
				unable = true;
			}
			rs.close();
			if (unable) {
				throw new EdmException("Nie mo�na pobra� liczby projektow");
			}
			return count == 0;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		} catch (SQLException e) {
			throw new EdmSQLException(e);
		}
	}

	public void delete() throws EdmException {
		try {
			DSApi.context().session().delete(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public void create() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

}
