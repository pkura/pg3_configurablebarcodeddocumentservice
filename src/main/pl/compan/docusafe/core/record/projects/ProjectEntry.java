package pl.compan.docusafe.core.record.projects;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AbstractQuery;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class ProjectEntry implements Lifecycle
{
	private static Logger log = LoggerFactory.getLogger(ProjectEntry.class);
	public static final String SETTLEMENT_KIND_NETTO = "NETTO";
	public static final String SETTLEMENT_KIND_BRUTTO = "BRUTTO";
	public static final String[] settlementKinds = new String[] { SETTLEMENT_KIND_BRUTTO, SETTLEMENT_KIND_NETTO };
	public static final Set<Integer> wynagrodzeniaCostKinds = new HashSet<Integer>(Arrays.asList(new Integer[] { 7, 8, 9, 11 }));
	public static final Integer KOSZT_POSREDNI_ID = 17;

	public static final String[] currencyKinds = new String[] { "PLN", "CHF", "EUR", "GBP", "USD" };
	public static final String BLOCADE = "BLOCADE";
	public static final String PLAN = "PLAN";
	public static final String COST = "COST";
	public static final String IMPACT = "IMPACT";

	private Long id;
	private Long projectId;
	private String caseNumber;
	private Date dateEntry;
	private Date expenditureDate;
	private Date costDate;
	private String costName;
	private Integer costKindId;
	private String costKind;
	private String costCategory;
	private BigDecimal gross;
	private BigDecimal net;
	private BigDecimal vat;
	private BigDecimal plnCost;
	private String entryType;
	private String entryTypeName;
	private String docNr;
	private String demandNr;
	private String description;
	private String settlementKind;
	private String currency;
	private BigDecimal rate;
	private CostKind costKindObj;
	private AccountingCostKind accountingCostKind;
	private Date mtime;
	private String entryNumber;
	private Long costId;

	public static HashMap<String, String> entryTypeList = new HashMap<String, String>(4);

	static
	{
		entryTypeList = new HashMap<String, String>(4);
		entryTypeList.put(ProjectEntry.BLOCADE, "Blokada");
		entryTypeList.put(ProjectEntry.COST, "Koszt");
		entryTypeList.put(ProjectEntry.PLAN, "Plan");
		entryTypeList.put(ProjectEntry.IMPACT, "Wp�yw");
	}

	public ProjectEntry()
	{
	}

	public static ProjectEntry find(Long id) throws EdmException
	{
		ProjectEntry entry = (ProjectEntry) DSApi.getObject(ProjectEntry.class, id);

		if (entry == null)
			throw new EntityNotFoundException(ProjectEntry.class, id);

		return entry;
	}

	public static List<ProjectEntry> findByDocNr(Long id) throws EdmException
	{
		try
		{
			Criteria criteria = DSApi.context().session().createCriteria(ProjectEntry.class);
			criteria.add(Restrictions.eq("docNr", id.toString()));
			List<ProjectEntry> list = criteria.list();

			if (list.size() > 0)
			{
				return list;
			}

			return Collections.EMPTY_LIST;
		}
		catch (HibernateException e)
		{
			log.error(e.getMessage(), e);
			throw new EdmHibernateException(e);
		}
	}
	
	public void create() throws EdmException
	{
		try
		{
			this.setMtime(new Date());
			DSApi.context().session().save(this);
			log.info("Dodano wpis do rejestru projekt�w wraz z nadaniem numeru sprawy");
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public void update() throws EdmException
	{
		try
		{
			this.mtime = new Date();
			DSApi.context().session().update(this);
			log.info("Uaktualniono wpis do rejestru projekt�w wraz z nadaniem numeru sprawy");
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public void onLoad(Session s, Serializable id)
	{
		try
		{
			costKindObj = CostKind.find(costKindId);
			costKind = costKindObj.getTitle();
			costCategory = costKindObj.getCn();
		}
		catch (Exception e)
		{
			log.error(e.getMessage());
		}
	}

	public void delete() throws EdmException
	{
		try
		{
			DSApi.context().session().delete(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public static SearchResults<ProjectEntry> getPrejectEntries(Query query)
	{

		Criteria criteria = null;
		try
		{
			criteria = DSApi.context().session().createCriteria(ProjectEntry.class);

			if (query.getCostName() != null)
				criteria.add(Restrictions.like("costName", query.getCostName()));
			if (query.getCostKindId() != null)
				criteria.add(Restrictions.eq("costKindId", query.getCostKindId()));
			if (query.getNotEqCostKindIdList() != null && query.getNotEqCostKindIdList().size() > 0)
			{
				for (Integer cki : query.getNotEqCostKindIdList())
				{
					criteria.add(Restrictions.not(Restrictions.eq("costKindId", cki)));
				}
			}
			if (query.getExpenditureDateFrom() != null)
				criteria.add(Restrictions.ge("expenditureDate", query.getExpenditureDateFrom()));
			if (query.getExpenditureDateTo() != null)
				criteria.add(Restrictions.le("expenditureDate", query.getExpenditureDateTo()));
			if (query.getCostDateFrom() != null)
				criteria.add(Restrictions.ge("costDate", query.getCostDateFrom()));
			if (query.getCostDateTo() != null)
				criteria.add(Restrictions.le("costDate", query.getCostDateTo()));
			if (query.getDateEntryTo() != null)
				criteria.add(Restrictions.le("dateEntry", query.getDateEntryTo()));
			if (query.getDateEntryFrom() != null)
				criteria.add(Restrictions.ge("dateEntry", query.getDateEntryFrom()));
			if (query.getDemandNr() != null)
				criteria.add(Restrictions.eq("demandNr", query.getDemandNr()));
			if (query.getDescription() != null)
				criteria.add(Restrictions.like("description", query.getDescription()));
			if (query.getDocNr() != null)
				criteria.add(Restrictions.eq("docNr", query.getDocNr()));
			if (query.getEntryType() != null)
			{

				criteria.add(Restrictions.eq("entryType", query.getEntryType().trim()));
			}
			if (query.getGrossFrom() != null)
				criteria.add(Restrictions.ge("gross", query.getGrossFrom()));
			if (query.getGrossTo() != null)
				criteria.add(Restrictions.le("gross", query.getGrossTo()));

			if (query.getCaseNumber() != null)
				criteria.add(Restrictions.like("caseNumber", query.getCaseNumber()));
			if (query.getEntryNumber() != null)
				criteria.add(Restrictions.like("entryNumber", query.getEntryNumber()));
			if (query.getNetFrom() != null)
				criteria.add(Restrictions.ge("net", query.getNetFrom()));
			if (query.getNetTo() != null)
				criteria.add(Restrictions.le("net", query.getNetTo()));

			if (query.getId() != null)
				criteria.add(Restrictions.eq("id", query.getId()));
			if (query.getProjectId() != null)
				criteria.add(Restrictions.eq("projectId", query.getProjectId()));
			if (query.getVat() != null)
				criteria.add(Restrictions.eq("vat", query.getVat()));
			if (query.getSortField() == null)
				query.setSortField("dateEntry");

			criteria.addOrder(query.isAscending() ? Order.asc(query.getSortField()) : Order.desc(query.getSortField()));

			List<ProjectEntry> results = criteria.list();
			List<ProjectEntry> entries = new ArrayList<ProjectEntry>();

			for (ProjectEntry entry : results)
			{
				if (Project.find(entry.getProjectId()).isHasPermissionToRead())
					entries.add(entry);
			}

			int totalCount = entries.size();

			if (query.limit > 0)
			{
				criteria.setFirstResult(query.offset);
				criteria.setMaxResults(query.limit);
			}

			results = criteria.list();
			entries = new ArrayList<ProjectEntry>();

			for (ProjectEntry entry : results)
			{
				if (Project.find(entry.getProjectId()).isHasPermissionToRead())
					entries.add(entry);
			}
			return new SearchResultsAdapter<ProjectEntry>(entries, query.offset, totalCount, ProjectEntry.class);

		}
		catch (EdmException e)
		{
			log.debug(e.getMessage(), e);
		}
		return null;
	}

	public static List<ProjectEntry> getProjectEntry(Long id) throws EdmException
	{
		return getProjectEntry(id, null, false);
	}

	public static List<ProjectEntry> getProjectEntry(Long id, String sortField, boolean asc) throws EdmException
	{
		Criteria c = DSApi.context().session().createCriteria(ProjectEntry.class);
		c.add(Restrictions.eq("projectId", id));
		if (sortField != null)
		{
			c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
		}
		return new ArrayList<ProjectEntry>(c.list());
	}

	public static List<ProjectEntry> getProjectEntryByDocumentId(String docId, String sortField, boolean asc) throws EdmException
	{
		Criteria c = DSApi.context().session().createCriteria(ProjectEntry.class);
		c.add(Restrictions.eq("docNr", docId));
		if (sortField != null)
		{
			c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
		}
		return new ArrayList<ProjectEntry>(c.list());
	}

	public Date getExpenditureDate()
	{
		return expenditureDate;
	}

	public void setExpenditureDate(Date expenditureDate)
	{
		this.expenditureDate = expenditureDate;
	}

	public Date getCostDate()
	{
		return costDate;
	}

	public void setCostDate(Date costDate)
	{
		this.costDate = costDate;
	}

	public String getCostName()
	{
		return costName;
	}

	public void setCostName(String costName)
	{
		this.costName = costName;
	}

	public Integer getCostKindId()
	{
		return costKindId;
	}

	public void setCostKindId(Integer costKindId)
	{
		this.costKindId = costKindId;
	}

	public BigDecimal getGross()
	{
		return gross;
	}

	public void setGross(BigDecimal gross)
	{
		this.gross = gross;
	}

	public BigDecimal getNet()
	{
		return net;
	}
	public BigDecimal getMainCurrencyRate (String currency,Set<String> msg) {
	   

	    if (currency.equals("EUR")) {
		msg.add(currency+" ("+Docusafe.getAdditionProperty("kurs_euro")+")");
		return new BigDecimal(Docusafe.getAdditionProperty("kurs_euro"));
	    }
	    else if (currency.equals("CHF")) {
		msg.add(currency+" ("+Docusafe.getAdditionProperty("kurs_chf")+")");
		return new BigDecimal(Docusafe.getAdditionProperty("kurs_chf"));
	    }
	    else if (currency.equals("GBP")) {
		msg.add(currency+" ("+Docusafe.getAdditionProperty("kurs_gbp")+")");
		return new BigDecimal(Docusafe.getAdditionProperty("kurs_gbp"));
	    }
	    else if (currency.equals("USD")) {
		msg.add(currency+" ("+Docusafe.getAdditionProperty("kurs_usd")+")");
		return new BigDecimal(Docusafe.getAdditionProperty("kurs_usd"));
	    }
	    else return new BigDecimal(1);
	}

	public BigDecimal getBalanceAmount(String currency, Set<String> msg)
	{
		if (SETTLEMENT_KIND_NETTO.equals(this.getSettlementKind()))
		{
			if (this.currency.equals(currency))
				return net.setScale(2, RoundingMode.HALF_UP);
			else if (this.currency.equals("PLN") && !currency.equals("PLN"))
			{
				BigDecimal _rate = getMainCurrencyRate (currency,msg);
				return net.setScale(4, RoundingMode.HALF_UP).divide(_rate, RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP);
			}
			else if (!this.currency.equals("PLN") && currency.equals("PLN"))
			{
				return net.setScale(4, RoundingMode.HALF_UP).multiply(this.rate).setScale(2, RoundingMode.HALF_UP);
			}
			else //(!this.currency.equals("PLN") && !currency.equals("PLN"))
			{
			    	BigDecimal _rate = getMainCurrencyRate (currency,msg);
				return net.setScale(4, RoundingMode.HALF_UP).multiply(this.rate).setScale(2, RoundingMode.HALF_UP).divide(_rate, RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP);
			}

		}
		else
		// if (SETTLEMENT_KIND_BRUTTO.equals(this.getSettlementKind())
		{
			if (this.currency.equals(currency))
				return gross.setScale(2, RoundingMode.HALF_UP);
			else if (this.currency.equals("PLN") && !currency.equals("PLN"))
			{
				BigDecimal _rate = getMainCurrencyRate (currency,msg);
				return gross.setScale(4, RoundingMode.HALF_UP).divide(_rate, RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP);
			}
			else if (!this.currency.equals("PLN") && currency.equals("PLN"))
			{
				return gross.setScale(4, RoundingMode.HALF_UP).multiply(this.rate).setScale(2, RoundingMode.HALF_UP);
			}
			else //(!this.currency.equals("PLN") && !currency.equals("PLN"))
			{
			    	BigDecimal _rate = getMainCurrencyRate (currency,msg);
				return gross.setScale(4, RoundingMode.HALF_UP).multiply(this.rate).setScale(2, RoundingMode.HALF_UP).divide(_rate, RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP);
			}
		}
	}

	public BigDecimal getBalanceAmountByCurrency(String currency, Set<String> msg)
	{
		return getBalanceAmount(currency,msg);
	}

	public void setNet(BigDecimal net)
	{
		this.net = net;
	}

	public BigDecimal getVat()
	{
		return vat;
	}

	public void setVat(BigDecimal vat)
	{
		this.vat = vat;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Long getId()
	{
		return id;
	}

	private void setId(Long id)
	{
		this.id = id;
	}

	public Long getProjectId()
	{
		return projectId;
	}

	public void setProjectId(Long projectId)
	{
		this.projectId = projectId;
	}

	public Date getDateEntry()
	{
		return dateEntry;
	}

	public void setDateEntry(Date dateEntry)
	{
		this.dateEntry = dateEntry;
	}

	public String getDocNr()
	{
		return docNr;
	}

	public void setDocNr(String docNr)
	{
		this.docNr = docNr;
	}

	public String getDemandNr()
	{
		return demandNr;
	}

	public void setDemandNr(String demandNr)
	{
		this.demandNr = demandNr;
	}

	public boolean onDelete(Session arg0) throws CallbackException
	{
		// TODO Auto-generated method stub
		return false;
	}

	public boolean onSave(Session arg0) throws CallbackException
	{
		BigDecimal zero = new BigDecimal(0.0000);
		if (rate == null || rate.equals(zero))
			rate = zero;
		this.setMtime(new Date());
		return false;
	}

	public boolean onUpdate(Session arg0) throws CallbackException
	{
		this.setMtime(new Date());
		return false;
	}

	@Override
	public String toString()
	{
		return "ProjectEntry [id=" + id + ", projectId=" + projectId + ", dateEntry=" + dateEntry + ", expenditureDate=" + expenditureDate + ", costDate=" + costDate + ", costName=" + costName + ", costKindId=" + costKindId + ", gross=" + gross + ", net=" + net + ", vat=" + vat + ", entryType="
				+ entryType + ", docNr=" + docNr + ", demandNr=" + demandNr + ", description=" + description + "]";
	}

	public String getEntryType()
	{
		return entryType;
	}

	public void setEntryType(String entryType)
	{
		this.entryType = entryType;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectEntry other = (ProjectEntry) obj;
		if (id == null)
		{
			if (other.id != null)
				return false;
		}
		else if (!id.equals(other.id))
			return false;
		return true;
	}

	public static class Query extends AbstractQuery
	{
		private int offset;
		private int limit;
		private String sortField;
		private boolean ascending;

		private Long id;
		private Long projectId;
		private Date dateEntryFrom;
		private Date dateEntryTo;
		private Date expenditureDateFrom;
		private Date expenditureDateTo;
		private Date costDateFrom;
		private Date costDateTo;
		private String costName;
		private Integer costKindId;
		private Set<Integer> notEqCostKindIdList;
		private BigDecimal grossFrom;
		private BigDecimal grossTo;
		private BigDecimal netFrom;
		private BigDecimal netTo;
		private BigDecimal vat;
		private String entryType;
		private String docNr;
		private String demandNr;
		private String description;
		private String caseNumber;
		private String entryNumber;

		public Query(int offset, int limit)
		{
			this.offset = offset;
			this.limit = limit;
		}

		protected boolean validSortField(String field)
		{
			return "dateEntry".equals(field) || "expenditureDate".equals(field) || "costName".equals(field) || "costDate".equals(field) || "costKindId".equals(field) || "gross".equals(field) || "net".equals(field) || "vat".equals(field) || "entryType".equals(field) || "docNr".equals(field)
					|| "demandNr".equals(field) || "description".equals(field);
		}

		public int getOffset()
		{
			return offset;
		}

		public void setOffset(int offset)
		{
			this.offset = offset;
		}

		public int getLimit()
		{
			return limit;
		}

		public void setLimit(int limit)
		{
			this.limit = limit;
		}

		public Long getId()
		{
			return id;
		}

		public void setId(Long id)
		{
			this.id = id;
		}

		public Long getProjectId()
		{
			return projectId;
		}

		public void setProjectId(Long projectId)
		{
			this.projectId = projectId;
		}

		public Date getDateEntryFrom()
		{
			return dateEntryFrom;
		}

		public void setDateEntryFrom(Date dateEntryFrom)
		{
			this.dateEntryFrom = dateEntryFrom;
		}

		public Date getDateEntryTo()
		{
			return dateEntryTo;
		}

		public void setDateEntryTo(Date dateEntryTo)
		{
			this.dateEntryTo = dateEntryTo;
		}

		public Date getExpenditureDateFrom()
		{
			return expenditureDateFrom;
		}

		public void setExpenditureDateFrom(Date expenditureDateFrom)
		{
			this.expenditureDateFrom = expenditureDateFrom;
		}

		public Date getExpenditureDateTo()
		{
			return expenditureDateTo;
		}

		public void setExpenditureDateTo(Date expenditureDateTo)
		{
			this.expenditureDateTo = expenditureDateTo;
		}

		public Date getCostDateFrom()
		{
			return costDateFrom;
		}

		public void setCostDateFrom(Date costDateFrom)
		{
			this.costDateFrom = costDateFrom;
		}

		public Date getCostDateTo()
		{
			return costDateTo;
		}

		public void setCostDateTo(Date costDateTo)
		{
			this.costDateTo = costDateTo;
		}

		public String getCostName()
		{
			return costName;
		}

		public void setCostName(String costName)
		{
			this.costName = costName;
		}

		public Integer getCostKindId()
		{
			return costKindId;
		}

		public void setCostKindId(Integer costKindId)
		{
			this.costKindId = costKindId;
		}

		public BigDecimal getGrossFrom()
		{
			return grossFrom;
		}

		public void setGrossFrom(BigDecimal grossFrom)
		{
			this.grossFrom = grossFrom;
		}

		public BigDecimal getGrossTo()
		{
			return grossTo;
		}

		public void setGrossTo(BigDecimal grossTo)
		{
			this.grossTo = grossTo;
		}

		public BigDecimal getNetFrom()
		{
			return netFrom;
		}

		public void setNetFrom(BigDecimal netFrom)
		{
			this.netFrom = netFrom;
		}

		public BigDecimal getNetTo()
		{
			return netTo;
		}

		public void setNetTo(BigDecimal netTo)
		{
			this.netTo = netTo;
		}

		public BigDecimal getVat()
		{
			return vat;
		}

		public void setVat(BigDecimal vat)
		{
			this.vat = vat;
		}

		public String getEntryType()
		{
			return entryType;
		}

		public void setEntryType(String entryType)
		{
			this.entryType = entryType;
		}

		public String getDocNr()
		{
			return docNr;
		}

		public void setDocNr(String docNr)
		{
			this.docNr = docNr;
		}

		public String getDemandNr()
		{
			return demandNr;
		}

		public void setDemandNr(String demandNr)
		{
			this.demandNr = demandNr;
		}

		public String getDescription()
		{
			return description;
		}

		public void setDescription(String description)
		{
			this.description = description;
		}

		public String getSortField()
		{
			return sortField;
		}

		public void setSortField(String sortField)
		{
			this.sortField = sortField;
		}

		public boolean isAscending()
		{
			return ascending;
		}

		public void setAscending(boolean ascending)
		{
			this.ascending = ascending;
		}

		public String getCaseNumber()
		{
			return caseNumber;
		}

		public void setCaseNumber(String caseNumber)
		{
			this.caseNumber = caseNumber;
		}

		public Set<Integer> getNotEqCostKindIdList()
		{
			return notEqCostKindIdList;
		}

		public void setNotEqCostKindIdList(Set<Integer> notEqCostKindIdList)
		{
			this.notEqCostKindIdList = notEqCostKindIdList;
		}

		public String getEntryNumber()
		{
			return entryNumber;
		}

		public void setEntryNumber(String entryNumber)
		{
			this.entryNumber = entryNumber;
		}
	}

	public String getCostKind()
	{
		return costKind;
	}

	public void setCostKind(String costKind)
	{
		this.costKind = costKind;
	}

	public String getEntryTypeName()
	{
		return entryTypeList.get(entryType);
	}

	public void setEntryTypeName(String entryTypeName)
	{
		this.entryTypeName = entryTypeName;
	}

	public static HashMap<String, String> getEntryTypeList()
	{
		return entryTypeList;
	}

	public static void setEntryTypeList(HashMap<String, String> entryTypeList)
	{
		ProjectEntry.entryTypeList = entryTypeList;
	}

	public String getSettlementKind()
	{
		return settlementKind;
	}

	public void setSettlementKind(String settlementKind)
	{
		this.settlementKind = settlementKind;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(String currency)
	{
		this.currency = currency;
	}

	public BigDecimal getRate()
	{
		return rate;
	}

	public void setRate(BigDecimal rate)
	{
		this.rate = rate;
	}

	public String getCostCategory()
	{
		if (costKindObj == null)
		{
			try
			{
				costKindObj = CostKind.find(costKindId);
				costKind = costKindObj.getTitle();
				costCategory = costKindObj.getCn();
			}
			catch (Exception e)
			{
				log.error(e.getMessage());
			}
		}
		return costCategory;
	}

	public void setCostCategory(String costCategory)
	{
		this.costCategory = costCategory;
	}

	public String getCaseNumber()
	{
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber)
	{
		this.caseNumber = caseNumber;
	}

	public CostKind getCostKindObj()
	{
		return costKindObj;
	}

	public void setCostKindObj(CostKind costKindObj)
	{
		this.costKindObj = costKindObj;
	}

	public AccountingCostKind getAccountingCostKind()
	{
		return accountingCostKind;
	}

	public void setAccountingCostKind(AccountingCostKind accountingCostKind)
	{
		this.accountingCostKind = accountingCostKind;
	}

	public static String[] getSettlementkinds()
	{
		return settlementKinds;
	}

	public static String[] getCurrencykinds()
	{
		return currencyKinds;
	}

	public Date getMtime()
	{
		return mtime;
	}

	public void setMtime(Date mtime)
	{
		this.mtime = mtime;
	}

	public String getEntryNumber()
	{
		return entryNumber;
	}

	public void setEntryNumber(String entryNumber)
	{
		this.entryNumber = entryNumber;
	}

	public BigDecimal getPlnCost()
	{

		if (SETTLEMENT_KIND_NETTO.equals(this.getSettlementKind()))
		{
			if (!currency.equals("PLN"))
				return net.setScale(4, RoundingMode.HALF_UP).multiply(rate).setScale(2, RoundingMode.HALF_UP);
			else
				return net.setScale(2, RoundingMode.HALF_UP);
		}
		else
		{
			if (!currency.equals("PLN"))
			{
				return gross.setScale(4, RoundingMode.HALF_UP).multiply(rate).setScale(2, RoundingMode.HALF_UP);
			}
				
			else
				return gross.setScale(2, RoundingMode.HALF_UP);
		}
	}

	public void setPlnCost(BigDecimal plnCost)
	{
		this.plnCost = plnCost;
	}

	public static ProjectEntry findByCostId(Long dicId) throws EdmException
	{
		try
		{
			Criteria criteria = DSApi.context().session().createCriteria(ProjectEntry.class);
			criteria.add(Restrictions.eq("costId", dicId));
			List<ProjectEntry> list = criteria.list();

			if (list.size() > 0)
			{
				return list.get(0);
			}
			else
				return null;
		}
		catch (HibernateException e)
		{
			log.error(e.getMessage(), e);
			throw new EdmHibernateException(e);
		}
	}

	public Long getCostId()
	{
		return costId;
	}

	public void setCostId(Long costId)
	{
		this.costId = costId;
	}

	public static List<ProjectEntry> findByDocNrProjectId(Long docId, String projectId) throws EdmException
	{
		try
		{
			Criteria criteria = DSApi.context().session().createCriteria(ProjectEntry.class);
			criteria.add(Restrictions.eq("docNr", docId.toString()))
			.add(Restrictions.eq("projectId", projectId));
			List<ProjectEntry> list = criteria.list();
			if (list.size() > 0)
			{
				return list;
			}
			return Collections.EMPTY_LIST;
		}
		catch (HibernateException e)
		{
			log.error(e.getMessage(), e);
			throw new EdmHibernateException(e);
		}
	}

}
