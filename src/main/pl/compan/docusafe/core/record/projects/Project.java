package pl.compan.docusafe.core.record.projects;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.record.projects.EditMainAction.KostKindCounter;

public class Project implements Lifecycle
{
	private Logger log = LoggerFactory.getLogger(Project.class);
	
	private static StringManager sm = GlobalPreferences.loadPropertiesFile(Project.class.getPackage().getName(), null);

	public static final String PROJECT_TYPE_STATUTOWE_NAUKOWE = "Statutowe-naukowe";
	public static final String PROJECT_TYPE_KO = "KO";
	public static final String PROJECT_TYPE_DWB = "DWB";
	public static final String PROJECT_TYPE_USLUGOWE = sm.getString("Uslugowe");
	public static final String PROJECT_TYPE_KONFEREWNCJE = "Konferencje";
	public static final String PROJECT_TYPE_NAUKOWE = "Naukowe";
	public static final String PROJECT_TYPE_INWESTYCYJNE = "Inwestycyjne";
	public static final String PROJECT_TYPE_KREDYT = "KO-Kredyt";
	public static final String[] projectTypes = new String[] { PROJECT_TYPE_STATUTOWE_NAUKOWE, PROJECT_TYPE_KO, PROJECT_TYPE_DWB, PROJECT_TYPE_USLUGOWE, PROJECT_TYPE_KONFEREWNCJE, PROJECT_TYPE_NAUKOWE, PROJECT_TYPE_INWESTYCYJNE, PROJECT_TYPE_KREDYT };

	public static List<String> projectTypesAsList()
	{
		return Arrays.asList(projectTypes);
	}

	private Long id;
	private String projectManager;
	private String projectName;
	private String nrIFPAN;
	private Date startDate;
	private Date finishDate;
	private List<ProjectEntry> entries;
	private String projectType;
	private String contractNumber;
	private String remarks;
	private String fullGross;
	private String defaultCurrency;
	private Set<Attachment> zalaczniki;
	private Float percent;
	private BigDecimal toSpend;
	boolean canRead = false;
	boolean canModify = false;
	private FinancingInstitutionKind financingInstitutionKind;
	private String applicationNumber;
	private Boolean clearance;
	private Boolean finalReport;
	private Boolean active;
	public Set<String> currencyExchangeMsgSet = new TreeSet<String>();
	private DSUser responsiblePerson;

	private Date mtime;

	public Project()
	{
	}

	public boolean isHasPermissionToRead()
	{
		try
		{
			String user = DSApi.context().getPrincipalName();
			for (DSDivision group : DSUser.findByUsername(user).getDivisionsAsList())
			{
				if (group.getGuid().equals(DSPermission.PROJECT_READ.getName()))
					canRead = true;
				if (group.getGuid().equals(DSPermission.PROJECT_READ.getName() + "_" + id))
					canRead = true;
				if (group.getGuid().equals(DSPermission.PROJECT_MODIFY.getName()))
					canRead = true;
				if (group.getGuid().equals(DSPermission.PROJECT_MODIFY.getName() + "_" + id))
					canRead = true;
			}
			return canRead;
		}
		catch (EdmException e)
		{
			log.error(e.getMessage());
			return false;
		}
	}

	public boolean isHasPermissionToModify()
	{
		try
		{
			DSApi.context().begin();
			String user = DSApi.context().getPrincipalName();
			for (DSDivision group : DSUser.findByUsername(user).getDivisionsAsList())
			{
				if (group.getGuid().equals(DSPermission.PROJECT_MODIFY.getName()))
					canModify = true;
				if (group.getGuid().equals(DSPermission.PROJECT_MODIFY.getName() + "_" + id))
					canModify = true;
			}
			DSApi.context().commit();
			return canModify;
		}
		catch (EdmException e)
		{
			log.error(e.getMessage());
			return false;
		}
	}

	public void addEntry(ProjectEntry entry) throws EdmException
	{
		entry.setProjectId(this.getId());
		List<ProjectEntry> rl = getEntries(null, false);
		rl.add(entry);
		DSApi.context().session().save(entry);
	}

	public void create() throws EdmException
	{
		try
		{
			this.mtime = new Date();
			DSApi.context().session().save(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public void delete() throws EdmException
	{
		try
		{
			DSApi.context().session().delete(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public static Project find(Long id) throws EdmException
	{
		Project project = (Project) DSApi.getObject(Project.class, id);

		if (project == null)
			throw new EntityNotFoundException(Project.class, id);

		return project;
	}

	public static List<Project> list(String order, boolean asc) throws EdmException
	{
		return list(order, asc, null, null);
	}

	public static List<Project> list(String order, boolean asc, String projectManager, String number) throws EdmException
	{
		Criteria c = DSApi.context().session().createCriteria(Project.class);
		if (StringUtils.isNotBlank(projectManager))
			c.add(Restrictions.eq("projectManager", projectManager));
		if (StringUtils.isNotBlank(number))
			c.add(Restrictions.like("nrIFPAN", "%" + number + "%"));
		if (StringUtils.isNotBlank(order))
			c.addOrder(asc == true ? Order.asc(order) : Order.desc(order));
		return c.list();

		// return
		// DSApi.context().session().createCriteria(Project.class).add()
		// .addOrder(asc == true ? Order.asc(order) : Order.desc(order))
		// .list();
	}

	public static List<Project> list() throws EdmException
	{
		return list(null, true, null, null);
	}

	public Long getId()
	{
		return id;
	}

	public String getContractNumber()
	{
		return contractNumber;
	}

	public void setContractNumber(String contractNumber)
	{
		this.contractNumber = contractNumber;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getFullGross()
	{
		return fullGross;
	}

	public void setFullGross(String fullGross)
	{
		this.fullGross = fullGross;
	}

	public String getDefaultCurrency()
	{
		return defaultCurrency;
	}

	public void setDefaultCurrency(String defaultCurrency)
	{
		this.defaultCurrency = defaultCurrency;
	}

	public static String[] getProjecttypes()
	{
		return projectTypes;
	}

	private void setId(Long id)
	{
		this.id = id;
	}

	public String getProjectManager()
	{
		return projectManager;
	}

	public void setProjectManager(String projectManager)
	{
		this.projectManager = projectManager;
	}

	public String getNrIFPAN()
	{
		return nrIFPAN;
	}

	public void setNrIFPAN(String nrIFPAN)
	{
		this.nrIFPAN = nrIFPAN;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getFinishDate()
	{
		return finishDate;
	}

	public void setFinishDate(Date finishDate)
	{
		this.finishDate = finishDate;
	}

	public String getProjectName()
	{
		return projectName;
	}

	public void setProjectName(String projectName)
	{
		this.projectName = projectName;
	}

	public List<ProjectEntry> getEntries(String sortField, boolean asc) throws EdmException
	{
		if (entries == null)
		{
			entries = ProjectEntry.getProjectEntry(getId(), sortField, asc);
		}
		return entries;
	}

	public void setEntries(List<ProjectEntry> entries)
	{
		this.entries = entries;
	}

	public boolean onDelete(Session arg0) throws CallbackException
	{
		// TODO Auto-generated method stub
		return false;
	}

	public void onLoad(Session arg0, Serializable arg1)
	{
		// TODO Auto-generated method stub

	}

	public boolean onSave(Session arg0) throws CallbackException
	{
		this.setMtime(new Date());
		return false;
	}

	public boolean onUpdate(Session arg0) throws CallbackException
	{
		this.setMtime(new Date());
		return false;
	}

	@Override
	public String toString()
	{
		return "Project [id=" + id + ", projectManager=" + projectManager + ", projectName=" + projectName + ", nrIFPAN=" + nrIFPAN + ", startDate=" + startDate + ", finishDate=" + finishDate + ", entries=" + entries + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (id == null)
		{
			if (other.id != null)
				return false;
		}
		else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getProjectType()
	{
		return projectType;
	}

	public void setProjectType(String projectType)
	{
		this.projectType = projectType;
	}

	public List<ProjectEntry> getEntries() throws EdmException
	{
		if (entries == null)
			entries = getEntries(null, false);
		return entries;
	}

	public Float getPercent()
	{
		return percent;
	}

	public void setPercent(Float percent)
	{
		this.percent = percent;
	}

	public Date getMtime()
	{
		return mtime;
	}

	public void setMtime(Date mtime)
	{
		this.mtime = mtime;
	}

	public BigDecimal getToSpend() throws EdmException
	{
		if (entries == null)
			entries = getEntries(null, false);

		String balanceCurr = "PLN";
		BigDecimal blocadeSummary = new BigDecimal(0);
		BigDecimal costSummary = new BigDecimal(0);
		BigDecimal impactSummary = new BigDecimal(0);

		for (ProjectEntry entry : entries)
		{
			if (entry.getEntryType().equals(ProjectEntry.BLOCADE))
			{
				blocadeSummary = blocadeSummary.add(entry.getBalanceAmount(balanceCurr,currencyExchangeMsgSet));
			}
			else if (entry.getEntryType().equals(ProjectEntry.COST))
			{
				costSummary = costSummary.add(entry.getBalanceAmount(balanceCurr,currencyExchangeMsgSet));
			}
			else if (entry.getEntryType().equals(ProjectEntry.IMPACT))
			{
				impactSummary = impactSummary.add(entry.getBalanceAmount(balanceCurr,currencyExchangeMsgSet));
			}
		}
		toSpend = impactSummary.subtract(costSummary).subtract(blocadeSummary);
		return toSpend;
	}

	public FinancingInstitutionKind getFinancingInstitutionKind()
	{
		return financingInstitutionKind;
	}

	public void setFinancingInstitutionKind(FinancingInstitutionKind financingInstitutionKind)
	{
		this.financingInstitutionKind = financingInstitutionKind;
	}

	public String getApplicationNumber()
	{
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber)
	{
		this.applicationNumber = applicationNumber;
	}

	public Boolean getClearance()
	{
		return clearance;
	}

	public void setClearance(Boolean clearance)
	{
		this.clearance = clearance;
	}

	public Boolean getFinalReport()
	{
		return finalReport;
	}

	public void setFinalReport(Boolean finalReport)
	{
		this.finalReport = finalReport;
	}

	public Set<Attachment> getZalaczniki()
	{
		return zalaczniki;
	}

	public void setZalaczniki(Set<Attachment> zalaczniki)
	{
		this.zalaczniki = zalaczniki;
	}

	public DSUser getResponsiblePerson() {
		return responsiblePerson;
	}

	public void setResponsiblePerson(DSUser responsiblePerson) {
		this.responsiblePerson = responsiblePerson;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
