package pl.compan.docusafe.core.crypto;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Obiekt typu factory odpowiedzialny za obsluge kryptografii
 * @author wkutyla
 * FIXME - interfejs wymaga testowania
 */
public class CryptographicFactory 
{
	static final Logger log = LoggerFactory.getLogger(CryptographicFactory.class);
	
	/**
	 * Metoda szyfruje strumien przy pomocy AESa. Pobiera obiekt InputStream i zwraca InputStream.
	 * Moze byc uzywana jako posrednik szyfrujacy dane ze strumienia.
	 * @param is strumien wejsciowy
	 * @param key klucz szyfrowania dlugosci 128, 192 lub 256 bitow
	 * @param encrypt
	 * @return strumien z zaszyfrowanymi danymi
	 * @throws DocusafeCryptoException
	 */
	public InputStream processStream(InputStream is, byte[] key, boolean encrypt, int blockSize) throws DocusafeCryptoException
	{
		
		if(key==null || key.length == 0)
			throw new EncryptionAccessException("BrakKlucza");
		
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		
		BlockCipher engine = new AESEngine();
		BufferedBlockCipher cipher = new BufferedBlockCipher(new CBCBlockCipher(engine));

		byte[] input = new byte[blockSize];
		try
		{
			cipher.init(encrypt, new KeyParameter(key));
			while(is.read(input)>0)
			{
				byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
				int outputLen = cipher.processBytes(input, 0, input.length, cipherText, 0);
				cipher.doFinal(cipherText, outputLen);
				os.write(cipherText);
			}
			is.close();
			InputStream ret = new ByteArrayInputStream(os.toByteArray());
			return ret;
		}
		catch (Exception e) 
		{
			throw new DocusafeCryptoException(e);
		}
	}
	
	public byte[] processBytes(byte[] in, byte[] key, boolean encrypt, int blockSize) throws DocusafeCryptoException
	{
		
		if(key==null || key.length == 0)
			throw new EncryptionAccessException("BrakKlucza");
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ByteArrayInputStream is = new ByteArrayInputStream(in);
		
		BlockCipher engine = new AESEngine();
		BufferedBlockCipher cipher = new BufferedBlockCipher(new CBCBlockCipher(engine));

		try
		{
			byte[] input = new byte[blockSize];
			
			cipher.init(encrypt, new KeyParameter(key));
			while(is.read(input)>0)
			{
				byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
				int outputLen = cipher.processBytes(input, 0, input.length, cipherText, 0);
				cipher.doFinal(cipherText, outputLen);
				os.write(cipherText);
			}
			is.close();
			return os.toByteArray();
			
		}
		catch (Exception e) 
		{
			throw new DocusafeCryptoException(e);
		}
	}
}
