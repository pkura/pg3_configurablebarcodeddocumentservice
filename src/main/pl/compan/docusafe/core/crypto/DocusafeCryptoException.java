package pl.compan.docusafe.core.crypto;

import pl.compan.docusafe.core.EdmException;

/**
 * Wyjatek dla uslug kryptograficznych Docusafe
 * @author wkutyla
 *
 */
public class DocusafeCryptoException extends EdmException 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DocusafeCryptoException(String message) 
	{
		super(message); 
	}
	
	public DocusafeCryptoException(String message,Throwable cause) 
	{
		super(message,cause); 
	}

	public DocusafeCryptoException(Throwable cause) 
	{
		super(cause); 
	}
	
}
