package pl.compan.docusafe.core.crypto;

import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

public class MainSystemKey extends DocusafeDatabaseKey
{
	
	public MainSystemKey()
	{
		this.status = NOWY;
		this.ctime = new Date();
	}
	
	public void createKey() throws DocusafeCryptoException
	{
		///
	}

	public boolean exisitsKeyForCode(String code) throws EdmException
	{
		List<DocusafeKeyStore> keys = DSApi.context().session().createCriteria(MainSystemKey.class)
		.add(Restrictions.eq("keyCode", code)).add(Restrictions.isNotNull("encryptedKey"))
		.list();
		return keys.size() > 0;
	}

	public List<DocusafeKeyStore> getActiveKeysForUserAndCode(String username, String code) throws EdmException
	{
		return DSApi.context().session().createCriteria(MainSystemKey.class)
		.add(Restrictions.eq("keyCode", code))
		.add(Restrictions.ne("status", WYGASLY)).list();
	}

	public List<DocusafeKeyStore> getAllGrantedKeys() throws EdmException
	{
		List<DocusafeKeyStore> keys = DSApi.context().session().createCriteria(MainSystemKey.class)
		.add(Restrictions.isNotNull("encryptedKey")).list();
		return keys;
	}

	public List<DocusafeKeyStore> getKeysForUser(String username) throws EdmException
	{
		return DSApi.context().session().createCriteria(MainSystemKey.class).list();
	}
	
	public byte[] getKey() throws DocusafeCryptoException
	{
		return Base64.decodeBase64(encryptedKey.getBytes());
	}

	public List<DocusafeKeyStore> getKeysToGrant() throws EdmException
	{
		return DSApi.context().session().createCriteria(MainSystemKey.class).list();
	}

	public void grantKey(DocusafeKeyStore toGrant) throws EdmException
	{
		status = AKTYWNY;
	}

	public void importKey(byte[] symetricKey) throws DocusafeCryptoException
	{
		try
		{
			this.encryptedKey = IOUtils.toString(Base64.encodeBase64(symetricKey));
			status = AKTYWNY;
		} 
		catch (Exception e)
		{
			throw new DocusafeCryptoException(e);
		}
		
	}

	public void retrieveKey() throws DocusafeCryptoException
	{
		//umyslnie pusto
	}

	public void setGrantedFrom(String grantedFrom)
	{
		//umyslnie pusto
		
	}

	public void setSecret(String secret)
	{
		//umyslnie pusto
	}


}
