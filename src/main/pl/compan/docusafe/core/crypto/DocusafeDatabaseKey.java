package pl.compan.docusafe.core.crypto;

import java.util.Date;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

public abstract class DocusafeDatabaseKey implements DocusafeKeyStore
{
	public static final String MAIN_SYSTEM_KEY_USERNAME = "#main.system.key#";
	public static final Integer NOWY = 1;
	public static final Integer AKTYWNY = 2;
	public static final Integer WYGASLY = 3;
	
	protected Long keyId;
	protected String keyCode;
	protected Integer status;
	protected String username;
	protected Date ctime;
	protected String encryptedKey;
	
	public static DocusafeKeyStore findKey(Long id) throws EdmException
	{
		return Finder.find(DocusafeDatabaseKey.class, id);
	}
	
	public DocusafeKeyStore find(Long id) throws EdmException
	{
		return Finder.find(DocusafeDatabaseKey.class, id);
	}
	
	public Date getCtime()
	{
		return ctime;
	}
	
	public String getKeyCode()
	{
		return keyCode;
	}
	
	public Long getKeyId()
	{
		return keyId;
	}
	
	public Integer getStatus()
	{
		return status;
	}
	
	public String getUsername()
	{
		return username;
	}
	
	public void setKeyCode(String code)
	{
		this.keyCode = code;	
	}
	
	public void setUsername(String username)
	{
		this.username = username;
		
	}
	
	
}
