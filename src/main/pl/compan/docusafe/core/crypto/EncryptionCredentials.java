package pl.compan.docusafe.core.crypto;

import java.io.InputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.StringManager;

public class EncryptionCredentials 
{
	private StringManager sm  = StringManager.getManager(EncryptionCredentials.class.getPackage().getName());
	private Logger log = LoggerFactory.getLogger(EncryptionCredentials.class);
	private Map<String, DocusafeKeyStore> mainKeys;
	private CryptographicFactory factory;
	private String owner;
	
	
	public EncryptionCredentials(String username)
	{
		owner = username;
		factory = new CryptographicFactory();
		mainKeys = new HashMap<String, DocusafeKeyStore>();
	}
	
	public InputStream encryptStream(InputStream is, String cn) throws DocusafeCryptoException
	{
		DocusafeKeyStore key = mainKeys.get(cn);
		if(key==null ||  key.getKey() == null)
			throw new DocusafeCryptoException(sm.getString("NieJestesAutoryzowanyDoOperacjiNaSzyfrowanychZalacznikachOKodzie", cn));
		return factory.processStream(is, key.getKey(), true, key.getKey().length);
	}
	
	public InputStream decryptStream(InputStream is, String cn) throws DocusafeCryptoException
	{
		DocusafeKeyStore key = mainKeys.get(cn);
		if(key==null)
			throw new DocusafeCryptoException(sm.getString("NieJestesAutoryzowanyDoOperacjiNaSzyfrowanychZalacznikachOKodzie", cn));
		return factory.processStream(is, key.getKey(), false, key.getKey().length);
	}
	
	public Collection<DocusafeKeyStore> getAcquiredCodes()
	{
		return mainKeys.values();
	}
	
	public Collection<DocusafeKeyStore> getAuthorizedCodes()
	{
		List<DocusafeKeyStore> ret = new ArrayList<DocusafeKeyStore>();
		for(DocusafeKeyStore dks:mainKeys.values())
		{
			try
			{
				if(dks.getKey()!=null) ret.add(dks);
			}
			catch (Exception e) 
			{
				
			}
		}
		return ret;
	}
	
	/**
	 * TODO napisac implementacje
	 * @param password
	 */
	public void authorize(String password, String codeName) throws EdmException
	{
		DocusafeKeyStore dks = mainKeys.get(codeName);
		if(dks==null)
			throw new EdmException(sm.getString("NieMaszPrzydzielonegoDostepuDoKodu",codeName));
		dks.setSecret(password);
		dks.retrieveKey();
	}
	
	public void requestPassword(String password, String codeName) throws EdmException
	{
		try
		{
			DSApi.context().begin();
			DocusafeKeyStore irk = DSApi.getKeyStoreInstance(codeName);
			if(irk.getActiveKeysForUserAndCode(this.owner, codeName).size()>0)
				throw new EdmException(sm.getString("PosiadaszJuzDostepDlaTegoKodu"));
			irk.setSecret(password);
			irk.setUsername(owner);
			irk.setKeyCode(codeName);
			irk.createKey();

			DSApi.context().session().save(irk);
			DSApi.context().commit();
		}
		catch (Exception e) 
		{
			
			DSApi.context().setRollbackOnly();
			log.error(e.getMessage(),e);
			throw new DocusafeCryptoException(e);
			// TODO: handle exception
		}
	}

	public CryptographicFactory getFactory() {
		return factory;
	}

	public void setFactory(CryptographicFactory factory) {
		this.factory = factory;
	}
	
	public void init()
	{
		try
		{
			Collection<DocusafeKeyStore> keys = new HashSet<DocusafeKeyStore>();
			for(DocusafeKeyStore dks:DSApi.getAvailableKeyStoresInstances())
			{
				keys.addAll(dks.getKeysForUser(this.owner));
			}
			for(DocusafeKeyStore dks: keys)
			{
				mainKeys.put(dks.getKeyCode(), dks);
			}
		}
		catch (EdmException e) 
		{
			log.error(e.getMessage(),e);
		}
	}
	
	public String grantKey(DocusafeKeyStore toGrant, String code) throws EdmException
	{
		if(!DSApi.context().hasPermission(DSPermission.CRYPTO_ADMINS))
		{
			throw new EdmException(sm.getString("NieMaszUprawnienDoZarzadzaniaKluczamiKryptograficznymi"));
		}
		DocusafeKeyStore own = mainKeys.get(code);
		//System.out.println(own);
		//System.out.println(own.exisitsKeyForCode(own.getKeyCode()));
		if(own != null && !own.exisitsKeyForCode(own.getKeyCode()))
		{
			
			try
			{
				String seed = System.currentTimeMillis() + "NieMaszUprawnienDoZarzadzaniaKluczamiKryptograficznymi";
				MessageDigest md = MessageDigest.getInstance("SHA-256");
				md.update(seed.getBytes());
				byte[] key = md.digest();
				own = DSApi.getKeyStoreInstance(code).find(own.getKeyId());
				own.importKey(key);
				own.setGrantedFrom(own.getUsername());
				DSApi.context().session().update(own);
				return IOUtils.toString(Base64.encodeBase64(key));
			}
			catch (Exception e) 
			{
				throw new EdmException(e);
			}
			
		}
		if(own == null || own.getKey() == null)
			throw new EdmException(sm.getString("BrakKlucza"));
		else own.grantKey(toGrant);
		
		return null;
	}
	
	public byte[] getKey(String cn) throws DocusafeCryptoException
	{
		if(mainKeys.containsKey(cn))
			return mainKeys.get(cn).getKey();
		else return null;
	}
	public void addDocusafeKeyStore(DocusafeKeyStore dks)
	{
		 mainKeys.put(dks.getKeyCode(), dks);  
	}
	
	
}