package pl.compan.docusafe.core.crypto;

/**
 * Interfejs reprezentuje klucz kryptograficzny symetryczny
 * sluzacy do szyfrowania i deszyfrowania zalacznikow 
 * 
 * @author wkutyla
 *
 */
public interface EncryptionKey 
{
	/**
	 * Kod klucza
	 * @return
	 */
	public String getKeyCode();
	
	/**
	 * Zwraca klucz 
	 * @return
	 */
	public byte[] getKey() throws KeyAccessException;
}
