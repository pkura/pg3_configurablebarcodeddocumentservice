package pl.compan.docusafe.core.crypto;


public class EncryptionAccessException extends DocusafeCryptoException
{
	private static final long serialVersionUID = 1L;

	public EncryptionAccessException(String message) 
	{
		super(message);
	}

}
