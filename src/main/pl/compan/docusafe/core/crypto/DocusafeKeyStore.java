package pl.compan.docusafe.core.crypto;

import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.EdmException;

/**
 *	Interfejs magazynu kluczy
 */
public interface DocusafeKeyStore
{
	
	public Long getKeyId();

	/**
	 * Inicjowanie klucza uzytkownika
	 * @throws Exception
	 */
	public void createKey() throws DocusafeCryptoException;
	
	/**
	 * Metoda koduje klucz symetryczny
	 * @param symetricKey
	 * @throws KeyAccessException
	 */
	public void importKey(byte[] symetricKey) throws DocusafeCryptoException;	
	/**
	 * Metoda abstrakcyjna - odzyskuje klucz ktory nie lezy jawnie w repozytorium
	 * @return
	 * @throws KeyAccessException
	 */
	public void retrieveKey() throws DocusafeCryptoException;
	
	
	public void setUsername(String username);
	
	public String getUsername();
	
	public void setKeyCode(String code);
	
	public String getKeyCode();
	
	public Date getCtime();
		
	public byte[] getKey() throws DocusafeCryptoException;

	public void setSecret(String secret);
	
	public void setGrantedFrom(String grantedFrom);
	
	public Integer getStatus();
	
	public List<DocusafeKeyStore> getKeysForUser(String username) throws EdmException;
	
	public List<DocusafeKeyStore> getActiveKeysForUserAndCode(String username, String code) throws EdmException;
	
	public List<DocusafeKeyStore> getKeysToGrant() throws EdmException;
	
	public List<DocusafeKeyStore> getAllGrantedKeys() throws EdmException;
	
	public boolean exisitsKeyForCode(String code) throws EdmException;
	
	public DocusafeKeyStore find(Long id) throws EdmException;
	
	public void grantKey(DocusafeKeyStore toGrant) throws EdmException;
	
	
}
