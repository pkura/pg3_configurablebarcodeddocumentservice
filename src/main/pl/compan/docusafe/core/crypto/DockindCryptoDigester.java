package pl.compan.docusafe.core.crypto;

import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.DateUtils;
import edu.emory.mathcs.backport.java.util.Arrays;

public class DockindCryptoDigester
{
	private static DockindCryptoDigester instance;

	private Logger log = LoggerFactory.getLogger(DockindCryptoDigester.class);
	private final String salt = "ab1$%(uhf1c76&C*(ju423o";
	private final Integer refreshPeriod = 8;
	private HashMap<String, Entry> keys;

	private static String[] verifiableDockinds = {"invoice_ic", "ald", "rockwell","eservice","dzk","pika","plg","plgout"};


	private DockindCryptoDigester()
	{
		keys = new HashMap<String, Entry>();
	}

	public boolean isDockindAvailable(String cn)
	{
		try
		{
			if(cn==null)
				return true;

			if(isVerifyable(cn))
			{
				Boolean testowy = AvailabilityManager.isAvailable("systemTestowy");
				Boolean available = isDockindAvailable(cn, testowy,new Date());
				if(!available && testowy)
					available = isDockindAvailable(cn, false,new Date());
				return available;
			}
			else
				return true;
		}
		catch(Exception e)
		{
			log.error("Cos nie bangla", e);
			return true;
		}
	}
	/**
	 * Sprawdza do kiedy jest wgrana licencaj na dockind, jesli dockind nie wymaga licencji zwraca plus 100 lat
	 * @param cn
	 * @return
	 * @throws EdmException
	 */
	public Date getDockindAvailabilityToDate(String cn) throws EdmException
	{
		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
		if(!isVerifyable(cn))
		{
			cal.add(Calendar.YEAR, 100);
			return  cal.getTime();
		}
		cal.add(Calendar.MONTH, -12);
        Date checkDate = cal.getTime();	
        Date availableDate = cal.getTime();	
        int i = 0;
		while(true)
		{
			checkDate = cal.getTime();
			boolean av = isDockindAvailable(cn, false,checkDate);
			if(av)
			{
				availableDate = cal.getTime();
			}

			if(!av && checkDate.after(new Date()))
				break;
			cal.add(Calendar.MONTH, 1);		
	        if(i > 500)
	        	break;
	        i++;
		}
		return availableDate;
	}


	private boolean isDockindAvailable(String cn, Boolean testowy,Date date) throws EdmException
	{
		try
		{
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			String toDigest = cn + salt + DateUtils.yearMonth_Format.format(date) + testowy;
			md.update(toDigest.getBytes());
			String result = IOUtils.toString(Base64.encodeBase64(md.digest()));

			if(keys.containsKey(result))
			{
				Entry e = keys.get(result);
				long diff = e.checkTime.getTime() - new Date().getTime();
				double hours = DateUtils.hours(diff);
				if(hours < refreshPeriod)
				{
					return true;
				}
				else
				{
					keys.remove(result);
				}
			}
			List<IndividualRepositoryKey> list = DSApi.context().session().createCriteria(IndividualRepositoryKey.class).
				add(Restrictions.eq("publicKey", result)).add(Restrictions.eq("username", "docusafe")).list();

			if(list.size()<1)
			{
				return false;
			}
			else
			{
				Entry e = new Entry(result, new Date());
				keys.put(result, e);
				return true;
			}
		}
		catch(Exception e)
		{
			throw new EdmException(e);
		}
	}

	public static DockindCryptoDigester getInstance()
	{
		if(instance==null)
			instance = new DockindCryptoDigester();
		return instance;
	}

	public boolean isVerifyable(String cn)
	{
		if(Arrays.asList(verifiableDockinds).contains(cn))
			return true;
		else
			return false;
	}

	private class Entry
	{
		private String code;
		private Date checkTime;

		public Entry(String code, Date checkTime)
		{
			this.code = code;
			this.checkTime = checkTime;
		}

		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public Date getCheckTime() {
			return checkTime;
		}
		public void setCheckTime(Date checkTime) {
			this.checkTime = checkTime;
		}
	}
}
