package pl.compan.docusafe.core.crypto;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

/**
 * Klasa reprezentujaca klucz kryptograficzny 
 *
 */
public class IndividualRepositoryKey extends DocusafeDatabaseKey
{
	static final Logger log = LoggerFactory.getLogger(IndividualRepositoryKey.class);
	
	private static Integer blockSize = 256;
	private static Integer keyLength = blockSize/8;

	private transient byte[] key;
	private transient String secret;
	private transient byte[] explicitPrivateKey;

	private String grantedFrom; 
	
	private CryptographicFactory factory = new CryptographicFactory();
	

	/**
	 * Klucz prywatny uzytkownika zaszyfrowany sekretem w base64
	 */
	private String privateKey;
	
	/**
	 * Klucz publiczny uzytkownika w base64
	 */
	private String publicKey = null;
	

	
	protected String getSecretSalt()
	{
		return "cjepoijfcdefckw-32dd2";
	}
		
	public IndividualRepositoryKey()
	{
		this.status = NOWY;
		this.ctime = new Date();
	}
	
	public void createKey() throws DocusafeCryptoException
	{	
		log.trace("inicjujemy klucz asymetryczny");
		try
		{
			if (secret==null || secret.trim().length()==0)
			{
				throw new KeyAccessException("nie podano sekretu");
			}		
			
			KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
			generator.initialize(1024);
			KeyPair keyPair = generator.generateKeyPair();
			publicKey =	new String(Base64.encodeBase64(keyPair.getPublic().getEncoded()));
            
			byte[] enc = factory.processBytes(keyPair.getPrivate().getEncoded(), getFullTemporaryKeyFromSecret(), true, blockSize);
			privateKey = new String(Base64.encodeBase64(enc));
			
		}		
		catch (NoSuchAlgorithmException e) 
		{
			throw new KeyAccessException("Blad w trakcie inicjowania klucza",e);			
		} 
		catch (Exception e) 
		{
			throw new KeyAccessException("Blad w trakcie inicjowania klucza",e);
		}		
		
	}
	
	public void importKey(byte[] symetricKey) throws KeyAccessException
	{
		try
		{
			KeyFactory rsaKeyFac = KeyFactory.getInstance("RSA");  
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.decodeBase64(publicKey.getBytes()));  
			RSAPublicKey pubKey = (RSAPublicKey )rsaKeyFac.generatePublic(keySpec);
			Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            encryptedKey = IOUtils.toString(Base64.encodeBase64(cipher.doFinal(symetricKey)));        
            status = AKTYWNY;
		} 
		catch (Exception e) 
		{
			throw new KeyAccessException(e.getMessage(),e);
		}
		
	}
	
	public void retrieveKey() throws KeyAccessException
	{
		try
		{
			explicitPrivateKey = factory.processBytes(Base64.decodeBase64(privateKey.getBytes()), getFullTemporaryKeyFromSecret(), false, blockSize);
			KeyFactory rsaKeyFac = KeyFactory.getInstance("RSA");  
			PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(explicitPrivateKey);  
			RSAPrivateKey privKey = (RSAPrivateKey)rsaKeyFac.generatePrivate(encodedKeySpec);  
			Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privKey);
            key = cipher.doFinal(Base64.decodeBase64(encryptedKey.getBytes())); 
		} 
		catch (Exception e) 
		{
			throw new KeyAccessException(e.getMessage(),e);
		}		
	}

	public void grantKey(DocusafeKeyStore toGrant) throws EdmException 
	{
		toGrant.setGrantedFrom(this.username);
		toGrant.importKey(this.key);
	}
	
	private byte[] getFullTemporaryKeyFromSecret() throws NoSuchAlgorithmException
	{
		String userkey = secret + getSecretSalt();
		while(userkey.length() < keyLength)
		{
			userkey = userkey + "#";
		}
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(userkey.getBytes());
		return md.digest();
	}
	
	public Long getKeyId() {
		return keyId;
	}

	public void setKeyId(Long keyId) {
		this.keyId = keyId;
	}

	public byte[] getKey() {
		return key;
	}

	public void setKey(byte[] key) {
		this.key = key;
	}

	public String getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEncryptedKey() {
		return encryptedKey;
	}

	public void setEncryptedKey(String encryptedKey) {
		this.encryptedKey = encryptedKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}
	
	public Date getCtime() {
		return ctime;
	}

	public void setGrantedFrom(String grantedFrom)
	{
		this.grantedFrom = grantedFrom;
	}
	
	public List<DocusafeKeyStore> getKeysToGrant() throws EdmException 
	{
		return DSApi.context().session().createCriteria(IndividualRepositoryKey.class)
		.add(Restrictions.eq("status", NOWY)).list();
	}

	public List<DocusafeKeyStore> getKeysForUser(String username) throws EdmException
	{
		return DSApi.context().session().createCriteria(IndividualRepositoryKey.class)
			.add(Restrictions.eq("username", username)).list();
	}
	public DocusafeKeyStore find(Long id) throws EdmException
	{
		return Finder.find(IndividualRepositoryKey.class, id);
	}

	public List<DocusafeKeyStore> getActiveKeysForUserAndCode(String username, String code) throws EdmException 
	{
		return DSApi.context().session().createCriteria(IndividualRepositoryKey.class)
		.add(Restrictions.eq("username", username)).add(Restrictions.eq("keyCode", code))
		.add(Restrictions.ne("status", WYGASLY)).list();
	}
	
	public boolean exisitsKeyForCode(String code) throws EdmException 
	{
		List<DocusafeKeyStore> keys = DSApi.context().session().createCriteria(IndividualRepositoryKey.class)
		.add(Restrictions.eq("keyCode", code)).add(Restrictions.isNotNull("encryptedKey"))
		.list();
		return keys.size() > 0;
	}

	public List<DocusafeKeyStore> getAllGrantedKeys() throws EdmException
	{
		List<DocusafeKeyStore> keys = DSApi.context().session().createCriteria(IndividualRepositoryKey.class)
		.add(Restrictions.isNotNull("encryptedKey")).list();
		return keys;
	}

	
}
