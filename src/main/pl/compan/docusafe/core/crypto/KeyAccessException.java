package pl.compan.docusafe.core.crypto;

/**
 * Wyjatek rzucany w sytuacji gdy uzytkownik nie ma dostepu dla klucza
 * @author wkutyla
 *
 */
public class KeyAccessException extends DocusafeCryptoException 
{
	private static final long serialVersionUID = 1L;

	public KeyAccessException(String message) 
	{
		super(message);
	}

	public KeyAccessException(String message,Throwable cause) 
	{
		super(message,cause);
	}
	
	public KeyAccessException(Throwable cause) 
	{
		super(cause);
	}
}
