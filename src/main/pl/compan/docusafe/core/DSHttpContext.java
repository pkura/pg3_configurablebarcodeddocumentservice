package pl.compan.docusafe.core;

import com.opensymphony.xwork.ActionContext;

/**
 * Klasa wspiera pobieranie i ustawianie zmiennych w sesji http;
 * Zwraca DSHttpContext, kt�ry zajmuje si� obs�ug�
 * element�w zwiazanych z zadaniami ActionContext.getContext()
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class DSHttpContext
{
    
    public static ActionContext getContext()
    {
      return ActionContext.getContext();   
    }
    /**
     * Ustawia zmienna flashow� w tablicy sesji. Nazwa Flashowa gdy� po pobraniu powinno byc usuwane
     * @param name
     * @param obj
     */
    public static void setHttpFlashAttribute(String name, Object obj)
    {
        getContext().getSession().put(name, obj);
    }

    /**
     * Pobiera zmienna flashow� z tablicy sesji.Zmienna jest odrazu usuwana. 
     * Nazwa Flashowa gdy� po pobraniu powinno byc usuwane
     * @param name 
     */
    public static Object getHttpFlashAttribute(String name)
    {
        Object obj = null;

        if(getContext().getSession().containsKey(name))
        {
            obj = getContext().getSession().get(name);
            getContext().getSession().remove(name);
        }

        return obj;
    }  
}
