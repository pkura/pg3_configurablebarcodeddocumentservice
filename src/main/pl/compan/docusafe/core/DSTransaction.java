package pl.compan.docusafe.core;

import com.google.common.base.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.web.filter.AuthFilter;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>Wrapper transakcji z <code>DSApi</code>. Patrz javadoc metody <code>execute</code>.</p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class DSTransaction {

    private static final Logger log = LoggerFactory.getLogger(DSTransaction.class);

    public DSTransaction() {
    }

    private static DSTransaction instance;

    public static DSTransaction instance() {
        if(instance == null) {
            instance = new DSTransaction();
        }
        return instance;
    }

    protected String getErrorMessage(Exception ex) {
        final String message = ex.getMessage();

        return message == null ? "B��d" : message;
    }

    protected void handleError(String myMessage, Exception ex) {
        log.error(myMessage, ex);
    }

    /**
     * <p>Wrapper na transakcje <code>DSApi</code></p>
     *
     * <p>Uruchamia <code>fn.apply</code> w kontek�cie transakcji:
     * <ul>
     *     <li>begin transaction</li>
     *     <li><code>fn.apply()</code></li>
     *     <li>commit transaction / rollback transaction</li>
     * </ul>
     * </p>
     *
     * <p>Po zako�czeniu transakcji pozytywnie (<code>commit</code>) wywo�ywana
     * jest metoda <code>fn.success()</code>.</p>
     *
     * <p>Je�li pojawi� si� jeden lub wi�cej wyj�tk�w <code>ex1, ex2, ...</code> nast�puje rollback
     * transakcji i wywo�ywana jest metoda <code>fn.failure(ex1, ex2, ...)</code></p>
     *
     * <p>Je�li po wywo�aniu <code>fn.apply()</code> spe�nione jest
     * <code>fn.isRollback() == true</code> to transakcja jest rollbackowana.
     * <code>fn.failure()</code> NIE JEST wywo�ywane w tej sytuacji.</p>
     *
     * <p>
     *     Przyk�ad:
     *
     *     <pre>
     *     {@code
     *
     *    new Transaction().execute(new Function<Optional> {
     *        &#64;Override
     *        public Optional apply() {
     *            // kod w transakcji: updaty, inserty, delety
     *        }
     *
     *        // implementacja tej metody jest opcjonalna
     *        &#64;Override
     *        public void succes() {
     *            // kod uruchomiony po commicie transakcji
     *        }
     *
     *        // implementacja tej metody jest opcjonalna
     *        &#64;Override
     *        public void failure(Exception... ex) {
     *            // kod uruchamiany gdy by�y jakie� wyj�tki
     *            // raczej nie powinno by� wi�cej ni� dw�ch wyj�tk�w,
     *            // a w 99% przypadk�w b�dzie to jeden wyj�tek
     *            // - w tej chwili drugi wyj�tek jest dodawany,
     *            // gdy jest problem z rollbackiem transakcji.
     *        }
     *    });
     *
     *      }
     *      </pre>
     * </p>
     *
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     * @param propagateErrors czy rzuca wyj�tkiem
     * @param fn
     * @return Optional result of fn.apply()
     */
    public <T> Optional<T> execute(boolean propagateErrors, Function<T> fn) throws Exception {
        DSContext context = DSApi.context();
        boolean inPrevTransaction = context.inTransaction();
        try {
            if(inPrevTransaction) {
                log.warn("[execute] context already in transaction, re-using existing transaction");
            } else {
                context.begin();
            }
        } catch (Exception e) {
            handleError("[Transaction] cannot begin", e);
            return Optional.absent();
        }

        try {
            final T ret = fn.apply();

            if(! fn.isRollback())
            {
                if(inPrevTransaction) {
                    log.warn("[execute] context was already in transaction, not commiting");
                } else {
                    DSApi.context().commit();
                }

                fn.success();

                return Optional.fromNullable(ret);
            }
            else
            {
                log.error("[Transaction] rollback");

                try {
                    if(inPrevTransaction) {
                        log.warn("[execute] context was salready in transaction, setting rollbackOnly");
                        DSApi.context().setRollbackOnly();
                    } else {
                        DSApi.context().rollback();
                    }
                } catch (EdmException e) {
                    handleError("[Transaction] rollback failed", e);
                    fn.failure(e);

                    if(propagateErrors) {
                        throw e;
                    }
                }

                return Optional.absent();
            }

        } catch (Exception e) {
            handleError("[Transaction] error", e);

            try {
                if(inPrevTransaction) {
                    log.warn("[execute] context was salready in transaction, setting rollbackOnly");
                    DSApi.context().setRollbackOnly();
                } else {
                    DSApi.context().rollback();
                }

                fn.failure(e);
            } catch (EdmException e1) {
                handleError("[Transaction] error", e1);
                fn.failure(e, e1);

                // nie propagujemy tutaj bledu,
                // bo wazniejsze jest, zeby glowny
                // blad dotarl do clienta
            }

            if(propagateErrors) {
                throw e;
            }

            return Optional.absent();
        }
    }

    public <T> Optional<T> executePropagateErrors(Function<T> fn) throws Exception {
        return execute(true, fn);
    }

    public <T> Optional<T> execute(Function<T> fn) {
        try {
            return execute(false, fn);
        } catch (Exception e) {
            // execute(false, ...) -> nie ma exceptionow
            log.error("[execute] this should not be here (all exceptions should have been caught)", e);
            return Optional.absent();
        }
    }

    public <T> Optional<T> executeOpenAdmin(Function<T> fn) {
        return executeOpenAdmin(true, fn);
    }

    public <T> Optional<T> executeOpenAdminPropagateErrors(Function<T> fn) {
        return executeOpenAdmin(false, fn);
    }

    public <T> Optional<T> executeOpenAdmin(boolean propagateError, Function<T> fn) {
        Subject adminSubject = DSApi.getAdminSubject();
        return executeOpenContext(adminSubject, propagateError, fn);
    }

    public <T> Optional<T> executeOpenContext(HttpServletRequest request, Function<T> fn) {
        Subject subject = (Subject) request.getSession().getAttribute(AuthFilter.SUBJECT_KEY);
        return executeOpenContext(subject, false, fn);
    }

    public <T> Optional<T> executeOpenContext(Subject subject, Function<T> fn) {
        return executeOpenContext(subject, false, fn);
    }

    public <T> Optional<T> executeOpenContext(Subject subject, boolean propagateError, Function<T> fn) {
        boolean contextWasOpened = false;
        try {
            if(subject == null) {
                throw new AccessDeniedException("Niezalogowano");
            }

            if(DSApi.isContextOpen()) {
                log.warn("[executeOpenContext] Context is already opened");
                contextWasOpened = true;
            } else {
                DSApi.open(subject);
            }
            return execute(propagateError, fn);
        } catch (Exception e) {
            handleError("[executeOpenContext] open context failed", e);
            fn.failure(e);

            if(propagateError) {
                throw new RuntimeException(e);
            } else {
                return Optional.absent();
            }

        } finally {
            if(! contextWasOpened) {
                DSApi._close();
            } else {
                log.warn("[executeOpenContext] context was opened, not closing");
            }
        }
    }

    static abstract public class Function<T> {
        public boolean rollback;

        abstract public T apply() throws Exception;

        public void success() {};
        public void failure(Exception... ex) {};

        public void save(Object t) throws EdmException {
            DSApi.context().session().save(t);
        }

        public boolean isRollback() {
            return rollback;
        }
    }

}

