package pl.compan.docusafe.core.drools;

import com.google.common.base.Preconditions;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderConfiguration;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderFactory;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DroolsButtonField;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Arrays;
import java.util.Properties;

/**
 * Klasa narz�dziowa do u�atwienia interakcji Drools <-> Docusafe
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class DroolsUtils {
    private final static Logger LOG = LoggerFactory.getLogger(DroolsUtils.class);

    /**
     * Wywo�uje regu�e pod DroolsButton na danym dokumencie
     * (dockind tego dokumentu musi mie� dany przycisk)
     *
     * Metoda musi by� wywo�ana w rozpocz�tej transakcji
     * 
     * @param documentId id dokumentu
     * @param fieldCn cn przycisku
     */
    public static void invokeDroolsButton(long documentId, String fieldCn) throws EdmException {
        DroolsButtonField field
                = (DroolsButtonField) Documents.document(documentId)
                    .getDocument().getFieldsManager().getField(fieldCn);
        field.apply(Documents.document(documentId));
    }

    private DroolsUtils(){}

    /**
     * Tworzy KnowledgeBuilder zgodny z Tomcatem
     * @return
     */
    public static KnowledgeBuilder createKnowledgeBuilder() {
        //http://community.jboss.org/wiki/RulesTomcat + java doc KnowledgeBuilderConfiguration
        Properties props = new Properties();
        props.setProperty("drools.dialect.java.compiler", "JANINO");
        props.setProperty("drools.dialect.mvel.strict", "false");
        KnowledgeBuilderConfiguration kbc = KnowledgeBuilderFactory.newKnowledgeBuilderConfiguration(props, null);
        return KnowledgeBuilderFactory.newKnowledgeBuilder(kbc);
    }

    public static void reportErrors(KnowledgeBuilder kb){
        Preconditions.checkArgument(kb.hasErrors(), "no errors!");
        LOG.error("Drools compilation error(s):");
        for(KnowledgeBuilderError error: kb.getErrors()){
            LOG.error("[" + Arrays.toString(error.getErrorLines()) + "] " + error.getMessage());
        }
    }
}
