package pl.compan.docusafe.core;

import com.opensymphony.webwork.ServletActionContext;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.*;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.Property;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.OfficePointPrefix;
import pl.compan.docusafe.core.certificates.auth.DSHostPublicKey;
import pl.compan.docusafe.core.certificates.auth.DSPublicKey;
import pl.compan.docusafe.core.certificates.auth.DSUserPublicKey;
import pl.compan.docusafe.core.certificates.auth.PublicKeyType;
import pl.compan.docusafe.core.crypto.DocusafeKeyStore;
import pl.compan.docusafe.core.crypto.EncryptionCredentials;
import pl.compan.docusafe.core.crypto.IndividualRepositoryKey;
import pl.compan.docusafe.core.crypto.MainSystemKey;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.RolePrincipal;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.archive.repository.search.SearchFilter;

import javax.security.auth.Subject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.security.Principal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;


/**
 * Punkt wej?ciowy do komunikacji z baz? danych.
 *
 * @see DSContext
 * @see pl.compan.docusafe.webwork.event.OpenHibernateSession
 * @see pl.compan.docusafe.webwork.event.CloseHibernateSession
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DSApi.java,v 1.174 2010/07/05 14:26:40 pecet1 Exp $
 */
public final class DSApi
{
    private static final Logger log = LoggerFactory.getLogger(DSApi.class);
    private static StringManager sm = StringManager.getManager(Constants.Package);

    private static final ThreadLocal<ThreadContext> contexts = new ThreadLocal<ThreadContext>();

    private static Collection<DocusafeKeyStore> docusafeKeyStoreInstances;

    /**
     * Aktualna konfiguracja Hibernate.
     */
    private static Configuration configuration = new Configuration();
    /**
     * Flaga okre?laj?ca, czy Hibernate zosta?o skonfigurowane.
     */
    private static boolean hibernateInitialized;
    private static org.hibernate.SessionFactory sessionFactory;
    private static org.hibernate.dialect.Dialect dialect;
    /**
     * Najmniejsza data mo?liwa do umieszczenia w bazie danych.  Odpowiada
     * limitowi dla SQL Server.
     */
    private static final Date MIN_DB_DATE;
    /**
     * Najwi?ksza data mo?liwa do umieszczenia w bazie danych.  Odpowiada
     * limitowi dla SQL Server.
     */
    private static final Date MAX_DB_DATE;
    private static final Map<String, EncryptionCredentials> credentialsMap = new HashMap<String, EncryptionCredentials>();
    static
    {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(Calendar.YEAR, 1753);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, 1);

        MIN_DB_DATE = cal.getTime();

        cal.clear();
        cal.set(Calendar.YEAR, 9999);
        cal.set(Calendar.DAY_OF_MONTH, 12);
        cal.set(Calendar.MONTH, 31);

        MAX_DB_DATE = cal.getTime();

        initDocusafeKeyStoreInstances();
    }

    /**
     * @developers Wymagane dodac klas? tutaj jesli uzywa sie 'javax.persistance.addnotation';
     * Nale?y zresetowa? serwer przy dodawaniu.
     *
     * @param hibernateConfiguration
     */
	private static void addAnnotatedClasses(Configuration hibernateConfiguration)  {
		hibernateConfiguration
            .addAnnotatedClass(pl.compan.docusafe.core.dockinds.process.ProcessActionEntry.class)
            .addAnnotatedClass(pl.compan.docusafe.core.dockinds.dictionary.CustomsAgency.class)
		    .addAnnotatedClass(pl.compan.docusafe.core.dockinds.dictionary.FixedAssetsGroup.class)
		    .addAnnotatedClass(pl.compan.docusafe.core.dockinds.dictionary.FixedAssetsSubgroup.class)
		    .addAnnotatedClass(pl.compan.docusafe.core.dockinds.dictionary.InsuranceParticipant.class)
            .addAnnotatedClass(pl.compan.docusafe.core.certificates.ElectronicSignature.class)
            .addAnnotatedClass(pl.compan.docusafe.core.certificates.ElectronicSignature.Key.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeCard.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ic.VatEntry.class)
            .addAnnotatedClass(pl.compan.docusafe.core.dockinds.dictionary.VatRate.class)
            .addAnnotatedClass(pl.compan.docusafe.core.dockinds.dictionary.AccountNumberComment.class)
            .addAnnotatedClass(pl.compan.docusafe.core.ima.dictionary.DoccaseDictionary.class)
            .addAnnotatedClass(pl.compan.docusafe.core.ima.dictionary.DocincaseDictionary.class)
            .addAnnotatedClass(pl.compan.docusafe.core.ima.dictionary.Doccase.class)
            .addAnnotatedClass(pl.compan.docusafe.core.ima.dictionary.CaseStatus.class)
            .addAnnotatedClass(pl.compan.docusafe.core.ima.dictionary.Docincase.class)
            .addAnnotatedClass(pl.compan.docusafe.core.ima.dictionary.Mediator.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.opzz.dictionary.OrganizationUserDictionary.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.opzz.dictionary.OpzzParam.class)
            .addAnnotatedClass(pl.compan.docusafe.core.dockinds.acceptances.AcceptanceModeRule.class)
            .addAnnotatedClass(pl.compan.docusafe.core.sso.Iis.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.pika.PikaExternalUser.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.plg.PlgNadawca.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.nfos.NfosSystem.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.nfos.NfosExport.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.nfos.LotusEmail.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.nfos.LotusAttachment.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.nfos.UpoLink.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.nfos.XSLWzory.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.nfos.LotusRecipient.class)
            .addAnnotatedClass(SearchFilter.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.utp.UTPLink.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.utp.internalSignature.InternalSignatureEntity.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.utp.internalSignature.InternalToDocumentEntity.class)
            .addAnnotatedClass(pl.compan.docusafe.core.jackrabbit.JackrabbitServer.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ins.entities.PozycjaFaktury.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ins.entities.PozycjaZapotrzebowania.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ins.entities.PozycjaBudzetowaFaktury.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.archiwum.UdostepnionyZasob.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.archiwum.PublicDocument.class)
            .addAnnotatedClass(pl.compan.docusafe.core.office.AccessToDocument.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.archiwum.reports.JasperReportsKind.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.FakturaDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.BudgetViewDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.BudzetPozycjaDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.FakturaPozycjaDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.RealizacjaBudzetuDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.CurrencyDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.PaymentTermsDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.FakturaExportStatusDB.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.imgwwaw.hbm.IMGWContractor.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.imgwwaw.hbm.IMGWAssetCardInfo.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.AMSSupplier.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.AMSProduct.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.FinancialDocument.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.SupplierOrderPositionItem.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.AMSEmployee.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.PaymentTerms.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.PatternsOfCostSharing.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.VatRatio.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.MPKDictionaryItem.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.DekretyDictionaryItem.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.RequestForReservation.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.ReservationPositionDictionaryItem.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.AmsAssetCardInfo.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.AmsAssetLiquidationReason.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.AmsAssetOperationType.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.AmsAssetRecordSystemTypeForOperationType.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.AMSDepartment.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.UnitBudgetKo.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.UnitBudget.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.PurchasePlan.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ams.hb.BudgetUnitOrganizationUnit.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.yetico.hb.CostOfComplaintDictionaryItem.class)
            .addAnnotatedClass(pl.compan.docusafe.core.templating.DocumentSnapshot.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.KontrahentDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.DeliveryLocationDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.PurchaseDocumentTypeDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.VatRateDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.CostInvoiceProductDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.UnitOfMeasureDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.FinancialTaskDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.ProjectDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.ProjektBudzetEtapDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.OpkDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.ProjektBudzetDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.ProjektBudzetZasobDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.SourcesOfProjectFundingDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.PrzeznaczenieZakupuDB.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ul.hib.StructureBudget.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ul.hib.UlBudgetsFromUnits.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.ul.hib.UlContractor.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.FakturaMulti.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.ProductionOrderDB.class)
            .addAnnotatedClass(pl.compan.docusafe.web.swd.management.sql.SwdUprawnienie.class)
            .addAnnotatedClass(pl.compan.docusafe.web.swd.management.sql.SwdSystems.class)
            .addAnnotatedClass(pl.compan.docusafe.web.swd.management.sql.SwdMailParts.class)
            .addAnnotatedClass(pl.compan.docusafe.web.swd.management.sql.ZbpatSwdCustomFilter.class)
            .addAnnotatedClass(pl.compan.docusafe.core.alfresco.DockindMappedField.class)
            .addAnnotatedClass(pl.compan.docusafe.process.ProcessId.class)
            .addAnnotatedClass(pl.compan.docusafe.process.ProcessForDocument.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.KontrahentKontoDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.WydatekStrukturalnyDB.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.utp.entities.Acceptation.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.TypyWnioskowORezerwacjeDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.OkresBudzetowyDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.PlanZakupowDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.RodzajePlanowZakupuDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.BudzetKOKomorekDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.RoleWBudzecieDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.RoleWProjekcieDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.BudzetPozycjaSimpleDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.BudzetDoKomorkiDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.SzablonyRozliczenFakturDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.ProjectAccountPiatkiDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.NumeryZamowienDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.ZamowienieSimplePozycjeDB.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.PozycjaZamowieniaFakturaDB.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.Contract.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.ContractBudget.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.CostInvoiceProduct.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.Currency.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.CurrencyExchangeTable.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.OrganizationalUnit.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.ProductionOrder.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.PurchaseDocumentType.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.Source.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.Supplier.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.UnitBudgetKo.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.VatRate.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.UnitOfMeasure.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.FinancialTask.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.UnitBudget.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.ProjectBudget.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.ProjectBudgetResource.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.ProjectBudgeItem.class)
           .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.RolesInProjects.class)
           .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.UnitBudgetKoKind.class)
            .addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.TypyUrlopowDB.class)
			.addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.PracownikDB.class)
            .addAnnotatedClass(pl.compan.docusafe.parametrization.uek.hbm.ZrodloFinansowaniaDict2.class)
			.addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjeDB.class)
			.addAnnotatedClass(pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjePozycjaDB.class)
            .addAnnotatedClass(pl.compan.docusafe.dictionary.erp.simple.RolesInUnitBudgetKo.class)
            .addAnnotatedClass( pl.compan.docusafe.dictionary.erp.simple.Project.class)
            .addAnnotatedClass( pl.compan.docusafe.general.hibernate.model.ZrodloDB.class)
            .addAnnotatedClass(DSPublicKey.class)
            .addAnnotatedClass(DSUserPublicKey.class)
            .addAnnotatedClass(DSHostPublicKey.class)
            .addAnnotatedClass(PublicKeyType.class)
            .addAnnotatedClass(OfficePointPrefix.class)

            ;


        if (AvailabilityManager.isAvailable("uek"))
        {
            hibernateConfiguration.addAnnotatedClass(pl.compan.docusafe.parametrization.uek.hbm.ZrodloFinansowaniaDict.class)
                .addAnnotatedClass(pl.compan.docusafe.parametrization.uek.hbm.StawkaVatDict.class)
                .addAnnotatedClass(pl.compan.docusafe.parametrization.uek.hbm.WnioskujacyDict.class)
                .addAnnotatedClass(pl.compan.docusafe.parametrization.uek.hbm.InvoiceEntry.class)
                .addAnnotatedClass(pl.compan.docusafe.parametrization.uek.hbm.ZapotrzebowanieEntry.class)
                .addAnnotatedClass(pl.compan.docusafe.parametrization.uek.hbm.RepositoryDictEntry.class)
                .addAnnotatedClass(pl.compan.docusafe.parametrization.uek.hbm.ZamowieniePubliczneZrFinansowania.class)
                .addAnnotatedClass(pl.compan.docusafe.parametrization.uek.hbm.ZamowieniePubliczneModuly.class);
        }


	}
    /**
     * Instancje tej klasy wi?zane s? z w?tkami wywo?uj?cymi
     * metod? {@link DSApi#open(javax.security.auth.Subject)}.
     */
    private static class ThreadContext
    {
        DSContext dsContext;
        Throwable stackTrace;
    }

    private DSApi()
    {
    }

    /**
     * Sprawdza, czy data mie?ci si? w zakresie obs?ugiwanym przez baz? danych.
     * Obecnie sprawdza limity dla SQL Server, kt?re narzucaj? niewielki zakres
     * dat w por?wnaniu z innymi bazami.
     */
    public static boolean isLegalDbDate(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");
        return date.getTime() >= MIN_DB_DATE.getTime() && date.getTime() <= MAX_DB_DATE.getTime();
    }

    /**
     * Otwiera kontekst komunikacji ze ?r?d?ami danych w imieniu u?ytkownika
     * admin z rol? admin.
     * <p>
     * TODO: Metoda powinna najpierw sprawdza? kto jest aktualnym u?ytkownikiem
     *  okre?lonym jako administrator (GlobalPreferences.getAdminUsername()),
     *  a nast?pnie upewnia? si?, ?e taki u?ytkownik istnieje.
     *
     * @see #open(javax.security.auth.Subject)
     * @return
     * @throws EdmException
     */
    public static DSContext openAdmin() throws EdmException
    {
        Subject subject = getAdminSubject();
        return open(subject);
    }
    public static DSContext openUserContextWithAdminRole(String userName) throws EdmException
    {
        Subject subject = getUserSubjectByNameWithAdminRole(userName);
        return open(subject);
    }
    public static Subject getAdminSubject() {
        Set<Principal> principals = new HashSet<Principal>();
        principals.add(new UserPrincipal("admin"));
        principals.add(new RolePrincipal("admin"));
        Subject subject = new Subject(true, principals, Collections.emptySet(), Collections.emptySet());
        return subject;
    }
    
    public static Subject getUserSubjectByNameWithAdminRole(String userName) {
        Set<Principal> principals = new HashSet<Principal>();
        principals.add(new UserPrincipal(userName));
        principals.add(new RolePrincipal("admin"));
        Subject subject = new Subject(true, principals, Collections.emptySet(), Collections.emptySet());
        return subject;
    }

    /**
     * see DSContext open(Subject subject)
     *
     * @return
     * @throws EdmException
     */
    public static DSContext open() throws EdmException {
        return DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
    }

    /**
     * Tworzy obiekt {@link DSContext} i wi??e go z bie??cym w?tkiem.
     * <p>
     * Bie??cy w?tek wi?zany jest z classloaderem kontekstowym odczytuj?cym
     * klasy wygenerowane dla zestaw?w atrybut?w dokument?w. Po wywo?aniu
     * metody {@link #close()} przywracany jest poprzedni classloader.
     * @param subject
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static DSContext open(Subject subject) throws EdmException
    {
        if (!hibernateInitialized)
            throw new IllegalStateException("Hibernate nie zosta?o zainicjalizowane.");

        if (contexts.get() != null)
        {
            ThreadContext oldContext = (ThreadContext) contexts.get();
            log.error("Z w?tkiem zwi?zany jest otwarty kontekst (wyj?tek pokazuje miejsce otwarcia kontekstu)", oldContext.stackTrace);
            /**Zeby bylo widac gdzie byl otwarty kontekst bo oldContext.stackTrace nic ciekawego nie daje
             * - Tym bardziej nic nie daje poni?szy kod,
             * - daje duzo kuba meczyl sie 2 h bo to wykasowales i nie wiadomo bylo gdzie byl niepotrzebnie otwarty kontekst.
             * og?lnie to nigdy nie powinno lecie? je?li jest poprawnie wszystko napisane.
             * Tak wi?c popraw kod a nie komentujesz
             * Ten kod powinien sluzyc w momencie dewelopmentu do wskazania bled?w.
             * Wersja produkcyjna nie powinna zawierac kodu kt?ry doprowadzi do wejscia w t? sekcje kmodu
             * Przydatne je?li wywo?anie jest w "dziwnych" metodach
             * */
            log.error("Kontekst jest juz otwarty. Miejsce wywolania:  ", new Throwable());

            try
            {
                oldContext.dsContext.close();
            }
            catch (EdmException e)
            {
                log.error(e.getMessage(), e);
            }
            finally
            {
                contexts.set(null);
            }
        }

        ThreadContext threadContext = new ThreadContext();
        threadContext.dsContext = new DSContext(subject);
        threadContext.stackTrace = new Throwable();

        contexts.set(threadContext);

        return threadContext.dsContext;
    }

    /**
     * Zamyka obiekt {@link DSContext} zwi?zany z bie??cym w?tkiem
     * i usuwa to powi?zanie.
     * @throws EdmException Je?eli z w?tkiem nie jest zwi?zany
     * obiekt Context.
     */
    public static void close() throws EdmException
    {
        if (!hibernateInitialized){
            throw new IllegalStateException("Hibernate nie zosta?o zainicjalizowane.");
        }

        try {
            DocumentsCache.getInstance().clear();
            //teoretycznie nigdy nic nie powinno wyskoczy? z powy?szego polecenia
            //ale jakby wkrad? si? b??d to nast?pne linijki b?d? poprawnie wykonywane
        } catch (Throwable ex) {
            log.error(ex.getMessage(), ex);
        }

        try
        {
            if((ThreadContext)contexts.get()!=null) context().close();
        }
        finally
        {
        	DataMartManager.closeSession();
            // niezale?nie od rzuconych wyj?tk?w usuwane jest powi?zanie w?tku z kontekstem
            contexts.set(null);
        }
    }

    /**
     * Wywo?uje {@link #close()} maskuj?c wyj?tek EdmException.
     */
    public static void _close()
    {
        try
        {
            close();
        }
        catch (EdmException e)
        {
            log.warn(e.getMessage(), e);
        }
    }

    /**
     * Zwraca obiekt {@link DSContext} zwi?zany z bie??cym w?tkiem.
     * @throws IllegalStateException Je?eli z bie??cym w?tkiem nie powi?zano
     * obiektu {@link DSContext}.
     * @return DSContext
     */
    public static DSContext context()
    {
        if (!hibernateInitialized)
        {
            throw new IllegalStateException("Hibernate nie zosta?o zainicjalizowane.");
        }
        ThreadContext threadContext = (ThreadContext) contexts.get();
        if (threadContext == null)
        {
            throw new IllegalStateException(sm.getString("dsapi.contextNotOpened"));
        }
        return threadContext.dsContext;
    }

    /**
     * otwiera kontekst Admina jesli jest zamkniety, zwraca boolean "contextOpened"
     * nalezy ten parametr przekazac pozniej do funkcji closeContextIfNeeded
     *
     * uzycie
     * 		boolean contextOpened;
     * 		try{
     * 			contextOpened= DSApi.openContextIfNeeded();
     * 			// ... praca
     * 		}finally{
     * 			DSApi.closeContextIfNeeded(contextOpened);
     * 		}
     *
     * @return
     * @throws EdmException
     */
    public static boolean openContextIfNeeded() throws EdmException{
    	if (!DSApi.isContextOpen()){
			DSApi.openAdmin();
			return false;
		}
    	return true;
    }
    /**
     * otwiera kontekst z podanego usernamea jesli jest zamkniety, zwraca boolean "contextOpened"
     * nalezy ten parametr przekazac pozniej do funkcji closeContextIfNeeded
     *
     * uzycie
     * 		boolean contextOpened;
     * 		try{
     * 			contextOpened= DSApi.openContextIfNeeded();
     * 			// ... praca
     * 		}finally{
     * 			DSApi.closeContextIfNeeded(contextOpened);
     * 		}
     *
     * @return
     * @throws EdmException
     */
    public static boolean openUserContexWithAdminRoletIfNeeded(String userName) throws EdmException{
    	if (!DSApi.isContextOpen()){
			DSApi.openUserContextWithAdminRole(userName);
			return false;
		}
    	return true;
    }
    /**
     * otwiera tranzakcje, o ile nie byla juz otwarta
     */
    public static void beginTransacionSafely() throws EdmException{
    	if(!DSApi.context().inTransaction()){
			DSApi.context().begin();
		}
    }

    /**
     * zamyka kontekst jesli byl otwarty w tym samym kontekscie, tzn. do uzywania
     * w parze z funkcja openContextIfNeeded
     * @param contextOpened
     */
    public static void closeContextIfNeeded(boolean contextOpened){
    	if (!contextOpened)
   			DSApi._close();
    }


    public static boolean isContextOpen()
    {
        ThreadContext threadContext = (ThreadContext) contexts.get();
        return threadContext != null;
    }

    /**
     * Generyczne opakowanie dla DSContext.load().
     * Zwraca obiekt o podanej klasie i identyfikatorze. Je?eli
     * obiekt o takim identyfikatorze nie istnieje, zwracana jest
     * warto?? null.
     */
    public static <T> T getObject(Class<T> clazz, Serializable id)
        throws EdmException
    {
        try
        {
            return context().load(clazz, id);
        }
        catch (EntityNotFoundException e)
        {
            return null;
        }
    }

    /**
     * Zwraca nazw? tabeli bazodanowej, z kt?r? zwi?zane s? instancje
     * przekazanej klasy.
     */
    public static String getTableName(Class clazz)
    {
        if (!hibernateInitialized)
        {
            throw new IllegalStateException("Hibernate nie zosta?o zainicjalizowane.");
        }
        return configuration.getClassMapping(clazz.getName()).getTable().getName();
    }

    // TODO: nie dzia?a dla p?l b?d?cych kluczami pierwotnymi
    public static String getColumnName(Class clazz, String property)
        throws EdmException
    {
        try
        {
        	configuration.createMappings();
            return ((Column) configuration.getClassMapping(clazz.getName()).getProperty(property).
                getColumnIterator().next()).getName();
        }
        catch (MappingException e)
        {
            // nie dzia?a prawid?owo - nie znajduje kolumny
            Property id = configuration.getClassMapping(clazz.getName()).
                getIdentifierProperty();

            if (id == null)
                throw new EdmException(sm.getString("dsapi.invalidColumnName", clazz.getName(), property));

            if (id.getName().equals(property)) // NPE
            {
                return ((Column) id.getColumnIterator().next()).
                    getName();
            }

            throw new EdmException("W klasie "+clazz.getName()+" nie ma" +
                " w?asno?ci "+property);
        }
    }

    public static Column getColumn(Class clazz, String property)
    throws EdmException
	{
	    try
	    {
	        return ((Column) configuration.getClassMapping(clazz.getName()).getProperty(property).
	            getColumnIterator().next());
	    }
	    catch (MappingException e)
	    {
	        // nie dzia?a prawid?owo - nie znajduje kolumny
	        Property id = configuration.getClassMapping(clazz.getName()).
	            getIdentifierProperty();

	        if (id == null)
	            throw new EdmException(sm.getString("dsapi.invalidColumnName", clazz.getName(), property));

	        if (id.getName().equals(property)) // NPE
	        {
	            return ((Column) id.getColumnIterator().next());
	        }

	        throw new EdmException("W klasie "+clazz.getName()+" nie ma" +
	            " w?asno?ci "+property);
	    }
	}

    public static Type getColumnType(Class clazz, String property)
    throws EdmException
	{
	    try
	    {
	        return configuration.getClassMapping(clazz.getName()).getProperty(property).getType();
	    }
	    catch (MappingException e)
	    {
	        // nie dzia?a prawid?owo - nie znajduje kolumny
	        Property id = configuration.getClassMapping(clazz.getName()).
	            getIdentifierProperty();

	        if (id == null)
	            throw new EdmException(sm.getString("dsapi.invalidColumnName", clazz.getName(), property));

	        if (id.getName().equals(property)) // NPE
	        {
	            return id.getType();
	        }

	        throw new EdmException("W klasie "+clazz.getName()+" nie ma" +
	            " w?asno?ci "+property);
	    }
	}


    /**
     * Zwraca instancj? {@link org.hibernate.dialect.Dialect}, z kt?rej korzysta Hibernate.
     * @return Instancja Dialect.
     * @throws RuntimeException
     */
    public static Dialect getDialect()
    {
        if (!hibernateInitialized)
        {
            throw new IllegalStateException("Hibernate nie zosta?o zainicjalizowane.");
        }
        return dialect;
    }

    public static boolean isOracleServer()
    {
    	return getDialect() instanceof Oracle10gDialect;
    }

    public static boolean isFirebirdServer()
    {
    	return getDialect() instanceof FirebirdDialect;
    }
    public static boolean isPostgresServer()
    {
    	return getDialect() instanceof PostgreSQLDialect;
    }


    /**
     * Zwraca odpowiedz czy jest to SQLServer
     * @return
     */
    public static boolean isSqlServer()
    {
        return configuration.getProperties().
                getProperty(Environment.DIALECT).equals(SQLServerDialect.class.getName());
    }

    /**
     * Metoda powinna byc wykorzystywana przy budowaniu selectow, ktore w MS SQL niestety sa blokujace
     * @return
     */
    public static String nolockString()
    {
    	return isSqlServer() ? " WITH (NOLOCK) " : "";
    }

    /**
     * Metoda zwraca tekst wymuszajacy blokowanie na poziomie wiersza
     * Na razie tylko dla MS SQL WITH (ROWLOCK)
     * @return
     */
    public static String rowlockString()
    {
    	return isSqlServer() ? " WITH (ROWLOCK) " : "";
    }

    public static String getHibernateJbpmCfg() throws EdmException
    {
        try
        {
            File jbpmConfig = new File(Docusafe.getHome(), "datasource_jbpm.config");
            Properties properties = new Properties();
            if (jbpmConfig.exists())
            {
                try
                {
                    properties = Docusafe.loadPropertiesFile(jbpmConfig);
                }
                catch (IOException e) {log.error("B??d podczas czytania pliku datasource_jbpm.config", e);}
            }


            org.dom4j.io.SAXReader reader = new org.dom4j.io.SAXReader();

            // SAXReader nie powinien ??czy? si? z internetem, by odczyta? DTD
            reader.setEntityResolver(new EntityResolver()
            {
                final static String BASE = "http://hibernate.sourceforge.net/";
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException
                {
                    if (systemId != null && systemId.startsWith(BASE))
                    {
                        return new InputSource(getClass().getClassLoader().getResourceAsStream(
                            "org/hibernate/"+systemId.substring(BASE.length())));
                    }
                    return null;
                }
            });
            reader.setIncludeExternalDTDDeclarations(false);
            reader.setIncludeInternalDTDDeclarations(false);
            reader.setValidation(false);

            org.dom4j.Document document4j =
                reader.read(DSApi.class.getClassLoader().getResourceAsStream("hibernate_jbpm.cfg.xml"));

            String databaseType = properties.getProperty("database.type");
            if (databaseType != null)
            {
                org.dom4j.Element elDialect = (org.dom4j.Element) document4j.selectSingleNode("/hibernate-configuration/session-factory/property[@name='hibernate.dialect']");
                elDialect.setText(Docusafe.getHibernateDialectClass(databaseType));
            }
            if (properties.getProperty("driver.class") != null)
            {
                org.dom4j.Element elDriverClass = (org.dom4j.Element) document4j.selectSingleNode("/hibernate-configuration/session-factory/property[@name='hibernate.connection.driver_class']");
                elDriverClass.setText(properties.getProperty("driver.class"));
            }
            if (properties.getProperty("url") != null)
            {
                org.dom4j.Element elUrl = (org.dom4j.Element) document4j.selectSingleNode("/hibernate-configuration/session-factory/property[@name='hibernate.connection.url']");
                elUrl.setText(properties.getProperty("url"));
            }
            if (properties.getProperty("username") != null)
            {
                org.dom4j.Element elUsername = (org.dom4j.Element) document4j.selectSingleNode("/hibernate-configuration/session-factory/property[@name='hibernate.connection.username']");
                elUsername.setText(properties.getProperty("username"));
            }
            if (properties.getProperty("password") != null)
            {
                org.dom4j.Element elPassword = (org.dom4j.Element) document4j.selectSingleNode("/hibernate-configuration/session-factory/property[@name='hibernate.connection.password']");
                elPassword.setText(properties.getProperty("password"));
            }

            return document4j.asXML();
        }
        catch (org.dom4j.DocumentException e)
        {
            throw new EdmException("B??d odczytu konfiguracji Hibernate dla JBPM", e);
        }
    }


    /**
     * Budowa konfiguracji HBM
     * Przeniesion do klasy HibernateBilder celem zmniejszenia ilosci modyfikacji DSApi
     * @return
     * @throws EdmException
     */
    private static org.w3c.dom.Document getHibernateCfg() throws EdmException
    {
    	HibernateBuilder builder = new HibernateBuilder();
    	return builder.getHibernateCfg();
    }

    /**
     * Pierwsze wywo?anie konfiguruje Hibernate, nast?pne
     * wywo?ania s? ignorowane.
     */
    public synchronized static void reloadHibernate() throws EdmException
    {
        try
        {
            configuration.configure(getHibernateCfg());
			addAnnotatedClasses(configuration);
            sessionFactory = configuration.buildSessionFactory();

            reloadMappings();

            dialect = (Dialect) Class.forName(configuration.getProperties().
                getProperty(Environment.DIALECT)).newInstance();

            hibernateInitialized = true;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Inicjalizuje JBPM4 je?li jest wspierany (istnieje plik konfiguracyjny w home'ie)
     * @throws EdmException
     */
    public static void initJbpm4() throws EdmException {
        if(Jbpm4Provider.getInstance().isJbpm4Supported()){
            Jbpm4Provider.getInstance().initialize(sessionFactory);
        }
    }

    public static void initializeProxy(Object proxy) throws EdmException
    {
        try
        {
            org.hibernate.Hibernate.initialize(proxy);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    private static void reloadMappings()
        throws EdmException
    {
        org.hibernate.Session session = null;

        // classloader kontekstowy - b?dzie przywr?cony je?eli wyst?pi wyj?tek
        ClassLoader contextLoader = Thread.currentThread().getContextClassLoader();
        try
        {
            session = sessionFactory.openSession(
                Docusafe.getDataSource().getConnection());

            Configuration hibernateConfiguration = new Configuration();
            hibernateConfiguration.configure(getHibernateCfg());
            addAnnotatedClasses(hibernateConfiguration);

            org.hibernate.SessionFactory factory = hibernateConfiguration.buildSessionFactory();

            DSApi.configuration = hibernateConfiguration;
            DSApi.sessionFactory = factory;
        }
        catch (HibernateException e)
        {
            Thread.currentThread().setContextClassLoader(contextLoader);
            log.error(e.getMessage(), e);
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            Thread.currentThread().setContextClassLoader(contextLoader);
            log.error(e.getMessage(), e);
            throw new EdmSQLException(e);
        }
        finally
        {
            if (session != null)
                try {
                    Connection conn = session.close();
                    if (conn != null) conn.close();
                } catch (Throwable t) { };
        }
    }


//    private static void addJbpm4Mappings(AnnotationConfiguration hibernateConfiguration) {
//        for(URL mapping: Jbpm4Provider.getMappingFiles()){
//            hibernateConfiguration.addURL(mapping);
//        }
//        log.info("dodano mapowanie jbpm4");
//    }

    /**
     * Otwiera i zwraca sesj? Hibernate.
     */

    static org.hibernate.Session openSession() throws EdmException
    {
        try
        {
            // otwieranie sesji Hibernate z nowym po??czeniem JDBC
            // po??czenie zostanie zamkni?te przez obiekt Context
        	//mierzymy czas pobrania polaczenia
        	long timeA = System.currentTimeMillis();
            Connection conn = Docusafe.getDataSource().getConnection();
            long duration = System.currentTimeMillis() - timeA;
            if (duration > 500L)
            {
            	log.error("Czas pobrania polaczenia wynosi {} [ms]",duration);
            }

            // dla sqlservera niski poziom izolacji transakcji
            // alternatyw? jest dodanie do wszystkich zapyta? select klauzuli
            // WITH(NOLOCK), aby unikn?? blokowania tabel - wiele zapyta? zadawanych
            // jest przez Hibernate, kt?re nie umo?liwia dodawania tej klauzuli,
            // st?d poziom izolacji transakcji
            //if (Boolean.valueOf(Docusafe.getConfigProperty("sqlserver.readuncommitted", "false")).booleanValue() &&
            //    getDialect() instanceof SQLServerDialect)
            if (getDialect() instanceof SQLServerDialect)
            {
            	log.trace("MS SQL Poziom transakcji=TRANSACTION_READ_UNCOMMITTED");
                conn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
            }
            assert conn != null;
            return sessionFactory.openSession(conn);
        }
        catch (SQLException e)
        {
        	log.error("",e);
            throw new EdmSQLException(e);
        }
    }

	public static Configuration getConfiguration() {
		return configuration;
	}

	public static EncryptionCredentials credentials(String username)
	{
		if(!credentialsMap.containsKey(username))
		{
			EncryptionCredentials ec = new EncryptionCredentials(username);
			ec.init();
			credentialsMap.put(username, ec);
		}
		return credentialsMap.get(username);
	}

	public static void clearCredentials(String username)
	{
		LoggerFactory.getLogger("tomekl").debug("clearCredentials {}",username);
		if(credentialsMap.containsKey(username))
		{
			credentialsMap.remove(username);
		}
	}

	public static DocusafeKeyStore getKeyStoreInstance(String code)
	{
		try
		{
			return DocumentKind.findByCn(code).logic().getKeyStoreInstance();
		}
		catch (EdmException e)
		{
			return new IndividualRepositoryKey();
		}
	}

	public static Collection<DocusafeKeyStore> getAvailableKeyStoresInstances()
	{
		return docusafeKeyStoreInstances;
	}

	private static void initDocusafeKeyStoreInstances()
	{
		docusafeKeyStoreInstances = new HashSet<DocusafeKeyStore>();
		docusafeKeyStoreInstances.add(new IndividualRepositoryKey());
		docusafeKeyStoreInstances.add(new MainSystemKey());
	}

//	public static void setConfiguration(AnnotationConfiguration configuration) {
//		DSApi.configuration = configuration;
//	}
}
