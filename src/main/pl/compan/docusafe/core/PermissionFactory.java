package pl.compan.docusafe.core;

import java.util.List;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;

public class PermissionFactory {

	private static PermissionFactory instance;

	public static PermissionFactory getInstance() {
		if (instance == null) {
			instance = new PermissionFactory();
		}
		return instance;
	}

	/**
	 * Kopiowanie uprawnien z folderu na dokument. Stare uprawnienia dokumentu
	 * sa usuwane. 
	 * @param input
	 * @param output
	 * @return
	 * @throws EdmException
	 */
	public Document CopyPermission(Folder source, Document destination)
			throws EdmException {
		// Pobranie listy uprawnien do sprawy
		List<ObjectPermission> folder_permission = ObjectPermission.find(
				Folder.class, source.getId());

		// Usuniecie uprawnien dla dokumentu
		List<ObjectPermission> document_permission = ObjectPermission.find(
				Document.class, destination.getId());
		for (ObjectPermission perm : document_permission) {
			DSApi.context().session().delete(perm);
		}

		// Utworzenie nowych uprawnien do dokumentu wg uprawnien folderu

		for (ObjectPermission perm : folder_permission) {
			ObjectPermission.create(Document.class, destination.getId(), perm
					.getSubject(), perm.getSubjectType(), perm.getName());
		}

        if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
            JackrabbitXmlSynchronizer.createEvent(destination.getId());
        }

		return destination;
	}
	
	/**
	 * Metoda zwraca uprawnienia do dokumentu w postaci obiektu Set
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	public java.util.Set<PermissionBean> GetDocumentPermission(Long document_id) throws EdmException
	{
		java.util.Set<PermissionBean> permission_set = new java.util.HashSet<PermissionBean>();
		
		List<ObjectPermission> permission_list = ObjectPermission.find(
				Document.class, document_id);
		
		for (ObjectPermission permission : permission_list)
		{
			PermissionBean pb = new PermissionBean(permission.getName(),permission.getSubject(),permission.getSubjectType());
			permission_set.add(pb);
		}
		
		return permission_set;
	}
	
	
	
	
	public Document CopyPermission(Document source, Document destination) throws EdmException
	{
		// Pobranie listy uprawnien do dokumentu
		List<ObjectPermission> source_permission = ObjectPermission.find(
				Document.class, source.getId());

		// Usuniecie uprawnien dla dokumentu
		List<ObjectPermission> destination_permission = ObjectPermission.find(
				Document.class, destination.getId());
		for (ObjectPermission perm : destination_permission) {
			DSApi.context().session().delete(perm);
		}

		// Utworzenie nowych uprawnien do destination_document wg uprawnien source_document

		for (ObjectPermission perm : source_permission) {
			ObjectPermission.create(Document.class, destination.getId(), perm
					.getSubject(), perm.getSubjectType(), perm.getName());
		}

        if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
            JackrabbitXmlSynchronizer.createEvent(destination.getId());
        }

		return destination;
	}
}
