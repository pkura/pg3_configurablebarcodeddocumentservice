package pl.compan.docusafe.core.activiti;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.process.DocuSafeActivitiesProcessView;
import pl.compan.docusafe.process.form.ActivityController;
import pl.compan.docusafe.process.form.ActivityProvider;
import pl.compan.docusafe.process.form.SelectItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.ExtendedRenderBean;
import pl.compan.docusafe.web.common.RenderBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Damian on 10.04.14.
 * Implementacja na bazie pl.compan.docusafe.core.jbpm4.AcceptancesSelectProcessView
 *
 */
public class AcceptancesSelectProcessView extends DocuSafeActivitiesProcessView {
    private final static Logger log = LoggerFactory.getLogger(AcceptancesSelectProcessView.class);

    public AcceptancesSelectProcessView(ActivityProvider activityController) {
        super(activityController);
    }

    @Override
    public RenderBean render(ProcessInstance pi) throws EdmException {
        RenderBean ret = super.render(pi);

        //extract document id
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        List<Execution> executionsList = runtimeService.createExecutionQuery().processInstanceId(pi.getId().toString()).list();

        Long docId;
        if(executionsList != null && !executionsList.isEmpty()){
            docId = (Long) runtimeService.getVariable(executionsList.get(0).getId(), "docId");
        }else{
            log.error("Couldn't find docId",new NullPointerException());
            return ret;
        }

        List<DocumentAcceptance> acceptances = DocumentAcceptance.find(docId);

        Document doc = Document.find(docId);
        Field docProcesses = doc.getDocumentKind().getFieldByCn("STATUS");
        String docStatus = doc.getFieldsManager().getEnumItemCn("STATUS");

        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        for (DocumentAcceptance itemAcceptance: acceptances) {
            itemAcceptance.initDescription();
            // nie pokazywanie na li�cie akceptacji dla takiego samego etapu, co aktualnie wykonywany
            if (!itemAcceptance.getAcceptanceCn().equals(docStatus))
                selectItems.add(ActivityController.createSelectItem(itemAcceptance.getId(), itemAcceptance.getAsFirstnameLastname(), itemAcceptance.getUsername(), docProcesses.getEnumItemByCn(itemAcceptance.getAcceptanceCn()).getTitle(), itemAcceptance.getAcceptanceCn()));
        }
        if(ret instanceof ExtendedRenderBean){
            ((ExtendedRenderBean) ret).putValue("selectItems", selectItems);
        }else{
            log.error("Couldn't find put selectItems becouse RenderBean isn't instanceof ExtendedRenderBean",new ClassCastException());
        }

        return ret;
    }
}
