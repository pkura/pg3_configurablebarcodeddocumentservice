package pl.compan.docusafe.core.activiti;

import com.google.common.base.Strings;
import org.activiti.engine.delegate.DelegateExecution;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.activiti.helpers.ActivitiDockindHelper;
import pl.compan.docusafe.core.exports.ExportHandler;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.RequestResult;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.parametrization.ams.ZamowienieLogic;
import pl.compan.docusafe.parametrization.ams.export.SimpleErpExportFactory;
import pl.compan.docusafe.parametrization.mosty.jbpm.BookKeeperDecision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import static pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocument.*;

@SuppressWarnings("serial")
@Component("exportDocumentHandler")
public class ExportDocumentHandler {

    private static final String FAST_REPEAT = "fast_repeat";
    private static final String REPEAT = "repeat";
    private static final String SUCCESS = "success";
    private static final String FAIL = "fail";
    private static final String DEFAULT_TRANSITION = FAIL;


    private static final String DOCUMENT_ID_VARIABLE = "docId";
    public static final String TO_CONFIRM = "to-confirm";
    private static final String MAX_FAST_REPEAT_VAR = "maxFastRepeat";


    //ZGL_BLEDU = ZGLOSZENIE_BLEDU, Formularz do przesyłania info o błędzie do admina (usługą lub przez człowieka)  ----------
    /** zg�oszenie b��du: ID statusu b��du nowego */				            String ZGL_BLEDU_nowy = "11";
    /** zg�oszenie b��du: ID b��du b�d�cego w obs�udze */                       String ZGL_BLEDU_obslugiwany = "12";
    /** zg�oszenie b��du: ID b��du rozwi�zanego */ 					            String ZGL_BLEDU_rozwiazany = "13";
    /** zg�oszenie b��du: ID b��du odrzuconego */ 					            String ZGL_BLEDU_odrzucony = "14";
    /** zg�oszenie b��du: ID b��du odsuni�tego w czasie */ 			            String ZGL_BLEDU_odsuniety= "15";

    /** Komunikat gotowy do pobrania przez szyn�. */				            String ERP_PRZESLANO_NA_SZYNE = "NEW";
    /** Komunikat zosta�� pobrany przez task */                                 String ESB_POBRANY_PRZEZ_SZYNE = "UPLOADED";
    /** Komunikat jest przetwarzany przez szyn� danych */			            String ESB_PRZETWARZANY_PRZEZ_SZYNE = "PROCESSING";
    /** Komunikat zosta� poprawnie dodany do Simple.ERP */			            String ESB_DODANO_DO_ERP = "FINISHED";
    /** Komunikat nie przeszed� wst�pnej walidacji XSD */			            String ESB_BLAD_WALIDACJI_PLIKU_XML = "INVALID";
    /** Komunikat nie zosta� dodany do Simple.ERP system zg�osi�� kod b��du */  String ESB_BLAD_WEWNETRZNY_ERP = "FAULT";
	
	private static final Logger log = LoggerFactory.getLogger(ExportDocumentHandler.class);

	private Integer fastRepeats = 5;
	private String action;

    @Autowired
    private ActivitiDockindHelper dockindHelper;


	public String handleExport(DelegateExecution exec) {

        boolean isOpened = true;
        try {
			if (!DSApi.isContextOpen()) {
				isOpened = false;
				DSApi.openAdmin();
			}

            if(exec.hasVariable("ignoreExport") && exec.getVariable("ignoreExport") instanceof Boolean && (Boolean) exec.getVariable("ignoreExport")){
                setAction(SUCCESS,exec);
                return action;
            }

            exec.setVariable(IS_EXPORT, true);
			Long docId = Long.valueOf(exec.getVariable(DOCUMENT_ID_VARIABLE).toString());
			
			Integer counter = 1;
			if (exec.hasVariable(EXPORT_REPEAT_COUNTER)) {
				counter = (Integer) exec.getVariable(EXPORT_REPEAT_COUNTER);
			}
            if (exec.hasVariable(MAX_FAST_REPEAT_VAR)) {
                fastRepeats = (Integer) exec.getVariable(MAX_FAST_REPEAT_VAR);
            }
			if (counter + 1 > fastRepeats) {
				setAction(REPEAT,exec);
			} else {
				setAction(FAST_REPEAT,exec);
			}
			exec.setVariable(EXPORT_REPEAT_COUNTER, ++counter);

			String guid;

			if (exec.hasVariable(DOC_GUID)) {
				guid = exec.getVariable(DOC_GUID).toString();

				ExportHandler handler = new ExportHandler(new SimpleErpExportFactory());
				RequestResult result = handler.checkExport(guid);

				int state = result.getStateId();
				exec.setVariable(EXPORT_RESULT_STATE, state);

				String stateName = result.getStateName();
				exec.setVariable(EXPORT_RESULT_STATE_NAME, stateName);

				String requestResult = result.getResult();

				if (ESB_BLAD_WEWNETRZNY_ERP.equals(stateName) || ESB_BLAD_WALIDACJI_PLIKU_XML.equals(stateName)) {
					log.error("B��d eksportu, counter: " + counter);
					log.error("B��d eksportu, guid: " + guid);
					log.error("B��d eksportu, docId: " + exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY));
					log.error("B��d eksportu, state: " + state);
					log.error("B��d eksportu, stateName: " + stateName);
					log.error("B��d eksportu, request result: " + requestResult);
					exec.removeVariable(EXPORT_REPEAT_COUNTER);
                    exec.removeVariable(DOC_GUID);

                    setAction(FAIL,exec);

					if (docId != null) {
						setResults("B��d eksportu!\n"+ buildExportRemarks(guid, stateName, requestResult), docId);
					}
				} else if (ESB_DODANO_DO_ERP.equals(stateName)) {
                    exec.removeVariable(DOC_GUID);
					exec.removeVariable(EXPORT_REPEAT_COUNTER);
                    exec.removeVariable(IS_EXPORT);
                    exec.setVariable(TO_CONFIRM, true);
					setAction(SUCCESS,exec);

                    dockindHelper.setStatus(docId, 1600);
                    setResults("Eksport zako�czony pomy�lnie\n"+ buildExportRemarks(guid, stateName, requestResult), docId);

                    exec.setVariable(ZamowienieLogic.ERP_DOCUMENT_IDM, getDocumentErpIdm(requestResult));
				}

			} else {
                OfficeDocument doc = OfficeDocument.find(docId);
                ExportedDocument exported = null;
                dockindHelper.setStatus(docId, 1500);

                try {
                    exported = doc.getDocumentKind().logic().buildExportDocument(doc);
                    if (exported == null) {
                        throw new NullPointerException("Export document not built!");
                    }
                } catch (Exception e) {
                    setResults("B��d eksportu, guid: brak, id dokumentu: "+docId, docId);
                    exec.removeVariable(EXPORT_REPEAT_COUNTER);
                    setAction(FAIL,exec);

                    throw new EdmException(e);
                }

                try {
                    ExportHandler handler = new ExportHandler(new SimpleErpExportFactory());
                    guid = handler.xmlExport(exported);
                    if (Strings.isNullOrEmpty(guid)) {
                        throw new EdmException("B�ad w eksporcie dokumentu, mo�liwy b��d w komunikacji, serwis nie zwr�ci� kodu guid dla dokumentu.");
                    }
                    exec.setVariable(DOC_GUID, guid);
                } catch (Exception e) {
                    if (counter > 3) {
                        setResults("B��d eksportu, przekroczono ilo�� 3 pr�b wys�ania dokumentu do szyny przetwarzaj�cej. Zg�o� ten b��d do administratora.", docId);
                        exec.removeVariable(EXPORT_REPEAT_COUNTER);
                        exec.removeVariable(DOC_GUID);
                        setAction(FAIL,exec);
                    }

                    throw new EdmException(e);
                }
            }
        } catch (Exception e) {
			log.error(e.getMessage(),e);
            setAction(FAIL,exec);
            return getAction();
        } finally {
			if (DSApi.isContextOpen() && !isOpened)
				DSApi._close();
		}

        exec.setVariable("action",action);
		return action;
	}


    private void setAction(String actionName, DelegateExecution exec) {
        action = actionName;
        exec.setVariable("action",action);
    }


    private String getAction() {
        return action != null ? action : DEFAULT_TRANSITION;
    }

    protected void setResults(String result, Long docId) throws EdmException {
        try {
            DSApi.context().begin();
            OfficeDocument doc = OfficeDocument.findOfficeDocument(docId);
            doc.addRemark(new Remark(result, "admin"));
            DSApi.context().commit();
        } catch (EdmException e) {
            log.error(e.getMessage(),e);
            DSApi.context().setRollbackOnly();
        }
    }


    private String getDocumentErpIdm(String requestResult) {
        try {
            Document requestResultXml = DocumentHelper.parseText(requestResult);
            Element root = requestResultXml.getRootElement();
            if (root != null) {
                if (root.element("IDM") != null && !Strings.isNullOrEmpty(root.element("IDM").getText())) {
                    return root.element("IDM").getText();
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private String buildExportRemarks(String guid, String stateName, String requestResult) {
        StringBuilder sb = new StringBuilder();

        try {
            Document requestResultXml = DocumentHelper.parseText(requestResult);
            Element root = requestResultXml.getRootElement();
            if (root != null) {
                if (root.element("ID") != null && !Strings.isNullOrEmpty(root.element("ID").getText())) {
                   sb.append("ID dokumentu ERP: ").append(root.element("ID").getText()).append("\n");
                }
                if (root.element("IDM") != null && !Strings.isNullOrEmpty(root.element("IDM").getText())) {
                    sb.append("IDM dokumentu ERP: ").append(root.element("IDM").getText()).append("\n");
                }
                if (root.element("StatusMessage") != null && !Strings.isNullOrEmpty(root.element("StatusMessage").getText())) {
                    sb.append("Status eksportu: ").append(root.element("StatusMessage").getText()).append("\n");
                }
            }
        } catch (Exception e) {
            log.error("CAUGHT: "+e.getMessage(),e);
        }

        sb.append("\n").append("Guid dokumentu: ").append(guid).append("\n");
        sb.append("Nazwa stanu: ").append(stateName).append("\n");
        return sb.toString();
    }


    public static String getFastRepeat() {
        return FAST_REPEAT;
    }

    public static String getRepeat() {
        return REPEAT;
    }

    public static String getSuccess() {
        return SUCCESS;
    }

    public static String getFail() {
        return FAIL;
    }
}
