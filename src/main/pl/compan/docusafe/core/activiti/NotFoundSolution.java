package pl.compan.docusafe.core.activiti;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;

/**
 * Enum for Helpers/Listeners to chose solution for user/division not found
 * enum do ustalania co Helper/Listener ma zrobi� je�eli nie znajdzie u�ytkownik�w/dzia��w do przypisania
 */
public enum NotFoundSolution {
    /**
     * Helper/Listener should throw exception
     */
    EXCEPTION,
    /**
     * Should assign to <code>admin</code> (return string with admin name)
     */
    ADMIN,

    /**
     * Ignores user/division not found
     */
    IGNORE,
    /**
     * Default solution implemented in helpers/listeners
     */
    DEFAULT;





    public static String ADMIN_NAME = "admin";



    /**
     * Example implementation of checking candidates were found <br/>
     * Checks if candidates are empty -> if true: takes action dependant on given solution type
     *  <br/>DEFAULT: throws exception
     * @param candidates
     * @param solution - CASE INSENSITIVE name of solution Enum
     * @return
     */
    public static String checkCandidates(String candidates, String solution) throws EdmException{
        NotFoundSolution solutionType = null;
        if(solution != null){
            solutionType = valueOf(solution.toUpperCase());
        }
        return checkCandidates(candidates,solutionType);
    }


    /**
     * Example implementation of checking candidates were found <br/>
     * Checks if candidates are empty -> if true: takes action dependant on given solution type
     *  <br/>DEFAULT: throws exception
     * @param candidates
     * @param solution
     * @return
     */
    public static String checkCandidates(String candidates, NotFoundSolution solution) throws EdmException {
        if(IGNORE.equals(solution)){
            return candidates;
        }
        if(StringUtils.isBlank(candidates)){
            if (solution == null) {
                solution = NotFoundSolution.DEFAULT;
            }
            switch (solution){
                case EXCEPTION:
                    throw new EdmException("Nie znaleziono kandydat�w");
                case ADMIN:
                    return NotFoundSolution.ADMIN_NAME;
                case DEFAULT:
                    throw new EdmException("Nie znaleziono kandydat�w");
            }
        }

        return candidates;
    }



}
