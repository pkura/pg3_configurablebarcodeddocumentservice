package pl.compan.docusafe.core.activiti.listeners;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.activiti.helpers.ActivitiDockindHelper;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Listener/Bean that adds acceptation to list of cacceptations and to assignment history
 * It should be placed on the SequenceFlow (that's mean activiti element visualized by arrow ->)
 */
@Component("acceptancesListener")
public class AcceptancesListener implements ExecutionListener {
    private static final Logger log = LoggerFactory.getLogger(AcceptancesListener.class);

    @Autowired
    private AddToHistoryListener addToHistoryListener;

    @Autowired
    private ActivitiDockindHelper activitiDockindHelper;


    @Override
    public void notify(DelegateExecution execution) throws Exception {
        boolean wasOpened = false;
        try{
            wasOpened = DSApi.openContextIfNeeded();

            Long docId = (Long) execution.getVariable("docId");
            addAcceptation(docId,execution);

        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        } finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }
    }


    /**
     * Adds current status to list of acceptations and to assignment history
     * @param docId
     * @param execution
     */
    public void addAcceptation(Long docId, DelegateExecution execution) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        String acceptationCn = activitiDockindHelper.getStatusCn(docId);
        addAcceptation(doc,execution,acceptationCn);
    }

    /**
     * Adds acceptation to list of acceptations and to assignment history
     * @param docId
     * @param execution
     * @param acceptationCn CN z pola STATUS
     */
    public void addAcceptationFromCn(Long docId, DelegateExecution execution, String acceptationCn) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);

        EnumItem statusItem = (doc.getFieldsManager().getField("STATUS")).getEnumItemByCn(acceptationCn);
        if(statusItem != null){
            addAcceptation(doc, execution,statusItem.getCn());
        }else{
            throw new EdmException("Nie znaleziono statusu dokumentu o cn = " + acceptationCn);
        }

    }

    /**
     * Adds given acceptation to list of acceptations and to assignment history
     * @param docId
     * @param execution
     * @param acceptationCn
     */
    public void addAcceptation(Long docId, DelegateExecution execution, String acceptationCn) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        addAcceptation(doc,execution,acceptationCn);
    }

    /**
     * Adds given acceptation to list of acceptations and to assignment history
     * @param doc
     * @param execution
     * @param acceptationCn
     */
    public void addAcceptation(OfficeDocument doc, DelegateExecution execution, String acceptationCn) throws EdmException {

        DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), acceptationCn, null);

        updateAssignmentHistory(execution, doc.getId(), acceptationCn, null);


        if (AvailabilityManager.isAvailable("addToWatch")){
            DSApi.context().watch(URN.create(Document.find(doc.getId())));
        }

        doc.getDocumentKind().logic().onAcceptancesListener(doc, acceptationCn);
    }



    private void updateAssignmentHistory(DelegateExecution execution, Long docId, String acceptation, String type) throws EdmException {
        addToHistoryListener.addAcceptationToHistory(docId,execution,type);
    }

}
