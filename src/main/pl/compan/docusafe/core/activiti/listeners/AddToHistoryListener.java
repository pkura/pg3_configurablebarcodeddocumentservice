package pl.compan.docusafe.core.activiti.listeners;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.task.IdentityLink;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.EnumItemImpl;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.regtype.EnumField;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;

/**
 * Handler/Listener s�u��cy wpisywaniu do historii z proces�w activiti
 */
@Component("historyListener")
public class AddToHistoryListener implements TaskListener,ExecutionListener {

    private final static Logger log = LoggerFactory.getLogger(AddToHistoryListener.class);

    /**
     * Typ wpisu do historii wykorzystywane w metodzie <code>notify()</code>:
     * <ul>
     *     <li><code>candidates</code> - wpis typu "Przekaza� zadanie do u�ytkownika .... (lub dzia�u .....) do etapu ....."</li>
     *     <li><code>accept</code> - wpis o zaakceptowaniu procesu</li>
     *     <li><code>export</code> - wpis o eksporcie dokumentu</li>
     *     <li><code>correction</code> - wpis o korekcie dokumentu</li>
     *     <li><code>reject</code> - wpis o odrzuceniu procesu</li>
     *     <li><code>finish</code> - wpis o zako�czeniu procesu</li>
     * </ul>
     */
    private String historyEntryType;



//_________________________METODY LISTENEROWE________________________

    /**
     * Metoda zostawiona dla tych co wol� u�ywa� standardowych listener�w a nie spring bean�w
     * @param task
     * @throws Exception
     */
    @Override public void notify(DelegateTask task) {
        boolean wasOpened = false;
        try{
            wasOpened = DSApi.openContextIfNeeded();

            Long docId = (Long) task.getVariable("docId");

            if("candidates".equalsIgnoreCase(historyEntryType)){
                addCandidatesToHistory(docId,task);
            }else if("finish".equalsIgnoreCase(historyEntryType)){
                addFinishToHistory(docId, task.getExecution());
            }else if("reject".equalsIgnoreCase(historyEntryType)){
                addRejectionToHistory(docId,task.getExecution());
            }else if("export".equalsIgnoreCase(historyEntryType)){
                addExportToHistory(docId,task.getExecution());
            }else if("accept".equalsIgnoreCase(historyEntryType)){
                addAcceptationToHistory(docId, task.getExecution());
            }else if("correction".equalsIgnoreCase(historyEntryType)){
                addCorrectionToHistory(docId, task.getExecution());
            }else{
                throw new EdmException("Podano b��dny typ wpisu do historii dekretacji (typ: " + historyEntryType +
                        " - jest nie w�a�ciwy lub nie mo�e by� u�yty przy Event type = " + task.getEventName()
                        + " w dokumencie id = " + docId);
            }
        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        } finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }

    }

    /**
     * Metoda zostawiona dla tych co wol� u�ywa� standardowych listener�w a nie spring bean�w
     * @param execution
     * @throws Exception
     */
    @Override public void notify(DelegateExecution execution) throws Exception {
        boolean wasOpened = false;
        try{
            wasOpened = DSApi.openContextIfNeeded();

            Long docId = (Long) execution.getVariable("docId");

            if("finish".equalsIgnoreCase(historyEntryType)){
                addFinishToHistory(docId, execution);
            }else if("reject".equalsIgnoreCase(historyEntryType)){
                addRejectionToHistory(docId,execution);
            }else if("export".equalsIgnoreCase(historyEntryType)){
                addExportToHistory(docId,execution);
            }else if("accept".equalsIgnoreCase(historyEntryType)){
                addAcceptationToHistory(docId,execution);
            }else if("correction".equalsIgnoreCase(historyEntryType)){
                addCorrectionToHistory(docId, execution);
            }else{
                throw new EdmException("Podano b��dny typ wpisu do historii dekretacji (typ: " + historyEntryType +
                        " - jest nie w�a�ciwy lub nie mo�e by� u�yty przy Event type = " + execution.getEventName()
                        + " w dokumencie id = " + docId);
            }
        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        } finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }
    }



//________________________ METODY BEAN'OWE_____________________

    /**
     * Metoda do wykorzystania jako activiti/spring bean <br/>
     * w tasku w listenerach wybra� event: <code>create</code> oraz typ <code>Expression</code> i zapisa� w postaci <br/>
     * <code>${historyHandler.addCandidatesToHistory(docId,task)}</code>
     * @param docId
     * @param task
     * @throws pl.compan.docusafe.core.EdmException
     */
    public void addCandidatesToHistory(Long docId,DelegateTask task) throws EdmException {
        OfficeDocument doc = OfficeDocument.findOfficeDocument(docId,false);

        String processName = task.getProcessDefinitionId();
        for(IdentityLink candidate : task.getCandidates()){
            String userName = candidate.getUserId();
            String guid = candidate.getGroupId();
            if (userName != null && StringUtils.isNotBlank(userName) && StringUtils.isBlank(guid)) {
                DSUser user = DSUser.findByUsername(userName);
                if (user != null ) {
                    DSDivision[] div = user.getOriginalDivisionsWithoutGroup();
                    if(div.length > 0){
                        guid = div[0].getGuid();
                    }
                }
            }


            addCandidatesToHistory(doc, processName, userName, guid);
        }
    }


    /**
     * Metoda do wykorzystania jako activiti/spring bean<br/>
     * Dodaje wpis o odrzuceniu procesu
     * @param docId
     * @param execution
     * @throws EdmException
     */
    public void addRejectionToHistory(Long docId, DelegateExecution execution) throws EdmException {
    	  OfficeDocument doc = OfficeDocument.findOfficeDocument(docId,false);
        String processName = execution.getProcessDefinitionId();
        addRejectionToHistory(doc,processName);
    }


    /**
     * Metoda do wykorzystania jako activiti/spring bean<br/>
     * Dodaje wpis o zako�czeniu procesu
     * @param docId
     * @param execution
     * @throws EdmException
     */
    public void addFinishToHistory(Long docId, DelegateExecution execution) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        String processName = execution.getProcessDefinitionId();
        addFinishToHistory(doc, processName);
    }

    /**
     * Metoda do wykorzystania jako activiti/spring bean<br/>
     * Dodaje wpis o wyeksportowaniu dokumentu
     * @param docId
     * @param execution
     * @throws EdmException
     */
    public void addExportToHistory(Long docId, DelegateExecution execution) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        String processName = execution.getProcessDefinitionId();
        addFinishToHistory(doc, processName);
    }

    /**
     * Metoda do wykorzystania jako activiti/spring bean<br/>
     * Dodaje wpis o wyeksportowaniu dokumentu oraz zako�czeniu procesu
     * @param docId
     * @param execution
     * @throws EdmException
     */
    public void addExportAndFinishToHistory(Long docId, DelegateExecution execution) throws EdmException {
        addExportToHistory(docId,execution);
        addFinishToHistory(docId,execution);
    }

    /**
     * Metoda do wykorzystania jako activiti/spring bean<br/>
     * Dodaje <b><u>tylko</u></b> wpis o zaakceptowaniu dokumentu w domy�lnym typie
      
     * @param docId
     * @param execution
     * @throws EdmException
     */
    public void addAcceptationToHistory(Long docId, DelegateExecution execution) throws EdmException {
        addAcceptationToHistory(docId, execution,null);
    }

    /**
     * Metoda do wykorzystania jako activiti/spring bean<br/>
     * Dodaje <b><u>tylko</u></b>  wpis o zaakceptowaniu dokumentu o danym typie
     * @param docId
     * @param execution
     * @param type typ wpisu do historii <br/>
     *             Je�eli ustawione na "end-task" to dodaje wpis typu <code>EntryType.JBPM_END_TASK</code>
     * @throws EdmException
     */
    public void addAcceptationToHistory(Long docId, DelegateExecution execution, String type) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        String processName = execution.getProcessDefinitionId();
        addAcceptationToHistory(doc, processName,type);
    }


    public void addCorrectionToHistory(Long docId, DelegateExecution execution) throws EdmException{
        OfficeDocument doc = OfficeDocument.find(docId);
        String processName = execution.getProcessDefinitionId();
        addCorrectionToHistory(doc,processName);
    }



//_________________________PRYWATNE METODY ________________________

    /**
     * Dodaje wpis do Historii dekretacji typu <br/>
     * "Przekaza� zadanie do u�ytkownika .... (lub dzia�u .....) do etapu ....."
     * <br/> Zaka�ada �e pole status ma cn = "STATUS"
     * @param doc
     * @param processName
     * @param user
     * @param guid
     * @throws pl.compan.docusafe.core.EdmException
     */
    protected void addCandidatesToHistory(OfficeDocument doc, String processName, String user, String guid) throws EdmException {
        String status = (String)doc.getFieldsManager().getValue("STATUS");
        addCandidatesToHistory(doc, processName, status, user, guid);
    }

    /**
     * Dodaje wpis do Historii dekretacji typu <br/>
     * "Przekaza� zadanie do u�ytkownika .... (lub dzia�u .....) do etapu ....."
     * @param doc
     * @param processName
     * @param status <u>warto��</u> pola STATUS
     * @param userName
     * @param guid
     * @throws pl.compan.docusafe.core.EdmException
     */
    protected void addCandidatesToHistory(OfficeDocument doc, String processName,String status, String userName, String guid) throws EdmException{
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(processName);
        if (userName == null) {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
            ahe.setTargetGuid(guid);
        } else {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
            ahe.setTargetUser(userName);
        }
        DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
        if (divs != null && divs.length > 0){
            ahe.setSourceGuid(divs[0].getName());
        }

        ahe.setCtime(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);

        if (status != null){
            ahe.setStatus(status);
        }

        doc.addAssignmentHistoryEntry(ahe);
    }

    /**
     * Dodaje wpis do historii o odrzuceniu dokumentu
     * <br/> Zaka�ada �e pole status ma cn = "STATUS"
     * @param doc
     * @param processName
     * @throws EdmException
     */
    protected void addRejectionToHistory(OfficeDocument doc, String processName) throws EdmException {
        String status = (String)doc.getFieldsManager().getValue("STATUS");
        addRejectionToHistory(doc,processName,status);
    }


    /**
     * Dodaje wpis do historii o odrzuceniu dokumentu
     * @param doc
     * @param processName
     * @param status <u>warto��</u> pola STATUS
     * @throws EdmException
     */
    protected void addRejectionToHistory(OfficeDocument doc, String processName, String status) throws EdmException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(processName);
        ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REJECTED);
        String divisions = "";
        for (DSDivision div : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
        {
            if (!divisions.equals(""))
                divisions = divisions + " / " + div.getName();
            else
                divisions = div.getName();
        }
        ahe.setSourceGuid(divisions);
        try {
            if (status != null){
                ahe.setStatus(status);
            }
        } catch (Exception e){
            log.warn(e.getMessage());
            throw new EdmException(e);
        }
        ahe.setCtime(new Date());

        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        doc.addAssignmentHistoryEntry(ahe);
    }


    /**
     * Dodaje wpis do historii o zako�czeniu procesu
     * @param doc
     * @param processName
     * @throws EdmException
     */
    protected void addFinishToHistory(OfficeDocument doc, String processName) throws EdmException {
        doc.addAssignmentHistoryEntry(
                new AssignmentHistoryEntry(DSApi.context().getPrincipalName(),
                        processName, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION, AssignmentHistoryEntry.EntryType.JBPM_FINISH)
        );
    }

    /**
     * Dodaje wpis do historii o dokonaniu eksportu
     * @param doc
     * @param processName
     * @throws EdmException
     */
    protected void addExportToHistory(OfficeDocument doc,String processName) throws EdmException {
                    doc.addAssignmentHistoryEntry(
                            new AssignmentHistoryEntry(DSApi.context().getPrincipalName(),
                                    processName,AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION, AssignmentHistoryEntry.EntryType.JBPM_EXPORT_FINISH)
                    );

    }


    /**
     * Dodaje <b>tylko</b> wpis do historii o zaakceptowaniu
     * <br/> Zaka�ada �e pole status ma cn = "STATUS"
     * @param doc
     * @param processName
     * @param type typ wpisu do historii <br/>
     *           Je�eli ustawione na "end-task" to dodaje wpis typu <code>EntryType.JBPM_END_TASK</code>
     * @throws pl.compan.docusafe.core.EdmException
     */
    protected void addAcceptationToHistory(OfficeDocument doc, String processName, String type) throws EdmException {
        String status = (String)doc.getFieldsManager().getValue("STATUS");
        addAcceptationToHistory(doc, processName, status, type);
    }

    /**
     * Dodaje <b>tylko</b> wpis do historii o zaakceptowaniu
     * @param doc
     * @param processName
     * @param status <u>warto��</u> pola STATUS
     * @param type typ wpisu do historii <br/>
     *               Je�eli ustawione na "end-task" to dodaje wpis typu <code>EntryType.JBPM_END_TASK</code>
     * @throws EdmException
     */
    protected void addAcceptationToHistory(OfficeDocument doc,String processName, String status, String type) throws EdmException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(processName);
        if (type != null && "end-task".equals(type)){
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM_END_TASK);
        }else{
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_ACCEPTED);
        }
        String divisions = "";

        for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
        {
            if (!divisions.equals("")){
                divisions = divisions + " / " + a.getName();
            }else{
                divisions = a.getName();
            }
        }
        ahe.setSourceGuid(divisions);

        if (status != null)
            ahe.setStatus(status);

        ahe.setCtime(new Date());

        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);

        doc.addAssignmentHistoryEntry(ahe);
    }
    /**
     * Dodanie wpisu do histori dekretacji  z uwzglednieniem cy cel (objective) dekretacji ma byc wziety ze statusu dokumentu 
     * @param docId
     * @param task
     * @param fromStatus
     * @throws EdmException
     */
    public void addCandidatesToHistory(Long docId,DelegateTask task, boolean objectivefromStatus) throws EdmException {
        OfficeDocument doc = OfficeDocument.findOfficeDocument(docId,false);
		String objective = "";
		if (objectivefromStatus)
		{

			Field f = doc.getFieldsManager().getField("STATUS");
			if (f != null)
			{
				EnumItem eitem = doc.getFieldsManager().getEnumItem("STATUS");
				if (eitem != null && eitem.getTitle() != null && !eitem.getTitle().isEmpty())
					objective = eitem.getTitle();
				if(task.getDescription()==null )
					task.setDescription(objective);
			}
		}
        String processName = task.getProcessDefinitionId();
        for(IdentityLink candidate : task.getCandidates()){
            String userName = candidate.getUserId();
            String guid = candidate.getGroupId();
            if (userName != null && StringUtils.isNotBlank(userName) && StringUtils.isBlank(guid)) {
                DSUser user = DSUser.findByUsername(userName);
                if (user != null ) {
                    DSDivision[] div = user.getOriginalDivisionsWithoutGroup();
                    if(div.length > 0){
                        guid = div[0].getGuid();
                    }
                }
            }


            addCandidatesToHistory(doc, processName, userName, guid);
        }
    }

    /**
     * Dodaje <b>tylko</b> wpis do historii o korekcji
     * <br/> Zaka�ada �e pole status ma cn = "STATUS"
     * @param doc
     * @param processName
     * @throws pl.compan.docusafe.core.EdmException
     */
    private void addCorrectionToHistory(OfficeDocument doc, String processName) throws EdmException {
        String status = (String)doc.getFieldsManager().getValue("STATUS");
        addCorrectionToHistory(doc,processName,status);
    }


    /**
     * Dodaje <b>tylko</b> wpis do historii o korekcji
     * @param doc
     * @param processName
     * @param status <u>warto��</u> pola STATUS
     * @throws EdmException
     */
    private void addCorrectionToHistory(OfficeDocument doc, String processName, String status) throws EdmException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(processName);
        ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_CORRECTION);

        String divisions = "";
        for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
        {
            if (!divisions.equals(""))
                divisions = divisions + " / " + a.getName();
            else
                divisions = a.getName();
        }
        ahe.setSourceGuid(divisions);
        ahe.setStatus(status);

        ahe.setCtime(new Date());

        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        doc.addAssignmentHistoryEntry(ahe);
        if (AvailabilityManager.isAvailable("addToWatch")){
            DSApi.context().watch(URN.create(doc));
        }
    } 

    //____________________GETTERS & SETTERS _____________________

    public String getHistoryEntryType() {
        return historyEntryType;
    }

    public void setHistoryEntryType(String historyEntryType) {
        this.historyEntryType = historyEntryType;
    }
}
