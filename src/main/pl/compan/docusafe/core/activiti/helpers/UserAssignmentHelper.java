package pl.compan.docusafe.core.activiti.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.base.Joiner;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.activiti.NotFoundSolution;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.uek.WyjazdZagranicznyLogic;
import pl.compan.docusafe.util.StringManager;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *  Activiti/Spring Bean s�u��ca do standardowego wyszukiwania user�w do przypisania
 */

@Component("userAssignmentHelper")
public class UserAssignmentHelper {

	
	private final static StringManager SM = GlobalPreferences.loadPropertiesFile(UserAssignmentHelper.class.getPackage().getName(), null);
	
    @Autowired
    private ActivitiDockindHelper activitiDockindHelper;




    /**
     * Returns Author of document
     * @param docId
     * @return Author of document with given id
     * @throws EdmException if document or author were not found
     */
    public String author(Long docId) throws EdmException {
        return author(docId, NotFoundSolution.EXCEPTION.name());
    }


    /**
     * Returns Author of document,
     * <br/>if user not found takes action dependant of given <code>solution</code>
     * @param docId
     * @param solution
     * @return Author of document with given id
     * @throws EdmException if document was not found
     */
    public String author(Long docId, String solution) throws EdmException {
        Document doc = Document.find(docId,false);
        if (doc == null) {
            throw new UserNotFoundException("Nie znaleziono autora dokumentu o id="+docId);
        }
        return NotFoundSolution.checkCandidates(doc.getAuthor(),solution);
    }





    /**
     * Returns comma separated users names from given collection of users names
     * @param usersNames
     * @return
     * @throws EdmException if no user was found
     */
    public String usersFromNamesCollection(Collection<String> usersNames) throws EdmException {
        return usersFromNamesCollection(usersNames,NotFoundSolution.EXCEPTION.name());
    }

    /**
     * Returns comma separated users names from given collection of users names
     * <br/>if user not found takes action dependant of given <code>solution</code>
     * @param usersNames
     * @return
     * @throws EdmException
     */
    public static String usersFromNamesCollection(Collection<String> usersNames, String solution) throws EdmException {
        StringBuilder names = new StringBuilder();
        for(String name : usersNames){
            names.append(name).append(",");
        }
        if(names.lastIndexOf(",") != -1){
            names.deleteCharAt(names.lastIndexOf(","));
        }
        return NotFoundSolution.checkCandidates(names.toString(),solution);
    }

    /**
     * Returns comma separated users names from given collection of users
     * @param users
     * @return
     * @throws EdmException if no user was found
     */
    public static String usersFromDSUserCollection(Collection<DSUser> users) throws EdmException {
        return usersFromDSUserCollection(users,NotFoundSolution.EXCEPTION.name());
    }

    /**
     * Returns comma separated users names from given collection of users
     * <br/>if user not found takes action dependant of given <code>solution</code>
     * @param users
     * @return
     * @throws EdmException
     */
    public static String usersFromDSUserCollection(Collection<DSUser> users, String solution) throws EdmException {
        StringBuilder names = new StringBuilder();
        for(DSUser user : users){
            names.append(user.getName()).append(",");
        }
        if(names.lastIndexOf(",") != -1){
            names.deleteCharAt(names.lastIndexOf(","));
        }
        return NotFoundSolution.checkCandidates(names.toString(),solution);
    }


    /**
     *  Returns user from addition property
     * @param usernameFromAdds
     * @return
     * @throws EdmException if no user was found
     */
    public String userFromAdds(String usernameFromAdds) throws EdmException {
        return userFromAdds(usernameFromAdds, NotFoundSolution.EXCEPTION.name());
    }


    /**
     *  Returns user from addition property
     * <br/>if user not found takes action dependant of given <code>solution</code>
     * @param usernameFromAdds
     * @return
     * @throws EdmException
     */
    public String userFromAdds(String usernameFromAdds, String solution) throws EdmException {
        String username = Docusafe.getAdditionProperty(usernameFromAdds);
        return NotFoundSolution.checkCandidates(username,solution);
    }
    /**
     *  Returns users from division
     * @param divisionGuid
     * @return
     * @throws EdmException if no user was found
     */

    public String divisionFromAdds(String divisionGuid) throws EdmException {
        return divisionFromAdds(divisionGuid,NotFoundSolution.EXCEPTION.name());
    }
    /**
     *  Returns users from division
     * <br/>if user not found takes action dependant of given <code>solution</code>
     * @param divisionGuid
     * @return
     * @throws EdmException
     */
    public String divisionFromAdds(String divisionGuid, String solution) throws EdmException {
        String guid = Docusafe.getAdditionProperty(divisionGuid);
        DSDivision div = DSDivision.find(guid);
        Collection<DSUser> users = Arrays.asList(div.getUsers());
        return usersFromDSUserCollection(users,solution);
    }


    public String getUserAcceptingForStatusByCn(Long docId, String statusCn) throws EdmException {
        Document doc = Document.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        String username = FieldsManagerUtils.getUserNameFromAcceptance(fm, statusCn);
        if (username==null) throw new EdmException("User has not been found in document acceptations");
        return username;
    }


    public String getUserAcceptingForStatusById(Long docId, Long statusId) throws EdmException {
        Document doc = Document.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        String statusCn = fm.getField("STATUS").getEnumItem(statusId.intValue()).getCn();
        String username = FieldsManagerUtils.getUserNameFromAcceptance(fm, statusCn);
        if (username==null) throw new EdmException("User has not been found in document acceptations");
        return username;
    }
    
    /**Metoda zwraca przelozonych zalogowanego uzytkownika. (userName,userName...)
     * Kiedy isAvailable decretationToAdminWhenUserNotFound == true i nie znaleziono przelozonego
     * pismo jest dekretowane do admina. Kiedy isAvailable decretationToAdminWhenUserNotFound == fasle
     * i nie znaleziono przelozonego jest rzucany stosowny komunikat.
     * @return userSupervisors
     * @throws EdmException
     */
    public String getUserSupervisors() throws EdmException {
        UserImpl currentUser = (UserImpl) UserImpl.getLoggedInUserSafely();
        Set<String> userSupervisors = new HashSet<String>();
        
        if (currentUser.getSupervisor() != null) {
        	userSupervisors.add(currentUser.getSupervisor().getName());
        }
        
        if (currentUser.getSupervisors() != null && currentUser.getSupervisors().size() > 0) {
        	for (UserImpl user : currentUser.getSupervisors()) {
        		userSupervisors.add(user.getName());
			}
        }
        
        if (AvailabilityManager.isAvailable("decretationToAdminWhenUserNotFound") && userSupervisors.size() == 0) {
        	userSupervisors.add("admin");
        }
        
        if (!AvailabilityManager.isAvailable("decretationToAdminWhenUserNotFound") && userSupervisors.size() == 0) {
        	throw new EdmException(SM.getString("userSupervisorNotFoundInfo", currentUser.asLastnameFirstname())); 
        }
        
        return Joiner.on(",").join(userSupervisors);
    }


//    public String getLastUserAccepting(Long docId) throws EdmException {
//        Document doc = Document.find(docId);
//        FieldsManager fm = doc.getFieldsManager();
//        //String statusCn = fm.getEnumItemCn("STATUS");
//        String username = FieldsManagerUtils.getUserNameFromLastAcceptance(fm);
//        if (username==null) throw new EdmException("User has not been found in document acceptations");
//        return username;
//    }
}
