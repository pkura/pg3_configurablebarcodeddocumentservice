package pl.compan.docusafe.core.activiti.helpers;

import org.springframework.stereotype.Component;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.activiti.NotFoundSolution;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Activiti/Spring Bean s�u��ca do wyci�gania danych z p�l z dokumentu
 */
@Component("activitiDockindHelper")
public class ActivitiDockindHelper {

    private final String STATUS = "STATUS";
    public static final String ADMIN_NAME = "admin";

    private final static Logger log = LoggerFactory.getLogger(ActivitiDockindHelper.class);

    /**
     * Returns boolean value from boolean field
     * @param docId
     * @param fieldCn
     * @return boolean value from boolean field
     * @throws EdmException if there is no field of given cn
     */
    public boolean getBoolFieldValue(Long docId, String fieldCn) throws EdmException {
        Document doc = Document.find(docId);
        FieldsManager fm = doc.getFieldsManager();

        log.info("documentid = " + docId + " field = " + fieldCn);
        if (fm.getField(fieldCn) != null) {
            return fm.getBoolean(fieldCn);
        }else{
            throw new EdmException("Brak pola o cn = " + fieldCn);
        }
    }

    /**
     * Sets fiels to given value in document with given id
     * @param docId
     * @param fieldCn field cn to which given value should be set
     * @param fieldValue value to set
     * @throws EdmException
     */
    public void setFieldValue(Long docId, String fieldCn, String fieldValue) throws EdmException {
        OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
        doc.getDocumentKind().setOnly(docId, Collections.singletonMap(fieldCn, fieldValue), false);
    }


    /**
     * Returns current cn of STATUS field
     * <br/> Assumes that STATUS field cn = "STATUS"
     * @param docId
     * @return
     * @throws EdmException current cn of STATUS field
     */
    public String getStatusCn(Long docId) throws EdmException {
        return getEnumItemCn(docId,STATUS);
    }
    /**
     * Returns current cn of given enum field
     * @param docId
     * @param enumFieldCn
     * @return
     * @throws EdmException current cn of enum field
     */
    public String getEnumItemCn(Long docId, String enumFieldCn) throws EdmException {
        OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
        return doc.getFieldsManager().getEnumItemCn(enumFieldCn);
    }
    
    public EnumItem getEnumItemField(Long docId, String enumFieldCn) throws EdmException {
        OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
        return doc.getFieldsManager().getEnumItem(enumFieldCn);
    }

    /**
     * Returns current id of STATUS field
     * <br/> Assumes that STATUS field cn = "STATUS"
     * @param docId
     * @return
     * @throws EdmException current id of STATUS field
     */
    public Integer getStatusId(Long docId) throws EdmException {
        return getEnumItemId(docId,STATUS);
    }
    /**
     * Returns current id of given enum field
     * <br/> Assumes that STATUS field cn = "STATUS"
     * @param docId
     * @param enumFieldCn
     * @return
     * @throws EdmException current id of given enum field
     */
    public Integer getEnumItemId(Long docId, String enumFieldCn) throws EdmException {
        OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
        return doc.getFieldsManager().getEnumItem(enumFieldCn).getId();
    }

    /**
     * Sets STATUS to given statusCn
     * <br/> Assumes that STATUS field statusCn = "STATUS"
     * @param docId
     * @param statusCn
     * @throws EdmException if there is no CN = statusCn in Status field
     */
    public void setStatusCn(Long docId, String statusCn) throws EdmException {
        OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
        EnumItem item = doc.getFieldsManager().getField(STATUS).getEnumItemByCn(statusCn);
        if (item == null) {
            throw new EdmException("Nie znaleziono STATUSU o statusCn = " + statusCn);
        }
        setStatus(docId,item.getId());
    }

    /**
     * Sets STATUS to given statusId
     * <br/> Assumes that STATUS field statusCn = "STATUS"
     * @param docId
     * @param statusId
     * @throws EdmException
     */
    public void setStatus(Long docId, Integer statusId) throws EdmException {
        setFieldValue(docId,STATUS,statusId.toString());
    }

    /**
     *Returns list of recipients
     * param: Long docId
     * @return List with recipients
     * @throws EdmException if there no any recipients
     */

    public List<String> getRecipients(Long docId) throws EdmException {

        try{

            OfficeDocument doc = OfficeDocument.find(docId);
            DSUser user;
            List<Recipient> recipients = doc.getRecipients();

            ArrayList<String> out = new ArrayList<String>();

            //String out = "";
           // Iterator iterator = recipients.iterator();

            for (Recipient r : recipients) {

                //iterator.next();
                user = extractUserFromRecipent(r);
                out.add(user.getName());
               // out = (out + user.getName());
               // if(iterator.hasNext()){
               //     out = (out+",");
              //  }

            }

            if (out.isEmpty()) {
                throw new UserNotFoundException("No recipients...");
            }

            return out;

        }catch(EdmException e) {

            throw e;
        }
    }


    /**
     *Returns members of AcceptanceDivision
     * param: Long docId
     * @return List with AcceptanceDivision members
     * @throws EdmException if there no any members
     */


    public List<String> getAcceptanceDivisionMembers(String Code) throws EdmException {

        //String out = "";
        List<AcceptanceCondition> members = AcceptanceCondition.find(Code);
        ArrayList<String> out = new ArrayList<String>();

       // Iterator iterator = members.iterator();

        for (AcceptanceCondition accept : members)
        {
            //iterator.next();

            if (accept.getUsername() != null ){

                out.add(accept.getUsername());
               // out = out + accept.getUsername();
              //  if(iterator.hasNext()) {
              //      out = out + ",";
              //  }
            }

        }

        if (out.isEmpty()){
            throw new EdmException("Brak drzewa akceptacji o podanym kodzie lub brak cz�onk�w");
        }

        return out;

    }


    private String extractGuidFromRecipent(Recipient recipient) {
        String guid = null;
        Pattern pat = Pattern.compile( ".*;d:(\\w*)");
        Matcher m = pat.matcher(recipient.getLparam());
        if(m.find()){
            guid = m.group(1);
        }
        return guid;
    }


    private DSUser extractUserFromRecipent(Recipient recipient) throws EdmException {
        if(recipient == null){
            throw new NullPointerException();
        }
        String firstName = recipient.getFirstname();
        String lastName = recipient.getLastname();

        DSUser user = DSUser.findByFirstnameLastname(firstName, lastName);
        String guid = extractGuidFromRecipent(recipient);
        if (user != null && guid != null && DSDivision.isInDivision(guid, user.getName())) {
            return user;
        }else if(guid != null && firstName != null && lastName != null){
            user = null;
            DSDivision div = DSDivision.find(guid);
            DSUser[] users = div.getUsers();
            for(DSUser u : users){
                if(firstName.equals(u.getFirstname()) && lastName.equals(u.getLastname())){
                    user = u;
                    break;
                }
            }
        }

        return user;
    }


    /**
     *Returns name of author supervisor
     * param: Long docId
     * @return String with name of author supervisor
     * @throws EdmException if there no supervisor
     */


    public String getAuthorSupervisor(Long docId) throws EdmException, UserNotFoundException {

        try{

            OfficeDocument doc = OfficeDocument.find(docId);
            DSUser user = DSUser.findByUsername(doc.getAuthor());
            DSUser supervisor = user.getSupervisor();

            if(supervisor == null) {

                throw new UserNotFoundException("Brak przelozonego");
            }
            else
            return supervisor.getName();

        }catch(UserNotFoundException e) {
            e.printStackTrace();
            throw e;
        }

    }

    public boolean isEmptyDictionary(long docId, String cnDictionary){
        Object o= null;
        try {
            OfficeDocument doc = OfficeDocument.find(docId);
            o = doc.getFieldsManager().getKey(cnDictionary);
        } catch (EdmException e) {
            e.printStackTrace();
        }
        if(o==null || (o instanceof List && ((List)o).isEmpty())){
            return true;
        }
        return false;
    }


    
    /**
     * Return user name from given field
     * @param docId
     * @param cn Field cn
     * @return user name
     */
    public String getDSUserFieldValueByCn(Long docId,String cn){

        try{

            Document document =  Document.find(docId);
            FieldsManager fm = document.getFieldsManager();
            long userId = fm.getEnumItem(cn).getId();
            return DSUser.findById(userId).getName();

        }catch (Exception e) {
            e.printStackTrace();
        }

        return ADMIN_NAME;
    }

    /**
     *
     * Return document author name
     * @param docId
     * @return
     */
    public String getAuthor(Long docId){

        try{

            OfficeDocument doc = OfficeDocument.find(docId);
            DSUser user = DSUser.findByUsername(doc.getAuthor());
            return  user.getName();

        }catch (Exception e) {
            e.printStackTrace();
        }

        return ADMIN_NAME;
    }
    
    
}
