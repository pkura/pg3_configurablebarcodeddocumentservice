package pl.compan.docusafe.core.activiti.helpers;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ActivitiWorkflowUtils {

    private final static Logger log = LoggerFactory.getLogger(ActivitiWorkflowUtils.class);
    private final static ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();

    public ActivitiWorkflowUtils() {}

    public void refreshTasklistAfterTimer(final Long docId) {
        Runnable task = new Runnable() {
            public void run() {
                boolean wasOpened = false;
                try {
                    wasOpened = DSApi.openContextIfNeeded();
                    try {
                        DSApi.context().begin();
                        TaskSnapshot.updateByDocumentId(docId, "anyType");
                        DSApi.context().commit();
                    } catch (Exception e) {
                        DSApi.context()._rollback();
                        log.error(e.getMessage(),e);
                    }
                } catch (EdmException e) {
                    log.error(e.getMessage(),e);
                } finally {
                    DSApi.closeContextIfNeeded(wasOpened);
                }
            }
        };
        worker.schedule(task, 2, TimeUnit.SECONDS);
    }
}
