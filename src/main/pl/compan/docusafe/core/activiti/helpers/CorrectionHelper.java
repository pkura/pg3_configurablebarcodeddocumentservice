package pl.compan.docusafe.core.activiti.helpers;

import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.activiti.listeners.AddToHistoryListener;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.jbpm4.CorrectionAssigner;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Damian on 11.04.14.
 */

@Component("correctionHelper")
public class CorrectionHelper {
    private final static Logger log = LoggerFactory.getLogger(CorrectionHelper.class);
    public final String CORRECTION_BACK = "correctionBack";
    public final String CORRECTION_FROM_USER = "correctionFromUser";
    public final String CORRECTION_FROM_STATE = "correctionFromState";
    public final String CORRECTION_USER_VARIABLE = "correctionUserKey";
    public final String CORRECTION_STATE_VARIABLE = "correctionState";
    public final String CORRECTION_ACCEPTANCE_VARIABLE = "acceptanceId";
    private final String NO_ONE = "noone";

    @Autowired
    private ActivitiDockindHelper activitiDockindHelper;
    @Autowired
    private AddToHistoryListener addToHistoryListener;


    public void prepareBasicCorrection(Long docId, DelegateExecution execution) throws EdmException {
        String state;
        if (execution.hasVariable(CORRECTION_STATE_VARIABLE)) {
            state = execution.getVariable(CORRECTION_STATE_VARIABLE).toString();
        }else{
            throw new EdmException("Zmienna " + CORRECTION_STATE_VARIABLE + " nie zosta�a zainicjalizowana");
        }

        prepareVariablesCorrectionFROM(docId, execution);
        clearAcceptances(docId,execution,state);
    }


    /**
     * Returns user that is set in <code>correctionUserKey</code>,
     * also sets <code>correctionUserKey = "noone"</code> and remove <code>correctionState</code> variable
     * or "noone" if there is no variable <code>correctionUserKey</code>
     * @param execution
     * @return user that is set in <code>correctionUserKey</code>
     */
    public String userFromBasicCorrection(DelegateExecution execution) throws EdmException {
        if(execution.hasVariable(CORRECTION_USER_VARIABLE)){
            String userName = execution.getVariable(CORRECTION_USER_VARIABLE).toString();

            if(!NO_ONE.equals(userName)){
                afterCorrection(execution);
            }
            return userName;
        }else{
            return NO_ONE;
        }
    }

    public void clearAfterCorrection(DelegateExecution execution) throws EdmException {
        String cUser = execution.getVariable(CORRECTION_USER_VARIABLE).toString();
        if(cUser != null && !"noone".equals(cUser)){
            afterCorrection(execution);
        }
    }




    /**
     * Returns Collection with user that is set in <code>correctionUserKey</code>,
     * also sets <code>correctionUserKey = "noone"</code> and remove <code>correctionState</code> variable
     * @param execution
     * @return Returns Collection with user that is set in <code>correctionUserKey</code>
     * or array with "noone" if there is no variable <code>correctionUserKey</code>
     */
    public Collection<String> userCollectionFromBasicCorrection(DelegateExecution execution) throws EdmException {
        String userName = userFromBasicCorrection(execution);
        Collection<String> names = new ArrayList<String>();
        names.add(userName);
        return names;
    }


    /**
     * Prepare process variables correctionFromUser and correctionFromState
     *
     * @param docId
     * @param execution
     * @throws pl.compan.docusafe.core.EdmException
     */
    private void prepareVariablesCorrectionFROM(Long docId, DelegateExecution execution) throws EdmException {
        String userName = DSApi.context().getPrincipalName();
        execution.setVariable(CORRECTION_FROM_USER, userName);
        String statusCn =  activitiDockindHelper.getStatusCn(docId);
        execution.setVariable(CORRECTION_FROM_STATE, statusCn);
    }


    /**
     * Cleans entries in DocumentAcceptance
     *
     * @param docId
     * @param execution
     * @param state
     * @throws pl.compan.docusafe.core.EdmException
     */
    private void clearAcceptances(Long docId, DelegateExecution execution, String state) throws EdmException {
        if (execution.hasVariable(CORRECTION_ACCEPTANCE_VARIABLE)) {
            Long accId = null;
            try {
                accId = (Long)execution.getVariable(CORRECTION_ACCEPTANCE_VARIABLE);
                if (accId != null) {
                    DocumentAcceptance acc = DocumentAcceptance.findById(accId);
                    if (acc != null) {
                        CorrectionAssigner.clearAcceptance(accId, docId, state);
                    }
                }
            } catch (ClassCastException e) {
                log.error("B��d przy rzutowaniu id akceptacji na potrzeby clearAcceptance");
            }
        } else {
            CorrectionAssigner.clearAcceptance(docId, state);
        }
    }

    /**
     * Sets <code>correctionUserKey = "noone"</code> and remove <code>correctionState</code> variable <br/>
     * Adds correction entry to History
     * @param execution
     * @throws EdmException
     */
    private void afterCorrection(DelegateExecution execution) throws EdmException {
        execution.removeVariable(CORRECTION_STATE_VARIABLE);
        execution.setVariable(CORRECTION_USER_VARIABLE,NO_ONE);
        Long docId = (Long) execution.getVariable("docId");
        addToHistoryListener.addCorrectionToHistory(docId,execution);
    }

    /**
     */
    public void prepareCorrectionBack(DelegateExecution execution) throws EdmException {
        execution.setVariable(CORRECTION_BACK,true);
        Long docId = (Long) execution.getVariable("docId");
        addToHistoryListener.addCorrectionToHistory(docId,execution);
    }
    /**
     */
    public void clearCorrectionBack(DelegateExecution execution) throws EdmException {
        execution.removeVariable(CORRECTION_BACK);
    }
    /**
     */
    public boolean isCorrectionBack(DelegateExecution execution) throws EdmException {
        Object isCorrectionBack = execution.getVariable(CORRECTION_BACK);
        return isCorrectionBack != null && (Boolean) isCorrectionBack;
    }
}
