/**
 * 
 */
package pl.compan.docusafe.core.activiti.helpers;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Joiner;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 12-05-2014
 * ds_trunk_2014
 * DivisionAssigmentHelper.java
 */

@Component("divisionAssignmentHelper")
public class DivisionAssigmentHelper {
	
	private final static Logger log = LoggerFactory.getLogger(DivisionAssigmentHelper.class);
	
	private final static StringManager SM = GlobalPreferences.loadPropertiesFile(DivisionAssigmentHelper.class.getPackage().getName(), null);

	@Autowired
    private ActivitiDockindHelper activitiDockindHelper;
	
	private Set<String> getUserDivisionsGuids() throws EdmException {
		UserImpl currentUser = (UserImpl) UserImpl.getLoggedInUserSafely();
		Set<String> userDivisions = new HashSet<String>();
		
		String[] guids = currentUser.getDivisionsGuidWithoutGroup();
		
		if (guids != null && guids.length > 0) {
			for (String guid : guids) {
				userDivisions.add(guid);
			}
		} else {
			throw new EdmException(SM.getString("userWithNoDivision", currentUser.asLastnameFirstname())); 
		}
		
		return userDivisions;
	}
	
	private Set<String> getDocumentAuthorDivisionsGuids(Long docId) throws EdmException {
		OfficeDocument officeDocument;
		DSUser authorUser;
		Set<String> userDivisions = new HashSet<String>();

		try {
			officeDocument = OfficeDocument.find(docId);
			
			if (officeDocument != null) {
				String author = officeDocument.getAuthor();
				authorUser = DSUser.findByUsername(author);
				
				String[] guids = authorUser.getDivisionsGuidWithoutGroup();
				
				if (guids != null && guids.length > 0) {
					for (String guid : guids) {
						userDivisions.add(guid);
					}
				} 
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		if (userDivisions.size() == 0) {
			throw new EdmException(SM.getString("userWithNoDivision", null)); 
		}
		
		return userDivisions;
	}
	
}
