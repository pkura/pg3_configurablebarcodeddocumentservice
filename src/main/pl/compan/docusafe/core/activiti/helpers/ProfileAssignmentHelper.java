package pl.compan.docusafe.core.activiti.helpers;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.activiti.NotFoundSolution;
import pl.compan.docusafe.core.jbpm4.ProfileInDivisionAssignmentHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;

import java.util.*;

/**
 *  Activiti/Spring Bean s�u��ca do standardowego wyszukiwania po Profilach
 */
@Component("profileAssignmentHelper")
public class ProfileAssignmentHelper {


    /**
     * Returns String of comma separated names of all users in profile with given id
     * @param profileId
     * @return String of comma separated names users
     * @throws EdmException if no user was found
     */
    public String allUsersWithProfile(Long profileId) throws EdmException {
        return allUsersWithProfile(profileId, NotFoundSolution.EXCEPTION.name());
    }

    /**
     * Returns String of comma separated names of all users in profile with given name
     * @param profileName
     * @return String of comma separated names users
     * @throws EdmException if no user was found
     */
    public String allUsersWithProfile(String profileName) throws EdmException {
        return allUsersWithProfile(profileName, NotFoundSolution.EXCEPTION.name());
    }

    /**
     * Returns String of comma separated names of all users in profile with id from adds.properties
     * @param profileIdAdds
     * @return String of comma separated names users
     * @throws EdmException if no user was found
     */
    public String allUsersWithProfileIdFromAdds(String profileIdAdds) throws EdmException {
        return allUsersWithProfileIdFromAdds(profileIdAdds, NotFoundSolution.EXCEPTION.name());
    }

    /**
     * Returns String of comma separated names of all users in set of profiles with id from adds.properties
     * @param profilesIdsAdds
     * @return String of comma separated names users
     * @throws EdmException if no user was found
     */
    public String allUsersWithProfilesIdsFromAdds(String... profilesIdsAdds) throws EdmException {
        return allUsersWithProfilesIdsFromAddsSolution(NotFoundSolution.EXCEPTION.name(), profilesIdsAdds);
    }

    /**
     * Returns String of comma separated names of all users in profile with name from adds.properties
     * @param profileNameAdds
     * @return String of comma separated names users
     * @throws EdmException if no user was found
     */
    public String allUsersWithProfileNameFromAdds(String profileNameAdds) throws EdmException {
        return allUsersWithProfileNameFromAdds(profileNameAdds, NotFoundSolution.EXCEPTION.name());
    }

    /**
     * Returns String of comma separated names of all users in profile with given id
     * <br/> If no users was found takes action dependant on given solution
     * @param profileId
     * @param solution name of solution to choose action to take if no user was found
     * @return String of comma separated names users
     * @throws EdmException
     */
    public String allUsersWithProfile(Long profileId, String solution) throws EdmException {
        Profile profile = Profile.findById(profileId);
        return getUsersToAssignFromProfile(profile, solution);
    }

    /**
     * Returns String of comma separated names of all users in profile with given name
     *  <br/> If no users was found takes action dependant on given solution
     * @param profileName
     * @param solution name of solution to choose action to take if no user was found
     * @return String of comma separated names users
     * @throws EdmException
     */
    public String allUsersWithProfile(String profileName, String solution) throws EdmException {
        Profile profile = Profile.findByProfilename(profileName);
        return getUsersToAssignFromProfile(profile,solution);
    }

    /**
     * Returns String of comma separated names of all users in profile with id from adds.properties
     *  <br/> If no users was found takes action dependant on given solution
     * @param profileIdAdds
     * @param solution name of solution to choose action to take if no user was found
     * @return String of comma separated names users
     * @throws EdmException
     */
    public String allUsersWithProfileIdFromAdds(String profileIdAdds,String solution) throws EdmException {
        int profileId = Docusafe.getIntAdditionPropertyOrDefault(profileIdAdds, -1);
        return allUsersWithProfile(Long.valueOf(profileId),solution);
    }

    /**
     * Returns String of comma separated names of all users in profile with id from adds.properties
     *  <br/> If no users was found takes action dependant on given solution
     * @param profilesIdsAdds
     * @param solution name of solution to choose action to take if no user was found
     * @return String of comma separated names users
     * @throws EdmException
     */
    public String allUsersWithProfilesIdsFromAddsSolution(String solution, String... profilesIdsAdds) throws EdmException {
        String usersNames;
//        try{

            final int PROFILE_NOT_FOUND = -1;
            if (profilesIdsAdds.length==0) throw new EdmException("NO PROFILE ID ADDS");

            int [] profilesIds = new int [profilesIdsAdds.length];
            for (int i = 0; i < profilesIdsAdds.length; ++i){
                profilesIds[i] = Docusafe.getIntAdditionPropertyOrDefault(profilesIdsAdds[i], PROFILE_NOT_FOUND);
                if (profilesIds[i]==PROFILE_NOT_FOUND)
                    throw new EdmException("NOT FOUND PROFILE ID ADDS: " + profilesIdsAdds[i]);
            }

            Profile [] profiles = new Profile [profilesIdsAdds.length];
            for (int i = 0; i < profilesIds.length; ++i){
                Profile profile = Profile.findById(Long.valueOf(profilesIds[i]));
                profiles[i] = profile;
            }

            Set<DSUser> users = profiles[0].getUsers();
            Iterator<DSUser> iterator = users.iterator();
            while (iterator.hasNext()) {
                DSUser user = iterator.next();
                String [] userProfiles = user.getProfileNames();

                for (int i = 1; i < profiles.length; ++i){
                    String requiredProfile = profiles[i].getName();

                    boolean contains = false;
                    for (String upn : userProfiles){
                        if (upn.equalsIgnoreCase(requiredProfile)){
                            contains = true;
                            break;
                        }
                    }
                    if (!contains){
                        iterator.remove();
                        break;
                    }
                }
            }

            Set<String> usersNamesSet = new HashSet<String>();
            for (DSUser user : users)
                usersNamesSet.add(user.getName());
            usersNames = StringUtils.join(usersNamesSet.iterator(),',');


//        }catch(EdmException e){
//            //log.error(e.getMessage(),e);
//            usersNames = "";
//        }

        return NotFoundSolution.checkCandidates(usersNames, solution);
    }

    /**
     * Returns String of comma separated names of all users in profile with name from adds.properties
     *  <br/> If no users was found takes action dependant on given solution
     * @param profileNameAdds
     * @param solution name of solution to choose action to take if no user was found
     * @return String of comma separated names users
     * @throws EdmException
     */
    public String allUsersWithProfileNameFromAdds(String profileNameAdds,String solution) throws EdmException {
        String profileName = Docusafe.getAdditionPropertyOrDefault(profileNameAdds, "");
        return allUsersWithProfile(profileName, solution);
    }

    /**
     * Returns String of comma separated names of all users in divisions with profile with id from adds.properties
     * @param divisionId
     * @param profileIdAdds
     * @param scopeLvl how far up from given diviosion should search for users<br/>
     *                 <code>scopeLvl = -1</code> means search to root
     * @return String of comma separated names users
     * @throws EdmException if no user was found
     */
    public String usersFromDivisionWithProfileIdFromAdds(Long divisionId, String profileIdAdds, Integer scopeLvl) throws EdmException {
        return usersFromDivisionWithProfileIdFromAdds(divisionId, profileIdAdds,scopeLvl,NotFoundSolution.EXCEPTION.name());
    }

    /**
     * Returns String of comma separated names of all users in divisions with profile with id from adds.properties
     *  <br/> If no users was found takes action dependant on given solution
     * @param divisionId
     * @param profileIdAdds
     * @param scopeLvl how far up from given diviosion should search for users<br/>
     *                 <code>scopeLvl = -1</code> means search to root
     * @param solution name of solution to choose action to take if no user was found
     * @return String of comma separated names users
     * @throws EdmException
     */
    public String usersFromDivisionWithProfileIdFromAdds(Long divisionId, String profileIdAdds, Integer scopeLvl, String solution) throws EdmException {
        int profileId = Docusafe.getIntAdditionPropertyOrDefault(profileIdAdds, -1);
        return usersFromDivisionWithProfile(divisionId, Long.valueOf(profileId),scopeLvl, solution);
    }

    /**
     * Returns String of comma separated names of all users in divisions with profile with given id
     * @param divisionId
     * @param profileId
     * @param scopeLvl how far up from given diviosion should search for users<br/>
     *                 <code>scopeLvl = -1</code> means search to root
     * @return String of comma separated names users
     * @throws EdmException if no user was found
     */
    public String usersFromDivisionWithProfile(Long divisionId, Long profileId, Integer scopeLvl) throws EdmException {
        return usersFromDivisionWithProfile(divisionId, profileId,scopeLvl, NotFoundSolution.EXCEPTION.name());
    }

    /**
     * Returns String of comma separated names of all users in divisions with profile with given id
     * <br/> If no users was found takes action dependant on given solution
     * @param divisionId
     * @param profileId
     * @param scopeLvl how far up from given diviosion should search for users<br/>
     *                 <code>scopeLvl = -1</code> means search to root
     * @param solution name of solution to choose action to take if no user was found
     * @return String of comma separated names users
     * @throws EdmException
     */
    public String usersFromDivisionWithProfile(Long divisionId, Long profileId, Integer scopeLvl, String solution) throws EdmException {
        Profile profile = Profile.findById(profileId);
        return getUsersFromDivisionWithProfile(divisionId,profile,scopeLvl,solution);
    }


    /**
     * Returns String of comma separated names of all users in divisions with profile with name from adds.properties
     * @param divisionId
     * @param profileNameAdds
     * @param scopeLvl how far up from given diviosion should search for users<br/>
     *                 <code>scopeLvl = -1</code> means search to root
     * @return String of comma separated names users
     * @throws EdmException if no user was found
     */
    public String usersFromDivisionWithProfileNameFromAdds(Long divisionId,String profileNameAdds, Integer scopeLvl) throws EdmException {
        return usersFromDivisionWithProfileNameFromAdds(divisionId, profileNameAdds, scopeLvl, NotFoundSolution.EXCEPTION.name());
    }


    /**
     * Returns String of comma separated names of all users in divisions with profile with name from adds.properties
     *  <br/> If no users was found takes action dependant on given solution
     *  @param divisionId
     * @param profileNameAdds
     * @param scopeLvl how far up from given diviosion should search for users<br/>
     *                 <code>scopeLvl = -1</code> means search to root
     * @param solution name of solution to choose action to take if no user was found
     * @return String of comma separated names users
     * @throws EdmException
     */
    public String usersFromDivisionWithProfileNameFromAdds(Long divisionId,String profileNameAdds, Integer scopeLvl, String solution) throws EdmException {
        String profileName = Docusafe.getAdditionPropertyOrDefault(profileNameAdds, "");
        return usersFromDivisionWithProfile(divisionId, profileName, scopeLvl, solution);
    }

    /**
     * Returns String of comma separated names of all users in divisions with profile with given name
     * @param divisionId
     * @param profileName
     * @param scopeLvl how far up from given diviosion should search for users<br/>
     *                 <code>scopeLvl = -1</code> means search to root
     * @return String of comma separated names users
     * @throws EdmException if no user was found
     */
    public String usersFromDivisionWithProfile(Long divisionId, String profileName, Integer scopeLvl) throws EdmException {
        return usersFromDivisionWithProfile(divisionId, profileName, scopeLvl, NotFoundSolution.EXCEPTION.name());
    }

    /**
     * Returns String of comma separated names of all users in divisions with profile with given name
     *  <br/> If no users was found takes action dependant on given solution
     *  @param divisionId
     * @param profileName
     * @param scopeLvl how far up from given diviosion should search for users<br/>
     *                 <code>scopeLvl = -1</code> means search to root
     * @param solution name of solution to choose action to take if no user was found
     * @return String of comma separated names users
     * @throws EdmException
     */
    public String usersFromDivisionWithProfile(Long divisionId, String profileName, Integer scopeLvl, String solution) throws EdmException {
        Profile profile = Profile.findByProfilename(profileName);
        return getUsersFromDivisionWithProfile(divisionId,profile,scopeLvl,solution);
    }






    /**
     * Returns String of comma separated names of all users in profile
     *  <br/> If no users was found takes action dependant on given solution
     * @param profile
     * @param solution name of solution to choose action to take if no user was found
     * @return String of comma separated names of all users in profile
     * @throws EdmException
     */
    private String getUsersToAssignFromProfile(Profile profile, String solution) throws EdmException {
        DSUser[] users = profile.getUsersArray();
        StringBuilder names = new StringBuilder();
        if (users != null) {
            for(DSUser user: users){
                if(user.getName() != null){
                    names.append(user.getName())
                            .append(",");
                }
            }
            //usuni�cie ostatniego przecinka
            if(names.lastIndexOf(",") != -1){
                names.deleteCharAt(names.lastIndexOf(","));
            }
        }

        return NotFoundSolution.checkCandidates(names.toString(), solution);
    }

    /**
     * Returns String of name of the author of the document
     * @param docId document id
     * @return String of author name
     * @throws EdmException
     */
    public String getAuthor(Long docId) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        return doc.getAuthor();
    }

    /**
     * Returns String of comma separated names of <b>all<b/> users with given profile in division with given id
     * and its parent divisions in given scope
     * <br/> If no users was found takes action dependant on given solution
     * @param divisionId id of started division
     * @param profile
     * @param scopeLvl how far up from given diviosion should search for users<br/>
     *                 <code>scopeLvl = -1</code> means search to root
     * @param solution name of solution to choose action to take if no user was found
     * @return
     * @throws Exception
     */
    private String getUsersFromDivisionWithProfile(Long divisionId, Profile profile, Integer scopeLvl ,String solution) throws EdmException {
        List<String> userNames = new ArrayList<String>();
        try{
        ProfileInDivisionAssignmentHandler.putUsersFromProfile(userNames, divisionId, profile.getId(), false, scopeLvl, true);
        }catch(Exception e){
            throw new EdmException(e);
        }

        StringBuilder names = new StringBuilder();
        for(String name : userNames){
            names.append(name)
                    .append(",");
        }

        //usuni�cie ostatniego przecinka
        if(names.lastIndexOf(",") != -1){
            names.deleteCharAt(names.lastIndexOf(","));
        }
        return NotFoundSolution.checkCandidates(names.toString(),solution);
    }

}
