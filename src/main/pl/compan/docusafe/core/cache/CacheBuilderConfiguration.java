/**
 * 
 */
package pl.compan.docusafe.core.cache;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;


import pl.compan.docusafe.boot.Docusafe;

/**
 * Konfiguracja dla builder�w cache'a
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class CacheBuilderConfiguration
{
	static final int DEFAULT_CONCURRENCY_LEVEL = 20;
	
	static final int DEFAULT_EXPIRATION_TIME = 5;
	
	static final TimeUnit DEFAULT_TIME_UNIT = TimeUnit.MINUTES;
	
	static final String ADDS_CACHE_KEY = "cache";
	
	static final String ADDS_CONCURRENCY_LEVEL = "concurrencyLevel";
	
	static final String ADDS_EXPIRATION_TIME = "expirationTime";
	
	static final String ADDS_TIME_UNIT = "timeUnit";
	
	private int concurrencyLevel = DEFAULT_CONCURRENCY_LEVEL;
	
	private int expirationTime = DEFAULT_EXPIRATION_TIME;
	
	private TimeUnit timeUnit = DEFAULT_TIME_UNIT;
	
	/**
	 * @return the concurrencyLevel
	 */
	public int getConcurrencyLevel(String cacheName)
	{
		String concurrencyLevelAdds = getValue(cacheName, ADDS_CONCURRENCY_LEVEL);
		if (StringUtils.isNotEmpty(concurrencyLevelAdds) && StringUtils.isNumeric(concurrencyLevelAdds))
			concurrencyLevel = Integer.parseInt(concurrencyLevelAdds);
		
		return concurrencyLevel;
	}
	
	/**
	 * @return the expirationTime
	 */
	public int getExpirationTime(String cacheName)
	{
		String expirationTimeAdds = getValue(cacheName, ADDS_EXPIRATION_TIME);
		if (StringUtils.isNotEmpty(expirationTimeAdds) && StringUtils.isNumeric(expirationTimeAdds))
			expirationTime = Integer.parseInt(expirationTimeAdds);
		
		return expirationTime;
	}
	
	/**
	 * @return the timeUnit
	 */
	public TimeUnit getTimeUnit(String cacheName)
	{
		String timeUnitAdds = getValue(cacheName, ADDS_TIME_UNIT);
		if (StringUtils.isNotBlank(timeUnitAdds))
			timeUnit = TimeUnit.valueOf(timeUnitAdds.toUpperCase());
		
		return timeUnit;
	}
	
	public String getValue(String cacheName, String value)
	{
		return Docusafe.getAdditionProperty(ADDS_CACHE_KEY + "." + cacheName + "." + value);
	}
}
