/**
 * 
 */
package pl.compan.docusafe.core.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class OrganizationTreeCacheBuilder extends CacheBuilderConfiguration
	implements pl.compan.docusafe.core.cache.CacheBuilder<String, String>
{
	private static final String CACHE_NAME = "OrganizationTreeCache";
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.cache.CacheBuilder#build()
	 */
	@Override
	public Cache<String, String> build()
	{
		Cache<String, String> cache = CacheBuilder.newBuilder()
	         .softValues()
	         .concurrencyLevel(getConcurrencyLevel(CACHE_NAME))
	         .expireAfterWrite(getExpirationTime(CACHE_NAME), getTimeUnit(CACHE_NAME))
	         .build();
			
		return cache;
	}
}
