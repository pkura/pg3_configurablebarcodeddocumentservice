package pl.compan.docusafe.core.cache;

import pl.compan.docusafe.core.users.DSUser;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
interface CacheLogic {
    void onUserChanged(DSUser user);
}

class EmptyCacheLogic implements CacheLogic {
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.cache.CacheLogic#onUserChanged(pl.compan.docusafe.core.users.DSUser)
	 */
	@Override
	public void onUserChanged(DSUser user)
	{
		// TODO Auto-generated method stub
		
	}
}
