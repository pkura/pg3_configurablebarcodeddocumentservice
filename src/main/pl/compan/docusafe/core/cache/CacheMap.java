/**
 * 
 */
package pl.compan.docusafe.core.cache;

import java.util.Map;

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.collect.MapMaker;

/**
 * Mapa cache'y
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
class CacheMap<K, V>
{
	private Map<String, Cache<K, V>> caches = new MapMaker().weakValues().makeMap();
	
	/**
	 * @return the caches
	 */
	public Map<String, Cache<K, V>> getCaches()
	{
		return caches;
	}
	
	public boolean contains(String key)
	{
		return caches.containsKey(key);
	}
	
	public Cache<K, V> getCache(String key)
	{
		return caches.get(key);
	}
	
	public Cache<K, V> putCache(String key, Cache<K, V> cache)
	{
		return caches.put(key, cache);
	}
	
	public CacheMap<K, V> invalidateAll()
	{
		for (Cache<K, V> entry : caches.values())
			if (entry != null) 
				entry.invalidateAll();
		return this;
	}
	
	public CacheMap<K, V> invalidate(String key)
	{
		Preconditions.checkArgument(caches.containsKey(key), 
			"Cache o nazwie: " + key + " nie istnieje!");
		
		Cache<K, V> cache = caches.get(key);
		if (cache != null)
			cache.invalidateAll();
		
		return this;
	}

    public CacheMap<K,V> invalidate(String key, Object member)
    {
        Preconditions.checkArgument(caches.containsKey(key),
                "Cache o nazwie: " + key + " nie istnieje!");

        Cache<K, V> cache = caches.get(key);
        if (cache != null)
            cache.invalidate(member);

        return this;
    }
}