/**
 * 
 */
package pl.compan.docusafe.core.cache;

import java.util.List;

import pl.compan.docusafe.web.common.Node;
import pl.compan.docusafe.web.common.tag.SitemapTag.SitemapRequest;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * Budowniczy cache'u sitemapy
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class SitemapCacheBuilder extends CacheBuilderConfiguration 
	implements pl.compan.docusafe.core.cache.CacheBuilder<SitemapRequest, List<Node>>
{
	private static final String CACHE_NAME = "SitemapCache";

	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.cache.CacheBuilder#build()
	 */
	@Override
	public Cache<SitemapRequest, List<Node>> build()
	{
		Cache<SitemapRequest, List<Node>> cache = CacheBuilder.newBuilder()
	            .concurrencyLevel(getConcurrencyLevel(CACHE_NAME))
	            .expireAfterWrite(getExpirationTime(CACHE_NAME), getTimeUnit(CACHE_NAME))
	            .softValues()
	            .build();
		return cache;
	}	
}