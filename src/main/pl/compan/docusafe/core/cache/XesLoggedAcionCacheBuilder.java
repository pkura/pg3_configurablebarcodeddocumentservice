/**
 * 
 */
package pl.compan.docusafe.core.cache;

import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class XesLoggedAcionCacheBuilder extends CacheBuilderConfiguration
	implements pl.compan.docusafe.core.cache.CacheBuilder<String, Boolean>
{
	private static final String CACHE_NAME = "xesLoggedActions";
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.cache.CacheBuilder#build()
	 */
	@Override
	public Cache<String, Boolean> build()
	{
		Cache<String, Boolean> cache = CacheBuilder.newBuilder()
	         .softValues()
	         .concurrencyLevel(getConcurrencyLevel(CACHE_NAME))
	         .expireAfterAccess(10, DEFAULT_TIME_UNIT)
	         .build();
			
		return cache;
	}
}
