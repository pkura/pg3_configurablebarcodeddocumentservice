/**
 * 
 */
package pl.compan.docusafe.core.cache;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;


import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.collect.Lists;

/**
 * Cache Builder dla uprawnie�
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class PermissionCacheBuilder extends CacheBuilderConfiguration 
	implements pl.compan.docusafe.core.cache.CacheBuilder<Integer, Collection<String>>
{
	private static final String CACHE_NAME = "PermissionCache";
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.cache.CacheBuilder#build()
	 */
	@Override
	public Cache<Integer, Collection<String>> build()
	{
		Cache<Integer, Collection<String>> cache = CacheBuilder.newBuilder()
            .softValues()
            .concurrencyLevel(getConcurrencyLevel(CACHE_NAME))
            .expireAfterWrite(getExpirationTime(CACHE_NAME), getTimeUnit(CACHE_NAME))
            .build(new CacheLoader<Integer, Collection<String>>() {
                @Override
                public Collection<String> load(Integer s) throws Exception {
                    return loadPermissionForUser(s);
                }
            });
		
		return cache;
	}
	
	/**
	 * @param userId
	 * @return
	 * @throws EdmException
	 */
	private Collection<String> loadPermissionForUser(Integer userId) throws EdmException 
	{
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<String> ret = Lists.newArrayList();
        try {
            ps = DSApi.context().prepareStatement("select guid from ds_division d where \n" +
                    "exists ( \n" +
                    "select null from DS_USER_TO_DIVISION ud where ud.DIVISION_ID = d.ID and ud.USER_ID = ?)");
            ps.setObject(1,userId);
            
            rs = ps.executeQuery();
            
            
            while(rs.next()){
                ret.add(rs.getString(1));
            }
            return ret;
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
        } 
    }

}
