/**
 * 
 */
package pl.compan.docusafe.core.cache;

/**
 * Rodzaje dostępnych cachy w systemie
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public enum CacheDefinition
{
	SITE_MAP, // cache site mapy
	PREFERENCES, // cache ustawień użytkowników
	PERMISSION,
	ORGANIZATION_TREE,
	DIVISIONS_BY_USER;
}

