/**
 * 
 */
package pl.compan.docusafe.core.cache;

import com.google.common.cache.Cache;

/**
 * Budowniczy cache'u danego typu
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
interface CacheBuilder<K, V>
{	
	Cache<K, V> build();
}
