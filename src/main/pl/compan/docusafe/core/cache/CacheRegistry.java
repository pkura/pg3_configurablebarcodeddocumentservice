/**
 * 
 */
package pl.compan.docusafe.core.cache;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.collect.Lists;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Maps;

/**
 * Rejestr cachy systemowych
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class CacheRegistry
{
	/** Logger */
	private static final Logger LOG = LoggerFactory.getLogger(CacheRegistry.class);
	
	@SuppressWarnings("rawtypes")
	private Map<CacheDefinition, CacheBuilder> builders;
	
	/**
	 * Budowniczy cache'ow
	 */ 
	{
		builders = Maps.newHashMap();
		builders.put(CacheDefinition.SITE_MAP, new SitemapCacheBuilder());
		builders.put(CacheDefinition.PERMISSION, new PermissionCacheBuilder());
		builders.put(CacheDefinition.PREFERENCES, new PreferencesCacheBuilder());
		builders.put(CacheDefinition.ORGANIZATION_TREE, new OrganizationTreeCacheBuilder());
		
	}
	
	private static CacheRegistry registry = new CacheRegistry();
	
	@SuppressWarnings("rawtypes")
	private Map<CacheDefinition, CacheMap> cacheDefs;
	
	public static CacheRegistry instance()
	{
		return registry;
	}
	
	@SuppressWarnings("rawtypes")
	private CacheRegistry()
	{
		cacheDefs = new MapMaker().makeMap();
		for (CacheDefinition def : builders.keySet())
			cacheDefs.put(def, new CacheMap());
				
		LOG.debug("Utworzono instancje rejestru cache'y!");
	}
	
	/**
	 * Zwraca liste definicji cache'y jaka obsluguje rejestr
	 * @return
	 */
	public List<CacheDefinition> getDefinitions()
	{
		return Lists.newArrayList(cacheDefs.keySet());
	}
	
	/**
	 * Tworzy nowy cache
	 * @param def
	 * @param key
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Cache createNewCache(CacheDefinition def, String key)
	{
		Preconditions.checkArgument(cacheDefs.containsKey(def), 
			"Definicja cache o nazwie: " + def.name() + " nie istnieje!");
		
		Cache cache = putCache(def, key);
		LOG.debug("Utworzono instancje nowego cache: " + def.name() + " o kluczu: " + key);
		return cache;
	}
	
	/**
	 * Zwraca cache'a z danej definicji i o podanym kluczu. Jesli create=true to w przypadku nie istnienia
	 * danego cache tworzy jego definicj z buildera
	 * @param def
	 * @param key
	 * @param create
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Cache getCache(CacheDefinition def, String key, boolean create)
	{
		Preconditions.checkArgument(cacheDefs.containsKey(def), 
				"Definicja cache o nazwie: " + def.name() + " nie istnieje!");
		
		if (!cacheDefs.get(def).contains(key) && create)
			createNewCache(def, key);
		
		return cacheDefs.get(def).getCache(key);
	}
	
	/**
	 * Zwraca cache o podanej definicji i kluczu
	 * @param def
	 * @param key
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Cache getCache(CacheDefinition def, String key)
	{
		Preconditions.checkArgument(cacheDefs.containsKey(def), 
			"Definicja cache o nazwie: " + def.name() + " nie istnieje!");
		
		return cacheDefs.get(def).getCache(key);
	}
	
	/**
	 * Dodaje nowy cache o podanej definicji i kluczu
	 * @param def
	 * @param key
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Cache putCache(CacheDefinition def, String key)
	{
		Preconditions.checkArgument(cacheDefs.containsKey(def), 
			"Definicja cache o nazwie: " + def.name() + " nie istnieje!");
		
		Cache cache = cacheDefs.get(def).putCache(key, builders.get(def).build());
		LOG.debug("Dodano nowy cache: " + def.name() + " o kluczu: " + key);
		return cache;
	}
	
	/**
	 * Czyscie wszystkie cache z podanej definicji
	 * @param def
	 */
	public void clean(CacheDefinition def)
	{
		Preconditions.checkArgument(cacheDefs.containsKey(def), 
			"Definicja cache o nazwie: " + def.name() + " nie istnieje!");
		
		cacheDefs.get(def).invalidateAll();
		LOG.debug("Cache: " + def.name() + " zostal wyczyszczony.");
	}
	
	/**
	 * Czysci caly rejestr cache'u
	 */
	@SuppressWarnings("rawtypes")
	public void cleanAll()
	{
		for (CacheMap map : cacheDefs.values())
			map.invalidateAll();
		LOG.debug("Wyczyszczono wszsytkie cache...");
	}

    public void clean(CacheDefinition def, Object member)
    {
        Preconditions.checkArgument(cacheDefs.containsKey(def),"Definicja cache o nazwie: " + def.name() + " nie istnieje!");
        cacheDefs.get(def).invalidate("DEFAULT",member);
    }
}
