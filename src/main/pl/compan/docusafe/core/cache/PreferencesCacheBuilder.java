/**
 * 
 */
package pl.compan.docusafe.core.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import pl.compan.docusafe.core.cfg.prefs.SQLPreferences;

/**
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class PreferencesCacheBuilder extends CacheBuilderConfiguration
	implements pl.compan.docusafe.core.cache.CacheBuilder<String, SQLPreferences>
{
	private static final String CACHE_NAME = "PreferencesCache";
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.cache.CacheBuilder#build()
	 */
	@Override
	public Cache<String, SQLPreferences> build()
	{
		Cache<String, SQLPreferences> cache = CacheBuilder.newBuilder()
	         .softValues()
	         .concurrencyLevel(getConcurrencyLevel(CACHE_NAME))
	         .expireAfterWrite(getExpirationTime(CACHE_NAME), getTimeUnit(CACHE_NAME))
	         .build();
		
		return cache;
	}
}
