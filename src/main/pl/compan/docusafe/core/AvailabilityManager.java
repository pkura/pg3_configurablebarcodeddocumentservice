/*
 */

package pl.compan.docusafe.core;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;
import org.jbpm.api.TaskService;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 *  Klasa do zarz�dzania dost�pno�ci� poszczeg�lnych funkcji.
 *
 * @author Mariusz Kilja�czyk
 */
public class AvailabilityManager
{
    public static final String OUTOFFICEDOCUMENT_BIND_TO_JOURNAL = "outofficedocument.bindToJournal";
    private static final Log log = LogFactory.getLog(AvailabilityManager.class);
    public static final String SET_OFFICE_NUMBER_FROM_ARCHIVE_TAB = "setOfficeNumberFromArchiveTab";
    public static final String AVAILABILITY_MAP_PROCESS_VARIABLE = "availability-map";

    private static Map<String,Boolean> availableMap;

    public static Map<String, Boolean> getAvailableMap() {
		return availableMap;
	}

    public static Map<String,Map<String, Boolean>> availableMapForDockind;

	public static void setAvailableMap(Map<String, Boolean> availableMap) {
		AvailabilityManager.availableMap = availableMap;
	}

	/** Inicjuje mape dost�pnych adds�w  */
    public static void initAdds()
    {
    	if(availableMapForDockind == null)
    		availableMapForDockind = new HashMap<String, Map<String,Boolean>>();
    	availableMap = new HashMap<String, Boolean>();
        Properties availableTmp = new Properties();
        try
        {
        	Set<Object> keys;
        	for(String module :Docusafe.getAvailableModules())
        	{
        		try
        		{
	        		System.out.println("/WEB-INF/available/available"+module+".properties");
	        		availableTmp.load(Docusafe.getServletContext().getResourceAsStream("/WEB-INF/available/available"+module+".properties"));
	        		keys = availableTmp.keySet();
	                for(Object obj : keys)
	                {
	                    availableMap.put(obj.toString(), "true".equals(availableTmp.get(obj).toString().trim()));
	                }
        		}
        		catch (Exception e) {
					log.error("",e);
        		}
        	}
        	availableTmp.clear();
            availableTmp.load(Docusafe.class.getClassLoader().getResourceAsStream("available.properties"));
            keys = availableTmp.keySet();
            for(Object obj : keys)
            {
                if(!availableMap.containsKey(obj))
                    availableMap.put(obj.toString(), "true".equals(availableTmp.get(obj).toString().trim()));
            }
            availableTmp.clear();
            if(Configuration.hasExtra("business"))
            {
                availableTmp.load(Docusafe.class.getClassLoader().getResourceAsStream("availableBusiness.properties"));
            }
            else
            {
                availableTmp.load(Docusafe.class.getClassLoader().getResourceAsStream("availableAdministration.properties"));
            }
            keys = availableTmp.keySet();
            for(Object obj : keys)
            {
                availableMap.put(obj.toString(), "true".equals(availableTmp.get(obj).toString().trim()));
            }
            availableTmp.clear();
            availableTmp.load(Docusafe.class.getClassLoader().getResourceAsStream("adds.properties"));
            keys = availableTmp.keySet();
            for(Object obj : keys)
            {
                availableMap.put(obj.toString(), "true".equals(availableTmp.get(obj).toString().trim()));
            }

            File tmp = new File(Docusafe.getHome() + "/adds.properties");
            if(tmp.exists())
            {
            	availableTmp.clear();
                availableTmp.load(new FileInputStream(tmp));
                if(availableTmp != null)
                {
                    keys = availableTmp.keySet();
                    for(Object obj : keys)
                    {
                        availableMap.put(obj.toString(), "true".equals(availableTmp.get(obj).toString().trim()));
                    }
                }
            }

            tmp = new File(Docusafe.getHome() + "/available.properties");
            if(tmp.exists())
            {
            	availableTmp.clear();
                availableTmp.load(new FileInputStream(tmp));
                if(availableTmp != null)
                {
                    keys = availableTmp.keySet();
                    for(Object obj : keys)
                    {
                    	try
                    	{
                    		boolean bool = Boolean.parseBoolean((String)availableTmp.get(obj).toString().trim());
                    		availableMap.put(obj.toString(), bool);
                    	}
                    	catch (Exception e)
						{
							log.error("B��d odczytu available.properties dla "+obj+" :"+e);
							availableMap.put(obj.toString(), false);
						}
                    }
                }
            }
        }
        catch (IOException e)
        {
        	log.error("", e);
        }
    }
    /***
     * Metoda wczytuj�ca indywidualne abvailable dla dokinda,
     * Wywo�ywana podczas parsowania xml-a
     * @param rootElement -element <availableProperties>
     * @param cn - nazwa kodowa dockinda
     */
    public static void readAvailable(Element rootElement,String cn)
    {
    	if(availableMapForDockind == null)
    		availableMapForDockind = new HashMap<String, Map<String,Boolean>>();
    	if(rootElement != null)
    	{
    		List<org.dom4j.Element> elAvailableList = rootElement.elements("available");
	        if (elAvailableList != null)
	        {
	        	HashMap<String, Boolean> tmpMap = new HashMap<String, Boolean>(availableMap);
	            for (Element element : elAvailableList)
	            {
	            	tmpMap.put(element.attributeValue("name"),"true".equals(element.attributeValue("value")));
				}
	            availableMapForDockind.put(cn,tmpMap);
	        }
    	}
    }

    /**
     * Medoda spawdza czy jest dostepna dana funkcja poprzez sprawdzenie zmiennej procesu, zmiennych dockinda, available.properties
     * @param key - klucz do sprawdzenia
     * @param dockindCn - cn dockinda
     * @param taskId - id procesu
     * @return true or false
     */
    public static boolean isAvailable(String key, String dockindCn, String taskId) {
        Boolean available = null;
        if(taskId != null && taskId.length() > 0) {
            taskId = taskId.contains(",") ? taskId.split(",")[1] : taskId;
            TaskService taskService = Jbpm4Provider.getInstance().getTaskService();
            Map<String,Boolean> thisAvailable =null;

            //TODO przerobi� do obs�ugi zar�wno JBPM jak i ACTIVITI
            //Hax �eby si� nie wywala�o jak nie task jest z activiti
            // !!!! problem b�dzie jak metoda p�jdzie z taska activiti a jbpm b�dzie mia� task o takim samym id !!!
            if(taskService != null && taskService.getTask(taskId) != null){
                thisAvailable = (Map<String,Boolean>) taskService.getVariable(taskId, AVAILABILITY_MAP_PROCESS_VARIABLE);
            }
            available = thisAvailable != null ? thisAvailable.get(key) : null;
        }
        return available != null ? available.booleanValue() : isAvailable(key, dockindCn);
    }

    /** Sprawdza czy dostepna jest dana funkcja, jesli dockindCn != null sprawdza w ustawieniach dockinda
     *  domyslne dost�pne funkcje s� zapisane w pliku available.properties
     *  s� te� pliki availableAdministration.properties i availableBusiness.properties
     *  */
    public static Boolean isAvailable(String x,String dockindCn)
    {
    	Map<String,Boolean> thisAvailable = null;
    	if(dockindCn != null){
    		thisAvailable = availableMapForDockind.get(dockindCn);
        }
    	if(thisAvailable == null){
    		thisAvailable = availableMap;
        }

        String [] splitX = x.split(".");
        
        if(splitX.length < 2)
        {
        	if(!thisAvailable.containsKey(x))
        		return false;
            return thisAvailable.get(x);
        }
        else
        {
           for (int i = 0; i < splitX.length; i++)
           {
               if(!thisAvailable.get(splitX[i]))
                   return false;
           }
           return  thisAvailable.get(splitX[splitX.length-1]);
        }
    }

    /** Sprawdza czy dostepna jest dana funkcja
     *  domyslne dost�pne funkcje s� zapisane w pliku available.properties
     *  s� te� pliki availableAdministration.properties i availableBusiness.properties
     *  */
    public static Boolean isAvailable(String x)
    {
    	if( availableMap == null )		// null pointer fix - przy generowaniu HOME
    		return false;
    	
    	//trzeba najpierw sprawdzi czy nie spowoduje bledow u klientow z powodu wpisywania Available do addsow
    	//return new Boolean(NewAvailabilityManager.isAvailable(x));
        String [] splitX = x.split(".");
        if(splitX.length < 2)
        {
        	if(!availableMap.containsKey(x))
        		return false;
            return availableMap.get(x);
        }
        else
        {
           for (int i = 0; i < splitX.length; i++)
           {
               if(!availableMap.get(splitX[i]))
                   return false;
           }
           return  availableMap.get(splitX[splitX.length-1]);
        }
    }
}
