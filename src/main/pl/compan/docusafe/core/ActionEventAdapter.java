package pl.compan.docusafe.core;

import pl.compan.docusafe.webwork.event.ActionEvent;

/**
* @author Micha� Sankowski <michal.sankowski@docusafe.pl>
*/
public class ActionEventAdapter implements Messages {
    final ActionEvent event;

    public ActionEventAdapter(ActionEvent event) {
        this.event = event;
    }

    public void addActionError(String s) {
        event.addActionError(s);
    }

    public void addActionMessage(String s) {
        event.addActionMessage(s);
    }
}
