package pl.compan.docusafe.core.db.criteria;

import org.apache.commons.lang.StringUtils;

/**
 * Native Join
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeJoin
{
	/** Nazwa tabeli */
	private String tableName;
	
	/** Alias tabeli */
	private String tableAlias;
	
	/** Typ join */
	private JoinType joinType;
	
	/** lewa kolumna ��cz�ca */
	private String leftColumn;
	
	/** prawa kolumna ��cz�ca */
	private String rightColumn;
	
	public enum JoinType
	{
		INNER ("INNER JOIN"),
		NATURAL ("NATURAL JOIN"),
		CROSS ("CROSS JOIN"),
		LEFT_OUTER ("LEFT OUTER JOIN"),
		RIGHT_OUTER ("RIGHT OUTER JOIN"),
		FULL_OUTER ("FULL OUTER");
		
		private String type;
		
		private JoinType(String type)
		{
			this.type = type;
		}
		
		public String getType()
		{
			return type;
		}
	}
	
	/**
	 * Konstruktor 
	 * @param tableName
	 * @param tableAlias
	 * @param joinType
	 * @param rightColumn
	 * @param leftColumn
	 */
	public NativeJoin(String tableName, String tableAlias, JoinType joinType, String leftColumn, String rightColumn)
	{
		if (StringUtils.isBlank(tableName))
			throw new IllegalStateException("Warto�� tableName jest pusta!");
		if (StringUtils.isBlank(tableAlias))
			throw new IllegalStateException("Warto�� tableAlias jest pusta!");
		if (joinType == null)
			throw new IllegalStateException("Warto�� joinTYpe jest null!");
		else if (joinType.equals(JoinType.NATURAL) || joinType.equals(JoinType.CROSS))
			throw new IllegalArgumentException("Ten konstruktor nie obs�uguje typu natural i cross join!");
		if (StringUtils.isBlank(rightColumn) || StringUtils.isBlank(leftColumn))
			throw new IllegalStateException("Nieprawid�owo podane nazwy kolumn ��cz�cych!");
		
		this.tableName = tableName;
		this.tableAlias = tableAlias;
		this.joinType = joinType;
		this.leftColumn = leftColumn;
		this.rightColumn = rightColumn;
	}
	
	/**
	 * Konstruktor
	 * @param tableName
	 * @param tableAlias
	 * @param joinType
	 */
	public NativeJoin(String tableName, String tableAlias, JoinType joinType)
	{
		if (StringUtils.isBlank(tableName))
			throw new IllegalStateException("Warto�� tableName jest pusta!");
		if (StringUtils.isBlank(tableAlias))
			throw new IllegalStateException("Warto�� tableAlias jest pusta!");
		if (joinType == null)
			throw new IllegalStateException("Warto�� joinTYpe jest null!");
		else if (!joinType.equals(JoinType.NATURAL) && !joinType.equals(JoinType.CROSS))
			throw new IllegalArgumentException("Ten konstruktor obs�uguje tylko natural join i cross join");
		
		this.tableName = tableName;
		this.tableAlias = tableAlias;
		this.joinType = joinType;
	}
	
	public String getTableName()
	{
		return tableName;
	}
	
	public String getTableAlias()
	{
		return tableAlias;
	}
	
	public String toSQL()
	{
		String joinSQL = joinType.getType() + " " + tableName + " " + tableAlias;
		if (joinType.equals(JoinType.NATURAL) || joinType.equals(JoinType.CROSS))
			return joinSQL;
		
		return joinSQL + " ON " + leftColumn + " = " + rightColumn;
	}
}
