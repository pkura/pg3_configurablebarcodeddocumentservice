package pl.compan.docusafe.core.db.criteria;

import java.util.Collection;
import java.util.List;

import pl.compan.docusafe.core.db.criteria.NativeJoin.JoinType;

/**
 * Klasa do budowania wyra�e� native SQL
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeExps
{
	/**
	 * Wyra�enie r�wno�ci
	 * @param columnName
	 * @param value
	 * @return
	 */
	public static NativeExp eq(String columnName, Object value)
	{
		return new NativeEqExp(columnName, value);
	}
	
	/**
	 * Wyra�enie nie r�wno�ci
	 * @param columnName
	 * @param value
	 * @return
	 */
	public static NativeExp notEq(String columnName, Object value)
	{
		return new NativeNotEqExp(columnName, value);
	}
	
	/**
	 * Wyra�enie in dla kolekcji
	 * @param columnName
	 * @param values
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static NativeExp in(String columnName, List values)
	{
		return new NativeInExp(columnName, values);
	}
	
	/**
	 * Wyra�enie in dla tablicy
	 * @param columnName
	 * @param values
	 * @return
	 */
	public static NativeExp in(String columnName, Object[] values)
	{
		return new NativeInExp(columnName, values);
	}
	
	/**
	 * Wyra�enie not in dla kolekcji
	 * @param columnName
	 * @param values
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static NativeExp notIn(String columnName, List values)
	{
		return new NativeNotInExp(columnName, values);
	}
	
	/**
	 * Wyra�enie not in dla tablicy
	 * @param columnName
	 * @param values
	 * @return
	 */
	public static NativeExp notIn(String columnName, Object[] values)
	{
		return new NativeNotInExp(columnName, values);
	}
	
	/**
	 * Wyra�enie is not null
	 * @param columnName
	 * @return
	 */
	public static NativeExp isNotNull(String columnName)
	{
		return new NativeIsNotNullExp(columnName);
	}
	
	/**
	 * Wyra�enie is null
	 * @param columnName
	 * @return
	 */
	public static NativeExp isNull(String columnName)
	{
		return new NativeIsNullExp(columnName);
	}
	
	/**
	 * Wyra�enie like
	 * @param columnName
	 * @param value
	 * @return
	 */
	public static NativeExp like(String columnName, String value)
	{
		return new NativeLikeExp(columnName, value);
	}
	
	public static NativeExp likeCaseInsensitive(String columnName, String value) {
		return new NativeLikeExp(columnName, value, true);
	}
	
	/**
	 * Wyra�enie like
	 * @param columnName
	 * @param value
	 * @return
	 */
	public static NativeExp notLike(String columnName, String value)
	{
		return new NativeNotLikeExp(columnName, value);
	}
	
	/**
	 * Wyra�enie between
	 * @param columnName
	 * @param lowValue
	 * @param highValue
	 * @return
	 */
	public static NativeExp between(String columnName, Object lowValue, Object highValue)
	{
		return new NativeBetweenExp(columnName, lowValue, highValue);
	}
	
	/**
	 * Wyra�enie not between
	 * @param columnName
	 * @param lowValue
	 * @param highValue
	 * @return
	 */
	public static NativeExp notBetween(String columnName, Object lowValue, Object highValue)
	{
		return new NativeNotBetweenExp(columnName, lowValue, highValue);
	}
	
	/**
	 * Wyra�enie wi�kszo�ci
	 * @param columnName
	 * @param value
	 * @return
	 */
	public static NativeExp gt(String columnName, Object value)
	{
		return new NativeGtExp(columnName, value);
	}
	
	/**
	 * Wyra�enie wi�ksze r�wne
	 * @param columnName
	 * @param value
	 * @return
	 */
	public static NativeExp gte(String columnName, Object value)
	{
		return new NativeGteExp(columnName, value);
	}
	
	/**
	 * Wyra�enie mniejszo�ci
	 * @param columnName
	 * @param value
	 * @return
	 */
	public static NativeExp lt(String columnName, Object value)
	{
		return new NativeLtExp(columnName, value);
	}
	
	/**
	 * Wyra�enie mniejsze r�wne
	 * @param columnName
	 * @param value
	 * @return
	 */
	public static NativeExp lte(String columnName, Object value)
	{
		return new NativeLteExp(columnName, value);
	}
	
	/**
	 * Tworzy obiekt order by dla native SQL
	 * @return
	 */
	public static NativeOrderExp order()
	{
		return new NativeOrderExp();
	}
	
	/**
	 * Projeckcja
	 * @return
	 */
	public static NativeProjection projection()
	{
		return new NativeProjection();
	}
	
	/**
	 * Inner join
	 * @param tableName
	 * @param tableAlias
	 * @param rightColumn
	 * @param leftColumn
	 * @return
	 */
	public static NativeJoin innerJoin(String tableName, String tableAlias, String leftColumn, String rightColumn)
	{
		return new NativeJoin(tableName, tableAlias, JoinType.INNER, leftColumn, rightColumn);
	}
	
	/**
	 * Right outer join
	 * @param tableName
	 * @param tableAlias
	 * @param rightColumn
	 * @param leftColumn
	 * @return
	 */
	public static NativeJoin rightJoin(String tableName, String tableAlias, String leftColumn, String rightColumn)
	{
		return new NativeJoin(tableName, tableAlias, JoinType.RIGHT_OUTER, leftColumn, rightColumn);
	}
	
	/**
	 * Left outer join
	 * @param tableName
	 * @param tableAlias
	 * @param rightColumn
	 * @param leftColumn
	 * @return
	 */
	public static NativeJoin leftJoin(String tableName, String tableAlias, String leftColumn, String rightColumn)
	{
		return new NativeJoin(tableName, tableAlias, JoinType.LEFT_OUTER, leftColumn, rightColumn);
	}
	
	/**
	 * Full outer join
	 * @param tableName
	 * @param tableAlias
	 * @param rightColumn
	 * @param leftColumn
	 * @return
	 */
	public static NativeJoin fullJoin(String tableName, String tableAlias, String leftColumn, String rightColumn)
	{
		return new NativeJoin(tableName, tableAlias, JoinType.FULL_OUTER, leftColumn, rightColumn);
	}
	
	/**
	 * Inner join
	 * @param tableName
	 * @param tableAlias
	 * @param rightColumn
	 * @param leftColumn
	 * @return
	 */
	public static NativeJoin naturalJoin(String tableName, String tableAlias)
	{
		return new NativeJoin(tableName, tableAlias, JoinType.NATURAL);
	}
	
	/**
	 * Cross join
	 * @param tableName
	 * @param tableAlias
	 * @param rightColumn
	 * @param leftColumn
	 * @return
	 */
	public static NativeJoin crossJoin(String tableName, String tableAlias)
	{
		return new NativeJoin(tableName, tableAlias, JoinType.CROSS);
	}
	
	/**
	 * Krzy�owanie wyra�e� SQL z operatorem AND
	 * @return
	 */
	public static NativeJunctionExp conjunction()
	{
		return new NativeConjunctionExp();
	}
	
	/**
	 * Krzy�owanie wyra�e� SQL z operatorem AND
	 * @return
	 */
	public static NativeJunctionExp conjunction(List<NativeExp> exps)
	{
		return new NativeConjunctionExp(exps);
	}
	
	/**
	 * Krzy�owanie wyra�e� SQL z operatorem OR
	 * @return
	 */
	public static NativeJunctionExp disjunction()
	{
		return new NativeDisjunctionExp();
	}
	
	/**
	 * Krzy�owanie wyra�e� SQL z operatorem OR
	 * @return
	 */
	public static NativeJunctionExp disjunction(List<NativeExp> exps)
	{
		return new NativeDisjunctionExp(exps);
	}

	/**
	 * Contains dla full-text-search
	 *
	 * @return
	 */
	public static NativeContainsExp contains(String columnName, String value)
	{
		return new NativeContainsExp(columnName, value);
	}
}
