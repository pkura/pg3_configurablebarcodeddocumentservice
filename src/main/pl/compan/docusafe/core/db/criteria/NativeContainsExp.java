package pl.compan.docusafe.core.db.criteria;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Pobiera rekordy poprzez full-text-search.
 *
 * User: Administrator
 * Date: 03.07.13
 * Time: 11:12
 */
public class NativeContainsExp implements NativeExp
{
	/** Nazwa kolumny */
	private String columnName;

	/** Nazwa zmiennej */
	private String varName;

	/** Warto�� por�wnywana */
	private String value;

	public NativeContainsExp(String columnName, String value)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Wartość columnName jest pusta!");
		if (StringUtils.isBlank(value))
			throw new IllegalStateException("Wartość value jest pusta!");
		this.columnName = columnName;
		if (DSApi.isSqlServer())
			this.value = "\"*" + value + "*\"";
		else if (DSApi.isOracleServer())
			this.value = "%" + value + "%";
	}

	@Override
	public String toSQL()
	{
		varName = VarGenerator.gen(columnName);
		if (DSApi.isSqlServer())
		{
			return "contains(" + columnName + ", :" + varName + ")";
		}
		else if (DSApi.isOracleServer())
			return "contains(" + columnName + ", :" + varName + ") > 0";
		else
			return "";
	}

	@Override
	public void setValues(SQLQuery query)
	{
		query.setString(varName, value);
	}
}
