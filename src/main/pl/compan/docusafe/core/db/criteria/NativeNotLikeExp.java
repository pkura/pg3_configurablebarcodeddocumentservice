package pl.compan.docusafe.core.db.criteria;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;

/**
 * Wyra�enie NOT LIKE w native SQL
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeNotLikeExp implements NativeExp
{
	/** Nazwa kolumny */
	private String columnName;
	
	/** Nazwa zmiennej */
	private String varName;
	
	/** Warto�� por�wnywana */
	private String value;
	
	/**
	 * Konstruktor
	 * @param columnName
	 * @param value
	 */
	public NativeNotLikeExp(String columnName, String value)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		if (StringUtils.isBlank(value))
			throw new IllegalStateException("Warto�� value jest pusta!");
		
		this.columnName = columnName;
		this.value = value;
	}
	
	public String toSQL()
	{
		varName = VarGenerator.gen(columnName);
		return columnName + " NOT LIKE ?";
	}
	
	public void setValues(SQLQuery query)
	{
		query.setString(varName, value);
	}
}
