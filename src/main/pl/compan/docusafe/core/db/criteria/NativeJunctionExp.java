package pl.compan.docusafe.core.db.criteria;

import java.util.List;

/**
 * Interfejs dla klas krzyżujących wyrażenia SQL
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public interface NativeJunctionExp extends NativeExp
{
	public NativeJunctionExp add(NativeExp exp);
	public NativeJunctionExp add(List<NativeExp> exps);
}
