package pl.compan.docusafe.core.db.criteria;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;

public class NativeInExp implements NativeExp
{
	/** Nazwa kolumny */
	private String columnName;
	
	/** Warto�ci w kolekcji */
	@SuppressWarnings("unchecked")
	private List values;
	
	/** Warto�ci w tablicy */
	private Object[] arrValues;
	
	/** Nazwa zmiennej */
	private String varName;
	
	/**
	 * Konstruktor z warto�ciami jako kolekcja
	 * @param columnName
	 * @param values
	 */
	@SuppressWarnings("unchecked")
	public NativeInExp(String columnName, List values)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		if (values == null)
			throw new IllegalStateException("Warto�� values jest null!");
		
		this.columnName = columnName;
		this.values = values;
	}
	
	/**
	 * Konstruktor z warto�ciami jako tablica
	 * @param columnName
	 * @param values
	 */
	public NativeInExp(String columnName, Object[] values)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		if (values == null)
			throw new IllegalStateException("Warto�� values jest null!");
		
		this.columnName = columnName;
		this.arrValues = values;
	}
	
	public String toSQL()
	{
		varName = VarGenerator.gen(columnName);
		return columnName + " IN (:" + varName + ")";
	}
	
	/**
	 * Ustawia warto�ci
	 */
	public void setValues(SQLQuery query)
	{
		if (values != null)
			query.setParameterList(varName, values);
		else if (arrValues != null)
			query.setParameterList(varName, arrValues);
	}
}
