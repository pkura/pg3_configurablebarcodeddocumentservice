package pl.compan.docusafe.core.db.criteria;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;

/**
 * Wyra�enie IS NOT NULL w native SQL
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeIsNotNullExp implements NativeExp
{
	/** Nazwa kolumny */
	private String columnName;
	
	public NativeIsNotNullExp(String columnName)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		
		this.columnName = columnName;
	}
	
	public String toSQL()
	{
		return columnName + " IS NOT NULL";
	}
	
	public void setValues(SQLQuery query)
	{
		// -------
	}
}
