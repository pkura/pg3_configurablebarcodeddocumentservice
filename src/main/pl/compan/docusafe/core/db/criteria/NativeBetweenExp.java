package pl.compan.docusafe.core.db.criteria;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;

/**
 * Wyra�enie between native SQL.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeBetweenExp implements NativeExp
{
	/** Nazwa kolumny */
	private String columnName;
	
	/** Mniejsza warto�� */
	private Object lowValue;
	
	/** Nazwa zmiennej lowValue */
	private String lowValueVar;
	
	/** Wi�ksza warto�� */
	private Object highValue;
	
	/** Nazwa zmiennej dla highValue */
	private String highValueVar;
	
	/**
	 * Konstruktor
	 * @param columnName Nazwa kolumny
	 * @param lowValue Wi�ksza warto��
	 * @param highValue Mniejsza warto��
	 */
	public NativeBetweenExp(String columnName, Object lowValue, Object highValue)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		if (lowValue == null)
			throw new IllegalStateException("Warto�� lowValue jest null!");
		if (highValue == null)
			throw new IllegalStateException("Warto�� highValue jest null!");
		
		this.columnName = columnName;
		this.lowValue = lowValue;
		this.highValue = highValue;
	}
	
	public String toSQL()
	{
		lowValueVar = VarGenerator.gen(columnName);
		highValueVar = VarGenerator.gen(columnName);
		return columnName + " BETWEEN :" + lowValueVar + " AND :" + highValueVar;
	}
	
	/**
	 * Ustawia warto�ci
	 */
	public void setValues(SQLQuery query)
	{
		query.setParameter(lowValueVar, lowValue);
		query.setParameter(highValueVar, highValue);
	}
}
