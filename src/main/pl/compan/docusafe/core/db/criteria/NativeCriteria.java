package pl.compan.docusafe.core.db.criteria;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa do budowania zapyta� native SQL.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeCriteria
{
	/** LOGGER */
	private static final Logger LOG = LoggerFactory.getLogger(NativeCriteria.class);
	
	/** Obiekt sesji hibernate */
	private Session session;
	
	/** Tabele z aliasami dla klauzuli FROM */
	private Map<String, String> tables;
	
	/** Jointy ;) */
	private List<NativeJoin> joins;
	
	/** Wyra�enia SQL dla klauzuli where */
	private Map<NativeExp, Operator> whereExps;
	
	/** Wyra�enia SQL dla klauzluli having */
	private Map<NativeExp, Operator> havingExps;
	
	/** Wyra�enie order */
	private NativeOrderExp orderExp;
	
	/** limit */
	private Integer limit;
	
	/** offset */
	private Integer offset;
	
	/** czy distinct */
	private boolean distinct;
	
	/** Projekcje */
	private NativeProjection projection;
	
	/** Operatory SQL */
	private enum Operator 
	{
		AND ("AND"),
		OR ("OR");
		
		private String value;
		
		private Operator(String value)
		{
			this.value = value;
		}
		
		public String getValue()
		{
			return value;
		}
	}
	
	/**
	 * Konstruktor
	 * @param session
	 */
	public NativeCriteria(Session session, String mainTable, String mainAlias)
	{
		if (session == null)
			throw new IllegalStateException("Obiekt sesji ma warto�� null!");
		if (StringUtils.isBlank(mainTable))
			throw new IllegalStateException("Brak nazwy dla tabeli g��wnej");
		if (StringUtils.isBlank(mainAlias))
			throw new IllegalStateException("Brak aliasu dla tabeli g��wnej");
		
		this.session = session;
		this.distinct = false;
		
		// tabela g��wna
		this.tables = new LinkedHashMap<String, String>();
		this.tables.put(mainTable, mainAlias);
		
		this.whereExps = new LinkedHashMap<NativeExp, Operator>();
		this.havingExps = new LinkedHashMap<NativeExp, Operator>();
		this.joins = new ArrayList<NativeJoin>();
	}
	
	/**
	 * Dodanie tabeli do klauzuli FROM
	 * @param tableName
	 * @param tableAlias
	 * @return
	 */
	public NativeCriteria addTable(String tableName, String tableAlias)
	{
		this.tables.put(tableName, tableAlias);
		return this;
	}
	
	/**
	 * Ustawienie projekcji
	 * @param projection
	 */
	public NativeCriteria setProjection(NativeProjection projection)
	{
		this.projection = projection;
		return this;
	}
	
	/**
	 * Dodanie nowego warunku do zapytania
	 * @param exp
	 * @return
	 */
	public NativeCriteria add(NativeExp exp)
	{
		if (exp == null)
			throw new IllegalStateException("Obiekt exp ma warto�� null!");

		whereExps.put(exp, Operator.AND);
		return this;
	}
	
	/**
	 * Dodanie warunku "or" do zapytania
	 * @param exp
	 * @return
	 */
	public NativeCriteria addOr(NativeExp exp)
	{
		if (exp == null)
			throw new IllegalStateException("Obiekt exp ma warto�� null!");
		
		whereExps.put(exp, Operator.OR);
		return this;
	}
	
	/**
	 * Dodaje having AND
	 * @param exp
	 * @return
	 */
	public NativeCriteria addHaving(NativeExp exp)
	{
		if (exp == null)
			throw new IllegalStateException("Obiekt exp ma warto�� null!");
		
		havingExps.put(exp, Operator.AND);
		return this;
	}
	
	/**
	 * Dodaje having OR
	 * @param exp
	 * @return
	 */
	public NativeCriteria addOrHaving(NativeExp exp)
	{
		if (exp == null)
			throw new IllegalStateException("Obiekt exp ma warto�� null!");
		
		havingExps.put(exp, Operator.OR);
		return this;
	}
	
	/**
	 * Dodaje joina
	 * @param join
	 * @return
	 */
	public NativeCriteria addJoin(NativeJoin join)
	{
		if (join == null)
			throw new IllegalStateException("Obiekt join ma warto�� null!");
		
		joins.add(join);
		return this;
	}
	
	/**
	 * Dodanie parametru order by
	 * @param orderExp
	 * @return
	 */
	public NativeCriteria addOrder(NativeOrderExp orderExp)
	{
		if (orderExp == null)
			throw new IllegalStateException("Obiekt orderExp ma warto�� null!");
		
		this.orderExp = orderExp;
		return this;
	}
	
	/**
	 * Ustawia wartos� distinct
	 * @param distinct
	 */
	public NativeCriteria setDistinct(boolean distinct)
	{
		this.distinct = distinct;
		return this;
	}
	
	/**
	 * Ustawienie limitu
	 */
	public NativeCriteria setLimit(Integer limit)
	{
		this.limit = limit;
		return this;
	}
	
	/**
	 * Ustawienie offsetu
	 * @param offset
	 */
	public NativeCriteria setOffset(Integer offset)
	{
		this.offset = offset;
		return this;
	}
	
	/**
	 * Zwr�cenie rezultat�w jako obiekt CriteriaResult
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public CriteriaResult criteriaResult()
	{
		//zliczenie kolumn
		return new CriteriaResultImpl(buildCriteriaQuery().list(), projection);
	}
	
	/**
	 * Zwr�cenie rezultat�w jako list� tablic obiekt�w
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> list()
	{
		return buildCriteriaQuery().list();
	}
	
	/**
	 * Rezultat pojedynczy
	 * @return
	 */
	public Object uniqueResult()
	{
		return buildCriteriaQuery().uniqueResult();
	}
	
	/**
	 * Buduje obiekt SQLQuery na podstawie u�o�onego zapytania criteria.
	 * @return
	 */
	public SQLQuery buildCriteriaQuery()
	{
		SQLQuery sqlQuery = session.createSQLQuery(buildSQL());
		
		// parametry dla klauzluli where
		for (NativeExp exp : whereExps.keySet())
			exp.setValues(sqlQuery);
		// parametry dla klauzuli having
		for (NativeExp exp : havingExps.keySet())
			exp.setValues(sqlQuery);
		
		if (limit != null)
			sqlQuery.setMaxResults(limit);
		if (offset != null)
			sqlQuery.setFirstResult(offset);
		
		return sqlQuery;
	}
	
	/**
	 * Buduje zapytanie SQL
	 * @return
	 */
	private String buildSQL()
	{
		final String SPACE = " ";
		
		StringBuilder sqlBuilder = new StringBuilder();
		
		// klauzula select i distinct
		sqlBuilder.append("SELECT").append(SPACE);
		if (distinct)
			sqlBuilder.append("DISTINCT").append(SPACE);
		
		// kolumny
		appendProjectionSQL(sqlBuilder);
		
		// klauzula FROM
		appendFromSQL(sqlBuilder);
		
		// dodanie join
		appendJoinSQL(sqlBuilder);
		
		// klauzula WHERE
		appendWhereSQL(sqlBuilder);
		
		// klauzula group by (projekcje)
		appendGroupBySQL(sqlBuilder);
		
		//klauzula having
		appendHavingSQL(sqlBuilder);
		
		// klauzula ORDER BY
		appendOrderBySQL(sqlBuilder);
		
		String sql = sqlBuilder.toString();
		LOG.debug("NativeCriteria SQL: " + sql);
		return sql;
	}

	/**
	 * Dodanie klauzuli having
	 * @param sqlBuilder
	 */
	private void appendHavingSQL(StringBuilder sqlBuilder)
	{
		final String SPACE = " ";
		boolean first = true;
		if (havingExps.size() > 0)
		{
			sqlBuilder.append("HAVING").append(SPACE);
			
			for (Entry<NativeExp, Operator> exp : havingExps.entrySet())
			{
				if (first)
				{
					sqlBuilder.append(exp.getKey().toSQL()).append(SPACE);
					first = false;
				}
				else
				{
					sqlBuilder.append(exp.getValue().getValue())
						.append(SPACE)
						.append(exp.getKey().toSQL())
						.append(SPACE);
				}
			}
		}
	}

	/**
	 * Dodanie klauzuli group by (projekcje)
	 * @param sqlBuilder
	 */
	private void appendGroupBySQL(StringBuilder sqlBuilder)
	{
		if (projection == null)
			return;
		
		sqlBuilder.append(projection.groupByToSQL());
	}

	/**
	 * Dodanie klauzuli order by
	 * @param sqlBuilder
	 */
	private void appendOrderBySQL(StringBuilder sqlBuilder)
	{
		if (orderExp == null)
			return;
		
		sqlBuilder.append(orderExp.toSQL());
	}

	/**
	 * Dodanie klauzuli where
	 * @param sqlBuilder
	 */
	private void appendWhereSQL(StringBuilder sqlBuilder)
	{
		final String SPACE = " ";
		boolean first = true;
		if (whereExps.size() > 0)
		{
			sqlBuilder.append("WHERE").append(SPACE);
			
			for (Entry<NativeExp, Operator> exp : whereExps.entrySet())
			{
				if (first)
				{
					sqlBuilder.append(exp.getKey().toSQL()).append(SPACE);
					first = false;
				}
				else
				{
					sqlBuilder.append(exp.getValue().getValue())
						.append(SPACE)
						.append(exp.getKey().toSQL())
						.append(SPACE);
				}
			}
		}
	}

	/**
	 * Dodanie join
	 * @param sqlBuilder
	 */
	private void appendJoinSQL(StringBuilder sqlBuilder)
	{
		final String SPACE = " ";
		for (NativeJoin join : joins)
			sqlBuilder.append(join.toSQL()).append(SPACE);
	}

	/**
	 * Dodanie klauzuli FROM
	 * @param sqlBuilder
	 */
	private void appendFromSQL(StringBuilder sqlBuilder)
	{
		final String SPACE = " ";
		sqlBuilder.append(SPACE).append("FROM").append(SPACE);
		
		boolean first = true;
		for (Entry<String, String> from : tables.entrySet())
		{
			if (first)
			{
				sqlBuilder.append(from.getKey()).append(SPACE)
					.append(from.getValue());
				first = false;
			}
			else
				sqlBuilder.append(", ").append(from.getKey()).append(SPACE)
					.append(from.getValue());
		}
		
		sqlBuilder.append(SPACE);
	}

	/**
	 * Dodaje kolumny do sql buildera
	 * @param sqlBuilder
	 */
	private void appendProjectionSQL(StringBuilder sqlBuilder)
	{
		// je�li podano kolumny to dodajemy do SQL
		if (projection != null && projection.hasProjections())
			sqlBuilder.append(projection.projectionToSQL());
		
		// nie okre�lono kolumn ani agregat�w projekcji
		if (projection == null || projection.countProjections() == 0)
		{
			boolean first = true;
			for (String alias : tables.values())
			{
				if (first)
				{
					sqlBuilder.append(alias).append(".*");
					first = false;
				}
				else
					sqlBuilder.append(", ").append(alias).append(".*");
			}
			if (joins.size() > 0)
				for (NativeJoin join : joins)
					sqlBuilder.append(", ")
						.append(join.getTableAlias())
						.append(".*");
		}
	}
}
