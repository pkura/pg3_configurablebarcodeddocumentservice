package pl.compan.docusafe.core.db.criteria;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

/**
 * Projekcje dla native SQL
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeProjection
{
	/** Lista projekcji */
	private List<ProjectionBean> projections;
	
	/** Projekcje dla GROUP BY */
	private List<String> groupProjections;
	
	/**
	 * Dost�pne agregaty
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	public enum AggregateProjection
	{
		AVG ("AVG"),
		SUM ("SUM"),
		MAX ("MAX"),
		MIN ("MIN"),
		COUNT ("COUNT");
		
		private String value;
		
		private AggregateProjection(String value)
		{
			this.value = value;
		}
		
		public String getValue()
		{
			return value;
		}
	}
	
	/**
	 * Bean dla kolumn b�d�cych agregatami
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class ProjectionBean
	{
		/** Nazwa kolumny */
		private String columnName;
		
		/** Alias kolumny */
		private String alias;
		
		/** Funkcja agregacji */
		private AggregateProjection aggregateProjection;
		
		/**
		 * Konstruktor
		 * @param columnName
		 * @param alias
		 */
		public ProjectionBean(String columnName, String alias)
		{
			this.columnName = columnName;
			this.alias = alias;
		}
		
		/**
		 * Konstruktor z agregatem
		 * @param columnName
		 * @param alias
		 * @param aggregateProjection
		 */
		public ProjectionBean(String columnName, String alias, 
				AggregateProjection aggregateProjection)
		{
			this.columnName = columnName;
			this.alias = alias;
			this.aggregateProjection = aggregateProjection;
		}
		
		public String getColumnName()
		{
			return columnName;
		}
		
		public String getAlias()
		{
			return alias;
		}
		
		public boolean isAggregate()
		{
			return aggregateProjection != null;
		}
		
		public String toSQL()
		{
			StringBuilder sql = new StringBuilder();
			if (aggregateProjection != null)
				sql.append(aggregateProjection.getValue())
					.append("(").append(columnName).append(")");
			else
				sql.append(columnName);
			
			return sql.append(" as ").append(alias).toString();
		}
	}
	
	/**
	 * Konstruktor
	 */
	public NativeProjection()
	{
		projections = new ArrayList<ProjectionBean>();
		groupProjections = new ArrayList<String>();
	}
	
	/**
	 * Dodanie projekcji
	 * @param columnName
	 * @return
	 */
	public NativeProjection addProjection(String columnName)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		
		projections.add(new ProjectionBean(columnName, VarGenerator.gen(columnName)));
		return this;
	}
	
	/**
	 * Dodanie projekcji z aliasem
	 * @param columnName
	 * @return
	 */
	public NativeProjection addProjection(String columnName, String alias)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		
		projections.add(new ProjectionBean(columnName, alias));
		return this;
	}
	
	/**
	 * Dodanie projekcji w postaci listy kolumn
	 * @param columns lista kolumn do projekcji
	 * @return
	 */
	public NativeProjection addProjection(List<String> columns)
	{
		if (columns.isEmpty())
			throw new IllegalStateException("Nie wybrano kolumn do projekcji!");
		
		for (String col : columns)
			projections.add(new ProjectionBean(col, VarGenerator.gen(col)));
		return this;
	}
	
	/**
	 * Dodanie projekcji w postaci mapy: nazwa kolumny - alias
	 * @param columns lista kolumn do projekcji
	 * @return
	 */
	public NativeProjection addProjection(Map<String, String> columns)
	{
		if (columns == null)
			throw new IllegalStateException("Nie wybrano kolumn do projekcji!");
		
		for (Entry<String, String> entry : columns.entrySet())
			projections.add(new ProjectionBean(entry.getKey(), entry.getValue()));
		return this;
	}
	
	/**
	 * Usuni�cie projekcji
	 * @param projection
	 * @return
	 */
	public NativeProjection removeProjection(String projection)
	{
		ProjectionBean bean = null;
		for (ProjectionBean b : projections)
			if (b.getColumnName().equalsIgnoreCase(projection))
			{
				bean = b;
				break;
			}
		
		projections.remove(bean);
		return this;
	}
	
	/**
	 * Wyczyszczenie projekcji
	 * @return
	 */
	public NativeProjection clearProjections()
	{
		projections.clear();
		return this;
	}
	
	/**
	 * Dodanie agregatu
	 * @param columnName
	 * @param projection
	 * @return
	 */
	public NativeProjection addAggregateProjection(String columnName, 
			AggregateProjection projection)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		if (projection == null)
			throw new IllegalStateException("Warto�� zmiennej projection jest null!");
		
		projections.add(new ProjectionBean(columnName, 
				VarGenerator.gen(columnName), projection));
		return this;
	}
	
	/**
	 * Dodanie agregatu z aliasem
	 * @param columnName
	 * @param projection
	 * @return
	 */
	public NativeProjection addAggregateProjection(String columnName, String alias, 
			AggregateProjection projection)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		if (projection == null)
			throw new IllegalStateException("Warto�� zmiennej projection jest null!");
		
		projections.add(new ProjectionBean(columnName, alias, projection));
		return this;
	}
	
	/**
	 * Dodanie projekcji grupuj�cej
	 * @param columnName
	 * @return
	 */
	public NativeProjection addGroupProjection(String columnName)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		
		groupProjections.add(columnName);
		return this;
	}
	
	/**
	 * Zwraca indeks danej projekcji
	 * @param columnName
	 * @return
	 */
	public int getProjectionIndex(String columnName)
	{
		for (int i = 0; i < projections.size(); i++)
			if (projections.get(i).getColumnName().equalsIgnoreCase(columnName) ||
				projections.get(i).getAlias().equalsIgnoreCase(columnName))
				return i;
		
		return -1;
	}
	
	/**
	 * Zwraca projekcje
	 * @return
	 */
	public boolean hasProjections()
	{
		return projections.size() > 0;
	}
	
	/**
	 * Zliczenie liczby projekcji
	 * @return
	 */
	public int countProjections()
	{
		return projections.size();
	}
	
	/**
	 * Czy s� agregaty
	 * @return
	 */
	public boolean hasAggregates()
	{
		for (ProjectionBean bean : projections)
			if (bean.isAggregate())
				return true;
		return false;
	}

	/**
	 * SQL projekcji
	 * @return
	 */
	public String projectionToSQL()
	{
		StringBuilder sqlBuilder = new StringBuilder();
		boolean first = true;
		for (ProjectionBean bean : projections)
		{
			if (first)
			{
				sqlBuilder.append(bean.toSQL());
				first = false;
			}
			else
				sqlBuilder.append(", ").append(bean.toSQL());
		}
		return sqlBuilder.toString();
	}

	/**
	 * Group BY SQL
	 * @return
	 */
	public String groupByToSQL()
	{
		// z automatu dodaje projekcje do klauzuli group by je�li s� agregaty
		if (hasAggregates())
    		for (ProjectionBean bean : projections)
    			if (!bean.isAggregate() && !groupProjections.contains(bean.getColumnName()))
    				groupProjections.add(bean.getColumnName());
		
		if (!groupProjections.isEmpty())
		{
			StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append("GROUP BY ");
			boolean first = true;
			
			for (String group : groupProjections)
			{
				if (first)
				{
					sqlBuilder.append(group);
					first = false;
				}
				else
					sqlBuilder.append(", ").append(group);
			}
			
			return sqlBuilder.append(" ").toString();
		}
		
		return "";
	}
}
