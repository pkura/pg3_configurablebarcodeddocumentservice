package pl.compan.docusafe.core.db.criteria;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;

/**
 * Wyra�enie LIKE w native SQL
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeLikeExp implements NativeExp
{
	/** Nazwa kolumny */
	private String columnName;
	
	/** Nazwa zmiennej */
	private String varName;
	
	/** Warto�� por�wnywana */
	private String value;
	
	/** Ignoruje wielko�� znak�w */
	private boolean caseInsensitive = false;
	
	/**
	 * Konstruktor
	 * @param columnName
	 * @param value
	 */
	public NativeLikeExp(String columnName, String value)
	{
		this(columnName, value, false);
	}
	
	public NativeLikeExp(String columnName, String value, boolean caseInsensitive) {
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		if (StringUtils.isBlank(value))
			throw new IllegalStateException("Warto�� value jest pusta!");
		
		this.columnName = columnName;
		this.value = value;
		this.caseInsensitive = caseInsensitive;
	}
	
	public String toSQL()
	{
		varName = VarGenerator.gen(columnName);
		if(caseInsensitive) {
			StringBuilder sb = new StringBuilder("LOWER(");
			sb.append(columnName).append(") LIKE :").append(varName);
			return sb.toString();
		} else {
			return columnName + " LIKE :" + varName;
		}
	}
	
	public void setValues(SQLQuery query)
	{
		if(caseInsensitive) {
			query.setString(varName, value.toLowerCase());
		} else {
			query.setString(varName, value);
		}
	}
}
