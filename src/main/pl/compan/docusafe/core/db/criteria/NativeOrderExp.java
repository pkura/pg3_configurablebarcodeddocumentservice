package pl.compan.docusafe.core.db.criteria;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

/**
 * Klauzula order by dla native SQL
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeOrderExp
{
	/** pola order by */
	private Map<String, OrderType> orders;
	
	/** typy order by */
	public enum OrderType
	{
		ASC ("ASC"), DESC ("DESC");
		
		private String type;
		
		private OrderType(String type)
		{
			this.type = type;
		}
		
		public String getType()
		{
			return type;
		}
	}
	
	/**
	 * Konstruktor
	 */
	public NativeOrderExp()
	{
		orders = new LinkedHashMap<String, OrderType>();
	}
	
	public NativeOrderExp add(String columnName, OrderType orderType)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		if (orderType == null)
			throw new IllegalStateException("Warto�� orderType jest null!");
		
		orders.put(columnName, orderType);
		return this;
	}
	
	public String toSQL()
	{
		StringBuilder order = new StringBuilder();
		if (orders.size() > 0)
		{
			for (Entry<String, OrderType> entry : orders.entrySet())
			{
				if (order.length() > 0)
					order.append(", ")
					.append(entry.getKey()).append(" ")
					.append(entry.getValue().getType());
				else
					order.append(entry.getKey()).append(" ")
					.append(entry.getValue().getType());
			}
		}
		
		if (order.length() > 0)
			return "ORDER BY " + order.toString();
		else
			return "";
	}
}
