package pl.compan.docusafe.core.db.criteria;

import org.hibernate.SQLQuery;

/**
 * Interfejs dla wyra�e� native SQL.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public interface NativeExp
{
	public String toSQL();
	public void setValues(SQLQuery query);
}
