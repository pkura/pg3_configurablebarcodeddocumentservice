package pl.compan.docusafe.core.db.criteria;

import java.util.Date;
import java.util.List;

/**
 * Implementacja CriteriaResult
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class CriteriaResultImpl implements CriteriaResult
{
	/** Rezultaty zapytania */
	private List<Object[]> results;
	
	/** Indeks rekordu */
	private int recordIdx = -1;
	
	/** Projekcje */
	private NativeProjection projection;
	
	/**
	 * Konstruktor
	 * @param results
	 */
	public CriteriaResultImpl(List<Object[]> results, NativeProjection projection)
	{
		if (results == null)
			throw new IllegalStateException("Warto�� zmiennej results jest null!");
		
		this.results = results;
		this.projection = projection;
	}
	
	/**
	 * Sprawdzenie czy jest kolejny rekord
	 */
	public boolean next()
	{
		recordIdx++;
		if (recordIdx < results.size())
			return true;
		return false;
	}
	
	public Boolean getBoolean(int idx, Boolean defaultResult)
	{
		Object val = getValue(idx, null);
		if (val == null)
			return defaultResult;
		
		Boolean res = null;
		if (val instanceof String)
			res = Boolean.valueOf(val.toString());
		else if (val instanceof Boolean)
			return res = ((Boolean) val).booleanValue();
		else
		{
			String v = val.toString();
			if (v.equals("1"))
				res = true;
			else
				res = false;
		}
		
		return res;
	}
	
	public Date getDate(int idx, Date defaultResult)
	{
		Object val = getValue(idx, null);
		if (val == null)
			return defaultResult;
			
		return (Date) val;
	}

	public Integer getInteger(int idx, Integer defaultResult)
	{
		Object val = getValue(idx, null);
		if (val == null)
			return defaultResult;
		
		return Integer.parseInt(val.toString());
	}

	public Long getLong(int idx, Long defaultResult)
	{
		Object val = getValue(idx, null);
		if (val == null)
			return defaultResult;
		
		return Long.parseLong(val.toString());
	}

	public Short getShort(int idx, Short defaultResult)
	{
		Object val = getValue(idx, null);
		if (val == null)
			return defaultResult;
		
		return Short.parseShort(val.toString());
	}

	public String getString(int idx, String defaultResult)
	{
		Object val = getValue(idx, null);
		if (val == null)
			return defaultResult;
		
		return val.toString();
	}
	
	public Object getValue(int idx, Object defaultResult)
	{
		if (recordIdx == -1 || recordIdx >= results.size())
			throw new IndexOutOfBoundsException("Rekord poza zakresem!");
		
		if (projection == null || projection.countProjections() > 1)
		{
			Object[] row = results.get(recordIdx);
			if (idx < 0 || idx >= row.length)
				throw new IndexOutOfBoundsException("Warto�� poza zakresem w rekordzie!");
			
			if (row[idx] == null)
				return defaultResult;
			
			return row[idx];
		}
		else
		{
			Object val = results.get(recordIdx);
			if (val == null)
				return defaultResult;
			
			return val;
		}
	}

	public Integer getRowsNumber()
	{
		return results.size();
	}

	public Boolean getBoolean(String columnName, Boolean defaultResult)
	{
		if (projection == null)
			throw new IllegalStateException(
					"Metoda obs�ugiwana tylko w zapytaniach z ustalon� projekcj�!");
		
		return getBoolean(projection.getProjectionIndex(columnName), defaultResult);
	}

	public Date getDate(String columnName, Date defaultResult)
	{
		if (projection == null)
			throw new IllegalStateException(
					"Metoda obs�ugiwana tylko w zapytaniach z ustalon� projekcj�!");
		return getDate(projection.getProjectionIndex(columnName), defaultResult);
	}

	public Integer getInteger(String columnName, Integer defaultResult)
	{
		if (projection == null)
			throw new IllegalStateException(
					"Metoda obs�ugiwana tylko w zapytaniach z ustalon� projekcj�!");
		return getInteger(projection.getProjectionIndex(columnName), defaultResult);
	}

	public Long getLong(String columnName, Long defaultResult)
	{
		if (projection == null)
			throw new IllegalStateException(
					"Metoda obs�ugiwana tylko w zapytaniach z ustalon� projekcj�!");
		return getLong(projection.getProjectionIndex(columnName), defaultResult);
	}

	public Short getShort(String columnName, Short defaultResult)
	{
		if (projection == null)
			throw new IllegalStateException(
					"Metoda obs�ugiwana tylko w zapytaniach z ustalon� projekcj�!");
		return getShort(projection.getProjectionIndex(columnName), defaultResult);
	}

	public String getString(String columnName, String defaultResult)
	{
		if (projection == null)
			throw new IllegalStateException(
					"Metoda obs�ugiwana tylko w zapytaniach z ustalon� projekcj�!");
		return getString(projection.getProjectionIndex(columnName), defaultResult);
	}

	public Object getValue(String columnName, Object defaultResult)
	{
		if (projection == null)
			throw new IllegalStateException(
					"Metoda obs�ugiwana tylko w zapytaniach z ustalon� projekcj�!");
		return getValue(projection.getProjectionIndex(columnName), defaultResult);
	}
}
