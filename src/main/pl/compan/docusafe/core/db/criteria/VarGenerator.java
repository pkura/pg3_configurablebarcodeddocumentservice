package pl.compan.docusafe.core.db.criteria;

import java.util.Random;

import org.hibernate.util.StringHelper;

/**
 * Generator nazw zmiennych
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class VarGenerator
{
	/**
	 * Generator liczb losowych
	 */
	private static Random random = new Random();
	
	/**
	 * Generuje nazw� zmiennej o okre�lonej d�ugo�ci znak�w, dodaj�c na ko�cu "_"
	 * 
	 * @param length
	 * @return
	 */
	public static String gen(String description)
	{
		return StringHelper.generateAlias(description, random.nextInt(1000)); 
	}
}
