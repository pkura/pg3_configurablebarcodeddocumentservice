package pl.compan.docusafe.core.db.criteria;

import java.util.Date;

/**
 * Interfejs CriteriaResult
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public interface CriteriaResult
{
	public boolean next();
	
	public String getString(int idx, String defaultResult);
	
	public String getString(String columnName, String defaultResult);
	
	public Integer getInteger(int idx, Integer defaultResult);
	
	public Integer getInteger(String columnName, Integer defaultResult);
	
	public Long getLong(int idx, Long defaultResult);
	
	public Long getLong(String columnName, Long defaultResult);
	
	public Short getShort(int idx, Short defaultResult);
	
	public Short getShort(String columnName, Short defaultResult);
	
	public Boolean getBoolean(int idx, Boolean defaultResult);
	
	public Boolean getBoolean(String columnName, Boolean defaultResult);
	
	public Date getDate(int idx, Date defaultResult);
	
	public Date getDate(String columnName, Date defaultResult);
	
	public Object getValue(int idx, Object defaultResult);
	
	public Object getValue(String columnName, Object defaultResult);
	
	public Integer getRowsNumber();
}
