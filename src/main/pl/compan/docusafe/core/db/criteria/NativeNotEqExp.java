package pl.compan.docusafe.core.db.criteria;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;

/**
 * Wyra�enie nie r�wno�ci w native SQL
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeNotEqExp implements NativeExp
{
	/** Nazwa kolumny */
	private String columnName;
	
	/** Nazwa zmiennej */
	private String varName;
	
	/** Warto�� por�wnywana */
	private Object value;
	
	/**
	 * Konstruktor
	 * @param columnName
	 * @param value
	 */
	public NativeNotEqExp(String columnName, Object value)
	{
		if (StringUtils.isBlank(columnName))
			throw new IllegalStateException("Warto�� columnName jest pusta!");
		if (value == null)
			throw new IllegalStateException("Warto�� value jest null!");
		
		this.columnName = columnName;
		this.value = value;
	}
	
	public String toSQL()
	{
		varName = VarGenerator.gen(columnName);
		return columnName + " <> :" + varName;
	}
	
	public void setValues(SQLQuery query)
	{
		query.setParameter(varName, value);
	}
}
