package pl.compan.docusafe.core.db.criteria;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;

/**
 * Klasa do krzy�owania wyra�e� SQL z operatorem OR
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeDisjunctionExp implements NativeJunctionExp
{
	/** Wyra�enia SQL */
	private List<NativeExp> exps;
	
	/**
	 * Konstruktor
	 */
	public NativeDisjunctionExp()
	{
		this.exps = new ArrayList<NativeExp>();
	}
	
	/**
	 * Konstruktor ustawiaj�cy list� wyra�e�
	 * @param exps
	 */
	public NativeDisjunctionExp(List<NativeExp> exps)
	{
		super();
		if (exps != null)
			this.exps = exps;
	}
	
	/**
	 * Dodaje wyra�enie SQL
	 */
	public NativeJunctionExp add(NativeExp exp)
	{
		if (exp == null)
			throw new IllegalStateException("Obiekt exp ma warto�� null!");
		
		exps.add(exp);
		return this;
	}

	/**
	 * Dodaje wyra�enia SQL
	 */
	public NativeJunctionExp add(List<NativeExp> exps)
	{
		if (exps == null)
			throw new IllegalStateException("Obiekt exp ma warto�� null!");
		
		this.exps.addAll(exps);
		return null;
	}

	/**
	 * Ustawia parametry zapytania
	 */
	public void setValues(SQLQuery query)
	{
		for (NativeExp exp : exps)
			exp.setValues(query);
	}

	/**
	 * Zwraca SQL zapytania
	 */
	public String toSQL()
	{
		if (exps.isEmpty()) return "";
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("(");
		final String SPACE = " ";
		boolean first = true;
		for (NativeExp exp : exps)
		{
			if (first)
			{
				sqlBuilder.append(exp.toSQL());
				first = false;
			}
			else
			{
				sqlBuilder.append(SPACE).append("OR").append(SPACE)
					.append(exp.toSQL());
			}
		}
		sqlBuilder.append(")");
		return sqlBuilder.toString();
	}
}
