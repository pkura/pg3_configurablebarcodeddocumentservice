package pl.compan.docusafe.core.db.criteria;

import java.util.Calendar;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.db.criteria.NativeOrderExp.OrderType;
import pl.compan.docusafe.core.db.criteria.NativeProjection.AggregateProjection;

/**
 * Przyk�adowa klasa z zapytaniami NativeCriteria
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class NativeExample
{
	/**
	 * Przyk�ad 1. Proste zapytanie. 
	 * @throws EdmException
	 */
	public void example1() throws EdmException
	{
		// proste zapytanie, wyci�gni�cie wbranych kolumn z warunkiem where
		NativeCriteria nc = DSApi.context().createNativeCriteria("DSW_TASKLIST", "d");
		nc.setProjection(NativeExps.projection()
				// ustalenie kolumn
				.addProjection("d.dso_document_id")
				.addProjection("d.assigned_user"))
			// warunek where
			.add(NativeExps.gt("d.dso_document_id", 2000))
			.add(NativeExps.eq("d.assigned_user", "admin"))
			// order by i limit
			.addOrder(NativeExps.order().add("d.dso_document_id", OrderType.DESC))
			.setLimit(50);
		
		// pobranie i wy�wietlenie wynik�w
		CriteriaResult cr = nc.criteriaResult();
		while (cr.next())
		{
			System.out.println("DOC ID: " + cr.getLong(0, null) + 
					", USER: " + cr.getString(1, null));
		}
	}
	
	/**
	 * Przyk�ad 2. Zapytanie z agregatem
	 * @throws EdmException
	 */
	public void example2() throws EdmException
	{
		// zapytanie zliczaj�ce liczb� dokument�w z tasklisty dla ka�dego usera
		// z ostatnich 30 miesi�cy
		
		NativeCriteria nc = DSApi.context().createNativeCriteria("DSW_TASKLIST", "d");
		
		Calendar date = Calendar.getInstance();
		date.add(Calendar.MONTH, -30);
		
		CriteriaResult cr = 
			nc.setProjection(NativeExps.projection()
				.addProjection("d.assigned_user")
				.addAggregateProjection("d.dso_document_id", AggregateProjection.COUNT)) // automatycznie doda group by po kolumnie d.assigned_user
			// receive date wi�ksza r�wna
			.add(NativeExps.gte("d.rcvDate", date.getTime()))
			// order by
			.addOrder(NativeExps.order().add("d.assigned_user", OrderType.ASC))
			// pobranie wynik�w
			.criteriaResult();
		
		while (cr.next())
		{
			System.out.println("USER: " + cr.getString(0, null) + 
					", LICZBA DOK: " + cr.getInteger(1, null));
		}
	}
	
	/**
	 * Przyk�ad 3. Zapytanie to samo co z metody wy�ej + join z tabel� DS_USER
	 * @throws EdmException
	 */
	public void example3() throws EdmException
	{
		// zapytanie zliczaj�ce liczb� dokument�w z tasklisty dla ka�dego usera
		// z ostatnich 30 miesi�cy, tylko zrzuca kobieciny, tzn. imi� ko�czy si� na 'a'.
		
		NativeCriteria nc = DSApi.context().createNativeCriteria("DSW_TASKLIST", "d");
		
		Calendar date = Calendar.getInstance();
		date.add(Calendar.MONTH, -30);
		
		CriteriaResult cr = 
			nc.setProjection(NativeExps.projection()
				.addProjection("d.assigned_user")
				.addAggregateProjection("d.dso_document_id", AggregateProjection.COUNT))
			.addJoin(NativeExps.innerJoin("DS_USER", "u", "u.name", "d.assigned_user"))
			// receive date wi�ksza r�wna
			.add(NativeExps.gte("d.rcvDate", date.getTime()))
			// imi� ko�czy si� na 'a'
			.add(NativeExps.like("u.firstname", "%a"))
			// order by
			.addOrder(NativeExps.order().add("d.assigned_user", OrderType.ASC))
			// pobranie wynik�w
			.criteriaResult();
		
		while (cr.next())
		{
			System.out.println("USER: " + cr.getString(0, null) + 
					", LICZBA DOK: " + cr.getInteger(1, null));
		}
	}
	
	/**
	 * Przyk�ad 4. Zapytanie to co w przyk�adzie 2 + warunek having
	 * @throws EdmException
	 */
	public void example4() throws EdmException
	{
		// zapytanie zliczaj�ce liczb� dokument�w z tasklisty dla ka�dego usera
		// z ostatnich 30 miesi�cy, gdzie liczba dokument�w wi�ksza od 5
		
		NativeCriteria nc = DSApi.context().createNativeCriteria("DSW_TASKLIST", "d");
		
		Calendar date = Calendar.getInstance();
		date.add(Calendar.MONTH, -30);
		
		CriteriaResult cr = 
			nc.setProjection(NativeExps.projection()
				.addProjection("d.assigned_user")
				.addAggregateProjection("d.dso_document_id", AggregateProjection.COUNT))
			// receive date wi�ksza r�wna
			.add(NativeExps.gte("d.rcvDate", date.getTime()))
			// liczba dokument�w wi�ksza od 5
			// MSSQL NIE OBS�UGUJE ALIAS�W W HAVING
			.addHaving(NativeExps.gt("COUNT(d.dso_document_id)", 5))
			// order by
			.addOrder(NativeExps.order().add("d.assigned_user", OrderType.ASC))
			// pobranie wynik�w
			.criteriaResult();
		
		while (cr.next())
		{
			System.out.println("USER: " + cr.getString(0, null) + 
					", LICZBA DOK: " + cr.getInteger(1, null));
		}
	}
	
	/**
	 * Przyk�ad 5. Zapytanie takie samo jak w przyk�adzie 4 + jak korzysta� z alias�w
	 * @throws EdmException
	 */
	public void example5() throws EdmException
	{
		// zapytanie zliczaj�ce liczb� dokument�w z tasklisty dla ka�dego usera
		// z ostatnich 30 miesi�cy, gdzie liczba dokument�w wi�ksza od 5
		
		NativeCriteria nc = DSApi.context().createNativeCriteria("DSW_TASKLIST", "d");
		
		Calendar date = Calendar.getInstance();
		date.add(Calendar.MONTH, -30);
		
		CriteriaResult cr = 
			nc.setProjection(NativeExps.projection()
				.addProjection("d.assigned_user", "asgnUser")
				.addAggregateProjection("d.dso_document_id", "docCount", AggregateProjection.COUNT))
			// receive date wi�ksza r�wna
			.add(NativeExps.gte("d.rcvDate", date.getTime()))
			// liczba dokument�w wi�ksza od 5
			// MSSQL NIE OBS�UGUJE ALIAS�W W HAVING
			.addHaving(NativeExps.gt("COUNT(d.dso_document_id)", 5))
			// order by
			.addOrder(NativeExps.order().add("d.assigned_user", OrderType.ASC))
			// pobranie wynik�w
			.criteriaResult();
		
		while (cr.next())
		{
			// pobranie warto�ci z alias�w
			System.out.println("USER: " + cr.getString("asgnUser", null) + 
					", LICZBA DOK: " + cr.getInteger("docCount", null));
		}
	}
}
