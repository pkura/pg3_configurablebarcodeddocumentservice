package pl.compan.docusafe.core.test;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.common.BeanBackedMap;
import std.lambda;

/**
 * Akcja do testu wydajno�ci wykonuj�ca czynno�� wyszukiwania dokument�w
 * przychodz�cych wed�ug okre�lonych kryteri�w.
 * 
 * @author Marcin Wlizlo
 *
 */
public class SearchTestActivity extends TestActivityBase
{
    private static final Log log = LogFactory.getLog(SearchTestActivity.class);

    private String username;
    
    private static final int LIMIT = 10;
    private Integer officeNumberYear;
    private Integer officeNumber;
    private Integer fromOfficeNumber;
    private Integer toOfficeNumber;
    private String assignedUser;
    private String stampDateFrom;
    private String stampDateTo;
    private String documentDateFrom;
    private String documentDateTo;
    private String incomingDateFrom;
    private String incomingDateTo;
    private Integer kindId;
    private Integer deliveryId;
    private String creatingUser;
    private String recipient;
    private String sender;
    private String barcode;
    private String referenceId;
    private String postalRegNumber;
    private String summary;
    private Long journalId;
    private Integer statusId;
    private String accessedBy;
    private String[] accessedAs;
    private String accessedFrom;
    private String accessedTo;
    private String source;
    private boolean flagsOn;
    private Long[] flagIds;
    private Long[] userFlagIds;
    private String flagsDateFrom;
    private String flagsDateTo;
    private String userFlagsDateFrom;
    private String userFlagsDateTo;
    private String sortField;
    private boolean ascending;
    private boolean attachmentsAsPdf;
    private SearchResults<Map> results;
    private String thisSearchUrl;
    private int offset;
    private String attachmentsAsPdfUrl;
    private Pager pager;

    public SearchTestActivity(String username)
    {
        super();
        this.username = username;
    }
    
    void performSearch()
    {
        InOfficeDocument.Query query = InOfficeDocument.query(0, LIMIT);

        if (officeNumberYear != null)
        {
            query.setOfficeNumberYear(officeNumberYear);
        }
        else
        {
            try
            {
                if (officeNumber != null)
                    query.setOfficeNumberYear(new Integer(GlobalPreferences.getCurrentYear()));
            }
            catch (EdmException e)
            {
                log.error(e.getMessage(), e);
                return;
            }
        }

        if (officeNumber != null)
        {
            query.setOfficeNumber(officeNumber);
        }
        else if (fromOfficeNumber != null || toOfficeNumber != null)
        {
            query.setFromOfficeNumber(fromOfficeNumber);
            query.setToOfficeNumber(toOfficeNumber);
        }
        query.setClerk(TextUtils.trimmedStringOrNull(assignedUser));
        query.setStampDateFrom(DateUtils.nullSafeParseJsDate(stampDateFrom));
        query.setStampDateTo(DateUtils.nullSafeParseJsDate(stampDateTo));
        query.setDocumentDateFrom(DateUtils.nullSafeParseJsDate(documentDateFrom));
        query.setDocumentDateTo(DateUtils.nullSafeParseJsDate(documentDateTo));
        query.setIncomingDateFrom(DateUtils.nullSafeParseJsDate(incomingDateFrom));
        query.setIncomingDateTo(DateUtils.nullSafeParseJsDate(incomingDateTo));
        query.setKindId(kindId);
        query.setDeliveryId(deliveryId);
        query.setCreatingUser(TextUtils.trimmedStringOrNull(creatingUser));
        query.setRecipient(TextUtils.trimmedStringOrNull(recipient));
        query.setSender(TextUtils.trimmedStringOrNull(sender));
        query.setBarcode(TextUtils.trimmedStringOrNull(barcode));
        query.setReferenceId(TextUtils.trimmedStringOrNull(referenceId));
        query.setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber));
        query.setSummary(TextUtils.trimmedStringOrNull(summary));
        query.setJournalId(journalId);
        query.setStatusId(statusId);
        query.setAccessedBy(TextUtils.trimmedStringOrNull(accessedBy));
        query.setAccessedAs(accessedAs);
        /* jak wybrany tylko zakres dolny, to zakresem gornym tez bedzie zakres dolny */
        Date accFrom = DateUtils.nullSafeParseJsDate(accessedFrom);
        Date accTo = DateUtils.nullSafeParseJsDate(accessedTo);
        query.setAccessedFrom(DateUtils.nullSafeMidnight(accFrom, 0));
        query.setAccessedTo(DateUtils.nullSafeMidnight((accFrom != null && accTo == null) ? accFrom : accTo, 1));
        query.setSource(TextUtils.trimmedStringOrNull(source));

        flagsOn = "true".equals(Configuration.getProperty("flags"));

        if (flagsOn)
        {
            List<Integer> flags = new ArrayList<Integer>();
            /*for (int i=1; i < flagIds.length; i++)
            {
                if (Boolean.valueOf(flagIds[i]).booleanValue())
                    flags.add(new Integer(i));
            }*/
            if (flags.size() > 0)
                query.setFlags(Arrays.asList(flagIds),
                    DateUtils.nullSafeParseJsDate(flagsDateFrom),
                    DateUtils.nullSafeParseJsDate(flagsDateTo));

            List<Integer> userFlags = new ArrayList<Integer>();
            /*for (int i=1; i < userFlagIds.length; i++)
            {
                if (Boolean.valueOf(userFlagIds[i]).booleanValue())
                    userFlags.add(new Integer(i));
            }*/
            if (userFlags.size() > 0)
                query.setUserFlags(Arrays.asList(userFlagIds),
                    DateUtils.nullSafeParseJsDate(userFlagsDateFrom),
                    DateUtils.nullSafeParseJsDate(userFlagsDateTo));
        }
        query.safeSortBy(sortField, "officeNumber", ascending);

        // generowanie tre�ci za��cznik�w jako PDF
        if (attachmentsAsPdf)
        {
            File tmp = null;
            try
            {
                DSApi.context().begin();

                SearchResults<InOfficeDocument> pdfResults = InOfficeDocument.search(query);

                InOfficeDocument[] documents = pdfResults.results();
                tmp = File.createTempFile("docusafe_", ".pdf");
                OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));
                PdfUtils.attachmentsAsPdf(documents, false, os);
                os.close();
            }
            catch (Exception e)
            {
                log.error(e.getMessage(), e);
            }
            finally
            {
                DSApi.context().setRollbackOnly();
            }

            if (tmp != null)
            {
                //event.setResult(NONE);
                //event.skip(EV_FILL);
                try
                {
                    ServletUtils.streamFile(ServletActionContext.getResponse(), tmp, "application/pdf");
                }
                catch (IOException e)
                {
                    log.error(e.getMessage(), e);
                }
                finally
                {
                    tmp.delete();
                }
            }

            return;
        }

        try
        {
            lambda<InOfficeDocument, Map> mapper =
                new lambda<InOfficeDocument, Map>()
                {
                    public Map act(InOfficeDocument doc)
                    {
                        BeanBackedMap result = new BeanBackedMap(doc,
                            "id", "referenceId",
                            "incomingDate", "sender", "recipients", "officeNumber",
                            "formattedOfficeNumber", "title", "doctype");
                        try
                        {
                            DSApi.initializeProxy(doc.getRecipients());
                            if (flagsOn)
                            {
                                result.put("setFlags", doc.getFlags().getDocumentFlags(false));
                                result.put("setUserFlags", doc.getUserFlags().getDocumentFlags(true));
                            }
                        }
                        catch (EdmException e)
                        {
                        }
                        return result;
                    }
                };

            results = InOfficeDocument.search(query, mapper, Map.class);

/*
            // stworzenie tablic dodatkowych w�asno�ci zwracanych wynik�w;
            // ka�dy obiekt InOfficeDocument, jaki zostanie wy�wietlony
            // na stronie wynik�w, zostanie "udekorowany" przy pomocy klasy
            // BeanEnhancer dodatkowymi w�asno�ciami: setFlags i setUserFlags

            // trzeba to robi� tutaj, poniewa� pobieranie tych w�asno�ci
            // wymaga istniej�cej sesji Hibernate, a podczas wy�wietlania JSP
            // sesja ju� nie istnieje
            final Map mappings = new HashMap();

            Object[] objects = results.results();
            for (int i=0; i < objects.length; i++)
            {
                Document doc = (Document) objects[i];
                Map extras = new HashMap();
                extras.put("setFlags", doc.getFlags().getSetFlags());
                extras.put("setUserFlags", doc.getUserFlags().getSetFlags());
                mappings.put("#"+doc.getId(), extras);
            }

            results.setMapper(new SearchResults.Mapper()
            {
                public Object map(Object o)
                {
                    Map extras = (Map) mappings.get("#"+((Document) o).getId());
                    BeanEnhancer be = new BeanEnhancer(o, extras);
                    return be.create();
                }
            });

            for (int i=0; i < objects.length; i++)
            {
                DSApi.initializeProxy(((InOfficeDocument) objects[i]).getRecipients());
            }
*/
            thisSearchUrl =
                HttpUtils.makeUrl("/office/find-office-documents.action",
                new Object[] {
                "doSearchIn", "true",
                "officeNumber", officeNumber,
                "officeNumberYear", officeNumberYear,
                "fromOfficeNumber", fromOfficeNumber,
                "toOfficeNumber", toOfficeNumber,
                //"officeNumberYear2", officeNumberYear2,
                "assignedUser", assignedUser,
                "documentDateFrom", documentDateFrom,
                "documentDateTo", documentDateTo,
                "incomingDateFrom", incomingDateFrom,
                "incomingDateTo", incomingDateTo,
                "kindId", kindId,
                "summary", summary,
                "creatingUser", creatingUser,
                "recipient", recipient,
                "sender", sender,
                "stampDateFrom", stampDateFrom,
                "stampDateTo", stampDateTo,
                "barcode", barcode,
                "referenceId", referenceId,
                "fromOfficeNumber", fromOfficeNumber,
                "toOfficeNumber", toOfficeNumber,
                "journalId", journalId,
                "statusId", statusId,
                "accessedBy", accessedBy,
                "accessedAs", accessedAs,
                "accessedFrom", accessedFrom,
                "accessedTo", accessedTo,
                "flagsDateTo", flagsDateTo,
                "flagsDateFrom", flagsDateFrom,
                "userFlagsDateTo", userFlagsDateTo,
                "userFlagsDateFrom", userFlagsDateFrom,    
                "offset", String.valueOf(offset)});
            String tmp = "";
            if (flagsOn)
            {
                for (int i=1; i < flagIds.length; i++)
                {
                   tmp = tmp+"&flagIds="+flagIds[i];
                }
                for (int i=1; i < userFlagIds.length; i++)
                {
                    tmp = tmp + "&userFlagIds="+userFlagIds[i];
                }
            }
            thisSearchUrl = thisSearchUrl + tmp;
            final String tmp2 = tmp;

            attachmentsAsPdfUrl = thisSearchUrl + "&attachmentsAsPdf=true";

            Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
            {
                public String getLink(int offset)
                {
                    String url = HttpUtils.makeUrl("/office/find-office-documents.action",
                        new Object[] {
                        "doSearchIn", "true",
                        "officeNumber", officeNumber,
                        "officeNumberYear", officeNumberYear,
                        "fromOfficeNumber", fromOfficeNumber,
                        "toOfficeNumber", toOfficeNumber,
                        //"officeNumberYear2", officeNumberYear2,
                        "assignedUser", assignedUser,
                        "documentDateFrom", documentDateFrom,
                        "documentDateTo", documentDateTo,
                        "incomingDateFrom", incomingDateFrom,
                        "incomingDateTo", incomingDateTo,
                        "kindId", kindId,
                        "summary", summary,
                        "creatingUser", creatingUser,
                        "recipient", recipient,
                        "sender", sender,
                        "stampDateFrom", stampDateFrom,
                        "stampDateTo", stampDateTo,
                        "barcode", barcode,
                        "referenceId", referenceId,
                        "journalId", journalId,
                        "statusId", statusId,
                        "accessedBy", accessedBy,
                        "accessedAs", accessedAs,
                        "accessedFrom", accessedFrom,
                        "accessedTo", accessedTo,
                        "flagsDateTo", flagsDateTo,
                        "flagsDateFrom", flagsDateFrom,
                        "userFlagsDateTo", userFlagsDateTo,
                        "userFlagsDateFrom", userFlagsDateFrom,
                        "ascending", String.valueOf(ascending),
                        "offset", String.valueOf(offset)});
                    return url + tmp2;
                }
            };
            pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
        }
        catch (EdmException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    void performFillForm()
    {
        
    }
    
    void actionPerformed()
    {
        try
        {
            performSearch();
            performFillForm();            
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    void afterActivity()
    {
        try
        {
            DSApi._close();
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    boolean prepareActivity()
    {
        Random rnd = new Random();
        try
        {
            officeNumberYear = null;
            officeNumber = null;
            fromOfficeNumber = null;
            toOfficeNumber = null;
            assignedUser = null;
            stampDateFrom = null;
            stampDateTo = null;
            documentDateFrom = null;
            documentDateTo = null;
            incomingDateFrom = null;
            incomingDateTo = null;
            kindId = null;
            deliveryId = null;
            creatingUser = null;
            recipient = null;
            sender = null;
            barcode = null;
            referenceId = null;
            postalRegNumber = null;
            summary = null;
            journalId = null;
            statusId = null;
            accessedBy = null;
            accessedAs = null;
            accessedFrom = null;
            accessedTo = null;
            source = null;
            flagsOn = false;
            flagIds = new Long[0];
            userFlagIds = new Long[0];
            flagsDateFrom = null;
            flagsDateTo = null;
            userFlagsDateFrom = null;
            userFlagsDateTo = null;
            sortField = null;
            ascending = false;
            attachmentsAsPdf = false;
            results = null;
            thisSearchUrl = null;
            offset = 0;
            attachmentsAsPdfUrl = null;
            Pager pager = null;
            DSApi.openAdmin();
            return true;
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            return false;
        }
    }
    
}
