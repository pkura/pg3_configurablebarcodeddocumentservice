package pl.compan.docusafe.core.test;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.office.tasklist.TaskListAction;
import pl.compan.docusafe.web.office.tasklist.TaskListAction.CaseTask;
import std.fun;
import std.lambda;

/**
 * Akcja do testu wydajno�ci wykonuj�ca czynno�� pobierania listy
 * pism(pisma w�asne u�ytkownika).
 * 
 * @author Marcin Wlizlo
 *
 */
public class TasksTestActivity extends TestActivityBase
{
    private static final Log log = LogFactory.getLog(TasksTestActivity.class);

    private String username;
    
    private boolean simpleTaskList;
    private boolean simpleTaskList1;
    private boolean simpleTaskList2;
    private boolean fullTaskList;
    private String tab;
    private List<Task> tasks;
    private Collection<CaseTask> cases;
    private Integer searchOfficeNumber;
    private String searchOfficeId;
    private String sortField;
    
    private static final int LIMIT = 30;

    public TasksTestActivity(String username)
    {
        super();
        this.username = username;
    }
    
    void actionPerformed()
    {
        try
        {
            DSUser user = DSApi.context().getDSUser();

            if (!simpleTaskList && !fullTaskList)
                simpleTaskList = DSApi.context().userPreferences().node("task-list").getBoolean("simple-task-list", false);
            if (!simpleTaskList1)
                simpleTaskList1 = DSApi.context().userPreferences().node("task-list").getBoolean("simple-task-list", false);
            if (!simpleTaskList2)
                simpleTaskList2 = DSApi.context().userPreferences().node("task-list").getBoolean("simple-task-list", false);
            
            int tasksSize = 0;
            boolean isPager = false;
            
            
            if (user != null)
            {
                
                user = DSUser.findByUsername(username);
                
                DSUser currentUser = user;//DSApi.context().getDSUser();

                //columns = getActualColumns();

                sortField = "receiveDate";
                boolean ascending = false;

                /* sprawdzanie czy uzywamy domyslnego sortowania */
                /*
                if (sortField == null) {
                    sortField = DSApi.context().userPreferences().node("task-list").get("sort-column_"+tab, "receiveDate");
                    boolean is = false;
                    for (Iterator iter = columns.iterator(); iter.hasNext();)
                    {
                        TableColumn column = (TableColumn) iter.next();
                        String field = column.getProperty();
                        if (field.equals(sortField))
                            is = true;
                    }
                    if (!is)
                    ascending = false;
                }
                */

                /* needSort <=> konieczne zewnetrzne (czyli "tutaj") sortowanie */
                boolean needSort = ("sender".equals(sortField) || "documentSender".equals(sortField) || "receiver".equals(sortField) || "orderStatus".equals(sortField) || "author".equals(sortField) || "clerk".equals(sortField)|| "answerCounter".equals(sortField) || "arrivalDate".equals(sortField) );

                TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);

                if (TaskListAction.TAB_IN.equals(tab))
                {
                    if (simpleTaskList)
                        tasks = (List<Task>) taskList.getSimpleInDocumentTasks(user, searchOfficeNumber, sortField, ascending);
                    else if (fullTaskList || needSort)
                        tasks = (List<Task>) taskList.getDocumentTasks(null, user, InOfficeDocument.TYPE, searchOfficeNumber, sortField, ascending, 0, LIMIT);
                    else {
                        Map map = taskList.getInDocumentTasks(null, user, 0, LIMIT, searchOfficeNumber, sortField, ascending);
                        tasksSize = ((Integer) map.get("size")).intValue();
                        tasks =  (List<Task>) map.get("tasks");
                        isPager = true;
                    }
                }
                else if (TaskListAction.TAB_OUT.equals(tab))
                {
                    tasks = (List<Task>) taskList.getDocumentTasks(null, user, OutOfficeDocument.TYPE, searchOfficeNumber, sortField, ascending, 0, LIMIT);
                }
                else if (TaskListAction.TAB_INT.equals(tab))
                {
                    tasks = (List<Task>) taskList.getDocumentTasks(null, user, OutOfficeDocument.INTERNAL_TYPE, searchOfficeNumber, sortField, ascending, 0, LIMIT);
                }
                /*
                else if (TAB_ORDER.equals(tab))
                {               
                    if (TAB_MY_ORDERS.equals(orderTab))
                        tasks = taskList.getUserOrderTasks(user, searchOfficeNumber, sortField, ascending);
                    else if (TAB_WATCH_ORDERS.equals(orderTab))
                        tasks = taskList.getWatchOrderTasks(user, searchOfficeNumber, sortField, ascending);
                    else
                        tasks = taskList.getDocumentTasks(user, OfficeOrder.TYPE, searchOfficeNumber, sortField, ascending);
                }
                */
                else if (TaskListAction.TAB_CASES.equals(tab))
                {
                    searchOfficeId = TextUtils.trimmedStringOrNull(searchOfficeId);
                    cases = fun.map(OfficeCase.findUserCases(user, searchOfficeId),
                        new lambda<OfficeCase, CaseTask>()
                        {
                            public CaseTask act(OfficeCase o)
                            {
                                return new CaseTask(o);
                            }
                        });
                }
                /*
                else if (TAB_WATCHES.equals(tab))
                {
                    watches = updateWatches(user);
                }
                */
                else
                {
                    throw new EdmException("Nieznany rodzaj zak�adki: "+tab);
                }
                
                if (tasks != null && searchOfficeNumber != null)
                {
                 /*   List<Task> tasksCopy = new ArrayList<Task>(tasks);

                    for (Iterator iter=tasksCopy.iterator(); iter.hasNext(); )
                    {
                        Task task = (Task) iter.next();
                        if (task.getDocumentOfficeNumber() == null ||
                            !task.getDocumentOfficeNumber().equals(searchOfficeNumber))
                            iter.remove();
                    }

                    // je�eli nie znaleziono szukanego dokumentu, wy�wietlana
                    // jest kompletna lista
                    if (tasksCopy.size() == 0)
                        addActionError("Nie znaleziono zadania o ��danym numerze KO");
                    else {
                        tasks = tasksCopy;
                        tasksSize = 1;
                    } */

                    // je�eli nie znaleziono szukanego dokumentu, wy�wietlana
                    // jest kompletna lista

                }

                // format u�ywany do formatowania liczb, kt�re pos�u�? za
                // klucze sortowania leksykalnego; dodaje na pocz?tku zera
                final NumberFormat millisFormat = NumberFormat.getIntegerInstance();
                millisFormat.setMinimumIntegerDigits((""+Long.MAX_VALUE).length());
                millisFormat.setGroupingUsed(false);
                millisFormat.setParseIntegerOnly(true);
                // napis o najmniejszej mo�liwej warto?ci u�ywany zamiast
                // napisu pustego w sortowaniu leksykalnym
                final String MIN_STR = String.valueOf((char) 0);
                
                // sortowanie tablicy zada�
                if (tasks != null && tasks.size() > 1 && needSort)
                {
                    List<String> sortKeys = new ArrayList<String>(tasks.size());

                    for (Iterator iter=tasks.iterator(); iter.hasNext(); )
                    {
                        TaskSnapshot task = (TaskSnapshot) iter.next();
                        String key;
                        String onKey = task.getDocumentOfficeNumber() != null ?
                                millisFormat.format(task.getDocumentOfficeNumber()) :
                                millisFormat.format(0);

                        // tylko kilka "if-�w", bo sortowanie po pozosta�ych polach odbywa si� w zapytaniu sql-owym         
                        if ("documentSender".equals(sortField))
                        {
                            key = task.getDocumentSender() != null ?
                                task.getDocumentSender().toUpperCase() : MIN_STR;
                        }
                        else if ("sender".equals(sortField))
                        {
                            key = task.getSender() != null ?
                                task.getSender().toUpperCase() : MIN_STR;
                        }
                        else if ("receiver".equals(sortField))
                        {
                            key = task.getReceiver() != null ?
                                task.getReceiver().toUpperCase() : MIN_STR;
                        }
                        else if ("author".equals(sortField))
                        {
                            key = task.getAuthor() != null ?
                                task.getAuthor().toUpperCase() : MIN_STR;
                        }
                        else if ("orderStatus".equals(sortField))
                        {
                            key = task.getOrderStatus() != null ?
                                task.getOrderStatus().toUpperCase() : MIN_STR;
                        }
                        else if ("clerk".equals(sortField))
                        {
                            key = task.getClerk() != null ?
                                task.getClerk().toUpperCase() : MIN_STR;  
                        }
                        
                        else // documentOfficeNumber
                        {
                            key = onKey;
                        }

                        // na ko�cu klucza zawsze numer kancelaryjny, poniewa�
                        // jest to poboczny klucz sortowania
                        key += ("<>" + onKey);

                        sortKeys.add(key);
                    }

                    Task[] taskArray = (Task[]) tasks.toArray(new Task[tasks.size()]);
                    std.sort.qsort(
                        (String[]) sortKeys.toArray(new String[sortKeys.size()]),
                        taskArray,
                        ascending);

                    tasks = Arrays.asList(taskArray);
                    
                    if (!fullTaskList && !simpleTaskList)
                        /* bylo stronicowanie z koniecznym sortowaniem tutaj
                         * czyli i tak zostaly wyszukane wszystkie zadania
                         * - kasuje odpowiednia czesc zadan
                         */
                    {
                        isPager = true;
                        tasksSize = tasks.size();
                        int count = 0;
                        List<Task> tasksCopy = new ArrayList<Task>(tasks);
                        for (Iterator iter=tasksCopy.iterator(); iter.hasNext(); )
                        {
                            Task task = (Task) iter.next();
                            if (count < 0 || count >= LIMIT)
                                iter.remove();
                            count++;
                        }
                        tasks = tasksCopy;
                    }
                }
                // sortowanie tablicy spraw
                else if (cases != null && cases.size() > 1)
                {
                    List<String> sortKeys = new ArrayList<String>(cases.size());

                    for (Iterator iter=cases.iterator(); iter.hasNext(); )
                    {
                        CaseTask task = (CaseTask) iter.next();
                        String key;
                        String oidKey = task.getCaseOfficeId().toUpperCase();

                        if ("caseStatus".equals(sortField))
                        {
                            key = task.getCaseStatus() != null ?
                                task.getCaseStatus().toUpperCase() : MIN_STR;
                        }
                        else if ("caseOpenDate".equals(sortField))
                        {
                            key = millisFormat.format(task.getCaseOpenDate() != null ?
                                task.getCaseOpenDate().getTime() : 0);
                        }
                        else if ("caseFinishDate".equals(sortField))
                        {
                            key = millisFormat.format(task.getCaseFinishDate() != null ?
                                task.getCaseFinishDate().getTime() : 0);
                        }
                        else if ("caseDaysToFinish".equals(sortField))
                        {
                            int daysToFinish = 0;
                            if (task.getCaseDaysToFinish() != null)
                                daysToFinish = task.getCaseDaysToFinish().intValue();
                            if (daysToFinish < 0)
                            {
                                daysToFinish = Integer.MIN_VALUE - daysToFinish;
                            }
                            key = millisFormat.format(daysToFinish);
                        }
                        else
                        {
                            key = oidKey;
                        }

                        // dodawanie pobocznego klucza sortowania (officeId)
                        key += ("<>" + oidKey);

                        sortKeys.add(key);
                    }

                    CaseTask[] taskArray = (CaseTask[]) cases.toArray(new CaseTask[cases.size()]);
                    std.sort.qsort(
                        (String[]) sortKeys.toArray(new String[sortKeys.size()]),
                        taskArray,
                        ascending);

                    cases = Arrays.asList(taskArray);
                }
                /*
                // sortowanie pism obserwowanych
                else if (watches != null && watches.size() > 1)
                {
                    List<String> sortKeys = new ArrayList<String>(watches.size());

                    for (Iterator iter=watches.iterator(); iter.hasNext(); )
                    {
                        Task task = (Task) iter.next();
                        String key;
                        String onKey = task.getDocumentOfficeNumber() != null ?
                                millisFormat.format(task.getDocumentOfficeNumber().intValue()) :
                                millisFormat.format(0);
                        
                        if ("receiveDate".equals(sortField))
                        {
                            key = millisFormat.format(task.getReceiveDate() != null ?
                                    task.getReceiveDate().getTime() : 0);
                        }
                        else if ("sender".equals(sortField))
                        {
                            key = task.getSender() != null ?
                                task.getSender().toUpperCase() : MIN_STR;
                        }
                        else if ("deadlineTime".equals(sortField))
                        {
                            key = millisFormat.format(task.getDeadlineTime() != null ?
                                    task.getDeadlineTime().getTime() : 0);
                        }
                        else if ("description".equals(sortField))
                        {
                            key = task.getDescription() != null ?
                                task.getDescription().toUpperCase() : MIN_STR;
                        }
                        else if ("objective".equals(sortField))
                        {
                            key = task.getObjective() != null ?
                                task.getObjective().toUpperCase() : MIN_STR;
                        }
                        else // documentOfficeNumber
                        {
                            key = onKey;
                        }

                        // na ko�cu klucza zawsze numer kancelaryjny, poniewa�
                        // jest to poboczny klucz sortowania
                        key += ("<>" + onKey);

                        sortKeys.add(key);                            
                    }

                    Task[] taskArray = (Task[]) watches.toArray(new Task[watches.size()]);
                    std.sort.qsort(
                        (String[]) sortKeys.toArray(new String[sortKeys.size()]),
                        taskArray,
                        ascending);

                   watches = Arrays.asList(taskArray);
                }
                */
            }

            /*
            if (DSApi.context().hasPermission(DSPermission.ZADANIE_KONCZENIE))
                actionManualFinish = true;
            if ((TAB_IN.equals(tab) || TAB_OUT.equals(tab)) &&
                !Configuration.hasExtra("business"))
                actionAddToCase = true;
            if (Configuration.hasExtra("business"))
                actionAttachmentsAsPdf = true;
            if ((TAB_OUT.equals(tab) && DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO)))
                actionAssignOfficeNumber = true;
            actionMultiAssignment = true;
            if (Docusafe.hasExtra("business"))
            {
                actionMultiBoxing = true;
            }
            */

            /*
            if (tasks != null && isPager)
            {
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return
                            HttpUtils.makeUrl(getBaseLink(),
                            new Object[] {
                            "tab", tab,
                            "sortField", sortField,
                            "ascending", String.valueOf(ascending),
                            "simpleTaskList", String.valueOf(simpleTaskList),
                            "fullTaskList", String.valueOf(fullTaskList),
                            "username", username,
                            "offset", String.valueOf(offset)});
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, tasksSize, 10);
            }
            */
        }
        catch (EdmException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    boolean prepareActivity()
    {
        Random rnd = new Random();
        try
        {
            fullTaskList = rnd.nextBoolean();
            simpleTaskList = !fullTaskList;
            switch (rnd.nextInt(4))
            {
                case 0:
                    tab = TaskListAction.TAB_IN;
                    break;
                case 1:
                    tab = TaskListAction.TAB_INT;
                    break;
                case 2:
                    tab = TaskListAction.TAB_OUT;
                    break;
                case 3:
                    tab = TaskListAction.TAB_CASES;
                    break;
                default:
                    tab = TaskListAction.TAB_IN;
                    break;                        
            }
            searchOfficeNumber = null;
            searchOfficeId = null;
            tasks = null;
            cases = null;
            DSApi.openAdmin();
            return true;
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    void afterActivity()
    {
        try
        {
            DSApi._close();
            /*
            if (tasks != null)
            {
                Iterator<Task> itr = tasks.iterator();
                while (itr.hasNext())
                {                    
                    Task t = itr.next();
                }
            }
            else if (cases != null)
            {
                Iterator<CaseTask> itr = cases.iterator();
                while (itr.hasNext())
                {                    
                    CaseTask t = itr.next();
                }
            }
            */
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
}
