package pl.compan.docusafe.core.test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.DocumentSignature;
import pl.compan.docusafe.core.base.Flags.Flag;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgencja;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgent;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.names.WfProcessURN;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.AssignmentDescriptor;
import pl.compan.docusafe.core.office.workflow.ProcessId;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowUtils;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
import pl.compan.docusafe.core.office.workflow.internal.WfActivityImpl;
import pl.compan.docusafe.core.office.workflow.internal.WfProcessImpl;
import pl.compan.docusafe.core.office.workflow.jbpm.JbpmManager;
import pl.compan.docusafe.core.office.workflow.jbpm.JbpmManager.ProcessSummary;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Daa;
import pl.compan.docusafe.web.DrsInfo;
import pl.compan.docusafe.web.common.BeanBackedMap;
import std.fun;
import std.lambda;

/**
 * Akcja do testu wydajno�ci wykonuj�ca czynno�� podgl�du podsumowania
 * dla dokumentu przychodz�cego.
 * 
 * @author Marcin Wlizlo
 *
 */
public class SummaryTestActivity extends TestActivityBase
{
    private static final Log log = LogFactory.getLog(SummaryTestActivity.class);

    private String username;

    private InOfficeDocument document;
    private String folderPrettyPath;
    private String caseLink;
    private boolean documentFax;
    private String activity;
    private String signatureSrc;
    private Long signatureDocumentId;
    private List<DrsInfo> drsDocs;
    private String trackingNumber;
    private List<Flag> globalFlags;
    private List<Flag> userFlags;
    private boolean flagsPresent;
    private Date deadlineTime;
    private String deadlineRemark;
    private String deadlineAuthor;
    private String process;
    private boolean externalWorkflow;
    private Collection<Map> attachments;
    private BeanBackedMap recentRemark;
    private String boxNumber;
    private boolean canInitJbpmProcess;
    private boolean showNotInBoxMessage;
    private Map<Long, String> jbpmProcesses;
    private Long jbpmProcessId;
    private Long documentId;
    private String initJbpmLink;
    private String jbpmLink;
    private List<ProcessSummary> jbpmSummary;
    private boolean canReopenWf;
    private Object finishOrAssignState;
    private LinkedHashMap<String, String> assignmentsMap;
    private LinkedHashMap<String, String> wfProcesses;
    private LinkedHashMap<String, String> substituted;
    private boolean enableCollectiveAssignments;
    private String caseDocumentId;
    private String assignedDivision;
    private String creatingUser;
    private boolean blocked;
    private DocumentSignature recentSignature;
    private boolean useSignature;
    private boolean canSign;
    private String daaSummary;
    private boolean daaUnassigned;
    
    
    public SummaryTestActivity(String username)
    {
        super();
        this.username = username;
    }
    
    private final String getActivity()
    {
        return activity;
    }

    private String getProcess()
    {
        return process;
    }

    void actionPerformed()
    {//Zakomentowane bo nie dziala. DLA RAFALA
        try
        {
            DSUser user = DSUser.findByUsername(username);
            TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
            List<Task> tasks = (List<Task>) taskList.getDocumentTasks(null, user, InOfficeDocument.TYPE, null, "receiveDate", false, 0, 0);
            if (tasks.isEmpty())
                return;
            documentId = tasks.get(0).getDocumentId();
            document = InOfficeDocument.findInOfficeDocument(documentId);
            boolean nullDockind = (document.getDocumentKind() == null);
            if (!nullDockind)
            {
                document.getDocumentKind().initialize();
                Map<String, String> properties = document.getDocumentKind().getProperties();
                if (properties.containsKey(DocumentKind.SHOW_FOLDER_IN_SUMMARY_KEY)
                    && properties.get(DocumentKind.SHOW_FOLDER_IN_SUMMARY_KEY).equals("true"))
                    folderPrettyPath = document.getFolderPath("/");
            }
            if(document.getContainingCase()!=null)
                caseLink = "/office/edit-case.do?id="+document.getContainingCase().getId()+"&tabId=summary"; 
            documentFax = "fax".equals(document.getSource());
            WfActivity activity = getActivity() != null
                ? WorkflowFactory.getWfActivity(getActivity())
                : null;
            fillForm(document, activity);

            // odno�niki do Dokument�w Rejestru Szk�d zwi�zanych z bie��cym dokumentem

            trackingNumber = document.getTrackingNumber();

            // flagi
            String flagsOn = Configuration.getProperty("flags");
            if ("true".equals(flagsOn))
            {
                globalFlags = document.getFlags().getDocumentFlags(false);
                userFlags = document.getFlags().getDocumentFlags(true);

                flagsPresent = globalFlags.size() > 0 || userFlags.size() > 0;
            }
            else
                flagsPresent = false;

            if (getActivity() != null)
            {
                WfProcessImpl wfpi = null;
                if (activity instanceof WfActivityImpl)
                {
                    wfpi = (WfProcessImpl) activity.container();
                }

                if (wfpi != null)
                {
                    deadlineTime = wfpi.getDeadlineTime();
                    deadlineRemark = wfpi.getDeadlineRemark();
                    deadlineAuthor = DSUser.safeToFirstnameLastname(wfpi.getDeadlineAuthor());
                }
            }
            else if (getProcess() != null)
            {
                WfProcessImpl wfpi = null;
                URN urn = URN.parse(getProcess());
                if (urn instanceof WfProcessURN &&
                    ((WfProcessURN) urn).getWorkflow().equals(InternalWorkflowService.NAME))
                {
                    wfpi = WfProcessImpl.findByKey(((WfProcessURN) urn).getKey());
                }

                if (wfpi != null)
                {
                    deadlineTime = wfpi.getDeadlineTime();
                    deadlineRemark = wfpi.getDeadlineRemark();
                    deadlineAuthor = DSUser.safeToFirstnameLastname(wfpi.getDeadlineAuthor());
                }
            }
        }
        catch (EdmException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    public void setExternalWorkflow(boolean externalWorkflow)
    {
        this.externalWorkflow = externalWorkflow;
    }

    protected void prepareAttachments(OfficeDocument document) throws EdmException
    {
        attachments = fun.map(document.getAttachments(), new lambda<Attachment, Map>()
        {
            public Map act(Attachment attachment)
            {
                BeanBackedMap map = new BeanBackedMap(attachment, 
                    "id", "title", "barcode", "lparam", "mostRecentRevision", "cn");
                try
                {
                    map.put("userSummary", DSUser.safeToFirstnameLastname(attachment.getAuthor()));
                    map.put("attCtime", attachment.getCtime());
                }
                catch (EdmException e)
                {
                }
                return map;
            }
        });
    }

    protected void updateRecentRemark(OfficeDocument document) throws EdmException
    {
        if (document.getRemarks().size() > 0)
        {
            recentRemark = new BeanBackedMap(document.getRemarks().size()-1, "content");
            recentRemark.put("author", DSUser.safeToFirstnameLastname(document.getRemarks().get(document.getRemarks().size()-1).getAuthor()));
            recentRemark.put("content", document.getRemarks().get(document.getRemarks().size()-1).getContent());
        }
    }

    public final Long getDocumentId()
    {
        return documentId;
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/incoming/external-workflow.action";
    }

    public boolean isExternalWorkflow()
    {
        return externalWorkflow;
    }

    protected void prepareCollectiveAssignments() throws EdmException
    {
        boolean collectiveAssignmentPossible =
            (Configuration.officeAvailable() || Configuration.faxAvailable())
                && !StringUtils.isEmpty(getActivity())
                && DSApi.context().hasPermission(DSPermission.WWF_DEKRETACJA_ZBIORCZA);

        if (!collectiveAssignmentPossible)
            return;

        Preferences prefs = null;
        String[] keys = null;
        try
        {
            if (DSApi.context().userPreferences().nodeExists("collectiveassignment")
                && DSApi.context().userPreferences().node("collectiveassignment").keys().length > 0)
            {
                prefs = DSApi.context().userPreferences().node("collectiveassignment");
            }
            else if (DSApi.context().systemPreferences().nodeExists("modules/office/collectiveassignment")
                && DSApi.context().systemPreferences().node("modules/office/collectiveassignment").keys().length > 0)
            {
                prefs = DSApi.context().systemPreferences().node("modules/office/collectiveassignment");
            }

            if (prefs != null) {
                keys = prefs.keys();
                Arrays.sort(keys);
            }
          
        }
        catch (BackingStoreException e)
        {
            log.error(e.getMessage(), e);
            throw new EdmException("B��d odczytu ustawie� u�ytkownika", e);
        }

        if (prefs == null)
        {
            return;
        }

        assignmentsMap = new LinkedHashMap<String, String>();
        wfProcesses = new LinkedHashMap<String, String>();
        substituted = new LinkedHashMap<String, String>();
        Date now = new Date();
        
        for (int i=0; i < keys.length; i++)
        {
            AssignmentDescriptor ad =
                AssignmentDescriptor.forDescriptor(prefs.get(keys[i], ""));

            assignmentsMap.put(ad.toDescriptor(), ad.format());
            
            DSUser temp=DSUser.findByUsername(ad.getUsername());
            if(temp.getSubstituteUser()!=null && temp.getSubstitutedFrom().before(now))
                substituted.put(temp.asFirstnameLastname(),temp.getSubstituteUser().asFirstnameLastname());
            else substituted.put("","");
            
        }

        StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        if (WorkflowUtils.isManual(WorkflowFactory.getWfActivity(getActivity()).container().manager()))
        {
            wfProcesses.put(
                ProcessId.create(
                    InternalWorkflowService.NAME,
                    WorkflowUtils.iwfPackageName(),
                    WorkflowUtils.iwfManualName()).toString(),
                    smG .getString("doRealizacji"));
        }
        wfProcesses.put(
            ProcessId.create(
                InternalWorkflowService.NAME,
                WorkflowUtils.iwfPackageName(),
                WorkflowUtils.iwfCcName()).toString(),
                smG.getString("doWiadomosci"));

        enableCollectiveAssignments = true;
    }

    protected void fillForm(OfficeDocument document, WfActivity activity) throws EdmException
    {
        //setTabs(prepareTabs());
        setExternalWorkflow(WorkflowUtils.isExternalWorkflow(activity));
        DSApi.initializeProxy(document.getRecipients());
        prepareAttachments(document);
        updateRecentRemark(document);

        if (document.getBox() != null)
            boxNumber = document.getBox().getName();

        // inicjalizacja na potrzeaby zewnetrzengo workflow JBPM
        canInitJbpmProcess = JbpmManager.canInitJbpmProcess(document, getActivity());
        showNotInBoxMessage = JbpmManager.showNotInBoxMessage(document);
        if (Configuration.workflowAvailable())
        {
            jbpmProcesses = JbpmManager.prepareJbpmProcesses();
            jbpmProcessId = JbpmManager.defaultProcess(document);

            String[] params = new String[] {
                "documentId", String.valueOf(getDocumentId()),
                "activity", getActivity(),
                "process", getProcess()};

            String tmpLink = HttpUtils.makeUrl(getExternalWorkflowBaseLink(), params);
            initJbpmLink = tmpLink + "&doInitJbpmProcess=true";
            jbpmLink = tmpLink + "&doShowTask=true";
            
            jbpmSummary = JbpmManager.createProcessSummaries(document);
        }


        if (isExternalWorkflow())
        {
            canReopenWf = false;
            finishOrAssignState = null;            
        }
        else
        {
            prepareCollectiveAssignments();
            canReopenWf = !InternalWorkflowService.documentHasTasks(
                getDocumentId(), WorkflowUtils.iwfPackageName(), WorkflowUtils.iwfManualName());
            //finishOrAssignState = WorkflowUtils.getFinishOrAssignState(document, activity);
        }
        caseDocumentId = document.getCaseDocumentId();
        assignedDivision = DSDivision.find(document.getAssignedDivision()).getPrettyPath();
        creatingUser = DSUser.safeToFirstnameLastname(document.getCreatingUser());
        if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE)) 
        {
            blocked = (document.getBlocked() != null && document.getBlocked());
            recentSignature = DocumentSignature.findMostRecentSignature(document.getId()/*, true*/);
            useSignature = DSApi.context().userPreferences().node("signature").getBoolean("use", false);
            canSign = DSApi.context().hasPermission(DSPermission.PISMO_PODPISANIE);
        }

        // tresc dla dokumentu archiwum agenta
        if (Docusafe.hasExtra("daa") && (document.getDoctype() != null) && ("daa".equals(document.getDoctype().getCn())))
        {
            Doctype doctype = Doctype.findByCn("daa");
            Map doctypeFields = doctype.getValueMap(document.getId());

            daaSummary = Daa.DEFAULT_TITLE;
          /*  
            Doctype.EnumItem typEnum = doctype.getFieldByCn("TYP_DOKUMENTU").getEnumItem(new Integer(doctypeFields.get(doctype.getFieldByCn("TYP_DOKUMENTU").getId()).toString()));
            summary = summary +" / "+typEnum.getTitle();
            */
            Doctype.EnumItem typEnum = doctype.getFieldByCn("TYP_DOKUMENTU").getEnumItem(new Integer(doctypeFields.get(doctype.getFieldByCn("TYP_DOKUMENTU").getId()).toString()));
            Doctype.EnumItem siecEnum = doctype.getFieldByCn("RODZAJ_SIECI").getEnumItem(new Integer(doctypeFields.get(doctype.getFieldByCn("RODZAJ_SIECI").getId()).toString()));
            String klasaId = doctypeFields.get(doctype.getFieldByCn("KLASA_RAPORTU").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("KLASA_RAPORTU").getId()).toString() : null;
            Doctype.EnumItem klasaRaportuEnum = klasaId != null ? doctype.getFieldByCn("KLASA_RAPORTU").getEnumItem(new Integer(klasaId)) : null;
            String agentIds = doctypeFields.get(doctype.getFieldByCn("AGENT").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("AGENT").getId()).toString() : null;
            String agencjaIds = doctypeFields.get(doctype.getFieldByCn("AGENCJA").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("AGENCJA").getId()).toString() : null;

            DaaAgent agent = null;
            DaaAgencja agencja = null;
            if (agentIds != null) {
                Long agentId =  new Long(agentIds);
                agent = new DaaAgent().find(agentId);
                //summary = summary + " / "+ agent.getImie() + " "+ agent.getNazwisko();
            }
            if (agencjaIds != null) {
                Long agencjaId =  new Long(agencjaIds);
                agencja = new DaaAgencja().find(agencjaId);
                //summary = summary + " / "+ agencja.getNazwa();
            }

            if ("RODZAJ_AGENCJA".equals(typEnum.getArg1()))
            {
                if (agencja != null)
                {
                    if ((agencja.getNumer() != null) || (agencja.getNip() != null))
                        daaSummary += ((agencja.getNumer() != null) ? (" / " + agencja.getNumer()) : "") + ((agencja.getNip() != null) ? (" / " + agencja.getNip()) : "");
                }
                daaSummary += " / " + siecEnum.getTitle();
            }
            else if ("RODZAJ_AGENT".equals(typEnum.getArg1()))
            {
                if (agent != null)
                {
                    if (agent.getNumer() != null)
                        daaSummary += " / " + agent.getNumer();
                    daaSummary += " / " + agencja.getNazwa() + ((agencja.getNumer() != null) ? (" / " + agencja.getNumer()) : "") + ((agencja.getNip() != null) ? (" / " + agencja.getNip()) : "");
                }
                daaSummary += " / " + siecEnum.getTitle();
            }
            else if ("RODZAJ_RAPORT".equals(typEnum.getArg1()))
            {
                daaSummary += " / " + klasaRaportuEnum.getTitle();
                if (agencja != null)
                {
                    if (agencja.getNumer() != null || agencja.getNip() != null)
                        daaSummary += ((agencja.getNumer() != null) ? (" / " + agencja.getNumer()) : "") + ((agencja.getNip() != null) ? (" / " + agencja.getNip()) : "");
                    daaSummary += " / " + siecEnum.getTitle();
                }
            }

            if (agentIds == null && agencjaIds == null && !("RODZAJ_RAPORT".equals(typEnum.getArg1())))
                daaUnassigned = true;
        }

    }

    void afterActivity()
    {
        try
        {
            DSApi._close();
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    boolean prepareActivity()
    {
        Random rnd = new Random();
        try
        {
            DSApi.openAdmin();
            return true;
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            return false;
        }
    }
    
}
