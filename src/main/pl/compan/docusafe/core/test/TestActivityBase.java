package pl.compan.docusafe.core.test;

/**
 * Klasa bazowa dla czynno�ci wykonywanych przez w�tek testu wydajno�ci.
 * Uwaga: testy nie powinny dokonywa� zmian w bazie danych!
 * 
 * @author Marcin Wlizlo
 */
public abstract class TestActivityBase
{
    private double probability;
    private boolean enabled;
    private String name;
    
    public TestActivityBase()
    {
        probability = 1.0;
        enabled = true;
        name = this.getClass().getName();
    }
    
    public TestActivityBase(double probability, boolean enabled, String name)
    {
        this.probability = probability;
        this.enabled = enabled;
        if (name != null)
            this.name = name;
        else
            this.name = this.getClass().getName();
    }
    
    abstract void actionPerformed();
    
    /**
     * Czynno�ci przygotowuj�ce akcj�. Je�eli z jakiego� powodu nie mo�na
     * wykona� akcji metoda powinna zwraca� false, w przeciwnym wypadku true.
     * @return
     */
    abstract boolean prepareActivity();

    /**
     * Czynno�ci wykonywane po wykonaniu akcji a tak�e w przypadku kiedy
     * akcja nie zosta�a wykonana z powodu zwr�cenia false przez metod�
     * prepareActivity.
     * @return
     */
    abstract void afterActivity();

    public void setProbability(double probability)
    {
        this.probability = probability;
    }

    public double getProbability()
    {
        return probability;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        if (name != null)
            this.name = name;
        else
            this.name = this.getClass().getName();
    }

}
