package pl.compan.docusafe.core.test;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Klasa obsługująca testy wydajnościowe. Uruchamia wątek symulujący
 * wykonywanie przez wybranego użytkownika określonych akcji interfejsu.
 * 
 * @author Marcin Wlizlo
 *
 */
public class PerformanceTest extends Thread
{
    private static final Log log = LogFactory.getLog(PerformanceTest.class);
    
    private String username = null;
    private Map<String, TestActivityBase> activities = new LinkedHashMap<String, TestActivityBase>();
    private Long minWaitMilis;
    private Long maxWaitMilis;
    private Long testTimeMilis;
    
    public PerformanceTest(String username, Long testMilis)
    {
        this.username = username;
        minWaitMilis = 4000L;
        maxWaitMilis = 6000L;
        testTimeMilis = testMilis;
    }
    
    public PerformanceTest(String username, Long testMilis, Long minWait, Long maxWait)
    {
        this.username = username;
        minWaitMilis = minWait;
        maxWaitMilis = maxWait;
        testTimeMilis = testMilis;
    }
    
    public void run()
    {
        if (username == null || activities.isEmpty())
            return;
        
        try
        {
            log.info("Rozpoczęcie testu wydajności dla użytkownika " + username);
            Random rnd = new Random();
            Long startTimeMilis = System.currentTimeMillis();
            Long prevTimeMilis = startTimeMilis;
            while (prevTimeMilis - startTimeMilis <= testTimeMilis)
            {
                Long wait = minWaitMilis;
                if (maxWaitMilis > minWaitMilis)
                    wait += Math.abs(rnd.nextLong() % (maxWaitMilis - minWaitMilis));
                log.info("Test[" + username + "]: " + "oczekiwanie " + wait + "ms");
                Thread.sleep(wait);
                
                TestActivityBase activity = randomActivity(rnd.nextDouble());
                log.info("Test[" + username + "]: " + "wybrana czynność: " + activity.getName());
                
                if (activity.prepareActivity())
                {
                    log.info("Test[" + username + "]: " + "czynność: " + activity.getName() + " - rozpoczęcie");
                    Long activityStart = System.currentTimeMillis();
                    activity.actionPerformed();
                    Long activityElapsed = System.currentTimeMillis() - activityStart;
                    log.info("Test[" + username + "]: " + "czynność: " + activity.getName() + " - zakończenie" +
                        ", czas wykonania: " + timeString(activityElapsed));
                }       
                else
                {
                    log.error("Test[" + username + "]: " + "czynność: " + activity.getName() + " nie została wykonana");
                }
                activity.afterActivity();
                
                prevTimeMilis = System.currentTimeMillis();
            }
            log.info("Test[" + username + "]: " + "zakończenie testu");
        }
        catch (Exception e)
        {
        }        
    }
    
    private String timeString(Long time)
    {
        /*
        long t = time.longValue();
        String milis = new Long(t % 1000).toString();
        while (milis.length() < 3)
            milis = "0" + milis;
        t /= 1000;
        String secs = new Long(t % 60).toString();
        return secs + "." + milis + "s";
        */
        Double dtime = new Double(time);
        dtime /= 1000.0;
        return dtime.toString() + "s";
    }

    private TestActivityBase randomActivity(double d)
    {
        double sum = 0.0;
        Iterator<TestActivityBase> itr = activities.values().iterator();
        while (itr.hasNext())
        {
            TestActivityBase activity = itr.next();
            sum += activity.getProbability();
            if (d < sum)
                return activity;
        }
        return null;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void addActivity(TestActivityBase activity)
    {
        activities.put(activity.getName(), activity);
    }
    
    public void addActivity(String name, TestActivityBase activity)
    {
        activity.setName(name);
        activities.put(activity.getName(), activity);
    }
    
    public void removeActivity(String name)
    {
        activities.remove(name);
    }
    
    public void enableActivity(String name, boolean enable)
    {
        TestActivityBase activity = activities.get(name);
        if (activity != null)
            activity.setEnabled(enable);
    }
    
    public void setActivityProb(String name, double prob)
    {
        TestActivityBase activity = activities.get(name);
        if (activity != null)
            activity.setProbability(prob);
    }
    
    public TestActivityBase getActivity(String name)
    {
        TestActivityBase activity = activities.get(name);
        return activity;
    }
    
    public void clearActivities()
    {
        activities.clear();
    }
    
    public void equalProbability(boolean enableAll)
    {
        int n = activities.size();
        Iterator<TestActivityBase> itr = activities.values().iterator();
        while (itr.hasNext())
        {
            TestActivityBase activity = itr.next();
            activity.setProbability(1.0 / n);
            if (enableAll)
                activity.setEnabled(true);
        }
    }

    public void setMinWaitMilis(Long minWaitMilis)
    {
        this.minWaitMilis = minWaitMilis;
    }

    public void setMaxWaitMilis(Long maxWaitMilis)
    {
        this.maxWaitMilis = maxWaitMilis;
    }

}
