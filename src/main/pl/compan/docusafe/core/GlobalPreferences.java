package pl.compan.docusafe.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.parametrization.p4.P4Helper;
import pl.compan.docusafe.util.BundleStringManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.CurrentDayAction;

import javax.security.auth.Subject;
import java.util.*;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;


/* User: Administrator, Date: 2005-04-26 16:32:43 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: GlobalPreferences.java,v 1.55 2010/07/27 13:01:27 kamilj Exp $
 */
public abstract class GlobalPreferences
{
	static final Logger log = LoggerFactory.getLogger(GlobalPreferences.class);
    private static final String NODE_BOK = "modules/office/bok";
    private static final String BOK_GUID = "divisionGuid";
    
    public static final String NODE_NEW_BARCODE_PRINTER = "newBarcodePrinter";
    
    private static final String NODE_DISPATCH_OFFICE ="modules/office/dispatch";
    private static final String DISPATCH_OFFICE_GUID_PROPERTY = "divisionGuid"; 
    
    // Lista u�ytkownik�w kt�rzy mog� sie logowa� w stanie awaryjnym 
    private static Map<String, String> freeUser =  new HashMap<String,String>();
    // Zmienna boolowska informuj�ca czy system jest w stanie awaryjnym
    public static boolean EMERGENCY = false;
    // Lista u�ytkownik�w kt�rzy loguj� sie awaryjnie z pliku
    private static Map<String, String> loginFile = new HashMap<String,String>();

    private static final String NODE_ADDRESS = "modules/coreoffice/address";

    public static final String ORGANIZATION = "organization";
    public static final String STREET = "street";
    public static final String ZIP = "zip";
    public static final String LOCATION = "location";
    public static final String REGION = "region";
    public static final Map<String,Locale> localeMap  = new HashMap<String,Locale>();
   // private static Preferences localePref ;
    public static final Map<String, ResourceBundle> resourceBundleMap = new HashMap<String, ResourceBundle>();
    
    
    public static final String EXPLORE_DOCUMENTS = "explore-documents";
    public static final String TASKS = "task-list";
    public static final String FIND_OFFICE_DOCUMENTS = "find-office-documents";
    public static final String TASKS_BOK = "tasks-bok";
    public static final String SEARCH_DOCUMENTS_REDIRECT = "search-documents-redirect";
    public static final String NEW_IN_OFFICE_DOCUMENT = "new";
    public static final String BOOKMARKS = "bookmarks";
    public static final String NEWS = "news";
    public static final String PORTLETS = "portlet-start-page";

    private static final String NODE_COREOFFICE = "modules/coreoffice";

    private static final String OPENYEAR = "openyear";
    private static final String CURRENTDAY = "currentday";
    private static final String ADMINUSER = "adminusername";
    private static final String INSTANTASSIGNMENTGROUP = "instantassignmentgroup";
    private static final String COLLECTIVEASSIGNMENTGROUP = "collectiveassignmentgroup";
    private static final String ENABLECOLLECTIVEASSIGNMENT = "enablecollectiveassignment";
    private static final String OFFICENUMBERSFROMAUXJOURNALS = "officenumbersfromauxjournals";
    private static final String WFPROCESSFORCLONEDINDOCUMENTS = "wfprocessforclonedindocuments";
    // pude�ko, w kt�rym znajduj� si� fizyczne kopie dokument�w - numer otwartego pud�a

    //private static final String 

    private static final String NODE_COREOFFICE_BOX = NODE_COREOFFICE + "/box";
    private static final String BOX_ID = "number";
    private static final String BOX_IS_OPEN = "open";
    private static final String NODE_COREOFFICE_BOX_LINES = NODE_COREOFFICE_BOX + "/lines";
    
    private static final String NODE_COREOFFICE_BARCODES = NODE_COREOFFICE + "/barcodes";
    private static final String BARCODE_PREFIX = "prefix";
    private static final String BARCODE_SUFFIX = "suffix";
    private static final String BARCODE_USE = "use";

    private static final String NODE_COMPILE_ATTACHMENTS = NODE_COREOFFICE + "/compile-att";

    private static final String UNODE_LOCAL = "local";
    private static final String LOCAL = "name";
    
    private static final String UNODE_STARTPAGE = "startpage";
    private static final String STARTPAGE = "name";

    private static final String NODE_CERTIFICATES = "modules/certificates";
    private static final String COMPILE_ORDER = "compile-order";
    
    private static final String NODE_CUSTOMER = "customer";
    private static final String CUSTOMER_NAME = "name";

    private static final String CA_CERT_PATH = "cacertificatepath";
    private static final String CA_KEY_PATH = "caprivatekeypath";

    private static final String UNODE_BARCODE_PRINTER = "barcode-printer";
    private static final String BARCODE_PRINTER_DEVICE = "device";
    private static final String BARCODE_PRINTER_USE = "use";

    private static final String DOCUSAFE_INSTANCE_TYPE = "docusafe-type";
    public static final String DOCUSAFE_TEST_INSTANCE = "test-instance";
    public static final String DOCUSAFE_PRODUCTION_INSTANCE = "production-instance";
    
    public static final String NEWS_PREFERENCES = "news";
    public static final String POST_NUMBER = "postNumber";
    
    public static final String BIP_DATAMART = "bip/datamart";
    public static final String BIP_DATAMART_LAST_EV_ID = "lastEventID";
    public static final String BIP_DATAMAERT_START_TIME = "startTime";
    public static final String BLANK_PAGE="blank-page";
    public static final String HAND_WRITTEN_SIGNATURE="hand-written-signature";
    

    static 
    {
        localeMap.put("pl",new Locale("pl", "PL"));
        localeMap.put("en",new Locale("en", "GB"));
//        localeMap.put("it",new Locale("it", "IT"));
//        localeMap.put("de",new Locale("de", "DE"));
        
        for(String lang : localeMap.keySet())
        {
        	resourceBundleMap.put(lang, ResourceBundle.getBundle("Resources", localeMap.get(lang)));
        }        
        
    } 
    public static String getBokDivisionGuid()
    {
        return DSApi.context().systemPreferences().node(NODE_BOK).get(BOK_GUID, null);
    }
    
    public static void setBokDivisionGuid(String guid)
    {
        if (guid == null)
        {
            DSApi.context().systemPreferences().node(NODE_BOK).remove(BOK_GUID);
        }
        else
        {
            DSApi.context().systemPreferences().node(NODE_BOK).put(BOK_GUID, guid);
        }
    }
    
    /**
     * Zwraca guid dzia�u wysy�ek. 
     */
    public static String getDispatchDivisionGuid(){
    	return DSApi.context().systemPreferences().node(NODE_DISPATCH_OFFICE).get(DISPATCH_OFFICE_GUID_PROPERTY, null);
    }
    
    public static void setDispatchDivisionGuid(String guid) {
		if (guid == null) {
			DSApi.context().systemPreferences().node(NODE_DISPATCH_OFFICE)
					.remove(DISPATCH_OFFICE_GUID_PROPERTY);
		} else {
			DSApi.context().systemPreferences().node(NODE_DISPATCH_OFFICE).put(
					DISPATCH_OFFICE_GUID_PROPERTY, guid);
		}
	}

    public static String getCustomer()
    {
        return DSApi.context().systemPreferences().node(NODE_CUSTOMER).get(CUSTOMER_NAME, null);
    }

    public static void setCustomer(String name)
    {
        if (name == null)
        {
            DSApi.context().systemPreferences().node(NODE_CUSTOMER).remove(CUSTOMER_NAME);
        }
        else
        {
            DSApi.context().systemPreferences().node(NODE_CUSTOMER).put(CUSTOMER_NAME, name);
        }
    }
    /**
     * Zwraca ilo�� post�w wy�wietlan� w aktualno�ciach.
     * @return
     */
    public static Integer getPostNumber()
    {
        return DSApi.context().systemPreferences().node(NEWS_PREFERENCES).getInt(POST_NUMBER, 3);
    }
    /**
     * Ustawia liczb� post�w wy�wietlanych w aktualno�ciach.
     * @param number
     */
    public static void setPostNumber(Integer number)
    {
        if (number == null)
        {
            DSApi.context().systemPreferences().node(NEWS_PREFERENCES).remove(POST_NUMBER);
        }
        else
        {
            DSApi.context().systemPreferences().node(NEWS_PREFERENCES).putInt(POST_NUMBER, number);
        }
    }

    public static String getCaCertificatePath()
    {
        return DSApi.context().systemPreferences().node(NODE_CERTIFICATES).get(CA_CERT_PATH, null);
    }

    public static void setCaCertificatePath(String path)
    {
        if (path == null)
        {
            DSApi.context().systemPreferences().node(NODE_CERTIFICATES).remove(CA_CERT_PATH);
        }
        else
        {
            DSApi.context().systemPreferences().node(NODE_CERTIFICATES).put(CA_CERT_PATH, path);
        }
    }
    
    public static String getCaPrivateKeyPath()
    {
        return DSApi.context().systemPreferences().node(NODE_CERTIFICATES).get(CA_KEY_PATH, null);
    }

    public static void setCaPrivateKeyPath(String path)
    {
        if (path == null)
        {
            DSApi.context().systemPreferences().node(NODE_CERTIFICATES).remove(CA_KEY_PATH);
        }
        else
        {
            DSApi.context().systemPreferences().node(NODE_CERTIFICATES).put(CA_KEY_PATH, path);
        }
    }

    /**
     * Pobiera ustawienia j�zyka u�ytkownika.
     */
    public static String getUserLocal(Subject s)
    {
        try
        {
            DSApi.open(s);
            if (DSApi.context().userPreferences().node(UNODE_LOCAL).get(LOCAL, null)!=null)
            {
                return  DSApi.context().userPreferences().node(UNODE_LOCAL).get(LOCAL, null);
            }
            else
            {    
                if(Docusafe.getAdditionProperty("locale")!=null)
                    return Docusafe.getAdditionProperty("locale"); 
                else
                    return "pl";  
            }
        }
        catch (EdmException e)
        {
        	log.debug("", e);
        }
        finally
        {
            DSApi._close();
        }

        if (Docusafe.getAdditionProperty("locale")!=null)
            return Docusafe.getAdditionProperty("locale"); 
        else
            return "pl";                     
    }
    
    /**
     * Wczytuje plik properties do klasy StrinManager o podanej nazwie i lokalizacji zwraca j�.
     * 
     * @param path  scie�ka do pliku properties jaki chcemy wczyta� 
     *        (pusty string spowoduje wczytanie pliku z katalogu g��wnego)
     * @param name  nazwa pliku properties(bez ko�c�wki okre�laj�cej j�zyk),
     *          je�li null wczytuje plik o nazwie LocalStrings_x gdzie x oznacza kod j�zyka
     * @return
     */
    public static StringManager loadPropertiesFile(String path,String name)
    {    
	    		return new BundleStringManager();     
    }

    public static void setUserLocal(String name)
    {
        if (name == null)
        {
            DSApi.context().userPreferences().node(UNODE_LOCAL).remove(LOCAL);
        }
        else
        {
            DSApi.context().userPreferences().node(UNODE_LOCAL).put(LOCAL, name);
           // localePref.put(userName,name);
        }
    }
    
    public static String getUserStartPage()
    {
        return DSApi.context().userPreferences().node(UNODE_STARTPAGE).get(STARTPAGE,Docusafe.getAdditionProperty("startPage"));
    }

    public static void setUserStartPage(String name)
    {
        if (name == null)
        {
            DSApi.context().userPreferences().node(UNODE_STARTPAGE).remove(STARTPAGE);
        }
        else
        {
            DSApi.context().userPreferences().node(UNODE_STARTPAGE).put(STARTPAGE, name);
        }
    }

    public static Map<String,String> getAddress()
    {
        Preferences prefs = DSApi.context().systemPreferences().node(NODE_ADDRESS);
        Map<String, String> address = new HashMap<String, String>();
        if (prefs.get(ORGANIZATION, null) != null)
            address.put(ORGANIZATION, prefs.get(ORGANIZATION, null));
        if (prefs.get(STREET, null) != null)
            address.put(STREET, prefs.get(STREET, null));
        if (prefs.get(ZIP, null) != null)
            address.put(ZIP, prefs.get(ZIP, null));
        if (prefs.get(LOCATION, null) != null)
            address.put(LOCATION, prefs.get(LOCATION, null));
        if (prefs.get(REGION, null) != null)
            address.put(REGION, prefs.get(REGION, null));
        return address;
    }

    public static void setAddress(Map address)
    {
        Preferences prefs = DSApi.context().systemPreferences().node(NODE_ADDRESS);

        String[] keys = new String[] {
            ORGANIZATION, STREET, ZIP,
            LOCATION, REGION
        };

        for (int i=0; i < keys.length; i++)
        {
            String value = address != null ? (String) address.get(keys[i]) : null;
            if (value == null)
            {
                prefs.remove(keys[i]);
            }
            else
            {
                prefs.put(keys[i], value);
            }
        }
    }

    public static Date getCurrentDay(String journalGuidOwner) throws EdmException
    {
        Calendar cal = Calendar.getInstance();
        if(AvailabilityManager.isAvailable("realCurrentDay"))
        {
            return cal.getTime();
        }
        String sDate = DSApi.context().systemPreferences().node(GlobalPreferences.NODE_COREOFFICE).
                node(GlobalPreferences.CURRENTDAY+"_"+journalGuidOwner).get(GlobalPreferences.CURRENTDAY, null);
        if (sDate == null)
            throw new CurrentDayNotOpenException("W systemie nie otwarto dnia");
        Date date = DateUtils.parsePortableDate(sDate);
        if(DateUtils.substract(date, new Date()) == 0)
        {
            cal.setTime(new Date());
        }
        else
        {
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY,23);
            cal.set(Calendar.MINUTE,59);
        }
        return cal.getTime();
    }

    public static void setCurrentDay(Date day, String journalGuidOwner)
    {
        if (day == null)
        {
            DSApi.context().systemPreferences().node(GlobalPreferences.NODE_COREOFFICE).
                    node(GlobalPreferences.CURRENTDAY+"_"+journalGuidOwner).remove(CURRENTDAY);
        }
        else
        {
            DSApi.context().systemPreferences().node(GlobalPreferences.NODE_COREOFFICE).
                    node(GlobalPreferences.CURRENTDAY+"_"+journalGuidOwner).
                    put(CURRENTDAY, DateUtils.formatPortableDate(day));
        }
    }

    public static class CurrentDayNotOpenException extends EdmException
    {
        public CurrentDayNotOpenException(String message)
        {
            super(message);
        }
    }

    public static class CurrentYearNotOpenException extends EdmException
    {
        public CurrentYearNotOpenException(String message)
        {
            super(message);
        }
    }

    /**
     * Zwraca dat� reprezentuj�c� dzie� otwarty w systemie. Je�eli nie
     * otwarto dnia, rzucany jest wyj�tek EdmException.
     * @return
     * @throws EdmException
     */
    public static Date getCurrentDay() throws EdmException
    {
        if (CurrentDayAction.P_4_CURRENT_DAY)
        {
            String journalGuidOwner = P4Helper.journaGuidOwner();
            return getCurrentDay(journalGuidOwner);
        }
        else
        {
            Calendar cal = Calendar.getInstance();
            if(AvailabilityManager.isAvailable("realCurrentDay"))
            {
                return cal.getTime();
            }
            String sDate = DSApi.context().systemPreferences().node(GlobalPreferences.NODE_COREOFFICE).get(GlobalPreferences.CURRENTDAY, null);
            if (sDate == null)
                throw new CurrentDayNotOpenException("W systemie nie otwarto dnia");
            Date date = DateUtils.parsePortableDate(sDate);
            if(DateUtils.substract(date, new Date()) == 0)
            {
                cal.setTime(new Date());
            }
            else
            {
                cal.setTime(date);
                cal.set(Calendar.HOUR_OF_DAY,23);
                cal.set(Calendar.MINUTE,59);
            }
            return cal.getTime();
        }
    }
    
    /**
     * Zwraca dat� reprezentuj�c� dzie� otwarty w systemie dla danego dziennika. Je�eli nie znaleziono dziennika o podanym id,
     * to rzucany jest wyj�tek EdmException
     * @param journalId - id dziennika dla kt�rego ma by� zwr�cony dzie� otwarty
     * @return dat� reprezentuj�c� dzie� otwarty w systemie dla danego dziennika
     * @throws EdmException
     */
    public static Date getCurrentDay(Long journalId) throws EdmException {
		Journal journal = (Journal) DSApi.context().session().get(Journal.class, journalId);
		if(journal != null) {
			return journal.getDay();
		} else {
			throw new EdmException("Nie znaleziono dziennika o podanym id: " + journalId.toString());
		}
    }
    
    public static void setCurrentDay(Date day)
    {
        if (day == null)
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE).remove(CURRENTDAY);
        }
        else
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE).
                put(CURRENTDAY, DateUtils.formatPortableDate(day));
        }
    }
    
    /**
     * Ustawia dat� reprezentuj�c� dzie� otwarty w systemie dla danego dziennika. Je�eli nie znaleziono dziennika o podanym id,
     * to rzucany jest wyj�tek EdmException
     * @param day - data reprezentuj�c� dzie� otwarty w systemie dla danego dziennika
     * @param journalId - id dziennika dla kt�rego ma by� ustawiony dzie� otwarty
     * @throws EdmException
     */
    public static void setCurrentDay(Date day, Long journalId) throws EdmException {
		Journal journal = (Journal) DSApi.context().session().get(Journal.class, journalId);
		if(journal != null) {
			DSApi.context().begin();
			journal.setDay(day);
			DSApi.context().session().update(journal);
			DSApi.context().commit();
		} else {
			throw new EdmException("Nie znaleziono dziennika o podanym id: " + journalId.toString());
		}
		
    }
    
    public static int getCurrentYear() throws EdmException
    {
    	if(AvailabilityManager.isAvailable("realCurrentDay"))
    	{
    		Calendar cal = Calendar.getInstance();
    		return cal.get(Calendar.YEAR);
    	}
    	
        int year = DSApi.context().systemPreferences().node(NODE_COREOFFICE).
            getInt(OPENYEAR, 0);
        if (year <= 0)
        {
            throw new CurrentYearNotOpenException("W systemie nie otwarto roku");
        }
        return year;
    }

    public static void setCurrentYear(Integer year)
    {
        if (year == null)
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE).remove(OPENYEAR);
        }
        else
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE).
                putInt(OPENYEAR, year.intValue());
        }
    }

    public static Long getBoxId() throws EdmException
    {
        long id = DSApi.context().systemPreferences().node(NODE_COREOFFICE_BOX).
            getLong(BOX_ID, 0);
        return id > 0 ? new Long(id) : null;
    }

    public static void setBoxId(Long id)
    {
        if (id == null)
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE).remove(BOX_ID);
        }
        else
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE_BOX).
                putLong(BOX_ID, id.longValue());
        }
    }

    public static boolean isBoxOpen()
    {
        return DSApi.context().systemPreferences().node(NODE_COREOFFICE_BOX).
            getBoolean(BOX_IS_OPEN, false);
    }

    public static void setBoxOpen(boolean open)
    {
        DSApi.context().systemPreferences().node(NODE_COREOFFICE_BOX).
            putBoolean(BOX_IS_OPEN, open);
    }

    public static Long getBoxId(String line)
    {
        long id = DSApi.context().systemPreferences().node(NODE_COREOFFICE_BOX_LINES).
            getLong(line, 0);
        return id > 0 ? new Long(id) : null;
    }
    
    public static void setBoxId(String line, Long id)
    {
        if (id == null)
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE_BOX_LINES).remove(line);
        }
        else
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE_BOX_LINES).
                putLong(line, id.longValue());
        }
    }
    
    /**
     * Nazwa u�ytkownika, kt�ry jest administratorem systemu.
     */
    public static String getAdminUsername()
    {
        return DSApi.context().systemPreferences().
            node(NODE_COREOFFICE).get(ADMINUSER, "admin");
    }

    public static void setAdminUsername(String username)
    {
        if (username == null)
        {
            DSApi.context().systemPreferences().
                node(NODE_COREOFFICE).remove(ADMINUSER);
        }
        else
        {
            DSApi.context().systemPreferences().
                node(NODE_COREOFFICE).put(ADMINUSER, username);
        }
    }

    /**
     * Guid grupy u�ytkownik�w, z kt�rej proponowani s� u�ytkownicy
     * do natychmiastowej dekretacji zaraz po przyj�ciu pisma.
     */
    public static String getInstantAssignmentGroup()
    {
        return DSApi.context().systemPreferences().
            node(NODE_COREOFFICE).get(INSTANTASSIGNMENTGROUP, null);
    }

    public static void setInstantAssignmentGroup(String guid)
    {
        if (guid == null)
        {
            DSApi.context().systemPreferences().
                node(NODE_COREOFFICE).remove(INSTANTASSIGNMENTGROUP);
        }
        else
        {
            DSApi.context().systemPreferences().
                node(NODE_COREOFFICE).put(INSTANTASSIGNMENTGROUP, guid);
        }
    }

    /**
     * Guid grupy u�ytkownik�w, kt�rej cz�onkowie znajd� si� na
     * fiszce natychmiastowej dekretacji.
     * @deprecated Nieu�ywane
     */
    public static String getCollectiveAssignmentGroup()
    {
        return DSApi.context().systemPreferences().
            node(NODE_COREOFFICE).get(COLLECTIVEASSIGNMENTGROUP, null);
    }

    public static void setCollectiveAssignmentGroup(String guid)
    {
        if (guid == null)
        {
            DSApi.context().systemPreferences().
                node(NODE_COREOFFICE).remove(COLLECTIVEASSIGNMENTGROUP);
        }
        else
        {
            DSApi.context().systemPreferences().
                node(NODE_COREOFFICE).put(COLLECTIVEASSIGNMENTGROUP, guid);
        }
    }

    /**
     * Okre�la, czy nale�y umo�liwia� zbiorcz� dekretacj�. Mo�e
     * przyjmowa� warto�ci "true", "false" lub null (domy�lnie "false").
     */
    public static boolean isEnableCollectiveAssignment()
    {
        return DSApi.context().systemPreferences().
            node(NODE_COREOFFICE).getBoolean(ENABLECOLLECTIVEASSIGNMENT, false);
    }

    public static void setEnableCollectiveAssignment(boolean enable)
    {
        DSApi.context().systemPreferences().node(NODE_COREOFFICE).
            putBoolean(ENABLECOLLECTIVEASSIGNMENT, enable);
    }

    /**
     * Je�eli parametr ma warto�� "true", numery kancelaryjne przyjmowanych
     * pism b�d� pochodzi�y z dziennik�w w dziale g��wnym, nie z g��wnego
     * dziennika. Dziennik, z kt�rego brany b�dzie numer musi mie� sw�j symbol.
     */
    public static boolean isOfficeNumbersFromAuxJournals()
    {
        return DSApi.context().systemPreferences().node(NODE_COREOFFICE).
            getBoolean(OFFICENUMBERSFROMAUXJOURNALS, false);
    }

    public static void setOfficeNumbersFromAuxJournals(boolean set)
    {
        DSApi.context().systemPreferences().node(NODE_COREOFFICE).
            putBoolean(OFFICENUMBERSFROMAUXJOURNALS, set);
    }

    /**
     * Je�eli "true", po sklonowaniu dokumentu (po utworzeniu dla niego kolejnej
     * sprawy), klon zostanie zwi�zany z w�asnym procesem workflow.
     */
    public static boolean isWfProcessForClonedInDocuments()
    {
        return DSApi.context().systemPreferences().node(NODE_COREOFFICE).
            getBoolean(WFPROCESSFORCLONEDINDOCUMENTS, false);
    }

    public static void setWfProcessForClonedInDocuments(boolean set)
    {
        DSApi.context().systemPreferences().node(NODE_COREOFFICE).
            putBoolean(WFPROCESSFORCLONEDINDOCUMENTS, set);
    }

    public static String getBarcodePrefix()
    {
        return DSApi.context().systemPreferences().node(NODE_COREOFFICE_BARCODES).
            get(BARCODE_PREFIX, null);
    }

    public static void setBarcodePrefix(String prefix)
    {
        if (prefix == null)
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE_BARCODES).
                remove(BARCODE_PREFIX);
        }
        else
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE_BARCODES).
                put(BARCODE_PREFIX, prefix);
        }
    }

    public static String getBarcodeSuffix()
    {

        return DSApi.context().systemPreferences().node(NODE_COREOFFICE_BARCODES).
            get(BARCODE_SUFFIX, null);
    }

    public static void setBarcodeSuffix(String suffix)
    {
        if (suffix == null)
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE_BARCODES).
                remove(BARCODE_SUFFIX);
        }
        else
        {
            DSApi.context().systemPreferences().node(NODE_COREOFFICE_BARCODES).
                put(BARCODE_SUFFIX, suffix);
        }
    }

    public static boolean isUseBarcodes()
    {
        return DSApi.context().systemPreferences().node(NODE_COREOFFICE_BARCODES).
            getBoolean(BARCODE_USE, false);
    }

    public static void setUseBarcodes(boolean use)
    {
        DSApi.context().systemPreferences().node(NODE_COREOFFICE_BARCODES).
            putBoolean(BARCODE_USE, use);
    }

    public static String getBarcodePrinterDevice()
    {
        return DSApi.context().userPreferences().node(UNODE_BARCODE_PRINTER).
            get(BARCODE_PRINTER_DEVICE, null);
    }

    public static void setBarcodePrinterDevice(String device)
    {
        if (device == null)
        {
            DSApi.context().userPreferences().node(UNODE_BARCODE_PRINTER).
                remove(BARCODE_PRINTER_DEVICE);
        }
        else
        {
            DSApi.context().userPreferences().node(UNODE_BARCODE_PRINTER).
                put(BARCODE_PRINTER_DEVICE, device);
        }
    }

    public static boolean getUseBarcodePrinter()
    {
        return DSApi.context().userPreferences().node(UNODE_BARCODE_PRINTER).
            getBoolean(BARCODE_PRINTER_USE, false);
    }

    public static void setUseBarcodePrinter(boolean use)
    {
        DSApi.context().userPreferences().node(UNODE_BARCODE_PRINTER).
            putBoolean(BARCODE_PRINTER_USE, use);
    }

    public static String getDocusafeInstanceType()
    {
        return DSApi.context().userPreferences().node(NODE_COREOFFICE).
            get(DOCUSAFE_INSTANCE_TYPE, DOCUSAFE_PRODUCTION_INSTANCE);
    }

    public static String getDocusafeInstanceTypeName()
    {
        String type = DSApi.context().userPreferences().node(NODE_COREOFFICE).
            get(DOCUSAFE_INSTANCE_TYPE, DOCUSAFE_PRODUCTION_INSTANCE);
        return getDocusafeTypes().get(type);
    }

    public static void setDocusafeInstanceType(String docusafeInstanceType)
    {
        if (docusafeInstanceType == null)
            docusafeInstanceType = DOCUSAFE_PRODUCTION_INSTANCE;
        DSApi.context().userPreferences().node(NODE_COREOFFICE).
            put(DOCUSAFE_INSTANCE_TYPE, docusafeInstanceType);

    }

    public static Map<String,String> getDocusafeTypes()
    {
        Map<String,String> instances = new HashMap<String,String>();
        instances.put(DOCUSAFE_PRODUCTION_INSTANCE, "Productive");
        instances.put(DOCUSAFE_TEST_INSTANCE, "Test");

        return instances;
    }
    
    public static String getCompileAttachmentsOrder()
    {
        return DSApi.context().systemPreferences().node(NODE_COMPILE_ATTACHMENTS).
            get(COMPILE_ORDER, null);
    }

    public static void setCompileAttachmentsOrder(String compileOrder)
    {
        DSApi.context().systemPreferences().node(NODE_COMPILE_ATTACHMENTS).
            put(COMPILE_ORDER, compileOrder);

    }
    
    public static void addFreeUser(String user)
    {
        freeUser.put(user,"true");
    }
    
    public static void removeFreeUser(String user)
    {
        freeUser.remove(user);
    }
    
    public static String getFreeUser(String user)
    {
        return freeUser.get(user);
    }
    
    public static Map getFreeUser()
    {
        return freeUser;
    }
    
    public static void clearFreeUser()
    {
        freeUser.clear();
    }
    
    public static void addLoginFile(String user)
    {
        loginFile.put(user,"true");
    }
    
    public static String getLoginFile(String user)
    {
        return loginFile.get(user);
    }
    
    public static void removeLoginFile(String user)
    {
        loginFile.clear();
    }
    
    public static void setBipDataMartReaderStartTime(Date startTime) throws BackingStoreException
    {
		DSApi.context().systemPreferences().node(BIP_DATAMART)
			.put(BIP_DATAMAERT_START_TIME, DateUtils.formatSqlDateTime(startTime));
		DSApi.context().systemPreferences().flush();
    }
    
    public static String getBipDataMartReaderStartTime()
    {
    	return DSApi.context().systemPreferences().node(BIP_DATAMART)
    		.get(BIP_DATAMAERT_START_TIME, null);
    }
    
    public static void setBipDataMartReaderLastEventId(String eventId) throws BackingStoreException
    {
    	DSApi.context().systemPreferences().node(BIP_DATAMART)
			.put(BIP_DATAMART_LAST_EV_ID, eventId);
    	DSApi.context().systemPreferences().flush();
    }
    
    public static String getBipDataMartReaderLastEventId(String def)
    {
    	return DSApi.context().systemPreferences().node(BIP_DATAMART)
			.get(BIP_DATAMART_LAST_EV_ID, def);
    }
}
