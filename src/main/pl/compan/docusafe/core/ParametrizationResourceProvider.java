package pl.compan.docusafe.core;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Klasa ktorej zadaniem wykonywanie roznego rodzaju czynnosci konfiguracyjnych zaleznych od 
 * indywidualnych wdrozen klienta. Zakladamy ze w systemie aktywny jest tylko jeden provider. 
 * Klasa ta powinny byc inicjowana przy starcie i byc dolaczana do roznych 
 * elementow w trakcie inicjowania  
 * @author wkutyla
 *
 */
public class ParametrizationResourceProvider 
{
	protected final static Logger log = LoggerFactory.getLogger(ParametrizationResourceProvider.class);
	
	private static ParametrizationResourceProvider provider = null;
	
	public static ParametrizationResourceProvider getInstance()
	{
		//FIXME - opracowac mechanizm inicjowania providera
		if (provider==null)
		{
			provider = new ParametrizationResourceProvider();
		}
		return provider;
	}
	
	/**
	 * Metoda dodaje do konfiguracji HBM wszystkie mozliwe definicje
	 * FIXME - po utworzeniu providerow "specyfikcznych" porozdzielac to na mniejsze twory
	 * @param sf
	 */
	public void addHibernateDefinitions(org.dom4j.Element sf)
	{
		log.debug("addHibernateDefinitions");
        // rzeczy specyficzne dla ALD
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ald/ALDInvoiceInfo.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ald/ALDInvoiceInfo2.hbm.xml"));

        // rzeczy specyficzne dla faktur kosztowych (Impuls i Aegon)
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/dictionary/CentrumKosztowDlaFaktury.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/dictionary/CentrumKosztow.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/dictionary/AccountNumber.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/dictionary/DicInvoice.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/dictionary/LocationForIC.hbm.xml"));
        
        

        //OBI
        if(AvailabilityManager.isAvailable("available.hibernate.obi"))
        {
        	log.debug("dodajemy konfiguracje OBI");
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/presale/ObiDictionaries.hbm.xml"));            
        }
        //TODO: Wiazanie do dsuser wymaga wczytania 
      //  if(AvailabilityManager.isAvailable("available.hibernate.prosika"))
        {
        	log.debug("dodajemy konfiguracje prosiki");
        	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/prosika/ProsikaDictionaries.hbm.xml"));
        	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/prosika/Package.hbm.xml"));        	
        	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/prosika/ProsikaArchiveDocument.hbm.xml"));
        	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/prosika/ProsikaDecisionRule.hbm.xml"));
        	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/prosika/DroznikBean.hbm.xml"));
        }
      //Barkody
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/prosika/barcodes/BarcodeRange.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/prosika/barcodes/BarcodePool.hbm.xml"));
        //slownik DicAegonCommission (aegon faktury prowizyjne)
        if(AvailabilityManager.isAvailable("available.hibernate.df"))
        {
        	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/dictionary/DicAegonCommission.hbm.xml"));
        }

        //Konta ksiegowe
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/dictionary/KontoKsiegowe.hbm.xml"));

        //Slowniki dla dokumentu leasingowego
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ilpol/DlContractDictionary.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ilpol/DlApplicationDictionary.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ilpol/LeasingObjectType.hbm.xml"));
        //Slownik dla dokumentu czlonkowskiego
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/dictionary/DCMember.hbm.xml"));

        //Slowniki dla parametryzacji PRESALE
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/presale/PresaleDictionaries.hbm.xml"));       

        // IFPAN
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ifpan/ErpPerson.hbm.xml"));
        
        //webservices dla Politechniki Częstochowskiej
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/polcz/ContractItemInvoice.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/polcz/ContractItem.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/polcz/Contract.hbm.xml"));
       
        //na potrzeby faktury kosztowej i nie tylko, dla Politechniki Częstochowskiej
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/polcz/MpkManagerOfDepartment.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/polcz/MpkManagerOfUnit.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/polcz/MpkFinancialSection.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/polcz/PczRyczalt.hbm.xml"));

        //IMGW
        if (AvailabilityManager.isAvailable("imgw")) {
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/imgwwaw/hbm/Budget.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/imgwwaw/hbm/Source.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/imgwwaw/hbm/CostInvoiceProduct.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/imgwwaw/hbm/ZrodlaFinansowaniaDict.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/imgwwaw/hbm/ContractorAccount.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/imgwwaw/hbm/Kontrakt.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/imgwwaw/hbm/IMGWVatRate.hbm.xml"));
        }

        // UL
		if (AvailabilityManager.isAvailable("uwlodzki")) {
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/VatRatesEnumItem.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/ProjectsEnumItem.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/ProjectBudgetsEnumItem.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/ProjectBudgetPositionsEnumItem.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/ProjectBudgetPositionResourcesEnumItem.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/ProjectsFundSourcesEnumItem.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/ResourcesFundSources.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/CostInvoiceDzialalnosc.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/OrganizationalUnitEnumItem.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/ExchangeRate.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/ProjectBudgetPositionResources.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/Product.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/ProductionOrders.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/ContractorAccount.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/DocumentTypes.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/UlBudgetPosition.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/UlObszaryMapping.hbm.xml"));
			sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ul/hib/OrderDocumentTypes.hbm.xml"));
		}
        
        //Slownik dla Rockwella
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/dictionary/RockwellVendors.hbm.xml"));
        
        // Yetico
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/hb/Klient.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/hb/NiezgodnoscWew.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/hb/Niezgodnosc.hbm.xml"));
       // sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/hb/OstDecyzjeDoRealizacji.hbm.xml"));
       // sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/hb/PropDecyzjeDoRealizacji.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/hb/Wyrob.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/hb/Zadania.hbm.xml"));
       // sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/hb/ZadREalizowaneWWeryfikacji.hbm.xml"));
        
        //CRM
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/Contractor.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/ContractorWorker.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/Region.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/Role.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/Contact.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/ContactKind.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/LeafletKind.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/Competition.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/CompetitionKind.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/CompetitionName.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/Marka.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/Patron.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/CooperationTerm.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/ContactStatus.hbm.xml"));    
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crm/Machine.hbm.xml"));
        
        //IC
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/other/InvoiceInfo.hbm.xml"));
        
        //PAA	
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/paa/PlanPracyZad.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/paa/PlanPracyPodzad.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/paa/PlanPracyMulti.hbm.xml"));
        
        //ZBP
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/zbp/hb/TerminPlatnosciMapping.hbm.xml"));

        //KM
        if (AvailabilityManager.isAvailable("available.hibernate.km")) {
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/km/dao/KMBudgetPosition.hbm.xml"));
        }
        
        //General
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/general/canonical/model/GeneralSimpleRepositoryAttribute.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/general/canonical/model/GeneralDictionary.hbm.xml"));
        
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ukw/entities/SimpleRepositoryAttribute.hbm.xml"));
        sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ukw/entities/Dictionary.hbm.xml"));
	}
}
