package pl.compan.docusafe.core;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import pl.compan.docusafe.boot.Docusafe;

/**
 * Klasa inicjuje konfiguracje Hibernate.
 * Wyciagnieta dla porzadku w kodzie
 * Dla porzadku w kodzie
 * @author wkutyla
 */

public class HibernateBuilder
{
	private static final Logger log = LoggerFactory.getLogger(HibernateBuilder .class);
	/**
     * Budowa konfiguracji HBM
     * @TODO - przygotowac zestaw avilables, ktore beda mowic czy wlaczac dane definicje. Zmniejszy to
     * ilosc klas generowanych przez CGLIB
     * @return
     * @throws EdmException
     */
    public org.w3c.dom.Document getHibernateCfg() throws EdmException
    {
        try
        {
            org.dom4j.io.SAXReader reader = new org.dom4j.io.SAXReader();

            // SAXReader nie powinien ��czy� si� z internetem, by odczyta� DTD
            reader.setEntityResolver(new EntityResolver()
            {
                final static String BASE = "http://hibernate.sourceforge.net/";
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException
                {
                    if (systemId != null && systemId.startsWith(BASE))
                    {
                        return new InputSource(getClass().getClassLoader().getResourceAsStream(
                            "org/hibernate/"+systemId.substring(BASE.length())));
                    }
                    return null;
                }
            });
            reader.setIncludeExternalDTDDeclarations(false);
            reader.setIncludeInternalDTDDeclarations(false);
            reader.setValidation(false);
            org.dom4j.Document document4j =
                reader.read(DSApi.class.getClassLoader().getResourceAsStream("hibernate.cfg.xml"));
            org.dom4j.Element elDialect = (org.dom4j.Element) document4j.selectSingleNode("/hibernate-configuration/session-factory/property[@name='dialect']");
            elDialect.setText(Docusafe.getHibernateDialectClass());

            org.dom4j.Element sf = (org.dom4j.Element) document4j.selectSingleNode("/hibernate-configuration/session-factory");
            
            org.dom4j.Element luceneDir = org.dom4j.DocumentHelper.createElement("property");
            luceneDir.addAttribute("name", "hibernate.search.default.indexBase");
            luceneDir.setText(Docusafe.getIndexesFiles().getAbsolutePath());
            sf.add(luceneDir);
            
            if(!AvailabilityManager.isAvailable("service.disable.EpuapImportService"))
            {
            	 sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/service/epuap/EpuapExportDocument.hbm.xml"));
            	 
            }

            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/service/reports/ProcessReportBean.hbm.xml"));

            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/web/swd/management/sql/passed-password.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/web/swd/management/sql/custom-filter.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/web/swd/management/sql/report.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/web/swd/management/sql/swd-quantity-query.hbm.xml"));

            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/service/epuap/multi/EpuapSkrytka.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/DSLog.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/BoxLine.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/Box.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/ExampleDoc.hbm.xml"));
            
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/aegon/wf/dictionary/WfNameDictionary.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/aegon/wf/dictionary/WfNameStatus.hbm.xml"));
            
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/PostalCode.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/BoxChangeHistory.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/Attachment.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/AttachmentRevision.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/DocumentPermission.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/FolderPermission.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/DocumentLock.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/DocumentWatch.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/Folder.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/Doctype.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/FaxCache.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/Image.hbm.xml"));
            
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/absences/FreeDay.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/absences/AbsenceType.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/absences/Absence.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/EmployeeCard.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/util/DSCountry.hbm.xml"));
            
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/durables/Durable.hbm.xml"));
            
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/reports/Report.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/users/sql/UserImpl.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/users/sql/Profile.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/users/sql/SubstituteHistory.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/users/sql/DivisionImpl.hbm.xml"));
            //nowe drzewko
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/users/sql/AcceptanceDivisionImpl.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/certificates/UserCertificate.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/resources/Resource.hbm.xml"));
            //sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/DocumentFlags.hbm.xml"));
            //sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/DocumentUserFlags.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/service/stagingarea/RetainedObject.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/service/stagingarea/RetainedObjectAttachment.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/calendar/sql/TimeImpl.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/calendar/sql/RejectionReason.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/calendar/sql/CalendarEvent.hbm.xml")); 
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/calendar/sql/UserCalendarBinder.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/calendar/Calendar.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/calendar/sql/AnyResource.hbm.xml")); 
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/DocumentKind.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/acceptances/AcceptanceCondition.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/dockinds/acceptances/DocumentAcceptance.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/labels/Label.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/labels/DocumentToLabel.hbm.xml"));

            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/regtype/RegistryEntry.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/regtype/Registry.hbm.xml"));

            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/imports/ImportedDocumentInfo.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/imports/ImportedFileInfo.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/imports/ImportedFileBlob.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/imports/ImportedDocumentCommon.hbm.xml"));
            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/imports/ImportedFileCsv.hbm.xml"));
			
			//ta metoda dodaje specyficzne mapowania
			ParametrizationResourceProvider provider = ParametrizationResourceProvider.getInstance();
			provider.addHibernateDefinitions(sf);

            //if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE))
            //{
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/AttachmentRevisionSignature.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/DocumentSignature.hbm.xml"));
                //}
                
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/address/Address.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/address/Phone.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/address/Floor.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/address/UserLocation.hbm.xml"));
                
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/news/NewsType.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/news/News.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/news/Notification.hbm.xml"));
                
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/bookmarks/FilterCondition.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/bookmarks/Bookmark.hbm.xml"));
                
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/bilings/Biling.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/bilings/UserPhoneNumber.hbm.xml"));
                
                //            if (Docusafe.moduleAvailable(Modules.MODULE_COREOFFICE))
                //{
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/Journal.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/JournalEntry.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/Role.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/Rwa.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/CaseStatus.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/CasePriority.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/InOfficeDocumentStatus.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/InOfficeDocumentKind.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/InOfficeDocumentDelivery.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/OutOfficeDocumentDelivery.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/OutOfficeDocumentReturnReason.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/ContractManagment.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/AssignmentHistoryEntry.hbm.xml"));

	            sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/MailWeightKind.hbm.xml"));
                
                if(AvailabilityManager.isAvailable("available.hibernate.pte"))
                {
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/PTE_Person.hbm.xml"));
                }
                else
                {
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/Person.hbm.xml"));
                }
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/School.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/Remark.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/Container.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/Adnotation.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/Gauge.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/CollectiveAssignmentEntry.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/AssignmentObjective.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/internal/DataTypeImpl.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/internal/WfActivityImpl.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/internal/WfAssignmentImpl.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/internal/WfPackageDefinition.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/internal/WfProcessImpl.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/internal/WfResourceImpl.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/internal/ParticipantMapping.hbm.xml"));
                // Document i OfficeDocument
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/OfficeDocument.hbm.xml"));

                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/TaskSnapshot.hbm.xml"));

                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/JBPMTaskSnapshot.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/TaskHistoryEntry.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/ProcessCoordinator.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/ChannelRequestBean.hbm.xml"));

                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/mail/MailMessage.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/mail/MessageAttachment.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/mail/MailMessageReceipt.hbm.xml"));

                //if (Docusafe.moduleAvailable(Modules.MODULE_INVOICES))
                //{
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/invoices/Invoice.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/invoices/Vendor.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/InvoicesKind.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/InvoicesCpvCodes.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/InvoicesDecretation.hbm.xml"));
                //}

                //if (Docusafe.moduleAvailable(Modules.MODULE_CONTRACTS))
                //{
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/contracts/Contract.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/contracts/ContractHistory.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/contracts/Vendor.hbm.xml"));
                //}

                // Rejestr projektow
                if (AvailabilityManager.isAvailable("ifpan.projects"))
                {
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/projects/Project.hbm.xml"));
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/projects/ProjectEntry.hbm.xml"));
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/projects/CostKind.hbm.xml"));
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/projects/FinancingInstitutionKind.hbm.xml"));
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/projects/AccountingKind.hbm.xml"));
                }
                if (AvailabilityManager.isAvailable("pig.compan"))
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/TopicFoundingSource.hbm.xml"));
                else 
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/TopicFoundingSourcePIG.hbm.xml"));
                //if (Docusafe.moduleAvailable(Modules.MODULE_GRANTS))
                //{
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/grants/GrantRequest.hbm.xml"));
                //}

                //if (Docusafe.moduleAvailable(Modules.MODULE_CONSTRUCTION))
                //{
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/construction/ConstructionApplication.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/construction/ConstructionDecision.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/construction/ConstructionDecisionCounter.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/record/construction/DecisionArchive.hbm.xml"));
                //}

                //if (Docusafe.moduleAvailable(Modules.MODULE_WORKFLOW))
                //{
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/jbpm/ProcessDefinition.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/jbpm/TaskDefinition.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/jbpm/ProcessReminder.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/workflow/jbpm/SwimlaneMapping.hbm.xml"));
                //}
                
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/mail/DSEmailChannel.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/naming/StringResource.hbm.xml"));
                //events
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/events/DocusafeEvent.hbm.xml"));
                //anonymous access
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/archive/AnonymousAccessTicket.hbm.xml"));
                //encryption
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/crypto/Crypto.hbm.xml"));

                //IMA
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/ima/dictionary/CaseType.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/ima/dictionary/DocInCaseType.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/ima/dictionary/DocInCaseStatus.hbm.xml"));                
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/ima/dictionary/MediatorNotes.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/ima/dictionary/Questionnaire.hbm.xml"));
                    
                
                //YETICO S�ownik maszyn i cz�ci zamiennych
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/tree/MachineElement.hbm.xml"));
//                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/tree/RodzajMaszyny.hbm.xml"));
//                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/tree/Maszyna.hbm.xml"));
//                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/tree/Podzespol.hbm.xml"));
//                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/tree/Czesc.hbm.xml"));
//                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/tree/CzescZamienna.hbm.xml"));
                  sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/hb/PrewencjaItem.hbm.xml"));
                  sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/hb/NiezgodnoscWewMultiple.hbm.xml"));
                  sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/yetico/hb/ReclamationCosts.hbm.xml"));
                //PLAY 
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/p4/ReportTask.hbm.xml"));
                
                //ILPOL
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/users/sql/UserAdditionalData.hbm.xml"));
                
                //ZBP
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/zbp/hb/MPK.hbm.xml"));

                //PG
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/pg/BarcodePool.hbm.xml"));

                // AMS
                //sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/MPK.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/FundingSource.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/FinancialTask.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/Warehouse.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/PurchaseDocumentType.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/ProductionOrder.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/BudgetView.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/ContractType.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/AMSContract.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/SupplierBankAccount.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/ContractBogdet.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/AMSVatRate.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/RoleInProject.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/SupplierOrderHeader.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/SupplierOrderPosition.hbm.xml"));
                //sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/ams/hb/AmsAssetCardInfo.hbm.xml"));

                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/imports/simple/repository/SimpleRepositoryAttribute.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/imports/simple/repository/Dictionary.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/imports/simple/repository/DictionaryMap.hbm.xml"));
                
                //UEK
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/uek/hbm/DeliveryCostUek.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/uek/hbm/DelegationCost.hbm.xml"));
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/uek/hbm/CurrencyDeposit.hbm.xml"));
                
				//obs�uga wielu drukarek
				sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/service/zebra/ZebraPrinter.hbm.xml"));
                
                if(AvailabilityManager.isAvailable("menu.left.user.to.briefcase"))
                sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/UserToBriefcase.hbm.xml"));
                
                if(AvailabilityManager.isAvailable("menu.left.user.to.jasper.reports"))
                    sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/OfficePoint.hbm.xml"));
                if(AvailabilityManager.isAvailable("menu.left.office.points"))
                    sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/UserToJasperReports.hbm.xml"));
                
                //sprawa do sprawy 
                if(AvailabilityManager.isAvailable("CaseToCase"))
                    sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/office/CaseToCase.hbm.xml"));
            
                if(AvailabilityManager.isAvailable("portletStartPage")) {
                    sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/portlet/Portlet.hbm.xml"));
                    sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/portlet/PortletConfig.hbm.xml"));
                }
                
                //Xes Log API
                    sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/XesLog.hbm.xml"));
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/XesLogData.hbm.xml"));
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/core/base/XesLoggedActionsTypes.hbm.xml"));
                	
                	// pG zadanie odbiorca 
  					if(AvailabilityManager.isAvailable("pgZadanieOdbiorca"))
                	sf.add(org.dom4j.DocumentHelper.createElement("mapping").addAttribute("resource", "pl/compan/docusafe/parametrization/pg/ZadanieOdbiorca.hbm.xml"));

                if (log.isDebugEnabled())
                log.debug("hibernate.cfg.xml="+document4j.asXML());

            return new org.dom4j.io.DOMWriter().write(document4j);
        }
        catch (org.dom4j.DocumentException e)
        {
            throw new EdmException("B��d odczytu konfiguracji Hibernate", e);
        }
    }

}
