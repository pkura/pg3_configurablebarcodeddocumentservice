package pl.compan.docusafe.core;

import com.opensymphony.webwork.ServletActionContext;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class DSContextOpener {

    /** Czy kontekst trzeba zamkn�� */
    private boolean toClose;

    private DSContextOpener(){
        this.toClose = false;
    }

    /** Je�eli kontekst jest zamkni�ty, zostanie otwarty jako u�ytkownik poprzez (DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()))).
     * Uzytkownik pobierany z ServletActionContext
     * @return obiekt zamykaj�cy kontekst, gdy to jest potrzebne poprzez metod� close()
     */
    public static DSContextOpener openAsUserIfNeed() throws EdmException {
        return openAsUserIfNeed(SubjectSource.ServletActionContext);
    }
    /** Je�eli kontekst jest zamkni�ty, zostanie otwarty jako u�ytkownik poprzez (DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()))).
     * @return obiekt zamykaj�cy kontekst, gdy to jest potrzebne poprzez metod� close()
     */
    public static DSContextOpener openAsUserIfNeed(SubjectSource subjectSource) throws EdmException {
        DSContextOpener dsContextOpener = new DSContextOpener();
        dsContextOpener.reopenAsUserIfNeed(subjectSource);
        return dsContextOpener;
    }

    public enum SubjectSource {
        HttpServlet,
        ServletActionContext
    }

    /** Je�eli kontekst jest zamkni�ty, zostanie otwarty jako u�ytkownik poprzez (DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()))).*/
    public void reopenAsUserIfNeed(SubjectSource subjectSource) throws EdmException {
        HttpServletRequest req = subjectSource==SubjectSource.HttpServlet ? WebContextFactory.get().getHttpServletRequest() : ServletActionContext.getRequest();
        if (!DSApi.isContextOpen()){
            DSApi.open(AuthUtil.getSubject(req));
            toClose = true;
        }
    }


    /** Je�eli kontekst jest zamkni�ty, zostanie otwarty jako admin.
     * @return obiekt zamykaj�cy kontekst, gdy to jest potrzebne poprzez metod� close()
     */
    public static DSContextOpener openAsAdminIfNeed() throws EdmException {
        DSContextOpener dsContextOpener = new DSContextOpener();
        dsContextOpener.reopenAsAdminIfNeed();
        return dsContextOpener;
    }

    /** Je�eli kontekst jest zamkni�ty, zostanie otwarty jako admin.*/
    public void reopenAsAdminIfNeed() throws EdmException {
        if (!DSApi.isContextOpen()){
            DSApi.openAdmin();
            toClose = true;
        }
    }


    /** Zamkni�cie kontekstu, je�eli zosta� otwarty poprzez re/openAsUserIfNeed(). */
    public void closeIfNeed(){
        if(toClose && DSApi.isContextOpen()){
            DSApi._close();
            toClose = false;
        }
    }

    /** Zamkni�cie kontekstu, je�eli zosta� otwarty poprzez re/openAsUserIfNeed() i przekazywany obiekt nie jest null-em.*/
    public static void closeIfNeed(DSContextOpener contextOpener) {
        if (contextOpener != null)
            contextOpener.closeIfNeed();
    }
}
