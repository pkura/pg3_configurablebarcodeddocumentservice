package pl.compan.docusafe.core;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ValueNotFoundException.java,v 1.1 2004/06/05 21:10:22 administrator Exp $
 */
public class ValueNotFoundException extends EdmException
{
    public ValueNotFoundException(String message)
    {
        super(message);
    }

    public ValueNotFoundException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ValueNotFoundException(Throwable cause)
    {
        super(cause);
    }
}
