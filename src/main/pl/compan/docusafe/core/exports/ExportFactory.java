package pl.compan.docusafe.core.exports;

public interface ExportFactory {
	XmlCreator getXmlCreator();
	Exporter getExporter();
	Checker getChecker();
}
