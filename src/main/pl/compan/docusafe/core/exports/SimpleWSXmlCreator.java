package pl.compan.docusafe.core.exports;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.SAXOMBuilder;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class SimpleWSXmlCreator extends XmlCreator {

    private static final Logger log = LoggerFactory.getLogger(SimpleWSXmlCreator.class);

    @Override
    public File getFile() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setFile(File file) {
        // TODO Auto-generated method stub
    }

    @Override
    public void buildXml(ExportedDocument doc) throws EdmException {
        if (doc instanceof JaxbDocument) {
            try {
                JaxbDocument jaxDoc = (JaxbDocument) doc;
                JAXBContext jaxbContext = JAXBContext.newInstance("pl.compan.docusafe.core.exports.simple.xml");
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

                SAXOMBuilder builder = new SAXOMBuilder();
                jaxbMarshaller.marshal(jaxDoc.create(), builder);

                this.setResult(builder.getRootElement());

                log.error(getResult().toString());
            } catch (JAXBException e) {
                log.error(e.getMessage(), e);
                new EdmException(e);
            }
        } else {
            buildHeaders(doc);
            if (doc instanceof PurchasingDocument) {
                this.setResult(buildPurchasingDocument((PurchasingDocument) doc));
            }
            if (doc instanceof OrderDocument) {
                this.setResult(buildOrderDocument((OrderDocument) doc));
            }
            if (doc instanceof DeliveryOrderDocument) {
                this.setResult(buildDeliveryOrderDocument((DeliveryOrderDocument) doc));
            }
            if (doc instanceof ReservationDocument) {
                this.setResult(buildReservationDocument((ReservationDocument) doc));
            }
            if (doc instanceof FixedAssetLiquidationDocument) {
                this.setResult(buildFixedAssetLiquidationDocument((FixedAssetLiquidationDocument) doc));
            }
        }

    }

    private OMElement buildFixedAssetLiquidationDocument(FixedAssetLiquidationDocument doc) throws EdmException {
        OMElement result = this.getResult();

        try {

            PreElement base = new PreElement();

            base.addElement(PreElement.newNormalIdentifiedElement("typ_ope_id", TypeAttribute.idn, null, doc.getOperationTypeCode(), true));
            base.addElement(new PreElement("uzytk_id", Type.T, getExportUser()));
            base.addElement(PreElement.newNormalIdentifiedElement("oddzial_id", TypeAttribute.idn, null, doc.getDepartmentCode(), false));
            base.addElement(PreElement.newSimpleValuedElement("data_ope",  DateUtils.formatSqlDate(Objects.firstNonNull(doc.getOperation(), new Date()))));
            base.addElement(PreElement.newSimpleValuedElement("uwagi", doc.getDescription()));
            base.addElement(PreElement.newSimpleValuedElement("data_obowiazywania",  DateUtils.formatSqlDate(Objects.firstNonNull(doc.getValidity(), new Date()))));

            if (!doc.getPositions().isEmpty()) {
                PreElement baseCostElem = new PreElement("mtf_opepozycje");
                base.addElement(baseCostElem);

                for (FixedAssetPosition item : doc.getPositions()) {
                    PreElement positionElem = new PreElement("mtf_opepoz");
                    baseCostElem.addElement(positionElem);

                    PreElement cardElem;
                    positionElem.addElement(cardElem = new PreElement("karta_id"));
                    cardElem.addElement(PreElement.newSimpleValuedElement("karta_id", item.getCardIdentifier()));

                    positionElem.addElement(PreElement.newNormalIdentifiedElement("likwidacja_id", TypeAttribute.idn, null, item.getLiquidationCode(), false));


                    if (!item.getPositions().isEmpty()) {
                        PreElement registrationElements;
                        positionElem.addElement(registrationElements = new PreElement("mtf_opepozycje_det"));
                        PreElement registrationElem;
                        for (FixedAssetPositionDetail fixedAssetPositionDetail : item.getPositions()) {
                            registrationElements.addElement(registrationElem = new PreElement("mtf_opepoz_det"));
                            registrationElem.addElement(PreElement.newNormalIdentifiedElement("typ_sysewi_id", TypeAttribute.idn, null, fixedAssetPositionDetail.getRegistrationSystemCode(), true));
                            registrationElem.addElement(PreElement.newSimpleValuedElement("kurs", fixedAssetPositionDetail.getRatio()));
                        }
                    }
                }
            }

            if (!doc.getAttachementInfo().isEmpty()) {
                PreElement baseAttach = new PreElement("sys_dok_zalaczniki");

                for (AttachmentInfo info : doc.getAttachementInfo()) {
                    PreElement attach = new PreElement("sys_dok_zalacznik");
                    attach.addElement(new PreElement("nazwa", info.getName()));
                    attach.addElement(new PreElement("sciezka", info.getUrl()));
                    attach.addElement(new PreElement("type", "URL"));

                    baseAttach.addElement(attach);
                }

                base.addElement(baseAttach);
            }


            result = batchPreElement(result, base);
            log.error(result.toString());
        } catch (Exception exc) {
            log.debug(exc.getMessage(), exc);
            throw new EdmException(exc);
        }

        return result;
    }

    private void buildHeaders(ExportedDocument doc) {
        String rootElementName = "dokzak";
        String xsdFileName = "Dokzak.xsd";
        if (doc instanceof ReservationDocument) {
            rootElementName = ((ReservationDocument) doc).getReservationState().getRootElementName();
            xsdFileName = ((ReservationDocument) doc).getReservationState().getXsdFileName();
        }
        if (doc instanceof DeliveryOrderDocument) {
            rootElementName = "zamdost";
            xsdFileName = "zamdost.xsd";
        }
        if (doc instanceof OrderDocument) {
            rootElementName = "zapdost";
            xsdFileName = "zapdost.xsd";
        }
        if (doc instanceof PurchasingDocument) {
            rootElementName = "dokzak";
            xsdFileName = "Dokzak.xsd";
        }
        if (doc instanceof FixedAssetLiquidationDocument) {
            rootElementName = "mtfope";
            xsdFileName = "mtfope.xsd";
        }

        OMFactory fac = getXmlFactory();
        fac = OMAbstractFactory.getOMFactory();
        OMNamespace nsS = fac.createOMNamespace("http://www.simple.com.pl/SimpleErp", "s");
        OMNamespace nsXSI = fac.createOMNamespace("http://www.w3.org/2001/XMLSchema-instance", "xsi");
        OMElement result = fac.createOMElement(rootElementName, nsS);
        result.addAttribute(fac.createOMAttribute("schemaLocation", nsXSI, "http://www.simple.com.pl/SimpleErp " + xsdFileName));

        this.setResult(result);
    }

    protected OMElement buildPurchasingDocument(PurchasingDocument doc) throws EdmException {
        OMElement result = this.getResult();

        try {
            DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd+00:00");

            PreElement base = new PreElement();

            base.addElement(new PreElement("dokzak_id", Type.T, TypeAttribute.idm, doc.getNumber()));
            base.addElement(new PreElement("firma_idn", doc.getType()));
            base.addElement(new PreElement("warplat_id", Type.T, doc.getPaymentMethod()));
            base.addElement(new PreElement("podokrozrejvat_id", doc.getSettlementTaxPeriodId()));
            base.addElement(new PreElement("stdost_id", Type.T, doc.getDelivery()));
            base.addElement(new PreElement("podokrrozl_id", Type.T, doc.getSettlementPeriodId()));
            base.addElement(new PreElement("tabkurs_id", Type.T, doc.getCurrencyRateCode()));
            base.addElement(new PreElement("pracownik_id", Type.T, doc.getCreatingUser()));
            base.addElement(new PreElement("uzytk_id", Type.T, getExportUser()));
            base.addElement(new PreElement("waluta_id", Type.T, "symbolwaluty", doc.getCurrencyCode()));
            base.addElement(new PreElement("typdokzak_id", Type.T, doc.getPurchaseType()));

            if (doc.getContractor() != null) {
                Person contractor = Person.find(doc.getContractor().longValue());
                if (contractor.getWparam() != null) {
                    base.addElement(new PreElement("dostawca_id", Type.N, contractor.getWparam()));
                } else {
                    PreElement contractorElement = new PreElement("dostfakt_id");

                    contractorElement.addElement(new PreElement("nip", contractor.getNip()));
                    contractorElement.addElement(new PreElement("nazwa", contractor.getOrganization()));
                    contractorElement.addElement(new PreElement("kodpoczt", contractor.getZip()));
                    contractorElement.addElement(new PreElement("miasto", contractor.getLocation()));
                    contractorElement.addElement(new PreElement("ulica", contractor.getStreet()));
                    contractorElement.addElement(new PreElement("nrdomu", ""));
                    contractorElement.addElement(new PreElement("nrmieszk", ""));
                    contractorElement.addElement(new PreElement("kraj_id", Type.T, TypeAttribute.idn, "PL"));

                    base.addElement(contractorElement);
                }
            } else if (doc.getExternalContractor() != null) {
                base.addElement(new PreElement("dostawca_id", Type.N, doc.getExternalContractor()));
            }

            base.addElement(new PreElement("kontobankowe_id", Type.N, doc.getContractorAccountId()));
            if (StringUtils.isNotBlank(doc.getContractorAccount())) {
            	base.addElement(new PreElement("kontobankowe_id", Type.T, TypeAttribute.idm, doc.getContractorAccount()));
            }

            base.addElement(new PreElement("datdok", timeFormat.format(doc.getIssued())));
            base.addElement(new PreElement("datoper", timeFormat.format(doc.getOperationOrDefault())));
            base.addElement(new PreElement("kurs", doc.getExchangeRate()));

            base.addElement(new PreElement("dok_uwagi", createRemarks(doc)));
            base.addElement(new PreElement("dokobcy", doc.getCode()));
            base.addElement(new PreElement("datprzyj", timeFormat.format(doc.getRecieved())));

            if (!doc.getBudgetItems().isEmpty()) {
                PreElement baseCostElem = new PreElement("dokzakpozycje");

                for (BudgetItem item : doc.getBudgetItems()) {
                    PreElement costElem = new PreElement("dokzakpoz");

                    costElem.addElement(PreElement.newNormalIdentifiedElement("bd_rezerwacja_id", TypeAttribute.idm, item.getReservationId(), item.getReservationCode(), false));
                    costElem.addElement(PreElement.newSimpleValuedElement("bd_rezerwacja_poz_id", item.getReservationPositionId()));

                    costElem.addElement(new PreElement("vatstaw_id", Type.T, TypeAttribute.ids, item.getVatRateCode()));
                    costElem.addElement(new PreElement("przeznzak", item.getTaxType() != null ? item.getTaxType().getSimpleErpId() : null));
                    costElem.addElement(new PreElement("wskaznik_id", item.getTaxRatio()));
                    costElem.addElement(new PreElement("jm_id", Type.T, item.getUnit()));
                    costElem.addElement(new PreElement("ilosc", item.getQuantity()));
                    costElem.addElement(new PreElement("nrpoz", item.getItemNo()));
                    costElem.addElement(new PreElement("nazwa", item.getName()));
                    costElem.addElement(new PreElement("rodzwytw", "0"));
                    costElem.addElement(new PreElement("rodztowaru", item.isFixedAsset() ? "1" : "0"));
                    costElem.addElement(new PreElement("zasob_id", item.getResourceId()));
                    costElem.addElement(new PreElement("zlecprod_id", item.getOrderId(), item.getOrderCode()));
                    costElem.addElement(new PreElement("wytwor_id", true, item.getCostKindId(), item.getCostKindCode()));
                    costElem.addElement(new PreElement("kwotnett", item.getNetAmount()));
                    costElem.addElement(new PreElement("kwotvat", item.getVatAmount()));
                    costElem.addElement(new PreElement("kontrakt_id", true, item.getProjectId(), item.getProjectCode()));
                    costElem.addElement(new PreElement("projekt_id", true, item.getBudgetId(), item.getBudgetCode()));
                    costElem.addElement(new PreElement("etap_id", item.getPositionId()));
                    costElem.addElement(PreElement.newNormalIdentifiedElement("komorka_id", TypeAttribute.idn, item.getMpkId(), item.getMpkCode(), false));

                    if (item.getPlanZamId() != null || item.getRodzajPlanZamId() != null) {
                        costElem.addElement(new PreElement("bd_plan_zam_id", Type.N, item.getPlanZamId()));
                        costElem.addElement(new PreElement("bd_rodzaj_plan_zam_id", item.getRodzajPlanZamId()));
                    } else {
                        costElem.addElement(new PreElement("bd_budzet_ko_id", Type.N, item.getBudgetKoId()));
                        costElem.addElement(new PreElement("bd_rodzaj_ko_id", item.getBudgetKindId()));
                    }
                    costElem.addElement(new PreElement("budzet_id", true, item.getBudgetDirectId(), item.getBudgetDirectCode()));
                    costElem.addElement(new PreElement("przeznzak", item.getSprzedazOpodatkowana()));

                    if (!item.getFundSources().isEmpty()) {
                        PreElement fundSourcesBase = new PreElement("dokzakpoz_zfi");

                        for (FundSource fundItem : item.getFundSources()) {
                            PreElement fundSources = new PreElement("dokzakpoz_zf");
                            fundSources.addElement(new PreElement("procent", fundItem.getFundPercent()));
                            fundSources.addElement(new PreElement("zrodlo_id", fundItem.getId(),fundItem.getCode()));//Type.T, fundItem.getCode()));
                            fundSources.addElement(new PreElement("rodzaj", (fundItem.getKind()!= null ? fundItem.getKind(): "2")));
                            fundSourcesBase.addElement(fundSources);
                        }

                        costElem.addElement(fundSourcesBase);
                    }

                    if (item.getBudgetTaskCode() != null) {
                        PreElement taskBase = new PreElement("dokzakpoz_zdi");
                        costElem.addElement(taskBase);
                        PreElement taskBaseItem = new PreElement("dokzakpoz_zd");
                        taskBase.addElement(taskBaseItem);

                        taskBaseItem.addElement(new PreElement("bd_zadanie_id", Type.T, TypeAttribute.idn, item.getBudgetTaskCode()));
                    }


                    if (!item.getFinanancialTasks().isEmpty()) {
                        PreElement itemBase = new PreElement("dokzakpoz_zdi");

                        for (FinancialTask rItem : item.getFinanancialTasks()) {
                            PreElement attribute = new PreElement("dokzakpoz_zd");
                            attribute.addElement(PreElement.newNormalIdentifiedElement("bd_zadanie_id", TypeAttribute.idn, rItem.getTaskId(), rItem.getTaskCode(), false));
                            attribute.addElement(new PreElement("kwota", rItem.getAmount()));
                            attribute.addElement(new PreElement("procent", rItem.getPercent()));
                            itemBase.addElement(attribute);
                        }

                        costElem.addElement(itemBase);
                    }

                    if (!item.getRepositoryItems().isEmpty()) {
                        PreElement repositoryItemsBase = new PreElement("rep_wartosci");

                        for (RepositoryItem rItem : item.getRepositoryItems()) {
                            PreElement attribute = new PreElement("rep_wartosc");
                            attribute.addElement(new PreElement("rep_atrybut_id", rItem.getAttribute()));
                            attribute.addElement(new PreElement("wartosc_instancja_id", rItem.getValue()));
                            repositoryItemsBase.addElement(attribute);
                        }

                        costElem.addElement(repositoryItemsBase);
                    }

                    if (item.getTemplateId() != null) {
                    	PreElement templateItemBase = new PreElement("szablon_id");
                    	templateItemBase.addElement(new PreElement("szablon_id", item.getTemplateId()));
                    	costElem.addElement(templateItemBase);
                    }
                    
                    baseCostElem.addElement(costElem);
                }

                base.addElement(baseCostElem);
            }

            if (!doc.getRepositoryItems().isEmpty()) {
                PreElement repositoryItemsBase = new PreElement("rep_wartosci");

                for (RepositoryItem rItem : doc.getRepositoryItems()) {
                    PreElement attribute = new PreElement("rep_wartosc");
                    attribute.addElement(new PreElement("rep_atrybut_id", rItem.getAttribute()));
                    attribute.addElement(new PreElement("wartosc_instancja_id", rItem.getValue()));
                    repositoryItemsBase.addElement(attribute);
                }

                base.addElement(repositoryItemsBase);
            }

            if (doc.getExpired() != null && doc.getInstallmentItems().isEmpty()) {
                PreElement repositoryItemsBase = new PreElement("warplatdokzaki");

                PreElement attribute = new PreElement("warplatdokzak");
                attribute.addElement(new PreElement("sposzapl", Objects.firstNonNull(doc.getPaymentMethodId(), 0)));
                attribute.addElement(new PreElement("czyproc", 1));
                attribute.addElement(new PreElement("prockwot", 100));
                attribute.addElement(new PreElement("typplat", 0));
                attribute.addElement(new PreElement("terplat", timeFormat.format(doc.getExpired())));
                repositoryItemsBase.addElement(attribute);

                base.addElement(repositoryItemsBase);
            }

            if (!doc.getInstallmentItems().isEmpty()) {
                PreElement baseWarPlatElement = new PreElement("warplatdokzaki");

                for (InstallmentItem item : doc.getInstallmentItems()) {
                    PreElement attribute = new PreElement("warplatdokzak");
                    attribute.addElement(new PreElement("sposzapl", item.getSposobZaplaty()));
                    attribute.addElement(new PreElement("czyproc", Boolean.TRUE.equals(item.getCzyProcent()) ? 1 : 0));
                    attribute.addElement(new PreElement("kwota", item.getKwota()));
                    attribute.addElement(new PreElement("prockwot", item.getProcentKwoty()));
                    attribute.addElement(new PreElement("typplat", Objects.firstNonNull(item.getTypPlatnosci(), 0)));
                    attribute.addElement(new PreElement("terplat", timeFormat.format(item.getTerminPlatnosci())));
                    baseWarPlatElement.addElement(attribute);
                }
                base.addElement(baseWarPlatElement);
            }

            if (!doc.getAttachementInfo().isEmpty()) {
                PreElement baseAttach = new PreElement("sys_dok_zalaczniki");

                for (AttachmentInfo info : doc.getAttachementInfo()) {
                    PreElement attach = new PreElement("sys_dok_zalacznik");
                    attach.addElement(new PreElement("nazwa", info.getName()));
                    attach.addElement(new PreElement("sciezka", info.getUrl()));
                    attach.addElement(new PreElement("type", "URL"));

                    baseAttach.addElement(attach);
                }

                base.addElement(baseAttach);
            }

            result = batchPreElement(result, base);
            log.error(result.toString());
        } catch (Exception exc) {
            log.debug(exc.getMessage(), exc);
            throw new EdmException(exc);
        }

        return result;
    }

    protected OMElement buildOrderDocument(OrderDocument doc) throws EdmException {
        OMElement result = this.getResult();

        try {
            DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd+00:00");

            PreElement base = new PreElement();

            base.addElement(new PreElement("pracownik_id", Type.T, TypeAttribute.idn, doc.getCreatingUser()));

            base.addElement(new PreElement("komzapotrz_id", Type.T, TypeAttribute.idn, doc.getMpkCode()));
            base.addElement(new PreElement("uzytk_id", Type.T, getExportUser()));
            base.addElement(new PreElement("typ_zapdost_id", Type.T, doc.getPurchaseType()));

            if (doc.getContractor() != null) {
                Person contractor = Person.find(doc.getContractor().longValue());
                if (contractor.getWparam() != null) {
                    base.addElement(new PreElement("zapdost_id", Type.N, contractor.getWparam()));
                }
            }

            if (doc.getCreated() != null) {
                base.addElement(new PreElement("datdok", timeFormat.format(doc.getCreated())));
            }
            base.addElement(new PreElement("datoper", timeFormat.format(new Date())));

            base.addElement(new PreElement("uwagi", createRemarks(doc)));

            if (!doc.getBudgetItems().isEmpty()) {
                PreElement baseCostElem = new PreElement("zapdostpozycje");

                for (BudgetItem item : doc.getBudgetItems()) {
                    PreElement costElem = new PreElement("zapdostpoz");
                    //costElem.addElement(new PreElement("kategobr_id", Type.T, "Podstawowa"));
                    costElem.addElement(new PreElement("vatstaw_id", Type.T, TypeAttribute.ids, item.getVatRateCodeOrDefault()));
                    costElem.addElement(new PreElement("jm_id", Type.T, item.getUnit()));
                    costElem.addElement(new PreElement("zasob_id", item.getResourceId()));
                    costElem.addElement(new PreElement("wytwor_id", true, item.getCostKindId(), item.getCostKindCode(), true));
                    costElem.addElement(new PreElement("ilosc", item.getQuantity()));
                    costElem.addElement(new PreElement("cena", item.getNetAmount()));
                    costElem.addElement(new PreElement("nrpoz", item.getItemNo()));
                    costElem.addElement(new PreElement("uwagi", item.getName()));
                    costElem.addElement(new PreElement("kontrakt_id", true, item.getProjectId(), item.getProjectCode()));
                    costElem.addElement(new PreElement("projekt_id", true, item.getBudgetId(), item.getBudgetCode()));
                    costElem.addElement(new PreElement("etap_id", item.getPositionId()));
                    costElem.addElement(PreElement.newNormalIdentifiedElement("komorka_id", TypeAttribute.idn, item.getMpkId(), item.getMpkCode(), false));
                    costElem.addElement(new PreElement("zlecprod_id", item.getOrderId(), item.getOrderCode()));

                    if (!item.getRepositoryItems().isEmpty()) {
                        PreElement repositoryItemsBase = new PreElement("rep_wartosci");

                        for (RepositoryItem rItem : item.getRepositoryItems()) {
                            PreElement attribute = new PreElement("rep_wartosc");
                            attribute.addElement(new PreElement("rep_atrybut_id", rItem.getAttribute()));
                            attribute.addElement(new PreElement("wartosc_instancja_id", rItem.getValue()));
                            repositoryItemsBase.addElement(attribute);
                        }

                        costElem.addElement(repositoryItemsBase);
                    }


                    baseCostElem.addElement(costElem);
                }

                base.addElement(baseCostElem);
            }

            if (!doc.getAttachementInfo().isEmpty()) {
                PreElement baseAttach = new PreElement("sys_dok_zalaczniki");

                for (AttachmentInfo info : doc.getAttachementInfo()) {
                    PreElement attach = new PreElement("sys_dok_zalacznik");
                    attach.addElement(new PreElement("nazwa", info.getName()));
                    attach.addElement(new PreElement("sciezka", info.getUrl()));
                    attach.addElement(new PreElement("type", "URL"));

                    baseAttach.addElement(attach);
                }

                base.addElement(baseAttach);
            }

            result = batchPreElement(result, base);
            log.error(result.toString());
        } catch (Exception exc) {
            log.debug(exc.getMessage(), exc);
            throw new EdmException(exc);
        }

        return result;
    }

    protected OMElement buildDeliveryOrderDocument(DeliveryOrderDocument doc) throws EdmException {
        OMElement result = this.getResult();

        try {
            DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd+00:00");

            PreElement base = new PreElement();

            base.addElement(new PreElement("pracownik_id", true, doc.getCreatingUser(), null, true));

            base.addElement(new PreElement("typzamdos_id", Type.T, doc.getDocumentTypeCode()));
            base.addElement(new PreElement("dostawca_id", true, doc.getExternalContractor(), null, true));
            base.addElement(new PreElement("komzamaw_id", true, doc.getMpkId(), doc.getMpkCode(), true));
            base.addElement(new PreElement("oddzial_id", true, doc.getDepartmentCode(), null, true));
            base.addElement(new PreElement("status", doc.getStatus()));


            if (doc.getCreated() != null) {
                base.addElement(new PreElement("datdok", timeFormat.format(doc.getCreated())));
            }

            base.addElement(new PreElement("datodb", timeFormat.format(doc.getExecution())));

            base.addElement(new PreElement("budzet_id", true, doc.getHeaderBudget().getBudgetDirectId(), doc.getHeaderBudget().getBudgetDirectCode()));

            base.addElement(new PreElement("uwagi", createRemarks(doc)));

            if (!doc.getBudgetItems().isEmpty()) {
                PreElement baseCostElem = new PreElement("zamdostpozycje");

                for (BudgetItem item : doc.getBudgetItems()) {
                    PreElement costElem = new PreElement("zamdostpoz");
                    costElem.addElement(new PreElement("vatstaw_id", Type.T, TypeAttribute.ids, item.getVatRateCodeOrDefault()));
                    costElem.addElement(new PreElement("jm_id", Type.T, item.getUnit()));
                    costElem.addElement(new PreElement("ilosc", item.getQuantity()));
                    costElem.addElement(new PreElement("nrpoz", item.getItemNo()));
                    costElem.addElement(new PreElement("uwagi", item.getName()));
                    costElem.addElement(new PreElement("zasob_id", item.getResourceId()));
                    costElem.addElement(new PreElement("zlecprod_id", item.getOrderId(), item.getOrderCode()));
                    costElem.addElement(new PreElement("wytwor_id", true, item.getCostKindId(), item.getCostKindCode()));
                    costElem.addElement(new PreElement("cena", item.getNetAmount()));
                    costElem.addElement(new PreElement("kontrakt_id", true, item.getProjectId(), item.getProjectCode()));
                    costElem.addElement(new PreElement("projekt_id", true, item.getBudgetId(), item.getBudgetCode()));
                    costElem.addElement(new PreElement("etap_id", item.getPositionId()));
                    costElem.addElement(new PreElement("komorka_id", item.getMpkId(), item.getMpkCode()));
                    if (item.getPlanZamId() != null || item.getRodzajPlanZamId() != null) {
                        costElem.addElement(new PreElement("bd_plan_zam_id", Type.N, item.getPlanZamId()));
                        costElem.addElement(new PreElement("bd_rodzaj_plan_zam_id", item.getRodzajPlanZamId()));
                    } else {
                        costElem.addElement(new PreElement("bd_budzet_ko_id", Type.N, item.getBudgetKoId()));
                        costElem.addElement(new PreElement("bd_rodzaj_ko_id", item.getBudgetKindId()));
                    }
                    costElem.addElement(new PreElement("budzet_id", true, item.getBudgetDirectId(), item.getBudgetDirectCode()));

                    if (!item.getRepositoryItems().isEmpty()) {
                        PreElement repositoryItemsBase = new PreElement("rep_wartosci");

                        for (RepositoryItem rItem : item.getRepositoryItems()) {
                            PreElement attribute = new PreElement("rep_wartosc");
                            attribute.addElement(new PreElement("rep_atrybut_id", rItem.getAttribute()));
                            attribute.addElement(new PreElement("wartosc_instancja_id", rItem.getValue()));
                            repositoryItemsBase.addElement(attribute);
                        }

                        costElem.addElement(repositoryItemsBase);
                    }


                    baseCostElem.addElement(costElem);
                }

                base.addElement(baseCostElem);
            }

            if (!doc.getAttachementInfo().isEmpty()) {
                PreElement baseAttach = new PreElement("sys_dok_zalaczniki");

                for (AttachmentInfo info : doc.getAttachementInfo()) {
                    PreElement attach = new PreElement("sys_dok_zalacznik");
                    attach.addElement(new PreElement("nazwa", info.getName()));
                    attach.addElement(new PreElement("sciezka", info.getUrl()));
                    attach.addElement(new PreElement("type", "URL"));

                    baseAttach.addElement(attach);
                }

                base.addElement(baseAttach);
            }

            result = batchPreElement(result, base);
            log.error(result.toString());
        } catch (Exception exc) {
            log.debug(exc.getMessage(), exc);
            throw new EdmException(exc);
        }

        return result;
    }

    protected OMElement buildReservationDocument(ReservationDocument doc) throws EdmException {
        OMElement result = this.getResult();

        try {

            PreElement base = new PreElement();

            base.addElement(new PreElement("uzytk_id", Type.T, getExportUser()));

            switch (doc.getReservationState()) {
                case WORKING:
                    refillWorkingReservationDocument(doc, base);
                    break;
                case ACCEPTED:
                case APPROVED:
                case REJECTED:
                    base.addElement(new PreElement("identyfikator", Type.T, TypeAttribute.idm, doc.getReservationNumber()));
                    break;
            }

            result = batchPreElement(result, base);
            log.error(result.toString());
        } catch (Exception exc) {
            log.debug(exc.getMessage(), exc);
            throw new EdmException(exc);
        }

        return result;
    }

    private void refillWorkingReservationDocument(ReservationDocument doc, PreElement base) {
        base.addElement(new PreElement("pracownik_id", false, doc.getCreatingUser(), null, false));

        base.addElement(new PreElement("datrez", DateUtils.formatAxisNullableDateTime(doc.getCreated())));
        base.addElement(new PreElement("podp_indeks", doc.isCostKindCodeAllowed() ? "1" : null));

        base.addElement(PreElement.newNormalIdentifiedElement("bd_typ_rezerwacja_id", TypeAttribute.idn, doc.getBudgetReservationTypeId(), doc.getBudgetReservationTypeCode(), true));
        base.addElement(new PreElement("metoda_rezerwacji", doc.getReservationMethod() != null ? doc.getReservationMethod().getId() : null));

        base.addElement(PreElement.newNormalIdentifiedElement("budzet_id", TypeAttribute.idm, doc.getHeaderBudget().getBudgetDirectId(), doc.getHeaderBudget().getBudgetDirectCode(), true));

        base.addElement(PreElement.newNormalIdentifiedElement("bd_budzet_ko_id", TypeAttribute.idm, doc.getHeaderBudget().getBudgetKoId(), doc.getHeaderBudget().getBudgetKoCode(), false));
        base.addElement(new PreElement("bd_plan_zam_id", Type.N, doc.getHeaderBudget().getPlanZamId()));

        base.addElement(new PreElement("adnotacje", createRemarks(doc)));

        if (!doc.getPostionItems().isEmpty()) {
            PreElement baseCostElem = new PreElement("bd_rezerwacja_pozycje");

            for (BudgetItem item : doc.getPostionItems()) {
                PreElement costElem = new PreElement("bd_rezerwacja_poz");

                costElem.addElement(PreElement.newNormalIdentifiedElement("vatstaw_id", TypeAttribute.ids, item.getVatRate(), item.getVatRateCode(), true));
                costElem.addElement(new PreElement("jm_id", Type.AUTO, TypeAttribute.idn, item.getUnit()));
                costElem.addElement(new PreElement("ilosc", item.getQuantity()));
                costElem.addElement(new PreElement("nrpoz", item.getItemNo()));
                costElem.addElement(new PreElement("opis", item.getName()));
                costElem.addElement(new PreElement("wytwor_id", true, item.getCostKindId(), item.getCostKindCode()));
                costElem.addElement(new PreElement("wytwor_edit_nazwa", item.getCostKindName()));
                costElem.addElement(new PreElement("cena", item.getNetAmount()));

                costElem.addElement(PreElement.newNormalIdentifiedElement("budzet_id", TypeAttribute.idm, item.getBudgetDirectId(), item.getBudgetDirectCode(), true));

                costElem.addElement(PreElement.newNormalIdentifiedElement("kontrakt_id", TypeAttribute.idm, item.getProjectId(), item.getProjectCode(), false));

                costElem.addElement(PreElement.newNormalIdentifiedElement("bd_budzet_ko_id", TypeAttribute.idm, item.getBudgetKoId(), item.getBudgetKoCode(), false));
                costElem.addElement(new PreElement("bd_rodzaj_ko_id", item.getBudgetKindId()));
                costElem.addElement(new PreElement("bd_str_budzet_id", Type.T, TypeAttribute.idn, item.getBudgetCellCode()));

                costElem.addElement(new PreElement("bd_plan_zam_id", Type.N, item.getPlanZamId()));
                costElem.addElement(new PreElement("bd_rodzaj_plan_zam_id", item.getRodzajPlanZamId()));

                costElem.addElement(new PreElement("projekt_id", true, item.getBudgetId(), item.getBudgetCode()));
                costElem.addElement(new PreElement("etap_id", item.getPositionId()));
                costElem.addElement(new PreElement("zasob_id", item.getResourceId()));

                costElem.addElement(new PreElement("bd_zadanie_id", Type.T, TypeAttribute.idn, item.getBudgetTaskCode()));


                if (!item.getFinanancialTasks().isEmpty()) {
                    PreElement itemBase = new PreElement("bd_rezerwacja_poz_zdi");

                    for (FinancialTask rItem : item.getFinanancialTasks()) {
                        PreElement attribute = new PreElement("bd_rezerwacja_poz_zd");
                        attribute.addElement(PreElement.newNormalIdentifiedElement("bd_zadanie_id", TypeAttribute.idn, rItem.getTaskId(), rItem.getTaskCode(), false));
                        attribute.addElement(new PreElement("kwota", rItem.getAmount()));
                        attribute.addElement(new PreElement("procent", rItem.getPercent()));
                        itemBase.addElement(attribute);
                    }

                    costElem.addElement(itemBase);
                }

                if (!item.getFundSources().isEmpty()) {
                    PreElement itemBase = new PreElement("bd_rezerwacja_poz_zfi");

                    for (FundSource rItem : item.getFundSources()) {
                        PreElement attribute = new PreElement("bd_rezerwacja_poz_zf");
                        attribute.addElement(PreElement.newNormalIdentifiedElement("zrodlo_id", TypeAttribute.idn, rItem.getId(), rItem.getCode(), false));
                        attribute.addElement(new PreElement("procent", rItem.getFundPercent()));
                        attribute.addElement(new PreElement("kwota", rItem.getAmount()));
                        itemBase.addElement(attribute);
                    }

                    costElem.addElement(itemBase);
                }

                //costElem.addElement(new PreElement("zrodlo_id", Type.T, TypeAttribute.idn, !item.getFundSources().isEmpty() ? item.getFundSources().get(0).getCode() : null));

                if (!item.getRepositoryItems().isEmpty()) {
                    PreElement repositoryItemsBase = new PreElement("rep_wartosci");

                    for (RepositoryItem rItem : item.getRepositoryItems()) {
                        PreElement attribute = new PreElement("rep_wartosc");
                        attribute.addElement(new PreElement("rep_atrybut_id", rItem.getAttribute()));
                        attribute.addElement(new PreElement("wartosc_instancja_id", rItem.getValue()));
                        repositoryItemsBase.addElement(attribute);
                    }

                    costElem.addElement(repositoryItemsBase);
                }


                baseCostElem.addElement(costElem);
            }

            base.addElement(baseCostElem);
        }

        if (!doc.getAttachementInfo().isEmpty()) {
            PreElement baseAttach = new PreElement("sys_dok_zalaczniki");

            for (AttachmentInfo info : doc.getAttachementInfo()) {
                PreElement attach = new PreElement("sys_dok_zalacznik");
                attach.addElement(new PreElement("nazwa", info.getName()));
                attach.addElement(new PreElement("sciezka", info.getUrl()));
                attach.addElement(new PreElement("type", "URL"));

                baseAttach.addElement(attach);
            }

            base.addElement(baseAttach);
        }
    }

    public OMElement batchPreElement(OMElement header, PreElement base) {
        OMElement element = null;
        if (!Strings.isNullOrEmpty(base.getName())) {
            element = getXmlFactory().createOMElement(base.getName(), null);
        }
        if (!base.getElements().isEmpty()) {
            for (PreElement item : base.getElements()) {
                OMElement elem = batchPreElement(null, item);
                if (elem != null) {
                    if (header == null) {
                        element.addChild(elem);
                    } else {
                        header.addChild(elem);
                    }
                }
            }
        } else {
            if (base.getValue() != null || base.getAdditionalValue() != null || base.isCannotNull()) {
                Object value = base.getValue() != null ? base.getValue() : base.getAdditionalValue();

                if (base.getType() == null) {
                    element.addChild(getXmlFactory().createOMText(value.toString()));
                } else {
                    if (base.getType() != Type.AUTO) {
                        element.addAttribute(getXmlFactory().createOMAttribute("type", null, base.getType().name()));
                    } else {
                        if (value == null && base.isCannotNull()) {
                            base.setType(Type.B);
                            element.addAttribute(getXmlFactory().createOMAttribute("type", null, Type.B.name()));
                        } else if (value instanceof Number) {
                            base.setType(Type.N);
                            element.addAttribute(getXmlFactory().createOMAttribute("type", null, Type.N.name()));
                            base.setTypeAttribute(null);
                            base.setForceTypeAttribute(null);
                        } else {
                            base.setType(Type.T);
                            element.addAttribute(getXmlFactory().createOMAttribute("type", null, Type.T.name()));
                        }
                    }

                    if (base.getType() != Type.B) {
                        String type = (base.getTypeAttribute() != null && base.getTypeAttribute() != TypeAttribute.AUTO) ? base.getTypeAttribute().name() : base
                                .getForceTypeAttribute();

                        if (type == null) {
                            if (base.getType() == Type.N) {
                                type = "id";
                            }

                            if (base.getType() == Type.T) {
                                type = "idm";
                                if (!base.isForceIdm()) {
                                    if (value.toString().length() <= 4) {
                                        type = "ids";
                                    } else if (value.toString().length() <= 15) {
                                        type = "idn";
                                    }
                                }
                            }
                        }

                        OMElement childElement = getXmlFactory().createOMElement(type, null);
                        childElement.addChild(getXmlFactory().createOMText(value.toString()));
                        element.addChild(childElement);
                    }
                }
            } else {
                element = null;
            }
        }

        return header != null ? header : element;
    }

    protected String createRemarks(ExportedDocument doc) {
        StringBuilder sb = new StringBuilder();
        sb.append(doc.getDescription());

        if (!Strings.isNullOrEmpty(doc.getAddInfo())) {
          sb.append("Dodatkowe informacje: ")
            .append(doc.getAddInfo())
            .append("; ");
        }

        if (!Strings.isNullOrEmpty(doc.getBarcode())) {
          sb.append("Kod kreskowy: ")
            .append(doc.getBarcode())
            .append("; ");
        }

//        try {
//            byte[] bytes = sb.toString().getBytes("UTF-8");
//            return new String(bytes);
//        } catch (UnsupportedEncodingException e) {
//            return sb.toString();
//        }
        return sb.toString();
    }

    private String getExportUser() {
        String user = Docusafe.getAdditionProperty("erp.faktura.user");
        return Strings.isNullOrEmpty(user) ? "esb" : user;
    }
}
