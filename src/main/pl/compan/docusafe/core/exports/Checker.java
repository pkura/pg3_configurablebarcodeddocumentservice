package pl.compan.docusafe.core.exports;

import pl.compan.docusafe.core.EdmException;

public interface Checker {
	void initChecker() throws EdmException;
	void check(String guid) throws EdmException;
	RequestResult getResult();
	void finalizeChecker();
}
