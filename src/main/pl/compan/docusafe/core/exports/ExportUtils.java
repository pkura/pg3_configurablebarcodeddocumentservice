package pl.compan.docusafe.core.exports;

/**
 * Created by kk on 15.05.14.
 */
public class ExportUtils {

    private ExportUtils() {
    }

    public static void checkState(boolean expression, Object errorMessage) throws BuildExportDocumentException {
        if (!expression) {
            throw new BuildExportDocumentException(String.valueOf(errorMessage));
        }
    }

    public static <T> T checkNotNull(T object, Object nullMessage) throws BuildExportDocumentException {
        if (object == null) {
            throw new BuildExportDocumentException(String.valueOf(nullMessage));
        } else {
            return object;
        }
    }
}
