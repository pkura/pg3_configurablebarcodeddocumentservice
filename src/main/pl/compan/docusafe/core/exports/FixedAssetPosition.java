package pl.compan.docusafe.core.exports;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by kk on 12.02.14.
 */
public class FixedAssetPosition {

    private Long cardIdentifier;
    private String liquidationCode;
    private List<FixedAssetPositionDetail> positions = Lists.newArrayList();

    public Long getCardIdentifier() {
        return cardIdentifier;
    }

    public void setCardIdentifier(Long cardIdentifier) {
        this.cardIdentifier = cardIdentifier;
    }

    public String getLiquidationCode() {
        return liquidationCode;
    }

    public void setLiquidationCode(String liquidationCode) {
        this.liquidationCode = liquidationCode;
    }


    public List<FixedAssetPositionDetail> getPositions() {
        return positions;
    }

    public void setPositions(List<FixedAssetPositionDetail> positions) {
        this.positions = positions;
    }

    public void addPosition(FixedAssetPositionDetail position) {
        this.positions.add(position);
    }
}
