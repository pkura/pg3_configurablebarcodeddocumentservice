package pl.compan.docusafe.core.exports;

import java.io.File;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import pl.compan.docusafe.core.EdmException;

public abstract class XmlCreator {
	private OMElement result;
	private OMFactory xmlFactory;
	

	abstract File getFile();
	abstract void setFile(File file);
	abstract void buildXml(ExportedDocument doc) throws EdmException;
	
	public OMElement getResult() {
		return result;
	}
	public void setResult(OMElement result) {
		this.result = result;
	}
	public OMFactory getXmlFactory() {
		return xmlFactory;
	}
	public void setXmlFactory(OMFactory fac) {
		this.xmlFactory = fac;
	}
}
