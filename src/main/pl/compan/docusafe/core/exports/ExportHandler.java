package pl.compan.docusafe.core.exports;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMFactory;

import pl.compan.docusafe.core.EdmException;

public class ExportHandler {
	
	ExportFactory factory;
	
	public ExportHandler(ExportFactory factory) {
		this.factory=factory;
	}
	
	public String xmlDebug(ExportedDocument doc) throws EdmException {
		XmlCreator creator = factory.getXmlCreator();
		OMFactory omFactory = OMAbstractFactory.getOMFactory();
		creator.setXmlFactory(omFactory);
		creator.buildXml(doc);
		return creator.getResult().getText();
	}
	
	public String xmlExport(ExportedDocument doc) throws EdmException {
		XmlCreator creator = factory.getXmlCreator();
		OMFactory omFactory = OMAbstractFactory.getOMFactory();
		creator.setXmlFactory(omFactory);
		creator.buildXml(doc);
		Exporter exporter = factory.getExporter();
		exporter.initExport(doc);
		exporter.exportXml(creator);
		exporter.finalizeExport();
		return exporter.getResult();
	}
	
	public RequestResult checkExport(String guid) throws EdmException {
		Checker checker = factory.getChecker();
		checker.initChecker();
		checker.check(guid);
		checker.finalizeChecker();
		return checker.getResult();
	}
}
