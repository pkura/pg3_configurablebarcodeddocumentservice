package pl.compan.docusafe.core.exports;

import java.util.Date;
import java.util.List;

import com.google.common.collect.Lists;

public class OrderDocument extends ExportedDocument {

    private String orderName;
    private String orderKind;
    private String orderMode;
    private String orderDescription;
    private String orderSubjectDescription;
	private Date excution;
	private Long contractor;
    private Long mpkId;
    private String mpkCode;
    private String purchaseType;
    private List<BudgetItem> budgetItems = Lists.newArrayList();

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

	public Date getExcution() {
		return excution;
	}

	public void setExcution(Date excution) {
		this.excution = excution;
	}

	public Long getContractor() {
		return contractor;
	}

	public void setContractor(Long contractor) {
		this.contractor = contractor;
	}

    public Long getMpkId() {
        return mpkId;
    }

    public void setMpkId(Long mpkId) {
        this.mpkId = mpkId;
    }

    public String getMpkCode() {
        return mpkCode;
    }

    public void setMpkCode(String mpkCode) {
        this.mpkCode = mpkCode;
    }

	public List<BudgetItem> getBudgetItems() {
		return budgetItems;
	}

	public void setBudgetItems(List<BudgetItem> budgetItems) {
		this.budgetItems = budgetItems;
	}
	
	public void addBudgetItem(BudgetItem budgetItem) {
		this.budgetItems.add(budgetItem);
	}

    public String getOrderKind() {
        return orderKind;
    }

    public void setOrderKind(String orderKind) {
        this.orderKind = orderKind;
    }

    public String getOrderMode() {
        return orderMode;
    }

    public void setOrderMode(String orderMode) {
        this.orderMode = orderMode;
    }

    public String getOrderDescription() {
        return orderDescription;
    }

    public void setOrderDescription(String orderDescription) {
        this.orderDescription = orderDescription;
    }

    public String getOrderSubjectDescription() {
        return orderSubjectDescription;
    }

    public void setOrderSubjectDescription(String orderSubjectDescription) {
        this.orderSubjectDescription = orderSubjectDescription;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }
}
