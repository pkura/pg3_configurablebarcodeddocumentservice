package pl.compan.docusafe.core.exports;

import com.google.common.collect.Lists;

import java.util.Date;
import java.util.List;

/**
 * User: kk
 * Date: 22.01.14
 * Time: 23:26
 */
public class DeliveryOrderDocument extends ExportedDocument {


    private Long externalContractor;
    private String departmentCode;

    private String orderName;
    private String orderKind;
    private String orderMode;
    private String orderDescription;
    private String orderSubjectDescription;
    private Date execution;
    private Long contractor;
    private Long mpkId;
    private String mpkCode;
    private String documentTypeCode;

    private BudgetItem headerBudget = new BudgetItem();
    private List<BudgetItem> budgetItems = Lists.newArrayList();

    public String getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(String documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public Date getExecution() {
        return execution;
    }

    public void setExecution(Date execution) {
        this.execution = execution;
    }

    public Long getContractor() {
        return contractor;
    }

    public void setContractor(Long contractor) {
        this.contractor = contractor;
    }

    public Long getMpkId() {
        return mpkId;
    }

    public void setMpkId(Long mpkId) {
        this.mpkId = mpkId;
    }

    public String getMpkCode() {
        return mpkCode;
    }

    public void setMpkCode(String mpkCode) {
        this.mpkCode = mpkCode;
    }

    public List<BudgetItem> getBudgetItems() {
        return budgetItems;
    }

    public void setBudgetItems(List<BudgetItem> budgetItems) {
        this.budgetItems = budgetItems;
    }

    public void addBudgetItem(BudgetItem budgetItem) {
        this.budgetItems.add(budgetItem);
    }

    public String getOrderKind() {
        return orderKind;
    }

    public void setOrderKind(String orderKind) {
        this.orderKind = orderKind;
    }

    public String getOrderMode() {
        return orderMode;
    }

    public void setOrderMode(String orderMode) {
        this.orderMode = orderMode;
    }

    public String getOrderDescription() {
        return orderDescription;
    }

    public void setOrderDescription(String orderDescription) {
        this.orderDescription = orderDescription;
    }

    public String getOrderSubjectDescription() {
        return orderSubjectDescription;
    }

    public void setOrderSubjectDescription(String orderSubjectDescription) {
        this.orderSubjectDescription = orderSubjectDescription;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Long getExternalContractor() {
        return externalContractor;
    }

    public void setExternalContractor(Long externalContractor) {
        this.externalContractor = externalContractor;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public BudgetItem getHeaderBudget() {
        return headerBudget;
    }
}
