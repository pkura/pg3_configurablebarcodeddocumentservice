package pl.compan.docusafe.core.exports;

import javax.xml.bind.JAXBElement;

/**
 * Created by kk on 27.03.14.
 */
public interface JaxbDocument {

    JAXBElement create();

}
