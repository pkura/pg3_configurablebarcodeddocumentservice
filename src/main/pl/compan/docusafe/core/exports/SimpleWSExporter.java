package pl.compan.docusafe.core.exports;

import org.apache.axiom.om.OMElement;
import pl.compan.docusafe.core.EdmException;
import org.apache.axis2.client.Stub;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.util.axis.AxisClientUtils;
import pl.compan.docusafe.ws.ul.ApplicationForReservationProxyStub;
import pl.compan.docusafe.ws.simple.CostInvoiceCreateProxyStub;
import pl.compan.docusafe.ws.simple.RequestCreateProxyStub;

public class SimpleWSExporter implements Exporter {

	private Stub stub;
	private String result;

	public Stub getStub() throws EdmException {
		return stub;
	}

	public void setStub(Stub stub) {
		this.stub = stub;
	}

	@Override
	public void initExport(ExportedDocument doc) throws EdmException {
        try {
            if (doc instanceof ServiceHandledDocument) {
                AxisClientConfigurator conf = new AxisClientConfigurator();
                ServiceHandledDocument jaxbMapped = (ServiceHandledDocument) doc;
                stub = jaxbMapped.getSenderStub(conf.getAxisConfigurationContext());
                conf.setUpHttpParameters(stub, jaxbMapped.getSenderStubServicesSuffix());
                conf.setHttpClient(AxisClientUtils.createHttpClient(18, 18));
            } else {
                if (doc instanceof PurchasingDocument) {
                    AxisClientConfigurator conf = new AxisClientConfigurator();
                    stub = new CostInvoiceCreateProxyStub(conf.getAxisConfigurationContext());
                    conf.setUpHttpParameters(stub, "/services/CostInvoiceCreateProxy");
                    conf.setHttpClient(AxisClientUtils.createHttpClient(18, 18));
                }
                if (doc instanceof OrderDocument) {
                    AxisClientConfigurator conf = new AxisClientConfigurator();
                    stub = new RequestCreateProxyStub(conf.getAxisConfigurationContext());
                    conf.setUpHttpParameters(stub, "/services/RequestCreateProxy");
                    conf.setHttpClient(AxisClientUtils.createHttpClient(18, 18));
                }
                if (doc instanceof ReservationDocument) {
                    AxisClientConfigurator conf = new AxisClientConfigurator();
                    stub = new ApplicationForReservationProxyStub(conf.getAxisConfigurationContext());
                    conf.setUpHttpParameters(stub, "/services/ApplicationForReservationProxy");
                    conf.setHttpClient(AxisClientUtils.createHttpClient(18, 18));
                }
            }
        } catch (Exception e) {
            throw new EdmException(e.getMessage(),e);
        }
	}

	@Override
	public void exportXml(XmlCreator creator) throws EdmException {
		try {
			OMElement result = creator.getResult();
			OMElement response = getStub()._getServiceClient().sendReceive(result);
			this.result = response.getFirstElement().getText();
		} catch (Exception e) {
			throw new EdmException(e.getMessage(), e);
		}
	}

	@Override
	public String getResult() {
		return this.result;
	}

	@Override
	public void finalizeExport() {
		// TODO Auto-generated method stub
		
	}

}
