package pl.compan.docusafe.core.exports;

import java.util.Date;
import java.util.List;

import com.google.common.collect.Lists;

public abstract class ExportedDocument {
    protected String barcode;
    private String id;
	private Integer status;
	
	private String title;
	private String description;
	private String addInfo;
	
	private Date created;	//date rejestracji
	private Date recieved;	//data wp�ywu

    private String creatingUser;

	public List<AttachmentInfo> attachementInfo = Lists.newArrayList();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getRecieved() {
		return recieved;
	}
	public void setRecieved(Date recieved) {
		this.recieved = recieved;
	}
	public List<AttachmentInfo> getAttachementInfo() {
		return attachementInfo;
	}
	public void setAttachementInfo(List<AttachmentInfo> attachementInfo) {
		this.attachementInfo = attachementInfo;
	}
	public void addAttachementInfo(AttachmentInfo attachementInfo) {
		getAttachementInfo().add(attachementInfo);
	}
	public String getAddInfo() {
		return addInfo;
	}
	public void setAddInfo(String addInfo) {
		this.addInfo = addInfo;
	}
    public String getCreatingUser() {
        return creatingUser;
    }

    public void setCreatingUser(String creatingUser) {
        this.creatingUser = creatingUser;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExportedDocument other = (ExportedDocument) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
