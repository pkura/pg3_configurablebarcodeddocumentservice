package pl.compan.docusafe.core.exports;

/**
 * Created by kk on 28.03.14.
 */
public interface JaxbServiceHandledDocument extends JaxbDocument, ServiceHandledDocument {
}
