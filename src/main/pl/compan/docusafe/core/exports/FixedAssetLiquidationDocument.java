package pl.compan.docusafe.core.exports;

import com.google.common.collect.Lists;

import java.util.Date;
import java.util.List;

/**
 * Created by kk on 12.02.14.
 */
public class FixedAssetLiquidationDocument extends ExportedDocument {

    private String operationTypeCode;
    private String departmentCode;
    private Date validity;
    private Date operation;

    private List<FixedAssetPosition> positions = Lists.newArrayList();

    public String getOperationTypeCode() {
        return operationTypeCode;
    }

    public void setOperationTypeCode(String operationTypeCode) {
        this.operationTypeCode = operationTypeCode;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public Date getValidity() {
        return validity;
    }

    public void setValidity(Date validity) {
        this.validity = validity;
    }

    public Date getOperation() {
        return operation;
    }

    public void setOperation(Date operation) {
        this.operation = operation;
    }

    public List<FixedAssetPosition> getPositions() {
        return positions;
    }

    public void setPositions(List<FixedAssetPosition> positions) {
        this.positions = positions;
    }

    public void addPosition(FixedAssetPosition position) {
        this.positions.add(position);
    }
}
