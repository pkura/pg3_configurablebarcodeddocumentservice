package pl.compan.docusafe.core.exports;

public class RepositoryItem {
	
	private Long attribute;
	private Long value;
	
	public RepositoryItem(Long attribute, Long value) {
		this.attribute = attribute;
		this.value = value;
	}
	public Long getAttribute() {
		return attribute;
	}
	public void setAttribute(Long attribute) {
		this.attribute = attribute;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
	
}
