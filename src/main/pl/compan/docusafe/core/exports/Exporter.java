package pl.compan.docusafe.core.exports;

import pl.compan.docusafe.core.EdmException;

public interface Exporter {
	void initExport(ExportedDocument doc) throws EdmException;
	void exportXml(XmlCreator creator) throws EdmException;
	String getResult();
	void finalizeExport();
}
