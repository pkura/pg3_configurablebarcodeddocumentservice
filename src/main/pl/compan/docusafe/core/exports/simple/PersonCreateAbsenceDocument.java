package pl.compan.docusafe.core.exports.simple;

import org.apache.axis2.client.Stub;
import org.apache.axis2.context.ConfigurationContext;
import pl.compan.docusafe.core.exports.JaxbServiceHandledDocument;
import pl.compan.docusafe.core.exports.simple.xml.ObjectFactory;
import pl.compan.docusafe.core.exports.simple.xml.PerAbsencjaUtworz;
import pl.compan.docusafe.ws.simple.AssetOperationsProxyStub;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;

/**
 * Created by kk on 27.05.14.
 */
public class PersonCreateAbsenceDocument extends PerAbsencjaUtworz implements JaxbServiceHandledDocument {

    @XmlElementDecl(namespace = "http://www.simple.com.pl/SimpleErp", name = "per_absencja_utworz")
    public JAXBElement create() {
        return new JAXBElement<PerAbsencjaUtworz>(ObjectFactory._PerAbsencjaUtworz_QNAME, PerAbsencjaUtworz.class, null, this);
    }

    @Override
    public Stub getSenderStub(ConfigurationContext context) throws Exception {
        return new AssetOperationsProxyStub(context,"https://zsiesb:8243/services/AbsenceInContractCreateProxy.AbsenceInContractCreateProxyHttpsSoap12Endpoint" );
    }

    @Override
    public String getSenderStubServicesSuffix() {
        return "/services/AbsenceInContractCreateProxy";
    }
}
