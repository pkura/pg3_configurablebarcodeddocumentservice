package pl.compan.docusafe.core.exports.simple;

import pl.compan.docusafe.core.exports.simple.xml.IDENTITYNATURETYPE;
import pl.compan.docusafe.core.exports.simple.xml.IDNIDTYPE;

import java.math.BigDecimal;

/**
 * Created by kk on 27.03.14.
 */
public class BuildUtils {

    public static IDNIDTYPE newIdnIdType(String value) {
        return newIdnIdType(value, false);
    }

    public static IDNIDTYPE newIdnIdType(Number value) {
        return newIdnIdType(value, false);
    }

    public static IDNIDTYPE newIdnIdType(String value, boolean required) {
        if (value == null) {
            if (!required) {
                return null;
            } else {
                return newIdnIdTypeB();
            }
        } else {
            IDNIDTYPE result = new IDNIDTYPE();
            result.setIdn(value);
            result.setType(IDENTITYNATURETYPE.T);
            return result;
        }
    }

    public static IDNIDTYPE newIdnIdType(Number value, boolean required) {
        if (value == null) {
            if (!required) {
                return null;
            } else {
                return newIdnIdTypeB();
            }
        } else {
            IDNIDTYPE result = new IDNIDTYPE();
            BigDecimal number = value instanceof BigDecimal ? (BigDecimal) value : new BigDecimal(String.valueOf(value));
            result.setId(number);
            result.setType(IDENTITYNATURETYPE.N);
            return result;
        }
    }

    private static IDNIDTYPE newIdnIdTypeB() {
        IDNIDTYPE result = new IDNIDTYPE();
        result.setType(IDENTITYNATURETYPE.B);
        return result;
    }


}
