package pl.compan.docusafe.core.exports.simple;

import org.apache.axis2.client.Stub;
import org.apache.axis2.context.ConfigurationContext;
import pl.compan.docusafe.core.exports.JaxbServiceHandledDocument;
import pl.compan.docusafe.core.exports.simple.xml.MtfOpeTYPE;
import pl.compan.docusafe.ws.simple.AssetOperationsProxyStub;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;

/**
 * Created by kk on 28.03.14.
 */
public class MtfOpeDocument extends MtfOpeTYPE implements JaxbServiceHandledDocument {

    @XmlElementDecl(namespace = "http://www.simple.com.pl/SimpleErp", name = "mtfope")
    public JAXBElement create() {
        return new JAXBElement<MtfOpeTYPE>(_Mtfope_QNAME, MtfOpeTYPE.class, null, this);
    }

    @Override
    public Stub getSenderStub(ConfigurationContext context) throws Exception {
        return new AssetOperationsProxyStub(context);
    }

    @Override
    public String getSenderStubServicesSuffix() {
        return "/services/AssetOperationsProxy";
    }

}
