package pl.compan.docusafe.core.exports;

import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class PreElement {
	private String name;
	private Type type;
	private TypeAttribute typeAttribute;
	private boolean forceIdm = false;
	private boolean cannotNull = false;
	private String forceTypeAttribute;
	private List<PreElement> elements = Lists.newArrayList();
	private Object value;
	private Object additionalValue;

	public PreElement(String name, Type type, Object value) {
		this.name = name;
		this.type = type;
		if (type == Type.T) {
			if (value != null && value.toString().length() > 15) {
				this.setTypeAttribute(TypeAttribute.idm);
			} else {
				this.setTypeAttribute(TypeAttribute.idn);
			}
		} else {
			this.setTypeAttribute(TypeAttribute.AUTO);
		}
		this.value = value;
	}

    public static PreElement newNormalIdentifiedElement(String name, TypeAttribute codeTypeAttribute, Number numericIdentifier, String codeIdentifier, boolean obligatory) {
        PreElement element = new PreElement();
        element.setType(Type.AUTO);
        element.setName(name);
        element.setTypeAttribute(codeTypeAttribute);
        element.setValue(codeIdentifier);
        element.setAdditionalValue(numericIdentifier);
        element.setCannotNull(obligatory);
        return element;
    }

    public static PreElement newSimpleValuedElement(String name, Object value) {
        PreElement element = new PreElement();
        element.setType(null);
        element.setName(name);
        element.setValue(value);
        return element;
    }

    public static PreElement newObligatoryPreElement(String name, TypeAttribute textValueTypeAtribute, Number value, String additionalValue) {
        PreElement elem = new PreElement();
        elem.setName(name);
        elem.setTypeAttribute(textValueTypeAtribute);
        elem.setValue(value);
        elem.setAdditionalValue(additionalValue);

        return elem;
    }
	
	public PreElement(String name, Type type, TypeAttribute typeAttribute, Object value) {
		this.name = name;
		this.type = type;
		this.typeAttribute = typeAttribute;
		this.value = value;
	}

	public PreElement(String name, Object value, Object additionalValue) {
		this.name = name;
		this.type = Type.AUTO;
		this.value = value;
		this.additionalValue = additionalValue;
	}
	
	public PreElement(String name, boolean forceIdm, Object value, Object additionalValue) {
		this.name = name;
		this.type = Type.AUTO;
		this.forceIdm = forceIdm;
		this.value = value;
		this.additionalValue = additionalValue;
	}

	public PreElement(String name, boolean forceIdm, Object value, Object additionalValue, boolean cannotNull) {
		this.name = name;
        this.type = Type.AUTO;
		this.forceIdm = forceIdm;
		this.value = value;
		this.additionalValue = additionalValue;
        this.cannotNull = cannotNull;
	}

	public PreElement(String name, Type type, String forceTypeAttribute, Object value) {
		this.name = name;
		this.type = type;
		this.forceTypeAttribute = forceTypeAttribute;
		this.value = value;
	}

	public PreElement(String name, Object value) {
		this.name = name;
		this.value = value;
	}
	
	public PreElement(String name) {
		this.name = name;
	}

	public PreElement() {
	}

	public void addElement(PreElement element) {
		Preconditions.checkArgument(value == null && additionalValue == null, "You can't addElement, if value(s) is set.");
		if (this.elements.isEmpty()) {
			this.elements = Lists.newArrayList();
		}
		this.elements.add(element);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public TypeAttribute getTypeAttribute() {
		return typeAttribute;
	}

	public void setTypeAttribute(TypeAttribute typeAttribute) {
		this.typeAttribute = typeAttribute;
	}

	public String getForceTypeAttribute() {
		return forceTypeAttribute;
	}

	public void setForceTypeAttribute(String forceTypeAttribute) {
		this.forceTypeAttribute = forceTypeAttribute;
	}

	public List<PreElement> getElements() {
		return elements;
	}

	public void setElements(List<PreElement> elements) {
		this.elements = elements;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Object getAdditionalValue() {
		return additionalValue;
	}

	public void setAdditionalValue(Object additionalValue) {
		this.additionalValue = additionalValue;
	}

	public boolean isForceIdm() {
		return forceIdm;
	}

	public void setForceIdm(boolean forceIdm) {
		this.forceIdm = forceIdm;
	}

    public boolean isCannotNull() {
        return cannotNull;
    }

    public void setCannotNull(boolean cannotNull) {
        this.cannotNull = cannotNull;
    }
}

enum Type {
	AUTO,N,T,B;
}
enum TypeAttribute {
	AUTO,ids,idn,idm,id;
}