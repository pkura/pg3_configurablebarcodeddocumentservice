package pl.compan.docusafe.core.exports;

import java.math.BigDecimal;

public class FundSource {
	private Long id;
	private String code;
	private String title;
	private BigDecimal fundPercent;
	private BigDecimal amount;
	private Integer kind;
	
	public static final Integer KIND_BD_BUDGET = 1;
	public static final Integer KIND_PROJECT = 2;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public FundSource setCode(String code) {
		this.code = code;
        return this;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public BigDecimal getFundPercent() {
		return fundPercent;
	}
	public void setFundPercent(BigDecimal fundPercent) {
		this.fundPercent = fundPercent;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public FundSource setAmount(BigDecimal amount) {
		this.amount = amount;
        return this;
	}
	public Integer getKind() {
		return kind;
	}
	/**
	 * Parametr okreslający czy zrodlo z projektów = 2 czy z budzetu komorek = 1
	 * @param kind
	 */
	public void setKind(Integer kind) {
		this.kind = kind;
	}
	
}
