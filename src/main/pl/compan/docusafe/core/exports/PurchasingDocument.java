package pl.compan.docusafe.core.exports;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

public class PurchasingDocument extends ExportedDocument {

	private Date issued; 	//data wystawienia
	private Date expired;	//termin p�atno�ci
    private Date operation; //data operacji

    private String number;
	private String code; //nr faktury

    private Integer kind;
	private String type;
    private String delivery;

	private Long contractor;
    private Long externalContractor;

	private String contractorAccount;
	private Integer contractorAccountId;

	private BigDecimal netAmount;
	private BigDecimal grossAmount;
	private BigDecimal vatAmount;

	private BigDecimal exchangeRate;
	private String currencyRateCode;
	private String currencyCode;
	private Integer currency;

	private String paymentMethod; //spos�b p�atno�ci
    private Integer paymentMethodId;
	private String returnMethod; //spos�b zwrotu

	private String purchaseType; //typ dokumentu zakupowego
	private Long settlementPeriodId;
	private Long settlementTaxPeriodId;

	private List<BudgetItem> budgetItems = Lists.newArrayList();
    private List<RepositoryItem> repositoryItems = Lists.newArrayList();
    private List<InstallmentItem> installmentItems = Lists.newArrayList();

    public Date getIssued() {
		return issued;
	}

	public void setIssued(Date issued) {
		this.issued = issued;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getKind() {
		return kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getContractor() {
		return contractor;
	}

	public void setContractor(Long contractor) {
		this.contractor = contractor;
	}

    public Long getExternalContractor() {
        return externalContractor;
    }

    public void setExternalContractor(Long externalContractor) {
        this.externalContractor = externalContractor;
    }

    public String getContractorAccount() {
		return contractorAccount;
	}

	public void setContractorAccount(String contractorAccount) {
		this.contractorAccount = contractorAccount;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public BigDecimal getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getReturnMethod() {
		return returnMethod;
	}

	public void setReturnMethod(String returnMethod) {
		this.returnMethod = returnMethod;
	}

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public Integer getContractorAccountId() {
		return contractorAccountId;
	}

	public void setContractorAccountId(Integer contractorAccountId) {
		this.contractorAccountId = contractorAccountId;
	}

	public List<BudgetItem> getBudgetItems() {
		return budgetItems;
	}

	public void setBudgetItems(List<BudgetItem> budgetItems) {
		this.budgetItems = budgetItems;
	}
	
	public void addBudgetItem(BudgetItem budgetItem) {
		this.budgetItems.add(budgetItem);
	}

    public List<RepositoryItem> getRepositoryItems() {
        return repositoryItems;
    }

    public void setRepositoryItems(List<RepositoryItem> repositoryItems) {
        this.repositoryItems = repositoryItems;
    }

    public void addRepositoryItem(RepositoryItem repositoryItem) {
        this.repositoryItems.add(repositoryItem);
    }

    public String getDelivery() {
        if (delivery == null) {
            delivery = "SD";
        }
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getCurrencyRateCode() {
        return currencyRateCode;
    }

    public void setCurrencyRateCode(String currencyRateCode) {
        this.currencyRateCode = currencyRateCode;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Long getSettlementPeriodId() {
        return settlementPeriodId;
    }

    public List<InstallmentItem> getInstallmentItems() {
        return installmentItems;
    }

    public void setInstallmentItems(List<InstallmentItem> installmentItems) {
        this.installmentItems = installmentItems;
    }

    public void addInstallmentItem(InstallmentItem installmentItem) {
        this.installmentItems.add(installmentItem);
    }

    public void setSettlementPeriodId(Long settlementPeriodId) {
        this.settlementPeriodId = settlementPeriodId;
    }

    public Long getSettlementTaxPeriodId() {
        return settlementTaxPeriodId;
    }

    public void setSettlementTaxPeriodId(Long settlementTaxPeriodId) {
        this.settlementTaxPeriodId = settlementTaxPeriodId;
    }

    public Date getOperation() {
        return operation;
    }

    public void setOperation(Date operation) {
        this.operation = operation;
    }

    public Date getOperationOrDefault() {
        return Objects.firstNonNull(operation, new Date());
    }
}
