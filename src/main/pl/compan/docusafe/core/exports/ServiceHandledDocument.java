package pl.compan.docusafe.core.exports;

import org.apache.axis2.client.Stub;
import org.apache.axis2.context.ConfigurationContext;

/**
 * Created by kk on 28.03.14.
 */
public interface ServiceHandledDocument {

    Stub getSenderStub(ConfigurationContext context) throws Exception;

    String getSenderStubServicesSuffix();

}
