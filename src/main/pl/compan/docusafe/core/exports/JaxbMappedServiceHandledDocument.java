package pl.compan.docusafe.core.exports;

import org.apache.axis2.client.Stub;
import org.apache.axis2.context.ConfigurationContext;

import javax.xml.bind.JAXBElement;

/**
 * Created by kk on 27.03.14.
 */
public class JaxbMappedServiceHandledDocument extends ExportedDocument implements JaxbServiceHandledDocument {

    private JaxbServiceHandledDocument object;

    private JaxbMappedServiceHandledDocument(){}

    public static JaxbMappedServiceHandledDocument newDocument(JaxbServiceHandledDocument object) {
        JaxbMappedServiceHandledDocument result = new JaxbMappedServiceHandledDocument();
        result.object = object;
        return result;
    }

    @Override
    public JAXBElement create() {
        return this.object.create();
    }

    @Override
    public Stub getSenderStub(ConfigurationContext context) throws Exception {
        return this.object.getSenderStub(context);
    }

    @Override
    public String getSenderStubServicesSuffix() {
        return this.object.getSenderStubServicesSuffix();
    }
}
