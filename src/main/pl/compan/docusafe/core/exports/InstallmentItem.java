package pl.compan.docusafe.core.exports;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by brs on 09.01.14.
 */
public class InstallmentItem {
    private Integer sposobZaplaty;

    private Boolean czyProcent;
    private BigDecimal procentKwoty;
    private BigDecimal kwota;

    private Integer typPlatnosci;
    private Date terminPlatnosci;
    private Integer liczbaDni;

    private BigDecimal kurs;

    public BigDecimal getKwotaWaluty() {
        return kwotaWaluty;
    }

    public void setKwotaWaluty(BigDecimal kwotaWaluty) {
        this.kwotaWaluty = kwotaWaluty;
    }

    public BigDecimal getKurs() {
        return kurs;
    }

    public void setKurs(BigDecimal kurs) {
        this.kurs = kurs;
    }

    public Integer getLiczbaDni() {
        return liczbaDni;
    }

    public void setLiczbaDni(Integer liczbaDni) {
        this.liczbaDni = liczbaDni;
    }

    public Date getTerminPlatnosci() {
        return terminPlatnosci;
    }

    public void setTerminPlatnosci(Date terminPlatnosci) {
        this.terminPlatnosci = terminPlatnosci;
    }

    public Integer getTypPlatnosci() {
        return typPlatnosci;
    }

    public void setTypPlatnosci(Integer typPlatnosci) {
        this.typPlatnosci = typPlatnosci;
    }

    public BigDecimal getKwota() {
        return kwota;
    }

    public void setKwota(BigDecimal kwota) {
        this.kwota = kwota;
    }

    public BigDecimal getProcentKwoty() {
        return procentKwoty;
    }

    public void setProcentKwoty(BigDecimal procentKwoty) {
        this.procentKwoty = procentKwoty;
    }

    public Boolean getCzyProcent() {
        return czyProcent;
    }

    public void setCzyProcent(Boolean czyProcent) {
        this.czyProcent = czyProcent;
    }

    public Integer getSposobZaplaty() {
        return sposobZaplaty;
    }

    public void setSposobZaplaty(Integer sposobZaplaty) {
        this.sposobZaplaty = sposobZaplaty;
    }

    private BigDecimal kwotaWaluty;
}
