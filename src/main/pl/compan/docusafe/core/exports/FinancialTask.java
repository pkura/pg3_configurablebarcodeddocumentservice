package pl.compan.docusafe.core.exports;

import java.math.BigDecimal;

/**
 * Created by kk on 05.03.14.
 */
public class FinancialTask {
    private Number taskId;
    private String taskCode;
    private BigDecimal amount;
    private BigDecimal percent;

    public String getTaskCode() {
        return taskCode;
    }

    public FinancialTask setTaskCode(String taskCode) {
        this.taskCode = taskCode;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public FinancialTask setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public FinancialTask setPercent(BigDecimal percent) {
        this.percent = percent;
        return this;
    }

    public Number getTaskId() {
        return taskId;
    }

    public FinancialTask setTaskId(Number taskId) {
        this.taskId = taskId;
        return this;
    }
}
