package pl.compan.docusafe.core.exports;

import java.math.BigDecimal;

/**
 * Created by kk on 13.02.14.
 */
public class FixedAssetPositionDetail {

    private String registrationSystemCode;
    private BigDecimal ratio;

    public String getRegistrationSystemCode() {
        return registrationSystemCode;
    }

    public FixedAssetPositionDetail setRegistrationSystemCode(String registrationSystemCode) {
        this.registrationSystemCode = registrationSystemCode;
        return this;
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public FixedAssetPositionDetail setRatio(BigDecimal ratio) {
        this.ratio = ratio;
        return this;
    }
}
