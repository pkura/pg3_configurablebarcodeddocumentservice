package pl.compan.docusafe.core.exports;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * User: kk
 * Date: 23.01.14
 * Time: 15:54
 */
public class ReservationDocument extends ExportedDocument {

    private boolean costKindCodeAllowed;
    private Number budgetReservationTypeId;

    public ReservationMethod getReservationMethod() {
        return reservationMethod;
    }

    public void setReservationMethod(ReservationMethod reservationMethod) {
        this.reservationMethod = reservationMethod;
    }

    public boolean isCostKindCodeAllowed() {
        return costKindCodeAllowed;
    }

    public void setCostKindCodeAllowed(boolean costKindCodeAllowed) {
        this.costKindCodeAllowed = costKindCodeAllowed;
    }

    public Number getBudgetReservationTypeId() {
        return budgetReservationTypeId;
    }

    public void setBudgetReservationTypeId(Number budgetReservationTypeId) {
        this.budgetReservationTypeId = budgetReservationTypeId;
    }

    public enum ReservationState {
        WORKING(1, "bd_rezerwacja", "bdrezerwacja.xsd"),
        ACCEPTED(2, "bd_rezerwacja_akceptacja", "bdrezerwacja_akceptacja.xsd"),
        APPROVED(3, "bd_rezerwacja_zatwierdz", "bdrezerwacja_zatwierdz.xsd"),
        REJECTED(0, "bd_rezerwacja_anuluj", "bdrezerwacja_anuluj.xsd");

        private int stateId;
        private String rootElementName;
        private String xsdFileName;

        private ReservationState(int stateId, String rootElementName, String xsdFileName) {
            this.stateId = stateId;
            this.rootElementName = rootElementName;
            this.xsdFileName = xsdFileName;
        }

        public String getRootElementName() {
            return rootElementName;
        }

        public String getXsdFileName() {
            return xsdFileName;
        }

        public int getStateId() {
            return stateId;
        }

        private static final Map<Integer, ReservationState> ENUMS = Maps.uniqueIndex(Lists.newArrayList(ReservationState.values()), new Function<ReservationState, Integer>() {
            @Override
            public Integer apply(ReservationState input) {
                return input.getStateId();
            }
        });

        public static ReservationState getReservationState(Integer stateId) {
            return ENUMS.get(stateId);
        }
    };


    public enum ReservationMethod {
        AUTO(2),
        ACCEPTED(0),
        APPROVED(1);

        private int id;

        private ReservationMethod(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        private static final Map<Integer, ReservationMethod> ENUMS = Maps.uniqueIndex(Lists.newArrayList(ReservationMethod.values()), new Function<ReservationMethod, Integer>() {
            @Override
            public Integer apply(ReservationMethod input) {
                return input.getId();
            }
        });

        public static ReservationMethod getReservationMethod(Integer id) {
            return ENUMS.get(id);
        }
    };

    private ReservationMethod reservationMethod;
    private ReservationState reservationState;
    private String reservationNumber;

    private String budgetReservationTypeCode;
    private Date reservationDate;
    private BudgetItem headerBudget = new BudgetItem();
    private List<BudgetItem> postionItems = Lists.newArrayList();

    public String getBudgetReservationTypeCode() {
        return budgetReservationTypeCode;
    }

    public void setBudgetReservationTypeCode(String budgetReservationTypeCode) {
        this.budgetReservationTypeCode = budgetReservationTypeCode;
    }

    public List<BudgetItem> getPostionItems() {
        return postionItems;
    }

    public void setPostionItems(List<BudgetItem> postionItems) {
        this.postionItems = postionItems;
    }

    public Date getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(Date reservationDate) {
        this.reservationDate = reservationDate;
    }

    public BudgetItem getHeaderBudget() {
        return headerBudget;
    }

    public ReservationState getReservationState() {
        return reservationState;
    }

    public void setReservationState(ReservationState reservationState) {
        this.reservationState = reservationState;
    }

    public void setReservationStateFromId(Integer reservationStateId) {
        this.reservationState = ReservationState.getReservationState(reservationStateId);
    }

    public String getReservationNumber() {
        return reservationNumber;
    }

    public void setReservationNumber(String reservationNumber) {
        this.reservationNumber = reservationNumber;
    }
}
