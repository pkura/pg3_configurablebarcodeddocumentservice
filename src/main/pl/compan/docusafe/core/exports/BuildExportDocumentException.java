package pl.compan.docusafe.core.exports;

import pl.compan.docusafe.core.EdmException;

/**
 * Created by kk on 02.04.14.
 */
public class BuildExportDocumentException extends EdmException {
    public BuildExportDocumentException(String message) {
        super(message);
    }
}
