package pl.compan.docusafe.core.exports;

import java.math.BigDecimal;
import java.util.List;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

public class BudgetItem implements Cloneable {

	private Integer id;
	private String code;
	private Integer itemNo;
	
	private String name;
	
	private boolean fixedAsset = false;
	
	private BigDecimal quantity;
	private Object unit;
	
	private BigDecimal netAmount;
	private BigDecimal vatAmount;
	private BigDecimal grossAmount;
	
	private String vatRateCode;
	private Integer vatRate;

	private BigDecimal exchangeRate;
	private String currencyCode;
	private Integer currency;
	
	private Long owner;
	private String ownerCode;
	
	private Long resourceId;
	private String resourceCode;
	private Long positionId;
	private String positionCode;
	private Long budgetId;
	private String budgetCode;
	private Long projectId;
	private String projectCode;
	private Long costKindId;
	private String costKindCode;
	private String costKindName;
	private Long mpkId;
	private String mpkCode;
	private Long orderId;
	private String orderCode;
	
	private List<RepositoryItem> repositoryItems = Lists.newArrayList();
	private List<FundSource> fundSources = Lists.newArrayList();
    private List<FinancialTask> finanancialTasks = Lists.newArrayList();
    private Long planZamId;
    private Long rodzajPlanZamId;
    private Long budgetKoId;
    private String budgetKoCode;
    private Long budgetKindId;
    private Long budgetDirectId;
    private String budgetDirectCode;
    private String budgetCellCode;
    private TaxType taxType;
    private BigDecimal templateId;
    private String templateCode;


    private Integer sprzedazOpodatkowana;
    private String budgetTaskCode;
    private Object reservationPositionId;
    private Number reservationId;
    private String reservationCode;
    private String taxRatio;

    public String getCostKindName() {
        return costKindName;
    }

    public void setCostKindName(String costKindName) {
        this.costKindName = costKindName;
    }

    public String getBudgetCellCode() {
        return budgetCellCode;
    }

    public void setBudgetCellCode(String budgetCellCode) {
        this.budgetCellCode = budgetCellCode;
    }

    public String getBudgetTaskCode() {
        return budgetTaskCode;
    }

    public void setBudgetTaskCode(String budgetTaskCode) {
        this.budgetTaskCode = budgetTaskCode;
    }

    public Object getReservationPositionId() {
        return reservationPositionId;
    }

    public void setReservationPositionId(Object reservationPositionId) {
        this.reservationPositionId = reservationPositionId;
    }

    public Number getReservationId() {
        return reservationId;
    }

    public void setReservationId(Number reservationId) {
        this.reservationId = reservationId;
    }

    public String getReservationCode() {
        return reservationCode;
    }

    public void setReservationCode(String reservationCode) {
        this.reservationCode = reservationCode;
    }

    public void setTaxRatio(String taxRatio) {
        this.taxRatio = taxRatio;
    }

    public String getTaxRatio() {
        return taxRatio;
    }

    public List<FinancialTask> getFinanancialTasks() {
        return finanancialTasks;
    }

    public void setFinanancialTasks(List<FinancialTask> finanancialTasks) {
        this.finanancialTasks = finanancialTasks;
    }

    public BudgetItem addFinanacialTask(FinancialTask finanancialTask) {
        this.finanancialTasks.add(finanancialTask);
        return this;
    }

    public BudgetItem addFundSource(FundSource fundSource) {
        this.fundSources.add(fundSource);
        return this;
    }

    public enum TaxType {
        TAXABLE(0), TAXFREE(1), UNDEFINED(2);
        int simpleErpId;

        TaxType(int simpleErpId) {
            this.simpleErpId = simpleErpId;
        }

        public int getSimpleErpId() {
            return simpleErpId;
        }
    }

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getItemNo() {
		return itemNo;
	}

	public void setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isFixedAsset() {
		return fixedAsset;
	}

	public void setFixedAsset(boolean fixedAsset) {
		this.fixedAsset = fixedAsset;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Object getUnit() {
		return unit;
	}

	public void setUnit(Object unit) {
		this.unit = unit;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public BigDecimal getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public String getVatRateCode() {
		return vatRateCode;
	}

	public String getVatRateCodeOrDefault() {
		return Objects.firstNonNull(vatRateCode, String.valueOf(23));
	}

	public void setVatRateCode(String vatRateCode) {
		this.vatRateCode = vatRateCode;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Long getOwner() {
		return owner;
	}

	public void setOwner(Long owner) {
		this.owner = owner;
	}

	public String getOwnerCode() {
		return ownerCode;
	}

	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}

	public String getResourceCode() {
		return resourceCode;
	}

	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}

	public String getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}

	public String getBudgetCode() {
		return budgetCode;
	}

	public void setBudgetCode(String budgetCode) {
		this.budgetCode = budgetCode;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getCostKindCode() {
		return costKindCode;
	}

	public void setCostKindCode(String costKindCode) {
		this.costKindCode = costKindCode;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getBudgetId() {
		return budgetId;
	}

	public void setBudgetId(Long budgetId) {
		this.budgetId = budgetId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getCostKindId() {
		return costKindId;
	}

	public void setCostKindId(Long costKindId) {
		this.costKindId = costKindId;
	}

	public Long getMpkId() {
		return mpkId;
	}

	public void setMpkId(Long mpkId) {
		this.mpkId = mpkId;
	}

	public String getMpkCode() {
		return mpkCode;
	}

	public void setMpkCode(String mpkCode) {
		this.mpkCode = mpkCode;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public List<RepositoryItem> getRepositoryItems() {
		return repositoryItems;
	}
	
	public void addRepositoryItem(RepositoryItem item) {
		repositoryItems.add(item);
	}

	public void setRepositoryItems(List<RepositoryItem> repositoryItems) {
		this.repositoryItems = repositoryItems;
	}

	public List<FundSource> getFundSources() {
		return fundSources;
	}

	public void setFundSources(List<FundSource> fundSources) {
		this.fundSources = fundSources;
	}

    public Long getBudgetKoId() {
        return budgetKoId;
    }

    public void setBudgetKoId(Long budgetKoId) {
        this.budgetKoId = budgetKoId;
    }

    public Long getBudgetKindId() {
        return budgetKindId;
    }

    public void setBudgetKindId(Long budgetKindId) {
        this.budgetKindId = budgetKindId;
    }

    public Long getPlanZamId() {
        return planZamId;
    }

    public void setPlanZamId(Long planZamId) {
        this.planZamId = planZamId;
    }

    public Long getRodzajPlanZamId() {
        return rodzajPlanZamId;
    }

    public void setRodzajPlanZamId(Long rodzajPlanZamId) {
        this.rodzajPlanZamId = rodzajPlanZamId;
    }

    public Long getBudgetDirectId() {
        return budgetDirectId;
    }

    public void setBudgetDirectId(Long budgetDirectId) {
        this.budgetDirectId = budgetDirectId;
    }

    public String getBudgetDirectCode() {
        return budgetDirectCode;
    }

    public void setBudgetDirectCode(String budgetDirectCode) {
        this.budgetDirectCode = budgetDirectCode;
    }

    public TaxType getTaxType() {
        return taxType;
    }

    public void setTaxType(TaxType taxType) {
        this.taxType = taxType;
    }

    public Integer getSprzedazOpodatkowana() {
        return sprzedazOpodatkowana;
    }

    public void setSprzedazOpodatkowana(Integer sprzedazOpodatkowana) {
        this.sprzedazOpodatkowana = sprzedazOpodatkowana;
    }

	public BigDecimal getTemplateId() {
		return templateId;
	}

	public void setTemplateId(BigDecimal templateId) {
		this.templateId = templateId;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

    public Integer getVatRate() {
        return vatRate;
    }

    public void setVatRate(Integer vatRate) {
        this.vatRate = vatRate;
    }

    public String getBudgetKoCode() {
        return budgetKoCode;
    }

    public void setBudgetKoCode(String budgetKoCode) {
        this.budgetKoCode = budgetKoCode;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public BudgetItem newCloned() {
        try {
            return (BudgetItem) this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}