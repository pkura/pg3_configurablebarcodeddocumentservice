package pl.compan.docusafe.core.exports;

import pl.compan.docusafe.core.base.Attachment;

public class AttachmentInfo {
	private String name;
	private String url;
	private Attachment attachment;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Attachment getAttachment() {
		return attachment;
	}
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}
}
