package pl.compan.docusafe.core.calendar;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.sql.AnyResource;
import pl.compan.docusafe.core.calendar.sql.CalendarEvent;
import pl.compan.docusafe.core.calendar.sql.UserCalendarBinder;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class Calendar 
{
	private static Logger log = LoggerFactory.getLogger(Calendar.class);
	public static final String USER_CALENDAR = "USER_CALENDAR";
	public static final String ANY_CALENDAR = "ANY_CALENDAR";
	
	private Long id;
	private String name;
	private String owner;
	private String type;
	private Set<DSTime> events;
	private TimeResource resource;
	
	public Set<DSTime> getEvents() 
	{
		return events;
	}

	public TimeResource getTimeResource() throws EdmException
	{
		if(this.resource  == null)
		{
			if(USER_CALENDAR.equals(type))
				this.resource =  DSUser.findByUsername(owner);
			else
				this.resource = AnyResource.findByCn(owner);
		}
		return this.resource;
	}
	
    /**
     * Zapisuje strone w sesji hibernate. Je�li tworzymy kalendarz uzytkownika jest on od razu nam udost�pniany
     * Zwraca obiekt powiazania wlasciciela z kalendarzem
     * @throws EdmException
     */
    public UserCalendarBinder create() throws EdmException
    {
    	UserCalendarBinder ucb = null;
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
            if(USER_CALENDAR.equals(type))
            {
    			ucb= new UserCalendarBinder();
	        	ucb.setPermissionType(CalendarFactory.PERMISSION_EDIT);
	        	ucb.setCalendarId(this.getId());
	        	ucb.setShow(true);
	        	ucb.setUsername(this.owner);
	        	ucb.setColorId(0); // na wszelki wypadek kolor automatczny
	        	ucb.create();
            }
        }
        catch (HibernateException e) 
        {
            log.error(e.getMessage(),e);
            throw new EdmException("Blad dodania kalendarza "+e.getMessage());
        }
        return ucb;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setEvents(Set<DSTime> events) {
		this.events = events;
	}

	public static List<Calendar> findUserCalendars(String username)
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(Calendar.class);
            c.add(Restrictions.eq("owner",username));
            List ucbs = c.list();
			return ucbs;
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania calendarzy dla "+ username +" ",e);
		}
        return null;
	}
	
	public static Calendar getDefaultCalendar(String cn)
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Calendar calendar = null;
        try
        {
            ps = DSApi.context().prepareStatement("select cal.id from ds_calendar cal,ds_user_calendar uc where cal.id = uc.calendarid and owner = ? and show = ? and username = ? order by uc.posn");
            ps.setString(1, cn);
            ps.setBoolean(2, true);
            ps.setString(3, cn);
            rs = ps.executeQuery();
            
            if(rs.next())
            {
            	Long id = rs.getLong(1);
            	calendar = DSApi.context().load(Calendar.class, id);
				
            }
            else
            {
            	boolean openContext = false;
        		if(!DSApi.context().isTransactionOpen())
        		{
        			DSApi.context().begin();
        			openContext = true;
        		}
        		calendar = new Calendar();
        		String name = "";
        		
        		try
        		{
        			name = DSUser.findByUsername(cn).asLastnameFirstname();
        		}
        		catch (Exception e) {
					name = AnyResource.findByCn(cn).getName();
				}
        		calendar.setName(name);
        		calendar.setOwner(cn);
        		calendar.setType(Calendar.USER_CALENDAR);
        		calendar.create();
        		if(openContext)
        			DSApi.context().commit();
            }
            calendar.getTimeResource();
        }
        catch (Exception e)
		{
			log.error("B��d wyszukania domuslnego calendarzy dla "+ cn +" ",e);
		}
        finally
        {
        	DbUtils.closeQuietly(rs);
        	DSApi.context().closeStatement(ps);
        }
        return calendar;
	}
	
	public static Calendar getDefaultCalendar()
	{
		return getDefaultCalendar(DSApi.context().getPrincipalName());
	
	}
	
	public static List<Calendar> getMyCalendar() {
		return findUserCalendars(DSApi.context().getPrincipalName());
	}

	public static Calendar find(Long calendarId) throws EdmException 
	{
		return DSApi.context().load(Calendar.class, calendarId);
	}


    /**
     * Usuwa calendarz
     * @throws EdmException
     */
    public void delete() throws Exception 
    {
    	try
    	{
        	List<UserCalendarBinder> ucbs = UserCalendarBinder.findAll(id);
        	if(ucbs != null)
        	{
        		for (UserCalendarBinder binder : ucbs) 
        		{
        			binder.delete();
				}
        	}
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
    	}
    	catch (Throwable e) {
			log.error(e.getMessage(),e);
		}
    }
    
    public List<CalendarEvent> getEventsBetween(Date startDate, Date stopDate) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(CalendarEvent.class);
		criteria.add(Restrictions.eq("calendarId", this.id));
		criteria.add(Restrictions.le("startTime", startDate));
		criteria.add(Restrictions.ge("endTime", stopDate));
		return criteria.list();
	}
    
    public List<CalendarEvent> getAllEventsBetween(Date startDate, Date stopDate) throws EdmException 
    {
    	Criteria criteria = DSApi.context().session().createCriteria(CalendarEvent.class);
    	criteria.add(Restrictions.eq("calendarId", this.id));
    	criteria.add(
    		Restrictions.or(
    			Restrictions.or
    			(
    					Restrictions.and
    					(
    							Restrictions.lt("startTime", startDate),
    							Restrictions.gt("endTime", startDate)
    					),
    					Restrictions.and
    					(
    							Restrictions.lt("startTime", stopDate),
    							Restrictions.gt("endTime", stopDate)
    					)    					
    			),
    			Restrictions.and
				(
						Restrictions.gt("startTime", startDate),
						Restrictions.lt("endTime", stopDate)
				)
				)
    	);
    	return criteria.list();
    }
    
	public List<CalendarEvent> getEvents(Date startDate, Date stopDate) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(CalendarEvent.class);
		criteria.add(Restrictions.eq("calendarId", this.id));
		criteria.add(Restrictions.between("startTime", startDate, stopDate));
		return criteria.list();
	}
}
