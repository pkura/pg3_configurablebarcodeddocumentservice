/*
 * Created on 2007-02-12
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pl.compan.docusafe.core.calendar;

import java.util.Calendar;
import java.util.Set;

import pl.compan.docusafe.core.calendar.sql.CalendarEvent;
import pl.compan.docusafe.core.users.DSUser;

/**
 * @author Marcin W
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public abstract class DSTime {

	public static final int SORT_NONE = 0;

	public static final int SORT_STARTTIME = 1;

	public static final int SORT_ENDTIME = 2;

	public static final int SORT_CREATORNAME = 3;

	public static final int SORT_DESCRIPTION = 4;

	public static final int SORT_PLACE = 5;

	Long id;

	public abstract Long getId();

	protected abstract void setId(Long id);

	public abstract DSUser getCreator();

	public abstract void setCreator(DSUser creator);
	
	public abstract boolean canModify(DSUser user);


	private static String getWeekdayName(int wd) {
		switch (wd) {
		case Calendar.MONDAY:
			return "Poniedzia�ek";
		case Calendar.TUESDAY:
			return "Wtorek";
		case Calendar.WEDNESDAY:
			return "�roda";
		case Calendar.THURSDAY:
			return "Czwartek";
		case Calendar.FRIDAY:
			return "Pi�tek";
		case Calendar.SATURDAY:
			return "Sobota";
		case Calendar.SUNDAY:
			return "Niedziela";
		default:
			return "Niepoprawny numer dnia";
		}
	}
}
