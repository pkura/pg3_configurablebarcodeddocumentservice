package pl.compan.docusafe.core.calendar;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.sql.SqlCalendarFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.web.office.tasklist.calendar.JSResource;

/**
 * @author Marcin W
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public abstract class CalendarFactory
{
	private static final Log log = LogFactory.getLog(CalendarFactory.class);
	
	public static final String DSUSER_RESOURCE_TYPE = "DSUSER";
	public static final String ANY_RESOURCE_TYPE = "ANY";
	
	public static final Integer PERMISSION_EDIT = 1;
	public static final Integer PERMISSION_SEE_ONLY = 2;
	public static final Integer PERMISSION_SEE_ONLY_STATE = 3;

	static CalendarFactory factory = new SqlCalendarFactory();

	public synchronized static void setFactory(String name)
	{
		factory = new SqlCalendarFactory();
	}

	public static CalendarFactory getInstance()
	{
		return factory;
	}

	public abstract boolean isLdap();

	public abstract int getTimeCount() throws EdmException;
	
	public abstract List<JSResource> getAllUsers() throws EdmException;
	
	public abstract List<JSResource> getAllResources() throws EdmException;
	
	public abstract TimeResource getResourceByCnAndType(String cn,String type) throws EdmException;

//	public abstract SearchResults<DSTime> searchTimesSpi(Date after, Date before, DSUser creator, TimeResource timeResource, int sortMode, boolean ascending) throws EdmException;

//	protected abstract DSTime createTimeSpi(Date startTime, Date endTime, String place, String description, DSUser creator) throws EdmException;

	protected abstract DSTime[] getTimesSpi() throws EdmException;
	
	protected abstract void deleteTimeSpi(Long id) throws EdmException;
/*
	public final SearchResults<DSTime> searchTimes(Date after, Date before, DSUser creator, TimeResource timeResource, int sortMode, boolean ascending) throws EdmException
	{
		if (sortMode != DSTime.SORT_STARTTIME && sortMode != DSTime.SORT_ENDTIME && sortMode != DSTime.SORT_CREATORNAME && sortMode != DSTime.SORT_DESCRIPTION && sortMode != DSTime.SORT_PLACE)
			sortMode = DSTime.SORT_NONE;

		return searchTimesSpi(after, before, creator, timeResource, sortMode, ascending);
	}
*/
//	public final DSTime createTime(Date startTime, Date endTime, String place, String description, DSUser creator) throws EdmException
	{
	//	DSTime time = createTimeSpi(startTime, endTime, place, description, creator);
	//	return time;
	}
	
	public final void deleteTime(Long id) throws EdmException {
		deleteTimeSpi(id);
	}
	 public abstract void deleteEventsByRelatedAbsence(Long relatedAbsence) throws Exception;
	public abstract void createEvents(DSUser user, Date from, Date to, String description, Long relatedAbsence) throws EdmException, Exception;
	public abstract void createEvents(DSUser user,Date from, Date to,String description) throws Exception;
}