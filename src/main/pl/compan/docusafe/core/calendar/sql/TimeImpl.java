package pl.compan.docusafe.core.calendar.sql;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.calendar.DSTime;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.tasklist.calendar.JSResource;

/**
 * @author Marcin W
 */
public class TimeImpl extends DSTime {
    private static final Logger log = LoggerFactory.getLogger(TimeImpl.class);

	public final static int MAX_PERIOD_EVENTS = 100;

	private Long id;

	private DSUser creator;
	private Long relatedAbsence;
	
	private Set<RejectionReason> rejectionReasons;
	private Set<CalendarEvent> calendarEvents;

    /**
     * Wersja (pierwsza wersja revision=0, serCalendarEvents inkrementuje o 1) dla standardu iCal
     */
    private int sequence = -1;
    
    public static TimeImpl find(long id) throws EdmException {
        return (TimeImpl) DSApi.context().session().get(TimeImpl.class, id);
    }

	public void create() throws Exception, EdmException
	{
		LoggerFactory.getLogger("kamil").debug("create()");
        DSApi.context().session().save(this);
        DSApi.context().session().flush();
        log.info("TimeImpl#create");
	}
	
	/**Dodaje eventy na podstawie danych z formularza zwraca id eventu zalogowanego uzytkownika
     * @param jsEventId id eventu : ==null => create, != null => update
	 * @param owner */
	public Long setCalendarEvent(List<JSResource> resources,
                                 Date startTime,
                                 Date endTime,
                                 String description,
                                 String place,
                                 boolean allDay,
                                 JSResource owner,
                                 boolean periodic,
                                 int periodKind,
                                 int periodLength,
                                 Date periodEnd,
                                 Long jsEventId,
                                 boolean periodicDetached) throws Exception
	{
		Logger kl = LoggerFactory.getLogger("kamil");
        sequence++;
		long loginUserEventId = 0;
		String principialName = DSApi.context().getPrincipalName();
		kl.debug("timeCalendarBinders: " + calendarEvents);
		if(calendarEvents == null)
			calendarEvents = new HashSet<CalendarEvent>();
		
		// szukamy zdarzenia cyklicznego, ktore edytujemy
		CalendarEvent primaryPeriodEvent = null;
		for(CalendarEvent calEvent : calendarEvents) {
			if(calEvent.getId().equals(jsEventId)) {
				primaryPeriodEvent = calEvent;
				break;
			}
		}
		
		kl.debug("RESOURCES: " + resources);
		for (JSResource jsr : resources)
		{
			CalendarEvent calendarEvent = new CalendarEvent(jsr,id);
			kl.debug("trying to add TimeCalendarBinder: " + calendarEvent);
			/**Sprawdzam czy jest juz event dla tego resourca*/
			CalendarEvent orgCalendarEvent = calendarEvent.isContains(calendarEvents);
			
			if(periodic && jsEventId != null) {
				calendarEvent.setId(jsEventId);
				calendarEvent.setPeriodIndex(primaryPeriodEvent.getPeriodIndex());
				
				// je�eli zmieniamy zdarzenie ze zwyk�ego na zdarzenie cykliczne
				if(calendarEvent.isPeriodic() == false) {
					calendarEvent.setPeriodIndex(0L);
					for(CalendarEvent calEvent : calendarEvents) {
						calEvent.setPeriodIndex(0L);
					}
				}

				// znajdujemy odpowiedni event, je�li uaktualniamy wydarzenie cykliczne
                //orgCalendarEvent = getParentEventByResource(jsr);
				Set<CalendarEvent> resEvents = calendarEvent.isContainsMany(calendarEvents);
				
				// to zdarzenie mo�e by� zar�wno rootem wydarze� cyklicznych
				// jak wydarzeniem zale�nym
				orgCalendarEvent = calendarEvent.isContainsByPeriodIndex(resEvents);

                kl.debug("Original cyclic event {}", orgCalendarEvent != null ? orgCalendarEvent.getId() : "NULL");
			}

			if(kl.isDebugEnabled()) {
				Set<CalendarEvent> currentResEvents = new HashSet<CalendarEvent>();
				for(CalendarEvent resCalEvent : calendarEvents) {
					if(resCalEvent.getCalendarId().equals(jsr.getCalendarId())) {
						currentResEvents.add(resCalEvent);
					}
				}
				kl.debug("For " + jsr.getCn() + " there are events: " + currentResEvents);
			}
			kl.debug("The EV-Object is: " + orgCalendarEvent);
			if( orgCalendarEvent == null) {
                loginUserEventId = createEvent(startTime, endTime, description, place, allDay, periodic, periodKind,
                        periodLength, periodEnd, kl, loginUserEventId, jsr, calendarEvent);
			} else {
				if((owner != null && principialName.equals(owner.getCn())) 	// je�li zalogowany user jest w�a�cicielem kalendarza
				    || principialName.equals(jsr.getCn())				   	// je�li zalogowany user znajduje si� w zasobach 
				    || (CalendarFactory.ANY_RESOURCE_TYPE.equals(jsr.getType()))) // je�li typ zasobu != u�ytkownik
				{
					orgCalendarEvent.setStartTime(startTime);
					orgCalendarEvent.setEndTime(endTime);
					orgCalendarEvent.setAllDay(allDay);
				}
				orgCalendarEvent.setDescription(description);
				orgCalendarEvent.setPlace(place);
				
				// Je�li wydarzenie cykliczne
				if(periodic && !periodicDetached) {
					updatePeriodicEvent(orgCalendarEvent, startTime, endTime, periodic, periodKind, periodLength, periodEnd);
				}
                orgCalendarEvent.update();
				if(principialName.equals(jsr.getCn())){
					loginUserEventId = orgCalendarEvent.getId();
                }
			}
		}
		for (CalendarEvent trb : new ArrayList<CalendarEvent>(calendarEvents))
		{
			kl.debug("checking TimeCalendarBinder " + trb);
			if(! trb.isContains(resources,this.id))
			{
				kl.debug("trying to remove TimeCalendarBinder" + trb);
				trb.delete();
				calendarEvents.remove(trb);
				kl.debug(trb + " removed!");
			} else {
				kl.debug(trb + " is in set");
			}
		}
		/**Jesli z jakiegos powodu nie ma uzytkonika zalogowanego w evencie*/
		if(loginUserEventId < 1 && calendarEvents.iterator().hasNext())
			loginUserEventId = calendarEvents.iterator().next().getId();
		return loginUserEventId;
	}
	
	private void updatePeriodicEvent(CalendarEvent orgCalendarEvent, Date startTime, Date endTime, 
			boolean periodic, int periodKind, int periodLength, Date periodEnd) throws Exception {
		Logger kl = LoggerFactory.getLogger("kamil");
		Criteria crit = DSApi.context().session().createCriteria(CalendarEvent.class);
		LogicalExpression matchId = Restrictions.and(
				Restrictions.eq("periodParentId", orgCalendarEvent.getPeriodParentId()), 
				Restrictions.ne("id", orgCalendarEvent.getId()));
		crit.add(Restrictions.and(matchId,
				Restrictions.gt("startTime", orgCalendarEvent.getStartTime()))); // modyfikujemy tylko wydarzenia w przysz�o�ci
		List<CalendarEvent> periodEvents = (List<CalendarEvent>)crit.list();
		
		// Przeliczamy na nowo eventy
		//orgCalendarEvent.setPeriodIndex(periodIndexStart);
		orgCalendarEvent.setPeriodic(periodic);
		orgCalendarEvent.setPeriodKind(periodKind);
		orgCalendarEvent.setPeriodLength(periodLength);
		orgCalendarEvent.setPeriodEnd(periodEnd);
			
		List<CalendarEvent> calcPeriodEvents = orgCalendarEvent.calculatePeriodEvents();
		// uaktualniamy eventy, kt�re znajduj� si� w bazie
		int i = 0;
		CalendarEvent newCalEvent = null;
		for(CalendarEvent calEvent : periodEvents) {
			try {
				newCalEvent = calcPeriodEvents.get(i);
			} catch(IndexOutOfBoundsException e) {
				break;
			}
			calEvent.updateProperties(newCalEvent);
			calEvent.setStartTime(newCalEvent.getStartTime());
			calEvent.setEndTime(newCalEvent.getEndTime());
			calEvent.setCalendar(orgCalendarEvent.getCalendar());
			calEvent.setCalendarId(orgCalendarEvent.getCalendarId());
			calEvent.setTimeId(orgCalendarEvent.getTimeId());
			i ++;
		}
		kl.debug("dbEvents: " + periodEvents.size() + ", calcEvents: " + calcPeriodEvents.size());
		if(periodEvents.size() < calcPeriodEvents.size()) { // musimy doda� nowe eventy do bazy
			calcPeriodEvents = calcPeriodEvents.subList(i, calcPeriodEvents.size());
			for(CalendarEvent calEvent : calcPeriodEvents) {
				calEvent.setCalendar(orgCalendarEvent.getCalendar());
				calEvent.setCalendarId(orgCalendarEvent.getCalendarId());
				calEvent.setTimeId(orgCalendarEvent.getTimeId());
				calEvent.setPeriodParentId(orgCalendarEvent.getPeriodParentId());
				kl.debug("Adding event");
				calEvent.create(false);
				calendarEvents.add(calEvent);
			}
		} else if(periodEvents.size() > calcPeriodEvents.size()) { // usuwamy eventy > periodEnd
			periodEvents = periodEvents.subList(calcPeriodEvents.size(), periodEvents.size());
			for(CalendarEvent calEvent : periodEvents) {
				kl.debug("Removing event " + calEvent.getId());
				calendarEvents.remove(calEvent);
				calEvent.setRemoved(true);
				calendarEvents.add(calEvent);
				
				if(kl.isDebugEnabled()) {
					kl.debug("Removing event #{} {}", calEvent.getId(), calEvent.isRemoved());
				}
				calEvent.delete();
			}
		}
	}

    private long createEvent(Date startTime, Date endTime, String description, String place, boolean allDay, boolean periodic, int periodKind, int periodLength, Date periodEnd, Logger kl, long loginUserEventId, JSResource jsr, CalendarEvent calendarEvent) throws Exception {
        calendarEvent.setAllDay(allDay);
        calendarEvent.setDescription(description);
        calendarEvent.setEndTime(endTime);
        calendarEvent.setPlace(place);
        calendarEvent.setStartTime(startTime);
        calendarEvent.setPeriodic(periodic);
        calendarEvent.setPeriodKind(periodKind);
        calendarEvent.setPeriodLength(periodLength);
        calendarEvent.setPeriodEnd(periodEnd);
        calendarEvent.create();
        calendarEvents.add(calendarEvent);
        if(periodic) {
        	calendarEvent.setPeriodParentId(calendarEvent.getId()); // to jest zdarzenie - w�a�ciciel
        	if(calendarEvent.getPeriodIndex() == null)
        		calendarEvent.setPeriodIndex(0L);
        	updatePeriodicEvent(calendarEvent, startTime, endTime, periodic, periodKind, periodLength, periodEnd);
            /*calendarEvent.setPeriodParentId(calendarEvent.getId());
            List<CalendarEvent> periodEvents = calendarEvent.calculatePeriodEvents();
            for(CalendarEvent calEvent : periodEvents) {
                calEvent.setPeriodParentId(calendarEvent.getId());
                calEvent.setTimeId(calendarEvent.getTimeId());
                calEvent.setCalendarId(calendarEvent.getCalendarId());
                calEvent.create();
            }*/
        }
        //calendarEvents.add(calendarEvent);
        kl.debug(calendarEvent + " added!");
        if(DSApi.context().getPrincipalName().equals(jsr.getCn()))
            loginUserEventId = calendarEvent.getId();
        return loginUserEventId;
    }

    private CalendarEvent getParentEventByResource(JSResource jsres) {
    	Logger kl = LoggerFactory.getLogger("kamil");
    	Long resourceCalendarId = jsres.getCalendarId();
    	
    	for(CalendarEvent evt : calendarEvents) {
    		if(resourceCalendarId.equals(evt.getCalendarId()) && evt.isParentPeriod()) {
    			kl.debug("PeriodParentId for {} is {}", jsres.getCn(), evt.getPeriodParentId());
    			return evt;
    		}
    	}
        
        return null;
    }
    
    public void addRejectionReasons(RejectionReason rejectionReason)
	{
		if(this.rejectionReasons == null)
			this.rejectionReasons = new HashSet<RejectionReason>();
		this.rejectionReasons.remove(rejectionReason);
		this.rejectionReasons.add(rejectionReason);
	}
	
	public void removeRejectionReason(RejectionReason rr) {
		if(this.rejectionReasons != null)
			this.rejectionReasons.remove(rr);
	}
	
	public void delete() throws EdmException
    {
        log.info("TimeImpl#delete:{}", getId());
        try
        {
        	for (CalendarEvent ce : getCalendarEvents())
			{
				ce.delete();
			}
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmException("Blad usuniecia wpisu. "+e.getMessage());
        }
    }
	
	public Set<RejectionReason> getRejectionReasonsByPeriodIndex(Long periodIndex) {
		Set<RejectionReason> reasons = new HashSet<RejectionReason>();
		for(RejectionReason rr : this.rejectionReasons) {
			if(rr.getPeriodIndex() != null && rr.getPeriodIndex().equals(periodIndex)) {
				reasons.add(rr);
			}
		}
		
		return reasons;
	}

    public Set<CalendarEvent> getCalendarEvents() {
		return calendarEvents;
	}

	public void setCalendarEvents(Set<CalendarEvent> calendarEvents) {
		this.calendarEvents = calendarEvents;
	}

	public Set<RejectionReason> getRejectionReasons() {
		return rejectionReasons;
	}

	public Long getRelatedAbsence() {
		return relatedAbsence;
	}

	public void setRelatedAbsence(Long relatedAbsence) {
		this.relatedAbsence = relatedAbsence;
	}
	
	public Long getId() {
		return id;
	}

	protected void setId(Long id) {
		this.id = id;
	}

	public DSUser getCreator() {
		return creator;
	}

	public void setCreator(DSUser creator) {
		this.creator = creator;
	}

	@Override
	public boolean canModify(DSUser user) {
		return false;
	}

    public int getSequence() {
        return sequence;
    }
}
