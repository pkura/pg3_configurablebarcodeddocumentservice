package pl.compan.docusafe.core.calendar.sql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.Calendar;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Powi�zanie kalendarza z uzytkownikami ktorzy go widza
 */
public class UserCalendarBinder implements Serializable
{
	private static final Logger log = LoggerFactory.getLogger(UserCalendarBinder.class);

	private Long calendarId;
	private String username;
	private Integer permissionType;
	private Boolean show;
	private Boolean disable;
	private Calendar calendar;
	private DSUser user;
	private Integer colorId;
	private Integer posn;

	/**
	 * Pobiera wszystkie kalendarze udostepnione dla uzytkownika jesli showInList = null,
	 * Jesli showInList = TRUE zwraca tylko wlaczone
	 * jesli showInList = FALSE zwraca tylko wylaczone
	 * Jesli nie ma zadnego to dodaje mu kalendarz.
	 * @param username
	 * @param showInList
	 * @return
	 * @throws EdmException
	 */
	public static List<UserCalendarBinder> getUserCalendars(String username, Boolean showInList)
	{
        try
        {
        	log.info("szukamy kalendarzy dla uzytkownika: " + username);
            Criteria c = DSApi.context().session().createCriteria(UserCalendarBinder.class);
            c.add(Restrictions.like("username",username).ignoreCase());
            c.addOrder(Order.asc("posn"));
            if(showInList != null)
            {
            	c.add(Restrictions.eq("show",showInList));
            }
            List<UserCalendarBinder> ucbs = c.list();
            log.info("Ilosc znalezionych kalendarzy: " + ucbs.size());
            if(ucbs == null || ucbs.size() < 1)
            {
            	boolean openContext = false;
        		if(!DSApi.context().isTransactionOpen())
        		{
        			DSApi.context().begin();
        			openContext = true;
        		}
        		Calendar cal = new Calendar();
        		cal.setName(DSUser.findByUsername(username).asLastnameFirstname());
        		cal.setOwner(username);
        		cal.setType(Calendar.USER_CALENDAR);
        		UserCalendarBinder ucb = cal.create();
        		if(openContext)
        			DSApi.context().commit();
        		ucbs = new ArrayList<UserCalendarBinder>();
        		ucbs.add(ucb);
            }
			return ucbs;
        }
        catch (Exception e)
		{
			log.debug("B��d wyszukania calendarzy dla "+ username +" ",e);
		}
        return null;
	}

	/**Pobiera liczbe kalendarzy
	 * @return 
	 * @throws EdmException */
	public static int getUserCalendarsCount(String username, Boolean showInList) throws EdmException
	{

        Criteria c = DSApi.context().session().createCriteria(UserCalendarBinder.class);
        c.add(Restrictions.eq("username",username).ignoreCase());
        c.addOrder(Order.asc("posn"));
        if(showInList != null)
        {
        	c.add(Restrictions.eq("show",showInList));
        }
        List<UserCalendarBinder> ucbs = c.list();
        if(ucbs == null)
        	return ucbs.size();
		return 0;
	}
	
	/**
	 * Pobiera liste komu jest ten kalendarz udost�pniony
	 */
	public static List<UserCalendarBinder> getShares(Long calendarId)
	{
		return getShares(calendarId,null);
	}

	/**
	 * Pobiera liste komu jest ten kalendarz udost�pniony na prawach permissionType
	 * Jesli permissionType is null pobiera wszystkie
	 */
	public static List<UserCalendarBinder> getShares(Long calendarId,Integer permissionType)
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(UserCalendarBinder.class);
            c.add(Restrictions.eq("calendarId",calendarId));
            if(permissionType != null)
            	c.add(Restrictions.eq("permissionType",permissionType));
            List ucbs = c.list();
			return ucbs;
        }
        catch (Exception e)
		{
			log.debug("B��d wyszukania calendarzy dla "+ calendarId +" ",e);
		}
        return null;
	}

	private void loadUser() throws EdmException
	{
		if(this.user == null || (this.user.getName() != null && !this.user.getName().equals(this.username)))
		{
			this.user = DSUser.findByUsername(username);
		}
	}

	public static void initUsers(List<UserCalendarBinder> ucbs) throws EdmException
	{
		if(ucbs == null)
			return;
		for (UserCalendarBinder ucb : ucbs)
		{
			ucb.loadUser();
		}
	}


	private void loadCalendar() throws EdmException
	{
		if(this.calendar == null || (this.calendar.getId() != null && !this.calendar.getId().equals(this.calendarId)))
		{
			this.calendar = DSApi.context().load(Calendar.class, calendarId);
			this.calendar.getTimeResource().getCn();
		}
	}



	public static void initCalendars(List<UserCalendarBinder> ucbs) throws EdmException
	{
		if(ucbs == null)
			return;
		for (UserCalendarBinder ucb : ucbs)
		{
			ucb.loadCalendar();
		}
	}

	public static UserCalendarBinder find(Long calendarId, String username) throws EdmException
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(UserCalendarBinder.class);
            c.add(Restrictions.eq("username",username).ignoreCase());
            c.add(Restrictions.eq("calendarId",calendarId));
            List<UserCalendarBinder> ucbs = c.list();
            if(ucbs != null && ucbs.size() > 0)
            	return ucbs.get(0);
        }
        catch (Exception e)
		{
			throw new EdmException("B��d wyszukania kalendarza "+calendarId+" "+username);
		}
        return null;
	}
	
	public static List<UserCalendarBinder> findAll(Long calendarId) throws EdmException
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(UserCalendarBinder.class);
            c.add(Restrictions.eq("calendarId",calendarId));
            return c.list();
        }
        catch (Exception e)
		{
			throw new EdmException("B��d wyszukania kalendarza "+calendarId);
		}
	}


    /**
     * Usuwa powi�zanie
     * @throws EdmException
     */
    public void delete() throws Exception
    {
    	try
    	{
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
    	}
    	catch (Throwable e) {
			log.error(e.getMessage(),e);
		}
    }

    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try
        {
        	if(this.colorId == null)
        	{
        		Random r = new Random();
        		this.colorId = r.nextInt(12);
        	}
        	if(posn == null)
        	{
        		posn = UserCalendarBinder.getUserCalendarsCount(username, null)+1;
        	}
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            log.error(e.getMessage(),e);
            throw new EdmException("Blad dodania kalendarza "+e.getMessage());
        }
    }

	public Integer getPermissionType() {
		return permissionType;
	}

	public void setPermissionType(Integer permissionType) {
		this.permissionType = permissionType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Boolean getShow() {
		return show;
	}

	public void setShow(Boolean show) {
		this.show = show;
	}


	public DSUser getUser() {
		return user;
	}


	public void setUser(DSUser user) {
		this.user = user;
	}


	public Long getCalendarId() {
		return calendarId;
	}


	public void setCalendarId(Long calendarId) {
		this.calendarId = calendarId;
	}


	public Calendar getCalendar() 
	{
		try
		{
			if(this.calendarId != null && ( this.calendar == null || (this.calendar.getId() != null && !this.calendar.getId().equals(this.calendarId))))
			{
				this.calendar = DSApi.context().load(Calendar.class, calendarId);
				this.calendar.getTimeResource();
			}
		}catch (Exception e) {
			log.error("B�ad inicjacji kalendarza",e);
		}
		return calendar;
	}

	public void setColorId(Integer colorId) {
		this.colorId = colorId;
	}

	public Integer getColorId() {
		return colorId;
	}

	public Integer getPosn() {
		return posn;
	}

	public void setPosn(Integer posn) {
		this.posn = posn;
	}

	public Boolean getDisable() {
		return disable;
	}

	public void setDisable(Boolean disable) {
		this.disable = disable;
	}
}
