package pl.compan.docusafe.core.calendar.sql;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.TextUtils;

import com.google.common.base.Objects;

/**
 * Klasa RejectionReasons.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class RejectionReason
{
	private Long id;
	private String  username;
	private String reason;
	private Long timeId;
	private Long periodIndex;
	
	public void create() throws Exception, EdmException {
		DSApi.context().session().save(this);
		DSApi.context().session().flush();
	}
	
	@Override
	public int hashCode()
	{
		if(periodIndex == null)
			return (this.username != null ? this.username.hashCode() : 0);
		return Objects.hashCode(this.username, this.periodIndex);
	}

	@Override
	public boolean equals(Object o) {
		return o.getClass().equals(this.getClass()) && o.hashCode() == this.hashCode();
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}


	public Long getTimeId() {
		return timeId;
	}


	public void setTimeId(Long timeId) {
		this.timeId = timeId;
	}

	public void convertIsoToUTF() throws Exception {
		reason = TextUtils.isoToUtf(reason);
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}

	public Long getPeriodIndex() {
		return periodIndex;
	}

	public void setPeriodIndex(Long periodIndex) {
		this.periodIndex = periodIndex;
	}
}
