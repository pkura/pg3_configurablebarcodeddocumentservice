/*
 * Created on 2007-02-12
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pl.compan.docusafe.core.calendar.sql;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.calendar.Calendar;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.calendar.DSTime;
import pl.compan.docusafe.core.calendar.TimeResource;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.web.office.tasklist.calendar.JSResource;

/**
 * @author Marcin W
 *
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class SqlCalendarFactory extends CalendarFactory {

	public boolean isLdap() {
		return false;
	}

	public int getTimeCount() throws EdmException {
		try {
			Query query = DSApi.context().session().createQuery(
					"select count(*) from u in class "
							+ TimeImpl.class.getName());
			List list = query.list();
			return ((Long) list.get(0)).intValue();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	protected void deleteTimeSpi(Long id) throws EdmException {
		Persister.delete(DSApi.context().session().load(TimeImpl.class, id));
	}

	protected DSTime[] getTimesSpi() throws EdmException {
		try {
			List<DSTime> list = (List<DSTime>) DSApi.context().classicSession().find(
					"from u in class " + TimeImpl.class.getName());
			return list.toArray(new DSTime[list.size()]);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	@Override
	public TimeResource getResourceByCnAndType(String cn, String type) throws EdmException
	{
		if(CalendarFactory.DSUSER_RESOURCE_TYPE.equals(type))
		{
			return DSUser.findByUsername(cn);
		}
		else if(CalendarFactory.ANY_RESOURCE_TYPE.equals(type))
		{
			return null;
		}
		else
		{
			return DSApi.context().getDSUser();
		}
	}

	@Override
	public List<JSResource> getAllResources() throws EdmException
	{
		ArrayList<JSResource> trs = new ArrayList<JSResource>();
		for (AnyResource ar : AnyResource.list())
		{
			JSResource jsuser = new JSResource(ar);
			jsuser.setCalendarId(ar.getCalendarId());
			trs.add(jsuser);
		}
		return trs;
	}
	
	public static Long getDefCal(String cn) 
	{
		Calendar cal = Calendar.getDefaultCalendar(cn);
		if(cal != null)
			return cal.getId();
		return null;
	}
	
	public static final CacheLoader<String, Long> defaultCalendars =
			new CacheLoader<String, Long>() {
			public Long load(String key) {
			return getDefCal(key);
			}
		};
		
		public static final	LoadingCache<String, Long> cache =
				CacheBuilder.newBuilder()
				.build(defaultCalendars);
	
    /**
     * Zwraca domyslny kalendarz
     * @return
     * @throws EdmException
     */
    public static Long getDefCalCache(String cn) throws EdmException {
        try {
            return cache.get(cn);
        } catch (ExecutionException e) {
            throw new EdmException(e);
        }
    }

	
	/**
	 * Na szybko cache przed szkoleniem, do przerobienia 
	 */
//	private static HashMap<String,Long> defaultCalendars = new HashMap<String, Long>();
	
	@Override
	public List<JSResource> getAllUsers() throws EdmException
	{
		ArrayList<JSResource> trs = new ArrayList<JSResource>();
		List<DSUser> dsusers = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
		trs.add(new JSResource(DSApi.context().getDSUser()));
		trs.get(0).setCalendarId(Calendar.getDefaultCalendar().getId());
		for(DSUser u : dsusers)
		{
			JSResource jsuser = new JSResource(u.getId(), u.getName(), u.getLastname(), u.getFirstname(),DSUSER_RESOURCE_TYPE);
			Long defCalId = getDefCalCache(u.getCn());
			jsuser.setCalendarId(defCalId);
			trs.add(jsuser);
		}
		return trs;
	}
	/**
	 * Tworzy wydarzenia calodniowe w zadanym okresie
	 * @param user
	 * @param from
	 * @param to
	 * @throws Exception
	 */
    public void createEvents(DSUser user,Date from, Date to,String description) throws Exception
    {
    	createEvents(user, from, to, description,null);
    }
    
    public void deleteEventsByRelatedAbsence(Long relatedAbsence) throws Exception
    {
        Criteria c = DSApi.context().session().createCriteria(TimeImpl.class);
        c.add(Restrictions.eq("relatedAbsence", relatedAbsence));
        List<TimeImpl> list = c.list();
        for (TimeImpl impl : list) 
        {
        	impl.delete();
		}
    }

    @Override
	public void createEvents(DSUser user, Date from, Date to, String description, Long relatedAbsence) throws EdmException, Exception 
	{
    	Date tmpDate = from;
    	TimeImpl time = new TimeImpl();
		time.setCreator(user); 
		time.setRelatedAbsence(relatedAbsence);
		time.create();
    	Set<CalendarEvent> events = new HashSet<CalendarEvent>();
    	//powiadomienie powinno by� tylko dla pierwszego. Jesli ju� wy�le to nast�pne ma ola� 
    	boolean createdNotifications = false; 
    	while(to.getTime() >= tmpDate.getTime())
    	{
    		CalendarEvent calendarEvent = new CalendarEvent();
    		calendarEvent.setTimeId(time.getId());
    		calendarEvent.setConfirm(true);
			Calendar cal = Calendar.getDefaultCalendar(user.getName());
			calendarEvent.setCalendarId(cal.getId());
    		calendarEvent.setAllDay(true);
			calendarEvent.setDescription(description);
			calendarEvent.setEndTime(tmpDate);
			calendarEvent.setPlace(" ");
			calendarEvent.setStartTime(tmpDate);
			calendarEvent.create(!createdNotifications);
			createdNotifications= true;
			events.add(calendarEvent);
    		tmpDate = DateUtils.addDays(tmpDate, 1);
    	}
		time.setCalendarEvents(events);
	}
}
