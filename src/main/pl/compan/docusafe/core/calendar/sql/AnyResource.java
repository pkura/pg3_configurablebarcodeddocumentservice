package pl.compan.docusafe.core.calendar.sql;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.calendar.Calendar;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.calendar.TimeResource;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AnyResource implements TimeResource
{
	private static final Logger log = LoggerFactory.getLogger(AnyResource.class);
	private String cn;
	private Long id;
	private String name;
	private Long calendarId;
	private boolean available;
	
	public String getCn() 
	{
		return cn;
	}

	public Long getId() {
		return id;
	}

	public String getPartOneOfName() {
		return "";
	}

	public String getPartTwoOfName() {
		return name;
	}

	public String getType() {
		return CalendarFactory.ANY_RESOURCE_TYPE;
	}
	
	public static List<AnyResource> list() throws EdmException
	{	
		if(AvailabilityManager.isAvailable("kalendarz.ZasobyNiewidoczneANieUsuniete")){
			  Criteria criteria = DSApi.context().session().createCriteria(AnyResource.class);
			  criteria.add(Restrictions.eq("available",true));
			  return criteria.list();
		}
        
		return Finder.list(AnyResource.class, "id");
	}
	/**
	 * 
	 * @return
	 * @throws EdmException
	 * Nie sprawdza dostepnosci kalendarza nawet jesli jes wlaczony adds:kalendarz.ZasobyNiewidoczneANieUsuniete
	 * 
	 */
	public static List<AnyResource> listAll() throws EdmException
	{	
		return Finder.list(AnyResource.class, "id");
	}

	
    /**
     * Usuwa zasob
     * @throws Exception 
     */
	public void delete() throws Exception 
	{
		if(AvailabilityManager.isAvailable("kalendarz.ZasobyNiewidoczneANieUsuniete")){
			this.available=false;
			DSApi.context().session().save(this);
			DSApi.context().session().flush();
		}
		else{
			DSApi.context().session().delete(this);
			DSApi.context().session().flush();
			if(this.calendarId != null)
			{
				Calendar.find(calendarId).delete();
			}
		}
	}
	
	public static AnyResource findByCn(String cn)
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(AnyResource.class);
            
            c.add(Restrictions.eq("cn",cn));
            List<AnyResource> ucbs = c.list();
            if(ucbs != null && ucbs.size() > 0)
            	return ucbs.get(0);
        }
        catch (Exception e) 
		{
			log.error("B��d wyszukania TimeCalendarBinder",e);
		}
        return null;
	}
	public static List<AnyResource> findByName(String name)
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(AnyResource.class);
            
            c.add(Restrictions.eq("name",name));
            List<AnyResource> ucbs = c.list();
            if(ucbs != null && ucbs.size() > 0)
            	return ucbs;
        }
        catch (Exception e) 
		{
			log.error("B��d wyszukania TimeCalendarBinder",e);
		}
        return null;
	}

	public Long getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(Long calendarId) {
		this.calendarId = calendarId;
	}

	public String getName() {
		return name;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	
    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error(e.getMessage(),e);
            throw new EdmException("Blad dodania zasobu "+e.getMessage());
        }
    }

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
}
