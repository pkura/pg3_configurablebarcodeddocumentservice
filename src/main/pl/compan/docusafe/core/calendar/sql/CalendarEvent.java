package pl.compan.docusafe.core.calendar.sql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.calendar.Calendar;
import pl.compan.docusafe.core.calendar.TimeResource;
import pl.compan.docusafe.core.news.NotificationsManager;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.tasklist.calendar.JSResource;

public class CalendarEvent implements Serializable
{
	private static final Logger log = LoggerFactory.getLogger(CalendarEvent.class);
	private Long id;
	private Long timeId;
	private Long calendarId;
    private boolean confirm;
    private Calendar calendar;

	private Date startTime;
	private Date endTime;

	private String place;
	/**
	 * Event trwaj�cy ca�y dzie� - domy�lnie false
	 */
	private boolean allDay;

	private String description;
	/**
	 *  Wydarzenie cykliczne.
	 */
	private boolean periodic = false;
	/**
	 * D�ugo�� interwa�u (nale�y te� okre�lic <code>periodKind</code>).
	 */
	private int periodLength = 0;
	/**
	 * Rodzaj interwa�u dla wydarzenia cyklicznego:
	 * <ol start=0>
	 * <li>= dzie�
	 * <li>= tydzie�
	 * <li>= miesi�c
	 * <li>= rok
	 * </ol>
	 */
	private int periodKind = 0;
	/**
	 * Data rozpocz�cia wydarzenia cyklicznego w formacie UnixStamp. <br />
	 * Jest to tylko data (bez czasu), tj. dzie�, miesi�c, rok.
	 */
	private Date periodStart;
	/**
	 * Data zako�czenia wydarzenia cyklicznego w formacie UnixStamp
	 */
	private Date periodEnd;
	/**
	 * Wska�nik na oryginalne wydarzenie cykliczne utworzone przez u�ytkownika
	 */
	private Long periodParentId;
	/**
	 * Indeks (kolejny numer) wydarzenia cyklicznego
	 */
	private Long periodIndex;
	/**
	 * Dla wydarze� cyklicznych - wydarzenie usuni�te
	 */
	private boolean removed;

    public Calendar getCalendar() throws EdmException
    {
    	if(calendar == null)
    		this.calendar = Finder.find(Calendar.class, calendarId);
    	return this.calendar;
	}

	public TimeResource getResource() throws EdmException
    {
		return getCalendar().getTimeResource();
	}

	public CalendarEvent()
    {

    }

    public void delete() throws EdmException
    {
        log.info("CalendarEvent#delete");
    	log.debug("deleting " + this);
        try
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            log.error("Blad usuniecia wpisu");
            throw new EdmException("Blad usuniecia wpisu. "+e.getMessage());
        }
    }
    
    public void create(boolean withNotification) throws Exception, EdmException {
    	log.info("CalendarEvent#create");
		LoggerFactory.getLogger("kamil").debug("create()");
		if(this.allDay)
		{
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(startTime);
			cal.set(GregorianCalendar.HOUR_OF_DAY, 0);
			cal.set(GregorianCalendar.MINUTE, 0);
			this.startTime = cal.getTime();
			cal.set(GregorianCalendar.HOUR_OF_DAY, 23);
			cal.set(GregorianCalendar.MINUTE, 59);
			this.endTime = cal.getTime();
		}
        DSApi.context().session().save(this);
        DSApi.context().session().flush();
        if(withNotification) {
	        try
	        {
	        	/**Powiadamiamy poza u�ytkownikiem, kt�ry dokonuje czynno�ci */
	        	String username = this.getCalendar().getOwner();
	        	if(!DSApi.context().getPrincipalName().equals(username)) {
	                sendCreateNotifications(username);
	            }
			}
	        catch (Exception e) {
				log.error(e.getMessage(),e);
			}
        }
    }

	public void create() throws Exception, EdmException
	{
        create(true);
	}

    /**
     * Pr�buje wys�a� danemu u�ytkownikowi powiadomienie o zdarzeniu
     * @param username zas�b, kt�ry mo�e by� u�ytkownikiem, je�li nie jest metoda nic nie robi
     * @throws EdmException
     */
    private void sendCreateNotifications(String username) throws EdmException {
        try
        {
            DSUser user = DSUser.findByUsername(username);
            //Wiadomo�� mailowa
            EventFactory.registerEvent("immediateTrigger", "CalendarEventNotifier", username + "|" + calendarId + "|" + timeId + "|create", null, new Date());
            StringBuilder sb = new StringBuilder();
            sb.append("Masz nowe zadanie");
            if(periodic) {
            	sb.append(" cykliczne");
            }
            sb.append(" w kalendarzu ( od:");
            sb.append(DateUtils.formatCommonDateTime(startTime));
            sb.append(" do:");
            sb.append(DateUtils.formatCommonDateTime(endTime));
            sb.append(")");

            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(startTime);
            StringBuilder calLink = new StringBuilder();
            calLink.append(NotificationsManager.CALENDAR_LINK);
            calLink.append("&eventId="); calLink.append(this.id);
            // Ustawiamy dat�, �eby kalendarz u�ytkownika by� otwarty w odpowiednim miejscu
            calLink.append("&year="); calLink.append(cal.get(GregorianCalendar.YEAR));
            calLink.append("&month="); calLink.append(cal.get(GregorianCalendar.MONTH)+1);
            calLink.append("&day="); calLink.append(cal.get(GregorianCalendar.DAY_OF_MONTH));

            if(periodic) {
            	NotificationsManager.addNotification(sb.toString(), calLink.toString(), user, 
            		NotificationsManager.OTHER_TYPE | NotificationsManager.CALENDAR_PERIODIC_TYPE);
            } else {
            	NotificationsManager.addNotification(sb.toString(), calLink.toString(), user);
            }
        }
        catch (UserNotFoundException e) {
            //jesli nie jest uzytkownikiem tylko zasobem to nie dodajemy
        }
    }

    public void update() throws EdmException {
        /**Powiadamiamy poza u�ytkownikiem, kt�ry dokonuje czynno�ci */
        String username = this.getCalendar().getOwner();
        if(!DSApi.context().getPrincipalName().equals(username)) {
            sendUpdateNotifications(username);
        }
    }

    /**
     * Pr�buje wys�a� danemu u�ytkownikowi powiadomienie o zdarzeniu
     * @param username zas�b, kt�ry mo�e by� u�ytkownikiem, je�li nie jest metoda nic nie robi
     * @throws EdmException
     */
    private void sendUpdateNotifications(String username) throws EdmException {
        try {
            DSUser user = DSUser.findByUsername(username);
            //Wiadomo�� mailowa
            EventFactory.registerEvent("immediateTrigger", "CalendarEventNotifier", username + "|" + calendarId + "|" + timeId + "|update", null, new Date());
            StringBuilder sb = new StringBuilder();
            sb.append("Zadanie");
            if(periodic) {
            	sb.append(" cykliczne");
            }
            sb.append(" w Twoim kalendarzu zosta�o zmienione ( od:");
            sb.append(DateUtils.formatCommonDateTime(startTime));
            sb.append(" do:");
            sb.append(DateUtils.formatCommonDateTime(endTime));
            sb.append(")");

            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(startTime);
            StringBuilder calLink = new StringBuilder();
            calLink.append(NotificationsManager.CALENDAR_LINK);
            calLink.append("&eventId=");
            calLink.append(this.id);
            // Ustawiamy dat�, �eby kalendarz u�ytkownika by� otwarty w odpowiednim miejscu
            calLink.append("&year=");
            calLink.append(cal.get(GregorianCalendar.YEAR));
            calLink.append("&month=");
            calLink.append(cal.get(GregorianCalendar.MONTH) + 1);
            calLink.append("&day=");
            calLink.append(cal.get(GregorianCalendar.DAY_OF_MONTH));
            
            if(periodic) {
            	NotificationsManager.addNotification(sb.toString(), calLink.toString(), user, 
            		NotificationsManager.OTHER_TYPE | NotificationsManager.CALENDAR_PERIODIC_TYPE);
            } else {
            	NotificationsManager.addNotification(sb.toString(), calLink.toString(), user);
            }
        } catch (UserNotFoundException e) {
            //jesli nie jest uzytkownikiem tylko zasobem to nie dodajemy
        }
    }

    public CalendarEvent(JSResource jsr, Long id)
	{
		this.timeId = id;
		this.confirm = jsr.isConfirm();
		if(jsr.getCalendarId() != null) {
			this.calendarId = jsr.getCalendarId();
        } else {
			Calendar cal = Calendar.getDefaultCalendar(jsr.getCn());
			if(cal != null)
				this.calendarId = cal.getId();
		}
		log.debug("TimeCalendarBinder created");
	}
    
    /**
     * Sprawdza czy event jest rodzicem wydarze� cyklicznych,
     * tzn. eventem od kt�rego liczone s� zdarzenia cykliczne
     * @return
     */
    public boolean isParentPeriod() {
    	if(this.id != null && this.periodParentId != null) {
    		return this.id.equals(this.periodParentId);
    	}
    	return false;
    }

	public boolean isConfirm() {
		return confirm;
	}
	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}
	public Long getCalendarId() {
		return calendarId;
	}
	public void setCalendarId(Long objectId) {
		this.calendarId = objectId;
	}
	public Long getTimeId() {
		return timeId;
	}
	public void setTimeId(Long timeId) {
		this.timeId = timeId;
	}



	@Override
	public int hashCode()
	{
		return  this.id.hashCode();
	}

	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(this.timeId == null || this.calendarId == null)
			return super.equals(obj);
		if(obj instanceof CalendarEvent)
		{
			return this.id.equals(((CalendarEvent)obj).id);
		}
		return false;
	}
	
	/**
	 * Sprawdza czy eventy maj� takie samo <code>timeId</code> 
	 * i <code>calendarId</code>
	 * @param event
	 * @return
	 */
	public boolean equalsCalendarTime(CalendarEvent event) {
		return this.timeId.equals(event.timeId) && this.calendarId.equals(event.calendarId);
	}
	
	/**Sprawdza czy istnieje taki element jesli tak zwraca orginal*/
	public CalendarEvent isContains(Set<CalendarEvent> calendarEvents)
	{
		if(calendarEvents == null) 
			return null;
		for (CalendarEvent ev : calendarEvents)
		{
			if(ev.equalsCalendarTime(this))
				return ev;
		}
		return null;
	}
	
	/**
	 * Sprawdza czy istnieje ju� element i je�li tak, to zwraca
	 * @param calendarEvents
	 * @return
	 */
	public Set<CalendarEvent> isContainsMany(Set<CalendarEvent> calendarEvents) {
		Set<CalendarEvent> events = new HashSet<CalendarEvent>();
		
		for (CalendarEvent ev : calendarEvents)
		{
			if(ev.equalsCalendarTime(this))
				events.add(ev);
		}
		
		return events;
	}
	
	/**
	 * Sprawdza czy istnieje event por�wnuj�c tylko po <code>periodIndex</code>.
	 * @param calendarEvents
	 * @return
	 */
	public CalendarEvent isContainsByPeriodIndex(Set<CalendarEvent> calendarEvents) {
		for(CalendarEvent ev : calendarEvents) {
			if(ev.equalsCalendarTime(this) && this.periodIndex != null && this.periodIndex.equals(ev.periodIndex)) {
				return ev;
			}
		}
		return null;
	}
	
	public List<CalendarEvent> isContainsPeriodic(Set<CalendarEvent> calendarEvents) {
		if(calendarEvents == null) 
			return null;
		List<CalendarEvent> retEvents = new ArrayList<CalendarEvent>();
		for (CalendarEvent ev : calendarEvents)
		{
			if(ev.periodParentId.equals(this.periodParentId))
				retEvents.add(ev);
		}
		return retEvents;
	}

	public boolean isContains(List<JSResource> resources,Long timeId)
	{
		if(resources == null)
			return false;
		for (JSResource res : resources)
		{
			if(this.equalsJSResources(res,timeId))
				return true;
		}
		return false;
	}

	public boolean equalsJSResources(JSResource res,Long timeId)
	{
		if(this.timeId == null || this.calendarId == null)
			return false;
		return this.calendarId.equals(res.getCalendarId()) && this.timeId.equals(timeId);
	}

	public boolean equalsTimeResource(TimeResource timeResource)
	{
		if(this.timeId == null || this.calendarId == null)
			return false;
		return this.getCalendarId().equals(timeResource.getId()) && this.timeId.equals(timeResource.getType());
	}

	public static CalendarEvent find(Long timeId, Long calendarId)
	{
        try
        {
            Criteria c = DSApi.context().session().createCriteria(CalendarEvent.class);
            c.add(Restrictions.eq("timeId",timeId));
            c.add(Restrictions.eq("calendarId",calendarId));
            List<CalendarEvent> ucbs = c.list();
            if(ucbs != null && ucbs.size() > 0)
            	return ucbs.get(0);
        }
        catch (Exception e)
		{
			log.debug("B��d wyszukania TimeCalendarBinder",e);
		}
        return null;

	}
	
	/**
	 * Ustawienie "bezpiecznych" w�a�ciwo�ci eventu:
	 * <ul>
	 * <li>allDay
	 * <li>confirm
	 * <li>description
	 * <li>place
	 * <li>periodEnd
	 * <li>periodic
	 * <li>periodKind
	 * <li>periodLength
	 * <li>periodParentId
	 * </ul>
	 * @param evt Zdarzenie, z kt�rego zostan� skopiowane w�a�ciwo�ci
	 */
	public void updateProperties(CalendarEvent evt) {
        log.info("CalendarEvent#update");
		this.allDay = evt.allDay;
		this.confirm = evt.confirm;
		this.description = evt.description;
		this.place = evt.place;
		this.periodEnd = evt.periodEnd;
		this.periodic = evt.periodic;
		this.periodKind = evt.periodKind;
		this.periodLength = evt.periodLength;
		this.periodParentId = evt.periodParentId;
	}

	/**
	 * @return Wyliczone zdarzenia cykliczne
	 */
	public List<CalendarEvent> calculatePeriodEvents() {
		long periodIndex = 1;
		if(this.periodIndex != null)
			periodIndex = this.periodIndex + 1;
		List<CalendarEvent> events = new ArrayList<CalendarEvent>();
		java.util.Calendar periodMax = GregorianCalendar.getInstance();
			periodMax.setTime(this.periodEnd); 
			periodMax.set(java.util.Calendar.HOUR_OF_DAY, 24);
			periodMax.set(java.util.Calendar.MINUTE, 59);
			periodMax.set(java.util.Calendar.MILLISECOND, 999);
		java.util.Calendar iterEventStart = GregorianCalendar.getInstance();
			iterEventStart.setTime(this.startTime);
		java.util.Calendar iterEventEnd = GregorianCalendar.getInstance();
			iterEventEnd.setTime(this.endTime);
			
		int i = 0;
		int calIncType = java.util.Calendar.DATE;	// domy�lnie zwi�kszamy dni (periodKind == 0 )
		switch(this.periodKind) {
			case 1:
				calIncType = java.util.Calendar.WEEK_OF_YEAR;
			break;
			case 2:
				calIncType = java.util.Calendar.MONTH;
			break;
			case 3:
				calIncType = java.util.Calendar.YEAR;
			break;
		}
		
		iterEventStart.add(calIncType, this.periodLength); iterEventEnd.add(calIncType, this.periodLength);
		while(i < TimeImpl.MAX_PERIOD_EVENTS && iterEventStart.compareTo(periodMax) <= 0) {
			CalendarEvent event = new CalendarEvent();
			event.updateProperties(this);
			event.setStartTime(iterEventStart.getTime()); event.setEndTime(iterEventEnd.getTime());
			event.setPeriodIndex(periodIndex);
			
			iterEventStart.add(calIncType, this.periodLength);
			iterEventEnd.add(calIncType, this.periodLength);
			i ++;
			periodIndex ++;
			events.add(event);
		}
		// zwracamy zdarzenia > periodMax
		return events;
	}
	
	/**
	 * Zwraca list� zdarze� cyklicznych w przysz�o�ci ��cznie z obecnym zdarzeniem.
	 * Tzn.: zwraca zdarzenia spe�niaj�ce wszystkie warunki:
	 * <ul>
	 * <li>Takie samo timeId, calendarId i periodParentId
	 * <li>periodIndex >= periodIndex zdarzenia
	 * @return Lista znalezionych zdarze� (niepusta - w li�cie znajduje si� przynajmniej obecne wydarzenie).
	 */
	@SuppressWarnings("unchecked")
	public List<CalendarEvent> getFutureEventsInclusive() {
		List<CalendarEvent> events = new ArrayList<CalendarEvent>();
		events.add(this);
		try {
			Criteria c = DSApi.context().session().createCriteria(CalendarEvent.class);
			c.add(Restrictions.eq("timeId", this.timeId));
			c.add(Restrictions.eq("calendarId", this.calendarId));
			c.add(Restrictions.eq("periodParentId", this.periodParentId));
			c.add(Restrictions.gt("periodIndex", this.periodIndex)); // pierwszy element ju� dodali�my
			events.addAll(c.list());
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		
		return events;
	}
	

	@Override
	public String toString() {
		//return "(" + this.id + ", " + this.timeId + ", " + this.startTime + "-" + this.endTime + ")";
		return this.id != null ? this.id + "" : "NULL";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isAllDay() {
		return allDay;
	}

	public void setAllDay(boolean allDay) {
		this.allDay = allDay;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}

	public String getShortDescription(int title_length) 
	{
		return StringUtils.left(description,title_length);
	}

	public void setPeriodic(boolean periodic) {
		this.periodic = periodic;
	}

	public boolean isPeriodic() {
		return periodic;
	}

	public void setPeriodLength(int periodLength) {
		this.periodLength = periodLength;
	}

	public int getPeriodLength() {
		return periodLength;
	}

	public void setPeriodKind(int periodKind) {
		this.periodKind = periodKind;
	}

	public int getPeriodKind() {
		return periodKind;
	}

	public void setPeriodStart(Date periodStart) {
		this.periodStart = periodStart;
	}

	public Date getPeriodStart() {
		return periodStart;
	}

	public void setPeriodEnd(Date periodEnd) {
		this.periodEnd = periodEnd;
	}

	public Date getPeriodEnd() {
		return periodEnd;
	}

	public void setPeriodParentId(Long periodParentId) {
		this.periodParentId = periodParentId;
	}

	public Long getPeriodParentId() {
		return periodParentId;
	}

	public boolean isRemoved() {
		return removed;
	}

	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

	public Long getPeriodIndex() {
		return periodIndex;
	}

	public void setPeriodIndex(Long periodIndex) {
		this.periodIndex = periodIndex;
	}

}
