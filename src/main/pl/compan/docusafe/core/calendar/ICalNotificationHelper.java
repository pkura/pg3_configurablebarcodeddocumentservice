package pl.compan.docusafe.core.calendar;

import net.fortuna.ical4j.model.*;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.*;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.sql.CalendarEvent;
import pl.compan.docusafe.core.calendar.sql.TimeImpl;
import pl.compan.docusafe.events.handlers.CalendarEventNotifier;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa pomocnicza do tworzenia tre�ci plik�w ics (iCalendar)
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class ICalNotificationHelper {
    private final static Logger LOG = LoggerFactory.getLogger(ICalNotificationHelper.class);


    /**
     * Tworzy zdarzenie ical na podstawie zdarzenia kalendarzowego.
     */
    public static String iCalStringFromEvent(CalendarEvent event) throws EdmException {
        net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
        icsCalendar.getProperties().add(new ProdId("-//DocuSafe Calendar//DocuSafe "+ Docusafe.getVersion()+"//PL"));
        icsCalendar.getProperties().add(CalScale.GREGORIAN);

        VEvent vevent;
        if(event.isAllDay()){
            vevent = new VEvent(
                new DateTime(event.getStartTime().getTime()),
                event.getDescription());
        } else {
            vevent = new VEvent(
                new DateTime(event.getStartTime().getTime()),
                new DateTime(event.getEndTime().getTime()),
                event.getDescription());
        }
        vevent.getProperties().add(new Uid(event.getTimeId().toString()));
        vevent.getProperties().add(
                event.getId() == null ? Status.VEVENT_CANCELLED
                : event.isConfirm() ? Status.VEVENT_CONFIRMED : Status.VEVENT_TENTATIVE);

        vevent.getProperties().add(new Sequence(TimeImpl.find(event.getTimeId()).getSequence()));

        icsCalendar.getComponents().add(vevent);

        LOG.info("== icsCalendar == \n{}", icsCalendar);
        return icsCalendar.toString();
    }

    private ICalNotificationHelper(){}
}