package pl.compan.docusafe.core.calendar;


public interface TimeResource 
{
	public abstract Long getId();
	
	public String getPartOneOfName();
	
	public String getPartTwoOfName();
	
	public String getCn();
	
	public String getType();

}
