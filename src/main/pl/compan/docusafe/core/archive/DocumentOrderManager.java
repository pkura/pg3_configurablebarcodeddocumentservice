package pl.compan.docusafe.core.archive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Klasa zarzadzajaca logika wypozyczania dokumentow
 * Budowana na potrzeby na razie na potrzeby prosiki
 * @author wkutyla
 *
 */
public class DocumentOrderManager 
{	
	final static Logger log = LoggerFactory.getLogger(DocumentOrderManager.class);
	
	
	/**
	 * Pelna lista priorytetow - projektujac priorytety i ich aspekty nalezy miec 
	 * na uwadze ze istnieja funkcje gdy powinny byc widoczne te wszystkie priorytety
	 * Dlatego musi istniec lista grupujaca wszystkie priorytety
	 * @return
	 */
	public Map<Integer,String> getFullOrderPrioritets()
	{
		return getOrderPrioritets("full");
	}
	/**
	 * Metoda zwraca liste priorytetow zamowienia
	 * @return
	 */
	public Map<Integer,String> getOrderPrioritets()
	{
		return getOrderPrioritets("default");
	}

	/**
	 * Metoda zwraca liste priorytetow dla zamowienia dla konkretnego aspektu
	 * @param documentAspect
	 * @return
	 */
	public static Map<Integer, String> getOrderPrioritets(String documentAspect)
	{
		if ("full".equalsIgnoreCase(documentAspect))
		{
			return OrderManagerDefs.fullPriorityMap;
		}
		else if ("cok".equalsIgnoreCase(documentAspect))
			return OrderManagerDefs.cokPriorityMap;
		return OrderManagerDefs.defaultPriorityMap;
	}
	
	/**
	 * Mapa statusow - na razie 
	 * Nie zalezy od aspetku
	 * @return
	 */
	public Map<Integer, String> getOrderStatusMap()
	{
		return OrderManagerDefs.statusMap;
	}
	
	/**
	 * Mapa rodzajow wysylki
	 * Byc moze bedzie zalezala od aspektow, na razie tego nie przewidujemy
	 * @return
	 */
	public Map<Integer, String> getOrderDeliveryKindMap()
	{
		return OrderManagerDefs.deliveryKindMap;
	}
	
	/**
	 * Mapa rodzajow zamowienia
	 * Na razie zakladamy ze jest niezalezna od aspektu zamowienia
	 * @return
	 */
	public Map<Integer, String> getOrderKindMap()
	{
		return OrderManagerDefs.orderKindMap;
	}
	
	/**
	 * Nazwa statusu, gdy przekazywany parametr jest Stringiem
	 * @param status
	 * @return
	 */
	public String getOrderStatusAsString(String status)
	{
		Integer stat = Integer.valueOf(status);
		return getOrderStatusAsString(stat);
	}
	/**
	 * Status jako String
	 * @param status
	 * @return
	 */
	public String getOrderStatusAsString(Integer status)
	{
		String resp = this.getOrderStatusMap().get(status);
		if (resp == null)
		{
			resp = "---";
		}
		return resp;
	}
	
	/**
	 * Typ zamowienia jako String
	 * @param kindStr
	 * @return
	 */
	public String getOrderKindAsString(String kindStr)
	{
		Integer kind = Integer.valueOf(kindStr);
		return getOrderKindAsString(kind);
	}
	/**
	 * Typ zamowienia jako String
	 * @param kind
	 * @return
	 */
	public String getOrderKindAsString(Integer kind)
	{
		String resp = this.getOrderKindMap().get(kind);
		if (resp == null)
		{
			resp = "---";
		}
		return resp;
	}
	
	/**
	 * Rodzaj odbioru jako String
	 * @param kindStr
	 * @return
	 */
	public String getOrderDeliveryKindAsString(String kindStr)
	{
		Integer kind = Integer.valueOf(kindStr);
		return this.getOrderDeliveryKindAsString(kind);
	}
	/**
	 * Nazwa rodzaju dostawy
	 * @param kind
	 * @return
	 */
	public String getOrderDeliveryKindAsString(Integer kind)
	{
		String resp = this.getOrderDeliveryKindMap().get(kind);
		
		if (resp == null)
		{
			resp = "---";
		}
		return resp;
	}
}


