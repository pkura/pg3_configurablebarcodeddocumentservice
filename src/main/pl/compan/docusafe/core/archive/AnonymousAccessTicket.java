package pl.compan.docusafe.core.archive;

import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

public class AnonymousAccessTicket 
{

	private Long id;
	private Long documentId;
	private Long attachmentId;
	private Long attachmentRevisionId;
	private String code;
	private Date ctime;
	private Date validTill;
	private Integer accessCount;
	private Integer maxAccess;
	private String creator;
	private String returnMethod;
	
	
	public static AnonymousAccessTicket findByCode(String code) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(AnonymousAccessTicket.class);
		if (code != null)
			criteria.add(Restrictions.eq("code", code));
		else
			throw new EdmException("Code is null");
    	
    	return (AnonymousAccessTicket)criteria.uniqueResult();
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	public Long getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}
	public Long getAttachmentRevisionId() {
		return attachmentRevisionId;
	}
	public void setAttachmentRevisionId(Long attachmentRevisionId) {
		this.attachmentRevisionId = attachmentRevisionId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	public Date getValidTill() {
		return validTill;
	}
	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}
	public Integer getAccessCount() {
		return accessCount;
	}
	public void setAccessCount(Integer accessCount) {
		this.accessCount = accessCount;
	}
	public Integer getMaxAccess() {
		return maxAccess;
	}
	public void setMaxAccess(Integer maxAccess) {
		this.maxAccess = maxAccess;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getReturnMethod() {
		return returnMethod;
	}
	public void setReturnMethod(String returnMethod) {
		this.returnMethod = returnMethod;
	}
	
	
	
}
