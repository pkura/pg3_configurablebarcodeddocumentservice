package pl.compan.docusafe.core.archive;

import java.util.*;
/**
 * Definicje dla OrderManagera
 * Do czasu zaimplementowania jakiegos bardziej eleganckiego mechamizmu zarzadzania 
 * konfiguracja klasa powinna wystarczyc
 * @author wkutyla
 *
 */
public class OrderManagerDefs  
{
	public static Map<Integer,String> defaultPriorityMap;
	/**
	 * Mapa priorytetow dla COKu
	 */
	public static Map<Integer,String> cokPriorityMap;
	
	/**
	 * Pelna mapa priorytetow - na potrzeby szukajki
	 */
	public static Map<Integer,String> fullPriorityMap;
	/**
	 * Mapa statusow
	 */
	public static Map<Integer,String> statusMap;
	/**
	 * Mapa typow zamowien
	 */
	public static Map<Integer,String> orderKindMap;
	/**
	 * Mapa rodzajow dostawy
	 */
	public static Map<Integer,String> deliveryKindMap;
	
	public static int STATUS_CANCELED = 6;
	public static int STATUS_WMAGAZYNIE = 5;
	
	
	static
	{
		cokPriorityMap = new LinkedHashMap<Integer, String>();
		cokPriorityMap.put(1, "P1 - Krytyczne");
		cokPriorityMap.put(2, "P2 - Wysoki");
		cokPriorityMap.put(3, "P3 - �redni");
		cokPriorityMap.put(4, "P4 - Niski");
		
		fullPriorityMap = new LinkedHashMap<Integer, String>();
		fullPriorityMap.put(1, "P1 - Krytyczne");
		fullPriorityMap.put(2, "P2 - Wysoki/Priorytet");
		fullPriorityMap.put(3, "P3 - �redni/Standard");
		fullPriorityMap.put(4, "P4 - Niski");
		
		defaultPriorityMap = new LinkedHashMap<Integer, String>();
		defaultPriorityMap.put(2, "Ekspres");
		defaultPriorityMap.put(3, "Standard");	
		
		deliveryKindMap = new LinkedHashMap<Integer, String>();
		deliveryKindMap.put(0, "Brak");
		deliveryKindMap.put(10, "Odbi�r w�asny");
		deliveryKindMap.put(20, "Firma kurierska");
		deliveryKindMap.put(30, "Kurier IM");
		deliveryKindMap.put(40, "Poczta Polska");	
		
		statusMap = new LinkedHashMap<Integer, String>();
		statusMap.put(1, "W realizacji");
		statusMap.put(2, "Wys�any");
		statusMap.put(3, "Przyj�ty");
		statusMap.put(4, "Wys�any do IM");
		statusMap.put(STATUS_WMAGAZYNIE, "W magazynie");
		statusMap.put(STATUS_CANCELED, "Anulowane");
		
		orderKindMap = new LinkedHashMap<Integer, String>();
		orderKindMap.put(1, "Skan");
		orderKindMap.put(2, "Orygina�");
		orderKindMap.put(3, "Orygina� + Skan");
		orderKindMap.put(4, "Kopia z potw. zg.");
	}
}
