package pl.compan.docusafe.core.news;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;

/**
 * Klasa opisuje typy aktualno�ci jakie mog� wyst�pi� w systemie. 
 */
public class NewsType implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	/**
	 * Nazwa zak�adki (typu) aktualno�ci.
	 */
	private String name;
	/**
	 * Okre�la czy dany typ jest aktywny.
	 */
	private boolean disabled;
	/**
	 * Czas utworzenia.
	 */
	private Date ctime;
	
	public NewsType() {}
	
	public NewsType(String name) {
		super();
		this.name = name;
	}
	/**
	 * Metoda zwraca typ o podanym id.
	 * @param id - id typu
	 * @return
	 * @throws EdmException
	 */
	public static NewsType find(Long id) throws EdmException
	{
		  return (NewsType)Finder.find(NewsType.class, id);
	}
	
	/**
	 * Metoda wyszukuje wszystkie typy dost�pne w systemie.
	 * @return
	 * @throws EdmException
	 */
	public static List<NewsType> findAll() throws EdmException 
	{
		return Finder.list(NewsType.class, null);
	}
	/**
	 * Metoda wszukuje wszystkie aktywne typy w systemie (z warto�ci� disabled na false).
	 * @return
	 * @throws EdmException
	 */
	public static List<NewsType> findAllActive() throws EdmException 
	{
    	Criteria crit = DSApi.context().session().createCriteria(NewsType.class);
    	crit.add(Restrictions.eq("disabled", false));
    	crit.addOrder(Order.asc("ctime"));
    	return crit.list();
	}
	/**
	 * Utworzenie danego obiektu w bazie danych.
	 * @throws EdmException
	 */
	public void create() throws EdmException 
	{
		 try 
        {
			this.ctime = new Date();
            DSApi.context().session().save(this);
            DSApi.context().session().flush();	       
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isDisabled() {
		return disabled;
	}
	
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
}
