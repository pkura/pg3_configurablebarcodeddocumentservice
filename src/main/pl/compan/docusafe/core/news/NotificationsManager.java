package pl.compan.docusafe.core.news;

import java.util.Calendar;
import java.util.Date;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListCounterManager;
import pl.compan.docusafe.core.office.workflow.snapshot.UserCounter;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
/**
 * Klasa s�u�y do wprowadzania powiadomie� do systemu.
 * Ka�de wprowadzone powiadomienie b�dzie wy�wietlane 
 * na zak�adce aktualno�ci danego u�ytkownika (dodaj�c powiadomienia
 * trzeba zawsze poda� u�ytkownika do kt�rego powiadomienie jest kierowane).
 *
 */
public class NotificationsManager 
{
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(NotificationsManager.class.getPackage().getName(),null);
	private static Logger log = LoggerFactory.getLogger(NotificationsManager.class);
	
	public static final String NOT_LIST_LINK = "/news/notification.action";
	public static final String DOCUMENT_LINK = "/document-archive.action";
	public static final String TASKLIST_LINK = "/office/tasklist/current-user-task-list.action";
	public static final String CALENDAR_LINK = "/office/tasklist/calendar-own.action?daily=true&tab=calendar";
	
	public static final Integer DEFINITION_TYPE = 0;
	public static final Integer TASKLIST_TYPE = 1;
	public static final Integer OTHER_TYPE = 2;
	public static final Integer CHILD_TYPE = 3;
	public static final Integer CALENDAR_PERIODIC_TYPE = 4;
	public static final Integer DELETED = 5;
	
	
	/**
	 * Metoda dodaje powiadomienie dla danego u�ytkownika.
	 * @param documentId - dokument powi�zany z powiadomieniem
	 * @param content - tre�� powiadomienia
	 * @param link - link do powiadomienia
	 * @param user - u�ytkownik do kt�rego kierowane jest powiadomienie
	 * @param creator - tworca powiadomienia
	 * @param notificationDate - data wyswietlenie powiadomienia
	 * @param read - true przeczytano
	 * @param newNotification - true nowe
	 * @throws EdmException
	 */
	public static void addNotification(Long documentId, String content, String link, DSUser user, DSUser creator, Integer type, Date notificationDate, boolean newNotification, boolean read, Long parentId) throws EdmException {
		Notification notification = new Notification(documentId, content, link, user, creator, OTHER_TYPE, notificationDate, newNotification, read, parentId);
		notification.create();
	}
	
	/**
	 * Metoda dodaje powiadomienie dla danego u�ytkownika.
	 * @param content - tre�� powiadomienia
	 * @param link - link do powiadomienia
	 * @param user - u�ytkownik do kt�rego kierowane jest powiadomienie
	 * @throws EdmException
	 */
	public static void addNotification(String content, String link, DSUser user) throws EdmException {
		addNotification(content, link, user,OTHER_TYPE);
	}
	/**
	 * Metoda dodaje powiadomienie dla danego u�ytkownika.
	 * @param content - tre�� powiadomienia
	 * @param link - link do powiadomienia
	 * @param user - u�ytkownik do kt�rego kierowane jest powiadomienie
	 * @throws EdmException
	 */
	public static void addNotification(String content, String link, DSUser user,Integer type) throws EdmException {
		Notification notification = new Notification(content, link, user,type);
		notification.create();
	}
	
	/**
	 * Metoda dodaje powiadomienie dla danego u�ytownika o ilo�ci nowych zada� na li�cie.
	 * @param user - u�ytkownik, do kt�rego kierowane jest powiadomienie {@link DSUser}.
	 * @param newTaskAmount - liczba nowych zada� na li�cie.
	 * @throws EdmException
	 */
	public static void addTasklistNotification(DSUser user,int newTaskAmount) throws EdmException {
		addNotification(sm.getString("NotManTasklist", newTaskAmount), TASKLIST_LINK, user,TASKLIST_TYPE);
	}
	/**
	 * Metoda dodaje powiadomienie dla danego u�ytownika o ilo�ci nowych zada� na li�cie.
	 * @param user - u�ytkownik, do kt�rego kierowane jest powiadomienie.
	 * @param newTaskAmount - liczba nowych zada� na li�cie.
	 * @throws EdmException
	 */
	public static void addTasklistNotification(String username, int newTaskAmount) throws EdmException {
		addTasklistNotification(DSUser.findByUsername(username), newTaskAmount);
	}
	
	public static void addTasklistNotification(String username)
	{
        /**Dodaje powiadomienie na zakladce aktualnosci o nowych zadaniach od ostatniego logowania*/
  		 if(AvailabilityManager.isAvailable("AddNotificationsOnLogin"))
		 {
	 			try
				{
					TaskListCounterManager manager = new TaskListCounterManager(false);
					manager.initCounter();
					Long time = DSApi.context().systemPreferences().node("taskListNotification").getLong(username, 0);
					System.out.println(time);
					UserCounter uc = manager.getUserCounter(username,time);
					Integer tasks = uc.getNewNotAcceptedTask();
					if(tasks > 0)
					{
						Notification.dropByUsertype(DSUser.findByUsername(username), TASKLIST_TYPE);
						NotificationsManager.addTasklistNotification(username, tasks);
					}
					DSApi.context().systemPreferences().node("taskListNotification").putLong(username, new Date().getTime());
				}
				catch (Exception e) 
				{
					log.error(e.getMessage(),e);
				}
		 }
	}
}
