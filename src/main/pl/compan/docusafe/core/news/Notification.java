package pl.compan.docusafe.core.news;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.users.DSUser;
/**
 * Klasa przechowuj�ca powiadomienia wy�wietlane
 * dla danego u�ytkownika w aktualno�ciach.
 */
public class Notification implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(Notification.class);
	
	private Long id;
	
	private Long documentId;
	/**
	 * Tre�� powiadomienia.
	 */
	private String content;
	/**
	 * Link do wiadomo�ci (odno�nie danego powiadomienia).
	 */
	private String link;
	/**
	 * U�ytkownik, dla kt�rego przekazane jest powiadomienie.
	 */
	private DSUser user;
	private DSUser creator;
	private Date ctime;
	/**
	 * Konstruktor bezargumentowy.
	 */
	private Integer type;
	
	private Date notificationDate;
	
	// czy przeczytane
	private boolean read;
	
	private boolean newNotification;
	
	private boolean delete;
	private boolean cantEdit = true;
	
	private Long parentId;
	
	public Notification(){};
	
	/**
	 * Konstruktor tworz�cy nowe powiadomienie.
	 * @param content - tre�� powiadomienia
	 * @param link - link do powiadomienia
	 * @param user - u�ytkownik, do kt�rego kierowane jest powiadomienie
	 */
	public Notification(String content, String link, DSUser user, Integer type) {
		this.content=content;
		this.link=link;
		this.user=user;
		this.type = type;
	}
	
	public Notification(Long documentId, String content, String link, DSUser user, DSUser creator, Integer type, Date notificationDate, boolean newNotification, boolean read, Long parentId) {
		this.documentId = documentId;
		this.content=content;
		this.link=link;
		this.creator=creator;
		this.user=user;
		this.type = type;
		this.notificationDate = notificationDate;
		this.newNotification = newNotification;
		this.read = read;
		this.parentId = parentId;
	}
	
	public static Notification find(Long id) throws EdmException
	{
		  return (Notification)Finder.find(Notification.class, id);
	}
	
	/**
	 * Metoda zwraca list� powiadomie� dla danego u�ytkownika.
	 * @param user - u�ytkownik
	 * @return
	 * @throws EdmException
	 */
	public static List<Notification> findByUser(DSUser user) throws EdmException 
	{
    	Criteria crit = DSApi.context().session().createCriteria(Notification.class);
    	// sortowanie po dacie powiadomienia
    	crit.addOrder(Order.asc("notificationDate"));
    	// powiadomienia odpowiedniego u�ytkownika
    	crit.add(Restrictions.eq("user", user));
    	return crit.list();
	}
	
	/**
	 * Metoda zwraca list� nowych powiadomie� dla danego u�ytkownika.
	 * @param user - u�ytkownik
	 * @return
	 * @throws EdmException
	 */
	public static List<Notification> findNewNotificationByUser(DSUser user) throws EdmException 
	{
    	Criteria crit = DSApi.context().session().createCriteria(Notification.class);
    	// sortowanie po dacie powiadomienia
    	crit.addOrder(Order.asc("notificationDate"));
    	crit.add(Restrictions.eq("newNotification", true));
    	// powiadomienia odpowiedniego u�ytkownika
    	crit.add(Restrictions.eq("user", user));
    	crit.add(Restrictions.eq("creator", user));
    	return crit.list();
	}
	
	/**
	 * Metoda zwraca list� otrzymanych powiadomie� dla danego u�ytkownika, kt�re przekroczy�y dat�
	 * @param user - u�ytkownik
	 * @return
	 * @throws EdmException
	 */
	public static List<Notification> findRecivedNotificationByUser(DSUser user) throws EdmException 
	{
    	Criteria crit = DSApi.context().session().createCriteria(Notification.class);
    	// sortowanie po dacie powiadomienia
    	crit.addOrder(Order.desc("notificationDate"));
    	// powiadomienia odpowiedniego u�ytkownika
    	crit.add(Restrictions.eq("user", user));
        crit.add(Restrictions.lt("notificationDate", new Date()));
    	return crit.list();
	}
	
	/**
	 * Metoda zwraca list� w�asnych powiadomie� dla danego u�ytkownika.
	 *
     * @param user - u�ytkownik
     * @param sortField
     *@param ascending @return
	 * @throws EdmException
	 */
	public static List<Notification> findOwnNotificationByUser(DSUser user, String sortField, boolean ascending) throws EdmException
	{
    	Criteria crit = DSApi.context().session().createCriteria(Notification.class);
    	// sortowanie po dacie powiadomienia
        if (sortField != null)
        {
            if (ascending)
                crit.addOrder(Order.asc(sortField));
            else
                crit.addOrder(Order.desc(sortField));
        }
        else
    	    crit.addOrder(Order.asc("notificationDate"));
    	// powiadomienia odpowiedniego u�ytkownika
    	crit.add(Restrictions.eq("creator", user));
    	//type - 0 definicja powiadomienia
    	crit.add(Restrictions.eq("type", 0));
    	return crit.list();
	}
	
	/**
	 * Metoda zwraca list� aktualnych nie przeczytanych powiadomie� dla danego u�ytkownika (kt�re przekroczy�y dat�).
	 * @param user - u�ytkownik
	 * @return
	 * @throws EdmException
	 */
	public static List<Notification> findActualNotificationNotReadByUser(DSUser user) throws EdmException
	{
    	Criteria crit = DSApi.context().session().createCriteria(Notification.class);
    	// sortowanie po dacie powiadomienia
    	crit.addOrder(Order.asc("notificationDate"));
    	// powiadomienia odpowiedniego u�ytkownika
    	crit.add(Restrictions.lt("notificationDate", new Date()));
    	crit.add(Restrictions.eq("read", false));
    	crit.add(Restrictions.eq("user", user));
    	return crit.list();
	}

    /**
     * Metoda zwraca list� aktualnych powiadomie� dla danego u�ytkownika (kt�re przekroczy�y dat�).
     *
     * @param user - u�ytkownik
     * @param sortField
     *@param ascending @return
     * @throws EdmException
     */
    public static List<Notification> findActualNotificationByUser(DSUser user, String sortField, boolean ascending) throws EdmException
    {
        Criteria crit = DSApi.context().session().createCriteria(Notification.class);
        // sortowanie po dacie powiadomienia
        if (sortField != null)
        {
            if (ascending)
                crit.addOrder(Order.asc(sortField));
            else
                crit.addOrder(Order.desc(sortField));
        }
        else
            crit.addOrder(Order.asc("notificationDate"));
        // powiadomienia odpowiedniego u�ytkownika
        crit.add(Restrictions.lt("notificationDate", new Date()));
        crit.add(Restrictions.eq("user", user));
        crit.add(Restrictions.eq("type", 3));
        return crit.list();
    }
    
    /**
	 * Metoda zwraca list� powiadome� powi�zanych z definicj� powiadomienia
	 * @param user - u�ytkownik
	 * @return
	 * @throws EdmException
	 */
	public static List<Notification> findChildNotifications(Long parentId) throws EdmException
	{
    	Criteria crit = DSApi.context().session().createCriteria(Notification.class);
    	crit.add(Restrictions.eq("parentId", parentId));
    	return crit.list();
	}
    
    
	/**
	 * Metoda zwraca rozmiar listy powiadomie� u�ytkownika.
	 * @param 
	 * @return crit.list().size()
	 * @throws EdmException
	 */
	public static Integer getMemberNotificationNumber() throws EdmException{
		Criteria crit;
		Boolean context = false;
		try {
			context = DSApi.openContextIfNeeded();
   		 	DSApi.context().begin();
			crit = DSApi.context().session().createCriteria(Notification.class);
			crit.add(Restrictions.eq("user", DSApi.context().getDSUser()));
	    	return crit.list().size();
		} catch (EdmException e) {
			log.error(e.getMessage());
		}
		finally {
			DSApi.context().session().flush();
			DSApi.closeContextIfNeeded(context);
		}
    	return 0;
	}
	
	public static String notificationToString() throws EdmException{
		Criteria crit;
		Boolean context = false;
		try {
			context = DSApi.openContextIfNeeded();
   		 	//DSApi.context().begin();
			crit = DSApi.context().session().createCriteria(Notification.class);
			crit.add(Restrictions.eq("user", DSApi.context().getDSUser()));
			crit.add(Restrictions.lt("notificationDate", new Date()));
	    	crit.add(Restrictions.eq("read", false));
	    	crit.add(Restrictions.eq("type", 3));
	    	return "(" + crit.list().size() + ")";
		} catch (EdmException e) {
			log.error(e.getMessage());
		}
		finally {
			DSApi.context().session().flush();
			DSApi.closeContextIfNeeded(context);
		}
    	return "";
	}
	
	/**
	 * Zwraca liczbe powiadomien dla zalogowanego uzytkownika
	 * @return notificationsCount
	 */
	public static int notificationsCount() {
		Criteria crit;
		Boolean context = false;
		try {
			DSUser user = DSUser.getLoggedInUser();
			context = DSApi.openContextIfNeeded();
			crit = DSApi.context().session().createCriteria(Notification.class);
			crit.add(Restrictions.eq("user", user));
			crit.add(Restrictions.lt("notificationDate", new Date()));
	    	crit.add(Restrictions.eq("read", false));
	    	crit.add(Restrictions.eq("type", 3));
	    	return crit.list().size();
		} catch (EdmException e) {
			log.info(e.getMessage());
			return 0;
		}
		finally {
			try {
				DSApi.context().session().flush();
			} catch (HibernateException e1) {
				log.info(e1.getMessage());
			} catch (EdmException e2) {
				log.info(e2.getMessage());
			}
			DSApi.closeContextIfNeeded(context);
		}
	}
	
	/**
	 * Metoda zwraca posortowana po notificationDate list� powiadomie� dla danego u�ytkownika.
	 * @param user - u�ytkownik
	 * @return
	 * @throws EdmException
	 */
	public static List<Notification> findByUserWithOrderByNotDate(DSUser user) throws EdmException 
	{
    	Criteria crit = DSApi.context().session().createCriteria(Notification.class);
    	// sortowanie po dacie utworzenia
    	crit.addOrder(Order.desc("ctime"));
    	// powiadomienia odpowiedniego u�ytkownika
    	crit.add(Restrictions.eq("user", user));
    	return crit.list();
	}
	
	/**
	 * Metoda zwraca list� powiadomie� dla danego u�ytkownika.
	 * @param user - u�ytkownik
	 * @return
	 * @throws EdmException
	 */
	public static void dropByUsertype(DSUser user,Integer type) throws EdmException 
	{
    	Criteria crit = DSApi.context().session().createCriteria(Notification.class);
    	crit.add(Restrictions.eq("user", user));
    	crit.add(Restrictions.eq("type", type));
    	List<Notification> list = crit.list();
    	for (Notification notification : list) 
    	{
    		notification.delete();
		}
    	DSApi.context().session().flush();
	}
	
	/**
	 * Tworzy obiekt w bazie danych.
	 * @throws EdmException
	 */
	public void create() throws EdmException 
	{
		 try 
        {
			this.ctime = new Date();
            DSApi.context().session().save(this);
            DSApi.context().session().flush();	       
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public String getTargetUsers() throws EdmException{
		StringBuilder sb = new StringBuilder();
		Criteria crit;
		Boolean context = false;
		int count = 1;
		
		try {
			context = DSApi.openContextIfNeeded();
   		 	DSApi.context().begin();
			crit = DSApi.context().session().createCriteria(Notification.class);
			crit.add(Restrictions.eq("parentId", id));
			List<Notification> nottificationList = crit.list();
			for(Notification not : nottificationList){
				if(not.getParentId().equals(id) && count == 1){
					sb.append(not.getUser().getLastname()).append(" ").append(not.getUser().getFirstname());
					count++;
				} else if(not.getParentId().equals(id)){
					sb.append(", ").append(not.getUser().getLastname()).append(" ").append(not.getUser().getFirstname());
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			DSApi.context().session().flush();
			DSApi.closeContextIfNeeded(context);
		}
		return sb.toString();
	}
	
	/**
	 * Aktualizuje obiekt w bazie danych.
	 * @throws EdmException
	 */
	public void update(Date notificationDate, String content, boolean read, boolean newNotification, Integer type) throws EdmException 
	{
		 try 
        {
			this.notificationDate = notificationDate;
			this.content = content;
			this.read = read;
			this.newNotification = newNotification;
			this.type = type;
            DSApi.context().session().update(this);
            DSApi.context().session().flush();	       
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	/**
	 * Aktualizuje obiekt w bazie danych.
	 * @throws EdmException
	 */
	public void update(Integer type) throws EdmException 
	{
		 try 
        {
			this.type = type;
            DSApi.context().session().update(this);
            DSApi.context().session().flush();	       
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	/**
	 * Oznacza obiekt jako przeczytany
	 * @throws EdmException
	 */
	public void notificationRead(DSUser user) throws EdmException 
	{
		 try 
        {
			this.read = true;
	        DSApi.context().session().update(this);
	        DSApi.context().session().flush();	  
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	/**
	 * Usuwa obiekt z bazy danych.
	 * @throws EdmException
	 */
	public void delete() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().delete(this);
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public static String formatDate(Date data){
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String formatedDate = dt.format(data);
		return formatedDate;
	}
	
	/**
	 * Dla wydarze� kalendarza ({@link NotificationsManager#OTHER_TYPE} informuje,
	 * czy wydarzenie jest wydarzeniem cyklicznym ({@link NotificationsManager#CALENDAR_PERIODIC_TYPE}
	 * @return <code>true</code> je�li wydarzenie jest wydarzeniem cyklicznym
	 */
	public boolean isPeriodic() {
		return ((type.intValue() & NotificationsManager.CALENDAR_PERIODIC_TYPE) != 0);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public String getContent() {
		return content;
	}

    public String getContentSub10() {
        if (content != null && content.length() > 10)
            return content.substring(0, 9) + "...";
        else return content;
    }

	public void setContent(String content) {
		this.content = content;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public DSUser getUser() {
		return user;
	}

	public void setUser(DSUser user) {
		this.user = user;
	}

	public DSUser getCreator() {
		return creator;
	}

	public void setCreator(DSUser creator) {
		this.creator = creator;
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public boolean isNewNotification() {
		return newNotification;
	}

	public void setNewNotification(boolean newNotification) {
		this.newNotification = newNotification;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

    public boolean isCantEdit()
    {
        return cantEdit;
    }

    public void setCantEdit(boolean cantEdit)
    {
        this.cantEdit = cantEdit;
    }

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
}
