package pl.compan.docusafe.core.news;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.util.QueryForm;
/**
 *	Klasa przechowuje wiadomo�ci jakie wy�wietlane s� w aktualno�ciach.
 *	Tre�� wiadomo�ci dost�pna jest jako Sting, ale w bazie przechowywana jest jako obiekt binarny. 
 *
 */
public class News implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String REGEX_SCRIPT_TAG = "<[ ]*script.*>";
	
	private Long id;
	/**
	 * Temat wiadomo�ci (og�oszenia, aktualno�ci).
	 */
	private String subject;
	/**
	 * Tre�� wiadomo�ci (nale�y pami�ta�, �e przechowuje dane ze wszystkimi tagami html, itp).
	 * Wy�wietlaj�c j� na stronie JSP nale�y ustawi� escape na false.
	 */
	private String description;
	/**
	 * Okre�la u�ytkownika dodaj�cego wiadomo��.
	 */
	private DSUser user;
	/**
	 * Czas utworzenia.
	 */
	private Date ctime;
	/**
	 * Czas modyfikacji.
	 */
	private Date mtime;
	/**
	 * Typ wiadomo�ci (og�oszenia). 
	 * {@link NewsType}
	 */
	private NewsType newsType;
	/**
	 * Okre�la czy dany post jest usuni�ty.
	 */
	private boolean deleted;
	
	public static News find(Long id) throws EdmException
	{
		  return (News)Finder.find(News.class, id);
	}
	
	
	public static SearchResults<? extends News> search(int offset,int limit,NewsType type) throws EdmException
	{
		Criteria c = DSApi.context().session().createCriteria(News.class);
		c.setProjection(Projections.rowCount());
		c.add(Restrictions.eq("newsType", type));
		c.add(Restrictions.eq("deleted", false));
		Long countLong = (Long) c.list().get(0);
		Integer count = countLong.intValue();
		if(count < 1)
			return SearchResultsAdapter.emptyResults(News.class);
		c = DSApi.context().session().createCriteria(News.class);
		c.setMaxResults(limit);
		c.setFirstResult(offset);
    	c.addOrder(Order.desc("ctime"));
    	// odpowiedni typ posta
    	c.add(Restrictions.eq("newsType", type));
    	// tylko nieusuni�te
    	c.add(Restrictions.eq("deleted", false));
		List<News> list = (List<News>) c.list();
		if (list.size() == 0)
		{
			return SearchResultsAdapter.emptyResults(News.class);
		}
		else
		{
			int toIndex;
			int fromIndex = offset < count ? offset : count - 1;
			if (limit == 0)
				toIndex = count;
			else
				toIndex = (offset + limit <= count) ? offset + limit : count;
			return new SearchResultsAdapter<News>(list, offset, count,
					News.class);
		}		
	}
	
	/**
	 * Zwraca wszystkie nieusuni�te (deleted==false) wiadomo�ci z danego typu.
	 * Ilo�� zwracanych typ�w ustalana jest na podstawie globalnych ustawie� ({@link GlobalPreferences}).
	 * @param type odpowiedni typ {@link NewsType}
	 * @return
	 * @throws EdmException
	 */
	public static List<News> findByType(NewsType type) throws EdmException 
	{
    	Criteria crit = DSApi.context().session().createCriteria(News.class);
    	// sortowanie po dacie utworzenia
    	crit.addOrder(Order.desc("ctime"));
    	// odpowiedni typ posta
    	crit.add(Restrictions.eq("newsType", type));
    	// tylko nieusuni�te
    	crit.add(Restrictions.eq("deleted", false));
    	// liczba post�w wg ustawie� zarz�dzaj�cego
    	//crit.setMaxResults(GlobalPreferences.getPostNumber());
    	return crit.list();
	}
	/**
	 * Tworzy obiekt w bazie danych.
	 * @throws EdmException
	 */
	public void create() throws EdmException 
	{
		 try 
        {
			this.ctime = new Date();
            DSApi.context().session().save(this);
            DSApi.context().session().flush();	       
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getDescription() {
		return description;
	}
	/**
	 * Metoda ustawia tre�� aktualno�ci.
	 * W metodzie wykrywane s� tagi z javasciptem i usuwane.
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description.replaceAll(REGEX_SCRIPT_TAG, "");
		this.mtime = new Date();
	}
	
	public DSUser getUser() {
		return user;
	}
	
	public void setUser(DSUser user) {
		this.user = user;
		this.mtime = new Date();
	}
	
	public Date getCtime() {
		return ctime;
	}
	
	public void setCtime(Date ctime) {
		this.ctime = ctime;
		this.mtime = new Date();
	}
	
	public Date getMtime() {
		return mtime;
	}
	
	public void setMtime(Date mtime) {
		this.mtime = mtime;
	}
	
	public NewsType getNewsType() {
		return newsType;
	}
	
	public void setNewsType(NewsType newsType) {
		this.newsType = newsType;
		this.mtime = new Date();
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
		this.mtime = new Date();
	}
	
}
