package pl.compan.docusafe.core.reports;

import org.hibernate.*;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;

import java.io.*;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Report.java,v 1.10 2009/02/28 09:14:56 wkuty Exp $
 */
public class Report implements Lifecycle
{
    private static final Log log = LogFactory.getLog(Report.class);

    /**
     * Status nadawany raportowi zaraz po utworzeniu wpisu w bazie danych.
     * Raport maj�cy ten status nie jest jeszcze utworzony.
     */
    public static final String STATUS_PENDING = "pending";
    /**
     * Status nadawany raportowi po utworzeniu.
     */
    public static final String STATUS_DONE = "done";
    /**
     * Status nadawany raportowi po nieudanym utworzeniu.
     */
    public static final String STATUS_FAILED = "failed";

    private Long id;
    private String reportId;
    private String version;
    private String author;
    private String title;
    private Date ctime;
    private String status;
    private String lparam;
    private Long wparam;

    Report()
    {
    }

    public Report(String reportId, String version, String username, String title)
    {
        this.reportId = reportId;
        this.version = version;
        this.author = username;
        this.title = title;
    }

    public static Report find(Long id) throws EdmException
    {
        return (Report) Finder.find(Report.class, id);
    }

    public static List findByAuthor(String author) throws EdmException
    {
        if (author == null)
            throw new NullPointerException("author");

        Criteria criteria = DSApi.context().session().createCriteria(Report.class);
        criteria.add(Expression.eq("author", author));
        criteria.addOrder(Order.desc("ctime"));
        try
        {
            return criteria.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public File getXmlAsFile() throws EdmException
    {
        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select contentdata from " +
                    DSApi.getTableName(Report.class)+" where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();

            if (!rs.next())
                throw new EdmException("Nie znaleziono raportu nr "+id);
            
            if(DSApi.isPostgresServer()){
            	InputStream is =rs.getBinaryStream(1);//  blob.getBinaryStream();
                File file = File.createTempFile("docusafe_report_", ".xml");
                OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
                byte[] buffer = new byte[1024];
                int count;
                while ((count = is.read(buffer)) > 0)
                {
                    os.write(buffer, 0, count);
                }
                org.apache.commons.io.IOUtils.closeQuietly(is);
                org.apache.commons.io.IOUtils.closeQuietly(os);

                if (log.isDebugEnabled())
                    log.debug("file="+file);

                return file;
            
            }else{ 
            Blob blob = rs.getBlob(1);
            if (blob != null)
            {
                InputStream is =rs.getBinaryStream(1);//  blob.getBinaryStream();
                File file = File.createTempFile("docusafe_report_", ".xml");
                OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
                byte[] buffer = new byte[1024];
                int count;
                while ((count = is.read(buffer)) > 0)
                {
                    os.write(buffer, 0, count);
                }
                org.apache.commons.io.IOUtils.closeQuietly(is);
                org.apache.commons.io.IOUtils.closeQuietly(os);

                if (log.isDebugEnabled())
                    log.debug("file="+file);

                return file;
            }
            else
                return null;
        }
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas tworzenia pliku tymczasowego", e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }

    public void create() throws EdmException
    {
        Persister.create(this);
    }

    public void delete() throws EdmException
    {
        Persister.delete(this);
    }

    public boolean isDone()
    {
        return STATUS_DONE.equals(status) || STATUS_FAILED.equals(status);
    }

    public boolean isFailed()
    {
        return STATUS_FAILED.equals(status); 
    }

    /* Lifecycle */

    public boolean onSave(Session s) throws CallbackException
    {
        if (ctime == null)
            ctime = new Date();

        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getReportId()
    {
        return reportId;
    }

    public void setReportId(String reportId)
    {
        this.reportId = reportId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getLparam()
    {
        return lparam;
    }

    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }

    public Long getWparam()
    {
        return wparam;
    }

    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }
}
