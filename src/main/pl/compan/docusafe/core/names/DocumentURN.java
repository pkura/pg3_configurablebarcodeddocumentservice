package pl.compan.docusafe.core.names;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
/* User: Administrator, Date: 2006-02-03 13:49:50 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocumentURN.java,v 1.5 2006/02/20 15:42:18 lk Exp $
 */
public class DocumentURN extends URN
{
    public static final String PREFIX = "document";

    private Long id;
    private String type;
    private Integer officeNumber;

    public DocumentURN(Document document)
    {
        id = document.getId();
        type = document.getType().getName();

        if (document instanceof OfficeDocument)
        {
            officeNumber = ((OfficeDocument) document).getOfficeNumber();
        }
    }

    public DocumentURN(String urn) throws MalformedURNException
    {
        String[] fields = urn.split(":", 2);
        String[] tmp = fields[1].split(",", 3);
        if (tmp.length < 1 || tmp.length > 3)
            throw new MalformedURNException(urn);
        try
        {
            id = new Long(Long.parseLong(tmp[0]));
        }
        catch (Exception e)
        {
            throw new MalformedURNException("Niepoprawny identyfikator dokumentu: "+urn);
        }

        if (tmp.length > 1)
        {
            if (!OutOfficeDocument.INTERNAL_TYPE.equals(tmp[1]) &&
                !OutOfficeDocument.TYPE.equals(tmp[1]) &&
                !InOfficeDocument.TYPE.equals(tmp[1]) &&
                !Document.TYPE.equals(tmp[1]))
                throw new MalformedURNException("Niepoprawny typ dokumentu: "+urn);
            type = tmp[1];
        }

        if (tmp.length > 2)
        {
            if (Document.TYPE.equals(type))
                throw new MalformedURNException("Dokument typu "+Document.TYPE+" nie mo�e posiada� numeru KO: "+urn);
            try
            {
                officeNumber = new Integer(Integer.parseInt(tmp[2]));
            }
            catch (Exception e)
            {
                throw new MalformedURNException("Niepoprawny numer KO: "+urn);
            }
        }
    }

    public Long getId()
    {
        return id;
    }

    public String getType()
    {
        return type;
    }

    public Integer getOfficeNumber()
    {
        return officeNumber;
    }

    public String toExternalForm()
    {
        return PREFIX+":"+id+
            (type != null ? ","+type+
                (officeNumber != null ? ","+officeNumber : "") : "");
    }
}
