package pl.compan.docusafe.core.names;

import pl.compan.docusafe.core.office.workflow.WfExecutionObject;
import pl.compan.docusafe.core.office.workflow.WorkflowException;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
/* User: Administrator, Date: 2006-02-03 13:55:33 */
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;


public class WorkflowActivityURN extends WfExecutionObjectURN
{
	//musi sie roznic od WfActivity
    public static final String PREFIX = "newtask";

    public WorkflowActivityURN(WorkflowActivity act) throws MalformedURNException, WorkflowException
    {
        super("task:internal,"+WorkflowFactory.getInstance().keyOfId(act.getTaskFullId()));
    }

    public WorkflowActivityURN(String urn)
        throws MalformedURNException
    {
        super(urn);
    }

    public String getKey() {
    	return super.getKey();
    }
    
    protected String getPrefix()
    {
        return PREFIX;
    }
}