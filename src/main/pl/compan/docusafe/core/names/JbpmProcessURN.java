package pl.compan.docusafe.core.names;
/* User: Administrator, Date: 2007-02-26 14:21:53 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
@Deprecated
public class JbpmProcessURN extends URN
{
    private Long processId;
    // nazwa zadania, z ktorego pobierane bedzie pole "Dekretujacy" ('actor' tego zadania)
    private String taskName;

    public static final String PREFIX = "jbpm_process";

    public JbpmProcessURN(Long processId, String taskName)
    {
        this.processId = processId;
        this.taskName = taskName;
    }

    public JbpmProcessURN(String urn) throws MalformedURNException
    {
        String[] fields = urn.split(":", 3);
        if (fields.length < 2 || fields.length > 3)
            throw new MalformedURNException(urn);

        processId = new Long(fields[1]);
        if (fields.length == 3)
            taskName = fields[2];
        else
            taskName = null;
    }

    public String toExternalForm()
    {
        return getPrefix()+":"+processId+(taskName != null ? ":"+taskName : "");
    }

    protected String getPrefix()
    {
        return PREFIX;
    }

    public Long getProcessId()
    {
        return processId;
    }

    public String getTaskName()
    {
        return taskName;
    }
}
