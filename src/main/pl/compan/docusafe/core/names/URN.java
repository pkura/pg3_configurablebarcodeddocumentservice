package pl.compan.docusafe.core.names;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.WfProcess;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.web.office.in.WorkflowAction;

import java.util.HashMap;
import java.util.Map;
/* User: Administrator, Date: 2006-02-03 13:38:40 */

/**
 * URN określający obiekt w systemie Docusafe.  Klasa URN jest abstrakcyjna,
 * konkretne reprezentacje realizowane są przez klasy dziedziczące. 
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: URN.java,v 1.5 2008/09/10 11:08:37 pecet4 Exp $
 */
public abstract class URN
{
    private static final Map<String, Class<? extends URN>> urnPrefixes = new HashMap<String, Class<? extends URN>>();
    static
    {
        urnPrefixes.put(DocumentURN.PREFIX, DocumentURN.class);
        urnPrefixes.put(WfActivityURN.PREFIX, WfActivityURN.class);
        urnPrefixes.put(WorkflowActivityURN.PREFIX, WorkflowActivityURN.class);
        urnPrefixes.put(WfProcessURN.PREFIX, WfProcessURN.class);
        urnPrefixes.put(JbpmProcessURN.PREFIX, JbpmProcessURN.class);
    }

    public static URN create(Document document)
    {
        return new DocumentURN(document);
    }

    public static URN create(WfActivity activity)
    {
        return new WfActivityURN(activity);
    }
    
    public static URN create(WorkflowActivity activity) throws EdmException
    {
    	try {
    		return new WorkflowActivityURN(activity);
    	} catch (Exception e) {
			throw new EdmException(e.getMessage());
		}
    }

    public static URN create(WfProcess process)
    {
        return new WfProcessURN(process);
    }

    public static URN create(Long processId, String taskName)
    {
        return new JbpmProcessURN(processId, taskName);
    }

    /**
     * Zwraca instancję URN odpowiadającą przekazanemu identyfikatorowi.
     * @param urn
     * @return
     * @throws MalformedURNException
     */
    public static URN parse(String urn) throws MalformedURNException
    {
        int pos;
        if ((pos=urn.indexOf(':')) <= 0)
            throw new MalformedURNException(urn);

        String prefix = urn.substring(0, pos);

        Class<? extends URN> clazz = urnPrefixes.get(prefix);

        if (clazz == null)
            throw new MalformedURNException("Nieznany prefiks: "+urn);

        try
        {
            return clazz.getConstructor(String.class).newInstance(urn);
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public abstract String toExternalForm();

    public String toString()
    {
        return toExternalForm();
    }
}
