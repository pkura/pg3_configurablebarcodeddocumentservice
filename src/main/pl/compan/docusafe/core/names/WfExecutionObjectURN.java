package pl.compan.docusafe.core.names;

import pl.compan.docusafe.core.office.workflow.WfExecutionObject;
import pl.compan.docusafe.core.office.workflow.WorkflowException;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
/* User: Administrator, Date: 2006-02-06 10:13:39 */

/**
 * URN dotycz�cy obiekt�w workflow (WfActivity i WfProcess).
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfExecutionObjectURN.java,v 1.2 2008/09/09 10:33:22 pecet4 Exp $
 */
public abstract class WfExecutionObjectURN extends URN
{
    private String workflow;
    private String key;

    protected abstract String getPrefix();

    protected WfExecutionObjectURN(WfExecutionObject eo)
    {
        workflow = "internal";
        try
        {
            key = eo.key();
        }
        catch (WorkflowException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    protected WfExecutionObjectURN(String urn) throws MalformedURNException
    {
        String[] fields = urn.split(":", 2);
        if (fields.length < 2)
            throw new MalformedURNException(urn);

        String[] tmp = fields[1].split(",", 2);
        if (tmp.length != 2)
            throw new MalformedURNException(urn);

        if (!InternalWorkflowService.NAME.equals(tmp[0]))
            throw new MalformedURNException("Nieznana nazwa workflow: "+urn);
        if (tmp[1].length() == 0)
            throw new MalformedURNException(urn);
        workflow = tmp[0];
        key = tmp[1];
    }

    public String getWorkflow()
    {
        return workflow;
    }

    public String getKey()
    {
        return key;
    }

    public String toExternalForm()
    {
        return getPrefix()+":"+workflow+","+key;
    }
}
