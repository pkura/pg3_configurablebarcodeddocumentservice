package pl.compan.docusafe.core.names;

import pl.compan.docusafe.core.office.workflow.WfExecutionObject;
/* User: Administrator, Date: 2006-02-03 16:32:46 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfProcessURN.java,v 1.3 2006/02/20 15:42:18 lk Exp $
 */
public class WfProcessURN extends WfExecutionObjectURN
{
    public static final String PREFIX = "process";

    public WfProcessURN(WfExecutionObject eo)
    {
        super(eo);
    }

    public WfProcessURN(String urn) throws MalformedURNException
    {
        super(urn);
    }

    protected String getPrefix()
    {
        return PREFIX;
    }
}
