package pl.compan.docusafe.core.names;

import pl.compan.docusafe.core.office.workflow.WfExecutionObject;
/* User: Administrator, Date: 2006-02-03 13:55:33 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfActivityURN.java,v 1.3 2006/02/20 15:42:18 lk Exp $
 */
public class WfActivityURN extends WfExecutionObjectURN
{
    public static final String PREFIX = "task";

    public WfActivityURN(WfExecutionObject eo)
    {
        super(eo);
    }

    public WfActivityURN(String urn)
        throws MalformedURNException
    {
        super(urn);
    }

    protected String getPrefix()
    {
        return PREFIX;
    }
}
