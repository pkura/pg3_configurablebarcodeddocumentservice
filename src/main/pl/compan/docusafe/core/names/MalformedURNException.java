package pl.compan.docusafe.core.names;

import pl.compan.docusafe.core.EdmException;
/* User: Administrator, Date: 2006-02-03 14:00:34 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: MalformedURNException.java,v 1.1 2006/02/03 16:04:36 lk Exp $
 */
public class MalformedURNException extends EdmException
{
    public MalformedURNException(String message)
    {
        super(message);
    }
}
