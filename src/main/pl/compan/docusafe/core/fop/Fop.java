package pl.compan.docusafe.core.fop;

import org.apache.avalon.framework.logger.Log4JLogger;
import org.apache.fop.apps.Driver;
import org.apache.fop.apps.Options;
import org.apache.log4j.Category;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import pl.compan.docusafe.core.EdmException;

import javax.xml.transform.*;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Fop.java,v 1.3 2005/11/24 16:16:37 lk Exp $
 */
public class Fop
{
    private static final Logger log = LoggerFactory.getLogger(Fop.class);

    /**
     * Inicjalizuje FOP.
     * @param directory
     * @throws Exception
     */
    public static void initFonts(File directory) throws Exception
    {
        Document doc = new SAXReader().read(Fop.class.getClassLoader().
            getResourceAsStream("fop.xml"));

        ((Element) doc.selectSingleNode("/configuration/entry[key='fontBaseDir']/value")).
            addText(directory.getAbsolutePath());

        if (log.isDebugEnabled())
            log.debug("fop.xml="+doc.asXML());

        new Options(new InputSource(new StringReader(doc.asXML())));
    }

    public static void renderPdf(InputStream xml, InputStream xsl, OutputStream pdf)
        throws EdmException
    {
        Driver driver = new Driver();

        driver.setLogger(new Log4JLogger(Category.getInstance(Fop.class)));
        driver.setRenderer(Driver.RENDER_PDF);

        driver.setOutputStream(pdf);

        Source src = new StreamSource(xml);
        Source xslSrc = new StreamSource(xsl);
        Result res = new SAXResult(driver.getContentHandler());

        try
        {
            
            Transformer t = TransformerFactory.newInstance().newTransformer(xslSrc);
            t.transform(src, res);
        }
        catch (TransformerException e)
        {
            throw new EdmException(e.getMessage(), e);
        }
    }
}
