package pl.compan.docusafe.core;

public class IntegrityVerificationException extends EdmException
{
	private static final long serialVersionUID = 1L;

	public IntegrityVerificationException(String message)
	{
		super(message);
	}

	public IntegrityVerificationException(String message, Throwable e)
	{
		super(message, e);
	}
	
	public IntegrityVerificationException(Throwable e)
	{
		super(e);
	}
}
