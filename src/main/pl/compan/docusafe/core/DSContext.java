package pl.compan.docusafe.core;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.prefs.SQLPreferencesFactory;
import pl.compan.docusafe.core.crypto.EncryptionCredentials;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.jackrabbit.JackrabbitPrivileges;
import pl.compan.docusafe.core.names.DocumentURN;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.names.WfProcessURN;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.workflow.*;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
import pl.compan.docusafe.core.office.workflow.internal.WfActivityImpl;
import pl.compan.docusafe.core.office.workflow.internal.WfAssignmentImpl;
import pl.compan.docusafe.core.office.workflow.jbpm.JbpmWorkflowService;
import pl.compan.docusafe.core.office.workflow.snapshot.RefreshRequestBean;
import pl.compan.docusafe.core.office.workflow.snapshot.SnapshotUtils;
import pl.compan.docusafe.core.security.PrivilegedProcedure;
import pl.compan.docusafe.core.security.SecurityHelper;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.StringManager;

import javax.security.auth.Subject;
import javax.transaction.Status;
import java.io.Serializable;
import java.sql.*;
import java.util.*;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

/**
 * Kontekst wi�zany po otwarciu z w�tkiem. Umo�liwia dost�p do sesji Hibernate.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DSContext.java,v 1.159 2010/08/17 12:14:45 pecet6 Exp $
 */
public final class DSContext
{
    private static StringManager sm = StringManager.getManager(Constants.Package);
    private static Logger log = LoggerFactory.getLogger(DSContext.class);
    private final static PreferencesFactory PREFERENCES_FACTORY = new SQLPreferencesFactory();
    private static final Messages NULL_HANDLER = new Messages(){
        public void addActionError(String s) {
        }
        public void addActionMessage(String s) {
        }
    };

    private Subject subject;
    private org.hibernate.Session session;
    private org.hibernate.Transaction transaction;
    private boolean closed;
    private boolean rollbackOnly;
    private DSUser dsuser;
    private String callingStack;
    //private EncryptionCredentials credentials;

    /**
     * Tu sa zapamietywane otwarte statementy
     */
    private Collection<Statement> openStatements;
    /**
     * W kontekscie zapisujemy przygotowane do aktualizacji zadania
     * Po to aby nie odbywaly sie w tej samej transakcji co inne zmiany
     */
    private Map<Long,RefreshRequestBean> refreshTasklist = null; 
    /**
     * Zbi�r obiekt�w DocumentWatch dodawanych przez metod� {@link #queueWatch(pl.compan.docusafe.core.base.DocumentWatch, java.util.Map)}.
     */
    private Set<DocumentWatch> watchQueue = new HashSet<DocumentWatch>();
    private int transactionStatus = Status.STATUS_NO_TRANSACTION;
    /**
     * Wyj�tek tworzony podczas tworzenia kontekstu. U�ywany przy wypisywaniu rozmaitych
     * wyj�tk�w dla �atwiejszego zlokalizowania w�a�ciwego kontekstu w kodzie.
     */
    private Throwable trace;
    private Preferences systemRoot;
    private Preferences userRoot;
    // TODO: przenie�� tworzenie tych kolekcji do metod, kt�re je po raz pierwszy u�ywaj�
    private List<DocumentWatch> watches = new LinkedList<DocumentWatch>();
    private List<Map<String, String>> watchArgs = new LinkedList<Map<String, String>>();
    private Messages messages = NULL_HANDLER;

    DSContext(Subject subject)
    {
        if (subject == null)
        {
            log.warn("Subject = null");
            throw new NullPointerException("subject");
        }

        Set users = subject.getPrincipals(UserPrincipal.class);
        if (users == null || users.size() == 0)
        {
            log.warn("W obiekcie Subject nie ma obiektu UserPrincipal: "+subject);
            throw new IllegalArgumentException(sm.getString("WobiekcieSubjectNieMa")+" " +
                sm.getString("obiektuUserPrincipal"));
        }


        this.trace = new Throwable();

        this.subject = subject;

        openStatements = new Vector<Statement>();
        //this.credentials = new EncryptionCredentials();
    }

    private Stack<Privileges> privileges;

    /**
     * Wykonuje akcj� ze zwi�kszonymi uprawnieniami.
     */
    public <T> T doPrivileged(PrivilegedProcedure<T> procedure, String[] roles, DSPermission[] permissions)
        throws EdmException
    {
        if (privileges == null)
            privileges = new Stack<Privileges>();
        privileges.push(new Privileges(permissions, roles));
        try
        {
            return procedure.run();
        }
        finally
        {
            privileges.pop();
        }
    }

    public void clearMessages() {
        this.messages = NULL_HANDLER;
    }

    private static class Privileges
    {
        private Set<DSPermission> permissions;
        private Set<String> roles;

        public Privileges(DSPermission[] permissions, String[] roles)
        {
            if (permissions != null && permissions.length > 0)
            {
                this.permissions = new HashSet<DSPermission>(permissions.length, 1);
                for (DSPermission perm : permissions)
                {
                    this.permissions.add(perm);
                }
            }
            if (roles != null && roles.length > 0)
            {
                this.roles = new HashSet<String>(roles.length, 1);
                for (String role : roles)
                {
                    this.roles.add(role);
                }
            }
        }

        public boolean hasRole(String role) { return roles != null && roles.contains(role); }
        public boolean hasPermission(DSPermission perm) { return permissions != null && permissions.contains(perm); }
    }

    public Messages messages(){
        return messages;
    }

    public void setMessages(Messages messages){
        this.messages = messages;
    }

    /**
     * Pobiera g��wn� ga��� preferencji systemowych.
     * <p>
     * U�ywane nazwy ga��zi: <ul>
     * <li><code>services</code> - u�ywane przez us�ugi, ka�da us�uga
     *  ma podga��� nazwan� tak, jak jej identyfikator w pliku services.xml.
     * <li><code>modules/certificate</code> - certyfikaty
     * <li><code>modules/coreoffice/officeid/GUID</code>
     * <li><code>modules/coreoffice/passwordpolicy</code>
     * <li><code>internalworkflow/deadlinewatch</code>
     * </ul>
     */
    public Preferences systemPreferences()
    {
        if (systemRoot == null)
            systemRoot = PREFERENCES_FACTORY.systemRoot();
        return systemRoot;
    }

    /**
     * Pobiera ga��� preferencji zalogowanego u�ytkownika.
     * <p>
     * U�ywane nazwy ga��zi: <ul>
     * <li><code>task-list</code> - klucz <code>columns:String</code> okre�la
     *   nazwy kolumn widoczne na li�cie zada� u�ytkownika</li>
     * <li><code>scanner</code> - klucz <code>use:Boolean</code> okre�la,
     *   czy u�ytkownik u�ywa skanera</li>
     * <li><code>signature</code> - klucz <code>use:Boolean</code> okre�la,
     *   czy u�ytkownik u�ywa podpisu</li>
     * </ul>
     */
    public Preferences userPreferences()
    {
        if (userRoot == null)
            userRoot = PREFERENCES_FACTORY.userRoot();
        return userRoot;
    }

    public Preferences userPreferences(String username)
    {
        return ((SQLPreferencesFactory) PREFERENCES_FACTORY).userRoot(username);
    }

    /**
     * Zamyka sesj� Hibernate zwi�zan� z tym kontekstem i po��czenie
     * z baz� danych. Je�eli w momencie wywo�ywania tej metody z kontekstem
     * zwi�zana jest nieuko�czona transakcja, a flaga {@link #rollbackOnly}
     * ma warto�� false, rzucany jest wyj�tek IllegalStateException,
     * w przeciwnym wypadku transakcja jest wycofywana.
     * <p>
     * Niezale�nie od wyj�tk�w rzuconych przez t� metod�, kontekst jest
     * oznaczany jako zamkni�ty.
     * @throws EdmException
     */
    void close() throws EdmException
    {
        if (closed)
        {
            log.warn(sm.getString("dscontext.alreadyClosed"), trace);
            return;
        }

        // je�eli sesja Hibernate nie by�a otwarta, zamkni�cie obiektu
        // Context sprowadza si� do ustawienia flagi closed=true
        if (session == null)
        {
            closed = true;
            return;
        }

        try
        {
            // w momencie zamykania kontekstu obecna jest niedoko�czona
            // transakcja - wycofuj� j�, nawet je�eli u�ytkownik tego
            // nie zaznaczy� przy pomocy setRollbackOnly()
            if (transaction != null)
            {
                if (!rollbackOnly)
                {
                    //throw new IllegalStateException("Nie wywo�ano metody commit() ani rollback()");
                    log.warn("Przed zamkni�ciem kontekstu zawieraj�cego transakcj� nie wywo�ano " +
                        "metody commit() ani rollback(). Wyj�tek pokazuje jedynie kontekst, w jakim " +
                        "si� to zdarzy�o.", new Throwable());
                    log.warn("Miejsce otwarcia kontekstu", trace);
                }
                transaction.rollback();
                transaction = null;
            }
            
            checkOpenedStatements();
            if(session.isOpen()){
                Connection conn = session.close();
                if (conn != null) conn.close();
            }

            session = null;
        }
        catch (SQLException e)
        {
            log.warn("Nieudane zamkni�cie po��czenia z baz� danych", e);
        }
        catch (HibernateException e)
        {
            log.info("ERROR:"+e.getMessage(), e);
            throw new EdmException(sm.getString("dscontext.rollbackFailed"), e);
        }
        catch (Throwable e){
            log.info("ERROR:"+e.getMessage(), e);
            throw new EdmException(sm.getString("dscontext.rollbackFailed"), e);
        }
        finally
        {
            // niezale�nie od wyj�tk�w, kt�re mog�y powsta� przy zamykaniu
            // kontekstu, oznaczam kontekst jako zamkni�ty
            closed = true;

            if (session != null && session.isOpen())
            {
                try
                {
                    Connection conn = session.close();
                    if (conn != null) conn.close();
                    session = null;
                }
                catch (SQLException e)
                {
                    log.warn("Nieudane zamkni�cie po��czenia z baz� danych", e);
                }
                catch (HibernateException e)
                {   //PO�YKANIE WYJ�TK�W Z BLOKU try catch - wyskakuje poni�szy wyj�tek zamiast z tamtego bloku
                    throw new EdmException(sm.getString("dscontext.closeFailed"), e);
                }
            }
        }
    }

    public boolean inTransaction()
    {
        return transaction != null;
    }

    /**
     * Zwraca sesj� Hibernate zwi�zan� z tym kontekstem. Sesja
     * jest tworzona w razie potrzeby.
     * @return
     * @throws EdmException
     */
    public org.hibernate.Session session() throws EdmException {
        return getSession(true);
    }

    /**
     * Zwraca sesj� Hibernate zwi�zan� z tym kontekstem. Sesja
     * jest tworzona w razie potrzeby.
     * @return
     * @throws EdmException
     * @deprecated nale�y u�ywa� session(), ta metoda zosta�a dodana tylko do
     * obs�ugi starych wywo�a� korzystaj�cych z org.hibernate.classic.Session,
     * kt�rego ju� nie ma w Hibernate 4
     */
    @Deprecated
    public org.hibernate.classic.Session classicSession() throws EdmException {
        return (org.hibernate.classic.Session) getSession(true);
    }

//    public PermissionsHelper getPermissionsHelper()
//    {
//        return permissionsHelper;
//    }

    /**
     * Zwraca true, je�eli u�ytkownik (Subject) zwi�zany z tym kontekstem
     * posiada rol� "admin".
     */
    public boolean isAdmin()
    {
        if (subject == null)
            throw new IllegalStateException(sm.getString("BrakZalogowanegoUzytkownika"));
        return AuthUtil.hasRole(subject, "admin") || AuthUtil.hasRole(subject, "data_admin") ||
            (privileges != null && !privileges.isEmpty() && privileges.peek().hasRole("admin"));
    }

    /**
     * Zwraca true, je�eli u�ytkownik (Subject) zwi�zany z tym kontekstem
     * posiada rol� "admin".
     */
    public boolean isAdmin(boolean notDataAdmin)
    {
        if (subject == null)
            throw new IllegalStateException(sm.getString("BrakZalogowanegoUzytkownika"));
        if (!notDataAdmin && AuthUtil.hasRole(subject, "data_admin"))
            return true;
        return AuthUtil.hasRole(subject, "admin") ||
            (privileges != null && !privileges.isEmpty() && privileges.peek().hasRole("admin"));
    }

    /**
     * Zwraca obiekt UserPrincipal umieszczony w obiekcie Subject.
     * Nazwa u�ytkownika pobierana z tego obiektu powinna by� u�ywana
     * tylko w�wczas, gdy nie jest konieczne, aby u�ytkownik o tej
     * nazwie faktycznie istnia� w systemie (np. u�ytkownik loguj�cy
     * si� awaryjnie na podstawie pliku HOME/passwd).
     * @throws IllegalStateException Je�eli w obiekcie Subject
     * nie by�o obiektu UserPrincipal.
     */
    private UserPrincipal getUserPrincipal()
    {
        Set users = subject.getPrincipals(UserPrincipal.class);

        if (users != null && users.size() > 0)
            return (UserPrincipal) users.iterator().next();
        else
            throw new IllegalStateException(
                sm.getString("dscontext.subjectMissingUserPrincipal"));
    }

    public String getPrincipalName()
    {
        return getUserPrincipal().getName();
    }

    public DSUser getDSUser() throws EdmException, UserNotFoundException
    {
        if (dsuser == null)
        {
            dsuser = DSUser.findByUsername(getUserPrincipal().getName());
        }
        return dsuser;
    }

    private DocumentHelper documentHelper;
    private SecurityHelper securityHelper;

    public DocumentHelper getDocumentHelper()
    {
        if (documentHelper == null)
            documentHelper = new DocumentHelper();

        return documentHelper;
    }

    public SecurityHelper getSecurityHelper()
    {
        if (securityHelper == null)
            securityHelper = new SecurityHelper();

        return securityHelper;
    }
    
    private AttachmentHelper attachmentHelper;
    
    public AttachmentHelper getAttachmentHelper()
    {
    	if (attachmentHelper == null)
    		attachmentHelper = new AttachmentHelper();
    	
    	return attachmentHelper;
    }

    /**
     * Rozpoczyna transakcj�.
     * @throws EdmException
     * @throws IllegalStateException Je�eli kontekst jest ju� zwi�zany
     * z transakcj� lub zosta� zamkni�ty.
     */
    public void begin() throws EdmException
    {
        if (closed)
            throw new IllegalStateException(sm.getString("KontekstZostalZamkniety"));

        if (transaction != null)
            throw new IllegalStateException(sm.getString("KontekstJestJuzZwiazanyZtransakcja"));

        try
        {
        	refreshTasklist = new TreeMap<Long, RefreshRequestBean>();
            transaction = getSession(true).beginTransaction();
            transactionStatus = Status.STATUS_ACTIVE;
            
      //      int depth = 3;
	//		final StackTraceElement[] ste = new Throwable().getStackTrace();
         //   System. out.println(ste[depth].getClassName()+"#"+ste[depth].getMethodName()+"#"+ste[depth].getLineNumber());
      //      callingStack = ste[depth].getClassName()+"#"+ste[depth].getMethodName()+"#"+ste[depth].getLineNumber();
            //poni�sze wywo�ania s� na wszelki wypadek,
            // konieczne jest by poni�sze mechanizmy by�y "czyste"
            //  w momencie rozpocz�cia transakcji
			DataMartEventBuilder.clearEvents();
            DocumentsCache.getInstance().clear();
        }
        catch (HibernateException e)
        {
            throw new EdmException(sm.getString("context.beginFailed"), e);
        }
    }
    /**NIe zalecamy uzywanie tej metody poniewaz moze podrowadzic do
     * dziwnych bledow trudnych w wykryciu*/
    @Deprecated
    public Boolean isTransactionOpen()
    {
    	return transaction != null;
    }

    public void setRollbackOnly()
    {
        this.transactionStatus = Status.STATUS_MARKED_ROLLBACK;
        this.rollbackOnly = true;
    }

    /**
     * Zatwierdza transakcj�.
     * <p>
     * Je�eli u�ytkownik wywo�a� metod� {@link #setRollbackOnly()},
     * wycofuje transakcj� i rzuca wyj�tek EdmException.
     *
     * @throws EdmException
     */
    public void commit() throws EdmException
    {
        if (closed)
            throw new IllegalStateException(sm.getString("KontekstZostalZamkniety"));

        if (transaction == null)
            throw new IllegalStateException(sm.getString("NieIstniejeRozpoczetaTransakcja"));

        try
        {
            // rejestrowanie zakolejkowanych powiadomie�
            if (watches.size() > 0)
            {
                for (int i=0; i < watches.size(); i++)
                {
                   watches.get(i).execute(watchArgs.get(i));
                }
                watches.clear();
                watchQueue.clear();
                watchArgs.clear();
            }
            // Preferences.flush() ma sens tylko w otwartej transakcji,
            // dlatego wykonywany jest w metodzie commit()
            if (systemRoot != null) systemRoot.flush();
            if (userRoot != null) userRoot.flush();

            if (rollbackOnly)
            {
                transactionStatus = Status.STATUS_ROLLEDBACK;
                transaction.rollback();
                transaction = null;
                transactionStatus = Status.STATUS_NO_TRANSACTION;
                throw new EdmException(sm.getString("dscontext.rolledBack"));
            }
            else
            {
                DataMartEventBuilder.logEvents();
                DocumentsCache.getInstance().saveChanges();
                transactionStatus = Status.STATUS_COMMITTED;
                transaction.commit();
                transactionStatus = Status.STATUS_NO_TRANSACTION;
                //Przekazujemy liste do update
                SnapshotUtils.updateTaskList(session, refreshTasklist);
            }
            refreshTasklist = null;
            transaction = null;
        }
        catch (BackingStoreException e)
        {
            throw new EdmException(sm.getString("NieudanyZapisPreferencji"), e);
        }
        catch (HibernateException e)
        {
            rollbackOnly = true;
            transactionStatus = Status.STATUS_MARKED_ROLLBACK;
            throw new EdmException(sm.getString("dscontext.commitFailed"), e);
        }
		finally {
			DataMartEventBuilder.clearEvents();
            DocumentsCache.getInstance().clear();
		}
    }

    public void updateOneTask(Long id)
    {
    	Map<Long,RefreshRequestBean> tmp = new HashMap<Long, RefreshRequestBean>();
    	RefreshRequestBean rrb = refreshTasklist.get(id);
    	tmp.put(id,rrb);
    	refreshTasklist.remove(id);
        SnapshotUtils.updateTaskList(session, tmp);
    }

    public void rollback() throws EdmException
    {
        if (closed)
            throw new IllegalStateException(sm.getString("KontekstZostalZamkniety"));

        if (transaction == null)
            throw new IllegalStateException(sm.getString("NieIstniejeRozpoczetaTransakcja"));

        transactionStatus = Status.STATUS_ROLLEDBACK;

        try
        {
            transaction.rollback();
            transaction = null;
            transactionStatus = Status.STATUS_NO_TRANSACTION;
        }
        catch (HibernateException e)
        {
            throw new EdmException(sm.getString("dscontext.rollbackFailed"), e);
        }
    }

    public void _rollback()
    {
        if (closed)
            throw new IllegalStateException(sm.getString("KontekstZostalZamkniety"));

        if (transaction == null)
            throw new IllegalStateException(sm.getString("NieIstniejeRozpoczetaTransakcja"));

        transactionStatus = Status.STATUS_ROLLEDBACK;

        try
        {
            transaction.rollback();
            transaction = null;
            transactionStatus = Status.STATUS_NO_TRANSACTION;
        }
        catch (HibernateException e)
        {
            log.debug("ERR-003 rollback");
        }
    }

    /* Ustawienia aplikacji */

    /**
     * Kolejkuje obiekt DocumentWatch. Wszystkie zakolejkowane obiekty
     * wy�l� swoje powiadomienia po wywo�aniu metody {@link #close()}.
     */
    public void queueWatch(DocumentWatch watch, Map<String, String> args)
    {
        if (watchQueue.contains(watch))
            return;

        if (log.isDebugEnabled())
            log.debug("Dodawanie "+watch+" (args="+args+")");

        watchQueue.add(watch);

        watches.add(watch);
        watchArgs.add(args);
    }


    /* Workflow */

    public void makeWfAssignment(String activityId) throws EdmException {
    	makeWfAssignment(activityId, getPrincipalName());
    }

    /**
     * Zapisuje assignment dla wielu u�ytkownik�w jednocze�nie zapisuj�c liczb� tych u�ytkownik�w w atrybucie resource_count,
     * metoda bardziej wydajna w stosunku do iterowania po wszystkich u�ytkownikach
     * @param activityIdString - stringowy activityId
     * @param users - u�ytkownicy, kt�rzy maj� otrzyma� pismo
     * @throws EdmException
     */
    public void makeWfAssignments(String activityIdString, Collection<DSUser> users)throws EdmException{
        log.info("makeWfAssignments : " + activityIdString + " / " + users.size());
        WorkflowActivity activity = WorkflowFactory.getWfActivity(activityIdString);
        Long activityId = ((WfActivityImpl)activity.getActivity()).getId();

        PreparedStatement resourcePs = null;
        PreparedStatement insertPs = null;
        try {
            resourcePs = prepareStatement("select id from dsw_resource where res_key = ?");
            if(DSApi.isOracleServer()) {
    			insertPs = prepareStatement("insert into dsw_assignment (id,accepted , activity_id, resource_id, resource_count) values (dsw_assignment_id.nextval,0, ?, ?, ?)");
            } else if(DSApi.isFirebirdServer()) {
    			insertPs = prepareStatement("insert into dsw_assignment (id,accepted , activity_id, resource_id, resource_count) " +
    					" values (gen_id(dsw_assignment_id, 1),0, ?, ?, ?)");
            } else {
    			insertPs = prepareStatement("insert into dsw_assignment (accepted, activity_id, resource_id, resource_count) values (0, ?, ?, ?)");
    		}
            for(DSUser user: users){
                makeWfAssignment(resourcePs, insertPs, activityId, user.getName(), users.size());
            }
        } catch (SQLException e) {
    		throw new EdmSQLException(e);
    	} finally {
    		closeStatement(resourcePs);
            closeStatement(insertPs);
    	}
    }

    private void makeWfAssignment(PreparedStatement resourcePs, PreparedStatement insertPs, long activityId, String username, int size) throws SQLException{
        ResultSet rs = null;
        resourcePs.setString(1,username);
        long resourceId;
        try{
            rs = resourcePs.executeQuery();
            rs.next();
            resourceId = rs.getLong("id");
        } finally {
            DbUtils.closeQuietly(rs);
        }

        insertPs.setLong(1, activityId);
    	insertPs.setLong(2, resourceId);
        insertPs.setInt(3, size);
        insertPs.executeUpdate();
    }

    public void makeWfAssignment(String actId, String username) throws EdmException {
    	WorkflowActivity activity = WorkflowFactory.getWfActivity(actId);
    	Long activityId = ((WfActivityImpl)activity.getActivity()).getId();
    	PreparedStatement ps = null;
    	Long resourceId = null;
    	try
    	{
    		ps = prepareStatement("select id from dsw_resource where res_key = ?");
    		ps.setString(1, username);
    		ResultSet rs = ps.executeQuery();
    		if (rs.next()) {
    			resourceId = rs.getLong("id");
    		} else
    		{
    			throw new SQLException("user not mapped to resource");
    		}
    		rs.close();
    		closeStatement(ps);
    		if(DSApi.isOracleServer())
    		{
    			ps = prepareStatement("insert into dsw_assignment (id,accepted , activity_id, resource_id) values (dsw_assignment_id.nextval,0, ?, ?)");
    		}
    		else if(DSApi.isFirebirdServer())
    		{
    			ps = prepareStatement("insert into dsw_assignment (id,accepted , activity_id, resource_id) " +
    					" values (gen_id(dsw_assignment_id, 1),0, ?, ?)");
    		}
    		else
    		{
    			ps = prepareStatement("insert into dsw_assignment (accepted, activity_id, resource_id) values (0, ?, ?)");
    		}
    		ps.setLong(1, activityId);
    		ps.setLong(2, resourceId);
    		ps.executeUpdate();

    	} catch (SQLException e) {
    		throw new EdmException(e);
    	} finally
    	{
    		closeStatement(ps);
    	}
    }

    public WfAssignment[]  getWfAssignments(Long documentId) throws EdmException {

    	return getWfAssignments(documentId, getPrincipalName());
    }
    /**
     * Odnajduje wszystkie zadania bie��cego u�ytkownika zwi�zane
     * z dokumentem o przekazanym identyfikatorze.
     */
    public WfAssignment[] getWfAssignments(Long documentId, String username) throws EdmException
    {
    	if (username == null) {
    		return getAllWfAssignments(documentId);
    	}
    	//To musi odwolywac sie do Managera
        String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
        PreparedStatement ps = null;
        try
        {
            String sql =
                "select distinct dsw_assignment.id from " +
                "    dsw_assignment "+nolock+
                " inner join dsw_resource "+nolock+" on dsw_assignment.resource_id = dsw_resource.id " +
                "    inner join dsw_activity "+nolock+" on dsw_activity.id = dsw_assignment.activity_id " +
                "    inner join dsw_process "+nolock+" on dsw_process.id = dsw_activity.process_id " +
                "    inner join dsw_process_context "+nolock+" on dsw_process_context.process_id = dsw_process.id " +
                "where dsw_process_context.param_name = 'ds_document_id' and " +
                "    dsw_process_context.param_value = ? " +
                "    and dsw_resource.res_key = ?";



            ps = prepareStatement(sql);

            // nie uwzgl�dnia zast�pstw ... (or res_key = ? ...)
            ps.setString(1, documentId.toString());
            ps.setString(2, username);

            List<WfAssignment> assignments = new LinkedList<WfAssignment>();
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                assignments.add(load(WfAssignmentImpl.class, new Long(rs.getLong(1))));
            }
            rs.close();
            return assignments.toArray(new WfAssignment[assignments.size()]);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            closeStatement(ps);
        }
    }

    public WfAssignment[] getAllWfAssignments(Long documentId) throws EdmException
    {
    	//To musi odwolywac sie do Managera
        String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
        PreparedStatement ps = null;
        try
        {
            String sql =
                "select distinct dsw_assignment.id from " +
                "    dsw_assignment "+nolock+
                "    inner join dsw_activity "+nolock+" on dsw_activity.id = dsw_assignment.activity_id " +
                "    inner join dsw_process "+nolock+" on dsw_process.id = dsw_activity.process_id " +
                "    inner join dsw_process_context "+nolock+" on dsw_process_context.process_id = dsw_process.id " +
                "where dsw_process_context.param_name = 'ds_document_id' and " +
                "    dsw_process_context.param_value = ? ";


            ps = prepareStatement(sql);

            // nie uwzgl�dnia zast�pstw ... (or res_key = ? ...)
            ps.setString(1, documentId.toString());

            List<WfAssignment> assignments = new LinkedList<WfAssignment>();
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                assignments.add(load(WfAssignmentImpl.class, new Long(rs.getLong(1))));
            }
            rs.close();
            return assignments.toArray(new WfAssignment[assignments.size()]);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            closeStatement(ps);
        }
    }

    /**
     * Zwraca obiekt WfAssignment przyporz�dkowany danemu identyfikatorowi
     * zadania lub rzuca wyj�tek EntityNotFoundException, je�eli obiekt nie
     * zostanie znaleziony. Metoda dzia�a tylko dla wewn�trznego workflow.
     * Zak�ada, �e zadanie nale�y do aktualnie zalogowanego u�ytkownika.
     */
    public WfAssignment getWfAssignment(ActivityId activityId)
        throws EdmException
    {
        return getWfAssignment(activityId, getPrincipalName());
    }

    /**
     * Zwraca obiekt WfAssignment przyporz�dkowany danemu identyfikatorowi
     * zadania lub rzuca wyj�tek EntityNotFoundException, je�eli obiekt nie
     * zostanie znaleziony. Metoda dzia�a tylko dla wewn�trznego workflow.
     *
     * @username nazwa u�ytkownika, kt�ry posiada to zadanie
     */
    public WfAssignment getWfAssignment(ActivityId activityId, String username)
        throws EdmException
    {
        final String wf = activityId.getWfName();
        final String activityKey = activityId.getActivityKey();

        if (InternalWorkflowService.NAME.equals(wf))
        {
            // znalezienie listy u�ytkownik�w, na kt�rych listach nale�y
            // szuka� zadania

            List<String> usernames = new ArrayList<String>(16);
            usernames.add(username);

            for (DSUser subst : getDSUser().getSubstituted())
            {
                usernames.add(subst.getName());
            }

            if (hasPermission(DSPermission.BOK_LISTA_PISM))
            {
                usernames.add("$bok");
            }

            String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";

            // TODO: czy mo�e wyst�pi� kilka element�w?
            //TO BOLI - Przerobic na prepared statement
            Collection<String> params = new Vector<String>();
            StringBuilder sql = new StringBuilder(200);
            sql.append("select asg.id from dsw_activity act "+nolock+
                " join dsw_assignment asg "+nolock+
                "on asg.activity_id = act.id " +
                "join dsw_resource res "+nolock+" on asg.resource_id = res.id "+
                "where (");
            for (int i=0; i < usernames.size(); i++)
            {
                sql.append("res.res_key = ?");
                params.add((String) usernames.get(i));
                if (i < usernames.size()-1)
                    sql.append(" OR ");
            }
            sql.append(") ");
            sql.append(" AND act.activity_key = ?");
            params.add(activityKey);

            PreparedStatement ps = null;
            try
            {
                ps = DSApi.context().prepareStatement(sql.toString());
                Iterator<String> it = params.iterator();
                int licznik = 1;
                while (it.hasNext())
                {
                	String val = it.next();
                	ps.setString(licznik, val);
                	licznik ++;
                }
                ResultSet rs = ps.executeQuery();
                if (!rs.next())
                {
                    throw new EntityNotFoundException(sm.getString("NieZnalezionoSzukanegoZadaniaNaLiscieZadanUzytkownika")+" "+ DSUser.findByUsername(username).asFirstnameLastnameName() +
                        sm.getString("PrawdopodobnieInnyUzytkownikZadekretowalToPismo")+" (activityId="+activityId+")");
                }

                long asgId = rs.getLong(1);

                while (rs.next())
                {
                    log.warn("Istnieje kolejny obiekt WfAssignment dla "+activityKey+
                        ": "+rs.getLong(1));
                }

//                WfAssignment wfAssignment = (WfAssignment) DSApi.context().session().
//                    load(WfAssignmentImpl.class, new Long(asgId));

                WfAssignment wfAssignment = DSApi.getObject(WfAssignmentImpl.class, new Long(asgId));

                if (log.isDebugEnabled())
                    log.debug("WfAssignment dla "+activityKey+": "+asgId);

                return wfAssignment;
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }
            finally
            {
                DSApi.context().closeStatement(ps);
            }
        }
        else
        {
            throw new EntityNotFoundException(sm.getString("NieZnalezionoZadania")+" "+activityKey+
            sm.getString("NieznanaNazwaWorkflow")+": "+wf);
        }
    }

    /**
     * Szuka danego zadania w�r�d zada� przydzielonych bie��cemu u�ytkownikowi
     * oraz u�ytkownikom przez niego zast�powanym.  Dzia�a tylko dla wewn�trznego
     * workflow.
     * @throws EntityNotFoundException Je�eli zadanie nie zostanie znalezione.
     */
    public WfActivity getWfActivity(ActivityId activityId)
        throws EdmException
    {
        final String wf = activityId.getWfName();
        final String activityKey = activityId.getActivityKey();

        if (InternalWorkflowService.NAME.equals(wf))
        {
            // znalezienie listy u�ytkownik�w, na kt�rych listach nale�y
            // szuka� zadania

            List<String> usernames = new ArrayList<String>(16);
            usernames.add(getPrincipalName());

            for (DSUser subst : getDSUser().getSubstituted())
            {
                usernames.add(subst.getName());
            }
            if(DSApi.context().hasPermission(getDSUser(), DSPermission.BOK_LISTA_PISM))
            {
            	usernames.add("$bok");
            }

//            if (hasPermission(DSPermission.BOK_LISTA_PISM))
//            {
//                usernames.add("$bok");
//            }

            String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
            //@TODO - niedopuszczanle ze nie jest PS
            Collection<String> params = new Vector<String>();
            StringBuilder sql = new StringBuilder(200);
            sql.append("select act.id from dsw_activity act "+nolock+
                " join dsw_assignment asg "+nolock+
                " on asg.activity_id = act.id " +
                "join dsw_resource res "+nolock+" on asg.resource_id = res.id "+
                "where (");
            for (int i=0; i < usernames.size(); i++)
            {
                sql.append("res.res_key = ?");
                params.add((String) usernames.get(i));
                if (i < usernames.size()-1)
                    sql.append(" OR ");
            }
            sql.append(") ");
            sql.append(" AND act.activity_key = ?");
            params.add(activityKey);


            PreparedStatement ps = null;
            try
            {
                ps = DSApi.context().prepareStatement(sql.toString());
                Iterator<String> it = params.iterator();
                int licznik = 1;
                while (it.hasNext())
                {
                	String val = it.next();
                	ps.setString(licznik, val);
                	licznik++;
                }
                ResultSet rs = ps.executeQuery();
                if (!rs.next())
                {
                    throw new EntityNotFoundException(sm.getString("NieZnalezionoSzukanegoZadaniaNaLiscieZadanUzytkownika")+" " + getDSUser().asFirstnameLastnameName()+ " "+
                            sm.getString("PrawdopodobnieInnyUzytkownikZadekretowalToPismo"));
                }

                long actId = rs.getLong(1);
                return DSApi.context().load(WfActivityImpl.class, new Long(actId));
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }
            finally
            {
                DSApi.context().closeStatement(ps);
            }
        }
        else if (JbpmWorkflowService.NAME.equals(wf))
        {
            return new pl.compan.docusafe.core.office.workflow.jbpm.WfActivityImpl(activityKey);
        }
        else
        {
            throw new EntityNotFoundException(sm.getString("NieZnalezionoZadania")+" "+activityKey+
            		sm.getString("NieznanaNazwaWorkflow")+": "+wf);
        }
    }

    /**
     * Szuka zadania o podanym ActivityId. U�ywana przez list�
     * zada� obserwowanych aby wy�wietla� tak�e zadania zadekretowane
     * na innych u�ytkownik�w. Dzia�a tylko dla wewn�trznego workflow.
     * @throws EntityNotFoundException Je�eli zadanie nie zostanie znalezione.
     */
    public WfActivity getWfActivityAll(ActivityId activityId)
        throws EdmException
    {
        final String wf = activityId.getWfName();
        final String activityKey = activityId.getActivityKey();

        if (InternalWorkflowService.NAME.equals(wf))
        {
            String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
            PreparedStatement ps = null;
            try
            {
                ps = DSApi.context().prepareStatement("select act.id from dsw_activity act "+nolock+
                "where act.activity_key = ?");
                ps.setString(1, activityKey);
                ResultSet rs = ps.executeQuery();
                if (!rs.next())
                {
                    throw new EntityNotFoundException(sm.getString("NieZnalezionoZadania")+" "+
                        sm.getString("PrawdopodobniePracaZpismemZostalaZakonczona"));
                }
                long actId = rs.getLong(1);
                return DSApi.context().load(WfActivityImpl.class, new Long(actId));
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }
            finally
            {
                DSApi.context().closeStatement(ps);
            }
        }
        else if (JbpmWorkflowService.NAME.equals(wf))
        {
            return new pl.compan.docusafe.core.office.workflow.jbpm.WfActivityImpl(activityKey);
        }
        else
        {
            throw new EntityNotFoundException(sm.getString("NieZnalezionoZadania")+" "+activityKey+
            		sm.getString("NieznanaNazwaWorkflow")+": "+wf);
        }
    }

    public WfProcess getWfProcess(WfProcessURN urn) throws EdmException
    {
        if (!InternalWorkflowService.NAME.equals(urn.getWorkflow()))
            throw new EdmException(sm.getString("NieznanyWorkflow")+": "+urn);

        return InternalWorkflowService.getWfProcess(urn.getKey());
    }

    /**
     * Zwraca true, je�eli u�ytkownik ma uprawnienie kancelaryjne
     * o danej nazwie w profilu z podanym kluczem.
     * @param perm Jedna ze sta�ych w klasie {@link pl.compan.docusafe.core.office.DSPermission}
     * @param userKey  klucz u�ytkownika do profilu
     */
    public boolean hasPermission(DSPermission perm, String userKey) throws EdmException
    {
    	if(hasPermission(DSApi.context().getDSUser(), perm, userKey))
    	{
    		return true;
    	}
    	else
    	{
    		DSUser[] users = DSApi.context().getDSUser().getSubstituted();
    		for (int i = 0; i < users.length; i++)
    		{
				if(hasPermission(users[i], perm, userKey))
				{
					return true;
				}
			}
    	}
    	return false;
        //return hasPermission(DSApi.context().getDSUser(), perm);
    }
    
    /**
     * Zwraca true, je�eli u�ytkownik ma uprawnienie kancelaryjne
     * o danej nazwie w praofilu bez klucza.
     * @param perm Jedna ze sta�ych w klasie {@link pl.compan.docusafe.core.office.DSPermission}
     */
    public boolean hasPermission(DSPermission perm) throws EdmException
    {
    	return hasPermission(perm, "");
    }

    /**
     * Zwraca true, je�eli u�ytkownik ma uprawnienie kancelaryjne
     * o danej nazwie w praofilu bez klucza.
     * @param perms Jedna ze sta�ych w klasie {@link pl.compan.docusafe.core.office.DSPermission}
     */
    public boolean hasPermission(DSPermission... perms) throws EdmException
    {
        boolean ok = false;
        for(DSPermission p : perms){
            if(hasPermission(p,"")){
                ok = true;
                break;
            }
        }
        return ok;
    }

    /**
     * Zwraca <code>true</code>, je�eli u�ytkownik posiada dane
     * uprawnienie kancelaryjne.
     * @param user U�ytkownik.
     * @param perm Jedna ze sta�ych zdefiniowanych w klasie
     * @param userKey klucz u�ytkownika do profilu
     *  {@link pl.compan.docusafe.core.office.DSPermission}.
     */
    public boolean hasPermission(DSUser user, DSPermission perm, String userKey) throws EdmException
    {
        // zalogowany u�ytkownik mo�e mie� dodatkowe uprawnienia
        // nadane dla wykonania konkretnej akcji
        if (user.equals(DSApi.context().getDSUser()) &&
            (privileges != null && !privileges.isEmpty() && privileges.peek().hasPermission(perm)))
        {
            if (log.isDebugEnabled())
                log.debug("hasPermission("+user.getName()+", "+perm.getName()+") - znaleziono w privileges");
            return true;
        }
        PermissionCache permCache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
        permCache.update(user.getName());
        if(Docusafe.getAdditionProperty("userProfileKeys") != null && Docusafe.getAdditionProperty("userProfileKeys").equals("true") && user.hasKeysForProfiles() && userKey != null && !"".equals(userKey)){
        	return permCache.hasPermissionWithContext(user.getName(), perm.getName(), userKey);
        }else{     	
            return permCache.hasPermission(user.getName(), perm.getName());
        }
        
    }
    /**
     * Zwraca <code>true</code>, je�eli u�ytkownik posiada dane
     * uprawnienie kancelaryjne.
     * @param user U�ytkownik.
     * @param perm Jedna ze sta�ych zdefiniowanych w klasie
     *  {@link pl.compan.docusafe.core.office.DSPermission}.
     */
    public boolean hasPermission(DSUser user, DSPermission perm) throws EdmException{
    	return hasPermission(user, perm, "");
    }

    public boolean canSetPermissions(Document document)
        throws EdmException
    {
        return /*!(document instanceof OfficeDocument) &&*/
            (document.getAuthor() != null &&
                document.getAuthor().equalsIgnoreCase(
                    DSApi.context().getPrincipalName())) ||
            (Configuration.hasExtra("business") /*&& document instanceof OfficeDocument*/);
        // TODO: je�eli OfficeDocument - sprawdza� uprawnienia jak dla kancelaryjnego
    }

    public boolean canSetPermissions(Folder folder)
        throws EdmException
    {
        return (folder.getAuthor() != null &&
                folder.getAuthor().equalsIgnoreCase(
                    DSApi.context().getPrincipalName())) ||
            (Configuration.hasExtra("business"));
        // TODO: nie pozwala� ka�demu na takie zmiany,
    }

    /**
     * Zwraca liste uprawnien do dokumentu
     * @return
     */
    public Set<PermissionBean> getDocumentPermissions(Document document) throws EdmException
    {
    	Set<PermissionBean> resp = new HashSet<PermissionBean>();
    	PreparedStatement ps = null;
    	try
    	{
    		ps = prepareStatement("select * from ds_document_permission where document_id = ?");
    		ps.setLong(1, document.getId());
    		ResultSet rs = ps.executeQuery();
    		while (rs.next())
    		{
    			PermissionBean perm = new PermissionBean(rs.getString("name"),rs.getString("subject"),rs.getString("subjecttype"));
    			resp.add(perm);
    		}
    		rs.close();
    	}
    	catch (Exception e)
    	{
    		throw new EdmException("Blad w czasie pobierania uprawnien",e);
    	}
    	finally
    	{
    		closeStatement(ps);
    	}
    	return resp;
    }
    public void grant(Document document, String[] permissions)
        throws EdmException
    {
        if (document == null)
            throw new NullPointerException("document");

        if (!canSetPermissions(document))
            throw new EdmException(sm.getString("UzytkownikNieMozeZmieniacUprawnienTegoDokumentu"));

        grant(DocumentPermission.class, document.getId(), permissions);
    }

    /**
     *
     * @param document
     * @param permissions
     * @param subject - cel nadania uprawnienia (guid/login)
     * @param subjectType
     * @throws EdmException
     */
    public void grant(Document document, String[] permissions, String subject, String subjectType) throws EdmException {
        if (document == null)
            throw new NullPointerException("document");

        if (!canSetPermissions(document))
            throw new EdmException(sm.getString("UzytkownikNieMozeZmieniacUprawnienTegoDokumentu"));

        try {
            if(Arrays.asList(permissions).contains(ObjectPermission.READ) && AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                if(ObjectPermission.USER.equals(subjectType)) {
                    JackrabbitPrivileges.instance().addReadPrivilege(document.getId(), subject, null, ObjectPermission.SOURCE_DOCUMENT);
                }
                else if(ObjectPermission.GROUP.equals(subjectType)) {
                    for(DSUser user : DSDivision.find(subject).getUsers()) {
                        JackrabbitPrivileges.instance().addReadPrivilege(document.getId(), user.getName(), subject, ObjectPermission.SOURCE_DOCUMENT);
                    }
                }
            }
        } catch (Exception e) {
            throw new EdmException(e);
        }

        grant(DocumentPermission.class, document.getId(), permissions, subject, subjectType);
    }


    /**
     * Dodaje uprawnienie na podstawie obiektu PermissionBean
     * @param document
     * @param permission
     * @throws EdmException
     */
    public void grant(Document document, PermissionBean permission) throws EdmException
    {
    	grant (document, new String[] { permission.getPermission()}, permission.getSubject(), permission.getSubjectType());
    }

    public void grant(Folder folder, String[] permissions,
                      String subject, String subjectType)
        throws EdmException
    {
        if (folder == null)
            throw new NullPointerException("document");

        if (!canSetPermissions(folder))
            throw new EdmException(sm.getString("UzytkownikNieMozeZmieniacUprawnienTegoFolderu"));

        grant(FolderPermission.class, folder.getId(), permissions,
            subject, subjectType);
    }

    public void grant(Folder folder, String[] permissions)
        throws EdmException
    {
        if (folder == null)
            throw new NullPointerException("document");

        if (!canSetPermissions(folder))
            throw new EdmException(sm.getString("UzytkownikNieMozeZmieniacUprawnienTegoFolderu"));

        grant(FolderPermission.class, folder.getId(), permissions);
    }

    private void grant(Class permissionClass, Long id, String[] permissions)
        throws EdmException
    {
        grant(permissionClass, id, permissions,
            DSApi.context().getPrincipalName(),
            ObjectPermission.USER);
    }

    private void grant(Class permissionClass, Long id, String[] permissions, String subject, String subjectType) throws EdmException {
        try {
            for (int i=0; i < permissions.length; i++) {
                ObjectPermission permission = (ObjectPermission) permissionClass.getConstructor(Long.class).newInstance(id);

                permission.setName(permissions[i]);
                permission.setSubject(subject);
                permission.setSubjectType(subjectType);

                try
                {
                    DSApi.context().session().load(permissionClass, permission);
                    // ? DSApi.context().session().save(permission);
                }
                catch (ObjectNotFoundException e)
                {
                    DSApi.context().session().save(permission);
                }
            }
        }
        catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
        catch (Exception e) {
            log.error(e.getMessage()+", permissionClass="+permissionClass+", id="+id);
            throw new EdmException(sm.getString("BladPodczasNadawaniaUprawnienObiektowi"), e);
        }
    }

    /**
     * Odebranie upranwienia od dokumentu
     * @param doc
     * @throws EdmException
     */
    public void revoke(Document doc,PermissionBean permissionBean) throws EdmException {
        PreparedStatement ps = null;
        try
        {
            session().flush();
            ps = prepareStatement("delete from ds_document_permission where name = ? and subjectType = ? and subject = ? and document_id = ?");
            ps.setString(1, permissionBean.getPermission());
            ps.setString(2, permissionBean.getSubjectType());
            ps.setString(3, permissionBean.getSubject());
            ps.setLong(4, doc.getId().longValue());
            ps.execute();

            if(ObjectPermission.READ.equals(permissionBean.getPermission()) && AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                if(ObjectPermission.USER.equals(permissionBean.getSubjectType())) {
                    JackrabbitPrivileges.instance().revokeReadPrivilege(doc.getId(), permissionBean.getSubject(), null, ObjectPermission.SOURCE_DOCUMENT);
                }
                else if(ObjectPermission.GROUP.equals(permissionBean.getSubjectType())) {
                    for(DSUser user : DSDivision.find(permissionBean.getSubject()).getUsers()) {
                        JackrabbitPrivileges.instance().revokeReadPrivilege(doc.getId(), user.getName(), permissionBean.getSubject(), ObjectPermission.SOURCE_DOCUMENT);
                    }
                }
            }

        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (Exception e) {
            throw new EdmException(e);
        }
        finally
        {
            closeStatement(ps);
        }
    }
    /** Czyszczenie uprawnien w archiwum dla dokumentu. Jesli nie wyczyscimy a zostanie zmieniony
    *dockind to moze byc naruszenie klucza wielowartosciowego*/
    public void clearPermissions(Document doc) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            session().flush();
            ps = prepareStatement("delete from ds_document_permission where document_id = ?");
            ps.setLong(1, doc.getId().longValue());
            ps.execute();

        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            closeStatement(ps);
        }
    }

    public void copyPermissions(Document from, Document to) throws EdmException
    {
    	//to do - to nie moze tak zostac
        Statement st = null;
        try
        {
        	/**
        	 * @TODO - do przerobienia na PS
        	 */
        	st = createStatement();
            st.executeUpdate("insert into "+DSApi.getTableName(DocumentPermission.class)+
                " (document_id, subject, subjecttype, name, negative) " +
                "select "+to.getId()+", subject, subjecttype, name, negative from "+
                DSApi.getTableName(DocumentPermission.class)+" where document_id = "+from.getId());
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            closeStatement(st);
        }
    }


    /* Metody pomocnicze */

    /**
     * Ta funkcja jet tylko dlatego, �e hasArchivepermission pozwala na
     * wsysztko dla dok. kancelaryjnych, a w editdocument nie mo�na usuwa�
     * takim pismom za��cznik�w
     */
    public boolean hasObjectPermission(Document document, String name) throws EdmException
    {
    	return hasObjectPermission(document, name, null);
    }

    /**
     * Ta funkcja jet tylko dlatego, �e hasArchivepermission pozwala na
     * wsysztko dla dok. kancelaryjnych, a w editdocument nie mo�na usuwa�
     * takim pismom za��cznik�w
     */
    public boolean hasObjectPermission(Document document, String name,Long binderId) throws EdmException
    {
        if (DSApi.context().isAdmin())
            return true;

        if (document.getId() == null)
        {
            return true;
        }
        //jesli z teczki
        if(binderId != null)
        	return document.canRead(binderId);

        Long id;
        Class permissionClass;
        String hql;

        id = document.getId();
        permissionClass = DocumentPermission.class;
        hql = "select perm from perm in class "+permissionClass.getName()+
            " where perm.documentId = ? and perm.name = ?";

        try
        {
            DSUser user = getDSUser();

            DSDivision[] divisions = user.getDivisions();

            List permissions = DSApi.context().session().createQuery(hql)
                    .setParameter(0, id, Hibernate.LONG)
                    .setParameter(1, name, Hibernate.STRING).list();
//                    .find(
//                hql,
//                new Object[] { id, name },
//                new Type[] { Hibernate.LONG, Hibernate.STRING });

            boolean hasPermission = false;

            for (Iterator iter=permissions.iterator(); iter.hasNext(); )
            {
                ObjectPermission permission = (ObjectPermission) iter.next();

                if (ObjectPermission.ANY.equals(permission.getSubjectType()) ||
                    (ObjectPermission.USER.equals(permission.getSubjectType()) &&
                     user.getName().equals(permission.getSubject())))
                {
                    // napotkanie prawa negatywnego anuluje napotkane
                    // wcze�niej prawa pozytywne dla tego samego u�ytkownika
                    if (permission.isNegative())
                    {
                        hasPermission = false;
                        break;
                    }
                    hasPermission = true;
                }
                else if (ObjectPermission.GROUP.equals(permission.getSubjectType()) &&
                    divisions.length > 0)
                {
                    for (int i=0; i < divisions.length; i++)
                    {
                        if (divisions[i].isGroup() &&
                            divisions[i].getGuid().equals(permission.getSubject()))
                        {
                            if (permission.isNegative())
                            {
                                hasPermission = false;
                                break;
                            }
                            hasPermission = true;
                        }
                    }
                }
            }

            return hasPermission;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public boolean documentPermission(Long id, String permission, DSUser user)
        throws EdmException
    {
        return objectPermission(DocumentPermission.class, id, permission, user);
    }

    public boolean documentPermission(Long id, String permission)
        throws EdmException
    {
        return objectPermission(DocumentPermission.class, id, permission, DSApi.context().getDSUser());
    }

    public boolean folderPermission(Long id, String permission, DSUser user)
        throws EdmException
    {
        return objectPermission(FolderPermission.class, id, permission, user);
    }

    public boolean folderPermission(Long id, String permission)
        throws EdmException
    {
        return objectPermission(FolderPermission.class, id, permission, DSApi.context().getDSUser());
    }

    private Map<String, Object> cache;

    public void setAttribute(String name, Object value)
    {
        if (cache == null)
            cache = new HashMap<String, Object>();
        cache.put(name, value);
    }

    public Object getAttribute(String name)
    {
        if (cache != null)
            return cache.get(name);
        return null;
    }

    /**
     * Dodaje obiekt do obserwowanych dla zalogowanego
     * @param urn
     * @throws EdmException
     */
    public void watch(URN urn) throws EdmException
    {
    	watch(urn, getPrincipalName());
    }

    /**
     * Sprawdza czy zalogowany u�ytkownik posiada obiekt w obserwowanych
     * @param urn
     * @return
     * @throws EdmException
     */
    public boolean hasWatch(URN urn) throws EdmException
    {
        return hasWatch(urn, getPrincipalName());
    }

    /**
     * Sprawdza czy u�ytkownik posada obiekt w obserwowanych
     * @param urn
     * @return
     * @throws EdmException
     */
    public boolean hasWatch(URN urn, String username) throws EdmException{
        boolean ok = false;

        PreparedStatement ps = null;
        ResultSet rs = null;
        try
        {
            ps = prepareStatement("select * from ds_watchlist where username = ? and urn = ?");
            ps.setString(1, username);
            ps.setString(2, urn.toExternalForm());
            rs = ps.executeQuery();

            if (rs.next())
                ok = true;

        }catch(Exception e){
            log.error("", e);
            throw new EdmException(e);
        }finally
        {
            closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }
        return ok;
    }
    /**
     * Dodaje obiekt do obserwowanych
     * @param urn
     * @throws EdmException
     */
    public void watch(URN urn, String username) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            boolean hasUrn = hasWatch(urn, username);
            if (!hasUrn)
            {
                ps = prepareStatement("insert into ds_watchlist (username, urn) values (?, ?)");
                ps.setString(1, username);
                ps.setString(2, urn.toExternalForm());
                ps.executeUpdate();

                if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                    String doc = urn.toExternalForm();
                    String[] id = null;
                    if (doc.contains(","))
                        id = doc.split(",");
                    else
                        id[0]=doc;

                    JackrabbitPrivileges.instance().addReadPrivilege(Long.parseLong(id[0].split(":")[1]), username, null, ObjectPermission.SOURCE_WATCH);

                    if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                        DocumentURN documentURN = new DocumentURN(urn.toExternalForm());
                        JackrabbitXmlSynchronizer.createEvent(documentURN.getId());
                    }
                }
            }
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (Exception e) {
            throw new EdmException(e);
        }
        finally
        {

            closeStatement(ps);
        }
        
       
        if(AvailabilityManager.isAvailable("addToWatch.forSubstitudes"))
        {
        	addToWatchForSubstitiudes(urn, username);
        }
    }
    
    /**
     * Dodaje do obserwowanych dla os�b zast�puj�cych
     * @param urn
     * @param username
     */
    public void addToWatchForSubstitiudes(URN urn, String username)
    {
    	try
		{
			List<SubstituteHistory> substitudes = SubstituteHistory.listActualSubstitutes(username);
			
			for(SubstituteHistory substitude : substitudes)
			{
				watch(urn, substitude.getSubstLogin());
			}
		} catch (EdmException e)
		{
			log.error("", e);
		}
    }

    /**
     * Usuwa obiekt z obserwowanych dla zalogowanego
     * @param urn
     * @throws EdmException
     */
    public void unwatch(URN urn) throws EdmException
    {
        unwatch(urn, getPrincipalName());
    }

    /**
     * Usuwa obiekt z obserwowanych dla u�tkownika
     * @param urn
     * @param username
     * @throws EdmException
     */
    public void unwatch(URN urn, String username) throws EdmException
    {
        PreparedStatement ps = null;
        String doc = urn.toExternalForm();
        String[] id = null;
        if (doc.contains(",")) 
            id = doc.split(","); 
        else
        	id[0]=doc;
        
        try
        {
            ps = prepareStatement("delete from ds_watchlist where username = ? and urn like '" + id[0] +"%'");
            ps.setString(1, username);
            ps.executeUpdate();

            if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                JackrabbitPrivileges.instance().revokeReadPrivilege(Long.parseLong(id[0].split(":")[1]), username, null, ObjectPermission.SOURCE_WATCH);
            }

            if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                DocumentURN documentURN = new DocumentURN(urn.toExternalForm());
                JackrabbitXmlSynchronizer.createEvent(documentURN.getId());
            }
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (Exception e) {
            throw new EdmException(e);
        }
        finally
        {
            closeStatement(ps);
        }
    }
    
    public void unwatch(Long id) throws EdmException{
    	PreparedStatement ps = null;
        try
        {
            ps = prepareStatement("delete from ds_watchlist where urn like 'document:"+id+",%'");
            log.info(ps.toString());
            ps.executeUpdate();

            if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                JackrabbitPrivileges.instance().revokeReadPrivilege(id, null, null, ObjectPermission.SOURCE_WATCH);
            }

            if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                JackrabbitXmlSynchronizer.createEvent(id);
            }
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (Exception e) {
            throw new EdmException(e);
        }
        finally
        {
            closeStatement(ps);
        }
    }

    /**
     * Lista obiekt�w obserwowanych przez zalogowanego u�ytkownika.
     */
    public List<URN> watches(DSUser user) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = prepareStatement("select urn from ds_watchlist where username = ?");
            ps.setString(1, user != null ? user.getName() : getPrincipalName());
            ResultSet rs = ps.executeQuery();
            List<URN> results = new LinkedList<URN>();
            String n = user.getName();
            String n2 = getPrincipalName();
            String s= null;
            while (rs.next())
            {
                results.add(URN.parse(rs.getString(1)));
                s = rs.getString(1);
            }
            return results;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	closeStatement(ps);
        }
    }

    /**
     * <p>
     *     Lista username�w obserwuj�cych dokument.
     * </p>
     *
     * @param document
     * @return list� username�w obserwuj�cych dokument
     */
    public List<String> watches(Document document) throws EdmException {
        URN urn = URN.create(document);

        List<String> users = new ArrayList<String>();

        PreparedStatement ps = null;
        ResultSet rs = null;
        try
        {
            ps = prepareStatement("select username from ds_watchlist where urn = ?");
            ps.setString(1, urn.toExternalForm());
            rs = ps.executeQuery();

            while (rs.next())
            {
                users.add(rs.getString(1));
            }
        }catch(Exception e){
            log.error("[watches] error", e);
            throw new EdmException(e);
        }finally
        {
            closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }

        return users;
    }

    @SuppressWarnings("unchecked")
    public <T> T load(Class<T> clazz, Serializable id) throws EdmException
    {
        try
        {
            return (T) session().load(clazz, id);
        }
        catch (ObjectNotFoundException e)
        {
            throw new EntityNotFoundException(clazz, id);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Metoda sprawdza, czy u�ytkownik ma nazwane uprawnienie do obiektu.
     * Nie jest brane pod uwag� jego rola administratora lub inne okoliczno�ci.
     * @param permissionClass DocumentPermission.class lub FolderPermission.class
     * @param objectId Identyfikator dokumentu lub folderu.
     * @param permissionName Nazwa uprawnienia.
     * @param user U�ytkownik, kt�rego uprawnienia s� przeszukiwane.
     */
    private boolean objectPermission(Class permissionClass, Long objectId,
                                     String permissionName, DSUser user)
        throws EdmException
    {
        // wyszukiwanie uprawnienia musi si� odby� po wszystkich dzia�ach,
        // w kt�rych jest u�ytkownik, poniewa� mog� istnie� w przysz�o�ci
        // uprawnienia negatywne, kt�re s� nadrz�dne wobec pozosta�ych, a
        // wi�c nale�y odczyta� wszystkie uprawnienia o danej nazwie, jakie
        // mo�e mie� u�ytkownik

        String hql;

        if (permissionClass == DocumentPermission.class)
        {
            hql = "select perm from perm in class "+DocumentPermission.class.getName()+
                " where perm.documentId = ? and perm.name = ?";
        }
        else if (permissionClass == FolderPermission.class)
        {
            hql = "select perm from perm in class "+FolderPermission.class.getName()+
                " where perm.folderId = ? and perm.name = ?";
        }
        else
        {
            throw new IllegalArgumentException(sm.getString("NieznanaKlasaUprawnienia")+": "+permissionClass);
        }

        try
        {
            DSDivision[] divisions = user.getDivisions();
            
            List permissions = DSApi.context().classicSession().find(
                hql,
                new Object[] { objectId, permissionName },
                new Type[] { Hibernate.LONG, Hibernate.STRING });

            boolean hasPermission = false;

            for (Iterator iter=permissions.iterator(); iter.hasNext(); )
            {
                ObjectPermission permission = (ObjectPermission) iter.next();

                if (ObjectPermission.ANY.equals(permission.getSubjectType()) ||
                    (ObjectPermission.USER.equals(permission.getSubjectType()) &&
                     user.getName().equals(permission.getSubject())))
                {
                    // napotkanie prawa negatywnego anuluje napotkane
                    // wcze�niej prawa pozytywne dla tego samego u�ytkownika
                    if (permission.isNegative())
                    {
                        hasPermission = false;
                        break;
                    }
                    hasPermission = true;
                }
                else if (ObjectPermission.GROUP.equals(permission.getSubjectType()) &&
                    divisions.length > 0 &&
                    (!AvailabilityManager.isAvailable("search.byOfficeRole") || 
                    		DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD) || DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD)))
                {
                    for (int i=0; i < divisions.length; i++)
                    {                 	
                        if ((divisions[i].isGroup() || AvailabilityManager.isAvailable("permission.byDivision")) &&
                            divisions[i].getGuid().equals(permission.getSubject()))
                        {
                            if (permission.isNegative())
                            {
                                hasPermission = false;
                                break;
                            }
                            hasPermission = true;
                        }
                    }
                }
            }
            if(AvailabilityManager.isAvailable("search.byOfficeRole")  && DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD))
            	hasPermission= true;
            return hasPermission;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca sesj� Hibernate zwi�zan� z tym kontekstem. Sesja pobierana
     * jest z obiektu DSApi przy pierwszym wywo�aniu tej metody.
     * @param create Je�eli bie��cy kontekst nie jest zwi�zany z sesj�
     * Hibernate, sesja zostanie utworzona przez t� metod� tylko wtedy,
     * gdy parametr create b�dzie mia� warto�� true.
     */
    private org.hibernate.Session getSession(boolean create) throws EdmException
    {
        if (closed)
            throw new IllegalStateException(sm.getString("KontekstZostalZamkniety"));

        if (session == null || !session.isOpen())
        {
            if (!create)
                return null;
            session = DSApi.openSession();
        }

        return session;
    }

   /**
    * Metoda rejestruje zgloszenie odswiezenia listy do kolejki
    * @param bean
    * TODO:Nalezy doda� sprawdzenie czy transakcja jest otwarta,
    *  je�li nie to po co rejestrowa� i tak nie zostanie wykonane
    */
   public void registerTasklistRefresh(RefreshRequestBean bean)
   {
	   if(refreshTasklist == null)
		   refreshTasklist = new HashMap<Long, RefreshRequestBean>();
		
	 //  System.out.println("REGISTER IN TRAN calling from "+ callingStack);
	   refreshTasklist.put(bean.getId(), bean);
   }

   public Map<Long,RefreshRequestBean> getRefreshTasklist()
   {
	   return refreshTasklist;
   }

   /**
    * Metoda kontroluje otwarte obiekty statement
    * Jesli ktorych byl niezamkniety to go zamykamy
    */
   protected void checkOpenedStatements()
   {
	   Collection<Statement> oS = new ArrayList<Statement>(openStatements);
	   for(Statement stm:oS)
	   {
		   log.warn("Awaryjne zamkniecie statementu "+stm.toString());
		   org.apache.commons.dbutils.DbUtils.closeQuietly(stm);
	   }
	   openStatements.clear();
   }

   /**
    * Sluszna metoda na zamykanie statementow
    * @param stm
    */
   public void closeStatement(Statement stm)
   {
	   try
	   {
		   org.apache.commons.dbutils.DbUtils.closeQuietly(stm);
		   openStatements.remove(stm);
	   }
	   catch (Exception e)
	   {
		   log.error("Blad w czasie zamykania polaczenia",e);
	   }
   }

   /**
    * Sluszna metoda na zamykanie statementow
    * @param stms
    */
   public void closeStatements(Statement... stms){
       for(Statement s: stms){
           closeStatement(s);
       }
   }


    public CallableStatement prepareCall(String sql) throws SQLException, EdmException
    {
        CallableStatement stmt = session().connection().prepareCall(sql);
        openStatements.add(stmt);
        return stmt;
    }

   /**
    * Jedyna dobra metoda na pobieranie statementow
    * @param sql
    * @return
    * @throws SQLException
    * @throws EdmException
    */

   public PreparedStatement prepareStatement(StringBuffer sql) throws SQLException, EdmException
   {
	   return prepareStatement(sql.toString());
   }

   /**
    * Jedyna dobra metoda na pobieranie statementow
    * @param sql
    * @return
    * @throws SQLException
    * @throws EdmException
    */

   public PreparedStatement prepareStatement(StringBuilder sql) throws SQLException, EdmException
   {
	   return prepareStatement(sql.toString());
   }
   /**
    * Jedyna dobra metoda na pobieranie statementow
    * @param sql
    * @return
    * @throws SQLException
    * @throws EdmException
    */
   public PreparedStatement prepareStatement(String sql) throws SQLException, EdmException
   {
	   PreparedStatement ps = session().connection().prepareStatement(sql);
	   openStatements.add(ps);
	   return ps;
   }

   public PreparedStatement prepareStatement(String sql,int param1, int param2) throws SQLException, EdmException
   {
	   PreparedStatement ps = session().connection().prepareStatement(sql,param1,param2);
	   openStatements.add(ps);
	   return ps;
   }
   /**
    * Jedyna poprawna metoda na utworzenie statementu
    * @return
    * @throws SQLException
    * @throws EdmException
    */
   public Statement createStatement() throws SQLException, EdmException
   {
	   Statement stm = session().connection().createStatement();
	   openStatements.add(stm);
	   return stm;
   }

   public Statement createStatement(int param1,int param2) throws SQLException, EdmException
   {
	   Statement stm = session().connection().createStatement(param1,param2);
	   openStatements.add(stm);
	   return stm;
   }

   public EncryptionCredentials credentials()
   {
	   return DSApi.credentials(getUserPrincipal().getName());
   }

   /**
    * Tworzy obiekt NativeCriteria
    * 
    * @param tableName
    * @param tableAlias
    * @return
    * @throws EdmException
    */
   public NativeCriteria createNativeCriteria(String tableName, String tableAlias)
   		throws EdmException
   {
   	   return new NativeCriteria(session(), tableName, tableAlias);
   }
}
