package pl.compan.docusafe.core.labels;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.tasklist.Task;

public class LabelsManager {

	/**
	 * Sprawdza czy etykieta o danej nazwie istnieje dla danego uzytkownika
	 * @param name - nazwa etykiety
	 * @param owner - nazwa uzytwkownika
	 * @param onwerType - type wlasnosci etykiety
	 * @return
	 * @throws EdmException
	 */
	public static boolean existsUserLabel(String name, String username, String type) throws EdmException {
		String in = "?,?";
		Collection<String> guids = new Vector<String>();
		guids.add(username);
		guids.add(Label.SYSTEM_LABEL_OWNER);
		for(DSDivision dsd: DSUser.findByUsername(username).getAllDivisions())
		{
			in += ",?";
			guids.add(dsd.getGuid());
		}
		String q =  "select d from d in class "+ Label.class.getName() + " where d.name =? and d.type = ?" +
        " and (d.readRights in (" + in + ") or d.writeRights in (" + in + ") or d.modifyRights in (" + in + "))";

		Query query = DSApi.context().session().createQuery(q);
		query.setString(0,name);
		query.setString(1,type);
		int licznik = 2;
		for (int i=0;i<3;i++)
		{
			Iterator<String> it = guids.iterator();
			while (it.hasNext())
			{
				String guid = it.next();
				query.setString(licznik, guid);
				licznik ++;
			}
		}
		List<Label> list = query.list();
        return list.size() > 0;
	}

	public static boolean isFlagPresent(long documentId, long labelId) throws EdmException{
		return existsDocumentToLabelBean(documentId, labelId);
	}

	public static boolean existsDocumentToLabelBean(long documentId, long labelId) throws EdmException {
    	String q =  "select d from d in class "+ DocumentToLabelBean.class.getName()+ " where d.documentId=? and d.labelId=?";
    	Query query = DSApi.context().session().createQuery(q);
    	query.setLong(0,documentId);
    	query.setLong(1,labelId);
		List<DocumentToLabelBean> list = query.list();
        return list.size() > 0;
    }

	public static boolean existsDocumentToLabelBeanByName(Long documentId, String labelName) throws EdmException {
    	String q =  "select d from d in class "+ DocumentToLabelBean.class.getName()+
    	" ,c in class "+Label.class.getName()+
    	" where d.documentId = ? and d.labelId = c.id and c.name = ?";
    	Query query = DSApi.context().session().createQuery(q);
    	query.setLong(0,documentId);
    	query.setString(1,labelName);
		List<DocumentToLabelBean> list = query.list();
        return list.size() > 0;
    }

	public static boolean existsLabel(String name, String type) throws EdmException {
		String q = "select d from d in class "+ Label.class.getName() + " where d.name=? and d.type=?";
		Query query = DSApi.context().session().createQuery(q);
		query.setString(0, name);
		query.setString(1, type);
		List<Label> list = query.list();
        return list.size() > 0;
	}


	public static void addLabel(Long[] documentIds, Long labelId, String username) throws EdmException
	{
		try
		{
			DSApi.context().begin();
			for(int i=0; i < documentIds.length;i++)
			{
				addLabel(documentIds[i], labelId, username);
				//odswiezenie listy zadan dla dokumentu bo sie walilo z listy zadan przeciez jest w funkcji addLabel
				//TaskSnapshot.updateLabelByDocumentId(documentIds[i],"anyType");
			}
			DSApi.context().commit();
		}
		catch(EdmException e)
		{
			DSApi.context().setRollbackOnly();
			throw new EdmException(e);
		}
	}
	/**
	 * Nadaje etykiete danemu dokumentowi dla danego usera
	 * @param documentId  - id dokumentu ktory dostanie etykiete
	 * @param labelId id - etykiety
	 * @param user - nazwa usera dla ktorego bedzie istniec powiazanie dokument-etykieta
	 * @return
	 * @throws EdmException
	 */
	public static boolean addLabel(Long documentId, Long labelId, String username) throws EdmException
	{
		if(existsDocumentToLabelBean(documentId, labelId)) return false;
		Label label = findLabelById(labelId);
		DocumentToLabelBean dtlb;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		if(label.getInactionTime()!=null && label.getInactionTime() > 0)
		{
			cal.add(Calendar.HOUR, label.getInactionTime());
			if(AvailabilityManager.isAvailable("label.actiontime.onStartDay"))
			{
				cal.set(Calendar.HOUR, 5);
				cal.set(Calendar.AM_PM, Calendar.AM);
			}
		}
		dtlb = new DocumentToLabelBean();
		dtlb.setDocumentId(documentId);
		dtlb.setLabelId(labelId);
		dtlb.setCtime(new Date());
		dtlb.setHidding(label.getHidden());
		dtlb.setUsername(username);
		if(label.getInactionTime()!=null && label.getInactionTime() > 0)
			dtlb.setActionTime(cal.getTime());
		Persister.create(dtlb);
		//Zapis informacji m
		TaskSnapshot.updateLabelByDocumentId(documentId,"anyType");
		String descParams = label.getName()+(label.getDescription() == null ? "" :"("+label.getDescription()+")");
		pl.compan.docusafe.core.datamart.DataMartManager.storeEvent(documentId, "document."+label.getType()+".add", username, descParams);
		return true;
	}

	/**
	 * Nadaje etykiete o podanej nazwie danemu dokumentowi dla danego usera
	 * @param documentId  - id dokumentu ktory dostanie etykiete
	 * @param labelName name - etykiety
	 * @param user - nazwa usera dla ktorego bedzie istniec powiazanie dokument-etykieta
	 * @param onStartDay - czy ustawi� na poczatek dnia zmine etykiety na nastepna
	 * @return true je�li nowa etykieta zosta�a dodana, false w pozosta�ych przypadkach
	 * @throws EdmException
	 */
	public static boolean addLabelByName(long documentId, String labelName, String username,Boolean onStartDay) throws EdmException
	{
		if(existsDocumentToLabelBeanByName(documentId, labelName)) return false;
		Label label = findLabelByName(labelName);
		DocumentToLabelBean dtlb;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		if(label.getInactionTime()!=null && label.getInactionTime() > 0)
		{
			cal.add(Calendar.HOUR, label.getInactionTime());
			if(onStartDay || AvailabilityManager.isAvailable("label.actiontime.onStartDay"))
			{
				cal.set(Calendar.HOUR, 5);
				cal.set(Calendar.AM_PM, Calendar.AM);
			}
		}
		dtlb = new DocumentToLabelBean();
		dtlb.setDocumentId(documentId);
		dtlb.setLabelId(label.getId());
		dtlb.setCtime(new Date());
		dtlb.setHidding(label.getHidden());
		dtlb.setUsername(username);
		if(label.getInactionTime()!=null && label.getInactionTime() > 0)
			dtlb.setActionTime(cal.getTime());
		Persister.create(dtlb);
		//Zapis informacji m

		TaskSnapshot.updateLabelByDocumentId(documentId,"anyType");
		String descParams = label.getName()+(label.getDescription() == null ? "" :"("+label.getDescription()+")");
		pl.compan.docusafe.core.datamart.DataMartManager.storeEvent(documentId, "document."+label.getType()+".add", username, descParams);
		return true;
	}

	/**
	 * zwraca liste etykiet dla dokumentu widocznych dla danego uzytwkownika
	 * @param documentId - id dokumentu
	 * @param username - nazwa uzytwkonika
	 * @return List<Label> - lista etykiet dla dokumentu i uzytkownika
	 * @throws EdmException
	 */
	public static List<Label> getReadLabelsForDocument(Long documentId, String username, String type) throws EdmException
	{
		Query query = createLabelsForDocumentQuery(documentId,username, type,true);
		return query.list();
	}

	protected static Query createLabelsForDocumentAndOwnQuery(Long documentId, String username, String type,boolean readRights, boolean own) throws EdmException
	{
		return 	createLabelsForDocumentAndOwn(documentId, username, type,readRights, own);
	}

	protected static Query createLabelsForDocumentQuery(Long documentId, String username, String type,boolean readRights) throws EdmException
	{
		return 	createLabelsForDocumentQuery(documentId, username, type,readRights,false);
	}

	public static List<Label> getLabelsForDocument(Long documentId, String username, String type, boolean readRights, boolean writeRights, boolean withModifiable) throws EdmException {
		return createLabelsForDocumentQuery(documentId, username, type, readRights, writeRights, withModifiable).list();
	}

	/** metoda ponizej (pod ta) ma troche dziwna semantyke, wiec napisalem swoja (plus powyzszy akcesor)
	 * ale tamta na razie zostawiam, mozna potem to przeanalizowac i przeciazyc
	 * tutaj read, write rights to flagi decydujace czy chcemy sprawdzic odpowiednie uprawnienia,
	 * natomiast modifiable jezeli jest null to nie chcemy sprawdzac, wpp znajdujemy tylko te ze wskazanym modifiable
	 */
	protected static Query createLabelsForDocumentQuery(Long documentId, String username, String type,boolean readRights, boolean writeRights, Boolean withModifiable) throws EdmException
	{
		Collection<String> guids = new Vector<String>();
		String q = "select d from d in class "+ Label.class.getName() + ",p in class "+ DocumentToLabelBean.class.getName()+
        " where d.id = p.labelId and p.documentId=? and d.type=? and ";
		boolean addAnd = false;
		if (readRights)
		{
			q += "d.readRights";
			q += " in (?,?";
			guids.add(username);
			guids.add(Label.SYSTEM_LABEL_OWNER);
	        for(DSDivision dsd: DSUser.findByUsername(username).getAllDivisions())
			{
				q += ",?";
				guids.add(dsd.getGuid());

			}
			q += ")";
			addAnd = true;
		}
		if (writeRights)
		{
			if (addAnd) {
				q+= " and ";
			}
			q += "d.writeRights";
			q += " in (?,?";
			guids.add(username);
			guids.add(Label.SYSTEM_LABEL_OWNER);
	        for(DSDivision dsd: DSUser.findByUsername(username).getAllDivisions())
			{
				q += ",?";
				guids.add(dsd.getGuid());

			}
			q += ")";
		}
		if (withModifiable != null)
		{
			q += " and modifiable=";
			if (withModifiable)
			q+="true";
			else 
			q+="false";
			
		}
		//modifiable=0
		Query resp = DSApi.context().session().createQuery(q);
		resp.setLong(0, documentId);
		resp.setString(1, type);
		
		int licznik = 2;
		Iterator<String> it = guids.iterator();
		while (it.hasNext())
		{
			String guid = it.next();
			resp.setString(licznik, guid);
			licznik++;
		}
		/*if (withModifiable != null)
		{
			q += " and modifiable=?";
			resp.setBoolean(licznik, withModifiable);
			
		}*/
		return resp;
	}

	protected static Query createLabelsForDocumentQuery(Long documentId, String username, String type,boolean readRights,boolean withModifiable) throws EdmException
	{
		Collection<String> guids = new Vector<String>();
		String q = "select d from d in class "+ Label.class.getName() + ",p in class "+ DocumentToLabelBean.class.getName()+
        " where d.id = p.labelId and p.documentId=? and d.type=? and ";
		if (readRights)
		{
			q += "d.readRights";
		}
		else
		{
			q += "d.writeRights";
		}
		q += " in (?,?";
		guids.add(username);
		guids.add(Label.SYSTEM_LABEL_OWNER);
        for(DSDivision dsd: DSUser.findByUsername(username).getAllDivisions())
		{
			q += ",?";
			guids.add(dsd.getGuid());

		}
		q += ")";
		/*if (withModifiable)
		{
			q += " and modifiable=";
			if (readRights)
			{
				q+="0";
			}
			else
			{
				q+="1";
			}
		}*/
		//modifiable=0
		Query resp = DSApi.context().session().createQuery(q);
		resp.setLong(0, documentId);
		resp.setString(1, type);
		int licznik = 2;
		Iterator<String> it = guids.iterator();
		while (it.hasNext())
		{
			String guid = it.next();
			resp.setString(licznik, guid);
			licznik++;
		}
		
		if (withModifiable)
		{
			q += " and modifiable=?";
			resp.setBoolean(licznik, readRights);
			
		}
		return resp;

	}


	protected static Query createLabelsForDocumentAndOwn(Long documentId, String username, String type,boolean readRights, boolean own) throws EdmException
	{
		Collection<String> guids = new Vector<String>();
		String q = "select d from d in class "+ Label.class.getName() + ",p in class "+ DocumentToLabelBean.class.getName()+
        " where d.id = p.labelId and p.documentId=?";
		
		if (!type.equals(Label.FLAG_AND_LABEL_TYPE))
		{
			q += " and d.type=?";
		}

		
		if (readRights)
		{
			q += " and d.readRights";
		}
		else
		{
			q += " and d.writeRights";
		}
		q += " = ?";

		guids.add(own ? username : Label.SYSTEM_LABEL_OWNER);

		Query resp = DSApi.context().session().createQuery(q);
		resp.setLong(0, documentId);
		int licznik = 1;
		
		if (!type.equals(Label.FLAG_AND_LABEL_TYPE))
		{
			resp.setString(1, type);
			licznik = 2;
		}
		Iterator<String> it = guids.iterator();
		while (it.hasNext())
		{
			String guid = it.next();
			resp.setString(licznik, guid);
			licznik++;
		}
		return resp;

	}



	public static List<Label> getReadLabelsForDocument(Long documentId, String username, String type, boolean own) throws EdmException
	{

		Query query = createLabelsForDocumentAndOwnQuery(documentId,username, type,true,own);
		return query.list();
	}
	/**
	 * Zwraca liste etykiet dla dokumentu zapisywalnych dla danego uzytwkownika ze wskazaniem czy maja to byc modyfikowalne czy niemodyfikowalne
	 * @param documentId
	 * @param modifiable
	 * @param username
	 * @return
	 * @throws EdmException
	 */
	public static List<Label> getWriteLabelsForDocument(Long documentId, String username, String type) throws EdmException
	{
		Query query = createLabelsForDocumentQuery(documentId,username, type,true,true);
		return query.list();

	}

	public static List<Label> getNonModifiableLabelsForDocument(Long documentId, String username, String type) throws EdmException
	{
		Query query = createLabelsForDocumentQuery(documentId,username, type,false,true);
		return query.list();
	}


	/**
	 * Zwraca liste beanow (powiazan dokument-etykieta) dla danego dokuemntu i uzytkownika
	 * @param documentId
	 * @param username
	 * @return
	 * @throws EdmException
	 */
	public static List<DocumentToLabelBean> getReadBeansForDocument(Long documentId, String username, String type)throws EdmException
	{
		Collection<String> guids = new Vector<String>();
		String q = "select p from p in class " + DocumentToLabelBean.class.getName()+ ",d in class "+ Label.class.getName()+
        " where d.id = p.labelId " +
		" and p.documentId=? and d.type=? and d.readRights in (?,?";

		guids.add(username);
		guids.add(Label.SYSTEM_LABEL_OWNER);

		for(DSDivision dsd: DSUser.findByUsername(username).getAllDivisions())
		{
			q += ",?";
			guids.add(dsd.getGuid());
		}

		q+=")";
		Query query = DSApi.context().session().createQuery(q);
		query.setLong(0,documentId);
		query.setString(1,type);
		int licznik = 2;
		Iterator<String> it = guids.iterator();
		while (it.hasNext())
		{
			String guid = it.next();
			query.setString(licznik, guid);
			licznik ++;
		}
		return query.list();
	}
	/**
	 * Zwraca liste beanow (powiazan dokument-etykieta) dla danego dokuemntu i uzytkownika ze wskazaniem czy maja to byc modyfikowalne czy niemodyfikowalne
	 * @param documentId
	 * @param modifiable
	 * @param username
	 * @return
	 * @throws EdmException
	 */
	public static List<DocumentToLabelBean> getReadBeansForDocument(Long documentId, boolean modifiable, String username, String type)throws EdmException
	{
		String q = "select p from p in class " + DocumentToLabelBean.class.getName()+ ",d in class "+ Label.class.getName()+
        " where d.id = p.labelId and d.modifiable= ? and p.documentId=? and d.type = ? and d.readRights in (?,?";

		Collection<String> guids = new Vector<String>();
		guids.add(username);
		guids.add(Label.SYSTEM_LABEL_OWNER);
		for(DSDivision dsd: DSUser.findByUsername(username).getAllDivisions())
		{
			q += ",?";
			guids.add(dsd.getGuid());
		}
		q += ")";
		Query query = DSApi.context().session().createQuery(q);
		if (modifiable)
		{
			query.setInteger(0, 1);
		}
		else
		{
			query.setInteger(0, 1);
		}
		query.setLong(1,documentId);
		query.setString(2, type);
		Iterator<String> it = guids.iterator();
		int licznik = 3;
		while (it.hasNext())
		{
			String guid = it.next();
			query.setString(licznik, guid);
			licznik ++;
		}
		return query.list();
	}
	/**
	 * Zwraca liste beanow (powiazan dokument-etykieta) dla danej etykiety
	 * @param l
	 * @return
	 * @throws EdmException
	 */
	public static List<DocumentToLabelBean> getBeansForLabel(Label l) throws EdmException
	{
		Query q = DSApi.context().session().createQuery("select p from p in class " + DocumentToLabelBean.class.getName()+
				" where p.labelId=?");
		q.setLong(0,l.getId());
		return q.list();
	}
	/**
	 * zwraca wszytskie beany (powiazania dokument-etykieta)
	 * @return
	 * @throws EdmException
	 */
	public static List<DocumentToLabelBean> getAllBeans() throws EdmException
	{
		return Finder.list(DocumentToLabelBean.class, "id");
	}


	/**
	 * usuwa etykiete o danym id wraz ze wszytskimi jej potomkami. Usuwa rowniez te etykiety z dokumentow.
	 * @param id
	 * @throws EdmException
	 */
	public static void deleteLabel(Long id) throws EdmException
	{
		List<Label> descendants = getAllDescendants(id);
		for(Label l:descendants)
		{
			delLab(l);
		}
		delLab(findLabelById(id));

	}

	private static void delLab(Label l) throws EdmException
	{
		for(DocumentToLabelBean dtlb:getBeansForLabel(l))
		{
			DSApi.context().session().delete(dtlb);
			TaskSnapshot.updateLabelByDocumentId(dtlb.getDocumentId(),"anyType");
		}
		DSApi.context().session().delete(l);
	}
	/**
	 * Zwraca wszystkich potomkow dla danej etykiety (w sensie hierarchi etykiet)
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public static List<Label> getAllDescendants(Long id) throws EdmException
	{
		List<Label> children = getChildren(id);
		List<Label> result = children;
		for(Label l : children)
		{
			result.addAll(getChildren(l.getId()));
		}
		return result;
	}
	/**
	 * Zwraca dzieci dla danej etykiety (w sensie hierarchi etykiet)
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public static List<Label> getChildren(Long id) throws EdmException
	{
		Query q = DSApi.context().session().createQuery("select d from d in class " + Label.class.getName()+
				" where d.parentId=?");
		q.setLong(0,id);
		return q.list();
	}

	/**
	 * zwraca wszystkie etykiety dostepne w systemie
	 * @return
	 * @throws EdmException
	 */
	public static List<Label> listAllLabels() throws EdmException
    {
        return Finder.list(Label.class,"name");
    }

	public static List<Label> getReadLabelsForUser(String username, String type) throws EdmException
    {
		String q = "select d from d in class "+ Label.class.getName() + " where d.type=? and d.readRights in (?,?";
		Collection<String> guids = new Vector<String>();
		guids.add(username);
		guids.add(Label.SYSTEM_LABEL_OWNER);
		for(DSDivision dsd: DSUser.findByUsername(username).getAllDivisions())
		{
			q += ",?";
			guids.add(dsd.getGuid());
		}
		q+=")";

		Query query = DSApi.context().session().createQuery(q);
		query.setString(0, type);
		Iterator<String> it = guids.iterator();
		int licznik = 1;
		while (it.hasNext())
		{
			String guid = it.next();
			query.setString(licznik, guid);
			licznik ++;
		}

		return query.list();
    }
	/**Zwraca etykiety/flagi dla dokumentu kt�re mo�e odczyta� badz modyfikowac uzytkownik*/
	public static List<Label> getDocumentLabelsForUser(String username,Long documentId, String type) throws EdmException
    {
		List<Label> res = new ArrayList<Label>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder inGuid = new StringBuilder("(?,?");
		Collection<String> guids = new Vector<String>();
		guids.add(username);
		guids.add(Label.SYSTEM_LABEL_OWNER);

		for(DSDivision dsd: DSUser.findByUsername(username).getAllDivisions())
		{
			inGuid.append(",?");
			guids.add(dsd.getGuid());
		}
		inGuid.append(")");

		StringBuilder sb = new StringBuilder();
		sb.append("select l.id from dso_document_to_label dl, dso_label l where ");
		sb.append("dl.document_id = ? and dl.label_id = l.id and l.type = ? and ( l.write_Rights in ");
		sb.append(inGuid.toString());
		sb.append(" or l.modify_Rights in ");
		sb.append(inGuid.toString());
		sb.append(")");
		try
		{
			ps = DSApi.context().prepareStatement(sb.toString());
			ps.setLong(1, documentId);
			ps.setString(2,type);

			int licznik = 3;
			Iterator<String> it = guids.iterator();
			while (it.hasNext())
			{
				String guid = it.next();
				ps.setString(licznik, guid);
				licznik ++;
			}
			it = guids.iterator();
			while (it.hasNext())
			{
				String guid = it.next();
				ps.setString(licznik, guid);
				licznik ++;
			}

			rs = ps.executeQuery();
			while (rs.next())
			{
				res.add(LabelsManager.findLabelById(rs.getLong(1)));
			}
		}
		catch (Exception e)
		{
			LogFactory.getLog("eprint").error("",e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return res;
    }

	/**Metoda speckjalna do wyciagania flag
	 * Zwraca mape <Long,String> Long to id etykiety a String to nazwa uzytkownika kt�ry j� ustawi�*/
	public static Map<Long, String> getLabelBeanByDocumentId(Long documentId,String type)
	{
		Map<Long,String> res = new HashMap<Long, String>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try
		{
			ps = DSApi.context().prepareStatement("select l.id,dl.username from dso_document_to_label dl, dso_label l where " +
					"dl.document_id = ? and dl.label_id = l.id and l.type = ?");
			ps.setLong(1, documentId);
			ps.setString(2,type);

			rs = ps.executeQuery();
			while (rs.next())
			{
				res.put(rs.getLong(1), rs.getString(2));
			}
		}
		catch (Exception e)
		{
			LogFactory.getLog("eprint").error("",e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return res;
	}

	public static List<Label> getWriteLabelsForUser(String username, String type) throws EdmException
    {
		
		//String q = "select d from d in class "+ Label.class.getName() +" where d.type = ? and modifiable = 1 and d.writeRights in ( ?,?";
		String q = "select d from d in class "+ Label.class.getName() +" where d.type = ? and modifiable = ? and d.writeRights in ( ?,?";
		Collection<String> guids = new Vector<String>();
		guids.add(username);
		guids.add(Label.SYSTEM_LABEL_OWNER);

		for(DSDivision dsd: DSUser.findByUsername(username).getAllDivisions())
		{
			q += ",?";
			guids.add(dsd.getGuid());
		}
		q +=")";
		Query query = DSApi.context().session().createQuery(q);
		query.setString(0,type);
		query.setBoolean(1,true);

		int licznik = 2;
		Iterator<String> it = guids.iterator();
		while (it.hasNext())
		{
			String guid = it.next();
			query.setString(licznik, guid);
			licznik ++;
		}

		return query.list();
    }

	public static List<Label> getModifyLabelsForUser(String username, String type) throws EdmException
    {
		String q = "select d from d in class "+ Label.class.getName() + " where d.type = ? and d.modifyRights in (?,?";
		Collection<String> guids = new Vector<String>();
		guids.add(username);
		guids.add(Label.SYSTEM_LABEL_OWNER);
		for(DSDivision dsd: DSUser.findByUsername(username).getAllDivisions())
		{
			q+=",?";
			guids.add(dsd.getGuid());
		}
		q +=")";
		Query query = DSApi.context().session().createQuery(q);
		query.setString(0, type);
		int licznik = 1;
		Iterator<String> it = guids.iterator();
		while (it.hasNext())
		{
			String guid = it.next();
			query.setString(licznik, guid);
			licznik++;
		}
		return query.list();
    }

	public static List<Label> getPrivateModifyLabelsForUser(String username, String type) throws EdmException
    {
		String q =  "select d from d in class "+ Label.class.getName() + " where d.modifyRights = ? and d.type=?";
		Query query = DSApi.context().session().createQuery(q);
		query.setString(0, username);
		query.setString(1, type);

		return query.list();
    }

	public static List<Label> getNonPrivateModifyLabelsForUser(String username, String type) throws EdmException
    {
		String q = "select d from d in class "+ Label.class.getName() + " where d.type = ? and d.modifyRights in (?";

		Collection<String> guids = new Vector<String>();
		guids.add(Label.SYSTEM_LABEL_OWNER);

		for(DSDivision dsd: DSUser.findByUsername(username).getDSGroups())
		{
			q+=",?";
			guids.add(dsd.getGuid());
		}

		q += ")";
		Query query = DSApi.context().session().createQuery(q);
		query.setString(0, type);
		int licznik = 1;
		Iterator<String> it = guids.iterator();
		while (it.hasNext())
		{
			String guid = it.next();
			query.setString(licznik, guid);
			licznik ++;
		}

		return query.list();
    }

	public static List<Label> getSystemLabels(String type) throws EdmException
    {
		String q = "select d from d in class "+ Label.class.getName() + " where d.modifyRights = ? and d.type=?";
		Query query = DSApi.context().session().createQuery(q);
		query.setString(0, Label.NONE_OWNER);
		query.setString(1, type);

		return query.list();
    }

	/**
	 * zwraca etykiete o danym id
	 * @param id
	 * @return
	 * @throws EdmException
	 */
    public static Label findLabelById(Long id) throws EdmException
    {
		Label result = null;
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			Criteria criteria = DSApi.context().session().createCriteria(Label.class);
			criteria.add(Restrictions.idEq(id));
			result = (Label) criteria.uniqueResult();
		} catch (EdmException e) {}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
        
        return result;
    }

	/**
	 * zwraca etykiete o danym id
	 * @param id
	 * @return
	 * @throws EdmException
	 */

	public static Label findLabelByName(String name) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(Label.class);
        c.add(Expression.eq("name",name).ignoreCase());
        List<Label> clist = (List<Label>) c.list();
        if(clist.size() > 0)
        	return clist.get(0);
        else
        	return null;
    }

    /**
     * zwraca etykiete o danej nazwie
     * @param name
     * @return
     * @throws EdmException
     */
    public static Label findSystemLabelByName(String name, String type) throws EdmException
    {
    	String q = "select d from d in class "+ Label.class.getName() + " where d.modifyRights=? and d.type=? and d.name=?";
		Query query = DSApi.context().session().createQuery(q);
		query.setString(0, Label.NONE_OWNER);
		query.setString(1, type);
		query.setString(2, name);

		List<Label> list = query.list();
		if(list!=null && list.size()>0) return list.get(0);
		return null;
    }

    /**
     *
     * @param documentId
     * @param labelId
     * @throws EdmException
     */
    @Deprecated
    public static void removeLabel(Long documentId, Long labelId) throws EdmException
    {
    	removeLabel(documentId,labelId,null);
    	TaskSnapshot.updateLabelByDocumentId(documentId,"anyType");
    }
    /***
     * Usuwa etykiety z dokumentu o podanych nazwach
     * @param documentId
     * @param labelsName
     * @throws EdmException
     * @throws SQLException
     */
    public static void removeLabelsByName(long documentId,String... labelsName)throws EdmException
    {
    	PreparedStatement ps = null;
    	try
    	{
	    	String namesSql = "";
	    	for (int i = 0; i < labelsName.length; i++)
	    	{
	    		namesSql += "'"+labelsName[i]+"'";
	    		if(i != labelsName.length -1)
	    		{
	    			namesSql += ", ";
	    		}
			}
	    	String inSql = "select id from " + DSApi.getTableName(Label.class) + " where name in ( "+namesSql+" ) ";
	    	String sql = "delete from " + DSApi.getTableName(DocumentToLabelBean.class) +" where document_id = ? and label_id in ( "+inSql+" )";
	    	ps = DSApi.context().prepareStatement(sql);
	    	ps.setLong(1, documentId);
	    	ps.execute();
	    	TaskSnapshot.updateLabelByDocumentId(documentId,"anyType");
    	}
    	catch (Exception e)
    	{
			LogFactory.getLog("eprint").error("",e);
			throw new EdmException(e);
		}
    	finally
    	{
    		DSApi.context().closeStatement(ps);
    	}
    }
    /**
     * usuwa etykiete z dokumentu dla danego uzytkownika
     * @param documentId
     * @param labelId
     * @username Uzytkownik
     * @throws EdmException
     */
    public static void removeLabel(Long documentId, Long labelId,String username) throws EdmException
    {
    	Label label = findLabelById(labelId);
    	if (label!=null)
    	{
    		List<DocumentToLabelBean> list = findDocumentToLabelBean(documentId,labelId);

    		for(DocumentToLabelBean dtlb:list)
    		{
    			DSApi.context().session().delete(dtlb);
    		}
    		if (username!=null)
    		{
    			String descParams = label.getName()+(label.getDescription() != null ? "( "+label.getDescription()+")":"");
    			pl.compan.docusafe.core.datamart.DataMartManager.storeEvent(documentId, "document."+label.getType()+".remove", username, descParams);
    		}
    		TaskSnapshot.updateLabelByDocumentId(documentId,"anyType");
    	}
    }

	public static List<DocumentToLabelBean> findDocumentToLabelBean(Long documentId, Long labelId) throws EdmException
	{
		String q = "select d from d in class " + DocumentToLabelBean.class.getName()+ " where d.labelId=? and d.documentId=?";
    	Query query = DSApi.context().session().createQuery(q);
    	query.setLong(0,labelId);
    	query.setLong(1, documentId);
    	List<DocumentToLabelBean> list = query.list();
    	return list;
	}

	/**
	 *
	 * @param l
	 * @param onlyTasks
	 * @throws EdmException
	 */
	public static void updateBeansForLabel(Label l, boolean onlyTasks) throws EdmException
	{
		if(onlyTasks)
		{
			PreparedStatement ps = null;
			try
			{
				ps = DSApi.context().prepareStatement("select tl.dso_document_id from dso_document_to_label dtlb join dsw_tasklist tl on dtlb.document_id=tl.dso_document_id where dtlb.label_id=?");
				ps.setLong(1, l.getId());
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					//System.out.println("aaaaaaaaaaaaaa");
					TaskSnapshot.updateLabelByDocumentId(rs.getLong(1),"anyType");
					for(DocumentToLabelBean dtlb:findDocumentToLabelBean(rs.getLong(1), l.getId()))
					{
						dtlb.setHidding(l.getHidden());
					}

				}
				rs.close();
				DSApi.context().closeStatement(ps);
			}
			catch (Exception e)
			{
				DSApi.context().closeStatement(ps);
				throw new EdmException(e);
				// TODO: handle exception
			}
		}
		else
		{
			List<DocumentToLabelBean> beans = getBeansForLabel(l);
			for(DocumentToLabelBean dtlb : beans)
			{
				TaskSnapshot.updateLabelByDocumentId(dtlb.getDocumentId(),"anyType");
				dtlb.setHidding(l.getHidden());
			}
		}

	}

	public static void createNewFlag(boolean userFlags, String c, String description, String divisionGuid, String viewersDivisionGuid) throws EdmException
	{
		Label l = new Label();
		l.setName(c);
		l.setDescription(description);
		l.setDeletesAfter(false);
		l.setInactionTime(0);
		l.setHidden(false);
		l.setType(Label.FLAG_TYPE);
		l.setModifiable(true);
		l.setParentId(0L);
		if(userFlags)
		{
			l.setWriteRights(DSApi.context().getPrincipalName());
			l.setModifyRights(DSApi.context().getPrincipalName());
			l.setReadRights(DSApi.context().getPrincipalName());
		}
		else
		{
			l.setWriteRights(divisionGuid==null?DSApi.context().getPrincipalName():divisionGuid);
			l.setModifyRights(divisionGuid==null?DSApi.context().getPrincipalName():divisionGuid);
			l.setReadRights(viewersDivisionGuid==null?DSApi.context().getPrincipalName():viewersDivisionGuid);
		}
		Persister.create(l);
	}
	public static void updateFlag(Long id, boolean userFlags, String c, String description, String divisionGuid, String viewersDivisionGuid) throws EdmException
	{
		Label l = findLabelById(id);
		l.setName(c);
		l.setDescription(description);
		l.setDeletesAfter(false);
		l.setInactionTime(0);
		l.setHidden(false);
		l.setType(Label.FLAG_TYPE);
		l.setModifiable(true);
		l.setParentId(0L);
		if(userFlags)
		{
			l.setWriteRights(DSApi.context().getPrincipalName());
			l.setModifyRights(DSApi.context().getPrincipalName());
			l.setReadRights(DSApi.context().getPrincipalName());
		}
		else
		{
			l.setWriteRights(divisionGuid==null?DSApi.context().getPrincipalName():divisionGuid);
			l.setModifyRights(divisionGuid==null?DSApi.context().getPrincipalName():divisionGuid);
			l.setReadRights(viewersDivisionGuid==null?DSApi.context().getPrincipalName():viewersDivisionGuid);
		}
		DSApi.context().session().save(l);


	}

	public static boolean canAddLabel(Long id, String username) throws EdmException{
		Label l = findLabelById(id);
		if(l.getWriteRights().equals(Label.SYSTEM_LABEL_OWNER) || l.getWriteRights().equals(username)) return true;
		for(DSDivision dsd: DSUser.findByUsername(username).getAllDivisions())
		{
			if(dsd.getGuid().equals(l.getWriteRights())) return true;
		}
		return false;

	}

	public static List<Task.FlagBean> getReadFlagBeansForDocument(Long id, String username, boolean own)throws EdmException
	{
		List<Task.FlagBean> beans = new ArrayList<Task.FlagBean>();
		for(Label l:getReadLabelsForDocument(id, username, Label.FLAG_TYPE, own))
		{
			beans.add(new Task.FlagBean(l.getName(), l.getDescription()));
		}

		return beans;
	}
	
	public static List<Task.FlagBean> getReadFlagAndLabelsBeansForDocument(Long id, String username, boolean own)throws EdmException
	{
		List<Task.FlagBean> beans = new ArrayList<Task.FlagBean>();
		for(Label l:getReadLabelsForDocument(id, username, Label.FLAG_AND_LABEL_TYPE, own))
		{
			beans.add(new Task.FlagBean(l.getName(), l.getDescription()));
		}

		return beans;
	}

	public static List<DocumentToLabelBean> getBeansForService() throws EdmException {
		Query ps = DSApi.context().session().createQuery("select d from d in class " + DocumentToLabelBean.class.getName() + " where d.actionTime < ?");
		ps.setTimestamp(0, new Timestamp(new java.util.Date().getTime()));
		return (List<DocumentToLabelBean>)ps.list();
	}
}
