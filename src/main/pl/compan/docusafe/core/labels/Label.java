package pl.compan.docusafe.core.labels;

import java.io.Serializable;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

public class Label implements Comparable<Label>, Serializable {

	public final static String SYSTEM_LABEL_OWNER = "#system#";
	public final static String NONE_OWNER = "#none#";

	public final static String LABEL_TYPE = "label";
	public final static String FLAG_TYPE = "flag";
	
	public final static String FLAG_AND_LABEL_TYPE = "flagAndLabel";


	protected Long id;
	protected String name;
    protected String cn;
	protected String description;
	protected Long parentId;
	protected Integer inactionTime;
	protected Boolean hidden;
	protected Long ancestorId;
	protected Boolean modifiable;
	protected Boolean deletesAfter;
	protected String type;
	protected String readRights;
	protected String writeRights;
	protected String modifyRights;
	protected String aliasName;

	/**
	 * Wyszukanie etykiety o podanym Id.
	 * 
	 * @param id
	 *            id etykiety
	 * @return
	 * @throws EdmException
	 */
	public static Label find(Long id) throws EdmException {
		return (Label) Finder.find(Label.class, id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * 
	 * @return zwraca czas nieaktywnosci atykiety w godzinach
	 */
	public Integer getInactionTime() {
		return inactionTime;
	}

	public void setInactionTime(Integer inactionTime) {
		this.inactionTime = inactionTime;
	}

	public Boolean getHidden() {
		return hidden;
	}

	public void setHidden(Boolean hidden) {
		this.hidden = hidden;
	}

	public Long getAncestorId() {
		return ancestorId;
	}

	public void setAncestorId(Long ancestorId) {
		this.ancestorId = ancestorId;
	}

	public Boolean getModifiable() {
		return modifiable;
	}

	public void setModifiable(Boolean modifiable) {
		this.modifiable = modifiable;
	}

	public Boolean getDeletesAfter() {
		return deletesAfter;
	}

	public void setDeletesAfter(Boolean deletesAfter) {
		this.deletesAfter = deletesAfter;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReadRights() {
		return readRights;
	}

	public void setReadRights(String readRights) {
		this.readRights = readRights;
	}

	public String getWriteRights() {
		return writeRights;
	}

	public void setWriteRights(String writeRights) {
		this.writeRights = writeRights;
	}

	public String getModifyRights() {
		return modifyRights;
	}

	public void setModifyRights(String modifyRights) {
		this.modifyRights = modifyRights;
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public int compareTo(Label o) {
		if (this.id.equals(o.id))
			return 0;
		else
			return -1;
	}

}
