package pl.compan.docusafe.core.labels;

import java.util.Date;

public class DocumentToLabelBean
{
	private Long id;
	private Long documentId;
	private Long labelId;
	private Date ctime;
	private String username;
	private Date actionTime;
	private Boolean hidding;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Long getDocumentId()
	{
		return documentId;
	}

	public void setDocumentId(Long documentId)
	{
		this.documentId = documentId;
	}

	public Long getLabelId()
	{
		return labelId;
	}

	public void setLabelId(Long labelId)
	{
		this.labelId = labelId;
	}

	public Date getCtime()
	{
		return ctime;
	}

	public void setCtime(Date ctime)
	{
		this.ctime = ctime;
	}

	public Date getActionTime()
	{
		return actionTime;
	}

	public void setActionTime(Date actionTime)
	{
		this.actionTime = actionTime;
	}

	public Boolean getHidding()
	{
		return hidding;
	}

	public void setHidding(Boolean hidding)
	{
		this.hidding = hidding;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("documentId ").append(documentId).append("\n");
		sb.append("labelId ").append(labelId).append("\n");
		sb.append("actionTime ").append(actionTime).append("\n");
		sb.append("username ").append(username).append("\n");
		sb.append("hidding ").append(hidding).append("\n");
		sb.append("ctime ").append(ctime).append("\n");
		return sb.toString();
	}
}
