package pl.compan.docusafe.archive.core;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Aspekt loguj�cy wszystkie wyj�tki rzucane w pakiecie
 * pl.compan.docusafe.archive.core.
 * UWAGA: Obecnie nieu�ywany (16/03/2006)
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LogExceptionsAspect.aj,v 1.2 2006/03/17 13:28:07 lk Exp $
 */
public aspect LogExceptionsAspect
{
    private static final Log log = LogFactory.getLog(LogExceptionsAspect.class);

    pointcut dsCall() : call(* pl.compan.docusafe..*(..));

    after() throwing (Throwable e): dsCall() // && !cflow(dsCall())
    {
        log.error(e.getMessage(), e);
    }
}
