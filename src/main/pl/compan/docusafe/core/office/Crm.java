package pl.compan.docusafe.core.office;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.parametrization.aegon.CrmDriver;
import pl.compan.docusafe.core.EdmException;
/**
 * Klasa obsługująca połączenie do bazy zewnętrznej w Impulsie
 * przekazuje zadania do CRM (zewnętrznego) 
 * 
 * @author Mariusz Kiljanczyk
 *
 */
public class Crm
{
    private static final Log log = LogFactory.getLog("agent_log");
    private Integer id;
    private Integer docId;
    private String activityId;
    private Integer activityKind;
    private Date exportDate;
    private String crmStatus;
    private String officeType;
    private String officeNumber;
    private Date entryDate;
    private String fromUser;
    private String nrPolisy;
    private Integer typDokumentu;
    private String typNazwa;
    private Date documentDate;
    private String status;
    private String sposobDostarczenia;
    
    //konstruktor dla hibernate
    public Crm()
    {}
    
    public void create(Connection conn) throws EdmException
    {
    	PreparedStatement ps = null;
        try
        {           
            log.trace("Połaczono z CRM");
            log.trace("Sposob dostarczenia "+ sposobDostarczenia );
            ps =conn.prepareStatement("insert into "+CrmDriver.getEntityName()+
                " (ID,DOC_ID,ACTIVITY_ID,ACTIVITY_KIND," +
                "EXPORT_DATE,CRM_STATUS,OFFICE_TYPE,OFFICE_NUMBER,DS_ENTRY_DATE,FROM_USER,NR_POLISY," +
                "TYP_DOKUMENTU,TYP_NAZWA,DOCUMENT_DATE,STATUS,sposobDostarczenia)" +
                "values("+CrmDriver.getEntityName()+"_id.nextval"+",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps.setInt(1,docId);
            ps.setString(2,activityId);
            ps.setInt(3,activityKind);
            /**
             * @TODO - wyjasnic koniecznie o co chodzi z TA DATA
             */
            if (exportDate==null)
            	exportDate = new java.util.Date();
            ps.setTimestamp(4,new Timestamp(exportDate.getTime()));
            ps.setString(5,crmStatus);
            ps.setString(6,officeType);
            ps.setString(7,officeNumber);
            ps.setTimestamp(8,new Timestamp(entryDate.getTime()));
            ps.setString(9,fromUser);
            ps.setString(10,nrPolisy);
            if (typDokumentu != null) ps.setInt(11,typDokumentu);else ps.setInt(11,0);
            ps.setString(12,typNazwa);
            ps.setTimestamp(13,new Timestamp(documentDate.getTime()));
            ps.setString(14,status);
            ps.setString(15,sposobDostarczenia);

            log.trace("Zbudowalem zapytanie do bazy danych ");
            ps.executeUpdate();            
            log.info("Zapisalem do bazy danych CRM ");
        }
        catch (SQLException e){

        	LogFactory.getLog("eprint").debug("", e);
            log.error("Blad polaczenia z baza danych CRM ",e);
        }
        finally
        {
        	if(ps != null)
        	{
        		try
        		{
        			ps.close();
        		}
        		catch (Exception e) {
        			log.error("Blad rozlaczenia z baza danych CRM ",e);
				}
        	}
        }
    }
    public String getActivityId()
    {
        return activityId;
    }
    public void setActivityId(String activityId)
    {
        this.activityId = activityId;
    }
    public Integer getActivityKind()
    {
        return activityKind;
    }
    public void setActivityKind(Integer activityKind)
    {
        this.activityKind = activityKind;
    }
    public String getCrmStatus()
    {
        return crmStatus;
    }
    public void setCrmStatus(String crmStatus)
    {
        this.crmStatus = crmStatus;
    }
    public Date getDocumentDate()
    {
        return documentDate;
    }
    public void setDocumentDate(Date documentDate)
    {
        this.documentDate = documentDate;
    }
    public Date getEntryDate()
    {
        return entryDate;
    }
    public void setEntryDate(Date entryDate)
    {
        this.entryDate = entryDate;
    }
    public Date getExportDate()
    {
        return exportDate;
    }
    public void setExportDate(Date exportDate)
    {
        this.exportDate = exportDate;
    }
    public String getFromUser()
    {
        return fromUser;
    }
    public void setFromUser(String fromUser)
    {
        this.fromUser = fromUser;
    }
    public String getNrPolisy()
    {
        return nrPolisy;
    }
    public void setNrPolisy(String nrPolisy)
    {
        this.nrPolisy = nrPolisy;
    }
    public String getOfficeNumber()
    {
        return officeNumber;
    }
    public void setOfficeNumber(String officeNumber)
    {
        this.officeNumber = officeNumber;
    }
    public String getOfficeType()
    {
        return officeType;
    }
    public void setOfficeType(String officeType)
    {
        this.officeType = officeType;
    }
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String ststus)
    {
        this.status = ststus;
    }
    public Integer getTypDokumentu()
    {
        return typDokumentu;
    }
    public void setTypDokumentu(Integer typDokumentu)
    {
        this.typDokumentu = typDokumentu;
    }
    public String getTypNazwa()
    {
        return typNazwa;
    }
    public void setTypNazwa(String typNazwa)
    {
        this.typNazwa = typNazwa;
    }
    public Integer getDocId()
    {
        return docId;
    }
    public void setDocId(Integer docId)
    {
        this.docId = docId;
    }
    public Integer getId()
    {
        return id;
    }
    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getSposobDostarczenia()
    {
        return sposobDostarczenia;
    }

    public void setSposobDostarczenia(String sposobDostarczenia)
    {
        this.sposobDostarczenia = sposobDostarczenia;
    }
}
