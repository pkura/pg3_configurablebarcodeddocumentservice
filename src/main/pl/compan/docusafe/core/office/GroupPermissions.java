package pl.compan.docusafe.core.office;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Sprawdza uprawnienia pochodzące z przynależności do grupy
 * @author Michał Sankowski
 */
public class GroupPermissions {


	private final static Logger log = LoggerFactory.getLogger(GroupPermissions.class);
	public final static String ARCHIVE_PROCESSING_GUID = "ARCHIVE_PROCESSING";
	public final static String REJECT_FINAL_ACCEPTANCE_GUID = "REJECT_FINAL_ACCEPTANCE";
    public final static String ACCOUNT_COMMENT_MODIFY = "ACCOUNT_COMMENT_MODIFY";

	public static boolean canProcessThroughArchive() throws EdmException{
		return canProcessThroughArchive(DSApi.context().getPrincipalName());
	}


	public static boolean canRejectFinalAcceptance(){
		return canRejectFinalAcceptance(DSApi.context().getPrincipalName());
	}
	public static boolean canRejectFinalAcceptance(String username){
		try {
			if (AvailabilityManager.isAvailable("workflow.finalAcceptance.reject.group")) {
				return DSDivision.isInDivision(REJECT_FINAL_ACCEPTANCE_GUID, username);
			} else if (AvailabilityManager.isAvailable("workflow.finalAcceptance.reject")) {
				return true;
			} else {
				return false;
			}
		} catch (EdmException e) {
			log.debug(e.getMessage(), e);
			return false;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return false;
		}
	}
	
	/**
	 * @param username
	 * @return
	 * @throws EdmException
	 */
    public static boolean canModifyAccountComments(String username) throws EdmException {
        try {
            return DSDivision.isInDivision(ACCOUNT_COMMENT_MODIFY, username);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    public static boolean canProcessThroughArchive(String username) throws EdmException {
        try {
            if (AvailabilityManager.isAvailable("archiwum.procesowanie.grupa"))
                return DSDivision.isInDivision(ARCHIVE_PROCESSING_GUID, username);
            if (AvailabilityManager.isAvailable("archiwum.procesowanie"))
                return true;
            else return false;

        } catch (Exception e) {
            log.debug("e", e);
            return false;
        }
    }

}
