package pl.compan.docusafe.core.office;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.criterion.Order;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CollectiveAssignmentEntry.java,v 1.3 2006/12/07 13:51:33 mmanski Exp $
 */
public class CollectiveAssignmentEntry
{
    private Integer id;
    private String objective;
    /**
     * Nazwa procesu workflow w formacie:
     * nazwa_workflow "," nazwa_pakietu "::" nazwa_procesu,
     * np.: "internal,docusafe_1::obieg_reczny".
     */
    private String wfProcess;

    public static CollectiveAssignmentEntry find(Integer id)
        throws EntityNotFoundException, EdmException
    {
        try
        {
            return (CollectiveAssignmentEntry) DSApi.context().session().
                load(CollectiveAssignmentEntry.class, id);
        }
        catch (ObjectNotFoundException e)
        {
            throw new EntityNotFoundException(CollectiveAssignmentEntry.class, id);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List list() throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(CollectiveAssignmentEntry.class);
            crit.addOrder(Order.asc("objective"));
            return crit.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getObjective()
    {
        return objective;
    }

    public void setObjective(String objective)
    {
        this.objective = objective;
    }

    public String getWfProcess()
    {
        return wfProcess;
    }

    public void setWfProcess(String wfProcess)
    {
        this.wfProcess = wfProcess;
    }
}
