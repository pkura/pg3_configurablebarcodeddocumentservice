package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.base.DuplicateNameException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DuplicateEntryException.java,v 1.1 2004/05/16 20:10:30 administrator Exp $
 */
public class DuplicateEntryException extends DuplicateNameException
{
    public DuplicateEntryException(String message)
    {
        super(message);
    }

    public DuplicateEntryException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DuplicateEntryException(Throwable cause)
    {
        super(cause);
    }
}
