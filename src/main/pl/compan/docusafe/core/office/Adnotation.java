package pl.compan.docusafe.core.office;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;

/**
 * Slownik uwag.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AssignmentObjective.java,v 1.7 2006/12/07 13:51:26 mmanski Exp $
 */
public class Adnotation
{
    private Integer id;
    private String name;


    protected Adnotation()
    {
    }

    public Adnotation(String arg)
    {
        name = arg;
    }
    
    public static Adnotation find(Integer id) throws EdmException
    {
        return Finder.find(Adnotation.class, id);
    }

    public static List<Adnotation> list() throws EdmException
    {
        return Finder.list(Adnotation.class, "id");
    }
    
    public void create() throws EdmException
    {
        Persister.create(this);
    }

    public void delete() throws EdmException
    {
        Persister.delete(this);
    }

    public Integer getId ()
    {
        return id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
