package pl.compan.docusafe.core.office;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.classic.Lifecycle;
import org.hibernate.Session;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

import java.io.Serializable;

/**
 * Rodzaj dokumentu kancelaryjnego.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OfficeDocumentKind.java,v 1.6 2006/12/07 13:52:26 mmanski Exp $
 */
public abstract class OfficeDocumentKind implements Lifecycle
{
    private Integer id;
    private String name;
    private Integer posn;

    protected OfficeDocumentKind()
    {
    }

    protected OfficeDocumentKind(String name)
    {
        this.name = name;
    }

    public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /* Lifecycle */

    public final boolean onSave(Session session) throws CallbackException
    {
        if (id != null)
        {
            posn = id;
        }
        else
        {
            // w bazach, gdzie identyfikatory pochodz� z pola autonumber
            // id b�dzie w tym momencie r�wne null
            posn = Dictionaries.updatePosnOnSave(getClass());
        }
        return false;
    }

    public boolean onUpdate(Session session) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session session) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session session, Serializable serializable)
    {
    }

    public Integer getId()
    {
        return id;
    }

    private void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getPosn()
    {
        return posn;
    }

    public void setPosn(Integer posn)
    {
        this.posn = posn;
    }

    public final boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof OfficeDocumentKind)) return false;

        final OfficeDocumentKind officeDocumentKind = (OfficeDocumentKind) o;

        if (id != null ? !id.equals(officeDocumentKind.getId()) : officeDocumentKind.getId() != null) return false;

        return true;
    }

    public final int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }
}
