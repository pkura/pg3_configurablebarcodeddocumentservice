package pl.compan.docusafe.core.office;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.Condition;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.ProcessChannel;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;

public class ChannelBean {
	
	private String cn;
	private String name;
	private Collection<String> conditions;
	private String guid;
	private Collection<DSUser> users;
	
	public ChannelBean() {
		setConditions(new LinkedList<String>());
		users = new LinkedList<DSUser>();
	}
	
	private static String convertCondition(Condition condition, DocumentKind dk) throws EdmException {
		StringBuilder result = new StringBuilder();
		Field field = dk.getFieldByCn(condition.getField());
		result.append(field.getName());
		result.append(" ");
		result.append(condition.getPrettyOperator());
		if (condition.getValue() != null &&
			!condition.getOperator().equals(Condition.OPERATOR_IS_NULL) && 
			!condition.getOperator().equals(Condition.OPERATOR_NOT_NULL)) {
			result.append(" ");
			result.append(field.getEnumItem(Integer.parseInt(condition.getValue())).getTitle());
		}
		return result.toString();
		
		
	}
	
	public static Collection<ChannelBean> prepareBeans() throws EdmException {
		List<ChannelBean> result = new LinkedList<ChannelBean>();
		DocumentKind dk = DocumentKind.findByCn(DocumentKind.getDefaultKind());
		List<ProcessChannel> channels = dk.getProcessChannels();
		LogFactory.getLog("kuba").debug("mam kanalow " + channels.size());
		
		for (ProcessChannel pc : channels) {
			ChannelBean cb = new ChannelBean();
			cb.setCn(pc.getCn());
			cb.setName(pc.getName());
			for (Condition cond : pc.getConditions()) {
				cb.getConditions().add(convertCondition(cond, dk));
			}
			ProcessCoordinator procCoor = ProcessCoordinator.find(dk.getCn(), pc.getCn()).get(0);
			String guid;
			try {
				guid = DSDivision.find(procCoor.getGuid()).getName();
			} catch (DivisionNotFoundException e) {
				guid = procCoor.getGuid();
			}
			cb.setGuid(guid);
			if (procCoor.getUsername() != null) {
				DSUser user = DSUser.findByUsername(procCoor.getUsername());
				cb.getUsers().add(user);
			} else {
				cb.setUsers(Arrays.asList(UserFactory.getInstance().findDivision(procCoor.getGuid()).getUsers(false)));
			}
			result.add(cb);
		}
		
		return result;
	}
		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Collection<DSUser> getUsers() {
		return users;
	}

	public void setUsers(Collection<DSUser> users) {
		this.users = users;
	}

	public void setConditions(Collection<String> conditions) {
		this.conditions = conditions;
	}

	public Collection<String> getConditions() {
		return conditions;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getCn() {
		return cn;
	}
}
