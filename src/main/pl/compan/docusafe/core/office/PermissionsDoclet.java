package pl.compan.docusafe.core.office;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.FieldDoc;
import com.sun.javadoc.RootDoc;
import com.sun.javadoc.Tag;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;

/**
 * Doclet, kt�ry analizuje klas� {@link DSPermission} i wypisuje
 * na stdout opisy uprawnie� wraz z ich kodami gotowe do wklejenia
 * do pliku LocalStrings_permissions.properties w bie��cym katalogu.
 * Je�eli plik ten ju� istnieje, doclet rzuci wyj�tek.
 *
 * @see DSPermission
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PermissionsDoclet.java,v 1.5 2006/02/20 15:42:19 lk Exp $
 */
public class PermissionsDoclet
{
    public static boolean start(RootDoc doc) throws Exception
    {
        File out = new File("LocalStrings_permissions.properties");

        if (out.exists())
            throw new Exception("Plik "+out+" ju� istnieje");

        Writer writer = new OutputStreamWriter(new FileOutputStream(out), Charset.forName("iso-8859-2"));

        ClassDoc cd = doc.classNamed(DSPermission.class.getName());
        FieldDoc[] fields = cd.fields();
        for (int i=0, n=fields.length; i < n; i++)
        {
            if (!fields[i].isField() ||
                !fields[i].type().qualifiedTypeName().equals(String.class.getName()))
                continue;

            String description;
            Tag[] tags = fields[i].tags("desc");
            if (tags != null && tags.length > 0)
                description = tags[0].text();
            else
                description = fields[i].commentText();

            writer.write(
                "permission."+
                fields[i].name()+
                " = "+
                /*u*/(description != null ? trim(description) : "")+
                "\n");
        }

        writer.close();

        return true;
    }

    /**
     * Zast�puje znaki spoza US-ASCII ich kodami unicode.
     */
    private static String u(String s)
    {
        StringBuilder sb = new StringBuilder();
        for (int i=0, n=s.length(); i < n; i++)
        {
            char c = s.charAt(i);
            if (c < 128) sb.append(c);
            else sb.append("\\u"+(int)c);
        }
        return sb.toString();
    }

    private static String trim(String s)
    {
        if (s == null)
            return s;

        StringBuilder sb = new StringBuilder(s);

        while (sb.length() > 0 && sb.charAt(sb.length()-1) == '.')
            sb.setLength(sb.length()-1);

        return sb.toString();
    }
}
