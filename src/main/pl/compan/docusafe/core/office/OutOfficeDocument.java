package pl.compan.docusafe.core.office;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.labels.DocumentToLabelBean;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.*;
import pl.compan.docusafe.webwork.event.ActionEvent;
import std.lambda;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Pismo wychodz�ce lub wewn�trzne.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OutOfficeDocument.java,v 1.128 2010/07/29 11:00:16 mariuszk Exp $
 */
public class OutOfficeDocument extends OfficeDocument
    implements Lifecycle
{
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(OutOfficeDocument.class.getPackage().getName(),null);
    private static final Log log = LogFactory.getLog(OutOfficeDocument.class);
    
    public static final String TYPE = "out";
    public static final String INTERNAL_TYPE = "internal";

    // TODO: podpisane przez
    public static final String PREP_STATE_DRAFT = "DRAFT";
    public static final String PREP_STATE_FAIR_COPY = "FAIR_COPY";

    private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    private Date documentDate;
    private Date officedate;    //data nadania numeru KO
    private String postalRegNumber;
    private String creatingUser;
    private InOfficeDocument inDocument;
    private Long inDocumentId;
    private List<Recipient> recipients;
    private Date zpoDate; //ZPO - "Za Potwierdzeniem Odbiory" Data potwierdzenia odbiory
    private String prepState;
    private OutOfficeDocumentDelivery delivery;
    private OutOfficeDocumentReturnReason returnReason;
    private boolean returned;
    private boolean internal;
    private Integer weightG;
    private Integer weightKg;
    private Boolean priority;
    private Boolean zpo;
    private Integer gauge;	//gabaryt
    private Boolean countryLetter;
    private Integer mailWeight;
    private Boolean additionalZpo; // dodatkowe ZPO dla s�d�w i prokuratury
  //  private Boolean cancelled;
	
	

	/**
	 * Dokument glowny
	 */
    private OutOfficeDocument masterDocument = null;

    /**
     * Warto�� znaczka skarbowego.
     */
    private BigDecimal stampFee;
    private String referenceId;

    @Override public DocumentType getType()
    {
        if (isInternal())
        {
            return DocumentType.INTERNAL;
        }
        else
        {
            return DocumentType.OUTGOING;
        }
    }
    @Override public String getStringType()
    {
        if (isInternal())
        {
            return OutOfficeDocument.INTERNAL_TYPE;
        }
        else
        {
            return OutOfficeDocument.TYPE;
        }
    }

    public String getFormattedOfficeNumber()
    {
        String superFormattedNumber = super.getFormattedOfficeNumber();

        if (superFormattedNumber == null)
            return null;

        if (masterDocument != null)
        {
            return "[" + superFormattedNumber + "]";
        }
        else
        {
            return superFormattedNumber;
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    protected void duplicate(Long[] attachmentIds) throws EdmException
    {
        List<Recipient> savedRecipients = this.recipients;
        this.recipients = null;
        super.duplicate(attachmentIds);

        if (savedRecipients != null)
        {
            this.recipients = new ArrayList<Recipient>(savedRecipients.size());
            for (Recipient recipient : savedRecipients)
            {
                recipient = (Recipient) recipient.cloneObject();
                recipient.setDocumentId(getId());
                this.recipients.add(recipient);
            }
        }
    }

    /**
     * Zwraca list� odbiorc�w dokumentu. Zwracana lista mo�e by� pusta,
     * ale nie mo�e by� r�wna null.
     * @throws EdmException
     */
    public List<Recipient> getRecipients() throws EdmException
    {
        if (recipients == null && getId() != null)
        {
    		Criteria c = DSApi.context().session().createCriteria(Recipient.class);
            c.add(Restrictions.eq("documentId", this.getId()));
            recipients = new ArrayList<Recipient>(c.list());
        }
        if(recipients == null && getId() == null )
        	recipients =  new ArrayList<Recipient>();
        return recipients;
    }    

    public void addRecipient(Recipient recipient) throws EdmException
    {	
    	getRecipients().add(recipient);
    	recipient.setDocument(this);
    }

    /* *
     * Wpisuje pismo do g��wnego dziennika pism wychodz�cych.
     * W systemie musi by� otwarty rok i u�ytkownik musi mie� prawo
     * do nadania numeru kancelaryjnego. Pismo musi by� czystopisem.
     * @throws EdmException
     */
/*
    public void assignOfficeNumber() throws EdmException
    {
        String sYear = DSApi.context().getProperty(Properties.OPEN_YEAR);
        if (sYear == null)
            throw new EdmException("W systemie nie otwarto roku, nie mo�na zrobi� wpisu w dzienniku");

        if (!DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO))
            throw new EdmException("Brak uprawnie� do nadawania numeru kancelaryjnego");

        if (!PREP_STATE_FAIR_COPY.equals(prepState))
            throw new EdmException("Numer kancelaryjny mo�na nada� tylko czystopisowi");

        Journal journal = Journal.getMainOutgoing();
        setOfficeNumber(journal.newEntry(getId()).getSequenceId());
        setOfficeNumberYear(new Integer(sYear));
        setJournal(journal);

        propertyChangeSupport.firePropertyChange("::assignOfficeNumber", null, null);
    }
*/
    public void bindToJournal(Long journalId, Integer sequenceId) throws EdmException
    {
    	bindToJournal(journalId, sequenceId, null,null);
    }
    
    public void bindToJournal(Long journalId, Integer sequenceId,Date officeDate) throws EdmException
    {
    	bindToJournal(journalId, sequenceId, officeDate,null);
    }
    
    public void bindToJournal(Long journalId, Integer sequenceId,Date officeDate, ActionEvent event) throws EdmException
    {
        if (journalId == null)
            throw new NullPointerException("journalId");

        if (sequenceId == null)
            throw new NullPointerException("sequenceId");

        int year = GlobalPreferences.getCurrentYear();

        if(!AvailabilityManager.isAvailable("dontCheckPermision.KO")){
        	if (!internal &&
                    !DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO))
                    throw new EdmException(sm.getString("BrakUprawnienDoNadawaniaNumeruKancelaryjnego"));
        	
        }

        if (!Configuration.hasExtra("business") &&
            !internal && !PREP_STATE_FAIR_COPY.equals(prepState))
            throw new EdmException(sm.getString("NumerKancelaryjnyMoznaNadacTylkoCzystopisowi"));

        getDocumentKind().logic().bindToJournal(this, event);
        Journal journal = Journal.find(journalId);
        JournalEntry entry = journal.findEntry(sequenceId);

        if (getJournal() == null || getJournal().equals(journal))
            setJournal(journal);
        setDailyOfficeNumber(entry.getDailySequenceId());
        setOfficeNumber(entry.getSequenceId());
        setOfficeNumberYear(new Integer(year));
        setOfficeNumberSymbol(entry.getJournal().isMain() ? null : entry.getJournal().getSymbol());
        if(officeDate == null)
        	setOfficeDate(GlobalPreferences.getCurrentDay());
        else
        	setOfficeDate(officeDate);
    }

    /**
     * Zapisuje dokument w sesji Hibernate.
     * <p>
     * Zapisuje w sesji warto�� pola sender (je�eli istnieje) oraz
     * przypisuje dokumentowi folder SN_OFFICE. Nast�pnie wywo�uj�
     * metod� {@link pl.compan.docusafe.core.base.Document#create()}.
     */
    public void create() throws EdmException
    {
        try
        {
            if (getDoctype() == null || !"daa".equals(getDoctype().getCn()))
                setFolder(Folder.findSystemFolder(Folder.SN_OFFICE));
            prepState = PREP_STATE_FAIR_COPY;

            if(Configuration.hasExtra("business"))
            {
            	if(AvailabilityManager.isAvailable("outofficedocument.prepState.fairCopy"))
            	{
            		prepState = PREP_STATE_FAIR_COPY;
            	}
            	else
            	{
            		prepState = PREP_STATE_DRAFT;
            	}
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }

        super.create();
    }


    public static OutOfficeDocument findOutOfficeDocument(Long id)
        throws EdmException
    {
        Document doc = find(id);

        if (!(doc instanceof OutOfficeDocument))
            throw new DocumentNotFoundException(id);

        return (OutOfficeDocument) doc;
    }

    /**
     * Zwraca wszystkie dokumenty przypisane o danej sprawy.
     */
    public static List<OutOfficeDocument> findByOfficeCase(OfficeCase officeCase)
        throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(OutOfficeDocument.class);

        try
        {
        	c.add(Restrictions.eq("containingCaseId", officeCase.getId()));
            return c.list();

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Zwraca liste dokumentow o danym numerze barcode.
     */
    public static List<OutOfficeDocument> findByBarcode(String barcode) throws EdmException {
    	Criteria crit = DSApi.context().session().createCriteria(OutOfficeDocument.class);
    	crit.add(Restrictions.eq("barcode", barcode));
    	
    	try {
    		return crit.list();
    	} catch(HibernateException e) {
    		log.error(e.getMessage(), e);
    		throw new EdmException("Nie znaleziono dokumentu o podanym numerze barcode " + barcode);
    	}	
    }

    /**
     * Zwraca liste dokumentow o danym numerze przesy�ki rejestrowanej.
     */
    public static List<OutOfficeDocument> findByPostalRegNumber(String postalRegNumber) throws EdmException {
    	Criteria crit = DSApi.context().session().createCriteria(OutOfficeDocument.class);
    	crit.add(Restrictions.eq("postalRegNumber", postalRegNumber));
    	
    	try {
    		if(crit.list().size() == 0){
    			return null;
    		} else {
    			return crit.list();
    		}
    	} catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }	
    }

    /**
     * Zwraca wszystkie dokumenty o danym numerze kancelaryjnym, od
     * najstarszego do najnowszego.
     */
    public static List findByOfficeNumber(int officeNumber)
        throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(OutOfficeDocument.class);
        c.add(org.hibernate.criterion.Expression.eq("officeNumber", new Integer(officeNumber)));
       // c.addOrder(org.hibernate.criterion.Order.asc("incomingDate"));

        try
        {
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca list� dokument�w wychodz�cych bez nadanego numeru
     * kancelaryjnego ("w przygotowaniu"). Dokumenty sortowane
     * s� rosn�co po czasie utworzenia.
     */
    public static List<OutOfficeDocument> findUnregistered() throws EdmException
    {
        try
        {
            List<OutOfficeDocument> docs = (List<OutOfficeDocument>)
                DSApi.context().classicSession().find(
                "select d from d in class "+OutOfficeDocument.class +
                " where d.officeNumber is null order by d.ctime");

            return docs;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    private static final lambda<OutOfficeDocument, OutOfficeDocument> identityMapper =
        new lambda<OutOfficeDocument, OutOfficeDocument>()
        {
            public OutOfficeDocument act(OutOfficeDocument o)
            {
                return o;
            }
        };

    public static SearchResults<OutOfficeDocument> search(Query query) throws EdmException
    {

        return search(query, identityMapper, OutOfficeDocument.class);
    }

    public static <T> SearchResults<T> search(OutOfficeDocument.Query query,
                                              lambda<OutOfficeDocument, T> mapper,
                                              Class<T> mappedClass)
        throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias docTable = from.createTable(DSApi.getTableName(OutOfficeDocument.class), true);
        TableAlias documentTable = from.createTable(DSApi.getTableName(Document.class), true);
        TableAlias senderTable = null;
        if (query.sender != null)
            senderTable = from.createTable(DSApi.getTableName(Sender.class), true);
        TableAlias recipientTable = null;
        if (query.recipient != null)
            recipientTable = from.createTable(DSApi.getTableName(Recipient.class), true);
        TableAlias journalEntryTable = null;
        if (query.journalId != null)
            journalEntryTable = from.createTable(DSApi.getTableName(JournalEntry.class), true);
        TableAlias flagsTable = null;
        if (query.flagIds != null)
            flagsTable = from.createJoinedTable(DSApi.getTableName(DocumentToLabelBean.class), true,
                docTable, FromClause.LEFT_OUTER, "$l.id = $r.document_id");
        TableAlias userFlagsTable = null;
        if (query.userFlagIds != null)
            userFlagsTable = from.createJoinedTable(DSApi.getTableName(DocumentToLabelBean.class), true,
                docTable, FromClause.LEFT_OUTER, "$l.id = $r.document_id");
//        TableAlias attachmentTable = null;
//        if (query.barcode != null)
//            attachmentTable = from.createTable(DSApi.getTableName(Attachment.class));

        WhereClause where = new WhereClause();

        if (query.flagIds != null && query.flagIds.length > 0)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < query.flagIds.length; i++)
            {
                Junction AND = Expression.conjunction();
                Long flag = query.flagIds[i];
                AND.add(Expression.eq(flagsTable.attribute("label_id"), flag));
                if (query.flagsDateFrom != null)
                    AND.add(Expression.ge(flagsTable.attribute("ctime"), DateUtils.midnight(query.flagsDateFrom, 0)));
                if (query.flagsDateTo != null)
                    AND.add(Expression.le(flagsTable.attribute("ctime"), DateUtils.midnight(query.flagsDateTo, 1)));
                OR.add(AND);
            }
            where.add(OR);
        }

        if (query.userFlagIds != null && query.userFlagIds.length > 0)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < query.userFlagIds.length; i++)
            {
                Junction AND = Expression.conjunction();
                Long flag = query.userFlagIds[i];
                AND.add(Expression.eq(userFlagsTable.attribute("label_id"), flag));
                if (query.userFlagsDateFrom != null)
                    AND.add(Expression.ge(userFlagsTable.attribute("ctime"), DateUtils.midnight(query.userFlagsDateFrom, 0)));
                if (query.userFlagsDateTo != null)
                    AND.add(Expression.le(userFlagsTable.attribute("ctime"), DateUtils.midnight(query.userFlagsDateTo, 1)));
                OR.add(AND);
            }
            where.add(OR);
            if (!OR.isEmpty())
                where.add(Expression.eq(userFlagsTable.attribute("username"), DSApi.context().getPrincipalName()));
        }

        TableAlias changelogTable = null;
        if (query.accessedBy != null || (query.accessedAs != null && query.accessedAs.length > 0))
        {
            changelogTable = from.createTable("ds_document_changelog", true);
            where.add(Expression.eqAttribute(docTable.attribute("id"),
                changelogTable.attribute("document_id")));

            if (query.accessedBy != null)
                where.add(Expression.eq(changelogTable.attribute("username"), query.accessedBy));

            if (query.accessedAs != null && query.accessedAs.length > 0)
            {
                Junction OR = Expression.disjunction();
                for (int i=0; i < query.accessedAs.length; i++)
                {
                    OR.add(Expression.eq(changelogTable.attribute("what"), query.accessedAs[i]));
                }
                where.add(OR);
            }

            if (query.accessedFrom != null)
                where.add(Expression.ge(changelogTable.attribute("ctime"), query.accessedFrom));
            if (query.accessedTo != null)
                where.add(Expression.le(changelogTable.attribute("ctime"), query.accessedTo));
        }
        else if(query.accessedFrom!=null || query.accessedTo!=null){
        	changelogTable = from.createTable("ds_document_changelog", true);
        	 where.add(Expression.eqAttribute(docTable.attribute("id"),
                     changelogTable.attribute("document_id")));
        	if (query.accessedFrom != null)
                where.add(Expression.ge(changelogTable.attribute("ctime"), query.accessedFrom));
            if (query.accessedTo != null)
                where.add(Expression.le(changelogTable.attribute("ctime"), query.accessedTo));
        }
        if (!DSApi.context().isAdmin() &&
            !(DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD) ||
              DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE) ||
              DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)))
        {
            DSUser user = DSApi.context().getDSUser();
            Junction OR = Expression.disjunction();

            if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD) ||
                DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_MODYFIKACJE))
            {
                DSDivision[] divisions = user.getDivisions();
                if (divisions.length > 0)
                {
                    for (int i=0; i < divisions.length; i++)
                    {
                        OR.add(Expression.eq(docTable.attribute("divisionguid"), divisions[i].getGuid()));
                        if (divisions[i].isPosition())
                        {
                            DSDivision parent = divisions[i].getParent();
                            if (parent != null)
                                OR.add(Expression.eq(docTable.attribute("divisionguid"), parent.getGuid()));
                        }
                    }
                    OR.add(Expression.eq(docTable.attribute("clerk"), user.getName()));
                    where.add(OR);
                }
            }

            if (DSApi.context().hasPermission(DSPermission.PISMO_KO_PODGLAD))
            {
                OR.add(Expression.eq(docTable.attribute("divisionguid"), DSDivision.ROOT_GUID));
            }

            OR.add(Expression.eq(docTable.attribute("clerk"), user.getName()));
            where.add(OR);
        }

        if (query.clerk != null)
        {
            where.add(Expression.eq(docTable.attribute("clerk"), query.clerk));
        }

        where.add(Expression.eq(docTable.attribute("internaldocument"), Boolean.valueOf(query.internal)));

        if (query.creatingUser != null)
        {
            where.add(Expression.eq(docTable.attribute("creatinguser"), query.creatingUser));
        }

        if (query.officeNumber != null)
        {
            where.add(Expression.eq(docTable.attribute("officenumber"), query.officeNumber));
        }

        if (query.officeNumberYear != null)
        {
            where.add(Expression.eq(docTable.attribute("officenumberyear"), query.officeNumberYear));
        }

        if (query.fromOfficeNumber != null && query.toOfficeNumber != null)
        {
            where.add(Expression.ge(docTable.attribute("officenumber"), query.fromOfficeNumber));
            where.add(Expression.le(docTable.attribute("officenumber"), query.toOfficeNumber));
        }

        if(query.referenceId != null)
        {
        	where.add(Expression.eq(docTable.attribute("referenceId"), query.referenceId));
        }

        if(query.duplicateDoc != null)
        {
        	where.add(Expression.eq(docTable.attribute("duplicateDoc"), query.duplicateDoc));
        }

        if(query.master_document_id != null)
        {
        	where.add(Expression.eq(docTable.attribute("master_document_id"), query.master_document_id));
        }


        if (query.documentDateFrom != null)
        {
            where.add(Expression.ge(docTable.attribute("documentdate"),
                DateUtils.midnight(query.documentDateFrom, 0)));
        }

        if (query.documentDateTo != null)
        {
            where.add(Expression.lt(docTable.attribute("documentdate"),
                DateUtils.midnight(query.documentDateTo, 1)));
        }

        if (query.zpoDateFrom != null)
        {
            where.add(Expression.ge(docTable.attribute("zpodate"),
                DateUtils.midnight(query.zpoDateFrom, 0)));
        }

        if (query.zpoDateTo != null)
        {
            where.add(Expression.lt(docTable.attribute("zpodate"),
                DateUtils.midnight(query.zpoDateTo, 1)));
        }

        if (query.officeDateFrom != null)
        {
            where.add(Expression.ge(docTable.attribute("officedate"),
                DateUtils.midnight(query.officeDateFrom, 0)));
        }

        if (query.officeDateTo != null)
        {
            where.add(Expression.lt(docTable.attribute("officedate"),
                DateUtils.midnight(query.officeDateTo, 1)));
        }

        if (query.sender != null)
        {
            where.add(Expression.eqAttribute(
                docTable.attribute("sender"), senderTable.attribute("id")));
            where.add(Expression.eq(senderTable.attribute("discriminator"), "SENDER"));

            String[] tokens = query.sender.toUpperCase().split("[\\s,.;:]+");

            if (tokens.length > 0)
            {
                Junction AND = Expression.conjunction();

                for (int i=0; i < tokens.length; i++)
                {
                    String token = tokens[i];

                    Junction OR = Expression.disjunction();

                    OR.add(Expression.like(Function.upper(senderTable.attribute("firstname")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("lastname")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("organization")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("organizationdivision")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("street")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("location")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("zip")),
                        "%"+StringUtils.substring(token, 0, 12)+"%"));

                    AND.add(OR);
                }

                where.add(AND);
            }
        }

        if (query.recipient != null)
        {
            where.add(Expression.eqAttribute(
                docTable.attribute("id"), recipientTable.attribute("document_id")));
            where.add(Expression.eq(recipientTable.attribute("discriminator"), "RECIPIENT"));

            String[] tokens = query.recipient.toUpperCase().split("[\\s,.;:]+");

            if (tokens.length > 0)
            {
                Junction AND = Expression.conjunction();

                for (int i=0; i < tokens.length; i++)
                {
                    String token = tokens[i];

                    Junction OR = Expression.disjunction();

                    OR.add(Expression.like(Function.upper(recipientTable.attribute("firstname")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("lastname")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("organization")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("organizationdivision")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("street")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("location")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("zip")),
                        "%"+StringUtils.substring(token, 0, 12)+"%"));

                    AND.add(OR);
                }

                where.add(AND);
            }
        }

        if (query.journalId != null)
        {
            where.add(Expression.eqAttribute(
                docTable.attribute("id"),
                journalEntryTable.attribute("documentid")));
            where.add(Expression.eq(journalEntryTable.attribute("journal_id"), query.journalId));
        }

        if (query.barcode != null)
        {
//            where.add(Expression.eqAttribute(docTable.attribute("id"), attachmentTable.attribute("document_id")));
//            where.add(Expression.eq(attachmentTable.attribute("barcode"), query.barcode));
            where.add(Expression.eq(docTable.attribute("barcode"), query.barcode));
        }

        if (!query.findCancelled)
        {
            where.add(Expression.eq(docTable.attribute("cancelled"), Boolean.FALSE));
        }

        if (query.postalRegNumber != null)
        {
            where.add(Expression.eq(docTable.attribute("postalregnumber"), query.postalRegNumber));
        }

        if (query.deliveryId != null)
        {
            where.add(Expression.eq(docTable.attribute("delivery"), query.deliveryId));
        }

        if (query.summary != null)
        {
            where.add(Expression.like(Function.upper(docTable.attribute("summary")), StringUtils.substring(query.summary.toUpperCase(), 0, 79)+"%"));
        }
        
        if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu"))
        {
        	where.add(Expression.eqAttribute(docTable.attribute("id"), documentTable.attribute("id")));
        	where.add(Expression.eq(documentTable.attribute("czy_aktualny"), Boolean.TRUE));
        }

        // kolumny, po kt�rych wykonywane jest sortowanie musz� si�
        // znale�� w SELECT
        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(docTable, "id");

        OrderClause order = new OrderClause();

        for (Iterator iter=query.sortFields().iterator(); iter.hasNext(); )
        {
            AbstractQuery.SortField field = (AbstractQuery.SortField) iter.next();
            // je�eli dodana b�dzie mo�liwo�� sortowania po innych polach,
            // nale�y uaktualni� metod� Query.validSortField()
            if (field.getName().equals("officeNumber"))
            {
                order.add(docTable.attribute("officenumber"), field.getDirection());
                selectId.add(docTable, "officenumber");
            }
            else if (field.getName().equals("documentDate"))
            {
                order.add(docTable.attribute("documentdate"), field.getDirection());
                selectId.add(docTable, "documentdate");
            }
        }

        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(distinct "+docTable.getAlias()+".id)");

        SelectQuery selectQuery;

        int totalCount;
        ResultSet rs = null;
        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);

            log.debug("xxfy");
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            if (!rs.next())
                throw new EdmException(sm.getString("NieMoznaPobracLiczbyDokumentow"));

            totalCount = rs.getInt(countCol.getPosition());

            rs.close();

            selectQuery = new SelectQuery(selectId, from, where, order);
            if (query.limit != 0)
            	selectQuery.setMaxResults(query.limit);
            selectQuery.setFirstResult(query.offset);
            rs = selectQuery.resultSet(DSApi.context().session().connection());

            List<T> results = new ArrayList<T>(query.limit);

            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                OutOfficeDocument doc = DSApi.context().load(OutOfficeDocument.class, id);
                results.add(mapper.act(doc));
            }

            rs.close();
            rs = null;

            return new SearchResultsAdapter<T>(results, query.offset, totalCount, mappedClass);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	if (rs!=null)
        	{
        		try { rs.close(); } catch (Exception e) { }
        	}
        }
    }

    public static Query query(int offset, int limit)
    {
        return new Query(offset, limit);
    }

    public static class Query extends AbstractQuery
    {
        private int offset;
        private int limit;
        private Integer officeNumber;
        private Integer officeNumberYear;
        private Date documentDateFrom;
        private Date documentDateTo;
        private Date officeDateFrom;
        private Date officeDateTo;
        private Date zpoDateFrom;
        private Date zpoDateTo;
        private String clerk;
        private String creatingUser;
        private String sender;
        private String recipient;
        private String barcode;
        private boolean findCancelled;
        private String postalRegNumber;
        private Integer deliveryId;
        private Integer fromOfficeNumber;
        private Integer toOfficeNumber;
        private Long journalId;
        private String summary;
        private boolean internal;
        private String accessedBy;
        private String[] accessedAs;
        private Date accessedFrom;
        private Date accessedTo;
        private Long[] flagIds;
        private Long[] userFlagIds;
        private Date flagsDateFrom;
        private Date flagsDateTo;
        private Date userFlagsDateFrom;
        private Date userFlagsDateTo;
		private String referenceId;
		private Long duplicateDoc;
		private Long master_document_id;
		private boolean czyAktualny;

		public Long getMaster_document_id() {
			return master_document_id;
		}

		public void setMaster_document_id(Long master_document_id) {
			this.master_document_id = master_document_id;
		}

		public Long getDuplicateDoc() {
			return duplicateDoc;
		}

		public void setDuplicateDoc(Long duplicateDoc) {
			this.duplicateDoc = duplicateDoc;
		}

		public Query(int offset, int limit)
        {
            this.offset = offset;
            this.limit = limit;
        }

        protected boolean validSortField(String field)
        {
            return "officeNumber".equals(field) ||
                "documentDate".equals(field);
        }

        public void setFlags(List<Long> flags, Date from, Date to)
        {
            flagIds = flags.toArray(new Long[flags.size()]);
            flagsDateFrom = from;
            flagsDateTo = to;
        }

        public void setUserFlags(List<Long> flags, Date from, Date to)
        {
            userFlagIds = flags.toArray(new Long[flags.size()]);
            userFlagsDateFrom = from;
            userFlagsDateTo = to;
        }

        public void setAccessedBy(String accessedBy)
        {
            this.accessedBy = accessedBy;
        }

        public void setAccessedAs(String[] accessedAs)
        {
            this.accessedAs = accessedAs;
        }

        public void setAccessedFrom(Date accessedFrom)
        {
            this.accessedFrom = accessedFrom;
        }

        public void setAccessedTo(Date accessedTo)
        {
            this.accessedTo = accessedTo;
        }

        public void setInternal(boolean internal)
        {
            this.internal = internal;
        }

        public void setDocumentDateFrom(Date documentDateFrom)
        {


            this.documentDateFrom = documentDateFrom;
        }

        public void setDocumentDateTo(Date documentDateTo)
        {
            this.documentDateTo = documentDateTo;
        }

        public void setZpoDateFrom(Date zpoDateFrom)
        {
            this.zpoDateFrom = zpoDateFrom;
        }

        public void setZpoDateTo(Date zpoDateTo)
        {
            this.zpoDateTo = zpoDateTo;
        }

        public void setClerk(String clerk)
        {
            this.clerk = clerk;
        }

        public void setCreatingUser(String creatingUser)
        {
            this.creatingUser = creatingUser;
        }

        public void setOfficeNumber(Integer officeNumber)
        {
            this.officeNumber = officeNumber;
        }

        public void setOfficeNumberYear(Integer officeNumberYear)
        {
            this.officeNumberYear = officeNumberYear;
        }

        public void setSender(String sender)
        {
            this.sender = sender;
        }

        public void setRecipient(String recipient)
        {
            this.recipient = recipient;
        }

        public void setBarcode(String barcode)
        {
            this.barcode = barcode;
        }

        public void setFindCancelled(boolean findCancelled)
        {
            this.findCancelled = findCancelled;
        }

        public void setPostalRegNumber(String postalRegNumber)
        {
            this.postalRegNumber = postalRegNumber;
        }

        public void setDeliveryId(Integer deliveryId)
        {
            this.deliveryId = deliveryId;
        }

        public void setFromOfficeNumber(Integer fromOfficeNumber)
        {
            this.fromOfficeNumber = fromOfficeNumber;
        }

        public void setToOfficeNumber(Integer toOfficeNumber)
        {
            this.toOfficeNumber = toOfficeNumber;
        }

        public void setJournalId(Long journalId)
        {
            this.journalId = journalId;
        }

        public void setSummary(String summary)
        {
            this.summary = summary;
        }

        public Date getOfficeDateFrom()
        {
            return officeDateFrom;
        }

        public void setOfficeDateFrom(Date officeDateFrom)
        {
            this.officeDateFrom = officeDateFrom;
        }

        public Date getOfficeDateTo()
        {
            return officeDateTo;
        }

        public void setOfficeDateTo(Date officeDateTo)
        {
            this.officeDateTo = officeDateTo;
        }

		public void setReferenceId(String referenceId)
		{

			this.referenceId = referenceId;
		}
        public String getReferenceId()
        {
			return referenceId;
		}
        
        public boolean getCzyAktaulny()
        {
        	return czyAktualny;
        }
        
        public void setCzyAktaulany(boolean czyAktualny)
        {
        	this.czyAktualny=czyAktualny;
        }

    }


    public Date getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(Date documentDate) throws EdmException {
		if (getId() != null) 
		{
			if (this.documentDate != null && documentDate != null && this.documentDate.getTime()!=documentDate.getTime()) 
			{
				addWorkHistoryEntry(Audit.create("documentDate",DSApi.context().getPrincipalName(),
							sm.getString("ZmianaDatyDokumentuNa",DateUtils.formatCommonDate(documentDate)),
								DateUtils.formatPortableDateTime(documentDate)));
			} 
			else if(this.documentDate == null && documentDate != null)	
			{
				addWorkHistoryEntry(Audit.create("documentDate",DSApi.context().getPrincipalName(),
							sm.getString("NadanieDatyDokumentu",DateUtils.formatCommonDate(documentDate)),
						DateUtils.formatPortableDateTime(documentDate)));
			} 
			else if(this.documentDate != null && documentDate == null)
			{
				addWorkHistoryEntry(Audit.create("documentDate",DSApi.context().getPrincipalName(),
						sm.getString("UsuniecieDatyDokumentu")));
			}
		}

		this.documentDate = documentDate;
	}

    public String getPostalRegNumber()
    {
        return postalRegNumber;
    }

    public void setPostalRegNumber(String postalRegNumber) throws EdmException {
		if (getId() != null) {

			if (this.postalRegNumber != null && postalRegNumber != null
					&& !this.postalRegNumber.equals(postalRegNumber)) {
				addWorkHistoryEntry(
						Audit.create("postalRegNumber", DSApi.context()
								.getPrincipalName(), sm.getString(
								"ZmianaNumeruPrzesylkiRejestrowanej",
								this.postalRegNumber, postalRegNumber)));
			} else if(this.postalRegNumber == null && (postalRegNumber != null && !postalRegNumber.equals(""))){
				addWorkHistoryEntry(
						Audit.create("postalRegNumber", DSApi.context()
								.getPrincipalName(), sm.getString(
								"NadanoNumerPrzesylkiRejestrowanej",
								postalRegNumber)));
			} else if(this.postalRegNumber != null && postalRegNumber == null){
				addWorkHistoryEntry(
						Audit.create("postalRegNumber", DSApi.context()
								.getPrincipalName(), sm.getString(
								"UsunietoNumerPrzesylkiRejestrowanej")));
			}
		}
		this.postalRegNumber = postalRegNumber;
	}

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }

    public Long getInDocumentId()
    {
        return inDocumentId;
    }

    public void setInDocumentId(Long inDocumentId)
    {
        this.inDocumentId = inDocumentId;
    }

    public Date getZpoDate()
    {
        return zpoDate;
    }

    public void setZpoDate(Date zpoDate) throws EdmException
    {
        if (getId() != null &&
            this.zpoDate != null && !this.zpoDate.equals(zpoDate))
        {
        	addWorkHistoryEntry(Audit.create("zpoDate", DSApi.context().getPrincipalName(),
                (zpoDate != null ?
                    sm.getString("ZmianaDatyZPOna",DateUtils.formatCommonDate(zpoDate)) :
                    sm.getString("UsuniecieDatyZPO")),
                null, zpoDate != null ? zpoDate.getTime() : 0));
        }
        else if (this.zpoDate == null && zpoDate != null)
        {
        	addWorkHistoryEntry(Audit.create("zpoDate", DSApi.context().getPrincipalName(),
                sm.getString("NadanieDatyZPO",DateUtils.formatCommonDate(zpoDate)),
                null, zpoDate.getTime()));
        }

//        propertyChangeSupport.firePropertyChange("zpoDate", this.zpoDate, zpoDate);

        this.zpoDate = zpoDate;
    }

    public String getPrepState()
    {
        return prepState;
    }
    public boolean canBeUpdate(){
    	if(Configuration.hasExtra("business")){
    		return true;
    	}
    	return prepState.equals(PREP_STATE_DRAFT);
    }

    public void setPrepState1(String prepState){
    	this.prepState = prepState;
    }
    public void setPrepState(String prepState) throws EdmException
    {

        if (prepState == null)
            throw new NullPointerException("prepState");

        if (getId() != null &&
            !prepState.equals(this.prepState))
        {
            if (DSApi.isContextOpen() && getId() != null &&
                ((PREP_STATE_FAIR_COPY.equals(prepState) && !DSApi.context().hasPermission(DSPermission.PISMO_AKCEPTACJA_CZYSTOPISU)) ||
                 (PREP_STATE_DRAFT.equals(prepState) && this.prepState != null && !DSApi.context().hasPermission(DSPermission.PISMO_AKCEPTACJA_CZYSTOPISU_COFNIECIE))))
            {
            		throw new AccessDeniedException(sm.getString("BrakUprawnienDoZmianyStanuPismaNa")+" "+
            				(PREP_STATE_DRAFT.equals(prepState) ? sm.getString("Brudnopis") : sm.getString("Czystopis")));
            }

            if(PREP_STATE_DRAFT.equals(prepState))
            	addWorkHistoryEntry(Audit.create(
                    "prepState", DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaStanuNaBrudnopis"),
                    prepState));
            else
            	addWorkHistoryEntry(Audit.create(
                    "prepState", DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaStanuNaCzystopis"),
                    prepState));
        }

//        propertyChangeSupport.firePropertyChange("prepState", this.prepState, prepState);

        this.prepState = prepState;
    }

    public OutOfficeDocumentDelivery getDelivery()
    {
        return delivery;
    }

    /**
     * Zmienia spos�b odbioru pisma.
     */
    public void setDelivery(OutOfficeDocumentDelivery delivery) throws EdmException
    {
        if (getId() != null &&
            this.delivery != null && !this.delivery.equals(delivery))
        {
        	addWorkHistoryEntry(Audit.create("delivery", DSApi.context().getPrincipalName(),
                sm.getString("ZmianaSposobuOdbioruPismaNa",delivery.getName()),
                delivery.getName(), new Long(delivery.getId())));
        }

        this.delivery = delivery;
    }

    public OutOfficeDocumentReturnReason getReturnReason()
    {
        return returnReason;
    }

    /**
     * Ustawia nowy pow�d zwrotu pisma. Je�eli przekazany obiekt
     * ma warto�� null, polu <code>returned</code> przypisywana
     * jest warto�� <code>false</code>.
     * @param returnReason
     * @throws EdmException
     */
    public void setReturnReason(OutOfficeDocumentReturnReason returnReason) throws EdmException
    {
        if (getId() != null &&
            this.returnReason != null &&
            !this.returnReason.equals(returnReason))
        {
            if (returnReason != null)
            {
            	addWorkHistoryEntry(Audit.create("returnReason", DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaPowoduZwrotuPismaNa",returnReason.getName()),
                    returnReason.getName(), new Long(returnReason.getId())));
            }
            else
            {
            	addWorkHistoryEntry(Audit.create("returnReason", DSApi.context().getPrincipalName(),
                    sm.getString("UsunieciePowoduZwrotuPisma")));
            }
        }

        this.returnReason = returnReason;
        if (returnReason == null)
            this.returned = false;
    }

    public boolean isReturned()
    {
        return returned;
    }

    public void setReturned(boolean returned)
    {
        this.returned = returned;
    }

    public boolean isInternal()
    {
        return internal;
    }

    public void setInternal(boolean internal)
    {
        this.internal = internal;
    }

    public void setOfficeNumber(Integer officeNumber) throws EdmException
    {
        if (getId() != null && getOfficeNumber() == null && officeNumber != null)
        {
        	addWorkHistoryEntry(Audit.create(
                "officeNumber", DSApi.context().getPrincipalName(),
                sm.getString("PismuNadanoNumerKancelaryjny",officeNumber), null,
                new Long(officeNumber)));
        }

        if(AvailabilityManager.isAvailable("usun.ko")) {
            if (getId() != null && officeNumber == null) {
                addWorkHistoryEntry(Audit.create(
                        "officeNumber", DSApi.context().getPrincipalName(),
                        sm.getString("NumerKOpismaZostalUsuniety"), null));
            }
        }

        super.setOfficeNumber(officeNumber);
    }

    public BigDecimal getStampFee()
    {
        return stampFee;
    }

    public void setStampFee(BigDecimal stampFee)
    {
        this.stampFee = stampFee;
    }

    public String getReferenceId()
    {
        return referenceId;
    }

    public void setReferenceId(String referenceId) throws EdmException
    {
        if (getId() != null &&
            this.referenceId != null && !this.referenceId.equals(referenceId))
        {
        	addWorkHistoryEntry(Audit.create("referenceId",
                DSApi.context().getPrincipalName(),
                sm.getString("ZmianaZnakuPismaNa",referenceId),
                referenceId));
        }

        this.referenceId = referenceId;
    }

    public static String getPrepStateString(String prepState)
    {
        if (PREP_STATE_DRAFT.equals(prepState))
            return sm.getString("Brudnopis");
        else if (PREP_STATE_FAIR_COPY.equals(prepState))
            return sm.getString("Czystopis");

        return null;
    }
    public Date getOfficedate()
    {
        return officedate;
    }
    public void setOfficeDate(Date officedate)
    {
        this.officedate = officedate;
    }

    public OutOfficeDocument getMasterDocument() throws EdmException
    {
    	if (getMasterDocumentId() == null)
    		return masterDocument;
        if (masterDocument == null || !masterDocument.getId().equals(getMasterDocumentId()))
        	masterDocument = Finder.find(OutOfficeDocument.class, getMasterDocumentId());
        return masterDocument;
    }

    public void setMasterDocument(OutOfficeDocument masterDocument)
    {
        this.masterDocument = masterDocument;
        super.setMasterDocumentId(masterDocument.getId());
    }

	public Integer getWeightG() {
		return weightG;
	}
	public void setWeightG(Integer weightG) {
		this.weightG = weightG;
	}
	public Integer getWeightKg() {
		return weightKg;
	}
	public void setWeightKg(Integer weightKg) {
		this.weightKg = weightKg;
	}
	public Boolean getPriority() {
		return priority;
	}
	public void setPriority(Boolean priority) {
		this.priority = priority;
	}

	/**
	 * @return the inDocument
	 */
	public InOfficeDocument getInDocument() {
		Criteria c;
		try {
			c = DSApi.context().session().createCriteria(InOfficeDocument.class);
			c.add(org.hibernate.criterion.Expression.eq("id", this.inDocumentId));
			if (c.list() != null && c.list().size() > 0)
				inDocument = (InOfficeDocument) c.list().get(0);
		} catch (EdmException ex) {
			log.error("", ex);
		}
		return inDocument;
	}
	
	@Override
	public void removeRecipient(Recipient recipient) throws EdmException
	{
		getRecipients();
		if(recipients==null) return;
		if(recipients.contains(recipient))
			recipients.remove(recipient);
		DSApi.context().session().delete(recipient);
	}
	public Boolean getZpo() {
		return zpo;
	}
	public void setZpo(Boolean zpo) {
		this.zpo = zpo;
	}
	
	public Integer getGauge() {
		return gauge;
	}
	
	public void setGauge(Integer gauge) {
		this.gauge = gauge;
	}
	public void setCountryLetter(Boolean countryLetter)
	{
		this.countryLetter = countryLetter;
	}
	public Boolean getCountryLetter()
	{
		return countryLetter;
	}

    public void setMailWeight(Integer mailWeight) {
        this.mailWeight = mailWeight;
    }

    public Integer getMailWeight() {
        return mailWeight;
    }

    public Boolean getAdditionalZpo() {
        return additionalZpo;
    }

    public void setAdditionalZpo(Boolean additionalZpo) {
        this.additionalZpo = additionalZpo;
    }

    /*public Boolean isCanceled()
	{
		return cancelled;
	}
	
	public void setCanceled(Boolean cancelled)
	{
		this.cancelled = cancelled;
	}*/
	
}

