package pl.compan.docusafe.core.office;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.select.*;

import java.sql.ResultSet;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Klasa reprezentuj�ca osoby. Klasy dziedzicz�ce {@link Recipient} i
 * {@link Sender} u�ywane s� w dokumentach kancelaryjnych w celu reprezentowania
 * odbiorc�w i nadawc�w pism.
 * <p/>
 * Wszystkie rodzaje os�b przechowywane s� w jednej tabeli bazodanowej. Na
 * podstawie tylko tej tabeli nie mo�na stwierdzi�, czy obiekt osoby jest
 * zwi�zany z jakim� dokumentem, dotyczy to obiekt�w Person przypisywanych do
 * pojedynczego pola dokumentu (nie kolekcji), np. InOfficeDocument.sender.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Person.java,v 1.69 2010/08/11 14:47:24 mariuszk Exp $
 */
public class Person extends AbstractDocumentKindDictionary implements Cloneable {
    public static final String NOT_NULLABLE_FIELDS_PARAM = "notNullableFields";
    public static final String DICTIONARY_RECIPIENT = "recipient";
    public static final String DICTIONARY_SENDER = "sender";
    public static final String DICTIONARY_SENDER_RECIPIENT = "c";
    public static final String DICTIONARY_CLERK = "clerk";
    private static final Logger LOG = LoggerFactory.getLogger(Person.class);

    public static final String SENDER_CN = "SENDER";
    public static final String RECIPIENT_CN = "RECIPIENT";

    private Long id;
    private boolean anonymous;
    private String title;
    private String firstname;
    private String lastname;
    private String personGroup;
    private String organization;
    private String organizationDivision;
    private String street;
    private String zip;
    private String location;
    private String country;
    private String pesel;
    private String nip;
    private String regon;
    private String email;
    private String fax;
    private String remarks;
    private String dictionaryType;
    private String dictionaryGuid;
    protected Document document;
    private Long documentId;
    private String lparam;
    private Long wparam;
    private Integer posn;
    private String adresSkrytkiEpuap;
    private String identifikatorEpuap;
    private Boolean zgodaNaWysylke;
    private Long basePersonId;
    private String phoneNumber;
    private String externalID;

    public Object getParam(String param, String prefix) throws EdmException {
        if (param.equals("id")) return id;
        else if (param.equals(prefix + "ANONYMOUS")) return anonymous;
        else if (param.equals(prefix + "TITLE")) return title;
        else if (param.equals(prefix + "FIRSTNAME")) return firstname;
        else if (param.equals(prefix + "LASTNAME")) return lastname;
        else if (param.equals(prefix + "PERSONGROUP")) return personGroup;
        else if (param.equals(prefix + "ORGANIZATION")) return organization;
        else if (param.equals(prefix + "ORGANIZATIONDIVISION")) return organizationDivision;
        else if (param.equals(prefix + "STREET")) return street;
        else if (param.equals(prefix + "ZIP")) return zip;
        else if (param.equals(prefix + "LOCATION")) return location;
        else if (param.equals(prefix + "COUNTRY")) return country;
        else if (param.equals(prefix + "PESEL")) return pesel;
        else if (param.equals(prefix + "NIP")) return nip != null ? nip.replace("-", "").replace(" ", "") : nip;
        else if (param.equals(prefix + "REGON")) return regon;
        else if (param.equals(prefix + "EMAIL")) return email;
        else if (param.equals(prefix + "FAX")) return fax;
        else if (param.equals(prefix + "REMARKS")) return remarks;
        else if (param.equals(prefix + "DICTIONARYTYPE")) return dictionaryType;
        else if (param.equals(prefix + "DICTIONARYGUID")) return dictionaryGuid;
        else if (param.equals(prefix + "DOCUMENT")) return document;
        else if (param.equals(prefix + "DOCUMENTID")) return documentId;
        else if (param.equals(prefix + "LPARAM")) return lparam;
        else if (param.equals(prefix + "WPARAM")) return wparam;
        else if (param.equals(prefix + "POSN")) return posn;
        else if (param.equals(prefix + "ADRESSKRYTKIEPUAP")) return adresSkrytkiEpuap;
        else if (param.equals(prefix + "IDENTIFIKATOREPUAP")) return identifikatorEpuap;
        else if (param.equals(prefix + "ZGODANAWYSYLKE")) return zgodaNaWysylke;
        else if (param.equals(prefix + "BASEPERSONID")) return basePersonId;
        else if (param.equals(prefix + "PHONENUMBER")) return phoneNumber;
        else if (param.equals(prefix + "EXTERNALID")) return externalID;
        else throw new EdmException("brak danego pola");
    }

    public void setParam(Map<String, Object> input) throws EdmException {
        try {
            for (String fieldName : input.keySet()) {
                if (fieldName.equals("ID")) id = (Long) input.get(fieldName);
                else if (fieldName.equals("ANONYMOUS")) anonymous = (Boolean) input.get(fieldName);
                else if (fieldName.equals("TITLE")) title = (String) input.get(fieldName);
                else if (fieldName.equals("FIRSTNAME")) firstname = (String) input.get(fieldName);
                else if (fieldName.equals("LASTNAME")) lastname = (String) input.get(fieldName);
                else if (fieldName.equals("PERSONGROUP")) personGroup = (String) input.get(fieldName);
                else if (fieldName.equals("ORGANIZATION")) organization = (String) input.get(fieldName);
                else if (fieldName.equals("ORGANIZATIONDIVISION")) organizationDivision = (String) input.get(fieldName);
                else if (fieldName.equals("STREET")) street = (String) input.get(fieldName);
                else if (fieldName.equals("ZIP")) zip = (String) input.get(fieldName);
                else if (fieldName.equals("LOCATION")) location = (String) input.get(fieldName);
                else if (fieldName.equals("COUNTRY")) country = (String) input.get(fieldName);
                else if (fieldName.equals("PESEL")) pesel = (String) input.get(fieldName);
                else if (fieldName.equals("NIP")) nip = (String) input.get(fieldName);
                else if (fieldName.equals("REGON")) regon = (String) input.get(fieldName);
                else if (fieldName.equals("EMAIL")) email = (String) input.get(fieldName);
                else if (fieldName.equals("FAX")) fax = (String) input.get(fieldName);
                else if (fieldName.equals("REMARKS")) remarks = (String) input.get(fieldName);
                else if (fieldName.equals("DICTIONARYTYPE")) dictionaryType = (String) input.get(fieldName);
                else if (fieldName.equals("DICTIONARYGUID")) dictionaryGuid = (String) input.get(fieldName);
                else if (fieldName.equals("DOCUMENT")) document = (Document) input.get(fieldName);
                else if (fieldName.equals("DOCUMENTID")) documentId = (Long) input.get(fieldName);
                else if (fieldName.equals("LPARAM")) lparam = (String) input.get(fieldName);
                else if (fieldName.equals("WPARAM")) wparam = (Long) input.get(fieldName);
                else if (fieldName.equals("POSN")) posn = (Integer) input.get(fieldName);
                else if (fieldName.equals("ADRESSKRYTKIEPUAP")) adresSkrytkiEpuap = (String) input.get(fieldName);
                else if (fieldName.equals("IDENTIFIKATOREPUAP")) identifikatorEpuap = (String) input.get(fieldName);
                else if (fieldName.equals("ZGODANAWYSYLKE")) zgodaNaWysylke = (Boolean) input.get(fieldName);
                else if (fieldName.equals("BASEPERSONID")) basePersonId = (Long) input.get(fieldName);
                else if (fieldName.equals("PHONENUMBER")) phoneNumber = (String) input.get(fieldName);
                else if (fieldName.equals("EXTERNALID")) externalID = (String) input.get(fieldName);
            }
        } catch (Exception e) {
            //TO-DO
        }
    }

    public Boolean getZgodaNaWysylke() {
        return zgodaNaWysylke;
    }

    public void setZgodaNaWysylke(Boolean zgodaNaWysylke) {
        this.zgodaNaWysylke = zgodaNaWysylke;
    }

    public final Person cloneObject() throws EdmException {
        try {
            Person clone = (Person) super.clone();
            clone.duplicate();
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }

    protected void duplicate() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public void create() throws EdmException {

        try {
            DSApi.context().session().save(this);
            //AbstractParametrization.getInstance().setAfterCreatePerson(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public void update() throws EdmException {
        try {
            DSApi.context().session().update(this);
            // AbstractParametrization.getInstance().setAfterCreatePerson(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException {
        try {
            DSApi.context().session().delete(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zapisuje w bazie danych bie��cy obiekt, je�eli nie istnieje inny o
     * takich samych warto�ciach p�l.
     *
     * @return
     * @throws EdmException
     */
    public boolean createIfNew() throws EdmException {
        try {
            QueryForm form = new QueryForm(0, 10);
            if (!StringUtils.isEmpty(firstname))
                form.addProperty("firstname", firstname.trim());
            if (!StringUtils.isEmpty(lastname))
                form.addProperty("lastname", lastname.trim());
            if (!StringUtils.isEmpty(organization))
                form.addProperty("organization", organization.trim());
            if (!StringUtils.isEmpty(street))
                form.addProperty("street", street.trim());
            if (!StringUtils.isEmpty(zip))
                form.addProperty("zip", zip.trim());
            if (!StringUtils.isEmpty(location))
                form.addProperty("location", location.trim());
            if (!StringUtils.isEmpty(country))
                form.addProperty("country", country.trim());
            if (!StringUtils.isEmpty(email))
                form.addProperty("email", email.trim());
            if (!StringUtils.isEmpty(fax))
                form.addProperty("fax", fax.trim());
            if (!StringUtils.isEmpty(nip))
                form.addProperty("nip", nip != null ? nip.replace("-", "").replace(" ", "") : nip.trim());
            if (!StringUtils.isEmpty(pesel))
                form.addProperty("pesel", pesel.trim());
            if (!StringUtils.isEmpty(regon))
                form.addProperty("regon", regon.trim());
            if (!StringUtils.isEmpty(personGroup))
                form.addProperty("personGroup", personGroup.trim());
            if (!StringUtils.isEmpty(dictionaryType))
                form.addProperty("dictionaryType", dictionaryType);
            if (!StringUtils.isEmpty(dictionaryGuid))
                form.addProperty("dictionaryGuid", dictionaryGuid);
            if (!StringUtils.isEmpty(phoneNumber))
                form.addProperty("phoneNumber", phoneNumber);
            if (!StringUtils.isEmpty(externalID))
                form.addProperty("externalID", externalID);
            if (wparam != null)
                form.addProperty("wparam", wparam);
            if (!StringUtils.isEmpty(lparam))
                form.addProperty("lparam", lparam);
            else {
                if (StringUtils.isEmpty(dictionaryGuid))
                    if (form.getProperty(dictionaryGuid) == null) {
                        form.addProperty("dictionaryGuid", DSDivision.ROOT_GUID);
                        if (!StringUtils.isEmpty(organizationDivision))
                            form.addProperty("organizationDivision", organizationDivision.trim());
                    }

            }
            //if (!StringUtils.isEmpty(organizationDivision))
            //	form.addProperty("organizationDivision", organizationDivision.trim());

            /** Szukamy tylko wpisow slownikowych */
            form.addProperty("DISCRIMINATOR", "PERSON");

            SelectClause select = new SelectClause(false);
            FromClause from = new FromClause();
            WhereClause where = new WhereClause();
            OrderClause order = new OrderClause();
            createQuery(select, from, where, order, form);

            /** Wyciaganie ilosci zadan */
            SelectClause selectCount = new SelectClause();
            SelectColumn countCol = selectCount.addSql("count(distinct id)");
            SelectQuery selectQueryCount = new SelectQuery(selectCount, from, where, null);
            ResultSet rs = selectQueryCount.resultSet();
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby zada�.");
            int totalCount = rs.getInt(countCol.getPosition());
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            if (totalCount == 0) {
                DSApi.context().session().save(this);
                return true;
            }
            return false;
        } catch (Exception e) {
            throw new EdmException(e);
        }
    }

    public static Person find(long id) throws EdmException {
        Person person = (Person) DSApi.getObject(Person.class, id);

        if (person == null)
            throw new EntityNotFoundException(Person.class, id);

        return person;
    }

    //metoda wyszukuje wszystkich person�w kt�rych base person ma id takie jak basePersonId
    public static List<Person> getByBasePerson(long basePersonId) throws EdmException {
        return DSApi.context().session().createCriteria(Person.class).add(Restrictions.eq("basePersonId", basePersonId)).list();
    }

    public static Person findIfExist(long id) throws EdmException {
        return (Person) DSApi.getObject(Person.class, id);
    }

    public static List<String> findGroups(String personGroup) throws EdmException {
        try {
            List groups = DSApi.context().classicSession().find("select distinct p.personGroup from p in class " + Person.class.getName() + " where p.personGroup is not null and p.personGroup like '%" + (personGroup == null ? "" : personGroup) + "%' order by p.personGroup");/*
                                                                                                                                                                                                                         * ,
																																																						 * new
																																																						 * Object
																																																						 * [
																																																						 * ]
																																																						 * {
																																																						 * personGroup
																																																						 * }
																																																						 * ,
																																																						 * new
																																																						 * Type
																																																						 * [
																																																						 * ]
																																																						 * {
																																																						 * Hibernate
																																																						 * .
																																																						 * STRING
																																																						 * }
																																																						 */

            return (List<String>) groups;
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public void createBasePerson(String dictionaryName, Map<String, FieldData> values) {
        PersonDictionary.prepareFieldNamesForBasePerson(dictionaryName, values);
        PersonDictionary.setPersonValue(dictionaryName, this, values);
    }

    /**
     * Znajduje obiekty Person na podstawie formularza wyszukiwania. UWAGA:
     * znajdowane s� wy��cznie obiekty klasy Person, nie klas pochodnych.
     *
     * @param form
     * @return
     * @throws EdmException
     */
    public static SearchResults<? extends Person> search(QueryForm form) throws EdmException {
        try {
            SelectClause select = new SelectClause(false);
            FromClause from = new FromClause();
            WhereClause where = new WhereClause();
            OrderClause order = new OrderClause();
            createQuery(select, from, where, order, form);

            /** Wyciaganie ilosci zadan */
            SelectClause selectCount = new SelectClause();
            SelectColumn countCol = selectCount.addSql("count(distinct id)");
            SelectQuery selectQueryCount = new SelectQuery(selectCount, from, where, null);
            ResultSet rs = selectQueryCount.resultSet();
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby zada�.");
            int totalCount = rs.getInt(countCol.getPosition());
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            /** Wyciaganie paczki wynikow */
            SelectQuery selectQuery = new SelectQuery(select, from, where, order);
            if (form.getLimit() > 0) {
                selectQuery.setMaxResults(form.getLimit());
                selectQuery.setFirstResult(form.getOffset());
            } else {
                selectQuery.setMaxResults(20);
            }
            rs = selectQuery.resultSet();
            Person p = null;
            List<Person> l = new ArrayList<Person>();
            while (rs.next()) {
                Long id = new Long(rs.getLong(1));
                p = DSApi.context().load(Person.class, id);
                l.add(p);
            }
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());
            return new SearchResultsAdapter<Person>(l, form.getOffset(), totalCount, Person.class);
        } catch (Exception e) {
            throw new EdmException(e);
        }
    }

    private static void createQuery(SelectClause select, FromClause from, WhereClause where, OrderClause order, QueryForm form) {
        TableAlias person = from.createTable("dso_person");
        select.add(person, "id");
        if (form.hasProperty("documentId")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(person.attribute("document_id"), form.getProperty("documentId")));
        }
        if (form.hasProperty("DISCRIMINATOR")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(person.attribute("DISCRIMINATOR"), form.getProperty("DISCRIMINATOR")));
        }
        if (form.hasProperty("lastname")) // like
        {
            pl.compan.docusafe.util.querybuilder.expression.Expression like = null;
            if (AvailabilityManager.isAvailable("person.likeLeftRight")) {
                like = pl.compan.docusafe.util.querybuilder.expression.Expression.likeLeftRight(person.attribute("lastname"), StringUtils.left((String) form.getProperty("lastname"), 48), true);
            } else {
                like = pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("lastname"), StringUtils.left((String) form.getProperty("lastname"), 49) + "%", true);
            }
            where.add(like);
        }
        if (form.hasProperty("title")) {
            pl.compan.docusafe.util.querybuilder.expression.Expression like = null;
            if (AvailabilityManager.isAvailable("person.likeLeftRight")) {
                like = pl.compan.docusafe.util.querybuilder.expression.Expression.likeLeftRight(person.attribute("title"), StringUtils.left((String) form.getProperty("title"), 48), true);
            } else {
                like = pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("title"), StringUtils.left((String) form.getProperty("title"), 49) + "%", true);
            }
            where.add(like);
        }
        if (form.hasProperty("dictionaryType")) {
            if (AvailabilityManager.isAvailable("person.selection") && !form.getProperty("dictionaryType").equals(Person.DICTIONARY_SENDER_RECIPIENT)) {
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(person.attribute("dictionaryType"), form.getProperty("dictionaryType")));
            }
        }
        if (form.hasProperty("dictionaryGuid")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(person.attribute("dictionaryGuid"), form.getProperty("dictionaryGuid")));
        }
        if (form.hasProperty("firstname")) // like
        {
            pl.compan.docusafe.util.querybuilder.expression.Expression like = null;
            if (AvailabilityManager.isAvailable("person.likeLeftRight")) {
                like = pl.compan.docusafe.util.querybuilder.expression.Expression.likeLeftRight(person.attribute("firstname"), StringUtils.left((String) form.getProperty("firstname"), 48), true);
            } else {
                like = pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("firstname"), StringUtils.left((String) form.getProperty("firstname"), 49) + "%", true);
            }
            where.add(like);
        }
        if (form.hasProperty("organization")) // like
        {
            pl.compan.docusafe.util.querybuilder.expression.Expression like = null;
            if (AvailabilityManager.isAvailable("person.likeLeftRight")) {
                like = pl.compan.docusafe.util.querybuilder.expression.Expression.likeLeftRight(person.attribute("organization"), StringUtils.left((String) form.getProperty("organization"), 247), true);
            } else {
                like = pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("organization"), StringUtils.left((String) form.getProperty("organization"), 248) + "%", true);
            }
            where.add(like);
        }
        if (form.hasProperty("street")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("street"), StringUtils.left((String) form.getProperty("street"), 49) + "%", true));
        }
        if (form.hasProperty("zip")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("zip"), StringUtils.left((String) form.getProperty("zip"), 13) + "%", true));
        }

        if (form.hasProperty("location")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("location"), StringUtils.left((String) form.getProperty("location"), 49) + "%", true));
        }
        if (form.hasProperty("country")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(person.attribute("country"), form.getProperty("country")));
        }
        if (form.hasProperty("email")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("email"), StringUtils.left((String) form.getProperty("email"), 49) + "%", true));
        }
        if (form.hasProperty("fax")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("fax"), StringUtils.left((String) form.getProperty("fax"), 29) + "%", true));
        }
        if (form.hasProperty("identifikatorEpuap")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(person.attribute("identifikatorEpuap"), form.getProperty("identifikatorEpuap")));
        }
        if (form.hasProperty("adresSkrytkiEpuap")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(person.attribute("adresSkrytkiEpuap"), form.getProperty("adresSkrytkiEpuap")));
        }
        if (form.hasProperty("nip")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("nip"), StringUtils.left(((String) form.getProperty("nip")).replace("-", "").replace(" ", ""), 14) + "%"));
        }
        if (form.hasProperty("pesel")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("pesel"), StringUtils.left((String) form.getProperty("pesel"), 29) + "%", true));
        }
        if (form.hasProperty("lparam")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(person.attribute("lparam"), form.getProperty("lparam")));
        }
        if (form.hasProperty("wparam")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(person.attribute("wparam"), form.getProperty("wparam")));
        }
        if (form.hasProperty("regon")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("regon"), StringUtils.left((String) form.getProperty("regon"), 14) + "%"));
        }
        if (form.hasProperty("organizationdivision")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("organizationDivision"), StringUtils.left((String) form.getProperty("organizationdivision"), 39) + "%"));
        }
        if (form.hasProperty("personGroup")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(person.attribute("personGroup"), form.getProperty("personGroup")));
        }
        if (form.hasProperty("phoneNumber")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("phoneNumber"), StringUtils.left(((String) form.getProperty("phoneNumber")).replace("-", "").replace(" ", ""), 14) + "%"));
        }
        if (form.hasProperty("externalID")) {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(person.attribute("externalID"), StringUtils.left((String) form.getProperty("externalID"), 49) + "%"));
        }
        if (form.hasProperty(NOT_NULLABLE_FIELDS_PARAM)) {
            try {
                List<String> notNullableFields = (List<String>) form.getProperty(NOT_NULLABLE_FIELDS_PARAM);
                for (String field : notNullableFields) {
                    where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ne(person.attribute(field), null));
                }
            } catch (Exception e) {
                LOG.error("Nie mo�na ustawi� wykonana� ograniczenia: not-nullable-fields dla s�ownika ds-person " + e.toString());
            }
        }
        for (Iterator iter = form.getSortFields().iterator(); iter.hasNext(); ) {
            QueryForm.SortField field = (QueryForm.SortField) iter.next();
            if (field.isAscending())
                order.add(person.attribute(field.getName()), true);
            else
                order.add(person.attribute(field.getName()), false);
        }
    }

    public Person fromJSON(String json) throws EdmException {
        return readJSON(json);
    }

    protected Person readJSON(String json) throws EdmException {
        JSONObject js;
        try {
            js = new JSONObject(json);
        } catch (ParseException e) {
            throw new EdmException("Niepoprawne wyra�enie json=" + json, e);
        }

        // poprzez metod�, bo wyst�puje tam sprawdzenie poprawno�ci typu
        // s�ownika
        setDictionaryType(js.has("dictionaryType") ? js.getString("dictionaryType") : null);

        dictionaryGuid = js.has("dictionaryGuid") ? TextUtils.trimmedStringOrNull(js.getString("dictionaryGuid"), 62) : null;
        title = js.has("title") ? TextUtils.trimmedStringOrNull(js.getString("title"), 30) : null;
        firstname = js.has("firstname") ? TextUtils.trimmedStringOrNull(js.getString("firstname"), 50) : null;
        lastname = js.has("lastname") ? TextUtils.trimmedStringOrNull(js.getString("lastname"), 50) : null;
        organization = js.has("organization") ? TextUtils.trimmedStringOrNull(js.getString("organization"), 249) : null;
        organizationDivision = js.has("organizationDivision") ? TextUtils.trimmedStringOrNull(js.getString("organizationDivision"), 50) : null;
        street = js.has("street") ? TextUtils.trimmedStringOrNull(js.getString("street"), 50) : null;
        zip = js.has("zip") ? TextUtils.trimmedStringOrNull(js.getString("zip"), 14) : null;
        location = js.has("location") ? TextUtils.trimmedStringOrNull(js.getString("location"), 50) : null;
        country = js.has("country") ? TextUtils.trimmedStringOrNull(js.getString("country"), 3) : null;
        pesel = js.has("pesel") ? TextUtils.trimmedStringOrNull(js.getString("pesel"), 11) : null;
        nip = js.has("nip") ? TextUtils.trimmedStringOrNull(js.getString("nip"), 15) : null;
        regon = js.has("regon") ? TextUtils.trimmedStringOrNull(js.getString("regon"), 15) : null;
        email = js.has("email") ? TextUtils.trimmedStringOrNull(js.getString("email"), 50) : null;
        fax = js.has("fax") ? TextUtils.trimmedStringOrNull(js.getString("fax"), 30) : null;
        remarks = js.has("remarks") ? TextUtils.trimmedStringOrNull(js.getString("remarks"), 200) : null;
        personGroup = js.has("group") ? TextUtils.trimmedStringOrNull(js.getString("group"), 50) : null;
        adresSkrytkiEpuap = js.has("adresSkrytkiEpuap") ? TextUtils.trimmedStringOrNull(js.getString("adresSkrytkiEpuap"), 100) : null;
        identifikatorEpuap = js.has("identifikatorEpuap") ? TextUtils.trimmedStringOrNull(js.getString("identifikatorEpuap"), 50) : null;
        id = getIDFromJSON(js);
        phoneNumber = js.has("phoneNumber") ? TextUtils.trimmedStringOrNull(js.getString("phoneNumber"), 15) : null;
        externalID = js.has("externalID") ? TextUtils.trimmedStringOrNull(js.getString("externalID"), 50) : null;

        return this;
    }

    private final static Map<String, String> dictionaryAttributes;

    static {
        Map<String, String> map = Maps.newLinkedHashMap();
        map.put("firstaname", "Imi�");
        map.put("lastname", "Nazwisko");
        map.put("organization", "Nazwa organizacji");
        map.put("location", "Miasto");
        map.put("street", "Ulica");
        map.put("zip", "Kod pocztowy");
        dictionaryAttributes = Collections.unmodifiableMap(map);
    }

    private final static Map<String, String> popUpDictionaryAttributes;

    static {
        Map<String, String> map = Maps.newLinkedHashMap();
        map.put("firstaname", "Imi�");
        map.put("lastname", "Nazwisko");
        map.put("organization", "Nazwa organizacji");
        map.put("street", "Ulica");
        map.put("zip", "Kod pocztowy");
        map.put("country", "Kraj");
        map.put("email", "Adres e-mail");
        map.put("fax", "Fax");
        map.put("nip", "NIP");
        map.put("regon", "Regon");
        map.put("organizationDivision", "Dzia�");
        map.put("pesel", "Pesel");
        map.put("location", "Miasto");
        popUpDictionaryAttributes = Collections.unmodifiableMap(map);
    }

    private final static Map<String, String> promptDictionaryAttributes;

    static {
        Map<String, String> map = Maps.newLinkedHashMap();
        map.put("firstaname", "Imi�");
        map.put("lastname", "Nazwisko");
        map.put("organization", "Nazwa organizacji");
        map.put("street", "Ulica");
        map.put("zip", "Kod pocztowy");
        map.put("country", "Kraj");
        map.put("email", "Adres e-mail");
        map.put("fax", "Fax");
        map.put("nip", "NIP");
        map.put("regon", "Regon");
        map.put("organizationDivision", "Dzia�");
        map.put("pesel", "Pesel");
        map.put("regon", "Regon");
        map.put("phoneNumber", "phoneNumber");
        map.put("externalID", "externalID");
        promptDictionaryAttributes = Collections.unmodifiableMap(map);
    }

    private Long getIDFromJSON(JSONObject js) {
        if (js.has("id")) {
            try {
                return Long.parseLong(js.getString("id"));
            } catch (Exception e) {
            }
        }
        return null;
    }

    /**
     * Wype�nia pola obiektu na podstawie przekazanej tablicy.
     *
     * @param map
     */
    public Person fromMap(Map map) throws EdmException {
        // poprzez metod�, bo wyst�puje tam sprawdzenie poprawno�ci typu
        // s�ownika
        setDictionaryType((String) map.get("dictionaryType"));

        dictionaryGuid = TextUtils.trimmedStringOrNull((String) map.get("dictionaryGuid"), 62);
        if (dictionaryGuid == null)
            dictionaryGuid = DSDivision.ROOT_GUID;
        title = TextUtils.trimmedStringOrNull((String) map.get("title"), 30);
        firstname = TextUtils.trimmedStringOrNull((String) map.get("firstname"), 50);
        lastname = TextUtils.trimmedStringOrNull((String) map.get("lastname"), 50);
        organization = TextUtils.trimmedStringOrNull((String) map.get("organization"), 249);
        organizationDivision = TextUtils.trimmedStringOrNull((String) map.get("organizationDivision"), 50);
        street = TextUtils.trimmedStringOrNull((String) map.get("street"), 50);
        zip = TextUtils.trimmedStringOrNull((String) map.get("zip"), 14);
        location = TextUtils.trimmedStringOrNull((String) map.get("location"), 50);
        country = TextUtils.trimmedStringOrNull((String) map.get("country"), 5);
        pesel = TextUtils.trimmedStringOrNull((String) map.get("pesel"), 11);
        nip = TextUtils.trimmedStringOrNull((String) map.get("nip"), 15);
        nip = nip != null ? nip.replace("-", "").replace(" ", "") : nip;
        regon = TextUtils.trimmedStringOrNull((String) map.get("regon"), 15);
        email = TextUtils.trimmedStringOrNull((String) map.get("email"), 50);
        fax = TextUtils.trimmedStringOrNull((String) map.get("fax"), 30);
        remarks = TextUtils.trimmedStringOrNull((String) map.get("remarks"), 200);
        personGroup = TextUtils.trimmedStringOrNull((String) map.get("group"), 50);
        lparam = TextUtils.trimmedStringOrNull((String) map.get("lparam"), 200);
        identifikatorEpuap = TextUtils.trimmedStringOrNull((String) map.get("identifikatorEpuap"), 200);
        adresSkrytkiEpuap = TextUtils.trimmedStringOrNull((String) map.get("adresSkrytkiEpuap"), 200);
        anonymous = Boolean.parseBoolean((String) map.get("anonymous"));
        phoneNumber = TextUtils.trimmedStringOrNull((String) map.get("phoneNumber"), 15);
        externalID = TextUtils.trimmedStringOrNull((String) map.get("externalID"), 50);
        try {
            if (map.get("wparam") != null) {
                wparam = Long.valueOf(map.get("wparam").toString());
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return this;
    }

    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<String, String>();

        map.put("title", title);
        map.put("firstname", firstname);
        map.put("lastname", lastname);
        map.put("organization", organization);
        map.put("organizationDivision", organizationDivision);
        map.put("street", street);
        map.put("zip", zip);
        map.put("location", location);
        map.put("country", country);
        map.put("pesel", pesel);
        map.put("nip", nip != null ? nip.replace("-", "").replace(" ", "") : nip);
        map.put("email", email);
        map.put("fax", fax);
        map.put("remarks", remarks);
        map.put("dictionaryType", dictionaryType);
        map.put("dictionaryGuid", dictionaryGuid);
        map.put("group", personGroup);
        map.put("id", "" + id);
        map.put("adresSkrytkiEpuap", "" + adresSkrytkiEpuap);
        map.put("identifikatorEpuap", "" + identifikatorEpuap);
        map.put("lparam", lparam != null ? "" + lparam : null);
        map.put("wparam", wparam != null ? "" + wparam : null);
        map.put("anonymous", "" + anonymous);
        map.put("phoneNumber", phoneNumber);
        map.put("externalID", externalID);
        return map;
    }

    public String getSummary() {
        return getSummary(anonymous, title, firstname, lastname, organization, street, location);
    }

    public static String getSummary(boolean anonymous, String title, String firstname, String lastname, String organization, String street, String location) {
        if (anonymous)
            return "Anonim";

        StringBuilder summary = new StringBuilder();

        if (!StringUtils.isEmpty(title))
            summary.append(title);

        if (AvailabilityManager.isAvailable("person.summary.asLastNameFirstname")) {
            if (!StringUtils.isEmpty(lastname)) {
                if (summary.length() > 0)
                    summary.append(' ');
                summary.append(lastname);
            }

            if (!StringUtils.isEmpty(firstname)) {
                if (summary.length() > 0)
                    summary.append(' ');
                summary.append(firstname);
            }
        } else {
            if (!StringUtils.isEmpty(firstname)) {
                if (summary.length() > 0)
                    summary.append(' ');
                summary.append(firstname);
            }

            if (!StringUtils.isEmpty(lastname)) {
                if (summary.length() > 0)
                    summary.append(' ');
                summary.append(lastname);
            }
        }
        if (!StringUtils.isEmpty(organization)) {
            if (summary.length() > 0)
                summary.append(" / ");
            summary.append(organization);
        }

        if (!StringUtils.isEmpty(street)) {
            if (summary.length() > 0)
                summary.append(" / ");
            summary.append(street);
        }

        if (!StringUtils.isEmpty(location)) {
            if (summary.length() > 0)
                summary.append(" / ");
            summary.append(location);
        }
        return summary.toString();
    }

    public static String getSummary(boolean anonymous, String title, String firstname, String lastname, String organization, String street, String location, String organizationDivision) {
        if (anonymous)
            return "Anonim";

        StringBuilder summary = new StringBuilder();

        if (!StringUtils.isEmpty(title))
            summary.append(title);

        if (!StringUtils.isEmpty(firstname)) {
            if (summary.length() > 0)
                summary.append(' ');
            summary.append(firstname);
        }

        if (!StringUtils.isEmpty(lastname)) {
            if (summary.length() > 0)
                summary.append(' ');
            summary.append(lastname);
        }

        if (!StringUtils.isEmpty(organization)) {
            if (summary.length() > 0)
                summary.append(" / ");
            summary.append(organization);
        }

        if (!StringUtils.isEmpty(organizationDivision) && (organization == null || (organization != null && !organization.equals(organizationDivision)))) {
            if (summary.length() > 0)
                summary.append(" / ");
            summary.append(organizationDivision);
        }

        if (!StringUtils.isEmpty(street)) {
            if (summary.length() > 0)
                summary.append(" / ");
            summary.append(street);
        }

        if (!StringUtils.isEmpty(location)) {
            if (summary.length() > 0)
                summary.append(" / ");
            summary.append(location);
        }

        return summary.toString();
    }

    public String getShortSummary() {
        return getShortSummary(firstname, lastname, organization);
    }

    public static String getShortSummary(String firstname, String lastname, String organization) {
        StringBuilder summary = new StringBuilder();

        if (!StringUtils.isEmpty(organization)) {
            summary.append(organization);
        }
        if (!StringUtils.isEmpty(firstname) || !StringUtils.isEmpty(lastname)) {
            if (summary.length() > 0)
                summary.append(", ");
            if (!StringUtils.isEmpty(firstname)) {
                summary.append(firstname).append(' ');
            }
            if (!StringUtils.isEmpty(lastname)) {
                summary.append(lastname);
            }
        }

        return summary.toString().trim();
    }

    public String getAddress() {
        return getAddress(street, zip, location);
    }

    public static String getAddress(String street, String zip, String location) {
        StringBuilder address = new StringBuilder();
        if (!StringUtils.isEmpty(street))
            address.append(street);
        if (!StringUtils.isEmpty(location)) {
            if (address.length() > 0)
                address.append(' ');
            address.append(location);
        }
        return address.toString();
    }

    public String toString() {
        return getClass().getName() + "[title=" + title + " firstname=" + firstname + " lastname=" + lastname + " organization=" + organization + " document=" + document + "]";
    }

    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPersonGroup() {
        return personGroup;
    }

    public void setPersonGroup(String personGroup) {
        this.personGroup = personGroup;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getNip() {
        return nip != null ? nip.replace("-", "").replace(" ", "") : nip;
    }

    public void setNip(String nip) {
        this.nip = nip != null ? nip.replace("-", "").replace(" ", "") : nip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getOrganizationDivision() {
        return organizationDivision;
    }

    public void setOrganizationDivision(String organizationDivision) {
        this.organizationDivision = organizationDivision;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDictionaryType() {
        return dictionaryType;
    }

    public void setDictionaryType(String dictionaryType) throws EdmException {
        if (!StringUtils.isEmpty(dictionaryType) && !DICTIONARY_RECIPIENT.equalsIgnoreCase(dictionaryType) && !DICTIONARY_SENDER.equalsIgnoreCase(dictionaryType))
            throw new EdmException("Niepoprawny rodzaj s�ownika: " + dictionaryType);

        this.dictionaryType = dictionaryType;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        if (document != null)
            this.documentId = document.getId();
        this.document = document;
    }

    public String getRegon() {
        return regon;
    }

    public void setRegon(String regon) {
        this.regon = regon;
    }

    public String getLparam() {
        return lparam;
    }

    public void setLparam(String lparam) {
        this.lparam = lparam;
    }

    public Long getWparam() {
        return wparam;
    }

    public void setWparam(Long wparam) {
        this.wparam = wparam;
    }

    public String getDictionaryGuid() {
        return dictionaryGuid;
    }

    public void setDictionaryGuid(String dictionaryGuid) {
        this.dictionaryGuid = dictionaryGuid;
    }

    public Integer getPosn() {
        return posn;
    }

    public void setPosn(Integer posn) {
        this.posn = posn;
    }

    /**
     * @return the documentId
     */
    public Long getDocumentId() {
        return documentId;
    }

    /**
     * @param documentId the documentId to set
     */
    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getAdresSkrytkiEpuap() {
        return adresSkrytkiEpuap;
    }

    public void setAdresSkrytkiEpuap(String adresSkrytkiEpuap) {
        this.adresSkrytkiEpuap = adresSkrytkiEpuap;
    }

    public String getIdentifikatorEpuap() {
        return identifikatorEpuap;
    }

    public void setIdentifikatorEpuap(String identifikatorEpuap) {
        this.identifikatorEpuap = identifikatorEpuap;
    }

    // ========== AbstractDocumentKindDictionary ==========

    @Override
    public Person find(Long key) throws EdmException {
        return find(key.longValue());
    }

    @Override
    public String dictionaryAccept() {
        return ".";
    }

    @Override
    public String getDictionaryDescription() {
        return "Odbiorca/Nadawca";
    }

    public Map<String, String> promptDictionaryAttributes() {
        return dictionaryAttributes;
    }

    @Override
    public Map<String, String> dictionaryAttributes() {
        return dictionaryAttributes;
    }

    @Override
    public Map<String, String> popUpDictionaryAttributes() {
        return popUpDictionaryAttributes;
    }

    public String dictionaryAction() {
        return ".";
    }

    public Person fromMapUpper(Map map) throws EdmException {
        // poprzez metod�, bo wyst�puje tam sprawdzenie poprawno�ci typu
        // s�ownika
        setDictionaryType((String) map.get("dictionaryType".toUpperCase()));

        dictionaryGuid = TextUtils.trimmedStringOrNull((String) map.get("dictionaryGuid".toUpperCase()), 62);
        if (dictionaryGuid == null)
            dictionaryGuid = DSDivision.ROOT_GUID;
        firstname = (String) map.get("title".toUpperCase());
        title = TextUtils.trimmedStringOrNull((String) map.get("title".toUpperCase()), 30);
        firstname = TextUtils.trimmedStringOrNull((String) map.get("FIRSTNAME"), 50);
        lastname = TextUtils.trimmedStringOrNull((String) map.get("lastname".toUpperCase()), 50);
        organization = TextUtils.trimmedStringOrNull((String) map.get("organization".toUpperCase()), 249);
        organizationDivision = TextUtils.trimmedStringOrNull((String) map.get("organizationDivision".toUpperCase()), 50);
        street = TextUtils.trimmedStringOrNull((String) map.get("street".toUpperCase()), 50);
        zip = TextUtils.trimmedStringOrNull((String) map.get("zip".toUpperCase()), 14);
        location = TextUtils.trimmedStringOrNull((String) map.get("location".toUpperCase()), 50);
        country = TextUtils.trimmedStringOrNull((String) map.get("country".toUpperCase()), 5);
        pesel = TextUtils.trimmedStringOrNull((String) map.get("pesel".toUpperCase()), 11);
        nip = TextUtils.trimmedStringOrNull((String) map.get("nip".toUpperCase()), 15);
        regon = TextUtils.trimmedStringOrNull((String) map.get("regon".toUpperCase()), 15);
        email = TextUtils.trimmedStringOrNull((String) map.get("email".toUpperCase()), 50);
        fax = TextUtils.trimmedStringOrNull((String) map.get("fax".toUpperCase()), 30);
        remarks = TextUtils.trimmedStringOrNull((String) map.get("remarks".toUpperCase()), 200);
        personGroup = TextUtils.trimmedStringOrNull((String) map.get("group".toUpperCase()), 50);
        lparam = TextUtils.trimmedStringOrNull((String) map.get("lparam".toUpperCase()), 50);
        phoneNumber = TextUtils.trimmedStringOrNull((String) map.get("phoneNumber".toUpperCase()), 15);
        externalID = TextUtils.trimmedStringOrNull((String) map.get("externalID".toUpperCase()), 50);
        try {
            if (map.get("wparam") != null) {
                wparam = Long.valueOf(map.get("wparam").toString());
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return this;

    }

    public Long getBasePersonId() {
        return basePersonId;
    }

    public void setBasePersonId(Long basePersonId) {
        this.basePersonId = basePersonId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getExternalID() {
        return externalID;
    }

    public void setExternalID(String externalID) {
        this.externalID = externalID;
    }

    public static Person findByNip(String nip) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(Person.class);
            criteria.add(Restrictions.eq("nip", nip != null ? nip.replace("-", "").replace(" ", "") : nip));
            List<Person> list = criteria.list();

            if (list.size() > 0) {
                return list.get(0);
            }

            return null;
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public static Person findPersonByNip(String nip) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(Person.class);
            criteria.add(Restrictions.eq("nip", nip));
            List<Person> list = criteria.list();

            for (Person p : list) {
                if (!(p instanceof Sender || p instanceof Recipient))
                    return p;
            }

            return null;
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public static List<Person> findAllByNip(String nip) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(Person.class);
            criteria.add(Restrictions.eq("nip", nip != null ? nip.replace("-", "").replace(" ", "") : nip));
            List<Person> list = criteria.list();

            if (list.size() > 0) {
                return list;
            }

            return null;
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }


    public static Person findPerson(Map<String, String> personData) throws EdmException {
        try {
            String firstName = personData.get("FIRSTNAME");
            String lastName = personData.get("LASTNAME");
            String email = personData.get("EMAIL");
            String pesel = personData.get("PESEL");
            Criteria criteria = DSApi.context().session().createCriteria(Person.class);

            if (StringUtils.isNotEmpty(firstName))
                criteria.add(Restrictions.like("firstname", firstName, MatchMode.ANYWHERE));
            if (StringUtils.isNotEmpty(lastName))
                criteria.add(Restrictions.like("lastname", lastName, MatchMode.ANYWHERE));
            if (StringUtils.isNotEmpty(email))
                criteria.add(Restrictions.like("email", email, MatchMode.ANYWHERE));
            if (StringUtils.isNotEmpty(pesel))
                criteria.add(Restrictions.like("pesel", pesel, MatchMode.ANYWHERE));

            List<Person> list = criteria.list();

            for (Person p : list) {
                if (!(p instanceof Sender || p instanceof Recipient))
                    return p;
            }

            return null;
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public static Person findPersonByOrganization(String organization) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(Person.class);
            if (StringUtils.isNotEmpty(organization))
                criteria.add(Restrictions.like("organization", organization, MatchMode.ANYWHERE));

            List<Person> list = criteria.list();

            for (Person p : list) {
                if (!(p instanceof Sender || p instanceof Recipient))
                    return p;
            }

            return null;
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public static Person findPersonByExternalIdAndLparam(String externalID, String lparam) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(Person.class);
            if (StringUtils.isNotEmpty(externalID) && StringUtils.isNotEmpty(lparam)) {
                criteria.add(Restrictions.eq("externalID", externalID));
                criteria.add(Restrictions.eq("lparam", lparam));
            }
            else
                throw new PersonNotFoundException("externalID == " + externalID + "; lparam == " + lparam);
            List<Person> list = criteria.list();
            if (list.size() > 0) {
                for (Person p : list) {
                    if (!(p instanceof Sender || p instanceof Recipient))
                        return p;
                }
            }
            throw new PersonNotFoundException("externalID == " + externalID);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public static List<Person> findByGivenFieldValue(String field, Object value) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(Person.class);
            if (StringUtils.isNotEmpty(field)) {
                criteria.add(Restrictions.eq(field, value));
            } else {
                return Lists.newArrayList();
            }
            return criteria.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public String[] getUserPerson() {
        if (this.getLparam() != null) {
            Pattern pattern = Pattern.compile("(u:)?([^;]*)(;)(d:)(.*)");
            Matcher matcher = pattern.matcher(this.getLparam());
            if (matcher.find()) {
                return new String[]{matcher.group(2), matcher.group(5)};
            }
        }
        return new String[2];
    }

    public String getUserPersonGuid() {
        String username = getUserPerson()[1];
        return Strings.emptyToNull(username);
    }

    public String getUserPersonName() {
        String username = getUserPerson()[0];
        return Strings.emptyToNull(username);
    }
}
