package pl.compan.docusafe.core.office;

import java.lang.Exception;
import java.lang.StringBuffer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Projections;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.EdmException;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.type.Type;
import org.hibernate.criterion.Order;

import pl.compan.docusafe.core.users.DSUser;

/**
 * @author �ukasz Wo�niak <l.g.wozniak@gmail.com>
 */
public class ContractManagment {

    private Long id;
    private Date contractDate;
    private Date returnDate;
    private String note;
    private Long documentId;
    private Long userId;
    private String userName;

    public ContractManagment() {
    }

    public ContractManagment(Long documentId) {
        this.documentId = documentId;
    }

    public static ContractManagment find(Long id) throws EdmException {

        try {
            Criteria criteria = DSApi.context().session().createCriteria(ContractManagment.class);
            criteria.add(org.hibernate.criterion.Expression.eq("id", id));
            List contractsManagments = criteria.list();

            if (contractsManagments.size() > 0) {
                return (ContractManagment) contractsManagments.get(0);
            }

            return null;

        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * 
     * @param documentId
     * @param sortField
     * @param asc
     * @return List<ContractManagment>
     * @throws EdmException 
     */
    public static List<ContractManagment> findByDocumentId(Long documentId, String sortField, boolean asc, Integer offset, int LIMIT, 
                        Date searchContractDate,Date searchReturnDate, String searchNote, Long searchUserId) throws EdmException {

        try {
            Criteria criteria = ContractManagment.getBaseHqlQuery(documentId);
            
            if(searchUserId != null)
                criteria.add(Expression.eq("userId",searchUserId));
            
            if(searchContractDate != null)
                criteria.add(Expression.eq("contractDate", searchContractDate));
            if(searchReturnDate != null)
                criteria.add(Expression.eq("returnDate", searchReturnDate));
            if(searchNote != null)
                criteria.add(Expression.like("note", "%"+searchNote+"%"));
            
            criteria.setFirstResult(offset);
            criteria.setMaxResults(LIMIT);

            if (sortField == null) {
                sortField = "id";
                asc = true;
            }
            if (ContractManagment.isRealColumn(sortField)) {
                criteria.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
            }

            List<ContractManagment> contractsManagments = criteria.list();

            if (contractsManagments.size() > 0) {
                return contractsManagments;
            }

            return null;

        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }

    }
    
    public static Long countContractsManagments(Long documentId,Date searchContractDate,Date searchReturnDate, String searchNote, Long searchUserId) throws EdmException {

        try {
            Criteria criteria = ContractManagment.getBaseHqlQuery(documentId);
            criteria.setProjection(Projections.rowCount());
            
            if(searchUserId != null)
                criteria.add(Expression.eq("userId",searchUserId));
            
            if(searchContractDate != null)
                criteria.add(Expression.eq("contractDate", searchContractDate));
            if(searchReturnDate != null)
                criteria.add(Expression.eq("returnDate", searchReturnDate));
            if(searchNote != null)
                criteria.add(Expression.like("note", "%"+searchNote+"%"));
            
            return ((Long) criteria.list().get(0));

        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca Criteria dla obecnego obiektu;
     * @param documentId
     * @return 
     */
    public static Criteria getBaseHqlQuery(Long documentId) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(ContractManagment.class);
            criteria.add(Expression.eq("documentId", documentId));

            return criteria;
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Sprawdza czy kolumna istnieje
     * @param sortField
     * @return boolean
     */
    public static boolean isRealColumn(String sortField) {
        if ("id".equals(sortField) || "userId".equals(sortField)
                || "contractDate".equals(sortField) || "returnDate".equals(sortField)
                || "note".equals(sortField)) {
            return true;
        } else {
            return false;
        }
    }

    public static List<ContractManagment> setContractsUsersName(List<ContractManagment> contracts, List<DSUser> users) {

        try {
            int i = 0;
            while (i < contracts.size()) {
                contracts.get(i).setUserName(findUserName(users, contracts.get(i).getUserId()));
                i++;
            }
        } catch (NullPointerException e) {
        } finally {
            return contracts;
        }

    }

    /**
     * Zwraca ContractManagment wybrany przez u�ytkownika
     * @param contracts List<ContractManagment>
     * @param id
     * @return ContractManagment
     */
    public static ContractManagment getContractManagmentFromList(List<ContractManagment> contracts, Long id) {

        Iterator contractIterator = contracts.iterator();

        while (contractIterator.hasNext()) {
            ContractManagment cm = (ContractManagment) contractIterator.next();

            if (cm.getId().equals(id)) {
                return cm;
            }
        }

        return null;
    }

    /**
     * @param users List<DSUser>
     * @param userId Long
     * @return String
     */
    public static String findUserName(List<DSUser> users, Long userId) {
        Iterator userIterator = users.iterator();

        while (userIterator.hasNext()) {
            DSUser user = (DSUser) userIterator.next();
            if ((user.getId()).equals(userId)) {
                return user.asFirstnameLastnameName();
            }
        }

        return userId.toString();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public static boolean isDocumentInDP(Long documentId) {
        ResultSet rs = null;
        PreparedStatement ps = null;
        boolean isDP = false;

        try {
            ps = DSApi.context().prepareStatement("SELECT count(*) as document_exist FROM DSG_PRAWNY as dp WHERE dp.DOCUMENT_ID = ? GROUP BY dp.DOCUMENT_ID;");

            ps.setLong(1, documentId);
            rs = ps.executeQuery();
            int count = 0;

            while (rs.next()) {
                count++;
            }

            if (count > 0) {
                isDP = true;
            }

        } catch (SQLException e) {
        } catch (EdmException e) {
        } finally {
            DSApi.context().closeStatement(ps);
        }

        return isDP;
    }

    /**
     * @param ids String[]
     */
    public static void deleteAll(String[] ids) throws EdmException {
//        final String separator = ",";
//
//        StringBuilder sb = new StringBuilder();
//        for (int i = 0; i < ids.length; i++) {
//            if (i != 0) {
//                sb.append(separator);
//            }
//            sb.append(ids[i]);
//        }
        try {
//            Query q = DSApi.context().session().createQuery("DELETE dso_contract_managment as ContrDactManagment WHERE id IN (?)");
//            q.setString(1, sb.toString());
//            q.executeUpdate();
            ContractManagment contract;
            for (int i = 0; i < ids.length; i++) {
                contract = (ContractManagment) DSApi.context().session().load(ContractManagment.class, new Long(ids[i]));
                DSApi.context().session().delete(contract);
            }

        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public boolean save() throws EdmException {

        try {
            DSApi.context().session().saveOrUpdate(this);
            return true;
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException {
        try {
            DSApi.context().session().delete(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
    //----GETTER --- SETTER

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getContractDate() {
        return contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
