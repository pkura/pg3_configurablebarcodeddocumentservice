package pl.compan.docusafe.core.office;

import com.google.common.collect.Lists;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.dialect.FirebirdDialect;

import static pl.compan.docusafe.core.db.criteria.NativeExps.*;

/**
 * Klasa s�u��ca do wyci�gania powi�zanych dokument�w
 *
 * Powsta�a ze wzgl�du na problemy z wydajno�ci�
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class AssociativeDocumentStore {
    private final static Logger LOG = LoggerFactory.getLogger(AssociativeDocumentStore.class);

    /**
     * Klasa zawieraj�ca cz�c informacji o powi�zanych dokumentach
     * - pe�ne informacje zbyt d�ugo si� wczytywa�y
     */
    @JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlRootElement(name = "associative-doc-bean")
    public static class AssociativeDocumentBean {
        long documentId;
        Integer officeNumber;
        String caseDocumentId;
        Long caseId;//identyfikator sprawy // doda� mkulbaka 28.02.2012

        public String getCaseDocumentId() {
            return caseDocumentId;
        }

        public long getDocumentId() {
            return documentId;
        }

        public Integer getOfficeNumber() {
            return officeNumber;
        }
        
        //zwraca identyfikator sprawy
        //doda� mkulbaka 28.02.2012
        public Long getCaseId(){
        	return caseId;
        }
        
        @Override
        public String toString() {
            return "AssociativeDocumentBean{" +
                    "caseDocumentId='" + caseDocumentId + '\'' +
                    ", documentId=" + documentId +
                    ", officeNumber=" + officeNumber +
                    '}';
        }
    }

    /**
     * Zwraca obiekty wszystkich spraw dokumentu, wszystko jest w jednej li�cie nie ma rozr�nienia
     * na dokumenty przychodz�ce i wychodz�ce
     * @param documentId
     * @return
     * @throws EdmException
     */
    public static List<AssociativeDocumentBean> getAllAssociatedDocuments(Long documentId) throws EdmException {
        OfficeDocument document = OfficeDocument.findOfficeDocument(Long.valueOf(documentId));
        List<AssociativeDocumentStore.AssociativeDocumentBean> associatedDocuments = new ArrayList<AssociativeDocumentStore.AssociativeDocumentBean>();
        associatedDocuments.add(AssociativeDocumentStore.fromOfficeDocument(document));

        if(document instanceof InOfficeDocument) {
            OfficeDocument master = ((InOfficeDocument)document).getMasterDocument();
            if(master != null) {
                associatedDocuments.addAll(AssociativeDocumentStore.getAssociatedDocumentsIn(master.getId()));
                associatedDocuments.add(AssociativeDocumentStore.fromOfficeDocument(master));
            } else {
                associatedDocuments.addAll(AssociativeDocumentStore.getAssociatedDocumentsIn(documentId));
            }
        } else {
            OfficeDocument master = ((OutOfficeDocument)document).getMasterDocument();
            if(master != null) {
                associatedDocuments.addAll(AssociativeDocumentStore.getAssociatedDocumentsOut(master.getId()));
                associatedDocuments.add(AssociativeDocumentStore.fromOfficeDocument(master));
            } else {
                associatedDocuments.addAll(AssociativeDocumentStore.getAssociatedDocumentsOut(documentId));
            }
        }

        return associatedDocuments;
    }
    /**
     * Zwraca list� dokument�w przychodz�cych powi�zanych z danym dokumentem
     * @param masterDocumentId
     * @return
     * @throws EdmException
     */
    public static List<AssociativeDocumentBean> getAssociatedDocumentsIn(long masterDocumentId) throws EdmException {
        List<AssociativeDocumentBean> ret = queryOfficeTable("DSO_IN_DOCUMENT", masterDocumentId);
        LOG.info("getAssociatedDocument = {}", ret);
        return ret;
    }

    public static List<AssociativeDocumentBean> getAssociatedDocumentsOut(long masterDocumentId) throws EdmException {
        List<AssociativeDocumentBean> ret = queryOfficeTable("DSO_OUT_DOCUMENT", masterDocumentId);
        LOG.info("getAssociatedDocument = {}", ret);
        return ret;
    }

    private static List<AssociativeDocumentBean> queryOfficeTable(String officeDocTable, long masterDocumentId) throws EdmException {
        List<AssociativeDocumentBean> ret = Lists.newArrayList();

        if(!DSApi.isFirebirdServer())
        {
	        CriteriaResult cr = DSApi.context()
	            .createNativeCriteria(officeDocTable, "ODOC")
	            .setProjection(projection()
	                    .addProjection("ODOC.ID")
	                    .addProjection("ODOC.OFFICENUMBER")
	                    .addProjection("ODOC.CASEDOCUMENTID")
	                    .addProjection("ODOC.CASE_ID"))
	            .add(eq("ODOC.MASTER_DOCUMENT_ID", masterDocumentId))
	            .criteriaResult();
	
	        while(cr.next()){
	            AssociativeDocumentBean adb = new AssociativeDocumentBean();
	            adb.documentId = cr.getLong(0, null);
	            adb.officeNumber = cr.getInteger(1, null);
	            adb.caseDocumentId = cr.getString(2, null);
	            adb.caseId = cr.getLong(3, null);
	            ret.add(adb);
	        }
	    }
        else
        {
        	PreparedStatement ps = null;
        	ResultSet rs = null;
        	try
        	{
        		//dodano pobranie kolumny CASE_ID - mkulbaka 28.02.2012
        		ps =DSApi.context().prepareStatement("select ID,OFFICENUMBER,CASEDOCUMENTID, CASE_ID from " + officeDocTable + " where MASTER_DOCUMENT_ID = ?" );
        		ps.setLong(1, masterDocumentId);
        		
        		rs = ps.executeQuery();
        		
        		while(rs.next())
        		{
    	            AssociativeDocumentBean adb = new AssociativeDocumentBean();
    	            adb.documentId = rs.getLong(1);
    	            adb.officeNumber = rs.getInt(2);
    	            adb.caseDocumentId = rs.getString(3);
    	            adb.caseId = rs.getLong(4);
    	            
    	            ret.add(adb);
        		}
        		rs.close();
        		ps.close();
        	}
    		catch (Exception e)
    		{
    			throw new EdmException(e);
    		}
    		finally
    		{
    			DbUtils.closeQuietly(rs);
    			DbUtils.closeQuietly(ps);
    		}
        }
        return ret;
    }//queryOfficeTable method

    public static AssociativeDocumentBean fromOfficeDocument(OfficeDocument od){
        AssociativeDocumentBean adb = new AssociativeDocumentBean();
        adb.documentId = od.getId();
        adb.caseDocumentId = od.getCaseDocumentId();
        adb.officeNumber = od.getOfficeNumber();
        try{
            adb.caseId =od.getContainingCase().getId();
        }catch(Exception e){
            LOG.error("Nie uda�o si� ustawi� id sprawy do obiektu AssociativeDocumentBean dla dokumentu {}", od.getId());
        }
        return  adb;
    }


    private AssociativeDocumentStore(){}
}
