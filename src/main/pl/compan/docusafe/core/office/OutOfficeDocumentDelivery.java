package pl.compan.docusafe.core.office;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;

/**
 * Spos�b wys�ania pisma.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OutOfficeDocumentDelivery.java,v 1.13 2010/07/29 11:00:16 mariuszk Exp $
 */
public class OutOfficeDocumentDelivery extends OfficeDocumentDelivery
{
	public static final String DELIVERY_EPUAP = "ePUAP";
    private Boolean foreignDelivery;
    private Integer priorityKind;

    public enum PriorityKind{
        ECONOMIC(1,"ECONIMIC"),
        REGISTERED_ECONOMIC(2, "REGISTERED_ECONOMIC"),
        PRIORITY(3, "PRIORITY"),
        REGISTERED_PRIORITY(4, "REGISTERED_PRIORITY");

        private final Integer id;
        private final String cn;

        PriorityKind(Integer id, String cn){
            this.id=id;
            this.cn=cn;
        }
        public Integer getId(){
            return id;
        }

        public String getCn(){
            return cn;
        }
    }

    /**
     * Konstruktor u�ywany przez Hibernate.
     */
    OutOfficeDocumentDelivery()
    {
    }

    public OutOfficeDocumentDelivery(String name)
    {
        super(name);
    }

    protected Class myDocumentClass()
    {
        return OutOfficeDocument.class;
    }

    /**
     * Zwraca list� wszystkich obiekt�w OutOfficeDocumentDelivery
     * znajduj�cych si� w bazie posortowanych po polu posn.
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static List<OutOfficeDocumentDelivery> list() throws EdmException
    {
        List<OutOfficeDocumentDelivery> list = Finder.list(OutOfficeDocumentDelivery.class, "posn");
        for (OutOfficeDocumentDelivery delivery : list)
        {
            DSApi.initializeProxy(delivery);
        }
        return list;
    }

    public static OutOfficeDocumentDelivery find(Integer id)
        throws EdmException
    {
        return (OutOfficeDocumentDelivery) Finder.find(OutOfficeDocumentDelivery.class, id);
    }

	public static OutOfficeDocumentDelivery findByName(String name) throws EdmException 
	{
		Criteria c = DSApi.context().session().createCriteria(OutOfficeDocumentDelivery.class);
		c.add(Restrictions.eq("name", name).ignoreCase());
		List<OutOfficeDocumentDelivery> list =  (List<OutOfficeDocumentDelivery>) c.list();
		if(list != null && list.size() > 0)
			return list.get(0);
		else
			return null;
	}

    public static List<OutOfficeDocumentDelivery> findByForeignDelivery(Boolean foreignDelivery) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(OutOfficeDocumentDelivery.class);
        c.add(Restrictions.eq("foreignDelivery", foreignDelivery));
        List<OutOfficeDocumentDelivery> list =  (List<OutOfficeDocumentDelivery>) c.list();
        if(list != null && list.size() > 0)
            return list;
        else
            return null;
    }

    public static OutOfficeDocumentDelivery findByPriorityKindAndForeignDelivery(Integer priorityKindId,
                                                                                       Boolean foreignDelivery) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(OutOfficeDocumentDelivery.class);
        c.add(Restrictions.eq("priorityKind", priorityKindId));
        c.add(Restrictions.eq("foreignDelivery", foreignDelivery));
        List<OutOfficeDocumentDelivery> list =  (List<OutOfficeDocumentDelivery>) c.list();
        if(list != null && list.size() > 0)
            return list.get(0);
        else
            return null;
    }

    public Boolean getForeignDelivery() {
        return foreignDelivery;
    }

    public void setForeignDelivery(Boolean foreignDelivery) {
        this.foreignDelivery = foreignDelivery;
    }

    public Integer getPriorityKind() {
        return priorityKind;
    }

    public void setPriorityKind(Integer priorityKind) {
        this.priorityKind = priorityKind;
    }
}
