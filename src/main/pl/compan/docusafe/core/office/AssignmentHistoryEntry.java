package pl.compan.docusafe.core.office;

import com.google.common.base.Optional;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import com.google.common.collect.Maps;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Klasa przechowuj�ce informacje o historii dekretacji i historii procesu
 * nazwa tabeli : dso_document_asgn_history
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AssignmentHistoryEntry.java,v 1.22 2009/12/10 15:30:40 tomekl Exp $
 */
public final class AssignmentHistoryEntry implements Cloneable {

    private static final Logger log = LoggerFactory.getLogger(AssignmentHistoryEntry.class);

	private Long id;
	private Long documentId;
	private List<AssignmentHistoryEntryTargets> assignmentHistoryTargets;

	public List<AssignmentHistoryEntryTargets> getAssignmentHistoryTargets() 
	{
		if(assignmentHistoryTargets == null)
		{
			assignmentHistoryTargets = new ArrayList<AssignmentHistoryEntryTargets>();
		}
		return assignmentHistoryTargets;
	}

	public void setAssignmentHistoryTargets(List<AssignmentHistoryEntryTargets> assignmentHistoryTargets) {
		this.assignmentHistoryTargets = assignmentHistoryTargets;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public Long getId() {
		return id;
	}

	/** data powstania wpisu razem z czasem */
    private Date ctime;
    /** data powstanie wpisu bez godziny */
    private Date cdate;
    private String sourceUser;
    private String sourceGuid;
    private String targetUser;
    private String targetGuid;
    
    //Zast�pstwo
    private String substituteUser;
    private String substituteGuid;
    
    private String objective;
    private boolean accepted;
    /**
     * Oznacza wpis "u�ytkownik sourceUser zako�czy� prac� z pismem".
     */
    private boolean finished;
    private String processName;
    private Integer type;         // gdy type == null to wpis dotyczacy obiegu recznego; wpp typ wpisu JBPM
    /** typ procesu: do realizacji albo do wiadomo�ci */
    private Integer processType;
    private String status;

    private String deadline = "BRAK";
    private String clerk;


    /** Inicjalizacja/utworzenie procesu */
    public static final EntryType JBPM_INIT = EntryType.JBPM_INIT;
    /** Otrzymanie zadania przez u�ytkownika (tylko jbpm3)*/
    public static final EntryType JBPM_NORMAL = EntryType.JBPM_NORMAL;
    /** Zako�czenie zadania */
    public static final EntryType JBPM_END_TASK = EntryType.JBPM_END_TASK;
    /** Przerwanie zadania (?) */
    public static final EntryType JBPM_CANCELLED = EntryType.JBPM_CANCELLED;
    /** Zako�czenie pracy z zadaniem */
    public static final EntryType JBPM_FINISH = EntryType.JBPM_FINISH;
    /** Otrzymanie zadania przez grup� (tylko jbpm3) */
    public static final EntryType JBPM_NORMAL_DIVISION = EntryType.JBPM_NORMAL_DIVISION;
    /** Przekzanie zadania */
    public static final EntryType JBPM_FORWARD = EntryType.JBPM_FORWARD;
    public static final EntryType REOPEN_WF = EntryType.REOPEN_WF; //ponowne otwarcie zadania (obieg reczny, doda�em to z powodu braku mo�liwo�ci zrobienia tego inaczej przez konstrukcje histori)
    public static final EntryType IMPORT = EntryType.IMPORT;

    public static final EntryType JBPM4_ACCEPT_TASK = EntryType.JBPM4_ACCEPT_TASK;
    public static final EntryType JBPM4_REASSIGN = EntryType.JBPM4_REASSIGN;
    public static final EntryType JBPM4_REASSIGN_SINGLE_USER = EntryType.JBPM4_REASSIGN_SINGLE_USER;
    public static final EntryType JBPM4_REASSIGN_DIVISION = EntryType.JBPM4_REASSIGN_DIVISION; 
    public static final EntryType JBPM4_REASSIGN_ME = EntryType.JBPM4_REASSIGN_ME;
    public static final EntryType JBPM4_CONFIRMED=EntryType.JBPM4_CONFIRMED;
    /** Ponowne utworzenie procesu (zwykle manual - proces r�czny) */
    public static final EntryType JBPM4_RESTART = EntryType.JBPM4_RESTART ;
    /**Akceptacja zadania**/
    public static final EntryType JBPM4_ACCEPTED = EntryType.JBPM4_ACCEPTED;
    /** Przekazanie zadania do poprawy***/
    public static final EntryType JBPM4_CORRECTION = EntryType.JBPM4_CORRECTION;
    /** Odrzucenie zadania***/
    public static final EntryType JBPM4_REJECTED = EntryType.JBPM4_REJECTED;

    /** Poprawne zako�czenie eksportu dokumentu*/
    public static final EntryType JBPM_EXPORT_FINISH = EntryType.JBPM_EXPORT_FINISH;

    public static final Integer PROCESS_TYPE_REALIZATION = 1;  // proces do realizacji
    public static final Integer PROCESS_TYPE_CC = 2;           // proces do wiadomo�ci

    public static final EntryType JBPM4_INTERNALSIGN = EntryType.JBPM4_INTERNALSIGN;
    
    private final static Map<Integer, EntryType> valueToEntryType;


    public static enum EntryType {
        //nie zmienia� numerk�w !!!
        JBPM_INIT(1),
        JBPM_NORMAL(2),
        JBPM_END_TASK(3),
        JBPM_CANCELLED(4),
        JBPM_FINISH(5),
        JBPM_NORMAL_DIVISION(6),
        JBPM_FORWARD(7),
        REOPEN_WF(8),
        IMPORT(9),
        
        
        JBPM4_REASSIGN_SINGLE_USER(100),
        JBPM4_REASSIGN_DIVISION(115),
        JBPM4_REASSIGN(110),
        JBPM4_ACCEPT_TASK(120),
        JBPM4_ASSIGN_COORDINATOR(130),
        JBPM4_REASSIGN_ME(140),
        JBPM4_RESTART(150),
        JBPM4_ACCEPTED(160),
        JBPM4_CORRECTION(170),
        JBPM4_REJECTED(180),
        JBPM4_PROCESS_ACTION(190),
        JBPM4_CONFIRMED(200),
        JBPM4_INTERNALSIGN(210),
        JBPM_EXPORT_FINISH(220);

        public final int value;
        EntryType(int value){
            this.value = value;
        }
    }

    static {
        Map<Integer, EntryType> map = Maps.newHashMap();
        for(EntryType type: EntryType.values()){
            map.put(type.value, type);
        }
        valueToEntryType = Collections.unmodifiableMap(map);
    }

    public static EntryType typeFromKey(int key){
        return valueToEntryType.get(key);
    }

    /**
     * Hibernatowi mowimy dziekuje
     */
    public AssignmentHistoryEntry(String targetUser)
    {
    	setCtimeAndCdate(new Date());
        this.sourceUser = targetUser;
        this.sourceGuid = "x";
        this.targetUser = targetUser;
        this.targetGuid = "x";
        this.objective = "x";
        this.accepted = false;
        this.finished = false;
        this.processName = "x";
        this.processType = REOPEN_WF.value;
        this.type = REOPEN_WF.value;
    }

    /**
     * Tworzy wpis oznaczaj�cy zako�czenie pracy z dokumentem przez u�ytkownika.
     * @param sourceUser Nazwa u�ytkownika.
     * @param processName Nazwa procesu, kt�ry u�ytkownik zako�czy�.
     */
    public AssignmentHistoryEntry(String sourceUser, String processName, Integer processType)
    {
        setCtimeAndCdate(new Date());
        this.sourceUser = sourceUser;
        this.sourceGuid = "x";
        this.targetUser = "x";
        this.targetGuid = "x";
        this.objective = "x";
        this.accepted = false;
        this.finished = true;
        this.processName = processName;
        this.processType = processType;
    }

    public AssignmentHistoryEntry(String sourceUser, String processName, Integer processType, EntryType type)
    {
        this(sourceUser, processName, processType);
        this.type = type.value;
    }
    
    /**
     * Tworzy wpis oznaczaj�cy przyj�cie pisma do systemu.
     * @param sourceUser Nazwa u�ytkownika.
     */
    public AssignmentHistoryEntry(String sourceUser, Integer processType)
    {
        setCtimeAndCdate(new Date());
        this.sourceUser = sourceUser;
        this.sourceGuid = "x";
        this.targetUser = null;
        this.targetGuid = "x";
        this.objective = "x";
        this.accepted = false;
        this.finished = false;
        this.processName = "rejestracja pisma";
        this.processType = processType;
    }

    AssignmentHistoryEntry(Date ctime, String sourceUser, String sourceGuid, String targetUser,
                           String targetGuid, String objective, String processName, boolean accepted, Integer processType)
    {
    	
        setCtimeAndCdate(ctime);
        this.sourceUser = sourceUser;
        setSourceGuid(sourceGuid);
        this.targetUser = targetUser;
        this.targetGuid = targetGuid;
        this.objective = objective;
        this.accepted = accepted;
        this.processName = processName;
        this.processType = processType;
    }

    public AssignmentHistoryEntry(String sourceUser, String sourceGuid, String targetUser,
                                  String targetGuid, String objective, String processName, boolean accepted, Integer processType)
    {
        this(new Date(), sourceUser, sourceGuid, targetUser, targetGuid, objective, processName, accepted, processType);
        
    }
    
    public AssignmentHistoryEntry(String sourceUser, String sourceGuid,
    							  String targetUser, String targetGuid, 
    							  String substituteUser, String substituteGuid,
    							  String objective, String processName, boolean accepted, Integer processType)
    {
    	
        this(new Date(), sourceUser, sourceGuid, targetUser, targetGuid, objective, processName, accepted, processType);        
        this.substituteGuid = substituteGuid;
        this.substituteUser = substituteUser;
    }

    /**
     * Tworzy wpis z Workflow JBPM dla typu operacji 'type'.
     * @param type
     * @param sourceUser
     * @param targetUser
     * @param objective
     * @param processName
     */
    public AssignmentHistoryEntry(EntryType type, String sourceUser, String targetUser, String targetGuid, String objective, String processName,
            boolean accepted, Integer processType)
    {
        if (sourceUser == null)
            sourceUser = "x";
        if (targetUser == null)
            targetUser = "x";
        if (targetGuid == null)
            targetGuid = "x";
        if (objective == null)
            objective = "x";
        setCtimeAndCdate(new Date());
        this.sourceUser = sourceUser;
        this.sourceGuid = "x";
        this.targetUser = targetUser;
        this.targetGuid = targetGuid;
        this.objective = objective;
        this.accepted = accepted;
        this.processName = processName;
        this.type = type.value;
        this.processType = processType;
    }
    
    
    /**Przywr�cenie zadania na liste*/
    public AssignmentHistoryEntry()
    {
        if (sourceUser == null)
            sourceUser = "x";
        if (targetUser == null)
            targetUser = "x";
        if (targetGuid == null)
            targetGuid = "x";
        if (objective == null)
            objective = "x";
        setCtimeAndCdate(new Date());
        this.sourceGuid = "x";
        this.processType = PROCESS_TYPE_REALIZATION;
        
        //this.sourceUser = sourceUser;
        //this.targetUser = targetUser;
        //this.targetGuid = targetGuid;
        //this.objective = objective;
        //this.accepted = accepted;
        //this.processName = processName;
        //this.type = type;
        //this.processType = processType;
    }

    public void setSubstituteUserByParentUser(DSUser user, OfficeDocument document, EventListenerExecution execution) {
        if(! AvailabilityManager.isAvailable("assignment.history.acceptanceAddSubstitute"))
        {
            return;
        }

        try
        {
            Optional<DSUser> subUser = getSubstituteUserByParentUser(user, document, execution);

            if(subUser.isPresent()) {
                this.setSubstituteUser(subUser.get().getName());
            } else {
                log.error("[setSubstituteUserByParentUser] no substitute found");
            }
        }
        catch (Exception e)
        {
            log.error("[setSubstituteUserByParentUser] error, cannot get substitute user", e);
        }
    }

    protected Optional<DSUser> getSubstituteUserByParentUser(DSUser user, OfficeDocument document, EventListenerExecution execution) throws SQLException, EdmException {

        String key = execution.getKey();
        String id = execution.getId();
        String name = execution.getName();

        log.debug("[getSubstituteUserByParentUser] user.name = {}, document.id = {}, key = {}, id = {}, name = {}", user.getName(), document.getId(), key, id, name);

        DSUser[] substitutedUsers = user.getSubstitutedUsers();
        if (substitutedUsers.length > 0)
        {
            for (DSUser subUser: substitutedUsers)
            {
                PreparedStatement ps = null;
                ResultSet rs = null;
                try {
                    ps = DSApi.context().prepareStatement("select * from dsw_jbpm_tasklist where assigned_resource = ? and document_id = ? and activity_key = ?");
                    ps.setString(1, subUser.getName());
                    ps.setLong(2, document.getId());
                    ps.setString(3, id);
                    rs = ps.executeQuery();
                    if(rs.next())
                    {
                        log.debug("[getSubstituteUserByParentUser] returning matching substitute = {}", subUser.getName());
                        return Optional.of(subUser);
                    }
                } finally {
                    DbUtils.closeQuietly(rs);
                    DbUtils.closeQuietly(ps);
                }
            }
        }

        log.debug("[getSubstituteUserByParentUser] nothing found, returning nothing");
        return Optional.absent();
    }

    public Object clone()
    {
        try
        {
            return super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public void setCtimeAndCdate(Date date)
    {
        this.ctime = date;
        Calendar calTime = Calendar.getInstance();
        calTime.setTime(date);
        
        Calendar calDate = Calendar.getInstance();
        calDate.clear();
        calDate.set(calTime.get(Calendar.YEAR), calTime.get(Calendar.MONTH), calTime.get(Calendar.DAY_OF_MONTH));
        this.cdate = calDate.getTime();
    }
    
    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public Date getCdate()
    {
        return cdate;
    }

    public void setCdate(Date cdate)
    {
        this.cdate = cdate;
    }

    public String getSourceUser()
    {
        return sourceUser;
    }

    public void setSourceUser(String sourceUser)
    {
        this.sourceUser = sourceUser;
    }

    public String getSourceGuid()
    {
        return sourceGuid;
    }

    /**
     * �le wykorzystane sourceGuid, wi�c obcinamy
     * @param sourceGuid
     */
    public void setSourceGuid(String sourceGuid)
    {
        this.sourceGuid = StringUtils.left(sourceGuid, 2048);
    }

    public String getTargetUser()
    {
        return targetUser;
    }

    public void setTargetUser(String targetUser)
    {
        this.targetUser = targetUser;
    }

    public String getTargetGuid()
    {
        return targetGuid;
    }

    public void setTargetGuid(String targetGuid){
        if(!StringUtils.isBlank(targetGuid)){ //w bazie targetGuid nie mo�e by� null, domy�lnie jest "x" :|
            this.targetGuid = targetGuid;
        } else {
            this.targetGuid = "x";
        }
    }

    public String getObjective()
    {
        return objective;
    }

    public void setObjective(String objective)
    {
        this.objective = objective;
    }

    public boolean isAccepted()
    {
        return accepted;
    }

    public void setAccepted(boolean accepted)
    {
        this.accepted = accepted;
    }

    public boolean isFinished()
    {
        return finished;
    }

    public void setFinished(boolean finished)
    {
        this.finished = finished;
    }

    public String getProcessName()
    {
        return processName;
    }

    public void setProcessName(String processName)
    {
        this.processName = processName;
    }

    public EntryType getType(){
        return valueToEntryType.get(type);
    }

    public void setType(EntryType type){
        if(type == null){
            this.type = null;
        } else {
            this.type = type.value;
        }
    }


//    public EntryType getEntryType(){
//        return valueToEntryType.get(type);
//    }
//
//    public void setEntryType(EntryType type){
//        this.type = type.value;
//    }
    
    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Integer getProcessType()
    {
        return processType;
    }

    public void setProcessType(Integer processType)
    {
        this.processType = processType;
    }

	public void setSubstituteUser(String substituteUser) {
		this.substituteUser = substituteUser;
	}

	public String getSubstituteUser() {
		return substituteUser;
	}

	public void setSubstituteGuid(String substituteGuid) {
		this.substituteGuid = substituteGuid;
	}

	public String getSubstituteGuid() {
		return substituteGuid;
	}

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getClerk() {
        return clerk;
    }

    public void setClerk(String clerk) {
        this.clerk = clerk;
    }
}