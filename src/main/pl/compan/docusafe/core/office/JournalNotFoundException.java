package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: JournalNotFoundException.java,v 1.5 2006/02/06 16:05:52 lk Exp $
 */
public class JournalNotFoundException extends EntityNotFoundException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public JournalNotFoundException(Long journalId)
    {
        super(sm.getString("journalNotFoundException", journalId));
    }

    protected JournalNotFoundException(String message)
    {
        super(message);
    }

    public static JournalNotFoundException mainNotFound(String type)
    {
        StringBuilder message = new StringBuilder("Nie znaleziono głównego dziennika pism ");
        if (Journal.INCOMING.equals(type))
            message.append("przychodzących");
        else if (Journal.OUTGOING.equals(type))
            message.append("wychodzących");
        else if (Journal.INTERNAL.equals(type))
            message.append("wewnętrznych");
        else
            message.append("typu '"+type+"'");

        return new JournalNotFoundException(message.toString());
    }

    public static JournalNotFoundException mainMultiple(String type)
    {
        StringBuilder message = new StringBuilder("Znaleziono więcej niż jeden główny dziennik pism ");
        if (Journal.INCOMING.equals(type))
            message.append("przychodzących");
        else if (Journal.OUTGOING.equals(type))
            message.append("wychodzących");
        else if (Journal.INTERNAL.equals(type))
            message.append("wewnętrznych");
        else
            message.append("typu '"+type+"'");

        return new JournalNotFoundException(message.toString());
    }
}
