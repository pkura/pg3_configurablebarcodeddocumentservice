package pl.compan.docusafe.core.office;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.classic.Lifecycle;
import org.hibernate.Session;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CasePriority.java,v 1.8 2006/12/07 13:51:26 mmanski Exp $
 */
@XmlRootElement(name = "case-priority")
public class CasePriority implements Lifecycle
{
    private Integer id;
    private Integer posn;
    private String name;

    CasePriority()
    {
    }

    public CasePriority(String name)
    {
        this.name = name;
    }

    public static CasePriority find(Integer id) throws EdmException
    {
        return (CasePriority) Finder.find(CasePriority.class, id);
    }

    public static List<CasePriority> list() throws EdmException
    {
        return Finder.list(CasePriority.class, "posn");
    }

    public void delete() throws EdmException
    {
        Persister.delete(this);
    }

    public void create() throws EdmException
    {
        Persister.create(this);
    }

    public boolean canDelete() throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias caseTable = from.createTable(DSApi.getTableName(OfficeCase.class));

        WhereClause where = new WhereClause();
        where.add(Expression.eq(caseTable.attribute("priority_id"), getId()));

        SelectClause selectCount = new SelectClause();
        /*SelectColumn countCol = */selectCount.addSql("count(*)");

        SelectQuery selectQuery;

        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);

            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());

            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby spraw");

            int count = rs.getInt(1);

            rs.close();

            return count == 0;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getPosn()
    {
        return posn;
    }

    public void setPosn(Integer posn)
    {
        this.posn = posn;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof CasePriority)) return false;

        final CasePriority priority = (CasePriority) o;

        if (id != null ? !id.equals(priority.id) : priority.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    /* Lifecycle */

    public boolean onSave(Session s) throws CallbackException
    {
        if (id != null)
        {
            posn = id;
        }
        else
        {
            // w bazach, gdzie identyfikatory pochodz� z pola autonumber
            // id b�dzie w tym momencie r�wne null
            posn = Dictionaries.updatePosnOnSave(getClass());
        }
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }
}
