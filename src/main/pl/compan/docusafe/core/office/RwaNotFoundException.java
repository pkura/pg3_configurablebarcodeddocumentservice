package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.base.EntityNotFoundException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RwaNotFoundException.java,v 1.1 2004/11/18 16:10:04 lk Exp $
 */
public class RwaNotFoundException extends EntityNotFoundException
{
    public RwaNotFoundException(String code)
    {
        super("Nie znaleziono RWA o symbolu "+code);
    }

    public RwaNotFoundException(Integer id)
    {
        super("Nie znaleziono RWA ("+id+")");
    }
}
