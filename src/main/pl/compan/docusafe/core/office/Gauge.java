/**
 * 
 */
package pl.compan.docusafe.core.office;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class Gauge {
	private Integer id;
	private String cn;
	private String title;
	
	public Gauge() {
		
	}
	
	public static Gauge find(Integer id) throws EdmException {
		return Finder.find(Gauge.class, id);
	}
	
	public static List<Gauge> list() throws EdmException {
		return Finder.list(Gauge.class, "id");
	}
	
	public void create() throws EdmException {
		Persister.create(this);
	}
	
	public void delete() throws EdmException {
		Persister.delete(this);
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCn() {
		return cn;
	}
	
	public void setCn(String cn) {
		this.cn = cn;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
}
