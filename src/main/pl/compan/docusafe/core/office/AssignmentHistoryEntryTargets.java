package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;


public class AssignmentHistoryEntryTargets
{
	public static int GUID = 1;
	public static int USER = 2;
	
	private Integer type;
	private String  value;
	
	public AssignmentHistoryEntryTargets() {}
	
	public AssignmentHistoryEntryTargets(Integer type, String  value) 
	{
		this.type = type;
		this.value = value;
	}
	
	public String toPrettyString()
	{
		try
		{
			if(GUID == this.type)
			{
				return DSDivision.find(this.value).getName();
			}
			else
			{
				return DSUser.findByUsername(this.value).asFirstnameLastname();
			}
		}
		catch(Exception e) { return ""; }		
	}
	
	public Integer getType() 
	{
		return type;
	}
	
	public void setType(Integer type) 
	{
		this.type = type;
	}
	
	public String getValue() 
	{
		return value;
	}
	
	public void setValue(String value) 
	{
		this.value = value;
	}

}