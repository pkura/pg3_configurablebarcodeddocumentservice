package pl.compan.docusafe.core.office;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

/**
 * Spos�b dostarczenia pisma.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InOfficeDocumentDelivery.java,v 1.10 2010/07/29 11:00:16 mariuszk Exp $
 */
public class InOfficeDocumentDelivery extends OfficeDocumentDelivery
{
	public static final String DELIVERY_EPUAP = "ePUAP";
	public static final String IMPORTED_DOCUMENT = "Zaimportowany";
    /**
     * Konstruktor u�ywany przez Hibernate.
     */
    InOfficeDocumentDelivery()
    {
    }

    public InOfficeDocumentDelivery(String name)
    {
        super(name);
    }

    protected Class myDocumentClass()
    {
        return InOfficeDocument.class;
    }

    /**      0
     * Zwraca list� wszystkich obiekt�w InOfficeDocumentDelivery
     * znajduj�cych si� w bazie posortowanych po polu posn.
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static List<InOfficeDocumentDelivery> list() throws EdmException
    {
    	String orderby = "posn";
    	if(AvailabilityManager.isAvailable("deliveryOrderAlpha"))
    	{
    		orderby = "name";
    	}
    	List<InOfficeDocumentDelivery> list = Finder.list(InOfficeDocumentDelivery.class, orderby);
        for (InOfficeDocumentDelivery delivery : list)
        {
            DSApi.initializeProxy(delivery);
        }
        return list;
    }
    
    public static InOfficeDocumentDelivery findByName(String name) throws EdmException
    {
    	Criteria c = DSApi.context().session().createCriteria(InOfficeDocumentDelivery.class);            
        c.add(Restrictions.eq("name",name)); 
        return (InOfficeDocumentDelivery) c.uniqueResult();
    }

    public static InOfficeDocumentDelivery find(Integer id)
        throws EdmException
    {
        return (InOfficeDocumentDelivery) Finder.find(InOfficeDocumentDelivery.class, id);
    }
}
