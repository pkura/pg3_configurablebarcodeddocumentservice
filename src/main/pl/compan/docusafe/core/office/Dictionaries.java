package pl.compan.docusafe.core.office;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import java.util.List;

/* User: Administrator, Date: 2005-07-04 16:35:33 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Dictionaries.java,v 1.4 2008/11/24 21:47:08 wkuty Exp $
 */
public class Dictionaries
{
    public static Integer updatePosnOnSave(Class clazz) throws CallbackException
    {
        try
        {
            Integer posn;

            List max = DSApi.context().classicSession().find("select max(s.posn) from s in class "+
                clazz.getName());

            if (max != null && max.size() > 0 && max.get(0) instanceof Long)
            {
                posn = new Integer(((Long) max.get(0)).intValue()+1);
            }
            else
            {
                posn = new Integer(0);
            }

            return posn;
        }
        catch (HibernateException e)
        {
            throw new CallbackException(e.getMessage(), e);
        }
        catch (EdmException e)
        {
            throw new CallbackException(e.getMessage(), e);
        }
    }
}
