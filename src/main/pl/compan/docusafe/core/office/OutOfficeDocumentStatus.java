package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.EdmException;

/**
 * Ta klasa nie jest obecnie u�ywana.
 * 
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OutOfficeDocumentStatus.java,v 1.3 2005/07/21 14:42:10 lk Exp $
 */
public class OutOfficeDocumentStatus extends OfficeDocumentStatus
{
    public boolean canDelete() throws EdmException
    {
        throw new UnsupportedOperationException();
    }
}
