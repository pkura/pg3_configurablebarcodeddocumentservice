package pl.compan.docusafe.core.office;

import org.apache.commons.lang.StringUtils;
import org.hibernate.*;
import org.hibernate.classic.Lifecycle;
import org.hibernate.type.Type;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;

import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.text.NumberFormat;
import java.util.*;
import java.util.prefs.Preferences;

/**
 * <p>Abstrakcyjna klasa bazowa dla teczek (OfficeFolder) oraz
 * spraw (OfficeCase).</p>
 *
 * <p>Walidacj� <code>officeId</code> poprzez RegExpy uruchamiamy
 * availablesem <code>numeracja.teczek.regexp</code>. Konfiguracja
 * RegExp�w odbywa si� w panelu admina: "Administracja/Numeracja teczek", <code>admin/office-ids.action</code> (<code>OfficeIdsAction</code>, <code>office-ids.jsp</code>)</p>
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Container.java,v 1.32 2009/10/21 13:40:36 mariuszk Exp $
 */
public abstract class Container implements Lifecycle
{
    private StringManager sm=
        GlobalPreferences.loadPropertiesFile(Container.class.getPackage().getName(),null);
    private static final Log log = LogFactory.getLog(Container.class);

    /**
     * Klucz Preferences, pod kt�rym znajd� si� formaty numer�w
     * spraw. Do nazwy tego klucza nale�y do��czy� guid dzia�u,
     * w kt�rym format jest efektywny. G��wny format znajduje si�
     * w ga��zi NODE_BASE + "/" + Division.ROOT_DIVISION.
     */
    public static final String NODE_BASE = "modules/coreoffice/officeid";

    public static final String OF_PREFIX = "officeFolderPrefix";
    public static final String OF_PREFIX_SEP = "officeFolderPrefixSeparator";
    public static final String OF_MIDDLE = "officeFolderMiddle";
    public static final String OF_SUFFIX_SEP = "officeFolderSuffixSeparator";
    public static final String OF_SUFFIX = "officeFolderSuffix";
    public static final String OF_REGEXP = "officeFolderRegexp";
    public static final String OF_REGEXP_INFO = "officeFolderRegexpInfo";

    public static final String[] OF_KEYS = {
        OF_PREFIX, OF_PREFIX_SEP, OF_MIDDLE, OF_SUFFIX_SEP, OF_SUFFIX
    };

    public static final String OSF_PREFIX = "officeSubfolderPrefix";
    public static final String OSF_PREFIX_SEP = "officeSubfolderPrefixSeparator";
    public static final String OSF_MIDDLE = "officeSubfolderMiddle";
    public static final String OSF_SUFFIX_SEP = "officeSubfolderSuffixSeparator";
    public static final String OSF_SUFFIX = "officeSubfolderSuffix";
    public static final String OSF_REGEXP = "officeSubfolderRegexp";
    public static final String OSF_REGEXP_INFO = "officeSubfolderRegexpInfo";

    public static final String[] OSF_KEYS = {
        OSF_PREFIX, OSF_PREFIX_SEP, OSF_MIDDLE, OSF_SUFFIX_SEP, OSF_SUFFIX
    };

    public static final String OC_PREFIX = "officeCasePrefix";
    public static final String OC_PREFIX_SEP = "officeCasePrefixSeparator";
    public static final String OC_MIDDLE = "officeCaseMiddle";
    public static final String OC_SUFFIX_SEP = "officeCaseSuffixSeparator";
    public static final String OC_SUFFIX = "officeCaseSuffix";
    public static final String OC_REGEXP = "officeCaseRegexp";
    public static final String OC_REGEXP_INFO = "officeCaseRegexpInfo";

    public static final String[] OC_KEYS = {
        OC_PREFIX, OC_PREFIX_SEP, OC_MIDDLE, OC_SUFFIX_SEP, OC_SUFFIX
    };

    /**
     * Nazwa klucza, pod kt�rym w Preferences znajdzie si� kod
     * formatuj�cy numer kancelaryjny pisma. Klucz trzymany
     * jest w tej samej ga��zi, co formaty dla numer�w teczek
     * i spraw - {@link Container#NODE_BASE}.
     */
    public static final String DOC_FORMAT = "documentFormat";
//    public static final String DOC_REGEXP = "documentRegexp";

    private Long id;
    private String officeId;
    private String officeIdPrefix;
    private String officeIdSuffix;
    private String officeIdMiddle;
    private String divisionGuid;
    private String author;
    private String clerk;
    private Date ctime;
    private Integer sequenceId;
    /** nieuzywane w module archiwum */
    private boolean archived;
    /** uzywane w module archiwum */
    private Integer archiveStatus;
    private Date archivedDate;
	private String lparam;
    private Long wparam;
    private Rwa rwa;
    private List<Audit> audit;
    private List<Remark> remarks;
    protected Integer year;

	
	public static enum ArchiveStatus{
		None,		// 0 odpowiednik null
		ToArchive,	// 1 na spisie, procesowany do zarchiwizowania [USTAWIANY TYLKO FOLDEROM, nie sprawom]
		Archived,	// 2 na spisie, zarchiwizowany
		ToShred,	// 3 zarchiwizowany i procesowany na protokole brakowania
		Shredded,	// 4 wybrakowany
		Withdraw,    // 5 zablokowanie sprawy w trakcie wycofywania
		ToTransferToNA, // 6 na spisie przekazania do archiwum panstwowego
		TransferedNA,    // 7 przekazane do AP
        Shared       // 8 udostepnione
    }
	/*public static enum RegExpInfoType{
		
		OC_REGEXP_INFO,	// 0 Regexp info text dla sprawy OC_REGEXP_INFO
		OF_REGEXP_INFO,	// 1 Regexp info text dla teczki OF_REGEXP_INFO
		OSF_REGEXP_INFO,	// 2  Regexp info text dla Podteczki OSF_REGEXP_INFO
    }*/
	/**
     * Metoda pomocnicza u�ywana w jsp.
     */
    public boolean isCase()
    {
        return this instanceof OfficeCase;
    }

    public void create(String officeId, String officeIdPrefix, String officeIdMiddle, String officeIdSuffix)
        throws EdmException, SQLException
    {
        /**
         * Je�li jest w��czona walidacja regexp to walidujemy pole <code>officeId</code>.
         * W przypadku nie przej�cia walidacji rzucany jest wyj�tek.
         */
        if(AvailabilityManager.isAvailable("numeracja.teczek.regexp")) {
            String regexp = getRegexp();

            if(StringUtils.isNotEmpty(regexp)) {
                boolean isValid = officeId.matches(regexp);

                if(! isValid) {
                    throw new EdmException(getFormatErrorMessage(officeId));
                }
            }
        }

        setOfficeId(officeId);
        setOfficeIdPrefix(officeIdPrefix);
        setOfficeIdMiddle(officeIdMiddle);
        setOfficeIdSuffix(officeIdSuffix);
        if (getSequenceId() == null)
            setSequenceId(suggestSequenceId());

        if (log.isDebugEnabled())
            log.debug("Tworzenie teczki "+officeId);    

        try
        {
            // sprawdzenie, czy istnieje ju� sprawa/teczka o wybranym identyfikatorze
            List list = DSApi.context().classicSession().find("from c in class "+
                Container.class.getName()+
                " where c.officeId = ? and c.year = ?",
                new Object[] { officeId, year },
                new Type[] { Hibernate.STRING, Hibernate.INTEGER });
            if (list != null && list.size() > 0)
            {
                Container c = (Container) list.get(0);
                throw new EdmException("Istnieje ju� "+
                    (c instanceof OfficeFolder ? "teczka" : "sprawa")+
                    " maj�ca identyfikator '"+officeId+"'. Nadaj tej "+((this instanceof OfficeFolder) ? "teczce" : "sprawie")+" unikalny numer.");
            }

            if (AvailabilityManager.isAvailable("menu.left.user.to.briefcase")){
            	pl.compan.docusafe.web.admin.UserToBriefcaseAction.onCreateBriefcase(this.sequenceId,officeId);
            }
            DSApi.context().session().save(this);
          //  DSApi.context().session().refresh(this);
            postCreateHook();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }catch (SQLException e) {
        	 throw new SQLException(e);
		}
    }

    protected abstract String getFormatErrorMessage(String officeId);

    public void changeOfficeId(String officeId, String officeIdPrefix, String officeIdMiddle, String officeIdSuffix, boolean creating)
    throws EdmException, SQLException
    {
        if (creating) {
            create(officeId,officeIdPrefix,officeIdMiddle,officeIdSuffix);
            return;
        }

        boolean changed = !officeId.equals(this.officeId);

        if (log.isDebugEnabled())
            log.debug("Zmiana numeru teczki "+officeId);

        if (changed)
        {
            try
            {
                // sprawdzenie, czy istnieje ju� sprawa/teczka o wybranym identyfikatorze
                List list = DSApi.context().classicSession().find("from c in class "+
                    Container.class.getName()+
                    " where c.officeId = ? and c.year = ?",
                    new Object[] { officeId, year },
                    new Type[] { Hibernate.STRING, Hibernate.INTEGER });
                if (list != null && list.size() > 0)
                {
                    Container c = (Container) list.get(0);
                        throw new EdmException("Istnieje ju� "+
                            (c instanceof OfficeFolder ? "teczka" : "sprawa")+
                            " maj�ca identyfikator "+officeId+". Nadaj tej "+((this instanceof OfficeFolder) ? "teczce" : "sprawie")+" unikalny numer.");
                }

            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
        }
        setOfficeId(officeId);
        setOfficeIdPrefix(officeIdPrefix);
        setOfficeIdMiddle(officeIdMiddle);
        setOfficeIdSuffix(officeIdSuffix);

        if (this instanceof OfficeFolder)
        {
        getAudit().add(Audit.create("changeNumber",
                DSApi.context().getPrincipalName(),
                sm.getString("ZmienionoNumerTeczkiNa",getOfficeId()),getOfficeId()));
        }
        else
        {
            getAudit().add(Audit.create("changeNumber",
                DSApi.context().getPrincipalName(),
                sm.getString("ZmienionoNumerSprawyNa",getOfficeId()),getOfficeId()));
        }
    }

    protected void postCreateHook() throws EdmException
    {
    }

    protected abstract String getCreateAuditDescription();

    public abstract void delete() throws EdmException;

    public abstract Integer suggestSequenceId() throws EdmException;

    /**
     * Zwraca kolejny numer sekwencyjny dla tej podteczki.
     * @param parentId Identyfikator teczki nadrz�dnej lub null, je�eli
     *   teczka nie ma rodzica.
     * @return
     * @throws EdmException
     */
    protected final Integer suggestSequenceId(Long parentId) throws EdmException
    {
        PreparedStatement pst = null;
        try
        {
        	if (parentId == null)
        	{
        		pst = DSApi.context().prepareStatement("select max(sequenceid) from "+
                        DSApi.getTableName(Container.class)+" where parent_id is null");
        	}
        	else
        	{
        		pst = DSApi.context().prepareStatement("select max(sequenceid) from "+
                        DSApi.getTableName(Container.class)+" where parent_id = ?");
                        pst.setLong(1, parentId);
        	}

            ResultSet rs = pst.executeQuery();
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� maksymalnej pozycji dla teczki");

            int max = rs.getInt(1);

            if (rs.wasNull())
            {
                return new Integer(1);
            }
            else
            {
                return new Integer(max+1);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }

    /**
     * Zwraca map� formatu na warto��. Np. d0 => nazwa dzia�u, y4 => rok, itd.
     * @param year
     * @return
     * @throws EdmException
     */
    protected Map<String, Object> getFormatContext(int year) throws EdmException
    {
        Map<String, Object> context = new HashMap<String, Object>();

        if (year == 0)
            year = GlobalPreferences.getCurrentYear();

        DSDivision division = DSDivision.find(getDivisionGuid());

        List<String> divisionCodes = new ArrayList<String>(8);
        DSDivision tmp = division;
        do
        {
            if (tmp.isRoot())
                break;
            if (tmp.getCode() == null)
                throw new EdmException("Brak kodu dzia�u "+division.getName()+" nie mo�na " +
                    "wygenerowa� numeru teczki/sprawy");
            divisionCodes.add(tmp.getCode());
            tmp = tmp.getParent();
        }
        while (tmp != null);
        Collections.reverse(divisionCodes);

        context.put("d0", division.getCode());
        for (int i=0; i < divisionCodes.size(); i++)
        {
            context.put("d"+(i+1), divisionCodes.get(i));
        }

        NumberFormat nf = NumberFormat.getIntegerInstance();
        nf.setMaximumIntegerDigits(2);
        nf.setMinimumIntegerDigits(2);
        context.put("y2", nf.format(year % 100));
        context.put("y4", String.valueOf(year));
        context.put("r", getRwa().getRwa());

        String userIdentifier = DSApi.context().getDSUser().getIdentifier();
        if (userIdentifier != null)
            context.put("p", userIdentifier);

        return context;
    }

    /**
     * Zwraca true, je�eli podany numer kolejny podteczki jest dost�pny.
     * Metoda nie powinna by� wywo�ywana dla teczek najwy�szego poziomu
     * (parent == null); rzuca w�wczas wyj�tek EdmException.
     * @throws EdmException
     */
    protected boolean sequenceIdAvailable(Long parentId, Integer sequenceId) throws EdmException
    {
        if (parentId == null)
            throw new EdmException("Metoda sequenceIdAvailable() nie powinna by� wywo�ywana " +
                "dla teczek najwy�szego poziomu");

        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select count(*) from "+
                DSApi.getTableName(Container.class)+" where parent_id = ? and "+
                DSApi.getColumnName(Container.class, "sequenceId")+" = ?");
            pst.setLong(1, parentId);
            pst.setInt(2, sequenceId);
            ResultSet rs = pst.executeQuery();

            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby teczek i spraw");

            return rs.getInt(1) == 0;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }

    /**
     * Zwraca list� dokument�w znajduj�cych si� w sprawie, zwracana lista
     * mo�e by� pusta.
     */
    public List getDocuments() throws EdmException
    {
        return getDocuments(OfficeDocument.class);
    }


    /**
     * Lista dokument�w w sprawie nie jest odczytywana przez Hibernate,
     * poniewa� mog� to by� dokumenty dziedzicz�ce po OfficeDocument,
     * czyli znajduj�ce si� w r�nych tabelach, czego nie da si� opisa�
     * w pliku hbm.xml.
     */
    public List getDocuments(Class documentClass) throws EdmException
    {
        if (!OfficeDocument.class.isAssignableFrom(documentClass))
            throw new IllegalArgumentException("Spodziewano si� nazwy klasy dziedzicz�cej" +
                " po OfficeDocument");

        try
        {
            Criteria crit = DSApi.context().session().createCriteria(documentClass);
            crit.add(org.hibernate.criterion.Expression.eq("containingCaseId", id));

            return crit.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void addRemark(Remark remark)
    {
        if (remarks == null)
            remarks = new ArrayList<Remark>(8);

        remarks.add(remark);
    }

    /* Lifecycle */

    public boolean onSave(Session s) throws CallbackException
    {
        if (author == null)
        {
            author = DSApi.context().getPrincipalName();
        }

        if (ctime == null)
        {
            ctime = new Date();
        }

        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }

    abstract protected String getRegexp();

    public final boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Container)) return false;

        final Container container = (Container) o;

        if (id != null ? !id.equals(container.id) : container.id != null) return false;

        return true;
    }

    public final int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getOfficeId()
    {
        return officeId;
    }

    public void setOfficeId(String officeId)
    {
        this.officeId = officeId;
    }

    public List<Audit> getAudit()
    {
        if (audit == null)
            audit = new LinkedList<Audit>();
        return audit;
    }

    public void setAudit(List<Audit> audit)
    {
        this.audit = audit;
    }

    public String getOfficeIdPrefix()
    {
        return officeIdPrefix;
    }

    public void setOfficeIdPrefix(String officeIdPrefix)
    {
        this.officeIdPrefix = officeIdPrefix;
    }

    public String getOfficeIdSuffix()
    {
        return officeIdSuffix;
    }

    public void setOfficeIdSuffix(String officeIdSuffix)
    {
        this.officeIdSuffix = officeIdSuffix;
    }

    public String getOfficeIdMiddle()
    {
        return officeIdMiddle;
    }

    public void setOfficeIdMiddle(String officeIdMiddle)
    {
        this.officeIdMiddle = officeIdMiddle;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getClerk()
    {
        return clerk;
    }

    public void setClerkNoChecks(String clerk) {
        this.clerk = clerk;
    }

    public void setClerk(String clerk)
        throws EdmException, AccessDeniedException
    {
        if (clerk == null)
            throw new NullPointerException("clerk");

        if (DSApi.isContextOpen() &&
            id != null &&
            !clerk.equals(this.clerk) &&
            ((this instanceof OfficeCase &&
                !DSApi.context().hasPermission(DSPermission.SPRAWA_ZMIANA_REFERENTA)) ||
             (this instanceof OfficeFolder &&
                !DSApi.context().hasPermission(DSPermission.TECZKA_ZMIANA_REFERENTA_KOMORKA))))
        {
            // najwyra�niej brak uprawnie� do zmiany referenta, sprawdzam\
            // jeszcze zast�pstwa
            boolean can = false;
            for (DSUser subst : DSApi.context().getDSUser().getSubstituted())
            {
                if (clerk.equals(subst.getName()))
                {
                    can = true;
                    break;
                }
            }

            if (!can)
                throw new AccessDeniedException("Brak uprawnie� do zmiany referenta sprawy");
        }

        String newClerk = DSUser.safeToFirstnameLastname(clerk);
        String oldClerk = this.clerk != null ? DSUser.safeToFirstnameLastname(this.clerk) : null;
        String tmp = null;
        if (oldClerk != null)
            tmp = sm.getString("ZmianaReferentaZna",oldClerk,newClerk);
        else
            tmp = sm.getString("ZmianaReferentaNa",newClerk);
        if (this.clerk != null && !this.clerk.equals(clerk))
            getAudit().add(Audit.create("clerk", DSApi.context().getPrincipalName(),
                tmp, clerk));

        this.clerk = clerk;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    /** flaga nieuzywana! */
    public boolean isArchived()
    {
        return archived;
    }

    /** flaga nieuzywana! */
    public void setArchived(boolean archived)
    {
        this.archived = archived;
    }

    public String getLparam()
    {
        return lparam;
    }

    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }

    public Long getWparam()
    {
        return wparam;
    }

    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }

    public Rwa getRwa()
    {
        return rwa;
    }

    public void setRwa(Rwa rwa)
    {
        this.rwa = rwa;
    }

    public List getRemarks()
    {
        return remarks;
    }

    public void setRemarks(List<Remark> remarks)
    {
        this.remarks = remarks;
    }

    public Integer getYear()
    {
        return year;
    }

    public void setYear(Integer year)
    {
        this.year = year;
    }
    public Date getArchivedDate() {
		return archivedDate;
	}

	public void setArchivedDate(Date archDate) {
		this.archivedDate = archDate;
	}

	public ArchiveStatus getArchiveStatus()
	{
		if( archiveStatus!=null && archiveStatus>=0 && archiveStatus<ArchiveStatus.values().length)
			return ArchiveStatus.values()[archiveStatus];
		else
			return ArchiveStatus.None;
	}

	void setArchiveStatus(Integer archiveStatus)
	{
		this.archiveStatus= archiveStatus;
	}
	
	public void setArchiveStatus(ArchiveStatus archiveStatus)
	{
		this.archiveStatus= archiveStatus.ordinal();
	}

	public static String getRegExpMessage(String type)
	{
	    	Preferences prefs = DSApi.context().systemPreferences()
	                .node(NODE_BASE + "/" + DSDivision.ROOT_GUID);
	    	
	        return prefs.get(type, "");
	}
}


