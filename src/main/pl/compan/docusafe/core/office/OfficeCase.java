package pl.compan.docusafe.core.office;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;

import com.google.gson.Gson;

import org.apache.commons.lang.StringUtils;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.type.Type;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.joda.time.Period;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AbstractQuery;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResults.Mapper;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.core.office.container.ContainerFinder;
import pl.compan.docusafe.core.office.container.OfficeCaseFinderProvider;
import pl.compan.docusafe.core.regtype.RegistryEntry;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FormatUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.admin.UserToBriefcaseAction;
import pl.compan.docusafe.web.office.FindCasesAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import std.lambda;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OfficeCase.java,v 1.61 2009/12/10 15:30:40 tomekl Exp $
 */
public class OfficeCase extends Container implements Lifecycle
{
	private static final Logger log = LoggerFactory.getLogger(OfficeCase.class);
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(OfficeCase.class.getPackage().getName(),null);
   // private Integer year;
    private Date openDate;
    private Date finishDate;
    private Integer documentSequenceId;
    private String trackingNumber;
    private String trackingPassword;
    private boolean closed;
    private OfficeFolder parent;
    private OfficeCase precedent;
    private CasePriority priority;
    private CaseStatus status;
    private Integer registries;
    private String description;
    /** data przeniesienia do archiwum */
    private Date archivedDate;
    /** data utworzenia protokolu brakowania */
    private Date protokolDate;
    /** tytul sprawy */
    private String title;

    /** okre�la, do kt�rych rejestr�w zosta�a ta sprawa wpisana (opr�cz stypendi�w i wniosk�w na budow�) */
    private Set<RegistryEntry> registryEntries;

    OfficeCase() {}

    public OfficeCase(OfficeFolder parent) throws EdmException
    {
        this.openDate = GlobalPreferences.getCurrentDay();
        this.year = new Integer(GlobalPreferences.getCurrentYear());
        this.parent = parent;
        setRwa(parent.getRwa());
        setDivisionGuid(parent.getDivisionGuid());

        String[] breakdown = suggestOfficeId();
        String suggestedOfficeId = StringUtils.join(breakdown, "");
        getAudit().add(Audit.create("creation", DSApi.context().getPrincipalName(),
            sm.getString("UtworzonoSpraweOnumerze",suggestedOfficeId), suggestedOfficeId));
    }

    protected String getRegexp() {
        Preferences prefs = DSApi.context().systemPreferences()
                .node(NODE_BASE + "/" + DSDivision.ROOT_GUID);

        return prefs.get(Container.OC_REGEXP, null);
    }

    @Override
    protected String getFormatErrorMessage(String officeId) {
        return "B��dny format numeru sprawy: "+officeId;
    }

    /**
     * TODO: przejsc na nowy mechanizm obslugi DM
     */
    protected void postCreateHook() throws EdmException
    {
        if (parent != null) parent.notifyCaseAdded(this);
        DataMartEvent dme = new DataMartEvent(false, this.getId(), null, DataMartDefs.BIP_CASE_INFO, null, null, null, null);
        DataMartManager.storeEvent(dme);

        String periodString = Docusafe.getAdditionProperty("case.reminder.period");
        if (periodString != null) {
            try {
                log.info("OfficeCase - przygotowywanie przypomnienia dla prze�o�onego");
//            Date eventDate = new Instant(this.getFinishDate().getTime())
                Period period = ISOPeriodFormat.standard().parsePeriod(periodString);
                //Prawid�owe wyliczanie daty:
//                Date eventDate = new DateTime(this.getFinishDate()).minus(period).toDate();
                //Do debugowania
                Date eventDate = new DateTime(new Date()).plus(Period.minutes(1)).toDate();

                log.info("finishDate = '{}' eventDate = '{}'", this.getFinishDate(), eventDate);

                EventFactory.registerEvent("case-finished-trigger", "case-reminder", this.getId().toString(), eventDate, eventDate);
            } catch (Exception ex) {
                log.info(ex.getMessage(), ex);
            }
        }
  /*      DataMartEventBuilder builder = DataMartEventBuilder.create();
        builder.event(DataMartEventProcessors.EventType.BIP_CASE_INFO).documentId(this.getId());*/
    }

    public boolean canViewOfficeCase() throws EdmException
    {
        boolean canView = DSApi.context().hasPermission(DSPermission.SPRAWA_WSZYSTKIE_PODGLAD) || DSApi.context().isAdmin();
        if (!canView)
        {
            DSUser user = DSApi.context().getDSUser();
            if (DSApi.context().hasPermission(DSPermission.SPRAWA_KOMORKA_PODGLAD))
            {
                DSDivision[] divisions = user.getDivisions();
                for (int i=0; i < divisions.length; i++)
                {
                    if (this.getDivisionGuid().equals(divisions[i].getGuid()))
                    {
                        canView = true;
                        break;
                    }
                    if (divisions[i].isPosition())
                    {
                        DSDivision parent = divisions[i].getParent();
                        if ((parent != null) && (this.getDivisionGuid().equals(parent.getGuid())))
                        {
                            canView = true;
                            break;
                        }
                    }
                }
            }

            if (this.getClerk().equals(user.getName()))
                canView = true;

        }

        return canView;
    }
    /**
     * Zwraca pi�cioelementow� tablic� reprezentuj�c� sugerowany
     * numer kancelaryjny. Kolejne elementy tablicy oznaczaj�:
     * officeIdPrefix, officeIdPrefixSeparator, officeIdMiddle,
     * officeIdSuffixSeparator, OfficeIdSuffix
     */
    public String[] suggestOfficeId() throws EdmException
    {
        // elementy odpowiadaj�ce kluczom w tablicy OF_KEYS
        String[] id = new String[5];

        Map<String, Object> context = getFormatContext(0);

        // TODO: pole parent powinno znale�� si� w klasie Container, parent() powinny
        // pozosta� w klasach dziedzicz�cych ze wzgl�du na r�ny typ zwracanych warto�ci
        if (parent != null)
        {
            context.put("lp", getSequenceId() != null ? getSequenceId() : suggestSequenceId());
            context.put("pp", parent.getOfficeIdPrefix());
            context.put("pm", parent.getOfficeIdMiddle());
            context.put("ps", parent.getOfficeIdSuffix());
        }

        Preferences prefs = DSApi.context().systemPreferences().node(
            NODE_BASE+"/"+DSDivision.ROOT_GUID);

        if (prefs.get(OC_KEYS[2], null) == null)
            throw new EdmException("Nie zdefiniowano formatu numeru sprawy");

        // forma
        for (int i=0; i < id.length; i++)
        {
            String format = prefs.get(OC_KEYS[i], null);
            if (format == null)
            {
                id[i] = "";
            }
            // dla drugiego i przedostatniego elementu formatowanie nie
            // jest potrzebne (s� to separatory)
            else if (i == 1 || i == 3)
            {
                id[i] = format;
            }
            else
            {
                id[i] = FormatUtils.formatExpression(format, context);
            }
        }

        return id;
    }

    /**
     * Zwraca kolejny numer sekwencyjny dla tej podteczki. Wywo�anie tej funkcji
     * nie ma sensu dla teczki najwy�szego poziomu (bez rodzica) i spowoduje
     * rzucenie wyj�tku EdmException.
     * @return
     * @throws EdmException
     */
    public Integer suggestSequenceId() throws EdmException
    {
        return suggestSequenceId(parent != null ? parent.getId() : null);
    }

    /**
     * Zwraca true, je�eli podany numer kolejny podteczki jest dost�pny.
     * Metoda nie powinna by� wywo�ywana dla teczek najwy�szego poziomu
     * (parent == null); rzuca w�wczas wyj�tek EdmException.
     * @throws EdmException
     */
    public boolean sequenceIdAvailable(Integer sequenceId) throws EdmException
    {
        return sequenceIdAvailable(parent.getId(), sequenceId);
    }

    /**
     * Zwraca bie��c� warto�� {@link #documentSequenceId} i zwi�ksza
     * warto�� pola o jeden.
     * @return
     * @throws EdmException
     */
    public Integer nextDocumentSequenceId() throws EdmException
    {
        Integer seq = documentSequenceId;
        documentSequenceId = new Integer(documentSequenceId.intValue() + 1);
        return seq;
    }

    public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
            DataMartEvent dme = new DataMartEvent(false, this.getId(), null, DataMartDefs.BIP_CASE_INFO, null, null, null, null);
            DataMartManager.storeEvent(dme);
      /*      DataMartEventBuilder builder = DataMartEventBuilder.create();
            builder.event(DataMartEventProcessors.EventType.BIP_CASE_INFO).documentId(this.getId());*/
        }
        catch (HibernateException e)
        {
            throw new EdmException(sm.getString("BladUsunieciaSpray")+e.getMessage());
        }

    }

    public static OfficeCase find(Long id) throws EdmException
    {
		return (OfficeCase) Finder.find(OfficeCase.class, id);
    }

    /**
     * Znajduje wszystkie sprawy danego referenta, kt�re nie zosta�y
     * oznaczone jako zarchiwizowane.
     * @param clerk
     * @return
     * @throws EdmException
     */
    public static List<OfficeCase> findByClerk(String clerk)
        throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(OfficeCase.class);
            crit.add(org.hibernate.criterion.Expression.eq("clerk", clerk));
            crit.add(org.hibernate.criterion.Expression.eq("archived", Boolean.FALSE));

            return (List<OfficeCase>) crit.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static OfficeCase findByOfficeId(String officeId) throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(OfficeCase.class);
            crit.add(org.hibernate.criterion.Expression.eq("officeId", officeId));
            List cases = crit.list();

            if (cases.size() > 0)
            {
                return (OfficeCase) cases.get(0);
            }

            throw new CaseNotFoundException(officeId);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static OfficeCase findByOfficeIdLike(String officeId) throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(OfficeCase.class);
            crit.add(org.hibernate.criterion.Expression.ilike("officeId", "%" + officeId + "%"));
            List cases = crit.list();

            if (cases.size() > 0)
            {
                return (OfficeCase) cases.get(0);
            }

            throw new CaseNotFoundException(officeId);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List<OfficeCase> findByOfficeIdLike(String number, Container parent) throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(OfficeCase.class);
            crit.add(org.hibernate.criterion.Expression.ilike("officeId", "%" + number + "%"));
            crit.add(org.hibernate.criterion.Expression.eq("parent", parent));
            List<OfficeCase> cases = crit.list();

            if (cases.size() > 0)
            {
                return cases;
            }

            throw new CaseNotFoundException("zawieraj�cym " + number);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }


    public static List<OfficeCase> findByParent(Long parentId) throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(OfficeCase.class);

            crit.add(org.hibernate.criterion.Expression.eq("parent", OfficeFolder.find(parentId)));
            List<OfficeCase> cases = crit.list();

            if (cases.size() > 0)
            {
                return cases;
            }

            throw new CaseNotFoundException("zawieraj�cym " + parentId);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List<OfficeCase> findByParentId(Long parentId) throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(OfficeCase.class);

            crit.add(org.hibernate.criterion.Expression.eq("parent", OfficeFolder.find(parentId)));
            List<OfficeCase> cases = crit.list();


                return cases;

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List<OfficeCase> findByDivision(DSDivision division) throws EdmException
	{
    	/* Criteria crit = DSApi.context().session().createCriteria(Container.class);
    	 
         crit.add(org.hibernate.criterion.Expression.eq("divisionGuid", division.getGuid()));
         crit.add(org.hibernate.criterion.Expression.eq("discriminator", "CASE"));
        */

         StringBuilder hql = new StringBuilder("select target from target in class ");
         hql.append(OfficeCase.class.getName());

         List<Type> types = new LinkedList<Type>();
         List<Object> values = new LinkedList<Object>();

         hql.append(" where closed = ? and (");
         values.add(Boolean.FALSE);
         types.add(Hibernate.BOOLEAN);

         hql.append("divisionGuid = ? )");
         values.add(division.getGuid());
         types.add(Hibernate.STRING);

         try
         {
             return DSApi.context().classicSession().find(hql.toString(),
                 values.toArray(),
                 types.toArray(new Type[types.size()]));
         }
         catch (HibernateException e)
         {
             throw new EdmHibernateException(e);
         }
        /* 
         List<OfficeCase> cases = crit.list();
            return cases;*/
	}


    public static List<OfficeCase> findUserCases(DSUser user, String officeId) throws EdmException
    {
        StringBuilder hql = new StringBuilder("select target from target in class ");
        hql.append(OfficeCase.class.getName());

        List<Type> types = new LinkedList<Type>();
        List<Object> values = new LinkedList<Object>();

        hql.append(" where closed = ? and (");
        values.add(Boolean.FALSE);
        types.add(Hibernate.BOOLEAN);

        hql.append("clerk = ?");
        values.add(user.getName());
        types.add(Hibernate.STRING);

        DSUser[] substs = user.getSubstituted();
        for (int i=0; i < substs.length; i++)
        {
            hql.append(" or clerk = ?");
            values.add(substs[i].getName());
            types.add(Hibernate.STRING);
        }

        hql.append(")");

        if (officeId != null)
        {
            hql.append("and officeid like ?");
            values.add("%"+officeId+"%");
            types.add(Hibernate.STRING);
        }

        try
        {
            return DSApi.context().classicSession().find(hql.toString(),
                values.toArray(),
                types.toArray(new Type[types.size()]));
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }//findUserCases method

    /**
     * Zwraca liczb� spraw danego u�ytkownika nie oznaczonych
     * jako zarchiwizowane.
     *
     * @param username
     * @param openOnly Je�eli true, liczone s� tylko sprawy otwarte.
     * @return
     * @throws EdmException
     */
    public static int countUserCases(String username, boolean openOnly)
        throws EdmException
    {
    	/*
        Criteria criteria = DSApi.context().session().createCriteria(OfficeCase.class);
        
        criteria.add(org.hibernate.criterion.Expression.eq("clerk", username));
        criteria.add(org.hibernate.criterion.Expression.eq("archived", Boolean.FALSE));
        if (openOnly)
            criteria.add(org.hibernate.criterion.Expression.eq("closed", Boolean.FALSE));
        */
    	String q = "select count(*) as ile from dso_container where discriminator = 'CASE' and clerk = ? and archived = 0";
    	if (openOnly)
    	{
    		q = q + " and closed = 0";
    	}
        try
        {

        	java.sql.PreparedStatement ps = DSApi.context().prepareStatement(q);
        	ps.setString(1,username);
        	java.sql.ResultSet rs = ps.executeQuery();
        	rs.next();
        	int resp = 0;
        	if (rs.getString("ile")!=null)
        	{
        		resp =rs.getInt("ile");
        	}
        	rs.close();
        	DSApi.context().closeStatement(ps);
        	return resp;
        	/*
            return criteria.list().size();
            
            */
        }
        catch (SQLException e1)
        {
            throw new EdmException(e1);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void notifyDocumentAdded(OfficeDocument document) throws EdmException
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 14);
        this.finishDate = calendar.getTime();
    }

    public static Query query(int offset, int limit)
    {
        return new Query(offset, limit);
    }

    private static final lambda<OfficeCase, OfficeCase> identityMapper =
        new lambda<OfficeCase, OfficeCase>()
        {
            public OfficeCase act(OfficeCase o)
            {
                return o;
            }
        };

    /**
     * search(query, lambda&lt;OfficeCase, OfficeCase>, OfficeCase.class);
     */
    public static SearchResults<OfficeCase> search(Query query) throws EdmException
    {
        ContainerFinder<OfficeCase, OfficeCase> provide;
        try {
            provide = OfficeCaseFinderProvider.instance().provide(query, identityMapper, OfficeCase.class);
        } catch (Exception e) {
            throw new EdmException("Wyst�pi� nieoczekiwany b��d, skontaktuj si� z administratorem", e);
        }
        return provide.find();
    }

    public static <T> SearchResults<T> search2(Query query2, lambda<OfficeCase, T> mapper,
            Class<T> mappedClass) throws EdmException
{
    List<T> results = new ArrayList<T>(query2.limit);

    if (AvailabilityManager.isAvailable("menu.left.user.to.briefcase")){

   	 	List<Long> list = UserToBriefcaseAction.getPrzypisaneSprawy(DSApi.context().getDSUser().getName());
       for (Long id : list)
    	   results.add(mapper.act(DSApi.context().load(OfficeCase.class,id)));
   }

    return new SearchResultsAdapter<T>(results, query2.offset, query2.limit, mappedClass);
}

    public static class Query extends AbstractQuery
    {
        private int offset;
        private int limit;
        private Integer seqNum;
        private Integer year;
        private Date cdateFrom;
        private Date cdateTo;
        private Date finishDateFrom;
        private Date finishDateTo;
        private String[] clerk;
        private String[] author;
        private Integer[] statusId;
        private Integer[] record;
        private Integer rwaId;
        private String officeId;
        private Boolean archived;
        private String title;
        private Date archDateFrom;
        private Date archDateTo;
		private Integer archiveStatus;

        public Query(int offset, int limit)
        {
            this.offset = offset;
            this.limit = limit;
        }

        protected boolean validSortField(String field)
        {
            return "officeId".equals(field) || "finishDate".equals(field) ||
                "statusId".equals(field) || "openDate".equals(field) ||
                "status".equals(field)||"title".equals(field)||"archivedDate".equals(field);        }

        public void setRecord(Integer[] record)
        {
            this.record = record;
        }

        public void setCdateFrom(Date cdateFrom)
        {
            this.cdateFrom = cdateFrom;
        }

        public void setCdateTo(Date cdateTo)
        {
            this.cdateTo = cdateTo;
        }

        public void setFinishDateFrom(Date finishDateFrom)
        {
            this.finishDateFrom = finishDateFrom;
        }

        public void setFinishDateTo(Date finishDateTo)
        {
            this.finishDateTo = finishDateTo;
        }

        public void setSeqNum(Integer seqNum)
        {
            this.seqNum = seqNum;
        }

        public void setYear(Integer year)
        {
            this.year = year;
        }

        public void setArchived(Boolean archived)
        {
            this.archived = archived;
        }

        public void setClerk(String[] clerk)
        {
            this.clerk = clerk;
        }

        public void setStatusId(Integer[] statusId)
        {
            this.statusId = statusId;
        }

        public void setAuthor(String[] author)
        {
            this.author = author;
        }

        public void setRwaId(Integer rwaId)
        {
            this.rwaId = rwaId;
        }

        public void setOfficeId(String officeId)
        {
            this.officeId = officeId;
        }

        public void setTitle(String title)
        {
            this.title = title;
        }

		public void setArchDateFrom(Date archDateFrom) {
			this.archDateFrom=archDateFrom;

		}

		public void setArchDateTo(Date archDateTo) {
			this.archDateTo=archDateTo;

		}

		public void setArchiveStatus(int archiveStatus) {
			this.archiveStatus=archiveStatus;
		}
		public void setArchiveStatusIsArchived(){
			//po prostu jest archiwum czyli potem status >=2
			this.archiveStatus=-1;
		}

		public void setArchiveStatusIsNotArchived() {
			this.archiveStatus=-2;

		}

        public int getOffset() {
            return offset;
        }

        public int getLimit() {
            return limit;
        }

        public Integer getSeqNum() {
            return seqNum;
        }

        public Integer getYear() {
            return year;
        }

        public Date getCdateFrom() {
            return cdateFrom;
        }

        public Date getCdateTo() {
            return cdateTo;
        }

        public Date getFinishDateFrom() {
            return finishDateFrom;
        }

        public Date getFinishDateTo() {
            return finishDateTo;
        }

        public String[] getClerk() {
            return clerk;
        }

        public String[] getAuthor() {
            return author;
        }

        public Integer[] getStatusId() {
            return statusId;
        }

        public Integer[] getRecord() {
            return record;
        }

        public Integer getRwaId() {
            return rwaId;
        }

        public String getOfficeId() {
            return officeId;
        }

        public Boolean getArchived() {
            return archived;
        }

        public String getTitle() {
            return title;
        }

        public Date getArchDateFrom() {
            return archDateFrom;
        }

        public Date getArchDateTo() {
            return archDateTo;
        }

        public Integer getArchiveStatus() {
            return archiveStatus;
        }
    }

    protected String getCreateAuditDescription()
    {
        return "Utworzono spraw� "+getOfficeId();
    }

    public int getDaysToFinish()
    {
        if (finishDate == null)
            throw new IllegalStateException("Nie okre�lono daty zako�czenia sprawy.");

        return DateUtils.substract(finishDate, new Date());
    }

    /* Lifecycle */

    public boolean onSave(Session s) throws CallbackException
    {
        if (parent == null){
            throw new CallbackException("Sprawa ("+getOfficeId()+") musi znajdowa� si� w teczce");
        }


/*
        if (year == null)
        {
            try
            {
                year = new Integer(GlobalPreferences.getCurrentYear());
            }
            catch (EdmException e)
            {
                throw new CallbackException("W systemie nie otwarto roku", e);
            }
        }

        if (openDate == null)
        {
            try
            {
                openDate = GlobalPreferences.getCurrentDay();
            }
            catch (EdmException e)
            {
                throw new CallbackException(e.getMessage(), e);
            }
        }
*/

        if (documentSequenceId == null)
        {
            documentSequenceId = new Integer(1);
        }

        return super.onSave(s);
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }

    public boolean isClosed()
    {
        return closed;
    }

    public void setClosed(boolean closed)
    {
        this.closed = closed;
    }

    public OfficeFolder getParent()
    {
        return parent;
    }

    public void setParent(OfficeFolder parent) throws EdmException
    {
        if (parent == null)
            throw new NullPointerException("parent");

        if (this.parent != null && !this.parent.equals(parent))
        {
            this.parent.notifyCaseRemoved(this);

            getAudit().add(Audit.create("parent",
                DSApi.context().getPrincipalName(),
                sm.getString("SprawePrzeniesionoDoTeczki",parent.getOfficeId(), this.parent.getOfficeId()),
                parent.getOfficeId(), parent.getId()));
        }
        // numer kolejny sugerowany dla nowego rodzica
        //setSequenceId(suggestSequenceId(parent.getId()));
        this.parent = parent;
    }

    public void setParentOnly(OfficeFolder parent) throws EdmException
    {
        if (parent == null)
            throw new NullPointerException("parent");

        //if (this.parent != null && parent.getId() != this.parent.getId())
        if (this.parent != null && !parent.getId().equals(this.parent.getId()))
        {
            this.parent.notifyCaseRemoved(this);

            getAudit().add(Audit.create("parent",
                DSApi.context().getPrincipalName(),
                sm.getString("SprawePrzeniesionoDoTeczki",parent.getOfficeId(), this.parent.getOfficeId()),
                parent.getOfficeId(), parent.getId()));
        }
        setSequenceId(0);
        this.parent = parent;
    }

    public OfficeCase getPrecedent()
    {
        return precedent;
    }

    public void setPrecedent(OfficeCase precedent)
    {
        this.precedent = precedent;
    }

   /* public Integer getYear()
    {
        return year;
    }

    public void setYear(Integer year)
    {
        this.year = year;
    }
     */
    public Date getOpenDate()
    {
        return openDate;
    }

    public void setOpenDate(Date openDate)
    {
        this.openDate = openDate;
    }

    public Date getFinishDate()
    {
        return finishDate;
    }

    /**
     * @param finishDate
     * @throws EdmException Gdy data zako�czenia jest wcze�niejsza od daty otwarcia.
     */
    public void setFinishDate(Date finishDate) throws EdmException
    {
        if (openDate != null && DateUtils.substract(finishDate, openDate) < 0)
            throw new EdmException("Data zako�czenia sprawy jest wcze�niejsza" +
                " ni� data otwarcia");

        if (this.finishDate != null && finishDate != null &&
            DateUtils.difference(this.finishDate, finishDate) > 0)
        {
            if(this.finishDate != null)
                getAudit().add(Audit.create("finishDate",
                    DSApi.context().getPrincipalName(),
                    sm.getString("ZmienionoPrzewidywanaDateZakonczeniaSprawyZna"
                    ,DateUtils.formatJsDate(this.finishDate),DateUtils.formatJsDate(finishDate)),
                    null, new Long(finishDate.getTime())));
            else
                getAudit().add(Audit.create("finishDate",
                    DSApi.context().getPrincipalName(),
                    sm.getString("ZmienionoPrzewidywanaDateZakonczeniaSprawyNa"
                    ,DateUtils.formatJsDate(finishDate)),
                    null, new Long(finishDate.getTime())));
        }
        this.finishDate = finishDate;
        log.trace("setFinishDate - Zmiana daty na now�={}",finishDate);
    }

    public Integer getDocumentSequenceId()
    {
        return documentSequenceId;
    }

    private void setDocumentSequenceId(Integer documentSequenceId)
    {
        this.documentSequenceId = documentSequenceId;
    }

    public String getTrackingNumber()
    {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber)
    {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingPassword()
    {
        return trackingPassword;
    }

    public void setTrackingPassword(String trackingPassword)
    {
        this.trackingPassword = trackingPassword;
    }

    public CasePriority getPriority()
    {
        return priority;
    }

    public void setPriority(CasePriority priority) throws EdmException
    {
    	//if (this.priority != null && priority != null && !priority.getId().equals(this.priority.getId()))
        if (this.priority != null && priority != null && this.priority.getId() != priority.getId())
    	{
            getAudit().add(Audit.create("priority",
                DSApi.context().getPrincipalName(),
                sm.getString("ZmienionoPriorytetSprawyNa",priority.getName()),
                priority.getName(), new Long(priority.getId())));
        }
        this.priority = priority;
    }

    public CaseStatus getStatus()
    {
        return status;
    }

    public void setStatus(CaseStatus status) throws EdmException
    {
        if (status == null)
            throw new NullPointerException("statusId");

        // sprawdzanie uprawnie� do zamykania sprawy
        if (!status.equals(this.status) &&
            DSApi.isContextOpen() &&
            getId() != null &&
            status.isClosingStatus() &&
            !DSApi.context().hasPermission(DSPermission.SPRAWA_ZAMYKANIE))
            throw new AccessDeniedException("Brak uprawnie� do zamykania sprawy");

        this.closed = status.isClosingStatus();

        if (!status.equals(this.status))
        {
            getAudit().add(Audit.create("statusId", DSApi.context().getPrincipalName(),
                sm.getString("ZmienionoStatusSprawyNa",status.getName()),
                status.getName(), new Long(status.getId())));

            if (status.isClosingStatus() && !this.status.isClosingStatus()) {
            	setFinishDate(Calendar.getInstance().getTime());
            	log.trace("setStatus - Zmiana statusu na ostatecznie zako�czona: {}",getFinishDate());
            }
        }
        this.status = status;
    }

    Integer getRegistries()
    {
        return registries;
    }

    void setRegistries(Integer registries)
    {
        this.registries = registries;
    }

    public void putInRegistry(int registry)
    {
        if (this.registries == null)
            this.registries = new Integer(registry);
        else
            this.registries = new Integer(this.registries.intValue() | registry);
    }

    public void removeFromRegistry(int registry)
    {
        if (this.registries != null)
            this.registries = new Integer(this.registries.intValue() - registry);
    }

    public boolean isInRegistry(int registry)
    {
        return this.registries != null && (this.registries.intValue() & registry) > 0;
    }

    public boolean isInAnyRegistry()
    {
        return this.registries != null && this.registries.intValue() != 0;
    }

    public Set<RegistryEntry> getRegistryEntries()
    {
        if (registryEntries == null)
            registryEntries = new LinkedHashSet<RegistryEntry>();
        return registryEntries;
    }

    public void setRegistryEntries(Set<RegistryEntry> registryEntries)
    {
        this.registryEntries = registryEntries;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    /** data przeniesienia do archiwum */
    public Date getArchivedDate()
	{
		return archivedDate;
	}

	public void setArchivedDate(Date archivedDate)
	{
		this.archivedDate= archivedDate;
	}

	/** data utworzenia protokolu brakowania */
	public Date getProtokolDate()
	{
		return protokolDate;
	}

	public void setProtokolDate(Date protokolDate)
	{
		this.protokolDate= protokolDate;
	}

	public String debugString() {
        return "OfficeCase{" +
                "closed=" + closed +
                ", sm=" + sm +
                ", openDate=" + openDate +
                ", finishDate=" + finishDate +
                ", documentSequenceId=" + documentSequenceId +
                ", trackingNumber='" + trackingNumber + '\'' +
                ", trackingPassword='" + trackingPassword + '\'' +
                ", parent=" + parent +
                ", precedent=" + precedent +
                ", priority=" + priority +
                ", status=" + status +
                ", registries=" + registries +
                ", description='" + description + '\'' +
                ", registryEntries=" + registryEntries +
                '}';
    }

	public String getTitle()
	{
		return title;
	}

	public void setTitle( String title)
	{
		this.title= title;
	}

    /**
     * weryfikacja uprawnie� dost�pu do sprawy
     */
    public void hasPermission() throws EdmException {
        boolean canView = DSApi.context().hasPermission(DSPermission.SPRAWA_WSZYSTKIE_PODGLAD) || DSApi.context().isAdmin();
        if(!canView){
            DSUser user = DSApi.context().getDSUser();
            if (DSApi.context().hasPermission(DSPermission.SPRAWA_KOMORKA_PODGLAD))
            {
                DSDivision[] divisions = user.getDivisions();
                for (int i=0; i < divisions.length; i++)
                {
                    if (getDivisionGuid().equals(divisions[i].getGuid()))
                    {
                        canView = true;
                        break;
                    }
                    if (divisions[i].isPosition())
                    {
                        DSDivision parent = divisions[i].getParent();
                        if ((parent != null) && (getDivisionGuid().equals(parent.getGuid())))
                        {
                            canView = true;
                            break;
                        }
                    }
                }
            }
        }

        if (!canView)
            throw new EdmException("Brak uprawnie� do podgl�du tej sprawy");

    }

}
