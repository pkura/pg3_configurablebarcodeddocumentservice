package pl.compan.docusafe.core.office;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.StringManager;
import std.filter;
import std.fun;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * Nazwy wszystkich uprawnie� dotycz�cych kancelarii.
 * <p>
 * Nowe uprawnienia powinny by� instancjami klasy String, za� warto�ci sta�ych
 * powinny by� identyczne z nazwami zmiennych (aby unikn�� duplikuj�cych si�
 * nazw).
 * <p>
 * Komentarze opisuj�ce uprawnienia zostan� u�yte do wygenerowania cz�ci pliku
 * LocalStrings.properties zawieraj�cej opisy uprawnie�. Je�eli w komentarzu
 * musz� znale�� si� dodatkowe informacje nie b�d�ce cz�ci� opisu, opis nale�y
 * umie�ci� po tagu @desc.
 * <p>
 * Fragment pliku LocalStrings.properties mo�na wygenerowa� przy pomocy
 * narz�dzia javadoc i klasy {@link PermissionsDoclet} poleceniem <br/>
 * <code>
 * javadoc -docletpath ideabuild/
 *  -encoding iso-8859-2
 *  -doclet pl.compan.docusafe.core.office.PermissionsDoclet
 *  -sourcepath src/main/ pl.compan.docusafe.core.office
 * </code> kt�re wygeneruje plik LocalStrings_permissions.properties (ten plik
 * nie mo�e wcze�niej istnie�, inaczej PermissionsDoclet rzuci wyj�tek)
 * 
 * <p>
 * Problemy z niekt�rymi uprawnieniami.
 * </p>
 * <p>
 * Tworzenie nowego u�ytkownika - akcja NewUser i UserList powinny by� dost�pne
 * dla nie-administratora. Nowych u�ytkownik�w mo�e tworzy� administrator lub
 * kto� z uprawnieniem "NOWY_UZYTKOWNIK".
 * 
 * Tworzenie nowego u�ytkownika.
 * 
 * TODO: validatePermissions()
 * 
 * <p>
 * UWAGA: usuni�cie i dodawanie uprawnie� wymaga synchronizacji z plikiem
 * dspermissions.xml
 * 
 * @see PermissionsDoclet
 * 
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DSPermission.java,v 1.96 2010/08/17 08:29:19 pecet1 Exp $
 */
public class DSPermission implements Cloneable {
	private static final Log log = LogFactory.getLog(DSPermission.class);
	public static final StringManager sm = StringManager
			.getManager(Constants.Package);
	public static final StringManager smXML = GlobalPreferences
			.loadPropertiesFile("pl.compan.docusafe.core.office",
					"permissionsNames");

	private String name;
	private String description;
	private String module;
	private String moduleName;
	private Boolean active;
	private Category category;

	public DSPermission(String name) {
		this.name = name;
		this.active = true;
	}

	public String getName() {
		return name;
	}

	public String getModule() {
		return module;
	}

	public String getModuleName() {
		return moduleName;
	}

	public Category getCategory() {
		return category;
	}

	public String getDescription() {
		return smXML.getString(name);
	}

	private void setModule(String module) {
		this.module = module;
	}

	private void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	private void setCategory(Category category) {
		this.category = category;
	}

	private void setDescription(String description) {
		this.description = description;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof DSPermission))
			return false;

		final DSPermission dsPermission = (DSPermission) o;

		if (name != null ? !name.equals(dsPermission.name)
				: dsPermission.name != null)
			return false;

		return true;
	}

	public int hashCode() {
		return (name != null ? name.hashCode() : 0);
	}

	protected Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public static List<DSPermission> getAllPermissions() {
		return all;
	}

	/**
	 * Zwraca nazwy uprawnie� dost�pnych dla zainstalowanych modu��w.
	 */
	public static List<DSPermission> getAvailablePermissions() {
		return available;
	}

	/**
	 * Zwraca obiekt reprezentuj�cy uprawnienie o danej nazwie lub null.
	 */
	public static DSPermission getByName(String name) {
		return (DSPermission) byname.get(name);
	}

	public static final DSPermission WSZYSTKIE_UPRAWNIENIA = new DSPermission(
			"WSZYSTKIE_UPRAWNIENIA");

	public static final DSPermission RAPORTY_TWORZENIE = new DSPermission(
			"RAPORTY_TWORZENIE");
	
	public static final DSPermission DOSTEP_DO_SYSTEMU_RAPORTOWEGO = new DSPermission(
			"DOSTEP_DO_SYSTEMU_RAPORTOWEGO");	
	
	/**
	 * Zamykanie dnia.
	 */
	public static final DSPermission DZIEN_ZAMKNIECIE = new DSPermission(
			"DZIEN_ZAMKNIECIE");

	/**
	 * Otwarcie dowolnego dnia.
	 */
	public static final DSPermission DZIEN_DOWOLNY_OTWARCIE = new DSPermission(
			"DZIEN_DOWOLNY_OTWARCIE");

	/**
	 * Tworzenie nowego dokumentu w archiwum
	 */
	public static final DSPermission ARCHIWUM_NOWY_DOKUMENT = new DSPermission(
			"ARCHIWUM_NOWY_DOKUMENT");

	/**
	 * Zamkni�cie roku.
	 */
	public static final DSPermission ROK_ZAMKNIECIE = new DSPermission(
			"ROK_ZAMKNIECIE");

	/**
	 * Modyfikacje drzewa RWA.
	 */
	public static final DSPermission RWA_ZMIANA = new DSPermission("RWA_ZMIANA");
	/* *
	 * Dost�p do w�asnej listy zada�
	 */
	// public static final Permissions ZADANIA_PODGLAD = new
	// Permissions("ZADANIA_PODGLAD");
	/**
	 * Mo�liwo�� exportu danych do BIP
	 */
	public static final DSPermission BIP_EKSPORT = new DSPermission(
			"BIP_EKSPORT");
	/**
	 * Przegl�danie log�w operacji na pi�mie
	 */
	// public static final Permissions PISMO_HISTORIA_PODGLAD = new
	// Permissions("PISMO_HISTORIA_PODGLAD");
	/**
	 * Mo�liwo�� dowolnej dekretacji
	 */
	public static final DSPermission WWF_DEKRETACJA_DOWOLNA = new DSPermission(
			"WWF_DEKRETACJA_DOWOLNA");
	/**
	 * Mo�liwo�� dekretacji zbiorczej.
	 */
	public static final DSPermission WWF_DEKRETACJA_ZBIORCZA = new DSPermission(
			"WWF_DEKRETACJA_ZBIORCZA");
	/**
	 * Odbi�r dekretacji na dzia�.
	 */
	public static final DSPermission WWF_ODBIOR_DEKRETACJI_NA_DZIAL = new DSPermission(
			"WWF_ODBIOR_DEKRETACJI_NA_DZIAL");
	/**
	 * Przyj�cie pisma wchodz�cego w Kancelarii Og�lnej.
	 */
	public static final DSPermission PISMA_KO_PRZYJECIE = new DSPermission(
			"PISMA_KO_PRZYJECIE");
	/**
	 * Przyj�cie pisma wchodz�cego w Kancelarii Og�lnej.
	 */
	public static final DSPermission PISMA_KO_PRZYJECIE_SIMPLE = new DSPermission(
			"PISMA_KO_PRZYJECIE_SIMPLE");
	/**
	 * Odbi�r dokument�w PLAY.
	 */
	public static final DSPermission ODBIOR_DOKUMENTOW = new DSPermission(
			"ODBIOR_DOKUMENTOW");
	/**
	 * Przyj�cie pisma wchodz�cego w dowolnym dziale.
	 */
	public static final DSPermission PISMO_WSZEDZIE_PRZYJECIE = new DSPermission(
			"PISMO_WSZEDZIE_PRZYJECIE");
	/**
	 * Usuwanie dziennika
	 */
	public static final DSPermission DZIENNIK_USUWANIE = new DSPermission(
			"DZIENNIK_USUWANIE");
	/**
	 * Utworzenie nowego dziennika
	 */
	public static final DSPermission DZIENNIK_TWORZENIE = new DSPermission(
			"DZIENNIK_TWORZENIE");
	/**
	 * Podgl�d dziennik�w w ka�dej kom�rce.
	 */
	public static final DSPermission DZIENNIK_WSZYSTKIE_PODGLAD = new DSPermission(
			"DZIENNIK_WSZYSTKIE_PODGLAD");
	/**
	 * Podgl�d dziennik�w g��wnych.
	 */
	public static final DSPermission DZIENNIK_GLOWNY_PODGLAD = new DSPermission(
			"DZIENNIK_GLOWNY_PODGLAD");
	/**
	 * Podgl�d dziennik�w w dziale.
	 */
	public static final DSPermission DZIENNIK_KOMORKA_PODGLAD = new DSPermission(
			"DZIENNIK_KOMORKA_PODGLAD");
	/**
	 * Zmiana kolejnego numeru pisma w dzienniku.
	 */
	public static final DSPermission DZIENNIK_ZMIANA_NUMERU_KOLEJNEGO = new DSPermission(
			"DZIENNIK_ZMIANA_NUMERU_KOLEJNEGO");
	/**
	 * Przyjecie pisma w kom�rce organizacyjnej.
	 */
	public static final DSPermission PISMA_KOMORKA_PRZYJECIE = new DSPermission(
			"PISMA_KOMORKA_PRZYJECIE");
	/**
	 * Przyj�cie pisma w nadrz�dnej kom�rce organizacyjnej.
	 */
	public static final DSPermission PISMA_KOMORKA_NADRZEDNA_PRZYJECIE = new DSPermission(
			"PISMA_KOMORKA_NADRZEDNA_PRZYJECIE");
	/**
	 * Akceptacja czystopisu.
	 */
	public static final DSPermission PISMO_AKCEPTACJA_CZYSTOPISU = new DSPermission(
			"PISMO_AKCEPTACJA_CZYSTOPISU");
	/**
	 * Cofni�cie akceptacji czystopisu.
	 */
	public static final DSPermission PISMO_AKCEPTACJA_CZYSTOPISU_COFNIECIE = new DSPermission(
			"PISMO_AKCEPTACJA_CZYSTOPISU_COFNIECIE");

	public static final DSPermission LINK_DODANE_PRZEZ_UZYTKOWNIKA = new DSPermission(
			"LINK_DODANE_PRZEZ_UZYTKOWNIKA");

	/**
	 * Podgl�d pism w kom�rce organizacyjnej.
	 */
	public static final DSPermission PISMO_KOMORKA_PODGLAD = new DSPermission(
			"PISMO_KOMORKA_PODGLAD");
	/**
	 * Podgl�d pism w kom�rce organizacyjnej oraz w komurkach podrz�dnych.
	 */
	public static final DSPermission PISMO_KOMORKA_PODGLAD_W_PODWYDZIALACH = new DSPermission(
			"PISMO_KOMORKA_PODGLAD_W_PODWYDZIALACH");
	/**
	 * Podgl�d wszystkich pism.
	 */
	public static final DSPermission PISMO_WSZYSTKIE_PODGLAD = new DSPermission(
			"PISMO_WSZYSTKIE_PODGLAD");
	/**
	 * Podgl�d pism w kancelarii og�lnej.
	 */
	public static final DSPermission PISMO_KO_PODGLAD = new DSPermission(
			"PISMO_KO_PODGLAD");
	/**
	 * Edycja pism w kom�rce organizacyjnej.
	 */
	public static final DSPermission PISMO_KOMORKA_MODYFIKACJE = new DSPermission(
			"PISMO_KOMORKA_MODYFIKACJE");
	/**
	 * Edycja wszystkich pism.
	 */
	public static final DSPermission PISMO_WSZYSTKIE_MODYFIKACJE = new DSPermission(
			"PISMO_WSZYSTKIE_MODYFIKACJE");
	/**
	 * Edycja pism w kancelarii og�lnej.
	 */
	public static final DSPermission PISMO_KO_MODYFIKACJE = new DSPermission(
			"PISMO_KO_MODYFIKACJE");
	/**
	 * Tworzenie wielu pism wychodz�cych jednocze�nie.
	 */
	public static final DSPermission PISMO_WYCH_WIELE_ODPOWIEDZI = new DSPermission(
			"PISMO_WYCH_WIELE_ODPOWIEDZI");
	/**
	 * Dzielenia za��cznika (skanu) na za��czniki sk�adaj�ce si� tylko z jednej
	 * strony.
	 */
	public static final DSPermission PISMO_DZIELENIE_ZALACZNIKA = new DSPermission(
			"PISMO_DZIELENIE_ZALACZNIKA");
	/**
	 * Tworzenie pisma z za��cznika innego pisma.
	 */
	public static final DSPermission PISMO_TWORZENIE_PISMA_Z_ZALACZNIKOW = new DSPermission(
			"PISMO_TWORZENIE_PISMA_Z_ZALACZNIKOW");
	/**
	 * Twozrenie pisma wewnetrzenego
	 */
	public static final DSPermission PISMO_WEWNETRZNE_TWORZENIE = new DSPermission(
			"PISMO_WEWNETRZNE_TWORZENIE");
	/**
	 * Tworzenie pisma wychodzacego
	 */
	public static final DSPermission PISMO_WYCHODZACE_TWORZENIE = new DSPermission(
			"PISMO_WYCHODZACE_TWORZENIE");
	/**
	 * Tworzenie pisma wychodzacego
	 */
	public static final DSPermission PISMO_WYCHODZACE_TWORZENIE_SIMPLE = new DSPermission(
			"PISMO_WYCHODZACE_TWORZENIE_SIMPLE");
/**
 * Umo�liwia usuniecie calkowite dokuemntu z systemu 
 */
	public static final DSPermission DOKUMENT_PERMANENTLY_DELETE = new DSPermission(
			"DOKUMENT_PERMANENTLY_DELETE");
	/**
	 * Podglad faksow w poczekalni
	 */
	public static final DSPermission FAX_PODGLAD = new DSPermission(
			"FAX_PODGLAD");

	/**
	 * Zak�adanie nowej sprawy
	 */
	public static final DSPermission SPRAWA_TWORZENIE = new DSPermission(
			"SPRAWA_TWORZENIE");
	/**
	 * Zamykanie sprawy.
	 */
	public static final DSPermission SPRAWA_ZAMYKANIE = new DSPermission(
			"SPRAWA_ZAMYKANIE");
	/**
	 * Wyb�r alternatywnego referenta sprawy.
	 */
	public static final DSPermission SPRAWA_TWORZENIE_WYBOR_REFERENTA = new DSPermission(
			"SPRAWA_TWORZENIE_WYBOR_REFERENTA");
	/**
	 * Tworzenie sprawy o dowolnym symbolu
	 */
	public static final DSPermission SPRAWA_TWORZENIE_DOWOLNY_SYMBOL = new DSPermission(
			"SPRAWA_TWORZENIE_DOWOLNY_SYMBOL");
	/**
	 * Podgl�d zada� dowolnego u�ytkownika
	 */
	public static final DSPermission ZADANIA_WSZYSTKIE_PODGLAD = new DSPermission(
			"ZADANIA_WSZYSTKIE_PODGLAD");
	/**
	 * Podgl�d zada� u�ytkownik�w w swoim dziale
	 */
	public static final DSPermission ZADANIA_KOMORKA_PODGLAD = new DSPermission(
			"ZADANIA_KOMORKA_PODGLAD");
	/**
	 * Ko�czenie procesu dekretacji r�cznej.
	 */
	public static final DSPermission ZADANIE_KONCZENIE = new DSPermission(
			"ZADANIE_KONCZENIE");
	/**
	 * Wyznaczenie terminu zako�czenia zadania.
	 */
	public static final DSPermission PROCES_WYZNACZENIE_TERMINU = new DSPermission(
			"PROCES_WYZNACZENIE_TERMINU");
	/**
	 * Podgl�d spraw w swoim dziale.
	 */
	public static final DSPermission SPRAWA_KOMORKA_PODGLAD = new DSPermission(
			"SPRAWA_KOMORKA_PODGLAD");
	/**
	 * Podgl�d wszystkich spraw.
	 */
	public static final DSPermission SPRAWA_WSZYSTKIE_PODGLAD = new DSPermission(
			"SPRAWA_WSZYSTKIE_PODGLAD");
	/**
	 * Zmiana referenta pisma.
	 */
	public static final DSPermission PISMO_ZMIANA_REFERENTA = new DSPermission(
			"PISMO_ZMIANA_REFERENTA");
	/**
	 * Nadadanie numeru kancelaryjnego pismu wychodz�cemu.
	 */
	public static final DSPermission PISMO_WYCH_PRZYJECIE_KO = new DSPermission(
			"PISMO_WYCH_PRZYJECIE_KO");
	/**
	 * Usuwanie numeru kancelaryjnego pisma wychodz�cego.
	 */
	public static final DSPermission PISMO_WYCH_USUNIECIE_KO = new DSPermission(
			"PISMO_WYCH_USUNIECIE_KO");
	/**
	 * Widok pola dost�p na pi�mie wychodz�cym PgDocOutLogic oraz PgDocInLogic
	 */
	public static final DSPermission PISMO_WYCH_DOSTEP = new DSPermission(
			"PISMO_WYCH_DOSTEP");
	/**
	 * Usuwanie uwagi dodanej do pisma.
	 */
	public static final DSPermission PISMO_USUWANIE_UWAGI = new DSPermission(
			"PISMO_USUWANIE_UWAGI");
	/**
	 * Zmiana rodzaju pisma.
	 */
	public static final DSPermission PISMO_ZMIANA_RODZAJU = new DSPermission(
			"PISMO_ZMIANA_RODZAJU");
	/**
	 * Zmiana rodzaju dokumentu Docusafe
	 */
	public static final DSPermission DOKUMENT_DOCUSAFE_ZMIANA_RODZAJU = new DSPermission(
			"DOKUMENT_DOCUSAFE_ZMIANA_RODZAJU");
	/**
	 * Podpisanie pisma.
	 */
	public static final DSPermission PISMO_PODPISANIE = new DSPermission(
			"PISMO_PODPISANIE");
	/**
	 * Akceptacja pisma (fake podpisu bez karty)
	 */
	public static final DSPermission PISMO_AKCEPTACJA = new DSPermission(
			"PISMO_AKCEPTACJA");
	/**
	 * Zmiana referenta sprawy.
	 */
	public static final DSPermission SPRAWA_ZMIANA_REFERENTA = new DSPermission(
			"SPRAWA_ZMIANA_REFERENTA");
	/**
	 * Zmiana terminu zako�czenia sprawy
	 */
	public static final DSPermission SPRAWA_ZMIANA_TERMINU = new DSPermission(
			"SPRAWA_ZMIANA_TERMINU");
	/**
	 * Przesuni�cie sprawy do innej teczki
	 */
	public static final DSPermission SPRAWA_PRZESUWANIE_DO_INNEJ_TECZKI = new DSPermission(
			"SPRAWA_PRZESUWANIE_DO_INNEJ_TECZKI");
	/**
	 * Zmiana numeru sprawy
	 */
	public static final DSPermission SPRAWA_ZMIANA_NUMERU = new DSPermission(
			"SPRAWA_ZMIANA_NUMERU");
	/**
	 * Edycja numeru kolejnego sprawy w teczce.
	 * 
	 * @deprecated
	 */
	public static final DSPermission TECZKA_EDYCJA_NUMERU_SPRAWY = new DSPermission(
			"TECZKA_EDYCJA_NUMERU_SPRAWY");
	/**
	 * Tworzenie teczki w kom�rce.
	 */
	public static final DSPermission TECZKA_TWORZENIE_KOMORKA = new DSPermission(
			"TECZKA_TWORZENIE_KOMORKA");
	/**
	 * Tworzenie teczki w dowolnej kom�rce.
	 */
	public static final DSPermission TECZKA_TWORZENIE_WSZEDZIE = new DSPermission(
			"TECZKA_TWORZENIE_WSZEDZIE");
	/**
	 * Usuwanie teczki w kom�rce.
	 */
	public static final DSPermission TECZKA_USUWANIE_KOMORKA = new DSPermission(
			"TECZKA_USUWANIE_KOMORKA");
	/**
	 * Usuwanie teczki w dowolnej kom�rce.
	 */
	public static final DSPermission TECZKA_USUWANIE_WSZEDZIE = new DSPermission(
			"TECZKA_USUWANIE_WSZEDZIE");
	/**
	 * Zmiana referenta teczki w kom�rce.
	 */
	public static final DSPermission TECZKA_ZMIANA_REFERENTA_KOMORKA = new DSPermission(
			"TECZKA_ZMIANA_REFERENTA_KOMORKA");
	/**
	 * Zmiana referenta teczki w dowolnej kom�rce.
	 */
	public static final DSPermission TECZKA_ZMIANA_REFERENTA_WSZEDZIE = new DSPermission(
			"TECZKA_ZMIANA_REFERENTA_WSZEDZIE");
	/**
	 * Zmiana numeru teczki
	 */
	public static final DSPermission TECZKA_ZMIANA_NUMERU = new DSPermission(
			"TECZKA_ZMIANA_NUMERU");
	/**
	 * Tworzenie teczki w kom�rce.
	 */
	public static final DSPermission TECZKA_MODYFIKACJA_KOMORKA = new DSPermission(
			"TECZKA_MODYFIKACJA_KOMORKA");
	/**
	 * Tworzenie teczki w dowolnej kom�rce.
	 */
	public static final DSPermission TECZKA_MODYFIKACJA_WSZEDZIE = new DSPermission(
			"TECZKA_MODYFIKACJA_WSZEDZIE");
	/**
	 * Tworzenie teczki w dowolnej kom�rce.
	 */
	public static final DSPermission TECZKA_PODGLAD_WSZEDZIE = new DSPermission(
			"TECZKA_PODGLAD_WSZEDZIE");
	/**
	 * Tworzenie teczki w kom�rce.
	 */
	public static final DSPermission TECZKA_WYDRUK_KOMORKA = new DSPermission(
			"TECZKA_WYDRUK_KOMORKA");
	/**
	 * Tworzenie teczki w dowolnej kom�rce.
	 */
	public static final DSPermission TECZKA_WYDRUK_WSZEDZIE = new DSPermission(
			"TECZKA_WYDRUK_WSZEDZIE");
	/**
	 * Przenoszenie (przepisywanie) teczek na nowy rok z poziomu edycji teczki.
	 */
	public static final DSPermission TECZKA_PRZEPISYWANIE_NA_NOWY_ROK = new DSPermission(
			"TECZKA_PRZEPISYWANIE_NA_NOWY_ROK");
	/**
	 * Okre�lanie zast�pstw.
	 */
	public static final DSPermission ZASTEPSTWA_EDYCJA = new DSPermission(
			"ZASTEPSTWA_EDYCJA");

	/**
	 * Okre�lanie zast�pstw, z pomini�ciem ograniczenia do swoich dzia�u:
	 * substitution.only.my.division = true
	 */
	public static final DSPermission ZASTEPSTWA_EDYCJA_WSZYSTKICH = new DSPermission(
			"ZASTEPSTWA_EDYCJA_WSZYSTKICH");
	/**
	 * Uzupe�nianie s�ownika nadawc�w/odbiorc�w pism.
	 */
	public static final DSPermission SLOWNIK_OSOB_DODAWANIE = new DSPermission(
			"SLOWNIK_OSOB_DODAWANIE");
	/**
	 * Usuwanie pozycji z s�ownika nadawc�w/odbiorc�w pism.
	 */
	public static final DSPermission SLOWNIK_OSOB_USUWANIE = new DSPermission(
			"SLOWNIK_OSOB_USUWANIE");
	/**
	 * Podglad liczby zada� u�ytkownik�w w dziale.
	 */
	public static final DSPermission PODGLAD_LICZBY_ZADAN_KOMORKA = new DSPermission(
			"PODGLAD_LICZBY_ZADAN_KOMORKA");
	/**
	 * Podglad liczby zada� dowolnego u�ytkownika.
	 */
	public static final DSPermission PODGLAD_LICZBY_ZADAN_WSZEDZIE = new DSPermission(
			"PODGLAD_LICZBY_ZADAN_WSZEDZIE");

	public static final DSPermission BOK_PRZYJECIE_PISMA = new DSPermission(
			"BOK_PRZYJECIE_PISMA");
	public static final DSPermission BOK_LISTA_PISM = new DSPermission(
			"BOK_LISTA_PISM");

	public static final DSPermission PUDLO_ARCH_WSZYSTKO = new DSPermission(
			"PUDLO_ARCH_WSZYSTKO");

	/**
	 * Szukanie dost�pu do pism - u�ytkownicy w kom�rce.
	 */
	public static final DSPermission SZUK_UZYTK_DOSTEP_KOMORKA = new DSPermission(
			"SZUK_UZYTK_DOSTEP_KOMORKA");
	/**
	 * Szukanie dost�pu do pism - wszyscy u�ytkownicy.
	 */
	public static final DSPermission SZUK_UZYTK_DOSTEP_WSZEDZIE = new DSPermission(
			"SZUK_UZYTK_DOSTEP_WSZEDZIE");

	/**
	 * Dost�p do poczekalni email
	 */
	public static final DSPermission MAIL_STAGGINGAREA = new DSPermission(
			"MAIL_STAGGINGAREA");

	/**
	 * Dost�p do modu�u Dokumenty.
	 */
	public static final DSPermission ARCHIWUM_DOSTEP = new DSPermission(
			"ARCHIWUM_DOSTEP");
	/**
	 * Blokada dodania nowego dokumentu w archiwum
	 */
	public static final DSPermission ARCHIWUM_NOWY_DOKUMENT_BLOCK = new DSPermission(
			"ARCHIWUM_NOWY_DOKUMENT_BLOCK");

	public static final DSPermission ETYKIETY_SYSTEM_DOSTEP = new DSPermission(
			"ETYKIETY_SYSTEM_DOSTEP");

	// zamawianie dokumentow
	public static final DSPermission ZAMAWIANIE_DOKUMENTOW = new DSPermission(
			"ZAMAWIANIE_DOKUMENTOW");
	public static final DSPermission ZAMAWIANIE_PUDEL = new DSPermission(
			"ZAMAWIANIE_PUDEL");
	public static final DSPermission ZAMAWIANIE_ZARZADZANIE = new DSPermission(
			"ZAMAWIANIE_ZARZADZANIE");

	/* EDG */

	public static final DSPermission EDG_PRZEDS_PRZEGLAD = new DSPermission(
			"EDG_PRZEDS_PRZEGLAD");
	public static final DSPermission EDG_PRZEDS_TWORZENIE = new DSPermission(
			"EDG_PRZEDS_TWORZENIE");
	public static final DSPermission EDG_PRZEDS_EDYCJA = new DSPermission(
			"EDG_PRZEDS_EDYCJA");
	public static final DSPermission EDG_PRZEDS_ZAWIESZENIE = new DSPermission(
			"EDG_PRZEDS_ZAWIESZENIE");
	public static final DSPermission EDG_PRZEDS_RAPORTY = new DSPermission(
			"EDG_PRZEDS_RAPORTY");

	/* Invoices */

	public static final DSPermission INV_WYSZUKIWANIE = new DSPermission(
			"INV_WYSZUKIWANIE");
	public static final DSPermission INV_WYSZUKIWANIE_SWOICH = new DSPermission(
			"INV_WYSZUKIWANIE_SWOICH");
	public static final DSPermission INV_WYSZUKIWANIE_SWOICH_DO_AKCEPTACJI = new DSPermission(
			"INV_WYSZUKIWANIE_SWOICH_DO_AKCEPTACJI");
	public static final DSPermission INV_WYSZUKIWANIE_Z_DZIALU = new DSPermission(
			"INV_WYSZUKIWANIE_Z_DZIALU");
	public static final DSPermission INV_WYSZUKIWANIE_Z_DZIALU_DO_AKCEPTACJI = new DSPermission(
			"INV_WYSZUKIWANIE_Z_DZIALU_DO_AKCEPTACJI");

	public static final DSPermission INV_PODGLAD_FAKTURY = new DSPermission(
			"INV_PODGLAD_FAKTURY");
	public static final DSPermission INV_PODGLAD_SWOICH_FAKTUR = new DSPermission(
			"INV_PODGLAD_SWOICH_FAKTUR");
	public static final DSPermission INV_PODGLAD_SWOICH_FAKTUR_DO_AKCEPTACJI = new DSPermission(
			"INV_PODGLAD_SWOICH_FAKTUR_DO_AKCEPTACJI");
	public static final DSPermission INV_PODGLAD_FAKTUR_Z_DZIALU = new DSPermission(
			"INV_PODGLAD_FAKTUR_Z_DZIALU");
	public static final DSPermission INV_PODGLAD_Z_DZIALU_DO_AKCEPTACJI = new DSPermission(
			"INV_PODGLAD_Z_DZIALU_DO_AKCEPTACJI");

	public static final DSPermission INV_MODYFIKACJE = new DSPermission(
			"INV_MODYFIKACJE");
	public static final DSPermission INV_MODYFIKACJE_SWOICH = new DSPermission(
			"INV_MODYFIKACJE_SWOICH");
	public static final DSPermission INV_MODYFIKACJE_SWOICH_DO_AKCEPTACJI = new DSPermission(
			"INV_MODYFIKACJE_SWOICH_DO_AKCEPTACJI");
	public static final DSPermission INV_MODYFIKACJE_Z_DZIALU = new DSPermission(
			"INV_MODYFIKACJE_Z_DZIALU");
	public static final DSPermission INV_MODYFIKACJA_Z_DZIALU_DO_AKCEPTACJI = new DSPermission(
			"INV_MODYFIKACJA_Z_DZIALU_DO_AKCEPTACJI");

	public static final DSPermission INV_DODAWANIE = new DSPermission(
			"INV_DODAWANIE");
	public static final DSPermission INV_IMPORT_WYCHODZACYCH = new DSPermission(
			"INV_IMPORT_WYCHODZACYCH");

	/* Contracts */

	public static final DSPermission CON_WYSZUKIWANIE = new DSPermission(
			"CON_WYSZUKIWANIE");
	public static final DSPermission CON_WYSZUKIWANIE_Z_DZIALU = new DSPermission(
			"CON_WYSZUKIWANIE_Z_DZIALU");
	public static final DSPermission CON_WYSZUKIWANIE_SWOICH = new DSPermission(
			"CON_WYSZUKIWANIE_SWOICH");
	public static final DSPermission CON_PODGLAD = new DSPermission(
			"CON_PODGLAD");
	public static final DSPermission CON_PODGLAD_Z_DZIALU = new DSPermission(
			"CON_PODGLAD_Z_DZIALU");
	public static final DSPermission CON_PODGLAD_SWOICH = new DSPermission(
			"CON_PODGLAD_SWOICH");
	public static final DSPermission CON_DODAWANIE = new DSPermission(
			"CON_DODAWANIE");
	public static final DSPermission CON_MODYFIKACJE = new DSPermission(
			"CON_MODYFIKACJE");
	public static final DSPermission CON_MODYFIKACJE_Z_DZIALU = new DSPermission(
			"CON_MODYFIKACJE_Z_DZIALU");
	public static final DSPermission CON_MODYFIKACJE_SWOICH = new DSPermission(
			"CON_MODYFIKACJE_SWOICH");

	/* Construction */
	/** Dost�p ca�kowity do modu�u Architektura. */
	public static final DSPermission CNS_DOSTEP_OGOLNY = new DSPermission(
			"CNS_DOSTEP_OGOLNY");

	/* Nationwide */

	public static final DSPermission NW_WPIS_RS = new DSPermission("NW_WPIS_RS");
	public static final DSPermission NW_KOLEJNOSC_KOMPILACJI_ZALACZNIKOW = new DSPermission(
			"NW_KOLEJNOSC_KOMPILACJI_ZALACZNIKOW");
	public static final DSPermission NW_KOMPILACJA_ZALACZNIKOW_Z_WYSZUKIWARKI = new DSPermission(
			"NW_KOMPILACJA_ZALACZNIKOW_Z_WYSZUKIWARKI");
	public static final DSPermission NW_QC = new DSPermission("NW_QC");

	/* Archiwum agenta */
	public static final DSPermission DAA_SLOWNIK_AGENTA_DODAWANIE = new DSPermission(
			"DAA_SLOWNIK_AGENTA_DODAWANIE");
	public static final DSPermission DAA_SLOWNIK_AGENTA_USUWANIE = new DSPermission(
			"DAA_SLOWNIK_AGENTA_USUWANIE");
	public static final DSPermission DAA_SLOWNIK_AGENTA_ODCZYTYWANIE = new DSPermission(
			"DAA_SLOWNIK_AGENTA_ODCZYTYWANIE");

	/* Dokumenty Czlonkowski */
	public static final DSPermission DC_SLOWNIK_DODAWANIE = new DSPermission(
			"DC_SLOWNIK_DODAWANIE");
	public static final DSPermission DC_SLOWNIK_USUWANIE = new DSPermission(
			"DC_SLOWNIK_USUWANIE");
	public static final DSPermission DC_SLOWNIK_ODCZYTYWANIE = new DSPermission(
			"DC_SLOWNIK_ODCZYTYWANIE");

	/* Uprawnienia do kalendarza */
	public static final DSPermission KALENDARZ_TERMINY_WSZYSTKIE = new DSPermission(
			"KALENDARZ_TERMINY_WSZYSTKIE");

	/** Uprawnienie do wyboru kolumn na li�cie zada� */
	public static final DSPermission WYBOR_KOLUMN_NA_LISCIE_ZADAN = new DSPermission(
			"WYBOR_KOLUMN_NA_LISCIE_ZADAN");

	/** Uprawnienie do przywracania zako�czonych zada� */
	public static final DSPermission PRZYWRACANIE_NA_LISTE_ZADAN = new DSPermission(
			"PRZYWRACANIE_NA_LISTE_ZADAN");

	/* Uprawnienia do podgl�du i modyfikacji kolejek (kana��w) */
	public static final DSPermission KANALY_PODGLAD = new DSPermission(
			"KANALY_PODGLAD");
	public static final DSPermission KANALY_MODYFIKACJA = new DSPermission(
			"KANALY_MODYFIKACJA");

	/* Uprawnienia do archiwum dokument�w prawnych */
	public static final DSPermission DP_SLOWNIK_DODAWANIE = new DSPermission(
			"DP_SLOWNIK_DODAWANIE");
	public static final DSPermission DP_SLOWNIK_USUWANIE = new DSPermission(
			"DP_SLOWNIK_USUWANIE");
	public static final DSPermission DP_SLOWNIK_ODCZYTYWANIE = new DSPermission(
			"DP_SLOWNIK_ODCZYTYWANIE");
	public static final DSPermission DP_SLOWNIK_MODYFIKACJA = new DSPermission(
			"DP_SLOWNIK_MODYFIKACJA");

	/* Uprawnienia do archiwum dokument�w finansowych */
	public static final DSPermission DF_SLOWNIK_DODAWANIE = new DSPermission(
			"DF_SLOWNIK_DODAWANIE");
	public static final DSPermission DF_SLOWNIK_USUWANIE = new DSPermission(
			"DF_SLOWNIK_USUWANIE");
	public static final DSPermission DF_SLOWNIK_ODCZYTYWANIE = new DSPermission(
			"DF_SLOWNIK_ODCZYTYWANIE");
	public static final DSPermission DF_SLOWNIK_MODYFIKACJA = new DSPermission(
			"DF_SLOWNIK_MODYFIKACJA");

	/** Modyfikowanie zaakceptowanych faktur */
	public static final DSPermission INVOICE_MODIFY_ACCEPTED = new DSPermission(
			"INVOICE_MODIFY_ACCEPTED");
	/** Wycofywanie faktur */
	public static final DSPermission INVOICE_WITHDRAW = new DSPermission(
			"INVOICE_WITHDRAW");
	/** Odrzucanie akceptacji dla wybranych centr�w kosztowych */
	public static final DSPermission INVOICE_REJECT_OBJECT_ACCEPTANCE = new DSPermission(
			"INVOICE_REJECT_OBJECT_ACCEPTANCE");
	/** Edycja kwoty przypisanej do centrum kosztowego */
	public static final DSPermission INVOICE_CHANGE_COST_CENTER_AMOUNT = new DSPermission(
			"INVOICE_CHANGE_COST_CENTER_AMOUNT");

	/** B#1474 - Mo�liwo�� wybrania dw�ch kolumn na li�cie zada� */
	public static final DSPermission INVOICE_TASKLIST_COLUMNS = new DSPermission(
			"INVOICE_TASKLIST_COLUMNS");
	public static final DSPermission TASK_COUNT = new DSPermission("TASK_COUNT");

	/** Tworzenie dokumentu leasingowego */
	public static final DSPermission DL_SLOWNIK_DODAWANIE = new DSPermission(
			"DL_SLOWNIK_DODAWANIE");
	/** Usuwanie dokumentu leasingowego */
	public static final DSPermission DL_SLOWNIK_USUWANIE = new DSPermission(
			"DL_SLOWNIK_USUWANIE");
	/** Podgl�d dokumentu leasingowego */
	public static final DSPermission DL_SLOWNIK_ODCZYTYWANIE = new DSPermission(
			"DL_SLOWNIK_ODCZYTYWANIE");
	/** Edytowanie dokumentu leasingowego */
	public static final DSPermission DL_SLOWNIK_EDYTOWANIE = new DSPermission(
			"DL_SLOWNIK_EDYTOWANIE");
	/** Dostep do CRM */
	public static final DSPermission CRM = new DSPermission("CRM");
	/** Dostep do Nowej teczki */
	public static final DSPermission NEW_PORTFOLIO = new DSPermission(
			"NEW_PORTFOLIO");

	/** Podgl�d kontrahent�w w regionie */
	public static final DSPermission CONTRACTOR_REGION = new DSPermission(
			"CONTRACTOR_REGION");
	/** Podgl�d wszystkich kontrahent�w */
	public static final DSPermission CONTRACTOR_ALL = new DSPermission(
			"CONTRACTOR_ALL");
	/** Podgl�d swoich kontrahent�w */
	public static final DSPermission CONTRACTOR_USER = new DSPermission(
			"CONTRACTOR_USER");

	/** Tworzenie dokumentu rockwell */
	public static final DSPermission ROCKWELL_SLOWNIK_DODAWANIE = new DSPermission(
			"ROCKWELL_SLOWNIK_DODAWANIE");
	/** Usuwanie dokumentu rockwell */
	public static final DSPermission ROCKWELL_SLOWNIK_USUWANIE = new DSPermission(
			"ROCKWELL_SLOWNIK_USUWANIE");
	/** Podgl�d dokumentu rockwell */
	public static final DSPermission ROCKWELL_SLOWNIK_ODCZYTYWANIE = new DSPermission(
			"ROCKWELL_SLOWNIK_ODCZYTYWANIE");
	/** Edytowanie dokumentu rockwell */
	public static final DSPermission ROCKWELL_SLOWNIK_EDYTOWANIE = new DSPermission(
			"ROCKWELL_SLOWNIK_EDYTOWANIE");

	/** Wnioski urlopowe */
	public static final DSPermission AR_MENAGMENT = new DSPermission(
			"AR_MENAGMENT");

	public static final DSPermission CRYPTO_ADMINS = new DSPermission(
			"CRYPTO_ADMINS");

	public static final DSPermission KONTAKTY_EDYCJA_WLASNEGO = new DSPermission(
			"KONTAKTY_EDYCJA_WLASNEGO");
	public static final DSPermission KONTAKTY_EDYCJA_W_DZIALE = new DSPermission(
			"KONTAKTY_EDYCJA_W_DZIALE");
	public static final DSPermission KONTAKTY_EDYCJA_WSZYSTKICH = new DSPermission(
			"KONTAKTY_EDYCJA_WSZYSTKICH");

	public static final DSPermission NEWS_EDYCJA_TYPOW = new DSPermission(
			"NEWS_EDYCJA_TYPOW");
	public static final DSPermission NEWS_DODAWANIE = new DSPermission(
			"NEWS_DODAWANIE");

	public static final DSPermission DURABLES_CREATE = new DSPermission(
			"DURABLES_CREATE");

	public static final DSPermission PROJECT_MODIFY = new DSPermission(
			"PROJECT_MODIFY");
	public static final DSPermission PROJECT_READ = new DSPermission(
			"PROJECT_READ");
	public static final DSPermission PROJECT_READ_SALARY = new DSPermission(
			"PROJECT_READ_SALARY");

	public static final DSPermission TICKET_READ = new DSPermission(
			"TICKET_READ");
	public static final DSPermission TICKET_MODIFY = new DSPermission(
			"TICKET_MODIFY");

	/** Uprawnienie do otrzymywania powiadomie� o op�nieniu w realizacji zada� */
	public static final DSPermission POWIADOMIENIA_OPOZNIONE_ZADANIE = new DSPermission(
			"POWIADOMIENIA_OPOZNIONE_ZADANIE");

	/** Uprawnienie do otrzymywania powiadomie� z serwisu OfficeMailer */
	public static final DSPermission OFFICE_MAILER_POWIADOMIENIA = new DSPermission(
			"OFFICE_MAILER_POWIADOMIENIA");

	/** Pozwolenia dla slownik�w dokument�w workflow dla aegonu */
	public static final DSPermission WF_READ_DICTIONARY = new DSPermission(
			"WF_READ_DICTIONARY");
	public static final DSPermission WF_ADMIN_DICTIONARY = new DSPermission(
			"WF_ADMIN_DICTIONARY");

	/**
	 * IMA
	 */
	// Tworzenie karty mediatora
	public static final DSPermission IMA_DOKUMENTY_KARTA_MEDIATORA = new DSPermission(
			"IMA_DOKUMENTY_KARTA_MEDIATORA");
	// Tworzenie nowego dokumentu w sprawie
	public static final DSPermission IMA_DOKUMENTY_DOKUMENT_W_SPRAWIE = new DSPermission(
			"IMA_DOKUMENTY_DOKUMENT_W_SPRAWIE");
	// Tworzenie oceny ankiety
	public static final DSPermission IMA_DOKUMENTY_OCENA_ANKIETY = new DSPermission(
			"IMA_DOKUMENTY_OCENA_ANKIETY");
	// Tworzenie nowej sprawy
	public static final DSPermission IMA_DOKUMENTY_SPRAWA = new DSPermission(
			"IMA_DOKUMENTY_SPRAWA");
	// Tworzenie szablonu dokumentu
	public static final DSPermission IMA_DOKUMENTY_SZABLON = new DSPermission(
			"IMA_DOKUMENTY_SZABLON");
	// Tworzenie zadania nie zwiazanego z dokumentem
	public static final DSPermission IMA_DOKUMENTY_ZADANIE = new DSPermission(
			"IMA_DOKUMENTY_ZADANIE");
	// Wyswietlenie zakladki archiwum
	public static final DSPermission ARCHIWUM_DOKUMENTOW = new DSPermission(
			"ARCHIWUM_DOKUMENTOW");
	public static final DSPermission EDYCJA_ZARCHIWIZOWANYCH_DOKUMENTOW = new DSPermission(
			"EDYCJA_ZARCHIWIZOWANYCH_DOKUMENTOW");
	// Wyswietlenie zakladki lista zadan
	public static final DSPermission LISTA_ZADAN = new DSPermission(
			"LISTA_ZADAN");
	// Wyswietlenie zakladki zarzadzanie sprawa
	public static final DSPermission ZARZADZANIE_SPRAWA = new DSPermission(
			"ZARZADZANIE_SPRAWA");

	/* bialystok */
	/** archiwum / bialystok */
	public static final DSPermission BRAKOWANIE_DOKUMENTOW = new DSPermission(
			"BRAKOWANIE_DOKUMENTOW");

	/* archiwum administracja */
	public static final DSPermission ADMINISTRACJA_WYKAZ_DOC = new DSPermission(
			"ADMINISTRACJA_WYKAZ_DOC");
	public static final DSPermission ADMINISTRACJA_PROTOKOL_BR = new DSPermission(
			"ADMINISTRACJA_PROTOKOL_BR");

	// wszystkie sprawy w podwydzia�ach danech uzytkownika bez wzg�edu na to czy
	// jest ich referentem
	public static final DSPermission PODGLAD_WSZYSTKICH_SPRAW_W_PODWYDZIALACH = new DSPermission(
			"PODGLAD_WSZYSTKICH_SPRAW_W_PODWYDZIALACH");
	// wszystkie teczki w podwydzia�ach bez wzg�edu na to czy jest ich
	// referentem
	public static final DSPermission PODGLAD_WSZYSTKICH_TECZEK_W_PODWYDZIALACH = new DSPermission(
			"PODGLAD_WSZYSTKICH_TECZEK_W_PODWYDZIALACH");

	public static final DSPermission DODAWANIE_PISM_DO_SPRAW_W_DZIALE = new DSPermission(
			"DODAWANIE_PISM_DO_SPRAW_W_DZIALE");

	/* GLOBALNE METODY UPRAWNIENIA */
	public static final DSPermission DOKUMENT_PRZENIES_DO_ARCHIWUM_GLOBALNA = new DSPermission(
			"DOKUMENT_PRZENIES_DO_ARCHIWUM_GLOBALNA");
	public static final DSPermission DOKUMENT_USUN_GLOBALNA = new DSPermission(
			"DOKUMENT_USUN_GLOBALNA");
	public static final DSPermission WYSZ_WSZYSTKICH_PISM = new DSPermission(
			"WYSZ_WSZYSTKICH_PISM");

	/* Uprawienia werjonowania */
	public static final DSPermission WERSJONOWANIE_UTWORZ_NOWA_WERSJE = new DSPermission(
			"WERSJONOWANIE_UTWORZ_NOWA_WERSJE");

	/* Oznaczenie dokumentu jako czystopis */
	public static final DSPermission OZNACZ_JAKO_CZYSTOPIS = new DSPermission(
			"OZNACZ_JAKO_CZYSTOPIS");

    /* Edycja pisma spoza listy zada� */
    public static final DSPermission EDYCJA_PISMA_SPOZA_LISTY_ZADAN = new DSPermission(
            "EDYCJA_PISMA_SPOZA_LISTY_ZADAN");
	
	/* Uprawnienie do recznego przekazania dokumentacji do CAP*/
	public static final DSPermission PRZEKAZ_DO_CAP = new DSPermission(
			"PRZEKAZ_DO_CAP");
	public static final DSPermission ADD_BARCODE_PACKAGE = new DSPermission(
			"ADD_BARCODE_PACKAGE");

    public static final DSPermission TEMPLATES = new DSPermission("TEMPLATES");
	
	/* Uprawnienia do powiadomie�*/
	public static final DSPermission POWIADOMIENIA = new DSPermission(
			"POWIADOMIENIA");
	public static final DSPermission DEFINIWANIE_POWIADOMIEN_DO_UZYTKOWNIKOW = new DSPermission(
			"DEFINIWANIE_POWIADOMIEN_DO_UZYTKOWNIKOW");
	
	/* Uprawnienie do masowego zwrotu dokument�w w kancelarii*/
	public static final DSPermission RETURN_DOCS = new DSPermission(
			"RETURN_DOCS");

    public static final DSPermission CRM_USER = new DSPermission("CRM_USER");
	
	/**
	 * Niemodyfikowalna lista wszystkich obiekt�w Permissions zadeklarowanych
	 * jako pola tej klasy. Kolekcja tworzona po za�adowaniu klasy (clinit).
	 */

	private static List<DSPermission> all;
	private static List<DSPermission> available;
	private static Set<String> availablePermissionNameSet;
	/**
	 * Nazwa uprawnienia -> DSPermission
	 */
	private static Map<String, DSPermission> byname;

	/**
	 * Zwraca list� obiekt�w Category w kolejno�ci, w jakiej kategorie
	 * wymieniono w pliku dspermissions.xml
	 */
	public static List<Category> categories() {
		return Category.list();
	}

	public static class Category {

		private static final Map<String, Category> categories = new LinkedHashMap<String, Category>();

		private String sid;
		private String name;

		private static void add(Category cat) {
			categories.put(cat.getSid(), cat);
		}

		private static List<Category> list() {
			return new ArrayList<Category>(categories.values());
		}

		private static Category get(String sid) {
			return (Category) categories.get(sid);
		}

		private Category(String sid, String name) {
			this.sid = sid;
			this.name = name;
		}

		public List permissions() {
			return fun.filter(getAvailablePermissions(),
					new filter<DSPermission>() {
						public boolean accept(DSPermission perm) {
							return perm.getCategory().equals(Category.this);
						}
					});
		}

		public String getSid() {
			return sid;
		}

		public String getName() {
			return smXML.getString(sid);
		}

		public String toString() {
			return "Category[" + name + "]";
		}

		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (!(o instanceof Category))
				return false;

			final Category category = (Category) o;

			if (sid != null ? !sid.equals(category.sid) : category.sid != null)
				return false;

			return true;
		}

		public int hashCode() {
			return (sid != null ? sid.hashCode() : 0);
		}
	}

	public static void init() throws EdmException {
		if (log.isDebugEnabled())
			log.debug("init()");

		Field[] fields = DSPermission.class.getDeclaredFields();
		List<DSPermission> perms = new ArrayList<DSPermission>(fields.length);
		Map<String, DSPermission> _byname = new HashMap<String, DSPermission>();
		try {
			for (int i = 0; i < fields.length; i++) {
				if (fields[i].getType().getName()
						.equals(DSPermission.class.getName())
						&& (fields[i].getModifiers() & Modifier.STATIC) > 0
						&& (fields[i].getModifiers() & Modifier.PUBLIC) > 0) {
					DSPermission perm = (DSPermission) fields[i].get(null);

					// nigdy nie nale�y u�ywa� oryginalnych obiekt�w
					// DSPermission,
					// poniewa� czasem klasy nie s� w�a�ciwie prze�adowywane i
					// wyst�puje
					// b��d "uprawnienie jest w wi�cej ni� jednej kategorii",
					// gdy
					// pole DSPermission.category != null
					perm = (DSPermission) perm.clone();

					perms.add(perm);
					_byname.put(perm.getName(), perm);
				}
			}
		} catch (IllegalAccessException e) {
			throw new EdmException(e.getMessage(), e);
		}

		all = Collections.unmodifiableList(perms);
		byname = Collections.unmodifiableMap(_byname);

		if (log.isDebugEnabled()) {
			log.debug("init(): all=" + all);
			log.debug("init(): byname=" + byname);
		}

		org.dom4j.io.SAXReader reader = new org.dom4j.io.SAXReader();
		try {
			DSApi.openAdmin();
			Document document = reader.read(DSPermission.class
					.getResourceAsStream("dspermissions.xml"));
			Element el = document.getRootElement();

			// kategorie
			Iterator categories = el.element("categories").elementIterator(
					"category");
			while (categories.hasNext()) {
				Element cat = (Element) categories.next();

				if (log.isDebugEnabled())
					log.debug("  nowa kategoria = " + cat);

				Category.add(new Category(cat.attributeValue("id"), cat
						.attributeValue("name")));

			}

			if (log.isDebugEnabled())
				log.debug("Category.list() = " + Category.list());

			Iterator modulePermissionsIterator = el
					.elementIterator("module-permissions");
			while (modulePermissionsIterator.hasNext()) {
				Element elModulePermissions = (Element) modulePermissionsIterator
						.next();
				String module = elModulePermissions.attributeValue("module");
				String moduleName = elModulePermissions.attributeValue("name");
				if (!Modules.modulePossible(module))
					throw new EdmException("Niepoprawna nazwa modu�u: "
							+ module);

				Iterator categoryIterator = elModulePermissions
						.elementIterator("category");
				while (categoryIterator.hasNext()) {
					Element elCategory = (Element) categoryIterator.next();
					boolean isCatEnabled = isEnabled(elCategory);

					Category category = Category.get(elCategory
							.attributeValue("ref"));
					if (category == null)
						throw new EdmException(
								"Nieznany identyfikator kategorii: "
										+ elCategory.attributeValue("ref"));

					Iterator permissionsIterator = elCategory
							.elementIterator("permission");
					while (permissionsIterator.hasNext()) {
						Element elPermission = (Element) permissionsIterator
								.next();

						boolean isPermEnabled = isEnabled(elPermission);
						String id = elPermission.attributeValue("id");
						String description = elPermission
								.attributeValue("name");

						DSPermission permission = getByName(id);
						if (permission == null)
							throw new EdmException("Nieznane uprawnienie: "
									+ id);

						if (permission.getCategory() != null)
							throw new EdmException("Uprawnienie " + id
									+ " znajduje si� w dw�ch kategoriach");

						permission.setCategory(category);
						permission.setModule(module);
						permission.setModuleName(moduleName);
						permission.setDescription(description);
						permission.setActive(isCatEnabled && isPermEnabled);

						if (log.isDebugEnabled()) {
							log.debug(id + "=>" + description);
							log.debug("  uprawnienie " + permission + " w "
									+ category);
						}
					}
				}
			}

			available = new ArrayList<DSPermission>(all.size());

			availablePermissionNameSet = new HashSet<String>();

			for (Iterator iter = getAllPermissions().iterator(); iter.hasNext();) {
				DSPermission permission = (DSPermission) iter.next();
				if (permission.getCategory() == null)
					throw new EdmException("Dla uprawnienia "
							+ permission.getName() + " nie okre�lono kategorii");
				if (permission.getModule() == null)
					throw new EdmException("Dla uprawnienia "
							+ permission.getName() + " nie okre�lono modu�u");

				if (Docusafe.moduleAvailable(permission.getModule())
						|| AvailabilityManager
								.isAvailable(("DSPermission." + permission
										.getName()))
						|| AvailabilityManager
								.isAvailable(("DSPermission.module." + permission
										.getModule()))) {
					if (permission.getActive()) {
						available.add(permission);
						availablePermissionNameSet.add(permission.name);
						if (log.isDebugEnabled())
							log.debug("  dodawanie " + permission
									+ " do dost�pnych");
					}
				} else {
					if (log.isDebugEnabled())
						log.debug("  uprawnienie " + permission
								+ " jest niedost�pne, wymaga "
								+ permission.getModule());
				}
			}

			if (log.isDebugEnabled())
				log.debug("init(): available=" + available);
		} catch (DocumentException e) {
			throw new EdmException(e.getMessage(), e);
		} finally {
			DSApi._close();
		}
	}

	private static boolean isEnabled(Element el) throws EdmException {
		// sprawdzam zaleznosc od dockindow
		String c = el.attributeValue("dockind");
		boolean enabled = false;

		String elementCn = el.attributeValue("id");
		if (elementCn == null)
			elementCn = el.attributeValue("ref");
		Boolean isEnabled = AvailabilityManager.getAvailableMap().get(
				"DSPermission." + elementCn);
		// jesli jest available to wygrywa ponad wszystko
		if (isEnabled != null) {
			return isEnabled;
		}

		String[] conds;
		if (c != null) {
			conds = c.split(",");
			for (String cond : conds) {
				cond = cond.trim();
				boolean positive = true;
				boolean partEnabled = true;
				if (cond.startsWith("!")) {
					positive = false;
					cond = cond.substring(1);
				}

				if ("administracja".equalsIgnoreCase(cond)) {
					partEnabled = (!Docusafe.isAvailableExtras("business")) == positive;
				} else {
					partEnabled = DocumentKind.isAvailable(cond) == positive;
				}

				enabled = enabled || partEnabled;
			}
		} else
			enabled = true;

		return enabled;
	}

	public String toString() {
		return "DSPermission[" + name + "]";
	}

	public boolean hasPermission(String username) throws EdmException {
		PermissionCache cache = (PermissionCache) ServiceManager
				.getService(PermissionCache.NAME);
		return cache.hasPermission(username, this.getName());
	}

	public boolean hasPermission() throws EdmException {
		return hasPermission(DSApi.context().getPrincipalName());
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public static void setAvailablePermissionNameSet(
			Set<String> availablePermissionNameSet) {
		DSPermission.availablePermissionNameSet = availablePermissionNameSet;
	}

	public static Set<String> getAvailablePermissionNameSet() {
		return availablePermissionNameSet;
	}
}
