package pl.compan.docusafe.core.office;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.classic.Lifecycle;
import org.hibernate.Session;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Klasa bazowa dla sposob�w dostarczenia pisma.
 * <p>
 * <b>Uwaga: poniewa� klasy dziedzicz�ce s� zadeklarowane w Hibernate
 * jako <i>lazy</i>, tzn. s� w�asnymi proxy, do swoich w�asnych
 * atrybut�w powinny odnosi� si� przez metody getXXX, a nie bezpo�rednio.
 * Prawdopodobnie ma to zwi�zek ze sposobem dzia�ania CGLIB.</b> 
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OfficeDocumentDelivery.java,v 1.8 2008/05/28 06:31:47 wkuty Exp $
 */
public abstract class OfficeDocumentDelivery implements Lifecycle
{
    private Integer id;
    private String name;
    private Integer posn;

    protected OfficeDocumentDelivery()
    {
    }

    protected OfficeDocumentDelivery(String name)
    {
        this.name = name;
    }

    protected abstract Class myDocumentClass();

    public final boolean canDelete() throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias docTable = from.createTable(DSApi.getTableName(myDocumentClass()));

        WhereClause where = new WhereClause();
        where.add(Expression.eq(docTable.attribute("delivery"), getId()));

        SelectClause selectCount = new SelectClause();
        /*SelectColumn countCol = */selectCount.addSql("count(*)");

        SelectQuery selectQuery;
        ResultSet rs = null;
        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);

            rs = selectQuery.resultSet(DSApi.context().session().connection());

            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby pism");

            int count = rs.getInt(1);

            rs.close();

            return count == 0;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	if (rs!=null)
        	{
        		try { rs.close(); } catch (Exception e1) { } 
        	}
        }
    }
    

    public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public final void delete() throws EdmException
    {
        if (!canDelete())
            throw new EdmException("Nie mo�na usun�� u�ywanego elementu");

        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /* Lifecycle */

    public final boolean onSave(Session s) throws CallbackException
    {
        if (id != null)
        {
            posn = id;
        }
        else
        {
            // w bazach, gdzie identyfikatory pochodz� z pola autonumber
            // id b�dzie w tym momencie r�wne null
            posn = Dictionaries.updatePosnOnSave(getClass());
        }
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }


    public Integer getId()
    {
        return id;
    }

    private void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getPosn()
    {
        return posn;
    }

    public void setPosn(Integer posn)
    {
        this.posn = posn;
    }

    public final boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof OfficeDocumentDelivery)) return false;

        final OfficeDocumentDelivery officeDocumentDelivery = (OfficeDocumentDelivery) o;

        if (getId() != null ? !getId().equals(officeDocumentDelivery.getId()) : officeDocumentDelivery.getId() != null) return false;

        return true;
    }

    public final int hashCode()
    {
        return (getId() != null ? getId().hashCode() : 0);
    }

    public String toString()
    {
        return getClass().getName()+"[id="+id+"]";
    }
}
