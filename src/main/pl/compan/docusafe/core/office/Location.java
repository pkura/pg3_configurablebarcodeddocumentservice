package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.PostalCode;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Location extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(Location.class);
    private static final String CodeFoundError = "CodeNotFound";
	/**
	 * Delegation method which handles GET and POST requests.
	 * 
	 * @param request
	 *            the request the client has made of the servlet
	 * @param response
     *            the response the servlet sends to the client
	 * @throws ServletException
	 * @throws IOException
	 */
	 public void doGet(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException
	 {
		    response.setContentType("Text/xml; charset=utf-8");
		 	response.setHeader("Cache-Control", "no-cache");

		 	String [] zipCodeParts = null;
		 	int changedZipCode = 0;
		 	String city = "";
		 	
		 	request.setCharacterEncoding("utf-8");
	        String zipCode = request.getParameter("zip").trim();
	        
	        if(zipCode.charAt(2)=='-') 
	        {
	        	zipCodeParts = zipCode.split("-");
	        	changedZipCode = Integer.parseInt(zipCodeParts[0].concat(zipCodeParts[1]) );
	        }
	    		 	
		 try
		 {
			 DSApi.openAdmin();
			 
		     PostalCode pc = PostalCode.findByCode(changedZipCode);
		     if(pc == null)
		     {
		    	 city = CodeFoundError;
		     }
		     else
		     {
		    	 city = pc.getCity();
		     }		  	     
		 }
		 catch (EdmException e) 
		 {
			 log.error(e.getMessage());
		 }                                                                         
		 finally 
		 {                                                                         
                                                                    
			 try                                                                 
			 {      
				 DSApi._close();                                                  
			 }                                                                   
			 catch (Exception ignored) {}                                        
			                                                                       
		 }                                                                         
  		
		 	if ((zipCode != null) ) {
		 		
	            response.setContentType("text/xml; charset=utf-8");
	            response.setHeader("Cache-Control", "no-cache");
	            response.getWriter().print("<valid>" + city + "</valid>");
	        } else {
	        	response.setContentType("text/xml; charset=utf-8");
	            response.setHeader("Cache-Control", "no-cache");
	            response.getWriter().print("<valid>" + city + "</valid>");
	        }
                                                                         
	 }
	 
	 public  void doPost(HttpServletRequest request, HttpServletResponse  response)
     throws IOException, ServletException {

		 doGet(request, response);
     }
}
