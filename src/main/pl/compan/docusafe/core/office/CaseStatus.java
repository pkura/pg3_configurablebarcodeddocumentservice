package pl.compan.docusafe.core.office;

import java.util.List;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CaseStatus.java,v 1.14 2008/02/14 12:42:02 mariuszk Exp $
 */
public class CaseStatus
{
    /**
     * Sprawa otwarta.
     */
    public static final Integer ID_OPEN = new Integer(1);
    /**
     * Sprawa ostatecznie zakończona.
     */
    public static final Integer ID_CLOSED = new Integer(3);
    /**
     * Sprawa tymczasowo zakończona.
     */
    public static final Integer ID_TMP_CLOSED = new Integer(2);
    /**
     * Sprawa  wznowiona.
     */
    public static final Integer ID_REOPEN = new Integer(4);
    /**
     * Sprawa planowana.
     */
    public static final Integer ID_PLANED = new Integer(5);

    
    private Integer id;
    private String name;


    public static List<CaseStatus> list() throws EdmException
    {
        try
        {
            return (List<CaseStatus>) DSApi.context().classicSession().find(
                "select s from s in class "+CaseStatus.class.getName()+
                " order by s.id");
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }


    public static CaseStatus find(Integer id) throws EdmException
    {
        CaseStatus status = (CaseStatus) DSApi.getObject(CaseStatus.class, id);

        if (status == null)
            throw new EntityNotFoundException(CaseStatus.class, id);

        return status;
    }

    public boolean isClosingStatus()
    {
        return id != null && (id.equals(ID_CLOSED) || id.equals(ID_TMP_CLOSED));
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
