package pl.compan.docusafe.core.office;

import java.util.Collection;

public class FinishOrAssignState
{
    private boolean canFinish;
    private boolean canAssign;
    private Collection<String> finishWarnings;
    private Collection<String> assignWarnings;
    private Collection<String> cantFinishReasons;
    private Collection<String> cantAssignReasons;

    public FinishOrAssignState(boolean canFinish, boolean canAssign, Collection<String> finishWarnings, Collection<String> assignWarnings, Collection<String> cantFinishReasons, Collection<String> cantAssignReasons)
    {
        this.canFinish = canFinish;
        this.canAssign = canAssign;
        this.finishWarnings = finishWarnings;
        this.assignWarnings = assignWarnings;
        this.cantFinishReasons = cantFinishReasons;
        this.cantAssignReasons = cantAssignReasons;
    }

    public boolean isCanFinish()
    {
        return canFinish;
    }

    public boolean isCanAssign()
    {
        return canAssign;
    }

    public Collection<String> getFinishWarnings()
    {
        return finishWarnings;
    }

    public Collection<String> getAssignWarnings()
    {
        return assignWarnings;
    }

    public Collection<String> getCantFinishReasons()
    {
        return cantFinishReasons;
    }

    public Collection<String> getCantAssignReasons()
    {
        return cantAssignReasons;
    }

    public String toString()
    {
        return "canFinish="+canFinish+" canAssign="+canAssign+
            " finishWarnings="+finishWarnings+" assignWarnings="+assignWarnings+
            " cantFinishReasons="+cantFinishReasons+" cantAssignReasons="+cantAssignReasons;
    }
}

