package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.users.Constants;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DivisionNotFoundException.java,v 1.3 2006/02/20 15:42:26 lk Exp $
 */
public class PersonNotFoundException extends EntityNotFoundException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public PersonNotFoundException(String msg)
    {
        super(sm.getString("PersonNotFoundException", msg));
    }
}
