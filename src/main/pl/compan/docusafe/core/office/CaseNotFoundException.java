package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CaseNotFoundException.java,v 1.4 2006/02/20 15:42:18 lk Exp $
 */
public class CaseNotFoundException extends EntityNotFoundException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public CaseNotFoundException(String caseId)
    {
        super("Nie znaleziono sprawy o identyfikatorze "+caseId);
    }

    public CaseNotFoundException(Long caseid)
    {
        super(sm.getString("caseNotFoundException", caseid));
    }
}
