package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RwaCategoryParentNotFoundException.java,v 1.1 2004/06/10 21:21:58 administrator Exp $
 */
public class RwaCategoryParentNotFoundException extends EntityNotFoundException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public RwaCategoryParentNotFoundException(Long childId)
    {
        super(sm.getString("RwaCategoryParentNotFoundException", childId));
    }
}
