package pl.compan.docusafe.core.office;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Constants.java,v 1.1 2004/05/16 20:10:30 administrator Exp $
 */
public final class Constants
{
    public static final String Package = Constants.class.getPackage().getName();
}
