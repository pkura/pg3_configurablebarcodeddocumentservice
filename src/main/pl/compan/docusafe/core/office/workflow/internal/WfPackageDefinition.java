package pl.compan.docusafe.core.office.workflow.internal;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.SQLException;

import oracle.jdbc.OraclePreparedStatement;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * Reprezentuje wersj� pliku XPDL.
 * <p>
 * Uwaga - obiekt typu read-only i taki rodzaj cache stosuje Hibernate.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfPackageDefinition.java,v 1.9 2008/10/29 23:06:56 wkuty Exp $
 */
public class WfPackageDefinition
{
    private Long id;
    private String packageId;
    private String hashId;
    private String xml;

    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getPackageId()
    {
        return packageId;
    }

    public void setPackageId(String packageId)
    {
        this.packageId = packageId;
    }

    public String getHashId()
    {
        return hashId;
    }

    public void setHashId(String hashId)
    {
        this.hashId = hashId;
    }

    public String getXml()
    {
        return xml;
    }

    public void setXml(String value)
    {
        xml = value;
    }

    public String toString()
    {
        return getClass().getName()+"[id="+id+" packageId="+packageId+"]";
    }
}
