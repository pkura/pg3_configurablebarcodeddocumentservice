package pl.compan.docusafe.core.office.workflow.snapshot;

import java.util.Set;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.bookmarks.Bookmark;
import pl.compan.docusafe.core.bookmarks.BookmarkManager;
import pl.compan.docusafe.core.bookmarks.FilterCondition;

public class TaskListCounterManager
{
	private static TaskListCounter taskListCounter = null;
	
	private Boolean withBackup;
	
	public TaskListCounterManager(boolean withBackup)
	{
		this.withBackup = withBackup;
	}

	protected static TaskListCounter getTaskListCounter()
	{
		if (taskListCounter==null)
		{
			if (AvailabilityManager.isAvailable("tasklist.jbpm4"))
			{
				taskListCounter = new JBPMTaskListCounter();;
			}
			else
			{
				taskListCounter = new InternalTaskListCounter();
			}
		}
		return taskListCounter;
	}

	/**Ustawia flage nieaktualnosci licznika dla uzytkownika*/
	public void reserUserTaskCount(String username)
	{
		getTaskListCounter().reserUserTaskCount(username);
	}

	/**Inicjuje countera - na razie nie ma tam nic*/
	public void initCounter()
	{
		getTaskListCounter().initCounter(withBackup);
	}

	/**Pobiera informacje o liscie zadan uzytkownika,
	 * lastDate = czas od jakiego beda loiczyc sie nowe i stare zadania
	 * @throws EdmException */
	public UserCounter getUserCounter(String username,Long lastDate) throws EdmException
	{
		return getTaskListCounter().getUserCounter(username,lastDate,withBackup);
	}
	/**
	 * metoda zwraca stringa z opilem ilosci poszczegolnych zadan dla uzytkownika np:
	 * "Pism na li�cie zada� - Przychodz�cych 50 / Wychod��cych 287
	 * @param username
	 * @param withBackup
	 * @return
	 * @throws EdmException
	 */
	public String getTaskListUserCount(String username,boolean withBackup) throws EdmException
	{
		return getTaskListCounter().getTaskListUserCount(username, withBackup);
	}
	public UserDockindCounter getUserDockindCounter(String username, String documentType, Long bookmarkId) throws EdmException {
		return getTaskListCounter().getUserDockindCounter(username, withBackup, documentType, bookmarkId);
	}
	
	/**Pobiera informacje o liscie zadan uzytkownika
	 * @param bookamrkId 
	 * @throws EdmException */
	public Integer getTaskListCount(String username,String documentType, Long bookamrkId) throws EdmException
	{
		return getTaskListCounter().getTaskListCount(username, withBackup, documentType,bookamrkId);
	}
	
	
	public String bookamrk(Long bookamrkId) throws EdmException
	{
		if(bookamrkId == null)
			return " and ";
		StringBuilder sb = new  StringBuilder(" and ");
		Set<FilterCondition> filter = Bookmark.find(bookamrkId).getFilters();
		for (FilterCondition condition : filter) 
		{
			BookmarkManager.parseFilter(condition);
			sb.append(condition.getColumnTableName()+ " in ("+ condition.asArrayString()+") and ");
		}
		return sb.toString();
	}
}
