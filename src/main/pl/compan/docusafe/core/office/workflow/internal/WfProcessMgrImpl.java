package pl.compan.docusafe.core.office.workflow.internal;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.type.Type;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.office.workflow.*;
import pl.compan.docusafe.core.office.workflow.xpdl.DataField;
import pl.compan.docusafe.core.office.workflow.xpdl.Participant;
import pl.compan.docusafe.core.office.workflow.xpdl.WorkflowProcess;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementacja WfProcessMgr u�ywana przez wewn�trzny system
 * workflow. Instancje tej klasy nie s� zarz�dzane przez Hibernate,
 * s� tworzone ad hoc przez metody {@link InternalWorkflowService#getAllProcessMgrs()}
 * oraz {@link InternalWorkflowService#getProcessMgrs(java.lang.String, java.lang.String)}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfProcessMgrImpl.java,v 1.7 2006/12/07 13:52:51 mmanski Exp $
 */
public class WfProcessMgrImpl implements WfProcessMgr //, Cloneable
{
    private WorkflowProcess process;
    /**
     * Identyfikator obiektu {@link WfPackageDefinition} zwi�zanego
     * z pakietem, w kt�rym znajduje si� proces tworzony przez ten
     * obiekt.
     */
    private Long packageDefinitionId;

    public WfProcessMgrImpl(WorkflowProcess process, Long packageDefinitionId)
    {
        this.process = process;
        this.packageDefinitionId = packageDefinitionId;
    }

    /* WfProcessMgr */

    public String getWorkflowName()
    {
        return InternalWorkflowService.NAME;
    }

    /**
     * Niezaimplementowane.
     */
    public String category() throws WorkflowException
    {
        throw new UnsupportedOperationException();
    }

    /**
     * Niezaimplementowane.
     */
    public String description() throws WorkflowException
    {
        throw new UnsupportedOperationException();
    }

    public String name() throws WorkflowException
    {
        return process.getName();
    }

    public String package_id() throws WorkflowException
    {
        return process.getPackage().getId();
    }

    public String process_definition_id() throws WorkflowException
    {
        return process.getId();
    }

    /**
     * Zwraca wszystkie dzia�aj�ce instancje proces�w zwi�zane
     * z tym mened�erem.
     */ 
    public WfProcess[] get_sequence_process() throws WorkflowException
    {
        try
        {
            return (WfProcess[]) DSApi.context().classicSession().find(
                "from wfp in class "+WfProcessImpl.class.getName()+
                " where wfp.packageId = ? and wfp.processDefinitionId = ?",
                new Object[] { package_id(), process_definition_id() },
                new Type[] { Hibernate.STRING, Hibernate.STRING }).
                toArray(new WfProcess[0]);
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
    }

    /**
     * Tworzy nowy proces.
     * @param requester Ta implementacja nie obs�uguje obiekt�w WfRequester,
     *  parametr powinien mie� warto�� null, w przeciwnym wypadku rzucany
     *  jest wyj�tek.
     */
    public WfProcess create_process(WfRequester requester) throws WorkflowException
    {
        if (requester != null)
            throw new IllegalArgumentException("Obs�uga obiektu WfRequester nie " +
                "jest zaimplementowana");

        // odczytywanie z bazy danych definicji pakietu, z kt�rym b�dzie
        // zwi�zany proces
        WfPackageDefinition packageDefinition;
        try
        {
            packageDefinition = (WfPackageDefinition) DSApi.context().session().
                load(WfPackageDefinition.class, packageDefinitionId);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }

        WfProcessImpl wfProcess = new WfProcessImpl();
        wfProcess.setPackageDefinition(packageDefinition);
        wfProcess.setPackageId(package_id());
        wfProcess.setProcessDefinitionId(process_definition_id());
        wfProcess.setState("open.not_running.not_started");
        wfProcess.setName(process.getName());
        wfProcess.setDescription(process.getName());
        // atrybut WfProcess.key zostanie uzupe�niony w WfProcessImpl.onSave()

        try
        {
            // zmienne procesu
            Set<NameValue> processContext = new HashSet<NameValue>();

            DataField[] dataFields = process.getDataFields();
            for (int i=0; i < dataFields.length; i++) 
            {
            	//Nie wiedzia�em jak to obejsc jak proces nie jest uruchamiany na uzytkownika tylko na grupe to nie potrzebujemy tej zmiennej 
            	//if("ds_assigned_user".equals(dataFields[i].getId()))
            	{
            //		System.out.println("Zemsta mariusza");
            //		continue;
            	}
                NameValueImpl nv = NameValueImpl.createNameValue(
                    dataFields[i], DSApi.context().session());
                processContext.add(nv);
            }

            wfProcess.setProcessContext(processContext);

            DSApi.context().session().save(wfProcess);
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }

        return wfProcess;
    }

    public Participant[] getParticipants() throws WorkflowException
    {
        return process.getParticipants();
    }

    public WorkflowProcess getProcessDefinition() throws WorkflowException
    {
        return process;
    }

    public String toString()
    {
        return getClass().getName()+"[process_definition_id="+process.getId()+"]";
    }
}
