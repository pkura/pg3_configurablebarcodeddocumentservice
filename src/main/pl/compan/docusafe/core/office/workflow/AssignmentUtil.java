package pl.compan.docusafe.core.office.workflow;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.xpdl.ExtendedAttribute;
import pl.compan.docusafe.core.office.workflow.xpdl.WorkflowProcess;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.SummaryTabAction;

/**
 * Klasa obslugujaca wykonanie dekretacji z roznych miejsc w aplikacji
 * Na wejscie metody do dekretacji podajemy klase AssigmentsBean kt�ra zawiera informacje o dekretacji
 * Posiada liste List<AssigmentParam> assigments zawieraj�c� pojedy�cze dektretacje
 * Klasa AssigmentParam zawiera :
 * ProcessId processId - u nas w praktyce wyst�puj� tylko dwa (realizacja i do wiadomosci)
 * 		new ProcessId("internal","docusafe_1","obieg_reczny"); - realizacja lub :
 * 		new ProcessId("internal","docusafe_1","do_wiadomosci"); - do wiadomosci
 * AssignmentDescriptor assignmentDescriptor - zawiera do kogo i na jaki guid
 * 		new AssignmentDescriptor(username,guid);
 * String objective - znane jako cel dekretacji (nie wymagane)
 * Zbi�r tych trzech dany (czyli jeden obiekt klasy AssigmentParam) definiuje jedna dekretacje
 * W klasie AssigmentsBean znajduj� sie r�wnie� dwie wazne informacje:
 * WfActivity wfActivity - identyfikuje aktywnosc na dokumecie;
 * Long docId - id dokumentu do dekretacji;
 * Po dokonaniu dekretacji metod�:
 * 		collectiveAssignment(AssigmentsBean assigmentsBean);
 * Z klasy AssigmentsBean mo�emy wyci�gn�� liste messages oraz errors kt�re najlepiej wy�wietli� 
 * u�ytkownikowi
 * 
 * @author Mariusz Kilja�czyk
 *
 */
public class AssignmentUtil 
{
	
	 private static StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
	 protected final static Log log = LogFactory.getLog(AssignmentUtil.class);
	 private static  StringManager sm = GlobalPreferences.loadPropertiesFile(SummaryTabAction.class.getPackage().getName(),null);
	/**
     * Wykonuje zbiorcz� dekretacj�.
     * @param assignments
     * @return True, je�eli bie��cy dokument zosta� przekierowany do kogo� innego
     *  i u�ytkownik nie mo�e ju� powr�ci� do jego edycji.
     * @throws EdmException
     */
	 
	 
    public static boolean collectiveAssignment(AssigmentsBean assigmentsBean) throws EdmException
    {
        smG = GlobalPreferences.loadPropertiesFile("",null);
        boolean mainDocumentReassigned = false;
        Long originalDocumentId = assigmentsBean.getDocId();
        Long docId = originalDocumentId;
        WfActivity wfActivity = assigmentsBean.getWfActivity();
        List<AssigmentParam> assigmentParam = assigmentsBean.getAssigments();
        for (AssigmentParam param : assigmentParam) 
        {
            String objective = param.getObjective();
            ProcessId processId = param.getProcessId();
            AssignmentDescriptor ad = param.getAssignmentDescriptor();

            if (objective == null || objective.length() == 0)
            {
                objective =
                    WorkflowUtils.iwfManualName().equals(processId.getProcessDefinitionId())
                    ? smG.getString("doRealizacji") : smG.getString("doWiadomosci");
            }

            String username = ad.getUsername();
            
            //sprawdzenie dla dekretacji na dzial
            if(username != null && username.equalsIgnoreCase("null"))
            	username = null;
            
            // sprawdzenie czy istnieje taki u�ytkownik
            if(username != null)
            {
            	
            	if(DSUser.findByUsername(username).isDeleted())
            	{
            		throw new UserNotFoundException(username);
            	}
            }
            
            String guid = ad.getGuid();

            if (log.isDebugEnabled())
                log.debug("pid="+processId+" username="+username+" guid="+guid);

            // dzia�, w kt�rym jest wybrany u�ytkownik
            DSDivision division = DSDivision.find(guid);

            if (log.isDebugEnabled())
                log.trace("division="+division);

            // proces, jaki ma zosta� uruchomiony
            WfProcessMgr mgr = WorkflowUtils.findProcessMgr(processId);

            // je�eli bie��cy proces jest typu cc - nie mo�na utworzy� procesu r�cznego
            // je�eli bie��cy proces jest typu r�cznego - nie mo�na przedekretowa� je�eli
            // canAssign si� na to nie zgadza
            if (WorkflowUtils.isManual(wfActivity))
            {
                Long documentId = WorkflowUtils.findParameterValue(wfActivity.process_context(), "ds_document_id", Long.class);
                OfficeDocument document = OfficeDocument.findOfficeDocument(documentId);
                try
                {
                    WorkflowUtils.checkCanAssign(document, new WorkflowActivity(wfActivity.key()));
                }
                catch (EdmException e)
                {
                	assigmentsBean.getErrors().add(sm.getString("NiePrzedekretowanoPismaDoRealizacji")+ "("+e.getMessage()+")");
                    continue;
                }
            }
            else
            {
                if (WorkflowUtils.isManual(mgr))
                {
                	assigmentsBean.getErrors().add(sm.getString("NieMoznaPrzedekretowacDoRealizacjiPismaOtrzymanegoDoWiadomosci"));
                }
            }

            WorkflowProcess processDef = mgr.getProcessDefinition();

            if (log.isDebugEnabled())
                log.trace("mgr="+mgr);

            // flaga okre�laj�ca, czy uruchamiany proces powinien mie�
            // w�asn� kopi� pisma
            boolean needsOwnDocument = false;

            // TODO: sprawdza� inaczej - nazw� procesu (obieg_reczny)
            ExtendedAttribute attr = processDef.getExtendedAttribute("ds_needs_own_document");
            if (attr != null && Boolean.valueOf(attr.getValue()).booleanValue())
            {
                needsOwnDocument = true;
            }

            OfficeDocument currentDocument;

            if (log.isDebugEnabled())
            {
                log.trace("process="+processId+", needsOwnDocument="+needsOwnDocument);
                log.trace("glowny dokument: "+docId);
            }

            // nowy proces potrzebuje w�asnej kopii dokumentu - tworzenie klonu w razie potrzeby
            // i umieszczanie go w currentDocument
            if (needsOwnDocument)
            {
                // tworzenie kopii dokumentu, je�eli bie��cy zosta�
                // ju� pos�any z innym procesem, za� proces wymaga oddzielnej kopii dokumentu
            	//mainDocumentReassigned = false;
                if (mainDocumentReassigned)
                {
                    OfficeDocument document = OfficeDocument.findOfficeDocument(docId);
                    // w wypadku pism przychodz�cych klonowaniu mo�e podlega� tylko dokument
                    // g��wny, kt�ry nale�y odszuka�, je�eli mamy jeden z dokument�w ju� sklonowanych
                    if (document.getType() == DocumentType.INCOMING
                        && ((InOfficeDocument) document).getMasterDocument() != null)
                    {
                        document = ((InOfficeDocument) document).getMasterDocument();
                    }

                    currentDocument = (OfficeDocument) document.cloneObject(null);

                    if (currentDocument.getType() == DocumentType.INCOMING)
                    {
                        ((InOfficeDocument) currentDocument).setMasterDocument((InOfficeDocument) document);
                    }

                    if (log.isDebugEnabled())
                        log.debug("kopia dokumentu "+docId+": "+currentDocument.getId());
                }
                else
                {
                    mainDocumentReassigned = true;
                    currentDocument = OfficeDocument.findOfficeDocument(docId);
                }
            }
            else
            {
                currentDocument = OfficeDocument.findOfficeDocument(docId);
            }
            WfProcess proc = mgr.create_process(null);
            NameValue[] procCtx = proc.process_context();

            WorkflowUtils.findParameter(procCtx, "ds_document_id").value().set(currentDocument.getId());
            WorkflowUtils.findParameter(procCtx, "ds_document_id").setValueInt(currentDocument.getId());
            WorkflowUtils.findParameter(procCtx, "ds_finished").value().set(Boolean.FALSE);
            WorkflowUtils.findParameter(procCtx, "ds_last_activity_user").value().set(DSApi.context().getPrincipalName());
            WorkflowUtils.findParameter(procCtx, "ds_objective").value().set(objective);
            WorkflowUtils.findParameter(procCtx, "ds_source_user").value().set(DSApi.context().getPrincipalName());
            WorkflowUtils.findParameter(procCtx, "ds_source_division").value().set(DSDivision.ROOT_GUID);

         //   WorkflowUtils.findParameter(procCtx, "ds_participant_id").value().set(username);
            WorkflowUtils.findParameter(procCtx, "ds_assigned_division").value().set(division.getGuid());
         //   WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(username); 
            
            if (username != null)
            {
                WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(username);
                WorkflowUtils.findParameter(procCtx, "ds_participant_id").value().set(username);              
            }
            else
            {
                WorkflowUtils.findParameter(procCtx, "ds_participant_id").value().set("ds_guid_"+division.getGuid());
                WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(DSApi.context().getPrincipalName());
            }            
            

            if (log.isDebugEnabled())
                log.trace("proc.start()");

            proc.start();

            if (WorkflowUtils.isManual(mgr))
                currentDocument.changelog(DocumentChangelog.WF_START_MANUAL);

            if (log.isDebugEnabled())
                log.trace("history");

            currentDocument.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                    DSApi.context().getPrincipalName(),
                    DSDivision.ROOT_GUID,
                    username,
                    division.getGuid(),
                    objective,
                    WorkflowUtils.isManual(proc.manager()) ? smG.getString("doRealizacji") : smG.getString("doWiadomosci"),
                    false, WorkflowUtils.isManual(proc.manager()) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION : AssignmentHistoryEntry.PROCESS_TYPE_CC));
            
            
            if (needsOwnDocument)
            {
                currentDocument.setCurrentAssignmentGuid(division.getGuid());
                currentDocument.setDivisionGuid(division.getGuid());
                currentDocument.setCurrentAssignmentUsername(username);
                currentDocument.setCurrentAssignmentAccepted(Boolean.FALSE);
            }
            if(currentDocument.getType() == DocumentType.ORDER)
            {
            	assigmentsBean.getMessages().add(sm.getString("PoleceniePrzedekretowanoNaUzytkownika",currentDocument.getOfficeNumber() != null ? currentDocument.getOfficeNumber() : "",
                    DSUser.findByUsername(ad.getUsername()).asFirstnameLastname(),division.getName()));
            }
            else if(username != null)
            {
            	assigmentsBean.getMessages().add(sm.getString("DokumentPrzedekretowanoNaUzytkownika",currentDocument.getOfficeNumber() != null ? currentDocument.getOfficeNumber() : "",
                   DSUser.findByUsername(ad.getUsername()).asFirstnameLastname(),division.getName()));
            }
            else
            {
            	assigmentsBean.getMessages().add(sm.getString("DokumentPrzedekretowanoNaDzial",currentDocument.getOfficeNumber() != null ? currentDocument.getOfficeNumber() : "",
                        division.getName()));
            }            
            TaskSnapshot.updateByDocumentId(currentDocument.getId(), currentDocument.getStringType());
        }
        if (mainDocumentReassigned)
        {
            WorkflowUtils.findParameter(wfActivity.process_context(),
                "ds_finished").value().set(Boolean.TRUE);
            wfActivity.complete();
            TaskSnapshot.updateByDocumentId(originalDocumentId, OfficeDocument.findOfficeDocument(originalDocumentId).getStringType());
        }

        return mainDocumentReassigned;
    }
    /**Dokonuje dekretacji przekazanych zadan na uzytkownika do realizacji*/
    public static void assignmentTasks(List<TaskSnapshot> tasks,String fromUsername,String toUsername,String division) throws UserNotFoundException, EdmException
    {
    	String[] documentActivity = new String[tasks.size()];
    	String[] plannedAssignment = new String[tasks.size()];
    	int i = 0;
    	for (TaskSnapshot task : tasks) 
    	{
    		plannedAssignment[i] = division+"/"+toUsername+"/"+WorkflowUtils.iwfManualName()+"//"+"realizacja";
    		documentActivity[i] = "internal,"+task.getActivityKey();

    		i++;
		}        
    	WorkflowFactory.multiAssignment(documentActivity,fromUsername, plannedAssignment, "", null);
    }
}
