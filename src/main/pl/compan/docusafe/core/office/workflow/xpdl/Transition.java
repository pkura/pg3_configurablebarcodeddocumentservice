package pl.compan.docusafe.core.office.workflow.xpdl;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Transition.java,v 1.2 2006/02/06 16:05:51 lk Exp $
 */
public class Transition
{
    private WorkflowProcess process;
    private String id;
    private Activity from;
    private Activity to;
    private String name;
    private String description;
    private Condition condition;
    private Map<String, ExtendedAttribute> extendedAttributes = new HashMap<String, ExtendedAttribute>();

    Transition(WorkflowProcess process, Element elTransition) throws EdmException
    {
        this.process = process;
        id = elTransition.attribute("Id").getValue();
        if (id == null)
            throw new EdmException("Brak identyfikatora przej�cia.");

        name = elTransition.attribute("Name") != null ?
            elTransition.attribute("Name").getValue() : null;

        description = elTransition.element("Description") != null ?
            elTransition.element("Description").getTextTrim() : null;

        from = process.getActivity(elTransition.attribute("From").getValue());
        if (from == null)
            throw new EdmException("Przej�cie "+id+" odnosi si� do nieistniej�cego "+
                "kroku �r�d�owego: "+process.getId()+";"+
                elTransition.attribute("From").getValue());

        to = process.getActivity(elTransition.attribute("To").getValue());
        if (to == null)
            throw new EdmException("Przej�cie "+id+" odnosi si� do nieistniej�cego "+
                "kroku docelowego: "+process.getId()+";"+
                elTransition.attribute("To").getValue());

        if (elTransition.element("Condition") != null)
            condition = new Condition(elTransition.element("Condition"));

        // rozszerzone atrybuty
        if (elTransition.element("ExtendedAttributes") != null)
        {
            List elExtendedAttributeList = elTransition.element("ExtendedAttributes").
                elements("ExtendedAttribute");
            if (elExtendedAttributeList != null && elExtendedAttributeList.size() > 0)
            {
                for (Iterator iter=elExtendedAttributeList.iterator(); iter.hasNext(); )
                {
                    Element elExtendedAttribute = (Element) iter.next();
                    ExtendedAttribute ea = new ExtendedAttribute(elExtendedAttribute);
                    extendedAttributes.put(ea.getName(), ea);
                }
            }
        }

    }

    public WorkflowProcess getProcess()
    {
        return process;
    }

    public String getId()
    {
        return id;
    }

    public Activity getFrom()
    {
        return from;
    }

    public Activity getTo()
    {
        return to;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public Condition getCondition()
    {
        return condition;
    }

    public ExtendedAttribute getExtendedAttribute(String name)
    {
        return (ExtendedAttribute) extendedAttributes.get(name);
    }

    public ExtendedAttribute[] getExtendedAttributes()
    {
        return (ExtendedAttribute[]) extendedAttributes.values().
            toArray(new ExtendedAttribute[extendedAttributes.size()]);
    }

    public String toString()
    {
        return "<Transition id="+id+" from="+from.getId()+" to="+to.getId()+">";
    }
}
