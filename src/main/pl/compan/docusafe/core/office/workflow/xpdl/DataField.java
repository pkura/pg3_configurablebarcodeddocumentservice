package pl.compan.docusafe.core.office.workflow.xpdl;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DataField.java,v 1.3 2006/02/20 15:42:21 lk Exp $
 */
public class DataField
{
    private String id;
    private String name;
    private String description;
    private boolean array;
    private DataType dataType;
    private Object initialValue;
    private int length;
    private Map<String, ExtendedAttribute> extendedAttributes = new HashMap<String, ExtendedAttribute>();

    public DataField(Element elDataField) throws EdmException
    {
        id = elDataField.attribute("Id").getValue();
        if (id == null)
            throw new EdmException("Brak identyfikatora zmiennej.");

        name = elDataField.attribute("Name") != null ?
            elDataField.attribute("Name").getValue() : null;

        // warto�� domy�lna - false
        array = elDataField.attribute("IsArray") != null &&
            Boolean.valueOf(elDataField.attribute("IsArray").getValue()).booleanValue();

        dataType = DataType.createDataType(elDataField.element("DataType"));

        description = elDataField.element("Description") != null ?
            elDataField.element("Description").getTextTrim() : null;

        if (elDataField.element("InitialValue") != null)
            initialValue = dataType.getValue(elDataField.element("InitialValue").getTextTrim());

        if (elDataField.element("Length") != null)
            length = Integer.parseInt(elDataField.element("Length").getTextTrim());

        // rozszerzone atrybuty
        if (elDataField.element("ExtendedAttributes") != null)
        {
            List elExtendedAttributeList = elDataField.element("ExtendedAttributes").
                elements("ExtendedAttribute");
            if (elExtendedAttributeList != null && elExtendedAttributeList.size() > 0)
            {
                for (Iterator iter=elExtendedAttributeList.iterator(); iter.hasNext(); )
                {
                    Element elExtendedAttribute = (Element) iter.next();
                    ExtendedAttribute ea = new ExtendedAttribute(elExtendedAttribute);
                    extendedAttributes.put(ea.getName(), ea);
                }
            }
        }
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public boolean isArray()
    {
        return array;
    }

    public DataType getDataType()
    {
        return dataType;
    }

    public Object getInitialValue()
    {
        return initialValue;
    }

    public int getLength()
    {
        return length;
    }

    public ExtendedAttribute getExtendedAttribute(String name)
    {
        return (ExtendedAttribute) extendedAttributes.get(name);
    }

    public ExtendedAttribute[] getExtendedAttributes()
    {
        return (ExtendedAttribute[]) extendedAttributes.values().
            toArray(new ExtendedAttribute[extendedAttributes.size()]);
    }

    public String toString()
    {
        return "<DataField id="+id+" name="+name+" description="+description+
            " length="+length+" initialValue="+initialValue+" isArray="+array+
            " dataType="+dataType+">";
    }
}
