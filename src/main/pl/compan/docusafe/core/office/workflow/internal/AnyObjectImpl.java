package pl.compan.docusafe.core.office.workflow.internal;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.AnyObject;
import pl.compan.docusafe.core.office.workflow.AnyObjectKind;
import pl.compan.docusafe.core.office.workflow.WorkflowException;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AnyObjectImpl.java,v 1.3 2006/02/06 16:05:49 lk Exp $
 */
public class AnyObjectImpl implements AnyObject
{
    private PropertyChangeSupport propertyChangeSupport =
        new PropertyChangeSupport(this);

    private Object value;
    private AnyObjectKindImpl kind;

    AnyObjectImpl(AnyObjectKindImpl kind, Object value) throws EdmException
    {
        if (kind == null)
            throw new NullPointerException("kind");
        if (value == null)
            throw new NullPointerException("value");
        if (!kind.typeCompatible(value))
            throw new EdmException("Warto�� nie jest zgodna z typem "+
                "(kind="+kind+", value="+value+")");

        this.kind = kind;
        this.value = value;
    }

    /**
     * Zwraca warto�� tego obiektu jako obiekt Javy odpowiadaj�cy
     * typowi ({@link #kind}).
     */
    public Object value() throws EdmException
    {
        return value;
    }

    /**
     * Zwraca typ przechowywanej warto�ci.
     */
    public AnyObjectKind kind()
    {
        return kind;
    }

    /**
     * Nadaje now� warto�� obiektowi. Klasa przekazanego obiektu musi
     * by� zgodna z typem okre�lonym przez atrybut {@link #kind()}.
     */
    public void set(Object value) throws EdmException
    {
        if (!kind.typeCompatible(value))
            throw new WorkflowException("Nowa warto�� jest niezgodna z typem zmiennej: "+
                value);

        Object oldValue = this.value;
        this.value = value;
        propertyChangeSupport.firePropertyChange("value", oldValue, this.value);
    }

    /**
     * Pozwala na dodanie obiektu, kt�ry b�dzie ka�dorazowo powiadamiany
     * o zmianie warto�ci atrybutu value.
     */
    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!(obj instanceof AnyObjectImpl)) return false;
        return value != null && value.equals(((AnyObjectImpl) obj).value);
    }

    public int hashCode()
    {
        return value != null ? value.hashCode() : 0;
    }

    public String toString()
    {
        return getClass().getName()+"[value="+value+" kind="+kind+"]";
    }
}
