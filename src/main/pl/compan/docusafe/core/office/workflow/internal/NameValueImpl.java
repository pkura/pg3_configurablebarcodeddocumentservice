package pl.compan.docusafe.core.office.workflow.internal;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.AnyObject;
import pl.compan.docusafe.core.office.workflow.NameValue;
import pl.compan.docusafe.core.office.workflow.xpdl.BasicDataType;
import pl.compan.docusafe.core.office.workflow.xpdl.DataField;
import pl.compan.docusafe.core.office.workflow.xpdl.DataType;
import pl.compan.docusafe.core.office.workflow.xpdl.EnumerationDataType;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Obiekt zarz�dzany przez Hibernate.
 * <p>
 * Tworzenie obiektu transient - musi dosta� nazw� i niepust� warto�� (AnyObject)
 * z odpowiednim typem.
 * <p>
 * Tworzenie obiektu przez Hibernate - dostaje nazw� i reprezentacj� napisow�
 * warto�ci oraz kod typu.
 * onLoad tworzy AnyObject (value) wraz z jego AnyObjectKind (na podstawie DataType)
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NameValueImpl.java,v 1.6 2008/07/23 12:44:10 mariuszk Exp $
 */
public class NameValueImpl implements NameValue, Cloneable, PropertyChangeListener
{
    private String name;
    /**
     * Reprezentacja napisowa warto�ci. Na podstawie tego pola tworzony
     * jest obiekt AnyObject w momencie wywo�ania metody value().
     */
    private String value;
    private Integer valueInt;
    private DataTypeImpl dataType;

    // pola niezarz�dzane przez Hibernate

    /**
     * Warto�� tworzona na podstawie p�l value oraz dataType.
     * Pole nie jest zarz�dzane przez Hibernate.
     */
    private AnyObject any;

    /**
     * Konstruktor prywatny u�ywany przez Hibernate.
     */
    private NameValueImpl()
    {
    }

    /**
     * Tworzy obiekt NameValueImpl o nazwie i typie pobranych
     * z przekazanego obiektu DataField. Je�eli obiekt DataField
     * posiada warto�� pocz�tkow�, jest ona u�ywana, w przeciwnym
     * wypadku u�yta zostanie warto�� domy�lna dla danego typu.
     * <p>
     * Je�eli typ okre�lony przez DataField.getDataType() nie istnieje
     * w bazie danych, jest tworzony jako instancja DataTypeImpl.
     * Je�eli istnieje taka mo�liwo��, u�ywane s� typy ju� znajduj�ce
     * si� w bazie danych.
     */
    static NameValueImpl createNameValue(DataField dataField, Session session)
        throws HibernateException
    {
        NameValueImpl nameValue = new NameValueImpl();

        nameValue.name = dataField.getId();

        if (dataField.getInitialValue() != null)
            nameValue.value = dataField.getInitialValue().toString();
        else
            nameValue.value = dataField.getDataType().getDefaultValue().toString();

        DataTypeImpl dataType;
        DataType xpdlDataType = dataField.getDataType();

        // je�eli jest to typ podstawowy - najpierw jest poszukiwany
        // w bazie danych, a je�eli nie istnieje - tworzony
        if (xpdlDataType instanceof BasicDataType)
        {
            // najpierw poszukiwanie typu w bazie danych
            List types = ((org.hibernate.classic.Session) session).find("from dt in class "+DataTypeImpl.class.getName()+
                " where dt.typeName = '"+xpdlDataType.getTypeName()+"' "+
                "and dt.basicType = '"+((BasicDataType) xpdlDataType).getType()+"'");
            if (types == null || types.size() == 0)
            {
                // tworzenie typu
                dataType = new DataTypeImpl();
                dataType.setTypeName(BasicDataType.NAME);
                dataType.setBasicType(((BasicDataType) xpdlDataType).getType());
                session.save(dataType);
            }
            else
            {
                // u�ywam istniej�cego typu
                dataType = (DataTypeImpl) types.get(0);
            }
        }
        // typ enumeracyjny jest zawsze tworzony
        else if (xpdlDataType instanceof EnumerationDataType)
        {
            EnumerationDataType enumerationDataType = (EnumerationDataType) xpdlDataType;
            dataType = new DataTypeImpl();
            dataType.setTypeName(EnumerationDataType.NAME);
            // TODO: ??? a = a
            //dataType.setEnumerationValues(dataType.getEnumerationValues());
            dataType.setEnumerationValues(enumerationDataType.getValues());
            session.save(dataType);
        }
        else
        {
            throw new IllegalArgumentException("Nieznany typ danej: "+xpdlDataType);
        }

        nameValue.dataType = dataType;

        // obiekt nameValue nie jest zapisywany w sesji Hibernate, bo nie
        // mo�e istnie� samodzielnie, tylko jako komponent kolekcji

        return nameValue;
    }

    /**
     * Aktualizuje pole value bie��cego obiektu na podstawie warto�ci
     * pola value przekazanego obiektu. Pole name pozostaje nienaruszone.
     * TODO: sprawdzi�
     */
/*
    void updateValue(NameValue nv) throws WorkflowException
    {
        this.value = nv.value().value().toString();
        this.any = null;
    }
*/

    /* NameValue */

    public String name()
    {
        return name;
    }


    public AnyObject value() throws EdmException
    {
        if (any == null)
        {
            AnyObjectKindImpl kind = new AnyObjectKindImpl(dataType);
            any = new AnyObjectImpl(kind, dataType.getXpdlDataType().getValue(value));
            //any = new AnyObjectImpl(kind, value); // �le - string
            ((AnyObjectImpl) any).addPropertyChangeListener(this);
        }

        return any;
    }

    /* PropertyChangeListener */

    /**
     * Odbiera komunikaty o zmianie warto�ci obiektu AnyObject utworzonego
     * po wywo�aniu funkcji value(). Obiekt AnyObject nie jest zarz�dzany
     * przez Hibernate, wi�c zmiany jego warto�ci nie s� automatycznie
     * uwzgl�dniane po zapisaniu obiektu NameValueImpl do bazy danych,
     * warto�� atrybutu value trzeba zmodyfikowa� explicite.
     */
    public void propertyChange(PropertyChangeEvent evt)
    {
        if (evt.getSource() == any && "value".equals(evt.getPropertyName()))
        {
            // nie trzeba sprawdza� typu warto�ci, jest to robione
            // w metodzie AnyObjectImpl.set()
            value = evt.getNewValue().toString();
        }
    }

    public NameValueImpl cloneObject()
    {
        try
        {
            NameValueImpl clone = (NameValueImpl) super.clone();
            // pole any nie jest zarz�dzane przez Hibernate
            clone.any = null;
            return clone;
        }
        catch (CloneNotSupportedException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public String getName()
    {
        return name;
    }

    void setName(String name)
    {
        this.name = name;
    }

    /**
     * Metoda prywatna, bo w�a�ciwa warto�� (AnyObject) tworzona jest
     * na podstawie reprezentacji napisowej zawartej w polu value.
     */
    private String getValue()
    {
        return value;
    }

    private void setValue(String value)
    {
        this.value = value;
    }

    private DataTypeImpl getDataType()
    {
        return dataType;
    }

    private void setDataType(DataTypeImpl dataType)
    {
        this.dataType = dataType;
    }



    public String toString()
    {
        return getClass().getName()+"[name="+name+" value="+value+
            " dataType="+dataType+"]";
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof NameValueImpl)) return false;

        final NameValueImpl nameValue = (NameValueImpl) o;

        if (dataType != null ? !dataType.equals(nameValue.dataType) : nameValue.dataType != null) return false;
        if (name != null ? !name.equals(nameValue.name) : nameValue.name != null) return false;
        if (value != null ? !value.equals(nameValue.value) : nameValue.value != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (name != null ? name.hashCode() : 0);
        result = 29 * result + (value != null ? value.hashCode() : 0);
        result = 29 * result + (dataType != null ? dataType.hashCode() : 0);
        return result;
    }

	public Integer getValueInt()
	{
		return valueInt;
	}

	public void setValueInt(Integer valueInt) 
	{
		this.valueInt = valueInt;
	}
	
	public void setValueInt(Long valueInt) 
	{
		this.valueInt = valueInt.intValue();
	}

	public Integer valueInt() 
	{
		return valueInt;
	}
}
