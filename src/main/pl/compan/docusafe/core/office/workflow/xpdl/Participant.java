package pl.compan.docusafe.core.office.workflow.xpdl;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Participant.java,v 1.1 2004/05/16 20:10:28 administrator Exp $
 */
public class Participant
{
    private WorkflowProcess process;
    private String id;
    private String name;
    private String type;
    /**
     * Je�eli true, identyfikator tego u�ytkownika jest wyra�eniem,
     * na podstawie kt�rego wybierany jest konkretny u�ytkownik.
     */
    private boolean expression;

    Participant(WorkflowProcess process, Element elParticipant) throws EdmException
    {
        this.process = process;
        id = elParticipant.attribute("Id").getValue();
        if (id == null)
            throw new EdmException("Brak identyfikatora uczestnika.");

        name = elParticipant.attribute("Name") != null ?
            elParticipant.attribute("Name").getValue() : null;
        type = elParticipant.element("ParticipantType").attribute("Type").getValue();

        if (!"RESOURCE_SET".equals(type) &&
            !"RESOURCE".equals(type) &&
            !"ROLE".equals(type) &&
            !"ORGANIZATIONAL_UNIT".equals(type) &&
            !"HUMAN".equals(type) &&
            !"SYSTEM".equals(type))
            throw new EdmException("Nieznany rodzaj uczestnika procesu: "+type);
    }

    Participant(WorkflowProcess process, String id, String name, String type,
                boolean expression) throws EdmException
    {
        this.process = process;
        this.id = id;
        this.name = name;
        this.type = type;
        this.expression = expression;

        if (!"RESOURCE_SET".equals(type) &&
            !"RESOURCE".equals(type) &&
            !"ROLE".equals(type) &&
            !"ORGANIZATIONAL_UNIT".equals(type) &&
            !"HUMAN".equals(type) &&
            !"SYSTEM".equals(type))
            throw new EdmException("Nieznany rodzaj uczestnika procesu: "+type);
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    /**
     * Zwraca typ uczestnika procesu: "RESOURCE_SET", "RESOURCE",
     * "ROLE", "ORGANIZATIONAL_UNIT", "HUMAN", "SYSTEM".
     */ 
    public String getType()
    {
        return type;
    }

    public boolean isExpression()
    {
        return expression;
    }

    public WorkflowProcess getProcess()
    {
        return process;
    }

    public String toString()
    {
        return "<Participant id="+id+" name="+name+" type="+type+">";
    }
}
