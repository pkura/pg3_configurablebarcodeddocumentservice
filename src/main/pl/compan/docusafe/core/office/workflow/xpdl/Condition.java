package pl.compan.docusafe.core.office.workflow.xpdl;

import org.dom4j.Element;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Condition.java,v 1.1 2004/05/16 20:10:28 administrator Exp $
 */
public class Condition
{
    public static final String CONDITION = "CONDITION";
    public static final String OTHERWISE = "OTHERWISE";
    public static final String EXCEPTION = "EXCEPTION";
    public static final String DEFAULTEXCEPTION = "DEFAULTEXCEPTION";

    private String expression;
    private String type;

    Condition(Element elCondition)
    {
        this.expression = elCondition.getTextTrim();
        this.type = elCondition.attribute("Type").getValue();
    }

    public String getExpression()
    {
        return expression;
    }

    public String getType()
    {
        return type;
    }

    public String toString()
    {
        return "<Condition expression="+expression+" type="+type+">";
    }
}
