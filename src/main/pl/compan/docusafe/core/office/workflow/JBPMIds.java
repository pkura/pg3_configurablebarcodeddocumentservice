package pl.compan.docusafe.core.office.workflow;

import java.util.Set;
import java.util.Vector;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
/**
 * klasa zarz�dza identyfikatorami aktor�w w jbpm
 * uzytkownika reprezenuje napis postaci [USERNAME_PREFIX][ds_username][;][ds_guid]
 * gdzie guid oznacza dzial na ktory uzytkownik otrzymuje zadanie
 * dzial jest reprezentowany przez napis [GUID_PREFIX][ds_guid]
 * 
 * @author kuba �wiat�y
 *
 */
public class JBPMIds 
{
	
	public static Logger log = LoggerFactory.getLogger(JBPMIds.class);
	
	private final static String USERNAME_PREFIX = "USER_";
	private final static String GUID_PREFIX = "GUID_";
	
	protected static String getJBPMUsername(String username, String guid) {
		return USERNAME_PREFIX+username+";"+guid;
	}
	
	protected static String getJBPMGuid(String guid) {
		return GUID_PREFIX+guid;
	}
	
	protected static Boolean isJBPMGuid(String id) {
		return id.startsWith(GUID_PREFIX);
	}
	
	protected static Boolean isJBPMUsername(String id) {
		return id.startsWith(USERNAME_PREFIX);
	}
	
	/**
	 * 
	 * @param username
	 * @return
	 * @throws EdmException
	 */
	protected static String getDSUsername(String username) throws EdmException 
	{
		log.trace("getDSUsername({})",username);
		if (username.indexOf(USERNAME_PREFIX) != 0 || username.indexOf(";") == -1) 
		{
			throw new EdmException("NotAProperJBPMUsername");
		} 
		else 
		{
			return username.substring(USERNAME_PREFIX.length(),username.indexOf(';'));
		}
	}
	
	/**
	 * Zwraca guid z nazwy usera
	 * @param username
	 * @return
	 * @throws EdmException
	 */
	protected static String getDSUserGuid(String username) throws EdmException 
	{
		log.trace("getDSUserGuid({})",username);
		if (username.indexOf(USERNAME_PREFIX) != 0 || username.indexOf(";") == -1) {
			throw new EdmException("NotAProperJBPMUsername");
		} else {
			return username.substring(username.indexOf(';')+1);
		}
	}
	
	protected static String getDSGuid(String guid) throws EdmException 
	{
		log.trace("getDSGuid {}",guid);
		String resp;
		if (guid.indexOf(USERNAME_PREFIX) == 0)
		{
			resp = getDSUserGuid(guid);
		}
		else if (guid.indexOf(GUID_PREFIX) != 0) 
		{
			throw new EdmException("NotAProperJBPMGuid");
		} else 
		{
			resp = guid.substring(GUID_PREFIX.length());
		}
		log.trace("guid={}",resp);
		return resp;
	}
	
}
