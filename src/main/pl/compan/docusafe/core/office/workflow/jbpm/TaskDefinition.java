package pl.compan.docusafe.core.office.workflow.jbpm;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import org.hibernate.HibernateException;
import org.hibernate.Criteria;

import java.util.List;
/* User: Administrator, Date: 2007-01-24 13:15:36 */

/**
 * Opisuje definicje jednego zadania z jednej definicj procesu JBPM. Unikalna musi by� para (name,processDefinitionId).
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class TaskDefinition
{
    private Long id;
    private String name;                // nazwa zadanie w JBPM
    private String nameForUser;
    /*private ProcessDefinition processDefinition;*/
    private String jbpmProcessDefinitionName;
    private String page;                // pierwsza strona do wyswietlenia w danym zadaniu

    public TaskDefinition()
    {

    }

    public synchronized void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static ProcessDefinition find(Long id) throws EdmException
    {
        return (ProcessDefinition) Finder.find(ProcessDefinition.class, id);
    }

    public static TaskDefinition find(String name, String jbpmProcessDefinitionName) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(TaskDefinition.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.eq("jbpmProcessDefinitionName",jbpmProcessDefinitionName));
            c.add(org.hibernate.criterion.Expression.eq("name",name));

            List list = c.list();
            if (list.size() == 0)
                return null;
            else if (list.size() > 1)
                throw new EdmException("Znaleziono wi�cej ni� jedn� definicje procesu o nazwie='"+name+"' i nazwie procesu='"+jbpmProcessDefinitionName+"'");
            else
                return (TaskDefinition) list.get(0);

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getNameForUser()
    {
        return nameForUser;
    }

    public void setNameForUser(String nameForUser)
    {
        this.nameForUser = nameForUser;
    }

  /*  public ProcessDefinition getProcessDefinition()
    {
        return processDefinition;
    }

    public void setProcessDefinition(ProcessDefinition processDefinition)
    {
        this.processDefinition = processDefinition;
    }*/

    public String getJbpmProcessDefinitionName()
    {
        return jbpmProcessDefinitionName;
    }

    public void setJbpmProcessDefinitionName(String jbpmProcessDefinitionName)
    {
        this.jbpmProcessDefinitionName = jbpmProcessDefinitionName;
    }

    public String getPage()
    {
        return page;
    }

    public void setPage(String page)
    {
        this.page = page;
    }
}
