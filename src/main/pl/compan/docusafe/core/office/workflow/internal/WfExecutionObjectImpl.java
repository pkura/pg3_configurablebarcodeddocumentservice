package pl.compan.docusafe.core.office.workflow.internal;

import pl.compan.docusafe.core.office.workflow.NameValue;
import pl.compan.docusafe.core.office.workflow.WfExecutionObject;
import pl.compan.docusafe.core.office.workflow.WorkflowException;

import java.util.Date;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfExecutionObjectImpl.java,v 1.6 2006/02/20 15:42:20 lk Exp $
 */
public abstract class WfExecutionObjectImpl implements WfExecutionObject
{
    protected String key;
    protected String name;
    protected String description;
    protected String state;
    protected Date lastStateTime;

    public String getWorkflowName()
    {
        return InternalWorkflowService.NAME;
    }

    public String getKey()
    {
        return key;
    }

    private void setKey(String key)
    {
        this.key = key;
    }

    public String getName()
    {
        return name;
    }

    void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    void setDescription(String description)
    {
        this.description = description;
    }

    public String getState()
    {
        return state;
    }

    void setState(String state)
    {
        this.state = state;
        this.lastStateTime = new Date();
    }

    public Date getLastStateTime()
    {
        return lastStateTime;
    }

    void setLastStateTime(Date lastStateTime)
    {
        this.lastStateTime = lastStateTime;
    }

    /* WfExecutionObject */

    public String key() throws WorkflowException
    {
        return key;
    }

    public String name() throws WorkflowException
    {
        return name;
    }

    public String state() throws WorkflowException
    {
        return state;
    }

    public String description() throws WorkflowException
    {
        return description;
    }

    /**
     * Czas ostatniej zmiany stanu obiektu.
     * <p>
     * UWAGA: niekt�re obiekty maj� do tego pola przypisan� instancj�
     * java.sql.Timestamp, a inne java.util.Date, co powoduje b��d
     * ClassCastException podczas por�wnywania w metodzie Timestamp.compareTo.
     * @return
     * @throws WorkflowException
     */
    public Date last_state_time() throws WorkflowException
    {
        return lastStateTime;
    }

    public abstract NameValue[] process_context() throws WorkflowException;

    public abstract void set_process_context(NameValue[] context) throws WorkflowException;
}
