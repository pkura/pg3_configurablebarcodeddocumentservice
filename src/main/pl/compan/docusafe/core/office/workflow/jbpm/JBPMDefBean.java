package pl.compan.docusafe.core.office.workflow.jbpm;

import java.util.*;

import org.jbpm.graph.def.ProcessDefinition;

public class JBPMDefBean {
	private String id;
	private String name;
	private Integer version;
	private long processNum;
	private long activeProcessNum;
	private String searchProcessURL;
	private Collection<JBPMDefBean> versions = new ArrayList<JBPMDefBean>();

    private Set<String> resources;
	
	public JBPMDefBean() {
		
	}
	
	public JBPMDefBean(ProcessDefinition def) {
		this.id = def.getId() + "";
		this.name = def.getName();
		this.version = def.getVersion();
		this.versions = new LinkedList<JBPMDefBean>();
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id + "";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public long getProcessNum() {
		return processNum;
	}
	public void setProcessNum(long processNum) {
		this.processNum = processNum;
	}
	public String getSearchProcessURL() {
		return searchProcessURL;
	}
	public void setSearchProcessURL(String searchProcessURL) {
		this.searchProcessURL = searchProcessURL;
	}

	public void setActiveProcessNum(long activeProcessNum) {
		this.activeProcessNum = activeProcessNum;
	}

	public long getActiveProcessNum() {
		return activeProcessNum;
	}

	public void setVersions(Collection<JBPMDefBean> versions) {
		this.versions = versions;
	}

	public Collection<JBPMDefBean> getVersions() {
		return versions;
	}


    public Set<String> getResources() {
        return resources;
    }

    public void setResources(Set<String> resources) {
        this.resources = resources;
    }
}
