package pl.compan.docusafe.core.office.workflow.snapshot;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.snapshot.UserDockindCounter.DockindCount;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class InternalTaskListCounter extends TaskListCounter
{
	private static Logger log = LoggerFactory.getLogger(InternalTaskListCounter.class);
	@Override
	public void initCounter(boolean withBackup)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void reserUserTaskCount(String username)
	{
		if(usersCounter.get(username) != null)
			usersCounter.get(username).setMustReload(true);
	}
	
	private static TaskListCounterManager manager;
	
	private TaskListCounterManager manager()
	{
		if(manager == null)
			manager = new TaskListCounterManager(false);
		return manager;
	}

	@Override
	public UserCounter getUserCounter(String username,Long lastDate,boolean withBackup) throws EdmException
	{
		if(usersCounter == null)
			usersCounter = new HashMap<String, UserCounter>();
		UserCounter uc = usersCounter.get(username);
		if(uc == null || uc.isMustReload())
		{
			uc = new UserCounter();
			uc.setUsername(username);
			uc.setLastSent(new Date(lastDate));
			//uc.setLastSent(new Date(DSApi.context().systemPreferences().node("taskListMailer").getLong(username, 0)));
			gatAllParam(uc,withBackup);
			uc.setLastReload(new Date());
			//TODO:Zastanowic sie jak zarzadzac aktualnoscia countera
			uc.setMustReload(Boolean.TRUE);
		}
		usersCounter.put(username, uc);
		return uc;
	}

	private void gatAllParam(UserCounter uc, boolean withBackup) throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			DSUser user = DSUser.findByUsername(uc.getUsername());
			DSUser[] users = user.getSubstituted();
			StringBuilder sb = new StringBuilder("select ");
			/**Wszystkie zadania*/
			sb.append(" ( select COUNT(1) from DSW_TASKLIST where  ").append(dswResources(users,uc.getUsername()));
			if(!withBackup)
				sb.append(" and backupPerson <> 1 ");
			/**Nowe zadania*/
			sb.append("), (select COUNT(1) from DSW_TASKLIST where  ").append(dswResources(users,uc.getUsername())).append(" and dsw_activity_laststatetime > ? ");
			if(!withBackup)
				sb.append(" and backupPerson <> 1 ");
			/**Nowe zaakceptowane*/
			sb.append("), (select COUNT(1) from DSW_TASKLIST where  ").append(dswResources(users,uc.getUsername())).append(" and dsw_activity_laststatetime > ? and dsw_assignment_accepted > 0 ");
			if(!withBackup)
				sb.append(" and backupPerson <> 1 ");
			/**Stare zaakceptowane*/
			sb.append("), (select COUNT(1) from DSW_TASKLIST where  ").append(dswResources(users,uc.getUsername())).append(" and dsw_activity_laststatetime <= ? and dsw_assignment_accepted > 0 ");
			if(!withBackup)
				sb.append(" and backupPerson <> 1 ");
			sb.append(" ) ");
			sb.append(" from ds_docusafe ");
			log.trace(sb.toString());
			log.trace("getLastSent "+ uc.getLastSent());
			ps = DSApi.context().prepareStatement(sb.toString());
			ps.setTimestamp(1, new java.sql.Timestamp(uc.getLastSent().getTime()));
			ps.setTimestamp(2, new java.sql.Timestamp(uc.getLastSent().getTime()));
			ps.setTimestamp(3, new java.sql.Timestamp(uc.getLastSent().getTime()));
			rs= ps.executeQuery();
			if(rs.next())
			{
				uc.setAllTask(rs.getInt(1));
				uc.setNewTask(rs.getInt(2));
				uc.setNewAcceptedTask(rs.getInt(3));
				uc.setNewNotAcceptedTask(uc.getNewTask() - uc.getNewAcceptedTask());
				uc.setOldTask(uc.getAllTask() - uc.getNewTask());
				uc.setOldAcceptedTask(rs.getInt(4));
				uc.setOldNotAcceptedTask(uc.getOldTask() - uc.getOldAcceptedTask());
			}
		}
		catch (SQLException e)
		{
			log.error("",e);
			throw new EdmException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
		}
	}

	private String dswResources(DSUser[] users,String username)
	{
		StringBuilder sb = new StringBuilder(" dsw_resource_res_key ");
		if(users != null && users.length > 0)
		{
			sb.append(" in( ");
			for (int i = 0; i < users.length;i++)
			{
				sb.append("'").append(users[i].getName()).append("',");
			}
			sb.deleteCharAt(sb.length()-1);
			sb.append(")");
		}
		else
		{
			sb.append(" = '").append(username).append("' ");
		}
		return sb.toString();
	}

	@Override
	public Integer getTaskListCount(String username, boolean withBackup,String documentType,Long bookamrkId) throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			DSUser user = DSUser.findByUsername(username);
			DSUser[] users = user.getSubstituted();
			if(documentType == null || documentType == "")
				throw new EdmException("Nieprawid�owy typ dokumentu");
			
			//System.out.println(bookamrkId + " select COUNT(1) from DSW_TASKLIST where  "+dswResources(users,username)+" and dsw_assignment_accepted = 0 "+ bookamrk(bookamrkId)+" document_type = ?");

			ps = DSApi.context().prepareStatement("select COUNT(1) from DSW_TASKLIST where  "+dswResources(users,username)+" and dsw_assignment_accepted = 0 "+ manager().bookamrk(bookamrkId)+" document_type = ?");
			ps.setString(1, documentType);
			rs = ps.executeQuery();
			
			if(!rs.next()) 
				throw new EdmException("Nie mo�na pobra� liczby niezaakceptowanych zada�");
			return rs.getInt(1);
		}
		catch (Exception e)
		{
			log.error("",e);
			throw new EdmException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
		}
	}

	@Override
	public UserDockindCounter getUserDockindCounter(String username,
			boolean withBackup, String documentType, Long bookmarkId)
			throws EdmException {
		UserDockindCounter counter = new UserDockindCounter();
		Map<String, DockindCount> dockindCountMap = new HashMap<String, DockindCount>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			DSUser user = DSUser.findByUsername(username);
			DSUser[] users = user.getSubstituted();
			counter.setUsername(username);
			counter.setDockindCount(dockindCountMap);
			
			ps = DSApi.context().prepareStatement("SELECT COUNT(1), dockindName, dsw_assignment_accepted from DSW_TASKLIST where " + dswResources(users, username) 
					+ " " + manager().bookamrk(bookmarkId) + " document_type = ? GROUP BY dsw_assignment_accepted, dockindName ORDER BY dockindName");
			ps.setString(1, documentType);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				int count = rs.getInt(1);
				String dockindName = rs.getString(2);
				boolean accepted = rs.getBoolean(3);
				DockindCount dockindCount;
				if(!dockindCountMap.containsKey(dockindName)) {
					dockindCount = new DockindCount();
					dockindCountMap.put(dockindName, dockindCount);
				} else {
					dockindCount = dockindCountMap.get(dockindName);
				}
				if(accepted) {
					dockindCount.accepted = count;
					counter.setAccepted(counter.getAccepted() + count);
				} else {
					dockindCount.notAccepted = count;
					counter.setNotAccepted(counter.getNotAccepted() + count);
				}
				dockindCount.all += count;
				counter.setAll(counter.getAll() + count);
			}
			
		} catch(SQLException e) {
			log.error("",e);
			throw new EdmException(e);
		} finally {
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
		}
		
		return counter;
	}


	@Override
	public String getTaskListUserCount(String username, boolean withBackup) throws EdmException
	{
		return "";
	}


	
	
}
