package pl.compan.docusafe.core.office.workflow.xpdl;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;

import java.util.*;

/**
 * Obiekt reprezentuj�cy jeden z proces�w zdefiniowanych w pliku XPDL.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkflowProcess.java,v 1.5 2006/02/20 15:42:21 lk Exp $
 */
public class WorkflowProcess
{
    private String id;
    private String name;
    /**
     * Pakiet, w kt�rym znajduje si� proces.
     */
    private XpdlPackage pkg;
    /**
     * Parametry formalne procesu, tablica indeksowana ich identyfikatorami (id).
     */
    private Map<String, FormalParameter> formalParameters = new HashMap<String, FormalParameter>();
    /**
     * Zmienne procesu, tablica indeksowana ich identyfikatorami (id).
     */
    private Map<String, DataField> dataFields = new HashMap<String, DataField>();
    /**
     * Uczestnicy procesu, tablica indeksowana ich identyfikatorami (id).
     */
    private Map<String, Participant> participants = new HashMap<String, Participant>();
    /**
     * Kroki procesu, tablica indeksowana ich identyfikatorami (id).
     */
    private Map<String, Activity> activities = new HashMap<String, Activity>();
    /**
     * Rozszerzone atrybuty procesu, tablica indeksowana ich nazwami (name).
     */
    private Map<String, ExtendedAttribute> extendedAttributes = new HashMap<String, ExtendedAttribute>();
    /**
     * Pierwszy krok procesu.
     */
    private Activity startingActivity;

    WorkflowProcess(XpdlPackage pkg, Element workflowProcess) throws EdmException
    {
        this.pkg = pkg;
        this.id = workflowProcess.attribute("Id").getValue();
        if (id == null)
            throw new EdmException("Brak identyfikatora procesu.");

        // atrybut opcjonalny
        this.name = workflowProcess.attribute("Name") != null ?
            workflowProcess.attribute("Name").getValue() : null;

        // parametry formalne procesu (element opcjonalny)
        Element elFormalParameters = workflowProcess.element("FormalParameters");

        if (elFormalParameters != null)
        {
            List elFormalParameterList = elFormalParameters.elements("FormalParameter");
            if (elFormalParameterList != null && elFormalParameterList.size() > 0)
            {
                for (Iterator iter=elFormalParameterList.iterator(); iter.hasNext(); )
                {
                    // element XpdlPackage/WorkflowProcesses/WorkflowProcess/FormalParameters/FormalParameter
                    Element elFormalParameter = (Element) iter.next();
                    FormalParameter formalParameter = new FormalParameter(elFormalParameter);
                    formalParameters.put(formalParameter.getId(), formalParameter);
                }
            }
        }

        // zmienne procesu (element opcjonalny)
        Element elDataFields = workflowProcess.element("DataFields");

        if (elDataFields != null)
        {
            List elDataFieldList = elDataFields.elements("DataField");
            if (elDataFieldList != null && elDataFieldList.size() > 0)
            {
                for (Iterator iter=elDataFieldList.iterator(); iter.hasNext(); )
                {
                    Element elDataField = (Element) iter.next();
                    DataField dataField = new DataField(elDataField);
                    dataFields.put(dataField.getId(), dataField);
                }
            }
        }

        // uczestnicy procesu
        Element elParticipants = workflowProcess.element("Participants");
        if (elParticipants != null)
        {
            List elParticipantList = elParticipants.elements("Participant");
            if (elParticipantList != null && elParticipantList.size() > 0)
            {
                for (Iterator iter=elParticipantList.iterator(); iter.hasNext(); )
                {
                    Element elParticipant = (Element) iter.next();
                    Participant participant = new Participant(this, elParticipant);
                    participants.put(participant.getId(), participant);
                }
            }
        }

        // kroki procesu
        Element elActivities = workflowProcess.element("Activities");
        if (elActivities != null)
        {
            List elActivityList = elActivities.elements("Activity");
            if (elActivityList != null && elActivityList.size() > 0)
            {
                for (Iterator iter=elActivityList.iterator(); iter.hasNext(); )
                {
                    Element elActivity = (Element) iter.next();
                    Activity activity = new Activity(this, elActivity);
                    activities.put(activity.getId(), activity);
                }
            }
        }

        // rozszerzone atrybuty
        if (workflowProcess.element("ExtendedAttributes") != null)
        {
            List elExtendedAttributeList = workflowProcess.element("ExtendedAttributes").
                elements("ExtendedAttribute");
            if (elExtendedAttributeList != null && elExtendedAttributeList.size() > 0)
            {
                for (Iterator iter=elExtendedAttributeList.iterator(); iter.hasNext(); )
                {
                    Element elExtendedAttribute = (Element) iter.next();
                    ExtendedAttribute ea = new ExtendedAttribute(elExtendedAttribute);
                    extendedAttributes.put(ea.getName(), ea);
                }
            }
        }

        // poszukiwanie pocz�tkowego kroku procesu

        // z listy wszystkich krok�w procesu usuwane s� te, kt�re
        // s� punktami docelowymi przej�� z innych krok�w
        // na ko�cu tej operacji na li�cie powinien zosta� jeden krok

        List<Activity> activitiesCopy = new ArrayList<Activity>(this.activities.values());

        // przej�cia mi�dzy krokami
        if (workflowProcess.element("Transitions") != null)
        {
            List elTransitionList = workflowProcess.element("Transitions").
                elements("Transition");
            if (elTransitionList != null && elTransitionList.size() > 0)
            {
                for (Iterator iter=elTransitionList.iterator(); iter.hasNext(); )
                {
                    Element elTransition = (Element) iter.next();
                    // obiekt Transition nie jest zapami�tywany w obiekcie WorkflowProcess
                    Transition transition = new Transition(this, elTransition);
                    transition.getFrom().addTransition(transition);
                    // usuwanie z listy tych krok�w, do kt�rych prowadz�
                    // przej�cia mi�dzy krokami
                    activitiesCopy.remove(transition.getTo());
                }
            }
        }

        if (activitiesCopy.size() > 1)
            throw new EdmException("Nie mo�na znale�� pierwszego kroku procesu; " +
                "potencjalne punkty pocz�tkowe: "+activitiesCopy);

        if (activitiesCopy.size() == 0)
            throw new EdmException("Nie mo�na znale�� pierwszego kroku procesu.");

        this.startingActivity = (Activity) activitiesCopy.get(0);
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public XpdlPackage getPackage()
    {
        return pkg;
    }

    public Activity[] getActivities()
    {
        return (Activity[]) activities.values().toArray(new Activity[activities.size()]);
    }

    public Participant[] getParticipants()
    {
        return (Participant[]) participants.values().toArray(new Participant[participants.size()]);
    }

    public Participant getParticipant(String id)
    {
        return (Participant) participants.get(id);
    }

    public Activity getActivity(String id)
    {
        return (Activity) activities.get(id);
    }

    public ExtendedAttribute getExtendedAttribute(String name)
    {
        return (ExtendedAttribute) extendedAttributes.get(name);
    }

    public ExtendedAttribute[] getExtendedAttributes()
    {
        return (ExtendedAttribute[]) extendedAttributes.values().
            toArray(new ExtendedAttribute[extendedAttributes.size()]);
    }

    public DataField getDataField(String id)
    {
        return (DataField) dataFields.get(id);
    }

    public DataField[] getDataFields()
    {
        return (DataField[]) dataFields.values().
            toArray(new DataField[dataFields.size()]);
    }

    public Activity getStartingActivity()
    {
        return startingActivity;
    }

    public String toString()
    {
        return "<WorkflowProcess id="+id+" name="+name+">";
    }
}
