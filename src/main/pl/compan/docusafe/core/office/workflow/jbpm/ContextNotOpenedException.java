package pl.compan.docusafe.core.office.workflow.jbpm;
import java.util.Collection;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public class ContextNotOpenedException extends EdmException{
	private final static Logger log = LoggerFactory.getLogger(ContextNotOpenedException.class);

	public ContextNotOpenedException(String message, Collection<String> messages) {
		super(message, messages);
	}

	public ContextNotOpenedException(Throwable cause) {
		super(cause);
	}

	public ContextNotOpenedException(String message, Throwable cause) {
		super(message, cause);
	}

	public ContextNotOpenedException(String message, boolean noLogging) {
		super(message, noLogging);
	}

	public ContextNotOpenedException(String message) {
		super(message);
	}

}
