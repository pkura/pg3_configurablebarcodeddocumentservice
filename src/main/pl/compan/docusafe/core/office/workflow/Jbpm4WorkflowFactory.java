package pl.compan.docusafe.core.office.workflow;

import com.google.common.base.Preconditions;

import org.activiti.engine.ProcessEngines;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.*;
import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;

import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.AssignmentHistoryEntryTargets;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.xes.XesLog;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementacja workflow factory dla jbpm4 na razie jej g��wnym zadaniem jest powstrzymanie r�nego rodzaju wyj�tk�w
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class Jbpm4WorkflowFactory extends WorkflowFactory{
    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(Jbpm4WorkflowFactory.class.getPackage().getName(),null);
    private static final Logger LOG = LoggerFactory.getLogger(Jbpm4WorkflowFactory.class);
    public static final String REASSIGNMENT_TIME_VAR_NAME = "reassignment-time";
    // Wymagana data zakonczenia
    public static final String DEADLINE_TIME = "DEADLINE_TIME";
    //Data przypomnienia o zadaniu
    public static final String REMINDER_DATE= "REMINDER_DATE";

    // Opis
    public static final String DOCDESCRIPTION = "DOCDESCRIPTION";
    //Referent
    public static final String CLERK = "CLERK";
    // Numer dokumentu
    public static final String DOCKIND_NUMER_DOKUMENTU = "DOCKIND_NUMER_DOKUMENTU";
    // Status dokumentu
    public static final String DOCKIND_STATUS = "DOCKIND_STATUS";
    // Nazwa procesu
    public static final String PROCESS_NAME = "PROCESS_NAME";

    public static final String OBJECTIVE = "OBJECTIVE";
    //Dzia� na ktory zostalkoi zadejretowane pismo, obowiazuje w akceptacji zadania 
    public static final String DS_DIVISIIN = "DS_DIVISIIN";
   
    {
        LOG.info("using jbpm4 workflow factory");
    }

    private static String cleanActivityId(String activityId){
        return activityId.replaceAll("[^,]*,","");
    }

    /**
     * Zwraca ci�g znak�w zawieraj�cy informacje o uczestnikach danego zadania
     * @param activityId id zadania, nie mo�e by� null
     * @return niepusty ci�g znak�w
     */
    public static String getParticipantsDescription(String activityId)throws EdmException {
		checkNotNull(activityId, "activityId cannot be null");
		activityId = cleanActivityId(activityId);

		StringBuilder ret = new StringBuilder();

		Task task = taskService().getTask(activityId);
		org.activiti.engine.task.Task activitiTask = null;
		if(task == null )
		  activitiTask = ProcessEngines.getDefaultProcessEngine().getTaskService().createTaskQuery().taskId(activityId).singleResult();
		
		if (task!=null && task.getAssignee() != null) {
			return sm.getString("PismoZnajdujeSieU", 
					DSUser.findByUsername(task.getAssignee()).asLastnameFirstname());
		}else if (activitiTask!=null && activitiTask.getAssignee()!=null){
			return sm.getString("PismoZnajdujeSieU", 
					DSUser.findByUsername(activitiTask.getAssignee()).asLastnameFirstname());
			}else {
			List<Participation> ps = taskService().getTaskParticipations(activityId);
			if (ps.size() == 1) {
				Participation p = ps.get(0);
//				
				
				if (p.getUserId() != null)
				{
					DSUser user = DSUser.findByUsername(p.getUserId());
					if(user.getSubstituteUser() == null)
					{
						ret.append(sm.getString("PismoZnajdujeSieU", user.asLastnameFirstname()));
					}
					else if (user.getSubstituteUser() != null)
					{
						ret.append(sm.getString("PismoZnajdujeSieU", 
						DSUser.findByUsername(p.getUserId()).asLastnameFirstname()+ 
						" ( zast�powany przez " + user.getSubstituteUser().asLastnameFirstname() + " )"));
					}
				}
				 else {
					ret.append(sm.getString("PismoZnajdujeSieWDziale", DSDivision.find(p.getGroupId()).asNameParentName()));
				}
			} else if (ps.size() > 1) {
				ret.append(sm.getString("PismoZnajdujeSieUWieluUzytkownikow")).append(':');
				for (Participation p : ps) {
					ret.append(participantToString(p)).append(", ");
				}
			}
		}
		return ret.toString();
	}

    private static String participantToString(Participation p) throws EdmException {
		if (p.getUserId() != null)
		{
			DSUser user = DSUser.findByUsername(p.getUserId());
			if(user.getSubstituteUser() == null)
			{
				return sm.getString("PismoZnajdujeSieU", user.asLastnameFirstname());
			}
			else if (user.getSubstituteUser() != null)
			{
				return user.asLastnameFirstname()+ 
				" ( zast�powany przez " + user.getSubstituteUser().asLastnameFirstname() + " )";
			}
		} else {
			 return sm.getString("dzial") + ':' + DSDivision.safeGetName(p.getGroupId());
		}
		return null;
	}

    @Override
    public Boolean checkCanAssignMe(Long documentId, String activityId) throws UserNotFoundException, EdmException 
    {
    	if (AvailabilityManager.isAvailable("NoAssignButton")) {
    		return false;
    	}
    	
         if (LOG.isTraceEnabled()){
        	LOG.trace("checkCanAssignMe");
        	LOG.trace("documentId={}", documentId);
            LOG.trace("activityId={}", activityId);
        }
        Boolean canAssignMe;
        String actId = activityId;
        if (actId == null){
        	Iterable<String> taskIds = Jbpm4ProcessLocator.taskIds(documentId);
        	for (String taskId : taskIds)
        		actId = taskId;
        }
        if (actId != null)
        {	
        	 actId = cleanActivityId(actId);
        	 TaskService ts = Jbpm4Provider.getInstance().getTaskService();
             Object currentGuid  = ts.getVariable(actId, "guid");
             Object assignee = ts.getVariable(actId, "assignee");
            if (assignee == null && currentGuid != null)
            {
                DSDivision div = DSDivision.find(currentGuid.toString());
                canAssignMe = UserFactory.getInstance().getCanAssignDivisions().contains(div);
            } 
            else if(assignee != null) 
            {
                DSUser user = UserFactory.getInstance().findByUsername(assignee.toString());
                if (user.getName().equals(DSApi.context().getPrincipalName())){
                    canAssignMe = false;
                } else 
                { 
                	if ( DSApi.context().isAdmin() || DSApi.context().hasPermission(DSPermission.ZADANIA_WSZYSTKIE_PODGLAD))
                    {
                		return true;
                    }
                	//to jest tak z dupy, �e nie wiem co napisa�
                    canAssignMe = UserFactory.getInstance().getCanAssignUsers().contains(user);
                }
            }
            else
            	return DSApi.context().isAdmin();
        }
        else
        {
            canAssignMe = false || DSApi.context().isAdmin();
        }
        return canAssignMe;
    }

    @Override
    public void manualFinish(String activityId, boolean checkCanFinish) throws EdmException {
        activityId = cleanActivityId(activityId);
        boolean toEnd = Jbpm4Provider.getInstance().getTaskService().getOutcomes(activityId).contains("to end");
        boolean finish = Jbpm4Provider.getInstance().getTaskService().getOutcomes(activityId).contains("finish-task");

        if(checkCanFinish && !toEnd && !finish){
            throw new EdmException(sm.getString("nieMoznaZakonczycZadania"));
        }
        Task task = Jbpm4Provider.getInstance().getTaskService().getTask(activityId);
        Execution ex = Jbpm4Provider.getInstance().getExecutionService().findExecutionById(task.getExecutionId());
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setCtimeAndCdate(new Date());
        ahe.setObjective("finish-task");
        ahe.setProcessName(ex.getProcessDefinitionId());
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setStatus("finished");
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        ahe.setType(AssignmentHistoryEntry.JBPM_FINISH);

        OfficeDocument od =OfficeDocument.find(Jbpm4ProcessLocator.documentIdForProcess(ex.getProcessInstance().getId()));
        od.addAssignmentHistoryEntry(ahe, false, activityId);
        TaskSnapshot.updateByDocument(od);

        if(checkCanFinish && finish){
            Jbpm4Provider.getInstance().getExecutionService()
                    .signalExecutionById(task.getExecutionId(), "finish-task");
        }
        else if (checkCanFinish && toEnd)
        {
            Jbpm4Provider.getInstance().getExecutionService()
                    .signalExecutionById(task.getExecutionId(), "to end");
        }
        else {
     	   String  procesInstanceId = ex.getProcessInstance().getId();
     	   Jbpm4Provider.getInstance().getExecutionService().deleteProcessInstance(procesInstanceId);
        }
    }

    @Override
    public void reassign(String activity, String fromUser, Set<String> candidateUsers, Set<String> candidateDivisionGuids) throws EdmException {
        reassign(activity, null, fromUser,
                candidateUsers.toArray(new String[candidateUsers.size()]),
                candidateDivisionGuids.toArray(new String[candidateDivisionGuids.size()]));
    }

    //TODO zrobi� wyszukiwanie dla historii danego procesu
    public static List<AssignmentHistoryEntry> provideHistory(long documentId, String activityId) throws EdmException {
        return OfficeDocument.find(documentId).getAssignmentHistory();
    }

    @Override
    public void assignToCoordinator(String activityId, ActionEvent event) throws EdmException {
        activityId = cleanActivityId(activityId);
        OfficeDocument document = Documents.officeDocument(Jbpm4ProcessLocator.documentIdForActivity(activityId), activityId).getDocument();
        ProcessCoordinator pc = document.getDocumentKind().logic().getProcessCoordinator(document);
        if(pc.getUsername() != null){
            assignForOne(activityId, DSApi.context().getPrincipalName(), pc.getUsername());
            assignForMany(activityId, new String[]{pc.getUsername()}, new String[0]);
        } else {
            assignForMany(activityId, new String[0], new String[]{pc.getGuid()});
        }

        TaskService ts = Jbpm4Provider.getInstance().getTaskService();
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setTargetUser(pc.getUsername());
        ahe.setTargetGuid(pc.getGuid());
        ahe.setProcessName(Jbpm4Provider.getInstance().getExecutionService().findExecutionById(
                ts.getTask(activityId).getExecutionId()).getProcessDefinitionId());
        ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_ASSIGN_COORDINATOR);
        ahe.setCtime(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        document.addAssignmentHistoryEntry(ahe);

        event.addActionMessage(sm.getString("PrzekazanoDoKoordynatora", activityId));

        TaskSnapshot.updateByDocumentId(document.getId(), "anyType");
    }

    @Override
    public boolean isManual(WorkflowActivity act) throws WorkflowException {
        return false;
    }

    @Override
    public boolean isManual(String id) throws EdmException {
    	if(StringUtils.isNotEmpty(id))
    		return StringUtils.contains(id, "internal");
        return false;
    }

    @Override
    public Map<String, Map<String, String>> prepareCollectiveAssignments(String activity) throws EdmException {
        return Collections.emptyMap();
    }

    public static void assignMe(String act, Long documentId) throws EdmException {
        LOG.info("assign me !!!! {} {}",act, documentId);
        Preconditions.checkNotNull(act);
        act = cleanActivityId(act);
        if(documentId == null){
            documentId = Jbpm4ProcessLocator.documentIdForActivity(act);
        }
//        taskService().
        taskService().assignTask(act, null);
        cleanParticipations(act);
        taskService().setVariables(act, Collections.singletonMap(REASSIGNMENT_TIME_VAR_NAME, new Date()));
        taskService().addTaskParticipatingUser(act, DSApi.context().getPrincipalName(), Participation.CANDIDATE);

        OfficeDocument doc = OfficeDocument.find(documentId);
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setTargetUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(Jbpm4Provider.getInstance().getExecutionService().findExecutionById(
                taskService().getTask(act).getExecutionId()).getProcessDefinitionId());
        ahe.setType(AssignmentHistoryEntry.JBPM4_REASSIGN_ME);
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        ahe.setCtime(new Date());
        doc.addAssignmentHistoryEntry(ahe);

        TaskSnapshot.updateByDocumentId(documentId, "anyType");
    }

    /**
     * R�czna dekretacja jbpmowa na wiele os�b / wiele dzia��w
     * @param act activity (najcz�ciej z query stringa)
     * @param documentId id dokumentu mo�e by� null (wtedy metoda sama znajdzie)
     * @param fromUsername u�ytkownik dekretuj�cy
     * @param targetUsers usernamesy
     * @param targetGuids guidy (niezwi�zane z u�ytkownikami!)
     * @throws EdmException
     */
    public static void reassign(String act, Long documentId,  String fromUsername, String[] targetUsers, String[] targetGuids) throws EdmException
    {
        reassign(act,documentId,fromUsername,targetUsers,targetGuids, null);
    }

    public static void reassign(String act, Long documentId,  String fromUsername, String[] targetUsers, String[] targetGuids, Map<String,Object> params) throws EdmException {
        act = cleanActivityId(act);
        if(documentId == null){
            documentId = Jbpm4ProcessLocator.documentIdForActivity(act);
        }
        TaskService ts = Jbpm4Provider.getInstance().getTaskService();

        if(params != null) {
            params.put(REASSIGNMENT_TIME_VAR_NAME, new Date());
            ts.setVariables(act, params);
        } else {
            ts.setVariables(act, Collections.singletonMap(REASSIGNMENT_TIME_VAR_NAME, new Date()));
        }

        assignForMany(act, targetUsers, targetGuids);
        LOG.info("reassigning by '{}' to users {} and guids {}", fromUsername, StringUtils.join(targetUsers), StringUtils.join(targetGuids));
        if (AvailabilityManager.isAvailable("addToWatch"))
       	    	DSApi.context().watch(URN.create(Document.find(documentId)));
        OfficeDocument doc = OfficeDocument.find(documentId);
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(fromUsername);
        ahe.setProcessName(Jbpm4Provider.getInstance().getExecutionService().findExecutionById(
                ts.getTask(act).getExecutionId()).getProcessDefinitionId());
        ahe.setType(AssignmentHistoryEntry.JBPM4_REASSIGN);
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        ahe.setCtime(new Date());
        
        if (targetUsers != null) {
	        for(String user : targetUsers)
	        	ahe.getAssignmentHistoryTargets().add(new AssignmentHistoryEntryTargets(AssignmentHistoryEntryTargets.USER, user));
        }
        if (targetGuids != null) {
	        for(String guid : targetGuids)
	        	ahe.getAssignmentHistoryTargets().add(new AssignmentHistoryEntryTargets(AssignmentHistoryEntryTargets.GUID, guid));
        }
        if(params != null && params.containsKey(Jbpm4WorkflowFactory.OBJECTIVE)) {
            ahe.setObjective((String) params.get(Jbpm4WorkflowFactory.OBJECTIVE));
        }

        if(params != null && params.containsKey(Jbpm4WorkflowFactory.DEADLINE_TIME)) {
            ahe.setDeadline(DateUtils.formatJsDate((Date) params.get(Jbpm4WorkflowFactory.DEADLINE_TIME)));
        }

        doc.addAssignmentHistoryEntry(ahe);
        
        TaskSnapshot taskSnaphot = TaskSnapshot.getAbsInstance().findByActivityKey(act.replaceAll("[^,]*,",""));
    	taskSnaphot.setAccepted(false);
        TaskSnapshot.updateByDocumentId(documentId, "anyType");
        TaskSnapshot taskSnapshot = TaskSnapshot.getInstance().findByActivityKey(act.replaceAll("[^,]*,",""));
    	taskSnapshot.setAccepted(false);
    	if (taskSnapshot instanceof JBPMTaskSnapshot)
    	{
    		((JBPMTaskSnapshot)taskSnapshot).update();
    	}
    	DSApi.context().session().update(taskSnapshot);
        
    }

    private static void assignForMany(String act, String[] targetUsers, String[] targetGuids) {
        Preconditions.checkArgument(targetGuids.length > 0 || targetUsers.length > 0);
        TaskService ts = Jbpm4Provider.getInstance().getTaskService();
        cleanParticipations(act);
        ts.assignTask(act, null);
//        ts.getTask(act).setAssignee(null);
        if (targetUsers != null) {
	        for(String username: targetUsers){
	            ts.addTaskParticipatingUser(act, username, Participation.CANDIDATE);
	        }
        }
        if (targetGuids != null) {
	        for(String guid: targetGuids){
	            ts.addTaskParticipatingGroup(act, guid, Participation.CANDIDATE);
	        }
        }
    }

    private static void cleanParticipations(String act) {
        for(Participation p: taskService().getTaskParticipations(act)){
            if(p.getGroupId() != null){
                taskService().removeTaskParticipatingGroup(act, p.getGroupId(), p.getType());
            } else {
                taskService().removeTaskParticipatingUser(act, p.getUserId(), p.getType());
            }
        }
    }

    /**
     * R�czna dekretacja jbpmowa na pojedyncz� osob�
     * @param act activityId (najcz�ciej z query stringa)
     * @param documentId id dokumentu, mo�e byc null
     * @param fromUsername u�ytkownik dekretuj�cy
     * @param toUsername u�ytkownik otrzymuj�cy zadanie
     * @throws EdmException
     */
    public static void reassign(String act, Long documentId,  String fromUsername, String toUsername) throws EdmException
    {
        reassign(act,documentId,fromUsername,toUsername,null);
    }

    public static void reassign(String act, Long documentId,  String fromUsername, String toUsername, Map<String,Object> params) throws EdmException {
    	act = cleanActivityId(act);

        if(documentId == null){
            documentId = Jbpm4ProcessLocator.documentIdForActivity(act);
        }

        TaskService ts = Jbpm4Provider.getInstance().getTaskService();

        if(params != null) {
            params.put(REASSIGNMENT_TIME_VAR_NAME, new Date());
            ts.setVariables(act, params);
        } else {
            ts.setVariables(act, Collections.singletonMap(REASSIGNMENT_TIME_VAR_NAME, new Date()));
        }
        OfficeDocument doc = Documents.officeDocument(documentId, act).getDocument();
        
        
        assignForOne(act, fromUsername, toUsername);
        
        if (AvailabilityManager.isAvailable("addToWatch"))
       	    	DSApi.context().watch(URN.create(Document.find(documentId)));
       
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(fromUsername);
        ahe.setTargetUser(toUsername);
        ahe.setProcessName(Jbpm4Provider.getInstance().getExecutionService().findExecutionById(
                ts.getTask(act).getExecutionId()).getProcessDefinitionId());
        ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
        ahe.setCtime(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);

        DSUser user = DSUser.findByUsername(ahe.getTargetUser());
        if(user != null && user.getSubstituteUser() != null){
        	ahe.setSubstituteUser(user.getSubstituteUser().getName());
        }

        LoggerFactory.getLogger("tomekl").debug("reassign {}",params != null);

        if(params != null && params.containsKey(Jbpm4WorkflowFactory.OBJECTIVE)) {
            ahe.setObjective((String) params.get(Jbpm4WorkflowFactory.OBJECTIVE));
        }

        if(params != null && params.containsKey(Jbpm4WorkflowFactory.DEADLINE_TIME)) {
            ahe.setDeadline(DateUtils.formatJsDate((Date) params.get(Jbpm4WorkflowFactory.DEADLINE_TIME)));
        }

        doc.addAssignmentHistoryEntry(ahe);
        
        TaskSnapshot taskSnapshot = TaskSnapshot.getInstance().findByActivityKey(act.replaceAll("[^,]*,",""));
    	taskSnapshot.setAccepted(false);
    	if (taskSnapshot instanceof JBPMTaskSnapshot)
    	{
    		((JBPMTaskSnapshot)taskSnapshot).update();
    	}
    	DSApi.context().session().update(taskSnapshot);
    	
    	doc.getDocumentKind().logic().onEndManualAssignment(doc, fromUsername, toUsername);
    	
        TaskSnapshot.updateByDocumentId(documentId, "anyType");
    }
    
    private static void assignForOne(String act, String fromUsername, String toUsername) {
		TaskService ts = Jbpm4Provider.getInstance().getTaskService();
		///ts.assignTask(act, null);
		Task task = ts.getTask(act);

        // U�ytkownik nie zaakceptowa� zadania
        List<Participation>  toReplace =  new ArrayList<Participation>();
        for (Participation p : ts.getTaskParticipations(act)) {
            //System.out.println("Fromuser "+ fromUsername + " to user " + toUsername + " p.getGroupId() " + p.getGroupId() + " p.getUserId() "+ p.getUserId()) ;
            if (p.getGroupId() != null) {
                try {
                    for (DSUser user : DSDivision.find(p.getGroupId()).getUsers()) {
                        if (fromUsername.equals(user.getName()))
                            toReplace.add(p);
                    }
                }
                catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                }
            }

            if (fromUsername.equals(p.getUserId()) || fromUsername.equals(task.getAssignee())) {
                toReplace.add(p);
            }
        }
        //Jesli nie znalazl to bierzemy pierwszy
        if (toReplace.isEmpty() && ts.getTaskParticipations(act).size() > 0)
        {
            //toReplace = (List<Participation>) ts.getTaskParticipations(act).get(0);
            toReplace.add(ts.getTaskParticipations(act).get(0));
        }
        ts.assignTask(act, null);

        // teoretycznie mo�liwe, ale ma�o prawdopodobne
        if (toReplace.isEmpty())
        {
            LOG.warn("reassinging task " + act + " from " + fromUsername + " : user does not participates in task");
            ts.assignTask(act, toUsername);
        }
        else
        {
            String type ="";
            for ( Participation p : toReplace) {
                if( p.getGroupId() != null ) {
                    ts.removeTaskParticipatingGroup(act, p.getGroupId(), p.getType());
                } else {
                    ts.removeTaskParticipatingUser(act, p.getUserId(), p.getType());
                }
                type = p.getType();
            }
            ts.addTaskParticipatingUser(act, toUsername, type);
        }
    }


    public static void acceptTaskIfNotAccepted(String act, ActionEvent event) throws EdmException {
        act = cleanActivityId(act);
        LOG.info("act = " + act);
        Task task = Jbpm4Provider.getInstance().getTaskService().getTask(act);

        ExecutionService executionService = Jbpm4Provider.getInstance().getExecutionService();
        String currentAssignee =  (String) executionService.getVariable(task.getExecutionId(), "currentAssignee");

        if(!DSApi.context().getPrincipalName().equals(currentAssignee)){
            acceptTask(task, event, OfficeDocument.find(Jbpm4ProcessLocator.documentIdForActivity(task.getId())),null);
        } else {
            LOG.info("task {} accepted by {}", task.getId(), currentAssignee);
        }
    }
    
    @Override
    public void acceptTask(String act, ActionEvent event, OfficeDocument document,String division) throws EdmException {
        //super.acceptTask(act, event, document);
        act = cleanActivityId(act);
        LOG.info("act = " + act);
        Task task = Jbpm4Provider.getInstance().getTaskService().getTask(act);

        acceptTask(task, event, document,division);
    }

    private static void acceptTask(Task task, ActionEvent event, OfficeDocument document,String division) throws EdmException {
        ExecutionService executionService = Jbpm4Provider.getInstance().getExecutionService();
        String currentAssignee =  (String) executionService.getVariable(task.getExecutionId(), "currentAssignee");
        Boolean notSubst=true;
        String assignee=null;
        isXesLogDecretation(executionService,task, document);
        if(AvailabilityManager.isAvailable("zastepstwa.przyjecieDokumentuWImieniuZastepowanego")&&DSApi.context().getDSUser().getSubstitutedUsers()!=null ){
        	DSUser[] subs=DSApi.context().getDSUser().getSubstitutedUsers();
        	String guid=null;
        	if(executionService.getVariable(task.getExecutionId(), "assignee")!=null){
        		assignee=(String) executionService.getVariable(task.getExecutionId(), "assignee");
        	}
        	if(executionService.getVariable(task.getExecutionId(), "guid")!=null){
        		guid=(String) executionService.getVariable(task.getExecutionId(), "guid");
        	}
        	for(int i=0;i<subs.length&&notSubst;i++){
        		if(subs[i].getName().equals(currentAssignee)){
        			List<JBPMTaskSnapshot> tasks=JBPMTaskSnapshot.findByDocumentId(document.getId());
        			for(int j=0;j<tasks.size()&&notSubst;j++){
        				if(tasks.get(j).getActivityKey().equals(task.getId())){
        					if(subs[i].getSubstitutedFrom().before(tasks.get(j).getReceiveDate())){
        						//wszystkie dokumenty, ktorych nie zaakceptowal zastepowany, a robi to zastepujacy wracaja do zastepowanego
        						//zakladam, ze uzytkownik, ktory jest zastepowany nie dekretuje na zastepujacego,
        						// gdyz inaczej wroci do zastepowanego
        						assignee=currentAssignee;
        						notSubst=false;
        					}
        				}
        			}
        		}
        		else if(subs[i].getName().equals(assignee)){
        			List<JBPMTaskSnapshot> tasks=JBPMTaskSnapshot.findByDocumentId(document.getId());
        			for(int j=0;j<tasks.size()&&notSubst;j++){
        				if(tasks.get(j).getActivityKey().equals(task.getId())){
        					if(subs[i].getSubstitutedFrom().before(tasks.get(j).getReceiveDate())){
        						//w przypadku, gdy byly dokumenty przedekretowane na zastepowanego
        						notSubst=false;
        					}
        				}
        			}
        		}
        		else if(guid!=null){
        			List<JBPMTaskSnapshot> tasks=JBPMTaskSnapshot.findByDocumentId(document.getId());
        			for(int j=0;j<tasks.size()&&notSubst;j++){
        				if(tasks.get(j).getActivityKey().equals(task.getId())){
        					DSUser[] userDiv=DSDivision.find(guid).getUsers(true);
        					for(int k=0;k<userDiv.length&&notSubst;k++){
        						if(subs[i].equals(userDiv[k])){
        							if(userDiv[k].getSubstitutedFrom().before(tasks.get(j).getReceiveDate())){
        								//w przypadku, gdy byly dokumenty przedekretowane na grupe, gdzie jest zastepowany/zastepowani, wowczas trafi do jednego z nich
        								assignee=userDiv[k].getName();
        								notSubst=false;
        							}
        						}
        					}
        				}
        			}
        		}
        	}
        }

        if(notSubst)
        {	
        	Jbpm4Provider.getInstance().getTaskService().assignTask(task.getId(), DSApi.context().getPrincipalName());
        	executionService.setVariable(task.getExecutionId(), "previousAssignee", currentAssignee);
        	executionService.setVariable(task.getExecutionId(), "currentAssignee", DSApi.context().getPrincipalName());
        	executionService.setVariable(task.getExecutionId(), DS_DIVISIIN, division);
        }
        else{
        	Jbpm4Provider.getInstance().getTaskService().assignTask(task.getId(), assignee);
        }
        LOG.info("ACCEPTED " + DSApi.context().getPrincipalName());

        if (document.getDocumentKind() != null)
        {
            document.getDocumentKind().logic().acceptTask(document, event, task.getId());
        }
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(executionService.findExecutionById(
                task.getExecutionId()).getProcessDefinitionId());
        ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_ACCEPT_TASK);
        String divisions = "";

        for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
        {
            if (!divisions.equals(""))
                divisions = divisions + " / " + a.getName();
            else
                divisions = a.getName();
        }
        ahe.setSourceGuid(divisions);
        
        String status = null;
        try
        {
            status = document.getFieldsManager().getField("STATUS").getEnumItemByCn(task.getActivityName()).getTitle();
        }
        catch (Exception e)
        {
        	try {
        		status = document.getFieldsManager().getEnumItem("STATUS").getTitle();
			} catch (Exception e2) {
				LOG.warn("No STATUS field in dockind");
			}
        }

        if (status != null)
            ahe.setStatus(status);
        
        
        ahe.setCtime(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        document.addAssignmentHistoryEntry(ahe);
    }

    private static void isXesLogDecretation(ExecutionService executionService, Task task, OfficeDocument document) throws UserNotFoundException, EdmException
	{
    	if(AvailabilityManager.isAvailable("XesLogApi")){
    		String previousAssignee =  (String) executionService.getVariable(task.getExecutionId(), "previousAssignee");
       	  String currentAssignee =  document.getCurrentAssignmentUsername();
       	  if(!previousAssignee.equals(currentAssignee)){
       		  XesLog.createAcceptedDecretation(document,previousAssignee,currentAssignee);
       	  }
    	}
    	
    	 
	}

	@Override
    public String getProcessName(String activityId) throws EdmException {
        activityId = cleanActivityId(activityId);
        Task task = taskService().getTask(activityId);
        String procDefId = executionService().findExecutionById(task.getExecutionId()).getProcessDefinitionId();
        ProcessDefinition pd = repositoryService().createProcessDefinitionQuery().processDefinitionId(procDefId).uniqueResult();
        return pd.getName();
    }
    

    private static TaskService taskService(){
        return Jbpm4Provider.getInstance().getTaskService();
    }

    private static ExecutionService executionService(){
        return Jbpm4Provider.getInstance().getExecutionService();
    }

    private static RepositoryService repositoryService(){
        return Jbpm4Provider.getInstance().getRepositoryService();
    }
    
    @Override
    public void reloadParticipantMapping() throws EdmException
    {
    	
    }
}