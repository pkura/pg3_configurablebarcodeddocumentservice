package pl.compan.docusafe.core.office.workflow.snapshot;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.UserDockindCounter.DockindCount;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.settings.TaskListAction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javassist.runtime.Desc;

public class JBPMTaskListCounter extends TaskListCounter
{
	private static Logger log = LoggerFactory.getLogger(JBPMTaskListCounter.class);
	@Override
	public void initCounter(boolean withBackup)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void reserUserTaskCount(String username)
	{
		if(usersCounter.get(username) != null)
			usersCounter.get(username).setMustReload(true);
	}
	
	private static TaskListCounterManager manager;
	
	
	private TaskListCounterManager manager()
	{
		if(manager == null)
			manager = new TaskListCounterManager(false);
		return manager;
	}

	@Override
	public UserCounter getUserCounter(String username,Long lastDate,boolean withBackup) throws EdmException
	{
		if(usersCounter == null)
			usersCounter = new HashMap<String, UserCounter>();
		UserCounter uc = usersCounter.get(username);
		if(uc == null || uc.isMustReload())
		{
			uc = new UserCounter();
			uc.setUsername(username);
			uc.setLastSent(new Date(lastDate));
			gatAllParam(uc,withBackup);
			uc.setLastReload(new Date());
			uc.setMustReload(Boolean.FALSE);
		}
		return uc;
	}

	private void gatAllParam(UserCounter uc, boolean withBackup) throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			
			log.debug("Liczy dla: "+uc.getUsername());
			DSUser user = DSUser.findByUsername(uc.getUsername());
			DSUser[] users = user.getSubstituted();
			StringBuilder sb = new StringBuilder("select ");
			/**Wszystkie zadania*/
			sb.append(" ( select COUNT(1) from DSW_JBPM_TASKLIST where  ").append(dswResources(users,uc.getUsername()));
			if(!withBackup)
				sb.append(" and backup_Person <> 1 ");
			/**Nowe zadania*/
			sb.append(") as AllTask, (select COUNT(1) from DSW_JBPM_TASKLIST where  ").append(dswResources(users,uc.getUsername())).append(" and last_state_time > ? ");
			if(!withBackup)
				sb.append(" and backup_Person <> 1 ");
			/**Nowe zaakceptowane*/
			sb.append(") as NewTask, (select COUNT(1) from DSW_JBPM_TASKLIST where  ").append(dswResources(users,uc.getUsername())).append(" and last_state_time > ? and accepted > 0 ");
			if(!withBackup)
				sb.append(" and backup_Person <> 1 ");
			/**Stare zaakceptowane*/
			sb.append(") as NewAcceptedTask, (select COUNT(1) from DSW_JBPM_TASKLIST where  ").append(dswResources(users,uc.getUsername())).append(" and last_state_time <= ? and accepted > 0 ");
			if(!withBackup)
				sb.append(" and backup_Person <> 1 ");
			sb.append(" ) as OldAcceptedTask ");
			if(DSApi.isOracleServer())
				sb.append(" from dual ");
			
			log.debug("SQL: "+sb.toString());
			
			
			StringBuilder sbAll = new StringBuilder("select COUNT(1) from DSW_JBPM_TASKLIST where  ").append(dswResources(users,uc.getUsername()));
			if(!withBackup)
				sb.append(" and backup_Person <> 1 ");
			
			StringBuilder sbNew = new StringBuilder("select COUNT(1) from DSW_JBPM_TASKLIST where  ").append(dswResources(users,uc.getUsername())).append(" and last_state_time > ? ");
			if(!withBackup)
				sb.append(" and backup_Person <> 1 ");
			
			StringBuilder sbNewAcceptedTask = new StringBuilder("select COUNT(1) from DSW_JBPM_TASKLIST where  ").append(dswResources(users,uc.getUsername())).append(" and last_state_time > ? and accepted > 0 ");
			if(!withBackup)
				sb.append(" and backup_Person <> 1 ");
			
			StringBuilder sbOldAcceptedTask = new StringBuilder("select COUNT(1) from DSW_JBPM_TASKLIST where  ").append(dswResources(users,uc.getUsername())).append(" and last_state_time <= ? and accepted > 0 ");
			if(!withBackup)
				sb.append(" and backup_Person <> 1 ");

			String dockindsType = "";		
			
//			if(clientAdds != null && Docusafe.getAdditionProperty("client").equals("play")){
			if(Docusafe.getAdditionProperty("dockindsToMail") != null && !Docusafe.getAdditionProperty("dockindsToMail").equals("")){
				String [] availabledockinds = Docusafe.getAdditionProperty("dockindsToMail").split(",");
				for(String dockindCn : availabledockinds){
					if(!dockindCn.equals("")){
//						w dockindsType zostana przechowane wylacznie zweryfikowane/poprawne nazwy dockindow
						if(DocumentKind.findByCn(dockindCn.trim()) != null){
							dockindsType += "'" + DocumentKind.findByCn(dockindCn.trim()).getName() + "',";
						}
					}
				}
				if(!dockindsType.equals("")){
					if(dockindsType.charAt(dockindsType.length()-1) == ',')
						dockindsType = dockindsType.substring(0, dockindsType.length()-1);
					sbAll.append(" and dockind_name IN (" + dockindsType + ")");
				}
			}
			
			ps =  DSApi.context().prepareStatement(sbAll.toString());
			rs = ps.executeQuery();	
			rs.next();
			Integer i1 = rs.getInt(1);
			rs.close();
			ps.close();

            DSApi.context().closeStatement(ps);
			ps =  DSApi.context().prepareStatement(sbNew.toString());
			ps.setTimestamp(1, new java.sql.Timestamp(uc.getLastSent().getTime()));
			rs = ps.executeQuery();		
			rs.next();
			Integer i2 = rs.getInt(1);
			rs.close();
			ps.close();

            DSApi.context().closeStatement(ps);
			ps =  DSApi.context().prepareStatement(sbNewAcceptedTask.toString());
			ps.setTimestamp(1, new java.sql.Timestamp(uc.getLastSent().getTime()));
			rs = ps.executeQuery();	
			rs.next();
			Integer i3 = rs.getInt(1);
			rs.close();
			ps.close();

            DSApi.context().closeStatement(ps);
			ps =  DSApi.context().prepareStatement(sbOldAcceptedTask.toString());
			ps.setTimestamp(1, new java.sql.Timestamp(uc.getLastSent().getTime()));
			rs = ps.executeQuery();	
			rs.next();
			Integer i4 = rs.getInt(1);
			rs.close();
			ps.close();
			
			
			
			
			
		/*	ps = DSApi.context().prepareStatement(sb.toString());
			ps.setDate(1, new java.sql.Date(uc.getLastSent().getTime()));
			ps.setDate(2, new java.sql.Date(uc.getLastSent().getTime()));
			ps.setDate(3, new java.sql.Date(uc.getLastSent().getTime()));
			log.debug("TIME :"+uc.getLastSent().getTime());
			log.debug("DATE: "+ DateUtils.formatCommonDateTime(uc.getLastSent()));
			rs= ps.executeQuery();
			if(rs.next())
			{*/
				
				uc.setAllTask(i1);
				uc.setNewTask(i2);
				uc.setNewAcceptedTask(i3);
				uc.setNewNotAcceptedTask(uc.getNewTask() - uc.getNewAcceptedTask());
				uc.setOldTask(uc.getAllTask() - uc.getNewTask());
				uc.setOldAcceptedTask(i4);
				uc.setOldNotAcceptedTask(uc.getOldTask() - uc.getOldAcceptedTask());
			//}
		}
		catch (SQLException e)
		{
			log.error("",e);
			throw new EdmException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
		}
	}

	private String dswResources(DSUser[] users,String username)
	{
		StringBuilder sb = new StringBuilder(" assigned_resource ");
		if(users != null && users.length > 0)
		{
			sb.append(" in( ");
			for (int i = 0; i < users.length;i++)
			{
				sb.append("'").append(users[i].getName()).append("',");
			}
			//sb.deleteCharAt(sb.length()-1);
			sb.append("'").append(username).append("' ");
			sb.append(")");
		}
		else
		{
			sb.append(" = '").append(username).append("' ");
		}
		return sb.toString();
	}
	

	public Integer getTaskListCount(String username, boolean withBackup,String documentType,Long bookamrkId) throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			DSUser user = DSUser.findByUsername(username);
			DSUser[] users = user.getSubstitutedUsers();
			if(documentType == null || documentType == "")
				throw new EdmException("Nieprawid�owy typ dokumentu");

			StringBuilder sql = new StringBuilder("select ");

            if (AvailabilityManager.isAvailable("bookmark.TaskListCountWithDistinctActivity"))
                sql.append(" count (distinct activity_key) ");
            else
                sql.append(" COUNT(1) ");
            sql.append(" from DSW_JBPM_TASKLIST where ").append(dswResources(users,username));
            if  (!AvailabilityManager.isAvailable("disabledIlpolHack"))
                sql.append(" and (case_id = 0 or case_id is null ) ");
			//sql.append("and accepted = 0  ").append(manager().bookamrk(bookamrkId)).append("  document_type = ?");
			sql.append("and accepted = ?  ").append(manager().bookamrk(bookamrkId)).append("  document_type = ?");
			if (AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow"))
			{
				 if(prepereCountWtchDocumentsPackage(sql, documentType ))
					 documentType = OutOfficeDocument.TYPE;
			}
			ps = DSApi.context().prepareStatement(sql.toString());
			ps.setBoolean(1, false);
            ps.setString(2, documentType);
			rs = ps.executeQuery();
			
			if(!rs.next()) 
				throw new EdmException("Nie mo�na pobra� liczby niezaakceptowanych zada�");
			return rs.getInt(1);
		}
		catch (SQLException e)
		{
			log.error("",e);
			throw new EdmException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
		}
	}
	/**
	 * metoda zwraca stringa z opisem ilosci poszczegolnych zadan dla uzytkownika np:
	 * "Pism na li�cie zada� - Przychodz�cych 50 / Wychod��cych 287
	 * @param username
	 * @param withBackup
	 * @return
	 * @throws EdmException
	 */
	public String getTaskListUserCount(String username, boolean withBackup) throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder desc = new StringBuilder(" ( Pism na li�cie zada� - ");
		try
		{
			DSUser user = DSUser.findByUsername(username);
			DSUser[] users = user.getSubstitutedUsers();
			StringBuilder sql = new StringBuilder("select ");
			sql.append(" count (id) as count ,DOCUMENT_TYPE from DSW_JBPM_TASKLIST");
			sql.append(" where  ASSIGNED_RESOURCE=? group by DOCUMENT_TYPE order by DOCUMENT_TYPE");

			ps = DSApi.context().prepareStatement(sql.toString());
			ps.setString(1, username);
			rs = ps.executeQuery();
			
			
			while(rs.next()){
				Long count	 = rs.getLong("count");
				String type	 = rs.getString("DOCUMENT_TYPE");
			
				if(DocumentType.INCOMING.toString().equals(type)){
					desc.append("Przychodzacych :"+count+" / ");
				}else if (DocumentType.INTERNAL.toString().equals(type)){
					
				}else if (DocumentType.OUTGOING.toString().equals(type)){
					desc.append("Pism :"+count);
				}
			}
			
		}
		
		catch (SQLException e)
		{
			log.error("",e);
			throw new EdmException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
		}
		desc.append(")");
		return desc.toString();
	}
	private boolean prepereCountWtchDocumentsPackage(StringBuilder sql, String documentType) throws EdmException
	{
		String dockindPackage = null;
		String dockindName = null;
		if (Docusafe.getAdditionProperty("kind-package") != null && !Docusafe.getAdditionProperty("kind-package").isEmpty())
		{
			dockindPackage = Docusafe.getAdditionProperty("kind-package");
			dockindName = DocumentKind.findByCn(dockindPackage).getName();
		}

		if (dockindName != null && documentType.contains(OutOfficeDocument.TYPE) || documentType.contains(InOfficeDocument.TYPE))
		{
			sql.append(" and dockind_name not like '" + dockindName + "'");

			return false;
		} else if (dockindName != null && documentType.contains(TaskListAction.TAB_DOCUMENT_PACKAGE))
		{
			sql.append(" and dockind_name like '" + dockindName + "'");
			documentType = OutOfficeDocument.TYPE;
			return true;
		} else
			return false;
	}

	@Override
	public UserDockindCounter getUserDockindCounter(String username,
			boolean withBackup, String documentType, Long bookmarkId)
			throws EdmException {
		UserDockindCounter counter = new UserDockindCounter();
		Map<String, DockindCount> dockindCountMap = new HashMap<String, DockindCount>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			DSUser user = DSUser.findByUsername(username);
			DSUser[] users = user.getSubstituted();
			counter.setUsername(username);
			counter.setDockindCount(dockindCountMap);
			
			ps = DSApi.context().prepareStatement("SELECT COUNT(1), dockind_name, accepted from DSW_JBPM_TASKLIST where " + dswResources(users, username) 
					+ " " + manager().bookamrk(bookmarkId) + " document_type = ? GROUP BY accepted, dockind_name ORDER BY dockind_name");
			ps.setString(1, documentType);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				int count = rs.getInt(1);
				String dockindName = rs.getString(2);
				boolean accepted = rs.getBoolean(3);
				DockindCount dockindCount;
				if(!dockindCountMap.containsKey(dockindName)) {
					dockindCount = new DockindCount();
					dockindCountMap.put(dockindName, dockindCount);
				} else {
					dockindCount = dockindCountMap.get(dockindName);
				}
				if(accepted) {
					dockindCount.accepted = count;
					counter.setAccepted(counter.getAccepted() + count);
				} else {
					dockindCount.notAccepted = count;
					counter.setNotAccepted(counter.getNotAccepted() + count);
				}
				dockindCount.all += count;
				counter.setAll(counter.getAll() + count);
			}
			
		} catch(SQLException e) {
			log.error("",e);
			throw new EdmException(e);
		} finally {
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
		}
		
		return counter;
	}
}
