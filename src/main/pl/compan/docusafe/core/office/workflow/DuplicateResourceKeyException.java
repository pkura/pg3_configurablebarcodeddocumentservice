package pl.compan.docusafe.core.office.workflow;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DuplicateResourceKeyException.java,v 1.1 2005/04/12 14:52:27 lk Exp $
 */
public class DuplicateResourceKeyException extends WorkflowException
{
    public DuplicateResourceKeyException(String key)
    {
        super("Istnieje ju� zas�b (u�ytkownik) o nazwie "+key);
    }

    public DuplicateResourceKeyException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DuplicateResourceKeyException(Throwable cause)
    {
        super(cause);
    }
}
