package pl.compan.docusafe.core.office.workflow;

/* User: Administrator, Date: 2005-06-13 15:08:11 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkflowResourceNotFoundException.java,v 1.1 2005/06/13 15:22:17 lk Exp $
 */
public class WorkflowResourceNotFoundException extends WorkflowException
{
    public WorkflowResourceNotFoundException(String message)
    {
        super(message);
    }

    public WorkflowResourceNotFoundException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public WorkflowResourceNotFoundException(Throwable cause)
    {
        super(cause);
    }
}
