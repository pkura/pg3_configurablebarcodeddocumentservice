package pl.compan.docusafe.core.office.workflow.snapshot;

import java.util.Map;

/**
 * Zawiera informacje o liczbie dokumentow na liscie zadan uzytkownika
 * z podzialem na dockindy i akceptacje zadan 
 * @author Kamil
 *
 */
public class UserDockindCounter {
	/**
	 * Nazwa u�ytkownika
	 */
	private String username;
	
	/**
	 * Liczba wszystkich zaakceptowanych zada�
	 */
	private int accepted;
	
	/**
	 * Liczba wszystkich niezaakceptowanych zada�
	 */
	private int notAccepted;

	/**
	 * Liczba wszystkich zada�
	 */
	private int all;
	
	public static class DockindCount {
		/**
		 * Liczba zaakceptowanych zadan
		 */
		int accepted;
		
		/**
		 * Liczba niezaakceptowanych (nowych) zadan
		 */
		int notAccepted;
		
		/**
		 * Liczba wszystkich zadan
		 */
		int all;
		
		DockindCount() {}
		
		DockindCount(int accepted, int notAccepted) {
			this.accepted = accepted;
			this.notAccepted = notAccepted;
			this.all = accepted + notAccepted;
		}

		public int getAccepted() {
			return accepted;
		}

		public void setAccepted(int accepted) {
			this.accepted = accepted;
		}

		public int getNotAccepted() {
			return notAccepted;
		}

		public void setNotAccepted(int notAccepted) {
			this.notAccepted = notAccepted;
		}

		public int getAll() {
			return all;
		}

		public void setAll(int all) {
			this.all = all;
		}
	}
	
	/**
	 * Mapa dockind_name -> DockindCount
	 */
	private Map<String, DockindCount> dockindCount;

	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAccepted() {
		return accepted;
	}

	public void setAccepted(int accepted) {
		this.accepted = accepted;
	}

	public int getNotAccepted() {
		return notAccepted;
	}

	public void setNotAccepted(int notAccepted) {
		this.notAccepted = notAccepted;
	}

	public int getAll() {
		return all;
	}

	public void setAll(int all) {
		this.all = all;
	}

	public Map<String, DockindCount> getDockindCount() {
		return dockindCount;
	}

	public void setDockindCount(Map<String, DockindCount> dockindCount) {
		this.dockindCount = dockindCount;
	}
}
