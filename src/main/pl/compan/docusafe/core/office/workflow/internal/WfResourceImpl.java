package pl.compan.docusafe.core.office.workflow.internal;

import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.office.workflow.WfAssignment;
import pl.compan.docusafe.core.office.workflow.WfResource;
import pl.compan.docusafe.core.office.workflow.WorkflowException;

/**
 * Odpowiada u�ytkownikom, w przysz�o�ci mo�e innym encjom.
 * Kiedy tworzy�? Przy starcie aplikacji?
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfResourceImpl.java,v 1.10 2010/05/17 11:35:52 pecet1 Exp $
 */
public class WfResourceImpl implements WfResource
{
    private Long id;
    /**
     * Nazwa u�ytkownika.
     */
    private String key;
    private String name;
    private String email;
    private Set<WfAssignment> assignments;

    public WfResourceImpl()
    {
    }

    /**
     * Zwraca nazw� u�ytkownika.
     */
    public String resource_key() throws WorkflowException
    {
        return key;
    }

    /**
     * Zwraca nazw� u�ytkownika.
     */
    public String resource_name() throws WorkflowException
    {
        return name;
    }

    /**
     * Zwraca null.
     */
    public String email_address() throws WorkflowException
    {
        return email;
    }

    /**
     * Zwraca null.
     */
    public String password() throws WorkflowException
    {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda znajduje si� tu dla kompletno�ci API, ale nie powinna by�
     * u�ywana, poniewa� jest ma�o wydajna.
     */
    public WfAssignment[] get_sequence_work_item() throws WorkflowException
    {
        if (assignments == null)
        {
            return new WfAssignment[0];
        }

        return (WfAssignment[]) assignments.toArray(new WfAssignment[assignments.size()]);
    }

    public int[] getTaskCount() throws WorkflowException
    {
        //String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
        try
        {
        	String sql = "select count(*) from a in class "+WfAssignmentImpl.class.getName()+ " where a.resource.id = ? and accepted= ?";
        	org.hibernate.Query q1 = DSApi.context().session().createQuery(sql);
        	q1.setLong(0,id);
        	q1.setBoolean(1, true);
        	List accepted = q1.list();

        	org.hibernate.Query q2 = DSApi.context().session().createQuery(sql);
        	q2.setLong(0,id);
        	q2.setBoolean(1, false);
        	List unaccepted = q2.list();
                        
            return new int[] { ((Long) accepted.get(0)).intValue(),
                               ((Long) unaccepted.get(0)).intValue() };
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(new EdmHibernateException(e));
        }
    }

    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }

    public String getKey()
    {
        return key;
    }

    void setKey(String key)
    {
        this.key = key;
    }

    public String getName()
    {
        return name;
    }

    void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    void setEmail(String email)
    {
        this.email = email;
    }

    public Set getAssignments()
    {
        return assignments;
    }

    private void setAssignments(Set<WfAssignment> assignments)
    {
        this.assignments = assignments;
    }


    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof WfResourceImpl)) return false;

        final WfResourceImpl wfResource = (WfResourceImpl) o;

        if (id != null ? !id.equals(wfResource.id) : wfResource.id != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public String toString()
    {
        return getClass().getName()+"[id="+id+" key="+key+"]";
    }
}
