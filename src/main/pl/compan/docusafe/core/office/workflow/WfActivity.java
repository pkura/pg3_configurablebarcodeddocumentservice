package pl.compan.docusafe.core.office.workflow;

import pl.compan.docusafe.core.office.workflow.xpdl.Activity;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfActivity.java,v 1.4 2008/09/09 10:33:21 pecet4 Exp $
 */
public interface WfActivity extends WfExecutionObject
{
    public static final String STATE_OPEN_RUNNING = "open.running";

    Activity getActivityDefinition() throws WorkflowException;
    String getWorkflowName() throws WorkflowException;

    String activity_definition_id() throws WorkflowException;
    void complete() throws WorkflowException;
    void set_result(NameValue[] result) throws WorkflowException;
    NameValue[] result() throws WorkflowException;
    NameValue[] extended_attributes() throws WorkflowException;
    NameValue[] process_context() throws WorkflowException;
    void set_process_context(NameValue[] context) throws WorkflowException;
    WfProcess container() throws WorkflowException;
}
