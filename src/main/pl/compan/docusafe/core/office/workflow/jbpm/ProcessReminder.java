package pl.compan.docusafe.core.office.workflow.jbpm;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.DateUtils;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Criteria;
/* User: Administrator, Date: 2007-01-22 14:39:54 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 * @deprecated - funkcja wygaszana
 */
@Deprecated
public class ProcessReminder
{
    /** 
     * informowanie użytkownika podczas gdy 'status zawieszony' o wpłynięciu podpisanej kopii polisy,
     * podpisanej kopii aneksu lub korespondencji przychodzącej (tworzenie tego remindera jest 
     * w {@link CesjaNaOsobeZawieszonyAction})
     */
    public static final Integer TYP_KOPIA_POLISY_ANEKSU_KOR_PRZYCH = 1;
    /** 
     * informowanie użytkownika podczas gdy 'status zawieszony' o wpłynięciu korespondencji przychodzącej 
     * (tworzenie tego remindera jest w {@link CesjaNaOsobeZawieszonyAction})
     */
    public static final Integer TYP_KOR_PRZYCH = 2;
    /** 
     * informowanie użytkownika podczas realizacji zadania OPS o wpłynięciu dokumentów o tym samym nr polisy
     * (tworzenie tego remindera jest w {@link HistoryActionHandler} 
     */
    public static final Integer TYP_DOKUMENT_O_DANYM_NR_POLISY = 3;
    
    private Long id;
    private Date checkTime;
    private Integer reminderType;
    private Long jbpmTaskId;
    private Long lparam;
    private String wparam;

    public ProcessReminder()
    {

    }

    public synchronized void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException
    {
         Persister.delete(this);
    }

    public static List<ProcessReminder> findObjectsNeedCheck() throws EdmException
    {
        Date now = DateUtils.getCurrentTime();
        Criteria c = DSApi.context().session().createCriteria(ProcessReminder.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.le("checkTime", now));

            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getCheckTime()
    {
        return checkTime;
    }

    public void setCheckTime(Date checkTime)
    {
        this.checkTime = checkTime;
    }

    public Integer getReminderType()
    {
        return reminderType;
    }

    public void setReminderType(Integer reminderType)
    {
        this.reminderType = reminderType;
    }

    public Long getJbpmTaskId()
    {
        return jbpmTaskId;
    }

    public void setJbpmTaskId(Long jbpmTaskId)
    {
        this.jbpmTaskId = jbpmTaskId;
    }

    public Long getLparam()
    {
        return lparam;
    }

    public void setLparam(Long lparam)
    {
        this.lparam = lparam;
    }

    public String getWparam()
    {
        return wparam;
    }

    public void setWparam(String wparam)
    {
        this.wparam = wparam;
    }
}
