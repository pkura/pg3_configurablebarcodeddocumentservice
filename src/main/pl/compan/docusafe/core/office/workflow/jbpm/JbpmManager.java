package pl.compan.docusafe.core.office.workflow.jbpm;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.boot.Docusafe;

import java.util.*;

import org.jbpm.JbpmContext;
/* User: Administrator, Date: 2007-02-14 11:19:42 */
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.taskmgmt.exe.TaskInstance;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 * @deprecated - bylo to pisane pod Nationwide - juz nie uzywane - nastepne implementacje beda robione inaczej
 */
@Deprecated
public class JbpmManager
{
    /**
     * Sprawdza czy mozna dla danego dokumentu zainicjowac proces JBPM.
     * Dokument musi by� biznesowy i mie� numer polisy r�ny od null.
     */
    public static boolean canInitJbpmProcess(Document document, String activity) throws EdmException
    {
    	return false;
    }

    /**
     * Sprawdza czy na ekranie poksumowania ma sie pojawiac ostrzezenie w przypadku braku przypisania
     * dokumentu do pudla. Ma sie ono pojawiac tylko dla dokumentow biznesowych (doctype != null) poza faksami.
     * Dodatkowe zabezpiecznie w przypadkach, gdy ustawiony doctype 'nationwide', a nie ustaiowny nr_polisy.
     */
    public static boolean showNotInBoxMessage(Document document) throws EdmException
    {
        return false;
    }

    /**
     * Zwraca map� z wszystkimi zarejestrowanymi definicjami proces�w w systemie.
     */
    public static Map<Long,String> prepareJbpmProcesses() throws EdmException
    {
        List<ProcessDefinition> list = ProcessDefinition.findOnlyNewestDefinitions();//findAll();

        Map<Long,String> jbpmProcesses = new LinkedHashMap<Long,String>();

        for (Iterator iter = list.iterator(); iter.hasNext();)
        {
            ProcessDefinition definition = (ProcessDefinition) iter.next();

            jbpmProcesses.put(definition.getDefinitionId(), definition.getNameForUser());
        }

        return jbpmProcesses;
    }

    /**
     * Zwraca domy�lny proces JBPM na podstawie typu dokumentu, kt�ry ma podany dokument.
     */
    public static Long defaultProcess(Document document) throws EdmException
    {

        Map vm = document.getDoctype().getValueMapByCn(document.getId());
        Integer typeId = (Integer) vm.get("RODZAJ");

        Doctype.Field rodzajField = document.getDoctype().getFieldByCn("RODZAJ");

        String processName = null;
        
        if (processName != null)
        {
            // pobieram najnowsza definicj� dla danej nazwy
            ProcessDefinition definition = ProcessDefinition.findNewestByDefinitionName(processName);

            if (definition != null)
                 return definition.getDefinitionId();
        }

        return null;
    }
    
    
    /**
     * Generuje obiekty opisuj�ce aktualny stan proces�w JBPM zwi�zanych z zadanym dokumentem.
     * 
     * @param document
     * @return
     * @throws EdmException
     */
    public static List<ProcessSummary> createProcessSummaries(Document document) throws EdmException
    {
        TaskSnapshot.Query query = new TaskSnapshot.Query();
        query.setDocumentId(document.getId());
        query.setExternalWorkflow(Boolean.TRUE);
        SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);        
        Map<Long,List<TaskSnapshot>> processes = groupTasks(results);
        List<ProcessSummary> summaries = new ArrayList<ProcessSummary>();
        
        JbpmContext jbpmContext = Docusafe.getJbpmConfiguration().createJbpmContext();
        try
        {
            for (Long processId : processes.keySet())
            {
                summaries.add(createSummary(jbpmContext, processId, processes.get(processId)));
            }
            return summaries;
        }
        finally
        {
            jbpmContext.close();
        }
    }
    
    /**
     * Grupuje zadania z wynik�w wyszukiwania wed�ug procesu JBPM, kt�rego te zadani dotycz�.
     * 
     * @param results
     * @return mapa indeksowana po identyfikatorach proces�w, kt�rej warto�ciami s� listy zada� w tych procesach
     */
    private static Map<Long,List<TaskSnapshot>> groupTasks(SearchResults<TaskSnapshot> results)
    {
        Map<Long,List<TaskSnapshot>> processes = new LinkedHashMap<Long, List<TaskSnapshot>>();
        for (TaskSnapshot task : results.results())
        {
            List<TaskSnapshot> list = processes.get(task.getExternalProcessId());
            if (list == null)
            {
                list = new ArrayList<TaskSnapshot>();
                processes.put(task.getExternalProcessId(), list);
            }
            list.add(task);
            
        }
        return processes;
    }
    
    /**
     * Tworzy obiekt opisuj�cy aktualny stan procesu o zadanym identyfikatorze. 
     * 
     * @param jbpmContext
     * @param processId identyfikator procesu
     * @param tasks lista zada� w tym procesie
     * @return
     * @throws EdmException
     * 
     * TODO: mo�e b�dzie konieczne w przysz�o�ci doda� rozr�nienie w zale�no�ci od definicj procesu
     *       bo niekt�re procesy mog� ��da� wy�wietlania specyficznych informacji dla danej definicji
     */
    private static ProcessSummary createSummary(JbpmContext jbpmContext, Long processId, List<TaskSnapshot> tasks) throws EdmException
    {
        ProcessInstance processInstance = jbpmContext.getProcessInstance(processId);
        ProcessDefinition definitionInfo = ProcessDefinition.findByDefinitionId(processInstance.getProcessDefinition().getId());
        ProcessSummary summary = new ProcessSummary();
        summary.setProcessName(definitionInfo.getNameForUser());        
        for (TaskSnapshot task : tasks)
        {
            TaskInstance taskInstance = jbpmContext.getTaskInstance(task.getExternalTaskId());
            
            if (summary.getDocumentStatus() == null)
                // ka�de zadanie powinno mie� taki sam status na li�cie zada� wi�c pobieram z "pierwszego lepszego"
                summary.setDocumentStatus(task.getObjective());
            
            TaskDefinition taskDefinition = TaskDefinition.find(taskInstance.getName(), definitionInfo.getDefinitionName());
            String taskName = (taskDefinition != null ? taskDefinition.getNameForUser() : taskInstance.getName());
            if (taskInstance.getActorId() != null)
                // zadanie ma przydzielonego aktora
                summary.getActiveTasks().put(taskName, DSUser.safeToFirstnameLastname(task.getDsw_resource_res_key()));
            else
            {
               // nikt jeszcze nie klikn�� z dzia�u na to zadanie
               if (summary.getActiveTasks().get(taskName) == null)
               {
                   // jak przetwarzam pierwsze takie zadanie to wpisuje jako aktora zadania ca�y dzia�
                   String swimlaneName = taskInstance.getSwimlaneInstance().getName();
                   SwimlaneMapping swimlaneMapping = SwimlaneMapping.findByName(swimlaneName);
                   summary.getActiveTasks().put(taskName, DSDivision.find(swimlaneMapping.getDivisionGuid()).getName());
               }
            }
        }        
        return summary;
    }
    
    /**
     * Opisuje aktualny stan danego procesu JBPM.
     * 
     * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
     */
    public static class ProcessSummary
    {
        private String processName;
        /**
         * kluczem mapy jest nazwa zadania, a warto�ci� u�ytkownik, kt�ry to zadanie realizuje
         */
        private Map<String,String> activeTasks;
        private String documentStatus;                

        public ProcessSummary()
        {
            this.activeTasks = new LinkedHashMap<String,String>();
        }
        
        public Map<String, String> getActiveTasks()
        {
            return activeTasks;
        }

        public void setActiveTasks(Map<String, String> activeTasks)
        {
            this.activeTasks = activeTasks;
        }

        public String getDocumentStatus()
        {
            return documentStatus;
        }

        public void setDocumentStatus(String documentStatus)
        {
            this.documentStatus = documentStatus;
        }

        public String getProcessName()
        {
            return processName;
        }

        public void setProcessName(String processName)
        {
            this.processName = processName;
        }               
    }
}
