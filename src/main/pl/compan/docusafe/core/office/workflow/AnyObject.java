package pl.compan.docusafe.core.office.workflow;

import pl.compan.docusafe.core.EdmException;

/**
 * Klasa przykrywaj�ca instancj� org.omg.CORBA.Any.
 * <p>
 * Mapowanie typ�w IDL na typy javy jest nast�puj�ce:
 * <br>(tabela pochodzi ze strony
 * http://info.borland.com/techpubs/bes/v6/html_books/vbjava_dg/idl2java.html).

    <table border="2">
    <tr>
    <th>IDL type</th>
    <th>Java type</th>
    </tr>
    <tr>

            <td><code>boolean</code> </td>
            <td><code>boolean</code> </td>
    </tr><tr>
            <td><code>char</code> </td>
            <td><code>char</code> </td>

    </tr><tr>
            <td><code>wchar</code> </td>
            <td><code>char</code> </td>
    </tr><tr>
            <td><code>octet</code> </td>
            <td><code>byte</code> </td>

    </tr><tr>
            <td><code>string</code> </td>
            <td><code>java.lang.String</code> </td>
    </tr><tr>
            <td><code>wstring</code> </td>
            <td><code>java.lang.String</code> </td>

    </tr><tr>
            <td><code>short</code> </td>
            <td><code>short</code> </td>
    </tr><tr>
            <td><code>unsigned short</code> </td>
            <td><code>short</code> </td>

    </tr><tr>
            <td><code>long</code> </td>
            <td><code>int</code> </td>
    </tr><tr>
            <td><code>unsigned long</code> </td>
            <td><code>int</code> </td>

    </tr><tr>
            <td><code>longlong</code> </td>
            <td><code>long</code> </td>
    </tr><tr>
            <td><code>unsigned longlong</code> </td>
            <td><code>long</code> </td>

    </tr><tr>
            <td><code>float</code> </td>
            <td><code>float</code> </td>
    </tr><tr>
            <td><code>double</code> </td>
            <td><code>double</code> </td>

    </tr>
    </table>
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AnyObject.java,v 1.2 2006/02/06 16:05:51 lk Exp $
 */
public interface AnyObject
{
    /**
     * Zwraca warto�� zmiennej jako odpowiedni typ javy (np. dla zmiennej
     * typu {@link AnyObjectKind#LONG} jest to java.lang.Long.
     * <p>
     * Dla zmiennej typu {@link AnyObjectKind#ENUM} zwracana jest aktualnie wybrana
     * warto�� jako java.lang.String.
     */
    Object value() throws EdmException;
    /**
     * Typ zmiennej umieszczonej w obiekcie zwracanym przez funkcj� value().
     */
    AnyObjectKind kind();
    /**
     * Nadaje obiektowi now� warto��. Warto�� musi by� zgodna z typem okre�lonym
     * przez metod� {@link #kind()}, w przeciwnym wypadku rzucany jest wyj�tek
     * RuntimeException.
     */
    void set(Object value) throws EdmException;
}
