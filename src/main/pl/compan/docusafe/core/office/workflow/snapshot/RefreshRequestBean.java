package pl.compan.docusafe.core.office.workflow.snapshot;

/**
 * 
 * @author wkutyla
 * Klasa zawierajaca zlecenie na odswiezenie listy zadan po wykonanej transakcji"
 */
public class RefreshRequestBean implements java.io.Serializable
{
	static final long serialVersionUID = 29342;
	private Long id;
	private String doctype;
	private int updateType;
	private String username;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDoctype() {
		return doctype;
	}
	public void setDoctype(String doctype) {
		this.doctype = doctype;
	}
	public int getUpdateType() {
		return updateType;
	}
	public void setUpdateType(int updateType) {
		this.updateType = updateType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String toString()
	{
		return "RefreshRequestBean : "+id+" updateType "+updateType+" username "+username+" doctype "+doctype;
	}
}
