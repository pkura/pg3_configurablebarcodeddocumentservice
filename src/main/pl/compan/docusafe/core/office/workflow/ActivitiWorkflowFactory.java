package pl.compan.docusafe.core.office.workflow;

import com.google.common.collect.ImmutableMap;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.process.DocuSafeProcessLogic;
import pl.compan.docusafe.process.WorkflowEngine;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.Set;

/**
 *  Implementacja WorkflowFactory dla Activiti
 */
public class ActivitiWorkflowFactory extends WorkflowFactory {
    private final static Logger LOG = LoggerFactory.getLogger(ActivitiWorkflowFactory.class);

    private final WorkflowEngine workflowEngine;

    private static String cleanActivityId(String activityId){
        return activityId.replaceAll("[^,]*,","");
    }

    public ActivitiWorkflowFactory(WorkflowEngine docusafeActivitiWorkflowEngine) {
        this.workflowEngine = docusafeActivitiWorkflowEngine;
    }

    @Override
    public void reassign(String activity, String fromUser, Set<String> candidateUsers, Set<String> candidateDivisionGuids) throws EdmException {
        activity = cleanActivityId(activity);
        workflowEngine.reassingTask(activity, candidateUsers, candidateDivisionGuids);
        workflowEngine.setTaskParams(activity, ImmutableMap.of(DocuSafeProcessLogic.ASSIGNER_PARAM_KEY, fromUser));
    }

    @Override
    public String getProcessName(String activityId) throws EdmException {
        return workflowEngine.getTask(cleanActivityId(activityId)).getProcessDefinitionName();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void acceptTask(String act, ActionEvent event, OfficeDocument document, String division) throws EdmException {
        String activityId = cleanActivityId(act);
        LOG.info("assigning task {} to {}", activityId, DSApi.context().getPrincipalName());
        workflowEngine.assignTask(activityId, DSApi.context().getPrincipalName());
    }

    @Override
    public void assignToCoordinator(String activityId, ActionEvent event) throws EdmException {
        LOG.info("assignToCoordinator '{}'", activityId);
    }

    @Override
    public void manualFinish(String activityId, boolean checkCanFinish) throws EdmException {
        LOG.info("manualFinish '{}'", activityId);
    }

    @Override
    public Boolean checkCanAssignMe(Long documentId, String activityId) throws UserNotFoundException, EdmException {
        LOG.info("checkCanAssignMe '{}'", activityId);
        return false;
    }

    @Override
    public void reloadParticipantMapping() throws EdmException {
    }
}
