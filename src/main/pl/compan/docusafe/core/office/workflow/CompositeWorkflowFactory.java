package pl.compan.docusafe.core.office.workflow;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementacja WorkflowFactory korzystaj�ca z kilku implementacji WorkflowFactory,
 *
 * Obiekty tej klasy korzystaj� z prefiks�w warto�ci activiti do okre�lenia, do kt�rej
 * instancji WorkflowFactory przekaza� wywo�anie metody
 */
public class CompositeWorkflowFactory extends WorkflowFactory {
    private final static Logger LOG = LoggerFactory.getLogger(CompositeWorkflowFactory.class);
    /**
     * Mapa klucz -> instancja WorkflowFactory
     */
    final Map<String, WorkflowFactory> factories;

    public CompositeWorkflowFactory(Map<String, WorkflowFactory> keyToFactoryMap){
        LOG.info("creating CompositeWorkflowFactory({})", keyToFactoryMap);
        this.factories = checkNotNull(keyToFactoryMap);
    }

    @Override
    public void reassign(String activity, String fromUser, Set<String> candidateUsers, Set<String> candidateDivisionGuids) throws EdmException {
        String prefix = getProcessKey(activity);
        getWorkflowFactory(prefix).reassign(activity, fromUser, candidateUsers, candidateDivisionGuids);
    }

    @Override
    public String getProcessName(String activityId) throws EdmException {
        String prefix = getProcessKey(activityId);
        return getWorkflowFactory(prefix).getProcessName(activityId);
    }

    private String getProcessKey(String activityId) {
        checkNotNull(activityId, "activitiId cannot be null");
        return activityId.substring(0, activityId.indexOf(','));
    }

    @Override
    public void reloadParticipantMapping() throws EdmException {
        for(WorkflowFactory wf : factories.values()){
            wf.reloadParticipantMapping();
        }
    }

    @Override
    public void acceptTask(String activityId, ActionEvent event, OfficeDocument document, String division) throws EdmException {
        String prefix = getProcessKey(activityId);
        getWorkflowFactory(prefix).acceptTask(activityId, event, document, division);
    }

    @Override
    public void assignToCoordinator(String activityId, ActionEvent event) throws EdmException {
        LOG.info("assignToCoordinator '{}'", activityId);
        String prefix = getProcessKey(activityId);
        getWorkflowFactory(prefix).assignToCoordinator(activityId, event);
    }

    @Override
    public void manualFinish(String activityId, boolean checkCanFinish) throws EdmException {
        LOG.info("manualFinish '{}'", activityId);
        String prefix = getProcessKey(activityId);
        getWorkflowFactory(prefix).manualFinish(activityId, checkCanFinish);
    }

    @Override
    public Boolean checkCanAssignMe(Long documentId, String activityId) throws UserNotFoundException, EdmException {
        LOG.info("checkCanAssignMe '{}'", activityId);
        if(activityId == null){
            return false;
        }
        String prefix = getProcessKey(activityId);
        return getWorkflowFactory(prefix).checkCanAssignMe(documentId, activityId);
    }

    @Override
    public boolean isManual(WorkflowActivity act) throws WorkflowException {
        return false;
    }

    @Override
    public Map<String, Map<String, String>> prepareCollectiveAssignments(String activity) throws EdmException {
        return Collections.emptyMap();
    }

    private WorkflowFactory getWorkflowFactory(String prefix) {
        WorkflowFactory ret = factories.get(prefix);
        if(ret == null){
            throw new RuntimeException("Brak WorkflowFactory o kluczu '" + prefix + "'");
        }
        return ret;
    }
}
