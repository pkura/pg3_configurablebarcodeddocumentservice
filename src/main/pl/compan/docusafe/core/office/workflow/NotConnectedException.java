package pl.compan.docusafe.core.office.workflow;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NotConnectedException.java,v 1.1 2004/05/16 20:10:29 administrator Exp $
 */
public class NotConnectedException extends WorkflowException
{
    public NotConnectedException(String message)
    {
        super(message);
    }

    public NotConnectedException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NotConnectedException(Throwable cause)
    {
        super(cause);
    }
}
