package pl.compan.docusafe.core.office.workflow.internal;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.type.Type;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.ConnectFailedException;
import pl.compan.docusafe.core.office.workflow.NameValue;
import pl.compan.docusafe.core.office.workflow.NotConnectedException;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.TasklistSynchro;
import pl.compan.docusafe.core.office.workflow.WfProcess;
import pl.compan.docusafe.core.office.workflow.WfProcessMgr;
import pl.compan.docusafe.core.office.workflow.WfResource;
import pl.compan.docusafe.core.office.workflow.WfService;
import pl.compan.docusafe.core.office.workflow.WorkflowException;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowResourceNotFoundException;
import pl.compan.docusafe.core.office.workflow.xpdl.BasicDataType;
import pl.compan.docusafe.core.office.workflow.xpdl.WorkflowProcess;
import pl.compan.docusafe.core.office.workflow.xpdl.XpdlPackage;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StreamUtils;
import pl.compan.docusafe.util.StringManager;

/**
 * Klasa stanowi�ca punkt wej�ciowy do obs�ugi wewn�trznego workflow.
 * Mo�e obs�ugiwa� dowoln� liczb� definicji proces�w, kt�re s�
 * dodawane w metodzie {@link #initialize()}.
 * <p>
 * Przed rozpocz�ciem u�ywania klasa powinna zosta� zainicjalizowana
 * przy pomocy metody {@link #initialize()}.
 * <p>
 * Ka�da definicja pakietu jest zapisywana w bazie danych jako instancja
 * {@link WfPackageDefinition}, a nast�pnie s� z ni� wi�zane instancje
 * {@link WfProcessImpl}.
 * <p>
 * Je�eli definicja procesu zmieni si� mi�dzy uruchomieniami aplikacji, jest
 * to wykrywane i w bazie danych tworzony jest nowy obiekt {@link WfPackageDefinition}
 * odpowiadaj�cy zmienionemu pakietowi, w przeciwnym razie w bazie
 * wyszukiwany jest wcze�niej utworzony obiekt odpowiadaj�cy ka�demu
 * dodawanemu pakietowi.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InternalWorkflowService.java,v 1.77 2009/04/08 08:21:50 mariuszk Exp $
 */
public class InternalWorkflowService implements WfService
{
    private static final Log log = LogFactory.getLog(InternalWorkflowService.class);
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public static final String NAME = "internal";

    /**
     * Identyfikator uczestnika procesu (Participant), dla kt�rego
     * konkretni u�ytkownicy s� okre�leni przez zmienne procesu
     * (expression participant).
     */
    public static final String EXPRESSION_PARTICIPANT_ID = "docusafe_expression";
    /**
     * Nazwa rozszerzonego atrybutu procesu, w kt�rym znajduje si� nazwa
     * zmiennej procesu (Data field), kt�rej warto�ci� z kolei jest
     * identyfikator uczestnika procesu.
     */
    public static final String PARTICIPANT_ID_EXTATTR = "docusafe_participant";
    /**
     * Nazwa rozszerzonego atrybutu procesu, kt�rego warto�� okre�la
     * nazw� zmiennej procesu (Data field), w kt�rej znajduje si�
     * nazwa u�ytkownika aplikacji, na kt�rego li�cie zada� powinien
     * znale�� si� dany krok procesu (Activity).
     */
    //public static final String ASSIGNED_USER_EXTATTR = "docusafe_username_datafield";
    /**
     * Nazwa rozszerzonego atrybutu procesu, kt�rego warto�� okre�la
     * nazw� zmiennej procesu (Data field), w kt�rej znajduje si�
     * nazwa dzia�u aplikacji. Na listach zada� u�ytkownik�w w tych
     * dzia�ach powinien znale�� si� dany krok procesu (Activity).
     */
    //public static final String ASSIGNED_DIVISION_EXTATTR = "docusafe_division_datafield";

    /**
     * Aktualnie "pod��czony" u�ytkownik.
     */
    private WfResourceImpl wfResource;
    /**
     * Tablica mened�er�w procesu. Musi by� zmienn� instancyjn�, poniewa�
     * ka�da instancja InternalWorkflowService pobiera w�asn� tablic�
     * sklonowanych obiekt�w WfProcessMgr, kt�re posiadaj� referencj�
     * do pobieraj�cego je obiektu InternalWorkflowService. Dzieje si�
     * tak ze wzgl�du na konieczno�� przekazywania obiektu Context.
     */
    private WfProcessMgr[] processMgrs;
    /**
     * Tablica pakiet�w xpdl obs�ugiwanych przez ten serwis.
     * Kluczami tablicy s� identyfikator (Long) obiekt�w
     * WfPackageDefinition, warto�ciami obiekty XpdlPackage.
     * <p>
     * W tej tablicy mog� znajdowa� si� tylko aktualne pakiety,
     * dodane podczas inicjalizacji serwisu.
     */
    private static final Map<Long, XpdlPackage> xpdlPackages = new HashMap<Long, XpdlPackage>();
    /**
     * Tablica pakiet�w xpdl kt�re nie s� aktualne, ale wci�� odwo�uj�
     * si� do nich niekt�re procesy.
     * <p>
     * Kluczami tablicy s� identyfikator (Long) obiekt�w
     * WfPackageDefinition, warto�ciami obiekty XpdlPackage.
     */
    private static final Map<Long, XpdlPackage> outdatedXpdlPackages = new HashMap<Long, XpdlPackage>(10);

    /**
     * Inicjalizuje wewn�trzny system workflow: <br>
     * tworzy obiekty WfResourceImpl odpowiadaj�ce u�ytkownikom; <br>
     * odczytuje definicje wbudowanych proces�w workflow.
     * @param ctx 
     */
    public static void initialize(DSContext ctx) throws IOException, EdmException
    {	
    	if(AvailabilityManager.isAvailable("reloadParticipantMapping"))
    	{
    		WorkflowFactory.getInstance().reloadParticipantMapping();
    	}
    	
    	
        if (Configuration.isAdditionOn(Configuration.ADDITION_CLEAN_TASKLIST_ON_BOOT))
            //czyscimy skonczone zadania
            cleanClosedTask();
           if(Configuration.isAdditionOn(Configuration.ADDITION_RELOAD_TASKLIST_ON_BOOT) && !AvailabilityManager.isAvailable("tasklist.jbpm4"))
           {
        	   // synchronizacja snapshota
        	   TasklistSynchro.Synchro(ctx);
           }
           

	     
        byte[] xpdlBytes = StreamUtils.toByteArray(InternalWorkflowService.class.
            getClassLoader().getResourceAsStream("simple_process.xpdl"));

        try
        {
        	
            addPackage(xpdlBytes);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }

        // utworzenie u�ytkownik�w (obiekt�w WfResourceImpl)
        DSUser[] users = DSUser.list(DSUser.SORT_NONE).toArray(new DSUser[0]);

        PreparedStatement psSelRes = null;
        PreparedStatement psSelMap = null;
        try
        {
            //Set existingUsernames = new HashSet();

            psSelRes = DSApi.context().prepareStatement("select id from dsw_resource where res_key = ?");
            psSelMap = DSApi.context().prepareStatement("select resource_id from dsw_participant_mapping where " +
                    "participant_id = ? and resource_id = ?");

            for (int i=0; i < users.length; i++)
            {

                psSelRes.setString(1, users[i].getName());

                ResultSet rsSelRes = psSelRes.executeQuery();

                long resourceId;
                if (!rsSelRes.next())
                {
                    log.info("Tworzenie uzytkownika workflow "+users[i].getName());
                    WfResourceImpl wfResource = new WfResourceImpl();
                    wfResource.setKey(users[i].getName());
                    wfResource.setEmail(users[i].getEmail());
                    wfResource.setName(StringUtils.concatenate(
                        new String[] { users[i].getFirstname(), users[i].getLastname() }));
                    DSApi.context().session().save(wfResource);
                    DSApi.context().session().flush();
                    resourceId = wfResource.getId().longValue();
                }
                else
                {
                    resourceId = rsSelRes.getLong(1);
                }

                psSelMap.setString(1, users[i].getName());
                psSelMap.setLong(2, resourceId);
                ResultSet rsSelMap = psSelMap.executeQuery();

                // mapowanie tego u�ytkownika na uczestnika o tej samej nazwie
                if (!rsSelMap.next())
                {
                    log.info("Mapowanie u�ytkownika workflow "+users[i].getName()+
                        " na "+users[i].getName());

                    WfResourceImpl wfResource = (WfResourceImpl)
                        DSApi.context().session().load(WfResourceImpl.class, new Long(resourceId));

                    ParticipantMapping mapping = new ParticipantMapping();
                    mapping.setParticipantId(users[i].getName());
                    mapping.setResource(wfResource);

                    DSApi.context().session().save(mapping);
                    DSApi.context().session().flush();
                    
                }

                // mapowanie u�ytkownika na dzia�y, w kt�rych si� znajduje
                DSDivision[] divisions = users[i].getAllDivisions();
                

                if (divisions.length > 0)
                {
                    List<String> guids = new ArrayList<String>(16);
                    for (int d=0; d < divisions.length; d++)
                    {
                        guids.add(divisions[d].getGuid());
                        if (divisions[d].isPosition())
                            guids.add(divisions[d].getParent().getGuid());
                    }
                }
            }
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(psSelRes);
        	DSApi.context().closeStatement(psSelMap);            
        }
    }

    /**
     * Dodaje do {@link #xpdlPackages} nowy pakiet xpdl. Szuka w bazie
     * danych obiektu WfPackageDefinition odpowiadaj�cego dodawanemu
     * pakietowi (sprawdzaj�c pole hashId) i w razie potrzeby tworzy
     * nowy obiekt WfPackageDefinition.
     * @param xmlContent Tre�� pliku xpdl.
     */
    private static void addPackage(byte[] xmlContent)
        throws WorkflowException, EdmException, HibernateException
    {
        XpdlPackage xpdlPackage = new XpdlPackage(new ByteArrayInputStream(xmlContent));



        // sprawdzam, czy pakiet zosta� ju� dodany
        for (Iterator iter=xpdlPackages.values().iterator(); iter.hasNext(); )
        {
            XpdlPackage xp = (XpdlPackage) iter.next();
            if (xp.getId().equals(xpdlPackage.getId()) &&
                xp.getHashId().equals(xpdlPackage.getHashId()))
                throw new WorkflowException("Dodano ju� pakiet "+xpdlPackage.getId());
        }

        // szukam w bazie danych opisu tego pakietu
        List packageDefinitions = DSApi.context().classicSession().find(
            "from def in class "+WfPackageDefinition.class.getName()+
            " where def.packageId = ? and def.hashId = ?",
            new Object[] { xpdlPackage.getId(), xpdlPackage.getHashId() },
            new Type[] { Hibernate.STRING, Hibernate.STRING });

        // obiekt w bazie danych opisuj�cy pakiet xpdl
        WfPackageDefinition packageDefinition;

        if (packageDefinitions.size() > 0)
        {
            packageDefinition = (WfPackageDefinition) packageDefinitions.get(0);

            log.info("Znaleziono WfPackageDefinition "+packageDefinition);

            if (log.isTraceEnabled())
                log.trace("xml="+packageDefinition.getXml());
        }
        else
        {
            // tworzenie nowego WfPackageDefinition
            packageDefinition = new WfPackageDefinition();
            packageDefinition.setPackageId(xpdlPackage.getId());
            packageDefinition.setHashId(xpdlPackage.getHashId());
            try
            {
                packageDefinition.setXml(new String(xmlContent, "utf-8"));
            }
            catch (UnsupportedEncodingException e)
            {
                throw new WorkflowException("Brak wsparcia dla UTF-8", e);
            }

            log.info("Tworzenie nowego WfPackageDefinition "+packageDefinition);


            DSApi.context().session().save(packageDefinition);


        }

        xpdlPackages.put(packageDefinition.getId(), xpdlPackage);
    }

    static XpdlPackage getXpdlPackage(Long wfPackageDefinitionId)
        throws WorkflowException
    {
        XpdlPackage xpdlPackage;

        xpdlPackage = (XpdlPackage) xpdlPackages.get(wfPackageDefinitionId);

        if (xpdlPackage != null)
            return xpdlPackage;

        xpdlPackage = (XpdlPackage) outdatedXpdlPackages.get(wfPackageDefinitionId);

        if (xpdlPackage != null)
            return xpdlPackage;

        try
        {
            List defs = DSApi.context().classicSession().find(
                "from def in class "+WfPackageDefinition.class.getName()+
                " where def.id = "+wfPackageDefinitionId);

            if (defs.size() == 0)
                throw new WorkflowException("Nie znaleziono obiektu WfPackageDefinition " +
                    "(id="+wfPackageDefinitionId+")");

            WfPackageDefinition def = (WfPackageDefinition) defs.get(0);


            String xml = def.getXml();
            if (xml == null)
                throw new WorkflowException("W pakiecie "+def.getPackageId()+" nie znaleziono " +
                    "opisu XML");
            xpdlPackage = new XpdlPackage(new ByteArrayInputStream(xml.getBytes("utf-8")));

            outdatedXpdlPackages.put(def.getId(), xpdlPackage);

            return xpdlPackage;
        }
        catch (UnsupportedEncodingException e)
        {
            throw new WorkflowException("Brak wsparcia dla UTF-8", e);
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
    }

    InternalWorkflowService()
    {
    }

    public String getName()
    {
        return NAME;
    }

    /**
     * Wi��e obiekt z konkretnym zasobem (WfResource).
     * @param username Nazwa zasobu (u�ytkownika).
     * @param password Nie jest sprawdzane.
     */
    public void connect(String username, String password) throws ConnectFailedException
    {
        // odczytuje WfResource
        try
        {
            List resources = DSApi.context().classicSession().
                find("from wfresource in class "+WfResourceImpl.class.getName()+
                " where wfresource.key = ?", username, Hibernate.STRING);

            if (resources == null || resources.size() == 0)
                throw new ConnectFailedException(
                    sm.getString("iws.resourceNotFound", username));

            wfResource = (WfResourceImpl) resources.get(0);
        }
        catch (HibernateException e)
        {
            throw new ConnectFailedException(EdmHibernateException.getMessage(e), e);
        }
        catch (EdmException e)
        {
            throw new ConnectFailedException(e.getMessage(), e);
        }
    }

    public void connect(String username)
        throws ConnectFailedException
    {
        connect(username, Configuration.getProperty("workflow.default.password"));
    }

    public void connectAdmin() throws ConnectFailedException
    {
        connect("admin", null);
    }

    public void disconnect() throws WorkflowException
    {
        wfResource = null;
    }

    public void _disconnect()
    {
        wfResource = null;
    }

    public WfResource getResourceObject() throws NotConnectedException, WorkflowException
    {
        if (wfResource == null)
            throw new NotConnectedException(sm.getString("iws.notConnected"));

        return wfResource;
    }

    public WfResource[] get_sequence_resource() throws WorkflowException
    {
        try
        {
            List<WfResource> resources = (List<WfResource>) DSApi.context().classicSession().
                find("from wfresource in class "+WfResourceImpl.class.getName());

            if (resources == null || resources.size() == 0)
                return new WfResource[0];

            return resources.toArray(new WfResource[resources.size()]);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
    }

    public String[] get_sequence_resource_key() throws WorkflowException
    {
        try
        {
            List<String> keys = (List<String>) DSApi.context().classicSession().
                find("select wfresource.key from wfresource in class "+
                WfResourceImpl.class.getName());

            if (keys == null || keys.size() == 0)
                return new String[0];

            return keys.toArray(new String[keys.size()]);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
    }

    public void create_resource(String username, String password, String realName,
                                String emailAddress, String ldapEntryDn)
        throws WorkflowException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(WfResourceImpl.class);
            criteria.add(Expression.eq("key", username));

            if (criteria.list().size() == 0)
            {
                WfResourceImpl resource = new WfResourceImpl();
                resource.setKey(username);
                resource.setEmail(emailAddress);
                resource.setName(realName);

                DSApi.context().session().save(resource);
                //mariusz
                DSApi.context().session().flush();
            }
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
    }

    /**
     * Usuwanie zasobu (u�ytkownika), jego mapowa� (ParticipantMapping)
     * i przypisa� do zada� (WfAssignment).
     * <p>
     * Wszystkie przypisania do zada� s� przekazywane u�ytkownikowi
     * administracyjnemu (okre�lonemu przez Properties.ADMIN_USERNAME).
     * <p>
     * Je�eli u�ytkownik okre�lony tym parametrem nie istnieje, rzucany
     * jest wyj�tek WorkflowException. 
     * @param username
     * @throws WorkflowException
     * @throws WorkflowResourceNotFoundException Je�eli nie znaleziono
     *  ��danego zasobu (u�ytkownika).
     */
    public void removeResource(String username,String toUser) throws WorkflowException
    {
    	TaskSnapshot.Query query;
        query = new TaskSnapshot.Query();
        try
        {
            DSUser user = DSUser.findByUsername(username);
            query.setUser(user);
            String guid = DSDivision.ROOT_GUID;
            if(user.getDivisionsWithoutGroup() != null && user.getDivisionsWithoutGroup().length > 0)
            {
            	guid = user.getDivisionsWithoutGroup()[0].getGuid();
            }
            SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);

            
            while (results.hasNext())
            {                
            	WorkflowFactory.simpleAssignment("internal,"+results.next().getActivityKey(), guid, toUser, DSApi.context().getPrincipalName(), null, WorkflowFactory.SCOPE_ONEUSER, null, null);
            }         

        }
        catch (Exception e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
    }

    /**
     * Pobiera definicje proces�w z simple_process.xpdl.
     * @see #getAllProcessMgrs()
     */
    public WfProcessMgr[] getProcessMgrs(String packageId, String processDefinitionId) throws WorkflowException
    {
        WfProcessMgr[] allMgrs = getAllProcessMgrs();
        List<WfProcessMgr> mgrs = new LinkedList<WfProcessMgr>();
        for (int i=0, n=allMgrs.length; i < n; i++)
        {
            if (allMgrs[i].package_id().equals(packageId) &&
                (processDefinitionId == null ||
                 allMgrs[i].process_definition_id().equals(processDefinitionId)))
                mgrs.add(allMgrs[i]);
        }

        return (WfProcessMgr[]) mgrs.toArray(new WfProcessMgr[mgrs.size()]);
    }

    /**
     * Pobiera wszystkie definicje proces�w z simple_process.xpdl.
     * @see #getProcessMgrs(java.lang.String, java.lang.String)
     */
    public WfProcessMgr[] getAllProcessMgrs() throws WorkflowException
    {
        if (processMgrs == null)
        {
            List<WfProcessMgr> mgrs = new LinkedList<WfProcessMgr>();

            for (Iterator iter=xpdlPackages.entrySet().iterator(); iter.hasNext(); )
            {
                Map.Entry entry = (Map.Entry) iter.next();

                Long packageDefinitionId = (Long) entry.getKey();
                XpdlPackage xpdlPackage = (XpdlPackage) entry.getValue();

                WorkflowProcess[] processes = xpdlPackage.getProcesses();
                for (int i=0; i < processes.length; i++)
                {
                    mgrs.add(new WfProcessMgrImpl(processes[i], packageDefinitionId));
                }
            }

            processMgrs = (WfProcessMgr[]) mgrs.toArray(new WfProcessMgr[mgrs.size()]);
        }

        return processMgrs;
    }

    public NameValue[] getParticipantToUsernameMappings(String packageId,
                                                        String processDefinitionId,
                                                        String participantId)
        throws WorkflowException
    {
        try
        {
            List mappings = DSApi.context().classicSession().find(
                "from map in class "+ParticipantMapping.class.getName()+
                " where map.participantId = ?", participantId, Hibernate.STRING);

            NameValue[] ret = new NameValue[mappings.size()];

            for (int i=0, n=mappings.size(); i < n; i++)
            {
                ParticipantMapping map = (ParticipantMapping) mappings.get(i);
                ret[i] = new NameValueTransientImpl(map.getParticipantId(),
                    map.getResource().getKey(), BasicDataType.DT_STRING);
            }

            return ret;
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
    }

    public void removeParticipantToUsernameMappings(String packageId,
                                                    String processDefinitionId,
                                                    String participantId)
        throws WorkflowException
    {
        try
        {
            DSApi.context().classicSession().delete(
                "from map in class "+ParticipantMapping.class.getName()+
                " where map.participantId = ?", participantId, Hibernate.STRING);
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
    }

    public void removeParticipantToUsernameMapping(String packageId,
                                                   String processDefinitionId,
                                                   String participantId,
                                                   String username)
        throws WorkflowException
    {
        try
        {
            DSApi.context().classicSession().delete(
                "from map in class "+ParticipantMapping.class.getName()+
                " where map.participantId = ? and map.resource.key = ?",
                new String[] { participantId, username },
                new Type[] { Hibernate.STRING, Hibernate.STRING });
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
    }

    /**
     * Tworzy mapowanie uczestnika na zas�b.
     * @param packageId Parametr nieu�ywany, mapowanie dotyczy wszystkich proces�w.
     * @param processDefinitionId Parametr nieu�ywany, mapowanie dotyczy wszystkich proces�w.
     * @param participantId
     * @param username
     * @throws WorkflowException
     * @throws WorkflowResourceNotFoundException Je�eli nie znaleziono zasobu
     *  (u�ytkownika) o nazwie username.
     */
    public void addParticipantToUsernameMapping(String packageId,
                                                String processDefinitionId,
                                                String participantId,
                                                String username)
        throws WorkflowException
    {
        try
        {
            List mappings = DSApi.context().classicSession().find(
                "from map in class "+ParticipantMapping.class.getName()+
                " where map.participantId = ? and map.resource.key = ?",
                new String[] { participantId, username },
                new Type[] { Hibernate.STRING, Hibernate.STRING });

            if (mappings.size() == 0)
            {
                ParticipantMapping map = new ParticipantMapping();
                map.setParticipantId(participantId);

                List resources = DSApi.context().classicSession().find(
                    "from wfr in class "+WfResourceImpl.class.getName()+
                    " where wfr.key = ?", username, Hibernate.STRING);

                if (resources.size() == 0)
                    throw new WorkflowResourceNotFoundException("Nie istnieje zas�b "+username);

                map.setResource((WfResourceImpl) resources.get(0));

                DSApi.context().session().save(map);
                DSApi.context().session().flush();
            }
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
    }

    public static boolean documentHasTasks(Long documentId,
                                           String packageId,
                                           String processDefinitionId) throws EdmException
    {
        // SQL Server blokuje tabele, kt�rych u�ywa w SELECT; poniewa�
        // s� to cz�sto u�ywane tabele, blokowanie powoduje deadlocki
        String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";

        String sql = "select count(*) " +
            "from dsw_process "+nolock+
            "inner join dsw_process_context docid "+nolock+" on (docid.process_id = dsw_process.id and docid.param_name = 'ds_document_id' and docid.param_value = ?)\n" +
            "where dsw_process.packageid = ? " +
            "and dsw_process.processdefinitionid = ? " +
            "and dsw_process.state != ?";

        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement(sql);
            ps.setString(1, documentId.toString());
            ps.setString(2, packageId);
            ps.setString(3, processDefinitionId);
            ps.setString(4, "closed.completed");

            ResultSet rs = ps.executeQuery();

            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby dokument�w");

            return rs.getInt(1) > 0;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);            
        }
    }
 
    /** 
     * Pobieranie pism przychodzacych ze stronicowaniem.
     * 
     */
    public static Map getInOfficeDocumentTasks(List<String> usernames, int offset, int LIMIT, Integer officeNumber, String sortField, boolean ascending) throws EdmException
    {
        // Lista usernames powinna byc niepusta
        String inNames = null;
        for (String username : usernames)
        {
            if (inNames == null)
                inNames = "(";
            else 
                inNames += ",";
            inNames += " ? ";
        }
        inNames += ")";
        
        String orderClause = null;
        if ("receiveDate".equals(sortField))
            orderClause = "dsw_tasklist.dsw_activity_laststatetime";
        else if ("description".equals(sortField))
            orderClause = "dsw_tasklist.dsw_activity_name";
        else if ("process".equals(sortField))
            orderClause = "dsw_tasklist.dsw_process_name";
        else if ("documentSummary".equals(sortField))
            orderClause = "dsw_tasklist.dso_document_summary";
        else if ("documentReferenceId".equals(sortField))
            orderClause = "dsw_tasklist.dso_document_referenceid";
        else if ("documentSender".equals(sortField))
            orderClause = "dsw_tasklist.dso_person_firstname";
        else if ("sender".equals(sortField))
            orderClause = "dsw_tasklist.dsw_process_context_param_val";
        else if ("objective".equals(sortField))
            orderClause = "dsw_tasklist.dsw_activity_context_param_val";
        else 
            orderClause = "dsw_tasklist.dso_document_officenumber";
        
        if (ascending)
            orderClause += " asc";
        else
            orderClause += " desc";
                       
        String sql="select * from dsw_tasklist " + DSApi.nolockString() + " where dsw_tasklist.dsw_resource_res_key in "+inNames+" and document_type=? order by "+orderClause;
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement(sql);
            int count = 0;
            for (String username : usernames)
            {
                count++;
                ps.setString(count, username);
            }

            
            ps.setString(count+1, InOfficeDocument.TYPE);
            
           count = 0;
            
            List<Map<String, Object>> results = new LinkedList<Map<String, Object>>();
            //long time = System.currentTimeMillis();
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                if (officeNumber == null) {
                    if (count < offset || count >= offset + LIMIT) {
                        count++;
                        continue;
                    }
                    else
                        count++;
                }
                
                Map<String, Object> bean = new HashMap<String, Object>();
                bean.put("assignment_accepted", new Boolean(rs.getBoolean(2)));
                bean.put("document_id", new Long(rs.getLong(3)));
                bean.put("document_officeNumber", new Integer(rs.getInt(4)));
                bean.put("document_officeNumberYear", new Integer(rs.getInt(5)));
                bean.put("activity_lastStateTime", new Date(rs.getTimestamp(6).getTime()));
                bean.put("activity_name", rs.getString(7));
                bean.put("sender_firstname", rs.getString(8));
                bean.put("sender_lastname", rs.getString(9));
                bean.put("sender_organization", rs.getString(10));
                bean.put("sender_street", rs.getString(11));
                bean.put("sender_location", rs.getString(12));
                bean.put("activity_lastActivityUser", rs.getString(13));
                bean.put("activity_objective", rs.getString(14));
                bean.put("activity_key", rs.getString(15));
                bean.put("document_summary", rs.getString(16));
                bean.put("document_referenceId", rs.getString(17));
                String au = rs.getString(18);
                if (au != null && au.length() > 0)
                    bean.put("assigned_user", au);
                bean.put("process_name", rs.getString(19));
                bean.put("document_source", rs.getString(20));
                bean.put("process_packageid", rs.getString(21));
                bean.put("process_definitionid", rs.getString(22));

                        
                
                java.sql.Date deadlineTime = rs.getDate(/*44*/24);
                if (deadlineTime != null)
                {
                    bean.put("deadlineTime", new Date(deadlineTime.getTime()));
                    bean.put("deadlineRemark", rs.getString(/*45*/25));
                    bean.put("deadlineAuthor", rs.getString(/*46)*/26));
                }

                results.add(bean);

            }


            Map map = new HashMap();
            map.put("tasks", results);
            map.put("size", Integer.valueOf(count));
            return map;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);            
        }
    }
    
    /** 
     * Standardowe pobieranie pism przychodzacych (wszystkie zwracamy).
     * 
     */ 
    
    static StringBuffer inQuery = new StringBuffer().
    append("select ").
    append("dsw_assignment_id, ").
    append("dsw_assignment_accepted, ").
    append("dso_document_id,").
    append("dso_document_officenumber, ").
    append("dso_document_officenumberyear, ").
    append("dsw_activity_laststatetime, ").
    append("dsw_activity_name, ").
    append("dso_person_firstname, ").
    append("dso_person_lastname, ").
    append("dso_person_organization, ").
    append("dso_person_street, ").
    append("dso_person_location, ").
    append("dsw_process_context_param_val, ").
    append("dsw_activity_context_param_val, ").
    append("dsw_activity_activity_key, ").
    append("dso_document_summary, ").
    append("dso_document_referenceid, ").
    append("assigned_user, ").
    append("dsw_process_name, ").
    append("ds_document_source, ").
    append("dsw_process_packageid, ").
    append("dsw_process_processdef, ").
    append("dsw_process_deadlinetime, ").
    append("dsw_process_deadlineremark, ").
    append("dsw_process_deadlineauthor ").
    append("from dsw_tasklist ").
    append(DSApi.nolockString()).
    append(" where dsw_resource_res_key = ? and document_type=? order by dso_document_officenumber desc"); 

    public static List<Map<String, Object>> getInOfficeDocumentTasks(String username) throws EdmException
    {       
        String sql = inQuery.toString();
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, InOfficeDocument.TYPE);
            
            List<Map<String, Object>> results = new LinkedList<Map<String, Object>>();            
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                Map<String, Object> bean = new HashMap<String, Object>();
                bean.put("assignment_accepted", new Boolean(rs.getBoolean(2)));
                bean.put("document_id", new Long(rs.getLong(3)));
                bean.put("document_officeNumber", new Integer(rs.getInt(4)));
                bean.put("document_officeNumberYear", new Integer(rs.getInt(5)));
                bean.put("activity_lastStateTime", new Date(rs.getTimestamp(6).getTime()));
                bean.put("activity_name", rs.getString(7));
                bean.put("sender_firstname", rs.getString(8));
                bean.put("sender_lastname", rs.getString(9));
                bean.put("sender_organization", rs.getString(10));
                bean.put("sender_street", rs.getString(11));
                bean.put("sender_location", rs.getString(12));
                bean.put("activity_lastActivityUser", rs.getString(13));
                bean.put("activity_objective", rs.getString(14));
                bean.put("activity_key", rs.getString(15));
                bean.put("document_summary", rs.getString(16));
                bean.put("document_referenceId", rs.getString(17));
                String au = rs.getString(18);
                if (au != null && au.length() > 0)
                    bean.put("assigned_user", au);
                bean.put("process_name", rs.getString(19));
                bean.put("document_bok", Boolean.valueOf("$bok".equals(username)));
                bean.put("document_source", rs.getString(20));
                bean.put("process_packageid", rs.getString(21));
                bean.put("process_definitionid", rs.getString(22));

                int flagCount = 0;
                
                java.sql.Date deadlineTime = rs.getDate(/*43*/23+flagCount);
                if (deadlineTime != null)
                {
                    bean.put("deadlineTime", new Date(deadlineTime.getTime()));
                    bean.put("deadlineRemark", rs.getString(/*44*/24+flagCount));
                    bean.put("deadlineAuthor", rs.getString(/*45*/25+flagCount));
                }

                results.add(bean);

            }

            return results;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
        }
    }

    /** 
     * Uproszczone pobieranie pism przychodzacych (wszystkie linie zwracamy,
     * ale opuszczamy niektore pola).
     * 
     */   
    
    static StringBuffer simpleQuery = new StringBuffer().
    append("select ").
    append("dsw_assignment_id, ").
    append("dsw_assignment_accepted, ").
    append("dso_document_id,").
    append("dso_document_officenumber, ").
    append("dsw_activity_laststatetime, ").
    append("dsw_activity_name, ").
    append("dsw_process_context_param_val, ").
    append("dso_document_summary, ").
    append("assigned_user, ").
    append("dsw_process_name, ").
    append("dsw_process_packageid, ").
    append("dsw_process_processdef, ").
    append("dsw_process_deadlinetime, ").
    append("dsw_process_deadlineremark, ").
    append("dsw_process_deadlineauthor ").
    append("from dsw_tasklist ").
    append(DSApi.nolockString()).
    append(" where dsw_resource_res_key = ? and document_type=? order by dso_document_officenumber desc");

    public static List<Map<String, Object>> getSimpleInOfficeDocumentTasks(String username) throws EdmException
    {
        String sql = simpleQuery.toString(); 
     
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, InOfficeDocument.TYPE);     

            List<Map<String, Object>> results = new LinkedList<Map<String, Object>>();
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                Map<String, Object> bean = new HashMap<String, Object>();
                bean.put("assignment_accepted", new Boolean(rs.getBoolean(2)));
                bean.put("document_id", new Long(rs.getLong(3)));
                bean.put("document_officeNumber", new Integer(rs.getInt(4)));
                bean.put("activity_lastStateTime", new Date(rs.getTimestamp(/*6*/5).getTime()));
                bean.put("activity_name", rs.getString(/*7*/6));
                bean.put("activity_lastActivityUser", rs.getString(/*13*/7));
                bean.put("activity_key", rs.getString(/*15*/8));
                bean.put("document_summary", rs.getString(/*16*/9));
                String au = rs.getString(/*18*/10);
                if (au != null && au.length() > 0)
                    bean.put("assigned_user", au);
                bean.put("process_name", rs.getString(/*19*/11));
                bean.put("document_bok", Boolean.valueOf("$bok".equals(username)));
                bean.put("process_packageid", rs.getString(/*21*/12));
                bean.put("process_definitionid", rs.getString(/*22*/13));
                     java.sql.Date deadlineTime = rs.getDate(/*43*/14);
                if (deadlineTime != null)
                {
                    bean.put("deadlineTime", new Date(deadlineTime.getTime()));
                    bean.put("deadlineRemark", rs.getString(/*44*/15));
                    bean.put("deadlineAuthor", rs.getString(/*45*/16));
                }

                results.add(bean);

            }


            return results;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
        }
    }

    static StringBuffer outQuery = new StringBuffer().
    append("select ").
    append("dsw_assignment_id, ").
    append("dsw_assignment_accepted, ").
    append("dso_document_id,").
    append("dso_document_officenumber, ").
    append("dso_document_officenumberyear, ").
    append("dsw_activity_laststatetime, ").
    append("dsw_activity_name, ").
    append("dso_person_firstname, ").
    append("dso_person_lastname, ").
    append("dso_person_organization, ").
    append("dso_person_street, ").
    append("dso_person_location, ").
    append("dsw_process_context_param_val, ").
    append("dsw_activity_context_param_val, ").
    append("dsw_activity_activity_key, ").
    append("dso_document_summary, ").
    append("assigned_user, ").
    append("dsw_process_name, ").
    append("document_type, ").
    append("ds_document_source, ").
    append("rcpt_firstname, ").
    append("rcpt_lastname, ").
    append("rcpt_organization, ").
    append("rcpt_street, ").
    append("rcpt_location, ").
    append("dsw_process_packageid, ").
    append("dsw_process_processdef, ").
    append("dsw_process_deadlinetime, ").
    append("dsw_process_deadlineremark, ").
    append("dsw_process_deadlineauthor ").
    append("from dsw_tasklist ").
    append(DSApi.nolockString()).
    append(" where dsw_resource_res_key = ? and document_type=? ").
    append("order by dso_document_officenumber desc");
	
    public static List<Map<String, Object>> getOutOfficeDocumentTasks(String username) throws EdmException
    {

        String sql = outQuery.toString();  

        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, OutOfficeDocument.TYPE);
            List<Map<String, Object>> results = new LinkedList<Map<String, Object>>();
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                Map<String, Object> bean = new HashMap<String, Object>();
                bean.put("assignment_accepted", new Boolean(rs.getBoolean(2)));
                bean.put("document_id", new Long(rs.getLong(3)));

                int ko = rs.getInt(4);
                if (!rs.wasNull())
                    bean.put("document_officeNumber", new Integer(ko));
                bean.put("document_officeNumberYear", new Integer(rs.getInt(5)));
                bean.put("activity_lastStateTime", new Date(rs.getTimestamp(6).getTime()));
                bean.put("activity_name", rs.getString(7));
                bean.put("sender_firstname", rs.getString(8));
                bean.put("sender_lastname", rs.getString(9));
                bean.put("sender_organization", rs.getString(10));
                bean.put("sender_street", rs.getString(11));
                bean.put("sender_location", rs.getString(12));
                bean.put("activity_lastActivityUser", rs.getString(13));
                bean.put("activity_objective", rs.getString(14));
                bean.put("activity_key", rs.getString(15));
                bean.put("document_summary", rs.getString(16));

                String au = rs.getString(17);
                if (au != null && au.length() > 0)
                    bean.put("assigned_user", au);
                bean.put("process_name", rs.getString(18));

                bean.put("internal", false);
                bean.put("document_source", rs.getString(20));

                bean.put("recipient_firstname", rs.getString(21));
                bean.put("recipient_lastname", rs.getString(22));
                bean.put("recipient_organization", rs.getString(23));
                bean.put("recipient_street", rs.getString(24));
                bean.put("recipient_location", rs.getString(25));

                bean.put("process_packageid", rs.getString(26));
                bean.put("process_definitionid", rs.getString(27));

                int flagCount = 0;

                java.sql.Date deadlineTime = rs.getDate(/*38*/28+flagCount);
                if (deadlineTime != null)
                {
                    bean.put("deadlineTime", new Date(deadlineTime.getTime()));
                    bean.put("deadlineRemark", rs.getString(/*39*/29+flagCount));
                    bean.put("deadlineAuthor", rs.getString(/*40*/30+flagCount));
                }

                bean.put("document_bok", Boolean.valueOf("$bok".equals(username)));
                results.add(bean);
                }
            

            return results;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);            
        }
    }
    
    static StringBuffer internalSQL = new StringBuffer().
    append("select ").
    append("dsw_assignment_id, ").
    append("dsw_assignment_accepted, ").
    append("dso_document_id,").
    append("dso_document_officenumber, ").
    append("dso_document_officenumberyear, ").
    append("dsw_activity_laststatetime, ").
    append("dsw_activity_name, ").
    append("dso_person_firstname, ").
    append("dso_person_lastname, ").
    append("dso_person_organization, ").
    append("dso_person_street, ").
    append("dso_person_location, ").
    append("dsw_process_context_param_val, ").
    append("dsw_activity_context_param_val, ").
    append("dsw_activity_activity_key, ").
    append("dso_document_summary, ").
    append("assigned_user, ").
    append("dsw_process_name, ").
    append("document_type, ").
    append("ds_document_source, ").
    append("rcpt_firstname, ").
    append("rcpt_lastname, ").
    append("rcpt_organization, ").
    append("rcpt_street, ").
    append("rcpt_location, ").
    append("dsw_process_packageid, ").
    append("dsw_process_processdef, ").
    append("dsw_process_deadlinetime, ").
    append("dsw_process_deadlineremark, ").
    append("dsw_process_deadlineauthor ").
    append("from dsw_tasklist ").
    append(DSApi.nolockString()).
    append(" where dsw_resource_res_key = ? and document_type=? ").
    append("order by dso_document_officenumber desc");
    
    public static List<Map<String, Object>> getInternalOfficeDocumentTasks(String username) throws EdmException
    {
        
    	String sql = internalSQL.toString();  

        

        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, OutOfficeDocument.INTERNAL_TYPE);            
            List<Map<String, Object>> results = new LinkedList<Map<String, Object>>();
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                Map<String, Object> bean = new HashMap<String, Object>();
                bean.put("assignment_accepted", new Boolean(rs.getBoolean(2)));
                bean.put("document_id", new Long(rs.getLong(3)));

                int ko = rs.getInt(4);
                if (!rs.wasNull())
                    bean.put("document_officeNumber", new Integer(ko));
                bean.put("document_officeNumberYear", new Integer(rs.getInt(5)));
                bean.put("activity_lastStateTime", new Date(rs.getTimestamp(6).getTime()));
                bean.put("activity_name", rs.getString(7));
                bean.put("sender_firstname", rs.getString(8));
                bean.put("sender_lastname", rs.getString(9));
                bean.put("sender_organization", rs.getString(10));
                bean.put("sender_street", rs.getString(11));
                bean.put("sender_location", rs.getString(12));
                bean.put("activity_lastActivityUser", rs.getString(13));
                bean.put("activity_objective", rs.getString(14));
                bean.put("activity_key", rs.getString(15));
                bean.put("document_summary", rs.getString(16));

                String au = rs.getString(17);
                if (au != null && au.length() > 0)
                    bean.put("assigned_user", au);
                bean.put("process_name", rs.getString(18));
                
                bean.put("internal", true);
                bean.put("document_source", rs.getString(20));

                bean.put("recipient_firstname", rs.getString(21));
                bean.put("recipient_lastname", rs.getString(22));
                bean.put("recipient_organization", rs.getString(23));
                bean.put("recipient_street", rs.getString(24));
                bean.put("recipient_location", rs.getString(25));

                bean.put("process_packageid", rs.getString(26));
                bean.put("process_definitionid", rs.getString(27));

                int flagCount = 0;
                java.sql.Date deadlineTime = rs.getDate(/*38*/28+flagCount);
                if (deadlineTime != null)
                {
                    bean.put("deadlineTime", new Date(deadlineTime.getTime()));
                    bean.put("deadlineRemark", rs.getString(/*39*/29+flagCount));
                    bean.put("deadlineAuthor", rs.getString(/*40*/30+flagCount));
                }

                bean.put("document_bok", Boolean.valueOf("$bok".equals(username)));
                results.add(bean);
                }
            

            return results;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);            
        }
    }

    /**
     * @TODO - poprawic pytanie SQL
     * @param username
     * @return
     * @throws EdmException
     */
    public static List<Map<String, Object>> getOfficeOrderTasks(String username) throws EdmException
    {
        boolean flagsOn = "true".equals(Configuration.getProperty("flags"));

        String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";

        /*String sql = "select " +
            "dsw_assignment.id, " +
            "dsw_assignment.accepted, " +
            "doc.id, " +
            "doc.officenumber, " +
            "doc.officenumberyear, \n" +
            "dsw_activity.laststatetime, " +
            "dsw_activity.name, " +
         //   "dso_person.firstname,\n" +
         //   "dso_person.lastname,\n" +
         //   "dso_person.organization,\n" +
         //   "dso_person.street,\n" +
         //   "dso_person.location,\n" +
            "lac.param_value,\n" +
            "obj.param_value,\n" +
            "dsw_activity.activity_key,\n"+
            "doc.summary,\n"+
            "au.param_value,\n"+
            "dsw_process.name,\n"+
          //  "doc.internaldocument,\n"+
            "dsdoc.source,\n"+
          //  "rcpt.firstname,\n" +
          //  "rcpt.lastname,\n" +
          //  "rcpt.organization,\n" +
          //  "rcpt.street,\n" +
          //  "rcpt.location,\n" +
            "dsw_process.packageid,\n"+
            "dsw_process.processdefinitionid,\n"+
            (flagsOn ?
                ("fl.fl1, fl.fl2, fl.fl3, fl.fl4, fl.fl5, fl.fl6, fl.fl7, fl.fl8, fl.fl9, fl.fl10, " +
                "ufl.fl1, ufl.fl2, ufl.fl3, ufl.fl4, ufl.fl5, ufl.fl6, ufl.fl7, ufl.fl8, ufl.fl9, ufl.fl10, ")
                : "")
            +
            "dsw_process.deadlinetime, "+
            "dsw_process.deadlineremark, "+
            "dsw_process.deadlineauthor "+
            "from\n" +
            "dsw_assignment "+nolock+" inner join dsw_resource "+nolock+" on (dsw_assignment.resource_id = dsw_resource.id  and dsw_resource.res_key = ?)\n" +
            "inner join dsw_activity "+nolock+" on dsw_activity.id = dsw_assignment.activity_id \n" +
            "inner join dsw_activity_context obj "+nolock+" on (obj.activity_id = dsw_activity.id and obj.param_name = 'ds_objective')\n" +
            "inner join dsw_process "+nolock+" on dsw_process.id = dsw_activity.process_id \n" +
            "inner join dsw_process_context docid "+nolock+" on docid.process_id = dsw_process.id \n" +
            "left outer join dsw_process_context lac "+nolock+" on (lac.process_id = dsw_process.id and lac.param_name='ds_last_activity_user')\n" +
            "left outer join dsw_process_context au "+nolock+" on (au.process_id = dsw_process.id and au.param_name='ds_assigned_user')\n" +
            "inner join dso_order doc "+nolock+" on (doc.id = docid.param_value and docid.param_name = 'ds_document_id')\n" +
            (flagsOn ?
                ("left outer join ds_document_flags fl "+nolock+" on (doc.id = fl.document_id)\n"+
                "left outer join ds_document_user_flags ufl "+nolock+" on (doc.id = ufl.document_id and ufl.username = ?)\n")
                : "")
            +
            "inner join ds_document dsdoc "+nolock+" on (doc.id = dsdoc.id) \n"+
          //  "left outer join dso_person "+nolock+" on (doc.sender = dso_person.id and dso_person.discriminator='SENDER')\n"+
          //  "left outer join dso_person rcpt "+nolock+" on (doc.id = rcpt.document_id and (rcpt.posn = 0 or rcpt.posn IS NULL))\n"+
            "order by doc.officenumber desc";*/
        String sql = "select " +
        "dsw_assignment_id, "+
    	"dsw_assignment_accepted, "+
    	"dso_document_id,"+
    	"dso_document_officenumber, "+
    	"dso_document_officenumberyear, "+
    	"dsw_activity_laststatetime, "+
    	"dsw_activity_name, "+
    	//"dso_person_firstname, "+
    	//"dso_person_lastname, "+
    	//"dso_person_organization, "+
    	//"dso_person_street, "+
    	//"dso_person_location, "+
    	"dsw_process_context_param_val, "+
    	"dsw_activity_context_param_val, "+
    	"dsw_activity_activity_key, "+
    	"dso_document_summary, "+
    	"assigned_user, "+
    	//"dso_document_referenceid, "+
    	"dsw_process_name, "+
    	//"document_type, "+
    	"ds_document_source, "+
    	//"rcpt_firstname, "+
    	//"rcpt_lastname, "+
    	//"rcpt_organization, "+
    	//"rcpt_street, "+
    	//"rcpt_location, "+
    	"dsw_process_packageid, "+
    	"dsw_process_processdef, "+
    	"dsw_process_deadlinetime, "+
    	"dsw_process_deadlineremark, "+
    	"dsw_process_deadlineauthor "+
    	"from dsw_tasklist where dsw_resource_res_key = ? and document_type=? "+
    	"order by dso_document_officenumber desc"; 

        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, OfficeOrder.TYPE);

            List<Map<String, Object>> results = new LinkedList<Map<String, Object>>();
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                Map<String, Object> bean = new HashMap<String, Object>();
                bean.put("assignment_accepted", new Boolean(rs.getBoolean(2)));
                bean.put("document_id", new Long(rs.getLong(3)));

                int ko = rs.getInt(4);
                if (!rs.wasNull())
                    bean.put("document_officeNumber", new Integer(ko));
                bean.put("document_officeNumberYear", new Integer(rs.getInt(5)));
                bean.put("activity_lastStateTime", new Date(rs.getTimestamp(6).getTime()));
                bean.put("activity_name", rs.getString(7));
            /*    bean.put("sender_firstname", rs.getString(8));
                bean.put("sender_lastname", rs.getString(9));
                bean.put("sender_organization", rs.getString(10));
                bean.put("sender_street", rs.getString(11));
                bean.put("sender_location", rs.getString(12));  */
                bean.put("activity_lastActivityUser", rs.getString(/*13*/8));
                bean.put("activity_objective", rs.getString(/*14*/9));
                bean.put("activity_key", rs.getString(/*15*/10));
                bean.put("document_summary", rs.getString(/*16*/11));

                String au = rs.getString(/*17*/12);
                if (au != null && au.length() > 0)
                    bean.put("assigned_user", au);
                bean.put("process_name", rs.getString(/*18*/13));

                // TODO: przeformu�owa� zapytanie SQL tak, by kolumna internal by�a
                // jako pierwsza, wtedy b�dzie mo�na pomin�� wiersz bez tworzenia beana
                // (niekt�re bazy wymagaj� odczytu ResultSet w kolejno�ci)
            /*    boolean isInternal = rs.getBoolean(19);
                if (!(internal == isInternal))
                    continue;

                bean.put("internal", Boolean.valueOf(internal));*/
                bean.put("document_source", rs.getString(/*20*/14));

            /*    bean.put("recipient_firstname", rs.getString(20));
                bean.put("recipient_lastname", rs.getString(22));
                bean.put("recipient_organization", rs.getString(23));
                bean.put("recipient_street", rs.getString(24));
                bean.put("recipient_location", rs.getString(25));*/

                bean.put("process_packageid", rs.getString(/*26*/15));
                bean.put("process_definitionid", rs.getString(/*27*/16));

                int flagCount = 0;
                if (flagsOn)
                {
                    flagCount = 20;
                    bean.put("fl1", Boolean.valueOf(rs.getBoolean(/*28*/17)));
                    bean.put("fl2", Boolean.valueOf(rs.getBoolean(/*29*/18)));
                    bean.put("fl3", Boolean.valueOf(rs.getBoolean(/*30*/19)));
                    bean.put("fl4", Boolean.valueOf(rs.getBoolean(/*31*/20)));
                    bean.put("fl5", Boolean.valueOf(rs.getBoolean(/*32*/21)));
                    bean.put("fl6", Boolean.valueOf(rs.getBoolean(/*33*/22)));
                    bean.put("fl7", Boolean.valueOf(rs.getBoolean(/*34*/23)));
                    bean.put("fl8", Boolean.valueOf(rs.getBoolean(/*35*/24)));
                    bean.put("fl9", Boolean.valueOf(rs.getBoolean(/*36*/25)));
                    bean.put("fl10", Boolean.valueOf(rs.getBoolean(/*37*/26)));

                    bean.put("ufl1", Boolean.valueOf(rs.getBoolean(/*38*/27)));
                    bean.put("ufl2", Boolean.valueOf(rs.getBoolean(/*39*/28)));
                    bean.put("ufl3", Boolean.valueOf(rs.getBoolean(/*40*/29)));
                    bean.put("ufl4", Boolean.valueOf(rs.getBoolean(/*41*/30)));
                    bean.put("ufl5", Boolean.valueOf(rs.getBoolean(/*42*/31)));
                    bean.put("ufl6", Boolean.valueOf(rs.getBoolean(/*43*/32)));
                    bean.put("ufl7", Boolean.valueOf(rs.getBoolean(/*44*/33)));
                    bean.put("ufl8", Boolean.valueOf(rs.getBoolean(/*45*/34)));
                    bean.put("ufl9", Boolean.valueOf(rs.getBoolean(/*46*/35)));
                    bean.put("ufl10", Boolean.valueOf(rs.getBoolean(/*47*/36)));
                }
                java.sql.Date deadlineTime = rs.getDate(/*28*/17+flagCount);
                if (deadlineTime != null)
                {
                    bean.put("deadlineTime", new Date(deadlineTime.getTime()));
                    bean.put("deadlineRemark", rs.getString(/*29*/18+flagCount));
                    bean.put("deadlineAuthor", rs.getString(/*30*/19+flagCount));
                }

                bean.put("document_bok", Boolean.valueOf("$bok".equals(username)));
                results.add(bean);
            }

            return results;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
            
        }
    }


    public static WfProcess getWfProcess(String key) throws EdmException
    {
        try
        {
            List<WfProcess> ps =
                DSApi.context().classicSession().find("select p from p in class "+WfProcessImpl.class+
                " where p.key = ?", key, Hibernate.STRING);
            if (ps.size() > 0)
                return ps.get(0);
            return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /* Procedura czysci zakonczone zadania workflow, sa one niepotrzebne, a obciazaja baze */
    public static void cleanClosedTask() throws EdmException
    { 
        try
        {
        java.sql.Connection con = DSApi.context().session().connection();
        boolean ac = con.getAutoCommit();
        con.setAutoCommit(true);
        java.sql.Statement stm = con.createStatement();
        ResultSet rs = stm.executeQuery("select * from dsw_activity where state='closed.completed'");        
        while (rs.next())
        {
            long id = rs.getLong("id");
            PreparedStatement ps = con.prepareStatement("delete from dsw_activity_context where activity_id = ?");
            ps.setLong(1,id);
            ps.execute();
            ps.close();
            ps = con.prepareStatement("delete from dsw_process_activities where activity_id = ?");
            ps.setLong(1,id);
            ps.execute();
            ps.close();
            ps = con.prepareStatement("delete from dsw_assignment where activity_id = ?");
            ps.setLong(1,id);
            ps.execute();
            ps.close();
            ps = con.prepareStatement("delete from dsw_activity where id = ?");
            ps.setLong(1,id);
            ps.execute();
            ps.close();
        }
        rs.close();
        rs = stm.executeQuery("select * from dsw_process where state='closed.completed'");        
        while (rs.next())
        {
            long id = rs.getLong("id");
            PreparedStatement ps = con.prepareStatement("delete from dsw_process_context where process_id = ?");
            ps.setLong(1,id);
            ps.execute();
            ps.close();
            ps = con.prepareStatement("delete from dsw_process_activities where process_id = ?");
            ps.setLong(1,id);
            ps.execute();
            ps.close();            
            ps = con.prepareStatement("delete from dsw_process where id = ?");
            ps.setLong(1,id);
            ps.execute();
            ps.close();
        }
        rs.close();
        stm.close();
        con.setAutoCommit(ac);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }
}
