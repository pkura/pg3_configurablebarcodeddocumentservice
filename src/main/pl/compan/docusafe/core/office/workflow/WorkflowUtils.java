package pl.compan.docusafe.core.office.workflow;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
import pl.compan.docusafe.core.office.workflow.internal.WfProcessImpl;
import pl.compan.docusafe.core.office.workflow.jbpm.JbpmWorkflowService;
import pl.compan.docusafe.util.StringManager;

import java.util.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkflowUtils.java,v 1.64 2010/07/27 11:26:30 kamilj Exp $
 */
public final class WorkflowUtils
{
    private WorkflowUtils()
    {
    }

    public static String iwfManualName()
    {
        return Docusafe.getProperty("internal.workflow.manual.process");
    }

    public static String iwfCcName()
    {
        return Docusafe.getProperty("internal.workflow.process.cc");
    }

    public static String iwfSecondOpinionName()
    {
        return Docusafe.getProperty("internal.workflow.second.opinion.process");
    }

    public static String iwfPackageName()
    {
        return Docusafe.getProperty("internal.workflow.package.id");
    }

    public static boolean isManual(WfProcessMgr mgr) throws WorkflowException
    {
        return
            InternalWorkflowService.NAME.equals(mgr.getWorkflowName()) &&
            mgr.package_id().equals(iwfPackageName()) &&
            mgr.process_definition_id().equals(iwfManualName());
    }

    public static boolean isManual(WfActivity activity) throws WorkflowException
    {
        return isManual(activity.container().manager());
    }

    public static boolean isExternalWorkflow(WfActivity activity)
    {
        return false;
    }

    public static boolean isCc(WfProcessMgr mgr) throws WorkflowException
    {
        return
            InternalWorkflowService.NAME.equals(mgr.getWorkflowName()) &&
            mgr.package_id().equals(WorkflowUtils.iwfPackageName()) &&
            mgr.process_definition_id().equals(WorkflowUtils.iwfCcName());
    }

    /**
     * Zwraca par� NameValue o podanej warto�ci atrybutu name
     * lub null.
     */
    public static NameValue findParameter(NameValue[] nv, String name)
    {
        if (nv == null || nv.length == 0)
            return null;

        for (int i=0, n=nv.length; i < n; i++)
        {
            if (name.equals(nv[i].name()))
                return nv[i];
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static <T> T findParameterValue(NameValue[] nv, String name, Class<T> cls)
        throws EdmException
    {
    	StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        NameValue namevalue = findParameter(nv, name);
        if (namevalue != null && namevalue.value() != null)
        {
            Object value = namevalue.value().value();
            if (value == null)
                return null;
            if (cls.isAssignableFrom(value.getClass()))
                return (T) value;
            throw new EdmException(smG.getString("NieprawidlowyTypZwracanejWartosciSpodziewanoSie")+" "+cls+
                smG.getString("otrzymano")+" "+value.getClass());
        }

        return null;
    }

    public static NameValue findParameter(Collection nameValues, String name)
    {
        for (Iterator iter=nameValues.iterator(); iter.hasNext(); )
        {
            NameValue nv = (NameValue) iter.next();
            if (name.equals(nv.name()))
                return nv;
        }

        return null;
    }

    /* *
     * Znajduje warto�� parametru kroku procesu. Zwracany jest obiekt
     * odpowiedniego typu.
     * <p>
     * Funkcja zak�ada, �e definicja procesu zosta�a utworzona w programie
     * JaWE, poniewa� warto�� parametru pobierana jest z niestandardowych
     * element�w ExtendedAttribute charakterystycznych dla plik�w XPDL
     * tworzonych przy pomocy tego programu.
     */
/*
    public static Object findParameterValue(String name, Activity activity)
    {
        // extendedattributes z Activity
        ExtendedAttribute[] actEA = activity.getExtendedAttributes();
        for (int i=0; i < actEA.length; i++)
        {
            // szukam atrybutu procesu o tej samej nazwie, co atrybut kroku
            ExtendedAttribute pkgEA = activity.getProcess().getPackage().getExtendedAttribute(actEA[i].getName());
            if (pkgEA != null && pkgEA.getTotal().equals(name) &&
                pkgEA.get("DataType") != null)
            {
                DataType dt = (DataType) pkgEA.get("DataType");
                return dt.getTotal(actEA[i].getTotal());
            }
        }

        return null;
    }
*/

    /**
     * Konwertuje przekazan� warto�� do odpowiedniego typu zgodnie
     * z warto�ci� parametru type (jedna ze sta�ych zdefiniowanych
     * w AnyObject).
     * <p>
     * Pusty napis lub warto�� null odpowiada warto�ci domy�lnej wybranego typu.
     * <p>
     * Je�eli wybrany typ to {@link AnyObjectKind#ENUM}, za� przekazana warto��
     * nie odpowiada �adnej z mo�liwych warto�ci enumeracji, w momencie
     * nadawania tej warto�ci zmiennej typu org.omg.CORBA.Any wyst�pi b��d.
     * Program wywo�uj�cy t� funkcj� musi w tej sytuacji sam zatroszczy� si�
     * o nadanie w�a�ciwej warto�ci.
     */
    public static Object safeConvert(String value, int type)
        throws WorkflowException
    {
    	StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        if (type == AnyObjectKind.BOOLEAN)
        {
            return Boolean.valueOf(value);
        }
        else if (type == AnyObjectKind.DOUBLE)
        {
            try
            {
                return new Double(Double.parseDouble(value));
            }
            catch (Exception e)
            {
                return new Double(0);
            }
        }
        else if (type == AnyObjectKind.LONG)
        {
            try
            {
                return new Long(Long.parseLong(value));
            }
            catch (Exception e)
            {
                return new Long(0);
            }
        }
        else if (type == AnyObjectKind.STRING || type == AnyObjectKind.ENUM)
        {
            return value != null ? value : "";
        }
        else
        {
            throw new WorkflowException(smG.getString("nieznanyTyp")+": "+type);
        }
    }

    public static String[] enumerationValues(AnyObjectKind kind) throws EdmException
    {
        String[] values = new String[kind.memberCount()];
        for (int i=0, n=values.length; i < n; i++)
        {
            values[i] = kind.memberName(i);
        }
        return values;
    }

    /**
     * Analizuje napisow� reprezentacj� obiektu workflow WfExecutionObject
     * (WfProcess lub WfActivity).
     * Napis powinien mie� posta�
     * <code>&lt;nazwa workflow&gt; "," &lt;nazwa obiektu&gt;</code>.
     * @param objectId
     * @return
     * @throws WorkflowException
     */
    static String[] parseWfObjectId(String objectId) throws WorkflowException
    {
    	StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        if (objectId == null)
            throw new WorkflowException("objectId == null");

        if (objectId.indexOf(',') <= 0)
        {
            throw new WorkflowException(smG.getString("NiepoprawnyIdentyfikatorObiektuWorkflow")+": "+objectId);
        }
        else
        {
            String wfName = objectId.substring(0, objectId.indexOf(','));

            if (!InternalWorkflowService.NAME.equals(wfName) &&
            		!JbpmWorkflowService.NAME.equals(wfName))
                throw new WorkflowException(smG.getString("NiepoprawnaNazwaWorkflow")+": "+wfName);

            return new String[] { wfName, objectId.substring(objectId.indexOf(',')+1) };
        }
    }

    public static WfProcessMgr findProcessMgr(ProcessId processId) throws WorkflowException
    {
    	StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        WfProcessMgr[] mgrs =
            WorkflowServiceFactory.newInstance(processId.getWfName()).
            newService().getProcessMgrs(processId.getPackageId(), processId.getProcessDefinitionId());

        if (mgrs.length > 0)
            return mgrs[0];

        throw new WorkflowException(smG.getString("NieZnalezionoProcesu")+" "+processId);
    }

    /**
     * Ko�czy bie��ce zadanie uprzednio sprawdzaj�c, czy u�ytkownik mo�e
     * to zrobi�. Zapisuje fakt zako�czenia zadania w historii dekretacji
     * dokumentu, oraz w historii zmian dokumentu, wysy�a aktualizacj� do BIP.
     * @see #checkCanFinish(pl.compan.docusafe.core.office.OfficeDocument, WfActivity)
     */
    public static void manualFinish(OfficeDocument document, WorkflowActivity activity)
        throws AccessDeniedException, EdmException
    {
        manualFinish(document, activity, false, new Boolean(true));
    }
    
    /**
     * Ko�czy bie��ce zadanie jesli checkCanFinish = true uprzednio sprawdzaj�c, czy u�ytkownik mo�e
     * to zrobi�.Jesli checkCanFinish = false nie sprawdza warunkow zakonczenia  Zapisuje fakt zako�czenia zadania w historii dekretacji
     * dokumentu, oraz w historii zmian dokumentu, wysy�a aktualizacj� do BIP.
     * @see #checkCanFinish(pl.compan.docusafe.core.office.OfficeDocument, WfActivity)
     */
    public static void manualFinish(OfficeDocument document, WorkflowActivity activity,Boolean checkCanFinish)
        throws AccessDeniedException, EdmException
    {
        manualFinish(document, activity, false, checkCanFinish);
    }

    /**
     * Ko�czy bie��ce zadanie uprzednio sprawdzaj�c, czy u�ytkownik mo�e
     * to zrobi�. Zapisuje fakt zako�czenia zadania w historii dekretacji
     * dokumentu, oraz w historii zmian dokumentu, wysy�a aktualizacj� do BIP.
     * @param beforeInitJbpm okre�la czy to ko�czenie zadania jest wykonywane w celu zainicjowania procesu JBPM
     * je�li tak to nie sprawdzamy warunk�w na status dokumentu i dodajemy inny wpis do historii dekretacji
     *  (w�wczas nie mo�na zako�czy� dokumentu ze statusem W_REALIZACJI i ZAWIESZONY)
     */
    public static void manualFinish(OfficeDocument document, WorkflowActivity activity, boolean beforeInitJbpm,Boolean checkCanFinish)
        throws AccessDeniedException, EdmException
    {
    	if(checkCanFinish)
    	{
    		checkCanFinish(document, activity, true);
    	}

    	if(document.getDocumentKind() != null)
    		document.getDocumentKind().logic().manualFinish(document);
        // TODO: sprawdza�, czy id dokumentu jest zgodne z id zapisanym w parametrze ds_document_id procesu
        // ale raczej w klasach prezentacji, nie tutaj

        WorkflowUtils.findParameter(activity.getActivity().process_context(),
            "ds_finished").value().set(Boolean.TRUE);

        // odczytanie identyfikatora dokumentu zwi�zanego z zadaniem,
        // je�eli nie istnieje - zadanie jest pomijane
/*
        final NameValue nvDocumentId =
            WorkflowUtils.findParameter(activity.process_context(),
                "ds_document_id");
*/

        if (!beforeInitJbpm)
            document.changelog(DocumentChangelog.WF_FINISH);

        if (document instanceof OfficeDocument)
        {
            ((OfficeDocument) document).setCurrentAssignmentGuid(null);
            ((OfficeDocument) document).setCurrentAssignmentUsername(null);
            ((OfficeDocument) document).setCurrentAssignmentAccepted(null);

            if (!beforeInitJbpm)
            {
                StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
                ((OfficeDocument) document).addAssignmentHistoryEntry(
                    new AssignmentHistoryEntry(
                        DSApi.context().getPrincipalName(),
                        isManual(activity.getActivity()) ? smG.getString("doRealizacji") : smG.getString("doWiadomosci"),
                        isManual(activity.getActivity()) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION : AssignmentHistoryEntry.PROCESS_TYPE_CC)
                );
            }
        }

        if (document.getType() == DocumentType.INCOMING)
        {
            InOfficeDocument in = (InOfficeDocument) document;
            in.setStatus(InOfficeDocumentStatus.find(InOfficeDocumentStatus.ID_FINISHED));
            if (in.isSubmitToBip())
            {
            	DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_DOCUMENT_INFO).documentId(in.getId());
            }
            /*
            try
            {
                Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                bip.submit(in);
            }
            catch (ServiceException e)
            {
            }
            */
        }
        document.setFinishDate(new Date());
        document.setFinishUser(DSApi.context().getPrincipalName());
        
        if(document.getDocumentKind() != null && document instanceof OfficeDocument)
        {
        	OfficeDocument doc = (OfficeDocument) document;
        	doc.getDocumentKind().logic().onEndProcess(doc, isManual(activity.getActivity()));
        }
        
        activity.getActivity().complete();
        TaskSnapshot.updateByDocumentId(document.getId(),document.getStringType());
    }

    /**
     * @see #checkCanFinish(pl.compan.docusafe.core.office.OfficeDocument, WfActivity)
     */
    public static boolean canFinish(OfficeDocument document, WorkflowActivity activity)
        throws EdmException
    {
        try
        {
            checkCanFinish(document, activity);
            return true;
        }
        catch (AccessDeniedException e)
        {
            return false;
        }
    }

    /**
     * Metoda sprawdza warunki zako�czenia pracy z dokumentem.  Je�eli zako�czenie
     * pracy nie jest mo�liwe, rzucany jest wyj�tek AccessDeniedException wyja�niaj�cy
     * pow�d.
     * @param document
     * @param activity Bie��cy krok procesu.  Je�eli null, rzucany jest wyj�tek
     *  AccessDeniedException.
     * @throws AccessDeniedException
     * @throws EdmException
     */
    public static void checkCanFinish(OfficeDocument document, WorkflowActivity activity)
        throws AccessDeniedException, EdmException
    {
        checkCanFinish(document, activity, true);
    }

    public static void checkCanFinish(OfficeDocument document, WorkflowActivity activity, boolean beforeInitJbpm)
        throws AccessDeniedException, EdmException
    {
    	StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        if (activity == null)
        {
            throw new AccessDeniedException(smG.getString("NieMoznaZakonczycZadaniaAbyZakonczycZadaniePrzejdzNaListeZadanUzytkownika"));
        }

        if (!beforeInitJbpm && !DSApi.context().hasPermission(DSPermission.ZADANIE_KONCZENIE))
        {
            throw new AccessDeniedException(smG.getString("UzytkownikNieMaUprawnieniaDoKonczeniaPracyZpismem"));
        }

        // warunki zako�czenia procesu wynikaj�ce z danego rodzaju dokumentu
        if (document.getDocumentKind() != null)
        {
            document.getDocumentKind().logic().checkCanFinishProcess(document);
        }

    }

    public static boolean canAssign(OfficeDocument document, WorkflowActivity activity)
        throws AccessDeniedException, EdmException
    {
        try
        {
            checkCanAssign(document, activity);
            return true;
        }
        catch (AccessDeniedException e)
        {
            return false;
        }
    }

    public static void checkCanAssign(OfficeDocument document, WorkflowActivity activity)
        throws EdmException
    {
    	StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        if (activity != null && !(WorkflowFactory.getInstance().isManual(activity) || AvailabilityManager.isAvailable("workflow.assignCC")))
            throw new AccessDeniedException(smG.getString("NieMoznaZadekretowacPismaOtrzymanegoDoWiadomosci"));

        if (!DSApi.context().hasPermission(DSPermission.WWF_DEKRETACJA_DOWOLNA))
            throw new AccessDeniedException(smG.getString("UzytkownikNieMaUprawnienDoDekretowaniaPism"));

        checkCanAssignOrFinishProcessWithDeadline(document, activity);
    }

    private static void checkCanAssignOrFinishProcessWithDeadline(OfficeDocument document, WorkflowActivity activity)
        throws EdmException
    {
    	StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
    	
    	
        // Brief IV: je�eli wyznaczono termin zako�czenia zadania,
        // u�ytkownik nie mo�e zako�czy� pracy z dokumentem, ani
        // przedekretowa� go dalej, dop�ki nie doda uwagi
            if (activity.getDeadlineTime() != null)
            {
                List<Remark> remarks = document.getRemarks();
                boolean found = false;
                for (Remark remark : remarks)
                {
                    if (remark.getCtime().getTime() > activity.getReceiveDate().getTime() &&
                        remark.getAuthor().equals(DSApi.context().getPrincipalName()))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    throw new AccessDeniedException(smG.getString("NieMoznaZakonczyCzadaniaZokreslonymTerminem")+" " +
                        smG.getString("wykonaniaNieDodajacUwagi"));
                }
            }
        
    }

    /**
     * Zwraca list� ostrze�e� dla u�ytkownika chc�cego zako�czy� prac� z dokumentem
     * @return Lista ostrze�e� (mo�e by� pusta).
     */
    public static List<String> getFinishWarnings(OfficeDocument document)
        throws EdmException
    {
    	StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        List<String> warnings = new ArrayList<String>();

        if (Configuration.hasExtra("business"))
        {
            if (
            		(document.getBox() == null && AvailabilityManager.isAvailable("zamknijzadanie.needBox") &&
            		!document.hasWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX))
            		&& 
            		(
            				document.getType() != DocumentType.INCOMING
            				|| 
            				(
	                    		((InOfficeDocument) document).getKind() != null &&  
	                    		((InOfficeDocument) document).getKind().getName().indexOf("iznesowy") > 0
                    		)
                    )
               )
            {
                warnings.add(smG.getString("NiePodanoNumeruPudlaArchiwalnego"));
            }
        }

        if (document.getOfficeNumber() == null)
        {
            warnings.add(smG.getString("DokumentNieMaNumeruKancelaryjnego"));
        }

        return warnings;
    }
    
    public static Long reopenWf(Long documentId) throws EdmException
    {
    	return reopenWf(documentId,null);
    }

    public static Long reopenWf(Long documentId,String user) throws EdmException
    {
    	Long result = new Long(-1);
    	StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        boolean hasTasks = InternalWorkflowService.documentHasTasks(documentId, WorkflowUtils.iwfPackageName(), WorkflowUtils.iwfManualName());

        if (hasTasks)
            throw new EdmException(smG.getString("NieMoznaWznowicProcesuDlaDokumentu")+" " +
                smG.getString("poniewazInnyUzytkownikMaGoNaSwojejLiscieZadan"));

        if (Configuration.officeAvailable() || Configuration.faxAvailable())
        {
        	if(user == null)
        		user = DSApi.context().getPrincipalName();
            Document document = Document.find(documentId);
           // DSDivision[] divisions = DSApi.context().getDSUser().getDivisions();
            DSDivision[] divisions = DSUser.findByUsername(user).getDivisions();

            String userDivisionGuid = null;
            if (divisions.length > 0)
            {
                for (int i=0; i < divisions.length; i++)
                {
                    if (divisions[i].isPosition())
                    {
                        userDivisionGuid = divisions[i].getParent().getGuid();
                        break;
                    }
                    else if (divisions[i].isDivision())
                    {
                        userDivisionGuid = divisions[i].getGuid();
                        break;
                    }
                }
            }

            if (userDivisionGuid == null)
                userDivisionGuid = DSDivision.ROOT_GUID;

            // tworzenie i uruchamianie wewn�trznego procesu workflow
            WfService ws = WorkflowServiceFactory.newInternalInstance().newService();
            ws.connect(user);

            WfProcessMgr mgr = ws.getProcessMgrs(WorkflowUtils.iwfPackageName(), WorkflowUtils.iwfManualName())[0];
            WfProcess proc = mgr.create_process(null);
            NameValue[] procCtx = proc.process_context();
            WorkflowUtils.findParameter(procCtx, "ds_document_id").value().set(documentId);
            WorkflowUtils.findParameter(procCtx, "ds_document_id").setValueInt(documentId);
            // do kontroli uprawnie� - wf z tego nie korzysta
            WorkflowUtils.findParameter(procCtx, "ds_finished").value().set(Boolean.FALSE);
            WorkflowUtils.findParameter(procCtx, "ds_last_activity_user").value().set(user);
            WorkflowUtils.findParameter(procCtx, "ds_objective").value().set("ProcesWznowiony");
            // parametry u�ywane podczas tworzenia historii dekretacji
            // w AcquireTaskAction
            // dotyczy tylko wbudowanego procesu dekretacji r�cznej
            // (s� tam wymagane zmienne)
            WorkflowUtils.findParameter(procCtx, "ds_source_user").value().set(user);
            WorkflowUtils.findParameter(procCtx, "ds_source_division").value().set(userDivisionGuid);

            // id uczestnika (patrz WfProcessImpl.findResources())
            WorkflowUtils.findParameter(procCtx, "ds_participant_id").value().set(user);
            WorkflowUtils.findParameter(procCtx, "ds_assigned_division").value().set(userDivisionGuid);
            WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(user);

            proc.start();
            result = ((WfProcessImpl)proc).getId();
            

            document.changelog(DocumentChangelog.WF_START_MANUAL);
            
            ((OfficeDocument)document).addAssignmentHistoryEntry(
            		new AssignmentHistoryEntry(DSApi.context().getPrincipalName()));
        }
        return result;
    }

    /**
     * @TODO - co ta metoda robi?
     * Sprawdza czy mozna zakonczyc zadanie na potrzeby umieszczenia przycisku fillform
     * @param document
     * @param activity
     * @return
     * @throws EdmException
     */
    public static FinishOrAssignState getFinishOrAssignState(OfficeDocument document, WorkflowActivity activity)
        throws EdmException
    {
        List<String> finishWarnings = getFinishWarnings(document);
        List<String> assignWarnings = Collections.emptyList();
        boolean canFinish = false;
        List<String> cantFinishReasons = null;
        try
        {
        	if (activity == null) {
        		throw new AccessDeniedException("Zadanie nie istnieje");
        	}
            checkCanFinish(document, activity);
            canFinish = true;
        }
        catch (EdmException e)
        {
            // TODO: MultiMessageEdmException
            cantFinishReasons = Collections.singletonList(e.getMessage());
        }
        boolean canAssign = false;
        List<String> cantAssignReasons = null;
        try
        {	
        	if (activity == null) {
        		throw new AccessDeniedException("Zadanie nie istnieje");
    		}
            checkCanAssign(document, activity);
            canAssign = true;
        }
        catch (AccessDeniedException e)
        {
            cantAssignReasons = Collections.singletonList(e.getMessage());
        }

        FinishOrAssignState state = new FinishOrAssignState(
            canFinish, canAssign,
            finishWarnings, assignWarnings,
            cantFinishReasons, cantAssignReasons
        );

        return state;
    }

    }
