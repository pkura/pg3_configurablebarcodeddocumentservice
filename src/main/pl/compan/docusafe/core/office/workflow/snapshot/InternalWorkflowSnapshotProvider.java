package pl.compan.docusafe.core.office.workflow.snapshot;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.logic.AttachmentsManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa zapewniajaca
 */
public class InternalWorkflowSnapshotProvider extends BaseSnapshotProvider
{
	private final static Logger log = LoggerFactory.getLogger(InternalWorkflowSnapshotProvider.class);

	@Override
	public void updateTaskList(Session session,Long id, String doctype,int updateType,String username) throws Exception
	{
		Transaction tx = null;
		log.debug("Update id={} doctype={}",id,doctype);
		PreparedStatement psIn = null;
		PreparedStatement psOut = null;
		PreparedStatement psOrd = null;
        String sqlIn = null;

		try
		{
            //wyczy�� nieaktualne dane (zaraz wstawimy nowe)
			deleteFromSnapshot(session,id,updateType,doctype, username);

			//mapa, ktora zawiera pary w postaci id dockinda <--> nazwa dockinda
			Map<Integer, String> dockindNames = new HashMap<Integer, String>();
			TaskSnapshot task;

			tx = session.beginTransaction();
			log.debug("Otworzylem transackje {}",tx );

			if(doctype.equals(InOfficeDocument.TYPE) || doctype.equals("anyType"))
			{
				log.debug("IN");
				sqlIn = getInSQL(username, updateType);
				psIn = DSApi.context().prepareStatement(sqlIn);
				setUpSnapshotStatement(psIn, updateType, username, id);
				ResultSet rsIn = psIn.executeQuery();
				while(rsIn.next())
				{
					task = new TaskSnapshot();
					setTaskByInResultSet(task, rsIn,dockindNames);
					session.save(task);
					log.debug("zapisalem "+task.getDocumentId());
				}
				rsIn.close();
				DSApi.context().closeStatement(psIn);
				psIn = null;

			}

			if(doctype.equals(OutOfficeDocument.TYPE) || doctype.equals(OutOfficeDocument.INTERNAL_TYPE) || doctype.equals("anyType"))
			{
				log.debug("OUT");
				String sqlOut = getOutSQL(username, updateType);
				psOut = DSApi.context().prepareStatement(sqlOut);
				setUpSnapshotStatement(psOut, updateType, username, id);
				ResultSet rsOut = psOut.executeQuery();
				while(rsOut.next())
				{
					task = new TaskSnapshot();
					setTaskByOutResultSet(task, rsOut,dockindNames);
					session.save(task);
					log.debug("zapisalem {}",task.getDocumentId());
				}
				rsOut.close();
				DSApi.context().closeStatement(psOut);
				psOut = null;

			}
        /* */
			if(doctype.equals(OfficeOrder.TYPE) || doctype.equals("anyType"))
			{
				log.debug("ORD");
				String sqlOrd = getOrdSQL(username, updateType);
				psOrd = DSApi.context().prepareStatement(sqlOrd);
				setUpSnapshotStatement(psOrd, updateType, username, id);
				ResultSet rsOrd = psOrd.executeQuery();

				while(rsOrd.next())
				{
					task = new TaskSnapshot();
					setTaskByOrdResultSet(task, rsOrd,dockindNames);
					log.debug("pobralem "+task.getDocumentId());
					session.save(task);
					log.debug("zapisalem "+task.getDocumentId());
				}
				rsOrd.close();
				DSApi.context().closeStatement(psOrd);
				psOrd = null;
			}



			tx.commit();
			log.debug("commit transakcji " + tx + " wykonany ");
			tx = null;
     }
     catch (Exception e)
     {
    	 log.error("",e);
        throw new Exception("SQL - "+sqlIn+" \n"+e.getMessage(),e);
     }
     finally
     {
    	 DSApi.context().closeStatement(psIn);
    	 DSApi.context().closeStatement(psOut);
    	 DSApi.context().closeStatement(psOrd);
            if (tx!=null){
				try	{
					log.debug("Wykonuje rollback transackji " + tx );
					tx.rollback();
				} catch (Exception eee)  {
					log.debug("Rollback transakcji " + tx + " niemozliwy",eee);
				}
			}
     }
  }
	@Override
	protected String prepareInSQL(String username,int updateType)
	{
		String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
		boolean countReplies = Configuration.isAdditionOn(Configuration.ADDITION_COUNT_REPLIES_ON_TASKLIST);

		return

		"select distinct " +
        "dsw_assignment.id, " +
        "dsw_assignment.accepted, " +
        "doc.id, " +
        "doc.officenumber, " +
        "doc.officenumberyear, \n" +
        "dsw_activity.laststatetime, " +
        "dsw_activity.name, " +
        "dso_person.firstname,\n" +
        "dso_person.lastname,\n" +
        "dso_person.organization,\n" +//10
        "dso_person.street,\n" +
        "dso_person.location,\n" +
        "lac.param_value,\n" +
        "obj.param_value,\n" +
        "dsw_activity.activity_key,\n"+
        "doc.summary,\n"+
        "au.param_value,\n"+
        "doc.referenceid,\n"+
        "dsw_process.name,\n"+
        "dsdoc.source,\n"+//20
        "dsw_process.packageid,\n"+
        "dsw_process.processdefinitionid,\n"+
        "dsw_resource.res_key, "+
        "dsw_process.deadlinetime, "+
        "dsw_process.deadlineremark, "+
        "dsw_process.deadlineauthor, "+
        "dso_container.id,"+
        "dso_container.officeid, "+
        "doc.creatingUser, "+
        "doc.clerk, " +//30
        (countReplies ? "(select count(indocument) from dso_out_document where indocument=doc.id), " : "0, ")+  //ilosc odpowiedzi na pismo
        "doc.incomingDate,"+
        "dsdoc.dockind_id,"+
        "doc.currentassignmentguid, "+
        "dsdoc.ctime, "+
        "doc.replyunnecessary, "+
        "dsdoc.lastRemark, "+
        "doc.DOCUMENTDATE, " +
        "obj2.param_value, " +
        "prtp.param_value, " +//40
        "dso_container.finishdate, " +
        "doc.dailyOfficeNumber, " +
        "dsw_assignment.resource_count " + //43
        "from\n" +
        "dsw_assignment "+nolock+" inner join dsw_resource "+nolock+" on (dsw_assignment.resource_id = dsw_resource.id "+(username != null ? "and dsw_resource.res_key = ?":"" )+")\n" +
        "inner join dsw_activity "+nolock+" on dsw_activity.id = dsw_assignment.activity_id \n" +
        "inner join dsw_activity_context obj "+nolock+" on (obj.activity_id = dsw_activity.id and obj.param_name = 'ds_objective')\n" +
        "inner join dsw_activity_context obj2 "+nolock+" on (obj2.activity_id = dsw_activity.id and obj2.param_name = 'ds_assigned_division')\n" +
        "inner join dsw_activity_context prtp "+nolock+" on (prtp.activity_id = dsw_activity.id and prtp.param_name = 'ds_participant_id')\n" +
        "inner join dsw_process "+nolock+" on dsw_process.id = dsw_activity.process_id \n" +
        "inner join dsw_process_context docid "+nolock+" on docid.process_id = dsw_process.id \n" +
        "left outer join dsw_process_context lac "+nolock+" on (lac.process_id = dsw_process.id and lac.param_name='ds_last_activity_user')\n" +
        "left outer join dsw_process_context au "+nolock+" on (au.process_id = dsw_process.id and au.param_name='ds_assigned_user')\n" +
        "inner join dso_in_document doc "+nolock+" on (doc.id = docid.param_value_int and "+ (updateType == 1 ? "doc.id = ? and":"" )+" docid.param_name = 'ds_document_id' )\n" +
        "inner join ds_document dsdoc "+nolock+" on (doc.id = dsdoc.id) \n"+
        "left outer join dso_person "+nolock+" on (doc.sender = dso_person.id and dso_person.discriminator='SENDER')\n"+
        "left outer join dso_container"+nolock+" on doc.case_id = dso_container.id\n";
	}

	@Override
	protected String prepareOutSQL(String username,int updateType)
	{
		String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
		boolean countReplies = Configuration.isAdditionOn(Configuration.ADDITION_COUNT_REPLIES_ON_TASKLIST);

		return

		"select distinct " +
        "dsw_assignment.id, " +
        "dsw_assignment.accepted, " +
        "doc.id, " +
        "doc.officenumber, " +
        "doc.officenumberyear, \n" +
        "dsw_activity.laststatetime, " +
        "dsw_activity.name, " +
        "dso_person.firstname,\n" +
        "dso_person.lastname,\n" +
        "dso_person.organization,\n" +//10
        "dso_person.street,\n" +
        "dso_person.location,\n" +
        "lac.param_value,\n" +
        "obj.param_value,\n" +
        "dsw_activity.activity_key,\n"+
        "doc.summary,\n"+
        "au.param_value,\n"+
        "dsw_process.name,\n"+
        "doc.internaldocument,\n"+
        "dsdoc.source,\n"+//20
        "rcpt.firstname,\n" +
        "rcpt.lastname,\n" +
        "rcpt.organization,\n" +
        "rcpt.street,\n" +
        "rcpt.location,\n" +
        "dsw_process.packageid,\n"+
        "dsw_process.processdefinitionid,\n"+
        "dsw_resource.res_key, "+
        "dsw_process.deadlinetime, "+
        "dsw_process.deadlineremark, "+//30
        "dsw_process.deadlineauthor, "+
        "dso_container.id,"+
        "dso_container.officeid, "+
        "doc.creatingUser,"+
        "doc.prepstate,"+
        "deliv.name, "+
        "doc.clerk, "+
        "doc.currentassignmentguid as currentassignmentguid, "+
        "dsdoc.ctime, "+
        "dsdoc.dockind_id, "+//40
        "dsdoc.lastRemark, "+
        "doc.DOCUMENTDATE, "+
        "doc.referenceid, "+
        "obj2.param_value, " +
        (countReplies ? "(select count(replied_document_id) from ds_document where replied_document_id=doc.id) as answer_number, " : "0 as answer_number, ")+  //ilosc odpowiedzi na pismo
        "participant.param_value, " +
        "doc.dailyOfficeNumber " +//47
        "from\n" +
        "dsw_assignment "+nolock+" inner join dsw_resource "+nolock+" on (dsw_assignment.resource_id = dsw_resource.id "+(username != null ? "and dsw_resource.res_key = ?":"" )+")\n" +
        "inner join dsw_activity "+nolock+" on dsw_activity.id = dsw_assignment.activity_id \n" +
        "inner join dsw_activity_context obj "+nolock+" on (obj.activity_id = dsw_activity.id and obj.param_name = 'ds_objective')\n" +
        "inner join dsw_activity_context obj2 "+nolock+" on (obj2.activity_id = dsw_activity.id and obj2.param_name = 'ds_assigned_division')\n" +
        "inner join dsw_activity_context participant "+nolock+" on (participant.activity_id = dsw_activity.id and participant.param_name = 'ds_participant_id')\n" +
        "inner join dsw_process "+nolock+" on dsw_process.id = dsw_activity.process_id \n" +
        "inner join dsw_process_context docid "+nolock+" on docid.process_id = dsw_process.id \n" +
        "left outer join dsw_process_context lac "+nolock+" on (lac.process_id = dsw_process.id and lac.param_name='ds_last_activity_user')\n" +
        "left outer join dsw_process_context au "+nolock+" on (au.process_id = dsw_process.id and au.param_name='ds_assigned_user')\n" +
        "inner join dso_out_document doc "+nolock+" on (doc.id  = docid.param_value_int and "+ (updateType == 1 ? "doc.id = ? and":"" )+" docid.param_name = 'ds_document_id')\n" +
        "inner join ds_document dsdoc "+nolock+" on (doc.id = dsdoc.id) \n"+
        "left outer join dso_person "+nolock+" on (doc.sender = dso_person.id and dso_person.discriminator='SENDER')\n"+
        "left outer join dso_person rcpt "+nolock+" on (doc.id = rcpt.document_id and (rcpt.posn = 0 or rcpt.posn IS NULL) and rcpt.discriminator='RECIPIENT')\n"+
        "left outer join dso_out_document_delivery deliv "+nolock+" on (doc.delivery = deliv.id)\n"+
        "left outer join dso_container"+nolock+" on doc.case_id = dso_container.id\n";

	}

	@Override
	protected String prepareOrdSQL(String username,int updateType)
	{
		String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
		return "select " +
        "dsw_assignment.id, " +
        "dsw_assignment.accepted, " +
        "doc.id, " +
        "doc.officenumber, " +
        "doc.officenumberyear, \n" +
        "dsw_activity.laststatetime, " +
        "dsw_activity.name, " +
        "lac.param_value,\n" +
        "obj.param_value,\n" +
        "dsw_activity.activity_key,\n"+
        "doc.summary,\n"+
        "au.param_value,\n"+
        "dsw_process.name,\n"+
        "dsdoc.source,\n"+
        "dsw_process.packageid,\n"+
        "dsw_process.processdefinitionid,\n"+
        "dsw_resource.res_key, "+
        "dsw_process.deadlinetime, "+
        "dsw_process.deadlineremark, "+
        "dsw_process.deadlineauthor, "+
        "dso_container.id,"+
        "dso_container.officeid, "+
        "doc.creatingUser, "+
        "doc.currentassignmentguid, "+
        "dsdoc.ctime, "+
        "dsdoc.lastRemark, "+
        "doc.dailyOfficeNumber " +
        "from\n" +
        "dsw_assignment "+nolock+" inner join dsw_resource "+nolock+" on (dsw_assignment.resource_id = dsw_resource.id  "+(username != null ? "and dsw_resource.res_key = ?":"" )+")\n" +
        "inner join dsw_activity "+nolock+" on dsw_activity.id = dsw_assignment.activity_id \n" +
        "inner join dsw_activity_context obj "+nolock+" on (obj.activity_id = dsw_activity.id and obj.param_name = 'ds_objective')\n" +
        "inner join dsw_process "+nolock+" on dsw_process.id = dsw_activity.process_id \n" +
        "inner join dsw_process_context docid "+nolock+" on docid.process_id = dsw_process.id \n" +
        "left outer join dsw_process_context lac "+nolock+" on (lac.process_id = dsw_process.id and lac.param_name='ds_last_activity_user')\n" +
        "left outer join dsw_process_context au "+nolock+" on (au.process_id = dsw_process.id and au.param_name='ds_assigned_user')\n" +
        "inner join dso_order doc "+nolock+" on (doc.id  = docid.param_value_int and "+ (updateType == 1 ? "doc.id = ? and":"" )+" docid.param_name = 'ds_document_id')\n" +
        "inner join ds_document dsdoc "+nolock+" on (doc.id = dsdoc.id) \n"+
        "left outer join dso_container"+nolock+" on doc.case_id = dso_container.id\n";

	}

	protected void setTaskByInResultSet(TaskSnapshot task, ResultSet rsIn,Map<Integer, String> dockindNames) throws SQLException,EdmException
	{
		task.setDsw_assignment_id(rsIn.getLong(1));
        task.setAccepted(rsIn.getBoolean(2));
        long docid = rsIn.getLong(3);
        log.debug("Inicjuje task dla document_id = {}",docid);
        task.setDocumentId(docid);
        int ko = rsIn.getInt(4);
        if(!rsIn.wasNull())
        	task.setDocumentOfficeNumber(ko);
        int dko = rsIn.getInt(42);
        if(!rsIn.wasNull())
        	task.setDailyOfficeNumber(dko);
        int koYear = rsIn.getInt(5);
        if (!rsIn.wasNull())
            task.setDocumentOfficeNumberYear(new Integer(koYear));
        task.setRcvAndReceiveDate(rsIn.getTimestamp(6));
        task.setDescription(cutString(rsIn.getString(7), 200, "Description"));
        task.setPerson_firstname(rsIn.getString(8));
        task.setPerson_lastname(rsIn.getString(9));
        task.setPerson_organization(cutString(rsIn.getString(10), 40, "Person_organization"));        
        task.setPerson_street(rsIn.getString(11));
        task.setPerson_location(rsIn.getString(12));
        task.setActivity_lastActivityUser(rsIn.getString(13));
        task.setObjective(rsIn.getString(14));
        task.setActivityKey(rsIn.getString(15));
        task.setDocumentSummary(cutString(rsIn.getString(16), 254, "DocumentSummary"));
        task.setAssigned_user(rsIn.getString(17));
        task.setDocumentReferenceId(rsIn.getString(18));
        task.setProcess(rsIn.getString(19));
        task.setDocument_source(rsIn.getString(20));
        task.setPackageId(rsIn.getString(21));
        task.setProcessDefinitionId(rsIn.getString(22));
        task.setDsw_resource_res_key(rsIn.getString(23));
        task.setDeadlineTime(rsIn.getDate(24));
        task.setDeadlineRemark(rsIn.getString(25));
        task.setDeadlineAuthor(rsIn.getString(26));
        task.setDocumentType(InOfficeDocument.TYPE);
        task.setCaseId(rsIn.getLong(27));
        task.setOfficeCase(rsIn.getString(28));
        task.setDocumentAuthor(rsIn.getString(29));
        task.setDocumentClerk(rsIn.getString(30));
        if(rsIn.getInt(36) == 0 )
        	task.setAnswerCounter(rsIn.getInt(31));
        else
        	task.setAnswerCounter(-1);
        task.setIncomingDate(rsIn.getTimestamp(32));
        task.setExternalWorkflow(Boolean.FALSE);
        task.setLabelhidden(hasHiddingLabel(docid, task.getDsw_resource_res_key()));
        task.setIsAnyImageInAttachments(AttachmentsManager.isAnyImageInAttachments(docid));
        task.setIsAnyPdfInAttachments(AttachmentsManager.isAnyPdfInAttachments(docid));
        int dockind_id = rsIn.getInt(33);


        if(dockind_id!=0)
        {
        	setDockindFields(docid, dockind_id, task);
        }
        else
        {
        	task.setDocumentDate(rsIn.getDate(38));
        }

        task.setDivisionGuid(rsIn.getString(34));
        task.setDocumentCtimeAndCdate(rsIn.getTimestamp(35));
        task.setLastRemark(cutString(rsIn.getString(37), 240, "LastRemark"));
        task.setBackupPerson(false);
        if (rsIn.getDate(41) != null)
            task.setCaseDeadlineDate(rsIn.getDate(41));
        try
        {
        	if(!rsIn.getBoolean(2) && rsIn.getString(34) != null)
        	{
        		if(DSDivision.isInDivisionAsBackup(DSDivision.find(rsIn.getString(34)), rsIn.getString(23)))
        		{
        			if (rsIn.getString(40).startsWith("ds_guid")) {
        				task.setBackupPerson(true);
        			}
        		}
        	}
		}
        catch (Exception e)
        {
        	log.error("Poprawic to jest brana zla kolumna pod uwage",e);

        }
        task.setSameProcessSnapshotsCount(rsIn.getInt(43));
	}

	protected void setTaskByOutResultSet(TaskSnapshot task, ResultSet rsOut,Map<Integer, String> dockindNames) throws SQLException,EdmException
	{
		task.setDsw_assignment_id(rsOut.getLong(1));
        task.setAccepted(rsOut.getBoolean(2));
        long docid = rsOut.getLong(3);
        log.debug("Inicjuje tast dla document_id = "+docid);
        task.setDocumentId(docid);
        int ko = rsOut.getInt(4);
        if(!rsOut.wasNull())
        	task.setDocumentOfficeNumber(ko);
        int dko = rsOut.getInt(47);
        if(!rsOut.wasNull())
        	task.setDailyOfficeNumber(dko);
        int koYear = rsOut.getInt(5);
        if (!rsOut.wasNull())
            task.setDocumentOfficeNumberYear(new Integer(koYear));
        task.setRcvAndReceiveDate(rsOut.getTimestamp(6));
        task.setDescription(rsOut.getString(7));
        task.setPerson_firstname(rsOut.getString(8));
        task.setPerson_lastname(rsOut.getString(9));
        task.setPerson_organization(cutString(rsOut.getString(10), 40, "Person_organization"));
        task.setPerson_street(rsOut.getString(11));
        task.setPerson_location(rsOut.getString(12));
        task.setActivity_lastActivityUser(rsOut.getString(13));
        task.setObjective(rsOut.getString(14));
        task.setActivityKey(rsOut.getString(15));
        task.setDocumentSummary(rsOut.getString(16));
        task.setAssigned_user(rsOut.getString(17));
        task.setProcess(rsOut.getString(18));
        task.setDocument_source(rsOut.getString(20));
        task.setPackageId(rsOut.getString(26));
        task.setProcessDefinitionId(rsOut.getString(27));
        task.setDsw_resource_res_key(rsOut.getString(28));
        task.setDeadlineTime(rsOut.getDate(29));
        task.setDeadlineRemark(rsOut.getString(30));
        task.setDeadlineAuthor(rsOut.getString(31));

        String type;
        if(rsOut.getBoolean(19)) type = OutOfficeDocument.INTERNAL_TYPE;
        else type=OutOfficeDocument.TYPE;
        task.setDocumentType(type);

        task.setRcpt_firstname(rsOut.getString(21));
        task.setRcpt_lastname(rsOut.getString(22));
        task.setRcpt_organization(cutString(rsOut.getString(23), 40, "Person_organization"));
        task.setRcpt_street(rsOut.getString(24));
        task.setRcpt_location(rsOut.getString(25));
        task.setCaseId(rsOut.getLong(32));
        task.setOfficeCase(rsOut.getString(33));
        task.setDocumentAuthor(rsOut.getString(34));
        task.setDocumentPrepState(OutOfficeDocument.getPrepStateString(rsOut.getString(35)));
        task.setDocumentDelivery(rsOut.getString(36));
        task.setDocumentClerk(rsOut.getString(37));
        task.setDivisionGuid(rsOut.getString(38));
        task.setDocumentCtimeAndCdate(rsOut.getTimestamp(39));
        task.setExternalWorkflow(Boolean.FALSE);
        task.setLastRemark(rsOut.getString(41));
        task.setDocumentReferenceId(rsOut.getString(43));
        int dockind_id = rsOut.getInt(40);

        task.setAnswerCounter(rsOut.getInt("answer_number"));
        task.setLabelhidden(hasHiddingLabel(docid, task.getDsw_resource_res_key()));
        task.setIsAnyImageInAttachments(AttachmentsManager.isAnyImageInAttachments(docid));
        task.setIsAnyPdfInAttachments(AttachmentsManager.isAnyPdfInAttachments(docid));
        if(dockind_id!=0)
        {
        	setDockindFields(docid, dockind_id, task);
        }
        else
        {
        	task.setDocumentDate(rsOut.getDate(42));
        }
        task.setBackupPerson(false);
        try
        {
        	if(!rsOut.getBoolean(2) && rsOut.getString(43) != null)
        	{
        		if(DSDivision.isInDivisionAsBackup(DSDivision.find(rsOut.getString(43)), rsOut.getString(28)))
        		{
        			if (rsOut.getString(46).startsWith("ds_guid")) {
        				task.setBackupPerson(true);
        			}
        		}
        	}
        }
        catch (Exception e)
        {
        	log.error("Poprawic to kurka wodna",e);
        }
	}

	protected void setTaskByOrdResultSet(TaskSnapshot task, ResultSet rsOrd,Map<Integer, String> dockindNames) throws SQLException,EdmException
	{
		task.setDsw_assignment_id(rsOrd.getLong(1));
		task.setAccepted(rsOrd.getBoolean(2));
        task.setDocumentId(rsOrd.getLong(3));
        int ko = rsOrd.getInt(4);
        if(!rsOrd.wasNull())
        	task.setDocumentOfficeNumber(ko);
        int dko = rsOrd.getInt(27);
        if(!rsOrd.wasNull())
        	task.setDailyOfficeNumber(dko);
        int koYear = rsOrd.getInt(5);
        if (!rsOrd.wasNull())
            task.setDocumentOfficeNumberYear(new Integer(koYear));
        task.setRcvAndReceiveDate(rsOrd.getDate(6));
        task.setDescription(rsOrd.getString(7));
        task.setPerson_firstname("");
        task.setPerson_lastname("");
        task.setPerson_organization("");
        task.setPerson_street("");
        task.setPerson_location("");
        task.setActivity_lastActivityUser(rsOrd.getString(8));
        task.setObjective(rsOrd.getString(9));
        task.setActivityKey(rsOrd.getString(10));
        task.setDocumentSummary(rsOrd.getString(11));
        task.setAssigned_user(rsOrd.getString(12));
        task.setProcess(rsOrd.getString(13));
        task.setDocument_source(rsOrd.getString(14));
        task.setPackageId(rsOrd.getString(15));
        task.setProcessDefinitionId(rsOrd.getString(16));
        task.setDsw_resource_res_key(rsOrd.getString(17));
        task.setDeadlineTime(rsOrd.getDate(18));
        task.setDeadlineRemark(rsOrd.getString(19));
        task.setDeadlineAuthor(rsOrd.getString(20));
        task.setDocumentType(OfficeOrder.TYPE);
        task.setCaseId(rsOrd.getLong(21));
        task.setOfficeCase(rsOrd.getString(22));
        task.setDocumentAuthor(rsOrd.getString(23));
        task.setDivisionGuid(rsOrd.getString(24));
        task.setDocumentCtimeAndCdate(rsOrd.getTimestamp(25));
        task.setExternalWorkflow(Boolean.FALSE);
        task.setLastRemark(rsOrd.getString(26));
        task.setLabelhidden(hasHiddingLabel(task.getDocumentId(), task.getDsw_resource_res_key()));
        task.setIsAnyImageInAttachments(AttachmentsManager.isAnyImageInAttachments(task.getDocumentId()));
        task.setIsAnyPdfInAttachments(AttachmentsManager.isAnyPdfInAttachments(task.getDocumentId()));
	}


  protected void setUpSnapshotStatement(PreparedStatement ps, int updateType, String username,Long id) throws SQLException
  {
	  if(username != null)
      {
      	ps.setString(1, username);
      	 if( updateType == 1 && id != null)
           	ps.setLong(2, id);
      }
      else
      {
      	if(updateType == 1 && id != null)
      		ps.setLong(1, id);
      }
  }
  

	@Override
	public TaskPositionBean getTaskPosition(String activityKey) 
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			log.trace("select dsw_resource_res_key,dsw_assignment_accepted,divisionguid from dsw_tasklist where dsw_activity_activity_key = {}",activityKey);
			ps = DSApi.context().prepareStatement("select dsw_resource_res_key,dsw_assignment_accepted,divisionguid from dsw_tasklist where dsw_activity_activity_key = ? ");
			ps.setString(1, activityKey);
			rs = ps.executeQuery();
			String resource = null;
			Integer position = null;
			String dsParticipantId = null;
			boolean accept = false;
			if(rs.next())
			{
				resource = rs.getString(1);
				accept = rs.getBoolean(2);
				dsParticipantId = rs.getString(3);
				log.trace("resource ={}, dsParticipantId = {},accept ={}",resource,dsParticipantId,accept);
			}
			else
			{
				return null;
			}
			if(rs.next())
			{/**na grupe*/
				ps.close();
				rs.close();
				log.trace("select na grupe");
				ps = DSApi.context().prepareStatement("select ac.param_value from dsw_activity aa, dsw_activity_context ac where aa.id = ac.activity_id and aa.activity_key = ? and ac.param_name = 'ds_participant_id'");
				ps.setString(1, activityKey);
				rs = ps.executeQuery();
				
				if(rs.next())
				{
					dsParticipantId = rs.getString(1); 
					ps.close();
					rs.close();
					ps = DSApi.context().prepareStatement("select count(1) from  dsw_activity aa, dsw_activity_context ac where aa.id = ac.activity_id and aa.laststatetime < " +
							"(select aa2.laststatetime  from dsw_activity aa2 where activity_key = ?) and ac.param_name = 'ds_participant_id' and ac.param_value = ? ");
					ps.setString(1, activityKey);
					ps.setString(2, dsParticipantId);
					rs = ps.executeQuery();
					if(rs.next())
						position = rs.getInt(1);
					log.trace("dsParticipantId {}",dsParticipantId);
					dsParticipantId = dsParticipantId.substring(8);
					resource = null;
					log.trace("substring dsParticipantId {}",dsParticipantId);
				}
			}
			else
			{/**na uzytkownika*/
				ps.close();
				rs.close();
				log.trace("select na uzytkownika {}",resource);
				ps = DSApi.context().prepareStatement("select count(id) from dsw_tasklist where dsw_activity_laststatetime < " +
						"(select dsw_activity_laststatetime from dsw_tasklist where dsw_activity_activity_key = ? ) and dsw_resource_res_key = ?");
				ps.setString(1, activityKey);
				ps.setString(2, resource);
				rs = ps.executeQuery();
				if(rs.next())
					position = rs.getInt(1);
			}
			TaskPositionBean bean = new TaskPositionBean();
			bean.setAccept(accept);
			if(dsParticipantId != null)
				bean.setDivision(dsParticipantId);
			bean.setPosition(position+1);
			bean.setUser(resource);
			return bean;
		}
		catch (Exception e) 
		{
			log.error("",e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return null;
	}
}
