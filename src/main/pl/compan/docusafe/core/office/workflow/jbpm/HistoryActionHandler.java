package pl.compan.docusafe.core.office.workflow.jbpm;

import java.util.Calendar;
import java.util.Date;

import org.jbpm.context.exe.JbpmType;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
/* User: Administrator, Date: 2007-02-27 10:10:37 */
import pl.compan.docusafe.util.DateUtils;

/**
 * "Handler" odpowiedzialny za wpisywanie do historii dekretacji informacji o procesach JBPM.
 * Przy okazji spe�nia te� kilka dodatkowych funkcji
 * <ul>
 * <li> usuwa pismo z obserwowanych DOK, gdy rozpoczyna si� zadanie 'Wznowienie zadania dla DOK'
 * <li> tworzy 'remindera' dla zadanie OPS (w momencie gdy si� rozpoczyna) sprawdzaj�cego codziennie o 9
 * czy wp�yn�y jakie� dokumenty o tym samym nr polisy
 * </ul>
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
@Deprecated
public class HistoryActionHandler implements ActionHandler
{
    String sourceSwimlane;
    String type;   // typ wpisu, ktory ma zostac dodany do historii (gdy == null to brany domyslny JBPM_NORMAL)

    public void execute(ExecutionContext executionContext) throws Exception
    {
        String activityKey = executionContext.getTaskInstance() != null ? String.valueOf(executionContext.getTaskInstance().getId()) : null;

        AssignmentHistoryEntry.EntryType jbpm_type = null;
        String sender = null;
        String currentActor = null;
        String taskName = null;

        Boolean Cancelled = (Boolean) executionContext.getVariable(Constants.VARIABLE_PROCESS_CANCELLED);
        boolean cancelled = Cancelled != null && Cancelled.booleanValue();
        
        String divisionGuid = null; // dzia�, kt�rego dotyczy ten wpis
        if (executionContext.getTaskInstance() != null)
        {
          
        }
        
        if ("init".equals(type))
            jbpm_type = AssignmentHistoryEntry.JBPM_INIT;
        else if ("end-task".equals(type))
            jbpm_type = AssignmentHistoryEntry.JBPM_END_TASK;
        else if ("finish".equals(type))
        {
            if (cancelled)
                jbpm_type = AssignmentHistoryEntry.JBPM_CANCELLED;
            else
                jbpm_type = AssignmentHistoryEntry.JBPM_FINISH ;
        }
        else
            jbpm_type = AssignmentHistoryEntry.JBPM_NORMAL;

        if (!AssignmentHistoryEntry.JBPM_FINISH.equals(jbpm_type) && !AssignmentHistoryEntry.JBPM_CANCELLED.equals(jbpm_type))
        {
            sender = executionContext.getTaskInstance().getPreviousActorId();
            currentActor = executionContext.getTaskInstance().getActorId();

            if (sourceSwimlane != null)
            {
                String swimlaneUser = executionContext.getTaskMgmtInstance()
                                         .getSwimlaneInstance(sourceSwimlane)
                                         .getActorId();
                if (sender == null)
                    sender = swimlaneUser;
            }
        }

              
        Long documentId = (Long) executionContext.getVariable(Constants.VARIABLE_DOCUMENT_ID);
        ProcessDefinition definitionInfo = ProcessDefinition.findByDefinitionId(executionContext.getProcessDefinition().getId());
        if (AssignmentHistoryEntry.JBPM_NORMAL.equals(jbpm_type) || AssignmentHistoryEntry.JBPM_END_TASK.equals(jbpm_type))
        {
            TaskDefinition taskDefinition = TaskDefinition.find(executionContext.getTaskInstance().getName(), definitionInfo.getDefinitionName());
            taskName = (taskDefinition != null ? taskDefinition.getNameForUser() : executionContext.getTaskInstance().getName());
            
            
            // dekretacja na grup� - dodaje wpis do historii, �e dany dzia� otrzyma� pismo
            if (currentActor == null && AssignmentHistoryEntry.JBPM_NORMAL.equals(jbpm_type))
            {
                OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId);
                doc.addAssignmentHistoryEntry(
                        new AssignmentHistoryEntry(AssignmentHistoryEntry.JBPM_NORMAL_DIVISION, sender, currentActor, divisionGuid, taskName, definitionInfo.getNameForUser(), false, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION),
                        false,activityKey
                    );
            }
        }
        
        // dodajemy wpis do historii (INIT, NORMAL lub END_TASK)  
        // nie dodajemy wpisu ze zakonczono zadanie jesli przerwano proces (lub jesli brak aktora)
        if ((!cancelled || !AssignmentHistoryEntry.JBPM_END_TASK.equals(jbpm_type)) && (currentActor != null))
        {
            // je�li jest to wpis do historii na skutek przekazania zadania (np. gdy "realizacja DOK po OPS")
            // typ wpisu do historii jest inny (aby mog�a zosta� wykonan inna akcja na potrzeby historii listy zada�)
            Boolean taskForwarded = (Boolean) executionContext.getTaskInstance().getVariableLocally(Constants.VARIABLE_TASK_FORWARDED);
            if (taskForwarded != null && taskForwarded.booleanValue())
                jbpm_type = AssignmentHistoryEntry.JBPM_FORWARD;
            OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId);
            doc.addAssignmentHistoryEntry(
                new AssignmentHistoryEntry(jbpm_type, sender, currentActor, divisionGuid, taskName, definitionInfo.getNameForUser(), false, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION),
                false,activityKey
            );
        }
        
        // dodajemy wpis ze zakonczono lub przerwano proces
        if (AssignmentHistoryEntry.JBPM_FINISH.equals(jbpm_type) || AssignmentHistoryEntry.JBPM_CANCELLED.equals(jbpm_type))
        {
            OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId);        
            doc.addAssignmentHistoryEntry(
                    new AssignmentHistoryEntry(jbpm_type, null, null, null, null, definitionInfo.getNameForUser(), false, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION),
                    false,activityKey
                );
        }                
        
        // ustalamy lokalna zmienna oznaczajaca "Dekretujacego"
        if (sender != null)
            executionContext.getTaskInstance().setVariableLocally(Constants.VARIABLE_TASK_SENDER, sender);

    }
}
