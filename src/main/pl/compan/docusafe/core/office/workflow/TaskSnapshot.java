package pl.compan.docusafe.core.office.workflow;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.bookmarks.FilterCondition;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
import pl.compan.docusafe.core.office.workflow.jbpm.JbpmWorkflowService;
import pl.compan.docusafe.core.office.workflow.snapshot.RefreshRequestBean;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class TaskSnapshot extends Task implements Cloneable
{
	public static Logger log = LoggerFactory.getLogger(TaskSnapshot.class);

    // flagi (bity) ustawiane na atrybucie processStatus
    public static final long STATUS_ZAWIESZONY = 1;
    public static final long STATUS_ZAWIESZONY_PRZYPOMNIENIE = 2;
    public static final long STATUS_WZNOWIENIE_ZADANIA = 4;
    public static final long STATUS_OTWARTE_W_OPS = 8;
    public static final long STATUS_NOWY_DOKUMENT_Z_NR_POLISY = 16;

    /** Od�wie�anie procesu dla jednego dokumentu - w ka�dym razie tak� mam nadziej� */
    public final static int UPDATE_TYPE_ONE_DOC = 1;
    public final static int UPDATE_TYPE_ONE_USER = 2;
    public final static int UPDATE_TYPE_DOC_LABEL = 3;
    public final static int UPDATE_TYPE_IS_ANY_IMAGE_ONE_DOC = 4;

    protected Long id;
    protected Long dsw_assignment_id;
    protected	String person_firstname;
    protected	String person_lastname;
    protected	String person_organization;
    protected	String person_street;
    protected	String person_location;
    protected	String activity_lastActivityUser;
    protected String assigned_user;
    protected String document_source;
    protected	String dsw_resource_res_key	;
    protected String decretation_to_div;
    protected	String rcpt_firstname;
    protected	String rcpt_lastname;
    protected	String rcpt_organization;
    protected	String rcpt_street;
    protected	String rcpt_location;
    protected String documentAuthor;  // login autora dokumentu/polecenia
    protected String author;          // imie i nazwisko autora
    protected String receiver;        // imie i nazwisko realizujacego zadanie (na podstawie "dsw_resource_res_key")
    protected String decretationTo;   // user do na kt�rego zadanie jest dekretowane
    protected String orderStatus;     // opis statusu polecenia
    protected Date reminderDate;      // "przypomnienie reczne na liscie zadan"
    // status procesu (moze miec np. wplyw na sposob wyswietlania na liscie zadan) - suma flag(bit�w)
    protected Long processStatus;
    protected String documentClerk;   // login referenta
    protected String clerk;           // imie i nazwisko referenta
    protected Date incomingDate;    // data przyjecia pisma w systemie
    protected Integer answerCounter;
    protected Integer dailyOfficeNumber;

    protected Boolean externalWorkflow;
    // ponizsze atrybuty dotycza zadan z workflow jbpm
    protected Long externalProcessId;
    protected Long externalTaskId;
    protected Date rcvDate;        // data otrzymania pisma (bez czasu)
    protected String divisionGuid; // guid dzia�u, do kt�rego zosta�o skierowane to zadanie (pole currentAssignmentGuis z OfficeDocument)
    protected Date documentCtime;  // data przyj�cia pisma (ctime z dokumentu)
    protected Date documentCdate;  // data przyj�cia pisma (ctime z dokumentu), ale bez czasu
   
	protected String lastRemark;
    protected Boolean backupPerson ;
	protected Boolean taskInPackage ; //pismo w paczce
    protected Long dockindId; // id dokindu
    private int sameProcessSnapshotsCount = 1;

    public TaskSnapshot()
    {
    }

//    private static TaskSnapshot ts = null;

    /*to nie jest singleton, ale
      umozliwia dynamiczna zmiane workflow
     */
    public static TaskSnapshot getInstance() {
    	if (WorkflowFactory.jbpm && Jbpm4Provider.getInstance().isJbpm4Supported()) {
    		return new JBPMTaskSnapshot();
    	} else {
    		return new TaskSnapshot();
    	}
    }

    public static TaskSnapshot getAbsInstance() {
    	return new TaskSnapshot();
    }


    public static Log getLog()
    {
    	return LogFactory.getLog("tasklist.snapshot");
    }
    /**
     * @return Zwraca liczbe odpowiedzi udzielonych do pisma
     */
    public Integer getAnswerCounter()
    {
        return answerCounter;
    }

    public void setAnswerCounter(Integer answerCounter)
    {
        this.answerCounter = answerCounter;
    }

    public Date getIncomingDate()
    {
        return incomingDate;
    }

    public void setIncomingDate(Date incomingDate)
    {
        this.incomingDate = incomingDate;
    }

    public String getClerk()
    {
        return clerk;
    }

    public void setClerk(String clerk)
    {
        this.clerk = clerk;
    }    

	public String getDocumentClerk()
    {
        return documentClerk;
    }

    public void setDocumentClerk(String documentClerk)
    {
        this.documentClerk = documentClerk;
    }

    public void setReceiver(String receiver)
    {
        this.receiver = receiver;
    }
    
	public void setDecretationTo(String decretationTo) {
		this.decretationTo = decretationTo;
	}

	
	
	@Override
    public boolean isAccepted()
    {
        return accepted;
    }

	@Override
    public void setAccepted(boolean accepted)
    {
        this.accepted = accepted;
    }

    public void setAccepted(int acc)
    {
//        if(acc==0) this.accepted=false;
//        else this.accepted=true;
        accepted = (acc != 0);// ==0 => false,  !=0 => true
    }

    public void setId(Long i){
        this.id=i;
    }

    public Long getId(){
        return this.id;
    }

    public String getActivity_lastActivityUser() {
        return activity_lastActivityUser;
    }

    public void setActivity_lastActivityUser(String activity_lastActivityUser) {
        this.activity_lastActivityUser = activity_lastActivityUser;
    }

    public String getAssigned_user() {
        return assigned_user;
    }

    public void setAssigned_user(String assigned_user) {
        this.assigned_user = assigned_user;
    }

    public String getDocument_source() {
        return document_source;
    }

    public void setDocument_source(String document_source) {
        this.document_source = document_source;
    }

    public Long getDsw_assignment_id() {
        return dsw_assignment_id;
    }

    public void setDsw_assignment_id(Long dsw_assignment_id) {
        this.dsw_assignment_id = dsw_assignment_id;
    }

    public String getDsw_resource_res_key() {
        return dsw_resource_res_key;
    }

    public void setDsw_resource_res_key(String dsw_resource_res_key) {
        this.dsw_resource_res_key = dsw_resource_res_key;
    }

    public String getPerson_firstname() {
        return person_firstname;
    }

    public void setPerson_firstname(String person_firstname) {
        this.person_firstname = person_firstname;
    }

    public String getPerson_lastname() {
        return person_lastname;
    }

    public void setPerson_lastname(String person_lastname) {
        this.person_lastname = person_lastname;
    }

    public String getPerson_location() {
        return person_location;
    }

    public void setPerson_location(String person_location) {
        this.person_location = person_location;
    }

    public String getPerson_organization() {
        return person_organization;
    }

    public void setPerson_organization(String person_organization) {
        this.person_organization = person_organization;
    }

    public String getPerson_street() {
        return person_street;
    }

    public void setPerson_street(String person_street) {
        this.person_street = person_street;
    }

    public String getRcpt_firstname() {
        return rcpt_firstname;
    }

    public void setRcpt_firstname(String rcpt_firstname) {
        this.rcpt_firstname = rcpt_firstname;
    }

    public String getRcpt_lastname() {
        return rcpt_lastname;
    }

    public void setRcpt_lastname(String rcpt_lastname) {
        this.rcpt_lastname = rcpt_lastname;
    }

    public String getRcpt_location() {
        return rcpt_location;
    }

    public void setRcpt_location(String rcpt_location) {
        this.rcpt_location = rcpt_location;
    }

    public String getRcpt_organization() {
        return rcpt_organization;
    }

    public void setRcpt_organization(String rcpt_organization) {
        this.rcpt_organization = rcpt_organization;
    }

    public String getRcpt_street() {
        return rcpt_street;
    }

    public void setRcpt_street(String rcpt_street) {
        this.rcpt_street = rcpt_street;
    }

    public Boolean isExternalWorkflow()
    {
        return externalWorkflow;
    }

    public void setExternalWorkflow(Boolean externalWorkflow)
    {
        this.externalWorkflow = externalWorkflow;
    }

    public Long getExternalProcessId()
    {
        return externalProcessId;
    }

    public void setExternalProcessId(Long externalProcessId)
    {
        this.externalProcessId = externalProcessId;
    }

    public Long getExternalTaskId()
    {
        return externalTaskId;
    }

    public void setExternalTaskId(Long externalTaskId)
    {
        this.externalTaskId = externalTaskId;
    }

    public String getDocumentAuthor()
    {
        return documentAuthor;
    }

    public void setDocumentAuthor(String documentAuthor)
    {
        this.documentAuthor = documentAuthor;
    }

    public String getAuthor()
    {
        return author;
    }

    public String getReceiver()
    {
        return receiver;
    }

    public String getDecretationTo() {
		return decretationTo;
	}
    
    public String getOrderStatus()
    {
        return orderStatus;
    }

    public Date getReminderDate()
    {
        return reminderDate;
    }

    public void setReminderDate(Date reminderDate)
    {
        this.reminderDate = reminderDate;
    }

    public Long getProcessStatus()
    {
        return processStatus;
    }

    public void setProcessStatus(Long processStatus)
    {
        this.processStatus = processStatus;
    }

    public Date getRcvDate()
    {
        return rcvDate;
    }

    public void setRcvDate(Date rcvDate)
    {
        this.rcvDate = rcvDate;
    }

    public String getDivisionGuid()
    {
        	return divisionGuid;    		
    }

	//maksymalna dlugosc w tabeli DSW_JBPM_TASKLIST.DIVISION_GUID = 62
    public void setDivisionGuid(String divisionGuid)
    {
      	this.divisionGuid = divisionGuid;
    }

    public Date getDocumentCdate()
    {
        return documentCdate;
    }

    public Date getDocumentCtime()
    {
        return documentCtime;
    }

    /**
     * Tworzy kopi� tego TaskSnapshota (nie kopiuje id)
     * @return
     */
    public TaskSnapshot clone() {
        TaskSnapshot ret = (TaskSnapshot) super.clone();
        ret.activity_lastActivityUser = this.activity_lastActivityUser;
        ret.answerCounter = this.answerCounter;
        ret.assigned_user = this.assigned_user;
        ret.author = this.author;
        ret.backupPerson = this.backupPerson;
        ret.clerk = this.clerk;
        ret.dailyOfficeNumber = this.dailyOfficeNumber;
        ret.decretationTo = this.decretationTo;
        ret.divisionGuid = this.divisionGuid;
        ret.document_source = this.document_source;
        ret.documentAuthor = this.documentAuthor;
        ret.documentCdate = this.documentCdate;
        ret.documentClerk = this.documentClerk;
        ret.documentCtime = this.documentCtime;
        ret.dsw_assignment_id = this.dsw_assignment_id;
        ret.dsw_resource_res_key = this.dsw_resource_res_key;
        ret.externalProcessId = this.externalProcessId;
        ret.externalTaskId = this.externalTaskId;
        ret.externalWorkflow = this.externalWorkflow;
        ret.incomingDate = this.incomingDate;
        ret.lastRemark = this.lastRemark;
        ret.orderStatus = this.orderStatus;
        ret.packageId = this.packageId;
        ret.person_firstname = this.person_firstname;
        ret.person_lastname = this.person_lastname;
        ret.person_location = this.person_location;
        ret.person_organization = this.person_organization;
        ret.person_street = this.person_street;
        ret.processStatus = this.processStatus;
        ret.receiver = this.receiver;
        ret.reminderDate = this.reminderDate;
        ret.rcpt_firstname = this.rcpt_firstname;
        ret.rcpt_lastname = this.rcpt_lastname;
        ret.rcpt_location = this.rcpt_location;
        ret.rcpt_organization = this.rcpt_organization;
        ret.rcpt_street = this.rcpt_street;
        ret.shortReceiveDate = this.shortReceiveDate;
        ret.workflowName = this.workflowName;
        return ret;
    }

    /**
     * Ustawia dat� przyj�cia pisma - zar�wno w pe�nej postaci (documentCtime) jak i niepe�nej bez czasu (documentCdate).
     */
    public void setDocumentCtimeAndCdate(Date date)
    {
        this.documentCtime = date;
        Calendar calTime = Calendar.getInstance();
        calTime.setTime(date);

        Calendar calDate = Calendar.getInstance();
        calDate.clear();
        calDate.set(calTime.get(Calendar.YEAR), calTime.get(Calendar.MONTH), calTime.get(Calendar.DAY_OF_MONTH));
        this.documentCdate = calDate.getTime();
    }

    /**
     * Ustawia dat� otrzymania pisma - zar�wno w pe�nej postaci (receiveDate) jak i niepe�nej bez czasu (rcvDate).
     */
    public void setRcvAndReceiveDate(Date date)
    {
        this.receiveDate = date;
        if (date == null)
        {
        	rcvDate = null;
        	log.trace("rcvDate==null w activityId={}",getActivityId());
        	return;
        }
        Calendar calTime = Calendar.getInstance();
        calTime.setTime(date);

        Calendar calDate = Calendar.getInstance();
        calDate.clear();
        calDate.set(calTime.get(Calendar.YEAR), calTime.get(Calendar.MONTH), calTime.get(Calendar.DAY_OF_MONTH));
        this.rcvDate = calDate.getTime();
    }

    public SearchResults<TaskSnapshot> search(Query query) throws EdmException
    {
	    log.trace("!!!! TaskSnapshot.query: {}", query.toString());
        FromClause from = new FromClause();
        ResultSet rs = null;
        final TableAlias tasklist = from.createTable("dsw_tasklist");
    //    final TableAlias taskWatches = from.createTable("ds_watchlist");
        final WhereClause where = new WhereClause();
        TableAlias asgnHistory = null;

        if (query.exTasks)
        {
            asgnHistory = from.createTable("dso_document_asgn_history");
            where.add(Expression.eqAttribute(asgnHistory.attribute("document_id"), tasklist.attribute("dso_document_id")));

            Junction OR = Expression.disjunction();
            OR.add(Expression.eq(asgnHistory.attribute("sourceUser"), query.exTasksUser));
            //OR.add(Expression.eq(asgnHistory.attribute("targetUser"), query.exTasksUser));

            where.add(OR);

            where.add(Expression.ne(tasklist.attribute("dsw_resource_res_key"), query.exTasksUser));
        }
        /** Dodaje linki do etykietek obowiazkowych */
        if (query.getLabelsFiltering()!=null && query.getLabelsFiltering().size()>0)
        {
        	TableAlias documentToLabel = from.createTable("dso_document_to_label");
    	    where.add(Expression.eqAttribute(documentToLabel.attribute("document_id"), tasklist.attribute("dso_document_id")));

    		if("or".equals(Docusafe.getAdditionProperty("tasklist.label.operator")))
    		{
    			Junction OR = Expression.disjunction();
    			java.util.Iterator<Label> it =query.getLabelsFiltering().iterator();
 	        	while (it.hasNext())
 	        	{
 	        		Label label = (Label) it.next();
 	        		OR.add(Expression.eq(documentToLabel.attribute("label_id"), label.getId().toString()));
 	        	}
    			where.add(OR);
    		}
    		else
    		{
	        	java.util.Iterator<Label> it =query.getLabelsFiltering().iterator();
	        	while (it.hasNext())
	        	{
	        		Label label = (Label) it.next();
	        		where.add(Expression.eq(documentToLabel.attribute("label_id"), label.getId().toString()));
	        	}
    		}
        }
        else if (AvailabilityManager.isAvailable("labels") && !query.isLabelsAll())
        {
    		if("true".equals(Docusafe.getAdditionProperty("tasklist.show.disapproved")))
			{
    			Junction OR = Expression.disjunction();
    			OR.add(Expression.eq(tasklist.attribute("LABELHIDDEN"), false));
    			OR.add(Expression.eq(tasklist.attribute("dsw_assignment_accepted"), false));
	        	where.add(OR);
			}
    		else
    		{
    			where.add(Expression.eq(tasklist.attribute("LABELHIDDEN"), false));
    		}
        }
        if(query.user!=null)
        {
     	   log.trace("!!!! TaskSnapshot -  query.user!=null {}", query.user.getName());
            if (query.substitutions || query.BOK)
            {
                DSUser[] substs = query.user.getSubstitutedUsers();
                int length = substs.length+1+(query.BOK ? 1 : 0);
                String[] users = new String[length];
                if (query.substitutions)
                {
                    users[0]=query.user.getName();
                    for (int i=0; i < substs.length; i++)
                    {
                        users[i+1] = substs[i].getName();
                    }
                }
                if (query.BOK)
                    users[substs.length + 1] = "$bok";
                log.trace("!!!! TaskSnapshot -  users {}", users.toString());
                where.add(Expression.in(tasklist.attribute("dsw_resource_res_key"), users));
            }
            else where.add(Expression.eq(tasklist.attribute("dsw_resource_res_key"), query.user.getName()));
        }

        boolean odbiorNaDzial = query.user == null ? false : DSApi.context().hasPermission(query.user, DSPermission.WWF_ODBIOR_DEKRETACJI_NA_DZIAL);
        if (query.user != null && !odbiorNaDzial)
        {
            Junction OR = Expression.disjunction();
            OR.add(Expression.ne(tasklist.attribute("assigned_user"), null));
            OR.add(Expression.eq(tasklist.attribute("external_workflow"), Boolean.TRUE));
            where.add(OR);
        }

        if (query.documentType != null)
            where.add(Expression.eq(tasklist.attribute("document_type"), query.documentType));
        if (query.assignmentId != null)
            where.add(Expression.eq(tasklist.attribute("dsw_assignment_id"), query.assignmentId));
        if (query.division != null)
        {
        	DSUser[] dsUser = query.division.getUsers();
        	if(dsUser != null && dsUser.length > 0)
        	{
        		String[] usernames = new String[dsUser.length];
        		for (int i = 0; i < dsUser.length; i++)
        		{
        			usernames[i] = dsUser[i].getName();
				}
        		where.add( Expression.in(tasklist.attribute("dsw_resource_res_key"), usernames));
        	}
        }
        if (query.division != null)
            where.add(Expression.eq(tasklist.attribute("DIVISIONGUID"), query.division.getGuid()));
        if (query.officeNumber != null)
            where.add(Expression.eq(tasklist.attribute("dso_document_officenumber"), query.officeNumber.toString()));
        if (query.processDefinitionId != null)
            where.add(Expression.eq(tasklist.attribute("dsw_process_processdef"), query.processDefinitionId));
        if (query.documentAuthor != null)
            where.add(Expression.eq(tasklist.attribute("documentAuthor"), query.documentAuthor));
        if (query.documentId != null)
            where.add(Expression.eq(tasklist.attribute("dso_document_id"), query.documentId));
        else if (query.documentIds != null && query.documentIds.length > 0)
            where.add(Expression.in(tasklist.attribute("dso_document_id"), query.documentIds));
        if (query.externalWorkflow != null)
            where.add(Expression.eq(tasklist.attribute("external_workflow"), query.externalWorkflow));

        if(query.withBackup!=null && query.withBackup.equals(Boolean.TRUE)){
        	 //Junction OR = Expression.disjunction();

             //OR.add(Expression.ne(tasklist.attribute("backupPerson"), true));
             //OR.add(Expression.eq(tasklist.attribute("backupPerson"), null));
             //where.add(OR);
        	//Junction AND = Expression.conjunction();
        	 where.add(Expression.eq(tasklist.attribute("backupPerson"), true));
        } else {
        	 Junction OR = Expression.disjunction();

             OR.add(Expression.ne(tasklist.attribute("backupPerson"), true));
             OR.add(Expression.eq(tasklist.attribute("backupPerson"), null));
             where.add(OR);
        }

        // FRAGMENT ODPOWIEDZIALNY ZA ODPOWIEDNIE FILTROWANIE
        
        //FIXME A jakby kto� tak napisa� o co dok�adnie poni�ej chodzi to by mu r�ka odpad�a?
        if("task_count_1".equals(query.filterBy)) 
        {
            where.add(Expression.eq(tasklist.attribute("resource_count"),1));
        } 
        else if("task_count_2_or_more".equals(query.filterBy))
        {
            where.add(Expression.ge(tasklist.attribute("resource_count"),2));
        }
        else if (query.filterBy != null && query.filterValue != null)
        {    
        	if(query.filterValue.getClass().equals(java.util.Date.class) )
        	{
        		Timestamp ts = new Timestamp( ((Date)query.filterValue).getTime());
        		where.add(Expression.ge(tasklist.attribute(query.filterBy),ts));
        		Timestamp ts2 = new Timestamp( ((Date)query.filterValue).getTime());
        		ts2.setHours(24);
        		where.add(Expression.le(tasklist.attribute(query.filterBy),ts2));
        	}
        	else
        	{
        		log.info("TaskSnapshot - query.filterBy: " + query.filterBy);
        		
        		if(AvailabilityManager.isAvailable("tasklist.filtruj.contaisAnywhere"))
				{
        			where.add(Expression.like(tasklist.attribute(query.filterBy),"%"+query.filterValue+"%",true));
				}
        		else if (AvailabilityManager.isAvailable("tasklist.filtruj.startWithe"))
        		{
        			where.add(Expression.like(tasklist.attribute(query.filterBy),query.filterValue+"%",true));
        		}
        		else
        		{
        			where.add(Expression.eq(tasklist.attribute(query.filterBy),query.filterValue));
        		}
        	}
        }
     //   Junction filterOR = Expression.disjunction();
        if (query.filterConditions != null) {
        	for(FilterCondition fc : query.filterConditions) {
        		//filterOR.add(Expression.in(tasklist.attribute(fc.getColumnTableName()), fc.getParsedValues().toArray() ));
        		where.add(Expression.in(tasklist.attribute(fc.getColumnTableName()), fc.getParsedValues().toArray() ));
        	}
        	//where.add(filterOR);
        }
        
        
   //     if(query.isWatches())
        {
    //    	where.add(Expression.eq(attribute, value));
        }
        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = null;
        GroupClause group = new GroupClause();
        if (query.isWatches())
        {
        	selectId.addSql(tasklist.getAlias()+".document_id");
        	idCol = selectId.addSql("min("+tasklist.getAlias()+".id)");
        	group.add(tasklist.attribute("document_id"));
        }
        else if(AvailabilityManager.isAvailable("tasklist.substitutions.filtering"))
        {
        	selectId.addSql(tasklist.getAlias()+".dsw_activity_activity_key");
        	idCol = selectId.addSql("min("+tasklist.getAlias()+".id)");
        	group.add(tasklist.attribute("dsw_activity_activity_key"));
        }
        else
        {
        	idCol = selectId.add(tasklist, "id");
        	group.add(tasklist.attribute("id"));
        }

        OrderClause order = new OrderClause();

        if ("receiveDate".equals(query.sortField)){
        	group.add(tasklist.attribute("dsw_activity_laststatetime"));
            order.add(tasklist.attribute("dsw_activity_laststatetime"), query.ascending);
            selectId.add(tasklist, "dsw_activity_laststatetime");
        }
        else if ("description".equals(query.sortField)){
        	group.add(tasklist.attribute("dsw_activity_name"));
            order.add(tasklist.attribute("dsw_activity_name"),query.ascending);
            selectId.add(tasklist, "dsw_activity_name");
        }
        else if ("process".equals(query.sortField)){
        	group.add(tasklist.attribute("dsw_process_name"));
            order.add(tasklist.attribute("dsw_process_name"), query.ascending);
            selectId.add(tasklist, "dsw_process_name");
        }
        else if ("documentSummary".equals(query.sortField)){
        	group.add(tasklist.attribute("dso_document_summary"));
            order.add(tasklist.attribute("dso_document_summary"), query.ascending);
            selectId.add(tasklist, "dso_document_summary");
        }
        else if ("documentReferenceId".equals(query.sortField)){
        	group.add(tasklist.attribute("dso_document_referenceid"));
            order.add(tasklist.attribute("dso_document_referenceid"),query.ascending);
            selectId.add(tasklist, "dso_document_referenceid");
        }
        else if ("documentSender".equals(query.sortField)){
        	group.add(tasklist.attribute("dso_person_firstname"));
            order.add(tasklist.attribute("dso_person_firstname"), query.ascending);
            selectId.add(tasklist, "dso_person_firstname");
        }
        else if ("sender".equals(query.sortField)){
        	group.add(tasklist.attribute("dsw_process_context_param_val"));
            order.add(tasklist.attribute("dsw_process_context_param_val"), query.ascending);
            selectId.add(tasklist, "dsw_process_context_param_val");
        }
        else if ("officeCase".equals(query.sortField)){
        	group.add(tasklist.attribute("case_office_id"));
            order.add(tasklist.attribute("case_office_id"), query.ascending);
            selectId.add(tasklist, "case_office_id");
        }
        else if ("objective".equals(query.sortField)){
        	group.add(tasklist.attribute("dsw_activity_context_param_val"));
            order.add(tasklist.attribute("dsw_activity_context_param_val"), query.ascending);
            selectId.add(tasklist, "dsw_activity_context_param_val");
        }
        else if ("documentDelivery".equals(query.sortField)){
        	group.add(tasklist.attribute("documentDelivery"));
            order.add(tasklist.attribute("documentDelivery"), query.ascending);
            selectId.add(tasklist, "documentDelivery");
        }
        else if ("documentPrepState".equals(query.sortField)){
        	group.add(tasklist.attribute("documentPrepState"));
            order.add(tasklist.attribute("documentPrepState"), query.ascending);
            selectId.add(tasklist, "documentPrepState");
        }
        else if ("answerCounter".equals(query.sortField)){
        	group.add(tasklist.attribute("answerCounter"));
        	order.add(tasklist.attribute("answerCounter"), query.ascending);
            selectId.add(tasklist, "answerCounter");
        }else if("lastRemark".equals(query.sortField)){
        	group.add(tasklist.attribute("lastRemark"));
        	order.add(tasklist.attribute("lastRemark"), !query.ascending);
        	selectId.add(tasklist,"lastRemark");
        	
	    }else if("dockindBusinessAtr6".equals(query.sortField)){
	    	group.add(tasklist.attribute("dockind_business_atr_6"));
	    	order.add(tasklist.attribute("dockind_business_atr_6"), !query.ascending);
	    	selectId.add(tasklist,"dockind_business_atr_6");
		}
	    else if("dockindBusinessAtr5".equals(query.sortField)){
	    	group.add(tasklist.attribute("dockind_business_atr_5"));
	    	order.add(tasklist.attribute("dockind_business_atr_5"), !query.ascending);
	    	selectId.add(tasklist,"dockind_business_atr_5");
		}
	    else if("dockindBusinessAtr4".equals(query.sortField)){
	    	group.add(tasklist.attribute("dockind_business_atr_4"));
	    	order.add(tasklist.attribute("dockind_business_atr_4"), !query.ascending);
	    	selectId.add(tasklist,"dockind_business_atr_4");
		}
        else if(query.sortField != null && ( query.sortField.equals("dockindKategoria") ||
        		query.sortField.equals("dockindKwota") ||
        		query.sortField.equals("dockindName") ||
        		query.sortField.equals("dockindNumerDokumentu") ||
        		query.sortField.equals("dockindStatus") ||
        		query.sortField.equals("dockindBusinessAtr3") ||
        		query.sortField.equals("dockindBusinessAtr2") ||
        		query.sortField.equals("dockindBusinessAtr1") ||
        		query.sortField.equals("documentDate") ||
        		query.sortField.equals("dockindKategoria")))
        {
        	group.add(tasklist.attribute(query.sortField));
        	order.add(tasklist.attribute(query.sortField), query.ascending);
        	selectId.add(tasklist,query.sortField);
        }
        else
        {
        	group.add(tasklist.attribute("dso_document_officenumber"));
            order.add(tasklist.attribute("dso_document_officenumber"), query.ascending);
            selectId.add(tasklist, "dso_document_officenumber");
        }

        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = null;
        if(AvailabilityManager.isAvailable("tasklist.substitutions.filtering"))
        {
        	countCol = selectCount.addSql("count(distinct "+tasklist.getAlias()+".dsw_activity_activity_key)");
        }
        else
        {
        	countCol = selectCount.addSql("count(distinct "+tasklist.getAlias()+".id)");
        }

        int totalCount;
        SelectQuery selectQuery;

        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            rs = selectQuery.resultSet();
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby zada�.");
            totalCount = rs.getInt(countCol.getPosition());
            //rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            if(AvailabilityManager.isAvailable("tasklist.substitutions.filtering"))
            {
            	selectQuery = new SelectQuery(selectId, from, where, order,group);
            }
            else
            {
            	selectQuery = new SelectQuery(selectId, from, where, order);
            }
            if (query.getLimit() > 0)
            {
            	selectQuery.setMaxResults(query.getLimit());
                if(totalCount < query.getOffset())
                {
                	selectQuery.setFirstResult(0);
                }
                else
                {
                	selectQuery.setFirstResult(query.getOffset());
                }
            }
            //ps = selectQuery.createStatement(DSApi.context().session().connection());
            rs = selectQuery.resultSet();
            List<TaskSnapshot> results = new ArrayList<TaskSnapshot>(query.getLimit());
            TaskSnapshot temp;
         //   boolean odbiorNaDzial = query.user == null ? false : DSApi.context().hasPermission(query.user, DSPermission.WWF_ODBIOR_DEKRETACJI_NA_DZIAL);

            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                temp = DSApi.context().load(TaskSnapshot.class, id);
           /*     if (query.user != null && !odbiorNaDzial && temp.assigned_user == null && !temp.externalWorkflow)
                {
                    continue;
                }*/
                if (DSApi.getObject(Document.class, id)!=null && !DSApi.getObject(Document.class, id).isDeleted())
                results.add(temp);
            }
        //    rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            return new SearchResultsAdapter<TaskSnapshot>(results, query.getOffset(), totalCount,
                    TaskSnapshot.class);
        }
        catch (SQLException e)
        {
        	LogFactory.getLog("eprint").error("", e);
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
        	LogFactory.getLog("eprint").error("", e);
            throw new EdmHibernateException(e);
        }
    }

    public SearchResultsAdapter<TaskSnapshot> searchForWatches(TaskSnapshot.Query query) throws EdmException{
    	List<TaskSnapshot> results = new ArrayList<TaskSnapshot>();
    	int totalCount = 0;
    	return new SearchResultsAdapter<TaskSnapshot>(
    			results, query.getOffset(), totalCount,
                TaskSnapshot.class
    			);    	
    }
    
    /** liczba wszystkich snaphot�w dotycz�cych tego dokumentu */
    public int getSameProcessSnapshotsCount() {
        return sameProcessSnapshotsCount;
    }

    public void setSameProcessSnapshotsCount(int nSameDocumentSnaphots) {
        this.sameProcessSnapshotsCount = nSameDocumentSnaphots;
    }

    public static class Query extends FormQuery
    {
        public DSUser user;
        public DSDivision division;
        public String documentType;
        public boolean substitutions = false;
        public boolean BOK = false;
        public Integer assignmentId;
        protected String sortField;
        protected boolean ascending;
        protected Integer officeNumber;
        protected String processDefinitionId;
        protected String documentAuthor;
        protected boolean exTasks;
        protected String exTasksUser;
        protected Boolean externalWorkflow;
        protected Long documentId;
        protected Long[] documentIds;
        protected String filterBy;
        protected Object filterValue;
        protected Boolean withBackup;
        protected Collection<Label> labelsFiltering = null;
        protected boolean labelsAll = false;
		protected Long  dockindId;
		protected boolean watches = false;
		protected Boolean taskInPackage;
        protected List<FilterCondition> filterConditions;
        protected String dockindName;

		public boolean isWatches() {
			return watches;
		}

		public void setWatches(boolean watches) {
			this.watches = watches;
		}

		public String getFilterBy() {
			return filterBy;
		}

		public void setFilterBy(String filterBy) {
			this.filterBy = filterBy;
		}

		public Object getFilterValue() {
			return filterValue;
		}

		public void setFilterValue(Object filterValue) {
			this.filterValue = filterValue;
		}

		public Query()
        {
        }

        public Query(int offset, int limit)
        {
            super(offset, limit);
        }

        public void setOfficeNumber(Integer i)
        {
            this.officeNumber=i;
        }

        public Integer getOfficeNumber()
        {
            return this.officeNumber;
        }

        public void setSubstitutions(boolean subst)
        {
            substitutions = subst;
        }

        public void setBOK(boolean BOK)
        {
            this.BOK = BOK;
        }

        public void setDocumentType(String type)
        {
            documentType = type;
        }

        public void setUser(DSUser username)
        {
            user = username;
        }
        
		public void setDivision(DSDivision divisionS) {
			this.division = divisionS;
		}

        public void setAssignmentId(Integer id)
        {
            assignmentId=id;
        }

        public void setProcessDefinitionId(String processDefinitionId)
        {
            this.processDefinitionId = processDefinitionId;
        }

        public String getProcessDefinitionId()
        {
            return processDefinitionId;
        }

        public String getDocumentAuthor()
        {
            return documentAuthor;
        }

        public void setDocumentAuthor(String documentAuthor)
        {
            this.documentAuthor = documentAuthor;
        }

        public void setExTasks(boolean exTasks)
        {
            this.exTasks = exTasks;
        }

        public void setExTasksUser(String exTasksUser)
        {
            this.exTasksUser = exTasksUser;
        }

        public void setSortField(String sort)
        {
            sortField=sort;
        }

        public void setAscending(boolean asc)
        {
            ascending=asc;
        }

        public void setExternalWorkflow(Boolean externalWorkflow)
        {
            this.externalWorkflow = externalWorkflow;
        }

        public void setDocumentId(Long documentId)
        {
            this.documentId = documentId;
        }
        
        public Long[] getDocumentIds()
        {
        	return documentIds;
        }

        public void setDocumentIds(Long[] documentIds)
        {
            this.documentIds = documentIds;
        }

		public Boolean getWithBackup() {
			return withBackup;
		}

		public void setWithBackup(Boolean withBackup) {
			this.withBackup = withBackup;
		}

		public Collection<Label> getLabelsFiltering() {
			return labelsFiltering;
		}

		public void setLabelsFiltering(Collection<Label> labelsFiltering) {
			this.labelsFiltering = labelsFiltering;
		}

		public void setLabelsAll(boolean labelsAll) {
			this.labelsAll = labelsAll;
		}

		public boolean isLabelsAll() {
			return labelsAll;
		}

		public List<FilterCondition> getFilterConditions() {
			return filterConditions;
		}

		public void setFilterConditions(List<FilterCondition> filterConditions) {
			this.filterConditions = filterConditions;
		}
		
		public Boolean isTaskInPackage()
		{
			return taskInPackage;
		}

		
		public void setInPackageTask(Boolean taskInPackage)
		{
			this.taskInPackage = taskInPackage;
		}
		 
		public String getDockindName()
		{
			return dockindName;
		}

		
		public void setDockindName(String dockindName)
		{
			this.dockindName = dockindName;
		}
	       
			public Long getDockindId()
			{
				return dockindId;
			}

			
			public void setDockindId(Long dockindId)
			{
				this.dockindId = dockindId;
			}

			
			public Boolean getTaskInPackage()
			{
				return taskInPackage;
			}

			
			public void setTaskInPackage(Boolean taskInPackage)
			{
				this.taskInPackage = taskInPackage;
			}

		
    }

    /**
     * Uzupelnianie w zadaniu p�l, kt�re nie s� bezpo�rednio mapowane z bazy, a s� potrzebne.
     *
     * @param ifFlags
     * @return
     * @throws EdmException
     */
    public Task toFullTask(boolean ifFlags) throws EdmException
    {
    	if(ifFlags)
    	{
	    	this.flags = LabelsManager.getReadFlagBeansForDocument(documentId, DSApi.context().getPrincipalName(), false);
	    	this.userFlags = LabelsManager.getReadFlagBeansForDocument(documentId, DSApi.context().getPrincipalName(), true);
    	}
        this.documentTask=true;
        if (this.externalWorkflow)
        {
            this.setWorkflowName(JbpmWorkflowService.NAME);
        }
        else
        {
            this.setWorkflowName(InternalWorkflowService.NAME);

            this.receiver = DSUser.safeToFirstnameLastname(this.dsw_resource_res_key);
            //if(this.deadlineTime.before(new Date())) this.pastDeadline = true;
        }
        this.setActivityId(ActivityId.make(this.getWorkflowName(), this.getActivityKey()));
        if (this.activity_lastActivityUser != null)
        {
            if (AvailabilityManager.isAvailable("tasklist.senderAsLastnameFirstname"))
            {
                DSUser.safeToLastnameFirstname(this.activity_lastActivityUser);
            }
            else
                this.setSender(DSUser.safeToFirstnameLastname(this.activity_lastActivityUser));
        }
        if (this.documentAuthor != null)
        {
            this.author = DSUser.safeToFirstnameLastname(this.documentAuthor);
        }

        this.setManual(WorkflowUtils.iwfPackageName().equals(this.getPackageId()) &&
                       WorkflowUtils.iwfManualName().equals(this.getProcessDefinitionId()));
        this.setDocumentSender(Person.getSummary(false, null,
                person_firstname,
                person_lastname,
                person_organization,
                person_street,
                person_location));
        this.setDocumentRecipient(Person.getSummary(false, null,
                rcpt_firstname,
                rcpt_lastname,
                rcpt_organization,
                rcpt_street,
                rcpt_location));
        this.setDocumentBok(Boolean.valueOf("$bok".equals(this.dsw_resource_res_key)));
        this.setDocumentFax("fax".equals(this.document_source));
        this.setDocumentCrm("crm".equals(this.document_source));

        this.setShortReceiveDate(this.getReceiveDate());

        if ("order".equals(this.documentType))
        {
            OfficeOrder order = OfficeOrder.findOfficeOrder(this.documentId);
            if (order.getStatus() != null)
                this.orderStatus = OfficeOrder.getStatusMap().get(order.getStatus());
        }
        /*if(ifFlags)
        {
            this.updateFlags();
            this.updateUserFlags();
        }*/

        // Ustawienie referenta pisma
        if (documentClerk != null)
        {
            clerk = DSUser.findByUsername(documentClerk).asLastnameFirstname();
        }
        if (InOfficeDocument.TYPE.equals(getDocumentType()))
        {
        	if (answerCounter != null && answerCounter.equals(0))
        		answerCounter = null;
        }
        /*if(this.getSameProcessSnapshotsCount()==1)
        {
        	decretationTo = DSUser.findByUsername(dsw_resource_res_key).asLastnameFirstname(); // lub dsw_reso   assignef
        }
        else
        {
        	decretationTo = DSDivision.find(divisionGuid).getName();
        }*/
        
       /* NIE mozna tak robic na liscie zadan trzeba to przeniesc do dsw_task_list
        if (InOfficeDocument.TYPE.equals(getDocumentType()))
        {
            // Jak liczba odpowiedzi na pismo rowna 0 to nic nie pokazujemy
            if (answerCounter != null && answerCounter.equals(0))
                answerCounter = null;
            InOfficeDocument indoc = (InOfficeDocument)InOfficeDocument.find(this.documentId);
            this.documentReferenceId = indoc.getCaseDocumentId();
            this.setReplyUnnecessary(indoc.isReplyUnnecessary());
        }
        else
        {
        	this.documentReferenceId = OfficeDocument.findOfficeDocument(this.documentId).getCaseDocumentId();
        }
         */
        return (Task)this;

    }
    
    public TaskSnapshot findByActivityKey(String act) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(TaskSnapshot.class);
        try
        {
            c.add(org.hibernate.criterion.Restrictions.eq("activityKey",act));

            List list = c.list();

            if (list.size() > 0)
                return (TaskSnapshot) list.get(0);
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static TaskSnapshot findExternalTaskById(Long taskId) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(TaskSnapshot.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.eq("externalTaskId", taskId));
            c.add(org.hibernate.criterion.Expression.eq("externalWorkflow", Boolean.TRUE));

            List list = c.list();

            if (list.size() > 0)
                return (TaskSnapshot) list.get(0);
            else
                return null;

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Uaktualnia zadania (w obiegu recznym) zwiazane z dokumentem o podanym id.
     * Poda� paparametr doctype r�wny "anyType" je�li szukamy wszystkich typ�w dokument�w
     *
     */
    public static void updateByDocumentId(Long id, String doctype) throws EdmException
    {
    	updateTaskList(id,doctype, UPDATE_TYPE_ONE_DOC,null);
    }

	public static void updateByDocument(Document doc) throws EdmException
    {
    	updateTaskList(doc.getId(),doc.getType().getName(), UPDATE_TYPE_ONE_DOC,null);
    }

    /**
     * Uaktualnia zadania (w obiegu recznym) zwiazane z dokumentem o podanym id.
     * Poda� paparametr doctype r�wny "anyType" je�li szukamy wszystkich typ�w dokument�w
     *
     */
    public static void updateLabelByDocumentId(Long id, String doctype) throws EdmException
    {
    	updateTaskList(id,doctype, UPDATE_TYPE_DOC_LABEL, null);
    }

    /**
     * Uaktualnia zadania (w obiegu recznym) zwiazane z dokumentem o podanym id.
     * Poda� paparametr doctype r�wny "anyType" je�li szukamy wszystkich typ�w dokument�w
     *
     */
    public static void updateIsAnyImageByDocumentId(Long id, String doctype) throws EdmException
    {
    	updateTaskList(id,doctype,UPDATE_TYPE_IS_ANY_IMAGE_ONE_DOC,null);
    }

    /**
     * Uaktualnia zadania (w obiegu recznym)
     * updateType - okresla co ma byc uaktualnione jesli = 1 to dla dokument
     * jesli = 2 to dla wszystkich dokumentow, jesli username != null to wszystkie dokumnety dla danego uzytkownika
     * UWAGA!!! Obecnie metoda ta jedynie zapisuje zadanie do kolejki. Aktualizacja wykonywana jest przy commicie kontekstu
     * w osobnych transakcjach.
     * @throws EdmException
     */
    public static void updateTaskList(Long id, String doctype, int updateType,String username) throws EdmException
    {
    	RefreshRequestBean bean = new RefreshRequestBean();
    	bean.setId(id);
    	bean.setDoctype(doctype);
    	bean.setUpdateType(updateType);
    	bean.setUsername(username);
    	DSApi.context().registerTasklistRefresh(bean);
    }

	/**
     * Uaktualnia wszystkie zadania zwiazane z dokumentem o podanym id
     * Poda� paparametr doctype r�wny "anyType" je�li szukamy wszystkich typ�w dokument�w
     */
    public static void updateAllTasksByDocumentId(Long documentId, String doctype) throws EdmException
    {
        updateByDocumentId(documentId, doctype);
    }

	public String getLastRemark() {
		return lastRemark;
	}

	public void setLastRemark(String lastRemark) {
		this.lastRemark = lastRemark;
	}

	public Boolean getBackupPerson() {
		return backupPerson;
	}

	public void setBackupPerson(Boolean backupPerson) {
		this.backupPerson = backupPerson;
	}

	public Integer getDailyOfficeNumber() {
		return dailyOfficeNumber;
	}

	public void setDailyOfficeNumber(Integer dailyOfficeNumber) {
		this.dailyOfficeNumber = dailyOfficeNumber;
	}

	public String getDecretation_to_div() {
		return decretation_to_div;
	}

	public void setDecretation_to_div(String decretation_to_div) {
		this.decretation_to_div = decretation_to_div;
	}

	public Boolean getTaskInPackage()
	{
		return taskInPackage;
	}

	
	public void setTaskInPackage(Boolean taskInPackage)
	{
		this.taskInPackage = taskInPackage;
	}
	public Long getDockindId()
	{
		return dockindId;
	}

	
	public void setDockindId(Long dockindId)
	{
		this.dockindId = dockindId;
	}



}
