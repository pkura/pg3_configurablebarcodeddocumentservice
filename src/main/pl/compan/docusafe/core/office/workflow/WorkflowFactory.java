package pl.compan.docusafe.core.office.workflow;
import static pl.compan.docusafe.core.NewAvailabilityManager.isAvailable;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.*;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.apache.axis.utils.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.FinishOrAssignState;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowManager;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
import pl.compan.docusafe.core.office.workflow.internal.WfProcessImpl;
import pl.compan.docusafe.core.office.workflow.snapshot.Jbpm4SnapshotProvider;
import pl.compan.docusafe.core.office.workflow.xpdl.ExtendedAttribute;
import pl.compan.docusafe.core.office.workflow.xpdl.Participant;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.process.ProcessEngineType;
import pl.compan.docusafe.process.WorkflowEngine;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * Klasa odpowiedzialna za obsluge procesow workflow Zakladamy ze w kodzie
 * programu wszystkie operacje zwiazane z dekretacja przechodza przez ta klase.
 *
 * @author Jakub �wiat�y
 *
 */
public abstract class WorkflowFactory
{

    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(InternalWorkflowManager.class.getPackage().getName(), null);
    private static StringManager smG = GlobalPreferences.loadPropertiesFile("", null);
    private static final Logger log = LoggerFactory.getLogger(WorkflowFactory.class);
    public static boolean jbpm = AvailabilityManager.isAvailable("tasklist.jbpm4");
    private static final WorkflowFactory INSTANCE;

    static
    {
        //dependency injection dla ubogich
        if (isAvailable("tasklist.multipleProviders")) {
            Map<String, WorkflowFactory> map = Maps.newHashMapWithExpectedSize(2);
            if(WorkflowFactory.jbpm){
                map.put(Jbpm4SnapshotProvider.PROCESS_KEY, new Jbpm4WorkflowFactory());
            }
            if(isAvailable("tasklist.activiti")){
                map.put(ProcessEngineType.ACTIVITI.getKey(),
                        new ActivitiWorkflowFactory(Docusafe.getBean("docusafeActivitiWorkflowEngine", WorkflowEngine.class)));
            }
            switch(map.size()){
                case 0:
                    throw new RuntimeException("Brak wybranych WorkflowFactory, a w��czono 'tasklist.multipleProviders'");
                case 1:
                    INSTANCE = map.values().iterator().next();
                    break;
                default:
                    INSTANCE = new CompositeWorkflowFactory(map);
                    break;
            }
        } else if (!WorkflowFactory.jbpm) {
            INSTANCE = new InternalWorkflowFactory();
        } else  {
            INSTANCE = new Jbpm4WorkflowFactory();
        }
    }

    public static WorkflowFactory getInstance()
    {
        return INSTANCE;
    }

    public static Logger getLog()
    {
        return log;
    }
    public static final String SCOPE_DIVISION = "*";
    public static final String SCOPE_SUBDIVISIONS = "!";
    public static final String SCOPE_ONEUSER = "#";
	private static final String OBJECTIVE = "OBJECTIVE";

    public static void switchWorkflow()
    {
        jbpm = !jbpm;
    }

    /*
     * DEKRETACJA (assignmentFactory?)
     */
    /**
     * Dekretacja na wiele dzia��w
     *
     * @throws EdmException
     */
    public static void assignForMany(String activitiId, String username,
            String... divGuids) throws EdmException
    {
        String[] plannedAssignments = new String[divGuids.length];

        for (int i = 0; i < divGuids.length; ++i)
        {
            plannedAssignments[i] = divGuids[i] + "//"
                    + WorkflowUtils.iwfManualName() + "/*/realizacja";
        }

        assignForMany(activitiId, username, plannedAssignments, null);
    }

    /**
     * dekretacja zadania do wielu osob/dzia�ow
     *
     * @param event
     *            - mo�e by� null;
     *
     **/
    public static void assignForMany(String activityId, String username,
            String[] plannedAssignments, ActionEvent event) throws EdmException
    {
        log.info("assignForMany");
        if (Configuration.additionAvailable(Configuration.ADDITION_JBPM))
        {
            JBPMFactory.getInstance().multiAssignment(
                    new String[]
                    {
                        getInstance().convertActivityIds(activityId,
                        true)
                    }, plannedAssignments, event);
            WorkflowActivity wa = WorkflowFactory.getWfActivity(activityId);
            Long docId = wa.getDocumentId();
            Document doc = Document.find(docId);
            TaskSnapshot.updateByDocumentId(docId, doc.getStringType());

        } else if (Configuration.additionAvailable(Configuration.ADDITION_INTERNAL))
        {
            internalAssignForMany(activityId, username, plannedAssignments, event);
        } else
        {
            throw new IllegalStateException("no workflow interface available");
        }
    }

    private static void internalAssignForMany(String activityId, String username, String[] plannedAssignments, ActionEvent event) throws EdmException
    {
        //dekretujemy na jednego usera/dzial a dla wszystkich pozostalych dorabiamy sztucznie assignmenty
        Set<DSUser> users = new HashSet<DSUser>();
        for (int i = 0; i < plannedAssignments.length; i++)
        {
            PlannedAssignment pa = new PlannedAssignment(
                    plannedAssignments[i]);
            if (i == 0)
            {
                InternalWorkflowManager.multiAssignment(
                        new String[]
                        {
                            getInstance().convertActivityIds(
                            activityId, false)
                        }, username, null, pa.getId(), event);
            } else
            {
                if (StringUtils.isEmpty(pa.getUsername()))
                {
                    users.addAll(Arrays.asList(pa.getDivision().getOriginalUsers()));
                    users.addAll(Arrays.asList(pa.getDivision().getOriginalBackupUsers()));
                } else
                {
                    users.add(pa.getUser());
                }
            }
        }

//        for (DSUser user : users) {
//            DSApi.context().makeWfAssignment(activityId, user.getName());
//        }
        DSApi.context().makeWfAssignments(activityId, users);
    }

    private static boolean oneElementArrayCheck(String[] array, String element)
    {
        return array != null
                && array.length == 1
                && array[0].equals(element);
    }

    /**
     * Dekretacja wielu czynnosci na raz + przyk�ad jak nie nale�y pisa� metod w Javie
     * @param activityIds identyfikatory proces�w kancelaryjnych
     * @param username name u�ytkownika dekretuj�cego, zwykle DSApi.context().getPrincipalName()
     * @param plannedAssignments zaplanowane assignmenty - magiczne stringi ze slashami
     * @param onePlannedAssignment pojedynczy magiczny string - dla os�b,
     *   kt�re nie wiedz�, jak w Javie zrobi� jednoelementow� tablice
     * @param event ActionEvent z Webworka do rejestrowania wiadomo�ci/bledow mo�e by� null
     * @throws EdmException
     */
    public static void multiAssignment(String[] activityIds, String username,
            String[] plannedAssignments, String onePlannedAssignment, ActionEvent event) throws EdmException
    {

        Preconditions.checkArgument((plannedAssignments != null || onePlannedAssignment != null), "Brak zaplanowanych dekretacji");

        if (log.isTraceEnabled())
        {
            log.trace("multiAssignment");
        } 

        // poni�szy kod "naprawia" wywo�anie je�eli plannedAssignments zawiera tylko onePlannedAssignment
        if (oneElementArrayCheck(plannedAssignments, onePlannedAssignment))
        {
            //wywo�anie *tej* funkcji bez argumentu plannedAssignments
            multiAssignment(activityIds, username, null, plannedAssignments[0],
                    event);
        } else if (plannedAssignments != null)
        {
            multiPlannedAssignment(activityIds, username, plannedAssignments, event);
        } else
        {
            multiOnePlannedAssignment(activityIds, username, onePlannedAssignment, event);
        }
    }

    //Metoda wydzielona z multi assignment
    private static void multiOnePlannedAssignment(String[] activityIds, String username, String onePlannedAssignment, ActionEvent event) throws EdmException
    {
        log.trace("multiOnePlannedAssignment");
        for (String activityId : activityIds) 
        {
            PlannedAssignment pa = new PlannedAssignment(onePlannedAssignment);

            // tutaj przydzielamy domyslny guid do dekretacji
            if (pa.getDivisionGuid().equals("null"))
            {
                pa.setDivisionGuid(getInstance().findDefaultGuidToAssign(
                        activityId, pa.getUsername()));
            }

            if (AssignmentPermission.getInstance().checkCanAssign(activityId,
                    pa.getId(), event))
            {
                if (Configuration.additionAvailable(Configuration.ADDITION_JBPM))
                {
                    log.trace("addition jbpm");
                    String[] actIds = new String[]
                    {
                        getInstance().convertActivityIds(activityId, true)
                    };
                    JBPMFactory.getInstance().multiAssignment(actIds,
                            new String[]
                            {
                                pa.getId()
                            }, event);

                    for (String actId : actIds)
                    {
                        WorkflowActivity wa = WorkflowFactory.getWfActivity(actId);
                        Long docId = wa.getDocumentId();
                        Document doc = Document.find(docId);
                        TaskSnapshot.updateByDocumentId(docId, doc.getStringType());

                    }
                }

                if (Configuration.additionAvailable(Configuration.ADDITION_INTERNAL))
                {

                    log.trace("addition internal");
                    InternalWorkflowManager.multiAssignment(
                            new String[]
                            {
                                getInstance().convertActivityIds(
                                activityId, false)
                            }, username, null, pa.getId(), event);
                }
            }
        }
    }

    //Metoda wydzielona z multi assignment
    private static void multiPlannedAssignment(String[] activityIds, String username, String[] plannedAssignments, ActionEvent event) throws EdmException
    {
        log.trace("multiPlannedAssignment");
        Set<String> manual = new HashSet<String>();
        for (String paString : plannedAssignments)
        {
            //TODO tworzony jest obiekt PlannedAssignment tylko po to by sprawdzi� czy zadanie jest
            PlannedAssignment pla = new PlannedAssignment(paString);
            if (pla.getKind().equals(wfManualName()))
            {
                manual.add(paString);
            } else
            {
                multiAssignment(activityIds, username, null, paString, event);
            }
        }

        for (String activityId : activityIds)
        {
            WorkflowFactory.assignForMany(activityId, username,
                    manual.toArray(new String[manual.size()]), event);

        }
    }

    /**
     * funkcja do dekretacji jednego zadania
     *
     * @param activityId
     *            - pole obowiazkowe, identyfikator czynnosci, mozna go uzyskac
     *            z samej czynnosci z uzyciem WorkflowFactory
     * @param guidTo
     *            - pole obowiazkowe
     * @param userTo
     *            - pole opcjonalne, jezeli null to dekretacja na dzial, w
     *            przeciwnym razie tylko na tego uzytkownika
     * @param userFrom
     *            - pole opcjonalne, jezeli null to zalogowany uzytkownik
     * @param kind
     *            - pole opcjonalne, rodzaj dekretacji, moze byc
     *            WorkflowFactory.wfManualName() albo WorkflowFactory.wfCcName()
     *            domyslnie manual
     * @param scope
     *            - dotyczy dekretacji na dzial, sa 3 zasiegi, do pobrania z
     *            WorkflowFactory
     * @param objective
     *            - cel dekretacji - wlasciwie dowolny
     * @param event
     *            - Action Event do przekazania komunikatow
     * @throws EdmException
     */
    public static void simpleAssignment(String activityId, String guidTo,
            String userTo, String userFrom, String kind, String scope,
            String objective, ActionEvent event) throws EdmException
    {
        if (log.isTraceEnabled())
        {
            log.trace("simpleAssignment");
            log.trace("activityId={}", activityId);
            log.trace("guidTo={}", guidTo);
            log.trace("userTo={}", userTo);
            log.trace("userFrom={}", userFrom);;
            log.trace("kind={}", kind);
            log.trace("scope={}", scope);
            log.trace("objective={}", objective);
        }

        if (activityId == null)
        {
            throw new WorkflowException(smG.getString("NieMaZadania"));
        }
        String[] activityIds = new String[]
        {
            activityId
        };

        if (guidTo == null)
        {
            throw new WorkflowException("division id cannot be null");
        }
        String plannedAssignment = guidTo;

        if (kind == null)
        {
            kind = wfManualName();
        }

        if (scope == null)
        {
            scope = SCOPE_DIVISION;
        }

        if (objective == null)
        {
            if (kind.equals(wfManualName()))
            {
                objective = smG.getString("doRealizacji");
            } else
            {
                objective = smG.getString("doWiadomosci");
            }
        }

        if (userFrom == null)
        {
            userFrom = DSApi.context().getPrincipalName();
        }
        if (userTo == null || userTo.equals("null") || userTo.length() < 1)
        {
            // dekretacja na dzial
            plannedAssignment += "//" + kind + "/" + scope + "/" + objective;
        } else
        {
            // dekretacja na uzytkownika
            plannedAssignment += "/" + userTo + "/" + kind + "//" + objective;
        }
        multiAssignment(activityIds, userFrom, null, plannedAssignment, event);

    }

    /**
     * Dekretacja na dzial
     * @param activityId
     * @param guidTo
     * @param userFrom
     * @param kind
     * @param scope
     * @param objective
     * @param event
     * @throws EdmException
     */
    public static void simpleDivisionAssignment(String activityId,
            String guidTo, String userFrom, String kind, String scope,
            String objective, ActionEvent event) throws EdmException
    {
        simpleAssignment(activityId, guidTo, null, userFrom, kind, scope,
                objective, event);
        getLog().debug("zadekretowano na dzial: " + guidTo);
    }

    /** mapy do szybkiej dekretacji albo null */
    public Map<String, Map<String, String>> prepareCollectiveAssignments(
            String activity) throws EdmException
    {
        if (StringUtils.isEmpty(activity))
        {
            return null;
        }
        return CollectiveAssignments.getInstance().prepareCollectiveAssignments(activity);
    }

    /**
     * Wyszukuje GUID do przekazania
     * W niektorych dekretacjach (domyslnych) dzial tam wskazany jest bledny
     * Dlatego trzeba szukac dzialu wynikajacego z koordynatora
     * @param activityId
     * @param username
     * @return
     * @throws EdmException
     */
    public String findDefaultGuidToAssign(String activityId, String username)
            throws EdmException
    {
        if (log.isTraceEnabled())
        {
            log.trace("findDefaultGuidToAssign");
            log.trace("activityId={}", activityId);
            log.trace("username={}", username);
        }
        WorkflowActivity activity = getWfActivity(activityId);

        String guidTo = null;
        String userGuid = null;

        DSUser user = DSUser.findByUsername(username);
        DSDivision[] divisions = user.getDivisions();
        String divisionGuid = null;
        // w pierwszej kolejno�ci dekretacja na stanowisko, jak nie ma �adnego
        // to na dzia�, a jak
        // nie ma �adnego dzia�u, to na ROOT_DIVISION
        for (DSDivision division : divisions)
        {
            if (division.isPosition())
            {
                userGuid = division.getGuid();
                break;
            } else if (divisionGuid == null && division.isDivision())
            {
                divisionGuid = division.getGuid();
            }
        }
        if (userGuid == null)
        {
            if (divisionGuid != null)
            {
                userGuid = divisionGuid;
            } else
            {
                userGuid = DSDivision.ROOT_GUID;
            }
        }

        guidTo = userGuid;

        log.trace("random guid: {}", guidTo);

        /*
         * nastepnie guid dzialu w ktorym jest czynnosc, o ile uzytkownik jest w
         * tym dziale
         */
        try
        {
            DSDivision activityGuid = DSDivision.find(activity.getCurrentGuid());
            if (DSDivision.isInDivision(activityGuid, username)
                    || DSDivision.isInDivisionAsBackup(activityGuid, username))
            {
                guidTo = activityGuid.getGuid();
            }
            log.debug("activity guid: " + activityGuid.getGuid() + " guidTo : " + guidTo);
        } catch (Exception ex)
        {
            log.warn(ex.getMessage(), ex);
        }

        /*
         * jesli rockwell - pismo trzeba dekretowac na dzial ktory wynika z
         * logiki dokumentu (dzial koordynatora)
         */
        if(activity != null)
        {
	        Document doc = Document.find(activity.getDocumentId());
	        DocumentKind dk = doc.getDocumentKind();
	        //FIXME - ten kawalek kodu jest krzywy - ta metoda musi byc gdzie zaszyta w dockindzie
	        if (dk != null && DocumentLogicLoader.ROCKWELL_KIND.equals(dk.getCn()))
	        {
	            ProcessCoordinator pc = dk.logic().getProcessCoordinator(
	                    (OfficeDocument) doc);
	            if (pc != null)
	            {
	                String corrGuid = pc.getGuid();
	                DSDivision divTo = DSDivision.find(corrGuid);
	                if (DSDivision.isInDivision(divTo, username) || DSDivision.isInDivisionAsBackup(divTo, username))
	                {
	                    guidTo = corrGuid;
	                }
	                log.trace("rockwell, guidTo: " + guidTo + "corrGuid: " + corrGuid + " activity guid: " + activity.getCurrentGuid());
	            } else
	            {
	                log.info("rockwell, nie ustalono koordynatora procesu");
	            }
	
	
	        }
        }
        return guidTo;
    }

    /**
     * Dekretacja na siebie
     * @param activityId
     * @param event
     * @throws EdmException
     */
    public void assignMe(String activityId, ActionEvent event)
            throws EdmException
    {
        String guidTo = findDefaultGuidToAssign(activityId, DSApi.context().getPrincipalName());
        simpleAssignment(activityId, guidTo,
                DSApi.context().getPrincipalName(), null, null, null, null,
                event);
    }

    /*
     * ZWROT DEKRETACJI
     */
    public static void returnAssignment(WorkflowActivity activity,
            ActionEvent event) throws WorkflowException, EdmException
    {
        if (Configuration.additionAvailable(Configuration.ADDITION_JBPM))
        {
            JBPMFactory.getInstance().returnAssignment(activity);
        } else
        {
            InternalWorkflowManager.returnAssignment(activity);
        }
    }

    /**
     * Tworzenie procesu
     * @param doc
     * @param toCoordinator
     * @throws EdmException
     */
    public static void createNewProcess(OfficeDocument doc,
            boolean toCoordinator) throws EdmException
    {
        if (log.isTraceEnabled())
        {
            log.trace("createNewProcess");
            if (doc != null)
            {
                log.trace("docId={}", doc.getId());
            } else
            {
                log.trace("doc==null");
            }
            log.trace("toCoordinator={}", toCoordinator);
        }
        String processKey = "0";
        if (Configuration.additionAvailable(Configuration.ADDITION_INTERNAL))
        {
            processKey = InternalWorkflowManager.createNewProcess(doc, toCoordinator);
        }
        if (Configuration.additionAvailable(Configuration.ADDITION_JBPM))
        {
            JBPMFactory.getInstance().createNewProcess(
                    doc.getId(),
                    DSApi.context().getPrincipalName(),
                    DSDivision.ROOT_GUID,
                    smG.getString("PismoPrzyjete"),
                    toCoordinator, processKey);
        }
        if (toCoordinator)
        {
            addCoordinatorHistEntry(doc);
        }
        TaskSnapshot.updateByDocumentId(doc.getId(), doc.getStringType());
    }
    
    /**
     * Tworzenie nowego procesu bez koordynatora
     * @param doc
     * @param objective
     * @throws EdmException
     */
    public static void createNewProcess(OfficeDocument doc, String objective)
            throws EdmException
    {
        createNewProcess(doc, false, objective);
    }

    /**
     * Tworzenie nowego procesu z podaniem celu
     * @TODO 0 nie rozumiem dlaczego metoda bez objective jest implementowana, a nie jest
     * przeciazona
     * @param doc
     * @param toCoordinator
     * @param objective
     * @throws EdmException
     */
    public static void createNewProcess(OfficeDocument doc,
            boolean toCoordinator, String objective) throws EdmException
    {
        if (log.isTraceEnabled())
        {
            log.trace("createNewProcess");
            if (doc != null)
            {
                log.trace("docId={}", doc.getId());
            } else
            {
                log.trace("doc==null");
            }
            log.trace("toCoordinator={}", toCoordinator);
            log.trace("objective={}", objective);
        }
        String processKey = "0";
        if (AvailabilityManager.isAvailable("dwr")){
        	doc.getDocumentKind().logic().onStartProcess(doc);
        }else if (Configuration.additionAvailable(Configuration.ADDITION_INTERNAL))
        {
            processKey = InternalWorkflowManager.createNewProcess(doc,
                    toCoordinator, objective);
        }	
        else if (Configuration.additionAvailable(Configuration.ADDITION_JBPM))
        {
            JBPMFactory.getInstance().createNewProcess(
                    doc.getId(),
                    DSApi.context().getPrincipalName(),
                    DSDivision.ROOT_GUID, objective, toCoordinator, processKey);
        }
        if (!AvailabilityManager.isAvailable("dwr") && toCoordinator)
        {
            addCoordinatorHistEntry(doc);
        }
        
      
        TaskSnapshot.updateByDocumentId(doc.getId(), doc.getStringType());
    }

    /**
     * @TODO JAK WYZEJ
     * @param doc
     * @param toCoordinator
     * @param objective
     * @param participantId
     * @param assignedDivision
     * @param assignedUser
     * @throws EdmException
     */
    public static void createNewProcess(OfficeDocument doc,
            boolean toCoordinator, String objective, String participantId,
            String assignedDivision, String assignedUser) throws EdmException
    {
        if (log.isTraceEnabled())
        {
            log.trace("createNewProcess");
            if (doc != null)
            {
                log.trace("docId={}", doc.getId());
            } else
            {
                log.trace("doc==null");
            }
            log.trace("toCoordinator={}", toCoordinator);
            log.trace("objective={}", objective);
            log.trace("participantId={}", participantId);
            log.trace("assignedDivision={}", assignedDivision);
            log.trace("assignedUser={}", assignedUser);
        }
        String processKey = "0";
        if (Configuration.additionAvailable(Configuration.ADDITION_INTERNAL))
        {
            processKey = InternalWorkflowManager.createNewProcess(doc,
                    toCoordinator, objective, participantId, assignedDivision,
                    assignedUser);
        }

        if (Configuration.additionAvailable(Configuration.ADDITION_JBPM))
        {
            JBPMFactory.getInstance().createNewProcess(doc.getId(),
                    assignedUser, assignedDivision, objective, toCoordinator,
                    processKey);
        }
        if (toCoordinator)
        {
            addCoordinatorHistEntry(doc);
        }
        ((OfficeDocument) doc).setCurrentAssignmentGuid(assignedDivision);
        TaskSnapshot.updateByDocumentId(doc.getId(), doc.getStringType());
    }

    /**
     * Dodaje wpis do historii
     * @param doc
     */
    private static void addCoordinatorHistEntry(OfficeDocument doc)
    {
        try
        {
            if (log.isTraceEnabled())
            {
                log.trace("addCoordinatorHistEntry");
                try
                {
                    log.trace("docId={}", doc.getId());
                } catch (Exception e)
                {
                    log.trace("doc==null");
                }
            }
            ProcessCoordinator pc = null;
            if (doc.getDocumentKind() != null)
            {
                pc = doc.getDocumentKind().logic().getProcessCoordinator(doc);
            }
         	String guid = DSDivision.ROOT_GUID;
            if (pc !=null &&  pc.getGuid() !=null)
            	 guid =  pc.getGuid();
            
            doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                    DSApi.context().getPrincipalName(),
                    DSDivision.ROOT_GUID,
                    pc == null ? null : pc.getUsername(),
                    guid,
                    smG.getString("DekrtacjaNaKoordynatora"),
                    smG.getString("doRealizacji"),
                    false,
                    AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION));

            
           
        }
        catch (EdmException e)
            
        {
            log.error("blad wpisu dekretacji do koordynatora", e);
        }
    }

    /**
     * Reczne konczenie procesu
     * @param activityId
     * @param checkCanFinish
     * @throws EdmException
     */
    public void manualFinish(String activityId, boolean checkCanFinish)
            throws EdmException
    {
        if (log.isTraceEnabled())
        {
            log.trace("manualFinish");
            log.trace("activityId={}", activityId);
            log.trace("checkCanFinish={}", checkCanFinish);
        }
        if (Configuration.additionAvailable(Configuration.ADDITION_INTERNAL))
        {
            String internalId = convertActivityIds(activityId, false);
            WorkflowActivity activity = getWfActivity(internalId, false);
            Long docId = getDocumentId2(activityId);
            if(docId == null) { //czasem mo�e nie znale��, wtedy absolutnie nic nie da si� zrobi� z dokumentem, dlatego to obej�cie
                docId = activity.getDocumentId();
            }
            Document doc = Document.find(docId);
            if (doc instanceof OfficeDocument)
            {
                WorkflowUtils.manualFinish((OfficeDocument) doc, activity, checkCanFinish);
            } else
            {
                deprFinish(activity.getTaskFullId());
            }
        }
        if (Configuration.additionAvailable(Configuration.ADDITION_JBPM))
        {

            String taskId = convertActivityIds(activityId, true);
            Document doc = Document.find(JBPMFactory.getInstance().getActivityByTaskId(Long.parseLong(taskId)).getDocumentId());
            doc.getDocumentKind().logic().onEndProcess((OfficeDocument) doc, JBPMFactory.getInstance().getActivityByTaskId(Long.parseLong(taskId)).getProcessKind().equalsIgnoreCase("obieg_reczny") ? true : false);
            JBPMFactory.getInstance().finishTask(Long.parseLong(taskId));
        }

    }

    /**
     * Reczne konczenie zadan dla dokumentu
     * @param docId
     * @param checkCanFinish
     * @throws AccessDeniedException
     * @throws EdmException
     */
    public static void manualFinish(Long docId, boolean checkCanFinish)
            throws AccessDeniedException, EdmException
    {
        String[] activityIds = findAllDocumentTasks(docId);
        //musimy wyaliminowac te same procesy z tablicy znajdujace sie u kilku osob  poniewaz wystepuja jesli zadanie nie zostalo jeszcze 
        //przez nikogo przejete a chcemy ja zamknac 
        List<String> list = Arrays.asList(activityIds);
        Set<String> activityIdsSet = new HashSet<String>(list);
        for (String id : activityIdsSet)
        {
            WorkflowFactory.getInstance().manualFinish(activityIds[0], checkCanFinish);
        }
    }

    /**
     * Akceptowanie zadania - do przeci��enia
     * @param act id czynno�ci z prefiksem okre�laj�cym workflow engine - np "internal,123", "jbpm4,3004", "activiti,4562"
     * @param event
     * @param document
     * @param division 
     * @throws EdmException
     */
    public void acceptTask(String act, ActionEvent event, OfficeDocument document, String division)
            throws EdmException
    {
        if (log.isTraceEnabled())
        {
            log.trace("acceptTask");
            log.trace("act={}", act);
            log.trace("ActionEvent={}", event);
            try
            {
                log.trace("documentId={}", document.getId());
            } catch (Exception e)
            {
                log.trace("documetId=null");
            }
        }
        String activityKey = keyOfId(act);
        if (Configuration.additionAvailable(Configuration.ADDITION_JBPM))
        {

            if (jbpm)
            {
                JBPMFactory.getInstance().acceptTask(Long.parseLong(activityKey));
            } else
            {
                JBPMFactory.getInstance().acceptTask(
                        Long.parseLong(getTaskIdOfActivityId("internal," + activityKey)));
            }
            StringManager smG = GlobalPreferences.loadPropertiesFile("", null);
            WorkflowActivity activity = getWfActivity(
                    keyToId(convertActivityIds(act, true)), true);

            ((OfficeDocument) document).addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                    activity.getLastUser(),
                    activity.getLastDivision(),
                    DSApi.context().getPrincipalName(),
                    activity.getCurrentUser(),
                    activity.getObjective(),
                    WorkflowFactory.getInstance().isManual(activity) ? smG.getString("doRealizacji")
                    : smG.getString("doWiadomosci"),
                    true,
                    WorkflowFactory.getInstance().isManual(activity) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION
                    : AssignmentHistoryEntry.PROCESS_TYPE_CC));
        }
        if (Configuration.additionAvailable(Configuration.ADDITION_INTERNAL))
        {
            // internal
            if (jbpm)
            {
                activityKey = WorkflowFactory.getActivityKeyOfTaskId(activityKey);
            }
            TaskSnapshot task = TaskSnapshot.getAbsInstance().findByActivityKey(activityKey);
            WfAssignment assignment = WorkflowFactory.getWfAssignment(
                    "internal", activityKey);
            WfActivity activity = assignment.activity();

            if (event.getLog().isDebugEnabled())
            {
                event.getLog().debug("assignment=" + assignment);
            }

            // odczytanie identyfikatora dokumentu
            NameValue[] nv = activity.process_context();

            final NameValue nvPerformer = WorkflowUtils.findParameter(nv,
                    "ds_performing_user");
            final NameValue nvAssignedUser = WorkflowUtils.findParameter(nv,
                    "ds_assigned_user");
            NameValue nvParticipant = WorkflowUtils.findParameter(nv,
                    "ds_participant_id");
            //WorkflowUtils.findParameter(nv,
            //		"ds_participant_id").value().set(
            //		DSApi.context().getPrincipalName());
            nvParticipant.value().set(DSApi.context().getPrincipalName());
            nvAssignedUser.value().set(DSApi.context().getPrincipalName());
            if (nvPerformer != null)
            {
                String performingUser = (String) nvPerformer.value().value();
                //nvAssignedUser.value().set(DSApi.context().getPrincipalName())
                // ;
                if (StringUtils.isEmpty(performingUser))
                {
                    nvPerformer.value().set(DSApi.context().getPrincipalName());
                }

            }

            // TODO: sprawdzanie, czy u�ytkownik ma prawo przyjmowa�
            // zadanie w dziale, dla kt�rego zosta�o przeznaczone

            activity.set_process_context(nv);

            task.setDocumentId(document.getId());

            // przyj�cie zadania przez tego u�ytkownika
            // i dopisanie odpowiedniej informacji w historii dokumentu
            if (!assignment.get_accepted_status())
            {
                final NameValue nvLastUser = WorkflowUtils.findParameter(nv,
                        "ds_last_activity_user");
                if (nvLastUser != null)
                {
                    nvLastUser.value().set(DSApi.context().getPrincipalName());
                }

                task.setActivity_lastActivityUser(DSApi.context().getPrincipalName());
                document.changelog(DocumentChangelog.WF_ACCEPT);
                assignment.set_accepted_status(true);
                task.setAccepted(true);
                task.setSameProcessSnapshotsCount(1);

                if (document instanceof OfficeDocument)
                {
                    NameValue sourceUser = WorkflowUtils.findParameter(nv,
                            "ds_source_user");
                    NameValue sourceGuid = WorkflowUtils.findParameter(nv,
                            "ds_source_division");
                    NameValue targetGuid = WorkflowUtils.findParameter(nv,
                            "ds_assigned_division");
                    NameValue objective = WorkflowUtils.findParameter(nv,
                            "ds_objective");
                    if (sourceUser != null && sourceUser.value() != null
                            && sourceGuid != null
                            && sourceGuid.value() != null
                            && // targetUser != null && targetUser.value() != null
                            // &&
                            targetGuid != null && targetGuid.value() != null
                            && objective != null && objective.value() != null)
                    {
                        StringManager smG = GlobalPreferences.loadPropertiesFile("", null);
                        ((OfficeDocument) document).addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                                (String) sourceUser.value().value(),
                                (String) sourceGuid.value().value(),
                                DSApi.context().getPrincipalName(),
                                (String) targetGuid.value().value(),
                                (String) objective.value().value(),
                                WorkflowUtils.isManual(activity) ? smG.getString("doRealizacji")
                                : smG.getString("doWiadomosci"),
                                true,
                                WorkflowUtils.isManual(activity) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION
                                : AssignmentHistoryEntry.PROCESS_TYPE_CC));

                        // TODO: dziwne sprawdzanie, czy proces manualny -
                        // zmieni� na isManual ???
                        ExtendedAttribute attr = activity.container().manager().getProcessDefinition().getExtendedAttribute(
                                "ds_needs_own_document");
                        if (attr != null
                                && Boolean.valueOf(attr.getValue()))
                        {
                            ((OfficeDocument) document).setCurrentAssignmentGuid((String) targetGuid.value().value());
                            //wydaje mi sie ze ta zmienna nie powinna byc zmieniana do tegi jest setCurrentAssignmentGuid
                            ///	((OfficeDocument) document).setDivisionGuid((String) targetGuid.value().value());

                            ((OfficeDocument) document).setCurrentAssignmentUsername(DSApi.context().getPrincipalName());
                            ((OfficeDocument) document).setCurrentAssignmentAccepted(Boolean.TRUE);
                        }
                    }
                }
            }
        }
        if (document.getDocumentKind() != null)
        {
            document.getDocumentKind().logic().acceptTask(document, event, act);
        }
    }

    /**
     * Spradza czy moge przekazac sobie
     * @param documentId
     * @param activityId
     * @return
     * @throws UserNotFoundException
     * @throws EdmException
     */
    public Boolean checkCanAssignMe(Long documentId, String activityId)
            throws UserNotFoundException, EdmException
    {
    	if (AvailabilityManager.isAvailable("NoAssignButton")) {
    		return false;
    	}
    	    	
        if (log.isTraceEnabled()){
            log.trace("checkCanAssignMe");
            log.trace("documentId={}", documentId);
            log.trace("activityId={}", activityId);
        }
        Boolean canAssignMe;
        String actId = activityId;
        if (actId == null){
            actId = WorkflowFactory.findManualTask(documentId);
        }
        if (actId != null){
            WorkflowActivity act = WorkflowFactory.getWfActivity(actId);
            if (StringUtils.isEmpty(act.getCurrentUser())){
                    DSDivision div = DSDivision.find(act.getCurrentGuid());
                    canAssignMe = UserFactory.getInstance().getCanAssignDivisions().contains(div);
            } else {
                DSUser user = UserFactory.getInstance().findByUsername(act.getCurrentUser());
                if (user.getName().equals(DSApi.context().getPrincipalName())){
                    canAssignMe = false;
                } else { //to jest tak z dupy, �e nie wiem co napisa�
                    canAssignMe = UserFactory.getInstance().getCanAssignUsers().contains(user);
                }
            }
        } else {
            canAssignMe = false;
        }
        return canAssignMe;
    }

    /**
     *
     * @param docId
     * @return
     * @throws EdmException
     */
    public static String[] findDocumentTasks(Long docId) throws EdmException
    {
        return findDocumentTasks(docId, DSApi.context().getPrincipalName());
    }

    /**
     *
     * @param docId
     * @return
     * @throws EdmException
     */
    public static String findManualTask(long docId) throws EdmException
    {
        String[] activityIds = findDocumentTasks(docId, null);
        Set<String> ss = new HashSet<String>();
        for (String activityId : activityIds)
        {
            if (getInstance().isManual(activityId))
            {
                ss.add(activityId);
            }
        }
        if (ss.size() > 1 || ss.size() <= 0)
        {
            log.debug("dla dokumentu przywroconego na liste zadan znaleziono "
                    + activityIds.length + " zadan");
            return null;
        } else
        {
            return ss.iterator().next();
        }
    }

    public static String[] findAllDocumentTasks(Long docId) throws EdmException
    {
        return findDocumentTasks(docId, null);
    }

    /**
     * Znajdz zadania dla dokumentu
     * @param docId
     * @param username
     * @return
     * @throws EdmException
     */
    public static String[] findDocumentTasks(Long docId, String username)
            throws EdmException
    {
        if (log.isTraceEnabled())
        {
            log.trace("findDocumentTasks");
            log.trace("docId={}", docId);
            log.trace("username={}", username);
        }
        if (jbpm)
        {
            Long[] tasksIds = JBPMFactory.getInstance().findDocumentTasks(
                    docId);
            String[] result = new String[tasksIds.length];
            for (int i = 0; i < tasksIds.length; i++)
            {
                result[i] = INSTANCE.keyToId(String.valueOf(tasksIds[i]));
            }
            return result;
        } else
        {
            WfAssignment[] asgn = DSApi.context().getWfAssignments(docId, username);
            String[] activityIds = new String[asgn.length];
            for (int i = 0; i < asgn.length; i++)
            {
                activityIds[i] = "internal," + asgn[i].activity().key();
            }
            return activityIds;
        }
    }

    @Deprecated
    /*  wywalic z iwf */
    public WfActivity getOldActivity(String activityKey) throws EdmException
    {
        return DSApi.context().getWfActivityAll(
                ActivityId.make("internal", activityKey));
    }

    public static WorkflowActivity getWfActivity(String activityId)
            throws EdmException
    {
        if (jbpm)
        {
            return null;
        } else
        {
            return getWfActivity(activityId, jbpm);
        }
    }

    public static WorkflowActivity getWfActivity(String activityId,
            Boolean jbpm_style) throws WorkflowException, EdmException
    {
        if (jbpm_style)
        {
            //jbpm4
            return JBPMFactory.getInstance().getActivityByTaskId(
                    Long.parseLong(getInstance().keyOfId(activityId)));
        } else
        { //internal
            return new WorkflowActivity(getInstance().keyOfId(activityId));
        }
    }

    /**
     * wyszukuje wszystkich uzytkownik�w kt�rzy maj� dost�p do zadania TYLKO
     * JBPM
     *
     * @throws EdmException
     */
    public String[] getTaskUsers(String activityId) throws EdmException
    {
        if (log.isTraceEnabled())
        {
            log.trace("getTaskUsers");
            log.trace("activityId={}", activityId);
        }
        Map<String, String[]> actors = JBPMFactory.getInstance().getTaskActors(
                activityId);
        Set<String> users = new HashSet<String>();
        for (String user : actors.get("users"))
        {
            users.add(user);
        }
        for (String guid : actors.get("divs"))
        {
            DSDivision div = DSDivision.find(guid);
            /*
             * @TODO - tu powinna byc metoda ktora sprawdza czy zadanie nalezy sie dzialowi
             * i dzialom podrzednym - takie cos powinno byc ustawienie w zmiennej dotyczacej zadania lub procesu
             * Jezeli rusze ten false zacznie sie sypac metoda acceptTask w JBPMFactory
             */
            for (DSUser user : div.getUsers(false))
            {
                users.add(user.getName());
            }
        }
        if (users.size() == 0)
        {
            log.debug("activityId={} nie znaleziono uzytkownikow", activityId);
            String admin = GlobalPreferences.getAdminUsername();
            if (admin != null)
            {
                users.add(admin);
                log.debug("zadanie przedekretowane na uzytkownika {}", admin);
            } else
            {
                log.debug("Nie okreslono administratora - kierujemy na usera admin");
                users.add("admin");
            }
        }
        /*LogFactory.getLog("kuba").debug(
        "znalazlem " + users.size()
        + " uzytkownikow majacych dostep do zadania "
        + activityId);
        LogFactory.getLog("kuba")
        .debug("pierwszy : " + users.iterator().next());*/
        return users.toArray(new String[users.size()]);
    }

    /*
     * NARZ�DZIA PRYWATNE
     */
    /**
     * @param ids
     *            identyfikatory postaci
     *            "internal,[id_czynnosci_zgodne_ze_zmienna_'jbpm']
     * @param toJbpm
     * @return jezeli toJbpm = true to daje sparsowanego id taska w jbpm, w
     *         przeciwnym przypadku id wew workflow postaci
     *         "internal,[id_wew_workflow]"
     * @throws EdmException
     */
    private String[] convertActivityIds(String[] ids, Boolean toJbpm)
            throws EdmException
    {
        String[] result = new String[ids.length];
        if (jbpm && toJbpm)
        {
            for (int i = 0; i < result.length; i++)
            {
                result[i] = getInstance().keyOfId(ids[i]);
            }
            return result;
        }
        if (!jbpm && !toJbpm)
        {
            return ids;
        }
        for (int i = 0; i < ids.length; i++)
        {
            if (toJbpm)
            {
                result[i] = getTaskIdOfActivityId(ids[i]);
            } else
            {
                result[i] = WorkflowFactory.getInstance().keyToId(
                        getActivityKeyOfTaskId(getInstance().keyOfId(ids[i])));
            }
        }
        return result;
    }

    /**
     * id ma w starym wf posta� "internal,"+key nie ma spojnosci w stosowaniu
     * key lub id (np w klasie task jest key) nalezy miec to na uwadze i
     * konwertowac przy uzyciu ponizysz metod
     *
     * TODO dobrze by bylo z czasem wyeliminowac ta nazwe workflow i poslugiwac
     * sie tylko key
     */
    public String keyToId(String key)
    {
        return "internal," + key;
    }
    
    public String keyToIdActivity(String key)
    {
        return ProcessEngineType.ACTIVITI.getKey() + "," + key;
    }
    
    public String keyOfId(String id)
    {
        String[] aa = id.split(",");
        return aa[1];
    }

    private String convertActivityIds(String id, Boolean toJbpm)
            throws EdmException
    {
        return getInstance().convertActivityIds(new String[]
                {
                    id
                }, toJbpm)[0];
    }

    private static String getActivityKeyOfTaskId(String taskId)
            throws EdmException
    {
        Long processId = JBPMFactory.getInstance().internalProcIdOfTaskId(
                taskId);
        WfProcess process = Finder.find(WfProcessImpl.class, processId);
        return process.get_activities_in_state_array("open.running")[0].key();
    }

    private static String getTaskIdOfActivityId(String activityId)
            throws EdmException
    {
        Long processId = JBPMFactory.getInstance().findProcessInstanceByActivityId(activityId);
        if (processId != null)
        {
            return String.valueOf(JBPMFactory.getInstance().findTaskByProcessId(processId));
        } else
        {
            return null;
        }
    }

    public static String activityIdOfWfNameAndKey(String workflowName,
            String activityKey)
    {
        ActivityId aid = ActivityId.make(workflowName, activityKey);
        return aid.toString();
    }

    @Deprecated
    public static String workflowNameOfId(String activityId)
            throws WorkflowException
    {
        return "internal";
    }

    /**
     * Pobranie zadania poprzez Id dokumentu
     * @param id
     * @return
     * @throws EdmException
     */
    public static WorkflowActivity getActivityByTaskId(String id)
            throws EdmException
    {
        return JBPMFactory.getInstance().getActivityByTaskId(Long.parseLong(id));
    }

    @Deprecated
    public static WfAssignment getWfAssignment(String workflowName,
            String activityKey) throws EdmException
    {
        return DSApi.context().getWfAssignment(
                ActivityId.make("internal", activityKey));
    }

    @Deprecated
    public static WfAssignment getWfAssignment(String workflowName,
            String activityKey, String username) throws EdmException
    {
        ActivityId aid = ActivityId.make("internal", activityKey);
        return DSApi.context().getWfAssignment(aid, username);
    }

    @Deprecated
    public void deprFinish(String activityId) throws WorkflowException,
            EdmException
    {
        WfActivity activity = getOldActivity(getInstance().keyOfId(activityId));
        NameValue[] nvs = activity.container().process_context();
        NameValue nv = WorkflowUtils.findParameter(nvs, "ds_finished");
        nv.value().set(Boolean.TRUE);
        //WorkflowUtils.findParameter(activity.process_context(),"ds_finished").
        // value().set(Boolean.TRUE);
        activity.complete();
    }

    /**
     * tworzenie u�ytkownika w serwerze workflow, nie jest tworzony jesli juz
     * istnieje uzytkownik o podanej nazwie
     *
     * @param username
     * @param password
     * @param realName
     * @param email
     * @param LdapEntry
     * @throws WorkflowException
     *
     */
    @Deprecated
    public static void createUser(String username, String password,
            String realName, String email, String LdapEntry)
            throws WorkflowException
    {
        WfService ws = WorkflowServiceFactory.newInternalInstance().newService();
        ws.connectAdmin();
        String[] wfUsers = ws.get_sequence_resource_key();
        Arrays.sort(wfUsers);
        if (Arrays.binarySearch(wfUsers, username) < 0)
        {
            ws.create_resource(username, password, realName, email, LdapEntry);
        }
        ws._disconnect();
    }

    @Deprecated
    public static void removeUser(String username,String toUsername) throws WorkflowException
    {

        if (AvailabilityManager.isAvailable("tasklist.jbpm4"))
        {
            return;
        }
        WfService iws = WorkflowServiceFactory.newInternalInstance().newService();
        iws.connectAdmin();
        try
        {
            iws.removeResource(username,toUsername);
        } catch (WorkflowResourceNotFoundException e)
        {
        }
        iws._disconnect();
    }

    /**
     * tworzenie mapowa� grup u�ytkownik�w na u�ytkownik�w
     *
     * @param packageId
     * @param processDefinitionId
     * @param log
     * @throws DivisionNotFoundException
     * @throws EdmException
     * @deprecated
     */
    public static void createUserGroupsToUsersMapping(String packageId,
            String processDefinitionId, org.apache.commons.logging.Log log)
            throws DivisionNotFoundException, EdmException
    {
        WfService ws = WorkflowServiceFactory.newInternalInstance().newService();
        ws.connectAdmin();
        WfProcessMgr[] mgrs = ws.getProcessMgrs(packageId, processDefinitionId);

        if (mgrs == null || mgrs.length != 1)
        {
            log.warn("Identyfikator procesu nie jest "
                    + "jednoznaczny lub nie odpowiada �adnemu procesowi :"
                    + processDefinitionId);
            return;
        }

        WfProcessMgr mgr = mgrs[0];

        // uczestnicy zdefiniowani w danym procesie
        Participant[] participants = mgr.getParticipants();

        for (int f = 0; f < participants.length; f++)
        {
            String participantId = participants[f].getId();
            if (participantId.startsWith("ds_guid_"))
            {
                // identyfikator dzia�u
                String guid = participantId.substring("ds_guid_".length());
                DSDivision division = DSDivision.find(guid);
                // u�ytkownicy w tym dziale i w dzia�ach podrz�dnych
                DSUser[] users = division.getUsers(true);

                log.info("Usuwanie mapowa� uczestnik�w " + "w procesie "
                        + packageId + "/" + processDefinitionId
                        + " dla uczestnika " + participantId);

                ws.removeParticipantToUsernameMappings(packageId,
                        processDefinitionId, participantId);

                for (int u = 0; u < users.length; u++)
                {
                    log.info("Dodawanie mapowania uczestnika " + "w procesie "
                            + packageId + "/" + processDefinitionId
                            + " dla uczestnika " + participantId
                            + " i u�ytkownika " + users[u].getName());

                    ws.addParticipantToUsernameMapping(packageId,
                            processDefinitionId, participantId, users[u].getName());
                }
            }
        }
        ws._disconnect();
    }

    @Deprecated
    public static void addParticipantToUsernameMapping(final String packageId,
            final String processDefinitionId, final String participantId,
            final String username) throws WorkflowException
    {
        WfService ws = WorkflowServiceFactory.newInternalInstance().newService();
        ws.connectAdmin();
        ws.addParticipantToUsernameMapping(packageId, processDefinitionId,
                participantId, username);
        ws._disconnect();
    }

    @Deprecated
    public static void removeParticipantToUsernameMapping(
            final String packageId, final String processDefinitionId,
            final String participantId, final String username)
            throws WorkflowException
    {
        WfService ws = WorkflowServiceFactory.newInternalInstance().newService();
        ws.connectAdmin();
        ws.removeParticipantToUsernameMapping(packageId, processDefinitionId,
                participantId, username);
        ws._disconnect();
    }

    @Deprecated
    public static void removeParticipantToUsernameMappings(
            final String packageId, final String processDefinitionId,
            final String participantId) throws WorkflowException
    {
        WfService ws = WorkflowServiceFactory.newInternalInstance().newService();
        ws.connectAdmin();
        ws.removeParticipantToUsernameMappings(packageId, processDefinitionId,
                participantId);
        ws._disconnect();
    }

    public static int[] getTaskCount(String username)
            throws NotConnectedException, WorkflowException
    {
        final WfService iws = WorkflowServiceFactory.newInternalInstance().newService();
        iws.connect(username);

        int[] taskCount = iws.getResourceObject().getTaskCount();

        iws.disconnect();

        return taskCount;
    }

    // metody WorkflowUtils

    /*
     * te trzy metody sa do odroznienia roznych rodzajow dekretacji, nie maja
     * nic wspolnego z wew. workflow
     */
    public static String wfManualName()
    {
        return WorkflowUtils.iwfManualName();
    }

    public static String wfCcName()
    {
        return WorkflowUtils.iwfCcName();
    }

    public static String wfSecondOpinionName()
    {
        return WorkflowUtils.iwfSecondOpinionName();
    }

    /* --- */
    public static void reopenWf(Long documentId) throws EdmException
    {
        reopenWf(documentId, DSApi.context().getPrincipalName());
    }

    public static void reopenWf(Long documentId, String user)
            throws EdmException
    {

        if (!DSPermission.PRZYWRACANIE_NA_LISTE_ZADAN.hasPermission())
        {
            throw new EdmException(sm.getString("NieMaszUprawnienDoPrzywracaniaDokumentowNaListeZadan"));
        }

        if (Configuration.additionAvailable(Configuration.ADDITION_JBPM) || AvailabilityManager.isAvailable("tasklist.jbpm4"))
        {
            StringManager smG = GlobalPreferences.loadPropertiesFile("", null);
            boolean hasTasks = (JBPMFactory.getInstance().getTasksNumber(documentId) > 0);

            LoggerFactory.getLogger("tomekl").debug("WF size " + JBPMFactory.getInstance().getTasksNumber(documentId));

            if (hasTasks)
            {
                throw new EdmException(
                        smG.getString("NieMoznaWznowicProcesuDlaDokumentu")
                        + " "
                        + smG.getString("poniewazInnyUzytkownikMaGoNaSwojejLiscieZadan"));
            }

            if (Configuration.officeAvailable() || Configuration.faxAvailable())
            {
                if (user == null)
                {
                    user = DSApi.context().getPrincipalName();
                }
                OfficeDocument document = OfficeDocument.find(documentId);
                // DSDivision[] divisions =
                // DSApi.context().getDSUser().getDivisions();
                DSDivision[] divisions = DSUser.findByUsername(user).getDivisions();

                String userDivisionGuid = null;
                if (divisions.length > 0)
                {
                    for (int i = 0; i < divisions.length; i++)
                    {
                        if (divisions[i].isPosition())
                        {
                            userDivisionGuid = divisions[i].getParent().getGuid();
                            break;
                        } else if (divisions[i].isDivision())
                        {
                            userDivisionGuid = divisions[i].getGuid();
                            break;
                        }
                    }
                }

                if (userDivisionGuid == null)
                {
                    userDivisionGuid = DSDivision.ROOT_GUID;
                }

                if(AvailabilityManager.isAvailable("tasklist.jbpm4")){
                	Map<String, Object> map = Maps.newHashMap();
            		map.put(ASSIGN_USER_PARAM, user);
            		map.put(ASSIGN_DIVISION_GUID_PARAM, userDivisionGuid);
            		document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
                }
                else{
                	Long procId = JBPMFactory.getInstance().createNewProcess(
                            document.getId(), user, userDivisionGuid,
                            "Przywr�cono na list� zada�", false,
                            null);
                }
                

            }
        } else
        {
            Long internalProcId = WorkflowUtils.reopenWf(documentId, user);
        }

    }

    /**
     * @TODO - co ta metoda robi dokladnie?? Wedlug mnie sprawdza tylko na
     *       potrzeby przyciskow na formatce
     * @param document
     * @param activity
     * @return
     * @throws EdmException
     */
    public static FinishOrAssignState getFinishOrAssignState(
            OfficeDocument document, WorkflowActivity activity)
            throws EdmException
    {
        return WorkflowUtils.getFinishOrAssignState(document, activity);
    }

    /* FIXME z poni�szych dwoch metod korzysta summaryTabAction - do przer�bki */
    public static ProcessId getManualProcesId()
    {
        return ProcessId.create(InternalWorkflowService.NAME, WorkflowUtils.iwfPackageName(), WorkflowUtils.iwfManualName());
    }

    public static ProcessId getCCProcesId()
    {
        return ProcessId.create(InternalWorkflowService.NAME, WorkflowUtils.iwfPackageName(), WorkflowUtils.iwfCcName());
    }

    /* ----------- */
    public boolean isManual(String id) throws EdmException
    {
        return getInstance().isManual(getWfActivity(id));
    }

    public boolean isManual(WorkflowActivity act) throws WorkflowException
    {
        if (jbpm)
        {
            return act.getProcessKind().equals(wfManualName());
        }
        return WorkflowUtils.isManual(act.getActivity());
    }

    public static Long getDocumentId(String activity) throws EdmException
    {
        String activityKey = WorkflowFactory.getInstance().keyOfId(activity);
        return TaskSnapshot.getInstance().findByActivityKey(activityKey).getDocumentId();
    }

    public static Long getDocumentId2(String activity) throws EdmException
    {
        String activityKey = WorkflowFactory.getInstance().keyOfId(activity);
        try { //kolejna �ata na spierdolonym mechanizmie internal workflow (patrz )
            return TaskSnapshot.getInstance().findByActivityKey(activityKey).getDocumentId();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return null;
        }
    }

    @Deprecated
    public static void setSourceUser(WfActivity activity, String username)
            throws WorkflowException, EdmException
    {
        WorkflowUtils.findParameter(activity.process_context(),
                "ds_source_user").value().set(username);
    }

    @Deprecated
    public static void setSourceDivision(WfActivity activity, String guid)
            throws WorkflowException, EdmException
    {
        WorkflowUtils.findParameter(activity.process_context(),
                "ds_source_division").value().set(guid);
    }

    /** rzuca wyjatek jesli nie mozna skonczyc pracy nie dodajac uwagi */
    public static void checkCanAssign(OfficeDocument document,
            WorkflowActivity activity) throws EdmException
    {
        WorkflowUtils.checkCanAssign(document, activity);
    }

    // metody InternalWorkflowService
    public static boolean documentHasTasks(long documentId) throws EdmException
    {
        if (Configuration.additionAvailable(Configuration.ADDITION_INTERNAL))
        {
            return InternalWorkflowService.documentHasTasks(documentId,
                    WorkflowUtils.iwfPackageName(), WorkflowUtils.iwfManualName());
        } else if (Configuration.additionAvailable(Configuration.ADDITION_JBPM))
        {
            return JBPMFactory.getInstance().getTasksNumber(documentId) > 0;
        }
        return false;
    }

    // pochodzi z WorkflowTabAction
    public static boolean addDeadLine(String activityId, String deadineAuthor,
            Date deadlineTime, String deadlineRemark) throws EdmException
    {
        if (DSApi.context().hasPermission(
                DSPermission.PROCES_WYZNACZENIE_TERMINU)
                && deadlineTime != null && deadlineRemark != null)
        {

            WorkflowActivity activity = getWfActivity(activityId);

            if (jbpm)
            {
                JBPMFactory.getInstance().addDeadline(activity.getTaskId(),
                        deadineAuthor, deadlineTime, deadlineRemark);
            }

            WfActivity act = activity.getActivity();
            WfProcess wfp = act.container();
            ((WfProcessImpl) wfp).setDeadline(deadlineTime, deadlineRemark);
            DSApi.context().watch(URN.create(wfp));
            return true;
        } else
        {
            return false;
        }
    }

    /**
     * ta metoda konweruje opis dekretacji z podsumowania na opis z zakladki
     * dekretacja obecnie nie da sie z podsumowania dekretowac na dzial z
     * klonowaniem, wiec zwracany opis na dzial jest typu 'kto pierwszy'
     */
    public static String getPreparedAssignment(String summaryAssignment,
            String kind, String objective)
    {
        String[] tmp = summaryAssignment.split(";");
        String username = tmp[0];
        String guid = tmp[1];
        String result;
        if (username.equals("null"))
        {
            result = guid + "//" + kind + "/" + SCOPE_ONEUSER + "/" + objective;
        } else
        {
            result = guid + "/" + username + "/" + kind + "//" + objective;
        }
        return result;
    }

    /**
     * Czy dekretacja jest dopusczalna - nie zawsze jest dopuszczalna dekretacja do wiadomosci
     * @param activity
     * @return
     * @throws WorkflowException
     */
    public boolean allowAssignment(WorkflowActivity activity) throws WorkflowException
    {
        if (activity == null)
        {
            return false;
        }
        return (isManual(activity) || AvailabilityManager.isAvailable("workflow.assignCC"));
    }

    /**
     * Lista uzytkownikow na ktorych zostanie wykonana dekretacja
     * @param documentId
     * @return
     * @throws EdmException
     */
    public List<String> getAssignedUsers(Long documentId) throws EdmException
    {
        List<String> result = new LinkedList<String>();
        if (!jbpm)
        {
            WfAssignment[] assigns = DSApi.context().getAllWfAssignments(documentId);
            for (WfAssignment assgn : assigns)
            {
                if (assgn.get_accepted_status())
                {
                    result.add(assgn.assignee().resource_name());
                }
            }
        }
        return result;
    }

    /**
     * Rejestracja w historii dekretacji i w komunikacie dla akcji
     * @param event
     * @param docId
     * @param username
     * @param guid
     * @param taskId
     */
    public void registerAssignmentEvent(ActionEvent event, Long docId,
            String username, String guid, String taskId)
    {
        if (log.isTraceEnabled())
        {
            log.trace("registerAssignmentEvent");
            log.trace("docId={}", docId);
            log.trace("username={}", username);
            log.trace("guid={}", guid);
            log.trace("taskId={}", taskId);
        } else if (log.isDebugEnabled())
        {
            log.debug("registerAssignmentEvent");
            log.debug("docId={}", docId);
            log.debug("username={}", username);
            log.debug("guid={}", guid);
        }
        if (event == null)
        {
            return;
        }
        try
        {
            OfficeDocument doc = (OfficeDocument) Document.find(docId);
            DSDivision div = DSDivision.find(guid);
            WorkflowActivity wa = WorkflowFactory.getInstance().getWfActivity(taskId);
            if (username == null)
            {

                doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                        DSApi.context().getPrincipalName(),
                        wa.getLastDivision(),
                        null,
                        guid,
                        null,
                        null,
                        wa.getObjective(),
                        isManual(wa) ? smG.getString("doRealizacji")
                        : smG.getString("doWiadomosci"),
                        false,
                        isManual(wa) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION
                        : AssignmentHistoryEntry.PROCESS_TYPE_CC));

                event.addActionMessage(((doc.getType() == DocumentType.ORDER) ? smG.getString("Polecenie")
                        : smG.getString("Pismo"))
                        + " "
                        + ((doc.getOfficeNumber() != null ? doc.getOfficeNumber()
                        + " " : " ")
                        + smG.getString("zadekretowanoNaDzial")
                        + " " + div.getName())
                        + " "
                        + (isManual(wa) ? smG.getString("doRealizacji")
                        : smG.getString("doWiadomosci")));

            } else
            {
                DSUser user = UserFactory.getInstance().findByUsername(username);
                DSUser substitute = isManual(wa) && user != null ? user.getSubstituteUser() : null;

                event.addActionMessage(((doc.getType() == DocumentType.ORDER) ? smG.getString("Polecenie")
                        : smG.getString("Pismo"))
                        + " "
                        + (doc.getOfficeNumber() != null ? doc.getOfficeNumber()
                        + " " : " ")
                        + smG.getString("zadekretowanoNaUzytkownika")
                        + " "
                        + user.asFirstnameLastname()
                        + " ("
                        + div.getName() + ") "
                        + (isManual(wa) ? smG.getString("doRealizacji")
                        : smG.getString("doWiadomosci")));

                doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                        DSApi.context().getPrincipalName(),
                        wa.getLastDivision(),
                        username,
                        guid,
                        substitute != null ? substitute.getName()
                        : null,
                        (substitute != null && substitute.getDivisions().length > 0) ? substitute.getDivisions()[0].getGuid()
                        : null,
                        wa.getObjective(),
                        isManual(wa) ? smG.getString("doRealizacji")
                        : smG.getString("doWiadomosci"),
                        false,
                        isManual(wa) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION
                        : AssignmentHistoryEntry.PROCESS_TYPE_CC));
            }
        } catch (Exception e)
        {
            log.error("exception while registering event", e);
            return;
        }
    }

    public void assignToCoordinator(String activityId, ActionEvent event) throws EdmException
    {

        WorkflowActivity activity = WorkflowFactory.getWfActivity(activityId);

        if (!getInstance().isManual(activity))
        {
            throw new EdmException("Dokument id: " + activity.getDocumentId() + " " + smG.getString("WymaganyJestProcesDekretacjiRecznej."));
        }

        OfficeDocument document = OfficeDocument.findOfficeDocument(activity.getDocumentId());

        if (document == null || document.getDocumentKind() == null)
        {
            throw new EdmException("Dokument id: " + activity.getDocumentId() + " " + smG.getString("NieMoznaUstalicKoordynatora."));
        }

        ProcessCoordinator pc = document.getDocumentKind().logic().getProcessCoordinator(document);
        if (pc == null)
        {
            throw new EdmException("Dokument id: " + activity.getDocumentId() + " " + smG.getString("NieMoznaUstalicKoordynatora."));
        }
        //WorkflowFactory.simpleAssignment(activityId, guidTo, userTo, DSApi.context().getPrincipalName(), null, WorkflowFactory.SCOPE_DIVISION, null, event);

        //event.addActionMessage("pc.getGuid() "+pc.getGuid());
        //event.addActionMessage("pc.getUsername() "+pc.getUsername());

        LoggerFactory.getLogger("tomekl").debug("pc.getGuid() " + pc.getGuid());
        LoggerFactory.getLogger("tomekl").debug("pc.getUsername() " + pc.getUsername());
        WorkflowFactory.simpleAssignment(activityId, pc.getGuid(), pc.getUsername(), DSApi.context().getPrincipalName(), null,
                pc.getUsername() != null ? WorkflowFactory.SCOPE_DIVISION : WorkflowFactory.SCOPE_ONEUSER, null, event);


        TaskSnapshot.updateByDocumentId(document.getId(), document.getStringType());
    }

    public static void forwardToCoordinator(String activityId, ActionEvent event) throws EdmException
    {
        if (activityId == null)
        {
            throw new EdmException(smG.getString("BrakDokumentu"));
        }
        getInstance().assignToCoordinator(activityId, event);
    }

    /** Przerzuc zadanie(a) do koordynatora.
     *  @param event - jezeli brak to null
     *  @param officeDocument - jezeli nie jest nullem to dlugosc activityIds powinien wynosic 1
     */
    public static void forwardToCoordinator(ActionEvent event, String[] activityIds, OfficeDocument officeDocument) throws EdmException
    {
        if (officeDocument != null && activityIds.length != 1)
        {
            LoggerFactory.getLogger("tomekl").debug("Nie przekazano dokumentu");
            throw new EdmException("Nie przekazano dokumentu");
        }

        try
        {
            for (String activityId : activityIds)
            {
                WorkflowActivity activity = WorkflowFactory.getWfActivity(activityId);

                if (!getInstance().isManual(activity))
                {
                    /*if (event != null)
                    event.addActionError(smG.getString("WymaganyJestProcesDekretacjiRecznej."));*/
                    LoggerFactory.getLogger("tomekl").debug("WymaganyJestProcesDekretacjiRecznej");
                    return;
                }

                OfficeDocument document = officeDocument;

                if (document == null)
                {
                    document = OfficeDocument.findOfficeDocument(activity.getDocumentId());
                }

                if (document == null || document.getDocumentKind() == null)
                {
                    /*if (event != null)
                    {
                    event.addActionError(smG.getString("NieMoznaUstalicKoordynatora"));
                    }*/
                    LoggerFactory.getLogger("tomekl").debug("NieMoznaUstalicKoordynatora");
                    return;
                }

                ProcessCoordinator pc = document.getDocumentKind().logic().getProcessCoordinator(document);
                if (pc == null)
                {
                    /*if (event != null)
                    {
                    event.addActionError(smG.getString("NieMoznaUstalicKoordynatora").concat(officeDocument == null ? "" : " : ".concat(document.getOfficeNumber().toString())));
                    
                    }*/
                    LoggerFactory.getLogger("tomekl").debug("NieMoznaUstalicKoordynatora2");
                    return;
                }

                DSApi.context().begin();

                LoggerFactory.getLogger("tomekl").debug("Dokument " + document.getId() + " dekretacja " + pc.getGuid() + " " + pc.getUsername());

                //WorkflowFactory.simpleAssignment(activityId, pc.getGuid(), pc.getUsername(), null, null, null, null, event);

                WorkflowFactory.simpleAssignment(activityId, pc.getGuid(), pc.getUsername(), null, null, WorkflowFactory.SCOPE_DIVISION, smG.getString("DekrtacjaNaKoordynatora"), event);

                LoggerFactory.getLogger("tomekl").debug("updateSnapshotu");
                TaskSnapshot.updateByDocumentId(document.getId(), document.getStringType());

                DSApi.context().commit();
            }
        } catch (Exception ex)
        {
            LoggerFactory.getLogger("tomekl").debug("wtf", ex);
            /*ex.printStackTrace();*/
            DSApi.context()._rollback();
            /*if (event != null)
            event.getLog().error(ex.getMessage(), ex);*/
        }
    }

    /**
     * Metoda przypisuj�ca zadanie grupie os�b
     * @param activity czynno��
     * @param fromUser u�ytkownik, kt�ry przypisuje zadanie
     * @param candidateUsers kandydaci do zadania
     * @param candidateDivisionGuids grupy kandydat�w do zadania
     * @throws EdmException
     */
    public abstract void reassign(String activity, String fromUser,
                                  Set<String> candidateUsers,
                                  Set<String> candidateDivisionGuids)
            throws EdmException;

    public abstract String getProcessName(String activityId) throws EdmException;
    
    public abstract void reloadParticipantMapping() throws EdmException;
    
}
