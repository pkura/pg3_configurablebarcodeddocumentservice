package pl.compan.docusafe.core.office.workflow;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.archive.repository.search.SearchDockindDocumentsAction;

import com.google.common.collect.Lists;

@XmlRootElement(name = "table")
@XmlAccessorType(XmlAccessType.FIELD)
public class TaskSnapshotXml {

	static StringManager sm = GlobalPreferences.loadPropertiesFile(
			SearchDockindDocumentsAction.class.getPackage().getName(), null);

	TaskSnapshotXml() {
	}

	@XmlElement(name = "row")
	private List<TaskSnapshotXmlDto> rows = Lists.newArrayList();

	public TaskSnapshotXml(List<Map<String, String>> map) {
		super();
		transform(map);
	}

	public TaskSnapshotXml(List<Map<String, Object>> map, List<String> list,
			Map<String, String> columns) {
		super();
		transform(map, list, columns);
	}

	private void transform(List<Map<String, Object>> map, List<String> list,
			Map<String, String> columns) {

		TaskSnapshotXmlDto xmlDto = new TaskSnapshotXmlDto();
		for (String key : list) {

			String title = columns.get(key);
			title = title == null ? key : title;
			xmlDto.add(title);
		}
		rows.add(xmlDto);

		for (Map<String, Object> s : map) {

			xmlDto = new TaskSnapshotXmlDto();

			for (String str : list) {

				if (s.get(str) != null) {

					xmlDto.add(sm.getString(s.get(str).toString()));
				} else {
					xmlDto.add("");
				}
			}
			rows.add(xmlDto);
		}

	}

	/**
	 * Konwertuje map� na liste obiekt�w do wyswietlenia w poprawnym formacie
	 * xml
	 * 
	 * @param map
	 */
	private void transform(List<Map<String, String>> map) {
		for (Map<String, String> s : map) {

			TaskSnapshotXmlDto xmlDto = new TaskSnapshotXmlDto();
			for (Map.Entry<String, String> entry : s.entrySet()) {

				String value = entry.getValue();

				if (value != null) {
					xmlDto.add(value);
				} else {
					xmlDto.add("");
				}
			}
			rows.add(xmlDto);
		}
	}

	public List<TaskSnapshotXmlDto> getRows() {
		return rows;
	}

	public static class TaskSnapshotXmlDto {

		List<String> value = Lists.newArrayList();

		public List<String> getValues() {
			return value;
		}

		public void setValues(List<String> values) {
			this.value = value;
		}

		private void add(String v) {
			value.add(v);
		}
	}

}
