package pl.compan.docusafe.core.office.workflow;

import java.util.ArrayList;
import java.util.List;

public class AssigmentsBean 
{
	private List<AssigmentParam> assigments;
	private List<String> errors;
	private List<String> messages;
	private WfActivity wfActivity;
	private Long docId;
	
	public AssigmentsBean()
	{
		this.assigments = new ArrayList<AssigmentParam>();
		this.errors = new ArrayList<String>();
		this.messages = new ArrayList<String>(); 
	}

	public List<AssigmentParam> getAssigments() {
		return assigments;
	}

	public void setAssigments(List<AssigmentParam> assigments) {
		this.assigments = assigments;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public WfActivity getWfActivity() {
		return wfActivity;
	}

	public void setWfActivity(WfActivity wfActivity) {
		this.wfActivity = wfActivity;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}
}


