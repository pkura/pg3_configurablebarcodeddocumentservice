package pl.compan.docusafe.core.office.workflow.jbpm;

import java.util.Map;
import java.util.LinkedHashMap;
/* User: Administrator, Date: 2006-12-15 14:30:41 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id: Constants.java,v 1.17 2009/03/22 21:45:46 wkuty Exp $
 */
public class Constants
{
    public static final String Package = Constants.class.getPackage().getName();

    // sposoby dekretacji
    /**
     * dekretacja na grup� - zadanie wykona ten z grupy kto pierwszy kliknie na to zadanie
     */
    public static final Integer ASSIGNMENT_GROUP = 1;
    /**
     * dekretacja na u�ytkownika naprzemiennie - system sam przydziela do zada� u�ytkownik�w z dzia�u sekwencyjnie
     */
    public static final Integer ASSIGNMENT_USER = 2;

    // zmienne procesu
    public static final String VARIABLE_DOCUMENT_ID = "dsDocumentId";
    public static final String VARIABLE_DOCUMENT_TYPE = "dsDocumentType";
    public static final String VARIABLE_ACTIVITY_CODE = "dsActivityCode";
    public static final String VARIABLE_ACTIVITY_TITLE = "dsActivityTitle";
    public static final String VARIABLE_NEXT_ACTOR = "dsNextActor";    
    public static final String VARIABLE_DOCUMENT_IS_NOT_FORM = "dsDocumentIsNotForm";
    public static final String VARIABLE_DOCUMENT_HAS_POSTSCRIPTS = "dsDocumentHasPostscripts";
    public static final String VARIABLE_DOCUMENT_STATUS = "dsDocumentStatus";
    public static final String VARIABLE_STATUS_REMINDER = "dsStatusReminder";
    public static final String VARIABLE_CURRENT_PAGE = "dsCurrentPage";
    public static final String VARIABLE_DECISION_REASON = "dsDecisionReason";
    public static final String VARIABLE_AUTOMATIC_REMINDER = "dsAutomaticReminder";
    
    /**
     * je�li ustawione na Boolean.TRUE to znaczy ze proces zostal przerwany (zapewne poprzez przycisk 
     * "Przejd� do obiegu r�cznego")
     */
    public static final String VARIABLE_PROCESS_CANCELLED = "dsProcessCancelled";
    
    // zmienne lokalne (wewnatrz zadania)
    public static final String VARIABLE_TASK_ACCEPTED = "dsTaskAccepted";
    public static final String VARIABLE_WYKONANO_KONSULTACJE = "dsWykonanoKonsultacje";
    /** "Dekretujacy" na li�cie zadan */
    public static final String VARIABLE_TASK_SENDER = "dsTaskSender";
    /** Zbi�r flag oznaczaj�cych dodatkowe statusy - obecnie mo�e to by� tylko {@link TaskSnapshot#STATUS_NOWY_DOKUMENT_Z_NR_POLISY} */
    public static final String VARIABLE_ADDITION_STATUS = "dsAdditionStatus";
    /** warto�� logiczna - okre�la, czy bie��ce zadanie zosta�o przekazane od kogo� innego */
    public static final String VARIABLE_TASK_FORWARDED = "dsTaskForwarded";


    public static Map<Integer,String> getAssignmentTypes()
    {
        Map<Integer,String> assignmentTypes = new LinkedHashMap<Integer,String>();
        assignmentTypes.put(Constants.ASSIGNMENT_GROUP, "na grup�");
        assignmentTypes.put(Constants.ASSIGNMENT_USER, "na u�ytkownik�w naprzemiennie");
        return assignmentTypes;
    }
    
 //   public static final Long STATUS_PRZED_ZAWIESZENIEM = new Long(10);
}
