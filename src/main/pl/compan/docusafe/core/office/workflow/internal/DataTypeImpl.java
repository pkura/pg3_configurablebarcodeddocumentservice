package pl.compan.docusafe.core.office.workflow.internal;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.xpdl.BasicDataType;
import pl.compan.docusafe.core.office.workflow.xpdl.DataType;
import pl.compan.docusafe.core.office.workflow.xpdl.EnumerationDataType;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa zarz�dzana przez Hibernate.
 *
 * Typ parametru (element DataType) w pliku XPDL. Mo�liwe typy w pliku
 * XPDL to BasicType, DeclaredType, SchemaType, ExternalReference, RecordType,
 * UnionType, EnumerationType, ArrayType, ListType.
 *
 * <BasicType Type="INTEGER"/>
 *
 * <EnumerationType>
 *  <EnumerationValue Name="OK"/>
 * </EnumerationType>
 *
 * <ArrayType LowerIndex="2" UpperIndex="10">
 *  <BasicType Type="STRING"/>
 * </ArrayType>
 *
 * <ListType>
 *  <BasicType Type="STRING"/>
 * </ListType>
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DataTypeImpl.java,v 1.2 2006/02/06 16:05:49 lk Exp $
 */
public class DataTypeImpl
{
    private Long id;
    /**
     * Nazwa typu odpowiadaj�ca nazwie elementu w pliku XPDL. Mo�liwymi
     * warto�ciami s� sta�e NAME zdefiniowane w klasach odpowiadaj�cych
     * typom XPDL, np. {@link pl.compan.docusafe.core.office.workflow.xpdl.BasicDataType#NAME},
     * {@link pl.compan.docusafe.core.office.workflow.xpdl.EnumerationDataType#NAME}.
     */
    private String typeName; // "BasicType", "EnumerationType" etc.
    /**
     * Nazwa typu pobrana z atrybutu Type elementu BasicType.
     * Warto�ciami mog� by� sta�e zdefiniowane w
     * {@link pl.compan.docusafe.core.office.workflow.xpdl.BasicDataType}.
     */
    private String basicType; // "INTEGER", "BOOLEAN" etc.
    private Integer lowerIndex; // atrybut LowerIndex w ArrayType
    private Integer upperIndex; // atrybut UpperIndex w ListType
    private DataTypeImpl memberType; // typ element�w ListType i ArrayType
    private List<String> enumerationValues; // warto�ci w EnumerationType (String)
    //private List members; // elementy RecordType i UnionType (DataType)


    /**
     * Zwraca definicj� typu XPDL odpowiadaj�c� temu obiektowi.
     * @throws EdmException Je�eli bie��cy obiekt reprezentuje
     * typ, kt�ry nie jest obs�ugiwany przez bie��c� implementacj� XPDL.
     */
    public DataType getXpdlDataType() throws EdmException
    {
        if (BasicDataType.NAME.equals(typeName))
            return DataType.createBasicDataType(basicType);
        else if (EnumerationDataType.NAME.equals(typeName))
            return DataType.createEnumerationDataType(enumerationValues);
        else
            throw new EdmException("Nieznany typ: "+toString());
    }


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getBasicType()
    {
        return basicType;
    }

    public void setBasicType(String basicType)
    {
        this.basicType = basicType;
    }

    public String getTypeName()
    {
        return typeName;
    }

    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }

    public Integer getLowerIndex()
    {
        return lowerIndex;
    }

    public void setLowerIndex(Integer lowerIndex)
    {
        this.lowerIndex = lowerIndex;
    }

    public Integer getUpperIndex()
    {
        return upperIndex;
    }

    public void setUpperIndex(Integer upperIndex)
    {
        this.upperIndex = upperIndex;
    }

    public DataTypeImpl getMemberType()
    {
        return memberType;
    }

    public void setMemberType(DataTypeImpl memberType)
    {
        this.memberType = memberType;
    }

    public List<String> getEnumerationValues()
    {
        return enumerationValues;
    }

    public void setEnumerationValues(List<String> enumerationValues)
    {
        this.enumerationValues = new ArrayList<String>(enumerationValues);
    }

    public String toString()
    {
        return getClass().getName()+"[id="+id+" typeName="+typeName+
            " basicType="+basicType+"]";
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof DataTypeImpl)) return false;

        final DataTypeImpl dataType = (DataTypeImpl) o;

        if (basicType != null ? !basicType.equals(dataType.basicType) : dataType.basicType != null) return false;
        if (enumerationValues != null ? !enumerationValues.equals(dataType.enumerationValues) : dataType.enumerationValues != null) return false;
        if (lowerIndex != null ? !lowerIndex.equals(dataType.lowerIndex) : dataType.lowerIndex != null) return false;
        if (memberType != null ? !memberType.equals(dataType.memberType) : dataType.memberType != null) return false;
        if (typeName != null ? !typeName.equals(dataType.typeName) : dataType.typeName != null) return false;
        if (upperIndex != null ? !upperIndex.equals(dataType.upperIndex) : dataType.upperIndex != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (typeName != null ? typeName.hashCode() : 0);
        result = 29 * result + (basicType != null ? basicType.hashCode() : 0);
        result = 29 * result + (lowerIndex != null ? lowerIndex.hashCode() : 0);
        result = 29 * result + (upperIndex != null ? upperIndex.hashCode() : 0);
        result = 29 * result + (memberType != null ? memberType.hashCode() : 0);
        result = 29 * result + (enumerationValues != null ? enumerationValues.hashCode() : 0);
        return result;
    }
}
