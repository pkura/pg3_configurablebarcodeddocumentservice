package pl.compan.docusafe.core.office.workflow.snapshot;

import java.util.Date;

/**Kontener zawierajacy informacje o liscie zadan uzytkownika */
public class UserCounter
{
	private String username;
	private Integer newTask;
	private Integer oldTask;
	private Integer oldAcceptedTask;
	private Integer oldNotAcceptedTask;
	private Integer newAcceptedTask;
	private Integer newNotAcceptedTask;
	private Integer allTask;
	private boolean mustReload = true;
	private Date lastReload;
	private Date lastSent;

	public Date getLastSent() {
		return lastSent;
	}
	public void setLastSent(Date lastSent) {
		this.lastSent = lastSent;
	}
	public Date getLastReload() {
		return lastReload;
	}
	public void setLastReload(Date lastReload) {
		this.lastReload = lastReload;
	}
	public boolean isMustReload() {
		return mustReload;
	}
	public void setMustReload(boolean mustReload) {
		this.mustReload = mustReload;
	}
	public Integer getAllTask() {
		return allTask;
	}
	public void setAllTask(Integer allTask) {
		this.allTask = allTask;
	}
	public Integer getNewTask() {
		return newTask;
	}
	public void setNewTask(Integer newTask) {
		this.newTask = newTask;
	}
	public Integer getOldTask() {
		return oldTask;
	}
	public void setOldTask(Integer oldTask) {
		this.oldTask = oldTask;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getNewAcceptedTask() {
		return newAcceptedTask;
	}
	public void setNewAcceptedTask(Integer newAcceptedTask) {
		this.newAcceptedTask = newAcceptedTask;
	}
	public Integer getOldAcceptedTask() {
		return oldAcceptedTask;
	}
	public void setOldAcceptedTask(Integer oldAcceptedTask) {
		this.oldAcceptedTask = oldAcceptedTask;
	}
	public Integer getNewNotAcceptedTask() {
		return newNotAcceptedTask;
	}
	public void setNewNotAcceptedTask(Integer newNotAcceptedTask) {
		this.newNotAcceptedTask = newNotAcceptedTask;
	}
	public Integer getOldNotAcceptedTask() {
		return oldNotAcceptedTask;
	}
	public void setOldNotAcceptedTask(Integer oldNotAcceptedTask) {
		this.oldNotAcceptedTask = oldNotAcceptedTask;
	}
}
