package pl.compan.docusafe.core.office.workflow.xpdl;

import java.util.List;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DataType.java,v 1.3 2007/11/27 12:11:38 mariuszk Exp $
 */
public abstract class DataType
{
    public static BasicDataType createBasicDataType(String type)
        throws EdmException
    {
        return new BasicDataType(type);
    }

    public static EnumerationDataType createEnumerationDataType(List<String> members)
        throws EdmException
    {
        return new EnumerationDataType(members);
    }

    /**
     * Tworzy konkretn� instancj� klasy dziedzicz�cej po DataType
     * na podstawie przekazanego elementu DOM.
     * <p>
     * Obecnie wspierany jest jedynie podtyp BasicType
     */
    static DataType createDataType(Element dataType) throws EdmException
    {
        if (dataType.element("BasicType") != null)
        {
            return new BasicDataType(dataType.element("BasicType"));
        }
        else if (dataType.element("EnumerationType") != null)
        {
            return new EnumerationDataType(dataType.element("EnumerationType"));
        }
        else
        {
            throw new EdmException("Wspierane s� tylko parametry " +
                "typu BasicType");
        }
    }

    /**
     * Zwraca obiekt utworzony na podstawie reprezentacji napisowej.
     * Typ obiektu jest determinowany na podstawie atrybut�w instancji
     * DataType, dla kt�rej wywo�ywana jest ta metoda.
     * @throws NullPointerException Je�eli przekazany argument ma warto�� null.
     * @throws IllegalArgumentException Je�eli przekazanej warto�ci nie
     * mo�na zinterpretowa� (w wypadku enumeracji - je�eli argumentu nie ma
     * na li�cie dozwolonych warto�ci).
     */
    public abstract Object getValue(String s);

    /**
     * Zwraca domy�ln� warto�� dla danego typu lub rzuca wyj�tek
     * UnsupportedOperationException, je�eli typ nie ma domy�lnej
     * warto�ci.
     */
    public abstract Object getDefaultValue();

    /**
     * Zwraca nazw� typu odpowiadaj�c� nazwie elementu w pliku XPDL,
     * np. "BasicType".
     */
    public abstract String getTypeName();
}
