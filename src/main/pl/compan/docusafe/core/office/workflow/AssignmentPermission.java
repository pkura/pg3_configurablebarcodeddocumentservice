package pl.compan.docusafe.core.office.workflow;

import java.util.Map;

import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class AssignmentPermission {
	
	private static AssignmentPermission instance = new AssignmentPermission();
	
	public static AssignmentPermission getInstance() {
		return instance;
	}
	
	
	private boolean isManual(String processKind) {
		return WorkflowFactory.wfManualName().equals(processKind);
	}
	
	
	public boolean checkDockindAllows(Document doc, String processKind) throws EdmException {
		if(doc.getDocumentKind() == null) {
			return true;
		}
		doc.getDocumentKind().initialize();
		Map<String, String> properties = doc.getDocumentKind().getProperties();
		
		if(properties.containsKey(DocumentKind.BLOCK_DECRETATION) && 
		    properties.get(DocumentKind.BLOCK_DECRETATION).contains(processKind))
		{
			 return false;
		}
		return true;
	}
	
	/** to nie tylko uwaga ale tez jakies uprawnienia */
	private String checkRemark(Document doc, WorkflowActivity act) {
		if (!(doc instanceof OfficeDocument)) {
			return null;
		}
		
		try {
			WorkflowFactory.checkCanAssign((OfficeDocument)doc, act);
		} catch (Exception e) {
			return e.getMessage();
		}
		return null;
	}
	
	private boolean checkIsArchiveOnly(String username)
	{
		if(username == null || username.length() < 1)
			return false;
		
		try
		{
			if(DSUser.findByUsername(username).getRoles().contains("archive_only"))
				return true;
		}
		catch (Exception e) 
		{
			//tu exception w sumie nas nie obchodzi
			//LoggerFactory.getLogger("tomekl").debug("wtf",e);
		}
		return false;
	}
	
	public boolean checkCanAssign(String activityId, String preparedAssignment, ActionEvent event) throws EdmException {
		String processKind = preparedAssignment.split("/")[2];
		
		WorkflowActivity activity = WorkflowFactory.getWfActivity(activityId);
		
		Long docId = activity.getDocumentId();
		Document doc = Document.find(docId);
		if (!(WorkflowFactory.getInstance().isManual(activity) || AvailabilityManager.isAvailable("workflow.assignCC"))) {
			event.addActionError("Zadanie nie jest w obiegu r�cznym");
			return false;
		}
		
		String errorMessage = checkRemark(doc, activity); 
		if (errorMessage != null) {
			event.addActionError(errorMessage);
			return false;
		}
		
		if (!checkDockindAllows(doc, processKind)) {
			event.addActionError("Dokument "+doc.getId()+ " blokada dekretacji "+processKind);
			return false;
		}

		if(checkIsArchiveOnly(preparedAssignment.split("/")[1]))
		{
			event.addActionError("Dokument "+doc.getId()+ " blokada dekretacji na uzytkownika archive_only");
			return false;
		}
		
		return true;
	}
}
