package pl.compan.docusafe.core.office.workflow.internal;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.AnyObjectKind;
import pl.compan.docusafe.core.office.workflow.xpdl.BasicDataType;
import pl.compan.docusafe.core.office.workflow.xpdl.EnumerationDataType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AnyObjectKindImpl.java,v 1.4 2010/06/29 06:54:37 mariuszk Exp $
 */
public class AnyObjectKindImpl implements AnyObjectKind
{
    private int type;
    private List<String> members;

/*
    AnyObjectKindImpl(int type, List members) throws EdmException
    {
        this.type = type;
        this.members = members;

        if (type != ENUM && members != null)
            throw new EdmException("Tylko typ enumeracyjny mo�e mie� list� sk�adnik�w.");

        if (type == ENUM && members == null)
            throw new EdmException("Typ enumeracyjny nie dosta� listy sk�adnik�w.");
    }
*/

/*
    AnyObjectKindImpl(int type) throws EdmException
    {
        this(type, null);
    }
*/

    /**
     * Tworzy obiekt na podstawie obiektu DataTypeImpl, kt�ry z kolei
     * tworzony jest na podstawie definicji w pliku XPDL.
     */
    AnyObjectKindImpl(DataTypeImpl dataType)
    {
        if (BasicDataType.NAME.equals(dataType.getTypeName()))
        {
            if (BasicDataType.BOOLEAN.equals(dataType.getBasicType()))
                this.type = BOOLEAN;
            else if (BasicDataType.DATETIME.equals(dataType.getBasicType()))
                throw new IllegalArgumentException("Typ DATETIME nie jest obs�ugiwany.");
            else if (BasicDataType.FLOAT.equals(dataType.getBasicType()))
                this.type = DOUBLE;
            else if (BasicDataType.INTEGER.equals(dataType.getBasicType()))
                this.type = LONG;
            else if (BasicDataType.STRING.equals(dataType.getBasicType()))
                this.type = STRING;
            else
                throw new IllegalArgumentException("Nieznany typ BasicType: "+dataType.getBasicType());
        }
        else if (EnumerationDataType.NAME.equals(dataType.getTypeName()))
        {
            this.type = ENUM;
            this.members = new ArrayList<String>(dataType.getEnumerationValues());
        }
    }

    AnyObjectKindImpl(pl.compan.docusafe.core.office.workflow.xpdl.DataType dataType)
    {
        if (dataType instanceof BasicDataType)
        {
            BasicDataType basicDataType = (BasicDataType) dataType;
            if (BasicDataType.BOOLEAN.equals(basicDataType.getType()))
                this.type = BOOLEAN;
            else if (BasicDataType.DATETIME.equals(basicDataType.getType()))
                throw new IllegalArgumentException("Typ DATETIME nie jest obs�ugiwany.");
            else if (BasicDataType.FLOAT.equals(basicDataType.getType()))
                this.type = DOUBLE;
            else if (BasicDataType.INTEGER.equals(basicDataType.getType()))
                this.type = LONG;
            else if (BasicDataType.STRING.equals(basicDataType.getType()))
                this.type = STRING;
            else
                throw new IllegalArgumentException("Nieznany typ BasicType: "+basicDataType.getType());
        }
        else
        {
            throw new IllegalArgumentException(dataType.toString());
        }
/*
        else if (dataType instanceof EnumerationDataType)
        {
            this.type = ENUM;
            this.members = new ArrayList(((EnumerationDataType) dataType).)
        }
*/
    }

    public int type()
    {
        return type;
    }

    public int memberCount() throws EdmException
    {
        if (type != ENUM)
            throw new EdmException("List� sk�adnik�w mo�na odczyta� tylko dla typu enumeracyjnego.");
        if (members == null)
            throw new EdmException("Brak listy sk�adnik�w typu.");

        return members.size();
    }

    public String memberName(int pos) throws EdmException
    {
        if (type != ENUM)
            throw new EdmException("List� sk�adnik�w mo�na odczyta� tylko dla typu enumeracyjnego.");
        if (members == null)
            throw new EdmException("Brak listy sk�adnik�w typu.");

        try
        {
            return (String) members.get(pos);
        }
        catch (IndexOutOfBoundsException e)
        {
            throw new EdmException("Pr�ba odczytania sk�adnika spoza dozwolonego zakresu: "+pos);
        }
    }

    boolean typeCompatible(Object value) throws EdmException
    {
        if (value == null)
            throw new NullPointerException("value " +this.toString());

        if (type == STRING && (value instanceof String)) return true;
        if (type == BOOLEAN && (value instanceof Boolean)) return true;
        if (type == LONG && (value instanceof Long)) return true;
        if (type == DOUBLE && (value instanceof Double)) return true;
        if (type == ENUM)
        {
            if (!(value instanceof String)) return false;
            for (int i=0, n=memberCount(); i < n; i++)
            {
                if (value.equals(memberName(i))) return true;
            }
        }
        return false;
    }

    public String toString()
    {
        return getClass().getName()+"[type="+type+" members="+members+"]";
    }
}
