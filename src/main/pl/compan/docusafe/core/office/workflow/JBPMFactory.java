package pl.compan.docusafe.core.office.workflow;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.jbpm.JbpmContext;
import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.taskmgmt.exe.PooledActor;
import org.jbpm.taskmgmt.exe.TaskInstance;
import com.asprise.util.tiff.t;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.internal.WfProcessImpl;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
/**
 * ta klasa nz
 * @author �wiat�y
 *
 */
public class JBPMFactory 
{
	
	public static Logger log = LoggerFactory.getLogger(JBPMFactory.class);
	
	private final static String MANUAL_PROCESS_NAME = "DSProcess";
	private final static String CC_PROCESS_NAME = "DSProcessCC";
	protected final static String PROCESS_KEY = "processKey";
	
	private final static String TRANSITION_ASSIGN = "assign";
	private final static String TRANSITION_FINISH = "finish";
	
	protected final static String DEADLINE_TIME = "deadlineTime";
	protected final static String DEADLINE_AUTHOR = "deadlineAuthor";
	protected final static String DEADLINE_REMARK = "deadlineRemark";
	
	public final static String DOC_ID = "docId";
	protected final static String CC_USER_FROM = "userFrom";
	
	/**klucz odpowiedniego procesu w wewnetrznym workflow,
	 * idea by�a taka, by mie� mo�liwo�� prowadzenia zada�
	 * w obu systemach workflow jednocze�nie, ale pomys� upad�
	 * z powodu rozbie�no�ci w tych systemach i trudno�ci w 
	 * utrzymaniu synchronizacji */
	@Deprecated
	protected final static String INTERNAL_PROCESS_KEY = "internalProcessKey";
	
	private final static JBPMFactory instance = new JBPMFactory();
	
	private static StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
	
	public static JBPMFactory getInstance() {
		return instance;
	}
	
	/** mapowanie nazw proces�w jbpm na nazwy w widoku */
	private String getProcessDesc(String processName) {
		if (processName.equals(MANUAL_PROCESS_NAME)) {
			return smG.getString("task.list.Zad.wDekr.recznej");
		} else if (processName.equals(CC_PROCESS_NAME)) {
			return smG.getString("task.list.DoWiadomosci");
		} else {
			return "NIEZNANY PROCESS DESC";
		}
	}
	
	/** mapowanie nazw proces�w jbpm na identyfikatory obowi�zuj�ce w ds */
	private String getProcessKind(String processName) {
		if (processName.equals(MANUAL_PROCESS_NAME)) {
			return WorkflowFactory.wfManualName();
		} else if (processName.equals(CC_PROCESS_NAME)) {
			return WorkflowFactory.wfCcName();
		} else {
			return "NIEZNANY PROCESS KIND";
		}
	}
	
	
	/*
	 * SZUKANIE ZADA�, PROCES�W
	 */
	
	
	/**
	 * wyszukiwanie identyfikator�w instancji PROCES�W, 
	 * kt�re maj� zmienn� o nazwie 'name' i warto�ci 'value'
	 * typ zmiennej jest brany z obiektu value, zaimplementowana obs�uga dla Long i String 
	 */
	private Long[] findProcessInstancesByVariable(String name, Object value) throws EdmException 
	{
		if (log.isTraceEnabled())
		{
			log.trace("findProcessInstancesByVariable");
			log.trace("name={}",name);
			log.trace("value={}",value);
		}
		PreparedStatement ps = null;
    	Set<Long> processIds;
    	JbpmContext ctx = null;
    	try {
    		ctx = DSjBPM.getContext();
	    	if (value instanceof Long) 
	    	{
	    		ps = ctx.getConnection().prepareStatement("select PROCESSINSTANCE_ from JBPM_VARIABLEINSTANCE where NAME_ = ? and LONGVALUE_ = ?" );
	    			ps.setLong(2, (Long)value);
			}
	    	else if(value instanceof String) 
	    	{
	    		ps = ctx.getConnection().prepareStatement("select PROCESSINSTANCE_ from JBPM_VARIABLEINSTANCE where NAME_ = ? and STRINGVALUE_ = ?" );
	    		ps.setString(2, (String)value);
			}
	    	else 
	    	{
				throw new EdmException("unknown type of jbpm variable");
			}
    		ps.setString(1, name);
    		ResultSet rs = ps.executeQuery();
    		processIds = new HashSet<Long>();
    		while (rs.next())
    		{
    			processIds.add(rs.getLong(1));
    			
    		}
    		rs.close();
    	} catch (SQLException e) 
    	{
    		throw new EdmException("sql exception: " + e.getMessage());
    	} finally {
    		DbUtils.closeQuietly(ps);
    		DSjBPM.closeContextQuietly(ctx);
    	}
    	return processIds.toArray(new Long[processIds.size()]);
	}
	
	
	/** znajduje identyfikator jednej instancji procesu, 
	 * kt�ry ma zmienn� o nazwie 'name' i warto�ci 'value'
	 * lub null je�li takiego nie ma.
	 * typ zmiennej jest wnioskowany z obiektu 'value', zaimplementowana 
	 * obs�uga dla Long i String
	 */
	public Long findFirstProcessInstanceByVariable(String name, Object value) throws EdmException {
		Long[] ids = findProcessInstancesByVariable(name, value);
		if (ids.length == 0) {
			return null;
		} else {
			return ids[0];
		}
	}
	
	/** wyszukiwanie identyfikatora instancji procesu, kt�ry jest powi�zany
	 * z procesem wew workflow o podanym activityId
	 * nieu�ywane, z powodu rezygnacji z synchronizacji system�w workflow */
	@Deprecated
	protected Long findProcessInstanceByActivityId(String activityId) throws WorkflowException, EdmException {
		String activityKey = WorkflowFactory.getInstance().keyOfId(activityId);
		WfActivity act = WorkflowFactory.getInstance().getOldActivity(activityKey);
		Long processId = ((WfProcessImpl)act.container()).getId();
		return findFirstProcessInstanceByVariable(INTERNAL_PROCESS_KEY, String.valueOf(processId));
	}
	
	
	/** znajduje instancj� AKTYWNEGO zadania dla podanej instancji procesu
	 * powinna by� taka dok�adnie jedna, bo proces jest liniowy
	 * rzuca wyj�tek, je�li nie znajdzie, lub znajdzie wi�cej. \
	 * wo�a� z otwartym kontekstem */
	private TaskInstance findTaskByProcessInstance(ProcessInstance pi) throws EdmException {
		JbpmContext ctx = DSjBPM.getContext();
		List<TaskInstance> tis = ctx.getTaskMgmtSession().findTaskInstancesByProcessInstance(pi);
		log.debug("znaleziono " + tis.size() + "otwartych zadan dla procesu " + pi.getId());
		if (tis.size() != 1) 
		{				
			throw new EdmException("Expected one processes active task instance, but  found " + tis.size() + " in process #"+pi.getId());
		}
		return tis.get(0);
	}
	
	/** znajduje identyfikator instancji AKTYWNEGO zadania dla podanego
	 * identyfikatora instancji procesu
	 * rzuca wyj�tek, je�li nie znajdzie, lub znajdzie wi�cej ni� jeden.*/
	protected Long findTaskByProcessId(Long processId) throws EdmException {
		JbpmContext ctx = null;
		try {
			ctx = DSjBPM.getContext();
			ProcessInstance pi = ctx.getProcessInstance(processId);
			return findTaskByProcessInstance(pi).getId();
		} catch (Exception e) { 
			throw new EdmException(e);
		} finally {
			DSjBPM.closeContext(ctx);
		}
	}
	
	/** znajduje identyfikatory instancji proces�w powi�zanych z dokumentem o podanym id 
	 * (aktywne i nieaktywne)*/
	private Long[] findDocumentProcesses(Long docId) throws EdmException {
		return findProcessInstancesByVariable(DOC_ID, docId);
	}
	
	/** ile aktywnych instancji zada� jest powi�zanych z dokumentem o podanym id 
	 * (cc i manual), powinno by� r�wnowa�ne z ilo�ci� aktywnych proces�w*/
	protected int getTasksNumber(Long docId) throws EdmException {
		return findDocumentTasks(docId).length;
	}
	
	/** znajduje idenyfikatory aktywnych instancji zada� dla dokumentu o podanym id */ 
	protected Long[] findDocumentTasks(Long docId) throws EdmException {
		Long[] proceses = findDocumentProcesses(docId);
		Set<Long> result = new HashSet<Long>();
		JbpmContext ctx = null;
		try {
			ctx = DSjBPM.getContext();
			for (Long processId : proceses) {
				ProcessInstance pi = ctx.getProcessInstance(processId);
				//zgodnie z api ta metoda daje tylko aktywne zadania
				List<TaskInstance> tis = ctx.getTaskMgmtSession().findTaskInstancesByProcessInstance(pi);
				for (TaskInstance ti : tis) {
						result.add(ti.getId());
				}
			}
			return result.toArray(new Long[0]);
		} catch (Exception e) { 
			throw new EdmException(e);
		} finally {
			DSjBPM.closeContext(ctx);
		}
	}
	
	/** por�wnywaczka do instancji zada�, wi�ksze jest to, kt�re by�o p�niej zamkni�te */
	private static Comparator<TaskInstance> taskDateComparator = new Comparator<TaskInstance>() {
		public int compare(TaskInstance ti1, TaskInstance ti2) {
			if (ti1.getEnd() == null) {
				return -1;
			} else if (ti2.getEnd() == null) {
				return 1;
			}
			return ti1.getEnd().compareTo(ti2.getEnd());
		}
	};
	
	/** 
	 * (szuka najpozniej zamknietego zadania z tego samego menedzera(procesu) i zwraca jego aktora,
	 * czyli faktycznie uzytkownika dekretujacego
	 * to rozwiazanie nie wprowadza redundancji ale byc moze jest zbyt wolne,
	 * alternatywnie moznaby te informacje trzymac w kontekscie zadania (ale nie procesu)
	 * wo�a� z otwartym kontekstem */
	private String findLastActor(TaskInstance ti) throws EdmException 
	{
		log.trace("findLastActor dla procesu {}",ti.getId());
		Collection<TaskInstance> tis = ti.getTaskMgmtInstance().getTaskInstances();
		TaskInstance[] tisArr = tis.toArray(new TaskInstance[tis.size()]);
		Arrays.sort(tisArr, taskDateComparator);
		String lastActor = tisArr[tisArr.length - 1].getActorId();
		return lastActor;
	}
	
	/** pobiera identyfikator procesu wewn�trznego workflow stowarzyszonego z podanym zadaniem
	 * nieu�ywane - zarzucona synchronizacja system�w workflow*/
	@Deprecated
	protected Long internalProcIdOfTaskId(String taskId) throws EdmException {
		JbpmContext ctx = null;
		Long result = null;
		try {
			Long ti = Long.parseLong(taskId);
			ctx = DSjBPM.getContext();
			result = Long.parseLong((String)ctx.getTaskMgmtSession().getTaskInstance(ti).getVariable(INTERNAL_PROCESS_KEY));
		} catch (Exception e) { 
			throw new EdmException(e);
		} finally {
			DSjBPM.closeContext(ctx);
		}
		return result;
	}
	
	
	/*
	 * TWORZENIE PROCESU
	 */
	
	/** tworzenie procesu manual,
	 * @param toCoordinator czy utzorzyc zadanie u koordynatora, 
	 * FIXME brzydkie rozwiazanie, lepiej wyabstrahowac i dac to wyzej, WorkflowFactory
	 * @param processKey identyfikator odpowiadaj�cego procesu wew workflow, nieuzywane
	 */
	protected Long createNewProcess(
			Long documentId, 
			String username, 
			String guid, 
			String objective, 
			Boolean toCoordinator,
			String processKey) throws EdmException {
		
		JbpmContext ctx = DSjBPM.getContext();
		
		//powstaje proces
		ProcessInstance processInstance = ctx.newProcessInstance(MANUAL_PROCESS_NAME);
		
		Long processInstanceId = processInstance.getId();
		ContextInstance contextInstance = processInstance.getContextInstance();
		contextInstance.setVariable(DOC_ID, documentId);
		contextInstance.setVariable(INTERNAL_PROCESS_KEY, processKey);
		
		//w tym momencie powstanie instancja zadania
		processInstance.signal(TRANSITION_ASSIGN);
		
		Collection tis = processInstance.getTaskMgmtInstance().getTaskInstances();
		if (tis.size() != 1) {
			throw new EdmException("Expected one new process task, but found " + tis.size());
		}
		TaskInstance ti = (TaskInstance)tis.iterator().next();
		
		
		//FIXME wyciaganie danych koordynatora mozna przeniesc do workflowfactory
		if (toCoordinator) {
			OfficeDocument doc = (OfficeDocument) Document.find(documentId);
			ProcessCoordinator pc = doc.getDocumentKind().logic().getProcessCoordinator(doc);
			//idenyfikatory w jbpm nie sa takie same jak w ds!
			if (pc.getUsername() == null) {
				ti.setPooledActors(new String[] {JBPMIds.getJBPMGuid(pc.getGuid())} );
			} else {
				ti.setActorId(JBPMIds.getJBPMUsername(pc.getUsername(), pc.getGuid()));
			}
		} else {
			if (guid != null && username != null) {
				ti.setActorId(JBPMIds.getJBPMUsername(username, guid));
			}
		}
		
		ti.setDescription(objective);
		
		// niech duedate oznacza czas utworzenia procesu
		ti.setDueDate(new Date());
		
		ctx.save(processInstance);
		DSjBPM.closeContext(ctx);
		return processInstanceId;
	}
	
	/** wo�a� z otwartym kontekstem 
	 * username, guid i fromId sa identyfikatorami jbpm 
	 * fromId to identyfikator uzytkownika dekretujacego - 
	 * w procesie recznym nie jest to potrzebne, bo na poczatku zadanie jest od nikogo, 
	 * a potem da sie znalezc aktora z poprzedniego zadania (findLastActor()). 
	 * zadanie cc od poczatku jest od kogos i te informacje trzeba trzymac w kontekscie.*/
	private Long createNewCCProcess(
			Long documentId, 
			String username, 
			String guid,
			String fromId) throws EdmException {
		
		//por. komentarze do tworzenia procesu recznego
		JbpmContext ctx = DSjBPM.getContext();
		ProcessInstance processInstance = ctx.newProcessInstance(CC_PROCESS_NAME);
		Long processInstanceId = processInstance.getId();
		
		ContextInstance contextInstance = processInstance.getContextInstance();
		contextInstance.setVariable(DOC_ID, documentId);
		contextInstance.setVariable(CC_USER_FROM, fromId);
		processInstance.signal(TRANSITION_ASSIGN);
		Collection tis = processInstance.getTaskMgmtInstance().getTaskInstances();
		if (tis.size() != 1) {
			throw new EdmException("Expected one new process task, but found " + tis.size());
		}
		TaskInstance ti = (TaskInstance)tis.iterator().next();
		if (username != null) {
			ti.setActorId(username);
		} else {
			ti.setPooledActors(new String[] {guid});
		}
		ti.setDescription(smG.getString("Potwierdzenie"));
		ti.setDueDate(new Date());
		ctx.save(processInstance);
		return ti.getId();
	}
	
	
	
	/*
	 * DEKRETACJA
	 */
	
	/** dekretacja zada�
	 * @param activityIds tablica identyfikatorow zadan
	 * @param plannedAssignments tablica zlecen dekretacji
	 */
	protected void multiAssignment(String[] activityIds, String[] plannedAssignments, ActionEvent event) throws EdmException 
	{
			
			if (log.isTraceEnabled())
			{
				log.trace("multiAssignment");							
			}
			//podzial zlecen:
			
			//reczna do uzytkownika
			Set<String> assignedUsers = new HashSet<String>();
			//cc do uzytkownika
			Set<String> assignedUsersCc = new HashSet<String>();
			//reczna na dzial w trybie 'kto pierwszy ten lepszy'
			Set<String> assignedDivsFirst = new HashSet<String>();
			//do wiadomosci na dzial 
			Set<String> assignedDivsFirstCc = new HashSet<String>();
			
			//klonowanie zadan - niezaimplementowane
			Set<String> assignedDivsEvery = new HashSet<String>();
			Set<String> assignedDivsEveryCc = new HashSet<String>();
			Set<String> assignedDivsEverySub = new HashSet<String>();
			Set<String> assignedDivsEverySubCc = new HashSet<String>();
			
			
			for (String plannedAssignment : plannedAssignments) 
			{			
				PlannedAssignment pa = new PlannedAssignment(plannedAssignment);
				log.trace("iteracja-->");
				if (StringUtils.isEmpty(pa.getDivisionGuid())) 
				{
					throw new EdmException("user division not specified");
				}
				
				DSDivision.find(pa.getDivisionGuid());
				if (StringUtils.isNotEmpty(pa.getUsername())) 
				{				
					DSUser.findByUsername(pa.getUsername());
					putUser(assignedUsers, assignedUsersCc, pa);
					//log.trace("user={}",pa.getUsername());
				} 
				else 
				{
					if (StringUtils.isEmpty(pa.getScope()) || pa.getScope().equals(WorkflowFactory.SCOPE_ONEUSER)) 
					{
						putDiv(assignedDivsFirst, assignedDivsFirstCc, pa);
					} 
					else if (pa.getScope().equals(WorkflowFactory.SCOPE_DIVISION)) 
					{
						putDiv(assignedDivsEvery, assignedDivsEveryCc, pa);
					} else if (pa.getScope().equals(WorkflowFactory.SCOPE_SUBDIVISIONS)) 
					{
						putDiv(assignedDivsEverySub, assignedDivsEverySubCc, pa);
					}
					//tu musi byc kon
				}
			}
			
			//koniec podzialu zlecen
			
			//czy 'do realizacji' dekretujemy jednemu, konkretnemu uzytkownikowi 
			Boolean assignUser = true;
			
			if (assignedDivsFirst.size() > 0) 
			{
				assignUser = false;
			}
			if (assignedUsers.size() > 1) 
			{
				assignUser = false;
			}
			
			//czy w ogole dekretujemy do realizacji
			Boolean assignManual = true;
			
			if (assignedUsers.size() + assignedDivsFirst.size() == 0) {
				assignManual = false;
			}
			
			for (String taskId : activityIds) 
			{	
				JbpmContext ctx = null;
				try 
				{
					ctx = DSjBPM.getContext();
					TaskInstance task = ctx.getTaskMgmtSession().getTaskInstance(Long.parseLong(taskId));
					ProcessInstance processInstance = task.getProcessInstance();
					ContextInstance contextInstance = processInstance.getContextInstance();
					Long docId = (Long)contextInstance.getVariable(DOC_ID);
					for (String userCC : assignedUsersCc) 
					{
						Long newCCId = createNewCCProcess(docId, userCC, null, task.getActorId());
						WorkflowFactory.getInstance().registerAssignmentEvent(event, docId, JBPMIds.getDSUsername(userCC), JBPMIds.getDSUserGuid(userCC), "internal,"+newCCId);
					}
					for (String divCC : assignedDivsFirstCc) 
					{
						Long newCCId = createNewCCProcess(docId, null, divCC, task.getActorId());
						WorkflowFactory.getInstance().registerAssignmentEvent(event, docId, null, JBPMIds.getDSGuid(divCC), "internal,"+newCCId);
					}
					
					
					if (assignManual) 
					{
						TaskInstance finishedTask = findTaskByProcessInstance(processInstance); 
						
						/*String currActor = assignedUsers.iterator().next();
						if(finishedTask.getActorId() != null && JBPMIds.getDSUsername(currActor).equalsIgnoreCase(JBPMIds.getDSUsername(finishedTask.getActorId())))
						{
							event.addActionError(smG.getString("DekretacjaDoRealizacjiSwojegoZadaniaNaSiebieJestZabroniona"));							//
							//DSjBPM.closeContext(ctx);
							//throw new EdmException(smG.getString("DekretacjaDoRealizacjiSwojegoZadaniaNaSiebieJestZabroniona"));
							return;
						}*/
						
						//konczymy zadanie, powstaje nowa instancja
						finishedTask.end();
						
						//tworzymy nowa instancje, tak jak w przypadku tworzenia procesu
						//nie startujemy zadania, tylko ustawiamy duedate - date otrzymania zadania
						//start nastepuje dopiero w momencie zaakceptowania zadania
						TaskInstance newTask = findTaskByProcessInstance(processInstance);
						newTask.setDescription("Realizacja");
						if (assignUser) 
						{					
							String currActor = assignedUsers.iterator().next();											
							
							//ustawiamy aktora
							newTask.setActorId(currActor);
							newTask.setDueDate(new Date());
							ctx.save(processInstance);
							DSjBPM.closeContext(ctx);
							WorkflowFactory.getInstance().registerAssignmentEvent(event, docId, JBPMIds.getDSUsername(currActor), JBPMIds.getDSUserGuid(currActor), "internal,"+task.getId());
						} 
						else 
						{						
							assignedUsers.addAll(assignedDivsFirst);
							String[] pool = assignedUsers.toArray(new String[assignedUsers.size()]);						
							//ustawiamy pule aktorow
							newTask.setPooledActors(pool);
							newTask.setActorId(null);
							newTask.setDueDate(new Date());
							ctx.save(processInstance);
							DSjBPM.closeContext(ctx);
							for (String id : pool) 
							{							
								if (JBPMIds.isJBPMGuid(id)) 
								{								
									WorkflowFactory.getInstance().registerAssignmentEvent(event, docId, null, JBPMIds.getDSGuid(id),"internal,"+task.getId());
								} else 
								{
									WorkflowFactory.getInstance().registerAssignmentEvent(event, docId, JBPMIds.getDSUsername(id), JBPMIds.getDSUserGuid(id), "internal,"+task.getId());
								}
							}
							
						}
						//konczymy zadanie, powstaje nowa instancja
						//finishedTask.end();
						
					}															
				} 
				catch (Exception e) 
				{
					log.error("jbpm assignment exception: ",e);
					throw new EdmException(e);
				} 										
		}		
	}
	
	/** metoda pomocnicza, dzieli zlecenia na cc i manual i konwertuje identyfikatory */
	private void putUser(Set<String> push, Set<String> cc, PlannedAssignment pa) throws EdmException 
	{
		log.trace("putUser");
		if (StringUtils.isEmpty(pa.getKind())) 
		{
			throw new EdmException("kind not specified");
		}
		
		if (pa.getKind().equals(WorkflowFactory.wfManualName())) 
		{		
			push.add(JBPMIds.getJBPMUsername(pa.getUsername(), pa.getDivisionGuid()));
			log.trace("pushManual username={}, guid={}",pa.getUsername(),pa.getDivisionGuid());
		} 
		else 
		{
			cc.add(JBPMIds.getJBPMUsername(pa.getUsername(), pa.getDivisionGuid()));
			log.trace("pushCC username={}, guid={}",pa.getUsername(),pa.getDivisionGuid());
		}
	}
	
	/** metoda pomocnicza, dzieli zlecenia na cc i manual i konwertuje identyfikatory */
	

	private void putDiv(Set<String> push, Set<String> cc, PlannedAssignment pa) throws EdmException 
	{
		if (log.isTraceEnabled())
		{
			log.trace("putDiv");
			log.trace("id={}",pa.getId());
		}
		if (StringUtils.isEmpty(pa.getKind())) 
		{
			throw new EdmException("kind not specified");
		}
		if (pa.getKind().equals(WorkflowFactory.wfManualName())) 
		{
			push.add(JBPMIds.getJBPMGuid(pa.getDivisionGuid()));
			log.trace("pushManual guid={}",pa.getDivisionGuid());
		} 
		else 
		{
			cc.add(JBPMIds.getJBPMGuid(pa.getDivisionGuid()));
			log.trace("pushCC guid={}",pa.getDivisionGuid());
		}
	}
	
	/** zwrot dekretacji - por. multiAssignment() */
	protected void returnAssignment(WorkflowActivity activity) throws EdmException {
		JbpmContext ctx = null;
		try {
			ctx = DSjBPM.getContext();
			ProcessInstance processInstance = ctx.getTaskMgmtSession().getTaskInstance(Long.parseLong(activity.getTaskId())).getProcessInstance();
			
			TaskInstance finishedTask = findTaskByProcessInstance(processInstance); 
			finishedTask.end();
				
			TaskInstance newTask = findTaskByProcessInstance(processInstance);
					
			String currActor = JBPMIds.getJBPMUsername(activity.getLastUser(),activity.getLastDivision());
			newTask.setDescription(smG.getString("ZwrotDeketacji"));
			newTask.setActorId(currActor);
			newTask.setDueDate(new Date());
			ctx.save(processInstance);			
		} catch (Exception e) { 
			throw new EdmException(e);
		} finally {
			DSjBPM.closeContext(ctx);
		}
		
	}
	
	
	/*
	 * AKCEPTACJA ZADANIA
	 */
	
	/** akceptacja zadania (pierwsze klikniecie w swoje pismo na liscie zadan)*/
	protected void acceptTask(Long tiId) throws WorkflowException, EdmException {
		JbpmContext ctx = null;
		try 
		{
			ctx = DSjBPM.getContext();
			TaskInstance ti = ctx.getTaskMgmtSession().getTaskInstance(tiId);
			String currActor = ti.getActorId();
			
			//currActor nie jest ustawione, jezeli byla dekretacja na dzial lub wielu uzytkownikow/dzialow
			if (currActor != null) {
				//jezeli jest ustawione to musi to byc zadanie zalogowanego uzytkownika
				if (!JBPMIds.getDSUsername(currActor).equals(DSApi.context().getPrincipalName())) {
					throw new EdmException("obce pismo na liscie zadan uzytkownika");
				}
				
			} else {
				//nie bylo aktora, tylko cala pula
				Set<PooledActor> actorsPool = (Set<PooledActor>)ti.getPooledActors();
				Set<String> pooledActorsIds = new HashSet<String>();
				
				for (PooledActor actor : actorsPool) 
				{
					pooledActorsIds.add(actor.getActorId());
				}
				
				//wyciagamy dzial na ktory byla dekretacja, zadziala jezeli
				//uzytkownik byl jednym z tych na ktorych byla dekretacja
				String currentGuid = getTaskUserGuid(pooledActorsIds);
				
				//wpp, tj jesli uzytkownikowi przysluguje zadanie, bo jest w jakims dziale,
				//na ktory byla dekretacja, to bierzemy pierwszy taki dzial z brzegu
				if (currentGuid == null) {
					currentGuid = getFirstUserGuid(pooledActorsIds);
				}
				
				//if (currentGuid == null and )
				//ups, ten uzytkownik nie moze zaakceptowac zadania, bo nie byl jego adresatem
				if (currentGuid == null) { 
					throw new EdmException("pismo nie zadekretowane na tego uzytkownika");
				}
				String jbpmUsername = JBPMIds.getJBPMUsername(DSApi.context().getPrincipalName(), currentGuid);
				//ustawiamy aktora
				ti.setActorId(jbpmUsername);
			}
			//start procesu, automatycznie ustawiana jest data akceptacji (ti.getStart() )
			ti.start();
			ctx.save(ti.getProcessInstance());
		} catch (Exception e) { 
			throw new EdmException(e);
		} finally {
			DSjBPM.closeContext(ctx);
		}
	}
	
	/** pomocnicza, szuka usera w puli i zwraca dzial z ktorym on sie w tej puli znajdowal */
	private String getTaskUserGuid(Set<String> actorsPool) throws EdmException {
		String thisUserName = DSApi.context().getPrincipalName();
		for (String id : actorsPool) {
			if (JBPMIds.isJBPMUsername(id) && thisUserName.equals(JBPMIds.getDSUsername(id))) {
				return JBPMIds.getDSUserGuid(id);
			}
		}
		return null;
	}
	
	/** pomocnicza, szuka w dzialach z puli pierwszego z brzegu dzialu w ktorym jest uzytkownik */	
	private String getFirstUserGuid(Set<String> actorsPool ) throws UserNotFoundException, EdmException 
	{
		log.trace("getFirstUserGuid");
		DSUser currentUser = DSApi.context().getDSUser();
		log.trace("user={}",currentUser.getName());
		DSDivision[] userDivs = currentUser.getAllDivisions();
		Set<String> divisionNames = new HashSet<String>();
		log.trace("userGuids");
		for (DSDivision div : userDivs) 
		{
			log.trace("guid={}",div.getGuid());
			divisionNames.add(div.getGuid());
		}
		log.trace("actors");
		for (String id : actorsPool) 
		{
			log.trace("actorId={}",id);
			if (JBPMIds.isJBPMGuid(id) && divisionNames.contains(JBPMIds.getDSGuid(id))) 
			{
				log.trace("ten wynik zwrocilem");
				return JBPMIds.getDSGuid(id);
			}
		}
		//Jesli nie znalazl, ale user jest adminem to zwroci pierwszy z brzegu
		//Oznacza to nie mniej ni wiecej tylko to ze admin dostal to pismo awaryjnie
		if (currentUser.isAdmin())
		{
			for (String id : actorsPool) 
			{
				log.trace("user jest adminem zwracam {}",id);
				return JBPMIds.getDSGuid(id);
			}
			return DSDivision.ROOT_GUID;
		}
		log.trace("return null");
		return null;
	}
	
	/*
	 * KO�CZENIE ZADANIA
	 */
	
	/** konczenie zadania i procesu */
	protected void finishTask(Long taskId) throws EdmException 
	{
		JbpmContext ctx = null;
		try {
			ctx = DSjBPM.getContext();
			TaskInstance ti = ctx.getTaskInstance(taskId);
			ti.end(TRANSITION_FINISH);
		} catch (Exception e) { 
			throw new EdmException(e);
		} finally {
			DSjBPM.closeContext(ctx);
		}
	}
	
	
	/**
	 * Pobranie czynnosci na podstawie identyfikatora
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public WorkflowActivity getActivityByTaskId(Long id) throws EdmException 
	{
		JbpmContext ctx = null;
		try 
		{
			ctx = DSjBPM.getContext();
			TaskInstance ti = ctx.getTaskInstance(id);
		
			Boolean accepted = ti.getStart() != null;
			Date receiveDate = ti.getDueDate();
			String source;
			if (ti.getProcessInstance().getProcessDefinition().getName().equals(CC_PROCESS_NAME)) 
			{
				log.trace("Proces {} - pobieramy uzytkownika ze zmiennej procesowej {}",CC_PROCESS_NAME,CC_USER_FROM);
				source = (String)ti.getContextInstance().getVariable(CC_USER_FROM);
			} else 
			{						
				source = findLastActor(ti);
			}
			if (source==null)
			{
				source = "USER_admin;rootdivision";
				log.warn("Nie znaleziono aktora source w procesie {} przyjmujemy wartosc domyslna",ti.getId());				
			}
			log.trace("source={}",source);
			String lastUser = JBPMIds.getDSUsername(source);
			String lastDivision = JBPMIds.getDSUserGuid(source);
			String currentUser;
			String currentDivision;
		
			if (ti.getActorId() != null) 
			{
				currentUser = JBPMIds.getDSUsername(ti.getActorId());
				currentDivision = JBPMIds.getDSUserGuid(ti.getActorId());	
			} 
			else 
			{
				//generalnie tu jest miejsce, gdzie nie zgadza sie ogolnosc tej klasy z reszta systemu
				//gdzie przyjmuje sie zalozenie ze jest co najwyzej jeden biezacy uzytkownik i dokladnie jeden dzial
				//stad to obejscie
				currentUser = null;
				String guid = null;
				Set<PooledActor> pooledActor = ti.getPooledActors();
				Iterator <PooledActor> itx = pooledActor.iterator();
				if (itx.hasNext())
				{
					guid = itx.next().getActorId();
				}
				if (guid==null)
				{
					guid = "GUID_rootdivision";
					log.warn("Nie znaleziono guid w procesie {} przyjmujemy wartosc domyslna",ti.getId());
				}
				log.trace("guid:{}",guid);
				currentDivision = JBPMIds.getDSGuid(guid);
			}
			String objective = ti.getDescription();
			String processKind = getProcessKind(ti.getProcessInstance().getProcessDefinition().getName());
			String processDesc = getProcessDesc(ti.getProcessInstance().getProcessDefinition().getName());
			ContextInstance ci = ti.getContextInstance();
			String deadlineAuthor = (String)ti.getVariableLocally(DEADLINE_AUTHOR);
			String deadlineRemark = (String)ti.getVariableLocally(DEADLINE_REMARK);
			Date deadlineTime = (Date)ti.getVariableLocally(DEADLINE_TIME);
			Long documentId = (Long)ci.getVariable(DOC_ID);
			
			return new WorkflowActivity(
					id,
					accepted,
					receiveDate,
					lastUser,
					lastDivision,
					currentUser,
					currentDivision,
					objective,
					processKind,
					processDesc,
					deadlineTime,
					deadlineAuthor,
					deadlineRemark,
					documentId);

		} catch (Exception e) { 
			throw new EdmException(e);
		} finally {
			DSjBPM.closeContext(ctx);
		}
	}
	
	/** aktorzy prszypisani do zadanie
	 * 
	 * @param taskId
	 * @return zwraca dwuelementow� map�: pod kluczem 'users' jest tablica nazw uzytkownikow (ds),
	 * a pod kluczem 'divs' tablica guidow (ds)
	 * @throws EdmException
	 */
	protected Map<String,String[]> getTaskActors(String taskId) throws EdmException {
		Map<String, String[]> result = new HashMap<String, String[]>();
		Long tiId = Long.parseLong(taskId);
		JbpmContext ctx = null;
		try {
			ctx = DSjBPM.getContext();
			TaskInstance ti = ctx.getTaskInstance(tiId);
			String mainActor = ti.getActorId();
			if (mainActor != null) {
				result.put("users", new String[] {JBPMIds.getDSUsername(mainActor)});
				result.put("divs", new String[] {});
			} else {
				Set<PooledActor> pas =  ti.getPooledActors();
				Set<String> users = new HashSet<String>();
				Set<String> divs = new HashSet<String>();
				for (PooledActor pa : pas) {
					String actor = pa.getActorId();
					if (JBPMIds.isJBPMUsername(actor)) {
						users.add(JBPMIds.getDSUsername(actor));
					} else if (JBPMIds.isJBPMGuid(actor)) {
						divs.add(JBPMIds.getDSGuid(actor));
					} else {
						throw new EdmException("not a proper actor id: " + actor);
					}
				}
				result.put("users", users.toArray(new String[(users.size())]));
				result.put("divs", divs.toArray(new String[(divs.size())]));
			}
			return result;
		}
		catch(Exception e)
		{
			throw new EdmException(e);
		}
		finally {
			DSjBPM.closeContext(ctx);
		}
	}
	
	/** ustawienie danych o terminie w kontekscie zadania */
	protected void addDeadline(String taskId, String author, Date time, String remark) throws EdmException {
		Long taskIdL = Long.parseLong(taskId);
		JbpmContext ctx = null;
		try {
			ctx = DSjBPM.getContext();
			TaskInstance ti = ctx.getTaskMgmtSession().getTaskInstance(taskIdL);
			ti.setVariableLocally(DEADLINE_AUTHOR, author);
			ti.setVariableLocally(DEADLINE_TIME, time);
			ti.setVariableLocally(DEADLINE_REMARK, remark);
		} catch (Exception e) { 
			throw new EdmException(e);
		} finally {
			DSjBPM.closeContext(ctx);
		}
	}
	
	/** TYLKO NA U�YTEK DEBUGOWANIA */ 
	public Map<String, Object> getTaskVariables(Long taskId) throws WorkflowException, EdmException {
		JbpmContext ctx = null;
		try {
			ctx = DSjBPM.getContext();
			ProcessInstance processInstance = ctx.getTaskInstance(taskId).getProcessInstance();
			ContextInstance contextInstance = processInstance.getContextInstance();
			Map result = contextInstance.getVariables();
			result.put(PROCESS_KEY, processInstance.getKey());
			return result;
		} finally {
			DSjBPM.closeContext(ctx);
		}
		
	}	
}
