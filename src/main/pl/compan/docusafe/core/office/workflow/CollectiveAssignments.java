package pl.compan.docusafe.core.office.workflow;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.StringManager;


public class CollectiveAssignments {
	private final static Logger LOG = LoggerFactory.getLogger(CollectiveAssignments.class);
	private static StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
	
	public StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	
	private static CollectiveAssignments instance = new CollectiveAssignments();
	
	public static CollectiveAssignments getInstance() {
		return instance;
	}
	
	
	public Map<String,Map<String, String>> prepareCollectiveAssignments(String activityId) throws EdmException {
//		Preferences prefs = null;
		List<String> prefs = null;
        WorkflowActivity activity = WorkflowFactory.getWfActivity(activityId);
        try
        {
			prefs = provideCollectiveAssignments();

            if (prefs != null) {
               
            } else {
				return null;
			}
          
        }
        catch (BackingStoreException e)
        {
            throw new EdmException("BladOdczytuUstawienUzytkownika", e);
        }
       
        LinkedHashMap<String, String> assignmentsMap = new LinkedHashMap<String, String>();
        LinkedHashMap<String, String> wfProcesses = new LinkedHashMap<String, String>();
        LinkedHashMap<String, String> substituted = new LinkedHashMap<String, String>();
        LinkedHashMap<String, String> substitutedNames = new LinkedHashMap<String, String>();
        Date now = new Date();
        
        for (String entry : prefs)
        {
            AssignmentDescriptor ad =
                AssignmentDescriptor.forDescriptor(entry);

            assignmentsMap.put(ad.toDescriptor(), ad.format());
            
            
            if(ad.getUsername() != null)
			{
	            DSUser temp=DSUser.findByUsername(ad.getUsername());
	            if(temp.getSubstituteUser()!=null&&temp.getSubstitutedFrom().before(now)) {
	            	substituted.put(temp.asFirstnameLastname(),temp.getSubstituteUser().asFirstnameLastname());
	            	substitutedNames.put(temp.getName(), temp.getSubstituteUser().getName());
	            }
	            else {
	            	substituted.put("","");
	            	substitutedNames.put("","");
	            }
			}
        }
        
        //dekretacja do koordynatora
        OfficeDocument document = null; 
        ProcessCoordinator pc = null;
        
        if(activity != null && activity.getDocumentId() != null)
        	document = OfficeDocument.findOfficeDocument(activity.getDocumentId());
        
        if(document != null && document.getDocumentKind() != null)
        {
        	pc = document.getDocumentKind().logic().getProcessCoordinator(document);
        }
        //FIXME - koordynator nie powinien byc okreslany w kazdym wyswietleniu szybkiej dekretacji
        //To np. w przypadku prosiki trwa zbyt dlugo - do enginu workflow powinien byc przekazany kod, ze dekretacja jest do
        //koordynatora i dopiero wtedy ustalamy kto to jest
        if(pc != null)
        {
        	AssignmentDescriptor coordinatorAd = new AssignmentDescriptor(pc.getUsername(), pc.getGuid());
			if(!assignmentsMap.containsKey(coordinatorAd.toDescriptor())){
				assignmentsMap.put(coordinatorAd.toDescriptor(), "( "+sm.getString("koordynatorProcesu")+" )");
			}
        }
        
        /*AssignmentDescriptor ad1 = new AssignmentDescriptor(null, DSDivision.ROOT_GUID);
        assignmentsMap.put(ad1.toDescriptor(), ad1.format());*/

        if (WorkflowFactory.getInstance().isManual(activity))
        {
        	if(document != null)
        	{
        		if( AssignmentPermission.getInstance().checkDockindAllows(document,"obieg_reczny"))
        			wfProcesses.put(WorkflowFactory.getManualProcesId().toString(),smG.getString("realizacja"));
        	}
        	else
        		wfProcesses.put(WorkflowFactory.getManualProcesId().toString(),smG.getString("realizacja"));
        }

    	if(document != null && AssignmentPermission.getInstance().checkDockindAllows(document,"do_wiadomosci"))
    	{
    		wfProcesses.put(
                WorkflowFactory.getCCProcesId().toString(),
                smG.getString("doWiadomosci"));
    	}
	
        Map<String, Map<String, String>> result = new HashMap<String, Map<String,String>>();
//		LOG.error("assignmentMap = " + assignmentsMap);
        result.put("assignmentsMap", assignmentsMap);
        result.put("substituted", substituted);
        result.put("wfProcesses", wfProcesses);
        result.put("substitutedNames", substitutedNames);
        return result;
	}

	Comparator<String> STRING_INT_COMPARATOR = 	new Comparator()
            {
                public int compare(Object o1, Object o2)
				{
					//return ((String)o1).compareTo((String)o2);
					return Integer.parseInt(o1.toString()) - Integer.parseInt(o2.toString());
				}
			};

	private List<String> provideCollectiveAssignments() throws BackingStoreException {
		Preferences prefs = null;
		Preferences prefs2 = null;
		if (DSApi.context().userPreferences().nodeExists("collectiveassignment")
                && DSApi.context().userPreferences().node("collectiveassignment").keys().length > 0) {
			prefs = DSApi.context().userPreferences().node("collectiveassignment");
		} else if (DSApi.context().systemPreferences().nodeExists("modules/office/collectiveassignment") && DSApi.context().systemPreferences().node("modules/office/collectiveassignment").keys().length > 0) {
			prefs = DSApi.context().systemPreferences().node("modules/office/collectiveassignment");
		}

		if(AvailabilityManager.isAvailable("workflow.collectiveAssignment.union")){
			prefs2 = DSApi.context().systemPreferences().node("modules/office/collectiveassignment");
		}
		
		List<String> ret = new ArrayList<String>();

		if (prefs != null) {
            String[] keys = prefs.keys();
			Arrays.sort(keys,STRING_INT_COMPARATOR);
//			LOG.error("prefs = " + Arrays.toString(keys));
			for(String key: keys){
				ret.add(prefs.get(key, ""));
			}
		}
		if(prefs2 != null) {
			String[] keys = prefs2.keys();
//			LOG.error("prefs2 = " + Arrays.toString(keys));
			Arrays.sort(keys,STRING_INT_COMPARATOR);
			for(String key: keys){
				ret.add(prefs2.get(key, ""));
			}
		}
//		LOG.error("ret " + ret);
		return ret;
	}
	
	
	/**
	 * usuwa uzytkownika z list szybkiej dekretacji wszystkich innych uzytkownikow
	 * @param username
	 * @throws EdmException 
	 * @throws BackingStoreException 
	 */
	public void removeUserFromCollectiveAssignments(String username) throws EdmException {
		DSUser[] users = UserFactory.getInstance().searchUsersSpi(0, 0, DSUser.SORT_NAME, true, null,false).results();
		PreparedStatement ps = null;
		PreparedStatement psd = null;
		ResultSet rs = null;
		String sql_select = "select id from ds_prefs_node where username = ? and name = 'collectiveassignment'";
		String sql_delete = "delete from ds_prefs_value where node_id = ? and pref_value like ?";
		
		for (DSUser user : users) {
			String name = user.getName();
			try {
				ps = DSApi.context().prepareStatement(sql_select);
				ps.setString(1, name);
				rs = ps.executeQuery();
				if (rs.next()) {
					Long id = rs.getLong("id");					
					psd = DSApi.context().prepareStatement(sql_delete);
					psd.setLong(1, id);
					psd.setString(2, username+";%");
					psd.execute();
					DSApi.context().closeStatement(psd);
				}
				rs.close();
				DSApi.context().closeStatement(ps);				
			} catch (SQLException e) {
				throw new EdmException(e.getMessage());
			}
		}
	}
	
}
