package pl.compan.docusafe.core.office.workflow;

import org.omg.CORBA.TCKind;
import pl.compan.docusafe.core.EdmException;

/**
 * Reprezentuje typ obiektu AnyObject.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AnyObjectKind.java,v 1.1 2004/05/16 20:10:28 administrator Exp $
 */
public interface AnyObjectKind
{
    int STRING = TCKind._tk_string;
    int BOOLEAN = TCKind._tk_boolean;
    int LONG = TCKind._tk_longlong;
    int DOUBLE = TCKind._tk_double;
    int ENUM = TCKind._tk_enum;

    /**
     * Zwraca kod typu tego obiektu.
     */
    int type();
    int memberCount() throws EdmException;
    String memberName(int pos) throws EdmException;
}
