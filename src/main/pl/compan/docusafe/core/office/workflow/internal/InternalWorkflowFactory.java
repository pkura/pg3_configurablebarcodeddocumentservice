package pl.compan.docusafe.core.office.workflow.internal;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.office.workflow.WorkflowException;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class InternalWorkflowFactory extends WorkflowFactory {
	
	private static final Logger log = LoggerFactory.getLogger(InternalWorkflowFactory.class);

    @Override
    public void reassign(String activity, String fromUser, Set<String> candidateUsers, Set<String> candidateDivisionGuids) throws EdmException {
        throw new UnsupportedOperationException("reassign not supported");//u�yj WorkflowFactory.simpleAssignment
    }

    @Override
    public String getProcessName(String activityId) throws EdmException {
        return isManual(activityId) ? "manual" : "read-only";
    }
    @Override
    public void reloadParticipantMapping() throws EdmException
    {
    	System.out.println("reloadParticipantMapping");
        try
        {
        	String hqlDelete = "delete from "+ParticipantMapping.class.getName();
        	log.error("Usuni�to " +DSApi.context().session().createQuery( hqlDelete ).executeUpdate()+" ParticipantMapping ");
//        	hqlDelete = "delete from "+Resource.class.getName();
//        	log.error("Usuni�to " +DSApi.context().session().createQuery( hqlDelete ).executeUpdate()+" ParticipantMapping ");
        	System.out.println("skasowa�");
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
        List<DSUser> users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
    	for (DSUser dsUser : users) 
    	{
    		try
    		{
	    		try
	    		{
	    		 WorkflowFactory.createUser(dsUser.getName(), "enhydra", "", dsUser.getEmail(), "");
	    		}
	    		catch (Exception e) 
	    		{
					log.error("B��d przy pr�bie za�o�enia resource "+dsUser.getName(), e);
				}
	    		WorkflowFactory.addParticipantToUsernameMapping(null, null, dsUser.getName(), dsUser.getName());
	    		for(String guid :dsUser.getDivisionsGuid())
	    		{
	    			WorkflowFactory.addParticipantToUsernameMapping(null, null, "ds_guid_".concat(guid), dsUser.getName());
	    		}
    		}
    		catch (Exception e) 
    		{
    			log.error("B��d przy pr�bie dodania Participant "+dsUser.getName(), e);
			}
		}
    }
}
