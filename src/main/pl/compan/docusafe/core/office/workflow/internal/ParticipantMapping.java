package pl.compan.docusafe.core.office.workflow.internal;

import java.io.Serializable;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ParticipantMapping.java,v 1.2 2004/06/05 21:10:19 administrator Exp $
 */
public class ParticipantMapping implements Serializable
{
    private String participantId;
    private WfResourceImpl resource;

    public String getParticipantId()
    {
        return participantId;
    }

    public void setParticipantId(String participantId)
    {
        this.participantId = participantId;
    }

    public WfResourceImpl getResource()
    {
        return resource;
    }

    public void setResource(WfResourceImpl resource)
    {
        this.resource = resource;
    }


    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ParticipantMapping)) return false;

        final ParticipantMapping participantMapping = (ParticipantMapping) o;

        if (participantId != null ? !participantId.equals(participantMapping.participantId) : participantMapping.participantId != null) return false;
        if (resource != null ? !resource.equals(participantMapping.resource) : participantMapping.resource != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (participantId != null ? participantId.hashCode() : 0);
        result = 29 * result + (resource != null ? resource.hashCode() : 0);
        return result;
    }

    public String toString()
    {
        return getClass().getName()+"[participantId="+participantId+
            " resource="+resource.getKey()+"]";
    }
}
