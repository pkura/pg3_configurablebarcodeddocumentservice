package pl.compan.docusafe.core.office.workflow.internal;

import org.hibernate.*;
import org.hibernate.classic.Lifecycle;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mozilla.javascript.JavaScriptException;
import org.mozilla.javascript.Scriptable;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.office.workflow.*;
import pl.compan.docusafe.core.office.workflow.xpdl.*;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.*;

/**
 * Spos�b dynamicznego wyboru u�ytkownik�w, kt�rzy zobacz� zadanie
 * na swoich listach zada�:
 * Identyfikator obiektu Participant zwi�zanego z Activity musi
 * mie� warto�� {@link InternalWorkflowService#EXPRESSION_PARTICIPANT_ID}.
 * W�wczas szukana jest warto�� rozszerzonego atrybutu procesu o nazwie
 * {@link InternalWorkflowService#PARTICIPANT_ID_EXTATTR}. Warto�ci�
 * tego atrybutu jest nazwa zmiennej procesu, w kt�rej z kolei
 * znajduje si� identyfikator uczestnika.
 * <p>
 * Je�eli nie istnieje mapowanie uczestnika na konkretnego u�ytkownika
 * procesu, zadanie przekazywane jest u�ytkownikowi 'admin'.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfProcessImpl.java,v 1.30 2010/05/17 09:07:56 pecet1 Exp $
 */
public class WfProcessImpl extends WfExecutionObjectImpl
    implements WfProcess, Lifecycle
{
    private static final Log log = LogFactory.getLog(Constants.Package);

    private Long id;
    private String packageId;
    private String processDefinitionId;
    /**
     * Zbi�r obiekt�w NameValueImpl.
     */
    private Set<? extends NameValue> processContext;

    private List<WfActivity> activities;

    private WfPackageDefinition packageDefinition;

    // pole tworzone ad hoc przez metod� manager()
    private WfProcessMgr wfProcessMgr;

    private Date deadlineTime;
    private String deadlineAuthor;
    private String deadlineRemark;
    private Date deadlineLastReport;

    public static WfProcessImpl findByKey(String key) throws EdmException
    {
        try
        {
            List<WfProcessImpl> results =
                DSApi.context().classicSession().find("from p in class "+WfProcessImpl.class.getName()+
                " where p.key = ?", key, Hibernate.STRING);
            if (results.size() > 0)
                return results.get(0);

            throw new EntityNotFoundException("Nie znaleziono procesu ("+key+")");
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /* WfProcess */

    public int how_many_step() throws WorkflowException
    {
        throw new UnsupportedOperationException();
    }

    public WfActivity[] get_activities_in_state_array(String state) throws WorkflowException
    {
        if (activities == null || activities.size() == 0)
            return new WfActivity[0];

        // tworz� kopi� tablicy activities i usuwam z niej te kroki,
        // kt�re s� w innym stanie ni� ��dany
        List<WfActivity> activitiesInState = new ArrayList<WfActivity>(activities);
        for (Iterator iter=activitiesInState.iterator(); iter.hasNext(); )
        {
            if (!((WfActivity) iter.next()).state().equals(state))
                iter.remove();
        }

        return (WfActivity[]) activitiesInState.
            toArray(new WfActivity[activitiesInState.size()]);
    }

    /**
     * Rozpoczyna proces.
     */
    public void start() throws WorkflowException
    {
        // zmienne procesu (processContext) s� tworzone w metodzie
        // WfProcessMgrImpl.create_process()

        this.state = "open.running"; // -> "closed.completed"

        XpdlPackage xpdlPackage = InternalWorkflowService.getXpdlPackage(packageDefinition.getId());

        if (xpdlPackage == null)
            throw new WorkflowException("Nie znaleziono pakietu "+packageId);

        // definicja tego procesu
        WorkflowProcess process = xpdlPackage.getProcess(processDefinitionId);

        if (process == null)
            throw new WorkflowException("W pakiecie "+packageDefinition+" nie znaleziono " +
                "definicji procesu "+processDefinitionId);

        // pierwszy krok procesu
        Activity activity = process.getStartingActivity();

        try
        {
            push(activity, null);
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
    }

    /**
     * Zwraca kolekcj� atrybut�w (zmiennych) procesu. Zmiany w warto�ciach
     * parametr�w s� od razu odnotowywane, nie ma potrzeby wywo�ywania
     * metody set_process_context.
     */
    public NameValue[] process_context() throws WorkflowException
    {
        if (processContext == null)
            return new NameValue[0];

        return (NameValue[]) processContext.toArray(new NameValue[processContext.size()]);
    }

    /**
     * Aktualizuje parametry formalne procesu. Parametry procesu, kt�re nie
     * znalaz�y si� w przekazanej tablicy pozostaj� bez zmian.
     * XXX: nie parametry formalne, tylko pola danych
     */
    public void set_process_context(NameValue[] nv) throws WorkflowException
    {
        throw new UnsupportedOperationException("Zmiany w warto�ciach parametr�w " +
            "procesu s� odnotowywane od razu po modyfikacji obiektu NameValueImpl.");
    }

    /**
     * Zwraca instancj� WfProcessMgr.
     */
    public WfProcessMgr manager() throws WorkflowException
    {
        if (wfProcessMgr == null)
        {
            XpdlPackage xpdlPackage = InternalWorkflowService.getXpdlPackage(packageDefinition.getId());

            if (xpdlPackage == null)
                throw new WorkflowException("Nie znaleziono pakietu "+packageId);

            // definicja tego procesu
            WorkflowProcess process = xpdlPackage.getProcess(processDefinitionId);

            wfProcessMgr = new WfProcessMgrImpl(process, packageDefinition.getId());
        }

        return wfProcessMgr;
    }

    /**
     * Je�eli przekazany krok procesu jest typu Route, metoda
     * znajduje pierwszy krok, kt�ry nie jest tego typu.
     * Je�eli taki krok nie zostanie znaleziony, za� parametr
     * currentActivity jest r�wny null (tzn. jest to pocz�tek
     * procesu), rzucany jest wyj�tek WorkflowException.
     * <p>
     * Po znalezieniu kolejnego kroku procesu bie��cy krok
     * (currentActivity) jest zamykany poprzez ustawienie
     * statusu closed.completed i usuni�cie przyporz�dkowa�
     * do u�ytkownik�w (WfAssignment).
     * <p>
     * Kolejny krok procesu jest najpierw poszukiwany na li�cie
     * krok�w ju� wykonanych i je�eli nie zostanie tam znaleziony,
     * jest tworzony. Tworzone s� dla niego przyporz�dkowania
     * do u�ytkownik�w (WfAssignment) i krok dostaje status
     * open.running.
     * @param activity Definicja bie��cego kroku procesu.
     * @param currentActivity Obiekt reprezentuj�cy bie��cy krok
     * procesu - dla pocz�tku procesu jest r�wny null.
     * @throws WorkflowException
     * @throws EdmException
     * @throws HibernateException
     */
    private void push(Activity activity, WfActivityImpl currentActivity)
        throws WorkflowException, EdmException, HibernateException
    {
        // pocz�tek procesu - znalezienie pierwszego "normalnego" kroku
        // utworzenie WfAssignments, WfActivity

        // �rodek procesu - przej�cie do kolejnego "normalnego" kroku
        // utworzenie WfAssignments, WfActivity


        // je�eli pierwszy krok jest typu route - szukanie pierwszego
        // "normalnego" kroku
        if (activity.isRoute())
            activity = route(activity,  this);

        if (log.isDebugEnabled())
            log.debug("push: activity="+activity);

        // proces sko�czy� si� zanim si� zacz��
        if (activity == null && currentActivity == null)
        {
            throw new WorkflowException("Nieprawid�owa definicja procesu - " +
                "brak krok�w do wykonania.");
        }

        // bie��cy krok procesu - przekazanie zmiennych z powrotem do procesu,
        // ustawienie statusu "closed.completed"
        if (currentActivity != null)
        {
            currentActivity.setState("closed.completed");

            // usuwanie obiekt�w WfAssignment zwi�zanych z poprzednim
            // krokiem procesu

            // tutaj wyst�puje deadlock
            // najwyra�niej inna transakcja modyfikuje te same wiersze DSW_ASSIGNMENT

            // deadlock wyst�puje wtedy, gdy jedna transakcja aktualizuje wiersz
            // (UPDATE), a druga pr�buje go usun�� lub zaktualizowa�
            DSApi.context().classicSession().delete(
                "from wfa in class "+WfAssignmentImpl.class.getName()+
                " where wfa.activity.id = "+currentActivity.getId());

            // przepisywanie parametr�w kroku procesu do parametr�w procesu
            // (tylko je�eli istnieje kolejny krok procesu, inaczej nie warto)
            if (this.processContext != null && activity != null)
            {
                NameValue[] activityContext = currentActivity.process_context();
                if (activityContext.length > 0)
                {
                    NameValue[] aProcessContext = (NameValue[]) this.processContext.toArray(
                        new NameValue[this.processContext.size()]);
                    for (int i=0; i < activityContext.length; i++)
                    {
                        NameValue processVar = WorkflowUtils.findParameter(aProcessContext, activityContext[i].name());
                        
                        if (processVar != null)
                        {
                            processVar.value().set(activityContext[i].value().value());
                        }
                    }
                }
            }

            // poszukiwanie kolejnego kroku procesu
            // ju� po przepisaniu zmiennych do kontekstu procesu, co jest
            // wa�ne, bo skrypty okre�laj�ce warunki przej�cia korzystaj�
            // z tych w�a�nie zmiennych

            // nie powinno si� zdarzy�
            if (activity == null)
                throw new WorkflowException("activity = null");

            // znalezienie kolejnego kroku procesu
            Transition[] transitions = activity.getTransitions();

            if (transitions.length > 1)
                throw new WorkflowException("Znaleziono wi�cej ni� jedno mo�liwe " +
                    "przej�cie z kroku "+activity.getId());

            // mo�e nie by� kolejnego kroku (bie��cy krok mo�e by� ostatnim)
            activity = transitions.length > 0 ? transitions[0].getTo() : null;

            if (activity != null && activity.isRoute())
                activity = route(activity, this);
        }

        // nie ma kolejnego kroku procesu - ustawiam status procesu "closed.completed"
        // i usuwam kolekcje zmiennych oraz activities zwi�zane z procesem
        // zak�adam, �e tylko jeden krok procesu mo�e by� w danym momencie aktywny
        // inaczej trzeba przejrze� pozosta�e zanim zako�czy si� proces
        if (activity == null)
        {
            if (log.isDebugEnabled())
                log.debug("push: ko�czenie procesu "+id);

            setState("closed.completed");
            if (this.processContext != null)
                this.processContext.clear();
            if (this.activities != null)
                this.activities.clear();
            this.deadlineTime = null;
            this.deadlineAuthor = null;
            this.deadlineLastReport = null;
            this.deadlineRemark = null;

            return;
        }
        // lista WfResource mapowanych na uczestnika zwi�zanego z activity
        Participant participant = activity.getPerformer();
        WfResourceImpl[] wfResources;
        try {
            wfResources = findResources(participant);
        } catch (Exception ex) {
            //b��d w tym momencie zwykle �wiadczy o zepsutym procesie, by za�ata� ten b��d ustawiam
            //pust� tablic� i licz�, �e pismo zostanie zadekretowane do administratora
            log.error("Error in findResources -> returning empty array, error below");
            log.error(ex.getMessage(), ex);
            wfResources = new WfResourceImpl[0];
        }

        // je�eli brak odbiorc�w, zadanie pojawi si� u administratora

        if (wfResources.length == 0)
        {
            log.warn("Nie znaleziono u�ytkownik�w odpowiadaj�cych uczestnikowi "+
                participant+", u kt�rych mo�e pojawi� si�" +
                " zadanie "+getId()+" ("+getProcessDefinitionId()+") - przekazywanie"+
                " zadania u�ytkownikowi admin");

            List resources = DSApi.context().classicSession().
                find("from wfresource in class "+WfResourceImpl.class.getName()+
                " where wfresource.key = 'admin'");

            if (resources.size() == 0)
                throw new WorkflowException("Brak u�ytkownika 'admin', nie mo�na " +
                    "przekaza� zadania �adnemu u�ytkownikowi");

            wfResources = new WfResourceImpl[1];
            wfResources[0] = (WfResourceImpl) resources.get(0);
        }
        // sprawdzam, czy ten krok by� wcze�niej wykonywany
        // XXX: ten sam krok nie mo�e by� jednocze�nie u dw�ch u�ytkownik�w,
        // kt�rzy doszli do niego niezale�nymi �cie�kami
        WfActivityImpl wfActivity = findActivity(activity);
        if (wfActivity != null)
        {
            wfActivity.setState("open.running");
        }
        else
        {
            wfActivity = new WfActivityImpl();
            wfActivity.setName(activity.getName());
            wfActivity.setDescription(activity.getDescription());
            wfActivity.setProcess(this);
            wfActivity.setActivityDefinitionId(activity.getId());

            wfActivity.setState("open.running");

            DSApi.context().session().save(wfActivity);
            //wfActivity.setKey(processDefinitionId+"_"+wfActivity.getId());

            if (activities == null)
                activities = new LinkedList<WfActivity>();

            activities.add(wfActivity);
        }
        // przepisywanie zmiennych procesu do zbioru zmiennych kroku procesu
        if (processContext == null || processContext.size() == 0)
        {
            wfActivity.setProcessContext(new HashSet<NameValue>());
        }
        else
        {
            Set<NameValue> activityContext = new HashSet<NameValue>(processContext.size());
            for (NameValue nv : processContext)
            {
                activityContext.add(((NameValueImpl) nv).cloneObject());
            }
            wfActivity.setProcessContext(activityContext);
        }

        // tworz� obiekty WfAssignment wi���ce WfActivity z WfResourceS
        for (int i=0; i < wfResources.length; i++)
        {
            WfResourceImpl wfResource = wfResources[i];
            WfAssignmentImpl wfAssignment = new WfAssignmentImpl();
            wfAssignment.setActivity(wfActivity);
            wfAssignment.setResource(wfResource);
            wfAssignment.setResourceCount(wfResources.length);
            DSApi.context().session().save(wfAssignment);
        }
        DSApi.context().session().flush();
    }

    public void setDeadline(Date date, String remark)
    {
        if (date == null)
            throw new NullPointerException("date");
        if (remark == null)
            throw new NullPointerException("remark");
        setDeadlineTime(date);
        setDeadlineRemark(remark);
        setDeadlineAuthor(DSApi.context().getPrincipalName());
    }

    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }

    public Set getProcessContext()
    {
        return processContext;
    }

    void setProcessContext(Set<? extends NameValue> processContext)
    {
        this.processContext = processContext;
    }

    public String getPackageId()
    {
        return packageId;
    }

    void setPackageId(String packageId)
    {
        this.packageId = packageId;
    }

    public String getProcessDefinitionId()
    {
        return processDefinitionId;
    }

    void setProcessDefinitionId(String processDefinitionId)
    {
        this.processDefinitionId = processDefinitionId;
    }

    public String getState()
    {
        return state;
    }

    void setState(String state)
    {
        this.state = state;
    }

    public String getKey()
    {
        return key;
    }

    private void setKey(String key)
    {
        this.key = key;
    }

    public WfPackageDefinition getPackageDefinition()
    {
        return packageDefinition;
    }

    void setPackageDefinition(WfPackageDefinition packageDefinition)
    {
        this.packageDefinition = packageDefinition;
    }

    public Date getDeadlineTime()
    {
        return deadlineTime;
    }

    void setDeadlineTime(Date deadlineTime)
    {
        this.deadlineTime = deadlineTime;
    }

    public String getDeadlineAuthor()
    {
        return deadlineAuthor;
    }

    void setDeadlineAuthor(String deadlineAuthor)
    {
        this.deadlineAuthor = deadlineAuthor;
    }

    public String getDeadlineRemark()
    {
        return deadlineRemark;
    }

    void setDeadlineRemark(String deadlineRemark)
    {
        this.deadlineRemark = deadlineRemark;
    }

    public Date getDeadlineLastReport()
    {
        return deadlineLastReport;
    }

    public void setDeadlineLastReport(Date deadlineLastReport)
    {
        this.deadlineLastReport = deadlineLastReport;
    }

    /* Lifecycle */

    public boolean onSave(Session s) throws CallbackException
    {
        if (key == null)
        {
            if (id != null)
            {
                key = processDefinitionId + "_" + id;
            }
            else
            {
                try
                {
                    List count = DSApi.context().classicSession().find("select count(*) from p in class "+
                        getClass().getName());
                    if (count != null && count.size() > 0 && count.get(0) instanceof Integer)
                    {
                        key = processDefinitionId + (((Long) count.get(0)).intValue() + 1);
                    }
                    else
                    {
                        key = processDefinitionId + "1";
                    }
                }
                catch (HibernateException e)
                {
                    throw new CallbackException(e.getMessage(), e);
                }
                catch (EdmException e)
                {
                    throw new CallbackException(e.getMessage(), e);
                }
            }
        }
        if (lastStateTime == null) lastStateTime = new Date();
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }

    public String toString()
    {
        return getClass().getName()+"[id="+id+" packageId="+packageId+
            " processDefinitionId="+processDefinitionId+" key="+key+
            " processContext="+processContext+"]";
    }

    /**
     * Wywo�ywane przez {@link WfAssignmentImpl#set_accepted_status(boolean)}.
     */
    void accept(WfAssignmentImpl assignment) throws WorkflowException
    {
        PreparedStatement pst = null;
        try
        {
            // usuwanie wszystkich obiekt�w WfAssignment zwi�zanych
            // z tym samym obiektem WfActivity, co przekazany obiekt
            // WfAssignment, z wyj�tkiem tego w�a�nie obiektu
            //log.info("DEADLOCK!!!");

            pst = DSApi.context().prepareStatement("DELETE FROM "+DSApi.getTableName(WfAssignmentImpl.class)+
                " WHERE ACTIVITY_ID = ? AND RESOURCE_ID != ?");
            pst.setLong(1, assignment.getActivity().getId());
            pst.setLong(2, assignment.getResource().getId());
            pst.executeUpdate();


        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (SQLException e)
        {
            throw new WorkflowException(EdmSQLException.getMessage(e), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
        finally
        {
        	DSApi.context().closeStatement(pst);
        }
    }

    /**
     * Wywo�ywane przez {@link WfAssignmentImpl#set_accepted_status(boolean)}.
     */
    void unaccept(WfAssignmentImpl assignment) throws WorkflowException
    {
        XpdlPackage xpdlPackage = InternalWorkflowService.getXpdlPackage(packageDefinition.getId());
        WorkflowProcess process = xpdlPackage.getProcess(processDefinitionId);
        Activity activity = process.getActivity(
            assignment.getActivity().getActivityDefinitionId());

        PreparedStatement pst = null;
        try
        {
            WfResourceImpl[] wfResources = findResources(activity.getPerformer());

            pst = DSApi.context().prepareStatement("DELETE FROM "+DSApi.getTableName(WfAssignmentImpl.class)+
                    " WHERE ACTIVITY_ID = ?");
            pst.setLong(1, assignment.getActivity().getId());
            pst.executeUpdate();

            for (int i=0; i < wfResources.length; i++)
            {
                WfResourceImpl wfResource = wfResources[i];

                WfAssignmentImpl wfAssignment = new WfAssignmentImpl();
                wfAssignment.setActivity(assignment.getActivity());
                wfAssignment.setResource(wfResource);

                //createdAssignment(wfResource);

                DSApi.context().session().save(wfAssignment);
            }
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (SQLException e)
        {
            throw new WorkflowException(EdmSQLException.getMessage(e), e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }

    void complete(WfActivityImpl wfActivity) throws WorkflowException {
        try
        {
            XpdlPackage xpdlPackage = InternalWorkflowService.getXpdlPackage(packageDefinition.getId());
            WorkflowProcess process = xpdlPackage.getProcess(processDefinitionId);

            Activity activity = process.getActivity(wfActivity.getActivityDefinitionId());
            push(activity, wfActivity);
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            throw new WorkflowException(EdmHibernateException.getMessage(e), e);
        }
    }

    /**
     * Zwraca rozszerzone atrybuty nale��ce do przekazanego obiektu
     * WfActivity. Metoda znajduje si� tutaj, poniewa� rozszerzone
     * atrybuty
     * @param wfActivity
     * @return
     * @throws WorkflowException
     */
    NameValue[] getExtendedAttributes(WfActivityImpl wfActivity)
        throws WorkflowException
    {
        XpdlPackage xpdlPackage = InternalWorkflowService.getXpdlPackage(packageDefinition.getId());
        WorkflowProcess process = xpdlPackage.getProcess(processDefinitionId);
        Activity activity = process.getActivity(wfActivity.getActivityDefinitionId());
        ExtendedAttribute[] extendedAttributes = activity.getExtendedAttributes();
        NameValue[] nvs = new NameValue[extendedAttributes.length];
        for (int i=0; i < extendedAttributes.length; i++)
        {
            nvs[i] = new NameValueTransientImpl(
                extendedAttributes[i].getName(), extendedAttributes[i].getValue(),
                BasicDataType.DT_STRING);
        }

        return nvs;
    }

    /**
     * Szuka w kolekcji {@link #activities} obiektu o atrybucie
     * activityDefinitionId zgodnym z atrybutem id przekazanego
     * obiektu activity.
     * @param activity Instancja WfActivityImpl lub null.
     */
    private WfActivityImpl findActivity(Activity activity)
    {
        if (activities != null && activities.size() > 0)
        {
            for (Iterator iter=activities.iterator(); iter.hasNext(); )
            {
                WfActivityImpl wfActivity = (WfActivityImpl) iter.next();
                if (wfActivity.getActivityDefinitionId().equals(activity.getId()))
                    return wfActivity;
            }
        }

        return null;
    }

    /**
     * Wyszukuje w bazie danych obiekty WfResource, kt�re odpowiadaj�
     * definicji u�ytkownika z opisu procesu.
     * @param participant
     * @return
     * @throws WorkflowException
     * @throws EdmException
     * @throws HibernateException
     */
    private WfResourceImpl[] findResources(Participant participant)
        throws WorkflowException, EdmException, HibernateException
    {
        WfResourceImpl[] wfResources;

        if (participant.isExpression())
        {
            throw new WorkflowException("U�ytkownik typu Expression (Shark) - brak wsparcia.");
        }
        else if (InternalWorkflowService.EXPRESSION_PARTICIPANT_ID.equals(participant.getId()))
        {
            XpdlPackage xpdlPackage = InternalWorkflowService.getXpdlPackage(packageDefinition.getId());
            WorkflowProcess process = xpdlPackage.getProcess(processDefinitionId);

            // nazwa zmiennej procesu, kt�r� okre�lany jest uczestnik
            // (czyli atrybut ParticipantMapping.participant)
            ExtendedAttribute participantVariable =
                process.getExtendedAttribute(InternalWorkflowService.PARTICIPANT_ID_EXTATTR);

            if (participantVariable == null || participantVariable.getValue() == null) {
                throw new WorkflowException("U�ytkownik typu expression - brak rozszerzonego " +
                    "atrybutu procesu o nazwie "+InternalWorkflowService.PARTICIPANT_ID_EXTATTR);
            }

            String variableName = participantVariable.getValue();

            if (variableName == null){
                throw new WorkflowException("U�ytkownik typu expression - rozszerzony " +
                    "atrybut procesu o nazwie "+InternalWorkflowService.PARTICIPANT_ID_EXTATTR+
                    " nie ma warto�ci");
            }

            NameValue nv = WorkflowUtils.findParameter(processContext, variableName);

            if (nv == null){
                throw new WorkflowException("Nie istnieje zmienna procesu, kt�rej nazwa " +
                    "jest warto�ci� atrybutu rozszerzonego o nazwie "+
                    InternalWorkflowService.PARTICIPANT_ID_EXTATTR);
            }

            if (log.isDebugEnabled()){
                log.debug("findResources: Pobieranie id uczestnika ze zmiennej procesu o nazwie "+
                    nv.name()+": "+nv.value().value());
            }

            // identyfikator uczestnika brany ze zmiennej nv
/*
            List mappings = DSApi.context().session().find(
                "from mapping in class "+ParticipantMapping.class.getName()+
                " where mapping.participantId = ?",
                nv.value().value(), Hibernate.STRING);
*/
            List<WfResource> resources = (List<WfResource>) DSApi.context().classicSession().find(
                "select mapping.resource from mapping in class "+
                    ParticipantMapping.class.getName()+
                " where mapping.participantId = ?",
                nv.value().value(), Hibernate.STRING);

            if (log.isDebugEnabled())
                log.debug("findResources: Znaleziono "+resources.size()+" uczestnik�w: "+resources);

            wfResources = resources.toArray(new WfResourceImpl[resources.size()]);
/*
            wfResources = new WfResourceImpl[mappings.size()];

            for (int i=0, n=mappings.size(); i < n; i++)
            {
                wfResources[i] = ((ParticipantMapping) mappings.get(i)).getResource();
            }
*/
        }
        else
        {
            if (log.isDebugEnabled())
                log.debug("Pobieranie mapowa� dla uczestnika "+participant.getId());

            // identyfikator
            List<WfResource> resources = (List<WfResource>) DSApi.context().classicSession().find(
                "select mapping.resource from mapping in class "+ParticipantMapping.class.getName()+
                " where mapping.participantId = ?", participant.getId(), Hibernate.STRING);

            if (log.isDebugEnabled())
                log.debug("Znaleziono "+resources.size()+" mapowa�: "+resources);

            wfResources = resources.toArray(new WfResourceImpl[resources.size()]);
        }

        if(wfResources == null || wfResources.length < 1){
        	log.error("Nie znalazl wfResources do dekretacji participantId = "+participant.getId());
        }
        return wfResources;
    }

    /**
     * Przechodzi przez kolejne kroki typu Route do momentu, a�
     * napotkany zostanie krok innego typu. Je�eli pocz�tkowy
     * krok przekazany jako argument nie jest typu Route, jest
     * on od razu zwracany. Je�eli po przej�ciu wszystkich krok�w
     * typu Route nie zostanie znaleziony krok nast�pny (tzn. proces
     * dojdzie do ko�ca), zwracana jest warto�� null.
     * @param activity
     * @param process
     */
    static Activity route(Activity activity, WfProcessImpl process)
        throws WorkflowException
    {
        // TODO: czy kroki typu Route nale�y dodawa� do kolekcji activities?

        // nale�y przej�� wszelkie kroki typu Route (o ile istniej�)
        while (activity.isRoute())
        {
            if (log.isDebugEnabled())
                log.debug("Proces "+process.processDefinitionId+" ("+process.id+"): "+
                    "krok typu Route: "+activity);

            // mo�liwe przej�cia z tego punktu procesu
            Transition[] transitions = activity.getTransitions();

            // osi�gni�to ostatni krok procesu
            if (transitions.length == 0)
            {
                if (log.isDebugEnabled())
                    log.debug("Proces "+process.processDefinitionId+" ("+process.id+"): "+
                        "osi�gni�to ostatni krok procesu.");
                return null;
            }

            // sprawdzam po kolei wszystkie przej�cia i szukam takiego,
            // kt�rego warunek zostanie spe�niony; je�eli takie przej�cie
            // nie zostanie znalezione, przyjmuj�, �e spe�niony jest warunek
            // ostatniego znalezionego przej�cia (bez sprawdzania)
            // XXX: przyjmuj� milcz�co, �e podzia� �cie�ki procesu jest zawsze typu Split
            for (int i=0; i < transitions.length; i++)
            {
                Transition transition = transitions[i];

                if (log.isDebugEnabled())
                    log.debug("Proces "+process.processDefinitionId+" ("+process.id+"): "+
                        "transition="+transition);

                // ostatnie przej�cie - automatyczna akceptacja bez sprawdzania
                // warunku (wszystkie poprzednie warunki nie zosta�y spe�nione)
                if (i >= transitions.length-1)
                {
                    if (log.isDebugEnabled())
                        log.debug("Proces "+process.processDefinitionId+" ("+process.id+"): "+
                            "ostatnie przej�cie - wykonywanie bez sprawdzania warunku");
                    activity = transition.getTo();
                    break; // z p�tli for
                }

                // warunek przej�cia
                Condition condition = transition.getCondition();

                // je�eli warunek przej�cia jest pusty - ignoruj� go
                // i przechodz� do kolejnego
                if (StringUtils.isEmpty(condition.getExpression()))
                {
                    log.warn("Proces "+process.processDefinitionId+" ("+process.id+"): "+
                        "napotkano pusty warunek przej�cia w kroku "+activity+" - pomijanie.");
                    continue;
                }

                // wykonanie wyra�enia umieszczonego w warunku przej�cia
                // jako javascript
                org.mozilla.javascript.Context context;
                try
                {
                    // zwi�zanie kontekstu z bie��cym w�tkiem
                    context = org.mozilla.javascript.Context.enter();
                    Scriptable scope = context.initStandardObjects(null);

                    // przekazywanie wszystkich zmiennych procesu do skryptu
                    if (process.processContext != null)
                    {
                        for (Iterator iter=process.processContext.iterator(); iter.hasNext(); )
                        {
                            NameValue nv = (NameValue) iter.next();
                            scope.put(nv.name(), scope, nv.value().value());
                        }
                    }

                    // wynik dzia�ania skryptu
                    Object result = context.evaluateString(scope,
                        condition.getExpression(), "Condition", 1, null);

                    if (log.isDebugEnabled())
                        log.debug("Proces "+process.processDefinitionId+" ("+process.id+"): "+
                            "Warunek "+condition.getExpression()+" zwr�ci� wynik "+result+
                            " ("+(result != null ? result.getClass() : null)+")");

                    if (result instanceof Boolean && ((Boolean) result).booleanValue())
                    {
                        activity = transition.getTo();
                        break;
                    }
                    else
                    {
                        log.warn("Proces "+process.processDefinitionId+" ("+process.id+"): "+
                            "wyra�enie '"+condition.getExpression()+"' "+
                            "umieszczone w kroku "+activity.getId()+
                            " nie zwr�ci�o warto�ci logicznej - pomijanie.");
                    }
                }
                catch (EdmException e)
                {
                    throw new WorkflowException(e.getMessage(), e);
                }
                catch (JavaScriptException e)
                {
                    throw new WorkflowException(e.getMessage(), e);
                }
                finally
                {
                    org.mozilla.javascript.Context.exit();
                }
            }
        }

        return activity;
    }


}
