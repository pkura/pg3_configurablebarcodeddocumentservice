package pl.compan.docusafe.core.office.workflow.xpdl;

import org.dom4j.Element;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EnumerationDataType.java,v 1.4 2006/02/20 15:42:21 lk Exp $
 */
public class EnumerationDataType extends DataType
{
    public static final String NAME = "EnumerationType";

    /**
     * Mo�liwe warto�ci typu enumeracyjnego (instancje String)
     */
    private List<String> values = new LinkedList<String>();

    EnumerationDataType(Element enumerationType)
    {
        List elEnumerationValues = enumerationType.elements("EnumerationValue");
        if (elEnumerationValues != null && elEnumerationValues.size() > 0)
        {
            for (Iterator iter=elEnumerationValues.iterator(); iter.hasNext(); )
            {
                String name = ((Element) iter.next()).attribute("Name").getValue();
                values.add(name);
            }
        }
    }

    EnumerationDataType(List<String> members)
    {
        values.addAll(members);
    }

    public List<String> getValues()
    {
        return new ArrayList<String>(values);
    }

    public Object getValue(String s)
    {
        if (s == null)
            throw new NullPointerException("s");

        for (Iterator iter=values.iterator(); iter.hasNext(); )
        {
            if (iter.next().equals(s))
                return s;
        }

        throw new IllegalArgumentException("W�r�d warto�ci enumeracji nie ma " +
            "przekazanej warto�ci: "+s+", ("+values+")");
    }

    public Object getDefaultValue()
    {
        if (values.size() == 0)
            throw new UnsupportedOperationException("Enumeracja nie ma �adnych warto�ci.");

        return values.get(0);
    }

    public String getTypeName()
    {
        return NAME;
    }
}
