package pl.compan.docusafe.core.office.workflow.snapshot;

import org.hibernate.Session;
import pl.compan.docusafe.core.EdmException;

import java.util.Map;

/**
 * Interfejs z wydzielonymi metodami z wcze�niejszego SnapshotProvidera,
 *
 * S�u�y do aktualizacji listy i do sprawdzania przydzia�u zada�
 */
public interface SnapshotProvider {

    void updateTaskList(Session session, Map<Long, RefreshRequestBean> requests);

    /** Zwraca true je�li jakie� zadanie zwi�zane z danym dokumentem jest przypisane danemu u�ytkownikowi */
    boolean isAssignedToUser(String userName, Long documentId) throws EdmException;

    /***
     * Zwraca informacje o pozycji zadania na liscie zadan.
     * Domyslenie sortowanie po dacie wejscia na liste.
     * @param activityKey
     * @return
     */
    TaskPositionBean getTaskPosition(String activityKey);
}
