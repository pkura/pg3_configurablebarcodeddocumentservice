package pl.compan.docusafe.core.office.workflow;

public class AssigmentParam
{
	private ProcessId processId;
	private AssignmentDescriptor assignmentDescriptor;
	private String objective;
	
	public AssigmentParam(ProcessId processId,AssignmentDescriptor assignmentDescriptor,String objective)
	{
		this.assignmentDescriptor = assignmentDescriptor;
		this.processId = processId;
		this.objective = objective;
	}

	public ProcessId getProcessId() {
		return processId;
	}

	public AssignmentDescriptor getAssignmentDescriptor() {
		return assignmentDescriptor;
	}

	public String getObjective() {
		return objective;
	}
}
