package pl.compan.docusafe.core.office.workflow;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.WorkflowTabAction;

/** klasa przeniesiona z WorkflowTabAction
 * 	ziarno, u�ywane, wi�c na razie dost�pne w akcjach
 */
public class PlannedAssignment {
	/**
     * Wszyscy w dziale.
     */
    public static final String SCOPE_DIVISION = WorkflowFactory.SCOPE_DIVISION;
    /**
     * Wszyscy w dziale i w dzia�ach podrz�dnych.
     */
    public static final String SCOPE_SUBDIVISIONS = WorkflowFactory.SCOPE_SUBDIVISIONS;
    /**
     * Dowolny (jeden) u�ytkownik w dziale.
     */
    public static final String SCOPE_ONEUSER = WorkflowFactory.SCOPE_ONEUSER;

    public static final String KIND_SECOND_OPINION = WorkflowUtils.iwfSecondOpinionName();
    public static final String KIND_CC = WorkflowFactory.wfCcName();
    /**
     * @deprecated Sprawdza�, czy proces wymaga w�asnej kopii poprzez
     * warto�� atrybutu ds_needs_own_document w procesie.
     */
    public static final String KIND_EXECUTE = WorkflowFactory.wfManualName();

    private String id;
    private String divisionGuid;
    private String username;
    private String kind;
    private String scope;
    private String objective;

    private DSDivision division;
    private DSUser user;

    private static final Pattern ptn = Pattern.compile("^([^/]*)/([^/]*)/([^/]*)/([^/]*)/([^/]*)$");

    private static StringManager sm
    = GlobalPreferences.loadPropertiesFile(WorkflowTabAction.class.getPackage().getName(),null);
    
    public PlannedAssignment(String id) throws EdmException
    {
        this.id = id.trim();

        Matcher matcher = ptn.matcher(this.id);
        if (!matcher.matches())
            throw new EdmException(sm.getString("NiepoprawnaPostacIdentyfikatoraPlanowanejDekretacji")+this.id);

        divisionGuid = matcher.group(1);
        
        username = matcher.group(2);
        kind = matcher.group(3);
        scope = matcher.group(4);
        objective = matcher.group(5);

        loadUserAndDivision();
    }
    
    public String getId() {
    	return divisionGuid+"/"+username+"/"+kind+"/"+scope+"/"+objective;
    }

    public void loadUserAndDivision() throws EdmException
    {
        if (!StringUtils.isEmpty(username))
        {
            user = DSUser.findByUsername(username);
            if (user.isDeleted())
                throw new UserNotFoundException(username);
        }

        if (!divisionGuid.equals("null")) { 
        	division = DSDivision.find(getDivisionGuid());
        	division.getPrettyPath(); // lazy
        }
    }

    public String getKindDescription()
    {
        if (PlannedAssignment.KIND_EXECUTE.equals(kind))
            return sm.getString("doRealizacji");
        else if (PlannedAssignment.KIND_CC.equals(kind))
            return sm.getString("doWiadomosci");
        else if (PlannedAssignment.KIND_SECOND_OPINION.equals(kind))
            return sm.getString("Konsultacje");
        else
            return "[nieznany: "+kind+"]";
    }

    public String getScopeDescription()
    {
        sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
        sm = GlobalPreferences.loadPropertiesFile("",null);
        if (scope.length() > 0)
        {
            if (PlannedAssignment.SCOPE_DIVISION.equals(scope))
                return sm.getString("wszyscyWdziale");
            else if (PlannedAssignment.SCOPE_SUBDIVISIONS.equals(scope))
                return sm.getString("WszyscyWdzialeIWdzialachPodrzednych");
            else if (PlannedAssignment.SCOPE_ONEUSER.equals(scope))
                return sm.getString("DowolnyJedenUzytkownikWdziale");
            else
                return "[nieznany: "+scope+"]";
        }

        return null;
    }

    public DSDivision getDivision()
    {
        return division;
    }

    public DSUser getUser()
    {
        return user;
    }

    public String getKind()
    {
        return kind;
    }

    public String getScope()
    {
        return scope;
    }

    public String getObjective()
    {
        return objective;
    }
    public String toString()
    {
        return getClass().getName()+"[division="+division+
            " user="+user+" kind="+getKindDescription()+
            " scope="+getScopeDescription()+" objective="+objective+"]";
    }

	public String getDivisionGuid() {
		return divisionGuid;
	}

	public String getUsername() {
		return username;
	}

	public void setDivisionGuid(String divisionGuid) throws EdmException {
		this.divisionGuid = divisionGuid;
		try {
			division = DSDivision.find(divisionGuid);
			division.getPrettyPath();
		} catch (DivisionNotFoundException e) {
			throw new EdmException(e.getMessage());
		}
    	
	}
}
