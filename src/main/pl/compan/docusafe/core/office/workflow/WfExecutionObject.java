package pl.compan.docusafe.core.office.workflow;

import java.util.Date;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfExecutionObject.java,v 1.4 2008/09/09 10:33:21 pecet4 Exp $
 */
public interface WfExecutionObject
{
    String key() throws WorkflowException;
    String name() throws WorkflowException;
    String state() throws WorkflowException;
    String description() throws WorkflowException;
    /**
     * Metoda zwracaj�ca tablic� parametr�w zdefiniowanych w procesie.
     * Po ewentualnych modyfikacjach tej tablicy nale�y przekaza� j�
     * do metody set_process_context(), aby zmiany zosta�y uwzgl�dnione
     * przez serwer workflow.
     */
    NameValue[] process_context() throws WorkflowException;
    void set_process_context(NameValue[] context) throws WorkflowException;
    Date last_state_time() throws WorkflowException;
    String getWorkflowName() throws WorkflowException;
}
