package pl.compan.docusafe.core.office.workflow.snapshot;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.logic.AttachmentsManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * @TODO - troche malo obiektowo rozwiazano temat wspolnych operacji ze SnapshotUtils - trzeba to zrobic elegancko przez
 * Dziedziczenie
 * 
 * UWAGA - Snapshot ma jedna duza wade - zle zbudowane iteratory dla uzytkownika i calosci - iteruje po calej 
 * bazie. Powinien iterowac tylko po aktywnych procesach. 
 * Zlaczenie wyniku z takim pytaniem powinno dac pozytywny rezultat:
 * 
 * select vi.longvalue_ from jbpm_processinstance pi, jbpm_variableinstance vi where vi.name_= 'docId' and vi.token_ = pi.id_ and pi.end_ is not null
 * 
 * 
 * @author wkutyla
 *
 */
public class JBPMSnapshotProvider extends BaseSnapshotProvider
{
	private final static Logger log = LoggerFactory.getLogger(JBPMSnapshotProvider.class);


    protected String getTable(){ return "dsw_jbpm_tasklist"; }
    protected String getReskey(){ return "assigned_resource"; }
    protected String getDocumentIdKey(){ return "document_id"; }

	@Override
	public void updateTaskList(Session session,Long id, String doctype,int updateType,String username) throws Exception
	{
		Transaction tx = null;
		log.debug("Update id={}, doctype={}",id,doctype);                        
		PreparedStatement psIn = null;
		PreparedStatement psOut = null;
		PreparedStatement psOrd = null;
		try
		{		
			
			deleteFromSnapshot(session,id,updateType,doctype, username);                                
			//mapa, ktora zawiera pary w postaci id dockinda <--> nazwa dockinda
			Map<Integer, String> dockindNames = new HashMap<Integer, String>();
			JBPMTaskSnapshot task;
	
			tx = session.beginTransaction();
			
			
			if(doctype.equals(InOfficeDocument.TYPE) || doctype.equals("anyType"))
			{
				log.debug("IN");
				String sqlIn = getInSQL(username, updateType);
				psIn = DSApi.context().prepareStatement(sqlIn);
				setUpSnapshotStatement(psIn, updateType, username, id);
				ResultSet rsIn = psIn.executeQuery();
				
				/* p�tla po dokumentach */
				
				while(rsIn.next())
				{                	
					
					Long docId = rsIn.getLong(1);
					log.trace("Odswiezam dla documentId={}",docId);
					WorkflowFactory wff = WorkflowFactory.getInstance();
					String[] tasksIds = WorkflowFactory.findDocumentTasks(docId);
					for (int i = 0;  i < tasksIds.length; i++) {
						tasksIds[i] = wff.keyOfId(tasksIds[i]);
					}
					
					/* p�tla po procesach dokumenu */
					
					
					for (String taskId : tasksIds) 
					{
						
						String[] users = wff.getTaskUsers(taskId);
						
						/* petla po aktorach procesu */
						for (String user : users) 
						{
							//jezeli update jest typu 2 (uzytkownik chce odswiezyc swoja liste)
							//to dodaje wpisy tylko dla niego
							if (updateType == 2 && id != SYNCHRO_CODE && !user.equals(DSApi.context().getPrincipalName())) {
								continue;
							}
							task = new JBPMTaskSnapshot();
					
							setProcessDependentAttributes(task, taskId, user);
							
							setTaskByInResultSet(task, rsIn,dockindNames);
							session.save(task);
							log.trace("zapisalem user={}, docId={},taskId={}",user,task.getDocumentId(),taskId);
						}
						
					}
					
				}
				rsIn.close();
				DSApi.context().closeStatement(psIn);
				psIn = null;
			} 

			if(doctype.equals(OutOfficeDocument.TYPE) || doctype.equals(OutOfficeDocument.INTERNAL_TYPE) || doctype.equals("anyType"))
			{
				log.debug("OUT");
				String sqlOut = getOutSQL(username, updateType);
				psOut = DSApi.context().prepareStatement(sqlOut);
				setUpSnapshotStatement(psOut, updateType, username, id);
				ResultSet rsOut = psOut.executeQuery();
				while(rsOut.next())
				{                	
					
					Long docId = rsOut.getLong(1);
					WorkflowFactory wff = WorkflowFactory.getInstance();
					String[] tasksIds = WorkflowFactory.findDocumentTasks(docId);
					for (int i = 0;  i < tasksIds.length; i++) {
						tasksIds[i] = wff.keyOfId(tasksIds[i]);
					}
					
					/* p�tla po procesach dokumenu */			
					for (String taskId : tasksIds) {
						String[] users = wff.getTaskUsers(taskId);
						
						/* petla po aktorach procesu */
						for (String user : users) {
							task = new JBPMTaskSnapshot();
							setProcessDependentAttributes(task, taskId, user);
							setTaskByOutResultSet(task, rsOut,dockindNames);
							session.save(task);
							log.trace("zapisalem user={}, docId={},taskId={}",user,task.getDocumentId(),taskId);
						}
					}
					
				}
				rsOut.close();
				DSApi.context().closeStatement(psOut);
				psOut = null;
				
			}
        /* */
			if(doctype.equals(OfficeOrder.TYPE) || doctype.equals("anyType"))
			{
				log.debug("ORD");
				String sqlOrd = getOrdSQL(username, updateType);
				psOrd = DSApi.context().prepareStatement(sqlOrd);
				setUpSnapshotStatement(psOrd, updateType, username, id);
				ResultSet rsOrd = psOrd.executeQuery();

				while(rsOrd.next())
				{                	
					
					Long docId = rsOrd.getLong(1);
					WorkflowFactory wff = WorkflowFactory.getInstance();
					String[] tasksIds = WorkflowFactory.findDocumentTasks(docId);
					for (int i = 0;  i < tasksIds.length; i++) {
						tasksIds[i] = wff.keyOfId(tasksIds[i]);
					}
					
					/* p�tla po procesach dokumenu */			
					for (String taskId : tasksIds) {
						String[] users = wff.getTaskUsers(taskId);
						
						/* petla po aktorach procesu */
						for (String user : users) {
							task = new JBPMTaskSnapshot();
							setProcessDependentAttributes(task, taskId, user);
							setTaskByOrdResultSet(task, rsOrd,dockindNames);
							session.save(task);
							log.trace("zapisalem user={}, docId={},taskId={}",user,task.getDocumentId(),taskId);
						}
					}					
				}
				
				rsOrd.close();
				DSApi.context().closeStatement(psOrd);				
				psOrd = null;
			}	
			
			tx.commit();
			log.debug("commit transakcji " + tx + " wykonany ");
			tx = null;
     }        
     finally
     {
    	 DSApi.context().closeStatement(psIn);
    	 DSApi.context().closeStatement(psOut);
    	 DSApi.context().closeStatement(psOrd);
            if (tx!=null){
				try	{ 
					log.debug("Wykonuje rollback transackji " + tx );
					tx.rollback();
				} catch (Exception eee)  { 
					log.debug("Rollback transakcji " + tx + " niemozliwy",eee);	
				} 
			}
     }
}
	
	static final String ACTIVE_PROCESSES_SUBQUERY = "select vi.longvalue_ from jbpm_processinstance pi, jbpm_variableinstance vi where vi.name_= 'docId' and vi.processinstance_ = pi.id_ and pi.end_ is null";
	@Override
	protected String prepareInSQL(String username,int updateType)
	{
		String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
		boolean countReplies = Configuration.isAdditionOn(Configuration.ADDITION_COUNT_REPLIES_ON_TASKLIST);
		
		return
		
		"select distinct " + //1
        "doc.id, " +		 
        "doc.officenumber, " +
        "doc.officenumberyear, \n" +
        "dso_person.firstname,\n" +  //5
        "dso_person.lastname,\n" +
        "dso_person.organization,\n" +
        "dso_person.street,\n" +
        "dso_person.location,\n" +
        "doc.summary,\n"+ //10
        "doc.referenceid,\n"+
        "dsdoc.source,\n"+
        "dso_container.id,"+
        "dso_container.officeid, "+
        "doc.creatingUser, "+ //15
        "doc.clerk, "+
        (countReplies ? "(select count(indocument) from dso_out_document where indocument=doc.id), " : "0, ")+  //ilosc odpowiedzi na pismo
        "doc.incomingDate,"+
        "dsdoc.dockind_id,"+
        "dsdoc.ctime, "+
        "doc.replyunnecessary, "+
        "dsdoc.lastRemark, "+
        "doc.DOCUMENTDATE, " +
        "doc.dailyOfficeNumber "+//24
        "from\n" +														
        "dso_in_document doc "+
        "inner join ds_document dsdoc "+nolock+" on (doc.id = dsdoc.id "+ 
        (updateType == 1 ? "and doc.id = ? ":"and doc.id in ("+ACTIVE_PROCESSES_SUBQUERY+")" )+
        ") \n"+
        "left outer join dso_person "+nolock+" on (doc.sender = dso_person.id and dso_person.discriminator='SENDER')\n"+
        "left outer join dso_container"+nolock+" on doc.case_id = dso_container.id\n";
	}
	
	@Override
	protected String prepareOutSQL(String username,int updateType)
	{
		String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
		//boolean countReplies = Configuration.isAdditionOn(Configuration.ADDITION_COUNT_REPLIES_ON_TASKLIST);
		
		return 
		
		"select distinct " + //1
        "doc.id, " +
        "doc.officenumber, " +
        "doc.officenumberyear, \n" +
        "dso_person.firstname,\n" + //5
        "dso_person.lastname,\n" +
        "dso_person.organization,\n" +
        "dso_person.street,\n" +
        "dso_person.location,\n" +
        "doc.summary,\n"+ //10
        "doc.internaldocument,\n"+
        "dsdoc.source,\n"+
        "rcpt.firstname,\n" +
        "rcpt.lastname,\n" +
        "rcpt.organization,\n" + //15
        "rcpt.street,\n" +
        "rcpt.location,\n" +
        "dso_container.id,"+
        "dso_container.officeid, "+
        "doc.creatingUser,"+ //20
        "doc.prepstate,"+
        "deliv.name, "+
        "doc.clerk, "+
        "doc.currentassignmentguid as currentassignmentguid, "+
        "dsdoc.ctime, "+ //25
        "dsdoc.dockind_id, "+
        "dsdoc.lastRemark, "+
        "doc.DOCUMENTDATE, "+
        "doc.referenceid, "+
        "doc.dailyOfficeNumber "+//30
        "from\n" + 
        "dso_out_document doc "+
        "inner join ds_document dsdoc "+nolock+" on (doc.id = dsdoc.id "+ 
        (updateType == 1 ? "and doc.id = ? ":"and doc.id in ("+ACTIVE_PROCESSES_SUBQUERY+")" )+
        ") \n"+
        "left outer join dso_person "+nolock+" on (doc.sender = dso_person.id and dso_person.discriminator='SENDER')\n"+
        "left outer join dso_person rcpt "+nolock+" on (doc.id = rcpt.document_id and (rcpt.posn = 0 or rcpt.posn IS NULL))\n"+
        "left outer join dso_out_document_delivery deliv "+nolock+" on (doc.delivery = deliv.id)\n"+
        "left outer join dso_container"+nolock+" on doc.case_id = dso_container.id\n";

	}
    
	@Override
	protected String prepareOrdSQL(String username,int updateType)
	{
		String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
		return "select " +
        "doc.id, " + //1
        "doc.officenumber, " +
        "doc.officenumberyear, \n" +
        "doc.summary,\n"+
        "dsdoc.source,\n"+ //5
        "dso_container.id,"+
        "dso_container.officeid, "+
        "doc.creatingUser, "+
        "dsdoc.ctime, "+ 
        "dsdoc.lastRemark, "+//10
        "doc.dailyOfficeNumber "+//11
        "from\n" +
        "dso_order doc "+
        "inner join ds_document dsdoc "+nolock+" on (doc.id = dsdoc.id "+ 
        (updateType == 1 ? "and doc.id = ? ":"and doc.id in ("+ACTIVE_PROCESSES_SUBQUERY+")" )+
        ") \n"+
        "left outer join dso_container"+nolock+" on doc.case_id = dso_container.id\n";

	}
	
	/**
	 * 
	 * @param task
	 * @param rsIn
	 * @param dockindNames
	 * @throws SQLException
	 * @throws EdmException
	 */
	protected void setTaskByInResultSet(JBPMTaskSnapshot task, ResultSet rsIn,Map<Integer, String> dockindNames) throws SQLException,EdmException
	{
        task.setDocumentType(InOfficeDocument.TYPE);
        long docid = rsIn.getLong(1);
        log.debug("Inicjuje tast dla document_id = "+docid);
        task.setDocumentId(docid);
        task.setLabelhidden(hasHiddingLabel(docid, task.getDsw_resource_res_key()));
        task.setIsAnyImageInAttachments(AttachmentsManager.isAnyImageInAttachments(docid));
		task.setIsAnyPdfInAttachments(AttachmentsManager.isAnyPdfInAttachments(docid));
        int ko = rsIn.getInt(4);
        if(!rsIn.wasNull())
        	task.setDocumentOfficeNumber(ko);
        int dko = rsIn.getInt(27);
        if(!rsIn.wasNull())
        	task.setDailyOfficeNumber(dko);
        int koYear = rsIn.getInt(3);
        if (!rsIn.wasNull())
            task.setDocumentOfficeNumberYear(new Integer(koYear));
        task.setPerson_firstname(rsIn.getString(4));
        task.setPerson_lastname(rsIn.getString(5));
        task.setPerson_organization(rsIn.getString(6));
        task.setPerson_street(rsIn.getString(7));
        task.setPerson_location(rsIn.getString(8));
        task.setDocumentSummary(cutString(rsIn.getString(9), 254, "DocumentSummary"));
        task.setDocumentReferenceId(rsIn.getString(10));
        task.setDocument_source(rsIn.getString(11));
        task.setCaseId(rsIn.getLong(12));
        task.setOfficeCase(rsIn.getString(13));
        task.setDocumentAuthor(rsIn.getString(14));
        task.setDocumentClerk(rsIn.getString(15));
        if(rsIn.getInt(16) == 0 )
        	task.setAnswerCounter(rsIn.getInt(16));
        else
        	task.setAnswerCounter(-1);
        task.setIncomingDate(rsIn.getTimestamp(17));                  
        
        int dockind_id = rsIn.getInt(18);
    
        if(dockind_id!=0)
        {
        	setDockindFields(docid, dockind_id, task);
        }
        else
        {
        	task.setDocumentDate(rsIn.getDate(23));
        }
        
        
        task.setDocumentCtimeAndCdate(rsIn.getTimestamp(19));
        task.setLastRemark(cutString(rsIn.getString(21), 240, "LastRemark"));
        task.setBackupPerson(false);
        try
        {
        	if(!rsIn.getBoolean(2) && task.getDivisionGuid() != null)
        	{
        		if(DSDivision.isInDivisionAsBackup(DSDivision.find(task.getDivisionGuid()), task.getDsw_resource_res_key()))
        		{
        			task.setBackupPerson(true);
        		}
        	}
		}
        catch (Exception e)
        {
        	log.error("Poprawic to jest brana zla kolumna pod uwage",e);
        	
        }
	}
	
	/**
	 * 
	 * @param task
	 * @param rsOut
	 * @param dockindNames
	 * @throws SQLException
	 * @throws EdmException
	 */
	protected void setTaskByOutResultSet(JBPMTaskSnapshot task, ResultSet rsOut,Map<Integer, String> dockindNames) throws SQLException,EdmException
	{
		long docid = rsOut.getLong(1);
        log.debug("Inicjuje task dla document_id = "+docid);
        task.setDocumentId(docid);
        task.setLabelhidden(hasHiddingLabel(docid, task.getDsw_resource_res_key()));
        task.setIsAnyImageInAttachments(AttachmentsManager.isAnyImageInAttachments(docid));
		task.setIsAnyPdfInAttachments(AttachmentsManager.isAnyPdfInAttachments(docid));
        int ko = rsOut.getInt(4);
        if(!rsOut.wasNull())
        	task.setDocumentOfficeNumber(ko);
        int dko = rsOut.getInt(27);
        if(!rsOut.wasNull())
        	task.setDailyOfficeNumber(dko);
        int koYear = rsOut.getInt(3);
        if (!rsOut.wasNull())
            task.setDocumentOfficeNumberYear(new Integer(koYear));
        
        
        task.setPerson_firstname(rsOut.getString(4));
        task.setPerson_lastname(rsOut.getString(5));
        task.setPerson_organization(rsOut.getString(6));
        task.setPerson_street(rsOut.getString(7));
        task.setPerson_location(rsOut.getString(8));
        task.setDocumentSummary(rsOut.getString(9));
        
        String type;
        if(rsOut.getBoolean(10)) type = OutOfficeDocument.INTERNAL_TYPE;
        else type=OutOfficeDocument.TYPE;
        task.setDocumentType(type);
        
        task.setDocument_source(rsOut.getString(11));

        task.setRcpt_firstname(rsOut.getString(12));
        task.setRcpt_lastname(rsOut.getString(13));
        task.setRcpt_organization(rsOut.getString(14));
        task.setRcpt_street(rsOut.getString(15));
        task.setRcpt_location(rsOut.getString(16));
        task.setCaseId(rsOut.getLong(17));
        task.setOfficeCase(rsOut.getString(18));
        
        task.setDocumentAuthor(rsOut.getString(19));
        task.setDocumentPrepState(OutOfficeDocument.getPrepStateString(rsOut.getString(20)));
        task.setDocumentDelivery(rsOut.getString(21));
        task.setDocumentClerk(rsOut.getString(22));
        
        task.setDocumentCtimeAndCdate(rsOut.getTimestamp(24));
        
        task.setLastRemark(rsOut.getString(26));
        
        int dockind_id = rsOut.getInt(25);
        
        if(dockind_id!=0)
        {
        	setDockindFields(docid, dockind_id, task);
        }
        else
        {
        	task.setDocumentDate(rsOut.getDate(27));
        }
        
        task.setDocumentReferenceId(rsOut.getString(28));
        task.setBackupPerson(false);
        try
        {
        	if(!task.isAccepted() && task.getDivisionGuid() != null)
        	{
        		if(DSDivision.isInDivisionAsBackup(DSDivision.find(task.getDivisionGuid()), task.getDsw_resource_res_key()))
        		{
        			task.setBackupPerson(true);
        		}
        	}
        }
        catch (Exception e)
        {
        	log.error("Poprawic to kurka wodna",e);
        }
	}
	
	/**
	 * 
	 * @param task
	 * @param rsOrd
	 * @param dockindNames
	 * @throws SQLException
	 * @throws EdmException
	 */
	protected void setTaskByOrdResultSet(JBPMTaskSnapshot task, ResultSet rsOrd,Map<Integer, String> dockindNames) throws SQLException,EdmException
	{

        task.setDocumentId(rsOrd.getLong(1));
        int ko = rsOrd.getInt(4);
        if(!rsOrd.wasNull())
        	task.setDocumentOfficeNumber(ko);
        int dko = rsOrd.getInt(27);
        if(!rsOrd.wasNull())
        	task.setDailyOfficeNumber(dko);
        int koYear = rsOrd.getInt(3);
        if (!rsOrd.wasNull())
            task.setDocumentOfficeNumberYear(new Integer(koYear));
        
        task.setPerson_firstname("");
        task.setPerson_lastname("");
        task.setPerson_organization("");
        task.setPerson_street("");
        task.setPerson_location("");
        task.setDocumentSummary(rsOrd.getString(4));
        task.setDocument_source(rsOrd.getString(5));
        task.setDocumentType(OfficeOrder.TYPE);
        task.setCaseId(rsOrd.getLong(6));
        task.setOfficeCase(rsOrd.getString(7));
        task.setDocumentAuthor(rsOrd.getString(8));
        task.setDocumentCtimeAndCdate(rsOrd.getTimestamp(9));
        task.setLastRemark(rsOrd.getString(10));        
        task.setLabelhidden(hasHiddingLabel(task.getDocumentId(), task.getDsw_resource_res_key()));
        task.setIsAnyImageInAttachments(AttachmentsManager.isAnyImageInAttachments(task.getDocumentId()));
		task.setIsAnyPdfInAttachments(AttachmentsManager.isAnyPdfInAttachments(task.getDocumentId()));
	}
	
	/**
	 * 
	 * @param task
	 * @param taskId
	 * @param user
	 * @throws EdmException
	 */
	protected void setProcessDependentAttributes(JBPMTaskSnapshot task, String taskId, String user) throws EdmException 
	{
		if (log.isTraceEnabled())
		{
			log.trace("setProcessDependentAttributes taskId={}, user={}",taskId,user);
		}
		WorkflowActivity activity = WorkflowFactory.getActivityByTaskId(taskId);
		task.setAccepted(activity.getAccepted());
		task.setRcvAndReceiveDate(activity.getReceiveDate());
		task.setActivity_lastActivityUser(activity.getLastUser());
	    task.setObjective(activity.getObjective());
	    task.setActivityKey(activity.getTaskId());
	    task.setDescription(activity.getProcessDesc());
	    task.setProcess(activity.getProcessKind());
	    task.setDsw_resource_res_key(user);
	    task.setDeadlineTime(activity.getDeadlineTime());
	    task.setDeadlineRemark(activity.getDeadlineRemark());
	    task.setDeadlineAuthor(activity.getDeadlineAuthor());
	    task.setDivisionGuid(activity.getCurrentGuid());
		
	}
	
	
  
  protected void setUpSnapshotStatement(PreparedStatement ps, int updateType, String username,Long id) throws SQLException
  {
	  if (log.isTraceEnabled())
	  {
		  log.trace("setUpSnapshotStatement");
		  log.trace("updateType={}",updateType);
		  log.trace("username={}",username);
		  log.trace("id={}",id);
	  }
	  //FIXME na razie odpuszczam update pism usera
	  if(updateType == 1 && id != null)
    		ps.setLong(1, id);
	  /*
	  if(username != null)
      {
      	ps.setString(1, username);
      	 if( updateType == 1 && id != null)
           	ps.setLong(2, id);
      }
      else
      {
      	if(updateType == 1 && id != null)
      		ps.setLong(1, id);
      }*/
  }

	@Override
	public TaskPositionBean getTaskPosition(String activityKey) 
	{
		return null;
	}
}
