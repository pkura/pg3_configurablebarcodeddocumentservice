package pl.compan.docusafe.core.office.workflow.internal;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Random;
import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.NameValue;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.WfProcess;
import pl.compan.docusafe.core.office.workflow.WorkflowException;
import pl.compan.docusafe.core.office.workflow.xpdl.Activity;

/**
 * XXX: WfActivity ma w�asn? kopi� zmiennych procesu.
 * Czy set_result jest konieczne? Mo�e przepisywanie warto?ci po complete()?
 * �ycie powi?zanych WfActivity ko�czy si� po complete()
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfActivityImpl.java,v 1.15 2009/04/16 09:55:46 mariuszk Exp $
 */
public class WfActivityImpl
    extends WfExecutionObjectImpl
    implements WfActivity, Lifecycle
{
	static final Logger log = LoggerFactory.getLogger(WfActivityImpl.class);
	protected static Random random = new Random();
    private Long id;
    private String activityDefinitionId;
    /**
     * Zbi�r obiekt�w NameValueImpl.
     */
    private Set<NameValue> processContext;
    private WfProcessImpl process;

    public Activity getActivityDefinition() throws WorkflowException
    {
        return InternalWorkflowService.getXpdlPackage(process.getPackageDefinition().getId()).
            getProcess(process.getProcessDefinitionId()).getActivity(activityDefinitionId);
    }

    public String activity_definition_id() throws WorkflowException
    {
        return activityDefinitionId;
    }

    /**
     * Zwraca zmienne zwi?zane z krokiem procesu.
     * @return Zmienne procesu lub pusta tablica, nigdy null.
     * @throws WorkflowException
     */
    public NameValue[] process_context() throws WorkflowException
    {
        if (processContext == null)
            return new NameValue[0];

        return processContext.toArray(new NameValue[processContext.size()]);
    }

    /**
     * Metoda nic nie robi, zmiany w warto?ciach par NameValue
     * odnosz? skutek od razu po wywo�aniu NameValue.value().set().
     */
    public void set_process_context(NameValue[] context) throws WorkflowException
    {
    }

    /**
     * Wywo�ywane po zako�czeniu wykonywania zadania. Powoduje przej?cie
     * procesu do kolejnego kroku (lub jego zako�czenie).
     * TODO: nie pozwoli� na complete() je�eli WfAssignment nie jest zaakceptowane
     * @see WfProcessImpl#complete(pl.compan.docusafe.core.office.workflow.internal.WfActivityImpl)
     */
    public void complete() throws WorkflowException
    {
        getProcess().complete(this);
    }

    /**
     * Metoda nic nie robi.
     */
    public void set_result(NameValue[] result) throws WorkflowException
    {
    }

    /**
     * R�wnowa�ne z wywo�aniem {@link #process_context()}.
     */
    public NameValue[] result() throws WorkflowException
    {
        return process_context();
    }

    /**
     * @see WfProcessImpl#getExtendedAttributes(pl.compan.docusafe.core.office.workflow.internal.WfActivityImpl)
     */
    public NameValue[] extended_attributes() throws WorkflowException
    {
        return process.getExtendedAttributes(this);
    }

    public WfProcess container() throws WorkflowException
    {
        return process;
    }

    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }

    public String getActivityDefinitionId()
    {
        return activityDefinitionId;
    }

    void setActivityDefinitionId(String activityDefinitionId)
    {
        this.activityDefinitionId = activityDefinitionId;
    }

    public Set getProcessContext()
    {
        return processContext;
    }

    void setProcessContext(Set<NameValue> processContext)
    {
        this.processContext = processContext;
    }

    public WfProcessImpl getProcess()
    {
        return process;
    }

    void setProcess(WfProcessImpl process)
    {
        this.process = process;
    }


    /**
     * Zwraca liczbe losowa
     * Jest synchronizowana, bo nie wiem czy Random jest thread safe
     * Dla elegancji zwracam zawsze dodatni wynik
     * @return
     */
    protected synchronized int randomSeed()
    {
    	int rnd = random.nextInt();
    	if (rnd <0)
    		rnd = -1* rnd;
    	return rnd;
    }
    /* Lifecycle */

    public boolean onSave(Session s) throws CallbackException
    {
        if (key == null)
        {
            long time = System.currentTimeMillis();
            if (id != null)
            {

                key = /*activityDefinitionId*/"or_" +time+"_"+id;
                log.trace("id!=null key={}",key);
            }
            else
            {
            	int rnd = randomSeed();
            	key = "or_" + time + "_" + rnd;
            	log.trace("klucz losowany key={}",key);
             }
        }
        if (lastStateTime == null) lastStateTime = new Date(); 
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }

    public String toString()
    {
        return getClass().getName()+"[id="+id+" key="+key+
            " activityDefinitionId="+activityDefinitionId+"]";
    }
}
