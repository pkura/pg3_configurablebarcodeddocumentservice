package pl.compan.docusafe.core.office.workflow.internal;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.AnyObject;
import pl.compan.docusafe.core.office.workflow.NameValue;
import pl.compan.docusafe.core.office.workflow.WorkflowException;
import pl.compan.docusafe.core.office.workflow.xpdl.DataType;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Implementacja NameValue nie przeznaczona do zapisywania
 * w bazie danych. Jest u�ywana do przedstawiania warto�ci
 * tylko do odczytu, o warto�ciach ustalonych z g�ry, np.
 * Activity.extendedAttributes.
 * <p>
 * Warto�� pary jest przeznaczona wy��cznie do odczytu, pr�ba
 * zmiany warto�ci skutkuje rzuceniem wyj�tku.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NameValueTransientImpl.java,v 1.2 2008/07/23 13:03:26 mariuszk Exp $
 */
public class NameValueTransientImpl implements NameValue, PropertyChangeListener
{
    private String name;
    private AnyObject any;
    private Integer valueInt;

	public NameValueTransientImpl(String name, String value, DataType dataType)
        throws WorkflowException
    {
        this.name = name;
        AnyObjectKindImpl kind = new AnyObjectKindImpl(dataType);
        try
        {
            any = new AnyObjectImpl(kind, dataType.getValue(value));
            ((AnyObjectImpl) any).addPropertyChangeListener(this);
        }
        catch (EdmException e)
        {
            throw new WorkflowException(e.getMessage(), e);
        }
    }

    public String name()
    {
        return name;
    }

    /**
     * Zwraca warto�� pary. Metoda set() zwr�conego obiektu nie mo�e
     * by� wywo�ywana (rzuca wyj�tek).
     */
    public AnyObject value() throws EdmException
    {
        return any;
    }

    public void propertyChange(PropertyChangeEvent evt)
    {
        throw new UnsupportedOperationException("Modyfikacja warto�ci nie jest" +
            " mo�liwa");
    }

    public String toString()
    {
        return getClass().getName()+"[name="+name+" value="+any+"]";
    }

	public void setValueInt(Long valueInt) 
	{
		this.valueInt = valueInt.intValue();
	}
	
    public Integer getValueInt()
    {
		return valueInt;
	}

	public void setValueInt(Integer valueInt) 
	{
		this.valueInt = valueInt;
	}

	public Integer valueInt() 
	{
		return valueInt;
	}
}
