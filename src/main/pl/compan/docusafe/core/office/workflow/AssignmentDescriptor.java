package pl.compan.docusafe.core.office.workflow;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

/* User: Administrator, Date: 2005-12-08 13:07:24 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: AssignmentDescriptor.java,v 1.8 2008/07/29 08:37:44 pecet1 Exp $
 */
public class AssignmentDescriptor {
	private String username;

	private String guid;

	public AssignmentDescriptor(String username, String guid) {
		if(username != null && username.equalsIgnoreCase("null"))
			this.username = null;
		else
			this.username = username;
		
		this.guid = guid;
	}

	public static AssignmentDescriptor forDescriptor(String descriptor)
			throws IllegalArgumentException {
		String[] asg = descriptor.split(";", 2);
		if (asg.length < 2)
			throw new IllegalArgumentException("Niepoprawny deskryptor: "
					+ descriptor);

		return new AssignmentDescriptor(asg[0], asg[1]);
	}

	public static AssignmentDescriptor createDescriptor(String username,
			String guid) throws IllegalArgumentException {
		return new AssignmentDescriptor(username, guid);
	}

	public String toDescriptor() {
		return username + ";" + guid;
	}

	public String format() throws EdmException {
		if (getUsername() != null)
			return DSUser.safeToFirstnameLastname(username) + " ("
				+ DSDivision.safeGetName(guid) + ")";
		else return " (" + DSDivision.safeGetName(guid) + ")";
	}

	public String getUsername() {
		return username;
	}

	public String getGuid() {
		return guid;
	}
}
