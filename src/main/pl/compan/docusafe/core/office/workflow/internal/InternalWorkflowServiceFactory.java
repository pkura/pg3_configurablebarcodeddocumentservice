package pl.compan.docusafe.core.office.workflow.internal;

import pl.compan.docusafe.core.office.workflow.WfService;
import pl.compan.docusafe.core.office.workflow.WorkflowException;
import pl.compan.docusafe.core.office.workflow.WorkflowServiceFactory;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InternalWorkflowServiceFactory.java,v 1.3 2006/02/06 16:05:49 lk Exp $
 */
public class InternalWorkflowServiceFactory extends WorkflowServiceFactory
{
    public InternalWorkflowServiceFactory()
    {
    }

    public WfService newService() throws WorkflowException
    {
        return new InternalWorkflowService();
    }
}
