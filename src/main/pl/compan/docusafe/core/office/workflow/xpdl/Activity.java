package pl.compan.docusafe.core.office.workflow.xpdl;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;

import java.util.*;

/**
 * Klasa opisuj�ca element Activity nale��cy do procesu zdefiniowanego
 * w pliku XPDL.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Activity.java,v 1.3 2006/02/06 16:05:50 lk Exp $
 */
public class Activity
{
    private WorkflowProcess process;
    private Participant performer;
    private String id;
    private String name;
    private String description;
    private boolean route;
    /**
     * "Automatic", "Manual" lub null.
     */
    private String startMode;
    /**
     * "Automatic", "Manual" lub null.
     */
    private String finishMode;
    private Map<String, ExtendedAttribute> extendedAttributes = new HashMap<String, ExtendedAttribute>();
    private List<Transition> transitions = new LinkedList<Transition>();

    Activity(WorkflowProcess process, Element elActivity) throws EdmException
    {
        this.process = process;
        this.id = elActivity.attribute("Id").getValue();
        if (id == null)
            throw new EdmException("Brak identyfikatora kroku.");

        this.name = elActivity.attribute("Name") != null ?
            elActivity.attribute("Name").getValue() : null;
        this.description = elActivity.element("Description") != null ?
            elActivity.element("Description").getTextTrim() : null;
        this.route = elActivity.element("Route") != null;

        // tryb rozpocz�cia kroku
        if (elActivity.element("StartMode") != null)
        {
            if (elActivity.element("StartMode").element("Automatic") != null)
                startMode = "Automatic";
            else if (elActivity.element("StartMode").element("Manual") != null)
                startMode = "Manual";
            else
                throw new EdmException("Nieznany tryb startu kroku "+process.getId()+";"+id);
        }

        // tryb zako�czenia kroku
        if (elActivity.element("FinishMode") != null)
        {
            if (elActivity.element("FinishMode").element("Automatic") != null)
                finishMode = "Automatic";
            else if (elActivity.element("FinishMode").element("Manual") != null)
                finishMode = "Manual";
            else
                throw new EdmException("Nieznany tryb zako�czenia kroku "+process.getId()+";"+id);
        }

        // rozszerzone atrybuty
        if (elActivity.element("ExtendedAttributes") != null)
        {
            List elExtendedAttributeList = elActivity.element("ExtendedAttributes").
                elements("ExtendedAttribute");
            if (elExtendedAttributeList != null && elExtendedAttributeList.size() > 0)
            {
                for (Iterator iter=elExtendedAttributeList.iterator(); iter.hasNext(); )
                {
                    Element elExtendedAttribute = (Element) iter.next();
                    ExtendedAttribute ea = new ExtendedAttribute(elExtendedAttribute);
                    extendedAttributes.put(ea.getName(), ea);
                }
            }
        }

        // pobieranie obiektu Participant z definicji procesu
        if (elActivity.element("Performer") != null)
        {
            String performerId = elActivity.element("Performer").getTextTrim();
            this.performer = process.getParticipant(performerId);

            // uczestnik typu "free text expression"
            // wyra�eniem jest jego identyfikator (id)
            ExtendedAttribute perfId = getExtendedAttribute("ParticipantID");
            if (this.performer == null && perfId != null &&
                perfId.getValue().equals("FreeTextExpressionParticipant"))
            {
                this.performer = new Participant(process, "FreeTextExpressionParticipant",
                    null, "SYSTEM", true);
            }

            if (this.performer == null)
                throw new EdmException("W definicji kroku "+process.getId()+";"+id+" u�yto " +
                    "nieznanej nazwy uczestnika: "+elActivity.element("Performer").getTextTrim());
        }

    }

    void addTransition(Transition transition) throws EdmException
    {
        if (transition.getFrom() != this)
            throw new EdmException("Pr�ba dodania przej�cia, kt�rego pocz�tkiem " +
                "nie jest bie��cy krok procesu ("+process.getId()+";"+id+")");

        transitions.add(transition);
    }

    public WorkflowProcess getProcess()
    {
        return process;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public boolean isRoute()
    {
        return route;
    }

    /**
     * Uczestnik procesu. Nie zawsze jest to jeden z obiekt�w Participant
     * obecnych w definicji procesu - w szczeg�lnym przypadku mo�e to by�
     * uczestnik okre�lony dowolnym wyra�eniem, w�wczas metoda
     * {@link Participant#isExpression()} zwraca warto�� true, a wyra�eniem
     * jest identyfikator uczestnika.
     */ 
    public Participant getPerformer()
    {
        return performer;
    }

    /**
     * Spos�b rozpocz�cia kroku.
     * @return "Automatic", "Manual" lub null.
     */
    public String getStartMode()
    {
        return startMode;
    }

    /**
     * Spos�b zako�czenia kroku.
     * @return "Automatic", "Manual" lub null.
     */
    public String getFinishMode()
    {
        return finishMode;
    }

    public ExtendedAttribute getExtendedAttribute(String name)
    {
        return (ExtendedAttribute) extendedAttributes.get(name);
    }

    public ExtendedAttribute[] getExtendedAttributes()
    {
        return (ExtendedAttribute[]) extendedAttributes.values().
            toArray(new ExtendedAttribute[extendedAttributes.size()]);
    }

    /**
     * Zwraca tablic� przej�� do kolejnych krok�w. Zwracana tablica mo�e by� pusta.
     */ 
    public Transition[] getTransitions()
    {
        return (Transition[]) transitions.toArray(new Transition[transitions.size()]);
    }

    public String toString()
    {
        return "<Activity id="+id+" name="+name+" description="+description+
            " route="+route+" startMode="+startMode+" finishMode="+finishMode+
            " transitions="+transitions+">";
    }
}
