package pl.compan.docusafe.core.office.workflow;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.zxing.Result;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.Type;

import org.springframework.util.CollectionUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.bookmarks.FilterCondition;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.labels.DocumentToLabelBean;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.GroupClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class JBPMTaskSnapshot extends TaskSnapshot {
    private static StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
    private String newProcessStatus;

    public static final Map<String, String> COLUMN_TASKLIST_MAPPING = ImmutableMap.<String, String>builder()
            .put("receiveDate", "last_state_time")
            .put("description", "description")
            .put("process", "process_name")
            .put("documentSummary", "summary")
            .put("documentReferenceId", "reference_id")
            .put("documentSender", "person_firstname")
            .put("sender", "last_user")
            .put("officeCase", "case_office_id")
            .put("objective", "objective")
            .put("documentDelivery", "document_delivery")
            .put("documentPrepState", "document_prep_state")
            .put("answerCounter", "answer_counter")
            .put("lastRemark", "last_remark")
            .put("documentId", "document_id")
            .put("dockindBusinessAtr1", "dockind_business_atr_1")
            .put("dockindBusinessAtr2", "dockind_business_atr_2")
            .put("dockindBusinessAtr3", "dockind_business_atr_3")
            .put("dockindBusinessAtr4", "dockind_business_atr_4")
            .put("dockindBusinessAtr6", "dockind_business_atr_6")
            .put("dockindBusinessAtr5", "dockind_business_atr_5")
            .put("dockindKategoria", "dockind_kategoria")
            .put("dockindKwota", "dockind_kwota")
            .put("dockindName", "dockind_name")
            .put("dockindNumerDokumentu", "dockind_numer_dokumentu")
            .put("dockindStatus", "dockind_status")
            .put("documentDate", "document_date")
            .put("documentCtime", "document_ctime")
            .put("decretationTo", "decretation_to")
            .put("decretation_to_div", "decretation_to_div")
            .build();

    @Override
    public JBPMTaskSnapshot clone() {
        JBPMTaskSnapshot ret = (JBPMTaskSnapshot)super.clone();
        ret.newProcessStatus = this.newProcessStatus;
        return ret;
    }

    public static boolean isOnLoggedTaskList(Document document) throws EdmException {
        boolean isOnList = false;
        List<JBPMTaskSnapshot> tasks = findByDocumentIdAndResourceKey(document.getId(), DSApi.context().getPrincipalName());
        if(!CollectionUtils.isEmpty(tasks))
            isOnList = true;
        return isOnList;
    }

    @Override
    public SearchResultsAdapter<TaskSnapshot> search(TaskSnapshot.Query query) throws EdmException
    {
        FromClause from = new FromClause();
        final TableAlias tasklist = from.createTable("dsw_jbpm_tasklist");
        final WhereClause where = new WhereClause();
        TableAlias asgnHistory = null;
        if (query.exTasks)
        {
            asgnHistory = from.createTable("dso_document_asgn_history");
            where.add(Expression.eqAttribute(asgnHistory.attribute("document_id"), tasklist.attribute("document_id")));

            Junction OR = Expression.disjunction();
            OR.add(Expression.eq(asgnHistory.attribute("sourceUser"), query.exTasksUser));
            //OR.add(Expression.eq(asgnHistory.attribute("targetUser"), query.exTasksUser));

            where.add(OR);

            where.add(Expression.ne(tasklist.attribute("assigned_resource"), query.exTasksUser));
        }
        if (query.division != null)
        {
        	DSUser[] dsUser = query.division.getUsers(true);
        	if(dsUser != null && dsUser.length > 0)
        	{
        		String[] usernames = new String[dsUser.length];
        		for (int i = 0; i < dsUser.length; i++)
        		{
        			usernames[i] = dsUser[i].getName();
				}
        		where.add( Expression.in(tasklist.attribute("assigned_resource"), usernames));
        	}
        	else
        	{
        		//jesli nie ma nikogo w dziale (a po nim wyszukujemy) to ma nic nie zwrocic. Bo inaczej zwraca wsyzstko 
        		return new SearchResultsAdapter<TaskSnapshot>(new ArrayList<TaskSnapshot>(0), 0, 0,
                        TaskSnapshot.class);
        	}
        }
        if(query.user!=null){
            if (query.substitutions || query.BOK)
            {
                DSUser[] substs = query.user.getSubstitutedUsersForNow(new Date());
                int length = substs.length+1+(query.BOK ? 1 : 0);
                String[] users = new String[length];
                if (query.substitutions)
                {
                    users[0]=query.user.getName();
                    for (int i=0; i < substs.length; i++)
                    {
                        users[i+1] = substs[i].getName();
                    }
                }
                if (query.BOK)
                    users[substs.length + 1] = "$bok";

                where.add(Expression.in(tasklist.attribute("assigned_resource"), users));
            }
            else where.add(Expression.eq(tasklist.attribute("assigned_resource"), query.user.getName()));
        }

        if (query.documentType != null)
            where.add(Expression.eq(tasklist.attribute("document_type"), query.documentType));
        if (query.officeNumber != null)
            where.add(Expression.eq(tasklist.attribute("office_number"), query.officeNumber.toString()));
        if (query.documentAuthor != null)
            where.add(Expression.eq(tasklist.attribute("document_author"), query.documentAuthor));
        if (query.documentId != null)
            where.add(Expression.eq(tasklist.attribute("document_id"), query.documentId));
        else if (query.documentIds != null && query.documentIds.length > 0)
            where.add(Expression.in(tasklist.attribute("document_id"), query.documentIds));
        
		if (query.taskInPackage != null && query.dockindId != null && !query.taskInPackage)
		{
			where.add(Expression.ne(tasklist.attribute("dockindid"), query.dockindId));
			where.add(Expression.eq(tasklist.attribute("taskinpackage"), query.taskInPackage));

		} else if (query.taskInPackage != null && query.dockindId != null && query.taskInPackage)
		{
			where.add(Expression.eq(tasklist.attribute("dockindid"), query.dockindId));
		}

        
        if(query.withBackup!=null && query.withBackup.equals(Boolean.FALSE))
        {
        	 Junction OR = Expression.disjunction();
             OR.add(Expression.ne(tasklist.attribute("backup_person"), true));
             OR.add(Expression.eq(tasklist.attribute("backup_person"), null));
             where.add(OR);
        }

        if (query.filterConditions != null) {
        	for(FilterCondition fc : query.filterConditions) {
        		where.add(Expression.in(tasklist.attribute(fc.getColumnTableName()), fc.getParsedValues().toArray() ));
        	}
        }
        
        
        /*****************************************/
        /// przeniesione z Tasksnapshot.java
        /** Dodaje linki do etykietek obowiazkowych */
        if (query.getLabelsFiltering()!=null && query.getLabelsFiltering().size()>0)
        {
        	TableAlias documentToLabel = from.createTable("dso_document_to_label");
    	    where.add(Expression.eqAttribute(documentToLabel.attribute("document_id"), tasklist.attribute("document_id")));

    		if("or".equals(Docusafe.getAdditionProperty("tasklist.label.operator")))
    		{
    			Junction OR = Expression.disjunction();
    			java.util.Iterator<Label> it =query.getLabelsFiltering().iterator();
 	        	while (it.hasNext())
 	        	{
 	        		Label label = (Label) it.next();
 	        		OR.add(Expression.eq(documentToLabel.attribute("label_id"), label.getId().toString()));
 	        	}
    			where.add(OR);
    		}
    		else
    		{
    			Criteria criteria = DSApi.context().session().createCriteria(DocumentToLabelBean.class);
				
    			Junction orJunction = Expression.disjunction();
    			
	        	java.util.Iterator<Label> it =query.getLabelsFiltering().iterator();
	        	List<Long> list = new ArrayList<Long>();
	        	
	        	while (it.hasNext())
	        	{
	        		Label label = (Label) it.next();
	        		list.add(label.getId());
	        	}
	        	
	        	criteria.add(Restrictions.in("labelId",list));
	        	criteria.setProjection( Projections.sqlGroupProjection("document_id AS dId", "document_id having count(*) = "+list.size(), new String[]{"dId"}, new Type[]{Hibernate.LONG}) );
	        
	        	List<Long> documentToLabelBeans = criteria.list();
	        	
	        	java.util.Iterator<Long> it2 = documentToLabelBeans.iterator();
	        	while (it2.hasNext())
	        	{
	        		Long documentId = (Long) it2.next();
	        		orJunction.add(Expression.eq(documentToLabel.attribute("document_id"), documentId));
	        	}
	        	
	        	if(documentToLabelBeans.isEmpty())
	        		orJunction.add(Expression.eq(documentToLabel.attribute("document_id"), null));
	        	
	        	where.add(orJunction);
    		}
        }
        else if (AvailabilityManager.isAvailable("labels") && !query.isLabelsAll())
        {
    		if("true".equals(Docusafe.getAdditionProperty("tasklist.show.disapproved")))
			{
    			Junction OR = Expression.disjunction();
    			OR.add(Expression.eq(tasklist.attribute("LABELHIDDEN"), false));
    			OR.add(Expression.eq(tasklist.attribute("dsw_assignment_accepted"), false));
	        	where.add(OR);
			}
    		else
    		{
    			where.add(Expression.eq(tasklist.attribute("LABELHIDDEN"), false));
    		}
        }
        /*****************************************/
       
        // przekazywana jest nazwa kolumny -> kicha
//        log.debug(""+query.filterBy+" "+query.filterValue);
        if (query.filterBy != null && query.filterValue != null) {
        	if(query.filterValue.getClass().equals(java.util.Date.class) )
        	{
        		Timestamp ts = new Timestamp( ((Date)query.filterValue).getTime());
        		where.add(Expression.ge(tasklist.attribute(query.filterBy),ts));
        		Timestamp ts2 = new Timestamp( ((Date)query.filterValue).getTime());
        		ts2.setHours(24);
        		where.add(Expression.le(tasklist.attribute(query.filterBy),ts2));

            } else if(query.filterBy.equalsIgnoreCase("last_user")) {
        		//narazie jest hax na mariusza wypociny , trzeba przerobic filtr na taksliscie zeby odrazu dawal dobre kolumny
            	//chyba hax na wasze wypociny kt�re co� popsu�y
        		//jakism dziwnym trafem hax juz nie jest potrzebny, ale nadal trzeba przerobic filtry w tasklistaction
            	//nie dziwnym trafem tylko kiedy� dzia�a�o do poki nie popsu�e� i trzeba by�o dawac hax bo zjeb..les
				PreparedStatement ps = null;
				try
				{
					List<String> names = new ArrayList<String>();
					if (DSApi.isPostgresServer())
					{
						ps = DSApi.context().prepareStatement("select distinct name from ds_user where lower (FIRSTNAME || Lastname)  like ?");
					} else
					{
						ps = DSApi.context().prepareStatement("select distinct name from ds_user where lower(FIRSTNAME+' '+LASTNAME) like ?");
					}
					ps.setString(1, "%" + query.filterValue.toString().toLowerCase() + "%");
					ResultSet rs = ps.executeQuery();
					log.trace("filtrujemy po uzytkowniku {}", query.filterValue.toString().toLowerCase());
					while (rs.next())
					{
        				String nn = rs.getString("name");
        				log.trace("filtrujemy po {}",nn);
        				names.add(nn);
        			}
        			rs.close();
        			DSApi.context().closeStatement(ps);
        			ps = null;
        			if(names.size() > 0)
        			{
        				where.add(Expression.in(tasklist.attribute("last_user"), names.toArray()));
        			}
        			else
        			{
        				where.add(Expression.eq(tasklist.attribute(query.filterBy), "cos_co_nie_istnieje"));
        			}
        		}
        		catch (Exception e) 
        		{
					log.debug("",e);					
				}
        		finally
        		{
        			DSApi.context().closeStatement(ps);
        		}
        		
        		//where.add(Expression.eq(tasklist.attribute(query.filterBy), "admin"));
        	}
        	
    		
    		else if(AvailabilityManager.isAvailable("tasklist.filtruj.contaisAnywhere") && query.filterValue instanceof String)
			{
    			where.add(Expression.like(tasklist.attribute(query.filterBy),"%"+query.filterValue+"%",true));
			}
    		else if (AvailabilityManager.isAvailable("tasklist.filtruj.startWithe"))
    		{
    			where.add(Expression.like(tasklist.attribute(query.filterBy),query.filterValue+"%",true));
    		}
    		// o co chodzi ??
        	else if(query.filterBy.equalsIgnoreCase("dockind_business_atr_1"))
        	{
        		where.add(Expression.like(tasklist.attribute(query.filterBy),"%"+query.filterValue+"%", true));  
        	}
        	else
        	{
        		where.add(Expression.eq(tasklist.attribute(query.filterBy),query.filterValue));            
        	}
        }
        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = null;
        GroupClause group = new GroupClause();
        if (query.isWatches())
        {
        	selectId.addSql(tasklist.getAlias()+".document_id");
        	idCol = selectId.addSql("min("+tasklist.getAlias()+".id)");
        	group.add(tasklist.attribute("document_id"));
        }
        else if(AvailabilityManager.isAvailable("tasklist.substitutions.filtering"))
        {
        	selectId.addSql(tasklist.getAlias()+".activity_key");
        	idCol = selectId.addSql("min("+tasklist.getAlias()+".id)");
        	group.add(tasklist.attribute("activity_key"));
        }
        else
        {
        	idCol = selectId.add(tasklist, "id");
        	group.add(tasklist.attribute("id"));
        }  

        OrderClause order = new OrderClause();

        //FIXME - to troche lamerska konstrukcja, to powinno byc zapisane w jakiejs mapie, albo propertiesach
        if ("receiveDate".equals(query.sortField)){
        	group.add(tasklist.attribute("last_state_time"));
            order.add(tasklist.attribute("last_state_time"), query.ascending);
            selectId.add(tasklist, "last_state_time");
        }
        else if ("description".equals(query.sortField)){
        	group.add(tasklist.attribute("description"));
            order.add(tasklist.attribute("description"),query.ascending);
            selectId.add(tasklist, "description");
        }
        else if ("process".equals(query.sortField)){
        	group.add(tasklist.attribute("process_name"));
            order.add(tasklist.attribute("process_name"), query.ascending);
            selectId.add(tasklist, "process_name");
        }
        else if ("documentSummary".equals(query.sortField)){
        	group.add(tasklist.attribute("summary"));
            order.add(tasklist.attribute("summary"), query.ascending);
            selectId.add(tasklist, "summary");
        }
        else if ("documentReferenceId".equals(query.sortField)){
        	group.add(tasklist.attribute("reference_id"));
            order.add(tasklist.attribute("reference_id"),query.ascending);
            selectId.add(tasklist, "reference_id");
        }
        else if ("documentSender".equals(query.sortField)){
        	group.add(tasklist.attribute("person_firstname"));
           order.add(tasklist.attribute("person_firstname"), query.ascending);
            selectId.add(tasklist, "person_firstname");
        }
        else if ("sender".equals(query.sortField)){
        	group.add(tasklist.attribute("last_user"));
            order.add(tasklist.attribute("last_user"), query.ascending);
            selectId.add(tasklist, "last_user");
        }
        else if ("officeCase".equals(query.sortField)){
        	group.add(tasklist.attribute("case_office_id"));
            order.add(tasklist.attribute("case_office_id"), query.ascending);
            selectId.add(tasklist, "case_office_id");
        }
        else if ("objective".equals(query.sortField)){
        	group.add(tasklist.attribute("objective"));
            order.add(tasklist.attribute("objective"), query.ascending);
            selectId.add(tasklist, "objective");
        }
        else if ("documentDelivery".equals(query.sortField)){
        	group.add(tasklist.attribute("document_delivery"));
            order.add(tasklist.attribute("document_delivery"), query.ascending);
            selectId.add(tasklist, "document_delivery");
        }
        else if ("documentPrepState".equals(query.sortField)){
        	group.add(tasklist.attribute("document_prep_state"));
            order.add(tasklist.attribute("document_prep_state"), query.ascending);
            selectId.add(tasklist, "document_prep_state");
        }
        else if ("answerCounter".equals(query.sortField)){
        	group.add(tasklist.attribute("answer_counter"));
        	order.add(tasklist.attribute("answer_counter"), query.ascending);
            selectId.add(tasklist, "answer_counter");
        }
        else if("lastRemark".equals(query.sortField))
        {
        	group.add(tasklist.attribute("last_remark"));
        	order.add(tasklist.attribute("last_remark"), !query.ascending);
        	selectId.add(tasklist,"last_remark");
        }  
        else if("documentId".equals(query.sortField))
        {
        	group.add(tasklist.attribute("document_id"));
        	log.trace("sortowanie dokument_id");
        	order.add(tasklist.attribute("document_id"), query.ascending);
        	selectId.add(tasklist,"document_id");
        } 
        else if("dockindBusinessAtr1".equals(query.sortField))
        {
        	group.add(tasklist.attribute("dockind_business_atr_1"));
        	order.add(tasklist.attribute("dockind_business_atr_1"), query.ascending);
        	selectId.add(tasklist,"dockind_business_atr_1");
        }
        else if("dockindBusinessAtr2".equals(query.sortField))
        {
        	group.add(tasklist.attribute("dockind_business_atr_2"));
        	order.add(tasklist.attribute("dockind_business_atr_2"), query.ascending);
        	selectId.add(tasklist,"dockind_business_atr_2");
        }
        else if("dockindBusinessAtr3".equals(query.sortField))
        {
        	group.add(tasklist.attribute("dockind_business_atr_3"));
        	order.add(tasklist.attribute("dockind_business_atr_3"), query.ascending);
        	selectId.add(tasklist,"dockind_business_atr_3");
        }
        else if("dockindBusinessAtr4".equals(query.sortField))
        {
        	group.add(tasklist.attribute("dockind_business_atr_4"));
        	order.add(tasklist.attribute("dockind_business_atr_4"), query.ascending);
        	selectId.add(tasklist,"dockind_business_atr_4");
        }
        else if("dockindBusinessAtr6".equals(query.sortField))
        {
        	group.add(tasklist.attribute("dockind_business_atr_6"));
        	order.add(tasklist.attribute("dockind_business_atr_6"), query.ascending);
        	selectId.add(tasklist,"dockind_business_atr_6");
        }
        else if("dockindBusinessAtr5".equals(query.sortField))
        {
        	group.add(tasklist.attribute("dockind_business_atr_5"));
        	order.add(tasklist.attribute("dockind_business_atr_5"), query.ascending);
        	selectId.add(tasklist,"dockind_business_atr_5");
        }
        else if("dockindKategoria".equals(query.sortField))
        {
        	group.add(tasklist.attribute("dockind_kategoria"));
        	order.add(tasklist.attribute("dockind_kategoria"), query.ascending);
        	selectId.add(tasklist,"dockind_kategoria");
        }
        else if("dockindKwota".equals(query.sortField))
        {
        	group.add(tasklist.attribute("dockind_kwota"));
        	order.add(tasklist.attribute("dockind_kwota"), query.ascending);
        	selectId.add(tasklist,"dockind_kwota");
        }
        else if("dockindName".equals(query.sortField))
        {
        	group.add(tasklist.attribute("dockind_name"));
        	order.add(tasklist.attribute("dockind_name"), query.ascending);
        	selectId.add(tasklist,"dockind_name");
        }
        else if("dockindNumerDokumentu".equals(query.sortField))
        {
        	group.add(tasklist.attribute("dockind_numer_dokumentu"));
        	order.add(tasklist.attribute("dockind_numer_dokumentu"), query.ascending);
        	selectId.add(tasklist,"dockind_numer_dokumentu");
        }
        else if("dockindStatus".equals(query.sortField))
        {
        	group.add(tasklist.attribute("dockind_status"));
        	order.add(tasklist.attribute("dockind_status"), query.ascending);
        	selectId.add(tasklist,"dockind_status");
        }
        else if("documentDate".equals(query.sortField))
        {
        	group.add(tasklist.attribute("document_date"));
        	order.add(tasklist.attribute("document_date"), query.ascending);
        	selectId.add(tasklist,"document_date");
        }
        else if("documentCtime".equals(query.sortField))
        {
        	group.add(tasklist.attribute("document_ctime"));
        	order.add(tasklist.attribute("document_ctime"), query.ascending);
        	selectId.add(tasklist,"document_ctime");
        }
        else if("decretationTo".equals(query.sortField))
        {
        	group.add(tasklist.attribute("decretation_to"));
        	order.add(tasklist.attribute("decretation_to"), query.ascending);
        	selectId.add(tasklist,"decretation_to");
        }
        else if("decretation_to_div".equals(query.sortField))
        {
        	group.add(tasklist.attribute("decretation_to_div"));
        	order.add(tasklist.attribute("decretation_to_div"), query.ascending);
        	selectId.add(tasklist,"decretation_to_div");
        }
        else 
        {
        	//group.add(tasklist.attribute("office_number"));
            order.add(tasklist.attribute("office_number"), query.ascending);
           // selectId.add(tasklist, "office_number");
            log.trace("domyslne sortowanie po officeNumber");
        }
        group.add(tasklist.attribute("office_number"));
        selectId.add(tasklist, "office_number");
        log.debug("sortowanie po {},{}",query.sortField,query.ascending);
        
        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = null;
        
        if(AvailabilityManager.isAvailable("tasklist.substitutions.filtering")) {
        	countCol = selectCount.addSql("count(distinct "+tasklist.getAlias()+".activity_key)");
        } else {
        	countCol = selectCount.addSql("count(distinct "+tasklist.getAlias()+".id)");
        }
 
        int totalCount;
        SelectQuery selectQuery;
        ResultSet rs = null;
        try
        { 
            selectQuery = new SelectQuery(selectCount, from, where, null);
            
            rs = selectQuery.resultSet();

            log.info("selectQuery.sql = " + selectQuery.getSql());

            if (!rs.next()) {
                throw new EdmException("Nie mo�na pobra� liczby zada�.");
            }
            
            totalCount = rs.getInt(countCol.getPosition());
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());
            
            if (query.getLimit() > 0) {
                selectQuery.setMaxResults(query.getLimit());
                selectQuery.setFirstResult(query.getOffset());
            }

            if(AvailabilityManager.isAvailable("tasklist.substitutions.filtering") || query.isWatches()) {
            	selectQuery = new SelectQuery(selectId, from, where, order,group);
            } else {
            	selectQuery = new SelectQuery(selectId, from, where, order);
            }

            if (query.getLimit() > 0) {
                selectQuery.setMaxResults(query.getLimit());
                selectQuery.setFirstResult(query.getOffset());
            }

            if (log.isDebugEnabled())  {
            	log.debug(selectQuery.getSql());
            }
            
            log.info("selectQuery.sql = " + selectQuery.getSql());
            
            rs = selectQuery.resultSet();
            ArrayList<TaskSnapshot> results = new ArrayList<TaskSnapshot>(query.getLimit());
            JBPMTaskSnapshot temp;
         //   boolean odbiorNaDzial = query.user == null ? false : DSApi.context().hasPermission(query.user, DSPermission.WWF_ODBIOR_DEKRETACJI_NA_DZIAL);

            while (rs.next()) {
                Long id = rs.getLong(idCol.getPosition());
                temp = DSApi.context().load(JBPMTaskSnapshot.class, id);
           /*     if (query.user != null && !odbiorNaDzial && temp.assigned_user == null && !temp.externalWorkflow)
                {
                    continue;
                }*/
                if (!results.contains(temp))
               	 results.add(temp);                
            }
            
            /*ArrayList<Long> ids = new ArrayList<Long>();
            for(TaskSnapshot r : results)
            	ids.add(r.getDocumentId());

    		ArrayList<Long> done = new ArrayList<Long>(); // lista dokmentow ktore juz byly porownywane
    		int counter = 0;
    		boolean exists = false;
    		int i = 0;
            while(i < results.size())
            {
            	exists = false;
            	counter = 0;
            	for(Long lo : done)
            	{
            		if(lo.equals(results.get(i).getDocumentId()))
            			exists = true;
            	}
            	if(exists)
            	{
            		i++;
            		continue;
            	}
            	else
            	{
        			done.add(results.get(i).getDocumentId());
            		counter = 0;
            		for(int j = 0; j < ids.size(); j++)
            		{
            			if(results.get(i).getDocumentId().equals(ids.get(j)))
            				counter++;
            		}
            		if(counter > 1)
            		{
            			results.remove(i);  
            			totalCount--;
            		}
            		else
            			i++;
            	}
            }*/
            //zamykane w finally
            //rs.close();
            loadCurrentDockindBusinessAttributes(results);
            return new SearchResultsAdapter<TaskSnapshot>(results, query.getOffset(), totalCount,
                    TaskSnapshot.class);
        }
        catch (Exception e)
        {
            throw new EdmException(e);
        }
        finally
        {
        	try
			{
				rs.close();
				DSApi.context().closeStatement(rs.getStatement());
			} catch (Throwable e2)
			{
				log.error(e2.getMessage(), e2);
			}
        }

    }


    /*
     * Od�wie�a (nie utrwala w tabeli tasksnapshota) warto�ci atrybut�w specyficznych dla danego typu dokumentu w zadaniach maj�cych pojawi� si� na li�cie
     * Metoda przydatna gdy potrzebujemy aktualnych, dla chwili za�adowania listy, informacji o specyficznych atrybutach danych zada�
     * @param results kolekcja zada�, kt�re zostan� wy�wietlone na li�cie u�ytkownika
     * @throws EdmException
     */
    private boolean loadCurrentDockindBusinessAttributes(List<TaskSnapshot> results) throws Exception {
        DocumentKind documentKind = null;
        String documentKindCn = null;
        if(results.size() > 0){
        	//wydaje si� �e nie powinnismy sprawdzac tutaj uprawnien, 1 to �le to dzial 2 co z obci�zeniem nie po to jest lista zadan jak juz moze podejzec zadanie bo je ma u siebie lu podglada zadania pracownika dzialu 
            documentKind = Document.find(results.get(0).getDocumentId(),false).getDocumentKind();
            if(documentKind != null && documentKind.getProperties() != null){
                if(Boolean.valueOf(documentKind.getProperties().get("load-current-business-attributes"))){
                    for(TaskSnapshot snapshot : results){
//                        przeladowanie i utrwalenie aktualnych wartosci atrybutow zadania wyswietlanych na liscie zdan, aktualnie wylacznie wyswietlamy aktualne atrybuty bez utrwalania ich wartosci
//                        SnapshotUtils.getSnapshotProvider().updateTaskList(DSApi.context().session(),snapshot.getDocumentId(), results.get(0).getDocumentType(), TaskSnapshot.UPDATE_TYPE_ONE_DOC, DSApi.context().getPrincipalName());
                        TaskListParams params = documentKind.logic().getTaskListParams(documentKind, snapshot.getDocumentId(), snapshot);
                        String [] currentParamValue = new String[TaskListParams.MAX_ATTRIBUTE_COUNT];
                        for(int i = 0; i < TaskListParams.MAX_ATTRIBUTE_COUNT; i++){
                            //sprawdzamy czy tekstowa wartosci atrybutu nie przekracza dopuszczalnej dlugosci, ewentualnie ja "korygujemy"
                            currentParamValue[i] = (params.getAttribute(i).length() <= 30 ? params.getAttribute(i) : StringUtils.left(params.getAttribute(i), 27) + "...");
                        }
                        snapshot.setDockindBusinessAtrs(currentParamValue);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Task toFullTask(boolean ifFlags) throws EdmException
    {
    	if(ifFlags)
    	{
	    	this.flags = LabelsManager.getReadFlagAndLabelsBeansForDocument(documentId, DSApi.context().getPrincipalName(), false);
	    	for(FlagBean a: flags)
	    	{
	    		a.toString();
	    	}
	    	this.userFlags = LabelsManager.getReadFlagAndLabelsBeansForDocument(documentId, DSApi.context().getPrincipalName(), true);
    	}

        if(description == null) {
            description = "";
        } else {
    	    this.description = smG.getString(this.description);
        }

        //TODO uzupe�ni�
        this.setManual(false);
       
        this.documentTask=true;
        this.receiver = DSUser.safeToFirstnameLastname(this.dsw_resource_res_key);

        if (this.getDeadlineTime()!=null && (this.getDeadlineTime().getTime() < System.currentTimeMillis()))
        {
            this.setPastDeadline(true);
        }

        if (this.activity_lastActivityUser != null)
        {
            this.setSender(DSUser.safeToFirstnameLastname(this.activity_lastActivityUser));
        }
        if (this.documentAuthor != null)
        {
            this.author = DSUser.safeToFirstnameLastname(this.documentAuthor);
        }

        this.setDocumentSender(Person.getSummary(false, null,
                person_firstname,
                person_lastname,
                person_organization,
                person_street,
                person_location));
        this.setDocumentRecipient(Person.getSummary(false, null,
                rcpt_firstname,
                rcpt_lastname,
                rcpt_organization,
                rcpt_street,
                rcpt_location));
        this.setDocumentBok(Boolean.valueOf("$bok".equals(this.dsw_resource_res_key)));
        this.setDocumentFax("fax".equals(this.document_source));
        this.setDocumentCrm("crm".equals(this.document_source));

        this.setShortReceiveDate(this.getReceiveDate());

        /*if(this.getSameProcessSnapshotsCount()==1)
        {
        	decretationTo = DSUser.findByUsername(dsw_resource_res_key).asLastnameFirstname(); // lub dsw_reso   assignef
        }
        else
        {
            try {
                decretationTo = DSDivision.find(divisionGuid).getName();
            }
            catch (DivisionNotFoundException e)
            {
                // nie znaleziono dziali
                log.error(e.getMessage(), e);
            }
            catch (Exception e)
            {
                log.error(e.getMessage(), e);
            }

        }*/
        
        if ("order".equals(this.documentType))
        {
            OfficeOrder order = OfficeOrder.findOfficeOrder(this.documentId);
            if (order.getStatus() != null)
                this.orderStatus = OfficeOrder.getStatusMap().get(order.getStatus());
        }

        // Ustawienie referenta pisma
        if (documentClerk != null)
        {
            clerk = DSUser.findByUsername(documentClerk).asLastnameFirstname();
        }
        if (InOfficeDocument.TYPE.equals(getDocumentType()))
        {
        	if (answerCounter != null && answerCounter.equals(0))
        		answerCounter = null; 
        }

        if(WorkflowFactory.jbpm) {
            this.setWorkflowName(this.getProcess());
        }
        this.setActivityId(ActivityId.make(this.getWorkflowName(), this.getActivityKey()));

	log.debug("jbpm to full, manual? : {}, process: {}, workflowName {}, jbpm {}", isManual(), process, getWorkflowName(), WorkflowFactory.jbpm);
	
		updateFromActivityWorkflow(this);
		
        return this;
    }
    
    private void updateFromActivityWorkflow(JBPMTaskSnapshot jbpmTaskSnapshot)
	{
    	
    	if(this.process.equals("activiti")){
    		
    	
    	String sql = "Select * from ACT_RU_TASK where ID_ =?";
    	PreparedStatement ps = null;
    	ResultSet rs = null;
		try
		{
			ps = DSApi.context().prepareStatement(sql);
		
    			ps.setString(1, this.activityKey);
    			 rs = ps.executeQuery();
    	if(rs.next()){
		if(this.description==null || this.description.isEmpty()){
			
			this.description=	getDesriptionFromActivitiTask(rs);
			
		}
		if(this.objective==null || this.objective.isEmpty()){
			
			this.objective=	getObjectiveFromActivitiTask(rs);
			
		}
		
    	}
    	DSApi.context().session().save(this);
    	rs.close();
    	ps.close();
    	
		
		} catch (SQLException e)
		{
	    	log.error("", e);
		} catch (EdmException e)
		{
			log.error("", e);
		}
    	}
			
		
		
		
	}

    /**
     * Dlaczego w tym kodzie nie ma JAVADOCA??????????
     * @param rs
     * @return
     * @throws SQLException
     */
	private String getDesriptionFromActivitiTask(ResultSet rs) throws SQLException
	{
		if(AvailabilityManager.isAvailable("taskList.ProcessActiviti.description.from.UserTaskName")){
		String r = rs.getString("NAME_");
		return (r!=null ? r : "");
		}
		else{
			return "Do Realizacji";
		}
			
		
	}

	/**
	 * JW????????????????????????????????
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private String getObjectiveFromActivitiTask(ResultSet rs) throws SQLException
	{
		String r = rs.getString("DESCRIPTION_");
		return (r!=null ? r : "");
	}
	@Override
    public TaskSnapshot findByActivityKey(String act) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(JBPMTaskSnapshot.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.eq("activityKey",act));

            List list = c.list();

            if (list.size() > 0)
                return (JBPMTaskSnapshot) list.get(0);
            else
                return null;

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void update() throws EdmException
	{
        try
        {
            DSApi.context().session().update(this);
          
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    public static JBPMTaskSnapshot findByProcessInstance(String taskId) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(JBPMTaskSnapshot.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.eq("jbpmTaskId",Long.parseLong(taskId)));

            List list = c.list();

            if (list.size() > 0)
                return (JBPMTaskSnapshot) list.get(0);
            else
                return null;

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Metoda zwraca wszystkie Taski powiazane z dokumentem
     * @param documentId
     * @return
     * @throws EdmException
     */
    public static List<JBPMTaskSnapshot> findByDocumentId(Long documentId) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(JBPMTaskSnapshot.class);

        try
        {
        	return DSApi.context().session().createCriteria(JBPMTaskSnapshot.class)
        		.add(Restrictions.eq("documentId",documentId)).list();
            
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Metoda zwraca wszystkie taski powi�zane z dokumentem na danej li�cie u�ytkownika
     * @param documentId
     * @param resourceKey name usera
     * @return
     */
    public static List<JBPMTaskSnapshot> findByDocumentIdAndResourceKey(Long documentId, String resourceKey) throws EdmException {
        Criteria c = DSApi.context().session().createCriteria(JBPMTaskSnapshot.class);

        try
        {
            return DSApi.context().session().createCriteria(JBPMTaskSnapshot.class)
                    .add(Restrictions.eq("documentId",documentId))
                    .add(Restrictions.eq("dsw_resource_res_key", resourceKey))
                    .list();

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static List<JBPMTaskSnapshot> findByAttribute5(String attribute5) throws EdmException
    {
    	 Criteria c = DSApi.context().session().createCriteria(JBPMTaskSnapshot.class);

         try
         {
         	return DSApi.context().session().createCriteria(JBPMTaskSnapshot.class)
         		.add(Restrictions.eq("dockindBusinessAtr5",attribute5)).list();
             
         }
         catch (HibernateException e)
         {
             throw new EdmHibernateException(e);
         }
    }

    public SearchResultsAdapter<TaskSnapshot> searchForWatches(TaskSnapshot.Query query) throws EdmException {
        FromClause from = new FromClause();
        final TableAlias tasklist = from.createTable("dsw_jbpm_tasklist");
        final WhereClause where = new WhereClause();

        mainFilterSetup(query, from, tasklist, where);
        wtfFilterSetup(query, tasklist, where);

        try {
            List<Long> taskIds = getTaskSnapshotIds(tasklist, from, where);

            ArrayList<TaskSnapshot> results = getTaskSnapshots(query, from, tasklist, where, taskIds);

            return new SearchResultsAdapter<TaskSnapshot>(results, query.getOffset(), taskIds.size(),
                    TaskSnapshot.class);

        } catch (Exception e) {
            log.debug("", e);
            throw new EdmException(e);
        }
    }

    private ArrayList<TaskSnapshot> getTaskSnapshots(Query query, FromClause from, TableAlias tasklist, WhereClause where, List<Long> taskIds) throws SQLException, EdmException {
        ArrayList<TaskSnapshot> results = new ArrayList<TaskSnapshot>(query.getLimit());
        ResultSet rs =  null;
        try {
            OrderClause order = new OrderClause();
            prepareOrderClause(query, tasklist, order);

            SelectClause selectId = new SelectClause();
            SelectColumn idCol = selectId.add(tasklist, "id");
            where.add(Expression.in(tasklist.attribute("id"), taskIds.toArray()));

            SelectQuery selectQuery = new SelectQuery(selectId, from, where, order);

            if (query.getLimit() > 0) {
                selectQuery.setMaxResults(query.getLimit());
                selectQuery.setFirstResult(query.getOffset());
            }

            if (log.isDebugEnabled()) {
                log.debug(selectQuery.getSql());
            }

            log.info("selectQuery.sql = " + selectQuery.getSql());

            rs = selectQuery.resultSet();

            while (rs.next()) {
                Long id = rs.getLong(idCol.getPosition());
                results.add(DSApi.context().load(JBPMTaskSnapshot.class, id));
            }
        } finally {
            if (rs != null) {
                rs.close();
                DSApi.context().closeStatement(rs.getStatement());
            }
        }
        return results;
    }

    private List<Long> getTaskSnapshotIds(TableAlias tasklist, FromClause from, WhereClause where) throws EdmException, SQLException {
        List<Long> taskIds = Lists.newArrayList();
        SelectClause selectTaskId = new SelectClause();
        selectTaskId.addSql(tasklist.getAlias() + ".document_id");
        SelectColumn idCol = selectTaskId.addSql("max(" + tasklist.getAlias() + ".id)");
        SelectQuery selectQuery = new SelectQuery(selectTaskId, from, where, null, new GroupClause().add(tasklist.attribute("document_id")));

        ResultSet rs = null;
        try {
            rs = selectQuery.resultSet();

            while (rs.next()) {
                taskIds.add(rs.getLong(idCol.getPosition()));
            }
        } finally {
            if (rs != null) {
                rs.close();
                DSApi.context().closeStatement(rs.getStatement());
            }
        }

        return taskIds;
    }

    private void mainFilterSetup(Query query, FromClause from, TableAlias tasklist, WhereClause where) throws EdmException {
        if (query.user != null) {
            if (query.substitutions || query.BOK) {
                DSUser[] substs = query.user.getSubstituted();
                int length = substs.length + 1 + (query.BOK ? 1 : 0);
                String[] users = new String[length];
                if (query.substitutions) {
                    users[0] = query.user.getName();
                    for (int i = 0; i < substs.length; i++) {
                        users[i + 1] = substs[i].getName();
                    }
                }
                if (query.BOK)
                    users[substs.length + 1] = "$bok";

                where.add(Expression.in(tasklist.attribute("assigned_resource"), users));
            } else where.add(Expression.eq(tasklist.attribute("assigned_resource"), query.user.getName()));
        }


        if (query.documentType != null)
            where.add(Expression.eq(tasklist.attribute("document_type"), query.documentType));
        if (query.officeNumber != null)
            where.add(Expression.eq(tasklist.attribute("office_number"), query.officeNumber.toString()));
        if (query.documentAuthor != null)
            where.add(Expression.eq(tasklist.attribute("document_author"), query.documentAuthor));
        if (query.documentId != null)
            where.add(Expression.eq(tasklist.attribute("document_id"), query.documentId));
        else if (query.documentIds != null && query.documentIds.length > 0)
            where.add(Expression.in(tasklist.attribute("document_id"), query.documentIds));
        if (query.withBackup != null && query.withBackup.equals(Boolean.FALSE)) {
            Junction OR = Expression.disjunction();
            OR.add(Expression.ne(tasklist.attribute("backup_person"), true));
            OR.add(Expression.eq(tasklist.attribute("backup_person"), null));
            where.add(OR);
        }

        if (query.filterConditions != null) {
            for (FilterCondition fc : query.filterConditions) {
                where.add(Expression.in(tasklist.attribute(fc.getColumnTableName()), fc.getParsedValues().toArray()));
            }
        }


        /*****************************************/
        /// przeniesione z Tasksnapshot.java
        /** Dodaje linki do etykietek obowiazkowych */
        if (query.getLabelsFiltering() != null && query.getLabelsFiltering().size() > 0) {
            TableAlias documentToLabel = from.createTable("dso_document_to_label");
            where.add(Expression.eqAttribute(documentToLabel.attribute("document_id"), tasklist.attribute("document_id")));

            if ("or".equals(Docusafe.getAdditionProperty("tasklist.label.operator"))) {
                Junction OR = Expression.disjunction();
                Iterator<Label> it = query.getLabelsFiltering().iterator();
                while (it.hasNext()) {
                    Label label = (Label) it.next();
                    OR.add(Expression.eq(documentToLabel.attribute("label_id"), label.getId().toString()));
                }
                where.add(OR);
            } else {
                Iterator<Label> it = query.getLabelsFiltering().iterator();
                while (it.hasNext()) {
                    Label label = (Label) it.next();
                    where.add(Expression.eq(documentToLabel.attribute("label_id"), label.getId().toString()));
                }
            }
        } else if (AvailabilityManager.isAvailable("labels") && !query.isLabelsAll()) {
            if ("true".equals(Docusafe.getAdditionProperty("tasklist.show.disapproved"))) {
                Junction OR = Expression.disjunction();
                OR.add(Expression.eq(tasklist.attribute("LABELHIDDEN"), false));
                OR.add(Expression.eq(tasklist.attribute("dsw_assignment_accepted"), false));
                where.add(OR);
            } else {
                where.add(Expression.eq(tasklist.attribute("LABELHIDDEN"), false));
            }
        }
    }

    private void prepareOrderClause(Query query, TableAlias tasklist, OrderClause order) {
        String column = COLUMN_TASKLIST_MAPPING.get(query.sortField);

        if (column != null) {
            order.add(tasklist.attribute(column), query.ascending);
        } else {
            order.add(tasklist.attribute("office_number"), query.ascending);
        }
    }

    private void wtfFilterSetup(Query query, TableAlias tasklist, WhereClause where) {
        if (query.filterBy != null && query.filterValue != null) {
            if (query.filterValue.getClass().equals(Date.class)) {
                Timestamp ts = new Timestamp(((Date) query.filterValue).getTime());
                where.add(Expression.ge(tasklist.attribute(query.filterBy), ts));
                Timestamp ts2 = new Timestamp(((Date) query.filterValue).getTime());
                ts2.setHours(24);
                where.add(Expression.le(tasklist.attribute(query.filterBy), ts2));

            } else if (query.filterBy.equalsIgnoreCase("last_user")) {
                //narazie jest hax na mariusza wypociny , trzeba przerobic filtr na taksliscie zeby odrazu dawal dobre kolumny
                //chyba hax na wasze wypociny kt�re co� popsu�y
                //jakism dziwnym trafem hax juz nie jest potrzebny, ale nadal trzeba przerobic filtry w tasklistaction
                //nie dziwnym trafem tylko kiedy� dzia�a�o do poki nie popsu�e� i trzeba by�o dawac hax bo zjeb..les
                PreparedStatement ps = null;
                try {
                    List<String> names = new ArrayList<String>();
                    ps = DSApi.context().prepareStatement("select distinct name from ds_user where lower(FIRSTNAME+' '+LASTNAME) like ?");
                    ps.setString(1, "%" + query.filterValue.toString().toLowerCase() + "%");
                    ResultSet rs = ps.executeQuery();
                    log.trace("filtrujemy po uzytkowniku {}", query.filterValue.toString().toLowerCase());
                    while (rs.next()) {
                        String nn = rs.getString("name");
                        log.trace("filtrujemy po {}", nn);
                        names.add(nn);
                    }
                    rs.close();
                    DSApi.context().closeStatement(ps);
                    ps = null;
                    if (names.size() > 0) {
                        where.add(Expression.in(tasklist.attribute("last_user"), names.toArray()));
                    } else {
                        where.add(Expression.eq(tasklist.attribute(query.filterBy), "cos_co_nie_istnieje"));
                    }
                } catch (Exception e) {
                    log.debug("", e);
                } finally {
                    DSApi.context().closeStatement(ps);
                }

                //where.add(Expression.eq(tasklist.attribute(query.filterBy), "admin"));
            } else if (AvailabilityManager.isAvailable("tasklist.filtruj.contaisAnywhere")) {
                where.add(Expression.like(tasklist.attribute(query.filterBy), "%" + query.filterValue + "%", true));
            } else if (AvailabilityManager.isAvailable("tasklist.filtruj.startWithe")) {
                where.add(Expression.like(tasklist.attribute(query.filterBy), query.filterValue + "%", true));
            }
            // o co chodzi ??
            else if (query.filterBy.equalsIgnoreCase("dockind_business_atr_1")) {
                where.add(Expression.like(tasklist.attribute(query.filterBy), "%" + query.filterValue + "%", true));
            } else {
                where.add(Expression.eq(tasklist.attribute(query.filterBy), query.filterValue));
            }
        }
    }

    public String getNewProcessStatus()
	{
		return newProcessStatus;
	}

	public void setNewProcessStatus(String newProcessStatus)
	{
		this.newProcessStatus = newProcessStatus;
	}
}