package pl.compan.docusafe.core.office.workflow;

/**
 * Interfejs, kt�ry powinien by� implementowany przez klasy
 * realizuj�ce dost�p do konkretnego serwera workflow.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfService.java,v 1.4 2006/02/06 16:05:51 lk Exp $
 */
public interface WfService
{
    /**
     * Nazwa serwisu workflow. Ta sama nazwa mo�e by� u�yta w metodzie
     * {@link WorkflowServiceFactory#newInstance(java.lang.String)}.
     */
    String getName();

    void connect(String username, String password)
        throws ConnectFailedException;

    void connect(String username)
        throws ConnectFailedException;

    void connectAdmin()
        throws ConnectFailedException;

    void disconnect()
        throws WorkflowException;

    /**
     * Wywo�uje {@link #disconnect()} ignoruj�c wszystkie wyj�tki.
     */
    void _disconnect();

    /**
     * Zalogowany u�ytkownik (zas�b).
     */
    WfResource getResourceObject() throws NotConnectedException, WorkflowException;

    WfResource[] get_sequence_resource() throws WorkflowException;

    public String[] get_sequence_resource_key() throws WorkflowException;

    public void create_resource(final String username, final String password,
                                final String realName, final String emailAddress,
                                final String ldapEntryDn) throws WorkflowException;

    public void removeResource(final String username,String toUser) throws WorkflowException;

    /**
     * Zwraca tablic� obiekt�w WfProcessMgr o podanych identyfikatorach
     * pakietu i procesu.
     * @param packageId
     * @param processDefinitionId
     * @return
     * @throws WorkflowException
     */
    WfProcessMgr[] getProcessMgrs(String packageId, String processDefinitionId)
        throws WorkflowException;

    public WfProcessMgr[] getAllProcessMgrs() throws WorkflowException;

    public NameValue[] getParticipantToUsernameMappings(final String packageId,
                                                        final String processDefinitionId,
                                                        final String participantId)
        throws WorkflowException;

    public void removeParticipantToUsernameMappings(final String packageId,
                                                    final String processDefinitionId,
                                                    final String participantId)
        throws WorkflowException;

    public void removeParticipantToUsernameMapping(String packageId,
                                                   String processDefinitionId,
                                                   String participantId,
                                                   String username)
        throws WorkflowException;

    public void addParticipantToUsernameMapping(final String packageId,
                                                final String processDefinitionId,
                                                final String participantId,
                                                final String username)
        throws WorkflowException;
}
