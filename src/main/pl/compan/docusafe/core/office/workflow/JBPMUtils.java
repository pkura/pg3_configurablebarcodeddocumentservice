package pl.compan.docusafe.core.office.workflow;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jbpm.JbpmContext;
import org.jbpm.db.GraphSession;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ProcessInstance;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.jbpm.ContextNotOpenedException;
import pl.compan.docusafe.core.office.workflow.jbpm.JBPMDefBean;

public class JBPMUtils {
	
	Log log = LogFactory.getLog("kuba");
	
	private Integer findActiveProcessNum(List<ProcessInstance> pis) {
		Integer result = 0;
		for (ProcessInstance pi : pis) {
			if (pi.getEnd() == null) {
				result++;
			}
		}
		return result;
	}

	/** wyszukuje definicje proces�w z wara
	 * 
	 * @return mapa: nazwy na �cie�ki
	 * @throws MalformedURLException 
	 * @throws UnsupportedEncodingException 
	 */
	public Map<String, String> getDefinitionsFromWar() throws MalformedURLException, UnsupportedEncodingException {
		URL url = Thread.currentThread().getContextClassLoader().getResource("procesy");
		log.debug(url.getFile());
		File procDir = new File(URLDecoder.decode(url.getPath(), "UTF-8"));
		Map<String, String> result = new LinkedHashMap<String, String>();
		if (procDir.exists() && procDir.isDirectory()) {
			for (File processDir : procDir.listFiles()) {
				if (processDir.isDirectory()) {
					for (File processFile : processDir.listFiles()) {
						if (processFile.getName().equals("processdefinition.xml")) {
							result.put(processFile.getPath(),processDir.getName());
						} else {
							log.debug("plik " + processFile.getName() + " nie jest definicja procesu");
						}
					}
				} else {
					log.debug("plik " + processDir.getName() + " nie jest katalogiem");
				}
			}
		} else {
			log.debug("nie ma katalogu procesy");
		}
		return result;
	}
	
	private JBPMDefBean getDefBean(ProcessDefinition procDef, GraphSession gs) {
		JBPMDefBean defBean = new JBPMDefBean(procDef);
		List<ProcessInstance> processes = gs.findProcessInstances(Integer.parseInt(defBean.getId()));
		defBean.setProcessNum(processes.size());
		defBean.setActiveProcessNum(findActiveProcessNum(processes));
		defBean.setSearchProcessURL("lalala");
		return defBean;
	}
	
	public List<JBPMDefBean> getDefBeans() throws EdmException {
		List<JBPMDefBean> result = new LinkedList<JBPMDefBean>();
		JbpmContext ctx = null;
		ctx = DSjBPM.getContext();
		List<ProcessDefinition> processDefs = (List<ProcessDefinition>)ctx.getGraphSession().findLatestProcessDefinitions();
		
		for (ProcessDefinition def : processDefs) {
			JBPMDefBean defBean = getDefBean(def, ctx.getGraphSession());
			List<ProcessDefinition> olderVersions = (List<ProcessDefinition>)ctx.getGraphSession().findAllProcessDefinitionVersions(def.getName());
			for (ProcessDefinition ver : olderVersions) {
				if (ver.getVersion() != def.getVersion()) {
					JBPMDefBean verBean = getDefBean(ver, ctx.getGraphSession());
					defBean.getVersions().add(verBean);
				}	
			}
			result.add(defBean);
		}
		DSjBPM.closeContext(ctx);
		return result;
	}
	
	/** laduje definicje procesu
	 * @param uploadedFile plik z formularza, moze byc archiwum .zip (definicja, gpd, bmp) albo tylko .xml z definicja
	 * @param orginalFilename oryginalna nazwa pliku (a nie jego tymczasowej kopii) 
	 * @throws EdmException
	 */
	public void loadProcessDefinition(File uploadedFile, String orginalFilename) throws EdmException {
		if (orginalFilename.endsWith(".xml")) {
			loadProcessDefinitionFromXml(uploadedFile);
		} else if (orginalFilename.endsWith(".zip")) {
			//TODO obsluzyc zipa
		} else {
			throw new EdmException("Wymagane jest rozszerzenie .zip lub .xml");
		}
	}
	
	public void loadPredefined(String path) throws EdmException {
		File file = new File(path);
		loadProcessDefinition(file,file.getName());
	}
	
	private void loadProcessDefinitionFromXml(File file) throws EdmException {
		JbpmContext ctx = null;
		try {
			ctx = DSjBPM.getContext();
			ProcessDefinition def = ProcessDefinition.parseXmlInputStream(new FileInputStream(file));
			ctx.deployProcessDefinition(def);
			DSjBPM.closeContext(ctx);
		} catch (FileNotFoundException fe) {
			throw new EdmException(fe);
		}
	}

	/**
	 * Wykonuje dan� cz�� kodu podczas otwartego kontekstu proces�w biznesowych - przydatne gdy dana metoda moze byc wykonywana w roznym kontekscie.
	 * @param run
	 */
	public static void runInJbpmSession(Runnable run) throws EdmException {
		boolean opened = false;
		try {
			try {
				DSjBPM.context();
			} catch (ContextNotOpenedException e) {
				opened = true;
				DSjBPM.open();
			}
			run.run();
		} catch(RuntimeException e){//jedyny spos�b by wyrzuci� wyj�tek z Runnable
			throw new EdmException(e);
		} finally {
			if (opened){
				DSjBPM.closeContextQuietly(DSjBPM.context());
			}
		}
	}
}
