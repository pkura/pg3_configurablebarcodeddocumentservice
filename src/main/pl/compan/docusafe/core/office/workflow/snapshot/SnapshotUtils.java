package pl.compan.docusafe.core.office.workflow.snapshot;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import org.hibernate.Session;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.process.ProcessEngineType;
import pl.compan.docusafe.process.WorkflowEngine;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import static pl.compan.docusafe.core.NewAvailabilityManager.isAvailable;

/** 
 * Klasa do ktorej wyprowadzam stale z tasklisty dla wiekszego porzadku
 *
 * Deleguje ona zadania do odpowiedniego SnapshotProvidera
 *
 * @author wkutyla
 **/
public class SnapshotUtils 
{
	public final static Logger log = LoggerFactory.getLogger(SnapshotUtils.class);
	
	/**
	 * Klasa ktora zapewnia...
	 */
	static final SnapshotProvider snapshotProvider;

    static {
        if(isAvailable("tasklist.multipleProviders"))  {
            log.info("budowanie CompositeSnapshotProvider");
            List<SnapshotProvider> list = Lists.newArrayListWithExpectedSize(2);
            if(WorkflowFactory.jbpm){
                list.add(new Jbpm4SnapshotProvider());
            }
            if(isAvailable("tasklist.activiti")){
                list.add(new ActivitiSnapshotProvider(Docusafe.getBean("docusafeActivitiWorkflowEngine", WorkflowEngine.class)));
            }
            switch(list.size()){
                case 0:
                    throw new RuntimeException("Brak wybranych snapshot provider, a w³±czono 'tasklist.multipleProviders'");
                case 1:
                    snapshotProvider = list.get(0);
                    break;
                default:
                    snapshotProvider = new CompositeSnapshotProvider(list);
                    break;
            }
        } else if (WorkflowFactory.jbpm) {
            snapshotProvider = new Jbpm4SnapshotProvider();
        } else {
            snapshotProvider = new InternalWorkflowSnapshotProvider();
        }
    }

	private static SnapshotProvider getSnapshotProvider() {
        log.info("getSnapshotProvider {}", snapshotProvider.getClass());
		return snapshotProvider;
	}

    public static boolean isAssignedToUser(String username, Long documentId) throws EdmException {
        return getSnapshotProvider().isAssignedToUser(username, documentId);
    }
	
	/***
	 * Zwraca informacje o pozycji zadania na liscie zadan.
	 * Domyslenie sortowanie po dacie wejscia na liste.
	 * @param activityKey
	 * @return
	 */
	public static TaskPositionBean getTaskPosition(String activityKey) {
		return getSnapshotProvider().getTaskPosition(activityKey);
	}
	
	/**  
	 * Metoda odswieza zapisana kolejke zgloszen
	 * W zwiazku z ryzykiem zakleszczania sie transakcji zmieniono model aktualizacji list zadana
	 * 1. W czasie transakcji glownej w kontekscie zapisywane sa jedynie zadania aktualizacji
	 * 2. Po wykonaniu transakcji glownej Context zaktualizuje zapisane zadania w nowej transakcji
	 * (jedna transakcja na jedno zadanie)
	 * @param session
	 * @param requests
	 */
	public static void updateTaskList(Session session, Map<Long, RefreshRequestBean> requests)
	{
		getSnapshotProvider().updateTaskList(session, requests);	
	}
}