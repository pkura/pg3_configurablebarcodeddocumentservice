package pl.compan.docusafe.core.office.workflow.snapshot;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.activiti.engine.HistoryService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.history.HistoricDetail;
import org.activiti.engine.history.HistoricDetailQuery;
import org.activiti.engine.history.HistoricIdentityLink;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.history.HistoricTaskInstanceQuery;
import org.activiti.engine.history.HistoricVariableInstanceQuery;
import org.activiti.engine.history.NativeHistoricActivityInstanceQuery;
import org.activiti.engine.history.NativeHistoricDetailQuery;
import org.activiti.engine.history.NativeHistoricProcessInstanceQuery;
import org.activiti.engine.history.NativeHistoricTaskInstanceQuery;
import org.activiti.engine.history.NativeHistoricVariableInstanceQuery;
import org.activiti.engine.task.DelegationState;
import org.activiti.engine.task.Task;

import pl.compan.docusafe.activiti.ActivitiWorkflowEngine;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.process.ProcessManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.process.ProcessForDocument;
import pl.compan.docusafe.process.WorkflowEngine;

import org.activiti.engine.FormService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.db.DbSchemaCreate;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;

public class ActivitiDecretationManager 
{
	ProcessEngine pe = ProcessEngines.getDefaultProcessEngine();
	@Autowired
	RepositoryService repositoryService = pe.getRepositoryService();

	@Autowired
	RuntimeService runtimeService = pe.getRuntimeService();

	@Autowired
	TaskService taskService = pe.getTaskService(); 

	@Autowired
	IdentityService identityService = pe.getIdentityService();
	 
	@Autowired
	HistoryService historyService = pe.getHistoryService();
	
	
	public void checkUnfinishedExecution()
	{
		for (ProcessInstance temp : runtimeService.createProcessInstanceQuery().list())
		{

			System.out.println(temp.getId());

		}

	}

	public void assignTask(String instantID, String assigneeName ,OfficeDocument doc)
	{
		Task task = taskService.createTaskQuery().processInstanceId(instantID).singleResult();
		task.setAssignee(assigneeName);
		taskService.saveTask(task);
	}

	public void assignTask(String taskId, OfficeDocument doc, String assigneeName, String assigneDivision ,  String objective, String decretationType, String clerk,  Date deadlineTime)
			throws EdmException
	{


		List<ProcessForDocument> lst = ProcessForDocument.getProcessesForDocument(doc.getId());
		for (ProcessForDocument pfd : lst)
		{
			String a = "";
			List<Task> taskList = taskService.createTaskQuery().processInstanceId(pfd.getId().getProcessId()).list();
			for (Task task : taskList)
			{
				if (task.getId().equals(taskId))
				{
					
					Task newTask = taskService.newTask();
					//Task task = taskService.createTaskQuery().processInstanceId(instantID).singleResult();
					if (objective != null)
						task.setDescription(objective);
					if (deadlineTime != null)
						task.setDueDate(deadlineTime);
					}
			
				if(decretationType!=null && !decretationType.isEmpty() ){
					taskService.setVariableLocal(task.getId(), Jbpm4WorkflowFactory.DOCDESCRIPTION, decretationType);
				}
				if(clerk!=null && !clerk.isEmpty() ){
					taskService.setVariableLocal(task.getId(), Jbpm4WorkflowFactory.CLERK, clerk);
					}
					
					task.setDelegationState(DelegationState.PENDING);
					taskService.saveTask(task);
					taskService.setVariableLocal(task.getId(), "newTaskAccepted", false);
					if(assigneeName!=null){
						taskService.addCandidateUser(task.getId(), assigneeName);
						taskService.delegateTask(task.getId(), assigneeName);
					}
					else if (assigneDivision!=null){
						taskService.addCandidateGroup(task.getId(), assigneDivision);
						taskService.delegateTask(task.getId(), assigneDivision);
					}
						
				
					
					//taskService.claim(task.getId(), assigneeName);
					//taskService.notify();
					//taskService.complete(newTask.getId());
					TaskSnapshot.updateAllTasksByDocumentId(doc.getId(), doc.getStringType());
					break;
				}
			}
	}
	public void createUser(String name)
	{
		User user = identityService.newUser(name);
		identityService.saveUser(user);
		System.out.println(user.getId() + " -->" + " has been created");
	}

	public void checkAssignTask(String instantID)
	{
		Task task = taskService.createTaskQuery().processInstanceId(instantID).singleResult();
		System.out.println(task.getName() + " assined To : " + task.getAssignee());
	}

	public void authUser(String name)
	{
		identityService.setAuthenticatedUserId("Rio");
	}

	public void checkStatus(String id)
	{

		Task temp = taskService.createTaskQuery().processInstanceId(id).singleResult();
		System.out.println(temp.getName() + " -> " + temp.getId());

	}

	public void complete(String id)
	{
		taskService.complete(id);
	}

	public void complete(String instantID, String name)
	{
		Task temp = taskService.createTaskQuery().processInstanceId(instantID).singleResult();
		taskService.complete(temp.getId());
	}

	public void assignDocument(JBPMTaskSnapshot ts, List<Map<String, Object>> paramsList, OfficeDocument od) throws EdmException
	{
		 for(Map<String,Object> params : paramsList)
         {
			String asigneUser = (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM);
			String guid =  (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM);
			String decretationType =  (String) params.get(Jbpm4WorkflowFactory.DOCDESCRIPTION);
			String objective = (String) params.get(Jbpm4WorkflowFactory.OBJECTIVE);
        	Date deadlineTime = (Date) params.get(Jbpm4WorkflowFactory.DEADLINE_TIME);
        	String clerk = 	(String) params.get(Jbpm4WorkflowFactory.CLERK);
        	String asigneGuid= (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM);
		
			assignTask(ts.getActivityKey() ,od, asigneUser,asigneGuid, objective, decretationType , clerk , deadlineTime );
			ProcessManager.empty().makeHistory(od, params);
         
         }
	}

}

/*

public void assignTask(String instantID, String assigneeName){
	 Task task = taskService.createTaskQuery().processInstanceId(instantID).singleResult();
	 
	 task.setAssignee(assigneeName);
	 taskService.saveTask(task);
	 }*/



