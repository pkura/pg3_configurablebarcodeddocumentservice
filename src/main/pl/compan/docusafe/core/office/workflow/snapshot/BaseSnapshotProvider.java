package pl.compan.docusafe.core.office.workflow.snapshot;

import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.compan.docusafe.boot.WatchDog;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.jackrabbit.JackrabbitPrivileges;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.logic.AttachmentsManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa zapewniajaca odswiezenie listy zadan
 * @author wkutyla
 *
 */
abstract class BaseSnapshotProvider implements SnapshotProvider
{
	private final static Logger log = LoggerFactory.getLogger(BaseSnapshotProvider.class);

	public static final long SYNCHRO_CODE = 5L;
	/**
	 * W tej mapie zapisujemy te kobylaste SQL
	 */
	protected static Map<String,String> taskSQL = new HashMap<String,String>();

	/**
	 * METODY ABSTRAKCYJNE
	 */

	/**
	 * Metoda przygotowuje pytanie dla pism przychodzacych, posiada ? na documentId je�li updateType przewiduje aktualizacj� dla jednego dokumentu
	 * @param username
	 * @param updateType
	 * @return
	 */
	protected abstract String prepareInSQL(String username,int updateType);

	public boolean isAssignedToUser(String userName, Long documentId) throws EdmException
	{
		return true;
	}
	/**
	 * Przygotowuje pytanie dla OUT SQL, posiada ? na documentId je�li updateType przewiduje aktualizacj� dla jednego dokumentu
	 * @param username
	 * @param updateType
	 * @return
	 */
	protected abstract String prepareOutSQL(String username,int updateType);

	/**
	 *
	 * @param username
	 * @param updateType
	 * @return
	 */
	protected abstract String prepareOrdSQL(String username,int updateType);

	/**
	 * Koniec metod abstrakcyjnych
	 * -----------------
	 */

	/**
	 * Metoda odswieza zapisana kolejke zgloszen
	 * W zwiazku z ryzykiem zakleszczania sie transakcji zmieniono model aktualizacji list zadana
	 * 1. W czasie transakcji glownej w kontekscie zapisywane sa jedynie zadania aktualizacji
	 * 2. Po wykonaniu transakcji glownej Context zaktualizuje zapisane zadania w nowej transakcji
	 * (jedna transakcja na jedno zadanie)
	 * @param session
	 * @param requests
	 */
	public void updateTaskList(Session session, Map<Long, RefreshRequestBean> requests)
	{
		try
		{
			Iterator<Long> it = requests.keySet().iterator();
			int ileZadan = requests.keySet().size();
			if (ileZadan>0)
			{
				log.debug("Mam do wykonania "+ileZadan+" zadan");
				int licznik = 0;
				while (it.hasNext())
				{
					licznik ++;
					Long id = it.next();
                    RefreshRequestBean bean = requests.get(id);
                    if (log.isTraceEnabled()) {
                        log.trace("Zadanie "+licznik+"/"+ileZadan+" bean id="+id+" username "+bean.getUsername());
                    }

                    updateSingleEntry(session, id, bean, licznik);
                }
			}
		}
		catch (Exception e)
		{
			log.error("Nie udalo sie odswiezyc listy zadan (blad globalny)",e);
		}
	}

    private void updateSingleEntry(Session session, Long id, RefreshRequestBean bean, int licznik) {
        try
        {
            session.flush();
            if(licznik%5000==0)
                session.clear();
            //tylko etykiety
            if(bean.getUpdateType() == TaskSnapshot.UPDATE_TYPE_DOC_LABEL)
            {

                log.trace("Update tylko etykiet wejscie");
                updateLabel(session,bean.getId(), bean.getUsername());
                log.trace("Update tylko etykiet wyj�cie doc_id = {}",bean.getId());
            }
            else if(bean.getUpdateType() == TaskSnapshot.UPDATE_TYPE_ONE_USER)
            {

                log.trace("Update dla uzytkownika wejscie");
                updateTaskList(session,bean.getId(),bean.getDoctype(),bean.getUpdateType(),bean.getUsername());
                //updateUserLabels(session,bean.getUsername());
                //niepotrzebne - kazde zadanie sprawdza etykiety dla siebie
                log.trace("Update dla uzytkownika wyjscie user = {}",bean.getUsername());
            }
            else if(bean.getUpdateType() == TaskSnapshot.UPDATE_TYPE_IS_ANY_IMAGE_ONE_DOC)
            {
                log.trace("Update tylko isAnyImage wejscie");
                updateIsAnyImage(session,bean.getId());
                log.trace("Update tylko isAnyImage user = {}",bean.getUsername());
            }
            else
            {
                log.trace("Update dla dokumentu wejscie");
                updateTaskList(session,bean.getId(),bean.getDoctype(),bean.getUpdateType(),bean.getUsername());
                //updateLabel(session,bean.getId());
                //niepotrzebne j/w
                log.trace("Update dla dokumentu wyj�cie doc_id = {}",bean.getId());
            }
        }
        catch (Exception e1)
        {
            log.error("Nie udalo sie odswiezyc listy zadan dla id="+id,e1);
        }
    }

    /**
	 *
	 * @param session
	 * @param id
	 * @param doctype
	 * @param updateType
	 * @param username
	 * @throws Exception
	 */
	public abstract void updateTaskList(Session session, Long id, String doctype, int updateType, String username) throws Exception;

	/**
	 * Metoda sprawdza etykiete hidding dla dokumentu.Jesli bedzie powodowac duze opoznienia trzeba bedzie zmienic
	 * @param documentId
	 * @return
	 * @throws Exception
	 */
	protected boolean hasHiddingLabel (Long documentId, String username) throws EdmException, SQLException
	{
		WatchDog wd = new WatchDog("hasHiddingLabel", 100);

		long time = System.currentTimeMillis();
		//Jesli nie ma obslugi labeli zawsze zwracamy false
		if (!AvailabilityManager.isAvailable("labels"))
			return false;
		boolean resp = false;
		PreparedStatement ps = null;
		try
		{
			log.trace("sprawdzam etykiete hidding dla dokumentu {}",documentId);
			ps = DSApi.context().prepareStatement("select l.read_rights from DSO_DOCUMENT_TO_LABEL dl join dso_label l "+
					"on l.id = dl.label_id where dl.hidding = 1 and dl.document_id = ?");
			ps.setLong(1, documentId);
			ResultSet rs = ps.executeQuery();
			DSUser u = DSUser.findByUsername(username);
			while(rs.next())
			{
				String read_rights = rs.getString(1);
				resp = resp || u.inDivisionByGuid(read_rights) || username.equals(read_rights);
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		wd.tick();
		return resp;
	}

	/***
	 * Aktualizuje kolumne odpowiedzialna za wyswietlenie lupki na liscie zadan
	 * @author Mariusz Kilja�czyk
	 * @param session
	 * @param id
	 * @throws Exception
	 */
	public void updateIsAnyImage(Session session,Long id) throws Exception
	{
		PreparedStatement ps = null;
		Transaction tx = null;
		try
		{
			tx = session.beginTransaction();
			log.debug("Otworzylem transackje {}",tx );
			boolean isAnyImageInAttachments  = AttachmentsManager.isAnyImageInAttachments(id);
			ps = DSApi.context().prepareStatement("update dsw_tasklist set isAnyImageInAttachments = ? where DSO_DOCUMENT_ID = ?");
			ps.setBoolean(1,isAnyImageInAttachments);
			ps.setLong(2, id);
			ps.execute();
			tx.commit();
			log.debug("commit transakcji {} wykonany ",tx);
			tx = null;
		}
		catch (Exception e)
		{
			log.error("",e);
			throw new EdmException("",e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
            if (tx!=null)
            {
				try
				{
					log.debug("Wykonuje rollback transackji {}",tx );
					tx.rollback();
				}
				catch (Exception eee)
				{
					log.debug("Rollback transakcji " + tx + " niemozliwy",eee);
				}
			}
		}
	}

	/***
	 * Aktualizuje widoczno�� zada� dokumentu w zale�no�ci od ustawionych etykiet (zmienia warto�� labelHIDDEN)
	 * @author Mariusz Kilja�czyk
	 * @param session
	 * @param username
	 * @throws Exception
	 */
	protected void updateLabel(Session session,Long id, String username) throws Exception
	{
		PreparedStatement ps = null;
		Transaction tx = null;
		List<String> users = new ArrayList<String>();
		if(username == null)
		{
			ps = DSApi.context().prepareStatement("select dsw_resource_res_key from dsw_tasklist where DSO_DOCUMENT_ID = ?");
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				users.add(rs.getString(1));
			}
			DSApi.context().closeStatement(ps);
		}
		else
		{
			users.add(username);
		}

		try
		{
			for(String u:users)
			{
				tx = session.beginTransaction();
				log.debug("Otworzylem transackje {}",tx);
				boolean hide  = hasHiddingLabel(id, u);
				ps = DSApi.context().prepareStatement("update dsw_tasklist set labelHIDDEN = ? where DSO_DOCUMENT_ID = ? and dsw_resource_res_key=?");
				ps.setBoolean(1,hide);
				ps.setLong(2, id);
				ps.setString(3, u);
				ps.execute();
				tx.commit();
				log.debug("commit transakcji {} wykonany ",tx);
				tx = null;
				DSApi.context().closeStatement(ps);
			}
		}
		catch (Exception e)
		{
			log.error("",e);
			throw new EdmException("",e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
            if (tx!=null)
            {
				try
				{
					log.debug("Wykonuje rollback transackji {}",tx );
					tx.rollback();
				}
				catch (Exception eee)
				{
					log.debug("Rollback transakcji " + tx + " niemozliwy",eee);
				}
			}
		}
	}

	/**
	 * Metoda ustawia pola zalezne od dockinda
	 * @param docid
	 * @param dockind_id
	 * @param task
	 * @throws EdmException
	 */

	final static void setDockindFields(long docid, int dockind_id, TaskSnapshot task) throws EdmException
	{
		log.trace("setDockindFields");
        task.setDockindName(cutString(DocumentKind.getDockindNameForId(dockind_id), 100, "DockindName"));

        try {
        	DocumentKind dk = DocumentKind.find((long)dockind_id);
        	TaskListParams params = dk.logic().getTaskListParams(dk,docid,task);
        	task.setDockindStatus(params.getStatus());
        	task.setDockindNumerDokumentu(cutString(params.getDocumentNumber(), 30, "DockindNumerDokumentu"));
            if (params.getAmount() != null) {
                task.setDockindKwota(params.getAmount().setScale(2, RoundingMode.HALF_UP));
            }
//        	processKwota(params.getKwota(), task);
        	task.setDockindKategoria(cutString(params.getCategory(), 30, "DockindKategoria"));
			
        	task.setDockindBusinessAtr1(cutString(params.getAttribute(0), 30, "Atr1"));
        	task.setDockindBusinessAtr2(cutString(params.getAttribute(1), 30, "Atr2"));
        	task.setDockindBusinessAtr3(cutString(params.getAttribute(2), 30, "Atr3"));
        	task.setDockindBusinessAtr4(cutString(params.getAttribute(3), 30, "Atr4"));
        	task.setDockindBusinessAtr5(cutString(params.getAttribute(4), 30, "Atr5"));
        	task.setDockindBusinessAtr6(cutString(params.getAttribute(5), 30, "Atr6"));
        	task.setDocumentDate(params.getDocumentDate());
        	log.debug("params z set : {}", params);
        	task.setMarkerClass(cutString(params.getMarkerClass(), 20, "MarkerClass"));
        } catch (Exception e1) {
        	log.warn("Dokument {}", docid);
        	log.warn("Blad w czasie odswiezania listy zadan",e1);
        	task.setDockindStatus("ERR");
        }
	}
	/**
	 * Metoda zwraca zapytanie dla odswiezenia listy zadan przychodzacych
	 * @param username
	 * @param updateType
	 * @return
	 */
	protected String getInSQL(String username,int updateType)
	{
		String key = prepareSQLMapKey("IN",username,updateType);
		String resp = taskSQL.get(key);
		if (resp == null)
		{
			resp = prepareInSQL(username,updateType);
			log.debug("Przygotowalem pytanie dla klucza {}",key);
			taskSQL.put(key, resp);
		}
		log.trace(resp);
		return resp;
	}


	/**
	 * Metoda zwraca outSQL
	 * @param username
	 * @param updateType
	 * @return
	 */
	protected String getOutSQL(String username,int updateType)
	{
		String key = prepareSQLMapKey("OUT",username,updateType);
		String resp = taskSQL.get(key);
		if (resp == null)
		{
			resp = prepareOutSQL(username,updateType);
			log.debug("Przygotowalem pytanie dla klucza {}",key);
			taskSQL.put(key, resp);
		}
		log.trace(resp);
		return resp;
	}



	/**
	 * Metoda pobiera pytanie dla zadan przychodzacych
	 * @param username
	 * @param updateType
	 * @return
	 */
	protected String getOrdSQL(String username,int updateType)
	{
		String key = prepareSQLMapKey("ORD",username,updateType);
		String resp = taskSQL.get(key);
		if (resp == null)
		{
			resp = prepareOrdSQL(username,updateType);
			log.debug("Przygotowalem pytanie dla klucza {}",key);
			taskSQL.put(key, resp);
		}
		log.trace(resp);
		return resp;
	}


	/**
	 * Metoda przygotowuje klucz dla mapy w ktorej przechowywane sa pytania SQL na potrzeby odswiezania listy zadan
	 * @param kind
	 * @param username
	 * @return
	 */
	protected String prepareSQLMapKey(String kind,String username, int updateType)
	{
		String pom = username != null ? "username" : "null";
		String key = kind+pom+updateType;
		log.trace("klucz do pobrania pytania SQL = {} ",key);
		return key;
	}

    protected String getTable(){ return "dsw_tasklist"; }
    protected String getReskey(){ return "dsw_resource_res_key"; }
    protected String getDocumentIdKey(){ return "dso_document_id"; }

	/**
	 * Metoda usuwa dane ze snapshotu - korzysta z metod getTable, getReskey i getDocumentIdKey
	 * @param session
	 * @param id
	 * @param updateType
	 * @param doctype
	 * @param username
	 * @throws SQLException
	 * @throws HibernateException
	 * @throws EdmException
	 */
	protected void deleteFromSnapshot(Session session,long id,int updateType,String doctype, String username) throws SQLException, HibernateException, EdmException
	 {
		  String table = getTable();
		  String reskey = getReskey();
		  String documentIdKey = getDocumentIdKey();
		  /*if (this instanceof JBPMSnapshotProvider)
		  {
			  table = "dsw_jbpm_tasklist";
			  reskey = "assigned_resource";
			  documentIdKey = "document_id";
			  log.trace("Lista zadan JBPM");
		  }*/
		  PreparedStatement erase = null;
		  Transaction tx = null;
		  try
		  {
			  tx = session.beginTransaction();
			  log.trace("Otworzylem transackje " + tx );
			  String del;

			  if(updateType == 1)
			  {
				  if(doctype.equals("anyType"))
				  {
					  //nie ma juz podzialu na external i internal, zakladamy ze pracuje jeden WF
					  //del="delete from dsw_tasklist where dso_document_id=? and external_workflow=?";
					  del="delete from "+table+" where "+documentIdKey+ "=?";
					  erase = DSApi.context().prepareStatement(del);
					  erase.setLong(1, id);
					  //erase.setInt(2, 0);
					  if (log.isTraceEnabled())
						  log.trace("delete from "+table+" where "+documentIdKey+ "={}",id);
				  }
				  else
				  {
					  //del="delete from dsw_tasklist where dso_document_id= ? and document_type=? and external_workflow=?";
					  del="delete from "+table+" where "+documentIdKey+ " = ? and document_type=?";
					  erase = DSApi.context().prepareStatement(del);
					  erase.setLong(1, id);
					  erase.setString(2,doctype);
					  //erase.setInt(3, 0);
					  if (log.isDebugEnabled())
						  log.trace("delete from "+table+" where "+documentIdKey+ " = {} and document_type={}",id,doctype);
				  }

			  }
			  else if(updateType == 2)
			  {
				  if(username != null)
				  {
					  del = "delete from "+table+" where "+reskey+" =?";
					  erase = DSApi.context().prepareStatement(del);
					  erase.setString(1, username);
					  log.trace("delete from "+table+" where "+reskey+" ={}",username);
				  }
				  else
				  {
					  del="delete from "+table;
					  erase = DSApi.context().prepareStatement(del);
					  log.trace("delete from "+table);
				  }
			  }
			  erase.execute();
			  DSApi.context().closeStatement(erase);

			  erase = null;

			  tx.commit();
			  log.trace("commit transakcji {} wykonany ",tx);
			  tx = null;

              try {
                  if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                              JackrabbitPrivileges.instance().revokeReadPrivilege(id, null, null, ObjectPermission.SOURCE_TASKLIST);
                  }
              } catch (Exception e) { throw new EdmException(e); }
		  }
		  finally
		  {
			  DSApi.context().closeStatement(erase);
			  if (tx!=null){
					try	{
						log.trace("Wykonuje rollback transackji {}",tx);
						tx.rollback();
					} catch (Exception eee)  {
						log.trace("Rollback transakcji " + tx + " niemozliwy",eee);
					}
				}
		  }
	  }

	/**
	   * Metoda obcina ciag znakow, ktory jest zbyt dlugi dla kolumny w snapshocie
	   * @param value
	   * @param len
	   * @param name
	   * @return
	   */
	static String cutString(String value, Integer len,String name)
	{
		if( value != null && value.length() > len )
		{
		    log.debug("Data truncation: przekroczono dopuszczaln� d�ugo�� dla pola "+ name+" dopuszczalna d�ugo�� to "+ len
				 + "\n value : "+ value);
		    return StringUtils.left(value, len-3)+"...";
		}
	    return value;
	}
	 
	 protected String getFormattedOfficeNumber(Integer ko,Integer dko)
	 {
		 return OfficeDocument.getFormattedOfficeNumber(ko, dko);
	 }
}
