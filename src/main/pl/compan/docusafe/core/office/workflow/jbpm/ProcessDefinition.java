package pl.compan.docusafe.core.office.workflow.jbpm;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Criteria;
/* User: Administrator, Date: 2007-01-15 11:27:09 */
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

/**
 * Opisuje jedn� definicj� procesu zarejestrowan� w systemie Docusafe powi�zan� z pewn� definicj� procesu
 * z bazy JBPM.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class ProcessDefinition
{
    private Long id;
    private String definitionName;       // nazwa definicji w JBPM
    private Long definitionId;           // id definicji w JBPM
    private String nameForUser;
    private String page;
    private Integer definitionVersion;   // wersja definicji w JBPM
    /*private Set<TaskDefinition> taskDefinitions;*/

    public ProcessDefinition()
    {

    }

    public synchronized void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException
    {
         Persister.delete(this);
    }    

    public static ProcessDefinition find(Long id) throws EdmException
    {
        return (ProcessDefinition) Finder.find(ProcessDefinition.class, id);
    }

    /**
     * Zwraca wszystkie zarejestrowane definicje proces�w (czyli tak�e wszystkie wersje dla ka�dej definicji).
     */
    public static List<ProcessDefinition> findAll() throws EdmException
    {
        return (List<ProcessDefinition>) Finder.list(ProcessDefinition.class, "nameForUser");
    }

    /**
     * Zwraca tylko najnowsze wersje wszystkich zarejestrowanych definicji.
     */
    @SuppressWarnings("unchecked")
    public static List<ProcessDefinition> findOnlyNewestDefinitions() throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(ProcessDefinition.class, "def");
        try
        {
            DetachedCriteria maxVersion = DetachedCriteria.forClass(ProcessDefinition.class, "v").
                add(Restrictions.eqProperty("v.definitionName", "def.definitionName")).
                setProjection(Projections.max("v.definitionVersion"));
            c.add(Subqueries.propertyEq("def.definitionVersion", maxVersion));
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static ProcessDefinition findByDefinitionId(Long definitionId) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(ProcessDefinition.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.eq("definitionId",definitionId));

            List list = c.list();
            if (list.size() == 0)
                return null;
            else if (list.size() > 1)
                throw new EdmException("Znaleziono wi�cej ni� jedn� definicje procesu o id="+definitionId);
            else
                return (ProcessDefinition) list.get(0);

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca najnowsz� definicj� dla danej nazwy definicji procesu JBPM.
     */
    public static ProcessDefinition findNewestByDefinitionName(String definitionName) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(ProcessDefinition.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.eq("definitionName", definitionName));
            c.addOrder(org.hibernate.criterion.Order.desc("definitionVersion"));

            List list = c.list();
            if (list.size() == 0)
                return null;
            else
                return (ProcessDefinition) list.get(0);

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDefinitionName()
    {
        return definitionName;
    }

    public void setDefinitionName(String definitionName)
    {
        this.definitionName = definitionName;
    }

    public Long getDefinitionId()
    {
        return definitionId;
    }

    public void setDefinitionId(Long definitionId)
    {
        this.definitionId = definitionId;
    }

    public String getNameForUser()
    {
        return nameForUser;
    }

    public void setNameForUser(String nameForUser)
    {
        this.nameForUser = nameForUser;
    }

    public String getPage()
    {
        return page;
    }

    public void setPage(String page)
    {
        this.page = page;
    }

    public Integer getDefinitionVersion()
    {
        return definitionVersion;
    }

    public void setDefinitionVersion(Integer definitionVersion)
    {
        this.definitionVersion = definitionVersion;
    }

  /*  public Set<TaskDefinition> getTaskDefinitions()
    {
        return taskDefinitions;
    }

    public void setTaskDefinitions(Set<TaskDefinition> taskDefinitions)
    {
        this.taskDefinitions = taskDefinitions;
    } */
}
