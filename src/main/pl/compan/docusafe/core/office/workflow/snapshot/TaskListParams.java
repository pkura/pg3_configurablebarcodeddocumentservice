package pl.compan.docusafe.core.office.workflow.snapshot;

import org.apache.commons.lang.ObjectUtils;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * Bean zawieraj�cy dane dla listy zada�
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class TaskListParams {
    private final static Logger LOG = LoggerFactory.getLogger(TaskListParams.class);

	public static final int MAX_ATTRIBUTE_COUNT = 6;

	private String status;

	private String category;

	private String documentNumber;
	
	private BigDecimal amount;

	private String[] attributes = new String[MAX_ATTRIBUTE_COUNT];

	private Date documentDate;
	
	private Date realizationDate;

	

	private String markerClass;

//    <li>param[0] = Dockind Status
//	<li>param[1] = Dockind Numer Dokumentu
//	<li>param[2] = Kwota
//	<li>param[3] = Dockind Kategoria
//	<li>param[4] = Atr1
//	<li>param[5] = Atr2
//	<li>param[6] = Atr3
//	<li>param[7] = Atr4
//	<li>param[8] = DocumentDate
//	<li>param[9] = TaskListMarkerClass
//	<li>param[10] = Atr5
//	<li>param[11] = Atr6

	@Deprecated
	public static TaskListParams fromMagicArray(Object[] magicArray){
		TaskListParams ret = new TaskListParams();

		ret.setStatus(ObjectUtils.toString(magicArray[0], null));

		ret.setDocumentNumber(ObjectUtils.toString(magicArray[1], null));

		if(magicArray[2] instanceof Float){
			ret.setAmount(new BigDecimal(magicArray[2].toString()).setScale(2));
		} else if(magicArray[2] instanceof BigDecimal) {
			ret.setAmount((BigDecimal) magicArray[2]);
		}

		ret.setCategory(ObjectUtils.toString(magicArray[3], null));

		ret.setAttribute(ObjectUtils.toString(magicArray[4] , null), 0);
		ret.setAttribute(ObjectUtils.toString(magicArray[5] , null), 1);
		ret.setAttribute(ObjectUtils.toString(magicArray[6] , null), 2);
		ret.setAttribute(ObjectUtils.toString(magicArray[7] , null), 3);

		ret.setDocumentDate((Date) magicArray[8]);

		ret.setMarkerClass(ObjectUtils.toString(magicArray[9], null));
		try {
			ret.setAttribute(ObjectUtils.toString(magicArray[10] , null), 4);
			ret.setAttribute(ObjectUtils.toString(magicArray[11] , null), 5);
		} catch (Exception e) {
			//nie wysztkie logigi obsluguja 12 wartosci
		}

		return ret;
	}

    /**
     * Tworzy TaskListParams na podstawie regu� zapisanych w xml w properties
     *
     * Wczytywane warto�ci properties kt�re wskazuj� na cn p�l dockindowych:
     *
     *   Atrybuty biznesowe od 1 do MAX_ATTRIBUTE_COUNT:
     *   "tasklist.param.0.cn"
     *   ...
     *   "tasklist.param.[MAX_ATTRIBUTE_COUNT-1].cn"
     *
     *   Kwota (tylko pola money):
     *   "tasklist.param.amount.cn"
     *
     *   Numer/identyfikator dokumentu:
     *   "tasklist.param.documentNumber.cn"
     *
     *   Kategoria/typ dokumentu
     *   "tasklist.param.category.cn"
     *
     *
     *
     * @param kind
     * @param documentId
     * @return
     */
    public static TaskListParams fromDockindProperties(DocumentKind kind, long documentId) throws EdmException {
        FieldsManager fm = Documents.document(documentId).getFieldsManager();
        Map<String, String> properties = kind.getProperties();

        TaskListParams ret = new TaskListParams();

        for(int i = 0; i < MAX_ATTRIBUTE_COUNT; ++i){
            String cn = properties.get(MessageFormat.format("tasklist.param.{0}.cn", i));
            if(cn != null){
                ret.setAttribute(fm.getStringValue(cn), i);
                LOG.info("wczytano warto�� pole '{}' dla atrybutu o indeksie {}", cn, i);
            }
        }
        {
            String amountCn = properties.get("tasklist.param.amount.cn");
            if(amountCn != null){
                ret.setAmount((BigDecimal)fm.getKey(amountCn));
                LOG.info("wczytano warto�� pole '{}' dla kwoty");
            }
        }
        {
            String numberCn = properties.get("tasklist.param.documentNumber.cn");
            if(numberCn != null){
                ret.setDocumentNumber(fm.getStringValue(numberCn));
                LOG.info("wczytano warto�� pole '{}' dla numeru dokumentu");
            }
        }
        {
            String categoryCn = properties.get("tasklist.param.category.cn");
            if(categoryCn != null){
                ret.setCategory(fm.getStringValue(categoryCn));
                LOG.info("wczytano warto�� pole '{}' dla kategorii");
            }
        }
        {
            String documentDateCn = properties.get("tasklist.param.documentDate.cn");
            if (documentDateCn != null) {
                ret.setDocumentDate((Date) fm.getKey(documentDateCn));
                LOG.info("wczytano warto�� pole '{}' dla daty dokumentu");
            }
        }

        return ret;
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal kwota) {
		this.amount = kwota;
	}

	public String getAttribute(int index) {
		if(index >= MAX_ATTRIBUTE_COUNT) {
			throw new IllegalArgumentException("maksymalna ilo�� atrybut�w :" + MAX_ATTRIBUTE_COUNT + " podano:" + index);
		} else {
			return attributes[index];
		}
	}
	/**Numerowane od 0*/
	public void setAttribute(String attribute, int index) {
		if(index >= MAX_ATTRIBUTE_COUNT) {
			throw new IllegalArgumentException("maksymalna ilo�� atrybut�w :" + MAX_ATTRIBUTE_COUNT + " podano:" + index);
		} else {
			this.attributes[index] = attribute;
		}
	}

	public Date getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}

	public String getMarkerClass() {
		return markerClass;
	}

	public void setMarkerClass(String markerClass) {
		this.markerClass = markerClass;
	}
	public Date getRealizationDate()
	{
		return realizationDate;
	}

	public void setRealizationDate(Date realizationDate)
	{
		this.realizationDate= realizationDate;
	}

    @Override
    public String toString() {
        return "TaskListParams{" +
                "\namount=" + amount +
                ", \nstatus='" + status + '\'' +
                ", \ncategory='" + category + '\'' +
                ", \ndocumentNumber='" + documentNumber + '\'' +
                ", \nattributes=" + (attributes == null ? null : Arrays.asList(attributes)) +
                ", \ndocumentDate=" + documentDate +
                ", \nmarkerClass='" + markerClass + '\'' +
                '}';
    }
}
