package pl.compan.docusafe.core.office.workflow;

import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowServiceFactory;

/**
 * Klasa abstrakcyjna po kt�rej powinny dziedziczy� klasy
 * zwracaj�ce instancje {@link WfService} pozwalaj�ce na
 * ��czenie si� z konkretnymi serwerami workflow.
 * <p>
 * G��wn� metod� zwracaj�c� instancje WorkflowServiceFactory
 * jest {@link #newInstance(java.lang.String)}, pozosta�e metody
 * ({@link #newExternalInstance()}, {@link #newInternalInstance()})
 * s� jedynie metodami pomocniczymi i powinny wywo�ywa� metod�
 * {@link #newInstance(java.lang.String)} z odpowiednim parametrem.
 * <p>
 * TODO: WfServiceS maj� konkretne nazwy, ale dostaje si� je poprzez og�lniejsze
 * nazwy zdefiniowane w interfejsie WfService - internal i external
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkflowServiceFactory.java,v 1.4 2009/02/25 21:49:14 wkuty Exp $
 */
public abstract class WorkflowServiceFactory
{
    /**
     * Zwraca obiekt WorkflowServiceFactory obs�uguj�cy zewn�trzny
     * workflow (obecnie Shark).
     */
	@Deprecated
    public static WorkflowServiceFactory newExternalInstance()
        throws WorkflowException
    {
		throw new IllegalStateException("NIE MA SHARKA");
    }

    /**
     * Zwraca obiekt WorkflowServiceFactory obs�uguj�cy wewn�trzny
     * workflow.
     */
    public static WorkflowServiceFactory newInternalInstance()
        throws WorkflowException
    {
        return newInstance(InternalWorkflowService.NAME);
    }

    /**
     * G��wna metoda zwracaj�ca instancje WorkflowServiceFactory.
     * @return Nowa instancja WorkflowServiceFactory.
     */
    public static WorkflowServiceFactory newInstance(String name)
        throws WorkflowException
    {
    	if (InternalWorkflowService.NAME.equals(name))
        {
            return new InternalWorkflowServiceFactory();
        }
        else
        {
            throw new WorkflowException("Nieznana nazwa serwisu workflow: "+name);
        }
    }

    /**
     * Zwraca instancj� WorkflowService obs�uguj�c� wybrany workflow.
     */
    public abstract WfService newService() throws WorkflowException;
}
