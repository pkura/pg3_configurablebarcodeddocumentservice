package pl.compan.docusafe.core.office.workflow;

import pl.compan.docusafe.core.office.workflow.xpdl.Participant;
import pl.compan.docusafe.core.office.workflow.xpdl.WorkflowProcess;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfProcessMgr.java,v 1.2 2004/10/12 21:41:34 lk Exp $
 */
public interface WfProcessMgr
{
    String category() throws WorkflowException;
    String description() throws WorkflowException;
    String name() throws WorkflowException;
    String package_id() throws WorkflowException;
    String process_definition_id() throws WorkflowException;
    WfProcess[] get_sequence_process() throws WorkflowException;
    WfProcess create_process(WfRequester requester) throws WorkflowException;
    /**
     * Zwraca list� uczestnik�w procesu na podstawie definicji w pliku XPDL
     * pobranym z serwera workflow.
     * Funkcja nie znajduje si� w standardzie.
     */
    Participant[] getParticipants() throws WorkflowException;
    String getWorkflowName();
    WorkflowProcess getProcessDefinition() throws WorkflowException;
}
