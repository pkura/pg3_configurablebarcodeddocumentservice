package pl.compan.docusafe.core.office.workflow;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkflowException.java,v 1.2 2006/02/20 15:42:20 lk Exp $
 */
public class WorkflowException extends EdmException
{
    public WorkflowException(String message)
    {
        super(message);
    }

    public WorkflowException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public WorkflowException(Throwable cause)
    {
        super(cause);
    }
}
