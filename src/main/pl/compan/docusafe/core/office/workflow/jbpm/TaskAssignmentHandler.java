package pl.compan.docusafe.core.office.workflow.jbpm;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.jbpm.taskmgmt.exe.Assignable;
import org.jbpm.taskmgmt.exe.SwimlaneInstance;
import org.jbpm.taskmgmt.exe.TaskInstance;
import org.jbpm.taskmgmt.def.AssignmentHandler;
import org.jbpm.graph.exe.ExecutionContext;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/* User: Administrator, Date: 2006-12-20 13:07:17 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 * @deprecated - Ten sposob realizacji workflow wywalamy
 */
@Deprecated
public class TaskAssignmentHandler implements AssignmentHandler
{
    private static final Log log = LogFactory.getLog(TaskAssignmentHandler.class);
    private static final Random generator = new Random();
    private static final long serialVersionUID = 1L;

    private static final String TYPE_DIVISION = "division";

    /** typ przypisania */
    String type;
    /** nazwa dzia�u w strukturze organizacyjnej, na kt�ry przypisa� zadanie */
    String divisionName;
    /**
     * Mapa<nrPolisy,kod_grupy> - okre�la specjalne sytuacje, w kt�rych nale�y zadanie zadekretowa�
     * na inn� grup� (tylko dla typu TYPE_DIVISION).
     */
    Map<String,String> specialCases;
    
    
    public void assign(Assignable assignable, ExecutionContext executionContext)
    {
        String actorId = (String) executionContext.getVariable(Constants.VARIABLE_NEXT_ACTOR);
  
        try
        {
            // pobieram oryginaln� nazw� swimlane'a
            String swimlaneName = assignable instanceof SwimlaneInstance ? ((SwimlaneInstance) assignable).getName() : null;
            
        
            if (actorId == null)
            {
                // przydzielamy na dzial (osobe z dzialu)
                if (TYPE_DIVISION.equals(type))
                {
                    String chosenDivisionName = null;

                    if (chosenDivisionName == null)
                        chosenDivisionName = divisionName;
                    
                    SwimlaneMapping swimlaneMapping = SwimlaneMapping.findByName(chosenDivisionName);
                    if (swimlaneMapping == null)
                    {
                        throw new IllegalStateException("Nie istnieje w systemie swimlane o nazwie: "+chosenDivisionName);                        
                    }
                    // je�li swimlane nie ma ustalonego sposobu dekretacji to wykonuje dekretacj� na grup�
                    Integer assignmentType = swimlaneMapping.getAssignmentType() != null ? swimlaneMapping.getAssignmentType() : Constants.ASSIGNMENT_GROUP;                                        
                    
                    if (Constants.ASSIGNMENT_USER.equals(assignmentType))
                    {    
                        // przydzielamy zadanie na jedna osobe z tego dzialu
                        actorId = assignSequence(swimlaneMapping);
    
                        if (actorId == null)
                        {
                            //actorId = "mnowicki";
                            throw new IllegalStateException("Nie mo�na przydzieli� osoby do zadania '"+executionContext.getTaskInstance().getName()+"'");
                        }
                        
                        assignable.setActorId(actorId);
                    }
                    else if (Constants.ASSIGNMENT_GROUP.equals(assignmentType))
                    {
                        String actorIds[] = assignGroup(swimlaneMapping);
    
                        if (actorIds == null || actorIds.length == 0)
                            throw new IllegalStateException("Do puli aktor�w w zadaniu '"+executionContext.getTaskInstance().getName()+"'nie mo�na przydzieli� �adnego aktora");
    
                        assignable.setPooledActors(actorIds);
                    }
                    
   
                }
                else
                    throw new IllegalStateException("W TaskAssignmentHandler napotkano nieznany spos�b przydzielania aktora do zadania");
            }        
    
        }
        catch (EdmException e) 
        {
            log.error("B��d podczas przydzielania aktor�w do zadania Workflow");
        }
    }

    /**
     * Sprawdza, czy ju� jaka� osoba pracuj�/realizuje zadania zwi�zane z dokumentem o takim samym typie i
     * numerze polisy, jak ten aktualnie przetwarzany. Je�li tak, to nie b�dzie "dekretacji" na dzia� czy nast�pnego
     * w kolejce u�ytkownika, ale na tego wyszukanego (to wszystko sprawdzane tylko dla dokument�w 'nationwide').
     * @return u�ytkownik, kt�remu przypisa� zadanie (null - gdy nie znalezioni odpowiedniego u�ytkownika) 
     */
    private String assignToWorkingUser(ExecutionContext executionContext) throws EdmException
    {
        Long documentId = (Long) executionContext.getVariable(Constants.VARIABLE_DOCUMENT_ID);
        Document document = Document.find(documentId);
        if (DocumentLogicLoader.NATIONWIDE_KIND.equals(document.getDocumentKind().getCn()))
        {
            DocumentKind kind = document.getDocumentKind();
            FieldsManager fm = kind.getFieldsManager(documentId);
            PreparedStatement ps = null;
            try
            {
                // szukam identyfikator�w dokument�w o zadanym typie i numerze polisy
                List<Long> docIds = new ArrayList<Long>();                 
                StringBuilder sql = new StringBuilder("select document_id from ");

                ps = DSApi.context().prepareStatement(sql.toString());
                //ResultSet rs = st.executeQuery(sql.toString());
                ResultSet rs = ps.executeQuery();
                while (rs.next())
                {
                    Long docId = rs.getLong(1);
                    docIds.add(docId);
                }
                DSApi.context().closeStatement(ps);                
                ps = null;
                
                // wyszukuje odpowiednich zada� - tylko z zewn�trznego Workflow i dla dokument�w,
                // kt�rego przed chwil� wyszuka�em
                TaskSnapshot.Query query = new TaskSnapshot.Query();
                query.setDocumentIds(docIds.toArray(new Long[]{}));
                query.setExternalWorkflow(Boolean.TRUE);
                SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
                for (TaskSnapshot task : results.results())
                {
                    TaskInstance taskInstance = executionContext.getJbpmContext().getTaskInstance(task.getExternalTaskId());
           
                }
            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
            finally
            {
            	DSApi.context().closeStatement(ps);                
            }
        }
        return null;
    }
    
    /**
     * Przydzialanie aktora losowo - losuje z wszystkich osob w dziale.
     * Pobiera mapowania dzia��w z docusafe.properties.
     *
     * @return login wybranej osoby
     */
    private String assignRandom(SwimlaneMapping swimlaneMapping) throws EdmException
    {
        String actorId = null;

        String divisionGuid = swimlaneMapping.getDivisionGuid();
        if (divisionGuid != null)
        {
            DSDivision division = DSDivision.find(divisionGuid);
            DSUser[] users = division.getUsers();

            int random = generator.nextInt(users.length);
            actorId = users[random].getName();
        }
        
        return actorId;
    }

    /**
     * Spos�b przydzielania aktora sekwencyjnie - po kolei ka�dej osobie z dzia�u. Pobiera mapowania dzia��w
     * z obiekt�w 'SwimlaneMapping'.
     *
     * @return login wybranej osoby
     */
    private String assignSequence(SwimlaneMapping swimlaneMapping) throws EdmException
    {
        String actorId = null;
        DSDivision division = DSDivision.find(swimlaneMapping.getDivisionGuid());
        DSUser[] users = division.getUsers();
        if (users.length > 0)
        {
            int no = swimlaneMapping.getLastUserNo() != null ? swimlaneMapping.getLastUserNo() + 1 : 0;
            if (no > users.length - 1)
                no = 0;
            actorId = users[no].getName();

            swimlaneMapping.setLastUserNo(no);
        }
            
        return actorId;
    }

    /**
     * Spos�b dekretacji na grup� - do zadania przydzielamy pul� aktor�w (wszystkich z danego dzia�u).
     * Zadanie trafi do tej osoby, kt�ra pierwsza kliknie na zadanie.
     * @return tablica login�w os�b z puli aktor�w
     */
    private String[] assignGroup(SwimlaneMapping swimlaneMapping) throws EdmException
    {
        String[] actorIds = null;

        DSDivision division = DSDivision.find(swimlaneMapping.getDivisionGuid());
        DSUser[] users = division.getUsers();
        actorIds = new String[users.length];
        for (int i=0; i < users.length; i++)
        {
            actorIds[i] = users[i].getName();
        }

        return actorIds;
    }
}
