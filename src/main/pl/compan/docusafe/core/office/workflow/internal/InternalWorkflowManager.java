package pl.compan.docusafe.core.office.workflow.internal;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.logic.Acceptable;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.AnyObjectKind;
import pl.compan.docusafe.core.office.workflow.AssignmentDescriptor;
import pl.compan.docusafe.core.office.workflow.NameValue;
import pl.compan.docusafe.core.office.workflow.PlannedAssignment;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.ProcessId;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.WfAssignment;
import pl.compan.docusafe.core.office.workflow.WfProcess;
import pl.compan.docusafe.core.office.workflow.WfProcessMgr;
import pl.compan.docusafe.core.office.workflow.WfService;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowException;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowServiceFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowUtils;
import pl.compan.docusafe.core.office.workflow.xpdl.ExtendedAttribute;
import pl.compan.docusafe.core.office.workflow.xpdl.WorkflowProcess;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.opensymphony.webwork.ServletActionContext;

/**
 * Menad�er wewn�trzenego Workflow (zawiera zbi�r r�nych funkcji cz�sto
 * u�ywanych).
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class InternalWorkflowManager
{
	private static StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(InternalWorkflowManager.class.getPackage().getName(),null);
    private static final Log log = LogFactory.getLog(InternalWorkflowManager.class);
    /**
     * Przygotowanie zbiorczej dekretacji.
     *
     * (wym�g utworzenia dw�ch tablic jeszcze zanim jest wiadomo, czy zostan� u�yte
     * mo�e si� wydawa� ma�o wydajny, ale w wi�kszo�ci wdro�e� zbiorcza dekretacja
     * JEST u�ywana, wi�c tablice przewa�nie s� u�ywane)
     *
     * @return mapa obiekt�w(kt�re tak�e s� mapami), wykorzystywanych do zbiorczej dekretacji na stronie JSP
     */
    public static Map<String,Map<String,String>> prepareCollectiveAssignments(String activity) throws EdmException
    {
        boolean collectiveAssignmentPossible =
            (Configuration.officeAvailable() || Configuration.faxAvailable())
                && !StringUtils.isEmpty(activity)
                && DSApi.context().hasPermission(DSPermission.WWF_DEKRETACJA_ZBIORCZA);

        if (!collectiveAssignmentPossible)
            return null;

        Preferences prefs = null;
        String[] keys = null;
        try
        {
            if (DSApi.context().userPreferences().nodeExists("collectiveassignment")
                && DSApi.context().userPreferences().node("collectiveassignment").keys().length > 0)
            {
                prefs = DSApi.context().userPreferences().node("collectiveassignment");
            }
            else if (DSApi.context().systemPreferences().nodeExists("modules/office/collectiveassignment")
                && DSApi.context().systemPreferences().node("modules/office/collectiveassignment").keys().length > 0)
            {
                prefs = DSApi.context().systemPreferences().node("modules/office/collectiveassignment");
            }

            if (prefs != null) {
                keys = prefs.keys();
                Arrays.sort(keys);
            }

        }
        catch (BackingStoreException e)
        {
            throw new EdmException(sm.getString("BladOdczytuUstawienUzytkownika"), e);
        }

        if (prefs == null)
        {
            return null;
        }


        //private Map<String,String> substituted;
        //private Map<String, String> assignmentsMap;
        //private Map<String, String> wfProcesses;

        Map<String,String> assignmentsMap = new LinkedHashMap<String, String>();
        Map<String,String> wfProcesses = new LinkedHashMap<String, String>();
        Map<String,String> substituted = new LinkedHashMap<String, String>();
        Date now = new Date();

        for (int i=0; i < keys.length; i++)
        {
            AssignmentDescriptor ad =
                AssignmentDescriptor.forDescriptor(prefs.get(keys[i], ""));

            assignmentsMap.put(ad.toDescriptor(), ad.format());


            DSUser temp = null;
            if(ad.getUsername()!=null)
            	temp=DSUser.findByUsername(ad.getUsername());

            if(temp!=null && temp.getSubstituteUser()!=null && temp.getSubstitutedFrom().before(now))
                substituted.put(temp.asFirstnameLastname(),temp.getSubstituteUser().asFirstnameLastname());
            else substituted.put("","");

        }

        StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        if (WorkflowUtils.isManual(WorkflowFactory.getWfActivity(activity).container().manager()))
        {
            wfProcesses.put(
                ProcessId.create(
                    InternalWorkflowService.NAME,
                    WorkflowUtils.iwfPackageName(),
                    WorkflowUtils.iwfManualName()).toString(),
                    smG .getString("doRealizacji"));
        }
        wfProcesses.put(
            ProcessId.create(
                InternalWorkflowService.NAME,
                WorkflowUtils.iwfPackageName(),
                WorkflowUtils.iwfCcName()).toString(),
                smG.getString("doWiadomosci"));

        Map<String,Map<String,String>> results = new LinkedHashMap<String, Map<String,String>>();
        results.put("assignmentsMap", assignmentsMap);
        results.put("substituted", substituted);
        results.put("wfProcesses", wfProcesses);
        return results;
    }



    /**
     * Wykonuje zbiorcz� dekretacj�.
     *
     * @param assignments
     * @return True, je�eli bie��cy dokument zosta� przekierowany do kogo� innego
     *  i u�ytkownik nie mo�e ju� powr�ci� do jego edycji.
     * @throws EdmException
     */
    public static boolean collectiveAssignment(final String[] assignments,
        final WfActivity wfActivity, Long docId, ActionEvent event) throws EdmException
    {
        StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        // flaga okre�laj�ca, czy bie��cy dokument zosta� ju�
        // przydzielony nowemu procesowi potrzebuj�cemu
        // w�asnej kopii dokumentu
        boolean mainDocumentReassigned = false;
        Long originalDocumentId = docId;
        for (int i=0; i < assignments.length; i++)
        {
            AssignmentDescriptor ad = AssignmentDescriptor.forDescriptor(assignments[i]);

            ProcessId processId = ProcessId.parse(
                ServletActionContext.getRequest().getParameter("assignmentProcess_"+assignments[i]));

            String objective =
                WorkflowUtils.iwfManualName().equals(processId.getProcessDefinitionId())
                ? smG.getString("doRealizacji")
                : smG.getString("doWiadomosci");

            String username = ad.getUsername();
            // sprawdzenie czy istnieje taki u�ytkownik
            DSUser.findByUsername(username);
            if(DSUser.findByUsername(username)==null)
            {
                log.info("Nie znaleziono u�ytkownika "+ username+"do dekretacji");
                return false;
            }
            String guid = ad.getGuid();

            if (log.isDebugEnabled())
                log.debug("pid="+processId+" username="+username+" guid="+guid);

            // dzia�, w kt�rym jest wybrany u�ytkownik
            DSDivision division = DSDivision.find(guid);

            if (log.isDebugEnabled())
                log.trace("division="+division);

            // proces, jaki ma zosta� uruchomiony
            WfProcessMgr mgr = WorkflowUtils.findProcessMgr(processId);

            // je�eli bie��cy proces jest typu cc - nie mo�na utworzy� procesu r�cznego
            // je�eli bie��cy proces jest typu r�cznego - nie mo�na przedekretowa� je�eli
            // canAssign si� na to nie zgadza
            if (WorkflowUtils.isManual(wfActivity))
            {
                Long documentId = WorkflowUtils.findParameterValue(wfActivity.process_context(), "ds_document_id", Long.class);
                OfficeDocument document = OfficeDocument.findOfficeDocument(documentId);
                try
                {
                    WorkflowUtils.checkCanAssign(document, new WorkflowActivity(wfActivity.key()));
                }
                catch (EdmException e)
                {
                    event.addActionError(sm.getString("NiePrzedekretowanoPismaDoRealizacji")+e.getMessage()+")");
                    continue;
                }
            }
            else
            {
                if (WorkflowUtils.isManual(mgr))
                {
                    event.addActionError(sm.getString("NieMoznaPrzedekretowacDoRealizacjiPismaOtrzymanegoDoWiadomosci"));
                }
            }

            WorkflowProcess processDef = mgr.getProcessDefinition();

            if (log.isDebugEnabled())
                log.trace("mgr="+mgr);

            // flaga okre�laj�ca, czy uruchamiany proces powinien mie�
            // w�asn� kopi� pisma
            boolean needsOwnDocument = false;

            // TODO: sprawdza� inaczej - nazw� procesu (obieg_reczny)
            ExtendedAttribute attr = processDef.getExtendedAttribute("ds_needs_own_document");
            if (attr != null && Boolean.valueOf(attr.getValue()).booleanValue())
            {
                needsOwnDocument = true;
            }

            OfficeDocument currentDocument;

            if (log.isDebugEnabled())
            {
                log.trace("process="+processId+", needsOwnDocument="+needsOwnDocument);
                log.trace("glowny dokument: "+docId);
            }

            // nowy proces potrzebuje w�asnej kopii dokumentu - tworzenie klonu w razie potrzeby
            // i umieszczanie go w currentDocument
            if (needsOwnDocument)
            {
                // tworzenie kopii dokumentu, je�eli bie��cy zosta�
                // ju� pos�any z innym procesem, za� proces wymaga oddzielnej kopii dokumentu

                if (mainDocumentReassigned)
                {
                    OfficeDocument document = OfficeDocument.findOfficeDocument(docId);
                    // w wypadku pism przychodz�cych klonowaniu mo�e podlega� tylko dokument
                    // g��wny, kt�ry nale�y odszuka�, je�eli mamy jeden z dokument�w ju� sklonowanych
                    if (document.getType() == DocumentType.INCOMING
                        && ((InOfficeDocument) document).getMasterDocument() != null)
                    {
                        document = ((InOfficeDocument) document).getMasterDocument();
                    }

                    currentDocument = (OfficeDocument) document.cloneObject(null);

                    if (currentDocument.getType() == DocumentType.INCOMING)
                    {
                        ((InOfficeDocument) currentDocument).setMasterDocument((InOfficeDocument) document);
                    }

                    if (log.isDebugEnabled())
                        log.debug("kopia dokumentu "+docId+": "+currentDocument.getId());
                }
                else
                {
                    mainDocumentReassigned = true;
                    currentDocument = OfficeDocument.findOfficeDocument(docId);
                }
            }
            else
            {
                currentDocument = OfficeDocument.findOfficeDocument(docId);
            }

            // tworzenie i uruchamianie procesu workflow
            // TODO: dla zewn�trznego wf Requester != null
            WfProcess proc = mgr.create_process(null);
            NameValue[] procCtx = proc.process_context();
            WorkflowUtils.findParameter(procCtx, "ds_document_id").value().set(currentDocument.getId());
            WorkflowUtils.findParameter(procCtx, "ds_document_id").setValueInt(currentDocument.getId());
            // do kontroli uprawnie� - wf z tego nie korzysta
            WorkflowUtils.findParameter(procCtx, "ds_finished").value().set(Boolean.FALSE);
            WorkflowUtils.findParameter(procCtx, "ds_last_activity_user").value().set(DSApi.context().getPrincipalName());
            WorkflowUtils.findParameter(procCtx, "ds_objective").value().set(objective);
            // parametry u�ywane podczas tworzenia historii dekretacji
            // w AcquireTaskAction
            // dotyczy tylko wbudowanego procesu dekretacji r�cznej
            // (s� tam wymagane zmienne)
            WorkflowUtils.findParameter(procCtx, "ds_source_user").value().set(DSApi.context().getPrincipalName());
            WorkflowUtils.findParameter(procCtx, "ds_source_division").value().set(DSDivision.ROOT_GUID);

            WorkflowUtils.findParameter(procCtx, "ds_participant_id").value().set(username);
            WorkflowUtils.findParameter(procCtx, "ds_assigned_division").value().set(division.getGuid());
            WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(username);

            if (log.isDebugEnabled())
                log.trace("proc.start()");

            proc.start();

            if (WorkflowUtils.isManual(mgr))
                currentDocument.changelog(DocumentChangelog.WF_START_MANUAL);

            if (log.isDebugEnabled())
                log.trace("history");

			{
				boolean isManual = WorkflowUtils.isManual(proc.manager());
				DSUser substitute = isManual ? DSUser.findByUsername(username)
						.getSubstituteUser() : null;
				currentDocument
						.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
								DSApi.context().getPrincipalName(),
								DSDivision.ROOT_GUID,
								username,
								division.getGuid(),
								substitute != null ? substitute.getName() : null,
								(substitute != null && substitute.getDivisions().length > 0) ? substitute.getDivisions()[0]
										.getGuid()
										: null,
								objective,
								isManual ? smG.getString("doRealizacji") : smG
										.getString("doWiadomosci"),
								false,
								isManual ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION
										: AssignmentHistoryEntry.PROCESS_TYPE_CC));
			}

            if (needsOwnDocument)
            {
                currentDocument.setCurrentAssignmentGuid(division.getGuid());
                currentDocument.setDivisionGuid(division.getGuid());
                currentDocument.setCurrentAssignmentUsername(username);
                currentDocument.setCurrentAssignmentAccepted(Boolean.FALSE);
            }
            event.addActionMessage(((currentDocument.getType() == DocumentType.ORDER) ? smG.getString("Polecenie"): smG.getString("Pismo"))+" "+currentDocument.getOfficeNumber()+" "+smG.getString("zadekretowanoNaUzytkownika") + " " +
                   DSUser.findByUsername(ad.getUsername()).asFirstnameLastname()+ " (" + division.getName()+")");
            TaskSnapshot.updateByDocumentId(currentDocument.getId(), currentDocument.getStringType());
        }

        if (mainDocumentReassigned)
        {
            WorkflowUtils.findParameter(wfActivity.process_context(),
                "ds_finished").value().set(Boolean.TRUE);
            wfActivity.complete();
            TaskSnapshot.updateByDocumentId(originalDocumentId, OfficeDocument.findOfficeDocument(originalDocumentId).getStringType());
        }

        return mainDocumentReassigned;
    }
    /**
     * Tworzy nowy proces dla podanego dokumentu - zadanie pojawia si� u u�ytkownika aktualnie
     * zalogowanego w sesji lub u koordynatora.
     * 
     * Proces ten b�dzie typu manual
     *
     * @param doc
     * @param toCoordinator   true <=> utworzenie zadanie mo�e nie pojawi� si� u osoby generuj�cej ten
     * dokument, ale u odpowiedniego koordynatora, kt�ry jest wyznaczany na podstawie rodzaju tego dokumentu.
     * @param objective
     *
     * @throws EdmException
     */
    public static String createNewProcess(OfficeDocument doc, boolean toCoordinator, String objective) throws EdmException {
    	return createNewProcess(doc,
    			toCoordinator,
    			objective,
    			DSApi.context().getPrincipalName(),
    			DSDivision.ROOT_GUID,
    			DSApi.context().getPrincipalName());
    }


    public static String createNewProcess(OfficeDocument doc,
    		boolean toCoordinator,
    		String objective, 
    		String participantName,
    		String assignedDivision,
    		String assignedUserName) throws EdmException
    {
    	String result = null;
        if (Configuration.officeAvailable() || Configuration.faxAvailable())
        {
            // tworzenie i uruchamianie wewn�trznego procesu workflow
            WfService ws = WorkflowServiceFactory.newInternalInstance().newService();
            ws.connect(DSApi.context().getPrincipalName());

            WfProcessMgr mgr = ws.getProcessMgrs(WorkflowUtils.iwfPackageName(), WorkflowUtils.iwfManualName())[0];
            WfProcess proc = mgr.create_process(null);
            NameValue[] procCtx = proc.process_context();
            WorkflowUtils.findParameter(procCtx, "ds_document_id").value().set(doc.getId());
            WorkflowUtils.findParameter(procCtx, "ds_document_id").setValueInt(doc.getId());
            // do kontroli uprawnie� - wf z tego nie korzysta
            WorkflowUtils.findParameter(procCtx, "ds_finished").value().set(Boolean.FALSE);
            WorkflowUtils.findParameter(procCtx, "ds_last_activity_user").value().set(DSApi.context().getPrincipalName());
            if(objective==null)
            	objective="x";
            
            WorkflowUtils.findParameter(procCtx, "ds_objective").value().set(objective);
            // parametry u�ywane podczas tworzenia historii dekretacji
            // w AcquireTaskAction
            // dotyczy tylko wbudowanego procesu dekretacji r�cznej
            // (s� tam wymagane zmienne)
            WorkflowUtils.findParameter(procCtx, "ds_source_user").value().set(DSApi.context().getPrincipalName());
            WorkflowUtils.findParameter(procCtx, "ds_source_division").value().set(DSDivision.ROOT_GUID);

            ProcessCoordinator coordinator = null;

        	if(toCoordinator && doc.getDocumentKind() != null)
        	{
        		coordinator = doc.getDocumentKind().logic().getProcessCoordinator(doc);
        	}
        	else
        	{
        		coordinator = null;
        	}
            	//coordinator = toCoordinator && doc.getDocumentKind() != null ? doc.getDocumentKind().logic().getProcessCoordinator(doc) : null;


            if (coordinator == null)
            {
                if (assignedUserName != null)
                {
                    WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(assignedUserName);
                    WorkflowUtils.findParameter(procCtx, "ds_participant_id").value().set(assignedUserName);
                }
                else
                {
                    WorkflowUtils.findParameter(procCtx, "ds_participant_id").value().set("ds_guid_"+assignedDivision);
                    WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(DSApi.context().getPrincipalName());
                }            
                WorkflowUtils.findParameter(procCtx, "ds_assigned_division").value().set(assignedDivision);	
            	
//                // dekretacja na bie��cego u�ytkownika
//                // id uczestnika (patrz WfProcessImpl.findResources())
//
//                WorkflowUtils.findParameter(procCtx, "ds_participant_id").value().set(participantName);
//                WorkflowUtils.findParameter(procCtx, "ds_assigned_division").value().set(assignedDivision);
//                if(assignedUserName != null)
//                	WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(assignedUserName);
//              //  else
//              //  	WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(participantName);

            }
            else
            {
	            	DSUser coordinatorUser = null;
	            	DSUser substitute = null;
	            	if(coordinator.getUsername() != null)
	            	{
	            		coordinatorUser = DSUser.findByUsername(coordinator.getUsername());
	            		substitute = coordinatorUser.getSubstituteUser();
	            	}
	                if (substitute != null)
	                {
	                    // dekretacja na zast�pc� koordynatora
	                    WorkflowUtils.findParameter(procCtx, "ds_participant_id").value().set(substitute.getName());
	                    WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(substitute.getName());
	                }
	                else
	                {
	                    if (coordinatorUser != null)
	                    {
	                        WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(coordinatorUser.getName());
	                        WorkflowUtils.findParameter(procCtx, "ds_participant_id").value().set(coordinatorUser.getName());
	                    }
	                    else
	                    {
	                        WorkflowUtils.findParameter(procCtx, "ds_participant_id").value().set("ds_guid_"+coordinator.getGuid());
	                        WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(DSApi.context().getPrincipalName());
	                    }

	                }

	                WorkflowUtils.findParameter(procCtx, "ds_assigned_division").value().set(coordinator.getGuid());

	                if(coordinator.getUsername() != null)
	                	doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
	                        DSApi.context().getPrincipalName(), "x",
	                        (String) (substitute == null ? coordinator.getUsername() : substitute.getName()),
	                        coordinator.getGuid(), (substitute != null ? sm.getString("PismoPrzekazaneDoUzytkownikaZastepujacegoKoordynatora")
	                        		: sm.getString("PismoPrzekazaneDoKoordynatora")+" "),
	                        		sm.getString("doRealizacji"), false, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION), true, null);
	                else
	                	doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
	                            DSApi.context().getPrincipalName(), "x",
	                            null,
	                            coordinator.getGuid(), sm.getString("PismoPrzekazaneDoKoordynatora") +
	                                DSDivision.find(coordinator.getGuid()).getName(),
	                            sm.getString("doRealizacji"), false, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION), true, null);
            	}
            proc.start();
            result = String.valueOf(((WfProcessImpl)proc).getId());
            doc.changelog(DocumentChangelog.WF_START_MANUAL);
        }
        return result;
    }

    public static String createNewProcess(OfficeDocument doc, boolean toCoordinator) throws EdmException
    {
        return createNewProcess(doc, toCoordinator, sm.getString("task.list.PismoPrzyjete"));
    }

    /**
     * Dekretacja wielu zada�.
     *
     * @param activityIds identyfikatory zada� kt�re przedekretowa�
     * @param username u�ytkownik, do kt�ego nale�� przekazane zadania
     * @param plannedAssignments
     * @param onePlannedAssignment
     * @param event
     */
    public static void multiAssignment(String[] activityIds, String username, String[] plannedAssignments, String onePlannedAssignment, ActionEvent event) throws EdmException
    {
        // dla ka�dego zadania

        if (!DSApi.context().hasPermission(DSPermission.WWF_DEKRETACJA_DOWOLNA)){
            throw new EdmException(sm.getString("UzytkownikNieMaUprawnienDoDekretacjiZadania"));
        }

        // dla ka�dego zadania
        for (int i=0; i < activityIds.length; i++) 
        {
        	WorkflowActivity aactivity = WorkflowFactory.getInstance().getWfActivity(activityIds[i],false);
        	String usern = aactivity.getCurrentUser(); 
        	String participantId = (String)WorkflowUtils.findParameter(
	                aactivity.getActivity().process_context(), "ds_participant_id").
	                value().value();

        	if (username == null) {
        		username = DSApi.context().getPrincipalName();
        	}

        	if (usern == null || participantId.startsWith("ds_guid")) {
        		usern = username;
        	}

        	WorkflowFactory.getLog().debug("internal dekretacja, wlasciciel czynnosci" + activityIds[i] + ": " + usern );

        	WfAssignment assignment;
        	try {
        		assignment = WorkflowFactory.getWfAssignment(
        				WorkflowFactory.workflowNameOfId(activityIds[i]),
        				WorkflowFactory.getInstance().keyOfId(activityIds[i]),
        				usern);
        	} catch (EdmException e) {
        		//kaszana, nie wiadomo jakie wziac wfactivity, niskopoziomowo robimy nowe
        		DSApi.context().makeWfAssignment(activityIds[i]);
        		assignment = WorkflowFactory.getWfAssignment(
        				WorkflowFactory.workflowNameOfId(activityIds[i]),
        				WorkflowFactory.getInstance().keyOfId(activityIds[i]),
        				DSApi.context().getPrincipalName());
        	}

            WfActivity activity = assignment.activity();

            final NameValue nvLastUser = WorkflowUtils.findParameter(activity.process_context(), "ds_last_activity_user");
            if (nvLastUser != null)
            {
                nvLastUser.value().set(DSApi.context().getPrincipalName());
            }

            Long docId = (Long) WorkflowUtils.findParameter(
                    activity.process_context(), "ds_document_id").value().value();

            if (!(WorkflowUtils.isManual(activity) || AvailabilityManager.isAvailable("workflow.assignCC")))
            {
            	System.err.println("IWFMNGR - 556");
                if(event!=null)
                    event.getLog().warn(sm.getString("WymaganyJestProcesDekretacjiRecznej"));
                continue;
            }

            if (!assignment.get_accepted_status()) {
                assignment.set_accepted_status(true);
            }

            if (!StringUtils.isEmpty(onePlannedAssignment)){
                // pojedyncza dekretacja
                if (multiOnePlannedAssignment(onePlannedAssignment, event, activity)) continue;
            } else {
                multiManyPlannedAssignments(plannedAssignments, event, activity);
            }

            try {
                DSApi.context().session().flush();
            } catch(HibernateException e) {
                throw new EdmException(e);
            }

            TaskSnapshot.updateByDocumentId(docId, OfficeDocument.findOfficeDocument(docId).getStringType());
        }
    }

    //metoda wydzielona z multiAssignment
    private static void multiManyPlannedAssignments(String[] plannedAssignments, ActionEvent event, WfActivity activity) throws EdmException {
        // przejrzenie listy planowanych dekretacji
        // dekretacja "do realizacji" powoduje przekazanie bie��cego
        // zadania innemu u�ytkownikowi, ka�da inna powoduje
        // uruchomienie nowego procesu

        for (int j=0; j < plannedAssignments.length; j++) {

            PlannedAssignment pa = new PlannedAssignment(plannedAssignments[j]);

            String currentDivisionGuid = (String) WorkflowUtils.findParameter(
                activity.process_context(), "ds_assigned_division").value().value();
            Long documentId = (Long) WorkflowUtils.findParameter(
                activity.process_context(), "ds_document_id").value().value();

            if (pa.getKind().equals(PlannedAssignment.KIND_EXECUTE))
            {
                // Brief IV: je�eli wyznaczono termin zako�czenia zadania,
                // u�ytkownik nie mo�e zako�czy� pracy z dokumentem, dop�ki
                // nie doda uwagi

                push(activity, pa, event);
            }
            else
            {
                start(pa, currentDivisionGuid, documentId, event);
            }
        }
    }

    //metoda wydzielona z multiAssignment
    private static boolean multiOnePlannedAssignment(String onePlannedAssignment, ActionEvent event, WfActivity activity) throws EdmException {
        PlannedAssignment pa = new PlannedAssignment(onePlannedAssignment);
        String currentDivisionGuid = (String) WorkflowUtils.findParameter(
            activity.process_context(), "ds_assigned_division").value().value();
        Long documentId = (Long) WorkflowUtils.findParameter(
            activity.process_context(), "ds_document_id").value().value();

        if (pa.getKind().equals(PlannedAssignment.KIND_EXECUTE)){
            // Brief IV: je�eli wyznaczono termin zako�czenia zadania,
            // u�ytkownik nie mo�e zako�czy� pracy z dokumentem, dop�ki
            // nie doda uwagi

            push(activity, pa, event);
        }
        else
        {
            start(pa, currentDivisionGuid, documentId, event);
        }
        return false;
    }

    private static boolean handleNationwide(ActionEvent event, WfActivity activity, Long documentId) throws EdmException {
        if (activity.container() instanceof WfProcessImpl)
        {
            OfficeDocument document = OfficeDocument.findOfficeDocument(documentId);
            WfProcessImpl wfpi = (WfProcessImpl) activity.container();
            if (wfpi.getDeadlineTime() != null)
            {
                List<Remark> remarks = document.getRemarks();
                boolean found = false;
                for (Remark remark : remarks)
                {
                    if (remark.getCtime().getTime() > activity.last_state_time().getTime() &&
                        remark.getAuthor().equals(DSApi.context().getPrincipalName()))
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    if(event!=null)
                        event.addActionError(sm.getString("NieZadekretowanoDokumentu")+" "+document.getOfficeNumber()
                            + sm.getString("PoniewazUzytkownikNieDodalUwagi"));
                    return true;
                }
            }

        }
        return false;
    }

    private static void push(WfActivity activity, PlannedAssignment plannedAssignment, ActionEvent event) throws EdmException
    {
        StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
        // aktualizacja parametr�w procesu
        final NameValue[] context = activity.process_context();
        final NameValue[] extAttrs = activity.extended_attributes();

        //final List inputFields = new LinkedList();
        for (int i=0; i < extAttrs.length; i++)
        {
            if (extAttrs[i].name().equals("ds_input_update"))
            {
                // nazwa zmiennej, dla kt�rej utworz� pole edycyjne
                final String value = (String) extAttrs[i].value().value();

                // nowa warto�� parametru
                final String newValue = ServletActionContext.getRequest().getParameter("wfparam_"+value);

                final NameValue nvVar = WorkflowUtils.findParameter(context, value);
                if (nvVar == null)
                {
                    //event.getLog().warn("W procesie brakuje parametru "+value);
                    continue;
                }

                // je�eli nie podano warto�ci dla typu ENUM (formularz
                // nie powinien na to pozwala�), pozostawiam warto��
                // aktualn�
                if (StringUtils.isEmpty(newValue) &&
                    nvVar.value().kind().type() == AnyObjectKind.ENUM)
                    continue;

                nvVar.value().set(
                    WorkflowUtils.safeConvert(newValue, nvVar.value().kind().type()));
            }
        }

        // jako �r�d�owy u�ytkownik dekretacji - zalogowany u�ytkownik
        WorkflowUtils.findParameter(activity.process_context(), "ds_source_user").
            value().set(DSApi.context().getPrincipalName());
        // jako �r�d�owy dzia� dekretacji - dzia�, na kt�ry dekretowano to zadanie
        String sourceGuid = (String) WorkflowUtils.findParameter(
            activity.process_context(), "ds_assigned_division").value().value();
        WorkflowUtils.findParameter(activity.process_context(), "ds_source_division").
            value().set(sourceGuid);

        // DivisionNotFoundException
        //Division.find(divisionGuid);

        // ustawiam parametry dotycz�ce dekretacji r�cznej
        String targetGuid = plannedAssignment.getDivisionGuid();
        WorkflowUtils.findParameter(
            activity.process_context(), "ds_assigned_division").
            value().set(targetGuid);

        DSUser user = plannedAssignment.getUser();

        if (user != null)
        {
            final String username = user.getName();

            DSUser u = DSUser.findByUsername(username); // UserNotFoundException

            WorkflowUtils.findParameter(
                activity.process_context(), "ds_assigned_user").
                value().set(username);

            WorkflowUtils.findParameter(
                activity.process_context(), "ds_participant_id").
                value().set(username);

            Long documentId = (Long) WorkflowUtils.findParameter(
                activity.process_context(), "ds_document_id").value().value();
            OfficeDocument currentDocument = OfficeDocument.findOfficeDocument(documentId);
            Integer officeNumber = currentDocument.getOfficeNumber();
            if(event!=null)
                event.addActionMessage(((currentDocument.getType() == DocumentType.ORDER) ? smG.getString("Polecenie") : smG.getString("Pismo"))+" "+(officeNumber != null ? officeNumber +" ": " ")+smG.getString("zadekretowanoNaUzytkownika") + " " +
                    plannedAssignment.getUser().asFirstnameLastname()+ (plannedAssignment.getDivision() != null ? " (" + plannedAssignment.getDivision().getName()+")" : ""));

        }
        else // if (assignmentTarget.startsWith("divisionGuid:"))
        {
            // nie sprawdzam warto�ci guid w assignmentTarget,
            // jest taka sama jak w parametrze divisionGuid
            WorkflowUtils.findParameter(
                activity.process_context(), "ds_participant_id"). 
                value().set("ds_guid_"+plannedAssignment.getDivision().getGuid());

            Long documentId = (Long) WorkflowUtils.findParameter(
                activity.process_context(), "ds_document_id").value().value();
            OfficeDocument currentDocument = OfficeDocument.findOfficeDocument(documentId);
            Integer officeNumber = currentDocument.getOfficeNumber();
            if(event!=null)
            event.addActionMessage(((currentDocument.getType() == DocumentType.ORDER) ? smG.getString("Polecenie") : smG.getString("Pismo"))+" "+(officeNumber != null ? officeNumber +" ": " ")+
            		smG.getString("zadekretowanoNaDzial") +" "+plannedAssignment.getDivision().getName());
        }

        WorkflowUtils.findParameter(
            activity.process_context(), "ds_objective").
            value().set(plannedAssignment.getObjective() != null ? plannedAssignment.getObjective() : "");
                //objective != null ? objective : "");

        activity.set_process_context(context);
        activity.complete();

        // odczytanie identyfikatora dokumentu zwi�zanego z zadaniem,
        // je�eli nie istnieje - zadanie jest pomijane
        final NameValue nvDocumentId =
            WorkflowUtils.findParameter(activity.process_context(),
                "ds_document_id");

        if (nvDocumentId != null && nvDocumentId.value() != null)
        {
            Document document = Document.find((Long) nvDocumentId.value().value());

			{
				boolean isManual = WorkflowUtils.isManual(activity);
				DSUser substitute = isManual && user!= null ? user.getSubstituteUser() : null;

				((OfficeDocument) document)
						.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
								DSApi.context().getPrincipalName(),
								sourceGuid,
								user != null ? user.getName() : null,
								targetGuid,
								substitute != null ? substitute.getName() : null,
								(substitute != null && substitute.getDivisions().length > 0)? substitute.getDivisions()[0].getGuid() : null,
								plannedAssignment.getObjective() != null ? plannedAssignment
										.getObjective()
										: "",
								isManual ? smG.getString("doRealizacji") 
										 : smG.getString("doWiadomosci"),
								false,
								isManual? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION
										: AssignmentHistoryEntry.PROCESS_TYPE_CC));
			}
            ((OfficeDocument) document).setCurrentAssignmentGuid(targetGuid);
            ((OfficeDocument) document).setDivisionGuid(targetGuid);
            ((OfficeDocument) document).setCurrentAssignmentUsername(user != null ? user.getName() : null);
            ((OfficeDocument) document).setCurrentAssignmentAccepted(Boolean.FALSE);

            try
            {
	            if(plannedAssignment.getObjective().equalsIgnoreCase(Docusafe.getAdditionProperty("acceptable.objective")) && document.getDocumentKind().logic() instanceof Acceptable)
	            {
	            	AcceptanceCondition fakeCondition = new AcceptanceCondition();
	            	fakeCondition.setDivisionGuid(targetGuid);
	            	fakeCondition.setUsername(user != null ? user.getName() : null);
	            	Acceptable logic = InvoiceLogic.getInstance();
	            	logic.onSendDecretation(document, fakeCondition);
	            }
            }
            catch (Exception e) 
            {
				log.debug(e.getMessage(),e);
			}
            
            if (document.getType() == DocumentType.INCOMING)
            {
                InOfficeDocument in = (InOfficeDocument) document;
                DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_DOCUMENT_INFO).documentId(in.getId());
                /*
                try
                {
                    Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                    bip.submit(in);
                }
                catch (ServiceException e)
                {
                }
                */
            }


        }
    }

    private static class Target { DSDivision division; DSUser user; }
    private static void start(PlannedAssignment plannedAssignment, String currentDivisionGuid,
                       Long documentId, ActionEvent event)
        throws EdmException {
        log.trace("start");    

        // je�eli dekretacja na u�ytkownika - zwyczajnie
        // je�eli na dzia�:
        //   je�eli na dowolnego u�ytkownika w dziale - zwyczajnie
        //   je�eli na wszystkich w dziale - lista user�w i oddzielne procesy
        //   je�eli na wszystkich w poddzia�ach - lista user�w i oddzielne procesy

        // je�eli na u�ytkownika lub (dzial) dowolnego u�ytkownika w dziale - zwyczajnie 
        // je�eli na wszystkich w dziale lub lub w poddzia�ach - lista u�ytkownik�w
        //   i dalej zwyczajnie

        // lista dzia��w (ew. z u�ytkownikami) na kt�rych trzeba zacz�� proces


        List<Target> targets = new LinkedList<Target>();

        //pocz�tek jest �adny - tworzymy targety - u�ytkownik�w na kt�rych dekretujemy
        prepareTargets(plannedAssignment, targets); 

        /// Porzu�cie wszelk� nadziej�, wy, kt�rzy tu wchodzicie 

        // w plannedAssignment jest nazwa procesu w pakiecie manual
        // tworzenie i uruchamianie wewn�trznego procesu workflow "do wiadomo�ci"
        WfService ws = WorkflowServiceFactory.newInternalInstance().newService();
        ws.connect(DSApi.context().getPrincipalName());
        for (Iterator iter=targets.iterator(); iter.hasNext(); )
        {
            Target target = (Target) iter.next();

            WfProcessMgr mgr = ws.getProcessMgrs(WorkflowUtils.iwfPackageName(), plannedAssignment.getKind())[0];
            WfProcess proc = mgr.create_process(null);

            NameValue[] procCtx = proc.process_context();
            WorkflowUtils.findParameter(procCtx, "ds_document_id").value().set(documentId);
            WorkflowUtils.findParameter(procCtx, "ds_document_id").setValueInt(documentId);
            // do kontroli uprawnie� - wf z tego nie korzysta
            if (target.user != null)
                WorkflowUtils.findParameter(procCtx, "ds_assigned_user").value().set(target.user.getName());
            NameValue dsFinished = WorkflowUtils.findParameter(procCtx, "ds_finished");
            if (dsFinished != null)
                dsFinished.value().set(Boolean.FALSE);
            WorkflowUtils.findParameter(procCtx, "ds_last_activity_user").value().set(DSApi.context().getPrincipalName());

            // jako �r�d�owy u�ytkownik dekretacji - zalogowany u�ytkownik
            WorkflowUtils.findParameter(proc.process_context(), "ds_source_user").
                value().set(DSApi.context().getPrincipalName());
            // jako �r�d�owy dzia� dekretacji - dzia�, na kt�ry dekretowano
            // bie��cy "g��wny" proces (ds_assigned_division pobierane
            // jest z obiektu activity)
            WorkflowUtils.findParameter(proc.process_context(), "ds_source_division").
                value().set(currentDivisionGuid);

            // ustawiam parametry dotycz�ce dekretacji r�cznej
            WorkflowUtils.findParameter(proc.process_context(), "ds_assigned_division").value().set(target.division.getGuid());

            StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
            if (target.user != null)
            {
                WorkflowUtils.findParameter(
                    proc.process_context(), "ds_assigned_user").
                    value().set(target.user.getName());

                WorkflowUtils.findParameter(
                    proc.process_context(), "ds_participant_id").
                    value().set(target.user.getName());

                OfficeDocument currentDocument = OfficeDocument.findOfficeDocument(documentId);
                Integer officeNumber = currentDocument.getOfficeNumber();
                if(event!=null)
                    event.addActionMessage(((currentDocument.getType() == DocumentType.ORDER) ? smG .getString("Polecenie") : smG.getString("Pismo"))+" "+(officeNumber != null ? officeNumber +" ": " ")+smG.getString("zadekretowanoNaUzytkownika") + " " +
                        target.user.asFirstnameLastname()+ " (" + target.division.getName()+")");
            }
            else
            {
                WorkflowUtils.findParameter(
                    proc.process_context(), "ds_participant_id").
                    value().set("ds_guid_"+target.division.getGuid());

                OfficeDocument currentDocument = OfficeDocument.findOfficeDocument(documentId);
                Integer officeNumber = currentDocument.getOfficeNumber();
                if(event!=null)
                    event.addActionMessage(((currentDocument.getType() == DocumentType.ORDER) ? smG.getString("Polecenie") : smG.getString("Pismo"))+" "+(officeNumber != null ? officeNumber +" ": " ")+smG.getString("zadekretowanoNaDzial") +
                        target.division.getName());
            }

            String kind;
            if (PlannedAssignment.KIND_CC.equals(plannedAssignment.getKind()))
                kind = sm.getString("doWiadomosci");
            else if (PlannedAssignment.KIND_SECOND_OPINION.equals(plannedAssignment.getKind()))
                kind = sm.getString("Konsultacja");
            else
                kind = sm.getString("doWiadomosci")+"*";

            String objective = TextUtils.trimmedStringOrNull(plannedAssignment.getObjective());
            String ds_objective = objective != null ? objective : kind;

            WorkflowUtils.findParameter(
                proc.process_context(), "ds_objective").value().set(ds_objective);

            OfficeDocument document = OfficeDocument.findOfficeDocument(documentId);

            document.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                    DSApi.context().getPrincipalName(),
                    currentDivisionGuid,
                    target.user != null ? target.user.getName() : null,
                    target.division.getGuid(),
                    kind,
                    WorkflowUtils.isManual(proc.manager()) ? smG.getString("doRealizacji") : smG.getString("doWiadomosci"),
                    false, WorkflowUtils.isManual(proc.manager()) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION : AssignmentHistoryEntry.PROCESS_TYPE_CC));

            if (PlannedAssignment.KIND_EXECUTE.equals(plannedAssignment.getKind()))
            {
                document.setCurrentAssignmentGuid(target.division.getGuid());
                document.setDivisionGuid(target.division.getGuid());
                document.setCurrentAssignmentUsername(target.user != null ? target.user.getName() : null);
                document.setCurrentAssignmentAccepted(Boolean.FALSE);
            }
            proc.start();
            if (WorkflowUtils.isManual(mgr))
            {
                Document.find(documentId).changelog(DocumentChangelog.WF_START_MANUAL);
            }
        }
    }

    /**
     * Przygotowuje Targety - u�ytkownik�w, kt�rzy b�d� widzieli zadania na swojej li�cie
     * @param plannedAssignment dekretacja
     * @param targets lista do wype�nienia
     * @throws EdmException
     */
    private static void prepareTargets(PlannedAssignment plannedAssignment, List<Target> targets) throws EdmException {
        if (plannedAssignment.getUser() != null)
        { 
            Target t = new Target();
            t.division = plannedAssignment.getDivision();
            t.user = plannedAssignment.getUser();
            targets.add(t);
        }
        else
        {
            // dowolny u�ytkownik w dziale
            if (plannedAssignment.getScope().equals(PlannedAssignment.SCOPE_ONEUSER))
            {
                Target t = new Target();
                t.division = plannedAssignment.getDivision();
                DSUser[] users = t.division.getUsers();
                if(users != null && users.length > 0 )
                {
                	t.user = users[0];
                }
                else
                	throw new EdmException("BrakUzytkownikowWdziale");
                targets.add(t);
            }
            // wszyscy w dziale
            else if (plannedAssignment.getScope().equals(PlannedAssignment.SCOPE_DIVISION))
            {
                DSUser[] users = plannedAssignment.getDivision().getUsers();
                for (int i=0; i < users.length; i++)
                {
                    Target t = new Target();
                    t.division = plannedAssignment.getDivision();
                    t.user = users[i];
                    targets.add(t);
                }
            }
            // wszyscy w dziale i w dzia�ach podrz�dnych
            else if (plannedAssignment.getScope().equals(PlannedAssignment.SCOPE_SUBDIVISIONS))
            {
                DSUser[] users = plannedAssignment.getDivision().getUsers(true);
                for (int i=0; i < users.length; i++)
                {
                    Target t = new Target();
                    t.division = plannedAssignment.getDivision();
                    t.user = users[i];
                    targets.add(t);
                }
            }
        }
    }


    public static void returnAssignment(WorkflowActivity activity) throws WorkflowException, EdmException {

    	 String sourceGuid = activity.getLastDivision();
         String sourceUser = activity.getLastUser();

    	 if (sourceUser != null) DSUser.findByUsername(sourceUser); // UserNotFoundException
         if (sourceGuid != null) DSDivision.find(sourceGuid); // DivisionNotFoundException

         WorkflowFactory.setSourceUser(activity.getActivity(),DSApi.context().getPrincipalName());

         String assignedDivision = TextUtils.trimmedStringOrNull(activity.getCurrentGuid());
         if (assignedDivision != null)
         {
         	WorkflowFactory.setSourceDivision(activity.getActivity(),assignedDivision);

         }

         // przekierowanie na dzia� i u�ytkownika �r�d�owego
         WorkflowUtils.findParameter(
             activity.getActivity().process_context(), "ds_assigned_division").
             value().set(sourceGuid);

         WorkflowUtils.findParameter(
             activity.getActivity().process_context(), "ds_assigned_user").
             value().set(sourceUser);

         WorkflowUtils.findParameter(
             activity.getActivity().process_context(), "ds_participant_id").
             value().set(sourceUser);

         WorkflowUtils.findParameter(
             activity.getActivity().process_context(), "ds_objective").
             value().set(sm.getString("ZwrotDekretacji"));

         final NameValue nvDocumentId =
             WorkflowUtils.findParameter(activity.getActivity().process_context(), "ds_document_id");

         Long id = null;
         if (nvDocumentId != null && nvDocumentId.value() != null)
         {
             id = (Long) nvDocumentId.value().value();
             Document.changelog(id, DSApi.context().getPrincipalName(),
                 DocumentChangelog.WF_RETURN);
         }

         activity.getActivity().complete();
    }

}
