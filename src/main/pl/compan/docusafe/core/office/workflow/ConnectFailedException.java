package pl.compan.docusafe.core.office.workflow;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ConnectFailedException.java,v 1.1 2004/05/16 20:10:29 administrator Exp $
 */
public class ConnectFailedException extends WorkflowException
{
    public ConnectFailedException(String message)
    {
        super(message);
    }

    public ConnectFailedException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ConnectFailedException(Throwable cause)
    {
        super(cause);
    }
}
