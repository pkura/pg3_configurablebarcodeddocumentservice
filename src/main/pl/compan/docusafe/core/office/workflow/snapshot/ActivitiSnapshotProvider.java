package pl.compan.docusafe.core.office.workflow.snapshot;

import com.google.common.base.Equivalence;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.task.Task;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.*;
import pl.compan.docusafe.logic.AttachmentsManager;
import pl.compan.docusafe.process.*;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.sql.JdbcUtils;
import static pl.compan.docusafe.core.office.workflow.snapshot.Jbpm4SnapshotProvider.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *  Implementacja SnapshotProvider dla activiti
 */
class ActivitiSnapshotProvider implements SnapshotProvider {
    private final static Logger LOG = LoggerFactory.getLogger(ActivitiSnapshotProvider.class);

    static final String TABLE = "dsw_jbpm_tasklist";
    static final String RES_COLUMN = "assigned_resource";
    static final String DOCUMENT_ID_COLUMN = "document_id";
    /** warto�� kolumny process_name s�u��ca do identyfikacji r�nych SnapshotProvider�w */

    static final String DELETE_ONE_DOC_QUERY = "delete from "+TABLE+" where "+DOCUMENT_ID_COLUMN+ "=? and process_name = '" + ProcessEngineType.ACTIVITI.getKey() + "'";
    static final String DELETE_ONE_DOC_DOCTYPE_QUERY = "delete from "+TABLE+" where "+DOCUMENT_ID_COLUMN+ "=? and document_type=? and process_name = '" + ProcessEngineType.ACTIVITI.getKey() + "'" ;
    static final String DELETE_ONE_USER_QUERY = "delete from "+TABLE+" where "+RES_COLUMN+" =? and process_name = '" + ProcessEngineType.ACTIVITI.getKey() + "'";

    private final String userGuidsQuery;

    private final WorkflowEngine activitiWorkflowEngine;

    private final String inSqlQuery;
    private final String outSqlQuery;

    private final String allAssignedGroupsQuery;

    static Equivalence<TaskInfo> ID_EQUIVALENCE = new Equivalence<TaskInfo>(){
        @Override
        protected boolean doEquivalent(TaskInfo taskInfo, TaskInfo taskInfo2) {
            return taskInfo.getId().equals(taskInfo2.getId());
        }
        @Override
        protected int doHash(TaskInfo taskInfo) {
            return taskInfo.getId().hashCode();
        }
    };

    ActivitiSnapshotProvider(WorkflowEngine activitiWorkflowEngine) {
        this.activitiWorkflowEngine = activitiWorkflowEngine;
        this.inSqlQuery = prepareInSqlQuery();
        this.outSqlQuery = prepareOutSqlQuery();
        this.allAssignedGroupsQuery = prepareAllAssignedGroupsQuery();
        this.userGuidsQuery = prepareUserGuidsQuery();
    }

    private String prepareUserGuidsQuery() {
        String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
        return "SELECT guid FROM ds_user_to_division utd " + nolock + "\n"
             + "INNER JOIN ds_division d " + nolock + " ON d.id = utd.division_id\n"
             + "WHERE utd.user_id = ?";
    }

    /** Zapytanie zwraca guidy dzia��w, na kt�rych jest co� zadekretowane - umo�liwia to zoptymalizowanie zapyta� */
    private String prepareAllAssignedGroupsQuery() {
        String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
        return "SELECT DISTINCT group_id_ FROM act_hi_identitylink " + nolock + "WHERE type_ = 'candidate'";
    }

    private String prepareOutSqlQuery(){
        String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
        String sTrue = 	DSApi.isPostgresServer() ? "'t'" : "1";
        return "SELECT doc.id AS documentId,\n"
                + "doc.officenumber AS officeNumber,\n"
                + "doc.officenumberyear AS officeNumberYear,\n"
                + "dso_person.firstname AS firstname,\n"
                + "dso_person.lastname AS lastname,\n"
                + "dso_person.organization AS organization,\n"
                + "dso_person.street AS street,\n"
                + "dso_person.location AS location,\n"
                + "doc.summary AS summary,\n"
                + "CASE doc.internaldocument WHEN "+sTrue+" THEN 'internal' ELSE 'out' END AS docType,\n"
                + "dsdoc.source AS source,\n"
                + "rcpt.firstname AS rcptFirstname,\n"
                + "rcpt.lastname AS rcptLastname,\n"
                + "rcpt.organization AS rcptOrganization,\n"
                + "rcpt.street AS rcptStreet,\n"
                + "rcpt.location AS rcptLocation,\n"
                + "dso_container.id AS caseId,\n"
                + "dso_container.officeid AS officeCase,\n"
                + "doc.creatingUser AS author,\n"
                + "doc.prepstate AS prepState,\n"
                + "deliv.name AS delivery,\n"
                + "doc.clerk AS clerk,\n"
                + "dsdoc.ctime AS documentCtime,\n"
                 // 25
                + "dsdoc.dockind_id AS dockindId,\n"
                + "dsdoc.lastRemark AS lastRemark,\n"
                + "doc.referenceid AS referenceId\n"
                + "FROM dso_out_document doc " + nolock
                + "\nINNER JOIN ds_document dsdoc " + nolock + " ON doc.id = dsdoc.id\n"
                + "LEFT JOIN dso_person " + nolock + " ON (doc.sender = dso_person.id and dso_person.discriminator='SENDER')\n"
                + "LEFT JOIN dso_person rcpt " + nolock + " ON (doc.id = rcpt.document_id and (rcpt.posn = 0 or rcpt.posn IS NULL))\n"
                + "LEFT JOIN dso_out_document_delivery deliv " + nolock + " on (doc.delivery = deliv.id)\n"
                + "LEFT JOIN dso_container" + nolock + " on doc.case_id = dso_container.id\n";
    }

    private String prepareInSqlQuery(){
        String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
        boolean countReplies = Configuration.isAdditionOn(Configuration.ADDITION_COUNT_REPLIES_ON_TASKLIST);

        return "SELECT doc.id AS documentId,\n"
                + "doc.officenumber AS officeNumber,\n"
                + "doc.officenumberyear AS officeNumberYear,\n"
                + "dso_person.firstname AS firstname,\n"
                + "dso_person.lastname AS lastname,\n"
                + "dso_person.organization AS organization,\n"
                + "dso_person.street AS street,\n"
                + "dso_person.location AS location,\n"
                + "doc.summary AS summary,\n"
                + "'in' AS doctype,\n"
                + "doc.referenceid AS referenceId,\n"
                + "dsdoc.source AS source,\n"
                + "dso_container.id AS caseId,\n"
                + "dso_container.officeid AS officeCase,\n"
                + "doc.creatingUser AS author,\n"
                + "doc.clerk AS clerk,\n"
                // ilosc odpowiedzi na pismo
                + (countReplies ? "(SELECT count(indocument) FROM dso_out_document WHERE indocument=doc.id),\n" : "0,\n")
                + "doc.incomingDate AS incomingDate,\n"
                + "dsdoc.dockind_id AS dockindId,\n"
                + "dsdoc.ctime AS documentCtime,\n"
                + "dsdoc.lastRemark AS lastRemark\n"
                + "FROM dso_in_document doc \n"+ nolock
                + "INNER JOIN ds_document dsdoc " + nolock +" ON doc.id = dsdoc.id\n"
                + "LEFT JOIN dso_container" + nolock + " ON doc.case_id = dso_container.id\n"
                + "LEFT JOIN dso_person " + nolock + " ON doc.sender = dso_person.id AND dso_person.discriminator='SENDER'";
    }

    @Override
    public void updateTaskList(Session session, Map<Long, RefreshRequestBean> requests) {
        LOG.info("updatingTaskList - {} update(s)", requests.size());
        for(RefreshRequestBean rrb: requests.values()){
            try {
                switch (rrb.getUpdateType()){
                    case TaskSnapshot.UPDATE_TYPE_ONE_USER:
                        deleteForOneUser(session, rrb.getUsername());
                        createForOneUser(session, rrb.getUsername());
                        break;
                    case TaskSnapshot.UPDATE_TYPE_ONE_DOC:
                        deleteForOneDoc(session, rrb.getId());
                        createForOneDoc(session, rrb.getId(), rrb.getDoctype());
                        break;
                    default:
                        throw new UnsupportedOperationException("Brak wsparcia dla updateType = " + rrb.getUpdateType());
                }
            } catch (EdmException ex) {
                LOG.error("B��d podczas procesowania aktualizacji {}", rrb);
                LOG.error("Stacktrace:", ex);
            }
        }
    }

    private void createForOneDoc(Session session, long id, String documentType) throws EdmException {
        LOG.trace("creating tasks for documentId = {}", id);
        Collection<TaskHistoryInfo> tasks = activitiWorkflowEngine.getTasksWithHistoryForParam(DocuSafeProcessLogic.DOCUMENT_ID_PARAM_KEY, id);
        LOG.trace(".. found {} tasks for", tasks.size());
        if(tasks.isEmpty()){
            return;
        }


        String query;
        if(documentType.equals("anyType")) {
            documentType = Document.find(id ,false).getType().getName();
        }

        if(documentType.equals(InOfficeDocument.TYPE)) {
            query = inSqlQuery + "WHERE doc.id = ?";
        } else if(documentType.equals(OutOfficeDocument.TYPE) || documentType.equals(OutOfficeDocument.INTERNAL_TYPE)){
            query = outSqlQuery + "WHERE doc.id = ?";
        } else {
            throw new UnsupportedOperationException("Brak wsparcia dla doctype = " + documentType);
        }
        PreparedStatement ps = null;
        Transaction tx = null;
        ResultSet rs = null;
        boolean commited = false;
        try {
            tx = session.beginTransaction();
            ps = DSApi.context().prepareStatement(query);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            boolean documentExists = rs.next();
            if(!documentExists){
                throw new RuntimeException("Brak rezultat�w dla id = " + id);
            }
            JBPMTaskSnapshot snapshot = prepareSnapshot(rs);
            for(TaskHistoryInfo task: tasks){
                prepareSnapshotAndSave(snapshot, task, session);
            }

            tx.commit();
            commited = true;
        } catch(SQLException ex) {
            throw new RuntimeException(ex);
        } finally {
            if(!commited){
                tx.rollback();
            }
            DSApi.context().closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }
    }

	private void prepareSnapshotAndSave(JBPMTaskSnapshot snapshot, TaskHistoryInfo task, Session session) throws EdmException
	{
		snapshot.setProcess(ProcessEngineType.ACTIVITI.getKey());
		snapshot.setActivityKey(task.getId());
		snapshot.setReceiveDate(task.getAssignmentTime() == null ? task.getCreateTime() : task.getAssignmentTime());
		snapshot.setDeadlineTime(task.getDueTime());
		TaskService ts = ProcessEngines.getDefaultProcessEngine().getTaskService();

		Task activitiTask = ts.createTaskQuery().taskId(task.getId()).singleResult();
		if (activitiTask != null)
		{
			String decretationType = (String) ts.getVariableLocal(task.getId(), Jbpm4WorkflowFactory.DOCDESCRIPTION);
			if (decretationType != null && !decretationType.isEmpty())
			{
				snapshot.setDescription(decretationType);
				ts.removeVariableLocal(task.getId(),  Jbpm4WorkflowFactory.DOCDESCRIPTION);
				
			} else
			{
				snapshot.setDescription("Do Realizacji");
			}
			String clerk = (String)  ts.getVariableLocal(task.getId(), Jbpm4WorkflowFactory.CLERK);
			if (clerk != null && !clerk.isEmpty())
			{
				snapshot.setClerk(clerk);
				ts.removeVariableLocal(task.getId(), Jbpm4WorkflowFactory.CLERK);
			}
			
			String objective = activitiTask.getDescription();
			if (objective != null && !objective.isEmpty())
			{
				snapshot.setObjective(objective);
			}
			String decretationSender = activitiTask.getOwner();
			if (decretationSender != null && !decretationSender.isEmpty())
			{
				snapshot.setActivity_lastActivityUser(decretationSender);
			}
			if (activitiTask.getAssignee() != null)
			{  if(DSUser.isUserExists(activitiTask.getAssignee()))
				snapshot.setDecretationTo(activitiTask.getAssignee());
				else
				snapshot.setDecretation_to_div(activitiTask.getAssignee());
			}

		
		
		}

		String assigner = ObjectUtils.toString(activitiWorkflowEngine.getTaskParam(task.getId(), DocuSafeProcessLogic.ASSIGNER_PARAM_KEY), null);

		if (task.getAssignee() != null)
		{
			snapshot = snapshot.clone();
			String substitutedUser = DSUserCache.getSubstituteUser(task.getAssignee());
			snapshot.setDsw_resource_res_key(substitutedUser == null ? task.getAssignee() : substitutedUser);
			snapshot.setAssigned_user(task.getAssignee());
			if (ts.getVariableLocal(task.getId(), "newTaskAccepted") != null)
			{
				snapshot.setAccepted((Boolean) ts.getVariableLocal(task.getId(), "newTaskAccepted"));
				ts.removeVariableLocal(task.getId(), "newTaskAccepted");
			} else
			{
				snapshot.setAccepted(true);
			}
			OfficeDocument doc = OfficeDocument.findOfficeDocument(snapshot.getDocumentId(), false);
			doc.getDocumentKind().logic().acceptTask(doc, null, null);
			snapshot.setDockindId(doc.getDocumentKind().getId());
			snapshot.setSameProcessSnapshotsCount(1);
			session.save(snapshot);
		} else
		{

			snapshot.setAccepted(false);
			TaskCandidates candidates = activitiWorkflowEngine.getTaskCandidates(task.getId());
			if (candidates == null)
			{
				LOG.error("Zadanie {} nie ma przydzielonego usera zostanie zadekretowane na admina", task.getId());
				snapshot = snapshot.clone();
				snapshot.setDsw_resource_res_key("admin");
				snapshot.setSameProcessSnapshotsCount(1);
				snapshot.setObjective("Brak przydzielonej osoby");
				session.save(snapshot);
			} else
			{
				Map<String, String> targetUsersAndDivisions = Maps.newHashMap();

				for (String user : candidates.getUserNames())
				{
					String substitutedUser = DSUserCache.getSubstituteUser(user);
					targetUsersAndDivisions.put(substitutedUser == null ? user : substitutedUser, null);
				}
				for (String divisionGuid : candidates.getGroupNames())
				{
					try
					{
						DSDivision div = DSDivision.find(divisionGuid);
						for (DSUser user : div.getUsers())
						{
							String substitutedUser = DSUserCache.getSubstituteUser(user.getName());
							targetUsersAndDivisions.put(substitutedUser == null ? user.getName() : substitutedUser, divisionGuid);
						}
					} catch (DivisionNotFoundException ex)
					{
						LOG.warn("brak dzia�u '{}' zadanie nie zostanie przydzielone do czasu utworzenia dzia�u i od�wie�enia listy zada�", divisionGuid);
					}
				}
				for (Map.Entry<String, String> userDivision : targetUsersAndDivisions.entrySet())
				{
					snapshot = snapshot.clone();
					snapshot.setDsw_resource_res_key(userDivision.getKey());
					snapshot.setDivisionGuid(userDivision.getValue());
					snapshot.setSameProcessSnapshotsCount(targetUsersAndDivisions.size());
					session.save(snapshot);
				}
			}
		}
	}

    JBPMTaskSnapshot prepareSnapshot(ResultSet rs) throws SQLException, EdmException {
        JBPMTaskSnapshot ret = new JBPMTaskSnapshot();
        long documentId = rs.getLong("documentId");
        ret.setDocumentId(documentId);
        int officeNumber = rs.getInt("officeNumber");
        if(!rs.wasNull()) {
            ret.setDocumentOfficeNumber(officeNumber);
        }
        int officeNumberYear = rs.getInt("officeNumberYear");
        if(!rs.wasNull()){
            ret.setDocumentOfficeNumberYear(officeNumberYear);
        }

        ret.setIsAnyImageInAttachments(AttachmentsManager.isAnyImageInAttachments(documentId));
        ret.setIsAnyPdfInAttachments(AttachmentsManager.isAnyPdfInAttachments(documentId));

        ret.setPerson_firstname(rs.getString("firstname"));
        ret.setPerson_lastname(rs.getString("lastname"));
        ret.setPerson_street(rs.getString("street"));
        ret.setPerson_location(rs.getString("location"));
        ret.setPerson_organization(rs.getString("organization"));

        ret.setDocumentSummary(rs.getString("summary"));
        ret.setDocumentReferenceId(rs.getString("referenceId"));
        ret.setDocumentAuthor(rs.getString("author"));
        ret.setDocument_source(rs.getString("source"));
        ret.setDocumentType(rs.getString("docType"));
        if(ret.getDocumentType().equals("in")){
            ret.setIncomingDate(rs.getTimestamp("incomingDate"));
            ret.setReceiveDate(ret.getIncomingDate());
        } else {
            ret.setDocumentPrepState(OutOfficeDocument.getPrepStateString(rs.getString("prepState")));
            ret.setDocumentDelivery(rs.getString("delivery"));
        }

        ret.setDocumentClerk(rs.getString("clerk"));

        ret.setCaseId(rs.getLong("caseId"));
        ret.setOfficeCase(rs.getString("officeCase"));
        ret.setLastRemark(rs.getString("lastRemark"));
        ret.setAnswerCounter(0);
        ret.setDocumentCtimeAndCdate(rs.getTimestamp("documentCtime"));

        ret.setLabelhidden(false);
        ret.setBackupPerson(false);

        int dockindId = rs.getInt("dockindId");
        if(!rs.wasNull()){
            BaseSnapshotProvider.setDockindFields(ret.getDocumentId(), dockindId, ret);
        }

        return ret;
    }

    private void deleteForOneDoc(Session session, long id) throws EdmException {
        LOG.trace("deleting tasks for documentId = {}", id);
        Transaction tx = session.beginTransaction();
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(DELETE_ONE_DOC_QUERY);
            ps.setLong(1, id);
            ps.execute();
            tx.commit();
        } catch (EdmException ex) {
            tx.rollback();
            throw ex;
        } catch (SQLException ex) {
            tx.rollback();
            throw new EdmException(ex);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }

    private void createForOneUser(Session session, String username) throws EdmException {
        LOG.trace("creating tasks for username = {}", username);
        List<String> userGuids = JdbcUtils.selectStrings(userGuidsQuery, DSUser.findByUsername(username).getId());
        List<String> activitiGuids = JdbcUtils.selectStrings(allAssignedGroupsQuery);
        Set<String> guidsToCheck = Sets.newHashSet(userGuids);
        guidsToCheck.retainAll(activitiGuids);
        Collection<TaskHistoryInfo> groupTasks = activitiWorkflowEngine.getTasksWithHistoryAssignedToGroups(guidsToCheck);
        Collection<TaskHistoryInfo> userTasks = activitiWorkflowEngine.getTasksWithHistoryAssignedToUser(username);
     
        //u�ytkownik, mo�e mie� jednocze�nie zadanie z grupy i ze swojego loginu - trzeba usun�� duplikaty
        Set<Equivalence.Wrapper<TaskHistoryInfo>> tasks = Sets.newHashSetWithExpectedSize(groupTasks.size() + userTasks.size());
        for(TaskHistoryInfo thi: groupTasks){
            tasks.add(ID_EQUIVALENCE.wrap(thi));
        }
        for(TaskHistoryInfo thi: userTasks){
            tasks.add(ID_EQUIVALENCE.wrap(thi));
        }
        Set<Long> documentIds = Sets.newHashSetWithExpectedSize(tasks.size());
        for(Equivalence.Wrapper<TaskHistoryInfo> task: tasks){
            documentIds.add(ProcessForDocument.documentIdForProcess(task.get().getProcessId(), ProcessEngineType.ACTIVITI));
        }
        //TODO je�li potrzebne b�dzie szybkie od�wie�anie dla u�ytkownika
        //to poni�szy fragment mo�na znacznie przyspieszy� wykonuj�c go na sql'ach dla grup
        //dokument�w
        for(Long documentId : documentIds){
            String docType = Document.find(documentId).getType().getName();
            deleteForOneDoc(session, documentId);
            createForOneDoc(session, documentId, docType);
        }
        LOG.debug("dokumenty = {}", documentIds);
    }

    private void deleteForOneUser(Session session, String username) throws EdmException {
        LOG.trace("deleting tasks for username = {}", username);
        Transaction tx = session.beginTransaction();
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(DELETE_ONE_USER_QUERY);
            ps.setString(1, username);
            ps.execute();
            tx.commit();
        } catch (EdmException ex) {
            tx.rollback();
            throw ex;
        } catch (SQLException ex) {
            tx.rollback();
            throw new EdmException(ex);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }

    @Override
    public boolean isAssignedToUser(String userName, Long documentId) throws EdmException {
        return isAssignedToUserImpl(userName, documentId);
    }

    @Override
    public TaskPositionBean getTaskPosition(String activityKey) {
        return getTaskPositionImpl(activityKey);
    }
}
