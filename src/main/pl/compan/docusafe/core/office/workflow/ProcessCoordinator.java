package pl.compan.docusafe.core.office.workflow;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

/**
 * Definicja (przydzielenie) koordynatora do danych dokument�w wewn�trz danego rodzaju dokumentu.
 * Przydzia�em zajmuje si� logika rodzaj�w dokument�w ({@link pl.compan.docusafe.core.dockinds.logic.DocumentLogic#getProcessCoordinator(pl.compan.docusafe.core.office.OfficeDocument)}).
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class ProcessCoordinator
{
    private Long id;
    private String documentKindCn;    
    private String channelCn;    
    private String guid;
    private String username;

    public ProcessCoordinator(DSUser user) throws EdmException
    {
    	this.username = user.getName();
    	String[]  divs = user.getOrdinaryDivisions();
    	if(divs != null && divs.length > 0)
    		this.guid = divs[0];
    	//else
    		//this.guid = DSDivision.ROOT_GUID;
    	this.channelCn = user.getName();
    }
    
    public ProcessCoordinator() throws EdmException
    {
    }
    
    
    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public synchronized void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public void delete() throws EdmException
    {
         Persister.delete(this);
    }
    
    @SuppressWarnings("unchecked")
    public static List<ProcessCoordinator> findByDocumentKind(String documentKindCn) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(ProcessCoordinator.class);
        
        try
        {
            c.add(org.hibernate.criterion.Expression.eq("documentKindCn", documentKindCn));
            
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca koordynator�w dla danego dockindu i kana�u, je�eli obie przekazane warto�ci s� null
     * zwr�ci wszystkich zapisanych w bazie koordynator�w
     *
     * @param documentKindCn cn dockindu, mo�e by� null
     * @param channelCn cn kana�u, mo�e by� null
     * @return lista koordynator�w, mo�e by� pusta, nie mo�e by� null
     * @throws EdmException
     */
    public static List<ProcessCoordinator> find(String documentKindCn, String channelCn) throws EdmException {
        Criteria c = DSApi.context().session().createCriteria(ProcessCoordinator.class);

        try {
            if (documentKindCn != null){
                c.add(org.hibernate.criterion.Expression.eq("documentKindCn", documentKindCn));
            }
            if (channelCn != null){
                c.add(org.hibernate.criterion.Expression.eq("channelCn", channelCn));
            }

            return c.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
    
    public String getDocumentKindCn()
    {
        return documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn)
    {
        this.documentKindCn = documentKindCn;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }    

    public String getChannelCn()
    {
		return channelCn;
	}

	public void setChannelCn(String channelCn)
	{
		this.channelCn = channelCn;
	}

	public String getGuid()
	{
		return guid;
	}

	public void setGuid(String guid) 
	{
		this.guid = guid;
	}
    
}
