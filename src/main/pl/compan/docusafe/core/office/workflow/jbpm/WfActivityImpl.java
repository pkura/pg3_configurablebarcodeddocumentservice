package pl.compan.docusafe.core.office.workflow.jbpm;

import pl.compan.docusafe.core.office.workflow.xpdl.Activity;
import pl.compan.docusafe.core.office.workflow.WorkflowException;
import pl.compan.docusafe.core.office.workflow.NameValue;
import pl.compan.docusafe.core.office.workflow.WfProcess;
import pl.compan.docusafe.core.office.workflow.WfActivity;
/* User: Administrator, Date: 2006-12-15 15:02:06 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class WfActivityImpl extends WfExecutionObjectImpl implements WfActivity
{
    public WfActivityImpl(String key)
    {
        this.key = key;
    }

    public Activity getActivityDefinition() throws WorkflowException
    {
        return null;
    }

    public String activity_definition_id() throws WorkflowException
    {
        return null;
    }
    public void complete() throws WorkflowException
    {

    }
    public void set_result(NameValue[] result) throws WorkflowException
    {

    }
    public NameValue[] result() throws WorkflowException
    {
        return null;
    }
    public NameValue[] extended_attributes() throws WorkflowException
    {
        return null;
    }
    public NameValue[] process_context() throws WorkflowException
    {
        return null;
    }
    public void set_process_context(NameValue[] context) throws WorkflowException
    {

    }
    public WfProcess container() throws WorkflowException
    {
        return null;
    }
}
