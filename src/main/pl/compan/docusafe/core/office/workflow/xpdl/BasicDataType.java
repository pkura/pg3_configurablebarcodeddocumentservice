package pl.compan.docusafe.core.office.workflow.xpdl;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;

import java.util.Date;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: BasicDataType.java,v 1.3 2006/02/06 16:05:50 lk Exp $
 */
public class BasicDataType extends DataType
{
    public static final String NAME = "BasicType";

    public static final String STRING = "STRING";
    /**
     * Typ FLOAT mapowany jest na klas� java.lang.Double.
     */
    public static final String FLOAT = "FLOAT";
    /**
     * Typ INTEGER mapowany jest na klas� java.lang.Long.
     */
    public static final String INTEGER = "INTEGER";
    public static final String DATETIME = "DATETIME";
    public static final String BOOLEAN = "BOOLEAN";

    // te pola nie musz� by� finalne, dop�ki konstruktor u�ywany
    // do ich tworzenia nie jest publiczny
    public static BasicDataType DT_STRING;
    public static BasicDataType DT_FLOAT;
    public static BasicDataType DT_INTEGER;
    public static BasicDataType DT_DATETIME;
    public static BasicDataType DT_BOOLEAN;

    static
    {
        try
        {
            DT_STRING = new BasicDataType(STRING);
            DT_FLOAT = new BasicDataType(FLOAT);
            DT_INTEGER = new BasicDataType(INTEGER);
            DT_DATETIME = new BasicDataType(DATETIME);
            DT_BOOLEAN = new BasicDataType(BOOLEAN);
        }
        catch (EdmException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Nazwa typu podstawowego (jedna ze sta�ych STRING, FLOAT etc.)
     */
    private String type;

    BasicDataType(Element basicType) throws EdmException
    {
        if (basicType.attribute("Type") == null)
            throw new EdmException("Element DataType nie posiada atrybutu Type.");

        type = basicType.attribute("Type").getValue().toUpperCase();

        if (!STRING.equals(type) && !FLOAT.equals(type) &&
            !INTEGER.equals(type) && !DATETIME.equals(type) &&
            !BOOLEAN.equals(type))
            throw new EdmException("Nieznany typ BasicType: "+type);
    }

    BasicDataType(String type) throws EdmException
    {
        this.type = type;

        if (!STRING.equals(type) && !FLOAT.equals(type) &&
            !INTEGER.equals(type) && !DATETIME.equals(type) &&
            !BOOLEAN.equals(type))
            throw new EdmException("Nieznany typ BasicType: "+type);
    }

    public String getTypeName()
    {
        return NAME;
    }

    public Object getValue(String s)
    {
        if (s == null)
            throw new NullPointerException("s");

        if (STRING.equals(type))
        {
            return s;
        }
        else if (FLOAT.equals(type))
        {
            return Double.valueOf(s);
        }
        else if (INTEGER.equals(type))
        {
            return Long.valueOf(s);
        }
        else if (DATETIME.equals(type))
        {
            return new Date(Long.parseLong(s));
        }
        else if (BOOLEAN.equals(type))
        {
            return Boolean.valueOf(s);
        }
        else
        {
            throw new IllegalArgumentException("Nieznany typ: "+type);
        }
    }

    public Object getDefaultValue()
    {
        if (STRING.equals(type))
        {
            return "";
        }
        else if (FLOAT.equals(type))
        {
            return new Double(0);
        }
        else if (INTEGER.equals(type))
        {
            return new Long(0);
        }
        else if (DATETIME.equals(type))
        {
            return new Date(0);
        }
        else if (BOOLEAN.equals(type))
        {
            return Boolean.FALSE;
        }
        else
        {
            throw new IllegalArgumentException("Nieznany typ: "+type);
        }
    }

    /**
     * Zwraca nazw� typu, jedn� ze sta�ych STRING, BOOLEAN etc.
     */
    public String getType()
    {
        return type;
    }

    public String toString()
    {
        return "<BasicType type="+type+">";
    }
}
