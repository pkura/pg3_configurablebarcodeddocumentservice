package pl.compan.docusafe.core.office.workflow;

import java.sql.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jbpm.JbpmConfiguration;
import org.jbpm.JbpmContext;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.jbpm.ContextNotOpenedException;

public class DSjBPM 
{
	static final Logger log = LoggerFactory.getLogger(DSjBPM.class);
	
	static JbpmConfiguration configuration = null;
	
	//wczesna inicjalizacja
	static{
		initialize();
	}

	private static void initialize() {
		configuration = JbpmConfiguration.parseResource("jbpm_test.cfg.xml");
	}

	/**
	 * Zwraca kontekst je�li zosta� otwarty - je�li nie wyrzuca wyj�tek
	 */
	public static JbpmContext context() throws EdmException, ContextNotOpenedException {
		JbpmContext currCtx = configuration.getCurrentJbpmContext();

		if (currCtx != null) {
			return currCtx;
		}

		throw new ContextNotOpenedException("Kontekst proces�w biznesowych nie zosta� otwarty");
	}

	/**
	 * Otwiera nowy kontekst jbpm'a
	 * @return
	 */
	public static JbpmContext open() throws EdmException {
		JbpmContext currCtx = configuration.getCurrentJbpmContext();

		if (currCtx != null) {
			throw new EdmException("Z tym w�tkiem jest ju� powi�zany kontekst proces�w biznesowych");
		}

		JbpmContext jbpmContext = configuration.createJbpmContext();
		//zrobione by wkuty - musimy wziac polaczenie nie z puli tylko z DSAPi
		//wczesniej musi byc otwarta sesja
		//jbpmContext.setConnection(Docusafe.getDataSource().getConnection());
		Connection con = DSApi.context().session().connection();
		try {
			con.setAutoCommit(false);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		jbpmContext.setConnection(con);
		
		log.trace("-------------------DSjBPM : open");
		return jbpmContext;
	}
	
	/** zwraca wierzcholek stosu kontekstow jbpm, a w przypadku gdy jest pusty tworzy nowy */
	@Deprecated
	public static JbpmContext getContext() throws EdmException {
		try {
			JbpmContext currCtx = configuration.getCurrentJbpmContext();
			
			if (currCtx != null) {
				return currCtx;
			}

			JbpmContext jbpmContext = configuration.createJbpmContext();
			//zrobione by wkuty - musimy wziac polaczenie nie z puli tylko z DSAPi
			//wczesniej musi byc otwarta sesja
			//jbpmContext.setConnection(Docusafe.getDataSource().getConnection());
			jbpmContext.setConnection(DSApi.context().session().connection());
			return jbpmContext;
		} catch (Exception e) {
			throw new EdmException("Nie mo�na uzyska� po��czenia z baz� danych");
		}
	}
	
	/**
	 * Zamykanie kontekstu bez rzucania wyjatku
	 * @param context
	 */
	public static void closeContextQuietly(JbpmContext context) {
		try {
			closeContext(context);
			log.trace("---------------------DSjBPM: closeContextQuietly(context)");
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
	}

	/**
	 * Zamykanie kontekstu bez rzucania wyjatku
	 */
	public static void closeContextQuietly() {
		try {
			closeContext(context());
			log.trace("---------------------DSjBPM: closeContextQuietly()");
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
	}

	/** zamyka kontekst dodatkowo zwalniajac polaczenie do bazy nawiazane na jego rzecz */
	public static void closeContext(JbpmContext context) throws EdmException {
		if (context == null) {
			return;
		}
		context.close();
		try {
			//nie zamykamy bo nie otwieramy polaczenie zamkniecie nastapi razem z zamknieciem DSAPi
			//context.getConnection().close();
		} catch (Exception e) {
			throw new EdmException("problem while closing connection " + e.getMessage());
		}
	}
}
