package pl.compan.docusafe.core.office.workflow;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NameValue.java,v 1.3 2008/07/23 12:44:02 mariuszk Exp $
 */
public interface NameValue
{
    String name();
    Integer valueInt();
    void setValueInt(Integer valueInt);
    void setValueInt(Long valueInt);
    AnyObject value() throws EdmException;
}
