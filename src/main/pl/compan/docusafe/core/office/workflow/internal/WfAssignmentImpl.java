package pl.compan.docusafe.core.office.workflow.internal;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.CallbackException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.office.workflow.NameValue;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.WfAssignment;
import pl.compan.docusafe.core.office.workflow.WfResource;
import pl.compan.docusafe.core.office.workflow.WorkflowException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfAssignmentImpl.java,v 1.12 2010/05/17 10:17:47 pecet1 Exp $
 */
public class WfAssignmentImpl implements WfAssignment, Lifecycle
{
    private static final Logger log = LoggerFactory.getLogger(WfAssignmentImpl.class);

    private Long id;
    private WfActivityImpl activity;
    private WfResourceImpl resource;
    private int resourceCount;
    private boolean accepted;

    public WfActivity activity() throws WorkflowException
    {
        return activity;
    }

    public boolean get_accepted_status() throws WorkflowException
    {
        return accepted;
    }

    public void set_accepted_status(boolean accepted) throws WorkflowException
    {
        if (this.accepted == accepted)
            return;

        this.accepted = accepted;

        if (accepted)
        {
        	this.resourceCount = 1;
            activity.getProcess().accept(this);
        }
        else
        {
            activity.getProcess().unaccept(this);
        }
        
    }

    public WfResource assignee() throws WorkflowException
    {
        return resource;
    }

    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }

    WfActivityImpl getActivity()
    {
        return activity;
    }

    void setActivity(WfActivityImpl activity)
    {
        this.activity = activity;
    }

    WfResourceImpl getResource()
    {
        return resource;
    }

    void setResource(WfResourceImpl resource)
    {
        this.resource = resource;
    }

    private boolean isAccepted()
    {
        return accepted;
    }

    /**
     * Metoda nieużywana, obecna tylko ze względu na wymagania Hibernate.
     * @see #set_accepted_status(boolean)
     */
    private void setAccepted(boolean accepted)
    {
        this.accepted = accepted;
    }

    public String toString()
    {
        return getClass().getName()+"[id="+id+" accepted="+accepted+
            " activity="+activity+" resource="+resource+"]";
    }

    /* Lifecycle */

    /**
     * W momencie utworzenia obiektu w bazie danych wywołuje funkcję
     * Document.changelog() z komunikatem DocumentChangelog.WF_ASSIGNED.
     * Dla użytkownika $bok znajduje mapowania na konkretnych użytkowników.
     */
    public boolean onSave(Session s) throws CallbackException
    {
        try
        {
            if (activity != null && activity.container() != null &&
                resource != null)
            {
                NameValue[] ctx = ((WfProcessImpl) activity.container()).process_context();
                if (ctx != null)
                {
                    for (int i=0; i < ctx.length; i++)
                    {
                        NameValue nv = ctx[i];
                        if ("ds_document_id".equals(nv.name()) &&
                            nv.value() != null && nv.value().value() != null &&
                            nv.value().value() instanceof Long)
                        {
                            Document.changelog((Long) nv.value().value(),
                                resource.getKey(),
                                DocumentChangelog.WF_ASSIGNED);

                            Document.changelog((Long) nv.value().value(),
                                DSApi.context().getPrincipalName(),
                                DocumentChangelog.WF_PUSH);
                        }
                    }
                }
            }
        }
        catch (EdmException e)
        {
            throw new CallbackException(e.getMessage(), e);
        }

        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }

    public int getResourceCount() {
        return resourceCount;
    }

    public void setResourceCount(int resourceCount) {
        this.resourceCount = resourceCount;
    }
}
