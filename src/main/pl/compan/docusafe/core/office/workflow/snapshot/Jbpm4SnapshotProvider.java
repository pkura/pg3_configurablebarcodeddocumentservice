package pl.compan.docusafe.core.office.workflow.snapshot;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.jackrabbit.JackrabbitPrivileges;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.*;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.logic.AttachmentsManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.DwrDocumentMainTabAction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @TODO - troche malo obiektowo rozwiazano temat wspolnych operacji ze
 *       SnapshotUtils - trzeba to zrobic elegancko przez Dziedziczenie
 * 
 *       UWAGA - Snapshot ma jedna duza wade - zle zbudowane iteratory dla
 *       uzytkownika i calosci - iteruje po calej bazie. Powinien iterowac tylko
 *       po aktywnych procesach. Zlaczenie wyniku z takim pytaniem powinno dac
 *       pozytywny rezultat:
 * 
 */
public final class Jbpm4SnapshotProvider extends BaseSnapshotProvider
{
    public static final java.util.HashSet<String> TASKLIST_JBPM_VARIABLES = Sets.newHashSet(
            Jbpm4WorkflowFactory.REASSIGNMENT_TIME_VAR_NAME,
            Jbpm4WorkflowFactory.DEADLINE_TIME,
            Jbpm4WorkflowFactory.REMINDER_DATE,
            Jbpm4WorkflowFactory.DOCDESCRIPTION,
            Jbpm4WorkflowFactory.DOCKIND_NUMER_DOKUMENTU,
            Jbpm4WorkflowFactory.DOCKIND_STATUS,
            Jbpm4WorkflowFactory.PROCESS_NAME,
            Jbpm4WorkflowFactory.OBJECTIVE,
            Jbpm4WorkflowFactory.CLERK,
            "currentAssignee",
            "previousAssignee",
            Jbpm4WorkflowFactory.DS_DIVISIIN
    );
    private final static Logger log = LoggerFactory.getLogger(Jbpm4SnapshotProvider.class);
	private final static Jbpm4SnapshotProvider INSTANCE = new Jbpm4SnapshotProvider();

    static final String TABLE = "dsw_jbpm_tasklist";
    static final String RES_COLUMN = "assigned_resource";
    static final String DOCUMENT_ID_COLUMN = "document_id";
    /** warto�� kolumny process_name s�u��ca do identyfikacji r�nych SnapshotProvider�w */
    public final static String PROCESS_KEY = "jbpm4";

    protected final String getTable(){ return TABLE; }
	protected final String getReskey(){ return RES_COLUMN; }
	protected final String getDocumentIdKey() { return DOCUMENT_ID_COLUMN; }

    /** Dodatkowe kryterium w przypadku gdy istnieje kilka Provider�w */
    private static final String MULTIPLE_PROVIDERS_CRITERIA
            = AvailabilityManager.isAvailable("tasklist.multipleProviders")
            ? " and process_name = '" + PROCESS_KEY + "'"
            : "";

    static final String DELETE_ONE_DOC_QUERY = "delete from "+TABLE+" where "+DOCUMENT_ID_COLUMN+ "=?" + MULTIPLE_PROVIDERS_CRITERIA;
    static final String DELETE_ONE_DOC_DOCTYPE_QUERY = "delete from "+TABLE+" where "+DOCUMENT_ID_COLUMN+ "=? and document_type=?" + MULTIPLE_PROVIDERS_CRITERIA;
    static final String DELETE_ONE_USER_QUERY = "delete from "+TABLE+" where "+RES_COLUMN+" =?" + MULTIPLE_PROVIDERS_CRITERIA;

    static void deleteFromSnapshotImpl(Session session, long id, int updateType, String doctype, String username)
            throws SQLException, HibernateException, EdmException {

        PreparedStatement erase = null;
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            log.trace("Otworzylem transakcje " + tx );

            switch(updateType) {
                case TaskSnapshot.UPDATE_TYPE_ONE_DOC : {
                    if(doctype.equals("anyType")) {
                        erase = DSApi.context().prepareStatement(DELETE_ONE_DOC_QUERY);
                        erase.setLong(1, id);
                        if (log.isTraceEnabled()) {
                            log.trace(DELETE_ONE_DOC_QUERY.replace("\\?","{}"),id);
                        }
                    } else {
                        erase = DSApi.context().prepareStatement(DELETE_ONE_DOC_DOCTYPE_QUERY);
                        erase.setLong(1, id);
                        erase.setString(2,doctype);
                        if (log.isTraceEnabled()){
                            log.trace(DELETE_ONE_DOC_DOCTYPE_QUERY.replace("\\?","{}"),id,doctype);
                        }
                    }
                    break;
                }
                case TaskSnapshot.UPDATE_TYPE_ONE_USER: {
                    if (username != null) {
                        erase = DSApi.context().prepareStatement(DELETE_ONE_USER_QUERY);
                        erase.setString(1, username);
                        if (log.isTraceEnabled()) {
                            log.trace(DELETE_ONE_USER_QUERY.replace("\\?", "{}"), username);
                        }
                    } else {
                        throw new IllegalArgumentException("Brak przekazanego u�ytkownika podczas od�wie�ania tabeli");
                    }
                    break;
                }
            }
            erase.execute();
            tx.commit();
            log.trace("commit transakcji {} wykonany ",tx);
            tx = null;
        } finally {
            DSApi.context().closeStatement(erase);
            if (tx != null){
                try	{
                    log.trace("Wykonuje rollback transackji {}",tx);
                    tx.rollback();
                } catch (Exception eee)  {
                    log.trace("Rollback transakcji " + tx + " niemozliwy",eee);
                }
            }
        }
    }

	@Override
	public void updateTaskList(Session session, Long id, String doctype, int updateType, String username) throws Exception
	{
		Transaction tx = null;
		log.debug("Update id={}, doctype={}", id, doctype);

		PreparedStatement psIn = null;
		PreparedStatement psOut = null;
		ResultSet rsIn = null;
		ResultSet rsOut = null;

		try {
			deleteFromSnapshotImpl(session, id, updateType, doctype, username);
			
			tx = session.beginTransaction();
	
			if (doctype.equals(OutOfficeDocument.TYPE) || doctype.equals(OutOfficeDocument.INTERNAL_TYPE) || doctype.equals("anyType"))
			{
				String sqlOut = prepareOutSQL(username, updateType);
				log.info("SqlOut: {}", sqlOut);
				psOut = DSApi.context().prepareStatement(sqlOut);
				if (updateType == 1)
					psOut.setLong(1, id);
				rsOut = psOut.executeQuery();
				
				while (rsOut.next())
					handleOut(session, rsOut.getLong(1));
				
				rsOut.close();
				DSApi.context().closeStatement(psOut);
				psOut = null;
			}

			if (doctype.equals(InOfficeDocument.TYPE) || doctype.equals("anyType"))
			{
				String sqlIn = prepareInSQL(username, updateType);
				log.info("SqlIn: {}", sqlIn);
				psIn = DSApi.context().prepareStatement(sqlIn);
				if (updateType == 1)
					psIn.setLong(1, id);
				rsIn = psIn.executeQuery();
				while (rsIn.next())
					handleIn(session, rsIn.getLong(1),tx);
				
				rsIn.close();
				DSApi.context().closeStatement(psIn);
				psIn = null;
			}

			tx.commit();
			log.debug("commit transakcji " + tx + " wykonany ");
			tx = null;

		}
		finally
		{
			DSApi.context().closeStatement(psIn);
			DSApi.context().closeStatement(psOut);
			if (tx != null)
			{
				try
				{
					log.debug("Wykonuje rollback transackji " + tx);
					tx.rollback();
				}
				catch (Exception eee)
				{
					log.debug("Rollback transakcji " + tx + " niemozliwy", eee);
				}
			}
		}
	}

	public static void handleOut(Session session, long id) throws EdmException
	{
		log.info("handleOut id = " + id);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String sql = documentOutTaskDataSql(id);
			log.info("sql = " + sql);
			ps = DSApi.context().prepareStatement(sql);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				// throw new RuntimeException("document " + id +
				// " cannot be found");
				return; // dokument innego typu
			}

			Iterable<String> taskIds = Jbpm4ProcessLocator.taskIds(id);
			log.info("taskIds.iterator().hasNext() = " + taskIds.iterator().hasNext());
			if (!taskIds.iterator().hasNext() && AvailabilityManager.isAvailable("unwatch.onEndProcess")) {
				DSApi.context().unwatch(id);
			}
			
			Map<Integer, String> stupidCacheMap = new HashMap<Integer, String>();

			
			for (String taskId : taskIds)
			{
				Task task = Jbpm4Provider.getInstance().getTaskService().getTask(taskId);
				DecretationTarget decretationTarget = new DecretationTarget();
				
				if (task.getAssignee() != null)
				{
					// ma przypisan� konkretn� osob�, kt�ra zaakceptowa�a
					// zadanie
					// w takim przypadku ignorowani s� kandydaci nawet
					// je�li istniej�
					DSUser assigneeUsr  = UserImpl.findByUsername(task.getAssignee());
					log.info("task assignee " + task.getAssignee());
					JBPMTaskSnapshot snapshot = new JBPMTaskSnapshot();
					snapshot.setAssigned_user(task.getAssignee());
					snapshot.setDsw_resource_res_key(task.getAssignee());
					snapshot.setDecretationTo(assigneeUsr.asLastnameFirstname());
					snapshot.setDecretation_to_div(cutString(DSDivision.find(Iterables.getFirst(Lists.newArrayList(assigneeUsr.getDivisionsGuidWithoutGroup()), DSDivision.ROOT_GUID)).getName(), 100, "setDecretation_to_div"));
					snapshot.setAccepted(true);
                    snapshot.setSameProcessSnapshotsCount(1);
					fillSnaphotFromTask(snapshot, task);
					INSTANCE.setTaskByOutResultSet(snapshot, rs, stupidCacheMap);
					
					long docid = rs.getLong(1);
					Document doc = Document.find(docid,false);
					snapshot.setTaskInPackage(doc.getInPackage());
					snapshot.setDockindId(doc.getDocumentKind().getId());
					String newProcessStatus = snapshot.getDockindStatus();
					
					try
					{
						newProcessStatus = doc.getFieldsManager().getField("STATUS").getEnumItemByCn(task.getActivityName()).getTitle();
					}
					catch (Exception e)
					{
						log.info("B�ad podczas pobrania warto�ci status procesu dla dokumentu o id {} na etapie procesu {}. Status procesu ustawiany na status dokumentu", docid, task.getActivityName());
					}
					snapshot.setNewProcessStatus(newProcessStatus);
					
					session.save(snapshot);

                    try {
                        if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                            JackrabbitPrivileges.instance().addReadPrivilege(id, task.getAssignee(), null, ObjectPermission.SOURCE_TASKLIST);
                        }
                    } catch (Exception e) { throw new EdmException(e); }
				}
				else
				{ // wiele kandydat�w do zadania
					Collection<Participation> participations = Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(task.getId());
					if (participations.isEmpty())
					{
						log.error("no participants for task : {}, putting task on admin tasklist", task.getId());
						JBPMTaskSnapshot snapshot = new JBPMTaskSnapshot();
						snapshot.setDsw_resource_res_key("admin");
						snapshot.setAccepted(false);
                        snapshot.setSameProcessSnapshotsCount(1);
						fillSnaphotFromTask(snapshot, task);
						INSTANCE.setTaskByOutResultSet(snapshot, rs, stupidCacheMap);
						snapshot.setObjective("NO PARTICIPANTS ERROR");
						long docid = rs.getLong(1);
						Document doc = Document.find(docid,false);
						snapshot.setTaskInPackage(doc.getInPackage());
						snapshot.setDockindId(doc.getDocumentKind().getId());
						String newProcessStatus = snapshot.getDockindStatus();
						try
						{
							newProcessStatus = doc.getFieldsManager().getField("STATUS").getEnumItemByCn(task.getActivityName()).getTitle();
						}
						catch (Exception e)
						{
							log.info("B�ad podczas pobrania warto�ci status procesu dla dokumentu o id {} na etapie procesu {}. Status procesu ustawiany na status dokumentu", docid, task.getActivityName());
						}
						snapshot.setNewProcessStatus(newProcessStatus);
						session.save(snapshot);
						log.info("task " + taskId + "NO PARTICIPANTS !!!  resources 'admin'");
					}
					else
					{
						Map<String,String> resourceNames;
						resourceNames = processParticipations(participations, decretationTarget);
						for (String key : resourceNames.keySet())
						{
							JBPMTaskSnapshot snapshot = new JBPMTaskSnapshot();
							snapshot.setDsw_resource_res_key(key);
							if(decretationTarget.getIsDivisionTarger()) {
								snapshot.setDecretationTo(decretationTarget.getDsDivision().getName());
							} else {
								snapshot.setDecretationTo(decretationTarget.getDsUser().asLastnameFirstname());
							}
							snapshot.setDivisionGuid(new String(resourceNames.get(key).length() > 62 ? resourceNames.get(key).substring(0, 61) : resourceNames.get(key)));
							snapshot.setAccepted(false);
                            snapshot.setSameProcessSnapshotsCount(resourceNames.size());
							fillSnaphotFromTask(snapshot, task);
							INSTANCE.setTaskByOutResultSet(snapshot, rs, stupidCacheMap);
							long docid = rs.getLong(1);
							Document doc = Document.find(docid,false);
							snapshot.setTaskInPackage(doc.getInPackage());
							snapshot.setDockindId(doc.getDocumentKind().getId());
							String newProcessStatus = snapshot.getDockindStatus();
							try
							{
								newProcessStatus = doc.getFieldsManager().getField("STATUS").getEnumItemByCn(task.getActivityName()).getTitle();
							}
							catch (Exception e)
							{
								log.info("B�ad podczas pobrania warto�ci status procesu dla dokumentu o id {} na etapie procesu {}. Status procesu ustawiany na status dokumentu", docid, task.getActivityName());
							}
							snapshot.setNewProcessStatus(newProcessStatus);
							session.save(snapshot);

                            try {
                                if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                                    JackrabbitPrivileges.instance().addReadPrivilege(id, key, null, ObjectPermission.SOURCE_TASKLIST);
                                }
                            } catch (Exception e) { throw new EdmException(e); }
						}
						log.info("task " + taskId + " resources " + resourceNames);
					}
				}
			}
			log.info("tasks.id = " + taskIds);
		}
		catch (SQLException e)
		{
			throw new EdmSQLException(e);
		} catch(Exception ea) {
			log.error(ea.toString());
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
		}
	}

	private static void fillSnaphotAnswerCounter(Long inDocumentId, JBPMTaskSnapshot taskSnapshot) {
		
		boolean contextOpened = false;
		Criteria c;
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			c = DSApi.context().session().createCriteria(OutOfficeDocument.class);
	        c.add(Restrictions.eq("inDocumentId", inDocumentId));
	        c.add(Restrictions.eq("cancelled", false));
	        int amountOfReplies = c.list().size();
	        
	        Integer answerCounter = new Integer(amountOfReplies);
	        taskSnapshot.setAnswerCounter(answerCounter);	
			
		} catch (EdmException e) {
			log.error("exception connected with context hibernate {}", e);
		} finally {
			DSApi.closeContextIfNeeded(contextOpened);
			log.info("close context for " + TaskSnapshot.class
					+ " used on " + Jbpm4SnapshotProvider.class);
		}

	}
	public static void handleIn(Session session, long id, Transaction tx) throws EdmException
	{
		log.info("handleIn id = " + id);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String sql = documentInTaskDataSql(id);
			log.info("sql = " + sql);
			ps = DSApi.context().prepareStatement(sql);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				// throw new RuntimeException("document " + id +
				// " cannot be found");
				return; // dokument innego typu
			}

			Iterable<String> taskIds = Jbpm4ProcessLocator.taskIds(id);
			if (!taskIds.iterator().hasNext() && AvailabilityManager.isAvailable("unwatch.onEndProcess")) {
				DSApi.context().unwatch(id);
			}
			Map<Integer, String> stupidCacheMap = new HashMap<Integer, String>();
			for (String taskId : taskIds)
			{
				log.info("TASKID: {}", taskId);
				Task task = Jbpm4Provider.getInstance().getTaskService().getTask(taskId);
				DecretationTarget decretationTarget = new DecretationTarget();
				
				if (task.getAssignee() != null)
				{ // ma przypisan� konkretn� osob�
					DSUser assigneeUsr  = UserImpl.findByUsername(task.getAssignee());
					log.info("task assignee " + task.getAssignee());
					JBPMTaskSnapshot snapshot = new JBPMTaskSnapshot();
					snapshot.setDsw_resource_res_key(task.getAssignee());
					snapshot.setDecretationTo(assigneeUsr.asLastnameFirstname());
					snapshot.setDecretation_to_div(cutString(DSDivision.find(Iterables.getFirst(Lists.newArrayList(assigneeUsr.getDivisionsGuidWithoutGroup()), DSDivision.ROOT_GUID)).getName(), 100, "setDecretation_to_div"));
					fillSnaphotFromTask(snapshot, task);
					INSTANCE.setTaskByInResultSet(snapshot, rs, stupidCacheMap);
					snapshot.setAssigned_user(task.getAssignee());
					snapshot.setAccepted(true);
					
					fillSnaphotAnswerCounter(id,snapshot);
					
                    snapshot.setSameProcessSnapshotsCount(1);
					long docid = rs.getLong(1);
					Document doc = Document.find(docid,false);
					snapshot.setTaskInPackage(doc.getInPackage());
					snapshot.setDockindId(doc.getDocumentKind().getId());
					String newProcessStatus = snapshot.getDockindStatus();
					
					try
					{
						newProcessStatus =doc.getFieldsManager().getField("STATUS").getEnumItemByCn(task.getActivityName()).getTitle();
					}
					catch (Exception e)
					{
						log.info("B�ad podczas pobrania warto�ci status procesu dla dokumentu o id {} na etapie procesu {}. Status procesu ustawiany na status dokumentu", docid, task.getActivityName());
					}
					snapshot.setNewProcessStatus(newProcessStatus);
					session.save(snapshot);

                    try {
                        if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                            JackrabbitPrivileges.instance().addReadPrivilege(id, task.getAssignee(), null, ObjectPermission.SOURCE_TASKLIST);
                        }
                    } catch (Exception e) { throw new EdmException(e); }
				}
				else
				{ // wiele kandydat�w do zadania
					Collection<Participation> participations = Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(task.getId());
					if (participations.isEmpty()) //jaki� b��d zadanie nie ma nikogo przypisanego do zadania
					{
						log.error("no participants for task : {}, putting task on admin tasklist", task.getId());
						JBPMTaskSnapshot snapshot = new JBPMTaskSnapshot();
						snapshot.setDsw_resource_res_key("admin");
						fillSnaphotFromTask(snapshot, task);
						INSTANCE.setTaskByInResultSet(snapshot, rs, stupidCacheMap);
						snapshot.setAccepted(false);
						fillSnaphotAnswerCounter(id,snapshot);
                        snapshot.setSameProcessSnapshotsCount(1);
						long docid = rs.getLong(1);
						String newProcessStatus = snapshot.getDockindStatus();
						Document doc = Document.find(docid,false);
						snapshot.setTaskInPackage(doc.getInPackage());
						snapshot.setDockindId(doc.getDocumentKind().getId());
						try
						{
							newProcessStatus = doc.getFieldsManager().getField("STATUS").getEnumItemByCn(task.getActivityName()).getTitle();
						}
						catch (Exception e)
						{
							log.info("B�ad podczas pobrania warto�ci status procesu dla dokumentu o id {} na etapie procesu {}. Status procesu ustawiany na status dokumentu", docid, task.getActivityName());
						}
						snapshot.setNewProcessStatus(newProcessStatus);
						session.save(snapshot);
					}
					else
					{
						Map<String,String> resourceNames;
						resourceNames = processParticipations(participations, decretationTarget);
						for (String key : resourceNames.keySet())
						{
							DSUser assigneeUsr = UserImpl.findByUsername(key);
							JBPMTaskSnapshot snapshot = new JBPMTaskSnapshot();
							snapshot.setDsw_resource_res_key(key);
							if(decretationTarget.getIsDivisionTarger()) {
								snapshot.setDecretationTo(decretationTarget.getDsDivision().getName());
							} else {
								snapshot.setDecretationTo(decretationTarget.getDsUser().asLastnameFirstname());
							}
							snapshot.setDecretation_to_div(cutString(DSDivision.find(Iterables.getFirst(Lists.newArrayList(assigneeUsr.getDivisionsGuidWithoutGroup()), DSDivision.ROOT_GUID)).getName(), 100, "setDecretation_to_div"));
							snapshot.setDivisionGuid(new String(resourceNames.get(key).length() > 62 ? resourceNames.get(key).substring(0, 61) : resourceNames.get(key)));
							fillSnaphotFromTask(snapshot, task);
							INSTANCE.setTaskByInResultSet(snapshot, rs, stupidCacheMap);
							snapshot.setAccepted(false);
							fillSnaphotAnswerCounter(id,snapshot);
                            snapshot.setSameProcessSnapshotsCount(resourceNames.size());
							long docid = rs.getLong(1);
							String newProcessStatus = snapshot.getDockindStatus();
							Document doc = Document.find(docid,false);
							snapshot.setTaskInPackage(doc.getInPackage());
							snapshot.setDockindId(doc.getDocumentKind().getId());
							try
							{
								newProcessStatus = doc.getFieldsManager().getField("STATUS").getEnumItemByCn(task.getActivityName()).getTitle();
							}
							catch (Exception e)
							{
								log.info("B�ad podczas pobrania warto�ci status procesu dla dokumentu o id {} na etapie procesu {}. Status procesu ustawiany na status dokumentu", docid, task.getActivityName());
							}
							snapshot.setNewProcessStatus(newProcessStatus);
							session.save(snapshot);

                            try {
                                if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                                    JackrabbitPrivileges.instance().addReadPrivilege(id, key, null, ObjectPermission.SOURCE_TASKLIST);
                                }
                            } catch (Exception e) { throw new EdmException(e); }
						}
						log.info("task " + taskId + " resources " + resourceNames);
					}
				}
			}
			log.info("tasks.id = " + taskIds);
		}
		catch (SQLException e)
		{
			throw new EdmSQLException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
		}
	}

	/**
	 * Iteruje po Participation i wyci�ga wszystkie zasoby do kt�rych nale�y
	 * przypisa� zadanie
	 * 
	 * @param participations
	 * @return
	 */
	private static Map<String,String> processParticipations(Collection<Participation> participations, DecretationTarget decretationTarget) throws EdmException
	{
		Preconditions.checkArgument(!participations.isEmpty(), "participations cannot be empty");
		HashMap<String,String> ret = new HashMap<String,String>();
		for (Participation participation : participations)
		{
			if (participation.getType().equals(Participation.CANDIDATE))
			{
				String groupId = participation.getGroupId();

				if(groupId != null && participation.getUserId() != null){
					decretationTarget.setUserName(participation.getUserId());
					decretationTarget.setDivisionGuid(groupId);
					decretationTarget.setIsDivisionTarger(false);
					ret.put(participation.getUserId(), groupId);
				}
				// participation.getUserId() zwraca username ...
				else if (groupId == null && participation.getUserId() != null ){
                    DSDivision division = DSDivision.find(Iterables.getFirst(Lists.newArrayList(DSUser.findByUsername(participation.getUserId()).getDivisionsGuidWithoutGroup()), DSDivision.ROOT_GUID));
					decretationTarget.setUserName(participation.getUserId());
					decretationTarget.setDivisionGuid(division.getGuid());
					decretationTarget.setIsDivisionTarger(false);
					ret.put(participation.getUserId(), division.getGuid());
					//ret.add(participation.getUserId());
				}
				else if (groupId != null){
					// TODO doda� obs�ug� zast�pstw, chyba nie bo zastepstwa dzialaja na zasadzie wyswietlkania zadan osoby zastepowanej. 
//					permissiony powinien sprawdzic razem z zastepstwem
					decretationTarget.setDivisionGuid(groupId);
					decretationTarget.setIsDivisionTarger(true);
					boolean addUser = false;
					for (DSUser user : DSDivision.find(groupId).getUsers())
					{
						
						if(DSApi.context().hasPermission(user, DSPermission.WWF_ODBIOR_DEKRETACJI_NA_DZIAL, groupId))
						{
							ret.put(user.getName(), groupId);
							addUser = true;
						}
					}
					if(!addUser)
						for (DSUser user : DSDivision.find(groupId).getUsers())
						{
								ret.put(user.getName(), groupId);
						}
				}
			}
			else{
				log.warn("unhandled participation type: " + participation.getType());
			}
		}
		log.info(" == processParticipations == result {}", ret);
		if(ret.size() < 1)
			throw new EdmException("Nie okre�lono u�ytkownika do dekretacji zadania");
		return ret;
	}

	/**
	 * Wype�nia JBPMTaskSnapshot danymi z Task (za wyj�tkiem danych zwi�zanych
	 * z osob� do kt�rej nale�y ten task)
	 * 
	 * @param ts
	 * @param task
	 */
	private static void fillSnaphotFromTask(JBPMTaskSnapshot ts, Task task)
	{
		ts.setActivityKey(task.getId());
		ts.setProcess(PROCESS_KEY);
		ts.setDescription(task.getDescription());

        ExecutionService service = Jbpm4Provider.getInstance().getExecutionService();
        Map<String, Object> variables = service.getVariables(task.getExecutionId(), TASKLIST_JBPM_VARIABLES);
        Date incoming = (Date) variables.get(Jbpm4WorkflowFactory.REASSIGNMENT_TIME_VAR_NAME);
        Date deadline_time = (Date) variables.get(Jbpm4WorkflowFactory.DEADLINE_TIME);
        Date reminder_date = (Date) variables.get(Jbpm4WorkflowFactory.REMINDER_DATE);
        String description = (String) variables.get(Jbpm4WorkflowFactory.DOCDESCRIPTION);
        String dockind_numer_dokumentu = (String) variables.get(Jbpm4WorkflowFactory.DOCKIND_NUMER_DOKUMENTU);
        String dockind_status = (String) variables.get(Jbpm4WorkflowFactory.DOCKIND_STATUS);
        String process_name = (String) variables.get(Jbpm4WorkflowFactory.PROCESS_NAME);
        String objective = (String) variables.get(Jbpm4WorkflowFactory.OBJECTIVE);
        // Ustawiamy deadline_time

        if(objective != null)
        {
            ts.setObjective(objective);
        }

		if (deadline_time != null)
		{
			ts.setDeadlineTime(deadline_time);
		}
		
        if (reminder_date != null)
        {
            ts.setReminderDate(reminder_date);
        }

		// ustawiamy opis
		if (description != null && !description.equals(""))
		{
			ts.setDescription(description);
		}

		// numer dokumentu
		if (dockind_numer_dokumentu != null && !dockind_numer_dokumentu.equals(""))
		{
			ts.setDockindNumerDokumentu(dockind_numer_dokumentu);
		}
		// status dokumentu
		if (dockind_status != null && !dockind_status.equals(""))
		{
			ts.setDockindStatus(dockind_status);
		}

		if (incoming == null)
		{
//			ts.setIncomingDate(task.getCreateTime());
//			ts.setReceiveDate(task.getCreateTime()); // to chyba to jest dat�
												// otrzymania
		}
		else
		{
			ts.setIncomingDate(incoming);
			ts.setReceiveDate(incoming); // to chyba to jest dat� otrzymania
		}
		// log.info("execution id = " + task.getExecutionId());
        String sender = (String) variables.get(  "currentAssignee");
		// log.info("currentAssignee ("+task.getExecutionId()+")=== " +
		// sender);
		// je�eli spe�nione -> akceptowanie tasku (we� ju� star� warto��)
		if (ts.getDsw_resource_res_key().equals(sender))
		{ // maksymalnie krzywe - czyli idealnie pasuje do reszty workflow
            sender = (String) variables.get("previousAssignee");
			// log.info("previousAssignee ("+task.getExecutionId()+") === " +
			// sender);
		}
		// log.info("SENDER === " + sender);
		// ts.setObjective(sender);
		ts.setActivity_lastActivityUser(sender); // tak to on!!! inaczej
		//mo�liwe �e wczesniej zostalo ustawione z participant�w 

		if(StringUtils.isEmpty(ts.getDivisionGuid()))
		{
            String division = (String) variables.get(Jbpm4WorkflowFactory.DS_DIVISIIN);
			if(StringUtils.isEmpty(division))
				division = DSDivision.ROOT_GUID;
			ts.setDivisionGuid(division);
		}
											// sender
		// ts.setSender(sender);
		// ts.setDocumentSender(sender);
		// Jbpm4Provider.getInstance().getTaskService
	}

	private static String documentOutTaskDataSql(long id)
	{
		String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
		return

		"select distinct "
				+ // 1
				"doc.id, " + "doc.officenumber, " + "doc.officenumberyear, \n"
				+ "dso_person.firstname,\n"
				+ // 5
				"dso_person.lastname,\n" + "dso_person.organization,\n" + "dso_person.street,\n" + "dso_person.location,\n"
				+ "doc.summary,\n"
				+ // 10
				"doc.internaldocument,\n" + "dsdoc.source,\n" + "rcpt.firstname,\n" + "rcpt.lastname,\n"
				+ "rcpt.organization,\n"
				+ // 15
				"rcpt.street,\n" + "rcpt.location,\n" + "dso_container.id," + "dso_container.officeid, "
				+ "doc.creatingUser,"
				+ // 20
				"doc.prepstate," + "deliv.name, " + "doc.clerk, " + "doc.currentassignmentguid as currentassignmentguid, "
				+ "dsdoc.ctime, "
				+ // 25
				"dsdoc.dockind_id, " + "dsdoc.lastRemark, " + "doc.DOCUMENTDATE, " + "doc.referenceid, "
				+ "doc.dailyOfficeNumber "
				+ // 30
				"from\n" + "dso_out_document doc " + "inner join ds_document dsdoc on (doc.id = dsdoc.id and doc.id = " + id + ") " + "left outer join dso_person " + nolock + " on (doc.sender = dso_person.id and dso_person.discriminator='SENDER')\n" + "left outer join dso_person rcpt " + nolock
				+ " on (doc.id = rcpt.document_id and (rcpt.posn = 0 or rcpt.posn IS NULL) and rcpt.discriminator='RECIPIENT')\n" + "left outer join dso_out_document_delivery deliv " + nolock + " on (doc.delivery = deliv.id)\n" + "left outer join dso_container" + nolock
				+ " on doc.case_id = dso_container.id\n";
	}

	private static String documentInTaskDataSql(long id)
	{
		String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
		String sql = "select distinct "
				+ // 1
				"doc.id, " + "doc.officenumber, " + "doc.officenumberyear, \n" + "dso_person.firstname,\n"
				+ // 5
				"dso_person.lastname,\n" + "dso_person.organization,\n" + "dso_person.street,\n" + "dso_person.location,\n" + "doc.summary,\n"
				+ // 10
				"doc.referenceid,\n" + "dsdoc.source,\n" + "dso_container.id," + "dso_container.officeid, " + "doc.creatingUser, "
				+ // 15
				"doc.clerk, " + "0, " + "doc.incomingDate," + "dsdoc.dockind_id," + "dsdoc.ctime, " + "doc.replyunnecessary, " + "dsdoc.lastRemark, " + "doc.DOCUMENTDATE, " + "doc.dailyOfficeNumber, "
				+ // 24
				" doc.currentassignmentguid " 
				+
				"from\n" + "dso_in_document doc " + "inner join ds_document dsdoc on (doc.id = dsdoc.id and doc.id = " + id + ") " + "left outer join dso_person " + nolock + " on (doc.sender = dso_person.id and dso_person.discriminator='SENDER')\n" + "left outer join dso_container" + nolock
				+ " on doc.case_id = dso_container.id\n";
		return sql;
	}

	private PreparedStatement handleOrd(Session session, Long id, int updateType, String username, PreparedStatement psOrd, Map<Integer, String> dockindNames) throws SQLException, EdmException
	{
		JBPMTaskSnapshot task;
		log.debug("ORD");
		String sqlOrd = getOrdSQL(username, updateType);
		psOrd = DSApi.context().prepareStatement(sqlOrd);
		setUpSnapshotStatement(psOrd, updateType, username, id);
		ResultSet rsOrd = psOrd.executeQuery();

		while (rsOrd.next())
		{
			Long docId = rsOrd.getLong(1);
			WorkflowFactory wff = WorkflowFactory.getInstance();
			String[] tasksIds = WorkflowFactory.findDocumentTasks(docId);
			for (int i = 0; i < tasksIds.length; i++)
			{
				tasksIds[i] = wff.keyOfId(tasksIds[i]);
			}

			/* p�tla po procesach dokumenu */
			for (String taskId : tasksIds)
			{
				String[] users = wff.getTaskUsers(taskId);

				/* petla po aktorach procesu */
				for (String user : users)
				{
					task = new JBPMTaskSnapshot();
					setProcessDependentAttributes(task, taskId, user);
					setTaskByOrdResultSet(task, rsOrd, dockindNames);
					session.save(task);
					log.trace("zapisalem user={}, docId={},taskId={}", user, task.getDocumentId(), taskId);
				}
			}
		}

		rsOrd.close();
		DSApi.context().closeStatement(psOrd);
		psOrd = null;
		return psOrd;
	}

    public static final String DOCUMENT_ID_FROM_TASKS_QUERY = "select long_value_ from jbpm4_variable v\n" +
            "join jbpm4_task t on v.execution_=t.procinst_ and v.key_='doc-id'\n" +
            "where t.assignee_=? or t.dbid_ in (\n" +
            "select task_ from JBPM4_PARTICIPATION where userid_=?\n" +
            "or groupid_ in (select GUID from ds_division where id in ( \n" +
            "select DIVISION_ID from DS_USER_TO_DIVISION where USER_ID = ( \n" +
            "select ID from ds_user where  NAME = ?)) and DIVISIONTYPE = 'division' \n" +
            "union \n" +
            "select d1.guid from ds_division d1 where d1.id in ( \n" +
            "select parent_id from ds_division where id in ( \n" +
            "select DIVISION_ID from DS_USER_TO_DIVISION where USER_ID = ( \n" +
            "select ID from ds_user where  NAME = ?)) and DIVISIONTYPE = 'position')))\n" +
            "group by LONG_VALUE_";

    static String ACTIVE_PROCESSES_SUBQUERY_WITH_DBID_ =  getActiveProcessesSubquery("DBID_");
    static String ACTIVE_PROCESSES_SUBQUERY_WITH_PARENT_ =  getActiveProcessesSubquery("PARENT_");

    static String getActiveProcessesSubquery(String dbid)
    {
        return
                new StringBuilder("select distinct LONG_VALUE_ from JBPM4_VARIABLE where EXECUTION_ in ( ")
                        .append("select ")
                        .append(dbid)
                        .append(" from JBPM4_EXECUTION where DBID_ in (")
                        .append("select PARENT_ from JBPM4_EXECUTION where DBID_ in (")
                        .append("select EXECUTION_ from JBPM4_TASK where DBID_ in (")
                        .append("select TASK_ from JBPM4_PARTICIPATION where USERID_ = '?' or GROUPID_ in (")

                        .append("select GUID from ds_division where id in ( ")
                        .append("select DIVISION_ID from DS_USER_TO_DIVISION where USER_ID = ( ")
                        .append("select ID from ds_user where  NAME = '?')) and DIVISIONTYPE = 'division' or DIVISIONTYPE ='group' ")
                        .append("union ")
                        .append("select d1.guid from ds_division d1 where d1.id in ( ")
                        .append("select parent_id from ds_division where id in ( ")
                        .append("select DIVISION_ID from DS_USER_TO_DIVISION where USER_ID = ( ")
                        .append("select ID from ds_user where  NAME = '?')) and DIVISIONTYPE = 'position')) ")

                        .append("or ASSIGNEE_ = '?') and PARENT_ is not null)")
                        .append("union ")
                        .append("select DBID_ from JBPM4_EXECUTION where DBID_ in (")
                        .append("select EXECUTION_ from JBPM4_TASK where DBID_ in (")
                        .append("select TASK_ from JBPM4_PARTICIPATION where USERID_ = '?' or GROUPID_ in (")

                        .append("select GUID from ds_division where id in ( ")
                        .append("select DIVISION_ID from DS_USER_TO_DIVISION where USER_ID = ( ")
                        .append("select ID from ds_user where  NAME = '?')) and DIVISIONTYPE = 'division' or DIVISIONTYPE ='group' ")
                        .append("union ")
                        .append("select d1.guid from ds_division d1 where d1.id in ( ")
                        .append("select parent_id from ds_division where id in ( ")
                        .append("select DIVISION_ID from DS_USER_TO_DIVISION where USER_ID = ( ")
                        .append("select ID from ds_user where  NAME = '?')) and DIVISIONTYPE = 'position')) )")

                        .append(" or ASSIGNEE_ = '?') and PARENT_ is null))")
                        .append("and KEY_ = 'doc-id'").toString();
    }

	@Override
	protected String prepareInSQL(String username, int updateType)
	{
		String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
		boolean countReplies = Configuration.isAdditionOn(Configuration.ADDITION_COUNT_REPLIES_ON_TASKLIST);

		return

                "select distinct "
                        + // 1
                        "doc.id, " + "doc.officenumber, " + "doc.officenumberyear, \n" + "dso_person.firstname,\n"
                        + // 5
                        "dso_person.lastname,\n" + "dso_person.organization,\n" + "dso_person.street,\n" + "dso_person.location,\n" + "doc.summary,\n"
                        + // 10
                        "doc.referenceid,\n" + "dsdoc.source,\n" + "dso_container.id," + "dso_container.officeid, " + "doc.creatingUser, "
                        + // 15
                        "doc.clerk, " + (countReplies ? "(select count(indocument) from dso_out_document where indocument=doc.id), " : "0, ")
                        + // ilosc odpowiedzi na pismo
                        "doc.incomingDate," + "dsdoc.dockind_id," + "dsdoc.ctime, " + "doc.replyunnecessary, " + "dsdoc.lastRemark, " + "doc.DOCUMENTDATE, " + "doc.dailyOfficeNumber, "
                        + // 24
                        " doc.currentassignmentguid "
                        +
                        "from\n" + "dso_in_document doc " + "inner join ds_document dsdoc " + nolock + " on (doc.id = dsdoc.id " + (updateType == 1 ? "and doc.id = ? " : "and doc.id in (" + DOCUMENT_ID_FROM_TASKS_QUERY.replace("?", "'" + username + "'") + ")") + ") \n" + "left outer join dso_person " + nolock
                        + " on (doc.sender = dso_person.id and dso_person.discriminator='SENDER')\n" + "left outer join dso_container" + nolock + " on doc.case_id = dso_container.id\n";
    }

	@Override
	protected String prepareOutSQL(String username, int updateType)
	{
		String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
		// boolean countReplies =
		// Configuration.isAdditionOn(Configuration.ADDITION_COUNT_REPLIES_ON_TASKLIST);

		return

                "select distinct "
                        + // 1
                        "doc.id, " + "doc.officenumber, " + "doc.officenumberyear, \n"
                        + "dso_person.firstname,\n"
                        + // 5
                        "dso_person.lastname,\n" + "dso_person.organization,\n" + "dso_person.street,\n" + "dso_person.location,\n"
                        + "doc.summary,\n"
                        + // 10
                        "doc.internaldocument,\n" + "dsdoc.source,\n" + "rcpt.firstname,\n" + "rcpt.lastname,\n"
                        + "rcpt.organization,\n"
                        + // 15
                        "rcpt.street,\n" + "rcpt.location,\n" + "dso_container.id," + "dso_container.officeid, "
                        + "doc.creatingUser,"
                        + // 20
                        "doc.prepstate," + "deliv.name, " + "doc.clerk, " + "doc.currentassignmentguid as currentassignmentguid, "
                        + "dsdoc.ctime, "
                        + // 25
                        "dsdoc.dockind_id, " + "dsdoc.lastRemark, " + "doc.DOCUMENTDATE, " + "doc.referenceid, "
                        + "doc.dailyOfficeNumber "
                        + // 30
                        "from\n" + "dso_out_document doc " + "inner join ds_document dsdoc " + nolock + " on (doc.id = dsdoc.id " + (updateType == 1 ? "and doc.id = ? " : "and doc.id in (" + DOCUMENT_ID_FROM_TASKS_QUERY.replace("?", "'" + username + "'") + ")") + ") \n" + "left outer join dso_person " + nolock
                        + " on (doc.sender = dso_person.id and dso_person.discriminator='SENDER')\n" + "left outer join dso_person rcpt " + nolock + " on (doc.id = rcpt.document_id and (rcpt.posn = 0 or rcpt.posn IS NULL))\n" + "left outer join dso_out_document_delivery deliv " + nolock
                        + " on (doc.delivery = deliv.id)\n" + "left outer join dso_container" + nolock + " on doc.case_id = dso_container.id\n";

    }

	@Override
	protected String prepareOrdSQL(String username, int updateType)
	{
		String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK) " : "";
		return "select " + "doc.id, "
				+ // 1
				"doc.officenumber, " + "doc.officenumberyear, \n" + "doc.summary,\n" + "dsdoc.source,\n"
				+ // 5
				"dso_container.id," + "dso_container.officeid, " + "doc.creatingUser, " + "dsdoc.ctime, " + "dsdoc.lastRemark, "
				+ // 10
				"doc.dailyOfficeNumber "
				+ // 11
				"from\n" + "dso_order doc " + "inner join ds_document dsdoc " + nolock + " on (doc.id = dsdoc.id " + (updateType == 1 ? "and doc.id = ? " : "and doc.id in (" + getActiveProcessesSubquery("DBID_") + ")") + ") \n" + "left outer join dso_container" + nolock
				+ " on doc.case_id = dso_container.id\n";

	}

	/**
	 * 
	 * @param task
	 * @param rsIn
	 * @param dockindNames
	 * @throws java.sql.SQLException
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	protected void setTaskByInResultSet(JBPMTaskSnapshot task, ResultSet rsIn, Map<Integer, String> dockindNames) throws SQLException, EdmException
	{
		task.setDocumentType(InOfficeDocument.TYPE);
		long docid = rsIn.getLong(1);
		log.debug("IInicjuje tast dla document_id = " + docid);
		task.setDocumentId(docid);
		task.setLabelhidden(hasHiddingLabel(docid, task.getDsw_resource_res_key()));
		task.setIsAnyImageInAttachments(AttachmentsManager.isAnyImageInAttachments(docid));
		task.setIsAnyPdfInAttachments(AttachmentsManager.isAnyPdfInAttachments(docid));
		int ko = rsIn.getInt(2);
		if (!rsIn.wasNull())
			task.setDocumentOfficeNumber(ko);
		// int dko = rsIn.getInt(27);
		// if(!rsIn.wasNull())
		// task.setDailyOfficeNumber(dko);
		int koYear = rsIn.getInt(3);
		if (!rsIn.wasNull())
			task.setDocumentOfficeNumberYear(new Integer(koYear));
		task.setPerson_firstname(rsIn.getString(4));
		task.setPerson_lastname(rsIn.getString(5));
		task.setPerson_organization(cutString(rsIn.getString(6), 128, "Person_organization"));
		task.setPerson_street(rsIn.getString(7));
		task.setPerson_location(rsIn.getString(8));
		task.setDocumentSummary(cutString(rsIn.getString(9), 254, "DocumentSummary"));
		task.setDocumentReferenceId(rsIn.getString(10));
		task.setDocument_source(rsIn.getString(11));
		task.setCaseId(rsIn.getLong(12));
		task.setOfficeCase(rsIn.getString(13));
		task.setDocumentAuthor(rsIn.getString(14));
		task.setDocumentClerk(rsIn.getString(15));
		if (rsIn.getInt(16) == 0)
			task.setAnswerCounter(rsIn.getInt(16));
		else
			task.setAnswerCounter(-1);
		task.setIncomingDate(rsIn.getTimestamp(17));
		if(task.getReceiveDate() == null)
			task.setReceiveDate(rsIn.getTimestamp(17));

		int dockind_id = rsIn.getInt(18);

		if (dockind_id != 0)
		{
			setDockindFields(docid, dockind_id, task);
		}
		else
		{
			task.setDocumentDate(rsIn.getDate(22));
		}

		task.setDocumentCtimeAndCdate(rsIn.getTimestamp(19));
		task.setLastRemark(cutString(rsIn.getString(21), 240, "LastRemark"));
		//ustawiane na podstawie taska
		//task.setDivisionGuid(rsIn.getString(24));
		task.setBackupPerson(false);
		try
		{
			if (!rsIn.getBoolean(2) && task.getDivisionGuid() != null)
			{
				if (DSDivision.isInDivisionAsBackup(DSDivision.find(task.getDivisionGuid()), task.getDsw_resource_res_key()))
				{
					task.setBackupPerson(true);
				}
			}
		}
		catch (Exception e)
		{
			log.error("Poprawic to jest brana zla kolumna pod uwage", e);

		}
	}

	/**
	 * 
	 * @param task
	 * @param rsOut
	 * @param dockindNames
	 * @throws java.sql.SQLException
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	protected void setTaskByOutResultSet(JBPMTaskSnapshot task, ResultSet rsOut, Map<Integer, String> dockindNames) throws SQLException, EdmException
	{
		long docid = rsOut.getLong(1);
		log.debug("Inicjuje task dla document_id = " + docid);
		task.setDocumentId(docid);
		task.setLabelhidden(hasHiddingLabel(docid, task.getDsw_resource_res_key()));
		task.setIsAnyImageInAttachments(AttachmentsManager.isAnyImageInAttachments(docid));
		task.setIsAnyPdfInAttachments(AttachmentsManager.isAnyPdfInAttachments(docid));
		int ko = rsOut.getInt(2);
		if (!rsOut.wasNull())
			task.setDocumentOfficeNumber(ko);
		int dko = rsOut.getInt(29);
		if (!rsOut.wasNull())
			task.setDailyOfficeNumber(dko);
		int koYear = rsOut.getInt(3);
		if (!rsOut.wasNull())
			task.setDocumentOfficeNumberYear(new Integer(koYear));

		task.setPerson_firstname(rsOut.getString(4));
		task.setPerson_lastname(rsOut.getString(5));
		task.setPerson_organization(cutString(rsOut.getString(6), 128, "Person_organization"));
		task.setPerson_street(rsOut.getString(7));
		task.setPerson_location(rsOut.getString(8));
		task.setDocumentSummary(rsOut.getString(9));

		String type;
		if (rsOut.getBoolean(10))
			type = OutOfficeDocument.INTERNAL_TYPE;
		else
			type = OutOfficeDocument.TYPE;
		task.setDocumentType(type);

		task.setDocument_source(rsOut.getString(11));

		task.setRcpt_firstname(rsOut.getString(12));
		task.setRcpt_lastname(rsOut.getString(13));
		task.setRcpt_organization(cutString(rsOut.getString(14), 50, "Rcpt_organization"));
		task.setRcpt_street(rsOut.getString(15));
		task.setRcpt_location(rsOut.getString(16));
		task.setCaseId(rsOut.getLong(17));
		task.setOfficeCase(rsOut.getString(18));

		task.setDocumentAuthor(rsOut.getString(19));
		task.setDocumentPrepState(OutOfficeDocument.getPrepStateString(rsOut.getString(20)));
		task.setDocumentDelivery(rsOut.getString(21));
		task.setDocumentClerk(rsOut.getString(22));
		

		task.setDocumentCtimeAndCdate(rsOut.getTimestamp(24));
		if(task.getReceiveDate() == null)
			task.setReceiveDate(rsOut.getTimestamp(24));
		task.setLastRemark(rsOut.getString(26));

		int dockind_id = rsOut.getInt(25);

		if (dockind_id != 0)
		{
			setDockindFields(docid, dockind_id, task);
		}
		else
		{
			task.setDocumentDate(rsOut.getDate(27));
		}

		task.setDocumentReferenceId(rsOut.getString(28));
		task.setBackupPerson(false);
		try
		{
			if (!task.isAccepted() && task.getDivisionGuid() != null)
			{
				if (DSDivision.isInDivisionAsBackup(DSDivision.find(task.getDivisionGuid()), task.getDsw_resource_res_key()))
				{
					task.setBackupPerson(true);
				}
			}
		}
		catch (Exception e)
		{
			log.error("Poprawic to kurka wodna", e);
		}
	}

	/**
	 * 
	 * @param task
	 * @param rsOrd
	 * @param dockindNames
	 * @throws java.sql.SQLException
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	protected void setTaskByOrdResultSet(JBPMTaskSnapshot task, ResultSet rsOrd, Map<Integer, String> dockindNames) throws SQLException, EdmException
	{

		task.setDocumentId(rsOrd.getLong(1));
		int ko = rsOrd.getInt(4);
		if (!rsOrd.wasNull())
			task.setDocumentOfficeNumber(ko);
		int dko = rsOrd.getInt(27);
		if (!rsOrd.wasNull())
			task.setDailyOfficeNumber(dko);
		int koYear = rsOrd.getInt(3);
		if (!rsOrd.wasNull())
			task.setDocumentOfficeNumberYear(new Integer(koYear));

		task.setPerson_firstname("");
		task.setPerson_lastname("");
		task.setPerson_organization("");
		task.setPerson_street("");
		task.setPerson_location("");
		task.setDocumentSummary(rsOrd.getString(4));
		task.setDocument_source(rsOrd.getString(5));
		task.setDocumentType(OfficeOrder.TYPE);
		task.setCaseId(rsOrd.getLong(6));
		task.setOfficeCase(rsOrd.getString(7));
		task.setDocumentAuthor(rsOrd.getString(8));
		task.setDocumentCtimeAndCdate(rsOrd.getTimestamp(9));
		task.setLastRemark(rsOrd.getString(10));
		task.setLabelhidden(hasHiddingLabel(task.getDocumentId(), task.getDsw_resource_res_key()));
		task.setIsAnyImageInAttachments(AttachmentsManager.isAnyImageInAttachments(task.getDocumentId()));
		task.setIsAnyPdfInAttachments(AttachmentsManager.isAnyPdfInAttachments(task.getDocumentId()));
	}

	/**
	 * 
	 * @param task
	 * @param taskId
	 * @param user
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	protected void setProcessDependentAttributes(JBPMTaskSnapshot task, String taskId, String user) throws EdmException
	{
		if (log.isTraceEnabled())
		{
			log.trace("setProcessDependentAttributes taskId={}, user={}", taskId, user);
		}
		WorkflowActivity activity = WorkflowFactory.getActivityByTaskId(taskId);
		task.setAccepted(activity.getAccepted());
		task.setRcvAndReceiveDate(activity.getReceiveDate());
		task.setActivity_lastActivityUser(activity.getLastUser());
		task.setObjective(activity.getObjective());
		task.setActivityKey(activity.getTaskId());
		task.setDescription(activity.getProcessDesc());
		task.setDsw_resource_res_key(user);
		task.setDeadlineTime(activity.getDeadlineTime());
		task.setDeadlineRemark(activity.getDeadlineRemark());
		task.setDeadlineAuthor(activity.getDeadlineAuthor());
		task.setDivisionGuid(activity.getCurrentGuid());
	}

	protected void setUpSnapshotStatement(PreparedStatement ps, int updateType, String username, Long id) throws SQLException
	{
		if (log.isTraceEnabled())
		{
			log.trace("setUpSnapshotStatement");
			log.trace("updateType={}", updateType);
			log.trace("username={}", username);
			log.trace("id={}", id);
		}
		// FIXME na razie odpuszczam update pism usera
		if (updateType == 1 && id != null)
			ps.setLong(1, id);
		/*
		 * if(username != null) { ps.setString(1, username); if( updateType ==
		 * 1 && id != null) ps.setLong(2, id); } else { if(updateType == 1 &&
		 * id != null) ps.setLong(1, id); }
		 */
	}
    @Override
    public TaskPositionBean getTaskPosition(String activityKey){
        return getTaskPositionImpl(activityKey);
    }

	static TaskPositionBean getTaskPositionImpl(String activityKey)
	{
		//TODO: przeronbioc na wyciaganie z JBPM
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			ps = DSApi.context().prepareStatement("select count(1), max(division_guid), max(last_state_time), max(assigned_resource), max(accepted), max(dockind_name) from dsw_jbpm_tasklist where activity_key = ? ");
			ps.setString(1, activityKey);
			rs = ps.executeQuery();
			String resource = null;
			Integer position = null;
			String dsParticipantId = null;
			String dockindname = null;
			java.sql.Timestamp last_state_time = null;
			boolean accept = false;
			Integer count = 0;
			
			if(rs.next())
			{
				count = rs.getInt(1);
				dsParticipantId =rs.getString(2);
				last_state_time = rs.getTimestamp(3);
				resource = rs.getString(4);
				accept = rs.getBoolean(5);
				dockindname = rs.getString(6);
			}
			if(count > 1)
			{/**na grupe*/
				ps.close();
				rs.close();
				log.trace("select na grupe");
				ps = DSApi.context().prepareStatement("select count(distinct document_id) from dsw_jbpm_tasklist where division_guid = ? and resource_count > 1 and last_state_time < ? and accepted < 1 and dockind_name = ? ");
				ps.setString(1, dsParticipantId);
				ps.setTimestamp(2, last_state_time);
				ps.setString(3, dockindname);
				rs = ps.executeQuery();
				
				if(rs.next())
				{
					position = rs.getInt(1);
					resource = null;
					
				}
			}
			else
			{/**na uzytkownika*/
				ps.close();
				rs.close();
				log.trace("select na uzytkownika {}",resource);
				ps = DSApi.context().prepareStatement("select count(id) from dsw_jbpm_tasklist where assigned_resource = ? and last_state_time < ? and dockind_name = ? ");
				ps.setString(1, resource);
				ps.setTimestamp(2, last_state_time);
				ps.setString(3, dockindname);
				rs = ps.executeQuery();
				if(rs.next())
					position = rs.getInt(1);
			}
			TaskPositionBean bean = new TaskPositionBean();
			bean.setAccept(accept);
			if(dsParticipantId != null)
				bean.setDivision(dsParticipantId);
			bean.setPosition(position+1);
			bean.setUser(resource);
			return bean;
		}
		catch (Exception e) 
		{
			log.error("",e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return null;
	}

    @Override
    public boolean isAssignedToUser(String userName, Long documentId) throws EdmException {
        return isAssignedToUserImpl(userName, documentId);
    }

    static boolean isAssignedToUserImpl(String userName, long documentId) throws EdmException
    {
		if (!Jbpm4Provider.getInstance().isJbpm4Supported())
			return true;
        try
        {
     	   for (JBPMTaskSnapshot task : JBPMTaskSnapshot.findByDocumentId(documentId))
     	   {
     		   log.debug("TASK: {}", task.getDsw_resource_res_key());
     		if (task.getDsw_resource_res_key().equalsIgnoreCase(userName))
     			return true;
     		else if (checkSubstitute(userName, task.getDsw_resource_res_key()))
     			return true;
     	   }
     	   return false;
        }
        catch (EdmException e)
        {
            throw new EdmException(e);
        }
    }

	private static boolean checkSubstitute(String logUser, String assignedUser) throws EdmException
	{
		for (SubstituteHistory sub: SubstituteHistory.findActualSubstituted(logUser))
	   		if (sub.getLogin().equals(assignedUser)) return true;	
		return false;
	}
	
static class DecretationTarget {
		
		private String userName;
		private String divisionGuid;
		private Boolean isDivisionTarger;
		
		public DecretationTarget() {
			
		}
		
		public DSDivision getDsDivision(){
			try {
				return DSDivision.find(this.divisionGuid);
			} catch (Exception e) {
				log.error("Nie znaleziono dzia�u" + this.divisionGuid);
				try {
					return DSDivision.find(DSDivision.ROOT_GUID);
				} catch(Exception ex) {
					log.error(ex.getMessage(), ex);
					return null;
				}
			}
		}
		
		public DSUser getDsUser(){
			try {
				return DSUser.findByUsername(this.userName);
			} catch (Exception e) {
				log.error("Nie znaleziono uzytkownika" + this.userName);
				try {
					return DSUser.findByUsername("admin");
				} catch(Exception ex) {
					log.error(ex.getMessage(), ex);
					return null;
				}
			}
		}
		
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getDivisionGuid() {
			return divisionGuid;
		}
		public void setDivisionGuid(String divisionGuid) {
			this.divisionGuid = divisionGuid;
		}

		public Boolean getIsDivisionTarger() {
			return isDivisionTarger;
		}

		public void setIsDivisionTarger(Boolean isDivisionTarger) {
			this.isDivisionTarger = isDivisionTarger;
		}
		
	}
	
}
