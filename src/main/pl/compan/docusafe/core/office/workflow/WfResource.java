package pl.compan.docusafe.core.office.workflow;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfResource.java,v 1.2 2005/02/09 16:59:30 lk Exp $
 */
public interface WfResource
{
    String resource_key() throws WorkflowException;
    String resource_name() throws WorkflowException;
    String email_address() throws WorkflowException;
    String password() throws WorkflowException;
    WfAssignment[] get_sequence_work_item() throws WorkflowException;
    int[] getTaskCount() throws WorkflowException;
}
