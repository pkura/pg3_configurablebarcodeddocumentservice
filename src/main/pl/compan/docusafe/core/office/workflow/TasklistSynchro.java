package pl.compan.docusafe.core.office.workflow;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;

/**
 * Klasa odpowiedzialna za synchronizacj� listy zada� - czyli uaktualnianie
 * snapshota (tabelki dsw tasklist) na podstawie rzeczywistych danych o
 * zadaniach.
 */
public class TasklistSynchro
{
	static final Logger log = LoggerFactory.getLogger(TasklistSynchro.class);
	
    /**
     * Synchronizuje wszystko. Uruchamiane tylko podczas startu tomcata.
     * 
     * @throws EdmException
     */
    public static void Synchro(DSContext ctx) throws EdmException
    {
    	log.debug("Docusafe: TasklistSynchro");
    
    	List<DSUser> users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
    	int i = 0;
    	for (DSUser user : users) 
    	{
    		try
    		{
	    		try
	    		{
	    			i++;
		    		Synchro(user.getName());
		    		//czyszczenie sesji
		    		ctx.commit();
		    		log.error("Czyszczenie sesi po "+i+" user = "+ user.getName());
					ctx.session().flush();
					ctx.session().clear();
		    		ctx.begin();
	    		}
	    		catch (Exception e) 
	    		{
	    			log.error("B�AD dla u�yutkownika "+ user.getName());
	    			log.error(e.getMessage(),e);
	    			try
	    			{
	    				ctx._rollback();
	    			}
	    			catch (Exception e2) {log.error(e2.getMessage(),e2);}
	    			try
    				{
    					DSApi.close();
    				}
    				catch (Exception e3) 
    				{
    					log.error(e3.getMessage(),e3);
    				}
	    			DSApi.openAdmin();
	    			ctx.begin();
				}
    		}
    		catch (Exception e) 
    		{
    			try
    			{
    				try
    				{
    					DSApi.close();
    				}
    				catch (Exception e3) 
    				{
    					log.error(e3.getMessage(),e3);
    				}
	    			DSApi.openAdmin();
	    			ctx.begin();
    			}
    			catch (Exception e2) 
    			{
    				log.error(e2.getMessage(),e2);
    			}
    		}
		}
    	//TaskSnapshot.updateTaskList(SnapshotProvider.SYNCHRO_CODE, "anyType", TaskSnapshot.UPDATE_TYPE_ONE_USER,null);
    	
//		if (AvailabilityManager.isAvailable("tasklist.jbpm")) {
//    		JBPMTaskSnapshot.updateTaskList(SYNCHRO_CODE, "anyType", 2,null);
//		}
    }

    /**
     * Synchronizacja dla zada� danego u�ytkownika. Uruchamiane z ustawie� listy
     * zada� przyciskiem "Od�wie� list� zada�".
     * @TODO - caly external workflow do przerobki. 
     * @param username
     * @throws EdmException
     */
    public static void Synchro(String username) throws EdmException
    {       
    	TaskSnapshot.updateTaskList(new Long(0), "anyType", TaskSnapshot.UPDATE_TYPE_ONE_USER, username);	       
    }    
}
