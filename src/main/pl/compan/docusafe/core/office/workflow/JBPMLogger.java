package pl.compan.docusafe.core.office.workflow;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public class JBPMLogger implements ActionHandler{

	private final static Logger log = LoggerFactory.getLogger(JBPMLogger.class);

	public void execute(ExecutionContext ec) throws Exception {
	
		if(log.isTraceEnabled()){
			log.trace("Proces [{}] {}: {}", ec.getProcessInstance().getId(), ec.getNode().getName(), ec.getEvent());
		}
	}
}
