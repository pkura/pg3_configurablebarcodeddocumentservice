package pl.compan.docusafe.core.office.workflow;

import java.util.Date;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.internal.WfActivityImpl;
import pl.compan.docusafe.core.office.workflow.internal.WfProcessImpl;
import pl.compan.docusafe.core.office.workflow.xpdl.Activity;

/**
 * klasa do pobierania parametr�w zadania zale�nych od procesu. instancje pobiera si� z WorkflowFactory
 * @author kuba �wiat�y
 *
 */
public class WorkflowActivity implements WfActivity, WfProcess, WfAssignment {

	private String taskId;
	
	private Boolean accepted;
	private Date receiveDate;
	
	private String lastUser;
	private String lastDivision;
	private String currentUser;
	private String currentGuid;
	
	private String objective;	
	private String processKind;
	private String processDesc;
	
	private Date deadlineTime;
	private String deadlineAuthor;
	private String deadlineRemark;
	
	private Long documentId;
	
	
	private Boolean external = false;
	private WfActivity iwActivity;
	
	public WorkflowActivity(Long taskId, Boolean accepted, Date receiveDate,
			String lastUser, String lastDivision, String currentUser, String currentGuid,
			String objective, String processKind, String processDesc, Date deadlineTime,
			String deadlineAuthor, String deadlineRemark, Long documentId) {
		super();
		this.taskId = String.valueOf(taskId);
		this.accepted = accepted;
		this.receiveDate = receiveDate;
		this.lastUser = lastUser;
		this.lastDivision = lastDivision;
		this.currentUser = currentUser;
		this.currentGuid = currentGuid;
		this.objective = objective;
		this.processKind = processKind;
		this.processDesc = processDesc;
		this.deadlineTime = deadlineTime;
		this.deadlineAuthor = deadlineAuthor;
		this.deadlineRemark = deadlineRemark;
		this.documentId = documentId;
	}

	public WorkflowActivity(String activityKey) throws EdmException {
		external = true;
		iwActivity = WorkflowFactory.getInstance().getOldActivity(activityKey);
	}
	
	

	public Boolean getAccepted() throws EdmException {
		if (external) {
			try {
				WfAssignment assignment = WorkflowFactory.getWfAssignment("internal",iwActivity.key());
				return assignment.get_accepted_status();
			} catch (WorkflowException e) {
				throw new EdmException(e);
			}
		}
		return accepted;
	}

	public Date getReceiveDate() throws WorkflowException {
		if (external) {
			return iwActivity.last_state_time();
		}
		return receiveDate;
	}

	public String getLastUser() throws WorkflowException, EdmException {
		if (external) {
			return WorkflowUtils.findParameterValue(getActivity().process_context(), "ds_source_user", String.class);
		}
		return lastUser;
	}
	
	public String getLastDivision() throws WorkflowException, EdmException {
		if (external) {
			return WorkflowUtils.findParameterValue(getActivity().process_context(), "ds_source_division", String.class);
		}
		return lastDivision;
	}


	public String getCurrentUser() throws WorkflowException, EdmException {
		if (external) {
			String participId = getParticipantId();
			if (participId != null && participId.startsWith("ds_guid")) {
				//to jest pismo zadekretowane na dzial
				return null;
			}
			return WorkflowUtils.findParameterValue(getActivity().process_context(), "ds_assigned_user", String.class);
		}
		return currentUser;
	}

	public String getCurrentGuid() throws WorkflowException, EdmException {
		if (external) {
			return WorkflowUtils.findParameterValue(getActivity().process_context(), "ds_assigned_division", String.class);
		}
		return currentGuid;
	}

	public String getObjective() throws WorkflowException, EdmException {
		if (external) {
			return (String) WorkflowUtils.findParameter(getActivity().process_context(), "ds_objective").value().value();
		}
		return objective;
	}

	public String getProcessKind() {
		return processKind;
	}
	
	public String getProcessDesc() throws WorkflowException {
		if (external)
			return getActivity().container().manager().getProcessDefinition().getName();
		return processDesc;
	}

	public Date getDeadlineTime() throws WorkflowException {
		if (external) {
			return ((WfProcessImpl)iwActivity.container()).getDeadlineTime();
		}
		return deadlineTime;
	}

	public String getDeadlineAuthor() throws WorkflowException {
		if (external) {
			return ((WfProcessImpl)iwActivity.container()).getDeadlineAuthor();
		}
		return deadlineAuthor;
	}

	public String getDeadlineRemark() throws WorkflowException {
		if (external) {
			return ((WfProcessImpl)iwActivity.container()).getDeadlineRemark();
		}
		return deadlineRemark;
	}

	public String getTaskId() throws WorkflowException {
		if (external && taskId == null) {
			return getTaskFullId();
		}
		return taskId;
	}
	
	public String getTaskFullId() throws WorkflowException {
		if (external) {
			return "internal,"+getActivity().key();
		} else {
			return "internal,"+taskId;
		}
	}
	
	public Long getDocumentId() throws WorkflowException, EdmException {
		if (external) {
			NameValue nv = WorkflowUtils.findParameter(iwActivity.container().process_context(), "ds_document_id");
			if (nv == null) {
				return null;
			}
			return (Long)nv.value().value();
		}
		return documentId;
	}
	
	/*
	 *  Zapewnienie zgodno�ci wew workflow i jbpm
	 */
	
	public WfActivity getActivity() {
		return iwActivity;
	}
	
	/** 
	 * dla cel�w diagnostycznych
	 * @throws EdmException 
	 */
	public String getParticipantId() throws EdmException {
		NameValue nv = WorkflowUtils.findParameter(iwActivity.process_context(), "ds_participant_id");
		if (nv == null) {
			return null;
		}
		return (String)nv.value().value();
	}
	
	
	/*
	 * metody WfActivity
	 */
	public Activity getActivityDefinition() throws WorkflowException {
		throw new WorkflowException("Deprecated Api: wfActivity");
	}
	
    public String getWorkflowName() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfActivity");
    }

    public String activity_definition_id() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfActivity");
    }
    public void complete() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfActivity");
    }
    public void set_result(NameValue[] result) throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfActivity");
    }
    public NameValue[] result() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfActivity");
    }
    public NameValue[] extended_attributes() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfActivity");
    }
    public NameValue[] process_context() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfActivity");
    }
    public void set_process_context(NameValue[] context) throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfActivity");
    }
    public WfProcess container() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfActivity");
    }
    
    /*
	 * metody WfProcess
	 */
    public int how_many_step() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfProcess");
    }

    public WfActivity[] get_activities_in_state_array(String state) throws WorkflowException{
    	throw new WorkflowException("Deprecated Api: wfProcess");
    }
    public void start() throws WorkflowException{
    	throw new WorkflowException("Deprecated Api: wfProcess");
    }
    public WfProcessMgr manager() throws WorkflowException{
    	throw new WorkflowException("Deprecated Api: wfProcess");
    }
    
    /*
     * metody WfAssignment
     */
    public WfActivity activity() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfAssignment");
    }
    public boolean get_accepted_status() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfAssignment");
    }
    public void set_accepted_status(boolean accepted) throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfAssignment");
    }
    public WfResource assignee() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfAssignment");
    }

    /*
     * WfExecutionObject
     */
    public String key() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfExecutionObject");
    }
    public String name() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfExecutionObject");
    }
    public String state() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfExecutionObject");
    }
    public String description() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfExecutionObject");
    }
    public Date last_state_time() throws WorkflowException {
    	throw new WorkflowException("Deprecated Api: wfExecutionObject");
    }




}
