package pl.compan.docusafe.core.office.workflow.jbpm;

import pl.compan.docusafe.core.office.workflow.WorkflowException;
import pl.compan.docusafe.core.office.workflow.NameValue;
import pl.compan.docusafe.core.office.workflow.WfExecutionObject;

import java.util.Date;
/* User: Administrator, Date: 2006-12-15 15:06:44 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public abstract class WfExecutionObjectImpl  implements WfExecutionObject
{
    protected String key;

    public String getKey()
    {
        return key;
    }

    private void setKey(String key)
    {
        this.key = key;
    }

    public String key() throws WorkflowException
    {
        return key;
    }
    public String name() throws WorkflowException
    {
        return null;
    }
    public String state() throws WorkflowException
    {
        return null;
    }
    public String description() throws WorkflowException
    {
        return null;
    }
    /**
     * Metoda zwracaj�ca tablic� parametr�w zdefiniowanych w procesie.
     * Po ewentualnych modyfikacjach tej tablicy nale�y przekaza� j�
     * do metody set_process_context(), aby zmiany zosta�y uwzgl�dnione
     * przez serwer workflow.
     */
    public NameValue[] process_context() throws WorkflowException
    {
        return null;
    }
    public void set_process_context(NameValue[] context) throws WorkflowException
    {

    }
    public Date last_state_time() throws WorkflowException
    {
        return null;
    }
    public String getWorkflowName()
    {
        return JbpmWorkflowService.NAME;
    }
}
