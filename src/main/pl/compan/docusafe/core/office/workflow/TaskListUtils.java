package pl.compan.docusafe.core.office.workflow;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.tasklist.TaskListAction;

/**
 * Klasa na narz�dzia zwi�zane z tasklist�
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class TaskListUtils {
    private final static Logger LOG = LoggerFactory.getLogger(TaskListUtils.class);
    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� wychodz?cych.
     */
    private static final String[] COLUMN_PROPERTIES_OUT = new String[]
        {
        "numberRows","shortReceiveDate","receiveDate","sender","flags","userFlags","documentOfficeNumber",
            "documentRecipient","documentSummary","process","description","documentDelivery",
            "objective","documentPrepState","documentReferenceId","deadlineTime","officeCase","clerk","lastRemark","viewerLink",
            "dockindStatus","dockindNumerDokumentu","dockindKwota","dockindKategoria","documentDate","documentId", "documentCtime",
            "dockindName",  "decretationTo", "newProcessStatus", "author", "decretation_to_div"
        };
    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� wewn�trznych.
     */
    private static final String[] COLUMN_PROPERTIES_INT = new String[]
        {
        "numberRows","shortReceiveDate","receiveDate","sender","flags","userFlags",
            "documentOfficeNumber", "documentSummary","description","objective","deadlineTime","clerk","lastRemark","officeCase","viewerLink"
            ,"documentReferenceId",
            "dockindStatus","dockindNumerDokumentu","dockindKwota","dockindKategoria","documentDate",
            "answerCounter","documentId", "documentCtime", "dockindName", "newProcessStatus","author", "decretation_to_div"

        };
    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� przychodz?cych.
     */
    private static final String[] COLUMN_PROPERTIES_IN = new String[]
        {
        "numberRows","shortReceiveDate","receiveDate","incomingDate" ,"sender","flags","userFlags","documentSender",
            "documentOfficeNumber","answerCounter","documentSummary","process","clerk","description",
            "objective","documentReferenceId","deadlineTime","officeCase", //"caseInOfficeFinishDate",
            "dockindName", "lastRemark","viewerLink",
            "dockindStatus","dockindNumerDokumentu","dockindKwota","dockindKategoria","documentDate","documentId",
            "caseDeadlineDate", "documentCtime", "decretationTo", "newProcessStatus","author", "decretation_to_div"
        };
    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� wydanych polece�.
     */
    public static final String[] COLUMN_PROPERTIES_MY_ORDERS = new String[]
        {
            "documentOfficeNumber", "receiveDate", "documentSummary", "orderStatus", "receiver","documentId", "documentCtime"
        };
    private static final String[] DEFAULT_MY_ORDERS_COLUMNS = COLUMN_PROPERTIES_MY_ORDERS;
    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� realizowanych polece�.
     */
    public static final String[] COLUMN_PROPERTIES_REALIZE_ORDERS = new String[]
        {
            "documentOfficeNumber", "receiveDate", "documentSummary", "orderStatus", "author", "sender", "documentCtime"
        };
    private static final String[] DEFAULT_REALIZE_ORDERS_COLUMNS = COLUMN_PROPERTIES_REALIZE_ORDERS;
    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� obserwowanych polece�.
     */
    public static final String[] COLUMN_PROPERTIES_WATCH_ORDERS = new String[]
        {
            "documentOfficeNumber", "receiveDate", "documentSummary", "orderStatus", "author", "sender", "receiver", "documentCtime"
        };
    private static final String[] DEFAULT_WATCH_ORDERS_COLUMNS = COLUMN_PROPERTIES_WATCH_ORDERS;
    private static final String[] COLUMN_PROPERTIES_WATCHLIST = new String[]
        {
        "numberRows","shortReceiveDate","receiveDate","incomingDate" ,"sender","flags","userFlags","documentSender",
	        "documentOfficeNumber","answerCounter","documentSummary","process","clerk","description",
	        "objective","documentReferenceId","deadlineTime","officeCase", //"caseInOfficeFinishDate",
	        "dockindName", "lastRemark","viewerLink",
	        "dockindStatus","dockindNumerDokumentu","dockindKwota","dockindKategoria","documentDate","documentId",
	        "caseDeadlineDate", "documentCtime","taskPosition", "newProcessStatus", "decretation_to_div"
        };
    private static final String[] DEFAULT_CASE_COLUMNS = new String[]
        {
            "caseOfficeId", "caseStatus", "caseOpenDate", "caseFinishDate",
            "caseDaysToFinish", "documentCtime" , "caseTitle"
        };
    private static final String[] DEFAULT_WATCHLIST_COLUMNS = new String[]
        {
            "documentOfficeNumber", "receiveDate", "sender", "description",
            "objective", "deadlineTime", "documentCtime"
        };
    private static final String[] AVAILABLE_FILTER_COLUMNS = new String[]
            {
    	"documentId","documentOfficeNumber","caseOfficeId","caseOpenDate","caseStatus","clerk","deadlineTime","description","dockindName","documentDelivery",
    	"documentPrepState","documentRecipient","documentReferenceId","documentSummary","incomingDate","objective","officeCase", //"caseInOfficeFinishDate",
    	"orderNumber","orderStatus","receiveDate","receiver","sender",
    	"dockindStatus","dockindNumerDokumentu","dockindKwota","dockindKategoria","documentDate",
    	"dockindBusinessAtr1","dockindBusinessAtr2","dockindBusinessAtr3","dockindBusinessAtr4","dockindBusinessAtr5","dockindBusinessAtr6", "documentCtime", "decretation_to_div", "decretationTo"
            };
    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie spraw.
     */
    private static final String[] COLUMN_PROPERTIES_CASES = new String[]
        {
            "caseOfficeId", "caseStatus", "caseOpenDate", "caseFinishDate",
            "caseDaysToFinish", "caseDescription", "caseTitle"
        };
    private static final String[] DEFAULT_TASK_COLUMNS = new String[]
        {
            "documentOfficeNumber", "receiveDate", "sender", "description",
            "objective", "documentCtime", "decretationTo" //, "deadlineTime"
        };
    public static final String[] COLUMN_PROPERTIES_DOCUMENT_PACKAGE = new String[]
    {
    	"numberRows","documentId","documentOfficeNumber","receiveDate",
       "description","objective","sender","dockindStatus", "documentCtime",
        "decretationTo", "author","viewerLink"
    	
    };

    public static String getColumnDescription(String property) {
		return getColumnDescription(property, null);
	}
    
    public static String getColumnDescription(String property, String bookmarkName)
    {
    	if(bookmarkName != null) {
    		StringBuilder sb = new StringBuilder(bookmarkName);
    		sb.append(".column.").append(property);
	    	String prop = Docusafe.getAdditionProperty(sb.toString());
	    	if(prop!= null && !prop.equals("false")){
	    		return prop;
	    	}
    	}
    	if(property.startsWith("dockindBusinessAt"))
    	{
    		return Docusafe.getAdditionProperty("column."+property);
    	}
        StringManager smL = GlobalPreferences.loadPropertiesFile(TaskListAction.class.getPackage().getName(),null);
        String a = smL.getString("column."+property);
        return smL.getString("column."+property);
    }

   
    
    public static String[] getColumnProperties(String tab, String orderTab)
    {
        if (TaskListAction.TAB_OUT.equals(tab))
            return COLUMN_PROPERTIES_OUT;
        else if (TaskListAction.TAB_IN.equals(tab))
        {
            return COLUMN_PROPERTIES_IN;
        }
        else if (TaskListAction.TAB_INT.equals(tab))
            return COLUMN_PROPERTIES_INT;
        else if (TaskListAction.TAB_ORDER.equals(tab))
        {
            if (TaskListAction.TAB_MY_ORDERS.equals(orderTab))
                return COLUMN_PROPERTIES_MY_ORDERS;
            else if (TaskListAction.TAB_REALIZE_ORDERS.equals(orderTab))
                return COLUMN_PROPERTIES_REALIZE_ORDERS;
            else if (TaskListAction.TAB_WATCH_ORDERS.equals(orderTab))
                return COLUMN_PROPERTIES_WATCH_ORDERS;
        }
        else if (TaskListAction.TAB_CASES.equals(tab))
            return COLUMN_PROPERTIES_CASES;
        else if (TaskListAction.TAB_WATCHES.equals(tab))
            return COLUMN_PROPERTIES_WATCHLIST;
        else if (TaskListAction.TAB_DOCUMENT_PACKAGE.equals(tab))
            return COLUMN_PROPERTIES_DOCUMENT_PACKAGE;
        return
            new String[0];
    }

    public static String[] getDefaultColumnProperties(String tab, String orderTab)
    {

        if (TaskListAction.TAB_IN.equals(tab) || TaskListAction.TAB_OUT.equals(tab) || TaskListAction.TAB_INT.equals(tab))
        {
        	String prop = DSApi.context().systemPreferences().node("default-task-list").get("default-columns_"+tab, null);
            return (prop != null ? prop.split(","): DEFAULT_TASK_COLUMNS);
        }
        else if (TaskListAction.TAB_ORDER.equals(tab))
        {
            if (TaskListAction.TAB_MY_ORDERS.equals(orderTab))
                return DEFAULT_MY_ORDERS_COLUMNS;
            else if (TaskListAction.TAB_REALIZE_ORDERS.equals(orderTab))
                return DEFAULT_REALIZE_ORDERS_COLUMNS;
            else if (TaskListAction.TAB_WATCH_ORDERS.equals(orderTab))
                return DEFAULT_WATCH_ORDERS_COLUMNS;
            else
                return new String[0];
        }
        else if (TaskListAction.TAB_WATCHES.equals(tab))
        {
        	String prop = DSApi.context().systemPreferences().node("default-task-list").get("default-columns_"+tab, null);
            return (prop != null ? prop.split(","): DEFAULT_WATCHLIST_COLUMNS);
        }
        else if (TaskListAction.TAB_DOCUMENT_PACKAGE.equals(tab))
            return COLUMN_PROPERTIES_DOCUMENT_PACKAGE;
        else if("filter".equals(tab))
        {
        	return AVAILABLE_FILTER_COLUMNS;
        }
        else
            return DEFAULT_CASE_COLUMNS;
    }
}