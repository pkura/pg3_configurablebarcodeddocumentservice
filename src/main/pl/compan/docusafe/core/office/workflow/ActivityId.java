package pl.compan.docusafe.core.office.workflow;

import pl.compan.docusafe.core.names.WfActivityURN;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ActivityId.java,v 1.7 2006/02/08 16:02:56 lk Exp $
 */
public class ActivityId
{
    private String wfName;
    private String activityKey;

    public static ActivityId parse(String activityId) throws WorkflowException
    {
        if (activityId == null)
            throw new WorkflowException("activityId == null");

        String[] parsed = WorkflowUtils.parseWfObjectId(activityId);

        return new ActivityId(parsed[0], parsed[1]);
    }

    public static ActivityId make(String workflowName, String activityKey)
    {
        return new ActivityId(workflowName, activityKey);
    }

    public static ActivityId make(WfActivity activity) throws WorkflowException
    {
        return new ActivityId(activity.getWorkflowName(), activity.key());
    }

    public static ActivityId make(WfActivityURN urn)
    {
        return new ActivityId(urn.getWorkflow(), urn.getKey());
    }

    private ActivityId(String wfName, String activityKey)
    {
        this.wfName = wfName;
        this.activityKey = activityKey;
    }

    public String getWfName()
    {
        return wfName;
    }

    public String getActivityKey()
    {
        return activityKey;
    }

    /**
     * Tworzy publiczną reprezentację identyfikatora zadania.
     */
    public String toString()
    {
        return wfName+","+activityKey;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ActivityId that = (ActivityId) o;
        if (activityKey != null ? !activityKey.equals(that.activityKey) : that.activityKey != null) return false;
        if (wfName != null ? !wfName.equals(that.wfName) : that.wfName != null) return false;
        return true;
    }

    public int hashCode()
    {
        int result;
        result = (wfName != null ? wfName.hashCode() : 0);
        result = 29 * result + (activityKey != null ? activityKey.hashCode() : 0);
        return result;
    }
}
