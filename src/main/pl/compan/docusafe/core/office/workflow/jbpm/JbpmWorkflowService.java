package pl.compan.docusafe.core.office.workflow.jbpm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jbpm.JbpmConfiguration;
import org.jbpm.JbpmContext;
import org.jbpm.taskmgmt.exe.TaskInstance;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 * @deprecated - Sposob realizacji JBPM jest obecnie inny
 */
@Deprecated
public class JbpmWorkflowService //implements WfService
{
    public static final String NAME = "jbpm";

    public static void setVariable(TaskSnapshot task, String variable, Object value)
    {
        setVariable(task.getExternalTaskId(), variable, value);
    }

    /**
     * Przyporzadkowanie w procesie danego zadania zmiennej "variable" wartosci "value".
     * @param taskId
     * @param variable
     * @param value
     */
    public static void setVariable(Long taskId, String variable, Object value)
    {
        JbpmConfiguration jbpmConfiguration = Docusafe.getJbpmConfiguration();
        JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
        try
        {
            TaskInstance taskInstance = jbpmContext.getTaskMgmtSession().loadTaskInstance(taskId);
            taskInstance.setVariable(variable, value);
        }
        finally
        {
            jbpmContext.close();
        }
    }

    /**
     * Przyporzadkowanie w procesie danego zadania zmiennym z tablicy "variable" wartosci z tablicy "value".
     * Obie tablice musza miec tan sam rozmiar.
     * @param taskId
     * @param variable
     * @param value
     */
    public static void setVariables(Long taskId, String[] variable, Object[] value)
    {
        JbpmConfiguration jbpmConfiguration = Docusafe.getJbpmConfiguration();
        JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
        try
        {
            TaskInstance taskInstance = jbpmContext.getTaskMgmtSession().loadTaskInstance(taskId);
            for (int i=0; i < variable.length; i++)
                taskInstance.setVariable(variable[i], value[i]);
        }
        finally
        {
            jbpmContext.close();
        }
    }

    /**
     * Przyporz�dkowanie w procesie danego zadania zmiennym z tablicy "variable" wartosci z tablicy "value".
     * Tablica 'locally' okre�la czy dana zmienna ma by� zapami�tana w ca�ym procesie czy lokalnie w zadaniu.
     * Wszystkie tablice musz� mie� tan sam rozmiar.
     * @param taskId
     * @param variable
     * @param value
     * @param locally
     */
    public static void setVariables(Long taskId, String[] variable, Object[] value, boolean[] locally)
    {
        JbpmConfiguration jbpmConfiguration = Docusafe.getJbpmConfiguration();
        JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
        try
        {
            TaskInstance taskInstance = jbpmContext.getTaskMgmtSession().loadTaskInstance(taskId);
            for (int i=0; i < variable.length; i++)
                if (locally[i])
                    taskInstance.setVariableLocally(variable[i], value[i]);
                else
                    taskInstance.setVariable(variable[i], value[i]);
        }
        finally
        {
            jbpmContext.close();
        }
    }

    /**
     * Przyporzadkowanie zmiennej lokalnej "variable" w danym zadaniu wartosci "value".
     */
    public static void setVariableLocally(Long taskId, String variable, Object value)
    {
        JbpmConfiguration jbpmConfiguration = Docusafe.getJbpmConfiguration();
        JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
        try
        {
            TaskInstance taskInstance = jbpmContext.getTaskMgmtSession().loadTaskInstance(taskId);
            taskInstance.setVariableLocally(variable, value);
        }
        finally
        {
            jbpmContext.close();
        }
    }

    /**
     * Przyporzadkowanie zmiennej lokalnej "variable" w danym zadaniu wartosci "value".
     */
    public static void setVariableLocally(TaskSnapshot task, String variable, Object value)
    {
        JbpmConfiguration jbpmConfiguration = Docusafe.getJbpmConfiguration();
        JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
        try
        {
            TaskInstance taskInstance = jbpmContext.getTaskMgmtSession().loadTaskInstance(task.getExternalTaskId());
            taskInstance.setVariableLocally(variable, value);
        }
        finally
        {
            jbpmContext.close();
        }
    }

    public static Object getVariable(Long taskId, String variable)
    {
        JbpmContext jbpmContext = Docusafe.getJbpmConfiguration().createJbpmContext();
        try
        {
            TaskInstance taskInstance = jbpmContext.getTaskMgmtSession().loadTaskInstance(taskId);
            return taskInstance.getVariable(variable);
        }
        finally
        {
            jbpmContext.close();
        }
    }

    /* uaktualnia "Snapshota" dla zadan JBPM */
    // TODO: dokonczyc
    public static void updateJbpmTasklistByDocumentId(Long documentId, Map<String,Object> changes) throws EdmException
    {
        TaskSnapshot.Query query;
        query = new TaskSnapshot.Query();
        query.setDocumentId(documentId);
        query.setExternalWorkflow(Boolean.TRUE);
        SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
    	List<Task> res = new ArrayList<Task>();
    	for (TaskSnapshot task : results.results())
        {
    		for (String key : changes.keySet())
            {

            }
        }
    }

    /**
     * Akceptowania zadanie JBPM - ta funkcja powinna byc wolana tylko z akcji AcceptWfAssignmentAction
     * dla zadan z zewnetrznego Workflow. Gdy zadanie OPS to takze zmieniamy status pisma.
     *
     * Gdy 'actorId' zadanie jest null, to znaczy, �e by�a dekretacja na grup� - zatem na 'actorId'
     * ustalam aktualnego u�ytkownika, aby zadanie sta�o si� przydzielone i nie pojawia�o si�
     * na listach zada� innych u�ytkownik�w (jako 'pooled task').
     *
     */
    public static void acceptTask(TaskSnapshot task, OfficeDocument document) throws EdmException
    {
        // zawsze zmieniam status na "W realizacji" jesli jest to zadanie OPS (zakladam ze to dokument o doctypie "nationwide")
        JbpmContext jbpmContext = Docusafe.getJbpmConfiguration().createJbpmContext();
        try
        {
            TaskInstance taskInstance = jbpmContext.loadTaskInstanceForUpdate(task.getExternalTaskId());

            // jak konieczne ustawiam aktora
            if (taskInstance.getActorId() == null)
            {
                taskInstance.setActorId(DSApi.context().getPrincipalName());
                // jak zadanie dla DOK, to musz� tak�e zapami�ta� tego aktora na zmiennej
            }

            // ustawiam zadanie jako zaakceptowane
            taskInstance.setVariableLocally(Constants.VARIABLE_TASK_ACCEPTED, Boolean.TRUE);
            
            // dodajemy do historii informacje, �e zadanie przyj�to
            ProcessDefinition definitionInfo = ProcessDefinition.findByDefinitionId(taskInstance.getToken().getProcessInstance().getProcessDefinition().getId());
            TaskDefinition taskDefinition = TaskDefinition.find(taskInstance.getName(), definitionInfo.getDefinitionName());
            String taskName = (taskDefinition != null ? taskDefinition.getNameForUser() : taskInstance.getName());
            document.addAssignmentHistoryEntry(
                    new AssignmentHistoryEntry(AssignmentHistoryEntry.JBPM_NORMAL, null, DSApi.context().getPrincipalName(), null/*dzial*/, taskName, definitionInfo.getNameForUser(), true, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION),
                    false, /*activityKey*/String.valueOf(taskInstance.getId())
                );

            // jesli zadanie OPS to zmieniam status na W_REALIZACJI i ustawiam zmienna OPS_TASK_ACCEPTED na TRUE
       
        }
        finally
        {
            jbpmContext.close();
        }
    }
    
    /**
     * Zmienia status dokumentu, dodaj�c o tym zdarzeniu wpis do historii pisma.
     */
    public static void changeDocumentStatus(OfficeDocument document, Long status) throws EdmException
    {
        StringManager sm =
            GlobalPreferences.loadPropertiesFile(JbpmWorkflowService.class.getPackage().getName(),null);
        Doctype doctype = Doctype.findByCn("nationwide");

        Doctype.Field field = doctype.getFieldByCn("STATUS");
        doctype.set(document.getId(), field.getId(), status);
        
        // dodajemy do historii wpis o zmianie statusu
        String statusName = field.getEnumItemById(status.intValue()).getTitle();
        document.addWorkHistoryEntry(Audit.create("status",
                DSApi.context().getPrincipalName(),
                sm.getString("ProcesWorkflowSpowodowalZmianeStatusuDokumentuNa",statusName),
                statusName, status));
    }
    
    /**
     * Sprawdza czy to zadanie jest moje. Powinno by� wo�ane tylko z akcji AcceptWfAssignmentAction.
     *
     * @return true <=> nie jest to nasze zadanie (dzieje si� tak tylko w przypadku, gdy dekretacja
     *         by�a na grup� i ju� kto� inny wcze�niej zaakceptowa� to zadanie)
     */
    public static boolean isMyTask(TaskSnapshot task)
    {
        JbpmContext jbpmContext = Docusafe.getJbpmConfiguration().createJbpmContext();
        try
        {
            TaskInstance taskInstance = jbpmContext.loadTaskInstanceForUpdate(task.getExternalTaskId());            
            
            // to zadanie nie jest nasze, gdy� by�a dekretacja na grup� i kto� inny nas "uprzedzi�"
            if (taskInstance.getActorId() != null && !DSApi.context().getPrincipalName().equals(taskInstance.getActorId()) /*&&
                taskInstance.getPooledActors() != null && taskInstance.getPooledActors().size() > 0*/)
                return false;
        }
        finally
        {
            jbpmContext.close();
        }
        return true;
    }
}
