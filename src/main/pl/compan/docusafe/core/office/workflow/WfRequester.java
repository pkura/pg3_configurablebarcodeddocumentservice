package pl.compan.docusafe.core.office.workflow;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfRequester.java,v 1.2 2006/02/06 16:05:51 lk Exp $
 */
public interface WfRequester
{
/*
    public int how_many_performer() throws BaseException;

    public WfProcessIterator get_iterator_performer() throws BaseException;

    public WorkflowModel.WfProcess[] get_sequence_performer(int i) throws BaseException;

    public boolean is_member_of_performer(WfProcess wfProcess) throws BaseException;
*/

    public void receive_event(WfEventAudit wfEventAudit)
        throws WorkflowException;
}
