package pl.compan.docusafe.core.office.workflow;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ProcessId.java,v 1.4 2008/02/27 13:29:51 mariuszk Exp $
 */
public class ProcessId
{
    private String wfName;
    private String packageId;
    private String processDefinitionId;

    public static ProcessId parse(String pid) throws WorkflowException
    {
        String[] objectId = WorkflowUtils.parseWfObjectId(pid);

        if (objectId[1].indexOf("::") <= 0)
            throw new WorkflowException("Niepoprawny identyfikator procesu: "+pid);

        return new ProcessId(objectId[0],
            objectId[1].substring(0, objectId[1].indexOf("::")),
            objectId[1].substring(objectId[1].indexOf("::")+2));
    }

    public static ProcessId create(String wfName, String packageId, String processDefinitionId)
    {
        return new ProcessId(wfName, packageId,  processDefinitionId);
    }

    public static ProcessId make(WfProcess wfp) throws WorkflowException
    {
        return new ProcessId(wfp.getWorkflowName(), wfp.manager().package_id(), wfp.manager().process_definition_id());
    }

    public ProcessId(String wfName, String packageId, String processDefinitionId)
    {
        this.wfName = wfName;
        this.packageId = packageId;
        this.processDefinitionId = processDefinitionId;
    }

    public String getWfName()
    {
        return wfName;
    }

    public String getPackageId()
    {
        return packageId;
    }

    public String getProcessDefinitionId()
    {
        return processDefinitionId;
    }

    public String toString()
    {
        return wfName+","+packageId+"::"+processDefinitionId;
    }
}
