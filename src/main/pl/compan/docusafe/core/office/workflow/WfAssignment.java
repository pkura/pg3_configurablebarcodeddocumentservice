package pl.compan.docusafe.core.office.workflow;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfAssignment.java,v 1.2 2004/11/04 16:01:33 lk Exp $
 */
public interface WfAssignment
{
    WfActivity activity() throws WorkflowException;
    boolean get_accepted_status() throws WorkflowException;
    void set_accepted_status(boolean accepted) throws WorkflowException;
    WfResource assignee() throws WorkflowException;
}
