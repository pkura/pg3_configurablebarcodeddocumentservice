package pl.compan.docusafe.core.office.workflow.xpdl;

import org.dom4j.Document;
import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Klasa reprezentuj�ca pakiet proces�w zawarty w pojedynczym pliku XPDL 1.0.
 * <p>
 * Na podstawie przekazanego strumienia tworzony jest DOM (dom4j.org),
 * a nast�pnie odpowiednie fragmenty drzewa DOM przekazywane s� do
 * obiekt�w reprezentuj�cych procesy, parametry etc., kt�re samodzielnie
 * si� na ich podstawie inicjalizuj�.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: XpdlPackage.java,v 1.7 2008/10/06 11:07:08 pecet4 Exp $
 */
public class XpdlPackage
{
    private String id;
    private String name;
    /**
     * Tablica obiekt�w WorkflowProcess indeksowana ich identyfikatorami (id).
     */
    private Map<String, WorkflowProcess> processes = new HashMap<String, WorkflowProcess>();
    /**
     * Tablica rozszerzonych atrybut�w pakietu indeksowana ich nazwami (name).
     */
    private Map<String, ExtendedAttribute> extendedAttributes = new HashMap<String, ExtendedAttribute>();
    /**
     * Unikalny identyfikator tego pakietu sk�adaj�cy si� ze skr�tu tre�ci
     * pliku xpdl (MD5) oraz rozmiaru tego pliku rozdzielonych my�lnikiem.
     */
    private String hashId;

    public String toString()
    {
        return getClass().getName()+"[id="+id+" name="+name+" hashId="+hashId+"]";
    }

    public XpdlPackage(final InputStream stream) throws EdmException
    {
        // tworzenie unikalnego identyfikatora tego pakietu (pole hashId)
        ByteArrayOutputStream cache = new ByteArrayOutputStream(20000);
        try
        {
            byte[] buffer = new byte[1024];
            int length = 0;
            MessageDigest md = MessageDigest.getInstance("MD5");

            int count;
            while ((count = stream.read(buffer)) > 0)
            {
                md.update(buffer, 0, count);
                cache.write(buffer, 0, count);
                length += count;
            }

            byte[] digest = md.digest();
            StringBuilder hash = new StringBuilder(digest.length * 2);
            for (int i=0; i < digest.length; i++)
            {
                if (digest[i] >= 0 && digest[i] < 0x10)
                    hash.append('0');
                hash.append(Integer.toHexString(digest[i] & 0xff));
            }

            hashId = hash.toString() + "-" + Integer.toHexString(length);
        }
        catch (IOException e)
        {
            throw new EdmException("B��d odczytu strumienia", e);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new EdmException("Nie mo�na obliczy� skr�tu strumienia", e);
        }
        finally
        {
            try
            {
                stream.close();
            }
            catch (IOException e)
            {
            }
        }

        // odczytanie i analiza pliku xpdl
        org.dom4j.io.SAXReader reader = new org.dom4j.io.SAXReader();
        try
        {
            Document document = reader.read(new ByteArrayInputStream(cache.toByteArray()));

            this.id = document.getRootElement().attribute("Id").getValue();
            if (id == null)
                throw new EdmException("Brak identyfikatora pakietu.");

            this.name = document.getRootElement().attribute("Name") != null ?
                document.getRootElement().attribute("Name").getValue() : null;

            // definicje proces�w (element opcjonalny)
            Element elWorkflowProcesses = document.getRootElement().
                element("WorkflowProcesses");

            if (elWorkflowProcesses != null)
            {
                List processDefinitions = elWorkflowProcesses.elements("WorkflowProcess");

                for (Iterator procDefIter=processDefinitions.iterator(); procDefIter.hasNext(); )
                {
                    // element XpdlPackage/WorkflowProcesses/WorkflowProcess
                    //processes.add(defineProcess((Element) procDefIter.next()));
                    WorkflowProcess process = new WorkflowProcess(this, (Element) procDefIter.next());
                    processes.put(process.getId(), process);
                }
            }

            // atrybuty rozszerzone (element opcjonalny)
            if (document.getRootElement().element("ExtendedAttributes") != null)
            {
                List elExtendedAttributeList = document.getRootElement().element("ExtendedAttributes").
                    elements("ExtendedAttribute");
                if (elExtendedAttributeList != null && elExtendedAttributeList.size() > 0)
                {
                    for (Iterator iter=elExtendedAttributeList.iterator(); iter.hasNext(); )
                    {
                        Element elExtendedAttribute = (Element) iter.next();
                        ExtendedAttribute ea = new ExtendedAttribute(elExtendedAttribute);
                        extendedAttributes.put(ea.getName(), ea);
                    }
                }
            }
        }
        catch (org.dom4j.DocumentException e)
        {
            throw new EdmException("B��d sk�adniowy w pliku xpdl", e);
        }
    }

    public String getHashId()
    {
        return hashId;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    /**
     * Zwraca definicje wszystkich proces�w zdefiniowanych w pakiecie
     * lub pust� tablic�, je�eli nie zdefiniowano �adnego procesu.
     */
    public WorkflowProcess[] getProcesses()
    {
        return (WorkflowProcess[]) processes.values().toArray(new WorkflowProcess[processes.size()]);
    }

    /**
     * Zwraca proces o ��danym id lub null.
     */
    public WorkflowProcess getProcess(String id)
    {
        return (WorkflowProcess) processes.get(id);
    }

    /**
     * Zwraca rozszerzony atrybut o ��danej nazwie lub null.
     */
    public ExtendedAttribute getExtendedAttribute(String name)
    {
        return (ExtendedAttribute) extendedAttributes.get(name);
    }

    /**
     * Zwraca wszystkie rozszerzone atrybuty zdefiniowane na poziomie
     * pakietu lub pust� tablic�.
     */
    public ExtendedAttribute[] getExtendedAttributes()
    {
        return (ExtendedAttribute[]) extendedAttributes.values().
            toArray(new ExtendedAttribute[extendedAttributes.size()]);
    }

    public static void main(String[] args) throws IOException, EdmException
    {
        XpdlPackage pkg = new XpdlPackage(new FileInputStream(args[0]));

/*
        WorkflowProcess[] processes = pkg.getProcesses();
        for (int i=0; i < processes.length; i++)
        {
            Activity[] activities = processes[i].getActivities();
            Participant[] participants = processes[i].getParticipants();

            print(activities);

            print(participants);
        }
*/

        WorkflowProcess obieg3 = pkg.getProcess("docusafe_1_Wor1");
        print(obieg3.getExtendedAttributes());
        print(pkg.getExtendedAttributes());
        print(obieg3.getDataFields());

    }

    static void print(Object[] array)
    {
        for (int i=0; array != null && i < array.length; i++)
        {
        }
    }
}
