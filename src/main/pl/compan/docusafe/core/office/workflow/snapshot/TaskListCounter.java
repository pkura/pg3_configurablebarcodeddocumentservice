package pl.compan.docusafe.core.office.workflow.snapshot;

import java.util.Map;

import pl.compan.docusafe.core.EdmException;

/**
 * Klasa odpowiedzialna za przechowywanie informacji o ilo�ci zada�
 * uzytkownikow systemu
 */
public abstract class TaskListCounter
{
	protected Map<String,UserCounter> usersCounter;

	/**Ustawia flage nieaktualnosci licznika dla uzytkownika*/
	public abstract void reserUserTaskCount(String username);

	public abstract void initCounter(boolean withBackup);

	/**Pobiera informacje o liscie zadan uzytkownika
	 * @throws EdmException */
	public abstract UserCounter getUserCounter(String username,Long lastDate,boolean withBackup) throws EdmException;
	
	/**Pobiera informacje o liscie zadan uzytkownika danego rodzaju 
	 * @param bookamrkId 
	 * @throws EdmException */
	public abstract Integer getTaskListCount(String username,boolean withBackup,String documentType, Long bookamrkId) throws EdmException;
	/**
	 * metoda zwraca stringa z opilem ilosci poszczegolnych zadan dla uzytkownika np:
	 * "Pism na li�cie zada� - Przychodz�cych 50 / Wychod��cych 287
	 * @param username
	 * @param withBackup
	 * @return
	 * @throws EdmException
	 */
	public abstract String getTaskListUserCount(String username,boolean withBackup) throws EdmException;
	public abstract UserDockindCounter getUserDockindCounter(String username, boolean withBackup, String documentType, Long bookmarkId) throws EdmException;
}
