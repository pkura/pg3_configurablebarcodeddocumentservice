package pl.compan.docusafe.core.office.workflow.snapshot;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

public class TaskPositionBean 
{
	private String user;
	private String division;
	private boolean accept;
	private Integer position;
	
	public String getStringToAjx() throws EdmException
	{
		if(user != null)
			return "Zadanie oczekuje na realizacje na "+ position+
			" miejscu na li�cie u�ytkownika: "+ DSUser.findByUsername(user).asFirstnameLastname()+" /"+
			(division != null ? DSDivision.find(division).getName()+"/" : " ");
		else
			return "Zadanie oczekuje na realizacje na "+ position+
			" miejscu w Departamencie: "+ DSDivision.find(division).getName();		
	}
	
	public boolean isAccept() {
		return accept;
	}
	public void setAccept(boolean accept) {
		this.accept = accept;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
}
