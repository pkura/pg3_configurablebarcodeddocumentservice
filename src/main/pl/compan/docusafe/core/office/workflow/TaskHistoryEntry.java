package pl.compan.docusafe.core.office.workflow;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;


/**
 * Historia listy zada�. Jeden wpis (obiekt) opisuje jednorazowy pobyt jednego dokumentu na li�cie
 * zada� pewnego u�ytkownika (nale��cego do pewnego dzia�u).
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class TaskHistoryEntry
{
    private Long id;
    private Long documentId;
    private String username;
    private String divisionGuid;
    /** data otrzymania zadania na list� (bez godziny) */
    private Date receiveDate;
    /** data akceptacji zadania na list� (bez godziny) */
    private Date acceptDate;
    /** data sko�czenia pracy z zadaniem (bez godziny) - czyli albo zako�czenie pracy z dokumentem albo dekretacja */
    private Date finishDate;
    /** czas otrzymania zadania na list� */
    private Date receiveTime;
    /** czas akceptacji zadania na list� */
    private Date acceptTime;
    /** czas sko�czenia pracy z zadaniem - czyli albo zako�czenie pracy z dokumentem albo dekretacja */
    private Date finishTime;
    private String activityKey;
    private Boolean externalWorkflow;
    
    // konstruktor dla Hibernate
    private TaskHistoryEntry()
    {
        
    }
    
    public TaskHistoryEntry(Long documentId, Date receiveDate, Date receiveTime, String username, String divisionGuid, String activityKey, Boolean externalWorkflow)
    {
        this.documentId = documentId;
        this.receiveDate = receiveDate;
        this.receiveTime = receiveTime;
        // dane pobierane s� z historii pisma wi�c mog�a by� warto�� 'x' oznaczaj�ca brak danych - w�wczas wpisuje tu null
        this.username = "x".equals(username) ? null : username;
        this.divisionGuid = "x".equals(divisionGuid) ? null : divisionGuid;
        this.externalWorkflow = externalWorkflow;
        this.activityKey = activityKey;
    }
    
    public static TaskHistoryEntry findCurrent(Long documentId, String activityKey, Boolean externalWorkflow) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(TaskHistoryEntry.class);

        try
        {
            c.add(org.hibernate.criterion.Restrictions.eq("documentId", documentId));
            if (activityKey != null)
                c.add(org.hibernate.criterion.Restrictions.eq("activityKey", activityKey));
            if (externalWorkflow != null)
                c.add(org.hibernate.criterion.Restrictions.eq("externalWorkflow", externalWorkflow));
            c.add(org.hibernate.criterion.Restrictions.isNull("finishDate"));

//            List list = c.list();
//
//            if (list.size() > 0)
//                return (TaskHistoryEntry) list.get(0);
//            else
                return(TaskHistoryEntry) c.uniqueResult();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static List<TaskHistoryEntry> findAllCurrent(Long documentId, Boolean externalWorkflow) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(TaskHistoryEntry.class);

        try
        {
            c.add(org.hibernate.criterion.Restrictions.eq("documentId", documentId));
            if (externalWorkflow != null)
                c.add(org.hibernate.criterion.Restrictions.eq("externalWorkflow", externalWorkflow));
            c.add(org.hibernate.criterion.Restrictions.isNull("finishDate"));

            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
    
    public Date getAcceptDate()
    {
        return acceptDate;
    }

    public void setAcceptDate(Date acceptDate)
    {
        this.acceptDate = acceptDate;
    }

    public Date getFinishDate()
    {
        return finishDate;
    }

    public void setFinishDate(Date finishDate)
    {
        this.finishDate = finishDate;
    }

    public Date getReceiveDate()
    {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate)
    {
        this.receiveDate = receiveDate;
    }
    
    public Date getAcceptTime()
    {
        return acceptTime;
    }

    public void setAcceptTime(Date acceptTime)
    {
        this.acceptTime = acceptTime;
    }

    public Date getFinishTime()
    {
        return finishTime;
    }

    public void setFinishTime(Date finishTime)
    {
        this.finishTime = finishTime;
    }

    public Date getReceiveTime()
    {
        return receiveTime;
    }

    public void setReceiveTime(Date receiveTime)
    {
        this.receiveTime = receiveTime;
    }

    public String getActivityKey()
    {
        return activityKey;
    }

    public void setActivityKey(String activityKey)
    {
        this.activityKey = activityKey;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public Boolean getExternalWorkflow()
    {
        return externalWorkflow;
    }

    public void setExternalWorkflow(Boolean externalWorkflow)
    {
        this.externalWorkflow = externalWorkflow;
    }       
}
