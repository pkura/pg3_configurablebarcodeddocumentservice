package pl.compan.docusafe.core.office.workflow.jbpm;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;
import org.hibernate.HibernateException;
import org.hibernate.Criteria;

import java.util.List;
/* User: Administrator, Date: 2007-02-14 15:54:27 */

/**
 * "Mapowanie struktury". Jeden obiekt tej klasy mapuje jeden 'swimlane' (reprezentuj�cy pewien dzia�) z JBPM
 * na dzia� w systemie Docusafe.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class SwimlaneMapping
{
    private Long id;
    /**
     * nazwa swimlane'a z JBPM
     */
    private String name;
    /**
     * identyfikator dzia�u z Docusafe
     */
    private String divisionGuid;
    /**
     * Numer ostatniej ostatniej osoby, wybranej w procesie przydzialania os�b do zada�. Osoby s� numerowane
     * od 0 do 'ilosc osob w dziale divisionGuid'-1 wedlug porzadku jaki zwraca funkcja DSDivision.getUsers().
     */
    private Integer lastUserNo;
    /**
     * spos�b dekretacji zada� w tym swimlane ({@link Constants#ASSIGNMENT_GROUP} lub {@link Constants#ASSIGNMENT_USER})
     */
    private Integer assignmentType;
    
    public SwimlaneMapping()
    {

    }

    public synchronized void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException
    {
         Persister.delete(this);
    }        

    public static SwimlaneMapping find(Long id) throws EdmException
    {
        return (SwimlaneMapping) Finder.find(SwimlaneMapping.class, id);
    }

    public static List<SwimlaneMapping> findAll() throws EdmException
    {
        return (List<SwimlaneMapping>) Finder.list(SwimlaneMapping.class, "id");
    }

    public static SwimlaneMapping findByName(String name) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(SwimlaneMapping.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.eq("name", name));

            List list = c.list();
            if (list.size() == 0)
                return null;
            else if (list.size() > 1)
                throw new EdmException("Znaleziono wi�cej ni� jedno mapowanie na dzia� swimlane'a '"+name+"'");
            else
                return (SwimlaneMapping) list.get(0);

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public Integer getLastUserNo()
    {
        return lastUserNo;
    }

    public void setLastUserNo(Integer lastUserNo)
    {
        this.lastUserNo = lastUserNo;
    }

    public Integer getAssignmentType()
    {
        return assignmentType;
    }

    public void setAssignmentType(Integer assignmentType)
    {
        this.assignmentType = assignmentType;
    }    
}
