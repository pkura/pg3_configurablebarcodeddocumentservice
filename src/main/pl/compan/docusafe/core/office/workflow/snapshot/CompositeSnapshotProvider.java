package pl.compan.docusafe.core.office.workflow.snapshot;

import com.google.common.collect.ImmutableList;
import org.hibernate.Session;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.*;

/**
 *  Snapshot provider korzystaj�cy z tabeli DSW_JBPM_TASKLIST i deleguj�cy cz�� zada� do
 *  wielu SnapshotProvider�w
 */
class CompositeSnapshotProvider implements SnapshotProvider{
    private final static Logger LOG = LoggerFactory.getLogger(CompositeSnapshotProvider.class);
    private final List<SnapshotProvider> providers;

    CompositeSnapshotProvider(List<SnapshotProvider> providers) {
        checkArgument(!checkNotNull(providers).isEmpty());
        this.providers = ImmutableList.copyOf(providers);
        LOG.info("initializing CompositeSnapshotProvider with providers:\n{}", providers);
    }

    @Override
    public void updateTaskList(Session session, Map<Long, RefreshRequestBean> requests) {
        for(SnapshotProvider provider: providers){
            provider.updateTaskList(session, requests);
        }
    }

    @Override
    public boolean isAssignedToUser(String userName, Long documentId) throws EdmException {
        for (SnapshotProvider provider : providers) {
            if(provider.isAssignedToUser(userName, documentId)){
                return true;
            }
        }
        return false;
    }

    @Override
    public TaskPositionBean getTaskPosition(String activityKey) {
        return providers.get(0).getTaskPosition(activityKey);
    }
}
