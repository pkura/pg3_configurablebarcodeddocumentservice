package pl.compan.docusafe.core.office.workflow;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfProcess.java,v 1.4 2008/09/09 10:33:21 pecet4 Exp $
 */
public interface WfProcess extends WfExecutionObject
{
    int how_many_step() throws WorkflowException;
    /**
     * Odpowiednik funkcji WfActivity.get_activities_in_state(String)
     * zwracaj�cy tablic� zada� zamiast iteratora.
     */
    WfActivity[] get_activities_in_state_array(String state) throws WorkflowException;
    void start() throws WorkflowException;
    WfProcessMgr manager() throws WorkflowException;

    String getWorkflowName() throws WorkflowException;
}
