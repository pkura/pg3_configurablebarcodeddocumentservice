package pl.compan.docusafe.core.office.workflow.xpdl;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FormalParameter.java,v 1.3 2007/11/27 12:11:38 mariuszk Exp $
 */
public class FormalParameter
{
    private String id;
    private String mode;
    private DataType dataType;
    private String description;

    public FormalParameter(Element elFormalParameter) throws EdmException
    {
        id = elFormalParameter.attribute("Id").getValue();
        if (id == null)
            throw new EdmException("Brak identyfikatora parametru formalnego.");

        mode = elFormalParameter.attribute("Mode") != null ?
            elFormalParameter.attribute("Mode").getValue() : "IN";

        // mode: "IN", "OUT", "INOUT", domy�lnie "IN"
        if (!"IN".equalsIgnoreCase(mode) &&
            !"OUT".equalsIgnoreCase(mode) &&
            !"INOUT".equalsIgnoreCase(mode))
        {
            throw new IllegalArgumentException("Nierozpoznany tryb (mode) " +
                "parametru formalnego procesu "+this.id+": "+mode);
        }

        dataType = DataType.createDataType(elFormalParameter.element("DataType"));
        description = elFormalParameter.element("Description") != null ?
            elFormalParameter.element("Description").getTextTrim() : null;
    }

    public String getId()
    {
        return id;
    }

    public String getMode()
    {
        return mode;
    }

    public DataType getDataType()
    {
        return dataType;
    }

    public String getDescription()
    {
        return description;
    }

    public String toString()
    {
        return "<FormalParameter id="+id+" mode="+mode+" dataType="+dataType+
            " description="+description+">";
    }
}
