package pl.compan.docusafe.core.office.workflow.xpdl;

import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;

import java.util.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ExtendedAttribute.java,v 1.3 2006/02/20 15:42:21 lk Exp $
 */
public class ExtendedAttribute
{
    private String name;
    private String value;
    /**
     * Kluczami tablicy s� nazwy element�w osadzonych wewn�trz ExtendedAttribute,
     * warto�ciami listy (List) obiekt�w utworzonych na ich podstawie.
     */
    private Map<String, List> embeddedElements = new HashMap<String, List>();

    ExtendedAttribute(Element extendedAttribute) throws EdmException
    {
        name = extendedAttribute.attribute("Name").getValue();
        if (name == null)
            throw new EdmException("Brak nazwy atrybutu.");

        value = extendedAttribute.attribute("Value") != null ?
            extendedAttribute.attribute("Value").getValue() : null;

        List elEmbeddedElements = extendedAttribute.elements();
        if (elEmbeddedElements != null && elEmbeddedElements.size() > 0)
        {
            for (Iterator iter=elEmbeddedElements.iterator(); iter.hasNext(); )
            {
                Element element = (Element) iter.next();
                if ("DataType".equals(element.getName()))
                {
                    List<DataType> dataTypes = (List<DataType>) embeddedElements.get("DataType");
                    if (dataTypes == null) dataTypes = new LinkedList<DataType>();
                    dataTypes.add(DataType.createDataType(element));
                    embeddedElements.put("DataType", dataTypes);
                }
                else if ("Description".equals(element.getName()))
                {
                    List<String> descriptions = (List<String>) embeddedElements.get("Description");
                    if (descriptions == null) descriptions = new LinkedList<String>();
                    descriptions.add(element.getTextTrim());
                    embeddedElements.put("Description", descriptions);
                }
                else if ("InitialValue".equals(element.getName()))
                {
                    List<String> initialValues = (List<String>) embeddedElements.get("InitialValue");
                    if (initialValues == null) initialValues = new LinkedList<String>();
                    initialValues.add(element.getTextTrim());
                    embeddedElements.put("InitialValue", initialValues);
                }
            }
        }
    }

    /**
     * Pobiera obiekt opisany elementami znajduj�cymi si� wewn�trz
     * elementu ExtendedAttribute. Je�eli element ExtendedAttribute
     * posiada� wi�cej ni� jeden element o danej nazwie, zwracany
     * jest obiekt utworzony na podstawie pierwszego z nich.
     * <p>
     * Rozpoznawane s� nast�puj�ce nazwy element�w: <ul>
     * <li>"DataType" - obiekty dziedzicz�ce po DataType
     * <li>"Description" - obiekty typu String
     * <li>"InitialValue" - obiekty typu String
     * </ul>
     */
    public Object get(String elementName)
    {
        List objects = (List) embeddedElements.get(elementName);
        if (objects != null && objects.size() > 0)
            return objects.get(0);
        return null;
    }

    public List<Object> getAll(String elementName)
    {
        List<? extends Object> objects = (List<? extends Object>) embeddedElements.get(elementName);
        if (objects != null)
            return Collections.unmodifiableList(objects);
        return null;
    }

    public String getName()
    {
        return name;
    }

    public String getValue()
    {
        return value;
    }

    public String toString()
    {
        return "<ExtendedAttribute name="+name+" value="+value+
            " embedded="+embeddedElements+">";
    }
}
