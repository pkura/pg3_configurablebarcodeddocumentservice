package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RoleNotFoundException.java,v 1.2 2004/06/10 21:21:58 administrator Exp $
 */
public class RoleNotFoundException extends EntityNotFoundException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public RoleNotFoundException(Long roleId)
    {
        super(sm.getString("roleNotFoundException", roleId));
    }
}
