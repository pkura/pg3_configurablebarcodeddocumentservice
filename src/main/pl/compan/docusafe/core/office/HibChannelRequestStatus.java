package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.IntEnumUserType;
import pl.compan.docusafe.core.office.ChannelRequestBean.ChannelRequestStatus;

public class HibChannelRequestStatus extends IntEnumUserType<ChannelRequestStatus> { 
    public HibChannelRequestStatus() { 
        // we must give the values of the enum to the parent.
        super(ChannelRequestStatus.class,ChannelRequestStatus.values()); 
    } 
}
