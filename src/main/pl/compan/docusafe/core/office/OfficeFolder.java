package pl.compan.docusafe.core.office;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.type.Type;

import pl.compan.docusafe.core.AbstractQuery;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.office.container.ContainerFinder;
import pl.compan.docusafe.core.office.container.OfficeFolderFinderProvider;
import pl.compan.docusafe.core.users.DSDivision;

import pl.compan.docusafe.util.FormatUtils;
import pl.compan.docusafe.util.StringManager;
import std.lambda;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OfficeFolder.java,v 1.27 2009/10/29 12:05:39 mariuszk Exp $
 */
public class OfficeFolder extends Container {
	private StringManager sm = GlobalPreferences.loadPropertiesFile(
			OfficeFolder.class.getPackage().getName(), null);
	private String name;
	private String barcode;

    /**
     * Je�li parent jest null, wtedy mamy doczynienia z teczk�,
     * w przeciwnym razie mamy doczynienia z podteczk�.
     */
	private OfficeFolder parent;

	private Date archivedDate;
	/**
	 * Liczba dni na za�atwienie dokumentu w teczce.
	 */
	private Integer days;

	/**
	 * Zbi�r obiekt�w Container (OfficeFolder, kt�re reprezentuj� podteczki lub
	 * OfficeCase).
	 */
	private Set<Container> children;

	OfficeFolder() {
	}

	public OfficeFolder(String name, DSDivision division, Rwa rwa,
			OfficeFolder parent) {
		this.name = StringUtils.left(name, 128);
		setRwa(rwa);
		this.parent = parent;
		setDivisionGuid(division.getGuid());
		getAudit().add(
				Audit.create("creation", DSApi.context().getPrincipalName(),
						sm.getString("UtworzonoTeczke")));
	}

	public String[] suggestOfficeId() throws EdmException {
		return suggestOfficeId(0);
	}

	/**
	 * Zwraca pi�cioelementow� tablic� reprezentuj�c� sugerowany numer
	 * kancelaryjny. Kolejne elementy tablicy oznaczaj�: officeIdPrefix,
	 * officeIdPrefixSeparator, officeIdMiddle, officeIdSuffixSeparator,
	 * OfficeIdSuffix
	 */
	public String[] suggestOfficeId(int year) throws EdmException {
		// elementy odpowiadaj�ce kluczom w tablicy OF_KEYS
		String[] id = new String[5];

		Map<String, Object> context = getFormatContext(year);

		// TODO: pole parent powinno znale�� si� w klasie Container, parent()
		// powinny
		// pozosta� w klasach dziedzicz�cych ze wzgl�du na r�ny typ zwracanych
		// warto�ci
		if (parent != null) {
			context.put("lp", getSequenceId() != null ? getSequenceId()
					: suggestSequenceId());
			context.put("pp", parent.getOfficeIdPrefix());
			context.put("pm", parent.getOfficeIdMiddle());
			context.put("ps", parent.getOfficeIdSuffix());
		}

		Preferences prefs = DSApi.context().systemPreferences()
				.node(NODE_BASE + "/" + DSDivision.ROOT_GUID);

		if (prefs.get(parent == null ? OF_KEYS[2] : OSF_KEYS[2], null) == null)
			throw new EdmException("Nie zdefiniowano formatu numeru teczki");

		// formatowanie
		for (int i = 0; i < id.length; i++) {
			String format = prefs.get(
					parent == null ? OF_KEYS[i] : OSF_KEYS[i], null);
			if (format == null) {
				id[i] = "";
			}
			// dla drugiego i przedostatniego elementu formatowanie nie
			// jest potrzebne (s� to separatory)
			else if (i == 1 || i == 3) {
				id[i] = format;
			} else {
				id[i] = FormatUtils.formatExpression(format, context);
			}
		}

		return id;
	}

	public Integer suggestSequenceId() throws EdmException {
		return suggestSequenceId(parent != null ? parent.getId() : null);
	}

	/**
	 * Zwraca true, je�eli podany numer kolejny podteczki jest dost�pny. Metoda
	 * nie powinna by� wywo�ywana dla teczek najwy�szego poziomu (parent ==
	 * null); rzuca w�wczas wyj�tek EdmException.
	 * 
	 * @throws EdmException
	 */
	public boolean sequenceIdAvailable(Integer sequenceId) throws EdmException {
		// teczki najwy�szego poziomu maj� nadawane kolejne id tylko ze wzgl�du
		// na istnienie unikalnego indeksu na kolumnach (parent_id, sequenceId),
		// u�ytkownik nie ma mo�liwo�ci dowolnego wyboru tych identyfikator�w
		if (parent == null)
			throw new EdmException(
					"Metoda sequenceIdAvailable() nie powinna by� wywo�ywana "
							+ "dla teczek najwy�szego poziomu.");

		return sequenceIdAvailable(parent.getId(), sequenceId);
	}

	public void delete() throws EdmException {
		if (getChildren().size() > 0)
			throw new EdmException(
					"Nie mo�na usun�� teczki zawieraj�cej podteczki lub sprawy");
		getAudit().clear();
		Persister.delete(this);
	}

	/**
	 * Znajduje wszystkie teczki najwy�szego poziomu przypisane do ��danego
	 * dzia�u.
	 * 
	 * @param divisionGuid
	 * @return
	 * @throws EdmException
	 */
	public static List<OfficeFolder> findByDivisionGuid(String divisionGuid) throws EdmException {
		try {
			return DSApi.context().classicSession()
					.find("from fl in class "
							+ OfficeFolder.class.getName()
							+ " where fl.parent.id is null and fl.divisionGuid = ?"
							+ " order by fl.year desc, fl.officeId asc",
							divisionGuid, Hibernate.STRING);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

    /**
     * Znajduje wszystkie teczki najwy�szego poziomu przypisane do ��danego dzia�u.
     * Teczki tylko te ktore nie  maja status nie zarchiwizowane lub nie przydzielony.
     * Funkcja uzywana w PortfolioAction do wy�wietlenia teczek

     * @param divisionGuid
     * @return
     * @throws EdmException
     */
    public static List<OfficeFolder> getByDivisionGuidToShowOnView(String divisionGuid) throws EdmException {
        try {
            return DSApi.context().classicSession()
                    .find("from fl in class "
                            + OfficeFolder.class.getName()
                            + " where fl.parent.id is null and fl.divisionGuid = ? "
                            + " and (fl.archiveStatus is null OR fl.archiveStatus in (" +
                             ArchiveStatus.None.ordinal() +", " + ArchiveStatus.ToArchive.ordinal()+ "))"
                            + " order by fl.year desc, fl.officeId asc",
                            divisionGuid, Hibernate.STRING);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Znajduje wszystkie teczki o danym kodzie kreskowym
     *
     * @param barcode
     * @return
     * @throws EdmException
     */
    public static List<OfficeFolder> findByBarcode(String barcode)
            throws EdmException {
        try {
            Criteria criteria = DSApi.context().session()
                    .createCriteria(OfficeFolder.class);
            criteria.add(Restrictions.eq("barcode", barcode)
                    .ignoreCase());
            return criteria.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

	/**
	 * Znajduje wszystkie teczki najwy�szego poziomu przypisane do ��danego
	 * dzia�u, od podanego roku wzwy�.
	 * 
	 * @param divisionGuid
	 * @return
	 * @throws EdmException
	 */
	public static List<OfficeFolder> findByDivisionGuidAndYear(
			String divisionGuid, Integer minYear) throws EdmException {
		try {
			return DSApi.context().classicSession()
					.find("from fl in class "
							+ OfficeFolder.class.getName()
							+ " where fl.parent.id is null and fl.divisionGuid = ? and fl.year >= ?"
							+ " order by fl.year desc, fl.officeId asc",
							new Object[] { divisionGuid, minYear },
							new Type[] { Hibernate.STRING, Hibernate.INTEGER });
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	/**
	 * Znajduje wszystkie teczki najwy�szego poziomu przypisane do ��danego
	 * dzia�u z danego roku.
	 * 
	 * @param divisionGuid
	 * @return
	 * @throws EdmException
	 */
	public static List<OfficeFolder> findByDivisionGuidYear(
			String divisionGuid, Integer exactYear) throws EdmException {
		try {
			return DSApi.context().classicSession()
					.find("from fl in class "
							+ OfficeFolder.class.getName()
							+ " where fl.parent.id is null and fl.divisionGuid = ? and fl.year = ?"
							+ " order by fl.year desc, fl.officeId asc",
							new Object[] { divisionGuid, exactYear },
							new Type[] { Hibernate.STRING, Hibernate.INTEGER });
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static List<OfficeFolder> findAll() throws EdmException {
		return Finder.list(OfficeFolder.class, null);
	}

	public static OfficeFolder find(Long id) throws EdmException {
		return (OfficeFolder) Finder.find(OfficeFolder.class, id);
	}

	public static OfficeFolder findByOfficeIdMiddle(String officeIdMiddle)
			throws EdmException {
		try {
			Criteria criteria = DSApi.context().session()
					.createCriteria(OfficeFolder.class);
			criteria.add(Restrictions.eq("officeIdMiddle", officeIdMiddle)
					.ignoreCase());
			List list = criteria.list();
			if (list.size() > 0) {
				return (OfficeFolder) list.get(0);
			}
			throw new EdmException("Brak teczki o podanym kodzie RWA");
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	/**
	 * Tworzy kopi� ka�dej teczki z nowym numerem roku. U�ywane podczas
	 * otwierania nowego roku.
	 *  
	 */
	public static void newYearFolders(int year) throws EdmException, SQLException {
		try {
			List rootFolders = DSApi.context().classicSession()
					.find("from fl in class " + OfficeFolder.class.getName()
							+ " where fl.parent.id is null");

			for (Iterator iter = rootFolders.iterator(); iter.hasNext();) {
				OfficeFolder rootFolder = (OfficeFolder) iter.next();
				/*
				 * Zamykamy tylko teczki z roku poprzedniego. TODO - zrobic aby
				 * to sie wybieralo w zapytaniu
				 */
				if (rootFolder.getYear().intValue() == year - 1) {
					/** Nie kopiujemy je�li jest nieaktywna */
					if (rootFolder.getRwa().getIsActive()) {
						OfficeFolder newFolder = new OfficeFolder(
								rootFolder.getName(),
								DSDivision.find(rootFolder.getDivisionGuid()),
								rootFolder.getRwa(), null);
						newFolder.setClerk(rootFolder.getClerk());
						newFolder.setSequenceId(rootFolder.getSequenceId());
						newFolder.setYear(new Integer(year));

						newFolder.create(rootFolder.getOfficeId(),
								rootFolder.getOfficeIdPrefix(),
								rootFolder.getOfficeIdMiddle(),
								rootFolder.getOfficeIdSuffix());

						createSubfolders(newFolder, rootFolder.getSubfolders(),
								year);
					}
				}
			}
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

    protected String getRegexp() {
        Preferences prefs = DSApi.context().systemPreferences()
                .node(NODE_BASE + "/" + DSDivision.ROOT_GUID);

        // teczka
        if(parent == null) {
            return prefs.get(Container.OF_REGEXP, null);
        }
        // podteczka
        else {
            return prefs.get(Container.OSF_REGEXP, null);
        }
    }

    @Override
    protected String getFormatErrorMessage(String officeId) {
        return "B��dny format numeru teczki: "+officeId;
    }

    /**
	 * Rekurencyjnie tworzy podteczki na podstawie listy przekazanych podteczek
	 * (otwieranie nowego roku).
	 * 
	 * Dost�p z 'private' na 'public' zmieniono ze wzgl�du na awaryjne
	 * przenoszenie teczki na nowy rok z formatki edycji teczki.
	 * 
	 */
	public static void createSubfolders(OfficeFolder folder,
			List oldSubfolders, int year) throws EdmException, SQLException {
		for (Iterator iter = oldSubfolders.iterator(); iter.hasNext();) {

			OfficeFolder subfolder = (OfficeFolder) iter.next();
			/** Nie kopiujemy je�li jest nieaktywna */
			if (subfolder.getRwa().getIsActive()) {
				OfficeFolder newFolder = new OfficeFolder(subfolder.getName(),
						DSDivision.find(subfolder.getDivisionGuid()),
						subfolder.getRwa(), folder);
				newFolder.setClerk(subfolder.getClerk());
				newFolder.setSequenceId(subfolder.getSequenceId());
				newFolder.setYear(new Integer(year));

				newFolder.create(subfolder.getOfficeId(),
						subfolder.getOfficeIdPrefix(),
						subfolder.getOfficeIdMiddle(),
						subfolder.getOfficeIdSuffix());

				createSubfolders(newFolder, subfolder.getSubfolders(), year);
			}
		}
	}

	public boolean isSubFolder() {
		return parent != null;
	}

	public List<OfficeFolder> getSubfolders() {
		if (getChildren() == null)
			return Collections.emptyList();

		List<OfficeFolder> results = new LinkedList<OfficeFolder>();
		for (Container c : getChildren()) {
			if (c instanceof OfficeFolder)
				results.add((OfficeFolder) c);
		}
		return results;
	}

	public void initializeChildrenRecursive() throws EdmException {
		DSApi.initializeProxy(getChildren());
		if (getChildren() != null) {
			for (Iterator iter = getChildren().iterator(); iter.hasNext();) {
				Container c = (Container) iter.next();
				if (c instanceof OfficeFolder) {
					((OfficeFolder) c).initializeChildrenRecursive();
				}
			}
		}
	}

	public void initializeChildrenRecursive(List selected) throws EdmException {
		DSApi.initializeProxy(getChildren());
		if (getChildren() != null) {
			for (Iterator iter = getChildren().iterator(); iter.hasNext();) {
				Container c = (Container) iter.next();
				if ((c instanceof OfficeFolder)
						&& (selected.contains(((OfficeFolder) c).getId()))) {
					((OfficeFolder) c).getRwa();
					((OfficeFolder) c).initializeChildrenRecursive();
				}
			}
		}
	}

	protected String getCreateAuditDescription() {
		return "Utworzono " + (isSubFolder() ? "pod" : "") + "teczk� "
				+ getOfficeId();
	}

	public void notifyCaseAdded(OfficeCase officeCase) throws EdmException {
		getAudit().add(
				Audit.create(
						"children",
						DSApi.context().getPrincipalName(),
						sm.getString("DoTeczkiDodanoSprawe",
								officeCase.getOfficeId()),
						officeCase.getOfficeId(), officeCase.getId()));
	}

	public void notifyCaseRemoved(OfficeCase officeCase) throws EdmException {
		getAudit().add(
				Audit.create(
						"children",
						DSApi.context().getPrincipalName(),
						sm.getString("ZteczkiUsunietoSprawe",
								officeCase.getOfficeId()),
						officeCase.getOfficeId(), officeCase.getId()));
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public OfficeFolder getParent() {
		return parent;
	}

	public void setParent(OfficeFolder parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

    public void setNameNoCheck(String name) {
        this.name = name;
    }

	public void setName(String name) throws EdmException {
		if (this.name != null && !this.name.equals(name)) {
			getAudit()
					.add(Audit.create("name", DSApi.context()
							.getPrincipalName(), sm.getString(
							"ZmienionoNazweTeczkiNa", name), name));
		}

		this.name = name;
	}

	public Set<? extends Container> getChildren() {
		return children;
	}

	public void setChildren(Set<Container> children) {
		this.children = children;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
        this.days = days;
	}
	
	public static Query query(int offset, int limit)
    {
        return new Query(offset, limit);
    }

    private static final lambda<OfficeFolder, OfficeFolder> identityMapper =
        new lambda<OfficeFolder, OfficeFolder>()
        {
            public OfficeFolder act(OfficeFolder o)
            {
                return o;
            }
        };



    public static SearchResults<OfficeFolder> search(Query query) throws EdmException
    {
        ContainerFinder folderSearch = null;
        try {
            folderSearch = OfficeFolderFinderProvider.instance().provide(query, identityMapper, OfficeFolder.class);
        } catch (Exception e) {
            throw new EdmException("Wyst�pi� b��d, skontaktuj si� z administratorem", e);
        }
        return folderSearch.find();
    }

    /**
     * Zwraca obiekty typu OfficeFolder.
     * @param mapper Obiekt mapuj�cy zwracane obiekty OfficeClass na typ T. Zwracany
     *  obiekt SearchResults zawiera obiekty zwr�cone przez mapper.
     */

    public static <T> SearchResults<T> search2(Query query2, lambda<OfficeFolder, T> mapper,
            Class<T> mappedClass) throws EdmException
	{
	    List<T> results = new ArrayList<T>(query2.limit);
	      
	    return new SearchResultsAdapter<T>(results, query2.offset, query2.limit, mappedClass);
	}


    public static class Query extends AbstractQuery
    {
        private int offset;
        private int limit;
        private String name;
        private Integer seqNum;
        private Integer year;
        private Date cdateFrom;
        private Date cdateTo;
        private String[] clerk;
        private String[] author;
        private Integer[] record;
        private Integer rwaId;
        private String officeId;
        private Boolean archived;
        private Date archDateFrom;
        private Date archDateTo;
        private Integer archiveStatus;

		public Query(int offset, int limit)
        {
            this.offset = offset;
            this.limit = limit;
        }

        protected boolean validSortField(String field)
        {
            return "officeId".equals(field);
        }

        public void setRecord(Integer[] record)
        {
            this.record = record;
        }

        public void setCdateFrom(Date cdateFrom)
        {
            this.cdateFrom = cdateFrom;
        }

        public void setCdateTo(Date cdateTo)
        {
            this.cdateTo = cdateTo;
        }

        public void setSeqNum(Integer seqNum)
        {
            this.seqNum = seqNum;
        }

        public void setYear(Integer year)
        {
            this.year = year;
        }

        public void setArchived(Boolean archived)
        {
            this.archived = archived;
        }

        public void setClerk(String[] clerk)
        {
            this.clerk = clerk;
        }

        public void setAuthor(String[] author)
        {
            this.author = author;
        }

        public void setRwaId(Integer rwaId)
        {
            this.rwaId = rwaId;
        }

        public void setOfficeId(String officeId)
        {
            this.officeId = officeId;
        }

		public void setName(String name) {
			this.name = name;
		}

        public Date getArchDateTo() {
			return archDateTo;
		}

		public void setArchDateTo(Date archDateTo) {
			this.archDateTo = archDateTo;
		}

		public Date getArchDateFrom() {
			return archDateFrom;
		}

		public void setArchDateFrom(Date archDateFrom) {
			this.archDateFrom = archDateFrom;
		}
		public void setArchiveStatus(int archiveStatus) {
			this.archiveStatus = archiveStatus;
		}
		public void setArchiveStatusIsArchived(){
			//po prostu jest archiwum czyli potem status >=2
			this.archiveStatus=-1;
		}
		public void setArchiveStatusIsNotArchived(){
			//po prostu jest archiwum czyli potem status >=2
			this.archiveStatus=-2;
		}

        public int getOffset() {
            return offset;
        }

        public int getLimit() {
            return limit;
        }

        public String getName() {
            return name;
        }

        public Integer getSeqNum() {
            return seqNum;
        }

        public Integer getYear() {
            return year;
        }

        public Date getCdateFrom() {
            return cdateFrom;
        }

        public Date getCdateTo() {
            return cdateTo;
        }

        public String[] getClerk() {
            return clerk;
        }

        public String[] getAuthor() {
            return author;
        }

        public Integer[] getRecord() {
            return record;
        }

        public Integer getRwaId() {
            return rwaId;
        }

        public String getOfficeId() {
            return officeId;
        }

        public Boolean getArchived() {
            return archived;
        }

        public Integer getArchiveStatus() {
            return archiveStatus;
        }
    }
    public static OfficeFolder findByOfficeIdLike(String officeId) throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(OfficeFolder.class);
            crit.add(org.hibernate.criterion.Expression.ilike("officeId", "%" + officeId + "%"));
            List folders = crit.list();

            if (folders.size() > 0)
            {
                return (OfficeFolder) folders.get(0);
            }

            throw new CaseNotFoundException(officeId);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static OfficeFolder findByOfficeId(String officeId) throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(OfficeFolder.class);
            crit.add(org.hibernate.criterion.Expression.eq("officeId", officeId));
            List folders = crit.list();

            if (folders.size() > 0)
            {
                return (OfficeFolder) folders.get(0);
            }

            throw new CaseNotFoundException(officeId);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

	public Date getArchivedDate() {
		return archivedDate;
	}

	public void setArchivedDate(Date archivedDate) {
		this.archivedDate = archivedDate;
	}

	public String getDivisionName()
	{
		return getName();
	}

}
