package pl.compan.docusafe.core.office;

import com.google.common.base.Strings;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.dialect.FirebirdDialect;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.Query;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.select.*;

import java.io.Serializable;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Dziennik przychodz�cych i wychodz�cych dokument�w.
 *
 * TODO: doda� pole okre�laj�ce rok? oddzielny dziennik dla ka�dego roku?
 *
 * TODO: pole okre�laj�ce otwart� dat� w dzienniku?
 * 
 * TODO: typ dziennika w metodach find() zast�pi� dokumentem
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Journal.java,v 1.120 2010/07/29 11:00:16 mariuszk Exp $
 */
public class Journal implements Serializable, Cloneable, Lifecycle
{
    private static final Log log = LogFactory.getLog(Journal.class);
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public static final String INCOMING = "incoming";
    public static final String OUTGOING = "outgoing";
    public static final String INTERNAL = "internal";

	
    private Long id;
    private String description;
    /**
     * GUID dzia�u (Division), z kt�rym zwi�zany jest dziennik.
     * Dziennik g��wny ma w tym polu NULL. Dla ka�dego rodzaju dziennika
     * mo�e istnie� tylko jeden dziennik g��wny.
     */
    private String ownerGuid;
    /**
     * Jedna z warto�ci {@link #INCOMING}, {@link #OUTGOING}.
     */
    private String journalType;
    /**
     * Numer, jaki powinien zosta� nadany nast�pnemu pismu
     * w tym dzienniku.
     */
    private Integer sequenceId;
    private Integer dailySequenceId;
    private Date day;
    private Integer hversion;

    private String symbol;
    private String lparam;
    private Long wparam;
    private String author;
    private Date ctime;
    private boolean archived;
    private boolean closed;
    private int cyear;

    /**
     * Konstruktor u�ywany przez Hibernate.
     */
    Journal()
    {
    }

    public Journal(String description, String ownerGuid, String journalType)
    {
        this.description = description;
        this.ownerGuid = ownerGuid;
        this.journalType = journalType;
        this.sequenceId = new Integer(1);
    }

    public static Integer bindToJournal(OfficeDocument document, Journal journal) throws EdmException {
        Long journalId = journal.getId();
        Integer sequenceId = Journal.TX_newEntry2(journalId, document.getId(), Calendar.getInstance().getTime());

        document.bindToJournal(journalId, sequenceId);

        return sequenceId;
    }

    /** Klasa uzywana do zworenia wynikow w wyszukiwarce dziennikow */
    public static class EntryBean
    {
    	private final static int RECIPIENT_USER_NAME_INDEX = 37;
    	private final static int RECIPIENT_USER_LASTNAME_INDEX = 38;
    	private final static int RECIPIENT_USER_FIRSTNAME_INDEX = 39;
    	private final static int MASTER_DOCUMENT_ID_INDEX = 40;
    	
        private Long documentId;
        private Date incomingDate;
        private Date documentDate;
        private String officeNumber;
        private String referenceId;
        private String summary;
        private String senderSummary;
        private String recipientSummary;
        private String recipientName;
        private String recipientAdress;
        private Map recipientUser;
        private Long masterDocumentId;
        private String divisionName;
        private String currentAssignmentUsername;
        private Integer sequenceId;
        private boolean internal;
        private int numAttachments;
        private String creatingUser;
        private String userName;
        private String postalregnumber;
        private String delivery;
        private String weightKg;
        private String weightG;
        private String stampFeeZl;
        private String stampFeeGr;
        private String caseSign;
        private String remarks;
       

        public String getPostalregnumber() {
			return postalregnumber;
		}

		public EntryBean(Long documentId, 
        		Date incomingDate, 
        		Date documentDate, 
        		String officeNumber, 
        		String referenceId, 
        		String summary, 
        		String senderSummary, 
        		String recipientSummary, 
        		Map<String, String> recipientUser,
        		Long masterDocumentId,
        		String divisionName, 
        		String currentAssignmentUsername, 
        		Integer sequenceId, 
        		boolean internal, 
        		int numAttachments, 
        		String creatingUser, 
        		String userName,
        		String postalregnumber,
        		String delivery,
        		String caseSign,
        		String recipientAdress,
        		String recipientName,
        		String remarks,   
        		String stampFeeZl,
        		String stampFeeGr,
        		String weightKg,
        		String weightG
				){
        	
            this.documentId = documentId;
            this.incomingDate = incomingDate;
            this.documentDate = documentDate;
            this.officeNumber = officeNumber;
            this.referenceId = referenceId;
            this.summary = summary;
            this.senderSummary = senderSummary;
            this.recipientSummary = recipientSummary;
            this.recipientUser = recipientUser;
            this.masterDocumentId = masterDocumentId;
            this.divisionName = divisionName;
            this.currentAssignmentUsername = currentAssignmentUsername;
            this.sequenceId = sequenceId;
            this.internal = internal;
            this.numAttachments = numAttachments;
            this.creatingUser = creatingUser;
            this.userName = userName;
            this.postalregnumber = postalregnumber;
            this.delivery = delivery;
            this.caseSign = caseSign;
            this.recipientAdress = recipientAdress;
            this.recipientName = recipientName;               
            this.remarks = remarks;  
            this.stampFeeZl = stampFeeZl;  
            this.stampFeeGr = stampFeeGr;  
            this.weightKg = weightKg;  
            this.weightG = weightG;  
            this.caseSign = caseSign;
        }
        
        public static class DivisionNameComparator implements Comparator <EntryBean> {
        	private boolean ascending;
        	
        	public DivisionNameComparator(boolean ascending) {
        		this.ascending = ascending;
        	}
        	
			public int compare(EntryBean arg0, EntryBean arg1) {

			    if (arg0.divisionName == null && arg1.divisionName == null) return 0;
			    if (arg0.divisionName != null && arg1.divisionName == null) return -1;
			    if (arg0.divisionName == null && arg1.divisionName != null) return 1;
			    if (ascending)
			    	return arg0.divisionName.compareToIgnoreCase(arg1.divisionName);
			    else
			    	return arg1.divisionName.compareToIgnoreCase(arg0.divisionName);
			}
        	
        }

        public String toString()
        {
            return "Entry[id="+documentId+" incomingDate="+incomingDate+
                " documentDate="+documentDate+" ko="+officeNumber+
                " referenceId="+referenceId+" summary="+summary+
                " sender="+senderSummary+" recipient="+recipientSummary+
                " assignmentUsername="+currentAssignmentUsername+
                " division="+divisionName+" sequenceId="+sequenceId+
                " creatingUser="+creatingUser+"]";
        }

        public Long getDocumentId()
        {
            return documentId;
        }

        public Date getIncomingDate()
        {
            return incomingDate;
        }

        public Date getDocumentDate()
        {
            return documentDate;
        }

        public String getOfficeNumber()
        {
            return officeNumber;
        }

        public String getReferenceId()
        {
            return referenceId;
        }

        public String getSenderSummary()
        {
            return senderSummary;
        }

        public String getRemarks() {
			return remarks;
		}

		public void setRemarks(String remarks) {
			this.remarks = remarks;
		}

		public String getSummary()
        {
            return summary;
        }

        public String getRecipientSummary()
        {
            return recipientSummary;
        }

        public String getDivisionName()
        {
            return divisionName;
        }

        public String getCurrentAssignmentUsername()
        {
            return currentAssignmentUsername;
        }

        public Integer getSequenceId()
        {
            return sequenceId;
        }

        public boolean isInternal()
        {
            return internal;
        }              
       

        public String getRecipientName() {
			return recipientName;
		}

		public void setRecipientName(String recipientName) {
			this.recipientName = recipientName;
		}

		public String getRecipientAdress() {
			return recipientAdress;
		}

		public void setRecipientAdress(String recipientAdress) {
			this.recipientAdress = recipientAdress;
		}

		public String getCaseSign() {
			return caseSign;
		}

		public void setCaseSign(String caseSign) {
			this.caseSign = caseSign;
		}

		public int getNumAttachments()
        {
            return numAttachments;
        }

        public String getCreatingUser()
        {
            return creatingUser;
        }

        public String getUserName()
        {
            return userName;
        }

        public void setUserName(String userName)
        {
            this.userName = userName;
        }        
                
        public String getWeightKg() throws Exception {
			return weightKg;
		}

		public void setWeightKg(String weightKg) {
			this.weightKg = weightKg;
		}

		public String getWeightG() throws Exception 
		{
			return weightG;
		}

		public void setWeightG(String weightG) {	
		}		

		public String getStampFeeZl() throws Exception
		{			
			return stampFeeZl;

		}
		public void setStampFeeZl(String stampFeeZl) {
			this.stampFeeZl = stampFeeZl;
		}

		public String getStampFeeGr() throws Exception
		{
			return stampFeeGr;
		}

		public void setStampFeeGr(String stampFeeGr) {
			this.stampFeeGr = stampFeeGr;
		}

		public static class EntryBeanComparatorByDivision implements Comparator<EntryBean>
        {
            public int compare(EntryBean e1, EntryBean e2)
            {
            	if(e1.getDivisionName() != null)
            		return e1.getDivisionName().compareTo(e2.getDivisionName());
            	else
            		return 0;
            }
        }

		public String getDelivery() {
			return delivery;
		}

		public void setDelivery(String delivery) {
			this.delivery = delivery;
		}

		public Map<String, String> getRecipientUser() {
			return recipientUser;
		}

		public void setRecipientUser(Map<String, String> recipientUser) {
			this.recipientUser = recipientUser;
		}

		public Long getMasterDocumentId() {
			return masterDocumentId;
		}

		public void setMasterDocumentId(Long masterDocumentId) {
			this.masterDocumentId = masterDocumentId;
		}
    }

    /** Klasa przechowujaca kryteria wyszukiwania dla wyszukiwarki dziennikow  */
    public static class JournalQuery extends Query
    {
        private Long journalId;
        private Boolean zpo;
        private List<Range> sequenceRanges;
        private boolean sortByRecipientLocation;
        private boolean sortByRegNumber;
        private Integer[] deliveryIds;
        private Integer[] incomingKinds;
        private String summary;
        private String sender;
        private String recipientFirstname;
        private String recipientLastname;
        private String recipientOrganization;
        private String recipient;    
        private String recipientUser;
        private String Author;
        private String authorDivision;
        private Date day;
        private Date fromDay;
        private Date toDay;
        private Integer fromNum;
        private Integer toNum;
        private Integer fromNumD;
        private Integer toNumD;
        private List<Long> numList;
        private Integer limit;
        private Integer offset;
        private Integer totalCount;
        private String orderBy;
        private Boolean ascending;
        private String postalregnumber;
        private String creatingEntry;
        private Boolean czyAktualny;
        private Integer weigthFrom;
        private Integer weigthTo;//integer.max == niesko�czono��
        
      


        public String getPostalregnumber() {
			return postalregnumber;
		}

		public void setPostalregnumber(String postalregnumber) {
			this.postalregnumber = postalregnumber;
		}

		/**
		 * @return the ascending
		 */
		public Boolean getAscending() {
			return ascending;
		}

		/**
		 * @param ascending the ascending to set
		 */
		public void setAscending(Boolean ascending) {
			this.ascending = ascending;
		}

		/**
		 * @return the orderBy
		 */
		public String getOrderBy() {
			return orderBy;
		}

		/**
		 * @param orderBy the orderBy to set
		 */
		public void setOrderBy(String orderBy) {
			this.orderBy = orderBy;
		}

		public Integer getTotalCount()
        {
            return totalCount;
        }

        public void setTotalCount(Integer totalCount)
        {
            this.totalCount = totalCount;
        }

        public Integer getLimit()
        {
            return limit;
        }

        public void setLimit(Integer limit)
        {
            this.limit = limit;
        }
        
        public Integer getOffset()
        {
            return offset;
        }

        public void setOffset(Integer offset)
        {
            this.offset = offset;
        }

        public void setDeliveryIds(Integer[] deliveryIds)
        {
            this.deliveryIds = deliveryIds;
        }

        public void setIncomingKinds(Integer[] incomingKinds)
        {
            this.incomingKinds = incomingKinds;
        }

        public void setJournalId(Long journalId)
        {
            this.journalId = journalId;
        }

        public void setNumList(List<Long> numList)
        {
            this.numList = numList;
        }

        public void setRecipient(String recipient)
        {
            this.recipient = recipient;
        }

        public void setSender(String sender)
        {
            this.sender = sender;
        }

        public void setSortByRecipientLocation(boolean sortByRecipientLocation)
        {
            this.sortByRecipientLocation = sortByRecipientLocation;
        }
        public void setSortByRegNumber(boolean sortByRegNumber){
        	this.sortByRegNumber=sortByRegNumber;
        }
        public String getAuthor()
        {
            return Author;
        }

        public void setAuthor(String author)
        {
            Author = author;
        }

        public void setSummary(String summary)
        {
            this.summary = summary;
        }


        public JournalQuery(Long journalId)
        {
            this.journalId = journalId;
        }

        public JournalQuery(Long journalId,int limit, int offset)
        {
            this.journalId = journalId;
            this.limit = limit;
            this.offset = offset;
        }

        List<Range> getSequenceRanges()
        {
            return sequenceRanges;
        }

        boolean isSortByRecipientLocation()
        {
            return sortByRecipientLocation;
        }

        Long getJournalId()
        {
            return journalId;
        }

        Integer[] getDeliveryIds()
        {
            return deliveryIds;
        }

        Integer[] getIncomingKinds()
        {
            return incomingKinds;
        }

        String getSummary()
        {
            return summary;
        }

        String getSender()
        {
            return sender;
        }

        String getRecipient()
        {
            return recipient;
        }

        public JournalQuery()
        {
            super();

        }

        public void setSequenceRanges(List<Range> sequenceRanges)
        {
            this.sequenceRanges = sequenceRanges;
        }

        /**
         * Tylko dla dokument�w przychodz�cych.
         */
        public void deliveries(Integer[] deliveryIds)
        {
            this.deliveryIds = deliveryIds;
        }

        /**
         * Tylko dla dokument�w przychodz�cych.
         */
        public void incomingKinds(Integer[] kindIds)
        {
            incomingKinds = kindIds;
        }

/*
        private void addLikeTokens(String s)
        {
            JournalQuery or = new JournalQuery();
            String[] tokens = s.split("\\s+\\s");
            for (int i=0; i < tokens.length; i++)
            {
                if (tokens[i].length() > 0)
                    or.addLike("sender", "%"+tokens[i]+"%");
            }
            addDisjunction(or);
        }
*/


        public void summary(String summary)
        {
            this.summary = summary;
        }

        /**
         * Tylko dla dokument�w przychodz�cych.
         */
        public void sender(String sender)
        {
            this.sender = sender;
        }

        public void recipient(String recipient)
        {
            this.recipient = recipient;
        }

        public void sortByLocation(boolean b)
        {
            this.sortByRecipientLocation = b;
        }
        public void sortByRegNumber(boolean b){
        	this.sortByRegNumber = b;
        }
       
        protected boolean validSortField(String field)
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public String getAlias(TableAlias tableAlias, String columnName)
        {
            // TODO Auto-generated method stub
            return null;
        }
        
        public Date getDay()
        {
            return day;
        }

        public void setDay(Date day)
        {
            this.day = day;
        }

        public Date getFromDay()
        {
            return fromDay;
        }

        public void setFromDay(Date fromDay)
        {
            this.fromDay = fromDay;
        }

        public Date getToDay()
        {
            return toDay;
        }

        public void setToDay(Date toDay)
        {
            this.toDay = toDay;
        }

        public Integer getFromNum()
        {
            return fromNum;
        }

        public void setFromNum(Integer fromNum)
        {
            this.fromNum = fromNum;
        }

        public List<Long> getNumList()
        {
            return numList;
        }

        public void setNumList(String num)
        {
            sequenceRanges = new ArrayList<Range>();
            if(num!=null )
            {
                num = num.trim();
                String[] t1 = num.split(",");
            
                for(int i=0;i< t1.length;i++)
                {
                    String[] t2=t1[i].split("-");
                    if(t2.length>1)
                    {
                        Range range = new Range(t2[0],t2[1]);
                        sequenceRanges.add(range);
                    }
                    else
                    {
                        Range range = new Range(t2[0],t2[0]);
                        sequenceRanges.add(range);
                    }
                }
            }
        }

        public Integer getToNum()
        {
            return toNum;
        }

        public void setToNum(Integer toNum)
        {
            this.toNum = toNum;
        }

		public boolean isSortByRegNumber() {
			return sortByRegNumber;
		}

		public Integer getFromNumD() {
			return fromNumD;
		}

		public void setFromNumD(Integer fromNumD) {
			this.fromNumD = fromNumD;
		}

		public Integer getToNumD() {
			return toNumD;
		}

		public void setToNumD(Integer toNumD) {
			this.toNumD = toNumD;
		}

		public String getRecipientFirstname() {
			return recipientFirstname;
		}

		public void setRecipientFirstname(String recipientFirstname) {
			this.recipientFirstname = recipientFirstname;
		}

		public String getRecipientLastname() {
			return recipientLastname;
		}

		public void setRecipientLastname(String recipientLastname) {
			this.recipientLastname = recipientLastname;
		}

		public String getRecipientOrganization() {
			return recipientOrganization;
		}

		public void setRecipientOrganization(String recipientOrganization) {
			this.recipientOrganization = recipientOrganization;
		}

		public String getRecipientUser() {
			return recipientUser;
		}

		public void setRecipientUser(String recipientUser) {
			this.recipientUser = recipientUser;
		}

		public String getAuthorDivision() {
			return authorDivision;
		}

		public void setAuthorDivision(String authorDivision) {
			this.authorDivision = authorDivision;
		}

		public String getCreatingEntry() {
			return creatingEntry;
		}

		public void setCreatingEntry(String creatingEntry) {
			this.creatingEntry = creatingEntry;
		}

		public Boolean getZpo() {
			return zpo;
		}

		public void setZpo(Boolean zpo) {
			this.zpo = zpo;
		}
		
		public Boolean getCzyAktualny() {
			return czyAktualny;
		}
		
		public void setCzyAktualny(Boolean czyAktualny) {
			this.czyAktualny=czyAktualny;
		}

		public Integer getWeigthFrom(){
    	return weigthFrom;
    }	
    public void setWeigthFrom(Integer weigthFrom){
    	this.weigthFrom = weigthFrom;
    }
    public Integer getWeigthTo(){
    	return weigthTo;
    }	
    public void setWeigthTo(Integer weigthTo){
    	this.weigthTo = weigthTo;
    }

    }

    public static class Range
    {
        private String min;
        private String max;

        public Range(String min, String max)
        {
            if (min == null || max == null)
                throw new NullPointerException("min == null || max == null");
            this.min = min;
            this.max = max;
        }

        public String getMin()
        {
            return min;
        }

        public String getMax()
        {
            return max;
        }

        public String toString()
        {
            return "Range["+min+"-"+max+"]";
        }
    }

    /**
     * Zapisuje w bazie kopi� tego obiektu dziennika, oznacza
     * bie��cy dziennik jako zamkni�ty.
     * @return
     * @throws EdmException
     */
    public Journal reopen(int cyear) throws EdmException
    {
        if (closed)
            throw new EdmException(sm.getString("DziennikJestJuzOznaczonyJakoUusuniety"));

        try
        {
            Journal copy = (Journal) super.clone();
            copy.id = null;
            copy.sequenceId = new Integer(1);
            copy.hversion = null;
            copy.ctime = new java.util.Date();
            copy.archived = false;
            copy.closed = false;
            copy.cyear = cyear;

            this.closed = true;

            DSApi.context().session().save(copy);

            return copy;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (CloneNotSupportedException e)
        {
            throw new Error(e.getMessage(), e);
        }
    }

    public static Journal getMainIncoming() throws EdmException
    {
        return getMainJournal(Journal.INCOMING);
    }

    public static Journal getMainOutgoing() throws EdmException
    {
        return getMainJournal(Journal.OUTGOING);
    }

    public static Journal getMainInternal() throws EdmException
    {
        return getMainJournal(Journal.INTERNAL);
    }

    public static List findMainJournals(boolean closed) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(Journal.class);
        criteria.add(Expression.isNull("ownerGuid"));
        criteria.add(Expression.eq("closed", Boolean.valueOf(closed)));

        try
        {
            List list = criteria.list();

            return list;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Znajduje jeden z g��wnych dziennik�w okre�lonego typu.
     * @param type
     * @return
     * @throws EdmException
     */
    public static Journal getMainJournal(String type) throws EdmException
    {

    	if(AvailabilityManager.isAvailable("journal.disable."+type))
    	{
    		return null;
    	}
        Criteria criteria = DSApi.context().session().createCriteria(Journal.class);
        criteria.add(Restrictions.isNull("ownerGuid"));
        criteria.add(Restrictions.eq("closed", Boolean.FALSE));
        criteria.add(Restrictions.eq("journalType", type));

        try
        {
            List list = criteria.list();

            if (list.size() == 0)
                throw JournalNotFoundException.mainNotFound(type);

            if (list.size() > 1)
                throw JournalNotFoundException.mainMultiple(type);

            ((Journal) list.get(0)).getId();
            return (Journal) list.get(0);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     *
     * @param query
     * @return List<EntryBean>
     * @throws EdmException
     */
    
    public static List search(JournalQuery query) throws EdmException
    {
        Journal journal = Journal.find(query.getJournalId());
        String type = journal.getJournalType();
        
        SelectClause select = new SelectClause(false);
        FromClause from = new FromClause();
        TableAlias doc = null;
        WhereClause where = new WhereClause();
        
        if(type.equals(Journal.INCOMING))
        {
            doc = from.createTable("dso_in_document");
        }
        else
        {
            doc = from.createTable("dso_out_document");
        }
        
        TableAlias je = from.createJoinedTable("dso_journal_entry",true,doc,FromClause.JOIN,"$l.id = $r.documentid");
        TableAlias rec = from.createJoinedTable("dso_person",true,doc,FromClause.LEFT_OUTER,"$l.id=$r.document_id and $r.discriminator='RECIPIENT'");
        TableAlias snd = from.createJoinedTable("dso_person",true,doc,FromClause.LEFT_OUTER,"$l.sender=$r.id and $r.discriminator='SENDER'");
        TableAlias div = from.createJoinedTable("ds_division",true,doc,FromClause.LEFT_OUTER,"$l.divisionGuid = $r.guid");
        TableAlias dsDoc = from.createJoinedTable("ds_document", true, doc, FromClause.LEFT_OUTER, "$l.id=$r.id");
        TableAlias recUser = null;
        
        if(type.equals(Journal.INCOMING))
        {
        	recUser = from.createJoinedTable("ds_user", true, doc, FromClause.LEFT_OUTER, "$l.recipientUser = $r.name");
            //select.add(doc,"id");
            select.addSql("DISTINCT " + doc.column("id")); //pomini�cie duplikat�w
            select.add(doc,"incomingdate");
            select.add(doc,"documentdate");
            select.add(doc,"officenumber");
            select.add(doc,"referenceid");
            select.add(doc,"summary");
            select.add(doc,"divisionGuid");
            select.add(rec,"title");
            select.add(rec,"firstname");
            select.add(rec,"lastname"); //10
            select.add(rec,"organization");
            select.add(rec,"street");
            select.add(rec,"location");
            select.add(snd,"anonymous");
            select.add(snd,"title");
            select.add(snd,"firstname");
            select.add(snd,"lastname");
            select.add(snd,"organization");
            select.add(snd,"street");
            select.add(snd,"location"); //20
            select.add(je,"sequenceid");
            select.addSql("0");
            select.add(doc,"numattachments");
            select.add(doc,"currentassignmentusername");
            select.add(doc,"creatinguser");
            select.addSql("NULL"); // za postal number po stronie else'a
            select.add(rec,"ORGANIZATIONDIVISION");
            select.add(doc,"dailyofficenumber");
            select.add(doc,"postalregnumber");
            select.add(div,"name"); //30            
            select.add(doc,"delivery");
            select.add(doc,"caseDocumentId");
            select.addSql("NULL"); // za postal number po stronie else'a
            select.addSql("NULL"); // za postal number po stronie else'a
            select.addSql("NULL"); // za postal number po stronie else'a
            select.add(doc,"CASEDOCUMENTID");
            select.add(recUser, "name");
            select.add(recUser, "lastname");
            select.add(recUser, "firstname");
            select.add(doc, "master_document_id");
        }
        else
        {
            //select.add(doc,"id");
            select.addSql("DISTINCT " + doc.column("id")); //pomini�cie duplikat�w
            select.addSql("officedate");
            select.add(doc,"documentdate");
            select.add(doc,"officenumber");
            
            select.addSql("NULL");
            select.add(doc,"summary");
            select.add(doc,"divisionGuid");
            select.add(rec,"title");
            select.add(rec,"firstname");
            select.add(rec,"lastname"); //10
            select.add(rec,"organization");
            select.add(rec,"street");
            select.add(rec,"location");
            select.add(snd,"anonymous");
            select.add(snd,"title");
            select.add(snd,"firstname");
            select.add(snd,"lastname");
            select.add(snd,"organization");
            select.add(snd,"street");
            select.add(snd,"location"); //20
            select.add(je,"sequenceid");
            select.add(doc,"internaldocument");
            select.addSql("0");
            select.add(doc,"currentassignmentusername");
            select.add(doc,"creatinguser");
            select.add(doc, "postalregnumber");
            select.add(rec,"ORGANIZATIONDIVISION");
            select.add(doc,"dailyofficenumber");
            select.add(doc,"postalregnumber");
            select.add(div,"name"); //30   
            select.addSql("NULL");
            select.add(doc,"caseDocumentId");
            select.add(doc,"weightKg");
            select.add(doc,"weightG");
            select.add(doc,"stampFee");
            select.add(doc,"CASEDOCUMENTID");

        }
        
        
        /*---------------------------------------------------------------------------------------------------*/
        /*													WHERE											 */
        /*---------------------------------------------------------------------------------------------------*/

        if(type.equals(Journal.INCOMING))
        {
            if(query.getDay()!=null)
            {
                Date tmpDate = query.getDay();
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(je.attribute("ENTRYDATE"),tmpDate));

                Date tmppdate =  DateUtils.plusDays(tmpDate, 1);
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.lt(je.attribute("ENTRYDATE"),tmppdate));
            }
            if(query.getFromDay()!=null && query.getToDay()!=null)
            {
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(je.attribute("ENTRYDATE"),new Timestamp( query.getFromDay().getTime())));
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.lt(je.attribute("ENTRYDATE"),new Timestamp( query.getToDay().getTime())));
            }
        }
        else
        {   
        	if(query.getDay()!=null)
        	{
        		Date tmpDate = query.getDay();
        		pl.compan.docusafe.util.querybuilder.expression.Junction or=pl.compan.docusafe.util.querybuilder.expression.Junction.disjunction();
        		pl.compan.docusafe.util.querybuilder.expression.Junction and=pl.compan.docusafe.util.querybuilder.expression.Junction.conjunction();

        		if(AvailabilityManager.isAvailable("dzienniki.pismaWychodzaceWyszukiwaniePoDacieWedlugDodaniaNumeruKO")){
        			or.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(doc.attribute("OFFICEDATE"), null));
        			and.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(doc.attribute("OFFICEDATE"),tmpDate));
        		}
        		else
        			where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(je.attribute("ctime"),tmpDate));

        		Date tmppdate =  DateUtils.plusDays(tmpDate, 1);
        		if(AvailabilityManager.isAvailable("dzienniki.pismaWychodzaceWyszukiwaniePoDacieWedlugDodaniaNumeruKO")){
        			and.add(pl.compan.docusafe.util.querybuilder.expression.Expression.lt(doc.attribute("OFFICEDATE"),tmppdate));
        			or.add(and);
        			where.add(or);
        		}
        		else
        			where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.lt(je.attribute("ctime"),tmppdate));
        	}
            if(query.getFromDay()!=null && query.getToDay()!=null)
            {	
            	if(AvailabilityManager.isAvailable("dzienniki.pismaWychodzaceWyszukiwaniePoDacieWedlugDodaniaNumeruKO")){
            		where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(doc.attribute("OFFICEDATE"),new Timestamp( query.getFromDay().getTime())));
            		where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.lt(doc.attribute("OFFICEDATE"),new Timestamp( query.getToDay().getTime())));
            	}
            	else{
            		where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(je.attribute("ctime"),new Timestamp( query.getFromDay().getTime())));
            		where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.lt(je.attribute("ctime"),new Timestamp( query.getToDay().getTime())));                
            	}
            }  
        }

        if(StringUtils.isNotEmpty(query.getCreatingEntry()))
        {
        	where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(je.attribute("author"),query.getCreatingEntry()));
        }
        if(query.getAuthor()!=null)
        {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(doc.attribute("creatinguser"),query.getAuthor()));
        }
        else if(query.getAuthorDivision() != null)
        {
        	DSUser[] users = DSDivision.find(query.getAuthorDivision()).getUsers(true);
        	if(users != null && users.length > 0)
        	{
        		String[] usersnames = new String[users.length];
        		for (int i = 0; i < users.length; i++) {
        			usersnames[i] = users[i].getName();
    			}
	        	where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.in(doc.attribute("creatinguser"), 
	        			usersnames));
        	}
        }
        if(query.getZpo() !=null && query.getZpo())
        {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(doc.attribute("zpo"),1));
        }
        if(query.getPostalregnumber()!=null && query.getPostalregnumber().length() > 0)
        {
        	if(AvailabilityManager.isAvailable("search_part_postalreg")) where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(doc.attribute("postalregnumber"), "%"+query.getPostalregnumber()+"%"));
        	else where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(doc.attribute("postalregnumber"),query.getPostalregnumber()));
            
        }
        if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu")){
        where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(dsDoc.attribute("czy_aktualny"), Boolean.TRUE));
        }
        if(journal.isMain())
        {        
            if(query.getFromNum()!=null)
            {
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(doc.attribute("OFFICENUMBER"),query.getFromNum()));
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.le(doc.attribute("OFFICENUMBER"),query.getToNum()));
            }
            else if(query.getFromNumD()!=null)
            {
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(doc.attribute("DAILYOFFICENUMBER"),query.getFromNumD()));
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.le(doc.attribute("DAILYOFFICENUMBER"),query.getToNumD()));
            }
        }
        else
        {
            if(query.getFromNum()!=null)
            {
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(je.attribute("sequenceid"),query.getFromNum()));
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.le(je.attribute("sequenceid"),query.getToNum()));
            }
            else if(query.getFromNumD()!=null)
            {
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(je.attribute("DAILYSEQUENCEID"),query.getFromNumD()));
                where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.le(je.attribute("DAILYSEQUENCEID"),query.getToNumD()));
            }
        }
        where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(je.attribute("journal_id"),query.getJournalId()));

        List<Integer> lista1 = new ArrayList<Integer>();
        List<Integer> lista2 = new ArrayList<Integer>();
        for(int i=0 ;i<20;i++)
        {
            lista1.add(i,i);
            lista2.add(i,i);
        }
        
        List<Range> sequenceRanges;
        if ((sequenceRanges=query.getSequenceRanges()) != null &&
            sequenceRanges.size() > 0)
        {
            pl.compan.docusafe.util.querybuilder.expression.Junction OR = pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();
            for (int i=0; i < sequenceRanges.size(); i++)
            {
                
                Range range = sequenceRanges.get(i);
                if (range.getMax() == range.getMin())
                {
                	if(AvailabilityManager.isAvailable("dailyOfficeNumber"))
                		OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(doc.attribute("DAILYOFFICENUMBER"),sequenceRanges.get(i).getMin()));
                	else
                		OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(doc.attribute("OFFICENUMBER"),sequenceRanges.get(i).getMin()));
                }
                else
                {
                    pl.compan.docusafe.util.querybuilder.expression.Junction whereOR = pl.compan.docusafe.util.querybuilder.expression.Expression.conjunction();
                    if(AvailabilityManager.isAvailable("dailyOfficeNumber"))
                    {
                    	whereOR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(doc.attribute("DAILYOFFICENUMBER"),sequenceRanges.get(i).getMin()));
                        whereOR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.le(doc.attribute("DAILYOFFICENUMBER"),sequenceRanges.get(i).getMax()));
                    }
                    else
                    {
                    	whereOR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(doc.attribute("OFFICENUMBER"),sequenceRanges.get(i).getMin()));
                        whereOR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.le(doc.attribute("OFFICENUMBER"),sequenceRanges.get(i).getMax()));
                    }
                    
                    OR.add(whereOR);
                }
                where.add(OR);                
            }
        }
        
        Integer[] deliveryIds;
        if ((deliveryIds=query.getDeliveryIds()) != null &&
            deliveryIds.length > 0)
        {
            pl.compan.docusafe.util.querybuilder.expression.Junction OR = pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();

            for (int i=0; i < deliveryIds.length; i++)
            {
                OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(doc.attribute("delivery"),deliveryIds[i]));               
            }
            where.add(OR);
        }
        
        if (type.equals(Journal.INCOMING))
        {
            Integer[] kindIds;
            if ((kindIds=query.getIncomingKinds()) != null &&
                kindIds.length > 0)
            {
                pl.compan.docusafe.util.querybuilder.expression.Junction OR = pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();
                for (int i=0; i < kindIds.length; i++)
                {
                    OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(doc.attribute("kind"),kindIds[i]));
                }
                where.add(OR);
            }
            if (query.getSender() != null)
            {    
                String[] tokens = query.getSender().split("\\s+");
                if (tokens.length > 0)
                {
                    pl.compan.docusafe.util.querybuilder.expression.Junction OR = pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();
                    for (int i=0; i < tokens.length; i++)
                    {
                        OR = pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(snd.attribute("firstname"),"%"+tokens[i]+"%"));
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(snd.attribute("lastname"),"%"+tokens[i]+"%"));
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(snd.attribute("organization"),"%"+tokens[i]+"%"));
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(snd.attribute("location"),"%"+tokens[i]+"%"));
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(snd.attribute("street"),"%"+tokens[i]+"%"));
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(snd.attribute("zip"),"%"+( tokens[i].length()> 12 ? tokens[i].substring(0,12) : tokens[i] )+"%"));
                        where.add(OR);
                    }                
                }
            }
        	if(StringUtils.isNotEmpty(query.getRecipientLastname()) || StringUtils.isNotEmpty(query.getRecipientOrganization()))
        	{
        		if(StringUtils.isNotEmpty(query.getRecipientLastname()))
        				where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(rec.attribute("lastname"),query.getRecipientLastname()));
        		if(StringUtils.isNotEmpty(query.getRecipientFirstname()))
        			where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(rec.attribute("firstname"),query.getRecipientFirstname()));
        		if(StringUtils.isNotEmpty(query.getRecipientOrganization()))
        			where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(rec.attribute("organization"),query.getRecipientOrganization()));
        	}
        }
        else
        {

        	if (query.getRecipient() != null)
            {
                String[] tokens = query.getRecipient().split("\\s+");
                if (tokens.length > 0)
                {
                	
                    pl.compan.docusafe.util.querybuilder.expression.Junction OR = pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();
                    for (int i=0; i < tokens.length; i++)
                    {
                        OR = pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(rec.attribute("firstname"),"%"+tokens[i]+"%"));
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(rec.attribute("lastname"),"%"+tokens[i]+"%"));
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(rec.attribute("organization"),"%"+tokens[i]+"%"));
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(rec.attribute("location"),"%"+tokens[i]+"%"));
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(rec.attribute("street"),"%"+tokens[i]+"%"));
                        OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(rec.attribute("zip"),"%"+(tokens[i].length()> 12 ? tokens[i].substring(0,12) : tokens[i] )+"%"));
                        where.add(OR);
                    }                    
                }
            }
        }
        if (query.getSummary() != null)
        {
            where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(doc.attribute("summary"),"%"+query.getSummary()+"%"));
        }
        
        /*---------------------------------------------------------------------------------------------------*/
        /*													ORDER											 */
        /*---------------------------------------------------------------------------------------------------*/
        
        OrderClause order = new OrderClause();
        
        Boolean ascending = (query.ascending == null || query.ascending.equals(true)) ? true : false;
        
        if (query.orderBy != null) {
        	if (query.orderBy.equals("senderSummary")) {
        		order.add(snd.attribute("anonymous"), ascending);
        		order.add(snd.attribute("title"), ascending);
        		order.add(snd.attribute("firstname"), ascending);
        		order.add(snd.attribute("lastname"), ascending);
        		order.add(snd.attribute("organization"), ascending);
        		order.add(snd.attribute("street"), ascending);
        		order.add(snd.attribute("location"), ascending);
        	} else if (query.orderBy.equals("recipientSummary")) {
        		order.add(rec.attribute("title"), ascending);
        		order.add(rec.attribute("firstname"), ascending);
        		order.add(rec.attribute("lastname"), ascending);
        		order.add(rec.attribute("organization"), ascending);
        		order.add(rec.attribute("street"), ascending);
        		order.add(rec.attribute("location"), ascending);
        	} else if (query.orderBy.equals("divisionName")) {
        		
        		// sortowanie po uzyskaniu wynikow zapytania do bazy
        		order.add(div.attribute("name"), ascending);
        		
        	}else if (query.orderBy.equals("incomingdate") && !type.equals(Journal.INCOMING)){
        		order.add(doc.attribute("officedate"), ascending);
        	}
        	else if(query.orderBy.equals("sequenceId"))
        	{
        		//order.add(je.attribute("DAILYSEQUENCEID"), ascending);
        		order.add(je.attribute("sequenceId"), ascending);
        	} 
        	else {
        		order.add(doc.attribute(query.orderBy), ascending);
        	}        	
        }
        else
        	if(!query.isSortByRegNumber()){ 
			    if(!query.isSortByRecipientLocation())
			    {
			        if(journal.isMain())
			            order.add(doc.attribute("officenumber"),true);
			        else
			            order.add(je.attribute("sequenceid"),true); 
			    }
			    else
			    {
			        order.add(rec.attribute("location"),true);
			    }
        }else{
        	order.add(doc.attribute("postalregnumber"),true);
        }
        
        /*---------------------------------------------------------------------------------------------------*/
        
        //SelectQuery selectQuery = new SelectQuery(select, from, where,order);
        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(DISTINCT "+doc.column("id")+")");


        Set<Long> seenIds = new HashSet<Long>();
        ResultSet rs = null;
        try
        {
            SelectQuery selectQuery = new SelectQuery(selectCount, from, where, null);
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            boolean unable = false;
            
            if (rs.next())
            {
            	query.totalCount = rs.getInt(countCol.getPosition());
            }
            else
            {
            	unable = true;
            }
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());
            if (unable)
            {
                throw new EdmException(sm.getString("NieMoznaPobracLiczbyDziennikow"));
            }                  
            selectQuery = new SelectQuery(select, from, where,order);
            
            if (query.limit > 0 && query.offset>=0) 
            {
               selectQuery.setMaxResults(query.getLimit());
               selectQuery.setFirstResult(query.getOffset());
            }
            
            
            
            rs = selectQuery.resultSet();
            
            List<EntryBean> results = new LinkedList<EntryBean>();
            while (rs.next())
            {
                long id = rs.getLong(1);
                Long Id = new Long(id);
                if (seenIds.contains(Id))
                {
                    continue;
                }

                int ko = rs.getInt(4);
                int dko = rs.getInt(28);

                String guid = rs.getString(7);
                String divisionName = null;
                Map<String, String> recipientUser = null;
                Long masterDocumentId = null;
                List journals = Journal.findByDocumentId(Id);
                for (Iterator iter=journals.iterator(); iter.hasNext(); )
                {
                    Journal jrnl = (Journal) iter.next();
                    // bez g��wnego dziennika
                    if (jrnl.getOwnerGuid() != null &&
                        !DSDivision.ROOT_GUID.equals(jrnl.getOwnerGuid()))
                    {
                        try
                        {
                            DSDivision division = DSDivision.find(jrnl.getOwnerGuid());
                            if (divisionName != null)
                                divisionName += ", " + division.getName();
                            else
                                divisionName = division.getName();
                        }
                        catch (DivisionNotFoundException e)
                        {

                        }
                    }
                }
                if (guid != null && divisionName == null)
                {
                    try
                    {
                        divisionName = DSDivision.find(guid).getName();
                    }
                    catch (DivisionNotFoundException e)
                    {

                    }
                }
                String username = rs.getString(24);
                String currentAssignmentUsername = null;
                if (username != null)
                {
                    try
                    {
                        DSUser user = DSUser.findByUsername(username);
                        currentAssignmentUsername = user.getFirstname()+" "+user.getLastname();
                    }
                    catch (UserNotFoundException e)
                    {

                    }
                }
                String cUser = rs.getString(25);
                String creatingUser = null;
                if (cUser != null)
                {
                    try
                    {
                        DSUser user = DSUser.findByUsername(cUser);
                        creatingUser = user.getFirstname()+" "+user.getLastname();
                    }
                    catch (UserNotFoundException e)
                    {

                    }
                }
                
                String organizationDivision = null;
                                
                
                try
                {
                	organizationDivision = rs.getString(27);
                }
                catch (Exception e) {}         
                
                String delivery = null;
                try
                {
                	delivery = InOfficeDocumentDelivery.find(rs.getInt(31)).getName();
                }
                catch (Exception e){} 
                
                String reciptientName = "";
                if(rs.getString(10) == null || rs.getString(10).equals(""))
                {
                	reciptientName = rs.getString(11);
                }
                else
                {
                	reciptientName = rs.getString(9) + " " + rs.getString(10);
                }
	


				String remarks = null;
				String feeZl = null;
				String feeGr = null;
				String weightKg = null;
				String weightG = null;
				String fee = null;
				String caseSign = null;


                Document document = Document.find(Id,false);
    			if(document instanceof OutOfficeDocument)
    			{
    				OutOfficeDocument docO = (OutOfficeDocument)document;
	                remarks = docO.getDelivery() != null ? docO.getDelivery().getName() : null;
    			}
    			
    			weightKg = rs.getString(33);
    			weightG = rs.getString(34);
                if(rs.getString(35) != null){
                	fee = rs.getString(35);
		            if(fee.indexOf('.')!=-1)
		            {
		            	feeGr = fee.substring(fee.indexOf('.')+1);
		            	feeZl = fee.substring(0, fee.indexOf('.'));
		            }
		            else {
		            	feeZl = fee;
		            }
                }
    			caseSign = rs.getString(36);
    			
    			if(recUser != null) {
	    			String recipientUserName = rs.getString(EntryBean.RECIPIENT_USER_NAME_INDEX);
	    			if(!Strings.isNullOrEmpty(recipientUserName)) {
	    				recipientUser = new HashMap<String, String>();
	    				recipientUser.put("name", recipientUserName.trim());
	    				recipientUser.put("lastname", rs.getString(EntryBean.RECIPIENT_USER_LASTNAME_INDEX).trim());
	    				recipientUser.put("firstname", rs.getString(EntryBean.RECIPIENT_USER_FIRSTNAME_INDEX).trim());
	    			}
	    			masterDocumentId = rs.getLong(EntryBean.MASTER_DOCUMENT_ID_INDEX);
	    			// dla hibernate 0 = null
	    			if(masterDocumentId == 0)
	    				masterDocumentId = null;
    			}

                
//    			if(document instanceof InOfficeDocument)
//    			{
//    				InOfficeDocument docI = (InOfficeDocument)document;
//	                caseSign = docI.getCaseDocumentId();
//    			}
    			
                EntryBean bean = new EntryBean(
                    Id,
                    DateUtils.nullSafeSqlDate(rs.getTimestamp(2)),
                    DateUtils.nullSafeSqlDate(rs.getDate(3)),
                    OfficeDocument.getFormattedOfficeNumber(ko, dko),//ko > 0 ? new Integer(ko) : null,
                    rs.getString(5), // referenceId
                    rs.getString(6), // summary
                    Person.getSummary(
                        rs.getBoolean(14),
                        rs.getString(15),
                        rs.getString(16),
                        rs.getString(17),
                        rs.getString(18),
                        rs.getString(19),
                        rs.getString(20)),
                    Person.getSummary(
                        false,
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13),
                        organizationDivision),
                    recipientUser,
                    masterDocumentId,
                    divisionName,
                    currentAssignmentUsername,
                    new Integer(rs.getInt(21)),
                    rs.getBoolean(22),
                    rs.getInt(23),
                    creatingUser,username,
                    rs.getString(29),
                    String.valueOf(delivery),
                    rs.getString(32), // caseSign
                    rs.getString(12) + " " + rs.getString(13),
                    reciptientName,
                    remarks,   
                    feeZl,
                    feeGr,
                    weightKg,
                    weightG
                );

                              
                results.add(bean);
                seenIds.add(Id);
            }            if (query.orderBy != null && query.orderBy.equals("divisionName")) 
            {
            	//TODO:przerobic to
            }
            return results;
            
            
        }
        catch (EdmException e)
        {
        	LogFactory.getLog("eprint").debug("", e);
        }
        catch (HibernateException e)
        {
        	LogFactory.getLog("eprint").debug("", e);
        }
        catch (SQLException e)
        {
        	LogFactory.getLog("eprint").debug("", e);
        }
        finally
        {
        	try { rs.close(); DSApi.context().closeStatement(rs.getStatement()); } catch (Exception e) { }
        }
        return null;
    }


    /**
     * Zwraca list� dziennik�w dowolnego typu zwi�zanych z danym
     * dzia�em.
     */
    public static List findByDivisionGuid(String divisionGuid) throws EdmException
    {
        return findByDivisionGuid(divisionGuid, null);
    }

    /**
     * Znajduje wszystkie dzienniki (r�wnie� zamkni�te), w ktorych
     * znajduje si� pismo.
     * @param id
     * @return
     * @throws EdmException
     */
    public static List findByDocumentId(Long id) throws EdmException
    {
        if (id == null)
            throw new NullPointerException("id");

        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(JournalEntry.class);
            criteria.add(Restrictions.eq("documentId", id));
            List entries = criteria.list();
            List<Journal> journals = new ArrayList<Journal>(entries.size());

            for (Iterator iter=entries.iterator(); iter.hasNext(); )
            {
                JournalEntry entry = (JournalEntry) iter.next();
                if (!journals.contains(entry.getJournal()))
                    journals.add(entry.getJournal());
            }

            return journals;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "id="+id);
        }
    }

    public static List<Journal> findByDivisionGuid(String divisionGuid, String type)
        throws EdmException
    {
        return findByDivisionGuid(divisionGuid, type, false);
    }

    public static List<Journal> findByDivisionGuid(String divisionGuid, String type, boolean closed)
        throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(Journal.class);
        criteria.add(Restrictions.eq("closed", Boolean.valueOf(closed)));
        criteria.add(Restrictions.eq("ownerGuid", divisionGuid));
        if (type != null)
            criteria.add(Restrictions.eq("journalType", type));

        try
        {
            List list = criteria.list();
            return list;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List findByDivisionGuidAndDescription(String divisionGuid, String description)
        throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(Journal.class);
        criteria.add(Restrictions.eq("closed", Boolean.FALSE));
        criteria.add(Restrictions.eq("ownerGuid", divisionGuid));
        criteria.add(Restrictions.eq("description", description));

        try
        {
            List list = criteria.list();
            return list;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List<Journal> findByDivisionGuidAndSymbol(String divisionGuid, String symbol)
        throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(Journal.class);
        criteria.add(Restrictions.eq("closed", Boolean.FALSE));
        criteria.add(Restrictions.eq("ownerGuid", divisionGuid));
        criteria.add(Restrictions.eq("symbol", symbol));

        try
        {
            return (List<Journal>) criteria.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca list� wszystkich dziennik�w nie oznaczonych jako zarchiwizowane
     * lub zamkni�te.
     */
    public static List<Journal> list() throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(Journal.class);
            crit.add(Restrictions.eq("closed", Boolean.FALSE));
            crit.add(Restrictions.eq("archived", Boolean.FALSE));
            return (List<Journal>) crit.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List<Journal> listAllByType(String type) throws EdmException {
        try {
            Criteria crit = DSApi.context().session().createCriteria(Journal.class);
            crit.add(Restrictions.eq("journalType", type));
            return (List<Journal>) crit.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    /** Zwraca tylko otwarte! */
    public static List<Journal> findByType(String type) throws EdmException
    {
        return findByType(type, false);
    }

    /**
     * Zwraca list� wszystkich dziennik�w nieoznaczonych jako zarchiwizowane.
     * @return
     * @throws EdmException
     */
    public static List<Journal> findByType(String type, boolean closed) throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(Journal.class);
            crit.add(Restrictions.eq("closed", closed));
            crit.add(Restrictions.eq("archived", Boolean.FALSE));
            if (type != null)
                crit.add(Restrictions.eq("journalType", type));
            return (List<Journal>) crit.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Journal find(Long id)
        throws EdmException, JournalNotFoundException
    {
        Journal journal = (Journal) DSApi.getObject(Journal.class, id);

        if (journal == null)
            throw new JournalNotFoundException(id);

        return journal;
    }

    public void delete() throws EdmException
    {
        if (!canDelete())
            throw new EdmException(sm.getString("NieMoznaUsunacDziennikaJezeliZnajdujaSieWnimPisma"));

        try
        {
//            List<JournalEntry> entries = JournalEntry.findByJournal(this);
//            for(JournalEntry entry : entries){
//            	DSApi.context().session().delete(entry);
//            }
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Sprawdza, czy dziennik mo�e by� usuni�ty. Jest to mo�liwe tylko
     * wtedy, gdy w dzienniku nie ma pism.
     */
    public boolean canDelete() throws EdmException
    {
        // TODO: sprawdza�, czy w dzienniku s� dokumenty

        FromClause fromClause = new FromClause();
        TableAlias tableJournalEntry = fromClause.createTable("dso_journal_entry");

        WhereClause whereClause = new WhereClause();
        whereClause.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(
            tableJournalEntry.attribute("journal_id"), id));

        SelectClause selectClause = new SelectClause();
        selectClause.addSql("count(*)");

        try
        {
            SelectQuery selectQuery = new SelectQuery(selectClause, fromClause, whereClause, null);
            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            boolean unable = false;
            boolean result = false;
            if (rs.next())
            {
            	result = rs.getInt(1) == 0;
            }
            else
            {
            	unable = true;
            }
            rs.close();
            if (unable)
            {            	
                throw new EdmException(sm.getString("NieMoznaPobracLiczbyPismWdzienniku")+" "+id);
            }                       
            return result;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

    public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

	/**
	 * Dodaje nowy wpis bez commitowania zmian
	 * @param journalId
	 * @param documentId
	 * @param entryDate
	 * @param conn
	 * @return sequenceId 
	 * @throws java.sql.SQLException
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	public static Integer TX_uncommitedNewEntry( Long journalId,Long documentId, Date entryDate, Connection conn) throws SQLException, EdmException {
		Integer sequenceId = null;
		Integer dailySequenceId= null;
		Date day = null;
		int currentSequenceId = -1;
		int currentDailySequenceId = -1;
		Integer result = null;
		PreparedStatement st = null;
		st = conn.prepareStatement("select sequenceid, dailySequenceId,openDay from dso_journal where id= ?");
		st.setLong(1, journalId);
		PreparedStatement ps = null;
		try 
		{
			do
			{
				ResultSet rs = st.executeQuery();
				if (rs.next()) 
				{
					currentSequenceId = rs.getInt(1);
					currentDailySequenceId = rs.getInt(2);
					day = rs.getDate(3);
					if(day == null )
						day = new Date();
					GregorianCalendar journalDay = new GregorianCalendar();
					journalDay.setTime(day);
					GregorianCalendar entryJournalDay = new GregorianCalendar();
					entryJournalDay.setTime(entryDate);					
					if(entryJournalDay.get(Calendar.DAY_OF_MONTH) != journalDay.get(Calendar.DAY_OF_MONTH) || 
							entryJournalDay.get(Calendar.MONTH) != journalDay.get(Calendar.MONTH) || 
							entryJournalDay.get(Calendar.YEAR) != journalDay.get(Calendar.YEAR))
					{
						currentDailySequenceId = 1;
					}
					rs.close();
					st.close();
				}
				else 
				{
					rs.close();
					st.close();
					throw new EdmException(sm.getString("NieMoznaPobracBiezacegoNumeruPismaZdziennika") + " " + journalId);
				}
		
				st = conn.prepareStatement("update dso_journal set sequenceid = ? ,dailySequenceId = ? ,openDay = ? where sequenceid = ? and id= ?");
				st.setInt(1,currentSequenceId + 1);
				st.setInt(2, currentDailySequenceId + 1);
				st.setDate(3, new java.sql.Date(entryDate.getTime()));
				st.setInt(4, currentSequenceId);
				st.setLong(5, journalId);
				
				
					//	"update dso_journal set sequenceid=" + (currentSequenceId + 1) + ",dailySequenceId = " + (currentDailySequenceId + 1) +
					//		",day = " + (entryday + 1) + " where sequenceid=" + currentSequenceId + " and id=" + journalId
				
				if (st.executeUpdate() == 1)
				{
					sequenceId = (currentSequenceId + 1);
					dailySequenceId = (currentDailySequenceId + 1);
					ps = conn.prepareStatement("INSERT INTO DSO_JOURNAL_ENTRY " + "(DOCUMENTID,SEQUENCEID,CTIME,ENTRYDATE,JOURNAL_ID,dailySequenceId,author) " + "VALUES (?, ?, ?, ?, ?, ?, ?)");
					ps.setLong(1, documentId.longValue());
					ps.setInt(2, currentSequenceId);
					ps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
					ps.setTimestamp(4, new Timestamp(entryDate.getTime()));
					ps.setLong(5, journalId.longValue());
					ps.setInt(6, currentDailySequenceId);
					ps.setString(7, DSApi.context().getPrincipalName());
					try
					{
						ps.executeUpdate();
					}
					catch (Exception e) 
					{
						/**Jak by ju� byo powi�zanie ale nie byo wpisane do dokumentu to to sprawdzamy*/
						ps.close();
						conn.rollback();
						ps = conn.prepareStatement("select sequenceid,dailySequenceId from DSO_JOURNAL_ENTRY where JOURNAL_ID = ? and DOCUMENTID = ? ");
						ps.setLong(1, journalId.longValue());
						ps.setLong(2, documentId);
						rs = ps.executeQuery();
						if(rs.next())
						{
							sequenceId = rs.getInt(1);
							dailySequenceId = rs.getInt(2);
							currentSequenceId = new Integer(sequenceId);
							log.error("Poprawiam numer KO w pismie ktore juz jest w dzienniku a nie dostao numeru");
						}
						else
						{
							throw new EdmException(e);
						}
					}
					ps.close();
				} 
				else
				{
					/** wycofanie transakcji, kolejna pr�ba*/
					st.close();
					conn.rollback();
				}
			} while (sequenceId == null || dailySequenceId== null);
		} finally {
			DbUtils.closeQuietly(st);
			DbUtils.closeQuietly(ps);
		}
		result = currentSequenceId;
		return result;
	}
	
    /**
     * Metoda otwiera w�asne po��czenie do bazy danych i w�asn� transakcj� niezale�n�
     * od aktualnie otwartego DSContext.  Ze wskazanego dziennika pobierany jest nowy
     * numer kolejny, dziennik jest uaktualniany i tworzony jest wpis w dzienniku
     * (JournalEntry) wi���cy z tym dziennikiem dokument.
     * @param journalId
     * @param documentId
     * @param entryDate
     * @return tab[0] = sequenceId tab[1] = dailySequenceId
     * @throws EdmException
     */
    public static Integer TX_newEntry2(Long journalId, Long documentId, Date entryDate) throws EdmException
    {
        return TX_newEntry2(journalId, documentId, entryDate, null);
    }
    
    public synchronized static Integer TX_newEntry2(Long journalId, Long documentId, Date entryDate, Connection conn)
        throws EdmException
    {
        if (journalId == null)
            throw new NullPointerException("journalId");
        if (documentId == null)
            throw new NullPointerException("documentId");
        if (entryDate == null)
            throw new NullPointerException("entryDate");

        //Connection conn = null;
        
        boolean newConnectionOpened = false;
        
        try {
			if (conn == null) {
				conn = Docusafe.getDataSource().getConnection();
				conn.setAutoCommit(false);
				newConnectionOpened = true;
			}

			Integer result = TX_uncommitedNewEntry(journalId, documentId, entryDate, conn);
			//EpuapExportManager.sendToEpuap(documentId,false);
            conn.commit();
			return (result);
			
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e2) {
				throw new EdmSQLException(e2);
			}
			throw new EdmSQLException(e);
		} finally {
			if (newConnectionOpened){
				DbUtils.closeQuietly(conn);
			}
		}
    }

    /**
     * Tworzy nowy obiekt JournalEntry i zwi�ksza o jeden
     * warto�� Journal.sequenceId.
     * <p>
     * Metoda otwiera w�asn� transakcj�.
     * @param journalId
     * @param documentId
     * @throws EdmException
     * @deprecated Nale�y u�ywa� metody TX_newEntry2
     */
    public synchronized static Integer ____TX_newEntry___(Long journalId, Long documentId, Date entryDate)
        throws EdmException
    {
        if (journalId == null)
            throw new NullPointerException("journalId");
        if (documentId == null)
            throw new NullPointerException("documentId");
        if (entryDate == null)
            throw new NullPointerException("entryDate");

        if (!(DSApi.getDialect() instanceof FirebirdDialect))
            throw new EdmException("Metoda TX_newEntry() dzia�a tylko z baz� Firebird");

        Connection conn = null;
        Statement st = null;
        PreparedStatement ps = null;
        try
        {
            conn = Docusafe.getDataSource().getConnection();
            conn.setAutoCommit(false);

            Integer result = null;

            int attempt = 0;
            while (attempt++ < 20)
            {
                try
                {
                    st = conn.createStatement();

                    // najpierw pobra�, a potem set seqid++ where seqid=...

                    // select sequenceid from dso_journal where id=journalId
                    // -> seq
                    // update dso_journal set sequenceid = seq+1 where id=journalId
                    //  and sequenceid=seq
                    //

                    if (st.executeUpdate("UPDATE "+DSApi.getTableName(Journal.class)+
                        " SET SEQUENCEID = SEQUENCEID + 1 WHERE ID = "+journalId) == 0)
                    {
                        try
                        {
                            Thread.sleep(200);
                        }
                        catch (InterruptedException e1)
                        {
                        }
                        continue;
                    }

                    ResultSet rs = st.executeQuery("SELECT SEQUENCEID FROM DSO_JOURNAL " +
                        "WHERE ID="+journalId);
                    if (!rs.next())
                        throw new EdmException(sm.getString("NieMoznaPobracKolejnegoNumeruPismaWdzienniku")+" "+journalId);

                    result = new Integer(rs.getInt(1));
                }
                catch (SQLException e)
                {
                    conn.rollback();

                    if (e.getErrorCode() == 335544336 || // deadlock
                        e.getErrorCode() == 335544349)   // duplicate value in index
                    {
                        log.info("nextId: "+e.getMessage());
                        try
                        {
                            Thread.sleep(200);
                        }
                        catch (InterruptedException e1)
                        {
                        }
                        continue;
                    }
                    throw new EdmException(sm.getString("NieMoznaPobracKolejnegoNumeruPismaWdzienniku")+" "+journalId);                }

                break;
            }

            if (result == null)
                throw new EdmException(sm.getString("NieMoznaPobracKolejnegoNumeruPismaWdzienniku")+" "+journalId);

            int sequenceId = result.intValue() - 1;

            ps = conn.prepareStatement("INSERT INTO DSO_JOURNAL_ENTRY " +
                "(DOCUMENTID,SEQUENCEID,CTIME,ENTRYDATE,JOURNAL_ID) " +
                "VALUES (?, ?, ?, ?, ?)");
            ps.setLong(1, documentId.longValue());
            ps.setInt(2, sequenceId);
            ps.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
            ps.setDate(4, new java.sql.Date(entryDate.getTime()));
            ps.setLong(5, journalId.longValue());

            ps.executeUpdate();

            conn.commit();

            return new Integer(sequenceId);
        }
        catch (SQLException e)
        {
            if (conn != null) try { conn.rollback(); } catch (Exception e2) { }
            throw new EdmSQLException(e);
        }
        finally
        {
            if (st != null) try { st.close(); } catch (Exception e) { }
            if (ps != null) try { ps.close(); } catch (Exception e) { }
            if (conn != null) try { conn.close(); } catch (Exception e) { }
        }
    }


    /**
     * Zwraca wpisy do dziennika z ��danego zakresu w��cznie z ostatnim
     * elementem. Je�eli nie zostanie znaleziony ci�g wpis�w, w kt�rym
     * ko�cowy i pocz�tkowy element maj� ��dane numery, rzucany jest
     * wyj�tek.
     * @param startSeq Numer pierwszego ��danego wpisu.
     * @param endSeq Numer ostatniego ��danego wpisu (w��cznie).
     */
    public List findEntries(int startSeq, int endSeq) throws EdmException
    {
        if (startSeq > endSeq)
            throw new EdmException(sm.getString("journal.startSeqGtEndSeq"));

        Criteria criteria = DSApi.context().session().createCriteria(JournalEntry.class);
        criteria.add(Expression.eq("journal.id", getId()));
        criteria.add(Expression.and(
            Expression.ge("sequenceId", new Integer(startSeq)),
            Expression.le("sequenceId", new Integer(endSeq))
        ));
        criteria.addOrder(Order.asc("sequenceId"));

        try
        {
            List entries = criteria.list();

            JournalEntry first = entries.size() > 0 ?
                (JournalEntry) entries.get(0) : null;
            JournalEntry last = entries.size() >= (endSeq-startSeq+1) ?
                (JournalEntry) entries.get(entries.size()-1) : null;

            if (first == null || first.getSequenceId().intValue() != startSeq)
                throw new EdmException(sm.getString("journal.startEntryNotFound"));

            if (last == null || last.getSequenceId().intValue() != endSeq)
                throw new EdmException(sm.getString("journal.endEntryNotFound"));

            return entries;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public List findEntries(Date fromDate, Date toDate) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(JournalEntry.class);
        criteria.add(Expression.eq("journal.id", getId()));
        criteria.add(Expression.ge("entryDate", fromDate));
        criteria.add(Expression.le("entryDate", toDate));
        criteria.addOrder(Order.asc("sequenceId"));

        try
        {
            if (log.isDebugEnabled())
                log.debug("criteria.list");
            List entries = criteria.list();
            return entries;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public JournalEntry[] getEntries(Date date) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(JournalEntry.class);
        criteria.add(Expression.eq("journal.id", getId()));
        criteria.add(Expression.eq("entryDate", date));
        criteria.addOrder(Order.asc("sequenceId"));

        try
        {
            List<JournalEntry> entries = (List<JournalEntry>) criteria.list();
            return entries.toArray(new JournalEntry[entries.size()]);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }

/*
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 1);

        Date date1 = cal.getTime();

        cal.add(Calendar.DATE, 1);
        cal.add(Calendar.MILLISECOND, -1);

        Date date2 = cal.getTime();

        log.debug("date1="+date1+", date2="+date2);

        Criteria criteria = DSApi.context().session().createCriteria(JournalEntry.class);
        criteria.add(Expression.eq("journal.id", getId()));
        criteria.add(Expression.and(
            Expression.ge("ctime", date1),
            Expression.le("ctime", date2)
        ));
        criteria.addOrder(Order.asc("sequenceId"));

        try
        {
            List entries = criteria.list();
            log.debug("entries="+entries);
            return (JournalEntry[]) entries.toArray(new JournalEntry[entries.size()]);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
*/
    }

    public JournalEntry findEntry(Integer sequenceId) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(JournalEntry.class);
        criteria.add(Expression.eq("journal.id", getId()));
        criteria.add(Expression.eq("sequenceId", sequenceId));

        try
        {
            List entries = criteria.list();

            if (entries.size() > 0)
            {
                return (JournalEntry) entries.get(0);
            }

            return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public JournalEntry findDocumentEntry(Long documentId) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(JournalEntry.class);
        criteria.add(Expression.eq("journal.id", getId()));
        criteria.add(Expression.eq("documentId", documentId));

        try
        {
            List entries = criteria.list();

            if (entries.size() > 0)
            {
                return (JournalEntry) entries.get(0);
            }

            return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public List findEntries(Range[] ranges) throws EdmException
    {
        if (ranges == null)
            throw new NullPointerException("ranges");
        if (ranges.length == 0)
            throw new IllegalArgumentException("ranges.length = 0");

        Criteria criteria = DSApi.context().session().createCriteria(JournalEntry.class);
        criteria.add(Expression.eq("journal.id", getId()));
        Junction OR = Expression.disjunction();
        for (int i=0; i < ranges.length; i++)
        {
            if (ranges[i].getMin().equals(ranges[i].getMax()))
            {
                OR.add(Expression.eq("sequenceId", ranges[i].getMin()));
            }
            else
            {
                Junction AND = Expression.conjunction();
                AND.add(Expression.ge("sequenceId", ranges[i].getMin()));
                AND.add(Expression.le("sequenceId", ranges[i].getMax()));
                OR.add(AND);
            }
        }
        criteria.add(OR);
        criteria.addOrder(Order.asc("sequenceId"));

        try
        {
            List entries = criteria.list();
            return entries;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public Integer getMaxSequenceId() throws EdmException
    {
        Statement st = null;
        try
        {
            st = DSApi.context().session().connection().createStatement();
            ResultSet rs = st.executeQuery("select max(sequenceid) from dso_journal_entry " +
                "where journal_id = "+getId());
            if (!rs.next())
                throw new EdmException(sm.getString("BladOdczytuDziennika"));

            int result = rs.getInt(1);
            if (rs.wasNull())
                return null;
            else
                return new Integer(result);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            if (st != null) try { st.close(); } catch (Exception e) { }
        }
    }

    public Integer getMinSequenceId() throws EdmException
    {
        Statement st = null;
        try
        {
            st = DSApi.context().session().connection().createStatement();
            ResultSet rs = st.executeQuery("select min(sequenceid) from dso_journal_entry " +
                "where journal_id = "+getId());
            if (!rs.next())
                throw new EdmException(sm.getString("BladOdczytuDziennika"));

            int result = rs.getInt(1);
            if (rs.wasNull())
                return null;
            else
                return new Integer(result);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            if (st != null) try { st.close(); } catch (Exception e) { }
        }
    }




    /**
     * @return True, je�eli jest to g��wny dziennik (ownerGuid == null).
     */
    public boolean isMain()
    {
        return ownerGuid == null;
    }

    public boolean isIncoming()
    {
        return Journal.INCOMING.equals(journalType);
    }

    public boolean isOutgoing()
    {
        return Journal.OUTGOING.equals(journalType);
    }

    public boolean isInternal()
    {
        return Journal.INTERNAL.equals(journalType);
    }

    /**
     * Zwraca podsumowanie opisu dziennika - nazw� dzia�u i opis.
     * Metoda musi by� wywo�ywana w otwartej sesji.
     * @return
     * @throws EdmException
     */
    public String getSummary() throws EdmException
    {
        String type;
        if (Journal.INCOMING.equals(journalType))
            type = sm.getString("PismaPrzychodzace");
        else if (Journal.OUTGOING.equals(journalType))
            type = sm.getString("PismaWychodzace");
      //  else if (Journal.DAA.equals(journalType))
      //      type = "pisma archiwum agenta";
        else // if (Journal.INTERNAL.equals(journalType))
            type = sm.getString("PimsmaWewnetrzne");
        if(description.equals("Dziennik g��wny"))
        	description = sm.getString("DziennikGlowny");
       return ((ownerGuid != null) ?
    		   (AvailabilityManager.isAvailable("print.journal.printOwnerGuidName")) ?
    				   DSDivision.find(ownerGuid).getName() + " - " + description
    				   : description
        :
            sm.getString("DziennikGlowny")+" - " + description) + " (" + type + ")";
    }

    public String getTypeDescription()
    {
        if (INCOMING.equals(journalType))
            return sm.getString("PismaPrzychodzace");
        else if (OUTGOING.equals(journalType))
            return sm.getString("PismaWychodzace");
        else if (INTERNAL.equals(journalType))
            return sm.getString("PimsmaWewnetrzne");
        else
            return sm.getString("NieznanyRodzaj")+":"+" "+journalType+"]";
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getOwnerGuid()
    {
        return ownerGuid;
    }

    public void setOwnerGuid(String ownerGuid)
    {
        this.ownerGuid = ownerGuid;
    }

    public String getJournalType()
    {
        return journalType;
    }

    public void setJournalType(String journalType)
    {
        this.journalType = journalType;
    }

    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId) throws EdmException
    {
        if (sequenceId == null)
            throw new NullPointerException("sequenceId");

        if (sequenceId.equals(this.sequenceId))
            return;

        if (sequenceId.compareTo(this.sequenceId) < 0){
            throw new EdmException(sm.getString("NieMoznaZmieniacNumeruKolejnegoPismaNaMniejszyNizObecny"));
        }
        

        if (!DSApi.context().hasPermission(DSPermission.DZIENNIK_ZMIANA_NUMERU_KOLEJNEGO)||!(
        		DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)||(
				DSApi.context().hasPermission(DSPermission.DZIENNIK_KOMORKA_PODGLAD) && 
				DSApi.context().getDSUser().getDivisionsAsList().contains(DSDivision.find(ownerGuid)))))
            throw new EdmException(sm.getString("BrakUprawnienDoZmianyNumeruKolejnegoWdzienniku"));

        this.sequenceId = sequenceId;
    }

    public String getSymbol()
    {
        return symbol;
    }

    public void setSymbol(String symbol)
    {
        this.symbol = symbol;
    }

    public String getLparam()
    {
        return lparam;
    }

    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }

    public Long getWparam()
    {
        return wparam;
    }

    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public boolean isArchived()
    {
        return archived;
    }

    public void setArchived(boolean archived)
    {
        this.archived = archived;
    }

    public boolean isClosed()
    {
        return closed;
    }

    private void setClosed(boolean closed)
    {
        this.closed = closed;
    }

    public int getCyear()
    {
        return cyear;
    }

    public void setCyear(int cyear)
    {
        this.cyear = cyear;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Journal)) return false;

        final Journal journal = (Journal) o;

        if (id != null ? !id.equals(journal.getId()) : journal.getId() != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public String toString()
    {
        return getClass().getName()+"[id="+id+"]";
    }

    /* Lifecycle */

    public boolean onSave(Session session) throws CallbackException
    {
        if (author == null)
        {
            author = DSApi.context().getPrincipalName();
        }

        if (ctime == null)
        {
            ctime = new Date();
        }

        return false;
    }

    public boolean onUpdate(Session session) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session session) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session session, Serializable serializable)
    {
    }

	public Integer getDailySequenceId() {
		return dailySequenceId;
	}

	public void setDailySequenceId(Integer dailySequenceId) {
		this.dailySequenceId = dailySequenceId;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}
	
	public class JournalSequenceBean
	{
		Integer sequenceId;
		Integer dailySequenceId;
		
		public Integer getDailySequenceId() {
			return dailySequenceId;
		}
		public void setDailySequenceId(Integer dailySequenceId) {
			this.dailySequenceId = dailySequenceId;
		}
		public Integer getSequenceId() {
			return sequenceId;
		}
		public void setSequenceId(Integer sequenceId) {
			this.sequenceId = sequenceId;
		}
	}
	
	
}
