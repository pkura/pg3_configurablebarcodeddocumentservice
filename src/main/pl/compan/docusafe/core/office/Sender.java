package pl.compan.docusafe.core.office;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Sender.java,v 1.2 2006/02/06 16:05:55 lk Exp $
 */
public class Sender extends Person
{
    public Sender fromJSON(String json) throws EdmException
    {
        return (Sender) readJSON(json);
    }
}
