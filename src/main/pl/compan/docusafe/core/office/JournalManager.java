package pl.compan.docusafe.core.office;

import com.lowagie.text.*;
import org.apache.commons.lang.ArrayUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.p4.P4Helper;
import pl.compan.docusafe.parametrization.p4.SignatureUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.office.CurrentDayAction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;


public class JournalManager 
{
	private static final Logger log = LoggerFactory.getLogger(JournalManager.class);
	private static List<String> inPrintColumn;
	private static int[] inWidths;
	private static List<String> inPrintMainColumn;
	private static int[] inMainWidths;
	private static List<String>outPrintColumn;
	private static int[]  outWidths;
	private static List<String> intPrintColumn;
	private static int[]  intWidths;
	private static List<String> additionalOutPrintColumn;
	private static int[]  additionalOutPrintWidths;
	private static final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	private static final DateFormat shortDateFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	/**Wydruk dzialowych */
	private static String [] inPrintDefaultColumn = {"officenumber","sequenceId","incomingdate","referenceId","senderSummary","summary","recipientSummary","Zal.","divisionName"};
	/**Wydruk glownych */
	private static String [] inMainPrintDefaultColumn = {"officenumber","incomingdate","referenceId","senderSummary","summary","recipientSummary","Zal.","divisionName","Podpis"};
	/**Wyszukiwarka glownych */
	private static String [] outMainDefaultColumn = {"officenumber","incomingdate","senderSummary","summary","recipientSummary","divisionName"};
	private static String [] intMainDefaultColumn = {"officenumber","incomingdate","creatingUser","summary","divisionName"};
	private static String [] inMainDefaultColumn = {"officenumber","incomingdate","referenceId","senderSummary","summary","recipientSummary","divisionName","delivery"};
	/**Wyszukiwarka dzialowych */
	private static String [] outDefaultColumn = {"officenumber","sequenceId","incomingdate","senderSummary","summary","recipientSummary","divisionName"};
	private static String [] intDefaultColumn = {"officenumber","sequenceId","incomingdate","creatingUser","summary","divisionName"};
	private static String [] inDefaultColumn = {"officenumber","sequenceId","referenceId","senderSummary","summary","recipientSummary","divisionName"};
	/**Wydruk dodatkowy definiowany w pliku printJournal.config */
	private static String [] additionalOutPrintDefaultColumn = {"officenumber","incomingdate","senderSummary","recipientSummary","summary","Podpis"};

	static
	{
		if(Configuration.hasExtra("business"))
			inMainWidths = (new int[] { 5,    8, 10, 18, 17, 17, 3, 22 }); 
		else
			inMainWidths = (new int[] { 4,    8, 10, 14, 17, 14, 3, 20, 10});
		
		inWidths = 		   (new int[] { 5, 5, 8, 10, 15, 16, 15, 3, 23 });
		
		//additionalInPrintWidths = 		   (new int[] { 2,7,15,10,12,10 });
		
		additionalOutPrintWidths = 		   (new int[] { 2,7,15,10,12,10 });
		
		inPrintColumn = new ArrayList<String>(Arrays.asList(inPrintDefaultColumn));
		inPrintMainColumn =  new ArrayList<String>(Arrays.asList(inMainPrintDefaultColumn));
		 if (Configuration.hasExtra("business"))
			 inPrintMainColumn.remove("Podpis");
		 
		//inPrintMainColumn.add("delivery");
		 
		File config = new File(Docusafe.getHome(), "printJournal.config");
    	if(config.exists())
    	{
    		try
    		{
	    		Properties configuration = new Properties();
	    		configuration.load(new FileInputStream(config));
	    		if(configuration.getProperty("inColumns") != null && configuration.getProperty("inColumns").length() > 0)
	    			inPrintColumn =  Arrays.asList(configuration.getProperty("inColumns").split(","));	    		
	    		if(configuration.getProperty("inMainColumn") != null && configuration.getProperty("inMainColumn").length() > 0)
	    			inPrintMainColumn = Arrays.asList(configuration.getProperty("inMainColumn").split(","));
	        	String inWidthsProp = configuration.getProperty("inWidths");
	        	
	        	
	        	if(inWidthsProp != null && inWidthsProp.length() > 0)
	        	{
		        	String[] tmpTab = inWidthsProp.split(",");
		        	inWidths = new int[tmpTab.length];
		        	for (int i = 0; i < tmpTab.length; i++)
		        	{
		        		inWidths[i] = Integer.parseInt(tmpTab[i]);
					}
	        	}
	        	String inMainWidthsProp = configuration.getProperty("inMainWidths");
	        	if(inMainWidthsProp != null && inMainWidthsProp.length() > 0)
	        	{
		        	String[] tmpTab = inMainWidthsProp.split(",");
		        	inMainWidths = new int[tmpTab.length];
		        	for (int i = 0; i < tmpTab.length; i++)
		        	{
		        		inMainWidths[i] = Integer.parseInt(tmpTab[i]);
					}
	        	}
	        	
	        	if(configuration.getProperty("additionalOutPrintColumn") != null && configuration.getProperty("additionalOutPrintColumn").length() > 0)
	    			additionalOutPrintColumn =  Arrays.asList(configuration.getProperty("additionalOutPrintColumn").split(","));
	        	if(configuration.getProperty("additionalOutPrintWidths") != null && configuration.getProperty("additionalOutPrintWidths").length() > 0)
	        	{
	        		List<String> temp = Arrays.asList(configuration.getProperty("additionalOutPrintWidths").split(","));
	        		additionalOutPrintWidths = new int[temp.size()];
		        	for (int i = 0; i < temp.size(); i++)
	    			{
	    				additionalOutPrintWidths[i] = Integer.valueOf(temp.get(i));
	    			}
	        	}
	        }
    		catch (Exception e) 
    		{
				log.error(e.getMessage(),e);
			}
    	}
    	if(AvailabilityManager.isAvailable("p4HandWrittenSignature")) {
    		int index = ArrayUtils.indexOf(inMainDefaultColumn, "recipientSummary");
    		inMainDefaultColumn = (String[]) ArrayUtils.add(inMainDefaultColumn, index+1, "recipientUser");
		}
	}
	
	public static List<TableColumn> getTableColumns(Journal journal,String searchLink,StringManager sm) 
	{
		List<TableColumn> result = new ArrayList<TableColumn>();
		String[] columns = null;
		if(journal.isMain())
		{
			if(journal.isIncoming())
			{
				columns = getInMainDefaultColumn();
			}
			else if(journal.isInternal())
			{
				columns = getIntMainDefaultColumn(); 
			}
			else
			{
				columns = getOutMainDefaultColumn(); 
			}
		}
		else
		{
			if(journal.isIncoming())
			{
                try
                {
                    if (CurrentDayAction.P_4_CURRENT_DAY && P4Helper.journaGuidOwner().equalsIgnoreCase(P4Helper.TASMOWA_GUID))
                    {
                        columns = P4Helper.getColumn();
                    }
                    else
                        columns = getInDefaultColumn();
                }
                catch (EdmException e)
                {
                    log.error(e.getMessage(), e);
                    columns = getInDefaultColumn();
                }
            }
			else if(journal.isInternal())
			{
				columns = getIntDefaultColumn(); 
			}
			else
			{
				columns = getOutDefaultColumn(); 
			}
		}
		if(journal.isOutgoing())
		result.add(new TableColumn("","Etykieta",null,null));
		for (String col : columns) 
		{
			String colName = null;
			if(col.equals("referenceId"))
				colName = sm.getString("Znak");
            else if (col.equals("incomingdate") && journal.isOutgoing())
                colName = sm.getString("dataNadania");
			else
				colName = sm.getString(col);
			result.add(new TableColumn(col,colName,searchLink+"&ascending=false&sortBy="+col,searchLink+"&ascending=true&sortBy="+col));
		}
		return result;
	}
	
	
	public static Cell getCellTitleByName(String cellName,Font font,StringManager sm) throws BadElementException
	{
		if(cellName.equals("lp"))
		{
		  Cell _ko = new Cell(new Phrase(sm.getString("lp"), font));
          _ko.setHorizontalAlignment(Cell.ALIGN_CENTER);
          _ko.setRowspan(2);
          return _ko;
		}
		else if(cellName.equals("documentId"))
		{
		  Cell _id = new Cell(new Phrase(sm.getString("documentId"), font));
          _id.setHorizontalAlignment(Cell.ALIGN_CENTER);
          _id.setRowspan(2);
          return _id;
		}
		else if(cellName.equals("officenumber"))
		{
		  Cell _ko = new Cell(new Phrase(sm.getString("officenumber"), font));
          _ko.setHorizontalAlignment(Cell.ALIGN_CENTER);
          _ko.setRowspan(2);
          return _ko;
		}
		else if(cellName.equals("caseSign"))
		{
		  Cell _ko = new Cell(new Phrase(sm.getString("Znak sprawy"), font));
          _ko.setHorizontalAlignment(Cell.ALIGN_CENTER);
          _ko.setRowspan(2);
          return _ko;
		}
		else if(cellName.equals("sequenceId"))
		{
			Cell _lp = new Cell(new Phrase(sm.getString("sequenceId"), font));
            _lp.setHorizontalAlignment(Cell.ALIGN_CENTER);
            _lp.setRowspan(2);
            return _lp;
		}
		else if(cellName.equals("incomingdate"))
		{
			Cell _data = new Cell(new Phrase(sm.getString("incomingdate"), font));
	          _data.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _data.setRowspan(2);
			return _data;
		}
		else if(cellName.equals("referenceId"))
		{
			Cell _znak = new Cell(new Phrase(sm.getString("referenceId"), font));
	          _znak.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _znak.setRowspan(2);
			return _znak;
		}
		else if(cellName.equals("documentDate"))
		{
	          Cell _znak = new Cell(new Phrase(sm.getString("documentDate"), font));
	          _znak.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _znak.setRowspan(2);
			return _znak;
		}
		else if(cellName.equals("DataKorespondencji"))
		{
	          Cell _znak = new Cell(new Phrase(sm.getString("DataKorespondencji"), font));
	          _znak.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _znak.setRowspan(2);
			return _znak;
		}
		else if(cellName.equals("senderSummary"))
		{
	          Cell _od = new Cell(new Phrase(sm.getString("senderSummary"), font));
	          _od.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _od.setRowspan(2);
			return _od;
		}
		else if(cellName.equals("summary"))
		{
	          Cell _sum = new Cell(new Phrase(sm.getString("summary"), font));
	          _sum.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _sum.setRowspan(2);
			return _sum;
		}
		else if(cellName.equals("Remarks"))
		{
	          Cell _do = new Cell(new Phrase(sm.getString("Uwagi"), font));
	          _do.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _do.setRowspan(2);
			return _do;
		}
		else if(cellName.equals("recipientSummary"))
		{
	          Cell _do = new Cell(new Phrase(sm.getString("recipientSummary"), font));
	          _do.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _do.setRowspan(2);
			return _do;
		}
		else if(cellName.equals("recipientName"))
		{
	          Cell _do = new Cell(new Phrase(sm.getString("ODBIORCAImieInazwiskoLubZazwa"), font));
	          _do.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _do.setRowspan(2);
			return _do;
		}
		else if(cellName.equals("recipientAdress"))
		{
	          Cell _do = new Cell(new Phrase(sm.getString("DokladneMiejsceDoreczenia"), font));
	          _do.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _do.setRowspan(2);
			return _do;
		}
		else if(cellName.equals("Zal."))
		{
	          Cell _zal = new Cell(new Phrase(sm.getString("Zal."), font));
	          _zal.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _zal.setRowspan(2);
			return _zal;
		}
		else if(cellName.equals("divisionName"))
		{
			Cell _uw = null;
			if(AvailabilityManager.isAvailable("printJournal.Dzial/wydzialZamiastWydzial")){
				_uw = new Cell(new Phrase(sm.getString("Dzial/wydzial"), font));
			}
			else if(AvailabilityManager.isAvailable("printJournal.dzialZamiastWydzial")){
				_uw = new Cell(new Phrase(sm.getString("Dzial"), font));
			}
			else{
				_uw = new Cell(new Phrase(sm.getString("divisionName"), font));
			}
	          _uw.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _uw.setRowspan(2);
			return _uw ;
		}
		else if(cellName.equals("Podpis"))
		{
            Cell _podpis = new Cell(new Phrase(sm.getString("Podpis"), font));
            _podpis.setHorizontalAlignment(Cell.ALIGN_CENTER);
            _podpis.setRowspan(2);
			return _podpis;
		}
		else if(cellName.equals("recipientDate"))
		{
			Cell _recDat = new Cell(new Phrase(sm.getString("recipientDate"), font));
			_recDat.setHorizontalAlignment(Cell.ALIGN_CENTER);
			_recDat.setRowspan(2);
			return _recDat;
		}
		else if(cellName.equals("recipientUser"))
		{
			Cell _recu = new Cell(new Phrase(sm.getString("recipientUser"), font));
			_recu.setHorizontalAlignment(Cell.ALIGN_CENTER);
			_recu.setRowspan(2);
			return _recu;
		}
		else if(cellName.equals("NrPrzesylkiRejestrowanej"))
		{
            Cell _nrpr = new Cell(new Phrase(sm.getString("NrPrzesylkiRejestrowanej"), font));
            _nrpr.setHorizontalAlignment(Cell.ALIGN_CENTER);
            _nrpr.setRowspan(2);
			return _nrpr;
		}
		else if(cellName.equals("delivery"))
		{
            Cell _nrpr = new Cell(new Phrase(sm.getString("delivery"), font));
            _nrpr.setHorizontalAlignment(Cell.ALIGN_CENTER);
            _nrpr.setRowspan(2);
			return _nrpr;
		}
		// Podw�jne kolumny
		else if(cellName.equals("Masa"))
		{
			Cell _weight = new Cell(new Phrase(sm.getString("Masa"), font));
            _weight.setHorizontalAlignment(Cell.ALIGN_CENTER);
            _weight.setColspan(2);
            return _weight;
		}
		else if(cellName.equals("Oplata"))
		{
			if(AvailabilityManager.isAvailable("printOutJournal.ams.special")){
				Cell _fee = new Cell(new Phrase(sm.getString("Oplata"), font));
				_fee.setHorizontalAlignment(Cell.ALIGN_CENTER);
				_fee.setRowspan(2);
				return _fee;
			} else {
				Cell _fee = new Cell(new Phrase(sm.getString("Oplata"), font));
				_fee.setHorizontalAlignment(Cell.ALIGN_CENTER);
				_fee.setColspan(2);
				return _fee;
			}
			
		}
		else if(cellName.equals("KwotaPobrania"))
		{
			Cell _cost = new Cell(new Phrase(sm.getString("KwotaPobrania"), font));
	        _cost.setHorizontalAlignment(Cell.ALIGN_CENTER);
	        _cost.setRowspan(2);
	        return _cost;
		}
		else if(cellName.equals("doc_number"))
		{
			Cell _znak = new Cell(new Phrase(sm.getString("NumerDokumentu"), font));
	          _znak.setHorizontalAlignment(Cell.ALIGN_CENTER);
	          _znak.setRowspan(2);
			return _znak;
		}
		else
		{
			throw new BadElementException("Bledna kolumna " +cellName);
		}
	}
	
	public static void getCellSecRowTitleByName(Table table, String cellName,Font font,StringManager sm) throws Exception
	{
		Cell _tmp;
		if(cellName.equals("Masa2"))
		{
			_tmp = new Cell(new Phrase(sm.getString("kg"), font));
            _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(_tmp);

            _tmp = new Cell(new Phrase(sm.getString("g"), font));
            _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(_tmp);
		}
		else if(cellName.equals("Oplata2"))
		{
			_tmp = new Cell(new Phrase(sm.getString("zl"), font));
            _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(_tmp);

            _tmp = new Cell(new Phrase(sm.getString("gr"), font));
            _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(_tmp);
		}
//		else if(cellName.equals("Kwota2"))
//		{
//            _tmp = new Cell(new Phrase(sm.getString("zl"), font));
//            _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
//            table.addCell(_tmp);
//
//            _tmp = new Cell(new Phrase(sm.getString("gr"), font));
//            _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
//            table.addCell(_tmp);
//		}
	}
	
	public static void getCellValueByName(Table table, Journal.EntryBean bean,String cellName,Font font,StringManager sm, Integer lp) throws Exception
	{
		if(cellName.equals("officenumber"))
		{
			table.addCell(new Phrase(bean.getOfficeNumber(), font));
		}
		else if(cellName.equals("documentId"))
		{
			table.addCell(new Phrase(bean.getDocumentId().toString(), font));
		}
		else if(cellName.equals("sequenceId"))
		{
			table.addCell(new Phrase(String.valueOf(bean.getSequenceId()), font));
		}
		else if(cellName.equals("caseSign"))
		{
			  if (bean.getCaseSign() != null)
	          {
				table.addCell(new Phrase(bean.getCaseSign(), font));
	          }
	          else
	          {
	        	  table.addCell( new Phrase("", font));
	          }
		}
		else if(cellName.equals("incomingdate"))
		{
			if (bean.getIncomingDate() != null)
	          {
	              synchronized (dateFormat)
	              {
	            	  table.addCell(new Phrase(dateFormat.format(bean.getIncomingDate()), font));
	              }
	          }
	          else
	          {
	        	  table.addCell(new Phrase("", font));
	          }
		}
		else if(cellName.equals("NrPrzesylkiRejestrowanej"))
		{
			  if (bean.getPostalregnumber() != null)
	          {
				  table.addCell( new Phrase(bean.getPostalregnumber(), font));
	          }
	          else
	          {
	        	  table.addCell( new Phrase("", font));
	          }
		}
		else if(cellName.equals("referenceId"))
		{
			if (bean.getReferenceId() != null)
	          {
				  table.addCell( new Phrase(bean.getReferenceId(), font));
	          }
	          else
	          {
	        	  table.addCell( new Phrase("", font));
	          }
		}
		else if(cellName.equals("documentDate"))
		{
	          StringBuilder data = new StringBuilder();
	          if(AvailabilityManager.isAvailable("printOutJournal.ams.special") && bean.getDocumentDate() != null){
	        	  synchronized (shortDateFormat)
	              {
	            	  data.append(shortDateFormat.format(bean.getDocumentDate()));
	              }
	          }
	          else if (bean.getDocumentDate() != null)
	          {
	              synchronized (dateFormat)
	              {
	            	  data.append(dateFormat.format(bean.getDocumentDate()));
	              }
	          }
	          table.addCell(new Phrase(data.toString(), font));
		}
		else if(cellName.equals("senderSummary"))
		{
	          // senderSummary
	          if (bean.getSenderSummary() != null)
	          {
	        	  table.addCell( new Phrase(bean.getSenderSummary(), font));
	          }
	          else
	          {
	        	  table.addCell(new Phrase("", font));
	          }
		}                        
		else if(cellName.equals("Remarks"))
		{
			String remarks = "";
			OfficeDocument officeDocument = OfficeDocument.find(bean.getDocumentId());
			if(officeDocument != null && officeDocument instanceof OutOfficeDocument){
				OutOfficeDocument document = ((OutOfficeDocument)officeDocument);
				if (AvailabilityManager.isAvailable("printJournal.ZpoAndGaugeAndPrInRemark")) {
					if (document.getZpo() != null && document.getZpo().equals(Boolean.TRUE)) {
						remarks = sm.getString("ZPO");
					}
					
					if (document.getGauge() != null) {
						Integer id = document.getGauge();
						String val = Gauge.find(id).getTitle();
						if (remarks.contains(sm.getString("ZPO"))) {
							remarks += ", ";
						}
						remarks += "Gabaryt " + val;
					}
					
					if (document.getDelivery() != null) {
						OutOfficeDocumentDelivery delivery = document.getDelivery();
						String deliveryName = delivery.getName().toLowerCase();
						if (deliveryName.contains("priorytet")) {
							if (remarks.contains(sm.getString("ZPO")) || remarks.contains("Gabaryt")) {
								remarks += ", ";
							}
							remarks += sm.getString("PR");
						}
					}
				}
				
				if (AvailabilityManager.isAvailable("printJournal.ZpoAndGaugeAndPrInRemarkAms")) {
					remarks = "";
					if(document.getDelivery() != null) {
						OutOfficeDocumentDelivery delivery = document.getDelivery();
						String deliveryName = delivery.getName().toLowerCase();
						if (deliveryName.contains("priorytet") && document.getZpo() != null && document.getZpo().equals(Boolean.FALSE)) {
							remarks = sm.getString("PR");
						} else if (deliveryName.contains("priorytet") && document.getZpo() != null && document.getZpo().equals(Boolean.TRUE)) {
							remarks = sm.getString("ZPO");
						}  
					}
				}
				
				if(remarks.length() > 0){
					table.addCell( new Phrase(remarks, font));
				} else {
					table.addCell(new Phrase("", font));
				}
			} else {
				if (bean.getRemarks() != null)
		          {
		        	  table.addCell( new Phrase(bean.getRemarks(), font));
		          }
		          else
		          {
		        	  table.addCell(new Phrase("", font));
		          }
			}
	          
		}
		else if(cellName.equals("Masa"))
		{
			table.addCell(new Phrase(bean.getWeightKg(), font));
			table.addCell(new Phrase(bean.getWeightG(), font));
		}
		else if(cellName.equals("Oplata"))
		{
			OfficeDocument officeDocument = OfficeDocument.find(bean.getDocumentId());
			if(officeDocument != null && officeDocument instanceof OutOfficeDocument && AvailabilityManager.isAvailable("printOutJournal.ams.special")){
				table.addCell(new Phrase(bean.getStampFeeZl() + "." + bean.getStampFeeGr(), font));
			} else {
				table.addCell(new Phrase(bean.getStampFeeZl(), font));
				table.addCell(new Phrase(bean.getStampFeeGr(), font));
			}
			
		}
		else if(cellName.equals("summary"))
		{
//			 opis
			table.addCell(new Phrase(bean.getSummary(), font));
		}
		else if(cellName.equals("delivery"))
		{
			table.addCell(new Phrase(bean.getDelivery(), font));
		}
		else if(cellName.equals("recipientSummary"))
		{
	          // odbiorca
	          if (bean.getRecipientSummary() != null)
	          {
	        	  table.addCell(new Phrase(bean.getRecipientSummary(), font));
	          }
	          else
	          {
	        	  table.addCell(new Phrase("", font));
	          }
		}
		else if(cellName.equals("recipientName"))
		{
	          if (bean.getRecipientSummary() != null)
	          {
	        	  table.addCell(new Phrase(bean.getRecipientName(), font));
	          }
	          else
	          {
	        	  table.addCell(new Phrase("", font));
	          }
		}
		else if(cellName.equals("recipientAdress"))
		{
	          if (bean.getRecipientSummary() != null)
	          {
	        	  table.addCell(new Phrase(bean.getRecipientAdress(), font));
	          }
	          else
	          {
	        	  table.addCell(new Phrase("", font));
	          }
		}
		else if(cellName.equals("Zal."))
		{
	          // zal�czniki
			table.addCell(new Phrase(String.valueOf(bean.getNumAttachments()), font));
		}
		else if(cellName.equals("divisionName"))
		{

	          //ZMIANY
	          // uwagi
	          String pom="";
	           if (bean.getDivisionName() != null) {
	        	   if(AvailabilityManager.isAvailable("printJournal.dzialZamiastWydzial"))
	        		   pom="Dzia�: "+bean.getDivisionName();
	        	   else
	        		   pom="Wydzia�: "+bean.getDivisionName();
	           }

	           if(bean.getUserName() != null) 
	           {
	               DSUser user =  DSUser.findByUsername(bean.getUserName());
	               DSDivision[] divisions =  user.getDivisions();
	               String position = null;
	               for (DSDivision div : divisions)
	               {
	                   if(div.isPosition())
	                   {
	                      position = div.getName();
	                      break;
	                   }
	               }
	               pom = pom + " / ";
	               if(position!=null)
	                   pom = pom  + position + ": ";
	               pom = pom + user.asFirstnameLastname();
	           }
	           table.addCell(new Phrase(pom, font));
		}
		else if(cellName.equals("Podpis"))
		{
			table.addCell(new Phrase("", font));
		}
		else if (cellName.equals("recipientDate"))
		{
			Cell cell = new Cell();
			cell.setVerticalAlignment(Cell.ALIGN_TOP);

			StringBuilder data = new StringBuilder("");
			Long masterDocumentId = bean.getMasterDocumentId();
			if (masterDocumentId != null)
			{

				InOfficeDocument masterDoc = (InOfficeDocument) InOfficeDocument.find(masterDocumentId, false);

				if (masterDoc != null)
				{
					Attachment att = masterDoc.getAttachmentByCn(SignatureUtils.SIGNATURE_ATTACHMENT);
					AttachmentRevision rev = att.getMostRecentRevision();
					synchronized (dateFormat)
					{
						data.append(dateFormat.format(rev.getCtime()));
					}
				}
			}
			table.addCell(new Phrase(data.toString(), font));
		}
		else if(cellName.equals("recipientUser"))
		{
			Cell cell = new Cell();
			cell.setVerticalAlignment(Cell.ALIGN_TOP);
			
			try {
				Long masterDocumentId = bean.getMasterDocumentId();
				if(masterDocumentId != null) {
					Image img = SignatureUtils.getSignatureImage(masterDocumentId);
					if(img != null) {
						img.setAlignment(Image.ALIGN_BASELINE);
						cell.add(img);
					}
				}
			} catch(EdmException e) {
				log.error(e.getMessage(), e);
			}
			
			try {
				Chunk userChunk = new Chunk("", font);
				Map<String, String> recipientUser = bean.getRecipientUser();
				if(recipientUser != null) {
					userChunk.append(recipientUser.get("firstname"))
						.append(" ").append(recipientUser.get("lastname"));
						//.append(" (").append(recipientUser.get("name")).append(")");
				}
				cell.add(userChunk);
			} catch(NullPointerException e) {
				log.error(e.getMessage(), e);
			}
			
			table.addCell(cell);
		}
		else if(cellName.equals("lp"))
		{
			table.addCell(new Phrase(""+lp, font));
		} else if(cellName.equals("KwotaPobrania"))
		{
			table.addCell(new Phrase(bean.getWeightKg(), font));
		} 
		else if(cellName.equals("doc_number")){
			String numerDokumentu = OfficeDocument.find(bean.getDocumentId()).getFieldsManager().getStringValue("doc_number");
			
			if(numerDokumentu!=null)
				table.addCell( new Phrase(numerDokumentu, font));
			else
				table.addCell( new Phrase("", font));
		}
        else
  		{
  			throw new BadElementException("Bledna kolumna "+cellName);
  		}
	}


	public static String[] getInMainPrintDefaultColumn() {
		return inMainPrintDefaultColumn;
	}


	public static void setInMainPrintDefaultColumn(String[] inMainPrintDefaultColumn) {
		JournalManager.inMainPrintDefaultColumn = inMainPrintDefaultColumn;
	}


	public static int[] getInMainWidths() {
		return inMainWidths;
	}


	public static void setInMainWidths(int[] inMainWidths) {
		JournalManager.inMainWidths = inMainWidths;
	}


	public static List<String> getInPrintColumn() {
		return inPrintColumn;
	}


	public static void setInPrintColumn(List<String> inPrintColumn) {
		JournalManager.inPrintColumn = inPrintColumn;
	}


	public static String[] getInPrintDefaultColumn() {
		return inPrintDefaultColumn;
	}


	public static void setInPrintDefaultColumn(String[] inPrintDefaultColumn) {
		JournalManager.inPrintDefaultColumn = inPrintDefaultColumn;
	}


	public static List<String> getInPrintMainColumn() {
		return inPrintMainColumn;
	}


	public static void setInPrintMainColumn(List<String> inPrintMainColumn) {
		JournalManager.inPrintMainColumn = inPrintMainColumn;
	}


	public static List<String> getIntPrintColumn() {
		return intPrintColumn;
	}


	public static void setIntPrintColumn(List<String> intPrintColumn) {
		JournalManager.intPrintColumn = intPrintColumn;
	}


	public static int[] getIntWidths() {
		return intWidths;
	}


	public static void setIntWidths(int[] intWidths) {
		JournalManager.intWidths = intWidths;
	}


	public static int[] getInWidths() {
		return inWidths;
	}


	public static void setInWidths(int[] inWidths) {
		JournalManager.inWidths = inWidths;
	}


	public static String[] getOutMainDefaultColumn() {
		return outMainDefaultColumn;
	}


	public static void setOutMainDefaultColumn(String[] outMainDefaultColumn) {
		JournalManager.outMainDefaultColumn = outMainDefaultColumn;
	}


	public static List<String> getOutPrintColumn() {
		return outPrintColumn;
	}


	public static void setOutPrintColumn(List<String> outPrintColumn) {
		JournalManager.outPrintColumn = outPrintColumn;
	}


	public static int[] getOutWidths() {
		return outWidths;
	}


	public static void setOutWidths(int[] outWidths) {
		JournalManager.outWidths = outWidths;
	}


	public static String[] getInDefaultColumn() {
		return inDefaultColumn;
	}


	public static void setInDefaultColumn(String[] inDefaultColumn) {
		JournalManager.inDefaultColumn = inDefaultColumn;
	}


	public static String[] getInMainDefaultColumn() {
		return inMainDefaultColumn;
	}


	public static void setInMainDefaultColumn(String[] inMainDefaultColumn) {
		JournalManager.inMainDefaultColumn = inMainDefaultColumn;
	}


	public static String[] getIntDefaultColumn() {
		return intDefaultColumn;
	}


	public static void setIntDefaultColumn(String[] intDefaultColumn) {
		JournalManager.intDefaultColumn = intDefaultColumn;
	}


	public static String[] getIntMainDefaultColumn() {
		return intMainDefaultColumn;
	}


	public static void setIntMainDefaultColumn(String[] intMainDefaultColumn) {
		JournalManager.intMainDefaultColumn = intMainDefaultColumn;
	}


	public static String[] getOutDefaultColumn() {
		return outDefaultColumn;
	}


	public static void setOutDefaultColumn(String[] outDefaultColumn) {
		JournalManager.outDefaultColumn = outDefaultColumn;
	}


	public static List<String> getAdditionalOutPrintColumn() {
		return additionalOutPrintColumn;
	}


	public static void setAdditionalOutPrintColumn(List<String> additionalOutPrintColumn) {
		JournalManager.additionalOutPrintColumn = additionalOutPrintColumn;
	}


	public static int[] getAdditionalOutPrintWidths() {
		return additionalOutPrintWidths;
	}


	public static void setadditionalOutPrintWidths(int[] additionalOutPrintWidths) {
		JournalManager.additionalOutPrintWidths = additionalOutPrintWidths;
	}


	public static String[] getadditionalOutPrintDefaultColumn() {
		return additionalOutPrintDefaultColumn;
	}


	public static void setadditionalOutPrintDefaultColumn(
			String[] additionalOutPrintDefaultColumn) {
		JournalManager.additionalOutPrintDefaultColumn = additionalOutPrintDefaultColumn;
	}
	
	
}
