package pl.compan.docusafe.core.office;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

/**
 * Uwaga: klasa nie definiuje metod equals() i hashCode(),
 * u�ywane s� metody z klasy Object.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Remark.java,v 1.6 2009/08/12 09:30:04 mariuszk Exp $
 */
public final class Remark implements Cloneable
{
	private Long id;
    private String content;
    private Date ctime;
    private String author;
    private Long documentId;

    public Long getDocumentId()
    {
		return documentId;
	}

	public void setDocumentId(Long documentId)
	{
		this.documentId = documentId;
	}

	/**
     * Prywatny konstruktor u�ywany przez Hibernate.
     */
    private Remark()
    {
    }
    public Remark(String content)
    {
        this.content = content;
        this.author = DSApi.context().getPrincipalName();
        this.ctime = new Date();
    }

    public Remark(String content, String author)
    {
        this.content = content;
        this.author = author;
        this.ctime = new Date();
    }

    public static Remark find(Long id) throws EdmException
    {
    	return Finder.find(Remark.class, id);
    }

    /**
     * Zwraca odrzucenia dokumentu
     * @param Long documentId
     * @param String author
     * @return
     * @throws EdmException 
     */
    public static List<Remark> find(Long documentId, String author) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(Remark.class);
        c.add(Restrictions.eq("documentId", documentId));
        c.add(Restrictions.eq("author", author));
        c.addOrder(Order.asc("ctime"));
        return new ArrayList<Remark>(c.list());
    }
    
    public static List<Remark> documentRemarks(Long id) throws EdmException
    {
		Criteria c = DSApi.context().session().createCriteria(Remark.class);
        c.add(Restrictions.eq("documentId", id));
        c.addOrder(Order.asc("ctime"));
        return new ArrayList<Remark>(c.list());
    }
    
    

    protected Object clone()
    {
        try
        {
            return super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            throw new Error(e.getMessage(), e);
        }
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public Date getCtime()
    {
        return ctime;
    }

    void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public String getAuthor()
    {
        return author;
    }

    private void setAuthor(String author)
    {
        this.author = author;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Remark)) return false;

        final Remark remark = (Remark) o;

        if (author != null ? !author.equals(remark.author) : remark.author != null) return false;
        if (content != null ? !content.equals(remark.content) : remark.content != null) return false;
        if (ctime != null ? !ctime.equals(remark.ctime) : remark.ctime != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (content != null ? content.hashCode() : 0);
        result = 29 * result + (ctime != null ? ctime.hashCode() : 0);
        result = 29 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String toString(){
		return content;
	}
}
