package pl.compan.docusafe.core.office;

/**
 * Rodzaj dokumentu wychodzącego.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OutOfficeDocumentKind.java,v 1.1 2004/08/12 15:30:37 lk Exp $
 */
public class OutOfficeDocumentKind extends OfficeDocumentKind
{

}
