package pl.compan.docusafe.core.office;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.classic.Lifecycle;
import org.hibernate.Session;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

import java.io.Serializable;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OfficeDocumentStatus.java,v 1.8 2006/12/07 13:52:26 mmanski Exp $
 */
public abstract class OfficeDocumentStatus implements Lifecycle
{
    private Integer id;
    private String name;
    private String cn;
    private Integer posn;

    OfficeDocumentStatus()
    {
    }

    protected OfficeDocumentStatus(String name)
    {
        this.name = name;
    }

    public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public abstract boolean canDelete() throws EdmException;

    public void delete() throws EdmException
    {
        if (!canDelete())
            throw new EdmException("Nie mo�na usun�� u�ywanego elementu");

        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public Integer getId()
    {
        return id;
    }

    private void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getPosn()
    {
        return posn;
    }

    public void setPosn(Integer posn)
    {
        this.posn = posn;
    }

    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public final boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof OfficeDocumentStatus)) return false;

        final OfficeDocumentStatus officeDocumentStatus = (OfficeDocumentStatus) o;

        if (id != null ? !id.equals(officeDocumentStatus.getId()) : officeDocumentStatus.getId() != null) return false;

        return true;
    }

    public final int hashCode()
    {
        int result;
        result = (id != null ? id.hashCode() : 0);
        return result;
    }

    public final boolean onSave(Session s) throws CallbackException
    {
        if (id != null)
        {
            posn = id;
        }
        else
        {
            // w bazach, gdzie identyfikatory pochodz� z pola autonumber
            // id b�dzie w tym momencie r�wne null
            posn = Dictionaries.updatePosnOnSave(getClass());
        }
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }
}
