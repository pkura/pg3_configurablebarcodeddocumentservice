package pl.compan.docusafe.core.office;

import java.util.Iterator;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.record.projects.CostKind;
import pl.compan.docusafe.core.users.DSUser;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

public class TopicFoundingSource {
	
	private Long id;
	private String pozycja_planu;
	private String kierownik_kod;
	private String kierownik_imie;
	private String kierownik_nazwisko;
	private String kierownik_konto_oracle;
	private String opiekun_kod;
	private String opiekun_imie;
	private String opiekun_nazwisko;
	private String opiekun_konto_oracle;

    public TopicFoundingSource()
    {

    }

    public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static List<TopicFoundingSource> list() throws EdmException {
		List<TopicFoundingSource> list = Finder.list(TopicFoundingSource.class,
				"id");
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			DSApi.initializeProxy(iter.next());
		}
		return list;
	}
    
    public static TopicFoundingSource find(long id) throws EdmException
    {
        return (TopicFoundingSource) Finder.find(TopicFoundingSource.class, id);
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getManager() {
		return kierownik_imie + " " + kierownik_nazwisko;
	}

	public String getSupervisor() {
		return opiekun_imie + " " + opiekun_nazwisko;
	}

	public String getPozycja_planu() {
		return pozycja_planu;
	}

	public void setPozycja_planu(String pozycja_planu) {
		this.pozycja_planu = pozycja_planu;
	}

	public String getKierownik_konto_oracle() {
		return kierownik_konto_oracle;
	}

	public void setKierownik_konto_oracle(String kierownik_konto_oracle) {
		this.kierownik_konto_oracle = kierownik_konto_oracle;
	}

	public String getOpiekun_konto_oracle() {
		return opiekun_konto_oracle;
	}

	public void setOpiekun_konto_oracle(String opiekun_konto_oracle) {
		this.opiekun_konto_oracle = opiekun_konto_oracle;
	}

}
