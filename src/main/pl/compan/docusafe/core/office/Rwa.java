package pl.compan.docusafe.core.office;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * 1. Tworzenie drzewa
 * select * from rwa where level=1
 * select * from rwa where level=2 and
 *
 * 2. Szukanie kategorii nadrz�dnej dla level=3, rwa=123
 * select * from rwa where level=2 and digit2=2
 *
 * 3. Szukanie kategorii podrz�dnych dla level=2, rwa=12
 * select * from rwa where level=3 and digit2=2 order by digit3
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Rwa.java,v 1.16 2009/03/04 15:04:00 pecet3 Exp $
 */
public class Rwa
{
    private Integer id;
    /**
     * Poziom RWA. 1..5.
     */
    private int level;
    private String digit1;
    private String digit2;
    private String digit3;
    private String digit4;
    private String digit5;
    /**
     * Kategoria archiwalna w kom�rce macierzystej.
     */
    private String acHome;
    /**
     * Kategoria archwiwalna w pozosta�ych kom�rkach.
     */
    private String acOther;
    private String description;
    private String remarks;
    private String lparam;
    private Long wparam;
    private Boolean isActive;

	Rwa()
    {
    }

    public Rwa(String rwa)
    {
        if (rwa == null)
            throw new NullPointerException("rwa");
        String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");
        if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){
	        if (rwa.length() < 2 || rwa.length() > 10)
	            throw new IllegalArgumentException("Niepoprawny kod RWA: "+rwa);
        }else{
        	if (rwa.length() < 1 || rwa.length() > 5)
	            throw new IllegalArgumentException("Niepoprawny kod RWA: "+rwa);        	
        }
        
        for (int i=0; i < rwa.length(); i++)
            if (rwa.charAt(i) < '0' || rwa.charAt(i) > '9')
                throw new IllegalArgumentException("Niepoprawny kod RWA: "+rwa);

        if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){
        	setDigit1(rwa.substring(0, 2));
	        if (rwa.length() > 2)
	            setDigit2(rwa.substring(2, 4));
	        if (rwa.length() > 4)
	            setDigit3(rwa.substring(4, 6));
	        if (rwa.length() > 6)
	            setDigit4(rwa.substring(6, 8));
	        if (rwa.length() > 8)
	            setDigit5(rwa.substring(8, 10));
	
	        setLevel(rwa.length()/2);
        } else {
        	setDigit1(""+rwa.charAt(0));
	        if (rwa.length() > 1)
	            setDigit2(""+rwa.charAt(1));
	        if (rwa.length() > 2)
	            setDigit3(""+rwa.charAt(2));
	        if (rwa.length() > 3)
	            setDigit4(""+rwa.charAt(3));
	        if (rwa.length() > 4)
	            setDigit5(""+rwa.charAt(4));
	
	        setLevel(rwa.length());
        }
    }

    public static Rwa find(Integer id)
        throws EdmException, RwaNotFoundException
    {
        try
        {
            return (Rwa) DSApi.context().session().load(Rwa.class, id);
        }
        catch (ObjectNotFoundException e)
        {
            throw new RwaNotFoundException(id);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Rwa findByCode(String rwaCode)
        throws EdmException, RwaNotFoundException
    {
        if (rwaCode == null)
            throw new NullPointerException("rwaCode");
        if (rwaCode.length() < 1 || rwaCode.length() > 5)
            throw new RwaNotFoundException(rwaCode);
        try
        {
            Integer.parseInt(rwaCode);
        }
        catch (NumberFormatException e)
        {
            throw new RwaNotFoundException(rwaCode);
        }
        String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");

        try
        {
      //      StringBuilder hql = new StringBuilder(
       //         "select r from r in class "+Rwa.class.getName()+" where ");

            Criteria criteria = DSApi.context().session().createCriteria(Rwa.class);
            
            for (int i=0; i < rwaCode.length(); i++)
            {
             //   if (i > 0)
               //     hql.append(" and ");
             ///   hql.append(" digit"+(i+1)+"='"+rwaCode.charAt(i)+"'");
                if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){
                	if((i+1)%2==1){
                		int k = (i+2)/2;
                		criteria.add(Restrictions.eq("digit"+k,rwaCode.substring(i,i+2)));
                	}
                }
                else
                	criteria.add(Restrictions.eq("digit"+(i+1),""+rwaCode.charAt(i)));
                
            }
            if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){
            	for (int i=rwaCode.length()/2; i < 5; i++)
	            {
	               // hql.append(" and digit"+(i+1)+" is null");
	                criteria.add(Restrictions.isNull("digit"+(i+1)));
	            }
            }else{
	            for (int i=rwaCode.length(); i < 5; i++)
	            {
	               // hql.append(" and digit"+(i+1)+" is null");
	                criteria.add(Restrictions.isNull("digit"+(i+1)));
	            }
            }
            List rwas = criteria.list();


          //  List rwas = DSApi.context().session().find(hql.toString());
            if (rwas.size() > 1)
                throw new EdmException("Uszkodzona baza danych, znaleziono dwie " +
                    "kategorie RWA o kodzie "+rwaCode);

            if (rwas.size() > 0)
                return (Rwa) rwas.get(0);
            else
                throw new RwaNotFoundException(rwaCode);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List listRoots(boolean showActive) throws EdmException
    {
        try
        {
        	
        	Criteria criteria = DSApi.context().session().createCriteria(Rwa.class);   
        	criteria.add(Restrictions.eq("level" , 1));        
        	if(!showActive)
        	{
        		criteria.add(Restrictions.eq("isActive" , true)); 
        	}
        	return criteria.list(); 
        	
//            return DSApi.context().session().find(
//                "select r from r in class "+Rwa.class.getName()+
//                " where r.level = 1 order by r.digit1");
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
  
    public static List<Rwa> list(boolean showActive) throws EdmException
    {
        try
        {
        	
        	Criteria criteria = DSApi.context().session().createCriteria(Rwa.class);         
        	if(!showActive)
        	{
        		criteria.add(Restrictions.eq("isActive" , true)); 
        	}
        	return (List<Rwa>)criteria.list(); 
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public void delete() throws EdmException
    {
        if (getChildren(false).size() > 0)
            throw new EdmException("Nie mo�na usun�� kategorii zawieraj�cej podkategorie");
        

        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public String getRwa()
    {
    	String[] digits = getDigits();
        StringBuilder rwa = new StringBuilder(5);
        for (int i=0; i < level; i++)
        {
            rwa.append(digits[i]);
        }
        return rwa.toString();
    }
    
    public boolean canDelete() throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias caseTable = from.createTable(DSApi.getTableName(OfficeCase.class));
        WhereClause where = new WhereClause();
        where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(caseTable.attribute("rwa_id"), getId()));
        SelectClause selectCount = new SelectClause();
        selectCount.addSql("count(*)");
        SelectQuery selectQuery;    

        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby u�ytych rwa na li�cie spraw");
            int count = rs.getInt(1);
            rs.close();
            return count == 0 ? true : false;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }


/*
    public Rwa parent() throws EdmException
    {
        if (level <= 1)
            return null;

        try
        {
            Criteria crit = DSApi.context().session().createCriteria(Rwa.class);

            crit.add(Expression.eq("level", new Integer(level-1)));
            for (int i=1; i < level; i++)
            {
                crit.add(Expression.eq("digit"+i, getDigits()[i-1]));
            }

            //return crit.list();
            return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
*/

    public List getChildren(boolean showActive) throws EdmException
    {
        if (level >= 5)
            return Collections.EMPTY_LIST;
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(Rwa.class);
            if(!showActive) 
            {
            	crit.add(Restrictions.eq("isActive", true));
            }
            crit.add(Expression.eq("level", new Integer(level+1)));

            for (int i=1; i <= level; i++)
            {
                crit.add(Expression.eq("digit"+i, getDigits()[i-1]));
            }

            crit.addOrder(Order.asc("digit"+(level+1)));

            return crit.list();

/*
            List children = DSApi.context().session().find(
                "select r from r in class "+Rwa.class.getName()+
                " where r.level = "+(level+1)+
                " and r.digit"+level+"="+getDigits()[level-1]+
                " order by r.digit"+(level+1));

            return children;
*/
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static void createRoot(String rwaCode, String description, String remarks,
                            String acHome, String acOther)
        throws EdmException
    {
        if (rwaCode == null)
            throw new NullPointerException("rwaCode");
        String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");

        if (DSApi.isContextOpen() &&
            !DSApi.context().hasPermission(DSPermission.RWA_ZMIANA))
            throw new AccessDeniedException("Brak uprawnie� do modyfikacji RWA");
        
        if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){
        	if (rwaCode.length() != 2)
                throw new EdmException("Nieprawid�owy kod g��wnej kategorii RWA: "+rwaCode);	
        }else{
        	if (rwaCode.length() != 1)
                throw new EdmException("Nieprawid�owy kod g��wnej kategorii RWA: "+rwaCode);	
        }

        char lastDigit = rwaCode.charAt(rwaCode.length()-1);
        if (lastDigit < '0' || lastDigit > '9')
            throw new EdmException("Kod RWA musi sk�ada� si� wy��cznie z cyfr");

        try
        {
            findByCode(rwaCode);
            throw new EdmException("Istnieje ju� kategoria RWA maj�ca kod "+rwaCode);
        }
        catch (RwaNotFoundException e)
        {
        }

        Rwa rwa = new Rwa(rwaCode);
        rwa.setDescription(description);
        rwa.setRemarks(remarks);
        rwa.setAcHome(acHome);
        rwa.setAcOther(acOther);

        try
        {
            DSApi.context().session().save(rwa);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void createChild(String rwaCode, String description, String remarks,
                            String acHome, String acOther)
        throws EdmException
    {
        if (rwaCode == null)
            throw new NullPointerException("rwaCode");

        if (DSApi.isContextOpen() &&
            !DSApi.context().hasPermission(DSPermission.RWA_ZMIANA))
            throw new AccessDeniedException("Brak uprawnie� do modyfikacji RWA");
        String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");
        if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){
        	if (!rwaCode.startsWith(getRwa()) || rwaCode.length() != getRwa().length()+2)
                throw new EdmException("W bie��cej kategorii RWA ("+getRwa()+") nie " +
                    "mo�na utworzy� podkategorii z kodem "+rwaCode);

            char lastDigit = rwaCode.charAt(rwaCode.length()-1);
            char beforeLastDigit = rwaCode.charAt(rwaCode.length()-2);
            if ((lastDigit < '0' || lastDigit > '9') && (beforeLastDigit < '0' || beforeLastDigit > '9'))
                throw new EdmException("Kod RWA musi sk�ada� si� wy��cznie z cyfr");
        	
        }else{
        	if (!rwaCode.startsWith(getRwa()) || rwaCode.length() != getRwa().length()+1)
                throw new EdmException("W bie��cej kategorii RWA ("+getRwa()+") nie " +
                    "mo�na utworzy� podkategorii z kodem "+rwaCode);

            char lastDigit = rwaCode.charAt(rwaCode.length()-1);
            if (lastDigit < '0' || lastDigit > '9')
                throw new EdmException("Kod RWA musi sk�ada� si� wy��cznie z cyfr");
        }
        
        try
        {
            findByCode(rwaCode);
            throw new EdmException("Istnieje ju� kategoria RWA maj�ca kod "+rwaCode);
        }
        catch (RwaNotFoundException e)
        {

        }

        Rwa rwa = new Rwa(rwaCode);
        rwa.setDescription(description);
        rwa.setRemarks(remarks);
        rwa.setAcHome(acHome);
        rwa.setAcOther(acOther);
        rwa.setIsActive(true);

        try
        {
            DSApi.context().session().save(rwa);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void setDigitAt(int pos, String c) 
    {
    	switch(pos)
    	{
    		case 1:
    			digit1 = c;
    			break;
    		case 2:
    			digit2 = c;
    			break;
    		case 3:
    			digit3 = c;
    			break;
    		case 4:
    			digit4 = c;
    			break;
    		case 5:
    			digit5 = c;
    			break;
    	}
    }
    /**
     * Musimy byc pewni, �e interesuj�cy nas char jest ustawiony
     * (ustawionych jest .getLevel() pierwszch char-ow)
     * 
     * @param pos - pozycja
     * @return
     */
    public String getDigitAt(int pos) 
    {
        String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");
    	switch(pos)
    	{
    		case 1:
    			return digit1;
    		case 2:
    			return digit2;
    		case 3:
    			return digit3;
    		case 4:
    			return digit4;
    		case 5:
    			return digit5;
    	}
    	if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2"))
    		return "00";
    	else
    		return "0";
    }
    
    public boolean isAncestor(Rwa rwa)
    {
        return rwa.getRwa().startsWith(getRwa());
    }

    private String[] getDigits()
    {
        return new String[] { digit1, digit2, digit3, digit4, digit5 };
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel(int level)
    {
        this.level = level;
    }

    public String getDigit1()
    {
        return digit1;
    }

    private void setDigit1(String digit1)
    {
        String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");
    	if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){
    		if(digit1.length()!=2)
    			digit1 = "0"+digit1;
    	}
		this.digit1 = digit1;	
    }

    public String getDigit2()
    {
        return digit2;
    }

    private void setDigit2(String digit2)
    {
        String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");
    	if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){
    		if(digit2.length()!=2)
    			digit2 = "0"+digit2;
    	}
    	this.digit2 = digit2;	
    }

    public String getDigit3()
    {
        return digit3;
    }

    private void setDigit3(String digit3)
    {
        String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");
    	if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){
    		if(digit3.length()!=2)
    			digit3 = "0"+digit3;
    	}
    	this.digit3 = digit3;	
    }

    public String getDigit4()
    {
        return digit4;
    }

    private void setDigit4(String digit4)
    {
        String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");
    	if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){
    		if(digit4.length()!=2)
    			digit4 = "0"+digit4;
    	}
    	this.digit4 = digit4;	
    }

    public String getDigit5()
    {
        return digit5;
    }

    private void setDigit5(String digit5)
    {
        String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");
    	if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){
    		if(digit5.length()!=2)
    			digit5 = "0"+digit5;
    	}
    	this.digit5 = digit5;	
    }

    public String getAcHome()
    {
        return acHome;
    }

    public void setAcHome(String acHome)
    {
        this.acHome = acHome;
    }

    public String getAcOther()
    {
        return acOther;
    }

    public void setAcOther(String acOther)
    {
        this.acOther = acOther;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getLparam()
    {
        return lparam;
    }

    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }

    public Long getWparam()
    {
        return wparam;
    }

    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }
    
    public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
}
