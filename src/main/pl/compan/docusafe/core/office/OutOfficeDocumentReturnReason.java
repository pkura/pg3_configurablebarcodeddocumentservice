package pl.compan.docusafe.core.office;

import org.hibernate.*;
import org.hibernate.classic.Lifecycle;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OutOfficeDocumentReturnReason.java,v 1.8 2006/12/07 13:52:34 mmanski Exp $
 */
public class OutOfficeDocumentReturnReason implements Lifecycle
{
    private Integer id;
    private Integer posn;
    private String name;

    /**
     * Konstruktor u�ywany przez Hibernate.
     */
    OutOfficeDocumentReturnReason()
    {
    }

    public OutOfficeDocumentReturnReason(String name)
    {
        this.name = name;
    }

    public static List<OutOfficeDocumentReturnReason> list() throws EdmException
    {
        return Finder.list(OutOfficeDocumentReturnReason.class, "posn");
    }

    public static OutOfficeDocumentReturnReason find(Integer id) throws EdmException
    {
        return (OutOfficeDocumentReturnReason) Finder.find(OutOfficeDocumentReturnReason.class, id);
    }

    public static List findByName(String name) throws EdmException
    {
        Criteria crit = DSApi.context().session().createCriteria(OutOfficeDocumentReturnReason.class);
        crit.add(org.hibernate.criterion.Expression.eq("name", name));

        try
        {
            return crit.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public final boolean canDelete() throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias docTable = from.createTable(DSApi.getTableName(OutOfficeDocument.class));

        WhereClause where = new WhereClause();
        where.add(Expression.eq(docTable.attribute("returnreason"), getId()));

        SelectClause selectCount = new SelectClause();
        /*SelectColumn countCol = */selectCount.addSql("count(*)");

        SelectQuery selectQuery;

        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);

            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());

            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby pism");

            int count = rs.getInt(1);

            rs.close();

            return count == 0;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

    public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public final void delete() throws EdmException
    {
        if (!canDelete())
            throw new EdmException("Nie mo�na usun�� u�ywanego elementu");

        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /* Lifecycle */

    public boolean onSave(Session s) throws CallbackException
    {
        if (id != null)
        {
            posn = id;
        }
        else
        {
            // w bazach, gdzie identyfikatory pochodz� z pola autonumber
            // id b�dzie w tym momencie r�wne null
            posn = Dictionaries.updatePosnOnSave(getClass());
        }
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }

    public Integer getId()
    {
        return id;
    }

    private void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getPosn()
    {
        return posn;
    }

    public void setPosn(Integer posn)
    {
        this.posn = posn;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof OutOfficeDocumentReturnReason)) return false;

        final OutOfficeDocumentReturnReason outOfficeDocumentReturnReason = (OutOfficeDocumentReturnReason) o;

        if (id != null ? !id.equals(outOfficeDocumentReturnReason.getId()) : outOfficeDocumentReturnReason.getId() != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }
}
