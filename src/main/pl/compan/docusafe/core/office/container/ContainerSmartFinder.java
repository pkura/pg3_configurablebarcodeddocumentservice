package pl.compan.docusafe.core.office.container;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.rest.RestConfiguration;
import pl.compan.docusafe.rest.solr.ContainerSolrServer;
import pl.compan.docusafe.rest.solr.LuceneQuery;

import static org.apache.solr.client.solrj.SolrQuery.SortClause;

import java.net.MalformedURLException;
import java.util.*;

/**
 * <p>
 *     Bezpo�rednie query do Apache Solr
 * </p>
 */
public class ContainerSmartFinder {

    private static ContainerSmartFinder instance;

    public static ContainerSmartFinder instance() {
        if (instance == null) {
            instance = new ContainerSmartFinder();
        }
        return instance;
    }

    public List<Map<String, Object>> find(String content, Integer limit, Integer offset) throws SolrServerException, MalformedURLException {
        SolrServer solrServer = getSolrServer();
        SolrQuery solrQuery = getSolrQuery(content, offset, limit);
        QueryResponse response = solrServer.query(solrQuery);

        Iterator<SolrDocument> it = response.getResults().iterator();

        Map<String, Map<String, List<String>>> highlightingMap = response.getHighlighting();

        List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

        while (it.hasNext()) {
            SolrDocument solrDocument = it.next();
            Map<String, Object> fieldValueMap = solrDocument.getFieldValueMap();
            Map<String, List<String>> hmap = highlightingMap.get(fieldValueMap.get("id").toString());
            fieldValueMap = addHighlighting(fieldValueMap, hmap);
            results.add(fieldValueMap);
        }

        return results;
    }

    private Map<String, Object> addHighlighting(Map<String, Object> fieldValueMap, Map<String, List<String>> highlighting) {
        Map<String, Object> ret = new HashMap<String, Object>();
        for(String key: fieldValueMap.keySet()) {
            List<String> h = highlighting.get(key);
            if(h != null) {
                ret.put(key, StringUtils.join(h, ", "));
            } else {
                ret.put(key, fieldValueMap.get(key));
            }
        }
        return ret;
    }

    protected SolrQuery getSolrQuery(String content, Integer offset, Integer limit) {
        LuceneQuery query = new LuceneQuery(LuceneQuery.Mode.PARANTHESES);
        query.stack(query.eq("title", content, LuceneQuery.PropertyType.STRING));
        query.stack(query.eq("description", content, LuceneQuery.PropertyType.STRING));
        query.stack(query.eq("officeId", content, LuceneQuery.PropertyType.STRING, LuceneQuery.Mode.QUOTE));
        query.stack(query.eq("year", content, LuceneQuery.PropertyType.STRING));
        query.stack(query.eqLike("divisionguid", content, LuceneQuery.PropertyType.STRING));
        query.orStack();
        String queryString = query.toString();

        SolrQuery solrQuery = new SolrQuery();

        solrQuery.setRows(limit);
        solrQuery.setStart(offset);
        solrQuery.addSort(SortClause.desc("score"));
        solrQuery.addSort(SortClause.desc("openDate"));
        solrQuery.addSort(SortClause.desc("archivedDate"));
        solrQuery.setQuery(queryString);

        solrQuery.setHighlight(true).setHighlightSnippets(2).setParam("hl.fl", "title,description,officeId");

        return solrQuery;
    }

    protected SolrServer getSolrServer() throws MalformedURLException {
        return new RestConfiguration().containerSolrServer();
    }
}
