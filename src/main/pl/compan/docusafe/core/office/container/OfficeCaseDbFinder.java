package pl.compan.docusafe.core.office.container;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.HibernateException;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.*;
import pl.compan.docusafe.web.admin.UserToBriefcaseAction;
import std.lambda;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class OfficeCaseDbFinder<T> extends ContainerFinder<OfficeCase, T> {

    private static FromClause from;
    private static WhereClause where;
    private OrderClause order;
    private TableAlias caseTable;

    private OfficeCase.Query caseQuery;
    private SelectClause selectId;
    private SelectColumn idCol;

    public OfficeCaseDbFinder(OfficeCase.Query query, lambda<OfficeCase, T> mapper, Class<T> mappedClass) {
        super(query, mapper, mappedClass);
    }

    /**
     * Zwraca obiekty typu OfficeCase.
     */
    public void prepareQuery()
    {
        caseQuery = (OfficeCase.Query) query;
        from = new FromClause();
        caseTable = from.createTable("dso_container");

        where = new WhereClause();

        where.add(Expression.eq(caseTable.attribute("discriminator"), "CASE"));

        Junction privilegesOR = Expression.disjunction();
        if(guids.size() > 0) {
            privilegesOR.add(Expression.in(caseTable.attribute("divisionguid"), guids.toArray()));
        }
        if(clerks.size() > 0) {
            privilegesOR.add(Expression.in(caseTable.attribute("clerk"), clerks.toArray()));
        }
        if(! privilegesOR.isEmpty()) {
            where.add(privilegesOR);
        }

       /* // alternatywa do ogranicze� na zwracane sprawy, aby nie pojawi�o si�
        // wyra�enie clerk='A' and clerk='B'.
        else */
        if (caseQuery.getClerk() != null)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < caseQuery.getClerk().length; i++)
                OR.add(Expression.eq(caseTable.attribute("clerk"), TextUtils.trimmedStringOrNull(caseQuery.getClerk()[i])));
            where.add(OR);
        }

        if (caseQuery.getAuthor() != null)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < caseQuery.getAuthor().length; i++)
                OR.add(Expression.eq(caseTable.attribute("author"),TextUtils.trimmedStringOrNull( caseQuery.getAuthor()[i])));
            where.add(OR);
        }
        if (caseQuery.getCdateFrom() != null)
        {
            where.add(Expression.ge(caseTable.attribute("opendate"), caseQuery.getCdateFrom()));
        }

        if (caseQuery.getCdateTo() != null)
        {
            where.add(Expression.le(caseTable.attribute("opendate"), caseQuery.getCdateTo()));
        }

        if (caseQuery.getFinishDateFrom() != null)
        {
            where.add(Expression.ge(caseTable.attribute("finishdate"), caseQuery.getFinishDateFrom()));
        }
        if (caseQuery.getFinishDateTo() != null)
        {
            where.add(Expression.le(caseTable.attribute("finishdate"), caseQuery.getFinishDateTo()));
        }
        if (caseQuery.getArchDateFrom() != null)
        {
            where.add(Expression.ge(caseTable.attribute("archivedDate"), caseQuery.getArchDateFrom()));
        }
        if (caseQuery.getArchDateTo() != null)
        {
            where.add(Expression.le(caseTable.attribute("archivedDate"), caseQuery.getArchDateTo()));
        }
        if (caseQuery.getSeqNum() != null)
        {
            //where.add(Expression.eq(caseTable.attribute("sequenceid"), query.seqNum));
            //wyszukuje odpowiedniego numeru w officeId zak�adaj�c �e znajduje si� on
            //pomi�dzy znakami - i /
            where.add(Expression.like(caseTable.attribute("officeid"), "%-" + caseQuery.getSeqNum() + "/%"));
           /* Junction OR = Expression.conjunction();
            OR.add(Expression.like(caseTable.attribute("officeid"), "%." + query.seqNum + ".%"));
            where.add(OR);*/

        }

        if (caseQuery.getYear() != null)
        {
            where.add(Expression.eq(caseTable.attribute("case_year"), caseQuery.getYear()));
        }

        if (caseQuery.getArchived() != null)
        {
            where.add(Expression.eq(caseTable.attribute("archived"), caseQuery.getArchived().booleanValue()));
        }

        if (caseQuery.getStatusId() != null)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < caseQuery.getStatusId().length; i++)
                OR.add(Expression.eq(caseTable.attribute("status_id"), caseQuery.getStatusId()[i]));
            where.add(OR);

        }

        if (caseQuery.getRecord() != null)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < caseQuery.getRecord().length; i++)
                OR.add(Expression.eq(caseTable.attribute("registries"), caseQuery.getRecord()[i]));
            where.add(OR);

        }

        if (caseQuery.getRwaId() != null)
        {
            where.add(Expression.eq(caseTable.attribute("rwa_id"), caseQuery.getRwaId()));
        }

        if (caseQuery.getOfficeId() != null)
        {
        	where.add(Expression.like(caseTable.attribute("officeid"), this.addPercentExpresion(caseQuery.getOfficeId())));//.eq(caseTable.attribute("officeid"), query.officeId));
        }

        if (caseQuery.getTitle() != null)
        { 
            where.add(Expression.like(caseTable.attribute("title"),this.addPercentExpresion(caseQuery.getTitle())));//.eq(caseTable.attribute("officeid"), query.officeId));
        }
        if(caseQuery.getArchiveStatus() !=null)
        {
            if(caseQuery.getArchiveStatus() ==(-1))
                where.add(Expression.ge(caseTable.attribute("archiveStatus"), 1));
            else if(caseQuery.getArchiveStatus() ==(-2)){
                Junction or=Expression.disjunction();
                or.add(Expression.eq(caseTable.attribute("archiveStatus"), Container.ArchiveStatus.None.ordinal()));
                or.add(Expression.eq(caseTable.attribute("archiveStatus"), Container.ArchiveStatus.ToArchive.ordinal()));
                or.add(Expression.eq(caseTable.attribute("archiveStatus"), null));
                where.add(or);
            }
            else
                where.add(Expression.eq(caseTable.attribute("archiveStatus"), caseQuery.getArchiveStatus()));
        }

        // kolumny, po kt�rych wykonywane jest sortowanie musz� si�
        // znale�� w SELECT
        selectId = new SelectClause();
        idCol = selectId.add(caseTable, "id");

        processOrdering();
    }

    private int getCount() throws EdmException, SQLException {
        ResultSet rs = null;
        try {
            SelectClause selectCount = new SelectClause();
            SelectColumn countCol = selectCount.addSql("count(*)");

            // przy zapytaniu z COUNT(*) nie mo�e by� klauzuli ORDER BY
            // Firebird 1.5 zwraca b��d
            SelectQuery selectQuery = new SelectQuery(selectCount, from, where, null);
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby spraw.");

            return rs.getInt(countCol.getPosition());
        } finally {
            DbUtils.closeQuietly(rs);
        }

    }

    protected void processOrdering() {
        order = new OrderClause();

        for (Iterator iter=query.sortFields().iterator(); iter.hasNext(); )
        {
            AbstractQuery.SortField field = (AbstractQuery.SortField) iter.next();
            // je�eli dodana b�dzie mo�liwo�� sortowania po innych polach,
            // nale�y uaktualni� metod� Query.validSortField()
            if (field.getName().equals("officeId"))
            {
                order.add(caseTable.attribute("officeid"), field.getDirection());
                selectId.add(caseTable, "officeid");
            }
            else if (field.getName().equals("finishDate"))
            {
                order.add(caseTable.attribute("finishdate"), field.getDirection());
                selectId.add(caseTable, "finishdate");
            }
            else if (field.getName().equals("openDate"))
            {
                order.add(caseTable.attribute("opendate"), field.getDirection());
                selectId.add(caseTable, "opendate");
            }
            else if (field.getName().equals("archivedDate"))
            {
                order.add(caseTable.attribute("archivedDate"), field.getDirection());
                selectId.add(caseTable, "archivedDate");
            }
            else if (field.getName().equals("status"))
            {
                order.add(caseTable.attribute("status_id"), field.getDirection());
                selectId.add(caseTable, "status_id");
            }else if(field.getName().equals("title"))
            {
                order.add(caseTable.attribute("title"), field.getDirection());
                selectId.add(caseTable, "title");
            }
        }
    }

    @Override
    protected SearchResults<T> getResult() throws SQLException, EdmException {
        ResultSet rs = null;
        try {

            int totalCount = getCount();

            SelectQuery selectQuery = new SelectQuery(selectId, from, where, order);
            if (caseQuery.getLimit() > 0)
            {
                selectQuery.setMaxResults(caseQuery.getLimit());
                selectQuery.setFirstResult(caseQuery.getOffset());
            }


            rs = selectQuery.resultSet(DSApi.context().session().connection());

            List<T> results = new ArrayList<T>(caseQuery.getLimit());

            if (AvailabilityManager.isAvailable("menu.left.user.to.briefcase")){

                List<Long> list = UserToBriefcaseAction.getPrzypisaneSprawy(DSApi.context().getDSUser().getName());
                for (Long id : list){
                    results.add(mapper.act(DSApi.context().load(OfficeCase.class,id)));
                    totalCount++;
                }
            }


            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(mapper.act(DSApi.context().load(OfficeCase.class, id)));
            }

            rs.close();

            return new SearchResultsAdapter<T>(results, caseQuery.getOffset(), totalCount, mappedClass);
        } finally {
            DbUtils.closeQuietly(rs);
        }
    }
}
