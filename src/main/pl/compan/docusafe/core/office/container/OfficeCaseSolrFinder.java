package pl.compan.docusafe.core.office.container;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.apache.solr.common.SolrDocument;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.rest.solr.LuceneQuery;
import pl.compan.docusafe.service.lucene.NewLuceneService;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.web.admin.UserToBriefcaseAction;
import std.lambda;

import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.*;

public class OfficeCaseSolrFinder<T> extends ContainerSolrFinder<OfficeCase, T> {

    private static final Logger log = LoggerFactory.getLogger(OfficeFolderSolrFinder.class);
    private OfficeCase.Query caseQuery;

    public OfficeCaseSolrFinder(OfficeCase.Query query, lambda<OfficeCase, T> mapper, Class<T> mappedClass) throws MalformedURLException {
        super(query, mapper, mappedClass);
        fullTextFields = Arrays.asList("title", "description", "clerk", "author");
    }

    @Override
    protected void prepareQuery() {
        LuceneQuery luceneQuery = new LuceneQuery(LuceneQuery.Mode.PARANTHESES);
        caseQuery = (OfficeCase.Query) query;

        luceneQuery.stack(luceneQuery.eq("discriminator", "CASE", LuceneQuery.PropertyType.STRING));

        if(guids.size() > 0) {
            luceneQuery.stack(luceneQuery.in("divisionguid", guids, LuceneQuery.PropertyType.STRING));
        }
        if(clerks.size() > 0) {
            luceneQuery.stack(luceneQuery.in("clerk", clerks, LuceneQuery.PropertyType.STRING));
        }

        if(caseQuery.getClerk() != null)
        {
            Collection<String> clerk = trimList(caseQuery.getClerk());
            luceneQuery.stack(luceneQuery.in("clerk", clerk, LuceneQuery.PropertyType.STRING));
        }

        if(caseQuery.getAuthor() != null)
        {
            Collection<String> author = trimList(caseQuery.getAuthor());
            luceneQuery.stack(luceneQuery.in("author", author, LuceneQuery.PropertyType.STRING));
        }

        if (caseQuery.getCdateFrom() != null)
        {
            luceneQuery.stack(luceneQuery.gte("openDate", caseQuery.getCdateFrom(), LuceneQuery.PropertyType.DATETIME));
        }

        if (caseQuery.getCdateTo() != null)
        {
            luceneQuery.stack(luceneQuery.lte("openDate", caseQuery.getCdateTo(), LuceneQuery.PropertyType.DATETIME));
        }

        if (caseQuery.getFinishDateFrom() != null)
        {
        	luceneQuery.stack(luceneQuery.gte("finishDate", caseQuery.getFinishDateFrom(), LuceneQuery.PropertyType.DATETIME));
        }
        if (caseQuery.getFinishDateTo() != null)
        {
        	luceneQuery.stack(luceneQuery.lte("finishDate", caseQuery.getFinishDateTo(), LuceneQuery.PropertyType.DATETIME));
        }

        if (caseQuery.getArchDateFrom() != null)
        {
            luceneQuery.stack(luceneQuery.gte("archivedDate", caseQuery.getArchDateFrom(), LuceneQuery.PropertyType.DATETIME));
        }

        if (caseQuery.getArchDateTo() != null)
        {
            luceneQuery.stack(luceneQuery.lte("archivedDate", caseQuery.getArchDateTo(), LuceneQuery.PropertyType.DATETIME));
        }

        if (caseQuery.getArchiveStatus() != null)
        {
            if(caseQuery.getArchiveStatus() ==(-1)) {
                luceneQuery.stack(luceneQuery.gte("archiveStatus", "1", LuceneQuery.PropertyType.INTEGER));
            }
            else if(caseQuery.getArchiveStatus() ==(-2)){
                LuceneQuery innerQuery = new LuceneQuery(LuceneQuery.Mode.PARANTHESES);
  
                innerQuery.stack(luceneQuery.eq("archiveStatus", String.valueOf(Container.ArchiveStatus.None.ordinal()), LuceneQuery.PropertyType.INTEGER));
                innerQuery.stack(luceneQuery.eq("archiveStatus", String.valueOf(Container.ArchiveStatus.ToArchive.ordinal()), LuceneQuery.PropertyType.INTEGER));
                
                innerQuery.orStack();
                luceneQuery.stack(innerQuery.toString());
            }
            else {
                luceneQuery.stack(luceneQuery.eq("archiveStatus", String.valueOf(caseQuery.getArchiveStatus()), LuceneQuery.PropertyType.INTEGER));
            }

        }

        if (caseQuery.getSeqNum() != null)
        {
            throw new UnsupportedOperationException("seqNum is not supported");
        }

        if (caseQuery.getYear() != null)
        {
            luceneQuery.stack(luceneQuery.eq("year", caseQuery.getYear(), LuceneQuery.PropertyType.STRING));
        }

        if (caseQuery.getArchived() != null)
        {
            luceneQuery.stack(luceneQuery.eq("archived", caseQuery.getArchived().toString(), LuceneQuery.PropertyType.BOOLEAN));
        }

        if (caseQuery.getStatusId() != null)
        {
            throw new UnsupportedOperationException("seqNum is not supported");
        }

        if (caseQuery.getRecord() != null)
        {
            throw new UnsupportedOperationException("record is not supported");
        }

        if (caseQuery.getRwaId() != null)
        {
            luceneQuery.stack(luceneQuery.eq("rwaId", caseQuery.getRwaId(), LuceneQuery.PropertyType.INTEGER));
        }

        if (caseQuery.getOfficeId() != null)
        {
            luceneQuery.stack(luceneQuery.eqLike("officeId", caseQuery.getOfficeId(), LuceneQuery.PropertyType.STRING, false));
        }

        if (caseQuery.getTitle() != null)
        {
            luceneQuery.stack(luceneQuery.eqLike("title", caseQuery.getTitle(), LuceneQuery.PropertyType.STRING, false));
        }

        if(caseQuery.getContent() != null) {

            LuceneQuery.Mode mode;
            if(caseQuery.isContentWordOrder()) {
                mode = LuceneQuery.Mode.QUOTE;
            } else {
                mode = LuceneQuery.Mode.PARANTHESES;
            }

            LuceneQuery innerQuery = new LuceneQuery(mode);
            for(String fullTextField: fullTextFields) {
            	if(mode == LuceneQuery.Mode.PARANTHESES){
            		innerQuery.stack(innerQuery.eqLike(fullTextField, caseQuery.getContent(), LuceneQuery.PropertyType.STRING, false));
            	}
            	else{
            		innerQuery.stack(innerQuery.eq(fullTextField, caseQuery.getContent(), LuceneQuery.PropertyType.STRING));
            	}
            }
            innerQuery.orStack();
            luceneQuery.stack(innerQuery.toString());
        }

        luceneQuery.andStack();

        luceneQueryString = luceneQuery.toString();

        log.debug("[prepareQuery] luceneQueryString = {}", luceneQueryString);
    }

    @Override
    protected SearchResults<T> getResult() throws SQLException, EdmException, SolrServerException {
        SolrQuery solrQuery = getSolrQuery();
        return getResult(solrQuery);
    }

    private SolrQuery getSolrQuery() {

        SolrQuery query = new SolrQuery();

        if(caseQuery.sortFields().size() > 0)
        {
            ImmutableList list = FluentIterable.from(caseQuery.sortFields()).transform(new com.google.common.base.Function<AbstractQuery.SortField, SolrQuery.SortClause>() {
                @Nullable
                @Override
                public SolrQuery.SortClause apply(@Nullable AbstractQuery.SortField sortField) {
                    return sortField.getDirection() ? SolrQuery.SortClause.asc(sortField.getName()) : SolrQuery.SortClause.desc(sortField.getName());
                }
            }).toList();

            query.setSorts(list);
        }

        query.setRows(caseQuery.getLimit());
        query.setStart(caseQuery.getOffset());

        query.setQuery(luceneQueryString);
        query.setHighlight(true).setHighlightSnippets(2).setParam("hl.fl", StringUtils.join(fullTextFields, ","));

        return query;
    }

    private SearchResults<T> getResult(SolrQuery solrQuery) throws EdmException, SolrServerException {
        QueryResponse response = solr.query(solrQuery);

        log.debug("[find] response = {}", response);

        Long numFound = response.getResults().getNumFound();
        log.debug("[getResults] numFound = {}", numFound);

        List<T> results = new ArrayList<T>(caseQuery.getLimit());

        if (AvailabilityManager.isAvailable("menu.left.user.to.briefcase")){
            List<Long> list = UserToBriefcaseAction.getPrzypisaneSprawy(DSApi.context().getDSUser().getName());
            for (Long id : list){
                results.add(mapper.act(DSApi.context().load(OfficeCase.class,id)));
                numFound++;
            }
        }

        Iterator<SolrDocument> it = response.getResults().iterator();

        Map<String,Map<String,List<String>>> highlighting = response.getHighlighting();

        while(it.hasNext())
        {
            SolrDocument solrDocument = it.next();
            String idString = (String) solrDocument.getFieldValue("id");
            Long id = Long.valueOf(idString);
            OfficeCase officeCase = DSApi.context().load(OfficeCase.class, id);

            addHighlighting(officeCase, highlighting.get(idString));

            results.add(mapper.act(officeCase));
        }

        return new SearchResultsAdapter<T>(results, caseQuery.getOffset(), numFound.intValue(), mappedClass);
    }

    @Override
    protected void addHighlighting(OfficeCase officeCase, Map<String, List<String>> highlights) {
        List<String> descriptionHighlight = highlights.get("description");
        List<String> titleHighlight = highlights.get("title");
//        List<String> authorHighlight = highlights.get("author");
//        List<String> clerkHighlight = highlights.get("clerk");

        if(descriptionHighlight != null) {
            officeCase.setDescription(StringUtils.join(descriptionHighlight, ", "));
        }

        if(titleHighlight != null) {
            officeCase.setTitle(StringUtils.join(titleHighlight, ", "));
        }

//        if(authorHighlight != null) {
//            officeCase.setAuthor(StringUtils.join(authorHighlight, ", "));
//        }
//
//        if(clerkHighlight != null) {
//            officeCase.setClerkNoChecks(StringUtils.join(clerkHighlight, ", "));
//        }
    }
}
