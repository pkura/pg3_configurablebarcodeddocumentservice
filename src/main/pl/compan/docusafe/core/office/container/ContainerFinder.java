package pl.compan.docusafe.core.office.container;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.AbstractQuery;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import std.lambda;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class ContainerFinder<U, T> {

    protected final AbstractQuery query;
    protected final lambda<U, T> mapper;
    protected final Class<T> mappedClass;

    protected List<String> guids;
    protected List<String> clerks;

    public ContainerFinder(AbstractQuery query, lambda<U, T> mapper, Class<T> mappedClass) {
        if (mapper == null) {
            throw new NullPointerException("mapper");
        }

        this.query = query;
        this.mapper = mapper;
        this.mappedClass = mappedClass;
    }

    protected abstract void prepareQuery();

    protected abstract SearchResults<T> getResult() throws Exception;

    public SearchResults<T> find() throws EdmException
    {
        try
        {
            computePrivileges();
            prepareQuery();

            return getResult();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch(EdmException e) {
            throw e;
        }
        catch(Exception e) {
            throw new EdmException("Nieznany b��d", e);
        }
    }


    /**
     * je�eli u�ytkownik nie ma uprawnie� do przegl�dania spraw w kom�rce
     * i wszystkich, mo�e ogl�da� tylko swoje
     * je�eli u�ytkownik nie jest adminem i nie ma uprawnie� do przegl�dania
     * wszystkich spraw, ograniczam mu wyniki wyszukiwania nast�puj�co:
     * je�eli ma prawo przegl�dania spraw w swoich dzia�ach, dodaj� odpowiedni
     * warunek (guid = A or guid = B or ...), je�eli nie ma lub nie znajduje
     * si� z �adnym dziale, mo�e przegl�da� tylko sprawy, kt�rych jest referentem
     */
    protected void computePrivileges() throws EdmException {
        guids = new ArrayList<String>();
        clerks = new ArrayList<String>();

        if (!DSApi.context().isAdmin() &&
                !DSApi.context().hasPermission(DSPermission.SPRAWA_WSZYSTKIE_PODGLAD))
        {
            DSUser user = DSApi.context().getDSUser();
            if (DSApi.context().hasPermission(DSPermission.SPRAWA_KOMORKA_PODGLAD))
            {
                DSDivision[] divisions = user.getDivisions();
                if (divisions.length > 0)
                {
                    Junction OR = Expression.disjunction();
                    for (int i=0; i < divisions.length; i++)
                    {
                        guids.add(divisions[i].getGuid());
                        if (divisions[i].isPosition())
                        {
                            DSDivision parent = divisions[i].getParent();
                            if (parent != null) {
                                guids.add(parent.getGuid());
                            }
                        }
                    }
                    clerks.add(user.getName());
                }
                else
                {
                    clerks.add(user.getName());
                }
            }
            else
            {
                clerks.add(user.getName());
            }
        }
    }
    public String addPercentExpresion(String stringToaddPercent){
    	
         if(stringToaddPercent.startsWith("%") && !stringToaddPercent.endsWith("%"))
        	 stringToaddPercent +=stringToaddPercent+"%";
         else if (!stringToaddPercent.startsWith("%") && stringToaddPercent.endsWith("%"))
        	 stringToaddPercent +="%"+stringToaddPercent;
         else if (!stringToaddPercent.startsWith("%") && !stringToaddPercent.endsWith("%"))
        	 stringToaddPercent="%"+stringToaddPercent+"%";
         	
     return stringToaddPercent;
         
    }
}
