package pl.compan.docusafe.core.office.container;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.office.OfficeFolder;
import std.lambda;

public class OfficeFolderFinderProvider {

    private static final String PROVIDE_DB = "db";
    private static final String PROVIDE_SOLR = "solr";

    private static OfficeFolderFinderProvider instance;

    public static OfficeFolderFinderProvider instance() {
        if(instance == null) {
            instance = new OfficeFolderFinderProvider();
        }
        return instance;
    }

    public <T> ContainerFinder<OfficeFolder, T> provide(OfficeFolder.Query query, lambda<OfficeFolder, T> mapper, Class<T> mapClass) throws Exception {
        String provide = AdditionManager.getPropertySafe("officeContainerFinderProvider");
        if(provide.equals(PROVIDE_DB)) {
            return new OfficeFolderDbFinder<T>(query, mapper, mapClass);
        } else if(provide.equals(PROVIDE_SOLR)) {
            return new OfficeFolderSolrFinder<T>(query, mapper, mapClass);
        } else {
            throw new Exception("Unknown provider for "+provide);
        }
    }

}
