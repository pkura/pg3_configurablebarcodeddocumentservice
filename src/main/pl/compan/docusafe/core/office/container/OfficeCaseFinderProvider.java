package pl.compan.docusafe.core.office.container;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeFolder;
import std.lambda;

public class OfficeCaseFinderProvider {
    private static final String PROVIDE_DB = "db";
    private static final String PROVIDE_SOLR = "solr";

    private static OfficeCaseFinderProvider instance;

    public static OfficeCaseFinderProvider instance() {
        if(instance == null) {
            instance = new OfficeCaseFinderProvider();
        }
        return instance;
    }

    public <T> ContainerFinder<OfficeCase, T> provide(OfficeCase.Query query, lambda<OfficeCase, T> mapper, Class<T> mapClass) throws Exception {
        String provide = AdditionManager.getPropertySafe("officeContainerFinderProvider");
        if(provide.equals(PROVIDE_DB)) {
            return new OfficeCaseDbFinder<T>(query, mapper, mapClass);
        } else if(provide.equals(PROVIDE_SOLR)) {
            return new OfficeCaseSolrFinder<T>(query, mapper, mapClass);
        } else {
            throw new Exception("Unknown provider for "+provide);
        }
    }
}
