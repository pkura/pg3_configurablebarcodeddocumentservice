package pl.compan.docusafe.core.office.container;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import org.apache.solr.client.solrj.SolrServer;
import pl.compan.docusafe.core.AbstractQuery;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.rest.RestConfiguration;
import pl.compan.docusafe.rest.solr.ContainerSolrServer;
import pl.compan.docusafe.util.TextUtils;
import std.lambda;

import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class ContainerSolrFinder<U, T> extends ContainerFinder<U ,T> {

    protected final SolrServer solr;
    protected String luceneQueryString;
    protected List<String> fullTextFields;

    public ContainerSolrFinder(AbstractQuery query, lambda<U, T> mapper, Class<T> mappedClass) throws MalformedURLException {
        super(query, mapper, mappedClass);
        solr = getSolrServer();
    }

    protected SolrServer getSolrServer() throws MalformedURLException {
        return new RestConfiguration().containerSolrServer();
    }

    protected Collection<String> trimList(String[] list) {
        return trimList(Arrays.asList(list));
    }

    protected Collection<String> trimList(List<String> list) {
        return FluentIterable.from(list).transform(new Function<String, String>() {
            @Nullable
            @Override
            public String apply(@Nullable String input) {
                return TextUtils.trimmedStringOrNull(input);
            }
        }).toList();
    }

    protected abstract void addHighlighting(U container, Map<String, List<String>> highlights);
}
