package pl.compan.docusafe.core.office.container;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.rest.RestConfiguration;
import pl.compan.docusafe.rest.solr.ContainerSolrServer;
import pl.compan.docusafe.rest.solr.DocumentSolrServer;
import pl.compan.docusafe.rest.solr.LuceneQuery;
import pl.compan.docusafe.service.lucene.NewLuceneService;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import std.lambda;

import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.*;

public class OfficeFolderSolrFinder<T> extends ContainerSolrFinder<OfficeFolder, T> {
    private static final Logger log = LoggerFactory.getLogger(OfficeFolderSolrFinder.class);

    private OfficeFolder.Query folderQuery;

    public OfficeFolderSolrFinder(OfficeFolder.Query query, lambda<OfficeFolder, T> mapper, Class<T> mapClass) throws MalformedURLException {
        super(query, mapper, mapClass);
        fullTextFields = Arrays.asList("name", "author", "clerk");
    }

    @Override
    protected void prepareQuery() {
        LuceneQuery luceneQuery = new LuceneQuery(LuceneQuery.Mode.PARANTHESES);
        folderQuery = (OfficeFolder.Query) query;

        luceneQuery.stack(luceneQuery.eq("discriminator", "FOLDER", LuceneQuery.PropertyType.STRING));

        if(guids.size() > 0) {
            luceneQuery.stack(luceneQuery.in("divisionguid", guids, LuceneQuery.PropertyType.STRING));
        }
        if(clerks.size() > 0) {
            luceneQuery.stack(luceneQuery.in("clerk", clerks, LuceneQuery.PropertyType.STRING));
        }

        if(folderQuery.getClerk() != null)
        {
            Collection<String> clerk = trimList(folderQuery.getClerk());
            luceneQuery.stack(luceneQuery.in("clerk", clerk, LuceneQuery.PropertyType.STRING));
        }

        if(folderQuery.getAuthor() != null)
        {
            Collection<String> author = trimList(folderQuery.getAuthor());
            luceneQuery.stack(luceneQuery.in("author", author, LuceneQuery.PropertyType.STRING));
        }

        if (folderQuery.getCdateFrom() != null)
        {
            luceneQuery.stack(luceneQuery.gte("ctime", folderQuery.getCdateFrom(), LuceneQuery.PropertyType.DATETIME));
        }

        if (folderQuery.getCdateTo() != null)
        { 
        	Calendar calendar = Calendar.getInstance(); 
        	calendar.setTime(folderQuery.getCdateTo()); 
        	calendar.add(Calendar.DAY_OF_YEAR, 1);
        	
            luceneQuery.stack(luceneQuery.lte("ctime", calendar.getTime(), LuceneQuery.PropertyType.DATETIME));
        }

        if (folderQuery.getArchDateFrom() != null)
        {
            luceneQuery.stack(luceneQuery.gte("archivedDate", folderQuery.getArchDateFrom(), LuceneQuery.PropertyType.DATETIME));
        }

        if (folderQuery.getArchDateTo() != null)
        {
        	luceneQuery.stack(luceneQuery.lte("archivedDate", folderQuery.getArchDateTo(), LuceneQuery.PropertyType.DATETIME));
        }

        if (folderQuery.getArchiveStatus() != null)
        {
            if(folderQuery.getArchiveStatus() ==(-1)) {
                luceneQuery.stack(luceneQuery.gte("archiveStatus", "1", LuceneQuery.PropertyType.INTEGER));
            }
            else if(folderQuery.getArchiveStatus() ==(-2)){
            	LuceneQuery innerQuery = new LuceneQuery(LuceneQuery.Mode.PARANTHESES);
                
            	innerQuery.stack(luceneQuery.eq("archiveStatus", String.valueOf(Container.ArchiveStatus.None.ordinal()), LuceneQuery.PropertyType.INTEGER));
                innerQuery.stack(luceneQuery.eq("archiveStatus", String.valueOf(Container.ArchiveStatus.ToArchive.ordinal()), LuceneQuery.PropertyType.INTEGER));

                innerQuery.orStack();
                luceneQuery.stack(innerQuery.toString());
            }
            else {
                luceneQuery.stack(luceneQuery.eq("archiveStatus", String.valueOf(folderQuery.getArchiveStatus()), LuceneQuery.PropertyType.INTEGER));
            }

        }

        if(folderQuery.getName() != null)
        {
        	luceneQuery.stack(luceneQuery.eqLike("name", folderQuery.getName(), LuceneQuery.PropertyType.STRING, false));
        }

        if (folderQuery.getSeqNum() != null)
        {
            throw new UnsupportedOperationException("seqNum is not supported");
        }

        if (folderQuery.getYear() != null)
        {
            luceneQuery.stack(luceneQuery.eq("year", folderQuery.getYear(), LuceneQuery.PropertyType.STRING));
        }

        if (folderQuery.getArchived() != null)
        {
            luceneQuery.stack(luceneQuery.eq("archived", folderQuery.getArchived().toString(), LuceneQuery.PropertyType.BOOLEAN));
        }

        if (folderQuery.getRecord() != null)
        {
            throw new UnsupportedOperationException("record is not supported");
        }

        if (folderQuery.getRwaId() != null)
        {
            luceneQuery.stack(luceneQuery.eq("rwaId", folderQuery.getRwaId(), LuceneQuery.PropertyType.INTEGER));
        }

        if (folderQuery.getOfficeId() != null)
        {
            luceneQuery.stack(luceneQuery.eqLike("officeId", folderQuery.getOfficeId(), LuceneQuery.PropertyType.STRING, false));
        }

        if(folderQuery.getContent() != null) {

            LuceneQuery.Mode mode;
            if(folderQuery.isContentWordOrder()) {
                mode = LuceneQuery.Mode.QUOTE;
            } else {
                mode = LuceneQuery.Mode.PARANTHESES;
            }

            LuceneQuery innerQuery = new LuceneQuery(mode);
            for(String fullTextField: fullTextFields) {
            	if(mode == LuceneQuery.Mode.PARANTHESES){
            		innerQuery.stack(innerQuery.eqLike(fullTextField, folderQuery.getContent(), LuceneQuery.PropertyType.STRING, false));
            	}
            	else{
            		innerQuery.stack(innerQuery.eq(fullTextField, folderQuery.getContent(), LuceneQuery.PropertyType.STRING));
            	}
            }
            innerQuery.orStack();
            luceneQuery.stack(innerQuery.toString());
        }

        luceneQuery.andStack();

        luceneQueryString = luceneQuery.toString();

        log.debug("[prepareQuery] luceneQueryString = {}", luceneQueryString);
    }

    @Override
    protected SearchResults<T> getResult() throws Exception {
        SolrQuery solrQuery = getSolrQuery();
        return getResult(solrQuery);
    }

    private SearchResults<T> getResult(SolrQuery solrQuery) throws SolrServerException, EdmException {
        QueryResponse response;
        try {
            response = solr.query(solrQuery);
        } catch(SolrServerException ex) {
            throw new EdmException("B��d podczas wykonywania zapytania", ex);
        }

        log.debug("[find] response = {}", response);

        Long numFound = response.getResults().getNumFound();
        log.debug("[getResults] numFound = {}", numFound);

        List<T> results = new ArrayList<T>(folderQuery.getLimit());

        Iterator<SolrDocument> it = response.getResults().iterator();

        Map<String,Map<String,List<String>>> highlighting = response.getHighlighting();

        while(it.hasNext())
        {
            SolrDocument solrDocument = it.next();
            String idString = (String) solrDocument.getFieldValue("id");
            Long id = Long.valueOf(idString);

            OfficeFolder officeFolder = DSApi.context().load(OfficeFolder.class, id);

            addHighlighting(officeFolder, highlighting.get(idString));

            results.add(mapper.act(officeFolder));
        }

        return new SearchResultsAdapter<T>(results, folderQuery.getOffset(), numFound.intValue(), mappedClass);

    }

    private SolrQuery getSolrQuery() {

        SolrQuery query = new SolrQuery();

        if(folderQuery.sortFields().size() > 0)
        {
            ImmutableList list = FluentIterable.from(folderQuery.sortFields()).transform(new com.google.common.base.Function<AbstractQuery.SortField, SolrQuery.SortClause>() {
                @Nullable
                @Override
                public SolrQuery.SortClause apply(@Nullable AbstractQuery.SortField sortField) {
                    return sortField.getDirection() ? SolrQuery.SortClause.asc(sortField.getName()) : SolrQuery.SortClause.desc(sortField.getName());
                }
            }).toList();

            query.setSorts(list);
        }

        query.setRows(folderQuery.getLimit());
        query.setStart(folderQuery.getOffset());

        query.setQuery(luceneQueryString);
        query.setHighlight(true).setHighlightSnippets(2).setParam("hl.fl", StringUtils.join(fullTextFields, ","));

        return query;
    }

    @Override
    protected void addHighlighting(OfficeFolder officeFolder, Map<String, List<String>> highlights) {
        List<String> nameHighlight = highlights.get("name");
//        List<String> authorHighlight = highlights.get("author");
//        List<String> clerkHighlight = highlights.get("clerk");

        if(nameHighlight != null) {
            officeFolder.setNameNoCheck(StringUtils.join(nameHighlight, ", "));
        }

//        if(authorHighlight != null) {
//            officeFolder.setAuthor(StringUtils.join(authorHighlight, ", "));
//        }

//        if(clerkHighlight != null) {
//            officeFolder.setClerkNoChecks(StringUtils.join(clerkHighlight, ", "));
//        }
    }
}
