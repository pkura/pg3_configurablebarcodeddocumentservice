package pl.compan.docusafe.core.office.container;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.*;
import std.lambda;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class OfficeFolderDbFinder<T> extends ContainerFinder<OfficeFolder, T> {

    private Logger log = LoggerFactory.getLogger(OfficeFolderDbFinder.class);

    private WhereClause where;
    private FromClause from;
    private OrderClause order;
    private SelectClause selectId;
    private SelectColumn idCol;
    private TableAlias folderTable;

    private OfficeFolder.Query folderQuery;

    public OfficeFolderDbFinder(OfficeFolder.Query query, lambda<OfficeFolder, T> mapper, Class<T> mappedClass) {
        super(query, mapper, mappedClass);
    }

    protected void prepareQuery() {
        folderQuery = (OfficeFolder.Query) query;

        from = new FromClause();
        folderTable = from.createTable("dso_container");
        where = new WhereClause();
        where.add(Expression.eq(folderTable.attribute("discriminator"), "FOLDER"));

        Junction privilegesOR = Expression.disjunction();
        if(guids.size() > 0) {
            privilegesOR.add(Expression.in(folderTable.attribute("divisionguid"), guids.toArray()));
        }
        if(clerks.size() > 0) {
            privilegesOR.add(Expression.in(folderTable.attribute("clerk"), clerks.toArray()));
        }
        if(! privilegesOR.isEmpty()) {
            where.add(privilegesOR);
        }

       /* // alternatywa do ogranicze� na zwracane sprawy, aby nie pojawi�o si�
        // wyra�enie clerk='A' and clerk='B'.
        else */
        if (folderQuery.getClerk() != null)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < folderQuery.getClerk().length; i++)
                OR.add(Expression.eq(folderTable.attribute("clerk"), TextUtils.trimmedStringOrNull(folderQuery.getClerk()[i])));
            where.add(OR);
        }

        if (folderQuery.getAuthor() != null)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < folderQuery.getAuthor().length; i++)
                OR.add(Expression.eq(folderTable.attribute("author"),TextUtils.trimmedStringOrNull( folderQuery.getAuthor()[i])));
            where.add(OR);
        }
        if (folderQuery.getCdateFrom() != null)
        {
            where.add(Expression.ge(folderTable.attribute("ctime"), folderQuery.getCdateFrom()));
        }

        if (folderQuery.getCdateTo() != null)
        {
            where.add(Expression.le(folderTable.attribute("ctime"), folderQuery.getCdateTo()));
        }

        if (folderQuery.getArchDateFrom() != null)
        {
            where.add(Expression.ge(folderTable.attribute("archivedDate"), folderQuery.getArchDateFrom()));
        }

        if (folderQuery.getArchDateTo() != null)
        {
            where.add(Expression.le(folderTable.attribute("archivedDate"), folderQuery.getArchDateTo()));
        }
        if (folderQuery.getArchiveStatus() != null)
        {
            if(folderQuery.getArchiveStatus() ==(-1))
                where.add(Expression.ge(folderTable.attribute("archiveStatus"), 1));
            else if(folderQuery.getArchiveStatus() ==(-2)){
                Junction or=Expression.disjunction();
                or.add(Expression.eq(folderTable.attribute("archiveStatus"), Container.ArchiveStatus.None.ordinal()));
                or.add(Expression.eq(folderTable.attribute("archiveStatus"), Container.ArchiveStatus.ToArchive.ordinal()));
                or.add(Expression.eq(folderTable.attribute("archiveStatus"), null));
                where.add(or);
            }
            else
                where.add(Expression.eq(folderTable.attribute("archiveStatus"), folderQuery.getArchiveStatus()));

        }
        if(folderQuery.getName() != null)
        {
            where.add(Expression.likeLeftRight(folderTable.attribute("name"), folderQuery.getName(),true));
        }

        if (folderQuery.getSeqNum() != null)
        {
            //where.add(Expression.eq(caseTable.attribute("sequenceid"), query.seqNum));
            //wyszukuje odpowiedniego numeru w officeId zak�adaj�c �e znajduje si� on
            //pomi�dzy znakami - i /
            where.add(Expression.like(folderTable.attribute("officeid"), "%-" + folderQuery.getSeqNum() + "/%"));
           /* Junction OR = Expression.conjunction();
            OR.add(Expression.like(caseTable.attribute("officeid"), "%." + query.seqNum + ".%"));
            where.add(OR);*/

        }

        if (folderQuery.getYear() != null)
        {
            where.add(Expression.eq(folderTable.attribute("case_year"), folderQuery.getYear()));
        }

        if (folderQuery.getArchived() != null)
        {
            where.add(Expression.eq(folderTable.attribute("archived"), folderQuery.getArchived().booleanValue()));
        }

        if (folderQuery.getRecord() != null)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < folderQuery.getRecord().length; i++)
                OR.add(Expression.eq(folderTable.attribute("registries"), folderQuery.getRecord()[i]));
            where.add(OR);

        }

        if (folderQuery.getRwaId() != null)
        {
            where.add(Expression.eq(folderTable.attribute("rwa_id"), folderQuery.getRwaId()));
        }

        if (folderQuery.getOfficeId() != null)
        {
         	where.add(Expression.like(folderTable.attribute("officeid"), this.addPercentExpresion(folderQuery.getOfficeId())));
        	//old
          //  where.add(Expression.like(folderTable.attribute("officeid"), folderQuery.getOfficeId()));//.eq(caseTable.attribute("officeid"), query.officeId));
        }

        // kolumny, po kt�rych wykonywane jest sortowanie musz� si�
        // znale�� w SELECT
        selectId = new SelectClause();
        idCol = selectId.add(folderTable, "id");

        order = new OrderClause();

        for (Iterator iter=query.sortFields().iterator(); iter.hasNext(); )
        {
            AbstractQuery.SortField field = (AbstractQuery.SortField) iter.next();
            // je�eli dodana b�dzie mo�liwo�� sortowania po innych polach,
            // nale�y uaktualni� metod� Query.validSortField()
            if (field.getName().equals("officeId"))
            {
                order.add(folderTable.attribute("officeid"), field.getDirection());
                selectId.add(folderTable, "officeid");
            }
        }
    }

    protected SearchResults<T> getResult() throws SQLException, EdmException {
        ResultSet rs = null;
        try {

            int totalCount = getCount();

            SelectQuery selectQuery = new SelectQuery(selectId, from, where, order);
            if (folderQuery.getLimit() > 0)
            {
                selectQuery.setMaxResults(folderQuery.getLimit());
                selectQuery.setFirstResult(folderQuery.getOffset());
            }

            rs = selectQuery.resultSet(DSApi.context().session().connection());

            List<T> results = new ArrayList<T>(folderQuery.getLimit());

            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(mapper.act(DSApi.context().load(OfficeFolder.class, id)));
            }

            return new SearchResultsAdapter<T>(results, folderQuery.getOffset(), totalCount, mappedClass);
        } finally {
            DbUtils.closeQuietly(rs);
        }
    }

    protected int getCount() throws SQLException, EdmException {
        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(*)");

        ResultSet rs = null;
        try {
            // przy zapytaniu z COUNT(*) nie mo�e by� klauzuli ORDER BY
            // Firebird 1.5 zwraca b��d
            SelectQuery selectQuery = new SelectQuery(selectCount, from, where, null);
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby teczek.");

            return rs.getInt(countCol.getPosition());
        } finally {
            DbUtils.closeQuietly(rs);
        }
    }

}