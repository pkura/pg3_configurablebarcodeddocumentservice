package pl.compan.docusafe.core.office;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

/**
 * Rodzaj dokumentu przychodz�cego.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InOfficeDocumentKind.java,v 1.24 2010/07/29 11:00:16 mariuszk Exp $
 */
public class InOfficeDocumentKind extends OfficeDocumentKind
{
    /**
     * Liczba dni na za�atwienie dokumentu.
     */
    private int days;
    
    private String language = Docusafe.getCurrentLanguageLocaleString();
    
    public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
    private String name_en;
    
    public String getName_en() {
		return name_en;
	}

	public void setName_en(String name_en) {
		this.name_en = name_en;
	}
  
	/**
     * Lista wymaganych za��cznik�w zwi�zanych z tym
     * typem dokumentu (obiekty {@link InOfficeDocumentKindRequiredAttachment}).
     */
    private List<InOfficeDocumentKindRequiredAttachment> requiredAttachments;

    /**
     * Konstruktor u�ywany przez Hibernate.
     */
    InOfficeDocumentKind()
    {
    }

    public InOfficeDocumentKind(String name, int days)
    {
        super(name);
        this.days = days;
    }


    /**
     * Zwraca list� wszystkich obiekt�w InOfficeDocumentKind
     * znajduj�cych si� w bazie posortowanych po polu posn
     * (zainicjalizowane proxy).
     * @return
     * @throws EdmException
     */
    public static List<InOfficeDocumentKind> list() throws EdmException
    {
        List<InOfficeDocumentKind> list = Finder.list(InOfficeDocumentKind.class, /*"posn"*/"name");
        for (Iterator iter=list.iterator(); iter.hasNext(); )
        {
            DSApi.initializeProxy(iter.next());
        }
        return list;
    }

    public static InOfficeDocumentKind find(Integer id) throws EdmException
    {
        return (InOfficeDocumentKind) Finder.find(InOfficeDocumentKind.class, id);

/*
        InOfficeDocumentKind kind = (InOfficeDocumentKind) DSApi.getObject(InOfficeDocumentKind.class, id);

        if (kind == null)
            throw new EntityNotFoundException(InOfficeDocumentKind.class, id);

        return kind;
*/
    }

    public static List<InOfficeDocumentKind> findByPartialName(String name) throws EdmException
    {
        try
        {
            return DSApi.context().classicSession().find("from k in class "+InOfficeDocumentKind.class.getName()+
            " where upper(k.name) like ?", "%"+name.toUpperCase()+"%", Hibernate.STRING);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static InOfficeDocumentKind getInOfficeDocumentKind(DocumentKind documentKind) throws EdmException
    {
		List<InOfficeDocumentKind> kinds = InOfficeDocumentKind.list();
		String kindName = documentKind.logic().getInOfficeDocumentKind();
		boolean canChooseKind = (kindName == null);
		if (!canChooseKind)
		{
			for (InOfficeDocumentKind inKind : kinds)
			{
				if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
					return InOfficeDocumentKind.find(inKind.getId());
			}
		}
		return kinds.get(0);
    }

    public boolean canDelete() throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias docTable = from.createTable(DSApi.getTableName(InOfficeDocument.class));

        WhereClause where = new WhereClause();
        where.add(Expression.eq(docTable.attribute("kind"), getId()));

        SelectClause selectCount = new SelectClause();
        /*SelectColumn countCol = */selectCount.addSql("count(*)");

        SelectQuery selectQuery;

        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            

            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            boolean unable = false;
            int count = 0;
            if (rs.next())
            {
            	count = rs.getInt(1);
            }
            else
            {
            	unable = true;
            }
            rs.close();
            if (unable)
            {	            
                throw new EdmException("Nie mo�na pobra� liczby pism");
            }                 
            return count == 0;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

    public void addRequiredAttachment(String title) throws DuplicateNameException
    {
        if (requiredAttachments != null && requiredAttachments.size() > 0)
        {
            for (Iterator iter=requiredAttachments.iterator(); iter.hasNext(); )
            {
                InOfficeDocumentKindRequiredAttachment req = (InOfficeDocumentKindRequiredAttachment) iter.next();
                if (req.getTitle().equals(title))
                    throw new DuplicateNameException("Istnieje ju� wymagany za��cznik o tytule '"+title+"'");
            }
        }

        if (requiredAttachments == null)
            requiredAttachments = new LinkedList<InOfficeDocumentKindRequiredAttachment>();
        requiredAttachments.add(new InOfficeDocumentKindRequiredAttachment(title));
    }

    public boolean deleteRequiredAttachment(String sid)
    {
        if (requiredAttachments != null && requiredAttachments.size() > 0)
        {
            for (Iterator iter=requiredAttachments.iterator(); iter.hasNext(); )
            {
                InOfficeDocumentKindRequiredAttachment req = (InOfficeDocumentKindRequiredAttachment) iter.next();
                if (req.getSid().equals(sid))
                {
                    iter.remove();
                    return true;
                }
            }
        }

        return false;
    }

    public int getDays()
    {
        return days;
    }

    public void setDays(int days)
    {
        this.days = days;
    }

    public List<InOfficeDocumentKindRequiredAttachment> getRequiredAttachments()
    {
        if (requiredAttachments == null)
            requiredAttachments = new LinkedList<InOfficeDocumentKindRequiredAttachment>();
        return requiredAttachments;
    }

    public void setRequiredAttachments(List<InOfficeDocumentKindRequiredAttachment> requiredAttachments)
    {
        this.requiredAttachments = requiredAttachments;
    }
}
