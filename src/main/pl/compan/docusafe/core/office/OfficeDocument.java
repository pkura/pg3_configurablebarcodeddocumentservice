package pl.compan.docusafe.core.office;

import com.beust.jcommander.internal.Lists;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;


import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.jackrabbit.JackrabbitPrivileges;
import pl.compan.docusafe.core.office.Journal.EntryBean;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.*;
import java.util.prefs.Preferences;
/**
 * Klasa abstrakcyjna, z kt�rej powinny wywodzi� si� wszelkie
 * rodzaje pism kancelaryjnych (przychodz�ce, wychodz�ce,
 * wewn�trzne i inne).
 *
 * <p>Uwaga: w {@link pl.compan.docusafe.core.users.UserFactory#deleteDivision(String, String)}
 * pewne rzeczy sprawdzane s� dla wszystkich klas dziedzicz�cych z osobna,
 * trzeba tam dopisywa� nowotworzone klasy.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OfficeDocument.java,v 1.132 2010/02/17 13:18:01 mariuszk Exp $
 */
public abstract class OfficeDocument extends Document
{
	private Logger log = LoggerFactory.getLogger(OfficeDocument.class);
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(OfficeDocument.class.getPackage().getName(),null);
    /**
     * Status pisma (officeStatus) - przyj�te.
     */
    public static final String STATUS_NEW = "new";
    /**
     * Status pisma (officeStatus) - przyj�te w dziale.
     */
    public static final String STATUS_IN_DEPARTMENT = "in_department";


    private Integer officeNumber;
    /**
     * Symbol zwi�zany z numerem kancelaryjnym.
     */
    private String officeNumberSymbol;
    private Integer officeNumberYear;
    /**
     * Sprawa, w jakiej znajduje si� dokument.
     * wywalone z hbm ladowane na zadanie
     */
    private OfficeCase containingCase = null;
    private Integer dailyOfficeNumber;
    private Long containingCaseId;
    /**
     * Identyfikator dokumentu w sprawie containingCase.
     */
    private String caseDocumentId;
    /**
     * Lista obiekt�w {@link AssignmentHistoryEntry}.
     */
    private List<AssignmentHistoryEntry> assignmentHistory = null;
    /**
     * Referent pisma.
     */
    private String clerk;
    /**
     * Dzia�, w kt�rym znajduje si� pismo.
     */
    private String assignedDivision;
    /**
     * Flaga okre�laj�ca, czy pismo jest anulowane.
     */
    private boolean cancelled;
    /**
     * Pow�d anulowania pisma podany przez u�ytkownika.
     */
    private String cancellationReason;
    /**
     * Nazwa u�ytkownika, kt�ry anulowa� pismo.
     */
    private String cancelledByUsername;
    /**
     * Dzia�, na kt�ry ostatnio zadekretowano pismo (dekretacja
     * nie musi by� jeszcze przyj�ta).
     */
    private String currentAssignmentGuid;
    /**
     * U�ytkownik, na kt�rego ostatnio zadekretowano pismo (dekretacja
     * nie musi by� jeszcze przyj�ta). U�ytkownik nie musi by� okre�lony
     * nawet, gdy okre�lony jest dzia� {@link #currentAssignmentGuid}.
     */
    private String currentAssignmentUsername;
    /**
     * Okre�la, czy dekretacja okre�lona polami {@link #currentAssignmentGuid}
     * i {@link #currentAssignmentUsername} zosta�a przyj�ta.
     */
    private Boolean currentAssignmentAccepted;
    /**
     * Status pisma.
     */
    private String officeStatus;
    /**
     * Opis (tre��) pisma kancelaryjnego.
     */
    private String summary;
    /**
     * Dziennik, z kt�rego pochodzi numer kancelaryjny (officeNumber).
     */
    private Journal journal;

    /**
     * Nadawca.
     */
    private Sender sender;

    private Long senderId;

    /**
     * Dokument z ktorego zrobiono duplikat
     */
    private Long masterDocumentId;

    private String barcode;

    private boolean archived;

    private String divisionGuid;
    
    private long versionGuid;
    private int version;
    private String versionDesc;
    private Boolean czyCzystopis;
    private Boolean czyAktualny;
    private Boolean isArchived;
    private Boolean countryLetter;
    private Date archivedDate;
    private Long accessToDocument;
    /**
     * Nazwy p�l wymaganych w identyfikatorze pisma.
     */
    private static final Set<String> requiredFormats;
    private static final Set<String> allowedFormats;
    //private static final Map formatMap;
    static
    {
        Set<String> req = new HashSet<String>();
        //req.add("d1");
        req.add("r");
        //req.add("d");
        //req.add("y2");
        requiredFormats = Collections.unmodifiableSet(req);

        Set<String> all = new HashSet<String>();
        all.add("d0");
        all.add("d1");
        all.add("d2");
        all.add("d3");
        all.add("d4");
        all.add("d5");
        //all.add("p");
        all.add("r");
        //all.add("c");
        //all.add("d");
        all.add("y2");
        all.add("y4");
        all.add("lp");
        all.add("pp");
        all.add("pm");
        all.add("ps");

        allowedFormats = Collections.unmodifiableSet(all);
    }



    protected void duplicate(Long[] attachmentIds) throws EdmException
    {
        List<Remark> savedRemarks = this.remarks;
        this.remarks = null;
        List<Audit> savedWorkHistory = getWorkHistory();
        setWorkHistory(null);
        List<AssignmentHistoryEntry> savedAssignmentHistory = this.assignmentHistory;
        this.assignmentHistory = null;

        super.duplicate(attachmentIds);

        // obiekty w kolekcji savedRemarks nie s� duplikowane
        if (savedRemarks != null)
        {
            this.remarks = new ArrayList<Remark>(savedRemarks.size());
            //for (Iterator iter=savedRemarks.iterator(); iter.hasNext(); )
            for (Remark remark : savedRemarks)
            {
                this.remarks.add(remark);
            }
        }

        if (savedWorkHistory != null)
        {
        	setWorkHistory(new ArrayList<Audit>(savedWorkHistory.size()));
            //for (Iterator iter=savedWorkHistory.iterator(); iter.hasNext(); )
            for (Audit audit : savedWorkHistory)
            {
            	getWorkHistory().add(audit);
            }
        }

        if (savedAssignmentHistory != null)
        {
            this.assignmentHistory = new ArrayList<AssignmentHistoryEntry>(savedAssignmentHistory.size());
            //for (Iterator iter=savedAssignmentHistory.iterator(); iter.hasNext(); )
            /**
             * @todo - historia sie nie klonuje
             */
            for (AssignmentHistoryEntry entry : savedAssignmentHistory)
            {
                this.assignmentHistory.add(entry);
            }
        }
    }

    public static void validateIdFormat(String format)
        throws EdmException
    {
        FormatUtils.validate(format, requiredFormats, allowedFormats);
    }

    /**
     * Przypisuje dokument do sprawy oraz nadaje mu numer
     * w sprawie (caseDocumentId).
     * @param officeCase Sprawa, w kt�rej znajdzie si� pismo, nie mo�e
     *   by� r�wna null.
     */
    public void setContainingCase(OfficeCase officeCase, boolean changeCaseFinishDate)
        throws EdmException
    {
        if (officeCase == null)
            throw new NullPointerException("officeCase");

        setCaseDocumentId("xxx");

        Map<String, Object> context = new HashMap<String, Object>();

        int year = GlobalPreferences.getCurrentYear();

        DSDivision division = DSDivision.find(officeCase.getDivisionGuid());

        List<String> divisionCodes = new ArrayList<String>(8);
        DSDivision tmp = division;
        do
        {
            if (tmp.isRoot())
                break;
            if (tmp.getCode() == null)
            	throw new EdmException(sm.getString("BrakKoduDzialuNieMoznaWygenerowacNumeruTeczkiSprawy", division.getName()));
                /*throw new EdmException("Brak kodu dzia�u "+division.getName()+" nie mo�na " +
                    "wygenerowa� numeru teczki/sprawy");*/
            divisionCodes.add(tmp.getCode());
            tmp = tmp.getParent();
        }
        while (tmp != null);
        Collections.reverse(divisionCodes);

        context.put("d0", division.getCode());
        for (int i=0; i < divisionCodes.size(); i++)
        {
            context.put("d"+(i+1), divisionCodes.get(i));
        }

        NumberFormat nf = NumberFormat.getIntegerInstance();
        nf.setMaximumIntegerDigits(2);
        nf.setMinimumIntegerDigits(2);
        context.put("y2", nf.format(year % 100));
        context.put("y4", String.valueOf(year));
        context.put("r", officeCase.getRwa().getRwa());

        context.put("lp", officeCase.nextDocumentSequenceId());
        context.put("pp", officeCase.getOfficeIdPrefix());
        context.put("pm", officeCase.getOfficeIdMiddle());
        context.put("ps", officeCase.getOfficeIdSuffix());

        Preferences prefs = DSApi.context().systemPreferences().node(
            Container.NODE_BASE+"/"+DSDivision.ROOT_GUID);

        String format = prefs.get(Container.DOC_FORMAT, null);
        if (format == null)
            throw new EdmException(sm.getString("NieZdefiniowanoFormatuNumeruPismaWsprawieNieMozna"));

        String officeId = FormatUtils.formatExpression(format, context);

        setCaseDocumentId(officeId);

        // pismo bez referenta dostaje referenta sprawy
        if (clerk == null)
            clerk = officeCase.getClerk();

        addWorkHistoryEntry(Audit.create("officeCase",
            DSApi.context().getPrincipalName(),
            this.containingCase != null?
            sm.getString("PrzeniesieniePismaZeSprawyDoSprawy",this.containingCase.getOfficeId(), officeCase.getOfficeId()):
            sm.getString("UmieszczeniePismaWsprawie",officeCase.getOfficeId()),
            officeCase.getOfficeId(), officeCase.getId()));

        //if (changeCaseFinishDate)
        //	officeCase.notifyDocumentAdded(this);

        this.containingCase = officeCase;
        this.containingCaseId = officeCase.getId();
    }

    /**
     * usuwa ze sprawy
     */
    public void removeFromCase()
    {
    	
        setCaseDocumentId(null);
        this.containingCase = null;
        this.containingCaseId = null;
    }

    protected OfficeDocument()
    {
        super(sm.getString("DokumentKancelaryjny"), sm.getString("DokumentKancelaryjny"));
    }

    /**
     * Dzia�a tak samo, jak {@link Document#find(Long)} z t� r�nic�,
     * �e rzucany jest wyj�tek EdmException, gdy ��dany dokument nie
     * jest instancj� OfficeDocument.
     */
    public static OfficeDocument findOfficeDocument(Long id)
        throws EdmException, DocumentNotFoundException
    {
        Document document = Document.find(id);

        if (!(document instanceof OfficeDocument))
            throw new EdmException(sm.getString("DokumentNieJestPismemKancelaryjnym", id));

        return (OfficeDocument) document;
    }


    public static OfficeDocument find(Long id) throws EdmException, DocumentNotFoundException{
		return findOfficeDocument(id);
	}
    /**
     * Wyszukuje Dokument z mozliwoscia szukania po sprawdzaniu  uprawnie� do dokumentu lub bez sprawdzania uprawnien  ;
     * @param id
     * @param checkPermission
     * @return
     * @throws EdmException
     * @throws DocumentNotFoundException
     */
    /*public static OfficeDocument find(Long id , boolean checkPermission) throws EdmException, DocumentNotFoundException{
		return findOfficeDocument(id , checkPermission);
	}*/
    public static OfficeDocument findOfficeDocument(Long id, boolean checkPermission) throws EdmException, DocumentNotFoundException
	{
    	 Document document = Document.find(id, checkPermission);
    	  return (OfficeDocument) document;
	}

	@SuppressWarnings("unchecked")
	public static List<OfficeDocument> findByOfficeNumberRange(Integer officeNumberStart, Integer officeNumberEnd, Long journalId) throws Exception
    {
    	List<OfficeDocument> result = new ArrayList<OfficeDocument>();

    	Journal.JournalQuery query = new Journal.JournalQuery(journalId,0,0);
    	query.setFromNum(officeNumberStart);
    	query.setToNum(officeNumberEnd);

    	List<Journal.EntryBean> journalsEntries = Journal.search(query);

    	for(Iterator<Journal.EntryBean> iter=journalsEntries.iterator(); iter.hasNext();)
    	{
    		EntryBean entry = (Journal.EntryBean) iter.next();
            result.add((OfficeDocument) OfficeDocument.find(entry.getDocumentId()));
    	}

    	Collections.sort(result, new OfficeDocument.OfficeDocumentComparatorByOfficeNumber());

    	return result;
    }

    public static ArrayList findByOfficeNumber(Long officeNumberStart, Long officeNumberEnd, Long journalId) throws Exception{
      	 if (officeNumberStart == null || officeNumberEnd==null)
               throw new NullPointerException("officeNumber");

       try
       {
           Criteria criteria = DSApi.context().session().createCriteria(JournalEntry.class);
           criteria.add( org.hibernate.criterion.Expression.eq("journal.id", journalId));

           List entries = criteria.list();
           ArrayList<Document> documents = new ArrayList<Document>();

           for (Iterator iter=entries.iterator(); iter.hasNext(); )
           {

               JournalEntry entry = (JournalEntry) iter.next();
               Long id = entry.getDocumentId();

               if(id!=null){
              	 OfficeDocument doc =(OfficeDocument)pl.compan.docusafe.core.base.Document.find(id);
              	 if(doc.getOfficeNumber() != null && doc.getOfficeNumber()>=officeNumberStart && doc.getOfficeNumber()<=officeNumberEnd){
              		documents.add(doc);
              	 }

               }
               else{
              	 throw new Exception("Dokument o id= "+id+" nie istnieje");

               }
           }

           return documents;
       }
       catch (HibernateException e)
       {
           throw new EdmHibernateException(e, "id=");
       }
    }

    public static List<OfficeDocument> findAllByBarcode(String barcode) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(OfficeDocument.class);
            criteria.add(Restrictions.eq("barcode", barcode));

            return criteria.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e, "id=");
        }
    }

    /**
     *
     * @param barcode
     * @return
     * @throws EdmException
     */
    public static List<Long> findDocumentsIdByBarcode(String barcode) throws EdmException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Long> results = Lists.newArrayList();

        try {
            ps = DSApi.context().prepareStatement("select id from dso_in_document where barcode = ? " +
                    "UNION ALL " +
                    "select id from dso_out_document where barcode = ? ");

            ps.setString(1, SqlUtils.escapeSql(barcode));
            ps.setString(2, SqlUtils.escapeSql(barcode));

            rs = ps.executeQuery();

            while(rs.next()){
                results.add(rs.getLong("id"));
            }
        } catch (Exception e) {
            throw new EdmException(e);
        } finally{
            DbUtils.closeQuietly(ps);
            DbUtils.closeQuietly(rs);
        }

        return results;
    }

    public static List<OfficeDocument> findAllByVersionGuid(Long id) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(OfficeDocument.class);
            criteria.add(Restrictions.eq("versionGuid", id));

            return criteria.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e, "id=");
        }
    }

    public void create() throws EdmException
    {
        setOfficeStatus(STATUS_NEW);
        super.create();
        int count  = 0;
        for (Recipient rec : getRecipients()) 
        {
			rec.setDocument(this);
			rec.setPosn(count);
			count ++;
			DSApi.context().session().save(rec);
		}
        if (getSender() != null) 
        {
            getSender().setDocument(this);
        }
       
        
        addWorkHistoryEntry(Audit.create("craetion",DSApi.context().getPrincipalName(),
        		sm.getString("UtworzonoDokument")));
    }

    public String getClerk()
    {
        return clerk;
    }

    public boolean canSetClerk() throws EdmException
    {
    	if (!permissionsOn)
    		return true;
        return DSApi.isContextOpen() &&
            DSApi.context().hasPermission(DSPermission.PISMO_ZMIANA_REFERENTA);
    }

    public void setClerk(String clerk)
        throws EdmException, AccessDeniedException
    {
        if (clerk == null)
            throw new NullPointerException("clerk");

        if (permissionsOn && DSApi.isContextOpen() &&
            getId() != null &&
            !clerk.equals(this.clerk) &&
            !DSApi.context().hasPermission(DSPermission.PISMO_ZMIANA_REFERENTA))
            throw new AccessDeniedException(sm.getString("BrakUprawnienDoZmianyReferentaPisma"));

        if (getId() != null && (this.clerk == null || !this.clerk.equals(clerk)))
        {
            String oldClerk = this.clerk != null ? DSUser.safeToFirstnameLastname(this.clerk) : null;
            String newClerk = DSUser.safeToFirstnameLastname(clerk);

            if (oldClerk != null )
            	addWorkHistoryEntry(Audit.create(
                    "clerk", DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaReferentaZna",oldClerk,newClerk), clerk));
            else
            	addWorkHistoryEntry(Audit.create(
                    "clerk", DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaReferentaNa",newClerk), clerk));
        }

        try {
            if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                JackrabbitPrivileges.instance().revokeReadPrivilege(this.getId(), this.clerk, null, ObjectPermission.SOURCE_CLERK);
                JackrabbitPrivileges.instance().addReadPrivilege(this.getId(), clerk, null, ObjectPermission.SOURCE_CLERK);
            }
        } catch (Exception e) { throw new EdmException(e); }


        this.clerk = clerk;
    }


    public Folder getFolder() throws EdmException, AccessDeniedException
    {
        return super.getFolder(this);
    }

    /**
     * Zwraca numer kancelaryjny w postaci napisu. Je�eli dokument posiada
     * niepuste pole officeNumberSymbol, jest ono uwzgl�dniane w zwracanej
     * warto�ci po znaku '/' (np. 42/AG).
     * <p>
     * Je�eli pismo nie ma nadanego g��wnego numeru kancelaryjnego
     * ({@link #officeNumber}), zwracana jest warto�� null.
     */
    public String getFormattedOfficeNumber() 
    {
		return getFormattedOfficeNumber(officeNumber, dailyOfficeNumber);
	}

	public static String getFormattedOfficeNumber(Integer ko, Integer dko) 
	{
		if (AvailabilityManager.isAvailable("dailyOfficeNumber")) 
		{//dailyOfficeNumber is available
			if (AvailabilityManager.isAvailable("officeNumber")) 
			{//officeNumber is available
				return (ko != null && !ko.equals(0) ? String.valueOf(ko) : "brak") + 
						(dko != null && !dko.equals(0)? ("/") + String.valueOf(dko) : "brak");
			}//officeNumber is available
			else
			{//officeNumber is not avaialable
				return dko != null && !dko.equals(0) ? String.valueOf(dko) : "brak";
			}//officeNumber is not available
		}//dailyOfficeNumber is available
		else 
		{//daliyOfficeNumber is not available
			return ko != null && !ko.equals(0) ? String.valueOf(ko) : "brak";
		}//daliyOfficeNumber is not available
	}

    /**
     * Dodaje wpis do historii dokumentu
     * @param audit
     * Deprecated - trzeba to bedzie zastapic DataMartManagerem
     */
    @Deprecated
    public void addWorkHistoryEntry(Audit audit)
    {
    	try
    	{
    		if (this.getId() != null)
    			DataMartManager.addHistoryEntry(this.getId(), audit);
    		else if (getWorkHistory() !=null)
    			getWorkHistory().add(audit);
    	}
    	catch (Exception e)
    	{
    		e.printStackTrace();
    	}

    }

    public List<AssignmentHistoryEntry> getAssignmentHistory()
    {
        if (assignmentHistory == null)
        	assignmentHistory = DataMartManager.getAssignmentHistory(this);
        return assignmentHistory;
    }

    public List<AssignmentHistoryEntry> getAssignmentHistoryDesc()
    {
        if (assignmentHistory == null)
        	assignmentHistory = DataMartManager.getAssignmentHistoryDesc(this);
        return assignmentHistory;
    }
    /**
     * Dodaje nowy wpis do historii dekretacji.
     */
    public void addAssignmentHistoryEntry(AssignmentHistoryEntry entry) throws EdmException
    {
        addAssignmentHistoryEntry(entry, false, null);
    }

    /**
     * Dodaje nowy wpis do historii dekretacji, jednocze�nie wykonuj�c akcje zwi�zane z histori� listy zada�.
     *
     * UWAGA: Przeniesione do obiektu DataMartManager
     *
     * @param entry
     * @param activityKey  identyfikator zadania, dla kt�rego uaktualniamy histori� listy (mo�e by� r�wne null, w�wczas
     * wyznacznikiem zadania, kt�re mamy uaktualni� jest tylko identyfikator dokumentu - co w wyj�tkowych
     * sytuacjach mo�e spowodowa� pewne nie�cis�o�ci w historii)
     * @param overrideCurrent  true <=> je�li aktualnie dodawana do historii operacja jest "dekretacj� na u�ytkownika"
     * to zostanie nadpisany dotychczasowy wpis (gdy istnieje) zamiast tworzy� nowy - u�ywane g��wnie
     * podczas przyj�cia pisma i natychmiastowej dekretacji np. na koordynatora, aby u�ytkownik przyjmuj�cy pismo
     * nie mia� w historii listy �adnego wpisu
     * @throws EdmException
     */
    public void addAssignmentHistoryEntry(AssignmentHistoryEntry entry, boolean overrideCurrent, String activityKey) throws EdmException
    {
    	DataMartManager.storeAssignmentHistory(this, entry,overrideCurrent,activityKey);
		if (assignmentHistory!=null)
		{
			assignmentHistory.add(entry);
		}
    }


    /** Numer KO */
    public Integer getOfficeNumber()
    {
        return officeNumber;
    }

    public void setOfficeNumber(Integer officeNumber) throws EdmException
    {
        this.officeNumber = officeNumber;
    }

    public Integer getOfficeNumberYear()
    {
        return officeNumberYear;
    }

    public void setOfficeNumberYear(Integer officeNumberYear)
    {
        this.officeNumberYear = officeNumberYear;
    }

    public OfficeCase getContainingCase() throws EdmException
    {
    	if (containingCaseId == null)
    		return containingCase;
        if (containingCase == null || !containingCase.getId().equals(containingCaseId))
        	containingCase = Finder.find(OfficeCase.class, containingCaseId);
        return containingCase;
    }

    public String getCaseDocumentId()
    {
        return caseDocumentId;
    }

    public void setCaseDocumentId(String caseDocumentId)
    {
        this.caseDocumentId = caseDocumentId;
    }

    public String getAssignedDivision()
    {
        return assignedDivision;
    }

    public void setAssignedDivision(String assignedDivision)
        throws EdmException
    {
        if (assignedDivision == null)
            throw new NullPointerException("assignedDivision");

        if (getId() != null &&
            this.assignedDivision != null &&
            !assignedDivision.equals(this.assignedDivision))
        {
            DSDivision division = DSDivision.find(assignedDivision);
            addWorkHistoryEntry(Audit.create(
                "assignedDivision", DSApi.context().getPrincipalName(),
                sm.getString("PismoPrzyjetoWdziale",division.getPrettyPath()), assignedDivision));
        }

        if (!DSDivision.ROOT_GUID.equals(assignedDivision) &&
            STATUS_NEW.equals(officeStatus))
            officeStatus = STATUS_IN_DEPARTMENT;

        this.assignedDivision = assignedDivision;
    }

    /**
     * Powoduje oznaczenie pisma jako anulowanego.
     * @deprecated Anulowanie pism nie jest ju� mo�liwe, poniewa�
     * ta funkcja by�a przez u�ytkownik�w �le zrozumiana i przez
     * to nadu�ywana.
     * @param reason Przyczyna anulowania, nie mo�e by� pusta.
     * @throws EdmException Je�eli podany pow�d anulowania jest pusty.
     * @see #getCancellationReason()
     */
    public void cancel(String reason) throws EdmException
    {
        if (reason == null || StringUtils.isEmpty(reason))
            throw new EdmException(sm.getString("NiePodanoPowoduAnulowaniaPisma"));

        setCancelled(true);
        setCancellationReason(reason);
        setCancelledByUsername(DSApi.context().getPrincipalName());

        addWorkHistoryEntry(Audit.create(
            "cancelled", DSApi.context().getPrincipalName(),
            sm.getString("PismoZostaloAnulowane"), reason));
    }

    /**
     * Funkcja jest prywatna i w przysz�o�ci zostanie usuni�ta.
     * @deprecated Anulowanie pism nie jest ju� mo�liwe, poniewa�
     * ta funkcja by�a przez u�ytkownik�w �le zrozumiana i przez
     * to nadu�ywana.
     */
    private boolean isCancelled()
    {
        return cancelled;
    }

    private void setCancelled(boolean cancelled)
    {
        this.cancelled = cancelled;
    }

    public String getCancellationReason()
    {
        return cancellationReason;
    }

    private void setCancellationReason(String cancellationReason)
    {
        this.cancellationReason = cancellationReason;
    }

    public String getCancelledByUsername()
    {
        return cancelledByUsername;
    }

    private void setCancelledByUsername(String cancelledByUsername)
    {
        this.cancelledByUsername = cancelledByUsername;
    }

    public String getCurrentAssignmentGuid()
    {
        return currentAssignmentGuid;
    }

    public void setCurrentAssignmentGuid(String currentAssignmentGuid)
    {
        this.currentAssignmentGuid = currentAssignmentGuid;
    }

    public String getCurrentAssignmentUsername()
    {
        return currentAssignmentUsername;
    }

    public void setCurrentAssignmentUsername(String currentAssignmentUsername)
    {
        this.currentAssignmentUsername = currentAssignmentUsername;
    }

    public Boolean isCurrentAssignmentAccepted()
    {
        return currentAssignmentAccepted;
    }

    public void setCurrentAssignmentAccepted(Boolean currentAssignmentAccepted)
    {
        this.currentAssignmentAccepted = currentAssignmentAccepted;
    }

    public String getOfficeNumberSymbol()
    {
        return officeNumberSymbol;
    }

    public void setOfficeNumberSymbol(String officeNumberSymbol)
    {
        this.officeNumberSymbol = officeNumberSymbol;
    }

    public String getOfficeStatus()
    {
        return officeStatus;
    }

    public void setOfficeStatus(String officeStatus)
    {
        this.officeStatus = officeStatus;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary) throws EdmException
    {
        if (getId() != null &&
            this.summary != null && !this.summary.equals(summary))
        	addWorkHistoryEntry(Audit.create(
                "summary", DSApi.context().getPrincipalName(),
                sm.getString("ZmianaOpisuPismaNa",summary)));

        this.summary = summary;
        if (summary != null && (getDoctype() == null || !("daa".equals(getDoctype().getCn()))))
        {
            super.description = summary;
            super.title = summary;
        }
    }

    public void setSummaryWithoutHistory(String summary) throws EdmException
    {
        this.summary = summary;
        if (summary != null)
        {
            super.description = summary;
            super.title = summary;
        }
    }

	public void addReply(OfficeDocument d){
		super.addReply(d);
		addWorkHistoryEntry(Audit.create(
                "replies", DSApi.context().getPrincipalName(),
                sm.getString("DodanoOdpowiedz",d.getOfficeNumber())));
	}

    public void setSummaryOnly(String summary) throws EdmException
    {
        this.summary = summary;
    }

    public Journal getJournal()
    {
        return journal;
    }

    public void setJournal(Journal journal)
    {
        this.journal = journal;
    }

    public String getBarcode()
    {
        return barcode;
    }

    public void setBarcode(String barcode) throws EdmException
    {
        if (getId() != null &&
            this.barcode != null && !this.barcode.equals(barcode))
        {
            if (!StringUtils.isEmpty(barcode))
            {
            	addWorkHistoryEntry(Audit.create(
                    "barcode", DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaKoduKreskowegoNa",barcode), barcode));
            }
            else
            {
            	addWorkHistoryEntry(Audit.create(
                    "barcode", DSApi.context().getPrincipalName(),
                    sm.getString("UsunietoKodKreskowy"), null));
            }
            Document.find(getId(), false).setDocumentBarcode(barcode);
        }

        this.barcode = barcode;
        
    }

    public boolean isArchived()
    {
        return archived;
    }

    public void setArchived(boolean archived)
    {
        this.archived = archived;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public abstract String getCreatingUser();

    public abstract List<Recipient> getRecipients() throws EdmException;

    public abstract void addRecipient(Recipient recipient) throws EdmException;
    
    public abstract void removeRecipient(Recipient recipient) throws EdmException;

	public void setAssignmentHistory(List<AssignmentHistoryEntry> assignmentHistory) {
		this.assignmentHistory = assignmentHistory;
	}

    public abstract void setCreatingUser(String principalName);

    public static class OfficeDocumentComparatorByOfficeNumber implements Comparator<OfficeDocument>
    {
        public int compare(OfficeDocument e1, OfficeDocument e2)
        {
            return e1.getOfficeNumber().compareTo(e2.getOfficeNumber());
        }
    }

	public boolean onSave(Session session) throws CallbackException
    {
        try
        {
        	if(getSender() != null)
        	{
	        	getSender().setDictionaryType(null);
	            getSender().setDocument(this);
	            DSApi.context().session().save(getSender());
        	}            
        	if(getSenderId() == null && getSender() != null && getSender().getId() == null)
        		DSApi.context().session().flush();
        	if(sender != null && sender.getId() != null)
        		setSenderId(sender.getId());
        }
        catch (Exception e)
        {
        	log.error(e.getMessage(),e);
        }
		return super.onSave(session);
    }
	
	public void setRecipients(String... recipients) throws EdmException
	{
		String json = null;
        Set<Recipient> newRecipient = new HashSet<Recipient>();
        Map<String,Recipient> recipientsId = new HashMap<String,Recipient>();
        List<Recipient> oldRecipients = new ArrayList<Recipient>(this.getRecipients());
        int counter = 0;
        /** jesli ma id to juz byl w dokumentcie jesli nie to jest nowy*/
        for (int i=0; i < recipients.length; i++)
        {
        	json = recipients[i];
        	Recipient recipient = new Recipient().fromJSON(json);
        	newRecipient.add(recipient);
        	recipient.setPosn(counter);
        	counter++;
        	if(recipient.getId() == null)
        	{
        		this.addRecipient(recipient);
        		DSApi.context().session().save(recipient);
        	}
        	else
        		recipientsId.put(recipient.getId().toString(),recipient);
        }

        for (Recipient r:oldRecipients)
        {
        	Long tmp = r.getId();
        	if(r.getId() != null && !recipientsId.containsKey(tmp.toString()))
        	{
        		r.delete();
        	}
        }
	}

	public Long getContainingCaseId() {
		return containingCaseId;
	}

	public void setContainingCaseId(Long containingCaseId) 
	{
		this.containingCaseId = containingCaseId;
	}
	
	public void setContainingCaseId(Long containingCaseId,boolean changeCaseFinishDate) throws EdmException 
	{
		OfficeCase officeCase = OfficeCase.find(containingCaseId);
		setContainingCase(officeCase, changeCaseFinishDate);
		this.containingCaseId = containingCaseId;
	}

	public Sender getSender() throws EdmException
	{
		if(senderId==null && sender == null) {
			return null;
		}
    	if (senderId == null)
    		return sender;
        if (sender == null || !sender.getId().equals(senderId))
        	sender = Finder.find(Sender.class, senderId);
        return sender;
	}

	public void setSender(Sender sender)
	{
		sender.setDocumentId(getId());
		this.senderId = sender.getId();
		this.sender = sender;
	}

	public Long getSenderId() {
		return senderId;
	}

	public void setSenderId(Long senderId) {
		this.senderId = senderId;
	}

	public Long getMasterDocumentId() {
		return masterDocumentId;
	}

	public void setMasterDocumentId(Long masterDocumentId) {
		this.masterDocumentId = masterDocumentId;
	}

	public Integer getDailyOfficeNumber() {
		return dailyOfficeNumber;
	}

	public void setDailyOfficeNumber(Integer dailyOfficeNumber) {
		this.dailyOfficeNumber = dailyOfficeNumber;
	}
    public String getPostalRegNumber()
    {
        return null;
    }

    public void setPostalRegNumber(String postalRegNumber) throws EdmException {

	}
    
    public long getVersionGuid()
    {
    	return versionGuid;
    }
    
    public void setVersionGuid(long versionGuid)
    {
    	this.versionGuid=versionGuid;
    }

	public int getVersion()
	{
		return version;
	}

	public void setVersion(int version) 
	{
		this.version = version;
	}

	public String getVersionDesc() 
	{
		return versionDesc;
	}

	public void setVersionDesc(String versionDesc) 
	{
		this.versionDesc = versionDesc;
	}

	public Boolean getCzyCzystopis() 
	{
		return czyCzystopis;
	}

	public void setCzyCzystopis(Boolean czyCzystopis) 
	{
		this.czyCzystopis = czyCzystopis;
	}

	public Boolean getCzyAktualny()
	{
		return czyAktualny;
	}

	public void setCzyAktualny(Boolean czyAktualny)
	{
		this.czyAktualny = czyAktualny;
	}

	public Boolean getIsArchived() {
		return isArchived;
	}

	public void setIsArchived(Boolean isArchived) {
		this.isArchived = isArchived;
	}

	public Date getArchivedDate() {
		return archivedDate;
	}

	public void setArchivedDate(Date archivedDate) {
		this.archivedDate = archivedDate;
	}
	
	public Long getAccessToDocument() 
	{
		return accessToDocument;
	}
	
	public void setAccessToDocument(Long accessToDocument)
	{
		this.accessToDocument=accessToDocument;
	}

	public void setCountryLetter(Boolean countryLetter)
	{
		this.countryLetter = countryLetter;
	}

	public Boolean getCountryLetter()
	{
		return countryLetter;
	}

    /**
     * Zwraca true jestli dokument ma status new.
     * @return
     */
    public boolean isOfficeStatusNew(){
        if(STATUS_NEW.equals(officeStatus))
            return true;

        return false;
    }

    public String getDocumentTypeAsString() {
        if (getType() == DocumentType.INCOMING)
            return " przychodz�ce ";
        if (getType() == DocumentType.OUTGOING)
            return " wychodz�ce ";
        if (getType() == DocumentType.INTERNAL)
            return " wewn�trzne ";
        return "";
    }

    public abstract void bindToJournal(Long journalId, Integer sequenceId) throws EdmException;

    public abstract Date getDocumentDate();

}
