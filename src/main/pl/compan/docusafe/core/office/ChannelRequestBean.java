package pl.compan.docusafe.core.office;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;

public class ChannelRequestBean {
	
	public enum ChannelRequestStatus {
		New, Processed, Invalid;
		
		public String print() {
			switch(this) {
				case New : return "Nowe";
				case Processed : return "Przetworzone";
				case Invalid : return "Nieprawidłowe";
			}
			return this.name();
		}
		
		public boolean isOpen() {
			return this.equals(ChannelRequestStatus.New);
		}
		
		public boolean isInvalid() {
			return this.equals(ChannelRequestStatus.Invalid);
		}
		
		public boolean isProcessed() {
			return this.equals(ChannelRequestStatus.Processed);
		}
		
		
	}
	private Long id;
	private String content;
	private String comment;
	private Date incomingDate;
	private Date processDate;
	private ChannelRequestStatus status;
	
	public static List<ChannelRequestBean> getChannelRequestBeans() throws EdmException {
		return Finder.list(ChannelRequestBean.class, "incomingDate");
	}
	
	public void process(String comment) {
		this.status = ChannelRequestStatus.Processed;
		this.comment = comment;
		processDate = new Date();
	}
	
	public void markInvalid(String comment) {
		this.status = ChannelRequestStatus.Invalid;
		this.comment = comment;
		processDate = new Date();
	}
	
	public static ChannelRequestBean create(String content) throws EdmException {
		ChannelRequestBean newBean = new ChannelRequestBean();
		newBean.content = content;
		newBean.status = ChannelRequestStatus.New;
		newBean.incomingDate = new Date();
		Persister.create(newBean);
		return newBean;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getIncomingDate() {
		return incomingDate;
	}

	public void setIncomingDate(Date incomingDate) {
		this.incomingDate = incomingDate;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public ChannelRequestStatus getStatus() {
		return status;
	}

	public void setStatus(ChannelRequestStatus status) {
		this.status = status;
	}
	
}
