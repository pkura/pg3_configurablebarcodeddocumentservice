package pl.compan.docusafe.core.office;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

/**
 * 
 * @author Mariusz Kiljanczyk
 */
public class InvoicesCpvCodes
{
    private Integer id;
    private String name;
    private String category;
    
    
    public InvoicesCpvCodes()
    {
    }
    
    public InvoicesCpvCodes(String string, String category)
    {
        this.name = string;
        this.category = category;
    } 

    /**
     * Zwraca list� wszystkich obiekt�w InvoicesCpvCodes
     * znajduj�cych si� w bazie posortowanych po polu posn
     * (zainicjalizowane proxy).
     * @return
     * 
     * @throws EdmException
     */
    public static List<InvoicesCpvCodes> list() throws EdmException
    {
        List<InvoicesCpvCodes> list = Finder.list(InvoicesCpvCodes.class,"name");
        for (Iterator iter=list.iterator(); iter.hasNext(); )
        {
            DSApi.initializeProxy(iter.next());
        }
        return list;
    }
    
    /**
     * 
     * @param id
     * @return Zwraca rodzaj dokumentu faktury o podanym id
     * @throws EdmException
     */
    public static InvoicesCpvCodes find(Integer id) throws EdmException
    {
        return (InvoicesCpvCodes) Finder.find(InvoicesCpvCodes.class, id);

    }
    /**
     * @return Sprawdza czy mo�na usun�� dany typ faktury
     * @throws EdmException
     */
    public boolean canDelete() throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias docTable = from.createTable(DSApi.getTableName(Invoice.class));

        WhereClause where = new WhereClause();
        where.add(Expression.eq(docTable.attribute("kind"), getId()));

        SelectClause selectCount = new SelectClause();
        selectCount.addSql("count(*)");

        SelectQuery selectQuery;

        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);

            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            boolean unable = false;
            int count = -1;
            if (rs.next())
            {
            	count = rs.getInt(1);
            }
            else
            {
            	unable = true;
            }            
            rs.close();
            if (unable)
            {
                throw new EdmException("Nie mo�na pobra� liczby faktur");
            }
            return count == 0;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }
    
    public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    
    public String getCategory()
    {
        return category;
    }
    
    public void setCategory(String category)
    {
        this.category = category;
    }
    
    public Integer getId()
    {
        return id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
}
