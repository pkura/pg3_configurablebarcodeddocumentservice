package pl.compan.docusafe.core.office;

import pl.compan.docusafe.util.IdUtils;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InOfficeDocumentKindRequiredAttachment.java,v 1.1 2004/11/02 16:02:09 lk Exp $
 */
public class InOfficeDocumentKindRequiredAttachment
{
    private String sid;
    private String title;

    private InOfficeDocumentKindRequiredAttachment()
    {
    }

    InOfficeDocumentKindRequiredAttachment(String title)
    {
        this.title = title;
        this.sid = IdUtils.getGuid();
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getSid()
    {
        return sid;
    }

    private void setSid(String sid)
    {
        this.sid = sid;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof InOfficeDocumentKindRequiredAttachment)) return false;

        final InOfficeDocumentKindRequiredAttachment inOfficeDocumentKindRequiredAttachment = (InOfficeDocumentKindRequiredAttachment) o;

        if (sid != null ? !sid.equals(inOfficeDocumentKindRequiredAttachment.sid) : inOfficeDocumentKindRequiredAttachment.sid != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (sid != null ? sid.hashCode() : 0);
    }
}
