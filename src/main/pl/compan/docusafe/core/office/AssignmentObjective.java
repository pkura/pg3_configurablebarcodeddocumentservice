package pl.compan.docusafe.core.office;

import org.hibernate.*;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Expression;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;

import java.io.Serializable;
import java.util.List;

/**
 * Cel dekretacji pisma.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AssignmentObjective.java,v 1.7 2006/12/07 13:51:26 mmanski Exp $
 */
public class AssignmentObjective implements Lifecycle
{
    private Integer id;
    private String name;
    private String officeStatus;
    private String documentType;
    private Integer posn;

    protected AssignmentObjective()
    {
    }

    public AssignmentObjective(String name)
    {
        this.name = name;
    }

    public static List<AssignmentObjective> list() throws EdmException
    {
        return Finder.list(AssignmentObjective.class, "posn");
    }

    public static AssignmentObjective find(Integer id) throws EdmException
    {
        return (AssignmentObjective) Finder.find(AssignmentObjective.class, id);
    }

    public static List<AssignmentObjective> findByName(String name) throws EdmException
    {
        Criteria crit = DSApi.context().session().createCriteria(AssignmentObjective.class);
        crit.add(Expression.eq("name", name));

        try
        {
            return crit.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void create() throws EdmException
    {
        Persister.create(this);
    }

    public void delete() throws EdmException
    {
        Persister.delete(this);
    }

    /* Lifecycle */

    public boolean onSave(Session s) throws CallbackException
    {
        if (id != null)
        {
            posn = id;
        }
        else
        {
            // w bazach, gdzie identyfikatory pochodz� z pola autonumber
            // id b�dzie w tym momencie r�wne null
            posn = Dictionaries.updatePosnOnSave(getClass());
        }
        return false;
    }

    public boolean onUpdate(Session s) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session s) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session s, Serializable id)
    {
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getPosn()
    {
        return posn;
    }

    public void setPosn(Integer posn)
    {
        this.posn = posn;
    }

    public String getOfficeStatus()
    {
        return officeStatus;
    }

    public void setOfficeStatus(String officeStatus)
    {
        this.officeStatus = officeStatus;
    }

    public String getDocumentType()
    {
        return documentType;
    }

    public void setDocumentType(String documentType)
    {
        this.documentType = documentType;
    }
}
