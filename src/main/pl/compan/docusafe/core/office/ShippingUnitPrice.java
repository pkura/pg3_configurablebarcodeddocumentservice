/**
 * 
 */
package pl.compan.docusafe.core.office;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name="dso_shipping_unit_prices")
public class ShippingUnitPrice {
    @Id
    @Column(name="ID")
	private Integer id;

    @Column(name="cn")
    private String cn;

    @Column(name="title")
    private String title;

    @ManyToOne
    @JoinColumn(name="gauge")
    private Gauge gauge;

    @ManyToOne
    @JoinColumn(name="weight")
    private MailWeightKind weight;

    @ManyToOne
    @JoinColumn(name="documentDelivery")
    private OutOfficeDocumentDelivery documentDelivery;

    @Column(name="zpo")
    private Boolean zpo;

    @Column(name="price")
    private BigDecimal price;

	public ShippingUnitPrice() {
		
	}
	
	public static ShippingUnitPrice find(Integer id) throws EdmException {
		return Finder.find(ShippingUnitPrice.class, id);
	}
	
	public static List<ShippingUnitPrice> list() throws EdmException {
		return Finder.list(ShippingUnitPrice.class, "id");
	}

    public static ShippingUnitPrice findByParameters(Gauge gauge,
                                                   MailWeightKind weight,
                                                   OutOfficeDocumentDelivery documentDelivery,
                                                   Boolean zpo) throws EdmException {

            Criteria criteria = DSApi.context().session().createCriteria(ShippingUnitPrice.class);
            criteria.add(Restrictions.eq("gauge", gauge));
            criteria.add(Restrictions.eq("weight", weight));
            criteria.add(Restrictions.eq("documentDelivery", documentDelivery));
            criteria.add(Restrictions.eq("zpo", zpo));

            if (criteria.list().size() == 0)
                return null;
            return (ShippingUnitPrice) criteria.list().get(0);
    }

    public void create() throws EdmException {
		Persister.create(this);
	}
	
	public void delete() throws EdmException {
		Persister.delete(this);
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCn() {
		return cn;
	}
	
	public void setCn(String cn) {
		this.cn = cn;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

    public Gauge getGauge() {
        return gauge;
    }

    public void setGauge(Gauge gauge) {
        this.gauge = gauge;
    }

    public MailWeightKind getWeight() {
        return weight;
    }

    public void setWeight(MailWeightKind weight) {
        this.weight = weight;
    }

    public OutOfficeDocumentDelivery getDocumentDelivery() {
        return documentDelivery;
    }

    public void setDocumentDelivery(OutOfficeDocumentDelivery documentDelivery) {
        this.documentDelivery = documentDelivery;
    }

    public Boolean getZpo() {
        return zpo;
    }

    public void setZpo(Boolean zpo) {
        this.zpo = zpo;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}

