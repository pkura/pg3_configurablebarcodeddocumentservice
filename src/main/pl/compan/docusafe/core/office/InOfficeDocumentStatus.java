package pl.compan.docusafe.core.office;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.web.office.in.DocumentMainAction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InOfficeDocumentStatus.java,v 1.15 2009/03/04 09:27:51 pecet3 Exp $
 */
public class InOfficeDocumentStatus extends OfficeDocumentStatus
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(InOfficeDocumentStatus.class.getPackage().getName(),null);
    public static final Integer ID_FINISHED = new Integer(4); // zako�czone
    public static final Integer ID_OPEN = new Integer(2); // w toku

    InOfficeDocumentStatus()
    {
    }

    public InOfficeDocumentStatus(String name)
    {
        super(name);
    }

    /**
     * Zwraca list� wszystkich status�w (zainicjalizowane proxy).
     * @return
     * @throws EdmException
     */
    public static List<InOfficeDocumentStatus> list() throws EdmException
    {
        List<InOfficeDocumentStatus> list = Finder.list(InOfficeDocumentStatus.class, "id");
        for (Iterator iter=list.iterator(); iter.hasNext(); )
        {
            DSApi.initializeProxy(iter.next());
        }
        return list; 
    }

    public static InOfficeDocumentStatus find(Integer id) throws EdmException
    {
        InOfficeDocumentStatus status = (InOfficeDocumentStatus) DSApi.getObject(InOfficeDocumentStatus.class, id);

        if (status == null)
            throw new EntityNotFoundException(InOfficeDocumentStatus.class, id);

        return status;
    }

    public static InOfficeDocumentStatus findByCn(String cn) throws EdmException
    {
        if (cn == null)
            throw new NullPointerException("cn");

        try
        {
            List list = DSApi.context().classicSession().find("from s in class "+
                InOfficeDocumentStatus.class.getName()+" where s.cn = ?",
                cn, Hibernate.STRING);

            if (list != null && list.size() > 0)
            {
                return (InOfficeDocumentStatus) list.get(0);
            }

            throw new EntityNotFoundException(sm.getString("NieZnalezionoStatusuOcn")+" = "+cn);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public final boolean canDelete() throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias docTable = from.createTable(DSApi.getTableName(InOfficeDocument.class));

        WhereClause where = new WhereClause();
        where.add(Expression.eq(docTable.attribute("status"), getId()));

        SelectClause selectCount = new SelectClause();
        /*SelectColumn countCol = */selectCount.addSql("count(*)");

        SelectQuery selectQuery;

        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            

            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            boolean unable = false;
            int count = -1;
            if (rs.next())
            {
            	count = rs.getInt(1);
            }
            else
            {
            	unable = true;
            }
            rs.close();
            if (unable)
            {
                throw new EdmException(sm.getString("NieMoznaPobracLiczbyPism"));
            }
            return count == 0 ? true : false;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

}
