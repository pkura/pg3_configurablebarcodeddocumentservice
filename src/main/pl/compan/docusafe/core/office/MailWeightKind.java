package pl.compan.docusafe.core.office;

import org.hibernate.CallbackException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

/**
 * MailWeightKind
 *
 * @version $Id$
 */
public class MailWeightKind implements Lifecycle {

	private Integer id;
	private String name;
	private Integer posn;
	private String cn;
	private Integer weightG;
	private Integer weightKg;

	@Override
	public boolean onSave(Session s) throws CallbackException {

		if (id != null) {
			posn = id;
		} else {
			posn = Dictionaries.updatePosnOnSave(getClass());
		}

		return false;
	}

	@Override
	public boolean onUpdate(Session s) throws CallbackException {
		return false;
	}

	@Override
	public boolean onDelete(Session s) throws CallbackException {
		return false;
	}

	@Override
	public void onLoad(Session s, Serializable id) {

	}

    public static MailWeightKind find(Integer id) throws EdmException {
        return Finder.find(MailWeightKind.class, id);
    }

	public static List<MailWeightKind> list() throws EdmException {

		List<MailWeightKind> list = Finder.list(MailWeightKind.class, "id");
		for (Iterator iter = list.iterator(); iter.hasNext(); ) {
			DSApi.initializeProxy(iter.next());
		}

		return list;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPosn() {
		return posn;
	}

	public void setPosn(Integer posn) {
		this.posn = posn;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public Integer getWeightKg() {
		return weightKg;
	}

	public void setWeightKg(Integer weightKg) {
		this.weightKg = weightKg;
	}

	public Integer getWeightG() {
		return weightG;
	}

	public void setWeightG(Integer weightG) {
		this.weightG = weightG;
	}
}
