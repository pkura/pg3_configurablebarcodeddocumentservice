package pl.compan.docusafe.core.office;

import org.apache.commons.lang.StringUtils;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.labels.DocumentToLabelBean;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.IdUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.*;
import pl.compan.docusafe.webwork.event.ActionEvent;
import std.lambda;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Pismo przychodz�ce.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InOfficeDocument.java,v 1.151 2010/03/15 08:08:36 mariuszk Exp $
 */
public class InOfficeDocument extends OfficeDocument
    implements PropertyChangeListener
{
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(InOfficeDocument.class.getPackage().getName(),null);

    private static final Logger log = LoggerFactory.getLogger(InOfficeDocument.class);
    
    /**
     * @author Jan �wi�cki <jan.swiecki@docusafe.pl>
     */
//    private static final Logger lnd = LoggerFactory.getLogger("lnd");
    
    public static final String TYPE = "in";

    /**
     * Warto�� dla pola Attachment.lparam okre�laj�ca, �e za��cznik
     * przedstawiono do wgl�du.
     */
    public static final String ATTACHMENT_SEEN = "seen";
    /**
     * Warto�� dla pola Attachment.lparam okre�laj�ca, �e za��cznik
     * jest posiadany (w aktach).
     */
    public static final String ATTACHMENT_FILED = "filed";
    /**
     * Warto�� dla pola Attachment.lparam okre�laj�ca, �e za��cznika
     * nie ma (brak).
     */
    public static final String ATTACHMENT_MISSING = "missing";
    /**
     * Warto�� dla pola Attachment.lparam okre�laj�ca, �e za��cznik
     * b�dzie okazany/uregulowany przy odbiorze pisma.
     */
    public static final String ATTACHMENT_INSPE = "inspe";

    public static String getAttachmentLparamDescription(String lparam)
        throws EdmException
    {
        if (ATTACHMENT_SEEN.equals(lparam))
        {
            return "przedstawiono do wgl�du";
        }
        else if (ATTACHMENT_FILED.equals(lparam))
        {
            return "posiadany";
        }
        else if (ATTACHMENT_INSPE.equals(lparam))
        {
            return "do okazania/uregulowania przy odbiorze";
        }
        else if (ATTACHMENT_MISSING.equals(lparam))
        {
            return "brak";
        }
        else
        {
            throw new EdmException("Nieznana warto�� lparam: "+lparam);
        }
    }

    /**
     * Data, jaka znajduje si� w nag��wku pisma.
     */
    private Date documentDate;
    /**
     * Data ostemplowania listu przez poczt�.
     */
    private Date stampDate;
    /**
     * Data przyj�cia dokumentu do aplikacji.
     */
    private Date incomingDate;
    /**
     * Numer przesy�ki rejestrowanej ("erka").
     */
    private String postalRegNumber;
    /**
     * Flaga oznaczaj�ca, �e nadawca jest anonimowy.
     */
    private boolean senderAnonymous;
    /**
     * Dyspozycje.
     */
    private String packageRemarks;
    /**
     * Inne uwagi.
     */
    private String otherRemarks;
    /**
     * Liczba za��cznik�w do pisma (nie ma nic wsp�lnego z obiektami
     * Attachment zwi�zanymi z obiektem Document).
     */
    private int numAttachments;
    /**
     * Identyfikator u�ywany do �ledzenia dokumentu w systemach typu BIP.
     */
    private String trackingNumber;
    private String trackingPassword;
    /**
     * Flaga okre�laj�ca, czy dokument ma by� przesy�any do BIP firmy ASI.
     */
    private boolean submitToBip;
    /**
     * Identyfikator okre�laj�cy email (ds_message) powi�zany z pismem
     */
    private Long messageId;
//    /**
//     * Je�eli bie��cy dokument jest kopi� innego, "g��wnego" dokumentu,
//     * odno�nik do tego dokumentu znajdzie si� w tym polu.
//     */
//    private IncomingDocumentAttributes relatedTo;
//    /**
//     * Zbi�r dokument�w, kt�re s� kopiami bie��cego dokumentu. Ka�dy
//     * z dokument�w w tej kolekcji powinien odwo�ywa� si� do dokumentu
//     * bie��cego w polu relatedTo.
//     */
//    private Set relatedDocuments;
    /**
     * Po�o�enie dokumentu wpisywane przez u�ytkownika.
     */
    private String location;

    //czy dokument jest oryginalem
    private Boolean original;

    /**
     * Rodzaj pisma.
     */
    private InOfficeDocumentKind kind;
    /**
     * U�ytkownik przyjmuj�cy pismo.
     */
    /**
     * Rodzaj faktury
     */
    private InvoicesKind invoiceKind;
    
    /**
     * Id Umowy wykorzystywane przy module Umowy z administracji 
     */
    private Long contractId;
  
	private boolean czyAktaulny;

	private String creatingUser;
    /**
     * U�ytkownik DocuSafe odbierarj�cy pismo.
     */
    private String recipientUser;

    private InOfficeDocumentStatus status;

    /**
     * Kolekcja obiekt�w {@link Recipient}.
     */
    private List<Recipient> recipients = null;

    private Set<OutOfficeDocument> outDocuments = null;

    private String referenceId;

	/**
	 * Dokument glowny
	 */
    private InOfficeDocument masterDocument = null;

    private InOfficeDocumentDelivery delivery;

    private OutOfficeDocumentDelivery outgoingDelivery;

    private boolean bok;

    private boolean replyUnnecessary;
    
    private List<OutOfficeDocument> outDocumentsAsList;
    
    @Override public DocumentType getType()
    {
        return DocumentType.INCOMING;
    }

    @Override public String getStringType()
    {
        return InOfficeDocument.TYPE;
    }

    /* PropertyChangeListener */

    public void propertyChange(PropertyChangeEvent evt)
    {
        if (evt.getSource() instanceof OutOfficeDocument &&
            "::assignOfficeNumber".equals(evt.getPropertyName()))
        {
            OutOfficeDocument doc = (OutOfficeDocument) evt.getSource();
            addWorkHistoryEntry(Audit.create("::reply",
                DSApi.context().getPrincipalName(),
                sm.getString("UtworzonoOdpowiedzNapismoWychodzaceNr",doc.getOfficeNumber()), null, doc.getId()));
        }
    }


    public InOfficeDocument()
    {
    	super();
    	this.original = true;
    }


    protected void duplicate(Long[] attachmentIds) throws EdmException
    {
        if (super.getSender() != null)
        {
        	super.setSender((Sender) super.getSender().cloneObject());
        }

        List<Recipient> savedRecipients = new ArrayList<Recipient>(this.getRecipients());
        this.recipients = null;
        if (savedRecipients != null)
        {
            this.recipients = new ArrayList<Recipient>(savedRecipients.size());
            for (Recipient recipient : savedRecipients)
            {
                recipient = (Recipient) recipient.cloneObject();
                this.addRecipient(recipient);
            }
        }
        /**Nie mozna powiazac*/
        this.outDocuments = null;

        super.duplicate(attachmentIds);
    }


    /**
     * Zapisuje dokument w sesji Hibernate.
     * <p>
     * Zapisuje w sesji warto�� pola sender (je�eli istnieje) oraz
     * przypisuje dokumentowi folder SN_OFFICE. Nast�pnie wywo�uj�
     * metod� {@link pl.compan.docusafe.core.base.Document#create()}.
     * <p>
     * Pismo wpisywane jest do g��wnego dziennika pism przychodz�cych
     * i dostaje numer kancelaryjny (officeNumber) pochodz�cy z tego
     * dziennika.
     */
    public void create() throws EdmException
    {
//        if (journal != null && !journal.isMain() && journal.getSymbol() == null)
//            throw new EdmException("Je�eli dokument jest przyjmowany w innym dzienniku " +
//                "ni� g��wny, dziennik ten musi mie� okre�lony symbol");
    	if(original==null){
    		original=false;
    	}
        try
        {
            if(getDoctype() == null || !"daa".equals(getDoctype().getCn()))    {
                setFolder(Folder.findSystemFolder(Folder.SN_OFFICE));
            }

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        
        super.create();
        /*if(this.isSubmitToBip())
        {
        	System.out.println("dasd");
        	DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_DOCUMENT_INFO).documentId(this.getId());
        	DataMartEventBuilder.get().log();
        }*/

        // dopiero teraz dokument dosta� id

/*
        if (journal == null)
            journal = Journal.getMainIncoming();
        JournalEntry entry = journal.newEntry(this.getId());

        String sYear = DSApi.context().getProperty(Properties.OPEN_YEAR);
        if (sYear == null)
            throw new EdmException("W systemie nie otwarto roku, nie mo�na zrobi� wpisu w dzienniku");

        setJournal(journal);
        setOfficeNumber(entry.getSequenceId());
        setOfficeNumberYear(new Integer(sYear));
        setOfficeNumberSymbol(journal.isMain() ? null : journal.getSymbol());
*/
    }
    public void bindToJournal(Long journalId, Integer sequenceId) throws EdmException
    {
    	bindToJournal(journalId, sequenceId,null);
    }
    
    public void bindToJournal(Long journalId, Integer sequenceId, ActionEvent event) throws EdmException
    {
        if (journalId == null)
            throw new NullPointerException("journalId");

        if (sequenceId == null)
            throw new NullPointerException("sequenceId");

        int year = GlobalPreferences.getCurrentYear();

        getDocumentKind().logic().bindToJournal(this, event);
        Journal journal = Journal.find(journalId);
        JournalEntry entry = journal.findEntry(sequenceId);

        if (getJournal() == null || getJournal().equals(journal))
            setJournal(journal);
        setDailyOfficeNumber(entry.getDailySequenceId());
        setOfficeNumber(entry.getSequenceId());
        setOfficeNumberYear(new Integer(year));
        setOfficeNumberSymbol(entry.getJournal().isMain() ? null : entry.getJournal().getSymbol());
    }

    public static InOfficeDocument findInOfficeDocument(Long id)
        throws EdmException, DocumentNotFoundException, DocumentLockedException,
        AccessDeniedException
    {
        Document doc = find(id);

        if (doc.getType() != DocumentType.INCOMING)
        {
            log.error("Nie znaleziono dokumetu przychodz�cego nr "+id+", znaleziony dokument: "+doc);
            throw new EdmException("Nie znaleziono dokumetu przychodz�cego nr "+id);
        }

        return (InOfficeDocument) doc;
    }

    /**
     * Zwraca wszystkie dokumenty przypisane o danej sprawy.
     */
    public static List<InOfficeDocument> findByOfficeCase(OfficeCase officeCase)
        throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(InOfficeDocument.class);
        try
        {
            c.add(Restrictions.eq("containingCaseId", officeCase.getId()));
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }


    /**
     * Zwraca wszystkie dokumenty o danym numerze kancelaryjnym, od
     * najstarszego do najnowszego.
     */
    public static List findByOfficeNumber(int officeNumber)
        throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(InOfficeDocument.class);
        c.add(org.hibernate.criterion.Expression.eq("officeNumber", new Integer(officeNumber)));
        c.addOrder(org.hibernate.criterion.Order.asc("incomingDate"));
        try
        {
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Zwraca liste dokumentow o danym numerze barcode.
     */
    public static List<InOfficeDocument> findByBarcode(String barcode, Criteria criteria) throws EdmException {
    	try {
    		return criteria.list();
    	} catch(HibernateException e) {
    		log.error(e.getMessage(), e);
    		throw new EdmException("Nie znaleziono dokumentu o podanym numerze barcode " + barcode);
    	}
    }

    public static List<InOfficeDocument> findByBarcode(String barcode) throws EdmException {
    	Criteria crit = DSApi.context().session().createCriteria(InOfficeDocument.class);
    	crit.add(Restrictions.eq("barcode", barcode));
        return findByBarcode(barcode, crit);
    }

    public static List<InOfficeDocument> findByBarcodePrefix(String barcode) throws EdmException {
    	Criteria crit = DSApi.context().session().createCriteria(InOfficeDocument.class);
    	crit.add(Restrictions.like("barcode", barcode, MatchMode.START));
        return findByBarcode(barcode, crit);
    }

    public static InOfficeDocument findByPostalRegNumber(String postalRegNumber) throws EdmException {
    	Criteria crit = DSApi.context().session().createCriteria(InOfficeDocument.class);
    	crit.add(Restrictions.eq("postalRegNumber", postalRegNumber));
    	InOfficeDocument doc = null;
    	
    	try {
    		doc = (InOfficeDocument) crit.uniqueResult();
    	} catch(HibernateException e) {
    		log.error(e.getMessage(), e);
    		throw new EdmException("Istnieje wi�cej ni� jeden dokument o numerze przesy�ki rejestrowanej " + postalRegNumber);
    	}
    	
    	if(doc == null) {
    		throw new EdmException("Nie znaleziono dokumentu o numerze przesylki rejestrowanej " + postalRegNumber);
    	}
    	
    	return doc;
    }
    
    /**
     * Find document by barcode. If not found find by cutBarCode.
     * If not found throw EdmException.
     * 
     * @author Jan �wi�cki <jan.swiecki@docusafe.pl>
     * @param barcode
     * @param cutBarCode
     * @return InOfficeDocument with PostalRegNumber = barcode or cutBarCode 
     * @throws EdmException if document was not found
     */
    public static InOfficeDocument findLatestByPostalRegNumber(String barcode, String cutBarCode)
    	throws EdmException
    {
    	LinkedList<String> barcodes = new LinkedList<String>();
    	
    	if(barcode.equals(cutBarCode))
    	{
    		barcodes.add(barcode);
    	}
    	else
    	{
    		barcodes.add(barcode);
    		barcodes.add(cutBarCode);
    	}
    	
    	return findLatestByPostalRegNumber(barcodes,0);
    }
    
    /**
     * Find document with matching PostalRegNumber from barcodes list
     * starting from the beginning of the list.
     * 
     * Throw EdmException if document was not found.
     * 
     * @author Jan �wi�cki <jan.swiecki@docusafe.pl>
     * @param barcodes
     * @param index
     * @throws HibernateException
     * @throws EdmException if document was not found
     * @return InOfficeDocument matching any PostalRegNumber from barcodes.
     */
    public static InOfficeDocument findLatestByPostalRegNumber(LinkedList<String> barcodes, Integer index)
    	throws EdmException, HibernateException
    {
    	if(index >= barcodes.size())
    	{
//    		lnd.debug("barcodes: not found");
    		throw new EdmException("Nie znaleziono dokumentu o numerze przesylki rejestrowanej " + barcodes.getLast());
    	}
    	
    	// get first barcode from barcodes
    	String barcode = barcodes.get(index); 
    	
//    	lnd.debug("barcode: "+barcode);
    	
    	Criteria crit = DSApi.context().session().createCriteria(InOfficeDocument.class);
    	crit.add(Restrictions.eq("postalRegNumber", barcode));
    	crit.addOrder(Order.desc("ctime"));
    	
    	List<InOfficeDocument> docList = crit.list();
    	
    	if(docList.size() == 0)
    	{
//    		lnd.debug("barcode not found, getting next");
    		return findLatestByPostalRegNumber(barcodes,index+1);
    	}
    	else
    	{
//    		lnd.debug("barcode found: return "+docList.get(0));
    		return docList.get(0);
    	}
    }
    
    public static List<InOfficeDocument> findByPostalRegNumber(String[] postalRegNumber) throws EdmException {
    	Criteria crit = DSApi.context().session().createCriteria(InOfficeDocument.class);
    	crit.add(Restrictions.in("postalRegNumber", postalRegNumber));
    	
    	List<InOfficeDocument> res = crit.list();
    	
    	return res;
    }
    
    public static List<InOfficeDocument> findByIds(Collection<Long> ids) throws EdmException, DocumentNotFoundException, DocumentLockedException,
   	AccessDeniedException {

   	Criteria criteria = DSApi.context().session().createCriteria(InOfficeDocument.class);
       criteria.add(org.hibernate.criterion.Expression.in("id", ids));
       
       List<InOfficeDocument> docs = criteria.list();


	    if (docs == null)
	        throw new EdmException("Documents not found");

	    return docs;
    }
    /**
     * Znajduje dokumenty o danym referenceId.
     * @param referenceId
     * @return
     * @throws EdmException
     */
    public static List<InOfficeDocument> findByReferenceId(String referenceId) throws EdmException
    {
        FromClause from = new FromClause();
        TableAlias docTable = from.createTable(DSApi.getTableName(InOfficeDocument.class), true);
        WhereClause where = new WhereClause();
        where.add(Expression.eq(Function.upper(docTable.attribute("referenceid")), referenceId.toUpperCase()));

        SelectClause selectId = new SelectClause();
        SelectColumn idCol = selectId.add(docTable, "id");
        SelectQuery selectQuery = new SelectQuery(selectId, from, where, null);

        List<InOfficeDocument> documents = new LinkedList<InOfficeDocument>();
        try
        {
            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                // TODO: co, je�eli dokument o takim referenceid istnieje, ale u�ytkownik nie ma do niego uprawnie�?
                try
                {
                    InOfficeDocument document = InOfficeDocument.findInOfficeDocument(id);
                    documents.add(document);
                }
                catch (AccessDeniedException e)
                {
                }
                catch (DocumentLockedException e)
                {
                }
                catch (DocumentNotFoundException e)
                {
                }
                // z�apane wszystko opr�cz EdmException
            }

            rs.close();

            return documents;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

    private static final lambda<InOfficeDocument, InOfficeDocument> identityMapper =
        new lambda<InOfficeDocument, InOfficeDocument>()
        {
            public InOfficeDocument act(InOfficeDocument o)
            {
                return o;
            }
        };

    public static SearchResults<InOfficeDocument> search(Query query) throws EdmException
    {

        return search(query, identityMapper, InOfficeDocument.class);
    }

    public static <T> SearchResults<T> search(Query query, lambda<InOfficeDocument, T> mapper,
                                              Class<T> mappedClass)
        throws EdmException
    {
        if (mapper == null)
            throw new NullPointerException("mapper");


        FromClause from = new FromClause();
        TableAlias docTable = from.createTable(DSApi.getTableName(InOfficeDocument.class), true);
        TableAlias documentTable = from.createTable(DSApi.getTableName(Document.class), true);
        TableAlias senderTable = null;
        if (query.sender != null)
            senderTable = from.createTable(DSApi.getTableName(Sender.class), true);
        TableAlias recipientTable = null;
        if (query.recipient != null)
            recipientTable = from.createTable(DSApi.getTableName(Recipient.class), true);
        TableAlias journalEntryTable = null;
        if (query.journalId != null)
            journalEntryTable = from.createTable(DSApi.getTableName(JournalEntry.class), true);
        TableAlias genericDocTable = null;
        if (query.source != null)
            genericDocTable = from.createTable(DSApi.getTableName(Document.class), true);
        TableAlias flagsTable = null;
        if (query.flagIds != null)
            flagsTable = from.createJoinedTable(DSApi.getTableName(DocumentToLabelBean.class), true,
                docTable, FromClause.LEFT_OUTER, "$l.id = $r.document_id");
        TableAlias userFlagsTable = null;
        if (query.userFlagIds != null)
            userFlagsTable = from.createJoinedTable(DSApi.getTableName(DocumentToLabelBean.class), true,
                docTable, FromClause.LEFT_OUTER, "$l.id = $r.document_id");

        WhereClause where = new WhereClause();
        if (query.flagIds != null && query.flagIds.length > 0)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < query.flagIds.length; i++)
            {
                Junction AND = Expression.conjunction();
                Long flag = query.flagIds[i];
                AND.add(Expression.eq(flagsTable.attribute("label_id"), flag));
                if (query.flagsDateFrom != null)
                    AND.add(Expression.ge(flagsTable.attribute("ctime"), DateUtils.midnight(query.flagsDateFrom, 0)));
                if (query.flagsDateTo != null)
                    AND.add(Expression.le(flagsTable.attribute("ctime"), DateUtils.midnight(query.flagsDateTo, 1)));
                OR.add(AND);
            }
            where.add(OR);
        }

        if (query.userFlagIds != null && query.userFlagIds.length > 0)
        {
            Junction OR = Expression.disjunction();
            for (int i=0; i < query.userFlagIds.length; i++)
            {
                Junction AND = Expression.conjunction();
                Long flag = query.userFlagIds[i];
                AND.add(Expression.eq(userFlagsTable.attribute("label_id"), flag));
                if (query.userFlagsDateFrom != null)
                    AND.add(Expression.ge(userFlagsTable.attribute("ctime"), DateUtils.midnight(query.userFlagsDateFrom, 0)));
                if (query.userFlagsDateTo != null)
                    AND.add(Expression.le(userFlagsTable.attribute("ctime"), DateUtils.midnight(query.userFlagsDateTo, 1)));
                OR.add(AND);
            }
            where.add(OR);
            if (!OR.isEmpty())
                where.add(Expression.eq(userFlagsTable.attribute("username"), DSApi.context().getPrincipalName()));
        }
        
        if (query.source != null)
        {
            where.add(Expression.eqAttribute(
                docTable.attribute("id"),
                genericDocTable.attribute("id")));
            where.add(Expression.eq(genericDocTable.attribute("source"), query.source));
        }

        TableAlias changelogTable = null;
        if (query.accessedBy != null || (query.accessedAs != null && query.accessedAs.length > 0))
        {
            changelogTable = from.createTable("ds_document_changelog", true);
            where.add(Expression.eqAttribute(docTable.attribute("id"),
                changelogTable.attribute("document_id")));

            if (query.accessedBy != null)
                where.add(Expression.eq(changelogTable.attribute("username"), query.accessedBy));

            if (query.accessedAs != null && query.accessedAs.length > 0)
            {
                Junction OR = Expression.disjunction();
                for (int i=0; i < query.accessedAs.length; i++)
                {
                    OR.add(Expression.eq(changelogTable.attribute("what"), query.accessedAs[i]));
                }
                where.add(OR);
            }

            if (query.accessedFrom != null)
                where.add(Expression.ge(changelogTable.attribute("ctime"), query.accessedFrom));
            if (query.accessedTo != null)
                where.add(Expression.le(changelogTable.attribute("ctime"), query.accessedTo));
        }
        else if(query.accessedFrom!=null || query.accessedTo!=null){
        	changelogTable = from.createTable("ds_document_changelog", true);
        	 where.add(Expression.eqAttribute(docTable.attribute("id"),
                     changelogTable.attribute("document_id")));
        	if (query.accessedFrom != null)
                where.add(Expression.ge(changelogTable.attribute("ctime"), query.accessedFrom));
            if (query.accessedTo != null)
                where.add(Expression.le(changelogTable.attribute("ctime"), query.accessedTo));
        }

        if (!DSApi.context().isAdmin() &&
            !(DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD) ||
              DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE) ||
              DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)))
        {

            DSUser user = DSApi.context().getDSUser();

            Junction OR = Expression.disjunction();

            if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD) ||
                DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_MODYFIKACJE))
            {
                DSDivision[] divisions = user.getDivisions();
                if (divisions.length > 0)
                {

                    for (int i=0; i < divisions.length; i++)
                    {
                        OR.add(Expression.eq(docTable.attribute("divisionguid"), divisions[i].getGuid()));
                        if (divisions[i].isPosition())
                        {
                            DSDivision parent = divisions[i].getParent();
                            if (parent != null)
                                OR.add(Expression.eq(docTable.attribute("divisionguid"), parent.getGuid()));
                        }

                        DSDivision[] children_POSITION = divisions[i].getChildren(DSDivision.TYPE_POSITION);
                        if(children_POSITION.length >0)
                        {
                            for (int j=0; j < children_POSITION.length; j++)
                            {
                                OR.add(Expression.eq(docTable.attribute("divisionguid"), children_POSITION[j].getGuid()));
                            }
                        }
                    }
                    OR.add(Expression.eq(docTable.attribute("clerk"), user.getName()));
                }
            }

            if (DSApi.context().hasPermission(DSPermission.PISMO_KO_PODGLAD))
            {
                OR.add(Expression.eq(docTable.attribute("divisionguid"), DSDivision.ROOT_GUID));
            }

            OR.add(Expression.eq(docTable.attribute("clerk"), user.getName()));

            where.add(OR);
        }

        if (query.originalDocument != null)
        {
            where.add(Expression.eq(docTable.attribute("original"), query.originalDocument));
        }

        if (query.clerk != null)
        {
            // alternatywa do ogranicze� na zwracane pisma, aby nie pojawi�o si�
            // wyra�enie clerk='A' and clerk='B'.
            where.add(Expression.eq(docTable.attribute("clerk"), query.clerk));
        }

        if (query.stampDateFrom != null)
        {
//            where.add(Expression.ge(Function.cast(docTable.attribute("stampdate"), Types.DATE),
//                query.stampDateFrom));
            where.add(Expression.ge(docTable.attribute("stampdate"),
                DateUtils.midnight(query.stampDateFrom, 0)));
        }

        if (query.stampDateTo != null)
        {
//            where.add(Expression.le(Function.cast(docTable.attribute("stampdate"), Types.DATE),
//                query.stampDateFrom));
            where.add(Expression.lt(docTable.attribute("stampdate"),
                DateUtils.midnight(query.stampDateTo, 1)));
        }

        if (query.incomingDateFrom != null)
        {
            where.add(Expression.ge(docTable.attribute("incomingdate"),
                DateUtils.midnight(query.incomingDateFrom, 0)));
        }

        if (query.incomingDateTo != null)
        {
            where.add(Expression.lt(docTable.attribute("incomingdate"),
                DateUtils.midnight(query.incomingDateTo, 1)));
        }

        if (query.documentDateFrom != null)
        {
            where.add(Expression.ge(docTable.attribute("documentdate"),
                DateUtils.midnight(query.documentDateFrom, 0)));
        }
        if (query.documentDateTo != null)
        {
            where.add(Expression.lt(docTable.attribute("documentdate"),
                DateUtils.midnight(query.documentDateTo, 1)));
        }

        if (query.creatingUser != null)
        {
            where.add(Expression.eq(docTable.attribute("creatinguser"), query.creatingUser));
        }

        if(StringUtils.isNotEmpty(query.caseDocumentId))
        {
            String string = (query.caseDocumentId + "%");
            where.add(Expression.like(docTable.attribute("CASEDOCUMENTID"), "%"+string));
        }

        if (query.clerk != null)
        {
            where.add(Expression.eq(docTable.attribute("clerk"), query.clerk));
        }

        if (query.master_document_id != null)
        {
            where.add(Expression.eq(docTable.attribute("master_document_id"), query.master_document_id));
        }

        if (query.kindId != null)
        {
            where.add(Expression.eq(docTable.attribute("kind"), query.kindId));
        }

        if (query.deliveryId != null)
        {
            where.add(Expression.eq(docTable.attribute("delivery"), query.deliveryId));
        }

        if (query.statusId != null)
        {
            where.add(Expression.eq(docTable.attribute("status"), query.statusId));
        }
        if (query.invoicekindId != null)
        {
            where.add(Expression.eq(docTable.attribute("invoiceKind"), query.invoicekindId));
        }
        if (query.contractId != null)
        {
            where.add(Expression.eq(docTable.attribute("contractId"), query.contractId));
        }

        if (query.sender != null)
        {
            where.add(Expression.eqAttribute(
                docTable.attribute("sender"), senderTable.attribute("id")));
            where.add(Expression.eq(senderTable.attribute("discriminator"), "SENDER"));

            String[] tokens = query.sender.toUpperCase().split("[\\s,.;:]+");

            if (tokens.length > 0)
            {
                Junction AND = Expression.conjunction();

                for (int i=0; i < tokens.length; i++)
                {
                    String token = tokens[i];

                    Junction OR = Expression.disjunction();

                    OR.add(Expression.like(Function.upper(senderTable.attribute("firstname")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("lastname")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("organization")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("organizationdivision")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("street")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("location")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(senderTable.attribute("zip")),
                        "%"+StringUtils.substring(token, 0, 12)+"%"));

                    AND.add(OR);
                }

                where.add(AND);
            }

/*
            OR.add(Expression.like(Function.upper(senderTable.attribute("firstname")),
                StringUtils.substring(query.sender.toUpperCase(), 0, 49)+"%"));
            OR.add(Expression.like(Function.upper(senderTable.attribute("lastname")),
                StringUtils.substring(query.sender.toUpperCase(), 0, 49)+"%"));
            OR.add(Expression.like(Function.upper(senderTable.attribute("organization")),
                StringUtils.substring(query.sender.toUpperCase(), 0, 49)+"%"));
            OR.add(Expression.like(Function.upper(senderTable.attribute("organizationdivision")),
                StringUtils.substring(query.sender.toUpperCase(), 0, 49)+"%"));
            OR.add(Expression.like(Function.upper(senderTable.attribute("street")),
                StringUtils.substring(query.sender.toUpperCase(), 0, 49)+"%"));
            OR.add(Expression.like(Function.upper(senderTable.attribute("location")),
                StringUtils.substring(query.sender.toUpperCase(), 0, 49)+"%"));
            OR.add(Expression.like(Function.upper(senderTable.attribute("zip")),
                StringUtils.substring(query.sender.toUpperCase(), 0, 13)+"%"));
*/
        }

        if (query.recipient != null)
        {
            where.add(Expression.eqAttribute(
                docTable.attribute("id"), recipientTable.attribute("document_id")));
            where.add(Expression.eq(recipientTable.attribute("discriminator"), "RECIPIENT"));

            String[] tokens = query.recipient.toUpperCase().split("[\\s,.;:]+");

            if (tokens.length > 0)
            {
                Junction AND = Expression.conjunction();

                for (int i=0; i < tokens.length; i++)
                {
                    String token = tokens[i];

                    Junction OR = Expression.disjunction();
 
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("firstname")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("lastname")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("organization")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("organizationdivision")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("street")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("location")),
                        "%"+StringUtils.substring(token, 0, 48)+"%"));
                    OR.add(Expression.like(Function.upper(recipientTable.attribute("zip")),
                        "%"+StringUtils.substring(token, 0, 12)+"%"));

                    AND.add(OR);
                }

                where.add(AND);
            }
        }

        if (query.journalId != null)
        {
            where.add(Expression.eqAttribute(
                docTable.attribute("id"),
                journalEntryTable.attribute("documentid")));
            where.add(Expression.eq(journalEntryTable.attribute("journal_id"), query.journalId));
        }

        /*
        join dso_person on document_id = id and discriminator = 'RECIPIENT'
        and firstname = '�ukasz%'
        */

        if (query.barcode != null)
        {
            where.add(Expression.eq(docTable.attribute("barcode"), query.barcode));
        }

        if (query.referenceId != null)
        {
            where.add(Expression.eq(Function.upper(docTable.attribute("referenceid")), query.referenceId.toUpperCase()));
        }

        if (query.postalRegNumber != null)
        {
            where.add(Expression.eq(Function.upper(docTable.attribute("postalregnumber")), query.postalRegNumber.toUpperCase()));
        }

        if (query.summary != null)
        {
            where.add(Expression.like(Function.upper(docTable.attribute("summary")), StringUtils.substring(query.summary.toUpperCase(), 0, 79)+"%"));
        }

        if (query.officeNumberYear != null)
        {
            where.add(Expression.eq(docTable.attribute("officenumberyear"), query.officeNumberYear));
        }

        if (query.officeNumber != null)
        {
            where.add(Expression.eq(docTable.attribute("officenumber"), query.officeNumber));
        }

        /* Zabezpieczenie przed wyszukiwaniem pustych pism. Kiedys pojawil sie blad podczas przyjmowania pism. */
        where.add(Expression.ne(docTable.attribute("officenumber"), null));


        if (query.fromOfficeNumber != null && query.toOfficeNumber != null)
        {
            where.add(Expression.ge(docTable.attribute("officenumber"), query.fromOfficeNumber));
            where.add(Expression.le(docTable.attribute("officenumber"), query.toOfficeNumber));
        }
        
        if (AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu"))
        {
        	where.add(Expression.eqAttribute(docTable.attribute("id"), documentTable.attribute("id")));
        	where.add(Expression.eq(documentTable.attribute("czy_aktualny"), Boolean.TRUE));
        }

        // kolumny, po kt�rych wykonywane jest sortowanie musz� si�
        // znale�� w SELECT
        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(docTable, "id");

        OrderClause order = new OrderClause();

        for (Iterator iter=query.sortFields().iterator(); iter.hasNext(); )
        {
            AbstractQuery.SortField field = (AbstractQuery.SortField) iter.next();
            // je�eli dodana b�dzie mo�liwo�� sortowania po innych polach,
            // nale�y uaktualni� metod� Query.validSortField()
            if (field.getName().equals("officeNumber"))
            {
                order.add(docTable.attribute("officenumber"), field.getDirection());
                selectId.add(docTable, "officenumber");
            }
            else if (field.getName().equals("incomingDate"))
            {
                order.add(docTable.attribute("incomingdate"), field.getDirection());
                selectId.add(docTable, "incomingdate");
            }
            else if (field.getName().equals("referenceId"))
            {
                order.add(docTable.attribute("referenceId"), field.getDirection());
                selectId.add(docTable, "referenceid");
            }
        }

        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(distinct "+docTable.getAlias()+".id)");

        SelectQuery selectQuery;
        ResultSet rs = null;
        int totalCount =-1;
        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            boolean unable = false;
            if (rs.next())
            {
            	totalCount = rs.getInt(countCol.getPosition());
            }
            else
            {
            	unable = true;
            }
            rs.close();
            rs = null;
            if (unable)
            {
                throw new EdmException("Nie mo�na pobra� liczby dokument�w.");
            }
            selectQuery = new SelectQuery(selectId, from, where, order);

            selectQuery.setMaxResults(query.limit);
            selectQuery.setFirstResult(query.offset);


            rs = selectQuery.resultSet(DSApi.context().session().connection());

            List<T> results = new ArrayList<T>(query.limit);

            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                InOfficeDocument doc = (InOfficeDocument)
                    DSApi.context().session().load(InOfficeDocument.class, id);
                results.add(mapper.act(doc));
            }

            rs.close();
            rs = null;
            return new SearchResultsAdapter<T>(results, query.offset, totalCount, mappedClass);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	if (rs != null)
        	{
        		try
        		{
        			rs.close();
        		}
        		catch (Exception e1) { }
        	}
        }
    }//search method

    public static Query query(int offset, int limit)
    {
        return new Query(offset, limit);
    }

    public static class Query extends AbstractQuery
    {
        private int offset;
        private int limit;
        private Date stampDateFrom;
        private Date stampDateTo;
        private Date incomingDateFrom;
        private Date incomingDateTo;
        private Date documentDateFrom;
        private Date documentDateTo;
        private String sender;
        private String recipient;
        private Integer kindId;
        private String creatingUser;
        private String clerk;
        private Integer officeNumber;
        private Integer officeNumberYear;
        private String barcode;
        private String referenceId;
        private Integer deliveryId;
        private String postalRegNumber;
        private String summary;
        private Integer fromOfficeNumber;
        private Integer toOfficeNumber;
        private Long journalId;
        private Integer statusId;
        private String accessedBy;
        private String[] accessedAs;
        private Date accessedFrom;
        private Date accessedTo;
        private String source;
        private Long[] flagIds;
        private Long[] userFlagIds;
        private Date flagsDateFrom;
        private Date flagsDateTo;
        private Date userFlagsDateFrom;
        private Date userFlagsDateTo;
        private String caseDocumentId;
        private Long master_document_id;
        private Boolean originalDocument;
        private Integer invoicekindId;
        private Long contractId;
        private boolean czyAktualny;
        
        public String toString()
        {
        	return "offset"+offset+"\n"+
        	"limit"+limit+"\n"+
        	"stampDateFrom"+stampDateFrom+"\n"+
        	"stampDateTo"+stampDateTo+"\n"+
        	"incomingDateFrom"+incomingDateFrom+"\n"+
        	"incomingDateTo"+incomingDateTo+"\n"+
        	"documentDateFrom"+documentDateFrom+"\n"+
        	"documentDateTo"+documentDateTo+"\n"+
        	"sender"+sender+"\n"+
        	"recipient"+recipient+"\n"+
        	"kindId"+kindId+"\n"+
        	"creatingUser"+creatingUser+"\n"+
        	"clerk"+clerk+"\n"+
        	"officeNumber"+officeNumber+"\n"+
        	"officeNumberYear"+officeNumberYear+"\n"+
        	"barcode"+barcode+"\n"+
        	"referenceId"+referenceId+"\n"+
        	"deliveryId"+deliveryId+"\n"+
        	"postalRegNumber"+postalRegNumber+"\n"+
        	"summary"+summary+"\n"+
        	"fromOfficeNumber"+fromOfficeNumber+"\n"+
        	"toOfficeNumber"+toOfficeNumber+"\n"+
        	"journalId"+journalId+"\n"+
        	"statusId"+statusId+"\n"+
        	"accessedBy"+accessedBy+"\n"+
        	"ccessedAs"+accessedAs+"\n"+
        	"accessedFrom"+accessedFrom+"\n"+
        	"accessedTo"+accessedTo+"\n"+
        	"source"+source+"\n"+
        	"flagIds"+flagIds+"\n"+
        	"userFlagIds"+userFlagIds+"\n"+
        	"flagsDateFrom"+flagsDateFrom+"\n"+
        	"flagsDateTo"+flagsDateTo+"\n"+
        	"userFlagsDateFrom"+userFlagsDateFrom+"\n"+
        	"userFlagsDateTo"+userFlagsDateTo+"\n"+
        	"caseDocumentId"+caseDocumentId+"\n"+
        	"master_document_id"+master_document_id+"\n"+
        	"originalDocument"+originalDocument+"\n"+
        	"invoicekindId"+invoicekindId;
        }

        public Long getMaster_document_id() {
			return master_document_id;
		}

		public void setMaster_document_id(Long master_document_id) {
			this.master_document_id = master_document_id;
		}

		public void setCaseDocumentId(String caseDocumentId)
        {
            this.caseDocumentId = caseDocumentId;
        }

        private Query(int offset, int limit)
        {
            this.offset = offset;
            this.limit = limit;
        }

        protected boolean validSortField(String field)
        {
            return "officeNumber".equals(field) ||
                "incomingDate".equals(field) ||
                "referenceId".equals(field);
        }

        public void setFlags(List<Long> flags, Date from, Date to)
        {
        	for(Long l:flags)
        	flagIds = flags.toArray(new Long[flags.size()]);
            flagsDateFrom = from;
            flagsDateTo = to;
        }

        public void setUserFlags(List<Long> flags, Date from, Date to)
        {
            userFlagIds = flags.toArray(new Long[flags.size()]);
            userFlagsDateFrom = from;
            userFlagsDateTo = to;
        }

        public void setSource(String source)
        {
            this.source = source;
        }

        public void setAccessedBy(String accessedBy)
        {
            this.accessedBy = accessedBy;
        }

        public void setAccessedAs(String[] accessedAs)
        {
            this.accessedAs = accessedAs;
        }

        public void setAccessedFrom(Date accessedFrom)
        {
            this.accessedFrom = accessedFrom;
        }

        public void setAccessedTo(Date accessedTo)
        {
            this.accessedTo = accessedTo;
        }

        public void setStatusId(Integer statusId)
        {
            this.statusId = statusId;
        }

        public void setReferenceId(String referenceId)
        {
            this.referenceId = referenceId;
        }

        public void setStampDateFrom(Date stampDateFrom)
        {
            this.stampDateFrom = stampDateFrom;
        }

        public void setStampDateTo(Date stampDateTo)
        {
            this.stampDateTo = stampDateTo;
        }

        public void setIncomingDateFrom(Date incomingDateFrom)
        {
            this.incomingDateFrom = incomingDateFrom;
        }

        public void setIncomingDateTo(Date incomingDateTo)
        {
            this.incomingDateTo = incomingDateTo;
        }

        public void setDocumentDateFrom(Date documentDateFrom)
        {


            this.documentDateFrom = documentDateFrom;
        }

        public void setDocumentDateTo(Date documentDateTo)
        {
            this.documentDateTo = documentDateTo;
        }

        public void setSender(String sender)
        {
            this.sender = sender;
        }

        public void setRecipient(String recipient)
        {
            this.recipient = recipient;
        }

        public void setKindId(Integer kindId)
        {
            this.kindId = kindId;
        }

        public void setCreatingUser(String creatingUser)
        {
            this.creatingUser = creatingUser;
        }

        public void setClerk(String clerk)
        {
            this.clerk = clerk;
        }

        public void setOfficeNumber(Integer officeNumber)
        {
            this.officeNumber = officeNumber;
        }

        public void setOfficeNumberYear(Integer officeNumberYear)
        {
            this.officeNumberYear = officeNumberYear;
        }

        public void setBarcode(String barcode)
        {
            this.barcode = barcode;
        }

        public void setDeliveryId(Integer deliveryId)
        {
            this.deliveryId = deliveryId;
        }

        public void setPostalRegNumber(String postalRegNumber)
        {
            this.postalRegNumber = postalRegNumber;
        }

        public void setSummary(String summary)
        {
            this.summary = summary;
        }

        public void setFromOfficeNumber(Integer fromOfficeNumber)
        {
            this.fromOfficeNumber = fromOfficeNumber;
        }

        public void setToOfficeNumber(Integer toOfficeNumber)
        {
            this.toOfficeNumber = toOfficeNumber;
        }

        public void setJournalId(Long journalId)
        {
            this.journalId = journalId;
        }

		public void setOffset(int offset) {
			this.offset = offset;
		}

		public boolean isOriginalDocument() {
			return originalDocument;
		}

		public void setOriginalDocument(boolean originalDocument) {
			this.originalDocument = originalDocument;
		}
		
		public boolean getCzyAktualny() {
			return czyAktualny;
		}
		
		public void setCzyAktualny(boolean czyAktualny) {
			this.czyAktualny=czyAktualny;
		}
    }//class Query

    /* Lifecycle */

    public boolean onSave(Session session) throws CallbackException
    {
        if (this.trackingNumber == null)
        {
            this.trackingNumber = IdUtils.getTrackingNumber();
        }

        if (this.trackingPassword == null)
        {
            this.trackingPassword = IdUtils.getTrackingPassword();
        }

        return super.onSave(session);
    }

    public void onLoad(Session s, Serializable id)
    {
    //	log.trace("INOfficeDocument onLoad in");
//    	try
//    	{
//        if (getOutDocuments() != null && getOutDocuments().size() > 0)
//	        {
//	            for (Iterator iter=getOutDocuments().iterator(); iter.hasNext(); )
//	            {
//	                ((OutOfficeDocument) iter.next()).addPropertyChangeListener(this);
//	            }
//	        }
//    	}
//    	catch (Exception e)
//    	{
//			log.error("",e);
//		}
        super.onLoad(s, id);
  //      log.trace("INOfficeDocument onLoad out");
    }

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }

    public Date getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(Date documentDate) throws EdmException {
		if (getId() != null) {
			if (this.documentDate != null && documentDate != null
					&& !this.documentDate.equals(documentDate)) {
				addWorkHistoryEntry(Audit.create("documentDate",DSApi.context().getPrincipalName(),
							sm.getString("ZmianaDatyDokumentuNa",DateUtils.formatCommonDate(documentDate)),
								DateUtils.formatPortableDateTime(documentDate)));
			} else if(this.documentDate == null && documentDate != null)
			{
				addWorkHistoryEntry(Audit.create("documentDate",DSApi.context().getPrincipalName(),
							sm.getString("NadanieDatyDokumentu",DateUtils.formatCommonDate(documentDate)),
						DateUtils.formatPortableDateTime(documentDate)));
			} else if(this.documentDate != null && documentDate == null){
				addWorkHistoryEntry(Audit.create("documentDate",DSApi.context().getPrincipalName(),
						sm.getString("UsuniecieDatyDokumentu")));
			}
		}

		this.documentDate = documentDate;
	}

    public Date getStampDate()
    {
        return stampDate;
    }

    public void setStampDate(Date stampDate) throws EdmException {
		if (getId() != null) {
			if (this.stampDate != null && stampDate != null
					&& !this.stampDate.equals(stampDate)) {
				addWorkHistoryEntry(
						Audit.create("stampDate", DSApi.context()
								.getPrincipalName(), sm.getString(
								"ZmianaDatyStemplaNa", DateUtils
										.formatCommonDate(stampDate)),
								DateUtils.formatPortableDateTime(stampDate)));
			} else if (this.stampDate == null && stampDate != null) {
				addWorkHistoryEntry(
						Audit.create("stampDate", DSApi.context()
								.getPrincipalName(), sm.getString(
								"NadanieDatyStempla", DateUtils
										.formatCommonDate(stampDate)),
								DateUtils.formatPortableDateTime(stampDate)));
			} else if (this.stampDate != null && stampDate == null){
				addWorkHistoryEntry(
						Audit.create("stampDate", DSApi.context()
								.getPrincipalName(), sm.getString(
								"UsuniecieDatyStempla")));
			}
		}

		this.stampDate = stampDate;
	}

    public Date getIncomingDate()
    {
        return incomingDate;
    }

    public void setIncomingDate(Date incomingDate) throws EdmException
    {
        if (getId() != null && this.incomingDate != null && incomingDate != null &&
            !this.incomingDate.equals(incomingDate))
        {
        	addWorkHistoryEntry(Audit.create("incomingDate",
                    DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaDatyPrzyjeciaNa",DateUtils.formatCommonDate(incomingDate)),
                    DateUtils.formatPortableDateTime(incomingDate)));
        }
        this.incomingDate = incomingDate;
    }

    /**
     * Zwraca numer kancelaryjny pisma taki, jaki zwraca metoda
     * {@link OfficeDocument#getFormattedOfficeNumber()} dodatkowo
     * ujmuj�c go w kwadratowe nawiasy, je�eli bie��cy dokument jest
     * klonem innego dokumentu.
     */
    public String getFormattedOfficeNumber()
    {
        String superFormattedNumber = super.getFormattedOfficeNumber();

        if (superFormattedNumber == null)
            return null;

        if (masterDocument != null)
        {
            return "[" + superFormattedNumber + "]";
        }
        else
        {
            return superFormattedNumber;
        }
    }

    public String getPostalRegNumber()
    {
        return postalRegNumber;
    }

    public void setPostalRegNumber(String postalRegNumber) {

    	if (getId() != null) {

			if (this.postalRegNumber != null && postalRegNumber != null
					&& !this.postalRegNumber.equals(postalRegNumber)) {
				addWorkHistoryEntry(
						Audit.create("postalRegNumber", DSApi.context()
								.getPrincipalName(), sm.getString(
								"ZmianaNumeruPrzesylkiRejestrowanej",
								this.postalRegNumber, postalRegNumber)));
			} else if(this.postalRegNumber == null && postalRegNumber != null){
				addWorkHistoryEntry(
						Audit.create("postalRegNumber", DSApi.context()
								.getPrincipalName(), sm.getString(
								"NadanoNumerPrzesylkiRejestrowanej",
								postalRegNumber)));
			} else if(this.postalRegNumber != null && postalRegNumber == null){
				addWorkHistoryEntry(
						Audit.create("postalRegNumber", DSApi.context()
								.getPrincipalName(), sm.getString(
								"UsunietoNumerPrzesylkiRejestrowanej")));
			}
		}
		this.postalRegNumber = postalRegNumber;
	}

    public boolean isSenderAnonymous()
    {
        return senderAnonymous;
    }

    public void setSenderAnonymous(boolean senderAnonymous)
    {
        this.senderAnonymous = senderAnonymous;
    }

    public String getPackageRemarks()
    {
        return packageRemarks;
    }

    public void setPackageRemarks(String packageRemarks)
    {
        this.packageRemarks = packageRemarks;
    }

    public String getOtherRemarks()
    {
        return otherRemarks;
    }

    public void setOtherRemarks(String otherRemarks)
    {
        this.otherRemarks = otherRemarks;
    }

    public int getNumAttachments()
    {
        return numAttachments;
    }

    public void setNumAttachments(int numAttachments) throws EdmException
    {
        if (getId() != null && numAttachments != this.numAttachments)
        {
        	addWorkHistoryEntry(Audit.create(
                "numAttachments", DSApi.context().getPrincipalName(),
                sm.getString("ZmienionoLiczbeZalacznikow",new Long(numAttachments)), null, new Long(numAttachments)));
        }

        this.numAttachments = numAttachments;
    }

    public String getTrackingNumber()
    {
        return trackingNumber;
    }

    private void setTrackingNumber(String trackingNumber)
    {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingPassword()
    {
        return trackingPassword;
    }

    private void setTrackingPassword(String trackingPassword)
    {
        this.trackingPassword = trackingPassword;
    }

    public boolean isSubmitToBip()
    {
        return submitToBip;
    }

    public void setSubmitToBip(boolean submitToBip)
    {
        this.submitToBip = submitToBip;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location) throws EdmException
    {
        location = TextUtils.nullSafeString(location).trim();

        if (getId() != null &&
            this.location != null && location != null &&
            !this.location.equals(location))
        {
        	addWorkHistoryEntry(
                Audit.create("location",
                    DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaPolozeniaDokumentuNa",location),
                    location));
        }

        this.location = location;
    }

    public InOfficeDocumentKind getKind()
    {
        return kind;
    }

    public void setKind(InOfficeDocumentKind kind)
    {
        this.kind = kind;
    }

    public InOfficeDocumentStatus getStatus()
    {
        return status;
    }

    public void setStatus(InOfficeDocumentStatus status) throws EdmException
    {
        if (status == null)
            throw new NullPointerException("status");

//        if (getId() != null &&
//            this.status != null && !this.status.equals(status))
        if (this.status == null || !this.status.equals(status))
        {
        	addWorkHistoryEntry(Audit.create("status",
                DSApi.context().getPrincipalName(),
                sm.getString("ZmianaStatusuDokumentuNa",status.getName()),
                status.getName(), new Long(status.getId())));
        }

        this.status = status;
    }

    /**
     * Zwraca list� odbiorc�w dokumentu. Zwracana lista mo�e by� pusta,
     * ale nie mo�e by� r�wna null.
     * @throws EdmException
     */
    public List<Recipient> getRecipients() throws EdmException
    {
        if (recipients == null && this.getId() != null)
        {
    		Criteria c = DSApi.context().session().createCriteria(Recipient.class);
            c.add(Restrictions.eq("documentId", this.getId()));
            recipients = new ArrayList<Recipient>(c.list());
        }
        if(recipients == null)
        	recipients = new ArrayList<Recipient>();
        return recipients;
    }

    public void addRecipient(Recipient recipient) throws EdmException
    {
    	getRecipients().add(recipient);
    	recipient.setDocument(this);
    }

    public Set<OutOfficeDocument> getOutDocuments() throws EdmException
    {
        if (outDocuments == null && this.getId() != null)
        {
    		Criteria c = DSApi.context().session().createCriteria(OutOfficeDocument.class);
            c.add(Restrictions.eq("inDocumentId", this.getId()));
            c.add(Restrictions.eq("cancelled", false));
            outDocuments = new HashSet<OutOfficeDocument>(c.list());
            outDocumentsAsList  = new ArrayList<OutOfficeDocument>(c.list());
        }
        return outDocuments;
    }
    
    public OutOfficeDocument getLastOutDocuments() throws EdmException
    {
    	getOutDocuments();
    	return outDocumentsAsList.get(outDocumentsAsList.size()-1);
    }

    public void addOutDocument(OutOfficeDocument outDocuments)
    {
    	outDocuments.setInDocumentId(this.getId());
    }

    public String getReferenceId()
    {
        return referenceId;
    }

    public void setReferenceId(String referenceId) throws EdmException
    {
        if (getId() != null &&
            this.referenceId != null && !this.referenceId.equals(referenceId))
        {
        	addWorkHistoryEntry(Audit.create("referenceId",
                DSApi.context().getPrincipalName(),
                sm.getString("ZmianaZnakuPismaNa",referenceId),
                referenceId));
        }

        this.referenceId = referenceId;
    }

    public InOfficeDocument getMasterDocument() throws EdmException
    {
    	if (getMasterDocumentId() == null)
    		return masterDocument;
        if (masterDocument == null || !masterDocument.getId().equals(getMasterDocumentId()))
        	masterDocument = Finder.find(InOfficeDocument.class, getMasterDocumentId());
        return masterDocument;
    }

    public void setMasterDocument(InOfficeDocument masterDocument)
    {
        this.masterDocument = masterDocument;
        super.setMasterDocumentId(masterDocument.getId());
    }

    public InOfficeDocumentDelivery getDelivery()
    {
        return delivery;
    }

    public void setDelivery(InOfficeDocumentDelivery delivery)
        throws EdmException
    {
        if (delivery == null)
            throw new NullPointerException("delivery");

        if (getId() != null &&
            this.delivery != null && !this.delivery.equals(delivery))
        	addWorkHistoryEntry(Audit.create(
                "delivery", DSApi.context().getPrincipalName(),
                sm.getString("ZmianaSposobuDostarczeniaPismaNa",delivery.getName()),
                delivery.getName(), new Long(delivery.getId())));

        this.delivery = delivery;
    }

    public OutOfficeDocumentDelivery getOutgoingDelivery()
    {
        return outgoingDelivery;
    }

    public void setOutgoingDelivery(OutOfficeDocumentDelivery outgoingDelivery)
        throws EdmException
    {
        if (outgoingDelivery == null)
            throw new NullPointerException("outgoingDelivery");

        if (getId() != null &&
            this.outgoingDelivery != null && !this.outgoingDelivery.equals(outgoingDelivery))
        	addWorkHistoryEntry(Audit.create(
                "outgoingDelivery", DSApi.context().getPrincipalName(),
                sm.getString("ZmianaSposobuOdbioruPismaNa",outgoingDelivery.getName()),
                outgoingDelivery.getName(), new Long(outgoingDelivery.getId())));

        this.outgoingDelivery = outgoingDelivery;
    }

    public boolean isBok()
    {
        return bok;
    }

    public void setBok(boolean bok)
    {
        this.bok = bok;
    }

    public boolean isReplyUnnecessary()
    {
        return replyUnnecessary;
    }

    public void setReplyUnnecessary(boolean replyUnnecessary)
    {
        if(this.replyUnnecessary == replyUnnecessary)
            return;
        else
        {
            String tmp = null;
            if (replyUnnecessary)
                tmp = sm.getString("PismoNieWymagaOdpowiedziZaznaczenie");
            else
                tmp = sm.getString("PismoWymagaOdpowiedziZaznaczenie");
            addWorkHistoryEntry(Audit.create(
                "replyUnnecessary", DSApi.context().getPrincipalName(),
                tmp));
        }
        this.replyUnnecessary = replyUnnecessary;
    }

	public Boolean getOriginal() {
		return original;
	}

	public void setOriginal(Boolean original) {
		this.original = original;
	}
	
	@Override
	public void removeRecipient(Recipient recipient) throws EdmException
	{
		if(recipients.contains(recipient))
			recipients.remove(recipient);
		DSApi.context().session().delete(recipient);
		
	}

	public String getRecipientUser() {
		return recipientUser;
	}

	public void setRecipientUser(String recipientUser) {
		this.recipientUser = recipientUser;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public InvoicesKind getInvoiceKind()
	{
		return invoiceKind;
	}

	public void setInvoiceKind(InvoicesKind kind)
	{
		this.invoiceKind = kind;
	}
	public Long getContractId()
	{
		return contractId;
	}

	
	public void setContractId(Long contractId)
	{
		this.contractId = contractId;
	}
}
