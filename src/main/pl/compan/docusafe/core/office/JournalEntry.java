package pl.compan.docusafe.core.office;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;

/**
 * Pozycja dziennika.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: JournalEntry.java,v 1.7 2009/10/23 12:39:01 mariuszk Exp $
 */
public class JournalEntry implements Serializable
{
    /**
     * Kolejny numer nadawany pismu w dzienniku.
     */
    private Integer sequenceId;
    private Integer dailySequenceId;
    /**
     * Identyfikator dokumentu, kt�ry figuruje pod tym numerem.
     */
    private Long documentId;
    /**
     * Czas utworzenia pozycji dziennika.
     */
    private Date ctime;
    /**
     * Dziennik, z kt�rym zwi�zana jest pozycja.
     */
    private Journal journal;
    /**
     * Data, jakiej dotyczy wpis w dzienniku.
     */
    private Date entryDate;
    
    /**
     * Uzytkonwik wpisuj�cyu do dziennika
     */
    private String author;

    /**
     * Zwraca true, je�eli w jakimkolwiek dzienniku istniej� wpisy
     * z dat� nowsz� ni� ��dana.
     * @param threshold
     * @return
     * @throws EdmException
     */
    public static boolean newerEntriesExist(Date threshold) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().
                prepareStatement("select count(*) from dso_journal_entry "+
                " where entrydate > ?");
            ps.setDate(1, new java.sql.Date(threshold.getTime()));

            ResultSet rs = ps.executeQuery();
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� wyniku zapytania");

            return rs.getInt(1) > 0;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
        }
    }
    
    
    /**
     * Znajduje wszystkie dzienniki (r�wnie� zamkni�te), w ktorych
     * znajduje si� pismo.
     * @param id
     * @return
     * @throws EdmException
     */
    public static List<JournalEntry> findByDocumentId(Long id) throws EdmException
    {
        if (id == null)
            throw new NullPointerException("id");

        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(JournalEntry.class);
            criteria.add(Restrictions.eq("documentId", id));
			List<JournalEntry> entries = (List<JournalEntry>)criteria.list();
            return entries;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "id="+id);
        }
    }
    
    /**
     * Znajduje wszystkie wpisy dla danego dziennika
     * @param journal
     * @return
     * @throws EdmException
     */
    public static List<JournalEntry> findByJournal(Journal journal) throws EdmException
    {
        if (journal == null)
            throw new NullPointerException("journal");

        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(JournalEntry.class);
            criteria.add(Restrictions.eq("journal", journal));
			List<JournalEntry> entries = (List<JournalEntry>)criteria.list();
            return entries;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "journal="+journal);
        }
    }
    
    public Date getEntryDate()
    {
        return entryDate;
    }

    public void setEntryDate(Date entryDate)
    {
        this.entryDate = entryDate;
    }

    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public Journal getJournal()
    {
        return journal;
    }

    public void setJournal(Journal journal)
    {
        this.journal = journal;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof JournalEntry)) return false;

        final JournalEntry journalEntry = (JournalEntry) o;

        if (journal != null ? !journal.equals(journalEntry.journal) : journalEntry.journal != null) return false;
        if (sequenceId != null ? !sequenceId.equals(journalEntry.sequenceId) : journalEntry.sequenceId != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (sequenceId != null ? sequenceId.hashCode() : 0);
        result = 29 * result + (journal != null ? journal.hashCode() : 0);
        return result;
    }

	public Integer getDailySequenceId() {
		return dailySequenceId;
	}

	public void setDailySequenceId(Integer dailySequenceId) {
		this.dailySequenceId = dailySequenceId;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
}
