package pl.compan.docusafe.core.office;

import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Recipient.java,v 1.5 2009/09/22 14:57:52 kubaw Exp $
 */
public class Recipient extends Person
{
    public Recipient fromJSON(String json) throws EdmException
    {
        return (Recipient) readJSON(json);
    }

    public Recipient recipientFromMap(Map map) throws EdmException
    {
    	map.put("dictionaryType",Person.DICTIONARY_RECIPIENT);
    	return (Recipient) fromMap(map);
    }

    
    @Override
    public boolean equals(Object r)
    {
    	if(r==null) 
    		return false;
    	Recipient rec = (Recipient)r;
    	if(this.getId()==null || rec.getId()==null)
    		return false;
    	return this.getId().equals(rec.getId());
    }

    
    @Override
    public Document getDocument() {
		Criteria c;
		try {
			c = DSApi.context().session().createCriteria(Recipient.class);
	        c.add(Restrictions.eq("id", this.getId()));
	        if (c.list() != null && c.list().size() > 0)
	        	document = (Document) (c.list().get(0));
		} catch (EdmException ex) {
		}
		return document;
    }
}
