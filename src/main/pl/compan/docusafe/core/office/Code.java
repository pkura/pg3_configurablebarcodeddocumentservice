package pl.compan.docusafe.core.office;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.List;
import java.util.Iterator;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.PostalCode;
import pl.compan.docusafe.service.tasklist.Task;

public class Code extends HttpServlet{

	private static final String LocationNotFoundError = "LocNotFound";
	private static final Logger log = LoggerFactory.getLogger(Location.class);
	/**
	 * Delegation method which handles GET and POST requests.
	 * 
	 * @param request
	 *            the request the client has made of the servlet
	 * @param response
     *            the response the servlet sends to the client
	 * @throws ServletException
	 * @throws IOException
	 */
	 public void doGet(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException
	 {

		 	String str_CodeTo   = ""; 
		 	String str_CodeFrom = "";
		 	
		 	int code = 0;
		 	String t = request.getCharacterEncoding();
		 	t = t;
		 	request.setCharacterEncoding("utf-8");		
		 	
		 	
		 	String location = request.getParameter("location").trim();	

		 try
		 {
			 DSApi.openAdmin();
			 
		     List <PostalCode> pc = PostalCode.findByLocation(location);
		    
		     if(pc.size() == 0)
		     {		    	
		    	 str_CodeTo = LocationNotFoundError;
		     }
		     else if(pc.size()==1)
		     {
		    	 str_CodeTo = Integer.toString(pc.get(0).getCode_to()/1000);
		    	 String secondPartTo = Integer.toString(pc.get(0).getCode_to()%1000);
		    	 String validatedCode_To = validateCode(str_CodeTo, secondPartTo);
		    	 
		    	 str_CodeFrom = Integer.toString(pc.get(0).getCode_from()/1000);
		    	 String secondPartFrom = Integer.toString(pc.get(0).getCode_from()%1000);
		    	 String validatedCode_From = validateCode(str_CodeFrom, secondPartFrom);
		    	 
		    	 if(validatedCode_To.equals(validatedCode_From))
		    	 {
		    		 str_CodeTo = validatedCode_To;
		    	 }
		    	 else
		    	 {
		    		 str_CodeTo = validatedCode_From + " - " + validatedCode_To;
		    	 }
 	 
		     }
		     else if(pc.size()>1)
		     {
		    	 String finalStr_CodeTo = "";
		    	 for (Iterator <PostalCode> i = pc.iterator( ); i.hasNext( ); ) {
		    		 PostalCode postcode = i.next();
		    		 
		    		 str_CodeTo = Integer.toString(postcode.getCode_to()/1000);
		    		 String secondPartTo = Integer.toString(postcode.getCode_to()%1000);
			    	 String validatedCode_To = validateCode(str_CodeTo, secondPartTo);
		    		 
			    	 str_CodeFrom = Integer.toString(postcode.getCode_from()/1000);
			    	 String secondPartFrom = Integer.toString(postcode.getCode_from()%1000);
			    	 String validatedCode_From = validateCode(str_CodeFrom, secondPartFrom);
			    	 
			    	 
			    	 finalStr_CodeTo = finalStr_CodeTo + "" + validatedCode_From + " - " + validatedCode_To + "  ; ";	 
		    	 }
		    	 str_CodeTo = finalStr_CodeTo;
		     }
		 }
		 catch (EdmException e) 
		 {
			 log.error(e.getMessage());
		 }                                                                         
		 finally 
		 {                                                                         
                                                                    
			 try                                                                 
			 {      
				 DSApi._close();                                                  
			 }                                                                   
			 catch (Exception ignored) {}                                        
			                                                                       
		 }                                                                         
  		

		 	if ((location != null) ) 
		 	{
		 		
		 		response.setContentType("text/xml; charset=ISO-8859-2");
	            response.setHeader("Cache-Control", "no-cache");
	            response.getWriter().print("<valid>" + str_CodeTo + "</valid>");
	        } 
		 	else
	        {
	        	response.setContentType("text/xml; charset=ISO-8859-2");
	            response.setHeader("Cache-Control", "no-cache");
	            response.getWriter().print("<valid>" + str_CodeTo + "</valid>");
	        }
                                                                         
	 }
	 public String validateCode(String code, String secondPart) 
	 {
		 if(code.length()==1)
    	 {
    		 code = "0" + code + "-";
    	 }
    	 else
    	 {
    		 code = code + "-";
    	 }
    	 
    	 	if(secondPart.length()==0) code = code + "000" + secondPart;
    	 	if(secondPart.length()==1) code = code + "00" + secondPart;
    	 	if(secondPart.length()==2) code = code + "0" + secondPart;
    	 	if(secondPart.length()==3) code = code + secondPart;
    	 	
    	 	
    	 	return code;
	 }
	 
	 
	 public  void doPost(HttpServletRequest request, HttpServletResponse  response)
     throws IOException, ServletException {

		 doGet(request, response);
     }
	
}
