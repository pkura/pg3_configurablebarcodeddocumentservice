package pl.compan.docusafe.core.office;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.classic.Lifecycle;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import std.lambda;

/**
 * Polecenie. Dziedziczy po OfficeDocument i w wiekszosci zachowuje sie tak samo jak
 * kazdy dokument kancelaryjny, ale czesc pol ktore posiada OfficeDocument sa niepotrzebne
 * (nieuzywane) - wowczas sa rowne null.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Micha Manski</a>
 * @version $Id$
 */
public class OfficeOrder extends OfficeDocument
    implements Lifecycle
{

    private static final Logger log = LoggerFactory.getLogger(OfficeOrder.class);

    private StringManager sm=
        GlobalPreferences.loadPropertiesFile(OfficeOrder.class.getPackage().getName(),null);
    public static final String TYPE = "order";
    public static final String PRIORYTET_ZWYKLY = "ZWYKLY";
    public static final String PRIORYTET_WAZNY = "WAZNY";
    public static final String PRIORYTET_PILNY = "PILNY";
    public static final String PRIORYTET_BARDZO_PILNY = "BARDZO_PILNY";
    public static final String STATUS_POLECENIA_OTWARTE = "OTWARTE";
    public static final String STATUS_POLECENIA_WYKONANE = "WYKONANE";

    private String creatingUser;
    /**
     * Dokument kancelaryjny zwiazany z tym poleceniem - mo�e by� null
     */
    private Document relatedOfficeDocument;
    /**
     * Termin polecenia - nieobowi�zkowy
     */
    private Date finishDate;
    private String status;
    private String priority;
    private String detailedDescription;
    private Date documentDate;



    private static final lambda<OfficeOrder, OfficeOrder> identityMapper =
        new lambda<OfficeOrder, OfficeOrder>()
    {
        public OfficeOrder act(OfficeOrder o)
        {
            return o;
        }
    };

    public static SearchResults<OfficeOrder> search(Query query) throws EdmException
    {

        return search(query, identityMapper, OfficeOrder.class);
    }

    public static <T> SearchResults<T> search(Query query, lambda<OfficeOrder, T> mapper,
                                              Class<T> mappedClass)
        throws EdmException
    {
        if (mapper == null)
            throw new NullPointerException("mapper");

        FromClause from = new FromClause();
        TableAlias docTable = from.createTable("DSO_ORDER");


        WhereClause where = new WhereClause();


        if (!DSApi.context().isAdmin() &&
            !(DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD) ||
              DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE) ||
              DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)))
        {
            DSUser user = DSApi.context().getDSUser();

            Junction OR = Expression.disjunction();

            if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD) ||
                DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_MODYFIKACJE))
            {
                DSDivision[] divisions = user.getDivisions();
                if (divisions.length > 0)
                {
                    for (int i=0; i < divisions.length; i++)
                    {
                        OR.add(Expression.eq(docTable.attribute("divisionguid"), divisions[i].getGuid()));
                        if (divisions[i].isPosition())
                        {
                            DSDivision parent = divisions[i].getParent();
                            if (parent != null)
                                OR.add(Expression.eq(docTable.attribute("divisionguid"), parent.getGuid()));
                        }
                    }
                    OR.add(Expression.eq(docTable.attribute("clerk"), user.getName()));
                }
            }

            if (DSApi.context().hasPermission(DSPermission.PISMO_KO_PODGLAD))
            {
                OR.add(Expression.eq(docTable.attribute("divisionguid"), DSDivision.ROOT_GUID));
            }

            OR.add(Expression.eq(docTable.attribute("clerk"), user.getName()));

            where.add(OR);
        }

        if (query.clerk != null)
        {
            where.add(Expression.eq(docTable.attribute("clerk"), query.clerk));
        }

        if (query.documentDateFrom != null)
        {
            where.add(Expression.ge(docTable.attribute("documentdate"),
                DateUtils.midnight(query.documentDateFrom, 0)));
        }
        if (query.documentDateTo != null)
        {
            where.add(Expression.lt(docTable.attribute("documentdate"),
                DateUtils.midnight(query.documentDateTo, 1)));
        }

        if (query.creatingUser != null)
        {
            where.add(Expression.eq(docTable.attribute("creatinguser"), query.creatingUser));
        }

        if (query.summary != null)
        {
            where.add(Expression.like(Function.upper(docTable.attribute("summary")), StringUtils.substring(query.summary.toUpperCase(), 0, 79)+"%"));
        }

        if (query.officeNumberYear != null)
        {
            where.add(Expression.eq(docTable.attribute("officenumberyear"), query.officeNumberYear));
        }

        if (query.officeNumber != null)
        {
            where.add(Expression.eq(docTable.attribute("officenumber"), query.officeNumber));
        }

        if (query.fromOfficeNumber != null && query.toOfficeNumber != null)
        {
            where.add(Expression.ge(docTable.attribute("officenumber"), query.fromOfficeNumber));
            where.add(Expression.le(docTable.attribute("officenumber"), query.toOfficeNumber));
        }

        if (query.status != null)
        {
            where.add(Expression.eq(docTable.attribute("status"), query.status));
        }

        if (query.priority != null)
        {
            where.add(Expression.eq(docTable.attribute("priority"), query.priority));
        }

        if (query.documentId != null)
        {
            where.add(Expression.eq(docTable.attribute("document_id"), query.documentId));
        }

        if (query.detailedDescription != null)
        {
            where.add(Expression.eq(docTable.attribute("detailedDescription"), query.detailedDescription));
        }



        // kolumny, po kt�rych wykonywane jest sortowanie musz� si�
        // znale�� w SELECT
        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(docTable, "id");

        OrderClause order = new OrderClause();
        if(query.sortField != null)
        {

                order.add(docTable.attribute(query.sortField), query.ascending);
                selectId.add(docTable, query.sortField);
        }

        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(distinct "+docTable.getAlias()+".id)");

        SelectQuery selectQuery;
        ResultSet rs = null;
        int totalCount;
        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby dokument�w.");

            totalCount = rs.getInt(countCol.getPosition());

            rs.close();

            selectQuery = new SelectQuery(selectId, from, where, order);
            selectQuery.setMaxResults(query.limit);
            selectQuery.setFirstResult(query.offset);
            log.debug(selectQuery.getSql());

            rs = selectQuery.resultSet(DSApi.context().session().connection());

            List<T> results = new ArrayList<T>(query.limit);

            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                OfficeOrder doc = (OfficeOrder)
                    DSApi.context().session().load(OfficeOrder.class, id);
                results.add(mapper.act(doc));
            }

            rs.close();

            return new SearchResultsAdapter<T>(results, query.offset, totalCount, mappedClass);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	if (rs!=null) {
        		try { rs.close(); } catch (Exception e) { }
        	}
        }
    }

    public static Map<String,String> getPriorityMap()
    {
        Map<String,String> priorities = new LinkedHashMap<String,String>(3);
        priorities.put(PRIORYTET_ZWYKLY, "zwyk�y");
        priorities.put(PRIORYTET_WAZNY, "wa�ny");
        priorities.put(PRIORYTET_PILNY, "pilny");
        priorities.put(PRIORYTET_BARDZO_PILNY, "bardzo pilny");
        return priorities;
    }

    public static Map<String,String> getStatusMap()
    {
        Map<String,String> statusy = new LinkedHashMap<String,String>(3);
        statusy.put(STATUS_POLECENIA_OTWARTE, "otwarte");
        statusy.put(STATUS_POLECENIA_WYKONANE, "wykonane");
        //statusy.put(STATUS_POLECENIA_ZAMKNIETE, "zamkni�te");
        return statusy;
    }

    public DocumentType getType()
    {
        return DocumentType.ORDER;
    }


    @Override public String getStringType()
    {
        return OfficeOrder.TYPE;
    }
        /**
     * Zapisuje dokument w sesji Hibernate.
     * <p>
     * Zapisuje w sesji warto�� pola sender (je�eli istnieje) oraz
     * przypisuje dokumentowi folder SN_OFFICE. Nast�pnie wywo�uj�
     * metod� {@link pl.compan.docusafe.core.base.Document#create()}.
     */
    public void create() throws EdmException
    {
        setFolder(Folder.findSystemFolder(Folder.SN_OFFICE));

        super.create();
    }

    public static OfficeOrder findOfficeOrder(Long id)
        throws EdmException
    {
        Document doc = find(id);

        if (!(doc instanceof OfficeOrder))
            throw new EdmException("Nie znaleziono polecenia nr "+id);

        return (OfficeOrder) doc;
    }

    /**
     * polecenie nigdy nie zawiera odbiorc�w
     * @return null
     */
    public List<Recipient> getRecipients()
    {
        return new ArrayList<Recipient>();
    }

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }

    public OfficeDocument getRelatedOfficeDocument()
    {
        if (relatedOfficeDocument != null && relatedOfficeDocument instanceof OfficeDocument)
            return (OfficeDocument) relatedOfficeDocument;
        else
            return null;
    }

    public void setRelatedOfficeDocument(OfficeDocument relatedOfficeDocument)
    {
        this.relatedOfficeDocument = relatedOfficeDocument;
    }

    public Date getFinishDate()
    {
        return finishDate;
    }
    
    public Date getDocumentDate()
    {
        return documentDate;
    }
    
    public void setDocumentDate(Date date)
    {
    	this.documentDate = date;
    }
    
    public void setFinishDate(Date finishDate)
    {
        if (getId() != null &&
            this.finishDate != null && !this.finishDate.equals(finishDate))
        {
            if (!StringUtils.isEmpty(finishDate.toString()))
            {
            	addWorkHistoryEntry(Audit.create(
                    "finishDate", DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaTerminuWykonaniaNa",DateUtils.formatJsDate(finishDate)), DateUtils.formatJsDate(finishDate)));
            }
            else
            {
            	addWorkHistoryEntry(Audit.create(
                    "finishDate", DSApi.context().getPrincipalName(),
                    sm.getString("UsunietoTerminWykonania")));
            }
        }
        this.finishDate = finishDate;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        if (getId() != null &&
            this.status != null && !this.status.equals(status))
        {
            if (!StringUtils.isEmpty(status))
            {
            	addWorkHistoryEntry(Audit.create(
                    "status", DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaStatusuNa",status), status));
            }
        }
        this.status = status;
    }

    public String getPriority()
    {
        return priority;
    }

    public void setPriority(String priority)
    {
        if (getId() != null &&
            this.priority != null && !this.priority.equals(priority))
        {
            if (!StringUtils.isEmpty(priority))
            {
            	addWorkHistoryEntry(Audit.create(
                    "priority", DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaPriorytetuNa",priority), priority));
            }
        }
        this.priority = priority;
    }

    public String getDetailedDescription()
    {
        return detailedDescription;
    }

    public void setDetailedDescription(String detailedDescription)
    {
        if (getId() != null &&
            this.detailedDescription != null && !this.detailedDescription.equals(detailedDescription))
        {
            if (!StringUtils.isEmpty(detailedDescription))
            {
            	addWorkHistoryEntry(Audit.create(
                    "detailedDescription", DSApi.context().getPrincipalName(),
                    sm.getString("ZmianaSzczegolowejTresciNa",detailedDescription), detailedDescription));
            }
            else
            {
            	addWorkHistoryEntry(Audit.create(
                    "detailedDescription", DSApi.context().getPrincipalName(),
                    sm.getString("UsunietoOpisSzczegolowy"), null));
            }
        }

        this.detailedDescription = detailedDescription;
    }

    public void assignOfficeNumber() throws EdmException
    {
        try
        {
            int year = GlobalPreferences.getCurrentYear();

            List max = DSApi.context().classicSession().find(
                "select max(o.officeNumber) from o in class "+OfficeOrder.class.getName()+" where o.officeNumberYear="+year);

            if (max.size() > 0 && max.get(0) instanceof Integer)
            {
            	Integer no = (Integer) max.get(0) + 1;
                setOfficeNumber(no.intValue());
            }
            else
                setOfficeNumber(1);
            setOfficeNumberYear(year);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Query query(int offset, int limit)
    {
        return new Query(offset, limit);
    }

    public static class Query
    {

        private Integer officeNumberYear;
        private Integer officeNumber;
        private Integer fromOfficeNumber;
        private Integer toOfficeNumber;
        private String clerk;
        private String summary;
        private Date documentDateFrom;
        private Date documentDateTo;
        private Long documentId;
        private String sortField;
        private String status;
        private String priority;
        private String creatingUser;
        private String detailedDescription;

        private int offset;
        private int limit;
        private boolean ascending;


        public Query(int offset, int limit)
        {
            this.offset = offset;
            this.limit = limit;
        }

        public void setClerk(String clerk)
        {
            this.clerk = clerk;
        }

        public void setDocumentDateFrom(Date documentDateFrom)
        {
            this.documentDateFrom = documentDateFrom;
        }

        public void setDocumentDateTo(Date documentDateTo)
        {
            this.documentDateTo = documentDateTo;
        }

        public void setDocumentId(Long documentId)
        {
            this.documentId = documentId;
        }

        public void setFromOfficeNumber(Integer fromOfficeNumber)
        {
            this.fromOfficeNumber = fromOfficeNumber;
        }

        public void setLimit(int limit)
        {
            this.limit = limit;
        }

        public void setOfficeNumber(Integer officeNumber)
        {
            this.officeNumber = officeNumber;
        }

        public void setOfficeNumberYear(Integer officeNumberYear)
        {
            this.officeNumberYear = officeNumberYear;
        }

        public void setOffset(int offset)
        {
            this.offset = offset;
        }

        public void setSortField(String sortField)
        {
            this.sortField = sortField;
        }

        public void setToOfficeNumber(Integer toOfficeNumber)
        {
            this.toOfficeNumber = toOfficeNumber;
        }

        public void setclerk(String clerk)
        {
            this.clerk = clerk;
        }
        public void setCreatingUser(String creatingUser)
        {
            this.creatingUser = creatingUser;
        }
        public void setDetailedDescription(String detailedDescription)
        {
            this.detailedDescription = detailedDescription;
        }
        public void setdocumentDateFrom(Date documentDateFrom)
        {
            this.documentDateFrom = documentDateFrom;
        }
        public void setdocumentDateTo(Date finishDateTo)
        {
        	this.documentDateTo = finishDateTo;
            //this.documentDateTo = documentDateTo;
        }
        public void setPriority(String priority)
        {
            this.priority = priority;
        }

        public void setStatus(String status)
        {
            this.status = status;
        }
        public void setSummary(String summary)
        {
            this.summary = summary;
        }

        public void setAscending(boolean ascending)
        {
           this.ascending = ascending;

        }

    }

	@Override
	public void addRecipient(Recipient recipient) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeRecipient(Recipient recipient) throws EdmException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setRecipients(String... recipients) throws EdmException {}

    @Override
    public void bindToJournal(Long journalId, Integer sequenceId) throws EdmException {
    }
}
