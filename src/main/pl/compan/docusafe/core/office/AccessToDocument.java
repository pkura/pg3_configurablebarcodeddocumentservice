package pl.compan.docusafe.core.office;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

@Entity
@Table(name = "DS_ACCESS_TO_DOCUMENT")

public class AccessToDocument {

	@Id
	@Column(name = "id", nullable = false)
	private Integer id;

	@Column(name = "posn")
	private Integer posn;

	@Column(name = "name")
	private String name;

	public AccessToDocument() {

	}

	public static List<AccessToDocument> list() throws EdmException{

		return DSApi.context().session().createCriteria(AccessToDocument.class).addOrder(Order.asc("id")).list();
	}

	public static AccessToDocument findById(Integer Id) throws EdmException{
		return (AccessToDocument) (DSApi.context().session().createCriteria(AccessToDocument.class).add(Restrictions.eq("id", Id)).list().get(0));
	}

	public final void create() throws EdmException
	{
		try
		{
			DSApi.context().session().save(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPosn() {
		return posn;
	}

	public void setPosn(Integer posn) {
		this.posn = posn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
