package pl.compan.docusafe.core.office;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.QueryForm;

/**
 * Klasa reprezentuj�ca szko��. 
 * 
 *@author <a href="mailto:michal.manski@com-pan.pl">Micha� Ma�ski</a>
 *
 */
public class School {

	private Long id;
    private String name;
    private String street;
    private String zip;
    private String location;
    
    public static School find(Long id) throws EdmException
    {
        School school = (School) DSApi.getObject(School.class, id);

        if (school == null)
            throw new EntityNotFoundException(School.class, id);

        return school;
    }
    
    /**
     * Zapisuje w bazie danych bie��cy obiekt, je�eli nie istnieje
     * inny o takich samych warto�ciach p�l.
     * @return
     * @throws EdmException
     */
    public boolean createIfNew() throws EdmException
    {
        try
        {
            Criteria crit = DSApi.context().session().createCriteria(School.class);
            if (name != null) crit.add(Expression.eq("name", name));
            if (street != null) crit.add(Expression.eq("street", street));
            if (zip != null) crit.add(Expression.eq("zip", zip));
            if (location != null) crit.add(Expression.eq("location", location));

            if (crit.list().size() == 0)
            {
                DSApi.context().session().save(this);
                return true;
            }

            return false;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Znajduje obiekty School na podstawie formularza wyszukiwania.

     * @param form
     * @return
     * @throws EdmException
     */
    public static SearchResults<? extends School> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(School.class);

            if (form.hasProperty("name"))
                c.add(Expression.like("name", StringUtils.left((String) form.getProperty("name"), 49)+"%"));
            if (form.hasProperty("street"))
                c.add(Expression.like("street", StringUtils.left((String) form.getProperty("street"), 49)+"%"));
            if (form.hasProperty("zip"))
                c.add(Expression.like("zip", StringUtils.left((String) form.getProperty("zip"), 13)+"%"));
            if (form.hasProperty("location"))
                c.add(Expression.like("location", StringUtils.left((String) form.getProperty("location"), 49)+"%"));

            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }

            List<School> list = (List<School>) c.list();

            if (list.size() == 0)
            {
                return SearchResultsAdapter.emptyResults(School.class);
            }
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                int toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();

                return new SearchResultsAdapter<School>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), School.class);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public Long getId()
    {
    	return id;
    }
    
    private void setId(Long id)
    {
    	this.id = id;
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }
}
