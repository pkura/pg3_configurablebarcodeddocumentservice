package pl.compan.docusafe.core.office;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.parametrization.opzz.dictionary.OrganizationUserDictionary;
import pl.compan.docusafe.util.SqlUtils;
import pl.compan.docusafe.web.common.form.UpdateUser;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Role.java,v 1.21 2010/03/05 11:17:11 tomekl Exp $
 */
public class Role
{
    private Long id;
    /**
     * Nazwa roli nadawana przez administratora.
     */
    private String name;
    /**
     * Kolekcja nazw u�ytkownik�w w tej roli.
     */
    private Set<String> usernames;
    private Map<String,String> usersSource;
    /**
     * Kolekcja nazw dzia��w w tej roli.
     */
    private Set<String> divisions;
    /**
     * Kolekcja nazw profili w tej roli.
     */
    private Set<String> profiles;
    /**
     * Nazwy uprawnie� przypisanych do tej roli (obiekty String).
     */
    private Set<String> permissions;

    //do przeniesienia w bardziej stosowne miejsce
    //public static final String[] archiveRoles = new String[] {"data_admin", "admin", "archive_only", "support"};
    public static final String[] archiveRoles = new String[] {"data_admin", "admin", "archive_only","odbior_only"};
    
    
    private Role()
    {
    }

    public Role(String name)
    {
        this.name = name;
    }


    public static Role find(Long id) throws EdmException
    {
        return (Role) Finder.find(Role.class, id);
    }
    
    
    public static Role findByName(String name) throws EdmException
    {
    	Criteria criteria = DSApi.context().session().createCriteria(Role.class);
    	
		if (name != null)
		{ 		// PAA wyszukiwanie roli jak nieznajdzie roli "Sekretariat" role zamiast sekretariat to osekretarka albo sekretarz
			if (name.contains("%"))
				criteria.add(Restrictions.like("name", name));
			 else 
			criteria.add(Restrictions.eq("name", name));
			
			List<Role> results = criteria.list();
			
			if(results != null && results.size() > 0)
				return results.get(0);
			else
				return null;
		}
		else
			return null;
    }
    
    /**
     * @author Mariusz Kilja�czyk
     * @param id roli
     * @return Liste permissions jakie posiada rola o id 
     * @throws EdmException
     */
    
    @SuppressWarnings("deprecation")
    public static List<DSPermission> findPermissions(Long id) throws EdmException
    {
        try
        {
            List roles = DSApi.context().classicSession().find(
                "select r from r in class "+Role.class.getName()+
                " where id = ? '",id, new LongType());
            
            List<DSPermission> permissions = new ArrayList<DSPermission>();
            List<DSPermission> tmp = new ArrayList<DSPermission>();
            for (Iterator iter = roles.iterator(); iter.hasNext();)
            {
                Role element = (Role) iter.next();
                tmp = element.getPermissionsObjects();
                for (Iterator iterator = tmp.iterator(); iterator.hasNext();)
                {
                    DSPermission per = (DSPermission) iterator.next();
                    permissions.add(per);                    
                }
                
            }
            return permissions;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca list� r�l posortowanych po nazwie.
     * TODO: zamieni� zwracany typ na list�
     */
    public static Role[] list() throws EdmException
    {
        return (Role[]) Finder.list(Role.class, "name").toArray(new Role[0]);
    }

    
    public void create() throws EdmException
    {
        try
        {
            List cnt = DSApi.context().classicSession().find(
                "select count(r) from r in class "+Role.class.getName()+
                " where upper(name) = ?", name.toUpperCase(), new StringType());



            if (cnt != null && cnt.size() > 0 &&
                ((Long) cnt.get(0)).intValue() > 0)
                throw new DuplicateNameException("Istnieje ju� rola o nazwie '"+name+"'");

            Persister.create(this);
        }
        catch (HibernateException e)
        { 
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException
    {
    	for (Profile prof : Profile.getProfiles()) {
    		if (prof.getOfficeRoles().contains(this)) {
    			prof.removeOfficeRole(this);
    		}
    	}
        Persister.delete(this);
    }


    /**Zwraca liste id rol przypisanych do uzytkownika, parametr wej�ciowy user.name
     * @author Mariusz Kilja�czyk
     * @param username 
     * @return ResultSet role_id
     * @throws EdmException
     */
    
    public static List<Long> listUserRoles(String username) throws EdmException
    {
        ResultSet rs = null;
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement("select role_id from dso_role_usernames where username = ? ");
            ps.setString(1, username);

            rs = ps.executeQuery();
            
            List<Long> result = new ArrayList<Long>(); 
            while(rs.next())
            {
            	result.add(rs.getLong("role_id"));
            }
            rs.close();            
            return result;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
        	LogFactory.getLog("eprint").debug("", e);
        }
        catch (EdmException e)
        {
        	LogFactory.getLog("eprint").debug("", e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
        return null;
    }


    public boolean addUser(String user, String source) throws EdmException {
    	if (usernames == null)
            usernames = new HashSet<String>();
        else
            DSApi.initializeProxy(usernames);
    	if (usersSource == null) {
    		usersSource = new HashMap<String, String>();
    	}
    	return UpdateUser.addObjectWithSource(user, source, user, usernames, usersSource, this);
    }
    
    public void addUser(String user) throws EdmException {
    	addUser(user, "own");
    }
    
    public boolean removeUser(String user, String source) throws EdmException
    {
        if (usernames == null || !usernames.contains(user)) {
        	return false;
        }
        if (UpdateUser.removeSource(usersSource, user, source)) {
        	DSApi.initializeProxy(usernames);
        	return usernames.remove(user);
        }
        return true;
    }
    
    public boolean removeUser(String user) throws EdmException {
    	return removeUser(user, "own");
    }
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name) throws EdmException
    {
        if (name == null)
            throw new NullPointerException("name");
        
        try
        {
            List cnt = DSApi.context().classicSession().find(
                "select count(r) from r in class "+Role.class.getName()+
                " where r.name = ? and r.id != "+id,
                name, new StringType());

            if (cnt != null && cnt.size() > 0 &&
                ((Long) cnt.get(0)).intValue() > 0)
                throw new DuplicateNameException("Istnieje ju� rola o nazwie '"+name+"'");
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }

        this.name = name;
    }

    /**
     * Zwraca list� obiekt�w {@link DSPermission} nale��cych
     * do tej roli.
     */
    public List<DSPermission> getPermissionsObjects()
    {
        if (permissions != null && permissions.size() > 0)
        {
            List<DSPermission> perms = new ArrayList<DSPermission>(permissions.size());
            for (Iterator iter=permissions.iterator(); iter.hasNext(); )
            {
                String permName = (String) iter.next();
                DSPermission perm = DSPermission.getByName(permName);
                if (perm != null)
                    perms.add(perm);
            }
            return perms;
        }

        return Collections.emptyList();
    }

    
    public String[] getUserSources(DSUser user) {
    	if (!usersSource.containsKey(user.getName())) {
    		return null;
    	}
    	
    	return StringUtils.split(usersSource.get(user.getName()), ";");
    }
    
    public String[] getUserSources(String username) {
    	if (!usersSource.containsKey(username)) {
    		return null;
    	}
    	
    	return StringUtils.split(usersSource.get(username), ";");
    }
    
    
    public void clearPermissions()
    {
        if (permissions != null) permissions.clear();
    }

    public void addPermission(DSPermission perm)
    {
        if (permissions == null) permissions = new HashSet<String>();
        permissions.add(perm.getName());
    }

    public Set<String> getPermissions()
    {
        return Collections.unmodifiableSet(permissions);
    }

    private void setPermissions(Set<String> permissions)
    {
        this.permissions = permissions;
    }

    public Set<String> getUsernames()
    {
        return usernames;
    }

    private void setUsernames(Set<String> usernames)
    {
        this.usernames = usernames;
    }

    public Set<String> getDivisions()
    {
        return divisions;
    }

    private void setDivisions(Set<String> divisions)
    {
        this.divisions = divisions;
    }
}
