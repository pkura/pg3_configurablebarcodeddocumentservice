package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class EnumCnDecisionHandler implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(EnumCnDecisionHandler.class);

    private String field;
    /**
     * open context as needed only then, when this is true
     */
    private boolean openContextAsNeeded = false;

    public String decide(OpenExecution openExecution) {
        boolean wasOpened = false;
        try{
            if (openContextAsNeeded && !DSApi.isContextOpen()) {
                wasOpened = true;
                DSApi.openAdmin();
            }
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            LOG.info("documentid = " + docId + " field = " + field);
            String cn = fm.getEnumItemCn(field);
            if(cn == null) {
                throw new IllegalStateException("Nie wype�niono pola " + fm.getField(field).getName());
            }
            return cn.toLowerCase();
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        } finally {
            if (openContextAsNeeded && DSApi.isContextOpen() && wasOpened)
                try {
                    DSApi.close();
                } catch (EdmException e) {
                    LOG.error(e.getMessage(), e);
                }
        }
    }
}

