/**
 * 
 */
package pl.compan.docusafe.core.jbpm4;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class IsEmptyListDecisionHandler implements DecisionHandler {
	private final static Logger LOG = LoggerFactory.getLogger(IsEmptyFieldDecisionHandler.class);
	private String isEmptyList;
	
	public String decide(OpenExecution openExecution) {
        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            LOG.info("documentid = " + docId + " isEmptyList = " + isEmptyList);
            if (fm.getKey(isEmptyList) instanceof List) {
            	List cn = (List)fm.getKey(isEmptyList);
            	if (cn == null || cn.isEmpty()) {
            		return "empty";
            	} else {
            		return "notEmpty";
            	}
            } else {
            	Object cn = fm.getKey(isEmptyList);
            	if (cn == null) {
            		return "empty";
            	} else {
            		return "notEmpty";
            	}
            }
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
	}

}
