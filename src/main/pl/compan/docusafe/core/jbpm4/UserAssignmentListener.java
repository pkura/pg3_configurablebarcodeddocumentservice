package pl.compan.docusafe.core.jbpm4;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Listener pozwalaj�cy dekretowa� na u�ytkownika,
 * w definicji procesu dok�adnie jedna z warto�ci "username"
 * lub "field" musi by� wype�niona by listener dzia�a� poprawnie
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class UserAssignmentListener extends AbstractEventListener{
    private final static Logger LOG = LoggerFactory.getLogger(UserAssignmentListener.class);

    /**
     * nazwa (login) u�ytkownika
     */
    String username;
    
    /**
     * nazwa pola w dokumencie, kt�re zawiera u�ytkownika (pole typu dsuser)
     */
    String field;

    Boolean valid;

    public UserAssignmentListener(){
    }

    private void check() {
        if (valid == null) {
            int check =
                  (StringUtils.isBlank(username) ? 0 : 1)
                + (StringUtils.isBlank(field) ? 0 : 1);
            valid = check == 1;
        }
        if (!valid) {
            throw new IllegalStateException("w definicji procesu dok�adnie jedna z warto�ci \"username\" lub \"field\" musi by� wype�niona");
        }
    }

    public void notify(EventListenerExecution eventListenerExecution) throws Exception {
        check();
        Long docId = Long.parseLong(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY)+"");
        LOG.info("doc id = " + docId);
        LOG.info("username = " + username);
        LOG.info("field = " + field);
        DSUser user;
        if(!StringUtils.isBlank(username)){
            user = DSUser.findByUsername(username);
        } else {
            user = DSUser.findByUsername(Document.find(docId).getFieldsManager().getEnumItemCn(field));
        }
        String activityId = eventListenerExecution.getVariable(Jbpm4Constants.ACTIVITY_ID_PROPERTY).toString();
        LOG.info("activityId = " + activityId);
        WorkflowFactory.simpleAssignment(
                activityId,
				user.getAllDivisions()[0].getGuid(),
				user.getName(),
				DSApi.context().getPrincipalName(),
				null,
				WorkflowFactory.SCOPE_ONEUSER,
				"do procesowania",
				null);
        eventListenerExecution.setVariable(Jbpm4Constants.REASSIGNMENT_PROPERTY, true);
    }
}
