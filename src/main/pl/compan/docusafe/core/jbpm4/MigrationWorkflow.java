package pl.compan.docusafe.core.jbpm4;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Maps;

/***
 * Metoda migruje zadania ze starego (internal) Workflow do JBPM
 */
public class MigrationWorkflow implements Runnable
{
	private final static Logger migrationLog = LoggerFactory.getLogger("migrationLog");
	private static Map<String,String> userToDivision = new HashMap<String, String>();
	@Override
	public void run() {
		//lista dekretacji na dzial
		Map<String, TaskSnapshot> taskToDivisions = new HashMap<String, TaskSnapshot>();
		List<Long> erroDocumentIds = new ArrayList<Long>();
		
		try
		{
			migrationLog.warn("START PROCEDURY MIGRACYJNEJ: {}", new Date());
			DSApi.openAdmin();
			int start = Docusafe.getAdditionProperty("migrationStartIndex") != null ? Integer.valueOf(Docusafe.getAdditionProperty("migrationStartIndex")) : 0;
			Criteria c = DSApi.context().session().createCriteria(TaskSnapshot.class).addOrder(Order.asc("documentId")).setFirstResult(start); 
			
			List list = c.list();
			int i = 0;
			DSApi.context().begin();
			for (TaskSnapshot oldSnpashot : (List<TaskSnapshot>)list)
			{
				i ++;
				Long documentID = oldSnpashot.getDocumentId();
				try
				{
					migrationLog.warn("!!!DocumentID: {} ", documentID +" i = "+i);
					String userName = oldSnpashot.getDsw_resource_res_key();
					String divisionGuid = oldSnpashot.getDivisionGuid();

					boolean isAccepted = oldSnpashot.isAccepted();
					int resourceCount = oldSnpashot.getSameProcessSnapshotsCount();
					String dockindName = oldSnpashot.getDockindName();
					String processDefinitionName = oldSnpashot.getProcessDefinitionId();
					migrationLog.warn("{}: -> DocumentId: {}, userName: {}, divisionGuid: {}, isAccepted: {}, resourceCount: {}, dockindName: {}, processDefinitionName: {}", 
							start++, documentID,userName,divisionGuid,isAccepted,resourceCount,dockindName,processDefinitionName );
			       
					if(divisionGuid != null && divisionGuid.equalsIgnoreCase("null"))
			        {
			        	migrationLog.error("Przekaza� guid jako null zmieniam na ROOT");
			        	divisionGuid = DSDivision.ROOT_GUID;
			        }
					
					Map<String, Object> map = Maps.newHashMap();
					if(isAccepted) 
						migrationLog.warn("!!!DocumentID: {} is accepted", documentID );
					OfficeDocument document = OfficeDocument.find(documentID);
					// teczka trzea bedzie na odpowiedni etap od razu;
					if (dockindName.equals("Teczka"))
					{
						map.put(ASSIGN_USER_PARAM,userName);
						map.put(ASSIGN_DIVISION_GUID_PARAM, divisionGuid);
						document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
						TaskSnapshot.updateByDocument(document);
					}
					else if (processDefinitionName.equals("obieg_reczny"))
					{
						// dekretacja na dzia�
						if (!isAccepted && resourceCount >1)
						{
							if (!taskToDivisions.containsKey(documentID.toString()))
								taskToDivisions.put(documentID.toString(), oldSnpashot);
							//map.put(ASSIGN_DIVISION_GUID_PARAM, divisionGuid);
						}
						else
						{
							map.put(ASSIGN_USER_PARAM,userName);
							map.put(ASSIGN_DIVISION_GUID_PARAM, divisionGuid);
							document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
							TaskSnapshot.updateByDocument(document);
						}
					}
					else // do wiadomosci
					{
//						// dekretacja na dzia�
//						if (!isAccepted && resourceCount >1)
//						{
//							if (!taskToDivisions.containsKey(documentID.toString()))
//								taskToDivisions.put(documentID.toString(), oldSnpashot);
//							continue;
//							//map.put(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, Collections.EMPTY_LIST.add(divisionGuid));
//						}
//						else
//						{
							List<String> users = new ArrayList<String>();
							users.add(userName);
							map.put(Jbpm4Constants.READ_ONLY_ASSIGNMENT_USERNAMES,users);
							List<String> guids = new ArrayList<String>();
							if (divisionGuid != null)
								guids.add(divisionGuid);
							map.put(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, guids);
							if(document.getDocumentKind() == null)
							{
								migrationLog.warn("DocumentKind is null {}", documentID );
								continue;
								
							}						
							document.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess("read-only").getLogic().startProcess(document, map);
							TaskSnapshot.updateByDocument(document);
//						}
					}
				}
				catch (Exception e) {
					migrationLog.error("MIGRATION ERROR B��d dokumentu ID = {}",documentID);
					migrationLog.error(e.getMessage(),e);
					erroDocumentIds.add(documentID);
				}
				//migrationLog.error("if(i % 100 == 0) "+ (i % 100 == 0)+ " i % 100 " + (i % 100));
				if(i % 100 == 0)
				{
					migrationLog.error("Wyczyszczenie sesji na idx: " + i);
					DSApi.context().commit();
//					migrationLog.error("Wyczyszczenie sesji commit na idx: " + i);
					DSApi.context().session().flush();
//					migrationLog.error("Wyczyszczenie sesji flush na idx: " + i);
					DSApi.context().session().clear();
//					migrationLog.error("Wyczyszczenie sesji  clear na idx: " + i);
					DSApi.close();
//					migrationLog.error("Wyczyszczenie sesji close na idx: " + i);
					DSApi.openAdmin();
//					migrationLog.error("Wyczyszczenie sesji open na idx: " + i);
					DSApi.context().begin();
//					migrationLog.error("Wyczyszczenie sesji begin na idx: " + i);
				}
			}
			DSApi.context().commit();
			DSApi.context().session().flush();
			DSApi.context().session().clear();
			DSApi.close();
			DSApi.openAdmin();
			DSApi.context().begin();
			i = 0;
			migrationLog.warn("START MIGRATION ONLY TO DIVISION: {}" + new Date());
			for (String docId : taskToDivisions.keySet())
			{
				i ++;
				TaskSnapshot oldSnpashot = taskToDivisions.get(docId);
				Long documentID = oldSnpashot.getDocumentId();
				String userName = oldSnpashot.getDsw_resource_res_key();
				String divisionGuid = oldSnpashot.getDivisionGuid();
				boolean isAccepted = oldSnpashot.isAccepted();
				int resourceCount = oldSnpashot.getSameProcessSnapshotsCount();
				String dockindName = oldSnpashot.getDockindName();
				String processDefinitionName = oldSnpashot.getProcessDefinitionId();
				Map<String, Object> map = Maps.newHashMap();
				migrationLog.warn("Only to division: DocumentId: {}, userName: {}, divisionGuid: {}, isAccepted: {}, resourceCount: {}, dockindName: {}, processDefinitionName: {}", 
						documentID,userName,divisionGuid,isAccepted,resourceCount,dockindName,processDefinitionName );
				if(isAccepted) 
					migrationLog.warn("!!!Only to division: DocumentID: {} is accepted", documentID );
				OfficeDocument document = OfficeDocument.find(documentID);
				if (processDefinitionName.equals("obieg_reczny"))
				{
					map.put(ASSIGN_DIVISION_GUID_PARAM, divisionGuid);
					document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
					TaskSnapshot.updateByDocument(document);
				}
				else // do wiadomosci
				{
					if(divisionGuid == null)
						divisionGuid = DSDivision.ROOT_GUID;
					map.put(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, Arrays.asList(divisionGuid));//Collections.EMPTY_LIST.add(divisionGuid));
					document.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess("read-only").getLogic().startProcess(document, map);
					TaskSnapshot.updateByDocument(document);
				}
				if(i % 100 == 0)
				{
					migrationLog.info("Wyczyszczenie sesji na idx: " + i);
					DSApi.context().session().flush();
					DSApi.context().session().clear();
					DSApi.context().commit();
					DSApi.close();
					DSApi.openAdmin();
					DSApi.context().begin();
				}
			}
			
		}
		catch (Throwable e)
		{
			migrationLog.error(e.getMessage(), e);
			
			migrationLog.warn("!!! W catch Exception: DocumentsId do dekretacji na dzial: {}" + taskToDivisions.keySet());
			migrationLog.warn("!!! W catch Exception: errorDocs  {}" + erroDocumentIds);
			
		}
		finally
		{
			DSApi._close();
		}
		
	}
	
	
	public static String getWorkFlowPlace(String username) throws DivisionNotFoundException, EdmException
	{
		if(userToDivision.size() < 1)
		{
			for(DSUser user :DSDivision.find("DL_BINDER_DU").getUsers())
			{
				userToDivision.put(user.getName(), "DL_BINDER_DU");
			}
			for(DSUser user :DSDivision.find("DL_BINDER_DO").getUsers())
			{
				userToDivision.put(user.getName(), "DL_BINDER_DO");
			}
			for(DSUser user :DSDivision.find("DL_BINDER_DS").getUsers())
			{
				userToDivision.put(user.getName(), "DL_BINDER_DS");
			}
			for(DSUser user :DSDivision.find("DL_BINDER_DR").getUsers())
			{
				userToDivision.put(user.getName(), "DL_BINDER_DR");
			}
			for(DSUser user :DSDivision.find("DL_BINDER_DO").getUsers())
			{
				userToDivision.put(user.getName(), "DL_BINDER_DO");
			}
		}
		
		return userToDivision.get(username) == null ? "DL_BINDER_DS" : userToDivision.get(username);
	}

}
