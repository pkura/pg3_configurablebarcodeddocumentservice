package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ForceAttachmentListener extends AbstractEventListener{
    private final static Logger LOG = LoggerFactory.getLogger(ForceAttachmentListener.class);

    private String attachmentTitle;

    public void notify(EventListenerExecution eventListenerExecution) throws Exception {
        Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        for(Attachment at: doc.getAttachments()){
            if((attachmentTitle== null && at != null) || (attachmentTitle != null && at.getTitle().equals(attachmentTitle))){
                return;
            }
        }
        
        String msg = "Brak wymaganego za��cznika";
       
        if(attachmentTitle != null)
        	msg += "- " + attachmentTitle;
        
        throw new IllegalStateException(msg);
    }
}
