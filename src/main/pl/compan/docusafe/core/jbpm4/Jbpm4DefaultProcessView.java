package pl.compan.docusafe.core.jbpm4;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.process.AbstractProcessView;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.jbpm4.ProcessExtensionsCache;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.ExtendedRenderBean;
import pl.compan.docusafe.web.common.RenderBean;
import pl.compan.docusafe.web.common.SimpleRenderBean;

/**
 * NA RAZIE NIE DZIA�A CZEKA� NA JBPM 4.4. lub stosowa� ActivityExtensionProcessView
 * 
 * Domy�lny bardzo uproszczony widok dla proces�w jbpmowych,
 * u�ytkownikom udost�pnia przyciski takie jakie przej�cia s� dost�pne z obecnego stanu procesu
 *
 * Je�eli proces wygl�da tak
 *
 *  a -x-> b -y-> c
 *
 * To je�eli proces jest w stanie a jest widoczny przycisk x,
 * je�li jest w stanie b, jest widoczny przycisk y, w stanie c nie jest widoczny �aden przycisk zwi�zany z procesem
 *
 * TODO doda� i18n
 *
 * na razie nie dzia�a, czeka� do JBPM 4.4
 * (https://jira.jboss.org/jira/browse/JBPM-2609)
 *
 * @author Micha� Sankowski
 */
public class Jbpm4DefaultProcessView extends AbstractProcessView {
    private final static Logger log = LoggerFactory.getLogger(Jbpm4DefaultProcessView.class);

    private final static String DEFAULT_TEMPLATE = "jbpm4-default-process-view.vm";

    public Jbpm4DefaultProcessView(final InstantiationParameters ip) throws Exception{

    }
    
    public RenderBean render(ProcessInstance pi, ProcessActionContext context) throws EdmException {
        return render((Jbpm4ProcessInstance)pi,context);
    }

    private RenderBean render(Jbpm4ProcessInstance pi, ProcessActionContext context) throws EdmException {
        throw new UnsupportedOperationException("not implemented yet");
    }
}
