package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ams.ProjMnarLogic;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class MultipleDictPrepareProcessListener implements ExternalActivityBehaviour {

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        final List<Long> argSets = new LinkedList<Long>();

        OfficeDocument doc = getOfficeDocument(execution);
        FieldsManager fm = doc.getFieldsManager();
        List<Map<String, Object>> users = FieldsManagerUtils.getDictionaryItems(
                fm,
                getDictCn (),
                getFieldsProperties());

        for (Map<String, Object> u : users){
            Long dysponentSelectedId = Long.parseLong(((EnumValues)u.get(ProjMnarLogic.joinDictionaryWithField(getDictCn (),getUserFieldCn ()))).getSelectedId());
            if (!argSets.contains(dysponentSelectedId)){
                if (checkConditions(u))
                    argSets.add(dysponentSelectedId);
            }
        }

        if (argSets.size() == 0) throw new Exception(getErrorMsgIfNoUsers());

        execution.setVariable("ids", argSets);
        execution.setVariable("count", argSets.size());
        execution.setVariable("count2", 2);
    }

    /**
     * Sprawdzenie p�l wiersza w s�owniku wp�ywaj�cych czy u�ytkownik powienien zosta� dodany do kandydat�w
     */
    protected boolean checkConditions(Map<String, Object> u){
        return true;
    }

    public abstract String getDictCn ();
    public abstract String getUserFieldCn ();

    public String getErrorMsgIfNoUsers(){
        return "Nie znaleziono u�ytkownik�w w wielowarto�ciowym s�owniku, do kt�rych powinien zosta� przekazany dokument";
    }

    public FieldsManagerUtils.FieldProperty[] getFieldsProperties(){
        return new FieldsManagerUtils.FieldProperty[]{
                new FieldsManagerUtils.FieldProperty(getDictCn (), getUserFieldCn (), EnumValues.class),
//                new FieldsManagerUtils.FieldProperty(ProjMnarLogic.FIELD_DYSPONENCI_SRODKOW_DICT, ProjMnarLogic.DICTFIELD_DYSPONENCI_DYSPONENT, EnumValues.class),
//                new FieldsManagerUtils.FieldProperty(ProjMnarLogic.FIELD_DYSPONENCI_SRODKOW_DICT, ProjMnarLogic.DICTFIELD_DYSPONENCI_POTWIERDZENIE, EnumValues.class)
        };
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
        // TODO Auto-generated method stub

    }

    public static OfficeDocument getOfficeDocument(ActivityExecution execution) throws EdmException {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        return doc;
    }
}