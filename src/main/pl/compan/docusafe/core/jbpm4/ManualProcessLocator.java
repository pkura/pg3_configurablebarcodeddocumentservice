package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.history.HistoryProcessInstance;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;

/**
 * Lokalizator proces�w dla proces�w r�cznych - wyszukuje nawet zamkniete procesy
 * (dzi�ki czemu jest mo�liwe ponowne otwarcie)
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ManualProcessLocator extends Jbpm4ProcessLocator{
    private final static Logger LOG = LoggerFactory.getLogger(ManualProcessLocator.class);

    public ManualProcessLocator(InstantiationParameters ip) {
        super(ip);
    }

    @Override
    protected Jbpm4ProcessInstance provide(String name, String processInstanceId) {
        Jbpm4ProcessInstance ret = super.provide(name, processInstanceId);
        if(ret == null){ // proces mo�e nie istnie� lub jest ju� zamkni�ty
            List<HistoryProcessInstance> list = Jbpm4Provider.getInstance()
                    .getHistoryService().createHistoryProcessInstanceQuery().processInstanceId(processInstanceId).list();
            if(!list.isEmpty()){//proces kiedy� istnia�
                LOG.info("history = {}", list);
                ret = new Jbpm4ProcessInstance(name, list.get(0));
            }
        }
        return ret;
    }
}