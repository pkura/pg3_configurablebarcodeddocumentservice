package pl.compan.docusafe.core.jbpm4;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.jbpm.api.task.Task;
import pl.compan.docusafe.api.ActivityInfo;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Jbpm4ActivityInfo extends ActivityInfo {
    private final static Logger LOG = LoggerFactory.getLogger(Jbpm4ActivityInfo.class);

    private String executionId;
    private Long taskId;
    private String activityId;
    private final Map<String, ActivityParam> params = Maps.newHashMap();
    private final Map<String, Object> cache = Maps.newHashMap();

    public Jbpm4ActivityInfo(String activityId) {
        params.put(VARIABLES, new VariablesParam());
        params.put(TASK_NAME_KEY, new TaskInfoParam());
        params.put(TASK_ASSIGNEE_USER_KEY, params.get(TASK_NAME_KEY));
        params.put(CURRENT_USER_KEY, new CurrentUserParam());
        params.put(ACTIVITY_ID_KEY, new ActivityIdParam());
        params.put(TASK_ID_KEY, new TaskIdParam());

        this.activityId = activityId;
        this.taskId = extractTaskId(activityId);
    }

    private Long extractTaskId(String activityId) {
        if (activityId != null) {
            Pattern p = Pattern.compile("(\\w+)(,)(\\d+)(.*)");
            Matcher m = p.matcher(activityId);

            if (m.find()) {
                activityId = m.group(3);
            } else {
                return null;
            }

            try {
                return Long.parseLong(activityId);
            } catch (NumberFormatException e) {
            }
        }
        return null;
    }

    @Override
    public boolean containsKey(Object key) {
        return params.containsKey(key);
    }

    @Override
    public Object get(Object key) {
        String param = key.toString();
        if (!cache.containsKey(param)) {
            Preconditions.checkArgument(params.containsKey(param), "Unsupported activity parameter: "+key);
            params.get(param).load(this, param);
        }
        return cache.get(param);
    }
    public Object getVariableValue(Object varKey) {
        Object vars = get(VARIABLES);
        if (vars != null && vars instanceof Map)
            return ((Map) vars).get(varKey);
        return null;
    }
    public <T> T getVariableValue(Object varKey, Class<T> cls) {
        Object var = getVariableValue(varKey);
        if (var != null)
            if (cls.isAssignableFrom(var.getClass()))
                return cls.cast(var);
        return null;
    }



    public String getTaskName() {
        return get(TASK_NAME_KEY) != null ? get(TASK_NAME_KEY).toString() : null;
    }

    @Override
    public boolean isEmpty() {
        return cache.isEmpty();
    }

    @Override
    public Set<String> keySet() {
        return cache.keySet();
    }

    @Override
    public Collection<Object> values() {
        return cache.values();
    }

    @Override
    public int size() {
        return cache.size();
    }

    private void putToCache(String param, Object value) {
        cache.put(param, value);
    }

    interface ActivityParam {
        void load(Jbpm4ActivityInfo activityInfo, String param);
    }

    private class VariablesParam implements ActivityParam {
        @Override
        public void load(Jbpm4ActivityInfo activityInfo, String param) {
            if (activityInfo.executionId == null) {
                if (activityInfo.taskId != null) {
                    Task task = Jbpm4Provider.getInstance().getTaskService().getTask(activityInfo.taskId.toString());
                    activityInfo.executionId = task.getExecutionId();
                }
            }
            if (activityInfo.executionId != null) {
                if (Jbpm4Provider.getInstance().getExecutionService().getVariableNames(activityInfo.executionId) != null) {
                    if (DSApi.isOracleServer()) {
                        Set<String> variablessss = Jbpm4Provider.getInstance().getExecutionService().getVariableNames(activityInfo.executionId);
                        boolean a = variablessss.remove("reassignment-time");
                        Map<String, Object> variables = Jbpm4Provider.getInstance().getExecutionService().getVariables(activityInfo.executionId, variablessss);
                        if (variables != null) {
                            activityInfo.putToCache(param, variables);
                        } else {
                            activityInfo.putToCache(param, new HashMap<String, Object>());
                        }
                    } else {
                        Map<String, Object> variables = Jbpm4Provider.getInstance().getExecutionService().getVariables(activityInfo.executionId, Jbpm4Provider.getInstance().getExecutionService().getVariableNames(activityInfo.executionId));
                        if (variables != null) {
                            activityInfo.putToCache(param, variables);
                        } else {
                            activityInfo.putToCache(param, new HashMap<String, Object>());
                        }
                    }
                }
            }
        }
    }

    private class TaskInfoParam implements ActivityParam {
        @Override
        public void load(Jbpm4ActivityInfo activityInfo, String param) {
            if (activityInfo.taskId != null) {
                Task task = Jbpm4Provider.getInstance().getTaskService().getTask(activityInfo.taskId.toString());
                if (task != null) {
                    activityInfo.putToCache(TASK_NAME_KEY, task.getName());
                    activityInfo.putToCache(TASK_ASSIGNEE_USER_KEY, task.getAssignee());
                }
            }
        }
    }

    private class CurrentUserParam implements ActivityParam {
        @Override
        public void load(Jbpm4ActivityInfo activityInfo, String param) {
            try {
                String userName = DSApi.context().getDSUser().getName();
                if (userName != null) {
                    activityInfo.putToCache(param, userName);
                } else {
                    new EdmException("Nie pobrano nazwy użytkownika");
                }
            } catch (EdmException e) {
                LOG.error(e.toString());
            }
        }
    }

    private class ActivityIdParam implements ActivityParam {
        @Override
        public void load(Jbpm4ActivityInfo activityInfo, String param) {
            activityInfo.putToCache(param, activityInfo.activityId);
        }
    }

    private class TaskIdParam implements ActivityParam {
        @Override
        public void load(Jbpm4ActivityInfo activityInfo, String param) {
            activityInfo.putToCache(param, activityInfo.taskId);
        }
    }
}