package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;

import java.util.Date;

/**
 * User: kk
 * Date: 02.09.13
 * Time: 17:07
 */
public class ReassignListener extends AbstractEventListener {

    @Override
    public void notify(EventListenerExecution execution) throws Exception {

        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);

        if (AvailabilityManager.isAvailable("addToWatch"))
            DSApi.context().watch(URN.create(Document.find(docId)));

        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(execution.getProcessDefinitionId());
        ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
        ahe.setCtime(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);

        doc.addAssignmentHistoryEntry(ahe);
    }
}
