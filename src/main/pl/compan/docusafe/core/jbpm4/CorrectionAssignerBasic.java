package pl.compan.docusafe.core.jbpm4;

import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;

/** Custom Activity. <p>
 * Je�li istnieje zmienna accId to usuwa odpowiadaj�c� jej DocumentAcceptance ale zapami�tuje mpkId, <br/>
 * w przeciwnym razie robi CorrectionAssigner.clearAcceptance(docId, state). <br/>
 * Na ko�cu idzie do czynno�ci podanej w zmiennej cState. */
@SuppressWarnings("serial")
public class CorrectionAssignerBasic implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(CorrectionAssigner.class);
    private static final String CORRECTION_FROM_USER = "correctionFromUser";
    private static final String CORRECTION_FROM_STATE = "correctionFromState";
    @Override
	public void execute(ActivityExecution execution) throws Exception
	{
		String state = null;
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		if (execution.hasVariable("cState")) {
			state = execution.getVariable("cState").toString();
		}
		
		if (execution.hasVariable("accId")) {
			Long accId = null;
			try {
				accId = (Long)execution.getVariable("accId");
				if (accId != null) {
					DocumentAcceptance acc = DocumentAcceptance.findById(accId);
					if (acc != null) {
						Long mpkId = acc.getObjectId();
						if (mpkId != null) {
							execution.setVariable("mpkId", mpkId);
						}
						String user = acc.getUsername();
						if (user != null) {
							execution.setVariable("cUser", user);
						}
						CorrectionAssigner.clearAcceptance(accId, docId, state);
					}
				}
			} catch (ClassCastException e) {
				log.error("B��d przy rzutowaniu id akceptacji na potrzeby clearAcceptance");
			}
		} else {
			CorrectionAssigner.clearAcceptance(docId, state);
		}
        String userName = DSApi.context().getPrincipalName();
        execution.setVariable(CORRECTION_FROM_USER, userName);
        String statusCn =  Jbpm4Utils.getDocument(execution).getFieldsManager().getEnumItemCn("STATUS");
        execution.setVariable(CORRECTION_FROM_STATE, statusCn);
		log.info("execution.take(state): {}", state);
		execution.take(state);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{

	}

}
