package pl.compan.docusafe.core.jbpm4;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;

public class CreateCaseListener extends AbstractEventListener
{

	private String rwa;
	private String fromField;
	private String toField;

	public void notify(EventListenerExecution execution) throws Exception
	{

		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument document = OfficeDocument.find(docId);

		if (fromField != null)
		{
			Long teczkaId = document.getFieldsManager().getEnumItem(fromField).getCentrum().longValue();
			rwa = teczkaId.toString();
		}
		if (rwa == null)
			throw new EdmException("Brak nr RWA");

		// odnajduje teczke na podstawie kodu RWA
		OfficeFolder teczka = OfficeFolder.findByOfficeIdMiddle(rwa);
		OfficeCase officeCase = new OfficeCase(teczka);

		// status
		officeCase.setStatus(CaseStatus.find(CaseStatus.ID_OPEN));
		// Proiorytet sprawy - zwykla
		officeCase.setPriority(CasePriority.find(1));
		// data zakonczenia - 30 dni
		Calendar cal = Calendar.getInstance();
		int days = teczka.getDays() != null ? teczka.getDays() : 30;
		cal.add(Calendar.DATE, days);
		officeCase.setFinishDate(cal.getTime());
		// opis
		officeCase.setDescription(teczka.getName() + " - " + document.getDescription());
		// referent
		officeCase.setClerk(DSApi.context().getPrincipalName());

		String[] breakdown = officeCase.suggestOfficeId();
		String suggestedOfficeId = StringUtils.join(breakdown, "");
		String customOfficeIdPrefix = breakdown[0];
		String customOfficeIdPrefixSeparator = breakdown[1];
		String customOfficeIdMiddle = breakdown[2];
		String customOfficeIdSuffixSeparator = breakdown[3];
		String customOfficeIdSuffix = breakdown[4];

		String oid = StringUtils.join(new String[] { customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "", customOfficeIdPrefixSeparator != null ? customOfficeIdPrefixSeparator.trim() : "", customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
				customOfficeIdSuffixSeparator != null ? customOfficeIdSuffixSeparator.trim() : "", customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : "" }, "");

		officeCase.create(oid, customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "", customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "", customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : "");

		document.setContainingCase(officeCase, false);
		if (toField != null)
		{
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(toField, oid);
			document.getDocumentKind().setOnly(document.getId(), values);
		}

	}

}
