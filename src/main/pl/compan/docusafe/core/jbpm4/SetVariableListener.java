package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Listener ustawiaj�cy zmienna
 * @author �ukasz
 * 
 */
public class SetVariableListener extends AbstractEventListener 
{
	private final static Logger log = LoggerFactory.getLogger(UserAssignmentListener.class);
	
	private String name;
	private String value;
	
	@Override
	public void notify(EventListenerExecution execution) throws Exception 
	{
		execution.setVariable(name, value);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}