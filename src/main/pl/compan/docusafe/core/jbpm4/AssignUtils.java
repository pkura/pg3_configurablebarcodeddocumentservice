package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;

/**
 * User: kk
 * Date: 21.01.14
 * Time: 11:20
 */
public class AssignUtils {

    public static final Logger log = LoggerFactory.getLogger(AssignUtils.class);

    private AssignUtils() {};

    /**
     * Dodaje wiadomo�� do uwag dokumentu i przypisuje do admina
     * @param doc
     * @param assignable
     * @param message
     */
    public static void addRemarkAndAssignToAdmin(OfficeDocument doc, Assignable assignable, String message) {
        String adminName = "admin";
        try {
            adminName = GlobalPreferences.getAdminUsername();
            doc.addRemark(new Remark(message,"admin"));
        } catch (Exception e1) {
            log.error(e1.getMessage(), e1);
        }
        assignable.addCandidateUser(adminName);
    }

    public static void checkState(boolean expression, Object errorMessage) throws AssigningHandledException {
        if (!expression) {
            throw new AssigningHandledException(String.valueOf(errorMessage));
        }
    }

    /**
     * Wyszukuje na drzewie akceptacji akceptacj� o podanym 'acceptanceCn',
     * pobiera przypisanych do niej u�ytkownik�w lub dzia�y i dodaje je do przekazanych w parametrach list
     * @param users lista u�ytkownik�w, do kt�rych ma by� przypisane zadanie wg drzewa akceptacji
     * @param divisions lista dzia��w, do kt�rych ma by� przypisane zadanie wg drzewa akceptacji
     * @param acceptanceCn nazwa akceptacji na drzewie akceptacji
     * @throws EdmException
     */
    public static void getAssigneesFromAcceptanceTree(List<String> users, List<String> divisions, String acceptanceCn) throws EdmException {
        for (AcceptanceCondition accept : AcceptanceCondition.find(acceptanceCn))
        {
            if (accept.getUsername() != null && !users.contains(accept.getUsername()))
                users.add(accept.getUsername());
            if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
                divisions.add(accept.getDivisionGuid());
        }
    }
}
