package pl.compan.docusafe.core.jbpm4;

import org.apache.commons.net.ftp.*;
import java.io.*;
import java.net.InetAddress;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SendAttachementToFtpListaner implements ExternalActivityBehaviour
{

	private final static Logger LOG = LoggerFactory.getLogger(SendAttachementToFtpListaner.class);
	@Override
	public void execute(ActivityExecution openExecution) throws Exception
	{
		
		String ftpServer = Docusafe.getAdditionProperty("ocrFtpServer");
		String ftpLogin = Docusafe.getAdditionProperty("ocrFtpLogin");
		String ftpPass = Docusafe.getAdditionProperty("ocrFtpPassword");
		
		if (ftpServer == null)
			throw new EdmException("Brak parametru 'ocrFtpServer' w adds.properties");
		if (ftpLogin == null)
			throw new EdmException("Brak parametru 'ocrFtpLogin' w adds.properties");
		if (ftpPass == null)
			throw new EdmException("Brak parametru 'ocrFtpPassword' w adds.properties");
		
		Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		LOG.debug("OcrListener docId: {}, attachemtns.size: {}", doc.getId(), doc.getAttachments().size() );
		for (Attachment att : doc.getAttachments())
		{
			LOG.debug("OcrListener docId: {},  att.getRevisions().size: {}", doc.getId(),  att.getRevisions().size() );
			uploud(ftpServer, ftpLogin, ftpPass, att.getMostRecentRevision());
		}
		LOG.debug("Context :{}", DSApi.isContextOpen());
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		

	}

	private void uploud(String ftpServer, String ftpLogin, String ftpPass, AttachmentRevision revision) throws EdmException
	{
		//FTPClient client = new FTPClient();
		FTPSClient client = new FTPSClient();
		FileInputStream fis = null;

		try
		{
			client.connect(ftpServer);
			client.login(ftpLogin, ftpPass);
			client.enterRemotePassiveMode();
			client.enterLocalPassiveMode();
			LOG.debug("getPassivePort: {}", client.getPassivePort());
			LOG.debug("client.getPassiveHost(): {}", client.getPassiveHost());
			LOG.debug("DefaultPort: {}", client.getDefaultPort());
			LOG.debug("getLocalPort: {}", client.getLocalPort());
			LOG.debug("getRemotePort: {}", client.getRemotePort());
			// Store file to server
			client.changeWorkingDirectory("\\DO_OCR"); 
			client.setFileType(FTP.BINARY_FILE_TYPE);

			boolean hura = client.storeFile(revision.getOriginalFilename(), revision.getBinaryStream() /* revision.getAttachmentStream()*/);
			LOG.debug("HURR: {}", hura);
			client.logout();
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (fis != null)
				{
					fis.close();
				}
				client.disconnect();
			}
			catch (IOException e)
			{
				LOG.error(e.getMessage(), e);
			}
		}
	}

}
