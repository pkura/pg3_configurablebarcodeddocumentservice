package pl.compan.docusafe.core.jbpm4;

import com.google.common.base.Preconditions;
import org.jbpm.api.history.HistoryProcessInstance;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.dockinds.process.SingleStateProcessInstance;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.*;

/**
 * Opakowanie na API Jbpmowe.
 */
public class Jbpm4ProcessInstance implements SingleStateProcessInstance {
    private final static Logger log = LoggerFactory.getLogger(Jbpm4ProcessInstance.class);

    private org.jbpm.api.ProcessInstance jbpm4ProcessInstance;
    private final String processName;
    private final String processDefinitionId;
    private final String processInstanceId;

    public Jbpm4ProcessInstance(String processName, org.jbpm.api.ProcessInstance jbpm4ProcessInstance) {
        this.jbpm4ProcessInstance = jbpm4ProcessInstance;
        this.processDefinitionId = jbpm4ProcessInstance.getProcessDefinitionId();
        this.processName = processName;
        this.processInstanceId = jbpm4ProcessInstance.getId();
    }

    public Jbpm4ProcessInstance(String processName, HistoryProcessInstance jbpm4ProcessInstance) {
        this.jbpm4ProcessInstance = null;
        this.processDefinitionId = jbpm4ProcessInstance.getProcessDefinitionId();
        this.processName = processName;
        this.processInstanceId = jbpm4ProcessInstance.getProcessInstanceId();
    }

    /**
     * Zwraca nazw� stanu (Activity w jbpm),
     * w kt�rym znajduje si� proces, w przypadku zako�czenia procesu zwraca null,
     * je�li proces ma kilka stan�w wyrzuca wyj�tek (IllegalStateException)
     * @return
     */
    public String getSingleState(){
        if(jbpm4ProcessInstance == null) {
            return null;
        }
        jbpm4ProcessInstance = Jbpm4Provider.getInstance().getExecutionService().findProcessInstanceById(jbpm4ProcessInstance.getId());
        if(jbpm4ProcessInstance == null || jbpm4ProcessInstance.isEnded()){
            return null;
        }
        Set<String> an = jbpm4ProcessInstance.findActiveActivityNames();
        if(an.isEmpty()){
            return null;
        } else if (an.size() > 1) {
            throw new MultipleExecutionsException("wiele aktywno�ci (stan�w) w procesie");
        } else { //ma dok�adnie jeden element
            return an.iterator().next();
        }
    }

    public void signal(String signal){
        Preconditions.checkNotNull(signal, "signal cannot be null");
        Preconditions.checkState(!isEnded(), "process has ended");
        jbpm4ProcessInstance = Jbpm4Provider.getInstance()
                .getExecutionService()
                .signalExecutionById(getId(),signal);
    }

    public String getProcessName() {
        return processName; 
    }

    public String getId() {
        return processInstanceId;
    }

    public String getProcessDefinitionId(){
        return processDefinitionId;
    }
    public String getProcessInstanceId(){
	return processInstanceId;
    }

    public boolean isEnded(){
        return jbpm4ProcessInstance == null || jbpm4ProcessInstance.isEnded();
    }
}
