package pl.compan.docusafe.core.jbpm4;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.ocr.OcrUtils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.AbstractParametrization;
import pl.compan.docusafe.service.tasklist.RefreshTaskList;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Maps;

public class GetOcrAttachementFromDisk implements ExternalActivityBehaviour
{

	private final static Logger LOG = LoggerFactory.getLogger(GetOcrAttachementFromDisk.class);
	
	@Override
	public void execute(ActivityExecution exec) throws Exception
	{
		OfficeDocument doc = null;
		String ocrGet = Docusafe.getAdditionProperty("ocr-get");
		
		if (ocrGet == null)
			throw new EdmException("Brak parametru 'ocr-get' w adds.properties");
		
		boolean isOpened = true;
		if (!DSApi.isContextOpen())
		{
			isOpened = false;
			DSApi.openAdmin();
		}
		
		Long docId = Long.valueOf(exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		LOG.info("Get ocr attachmetn for  documentId: {}", docId);

		doc = OfficeDocument.find(docId);

		boolean sended = false;
		if (doc.getAttachments().size() == 0)
			sended = true;
		for (Attachment att : doc.getAttachments())
		{
			sended = download(ocrGet, att.getMostRecentRevision(), docId,doc);
			break; // tylko jeden
		}
		
		if (DSApi.isContextOpen() && !isOpened)
			DSApi.close();
		
		if (sended)
			exec.take("success");
		else
			exec.take("repeat");
		
	}
	
	private boolean download(String fromOcrFolder, AttachmentRevision attRev, Long docId,OfficeDocument document)
	{
		String oldType = OcrUtils.fileExtension(attRev.getOriginalFilename());
		if(!SaveAttachmentOnDiskListener.ocrExtension.contains(oldType.toLowerCase()))
		{
			LOG.warn("DocId: " + docId + ": " + oldType + " nie jest wspierane przez ocr");
			return true;
		}
		try
		{
			File temp = new File(fromOcrFolder + attRev.getFileName().replace(".DAT", ".xml"));
			createDoc(temp, docId,document);
		}
		catch (Exception e)
		{
			LOG.error("DocId: " + docId + ": " + e.getMessage(), e);
			return false;
		}
		return true;
	}
	
	private void createDoc(File file, Long docId, OfficeDocument document) throws IOException, DocumentException, ParseException, EdmException
	{
		
		Map<String, Object> toReload = Maps.newHashMap();
		
		SAXReader reader = new SAXReader();
		LOG.info("Start parsing xml file: {} for documentId: {}", file.getName(), docId);
		Document doc = reader.read(file);
		DSApi.context().begin();
		LOG.info("Start fill docId: {}", docId);
		AbstractParametrization.getInstance().createDocFromOcr(doc, toReload, docId);
		
		// przeladowanie wartosci
		LOG.info("Start setOnly docId: {}", docId);
		document.getDocumentKind().setOnly(docId, toReload);
		DSApi.context().commit();
		LOG.info("Start addToRefresh docId: {}", docId);
		// dodanie id dokumentu to poznijeszcego oswiezenia listy zadan dla niego przez serwis
		RefreshTaskList.addDocumentToRefresh(docId);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub
		
	}

}
