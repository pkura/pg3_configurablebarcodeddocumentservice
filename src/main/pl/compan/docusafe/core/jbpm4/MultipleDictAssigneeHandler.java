package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class MultipleDictAssigneeHandler implements AssignmentHandler {

    private String taskAccVar;

    private static final Logger log = LoggerFactory.getLogger(MultipleDictAssigneeHandler.class);

    @Override
    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {

        boolean isOpened  = false;

        try {
            isOpened = DSApi.openContextIfNeeded();

            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId,false);

            Long userId = (Long) openExecution.getVariable(taskAccVar);
            boolean assigned = AssigneeHandlerUtils.assignToUserWhileFork(doc, assignable, openExecution, userId);

            if (!assigned)
                throw new Exception("");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (isOpened) {
                DSApi.closeContextIfNeeded(isOpened);
            }
        }

    }
}
