package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: jtarasiuk
 * Date: 21.06.13
 * Time: 12:05
 * To change this template use File | Settings | File Templates.
 */
public class OfficeNumberDecision  implements DecisionHandler
{
    private final static Logger LOG = LoggerFactory.getLogger(OfficeNumberDecision.class);
    public static final String IS_SET = "is-set";
    public static final String IS_NOT_SET = "is-not-set";

    @Override
    /**
     * zwaraca 'is-set' w przypadku kiedy:
     * - dokument posiada numer ko
     * - dokument nie ma wlasciowosci 'setOfficeNumberFromArchiveTab' zezwalajacej na nadawanie numeru ko z poziomu zakladki archiwizacja, za pomoca przycisku procesu
     *   wlasciwosc ma byc zapisana w dockindzie od danego dokumentu w sekcji <availableProperties>
     * zwaraca 'is-not-set' w innym przypadku
     */
    public String decide(OpenExecution openExecution)
    {
        try
        {
            OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
            if (doc.getOfficeNumber() != null || !AvailabilityManager.isAvailable(AvailabilityManager.SET_OFFICE_NUMBER_FROM_ARCHIVE_TAB, doc.getDocumentKind().getCn()))
            {
                return IS_SET;
            }
        }
        catch (Exception e)
        {
            LOG.error(e.getMessage(), e);
        }

        return IS_NOT_SET;
    }
}
