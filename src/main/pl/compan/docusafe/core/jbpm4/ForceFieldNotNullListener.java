package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ForceFieldNotNullListener extends AbstractEventListener{
    private final static Logger LOG = LoggerFactory.getLogger(ForceFieldNotNullListener.class);

    String field;

    public void notify(EventListenerExecution eventListenerExecution) throws Exception {
        Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        if(null == doc.getFieldsManager().getValue(field)){
            throw new IllegalStateException("Brak wype�nionego pola - " + doc.getFieldsManager().getField(field).getName());
        }
    }
}