package pl.compan.docusafe.core.jbpm4;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class Jbpm4Constants {

    public final static String DOCUMENT_ID_PROPERTY = "doc-id";
    public final static String ACTIVITY_ID_PROPERTY = "ds-activity-id";
    public final static String EXECUTION_ID_PROPERTY = "ds-execution-id";
    public final static String PARENT_EXECUTION_ID_PROPERTY = "parrent-ds-execution-id";
    /**
     * Boolean m�wi�cy czy pismo, kt�rego dotyczy proces zosta�o przedekretowane na kogo� innego
     * (konieczno�� powrotu na list� zada�)
     */
    public final static String REASSIGNMENT_PROPERTY = "reassigned";

    public final static String MANUAL_PROCESS_NAME = "manual";
    public final static String KEY_PROCESS_NAME = "PROCESS_NAME";

    public final static String READ_ONLY_ASSIGNEE_PROPERTY = "assignee";
    public final static String READ_ONLY_PROCESS_NAME = "read-only";
    public final static String READ_ONLY_ASSIGNMENT_USERNAMES = "assignee-usernames";
    public final static String READ_ONLY_ASSIGNMENT_GUIDS = "assignee-guid";
    
    public final static String ZAMOWIENIE = "Zamowienie";
    public final static String OFERTA = "Oferta";
    
    public final static Integer CHANGE_REASON_CORRECTION = 1;
    public final static Integer CHANGE_REASON_FINISH = 2;
    public final static Integer CHANGE_REASON_NONE = 0;

    public final static String KEY_FORK_PUSH = "KEY_FORK_PUSH";
    public final static String FORK = "FORK";
    public final static String PUSH = "PUSH";
}
