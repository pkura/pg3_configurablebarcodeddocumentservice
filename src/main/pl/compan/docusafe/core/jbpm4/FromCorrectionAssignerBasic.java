package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Map;


@SuppressWarnings("serial")
public class FromCorrectionAssignerBasic implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(CorrectionAssigner.class);
    private static final String CORRECTION_FROM_USER = "correctionFromUser";
    private static final String CORRECTION_FROM_STATE = "correctionFromState";
    @Override
	public void execute(ActivityExecution execution) throws Exception
	{
		String state = null;
		state = (String) execution.getVariable(CORRECTION_FROM_STATE);

        execution.setVariable("user",  execution.getVariable(CORRECTION_FROM_USER));
		execution.take(state);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{

	}

}
