package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;

public class FinishReadOnlyListener extends AbstractEventListener
{

	@Override
	public void notify(EventListenerExecution execution) throws Exception
	{
		OfficeDocument doc = Jbpm4Utils.getDocument(execution);
		doc.addAssignmentHistoryEntry(
                new AssignmentHistoryEntry(DSApi.context().getPrincipalName(),
                        execution.getProcessDefinitionId(), AssignmentHistoryEntry.PROCESS_TYPE_CC, AssignmentHistoryEntry.EntryType.JBPM4_CONFIRMED)
            );
	}

}
