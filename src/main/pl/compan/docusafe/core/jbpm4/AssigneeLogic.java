package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * User: kk
 * Date: 21.01.14
 * Time: 11:41
 */
public interface AssigneeLogic {
    void assign(OfficeDocument doc, FieldsManager fm, Assignable assignable, OpenExecution openExecution) throws EdmException;
}
