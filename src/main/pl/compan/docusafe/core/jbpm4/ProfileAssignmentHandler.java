package pl.compan.docusafe.core.jbpm4;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import org.jfree.util.Log;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.users.Profile;

import java.util.Date;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ProfileAssignmentHandler implements AssignmentHandler {

    /**
     * Nazwa adds-a, kt�ry okre�la id profilu
     */
    public String profileIdAdds;
    /**
     * Kod profilu, kt�ry musi zosta� przet�umaczony w logice implementuj�cej interfejs {@link pl.compan.docusafe.core.jbpm4.AssigneeHandler.ProfileCodeTranslator} na nazw� profilu
     */
    public String profileCode;
    /**
     * NIEZALECANE
     */
    public String profileName;

    @Override
    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
        DSContextOpener contextOpener = DSContextOpener.openAsAdminIfNeed();
        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);

            Profile profile;
            if (StringUtils.isNotBlank(profileIdAdds)){
                int profileId = Docusafe.getIntAdditionPropertyOrDefault(profileIdAdds, -1);
                profile = Profile.findById((long)profileId);
            } else {
                if (StringUtils.isNotBlank(profileCode)){
                    DocumentLogic logic = doc.getDocumentKind().logic();
                    if (logic instanceof AssigneeHandler.ProfileCodeTranslator){
                        profileName = ((AssigneeHandler.ProfileCodeTranslator) logic).translateProfilesCode(profileCode);
                    }
                    else
                        throw new Exception("Logic not implemented " + AssigneeHandler.ProfileCodeTranslator.class.getName());
                }

                profile = Profile.findByProfilename(profileName);
            }
            if (profile == null) throw new Exception("Not found profile");

            openExecution.setVariable(Jbpm4WorkflowFactory.REASSIGNMENT_TIME_VAR_NAME, new Date());
            boolean assigned = AssigneeHandlerUtils.assignByProfile(doc, assignable, openExecution, profile);

            if (!assigned)
                throw new Exception("Not assigned");

        } catch (EdmException e) {
            Log.error(e.getMessage());
            throw e;
        } finally {
            contextOpener.closeIfNeed();
        }
    }

    public String getProfileIdAdds() {
        return profileIdAdds;
    }

    public String getProfileCode() {
        return profileCode;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileIdAdds(String profileIdAdds) {
        this.profileIdAdds = profileIdAdds;
    }

    public void setProfileCode(String profileCode) {
        this.profileCode = profileCode;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }
}
