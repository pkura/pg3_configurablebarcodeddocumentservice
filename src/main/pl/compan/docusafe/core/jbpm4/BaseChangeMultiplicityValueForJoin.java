package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.util.Map;

/**
 * Jedyna i wlasciwy mechanizmy zmieniacy wartosc count dla forkow w przypadku przekazania do porpawy/odrzucenia dokuemntu w 'forku'
 * @author jtarasiuk
 *
 */
public class BaseChangeMultiplicityValueForJoin implements ExternalActivityBehaviour
{

	private static final Logger log = LoggerFactory.getLogger(BaseChangeMultiplicityValueForJoin.class);

	public void execute(ActivityExecution execution) throws Exception
	{
		execution.setVariable("correction", 11);
		Long documnetId = Jbpm4Utils.getDocumentId(execution);
		String acceptanceName = getAcceptanceName(execution);
		int countAcceptances = DocumentAcceptance.find(documnetId, acceptanceName).size();
		execution.setVariable("count", ++countAcceptances);
		log.info("Zamykamy wszystkie subprocesy utworzone w fork'u i ustawiamy warto�� 'correction' na 11");
	}

	private String getAcceptanceName(ActivityExecution execution) throws EdmException
	{
		NativeCriteria nc = DSApi.context().createNativeCriteria("JBPM4_EXECUTION", "d");
		nc.setProjection(NativeExps.projection()
		// ustalenie kolumn
				.addProjection("d.name_")).add(NativeExps.like("d.ID_", execution.getId() + ".%"));
		// pobranie i wy�wietlenie wynik�w
		CriteriaResult cr = nc.criteriaResult();
		while (cr.next())
		{
			return cr.getString(0, null);
		}
		return null;
	}

	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception
	{
		execution.takeDefaultTransition();
	}
}
