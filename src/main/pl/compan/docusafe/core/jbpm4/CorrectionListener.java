package pl.compan.docusafe.core.jbpm4;

import java.util.Date;
import org.jbpm.api.listener.EventListenerExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

public class CorrectionListener extends AbstractEventListener
{
	private static final Logger log = LoggerFactory.getLogger(CorrectionListener.class);

	public void notify(EventListenerExecution execution) throws Exception
	{

		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);

		// Przekazania do poprawy - infomracja dla użytkownika
		AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
		ahe.setSourceUser(DSApi.context().getPrincipalName());
		ahe.setProcessName(execution.getProcessDefinitionId());
		ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_CORRECTION);

		String divisions = "";
		for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
		{
			if (!divisions.equals(""))
				divisions = divisions + " / " + a.getName();
			else
				divisions = a.getName();
		}
		ahe.setSourceGuid(divisions);
		ahe.setStatus(doc.getFieldsManager().getValue("STATUS").toString());

		ahe.setCtime(new Date());

		ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(ahe);
		if (AvailabilityManager.isAvailable("addToWatch"))
			DSApi.context().watch(URN.create(Document.find(docId)));
	}
}
