package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jfree.util.Log;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class AssigneeHandlerUtils {


    public static boolean assignToUserWhileFork(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Long userId) {
        try {
            DSUser user = DSUser.findById(userId);
            return assignToUser(doc, assignable, openExecution, user);
        } catch (EdmException e) {
            Log.error(e.getMessage(), e);
            return false;
        }
    }

    public static boolean assignToDivisionWhileFork(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Long divId) {
        try {
            DSDivision division = DSDivision.findById(divId);
            return assignToDivision(doc, assignable, openExecution, division);
        } catch (EdmException e) {
            Log.error(e.getMessage(), e);
            return false;
        }
    }

    public static boolean assignToDivision(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, DSDivision division) {
        return assignToDivision(doc, assignable, openExecution, division.getGuid());
    }

    public static boolean assignToDivision(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String divisionGuid) {
        try {
            assignable.addCandidateGroup(divisionGuid);
            AssigneeHandler.addToHistory(openExecution, doc, null, divisionGuid);
            return true;
        } catch (EdmException e) {
            Log.error(e.getMessage(), e);
            return false;
        }
    }

    public static boolean assignToUser(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, DSUser user) {
        return assignToUser(doc, assignable, openExecution, user.getName());
    }

    public static boolean assignToUser(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String username) {
        try {
            assignable.addCandidateUser(username);
            AssigneeHandler.addToHistory(openExecution, doc, username, null);
            return true;
        } catch (EdmException e) {
            Log.error(e.getMessage(), e);
            return false;
        }
    }




    public static boolean assignByPermissionSet(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String[] permissionNames) throws EdmException {
        boolean assigned = false;

        for (String permission : permissionNames) {
            assigned = assignByPermission(doc, assignable, openExecution, permission) || assigned;
        }

        return assigned;
    }

    public static boolean assignByPermission(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String permissionName) {
        boolean assigned = false;

        try {
            DSPermission permission = DSPermission.getByName(permissionName);

            Set<Role> rolesToPermission = new HashSet<Role>();
            Role[] allRoles = Role.list();
            for (Role role : allRoles) {
                Set<String> permissionToRole = role.getPermissions();
                if (permissionToRole.contains(permission.getName())) {
                    rolesToPermission.add(role);
                }
            }

            assigned = assignByRoleSet(doc, assignable, openExecution, rolesToPermission);

        } catch (EdmException e) {
            Log.error(e.getMessage(), e);
        }

        return assigned;
    }

    public static boolean assignByProfileSet(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String[] profilesNames) throws EdmException {
        boolean assigned = false;

        for (String profile : profilesNames)
            assigned = assignByProfile(doc, assignable, openExecution, profile) || assigned;

        return assigned;
    }

    public static boolean assignByProfile(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String profileName) {
        boolean assigned = false;
        try {
            Profile profile = Profile.findByProfilename(profileName);
            assigned = assignByProfile(doc, assignable, openExecution, profile);
        } catch (EdmException e) {
            Log.error(e.getMessage(), e);
        }
        return assigned;
    }

    public static boolean assignByProfile(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Profile profile) {
        boolean assigned = false;

        try {

            String[] divisionGuids = profile.getGuids();
            Set<DSUser> users = profile.getUsers();
            Set<Role> roles = profile.getOfficeRoles();

            assigned = assignByDivisionGuids(doc, assignable, openExecution, divisionGuids) || assigned;
            assigned = assignByUsers(doc, assignable, openExecution, users) || assigned;
            assigned = assignByRoleSet(doc, assignable, openExecution, roles) || assigned;

        } catch (EdmException e) {
            Log.error(e.getMessage(), e);
        }

        return assigned;
    }

    public static boolean assignByRoleSet(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Collection<Role> roles) throws EdmException {
        boolean assigned = false;

        for (Role role : roles)
            assigned = assignByRole(doc, assignable, openExecution, role) || assigned;

        return assigned;
    }

    public static boolean assignByRole(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Role role) {
        boolean assigned = false;

        Set<String> usernames = role.getUsernames();
        Set<String> divisions = role.getDivisions(); // todo - is it divisions' names or guids?

        assigned = assignByDivisionGuids(doc, assignable, openExecution, divisions) || assigned;
        assigned = assignByUsernames(doc, assignable, openExecution, usernames) || assigned;

        return assigned;
    }

    public static boolean assignByRoleNameSet(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String[] roleNames) throws EdmException {
        boolean assigned = false;

        for (String role : roleNames)
            assigned = assignByRoleName(doc, assignable, openExecution, role) || assigned;

        return assigned;
    }

    public static boolean assignByRoleName(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String roleName) {
        try {
            Role role = Role.findByName(roleName);
            return assignByRole(doc, assignable, openExecution, role);

        } catch (EdmException e) {
            Log.error(e.getMessage(), e);
            return false;
        }
    }

    public static boolean assignByUsernames(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Collection<String> usernames) {
        return assignByUsernames(doc, assignable, openExecution, usernames.toArray(new String[usernames.size()]));
    }

    public static boolean assignByUsernames(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String... usernames) {
        if (usernames.length == 0)
            return false;
        try {
            for (String name : usernames) {
                assignable.addCandidateUser(name);
                AssigneeHandler.addToHistory(openExecution, doc, name, null);
            }
            return true;
        } catch (EdmException e) {
            Log.error(e.getMessage(), e);
            return false;
        }
    }

    public static boolean assignByDivisionGuids(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Collection<String> divisionGuids) {
        return assignByDivisionGuids(doc, assignable, openExecution, divisionGuids.toArray(new String[divisionGuids.size()]));
    }

    public static boolean assignByDivisionGuids(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String... divisionGuids) {
        if (divisionGuids.length == 0)
            return false;
        try {
            for (String guid : divisionGuids) {
                assignable.addCandidateGroup(guid);
                AssigneeHandler.addToHistory(openExecution, doc, null, guid);
            }
            return true;
        } catch (EdmException e) {
            Log.error(e.getMessage(), e);
            return false;
        }
    }

    public static boolean assignByUsers(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Collection<DSUser> users) {
        Set<String> usernamesArray = new HashSet<String>(users.size());
        for (DSUser user : users)
            usernamesArray.add(user.getName());
        return assignByUsernames(doc, assignable, openExecution, usernamesArray);
    }
}
