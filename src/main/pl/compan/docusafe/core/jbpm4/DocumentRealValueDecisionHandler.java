package pl.compan.docusafe.core.jbpm4;

import com.google.common.base.Splitter;
import org.apache.commons.lang.ObjectUtils;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * 
 * @author KK
 *
 */
public class DocumentRealValueDecisionHandler implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(DocumentRealValueDecisionHandler.class);

    private String field;
    private String checkedValue;

    public String decide(OpenExecution openExecution) {
        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

            boolean isOpened = false;

            try {
                if (!DSApi.isContextOpen()) {
                    DSApi.openAdmin();
                    isOpened = true;
                }

                OfficeDocument doc = OfficeDocument.find(docId);
                FieldsManager fm = doc.getFieldsManager();

                if (checkedValue == null) {
                    throw new IllegalArgumentException("Brak parametru checkedValue");
                }

                Iterable<String> values = Splitter.on('|').split(checkedValue);

                LOG.info("documentid = " + docId + " field = " + field);
                for (String val : values) {
                    if (ObjectUtils.defaultIfNull(fm.getKey(field), "").toString().equals(val)) {
                        return "true";
                    }
                }

                return "false";

            } finally {
                if (isOpened && DSApi.isContextOpen()) {
                    DSApi.close();
                }
            }

        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
    }
}

