package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class IsEmptyFieldDecisionHandler implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(IsEmptyFieldDecisionHandler.class);

    private String isEmptyField;

    public String decide(OpenExecution openExecution) {
        try{
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            LOG.info("documentid = " + docId + " isEmptyField = " + isEmptyField);
            Object cn = fm.getKey(isEmptyField);
            if(cn == null) 
            {
                return "empty";
            }
            else
            {
            	return "notEmpty";
            }
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
    }
}
