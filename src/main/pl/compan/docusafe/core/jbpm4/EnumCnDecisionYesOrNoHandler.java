package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class EnumCnDecisionYesOrNoHandler implements DecisionHandler {
	
	private final static Logger LOG = LoggerFactory.getLogger(EnumCnDecisionYesOrNoHandler.class);

    private String field;

	@Override
	public String decide(OpenExecution openExecution) {
		try{
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            LOG.info("documentid = " + docId + " field = " + field);
            String cn = fm.getEnumItemCn(field);
            if(cn == null) {
                return "nie";
            }
            return cn.toLowerCase();
        } catch (EdmException ex) {
            return "error";
        }
	}
}
