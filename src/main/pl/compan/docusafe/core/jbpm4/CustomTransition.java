package pl.compan.docusafe.core.jbpm4;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

public class CustomTransition implements ExternalActivityBehaviour 
{
	
	public void execute(ActivityExecution execution) throws Exception {}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {} 

}
