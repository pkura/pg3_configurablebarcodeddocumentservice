/**
 * 
 */
package pl.compan.docusafe.core.jbpm4;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.ManualMultiAssignmentTabAction;

/**Klasa dekretuje dokument do uzytkownika z pola dataBaseEnumField
 * 
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 09-04-2014
 * ds_trunk_2014
 * DataBaseEnumFieldAssigneHandler.java
 */
public class DataBaseEnumFieldAssigneHandler implements AssignmentHandler{

	/**
	 * CN pola dataBaseUnumField
	 */
	private String dataBaseEnumField;
	
	/**
	 * Mozliwe wartosci: id, cn, title, refValue
	 */
	private String assigneBy;
	
	private final static String ID = "id";
	private final static String CN = "cn";
	private final static String TITLE = "title";
	private final static String REFVALUE = "refValue";
	
	private static final Logger log = LoggerFactory.getLogger(DataBaseEnumFieldAssigneHandler.class);
	private final static StringManager SM = GlobalPreferences.loadPropertiesFile(DataBaseEnumFieldAssigneHandler.class.getPackage().getName(), null);
	
	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		boolean isOpened = false;
		boolean assigned = false;
		try {
			isOpened = DSApi.openContextIfNeeded();
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId,false);
			
			if(StringUtils.isNotBlank(dataBaseEnumField) && StringUtils.isNotBlank(assigneBy)){
				
				FieldsManager fm = doc.getFieldsManager();
				EnumItem dataBaseField = fm.getEnumItem(dataBaseEnumField);

				if (assigneBy.equals(ID)) {
					try {
						DSUser user = DSUser.findById(dataBaseField.getId().longValue());
						assignable.addCandidateUser(user.getName());
						AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
						assigned = true;
					} catch (Exception e) {
						log.error(e.getMessage(), e);
						// TODO
					}
				} else if (assigneBy.equals(CN)) {
					try {
						DSUser user = DSUser.findByUsername(dataBaseField.getCn());
						assignable.addCandidateUser(user.getName());
						AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
						assigned = true;
					} catch (Exception e) {
						log.error(e.getMessage(), e);
						// TODO
					}
				} else if (assigneBy.equals(TITLE)) {
					try {
						String[] split = dataBaseField.getTitle().split(" ");
						if (split != null && split.length >= 2) {
							String firstName = split[0].trim();
							String lastName = split[1].trim();
							List<DSUser> user = UserImpl.findUsersByFirstnameLastname(firstName, lastName);
							if (user != null) {
								for (DSUser dsUser : user) {
									assignable.addCandidateUser(dsUser.getName());
									AssigneeHandler.addToHistory(openExecution, doc, dsUser.getName(), null);
									assigned = true;	
								}
							}
						} else {
							throw new EdmException("Nie uda�o si� przypisa� osoby");
						}
					} catch (Exception e) {
						log.error(e.getMessage(), e);
						// TODO
					}
				} else if (assigneBy.equals(REFVALUE)) {
					try {
						String userName = dataBaseField.getRefValue();
						if (StringUtils.isNotBlank(userName)) {
							DSUser user = DSUser.findByUsername(userName);
							assignable.addCandidateUser(user.getName());
							AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
							assigned = true;	
						} else {
							throw new EdmException("Nie uda�o si� przypisa� osoby");
						}
					} catch (UserNotFoundException e) {
						log.error(e.getMessage(), e);
						// TODO
					} catch (Exception e) {
						log.error(e.getMessage(), e);
						// TODO
					}
				} 
			}
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			if (!assigned) {
				throw new EdmException("NieUdalosieUstalicDoKogoMaBycZadekretowanePismo");
			}
		}

		
	}

}
