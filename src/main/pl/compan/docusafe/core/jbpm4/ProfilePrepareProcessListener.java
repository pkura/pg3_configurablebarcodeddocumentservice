package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jfree.util.Log;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.util.TextUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <b>UWAGA! Nale�y przeciazyc metode getProfileIdAdds lub getProfileId</b><br></br>
 *
 * Do przypisania procesu-zadania do u�ytkownik�w mo�na wykorzysta� {@code ProfileForkAssignmentHandler}
 *
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class ProfilePrepareProcessListener implements ExternalActivityBehaviour {

    private static final String LINKER = "::";

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        final List<String> argSets = new LinkedList<String>();

        long profileId = getProfileId();
        addProfile(argSets, profileId);

        execution.setVariable("ids", argSets);
        execution.setVariable("count", argSets.size());
        execution.setVariable("count2", 2);
    }

    /** Metoda wykorzystywana w getProfileId. Implementacja w klasie pochodnej nie jest konieczna w przypadku przeciazania getProfileId.
     * @return nazwa adds-a z ktorego pobrany zostanie id profilu
     * @throws Exception "Not implemented method"
     */
    protected String getProfileIdAdds() throws Exception {
        throw new Exception("Not implemented method");
    }

    /** Metoda korzysta z getProfileIdAdds, ktora nalezy przeciazyc.
     * @return id profilu uzyskanego na podstawie nazwy adds-a pobieranego w getProfileIdAdds
     * @throws Exception
     */
    private long getProfileId() throws Exception {
        String adds = getProfileIdAdds();
        Integer id = Docusafe.getIntAdditionPropertyOrDefault(adds,-1);
        if (id < 0)
            throw new Exception("Profile has not been found by adds");
        return (long)id;
    }

    public enum AssigneeType{DIVISION_GUID, USER_NAME;}

    private static void addProfile(List<String> argSets, long profileName) {

        try {
            Profile profile = Profile.findById(profileName);

            String[] divisionGuids = profile.getGuids();
            Set<DSUser> users = profile.getUsers();
//            Set<Role> roles = profile.getOfficeRoles();

            for (String divisionGuid : divisionGuids)
                argSets.add(AssigneeType.DIVISION_GUID + LINKER + divisionGuid);
            for (DSUser user : users)
                argSets.add(AssigneeType.USER_NAME + LINKER + user.getName());
//            for (String divisionGuid : divisionGuids)
//                argSets.add(ROLE + divisionGuid);

        } catch (EdmException e) {
            Log.error(e.getMessage(), e);
        }
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception { }


    public static AssigneeType getAssigneeType (String arg){
        String assigneeTypeName = TextUtils.getTextToSign(arg, LINKER);
        return AssigneeType.valueOf(assigneeTypeName);
    }
    public static String getAssigneeValue (String arg){
        String value = TextUtils.getTextFromSign(arg, LINKER, 1);
        return value;
    }
}
