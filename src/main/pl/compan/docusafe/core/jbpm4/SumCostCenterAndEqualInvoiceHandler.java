package pl.compan.docusafe.core.jbpm4;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class SumCostCenterAndEqualInvoiceHandler implements DecisionHandler
{
	private final static Logger log = LoggerFactory.getLogger(SumCostCenterAndEqualInvoiceHandler.class);

	public String decide(OpenExecution openExecution)
	{

		try
		{
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			Document document = Document.find(docId);
			Float kwotaZamowienia = null;

			Long idZamownienia = new Long((String) document.getFieldsManager().getValue("BECK_ZAMOWIENIE"));
			System.out.println("Id zamowienia do ktorego dodamy id umowy: " + idZamownienia);

			Statement stat = DSApi.context().createStatement();
			ResultSet result = stat.executeQuery("SELECT KWOTA_NETTO FROM dsg_beck_zamowie WHERE DOCUMENT_ID = " + idZamownienia);
			if (result.next())
			{
				kwotaZamowienia = new Float(result.getString(1));
			}
			else
			{
				System.out.println("Brak wynikow");
			}
			DSApi.context().closeStatement(stat);

			float kwotaFaktury = new Float(((String) document.getFieldsManager().getValue("KWOTA_NETTO")).replace(" ", ""));

			System.out.println("Kwota netto zamowienia to: " + kwotaZamowienia + " kwota faktury: " + kwotaFaktury);

			if (kwotaFaktury == kwotaZamowienia)
			{
				return "equal";
			}
			else
			{
				return "notEqual";

			}
		}
		catch (EdmException ex)
		{
			log.error(ex.getMessage(), ex);
			return "error";
		}
		catch (SQLException e)
		{
			log.error(e.getMessage(), e);
			return "error";
		}
	}
}
