package pl.compan.docusafe.core.jbpm4;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Przekazuje do logiki zmienn� inn� ni� acceptation
 *
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class AbsSimpleAcceptanceListener extends AbstractEventListener {

    private static final Logger log = LoggerFactory.getLogger(AbsSimpleAcceptanceListener.class);

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        OfficeDocument doc = getOfficeDocument(execution);
        executeTask(execution, doc);
    }

    protected void executeTask(EventListenerExecution execution, OfficeDocument doc) throws EdmException {
        if (!isDoNotSaveAcceptance())
            saveAcceptance(execution, doc, getAcception());

        if (!isDoNotUpdateAssignmentHistory())
            AcceptancesListener.updateAssignmentHistory(execution, doc, getAcception(), null);

        if (!isDoNotInformLogic())
            doc.getDocumentKind().logic().onAcceptancesListener(doc, getSimpleAcception());

        if (StringUtils.isNotEmpty(getClearVariables())) {
            String[] vars = getClearVariables().split(",");

            for (String var : vars) {
                if (var != null && !var.equals("doc-id"))
                    execution.removeVariable(var);
            }
        }
    }

    public static OfficeDocument getOfficeDocument(EventListenerExecution execution) throws EdmException {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        return doc;
    }

    public static void saveAcceptance(EventListenerExecution execution, OfficeDocument doc, String acception) {
        try {
            DSApi.context().session().flush();
            DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), acception, null);
            DSApi.context().session().flush();
        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        }
    }

    public abstract String getAcception();

    public abstract void setAcception(String acception);

    public abstract String getSimpleAcception();

    public abstract void setSimpleAcception(String simpleAcception);

    public abstract String getDoNotUpdateAssignmentHistory();

    public abstract void setDoNotUpdateAssignmentHistory(String doNotUpdateAssignmentHistory);

    public abstract boolean isDoNotUpdateAssignmentHistory ();

    public abstract String getType();

    public abstract void setType(String type);

    public abstract String getDoNotInformLogic();

    public abstract void setDoNotInformLogic(String doNotInformLogic);

    public abstract boolean isDoNotInformLogic();


    public abstract String getDoNotSaveAcceptance();

    public abstract void setDoNotSaveAcceptance(String doNotSaveAcceptance);

    public abstract boolean isDoNotSaveAcceptance();


    public abstract String getClearVariables();

    public abstract void setClearVariables(String clearVariables);
}
