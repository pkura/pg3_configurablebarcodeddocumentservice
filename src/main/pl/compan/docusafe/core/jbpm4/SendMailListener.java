package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;
import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.TaskListMarkerManager;
import pl.compan.docusafe.core.users.*;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;

import java.io.IOException;
import java.util.*;

public class SendMailListener extends AbstractEventListener
{
	private static final Logger log = LoggerFactory.getLogger(SendMailListener.class);

	private String user;
	private String userField;
	private String guid;
	private String acceptationCn;
	private String fieldCnValue;
    /** cn przekazywany do logiki w dokumencie aby wyci�gn�� maila z jakiego� okreslonego innego miejsca*/
    private String mailFromLogicCn;
	private String mail;
	private String template;
	private String realization_time_m;
	private String realization_time_h;
	private String realization_time_d;
	private String realization_time_M;
	private String realization_time_y;

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        boolean isOpened = true;
        Long docId = Jbpm4Utils.getDocumentId(execution);
        if (!AvailabilityManager.isAvailable("send.mail.listener"))
            return;
        try {
            if (!DSApi.isContextOpen()) {
                isOpened = false;
                DSApi.openAdmin();
            }
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            boolean checkPermissions = AvailabilityManager.isAvailable("send.mail.listener.permissions.check");
            if ("zapotrzeb_ifpan".equals(doc.getDocumentKind()) && "dpzie".equalsIgnoreCase(fm.getEnumItem("STATUS").getCn()) && fm.getBoolean("PRZETARG")) {
                return;
            }
            if (AvailabilityManager.isAvailable("eservice")) {
                Date dataPlatnosci = (Date) fm.getKey("DATA_PLATNOSCI");
                Date today = new Date();
                DateTime dt1 = new DateTime(dataPlatnosci);
                DateTime dt2 = new DateTime(today);

                log.error(Days.daysBetween(dt2, dt1).getDays() + " days");
                log.error(Hours.hoursBetween(dt2, dt1).getHours() % 24 + " hours");
                log.error(Minutes.minutesBetween(dt2, dt1).getMinutes() % 60 + " minutes");

                int daysBetween = Days.daysBetween(dt2, dt1).getDays();

                if (daysBetween > 3)
                    return;
            }

            // wyr�nienie op�nionych zada� na li�cie zada�
            if (AvailabilityManager.isAvailable("task.alert")) {
                List<JBPMTaskSnapshot> taskSnapshots = JBPMTaskSnapshot.findByDocumentId(docId);
                DSApi.context().begin();
                for (JBPMTaskSnapshot snapshot : taskSnapshots) {
                    snapshot.setMarkerClass(TaskListMarkerManager.PREFIX + "Alert");
                    DSApi.context().session().update(snapshot);
                }
                DSApi.context().commit();
            }

            if (template == null) {
                log.error("Nie podano szablonu maila!");
                throw new EdmException("Nie podano szablonu maila!");
            }
            if (ServiceManager.getService(Mailer.NAME) == null)
                return;
            MailerDriver mailer = (MailerDriver) ServiceManager.getService(Mailer.NAME);

            getMailFromDocumentLogic(doc);
            if (mail != null) {
                String[] mailArr = mail.split(";");
                for(String toMail : mailArr){
                    mailer.send(toMail, toMail, null, Configuration.getMail(template), prepareContext(doc.getFieldsManager(), null, execution, docId), false);
                }
            } else if (user != null) {
                String[] juzers = user.split(",");
                mailUsers(execution, docId, doc, fm, mailer, juzers, checkPermissions);
            } else if (userField != null) {
                DSUser user = DSUser.findById(((Integer) fm.getKey(userField)).longValue());
                if (checkPermissions && DSApi.context().hasPermission(user, DSPermission.POWIADOMIENIA_OPOZNIONE_ZADANIE))
                    mailer.send(user.getEmail(), user.getEmail(), null, Configuration.getMail(template),
                            prepareContext(doc.getFieldsManager(), user, execution, docId), false);
                else if (!checkPermissions)
                    mailer.send(user.getEmail(), user.getEmail(), null, Configuration.getMail(template),
                            prepareContext(doc.getFieldsManager(), user, execution, docId), false);
            } else if (guid != null) {
                if (fieldCnValue != null)
                    guid = fm.getEnumItemCn(fieldCnValue);

                for (DSUser user : Arrays.asList(UserFactory.getInstance().findDivision(guid).getUsers())) {
                    if (checkPermissions && DSApi.context().hasPermission(user, DSPermission.POWIADOMIENIA_OPOZNIONE_ZADANIE))
                        mailer.send(user.getEmail(), user.getEmail(), null, Configuration.getMail(template),
                                prepareContext(doc.getFieldsManager(), user, execution, docId), false);
                    else if (!checkPermissions)
                        mailer.send(user.getEmail(), user.getEmail(), null, Configuration.getMail(template),
                                prepareContext(doc.getFieldsManager(), user, execution, docId), false);
                }
            } else if (acceptationCn != null) {
                Map<String, DSUser> mails = new HashMap<String, DSUser>();
                boolean assigned = false;
                String fieldValue = null;
                String[] acceptationCNs = acceptationCn.split(",");

                if (template.toLowerCase().contains("supervisor")) {
                    for (String taskId : Jbpm4ProcessLocator.taskIds(docId)) {
                        Task task = Jbpm4Provider.getInstance().getTaskService().getTask(taskId);
                        String executionId = execution.getId().split(".to")[0];
                        if (task.getExecutionId().contains(executionId)) {
                            if (task.getAssignee() != null) {
                                for (String acceptation : Arrays.asList(acceptationCNs)) {
                                    if (assigned)
                                        break;
                                    fieldValue = DSUser.findByUsername(task.getAssignee()).getDivisionsWithoutGroupPosition()[0].getGuid();
                                    assigned = assign(mails, assigned, fieldValue, acceptation);
                                }
                            } else {
                                for (Participation part : Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(taskId)) {
                                    for (String acceptation : Arrays.asList(acceptationCNs)) {
                                        if (assigned)
                                            break;
                                        if (part.getUserId() != null) {
                                            fieldValue = DSUser.findByUsername(part.getUserId()).getDivisionsWithoutGroupPosition()[0]
                                                    .getGuid();
                                            assigned = assign(mails, assigned, fieldValue, acceptation);
                                        } else {
                                            fieldValue = part.getGroupId();
                                            assigned = assign(mails, assigned, fieldValue, acceptation);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    for (String acceptation : Arrays.asList(acceptationCNs)) {
                        if (assigned)
                            break;
                        if (AcceptanceDivisionImpl.findByCode(acceptation) != null
                                && AcceptanceDivisionImpl.findByCode(acceptation).getOgolna() == 1)
                            fieldValue = null;
                        else if (fieldCnValue != null)
                            fieldValue = doc.getFieldsManager().getEnumItem(fieldCnValue).getCn();

                        assigned = assign(mails, assigned, fieldValue, acceptation);
                    }
                }
                for (String user : mails.keySet()) {
                    if (checkPermissions && DSApi.context().hasPermission(DSUser.findByUsername(user), DSPermission.POWIADOMIENIA_OPOZNIONE_ZADANIE))
                        mailer.send(mails.get(user).getEmail(), mails.get(user).getEmail(), null, Configuration.getMail(template),
                                prepareContext(doc.getFieldsManager(), mails.get(user), execution, docId), false);
                    else if (!checkPermissions)
                        mailer.send(mails.get(user).getEmail(), mails.get(user).getEmail(), null, Configuration.getMail(template),
                                prepareContext(doc.getFieldsManager(), mails.get(user), execution, docId), false);
                }
            } else {
                log.debug("ProcesdefinitionId: {}", execution.getProcessDefinitionId());
                log.debug("Execution.Id: {}", execution.getId());
                for (String taskId : Jbpm4ProcessLocator.taskIds(docId)) {
                    Task task = Jbpm4Provider.getInstance().getTaskService().getTask(taskId);
                    String executionId = execution.getId().split(".to")[0];
                    if (task.getExecutionId().contains(executionId)) {
                        log.debug("Task.assignee: {}", task.getAssignee());
                        if (task.getAssignee() != null) {
                            if (checkPermissions && DSApi.context().hasPermission(DSUser.findByUsername(task.getAssignee()), DSPermission.POWIADOMIENIA_OPOZNIONE_ZADANIE))
                                mailer.send(DSUser.findByUsername(task.getAssignee()).getEmail(), DSUser.findByUsername(task.getAssignee()).getEmail(), null, Configuration.getMail(template), prepareContext(doc.getFieldsManager(), DSUser.findByUsername(task.getAssignee()), execution, docId), false);
                            else if (!checkPermissions)
                                mailer.send(DSUser.findByUsername(task.getAssignee()).getEmail(), DSUser.findByUsername(task.getAssignee()).getEmail(), null, Configuration.getMail(template), prepareContext(doc.getFieldsManager(), DSUser.findByUsername(task.getAssignee()), execution, docId), false);
                        } else {
                            log.debug("Task.createTime: {}", task.getCreateTime());
                            log.debug("Task.Duedate: {}", task.getDuedate());
                            for (Participation part : Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(taskId)) {
                                if (part.getUserId() != null) {
                                    log.debug("part.getUserId() = {}", part.getUserId());
                                    if (checkPermissions && DSApi.context().hasPermission(DSUser.findByUsername(part.getUserId()), DSPermission.POWIADOMIENIA_OPOZNIONE_ZADANIE))
                                        mailer.send(DSUser.findByUsername(part.getUserId()).getEmail(), DSUser.findByUsername(part.getUserId()).getEmail(), null, Configuration.getMail(template), prepareContext(doc.getFieldsManager(), DSUser.findByUsername(part.getUserId()), execution, docId), false);
                                    else if (!checkPermissions)
                                        mailer.send(DSUser.findByUsername(part.getUserId()).getEmail(), DSUser.findByUsername(part.getUserId()).getEmail(), null, Configuration.getMail(template), prepareContext(doc.getFieldsManager(), DSUser.findByUsername(part.getUserId()), execution, docId), false);
                                } else {
                                    for (DSUser user : DSDivision.find(part.getGroupId()).getUsers()) {
                                        if (checkPermissions && DSApi.context().hasPermission(user, DSPermission.POWIADOMIENIA_OPOZNIONE_ZADANIE))
                                            mailer.send(user.getEmail(), user.getEmail(), null, Configuration.getMail(template), prepareContext(doc.getFieldsManager(), user, execution, docId), false);
                                        else if (!checkPermissions)
                                            mailer.send(user.getEmail(), user.getEmail(), null, Configuration.getMail(template), prepareContext(doc.getFieldsManager(), user, execution, docId), false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (DSApi.isContextOpen() && !isOpened) {
                DSApi.close();
            }
        }

    }

	private boolean assign(Map<String, DSUser> mails, boolean assigned, String fieldValue, String acceptation) throws EdmException,
			UserNotFoundException, DivisionNotFoundException {
		for (AcceptanceCondition accept : AcceptanceCondition.find(acceptation, fieldValue)) {
			if (accept.getUsername() != null) {
				DSUser assignee = DSUser.findByUsername(accept.getUsername());
				if (assignee.getSubstituteUser() != null) {
					mails.put(assignee.getSubstituteUser().getName(), assignee.getSubstituteUser());
				}
				mails.put(accept.getUsername(), assignee);
				assigned = true;
			}
			if (accept.getDivisionGuid() != null) {
				for (DSUser user : Arrays.asList(UserFactory.getInstance()
						.findDivision(accept.getDivisionGuid()).getUsers())) {
					if (user.getSubstituteUser() != null) {
						mails.put(user.getSubstituteUser().getName(), user.getSubstituteUser());
					}
					mails.put(user.getName(), user);
				assigned = true;
				}
			}
		}
		return assigned;
	}
	private void mailUsers(EventListenerExecution execution, Long docId, OfficeDocument doc, FieldsManager fm, MailerDriver mailer,
			String[] users, boolean checkPermissions) throws EdmException, UserNotFoundException, IOException {
		for (String juzer : users) {
			if (checkPermissions && !DSApi.context().hasPermission(DSUser.findByUsername(juzer), DSPermission.POWIADOMIENIA_OPOZNIONE_ZADANIE))
				continue;
			String email = null;
			if (juzer.equals("author"))
				email = DSUser.findByUsername(doc.getAuthor()).getEmail();
			else if (fieldCnValue != null)
				email = DSUser.findByUsername(fm.getEnumItemCn(fieldCnValue)).getEmail();
			else
				email = DSUser.findByUsername(juzer).getEmail();
			mailer.send(email, email, null, Configuration.getMail(template),
					prepareContext(doc.getFieldsManager(), DSUser.findByUsername(juzer), execution, docId), false);
		}
	}

	private Map<String, Object> prepareContext(FieldsManager fm, DSUser user, EventListenerExecution execution, Long docId) throws UserNotFoundException, EdmException
	{
		Map<String, Object> ctext = new HashMap<String, Object>();
		if (user != null)
		{
			ctext.put("first-name", user.getFirstname());
			ctext.put("last-name", user.getLastname());			
		}
		ctext.put("document-name", fm.getDocumentKind().getName());
		ctext.put("document-id", fm.getDocumentId());
		ctext.put("document-state", fm.getEnumItem("STATUS") != null ? fm.getEnumItem("STATUS").getTitle() : "");
        Document doc = Document.find(docId);

        if (doc instanceof OfficeDocument)
        {
            ctext.put("document-number", ((OfficeDocument)doc).getOfficeNumber());
            ctext.put("document-ko", ((OfficeDocument)doc).getOfficeNumber());
        }
        fm.getDocumentKind().logic().addSpecialValueForProcessMailNotifier(fm, ctext);

		for (String taskId : Jbpm4ProcessLocator.taskIds(docId))
		{
			Task task = Jbpm4Provider.getInstance().getTaskService().getTask(taskId);
			String executionId = execution.getId().split(".to")[0];
			if (task.getExecutionId().contains(executionId))
			{
				if (task.getAssignee() != null)
					ctext.put("document-assignee", DSUser.findByUsername(task.getAssignee()).getWholeName());
				else
				{
					for (Participation part : Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(taskId))
					{
						StringBuilder zadekretowani = new StringBuilder();
						if (part.getUserId() != null) {
							ctext.put("document-assignee", DSUser.findByUsername(part.getUserId()).getWholeName());
						} else {
							for (DSUser juser : DSDivision.find(part.getGroupId()).getUsers()) {
								zadekretowani.append(juser.getWholeName() + ", ");
							}
							ctext.put("document-assignee", zadekretowani);
							}
						}
					}
				}
			}
		for (String taskId : Jbpm4ProcessLocator.taskIds(fm.getDocumentId()))
		{
			Task task = Jbpm4Provider.getInstance().getTaskService().getTask(taskId);
			if (execution.getId().equals(task.getExecutionId()))
			{
				GregorianCalendar gregorianCalendar = new GregorianCalendar();
				gregorianCalendar.setTime(task.getCreateTime());
				
				if (realization_time_y != null)
				{
					gregorianCalendar.add(Calendar.YEAR, Integer.valueOf(realization_time_y));
					ctext.put("document-state-realization-time", realization_time_y + " lat");
				}
				else if (realization_time_M != null)
				{
					gregorianCalendar.add(Calendar.MONTH, Integer.valueOf(realization_time_M));
					ctext.put("document-state-realization-time", realization_time_M + " miesiecy");	
				}
				else if (realization_time_d != null)
				{
					gregorianCalendar.add(Calendar.DAY_OF_MONTH, Integer.valueOf(realization_time_d));
					ctext.put("document-state-realization-time", realization_time_d + " dni");	
				}
				else if (realization_time_h != null)
				{
					gregorianCalendar.add(Calendar.HOUR, Integer.valueOf(realization_time_h));
					ctext.put("document-state-realization-time", realization_time_h + " godzin");	
				}
				else if (realization_time_m != null)
				{
					gregorianCalendar.add(Calendar.MINUTE, Integer.valueOf(realization_time_m));
					ctext.put("document-state-realization-time", realization_time_m + " minut");	
				}

				ctext.put("document-deadline", gregorianCalendar.getTime().toLocaleString());
			}
		}
		Map<String, Object> values = fm.getFieldValues();

        /**
         * Dodanie do szablonu wszystkich warto�ci pol na dokumencie
         */
        for (String cn : fm.getFieldsCns())
        {
            Object value = fm.getValue(cn);
            if (value instanceof Date)
            {
                String date = DateUtils.formatCommonDate((Date)value);
                ctext.put(cn, date);
            }
            else
                ctext.put(cn, value);
        }

		return ctext;
	}

    /**
     * Wyciaga maila na jakiego ma zosta� wyslane z logiki dokumentu
     * @param document
     */
    public void getMailFromDocumentLogic(OfficeDocument document)
    {
        if(mailFromLogicCn != null)
        {
            mail = document.getDocumentKind().logic().getMailForJbpmMailListener(document, mailFromLogicCn);
        }
    }
}
