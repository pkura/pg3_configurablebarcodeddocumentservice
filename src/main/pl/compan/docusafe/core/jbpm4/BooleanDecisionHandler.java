package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BooleanDecisionHandler implements DecisionHandler{

	private final static Logger LOG = LoggerFactory.getLogger(BooleanDecisionHandler.class);

	private String field;

	public String decide(OpenExecution openExecution) 
	{
        boolean hasOpen = true;
		try
		{
            if (!DSApi.isContextOpen())
            {
                hasOpen = false;
                DSApi.openAdmin();
            }
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			LOG.info("documentid = " + docId + " field = " + field);
			Boolean cn = fm.getBoolean(field);
			if(cn == null) {
				throw new IllegalStateException("Nie wypełniono pola " + fm.getField(field).getName());
			}
			return Boolean.toString(cn);
		}
		catch (EdmException ex)
		{
			LOG.error(ex.getMessage(), ex);
			return "error";
		} finally {
            if (DSApi.isContextOpen() && !hasOpen)
                DSApi._close();
        }
	}
}
