package pl.compan.docusafe.core.jbpm4;

import pl.compan.docusafe.process.form.ActivityController;

/**
 * Rozszerzenia do pewnej wersji procesu jbpm4. ie dodatkowe pliki xml, bitmapy itp
 *
 * Klasa "trzyma" zasoby dla r�nych wersji procesu, inicjalizacja mo�e by� op�niona 
 * Klasa mo�e by� tworzona wielokrotnie dla tej samej wersji tego samego procesu
 */
public class ProcessExtensions {

    /**
     * rozszerzenie okre�laj�ce mo�liwe do wykonania czynno�ci
     */
    private ActivityController activityController = null;

    public ActivityController getActivityController() {
        return activityController;
    }

    public void setActivityController(ActivityController activityController) {
        this.activityController = activityController;
    }
}
