package pl.compan.docusafe.core.jbpm4;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import org.jfree.util.Log;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ProfileInDivisionAssignmentHandler implements AssignmentHandler {

    /**
     * Nazwa adds-a, kt�ry okre�la id profilu
     */
    public String profileIdAdds;
    /**
     * Nazwa zmiennej procesu, kt�ra przechowuje id profilu
     */
    public String profileIdVariableName;
    /**
     * Nazwa pola dokumentu typu dsdivision - kluczem pola jest id dzia�u
     */
    public String divisionFieldCn;
    /**
     * Nazwa s�ownika, w kt�rym wyszukiwane ma by� pole dsdivision - kluczem pola jest id dzia�u
     */
    public String dictionaryCn;
    /**
     * Nazwa s�ownika, w kt�rym wyszukiwane ma by� pole dsdivision - kluczem pola jest kod dzia�u
     */
    public String divisionCodeFieldCn;
    /**
     * Nazwa zmiennej procesu, kt�ra przechowuje id dzia�u
     */
    public String divisionIdVariableName;
    /**
     * Kod dzia�u z kt�rego nale�y zacz�� wyszukiwanie u�ytkownika o okre�lonym profilu
     */
    public String divisionCodeFromAdds;
    /**
     * Kod dzia�u z kt�rego nale�y zacza� wyszukiwanie u�ytkownika o okre�lonym profilu
     */
    public String divisionCodeIfNotSet;
    /**
     * Zakres przeszukiwania. Okre�la liczba przeszukiwanych poziom�w w g�r� od danego dzia�u
     * -1 do root
     */
    public Integer scopeLvl = 0;
    /**
     * Zako�cz przeszukiwanie na poziomie, na kt�rym znaleziono u�ytkownik�w o danym profilu
     */
    public Boolean endOnFirst = true;
    /**
     * Okre�la czy w momencie nie znalezienia w zadanym zakresie u�ytkownik�w o danym profilu maj�
     * zosta� przypisani wszyscy z pierwszego napotkanego dzia�u, w kt�rym kto� si� znajduje
     */
    public Boolean assignToDivisionIfNoProfile = false;

    public long getDivisionId(OpenExecution openExecution, FieldsManager fm) throws Exception {
        Long divisionId = null;
        if (StringUtils.isNotBlank(dictionaryCn)){


            //todo


            Object dictKey = fm.getKey(dictionaryCn);
            Object dictField = fm.getField(dictionaryCn);

            //dict.getdivisionFieldCn);
        }else if(StringUtils.isNotBlank(divisionFieldCn)){
            divisionId = Long.parseLong(fm.getKey(divisionFieldCn).toString());
        }else if(StringUtils.isNotBlank(divisionCodeFieldCn)){
            DSDivision division = DSDivision.findByCode((String) fm.getKey(divisionCodeFieldCn));
            divisionId = division.getId();
        } else if (StringUtils.isNotBlank(divisionIdVariableName)){
            divisionId = getLongFromExecution(openExecution, divisionIdVariableName);
            if (divisionId==null)throw new Exception("Class of variable=" + divisionIdVariableName +" can not be casted to Long");
        } else if (StringUtils.isNotBlank(divisionCodeFromAdds)) {
        	String code;
        	if(StringUtils.isNotBlank(divisionCodeIfNotSet)) {
        		code = Docusafe.getAdditionPropertyOrDefault(divisionCodeFromAdds, divisionCodeIfNotSet);
        	} else {
        		code = Docusafe.getAdditionPropertyOrDefault(divisionCodeFromAdds, "-1");
        	}
        	DSDivision division = DSDivision.findByCode((code));
        	divisionId = division.getId();
        } else
            throw new Exception("No division id source");


        if (divisionId==null) throw new Exception("No division id");
        return divisionId;
    }

    public Long getProfileId (OpenExecution openExecution) throws Exception {
        Long profileId;
        if (StringUtils.isNotBlank(profileIdAdds)){
            profileId = (long)Docusafe.getIntAdditionPropertyOrDefault(profileIdAdds, -1);
            if (profileId < 0) throw new Exception("No adds=" + profileIdAdds);
        } else if (StringUtils.isNotBlank(profileIdVariableName)){
            profileId = getLongFromExecution(openExecution, profileIdVariableName);
            if (profileId==null)throw new Exception("Class of variable=" + profileIdVariableName +" can not be casted to Long");
        } else
            throw new Exception("No profile id source");

        if (profileId==null) throw new Exception("No profile id");
        return profileId;
    }

    public static Long getLongFromExecution (OpenExecution openExecution, String variableName){
        Object profileIdObj = openExecution.getVariable(variableName);
        if (profileIdObj instanceof Integer)
            return ((Integer) profileIdObj).longValue();
        if (profileIdObj instanceof Long)
            return (Long) profileIdObj;
        return null;
    }

    @Override
    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
        DSContextOpener contextOpener = DSContextOpener.openAsAdminIfNeed();

        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
            FieldsManager fm = doc.getFieldsManager();

            Long profileId = getProfileId(openExecution);
            Long divisionId = getDivisionId(openExecution, fm);

            openExecution.setVariable(Jbpm4WorkflowFactory.REASSIGNMENT_TIME_VAR_NAME, new Date());

            List<String> outUsernames = new ArrayList<String>();
            boolean assigned = assignToUsersFromProfile(outUsernames, doc, assignable, openExecution, divisionId, profileId, endOnFirst, scopeLvl, assignToDivisionIfNoProfile);
            if (!assigned && assignToDivisionIfNoProfile)
                assigned = assignToAllUserInDivision(outUsernames, doc, assignable, openExecution, divisionId, profileId, endOnFirst);

            if (!assigned)
                throw new Exception("Not assigned, profileId=" + profileId + ", divisionFieldCn=" + divisionFieldCn);
        } catch (EdmException e) {
            Log.error(e.getMessage());
            throw e;
        } finally {
            contextOpener.closeIfNeed();
        }
    }

    public static boolean assignToUsersFromProfile(List<String> outUsernames, OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Long divisionId, Long profileId, boolean endOnFirst, int counter, boolean assignToDivisionIfNoProfile) throws Exception {
        boolean assigned = putUsersFromProfile(outUsernames, divisionId, profileId, endOnFirst, counter, assignToDivisionIfNoProfile);
        if (assigned){
            assigned = AssigneeHandlerUtils.assignByUsernames(doc,assignable,openExecution, outUsernames);
        }
        return assigned;
    }
    public static boolean putUsersFromProfile(List<String> outUsernames, Long divisionId, Long profileId, boolean endOnFirst, int counter, boolean ifNotFoundNotThrowException) throws Exception {
        String sqlQuery = "select u.NAME " +
                "from DS_DIVISION d " +
                "join DS_USER_TO_DIVISION ud on d.ID = ud.DIVISION_ID " +
                "join DS_USER u on ud.USER_ID = u.ID " +
                "join ds_profile_to_user pu on u.ID = pu.user_id " +
//                "where pu.profile_id = ? and d.GUID = ?";
                "where pu.profile_id = ? and d.id = ? and u.DELETED = 0";

        PreparedStatement ps;
        ResultSet rs;

        ps = DSApi.context().prepareStatement(sqlQuery);
        ps.setLong(1, profileId);
//        ps.setString(2, divisionId);
        ps.setLong(2, divisionId);
        rs = ps.executeQuery();

        List<String> resultList = new ArrayList<String>();
        while (rs.next()) {
            resultList.add(rs.getString(1));
        }
//        for (String user : resultList) {
//            assignable.addCandidateUser(user);
//            AssigneeHandler.addToHistory(openExecution, doc, user, null);
//        }

        outUsernames.addAll(resultList);
        if (endOnFirst && !outUsernames.isEmpty())
            return true;
        resultList.clear();

        if (counter == 0)
            return endOfAssignmentToUsersFromProfile(ifNotFoundNotThrowException, !outUsernames.isEmpty());

        DSDivision div = DSDivision.findById(divisionId);
        div = div.getParent();
        if (div == null || div.isRoot())
            return endOfAssignmentToUsersFromProfile(ifNotFoundNotThrowException, !outUsernames.isEmpty());

        return putUsersFromProfile(outUsernames, div.getId()/*div.getGuid()*/, profileId, endOnFirst, counter - 1, ifNotFoundNotThrowException);
    }

    public static boolean endOfAssignmentToUsersFromProfile(boolean ifNotFoundNotThrowException, boolean success) throws Exception {
        if (success)
            return true;
        if (ifNotFoundNotThrowException)
            return false;
        throw new Exception("W zadanym zasi�gu nie znaleziono u�ytkownik�w");
    }

    public static boolean assignToAllUserInDivision(List<String> outUsernames, OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Long divisionId, Long profileId, boolean endOnFirst) throws EdmException {
        DSDivision div = DSDivision.findById(divisionId);
        while (div.getOriginalUsers().length == 0) {
            div = div.getParent();
            if (div == null) {
                throw new DivisionNotFoundException("null");
            }
        }
        assignable.addCandidateGroup(div.getGuid());
        AssigneeHandler.addToHistory(openExecution, doc, null, div.getGuid());
        return true;
    }
}
