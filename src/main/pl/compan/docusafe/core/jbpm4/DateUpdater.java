package pl.compan.docusafe.core.jbpm4;

import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.listener.EventListenerExecution;
import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.TaskListMarkerManager;
import pl.compan.docusafe.core.users.*;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public class DateUpdater extends AbstractEventListener
{
	private static final Logger log = LoggerFactory.getLogger(DateUpdater.class);



    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        boolean isOpened = true;
        Long docId = Jbpm4Utils.getDocumentId(execution);

        try {
            if (!DSApi.isContextOpen()) {
                isOpened = false;
                DSApi.openAdmin();
            }
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();



            Date data=  (Date)fm.getValue("DATA_REJESTRACJI");
            long now = System.currentTimeMillis();
            Date data2 = new Date(now);
            int days = Days.daysBetween(new DateTime(data).toLocalDate(), new DateTime(data2).toLocalDate()).getDays();
            putendtime(docId, days+1);






        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (DSApi.isContextOpen() && !isOpened) {
                DSApi.close();
            }
        }

    }
    private void putendtime(Long id, Integer days) throws EdmException {

        PreparedStatement ps = null;
        try {
            String sql = "UPDATE  dsg_yetico_rek_zew set data_zakonczenia=? where document_id=?";
            ps = DSApi.context().prepareStatement(sql);
            ps.setInt(1, days);
            ps.setLong(2, id);
            ps.executeQuery();

        }  catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }

    }

}
