package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jtarasiuk
 * Date: 21.06.13
 * Time: 12:10
 */
public class SetOfficeNumber  implements ExternalActivityBehaviour
{
    private final static Logger LOG = LoggerFactory.getLogger(SetOfficeNumber.class);

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception
    {
        try
        {
            OfficeDocument document = Jbpm4Utils.getDocument(activityExecution);
            Date entryDate = GlobalPreferences.getCurrentDay();
            // jeszcze raz sprawdzam
            if (document.getOfficeNumber() == null)
            {
                if (document instanceof InOfficeDocument)
                {
                    bindToJournalInDocument((InOfficeDocument) document, entryDate);
                }
                else if (document instanceof OutOfficeDocument)
                {
                    bindToJournalOutDocument((OutOfficeDocument) document, entryDate);
                }
            }
            activityExecution.takeDefaultTransition();
        }
        catch (Exception e)
        {
            LOG.error(e.getMessage(), e);
        }
    }

    private void bindToJournalOutDocument(OutOfficeDocument doc, Date entryDate) throws EdmException
    {
        Journal journal = null;
        if (doc.isInternal())
        {
            journal = Journal.getMainInternal();
        }
        else
        //TODO: moim zdaniem zmieni� na: if (AvailabilityManager.isAvailable(AvailabilityManager.OUTOFFICEDOCUMENT_BIND_TO_JOURNAL, doc.getDocumentKind().getCn()))
        if (AvailabilityManager.isAvailable(AvailabilityManager.OUTOFFICEDOCUMENT_BIND_TO_JOURNAL))
        {
            journal = Journal.getMainOutgoing();
        }
        Long journalId = journal.getId();
        DSApi.context().session().flush();
        Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
        doc.bindToJournal(journalId, sequenceId);
        doc.setOfficeDate(entryDate);
    }

    private void bindToJournalInDocument(InOfficeDocument doc, Date entryDate) throws EdmException
    {
        Journal journal = Journal.getMainIncoming();
        Long journalId  = journal.getId();
        DSApi.context().session().flush();
        Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
        doc.bindToJournal(journalId, sequenceId);
    }

    @Override
    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception
    {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
