package pl.compan.docusafe.core.jbpm4;

import org.apache.chemistry.opencmis.commons.impl.IOUtils;
import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.EnumerationField;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskListUtils;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.tasklist.TaskListAction;
import pl.compan.docusafe.web.office.tasklist.TaskListCountAction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

public class AcceptancesListener extends AbstractEventListener
{

	private static final Logger log = LoggerFactory.getLogger(AcceptancesListener.class);
	private String acception;
	private String var;
	private String type;
	private String clearVariables;
	
	public void notify(EventListenerExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);

		/* 
		 * wymuszenie niedodawania do listy akceptacji dokumentu, 
		 * tzw. pośredniego stanu poprawy
		 * umownie nazwanego "taskCorrectionForFork"
		*/ 
		if (acception != null && !acception.equals("taskCorrectionForFork")) {
			// Zapis akceptacji
			if (var != null && execution.getVariable(var) != null) {
				Object varObject = execution.getVariable(var);
				if (varObject instanceof Long)
					DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), acception, (Long) execution.getVariable(var));
				else if (varObject instanceof String)
					DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), acception, Long.valueOf((String) execution.getVariable(var)));
				else
					DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), acception, null);
			} else
				DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), acception, null);
		}


        updateAssignmentHistory(execution, doc, acception, type);


        if (AvailabilityManager.isAvailable("addToWatch"))
            DSApi.context().watch(URN.create(Document.find(docId)));

        if (clearVariables != null && !clearVariables.equals("")) {
            String[] vars = clearVariables.split(",");

            for (String var : vars) {
                if (var != null && !var.equals("doc-id"))
                    execution.removeVariable(var);
            }
        }

		doc.getDocumentKind().logic().onAcceptancesListener(doc, acception);
	}

    public static void updateAssignmentHistory(EventListenerExecution execution, OfficeDocument doc, String acception, String type) throws EdmException {

        DSUser user = DSApi.context().getDSUser();

        // Dokonanie akpcetacji - informacja dla użytkownika
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(user.getName());
        ahe.setProcessName(execution.getProcessDefinitionId());
        if (type != null && "end-task".equals(type))
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM_END_TASK);
        else
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_ACCEPTED);
        String divisions = "";

        for (DSDivision a : user.getOriginalDivisionsWithoutGroup())
        {
            if (!divisions.equals(""))
                divisions = divisions + " / " + a.getName();
            else
                divisions = a.getName();
        }
        ahe.setSourceGuid(divisions);
        String status = null;
        try
        {
            status = ((EnumerationField) doc.getFieldsManager().getField("STATUS")).getEnumItemByCn(acception).getTitle();
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
        }

        if (status != null)
            ahe.setStatus(status);

        ahe.setCtime(new Date());

        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);

        ahe.setSubstituteUserByParentUser(user, doc, execution);

        doc.addAssignmentHistoryEntry(ahe);
    }

//    private a

	public String getAcception()
	{
		return acception;
	}

	public void setAcception(String acception)
	{
		this.acception = acception;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
