package pl.compan.docusafe.core.jbpm4;

import com.google.common.base.Preconditions;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.ObjectUtils;
import org.jbpm.JbpmException;
import org.jbpm.api.Execution;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.ProcessDefinitionQuery;
import org.jbpm.api.TaskService;
import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.EnumerationField;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.process.*;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Klasa obs�uguj�ca proces jbpm4. Klasa odpala signal na procesie z argumentem b�d�cym przekazan� nazw� czynno�ci.
 * Dodatkowo umieszcza zmienn� activityID w�r�d zmiennych procesu.
 * Odzielna instancja jest tworzona, dla r�nych proces�w i dla r�nych dockind�w
 *
 * Parametry wykorzystywane podczas tworzenia z xml'a
 * jbpm4-process-name - okre�la nazw� tworzonego procesu jbpmowego, je�li nie podana korzysta z nazwy procesu
 * max-instances-per-document - okre�la maksymaln� liczb� proces�w tego typu dla danego dokumentu, domy�lna warto�� - 1 - warto�� sta�a dla wszystkich wersji procesu
 * status-field - okre�la jakie pole w dockindzie wskazuje na kolumn� ze statusem (type="enum"), je�li status w procesie nie odpowiada rzadnej warto�ci enum, pole nie jest zmieniane
 */
public class Jbpm4ProcessLogicSupport extends AbstractProcessLogic {
    private final static Logger log = LoggerFactory.getLogger(Jbpm4ProcessLogicSupport.class);

    private final String processName;
    /** Maksymalna liczba proces�w, je�li null to jest nieograniczona */
    private final Integer maxInstances;

    private final String statusField;

    public Jbpm4ProcessLogicSupport(InstantiationParameters ip){
        Preconditions.checkNotNull(ip, "ip cannot be null");
        Object mi = ip.getParamOrDefault(InstantiationParameters.Param.MAX_INSTANCES_PER_DOCUMENT, "1");
        if(mi.equals("unlimited")){
            maxInstances = null;
        } else {
            maxInstances = Integer.parseInt(mi.toString());
        }
        if(ip.hasParam(InstantiationParameters.Param.STATUS_FIELD)){
            statusField = ip.getParam(InstantiationParameters.Param.STATUS_FIELD).toString();
        } else {
            statusField = null;
        }
        log.info("max instances = " + maxInstances);
        if(ip.hasParam(InstantiationParameters.Param.JBPM4_PROCESS_NAME)){
            processName = ip.getParam(InstantiationParameters.Param.JBPM4_PROCESS_NAME).toString();
        } else {
            ip.assertParam(InstantiationParameters.Param.PROCESS_NAME);
            processName = ip.getParam(InstantiationParameters.Param.PROCESS_NAME).toString();
        }
        log.info("Jbpm4ProcessLogicSupport process name " + processName);
    }

    /**
     * Umieszcza zmienn� activityId w�r�d zmiennych procesu i opala signal z actionName
     * @param pi nie mo�e by� null, tylko Jbpm4ProcessInstance
     * @param pa nie mo�e by� null, musi zawiera� activityId kt�re jest r�wne taskId w jbpm4,
     *          actionName okre�la jaki sygna� (signal) zostanie wys�any w jbpm4
     * @throws EdmException
     */
    public final void process(ProcessInstance pi, ProcessAction pa) throws EdmException {
        process((Jbpm4ProcessInstance)pi, pa);
    }

    protected void process(Jbpm4ProcessInstance pi, ProcessAction pa) throws EdmException{
        log.info("ACTION:" + pa.getActionName());
        Jbpm4Provider.getInstance().getExecutionService().setVariable(pi.getId(), Jbpm4Constants.ACTIVITY_ID_PROPERTY,pa.getActivityId());
        if(pa.getActivityId() != null && WorkflowFactory.jbpm){
     	   try {
            Jbpm4Provider.getInstance().getTaskService()
                .completeTask(
                    pa.getActivityId().replaceAll("[^,]*,",""),
                    pa.getActionName());;
     	   }
     	   catch (org.jbpm.api.JbpmException e)
     	   {
     		   if (pi.getProcessDefinitionId().contains("Delegacja") && e.getMessage().contains("No transition named 'to finish'"))
     		   {
     			   Jbpm4Provider.getInstance().getTaskService()
     	                .completeTask(
     	                    pa.getActivityId().replaceAll("[^,]*,",""),
     	                    "to end");;
     		   }
     		   else throw e;
     	   }
            pa.reassigned(true);
            updateStatusField(pi, (OfficeDocument)pa.getDocument());
            //podczas procesowania mog� si� zmieni� atrybuty biznesowe - trzeba aktualizowa�
            TaskSnapshot.updateByDocumentId(pa.getDocumentId(),"anyType");
        } else {
            pi.signal(pa.getActionName());
            updateStatusField(pi, (OfficeDocument)pa.getDocument());
            Boolean reassigned = false;
            try{
                reassigned =  "true".equals(Jbpm4Provider.getInstance().getExecutionService().getVariable(pi.getId(), Jbpm4Constants.REASSIGNMENT_PROPERTY)+"");
                if(reassigned != null){
                    Jbpm4Provider.getInstance().getExecutionService().setVariable(pi.getId(), Jbpm4Constants.REASSIGNMENT_PROPERTY,false);
                }
            } catch (Exception e) {
                //TODO POPRAWI�
                reassigned = true;
                log.error(e.getMessage());
            }
            //proces mo�e zmieni� atrybuty biznesowe - trzeba uaktualni� stan poprzez wywo�anie metody logiki
            pa.getDocument().getDocumentKind().logic().archiveActions(pa.getDocument(),0);
            pa.reassigned(reassigned);
            TaskSnapshot.updateByDocumentId(pa.getDocumentId(),"anyType");
        }
        if(pa.isStored()){
            log.info("storing processactionentry");
            AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
            ahe.setSourceUser(DSApi.context().getPrincipalName());
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_PROCESS_ACTION);
            ahe.setObjective(pa.getActionName());
            ahe.setProcessName(processName);
            if(pa.isReassigned()){
                TaskService taskService = Jbpm4Provider.getInstance().getTaskService();
                List<Task> tasks = taskService.createTaskQuery().processInstanceId(pi.getId()).list();
                if(tasks.size() == 1){
                    List<Participation> ps = taskService.getTaskParticipations(tasks.get(0).getId());
                    if(ps.size() == 1) {
                        ahe.setTargetGuid(ps.get(0).getGroupId());
                        ahe.setTargetUser(ps.get(0).getUserId());
                    }
                }
            }
            pa.getDocument().addAssignmentHistoryEntry(ahe);

            DSApi.context().session().save(new ProcessActionEntry(pa.getActionName(),
                    pa.getDocumentId(),
                    pi,
                    DSApi.context().getPrincipalName()));
        }
    }

    /**
     * Uaktualnia pole dockindowe status
     * @param pi
     * @param doc
     * @throws EdmException
     */
    private void updateStatusField(Jbpm4ProcessInstance pi, OfficeDocument doc) throws EdmException{
    	
    	if(statusField != null)
        {
    		String state = pi.getSingleState();
        	if(state != null)
        	{
//                log.info("new status" + state);
        		EnumItem ei = doc.getFieldsManager().getField(statusField).getEnumItemByCn(pi.getSingleState());
//                log.info("status field next value : " + ei );
        		if(ei != null)
        		{
//                    log.info(" changing key to :  " + ei.getId());
        			doc.getDocumentKind().setOnly(doc.getId(),
                    Collections.singletonMap(statusField, ei.getId()));
        		}
        	}
        }
    	
    	
    }

    public final String getProcessName(){
        return processName;
    }

    /**
     * Liczy ile instancji procesu istnieje
     * @param processName nazwa procesu
     * @param documentId id dokumentu
     * @return liczba proces�w dla danego dokumentu
     * @throws EdmException
     */
    private static int countForDocumentId(String processName , long documentId) throws EdmException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        int ret;
        try{
            ps = DSApi.context().prepareStatement("SELECT count(1) FROM ds_document_to_jbpm4 WHERE document_id = ? AND process_name = ?");
            ps.setLong(1, documentId);
            ps.setString(2, processName);
            rs = ps.executeQuery();
            rs.next();
            ret = rs.getInt(1);
            log.info("found " + ret + " processes for name = " + processName + " and id = " + documentId);
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }
        return ret;
    }
    
    private static void eraseOldProcessEntry(String processName, long documentId) throws EdmException {
        PreparedStatement ps = null;
        int ret;
        try{
            ps = DSApi.context().prepareStatement("DELETE FROM ds_document_to_jbpm4 WHERE document_id = ? AND process_name = ? AND "+
            		"( select count(1) from jbpm4_task task where task.execution_id_ = ds_document_to_jbpm4.JBPM4_id) < 1");
            ps.setLong(1, documentId);
            ps.setString(2, processName);
            int rows = ps.executeUpdate();
            if(rows > 1){
                throw new EdmException("Wiele proces�w r�cznych :  " + rows);
            }
//            log.info("found " + ret + " processes for name = " + processName + " and id = " + documentId);
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }

    /**
     * Tworzy now� instancj� procesu je�eli nie zosta�a przekroczona ich maksymalna liczba (ustalana w xmlu, domy�lnie 1)
     * @param od dokument, do kt�rego nale�y proces
     * @param params parametry przekazywane do jbpma jako zmienne procesu
     * @throws EdmException je�eli maksymalna liczba proces�w zosta�a osi�gni�ta
     */
    @Override
    public void startProcess(OfficeDocument od, Map<String, ?> params) throws EdmException {
        
    	//TODO:Doda� warunek w procesie czy moze byc wznawiany
    	if(maxInstances != null)
    		eraseOldProcessEntry(processName, od.getId());
    	
    	if(maxInstances != null && maxInstances <= countForDocumentId(processName, od.getId())){
            throw new EdmException("Maksymalna liczba zosta�a ju� osi�gni�ta");
        }
        log.info("creating process '{}' for document {} : maxInstances = {}; all instances = {}",
                getProcessName(), od.getId(), maxInstances, countForDocumentId(processName, od.getId()));
        
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setCtimeAndCdate(new Date());
        if(params.containsKey(Jbpm4WorkflowFactory.OBJECTIVE)) {
            ahe.setObjective((String) params.get(Jbpm4WorkflowFactory.OBJECTIVE));
        } else {
            ahe.setObjective("start-process");
        }
        ahe.setProcessName(getProcessName());
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        String divisions = "";
		for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
		{
			if (!divisions.equals(""))
				divisions = divisions + " / " + a.getName();
			else
				divisions = a.getName();
		}
		if (divisions.isEmpty())
			divisions = "x";
		ahe.setSourceGuid(divisions);
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        ahe.setType(AssignmentHistoryEntry.EntryType.JBPM_INIT);
        od.addAssignmentHistoryEntry(ahe);
        

        
        
        create(od, params);
        od.getDocumentKind().logic().archiveActions(od,DocumentLogic.TYPE_SAVE_ALL);
    }

    private void create(OfficeDocument doc, Map<String, ?> params) throws EdmException{
        HashMap<String, Object> procesVariables = new HashMap<String, Object>(params);
        //Sankowski powiediza� �e moze zadziala ustawiam osobe dekretuj�ca na autora
        if(!procesVariables.containsKey("previousAssignee"))
        	procesVariables.put("previousAssignee",DSApi.context().getPrincipalName());
        if(!procesVariables.containsKey("currentAssignee"))
        	procesVariables.put("currentAssignee", DSApi.context().getPrincipalName());
        procesVariables.put(Jbpm4Constants.DOCUMENT_ID_PROPERTY, doc.getId());
        ExecutionService execService = Jbpm4Provider.getInstance().getExecutionService();
        org.jbpm.api.ProcessInstance pi
           = execService.startProcessInstanceByKey(
                    findKey(processName),
                    procesVariables);
        
        try{
        	execService.createVariable(pi.getId(), Jbpm4Constants.DOCUMENT_ID_PROPERTY, doc.getId(), true);
        }catch(Exception e){
        	log.error("Nie we wszystkich procesach dzia�a: {}",e.getMessage());
        }
        
        add(processName, doc.getId(), pi.getId());
        updateStatusField(new Jbpm4ProcessInstance(processName, pi), OfficeDocument.find(doc.getId()));
    }

    /**
     * Znajduje jbpmowe id najnowszej wersji procesu,
     * mo�na pomy�le� o jakim� cacheowaniu tej warto�ci w przysz�o�ci
     * tylko trzeba pami�ta� by j� aktualizowa� przy wczytaniu nowego procesu
     * @param processName jbpmowa nazwa procesu - najcz�ciej taka sama jak docusafeowa
     * @return jbpmowe id definicji procesu
     */
    private static String findKey(String processName){
        List<org.jbpm.api.ProcessDefinition> list
            = Jbpm4Provider
                .getInstance()
                .getRepositoryService()
                .createProcessDefinitionQuery()
                .processDefinitionName(processName)
                .orderDesc(ProcessDefinitionQuery.PROPERTY_VERSION)
                .list();

        String ret;
        if(list.isEmpty()){
            throw new IllegalArgumentException("Nie istnieje proces o nazwie:" + processName);
        }
        ret = list.get(0).getKey();

        log.info("key =" + ret + " for name = " + processName);
        return ret;
    }

    private static void add(String processName, long documentId, String processId)throws EdmException{
        PreparedStatement ps = null;
        try{
            ps = DSApi.context().prepareStatement("INSERT INTO ds_document_to_jbpm4(document_id, jbpm4_id, process_name) VALUES (?,?,?)");
            ps.setLong(1, documentId);
            ps.setString(2, processId);
            ps.setString(3, processName);
            ps.execute();
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }
}
