package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa, kt�ra dekretuje pismo na podstawie parametr�w wskazuj�cych u�ytkownika
 * lub i dzia�
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ProcessParamsAssignmentHandler implements AssignmentHandler{
    private static Logger LOG = LoggerFactory.getLogger(ProcessParamsAssignmentHandler.class);

    public final static String ASSIGN_USER_PARAM = "assignee";
    public final static String ASSIGN_DIVISION_GUID_PARAM = "guid";
    public final static String OBJECTIVE = "OBJECTIVE";

    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
        String username = (String) openExecution.getVariable(ASSIGN_USER_PARAM);
        String guids = (String) openExecution.getVariable(ASSIGN_DIVISION_GUID_PARAM);
        
        LOG.info("trying to assign, execution params : assignee = '{}', guid = '{}'", username, guids);

        if(username == null){
            if(guids == null) {
                LOG.error("fast assignment params not set - assigning 'admin'");
                assignable.addCandidateUser("admin");
            } else {
            	for(String g: guids.split(",")){
                try {
                    DSDivision.find(g);
                } catch (DivisionNotFoundException ex) {
                    LOG.error("guid '{}' not found - assigning 'admin'", g);
                    assignable.addCandidateUser("admin");
                }
                assignable.addCandidateGroup(g);
            }
            }
        } else {
            try {
                DSUser.findByUsername(username);
                assignable.addCandidateUser(username);
            } catch (UserNotFoundException ex) {
                LOG.error("user '{}' not found - assigning 'admin'", username);
                assignable.addCandidateUser("admin");
            }
        }
    }
}
