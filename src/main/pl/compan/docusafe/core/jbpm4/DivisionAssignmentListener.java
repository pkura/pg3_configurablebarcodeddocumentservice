package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * EventListener, kt�ry dekretuje pismo na dzia� podany w parametrze "guid"
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DivisionAssignmentListener extends AbstractEventListener{
    private final static Logger LOG = LoggerFactory.getLogger(DivisionAssignmentListener.class);

    private String guid;

    public void notify(EventListenerExecution eventListenerExecution) throws Exception {
//        String docId = eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "";
//        LOG.info("doc id = " + docId);
        LOG.info("guid = " + guid);
        LOG.info("activityId = " + eventListenerExecution.getVariable(Jbpm4Constants.ACTIVITY_ID_PROPERTY));
        WorkflowFactory.simpleAssignment(
                eventListenerExecution.getVariable(Jbpm4Constants.ACTIVITY_ID_PROPERTY).toString(),
				guid,//grupa
				null,
				DSApi.context().getPrincipalName(),
				null,
				WorkflowFactory.SCOPE_DIVISION,
				"do procesowania",
				null);
        eventListenerExecution.setVariable(Jbpm4Constants.REASSIGNMENT_PROPERTY, true);
    }
}
