package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;

import org.jbpm.pvm.internal.jobexecutor.JobExecutor;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Collections;

/**
 * Listener ustawia pole w "field" na warto�� w "value"
 * 
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class FieldSetterListener extends AbstractEventListener
{
	private final static Logger LOG = LoggerFactory.getLogger(FieldSetterListener.class);

	private String field;
	private String value;

	public void notify(EventListenerExecution eventListenerExecution) throws Exception
	{
		boolean hasOpen = true;
		if (!DSApi.isContextOpen())
		{
			hasOpen = false;
			DSApi.openAdmin();
		}
		Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
		doc.getDocumentKind().setOnly(docId, Collections.singletonMap(field, value), false);
		if (DSApi.isContextOpen() && !hasOpen)
			DSApi.close();
	}
}
