package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;

public class UpdateTaskListListener extends AbstractEventListener
{


	@Override
	public void notify(EventListenerExecution exec) throws Exception
	{
		if (!DSApi.isContextOpen())
			DSApi.openAdmin();
		
		Long docId = Long.valueOf(exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		//DSApi.context().begin();
		//TaskSnapshot.updateByDocument(Document.find(docId));
		System.out.println("ProcessdefinitionID: " + exec.getProcessDefinitionId());
		System.out.println("exec.getProcessInstance().getId(): " + exec.getProcessInstance().getId());
		System.out.println("exec.getProcessInstance().getKey(): " + exec.getProcessInstance().getKey());
		System.out.println("exec.getProcessInstance().getName(): " + exec.getProcessInstance().getName());
		//System.out.println("exec.getProcessInstance().getParent().getId(): " + exec.getProcessInstance().getParent().getId());
		//DSApi.context().commit();
		if (DSApi.isContextOpen())
			DSApi.close();
		
	}

}
