package pl.compan.docusafe.core.jbpm4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.Execution;
import org.jbpm.api.JbpmException;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.task.Task;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeOrderExp.OrderType;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMFactory;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class StartProcess implements ExternalActivityBehaviour
{
	protected static Logger log = LoggerFactory.getLogger(StartProcess.class);

	String fieldValue = null;
	String user;
	String guid;
	String acceptationCn;
	String fieldCnValue;
	String field;
	String processName;
	
	String waitForSignal;

	public void execute(ActivityExecution activityExecution) throws Exception
	{
		long documentId = (Long) activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);
		OfficeDocument od = OfficeDocument.find(documentId);
		LinkedList<String> guids = new LinkedList<String>();
		LinkedList<String> users = new LinkedList<String>();
		
		if (StringUtils.isNotBlank(guid))
		{
			guids.add(guid);
			od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(processName).getLogic().startProcess(od, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, guids));
		}
		else if (StringUtils.isNotBlank(user))
		{
			if (user.equals("author"))
				user = od.getAuthor();
			od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(processName).getLogic().startProcess(od, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, user));
		}
		else if(acceptationCn!=null && acceptationCn.equals("principalUser"))
		{
			od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(processName).getLogic().startProcess(od, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, DSApi.context().getPrincipalName()));
			od.setDocumentKind(DocumentKind.findByCn("fakt_zap_ifpan"));
		}
		else if (StringUtils.isNotBlank(field))
		{
			user = od.getFieldsManager().getEnumItemCn(field);
			od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(processName).getLogic().startProcess(od, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, user));
		}
		else if (StringUtils.isNotBlank(acceptationCn))
		{

			String[] assigness = acceptationCn.split(",");
			for (String ass : Arrays.asList(assigness))
			{

				if (AcceptanceDivisionImpl.findByCode(ass) != null && AcceptanceDivisionImpl.findByCode(ass).getOgolna() == 1)
					fieldValue = null;
				else if (fieldCnValue != null)
					fieldValue = od.getFieldsManager().getEnumItem(fieldCnValue).getCn();

				for (AcceptanceCondition accept : AcceptanceCondition.find(ass, fieldValue))
				{
					if (accept.getUsername() != null)
					{
						users.add(accept.getUsername());
					}
					if (accept.getDivisionGuid() != null)
					{
						guids.add(accept.getDivisionGuid());
					}
				}				
			}
	
			if (!guids.isEmpty())
			{
				od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(processName).getLogic().startProcess(od, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, guids));
			}
			if (!users.isEmpty())
			{
				od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(processName).getLogic().startProcess(od, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_USERNAMES, users));
			}
		}
		else
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(Jbpm4Constants.EXECUTION_ID_PROPERTY, activityExecution.getId());
			map.put("startState", "author_correction");
			od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(processName).getLogic().startProcess(od, map);
		}
		
		if (waitForSignal != null)
			activityExecution.waitForSignal();
		else
		{
			try 
			{
				activityExecution.takeDefaultTransition();
			}
			catch (JbpmException exception)
			{
				if (od.getDocumentKind().getCn().equals("delegacja_ifpan"))
					activityExecution.end();
				else
					throw exception;
			}
		}
	}

	@Override
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception
	{	
		if (signalName != null)
		{
			if (!signalName.equals("to join"))
			{
				for (String key : findWithout(execution.getId()))
				{
					Jbpm4Provider.getInstance().getExecutionService().deleteProcessInstance(key);
				}
			}
			execution.take(signalName);
		}
		else
			execution.takeDefaultTransition();
	}

	private List<String> findWithout(String a) throws EdmException
	{
		NativeCriteria nc = DSApi.context().createNativeCriteria("JBPM4_VARIABLE", "d");
		nc.setProjection(NativeExps.projection()
		// ustalenie kolumn
				.addProjection("d.EXECUTION_"))
		// warunek where
				.add(NativeExps.like("d.STRING_VALUE_", a.split(".to")[0] + "%"))
				.add(NativeExps.eq("d.KEY_", "ds-execution-id"))
				.add(NativeExps.notEq("d.STRING_VALUE_",a))
				// order by i limit
				.addOrder(NativeExps.order().add("d.EXECUTION_", OrderType.DESC));
		CriteriaResult cr = nc.criteriaResult();

		List<String> executindIds = new ArrayList<String>();
		DSApi.context().createNativeCriteria("JBPM4_EXECUTION", "d");
		while (cr.next())
		{
			nc = DSApi.context().createNativeCriteria("JBPM4_EXECUTION", "d");
			nc.setProjection(NativeExps.projection()
			// ustalenie kolumn
					.addProjection("d.ID_"))
			// warunek where
					.add(NativeExps.eq("d.DBID_", new Long(cr.getString(0, null))));
			executindIds.add((String)nc.uniqueResult());
		}
		return executindIds;
		
	}
}
