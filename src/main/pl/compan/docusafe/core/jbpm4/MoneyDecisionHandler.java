package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * 
 * @author Kamil Skrzypek
 * 
 */
public class MoneyDecisionHandler implements DecisionHandler
{
	private final static Logger LOG = LoggerFactory.getLogger(MoneyDecisionHandler.class);

	private String field;
	private String ammount;
	private Float cnV, amV;
	private String result;

	public String decide(OpenExecution openExecution)
	{
		try
		{
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			LOG.info("money decision dla documentid = " + docId + " field = " + field);
			String cn = fm.getStringValue(field);
			if (cn == null)
			{
				throw new IllegalStateException("Nie wypełniono pola " + fm.getField(field).getName());
			}
			else
			{
				cnV = new Float(cn.replaceAll(" ", ""));
				amV = new Float(ammount.replaceAll(" ", ""));
			}
			// amV=1000, cnV=999
			if (amV > cnV)
			{
				LOG.info("money decision dla documentid = " + docId + " return less");
				return "less";
			}
			else if (amV < cnV)
			{
				LOG.info("money decision dla documentid = " + docId + " return more");
				return  "more";
			}
			else
			{
				// dla fipan zapotrzebowanie z powodu bledu w procesie
				if (AvailabilityManager.isAvailable("ifpan") && doc.getDocumentKind().getCn().equals("zapotrzeb_ifpan") && field.equals("KWOTA_BRUTTO"))
					return "more";
				else if (AvailabilityManager.isAvailable("ifpan") && doc.getDocumentKind().getCn().equals("zapotrzeb_ifpan") && field.equals("CONTRACTEVALUE"))
					return "less";
				else
					return "equals";
			}
				
		}
		catch (EdmException ex)
		{
			LOG.error(ex.getMessage(), ex);
			return "error";
		}
	}
}
