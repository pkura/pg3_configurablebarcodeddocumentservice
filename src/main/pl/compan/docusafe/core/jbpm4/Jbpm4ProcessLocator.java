package pl.compan.docusafe.core.jbpm4;

import com.google.common.base.Preconditions;
import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.task.Task;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.process.AbstractProcessLocator;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Podstawowy ProcessLocator proces�w Jbpm4, wymaga podania nazwy procesu jbpm4 paramatrze
 *
 * Korzysta z klasy Jbpm4ProcessInstance by opakowywa� API jbpmowe.
 *
 * Param.JBPM4_PROCESS_NAME lub w nazwie procesu (korzysta z tej warto�ci je�li poprzedni parametr nie zosta� okre�lony)
 */
public class Jbpm4ProcessLocator extends AbstractProcessLocator {
    private final static Logger log = LoggerFactory.getLogger(Jbpm4ProcessLocator.class);

    private final String name;

    public Jbpm4ProcessLocator(InstantiationParameters ip){
        Preconditions.checkNotNull(ip, "ip cannot be null");
        if(ip.hasParam(InstantiationParameters.Param.JBPM4_PROCESS_NAME)){
            name = ip.getParam(InstantiationParameters.Param.JBPM4_PROCESS_NAME).toString();
        } else {
            ip.assertParam(InstantiationParameters.Param.PROCESS_NAME);
            name = ip.getParam(InstantiationParameters.Param.PROCESS_NAME).toString();
        }
        log.info("Jbpm4ProcessLocator process name " + name);
    }

    /**
     * Zwraca instancj� procesu na podstawie danego identyfikatora lub null je�li nie mo�e go znale��
     * @param id
     * @return
     * @throws EdmException
     */
    @Override
    public final Jbpm4ProcessInstance lookupForId(Object id) throws EdmException {
        return lookupForId(name, id.toString());
    }

    private Jbpm4ProcessInstance lookupForId(String name, String processInstanceId) throws EdmException {
        return provide(name, processInstanceId);
    }

    @Override
    public final Jbpm4ProcessInstance lookupForTaskId(String id) throws EdmException {
        id = id.replaceAll("[^,]*,","");
        Task task = getTask(id);
	
	if(task == null) {
	    return null;
	}
	
        String executionId = task.getExecutionId();
        if(executionId.startsWith(name) || executionId.startsWith("read_only")) {
        	return provide(name, executionId);
        } else {
        	return null;
        }
    }

    private static Task getTask(String id) {
        return Jbpm4Provider.getInstance().getTaskService().getTask(id);
    }


    protected org.jbpm.api.ProcessInstance findProcessInstanceById(String processInstance){
        return Jbpm4Provider.getInstance().getExecutionService().findProcessInstanceById(processInstance);
    }

    @Override
    public final Collection<? extends Jbpm4ProcessInstance> lookupForDocument(Document doc) throws EdmException {
        return lookupForDocumentId(name, doc.getId());
    }

    protected Jbpm4ProcessInstance provide(String name, String processInstanceId){
        if(processInstanceId != null){
            org.jbpm.api.ProcessInstance pi = findProcessInstanceById(processInstanceId);
            return pi == null ? null : new Jbpm4ProcessInstance(name,pi);
        } else {
            return null;
        }
    }

    /**
     * Zwraca wszystkie niezako�czone procesy o danej nazwie procesu i danym id dokumentu
     * @param processName
     * @param documentId
     * @return
     * @throws EdmException
     */
    private List<Jbpm4ProcessInstance> lookupForDocumentId(String processName , long documentId) throws EdmException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Jbpm4ProcessInstance> ret;
        try{
            ps = DSApi.context().prepareStatement("SELECT jbpm4_id FROM ds_document_to_jbpm4 WHERE document_id = ? AND process_name = ?");
            ps.setLong(1, documentId);
            ps.setString(2, processName);
            rs = ps.executeQuery();
            ret = new ArrayList<Jbpm4ProcessInstance>();
            while(rs.next()){
                Jbpm4ProcessInstance pi = provide(processName,rs.getString(1));
                if(pi != null){
                    ret.add(pi);
                }
            }
            log.info("found " + ret.size() + " processes for name = " + processName + " and id = " + documentId);
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }
        return ret;
    }

    public static Iterable<String> processIds(long documentId) throws EdmException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<String> ret = null;
        try{
            ps = DSApi.context().prepareStatement("SELECT jbpm4_id FROM ds_document_to_jbpm4 WHERE document_id = ?");
            ps.setLong(1, documentId);
            rs = ps.executeQuery();
            ret = new ArrayList<String>();
            while(rs.next()){
                ret.add(rs.getString(1));
            }
            log.info("found " + ret.size() + " processes for doc.id = " + documentId);
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }
        return ret;
    }

    public static Iterable<String> taskIds(long documentId) throws EdmException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<String> ret = null;
        try{
            ps = DSApi.context().prepareStatement(
                 "SELECT jbpm4_task.dbid_ " +
                 "FROM ds_document_to_jbpm4, jbpm4_task, jbpm4_execution " +
                 "WHERE document_id = ?" +
                         " AND jbpm4_execution.id_ = ds_document_to_jbpm4.jbpm4_id" +
                         " AND jbpm4_execution.instance_ = jbpm4_task.procinst_");
            ps.setLong(1, documentId);
            rs = ps.executeQuery();
            ret = new ArrayList<String>();
            while(rs.next()){
          	  log.info("Found taskID: {}", rs.getString(1));
                ret.add(rs.getString(1));
            }
            log.info("found " + ret.size() + " processes for doc.id = " + documentId);
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }
        return ret;
    }

    public static long documentIdForProcess(String processInstanceId) throws EdmException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Long> ret = null;
        try{
            ps = DSApi.context().prepareStatement(
                 "SELECT document_id " +
                 "FROM ds_document_to_jbpm4 " +
                 "WHERE jbpm4_id = ?");
            ps.setString(1, processInstanceId);
            rs = ps.executeQuery();
            ret = new ArrayList<Long>();
            while(rs.next()){
                ret.add(rs.getLong(1));
            }
            log.info("found " + ret.size() + " documents for doc.id = " + processInstanceId);
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }
        return ret.get(0);
    }
    
    public static long documentIdForExtenedProcesssId(String processInstanceId) throws EdmException {
	Pattern p = Pattern.compile("(\\w+)(.)(\\d+)(.*)");
	Matcher m = p.matcher(processInstanceId);
	
	if (m.find()) {
	    processInstanceId=m.group(1)+m.group(2)+m.group(3);
	}
	return documentIdForProcess(processInstanceId);
    }

    public static long documentIdForActivity(String taskId) throws EdmException {
        return documentIdForProcess(
                Jbpm4Provider.getInstance().getExecutionService().findExecutionById(
                    getTask(taskId).getExecutionId())
                        .getProcessInstance().getId());
    }
}
