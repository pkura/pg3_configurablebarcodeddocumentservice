package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

public class GetProcessVariableDecision implements DecisionHandler
{
	String variableName = null;
	@Override
	public String decide(OpenExecution execution)
	{
		if (variableName!=null && execution.getVariable(variableName) != null)
		{
			return (String)execution.getVariable(variableName);
		}
		else
			return "normal";
	}

}
