package pl.compan.docusafe.core.jbpm4;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import org.jbpm.api.Execution;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.model.OpenExecution;

import com.google.common.base.Strings;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Ko�czy dzieci rodzica tego executiona, z wyj�tkiem tego.
 * czyt. zamyka pozosta�e taski z foreach'a
 */
public class CloseOtherExecutions implements ExternalActivityBehaviour {
	private static final Logger log = LoggerFactory.getLogger(CloseOtherExecutions.class);
	private static final long serialVersionUID = 1L;
	
	private String counterVariableName;

	@Override
	public void execute(ActivityExecution execution) throws Exception {
	    int counter = 0;
        List<Execution> executionToEnd = Lists.newArrayList();
        try {
			OpenExecution parentExecution;
			if ((parentExecution = execution.getParent()) != null) {
				for (Execution exec : parentExecution.getExecutions()) {
					if (!exec.equals(parentExecution)) {
						if (!exec.equals(execution) && !Execution.STATE_INACTIVE_JOIN.equals(exec.getState())) {
							executionToEnd.add(exec);
						} else {
							counter++;
						}
					}
				}
			}
            endExecutions(executionToEnd);
        } catch (Exception e) {
			log.error(e.getMessage(),e);
		}
        execution.setVariable(Strings.isNullOrEmpty(counterVariableName)?"count":counterVariableName, counter==0?1:counter);
        execution.takeDefaultTransition();
	}

    private void endExecutions(List<Execution> executionToEnd) {
        for (Execution exec : executionToEnd) {
            if (exec instanceof ActivityExecution) {
                ((ActivityExecution) exec).end();
            }
        }
    }

    @Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
		// TODO Auto-generated method stub

	}

}
