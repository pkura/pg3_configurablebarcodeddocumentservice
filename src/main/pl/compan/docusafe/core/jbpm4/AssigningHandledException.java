package pl.compan.docusafe.core.jbpm4;

import pl.compan.docusafe.core.EdmException;

/**
 * User: kk
 * Date: 21.01.14
 * Time: 23:57
 */
public class AssigningHandledException extends EdmException {
    public AssigningHandledException(String message) {
        super(message);
    }
}