/**
 * 
 */
package pl.compan.docusafe.core.jbpm4;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Handler wywo�uje metede assignee z logiki dokumentu. W logice dokumentu nale�y obs�u�y� 
 * do kogo ma by� przedekretowane pismo.
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 07-02-2014
 * ds_trunk_2014
 * AssigneeHandlerFromLogic.java
 */
public class AssigneeHandlerFromLogic implements AssignmentHandler {

	private static final long serialVersionUID = 1L;
	
	private String acceptationCn;
	private String fieldCnValue
	;
	private static final Logger log = LoggerFactory.getLogger(AssigneeHandler.class);
	
	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		boolean isOpened  = false;
		
		try {
			isOpened = DSApi.openContextIfNeeded();
			
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId,false);
			
			doc.getDocumentKind().logic().assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			if (isOpened) {
				DSApi.closeContextIfNeeded(isOpened);
			}
		}
		
	} 
	
	/**
	 * @param doc
	 * @param assignable
	 * @param openExecution
	 * @param acceptationCn - kod akceptacji z drzewa akceptacji
	 * @param fieldCnValue
	 * @param addToHistory 
	 * @return
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	public static boolean assign(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue, boolean addToHistory) throws UserNotFoundException, EdmException {
		boolean assigned = false;
		String fieldValue = null;
		String[] assigness = acceptationCn.split(",");
		log.debug("assignee operations for acceptationCn: {}, fieldCnValue: {}", acceptationCn, fieldCnValue);
		for (String ass : Arrays.asList(assigness))
		{
			List<String> users = new ArrayList<String>();
			List<String> divisions = new ArrayList<String>();
			if (AcceptanceDivisionImpl.findByCode(ass) != null && AcceptanceDivisionImpl.findByCode(ass).getOgolna() == 1)
				fieldValue = null;
			else if (fieldCnValue != null)
				fieldValue = doc.getFieldsManager().getEnumItem(fieldCnValue).getCn();

			for (AcceptanceCondition accept : AcceptanceCondition.find(ass, fieldValue))
			{
				if (accept.getUsername() != null && !users.contains(accept.getUsername()))
					users.add(accept.getUsername());
				if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
					divisions.add(accept.getDivisionGuid());
			}
			for (String user : users)
			{
				assignable.addCandidateUser(user);
				log.info("For accept " + ass + " and fieldValue " + fieldValue + " - candidateUser: " + user);
				if (addToHistory)
					AssigneeHandler.addToHistory(openExecution, doc, user, null);
				assigned = true;
			}
			for (String division : divisions)
			{
				assignable.addCandidateGroup(division);
				log.info("For accept " + ass + " and fieldValue " + fieldValue + " - candidateGroup: " + division);
				if (addToHistory)
					AssigneeHandler.addToHistory(openExecution, doc, null, division);
				assigned = true;
			}
		}
		return assigned;
	}
	
}
