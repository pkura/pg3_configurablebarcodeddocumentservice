package pl.compan.docusafe.core.jbpm4;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumerationField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;

import java.util.*;

public class AssigneeHandler implements AssignmentHandler
{
	public static final String GET_PROFILE_SQL =  "DECLARE @role_id int = ?, @div_id int = ?; select u.NAME from DS_DIVISION d join DS_USER_TO_DIVISION ud on d.ID=ud.DIVISION_ID join DS_USER u on ud.USER_ID=u.ID join dso_role_usernames rd on u.NAME=rd.username join ds_profile_to_role pr on pr.role_id=rd.role_id join ds_profile_to_user pu on pr.profile_id=pu.profile_id and pu.user_id=u.id and (pu.USERKEY is null or pu.USERKEY like '' or pu.USERKEY like '#' or pu.USERKEY like d.GUID) where rd.role_id=@role_id and d.ID=@div_id union select u.NAME from DS_DIVISION d join DS_USER_TO_DIVISION ud on d.ID=ud.DIVISION_ID join DS_USER u on ud.USER_ID=u.ID join dso_role_usernames rd on u.NAME=rd.username and rd.source ='own' where rd.role_id=@role_id and d.ID=@div_id;";
	private String acceptationCn;
	private String guid;
	private String user;
	private String fieldCnValue;
	private String field;
	private String fieldDataBaseUser;
	private String fieldDataBaseDiv;
	private String guidFromAdditionProperty;
	private String guidFromAdditionPropertyWithPermissionToWholeGroup;
    /**role laczone przez OR*/
	private String roleNames;
    /**profile laczone przez OR*/
	private String profileNames;
    /**profile laczone przez OR, wymaga implementacji {@link ProfileCodeTranslator} przez logik� dokumentu*/
	private String profileCodes;
    /**uprawnienia laczone przez OR*/
	private String permissionNames;
	private Boolean mustContainsUser;
	private String userFromVariable;
	private String addToHistory = "true";
	private String fieldCnSourceType;
	
	private final String PREVIOUS_USER = "assignee_to_previous_user";
	private final String CURRENT_USER = "current_user";
	private final String IMMEDIATE_SUPERVISOR = "immediate_supervisor";
	
	private static final Logger log = LoggerFactory.getLogger(AssigneeHandler.class);

	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception
	{

		boolean isOpened = true;
		if (!DSApi.isContextOpen())
		{
			isOpened = false;
			DSApi.openAdmin();
		}
		Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId,false);

		String fieldValue = null;
		boolean assigned = false;
		try
		{
			openExecution.setVariable(Jbpm4WorkflowFactory.REASSIGNMENT_TIME_VAR_NAME, new Date());
			// -1 z powodu braku synchornizacja z bazqa danych pomiedzy
			// stanami procesu.
			if (openExecution.getVariable("user") != null)
			{
				if (!openExecution.getVariable("user").equals("-1"))
				{
					assignable.addCandidateUser(openExecution.getVariable("user").toString());
					if (Boolean.parseBoolean(addToHistory))
						addToHistory(openExecution, doc, openExecution.getVariable("user").toString(), null);
					log.info("User is set in correction -CandidateUser: {}", openExecution.getVariable("user"));
					openExecution.setVariable("user", "-1");
					return;
				}
				else
					openExecution.removeVariable("user");
			}
			
			if (openExecution.getVariable(userFromVariable) != null) {
				String users = openExecution.getVariable(userFromVariable).toString();
				for (String user : users.split(",")) {
					assignable.addCandidateUser(user);
					if (Boolean.parseBoolean(addToHistory))
						addToHistory(openExecution, doc, user, null);
				}
				openExecution.removeVariable(userFromVariable);
				return;
			}
			
			if (PREVIOUS_USER.equals(acceptationCn))
			{
				
			}
			else if (CURRENT_USER.equals(acceptationCn))
			{
				assignable.addCandidateUser(DSApi.context().getPrincipalName());
				if (Boolean.parseBoolean(addToHistory))
					addToHistory(openExecution, doc, DSApi.context().getPrincipalName(), null);
				assigned = true;
			}
			/**
			 * Przypisanie zadania bezpo�redniemu prze�o�onemu u�ytkownika, kt�ry jest wybrany w polu o CN == fieldCnValue
			 * na dokumenice, np. w polu "WNIOSKODAWCA". Bezpo�redni prze�o�eni musz� mie� w�a��iwo�� 'isSupervisor' ustawion� na true.
			 */
			else if (IMMEDIATE_SUPERVISOR.equals(acceptationCn) && fieldCnValue != null)
			{
                		FieldsManager fm = doc.getFieldsManager();
                		if ("FORK".equalsIgnoreCase(fieldCnValue)) {
                		    Long userId = (Long) openExecution.getVariable("userId");
                		    DSUser supervisor = DSUser.findById(userId);
                		    assignable.addCandidateUser(supervisor.getName());
                		} else if (fm.getField(fieldCnValue).getType().equals("document-autor")){
                            DSUser user = DSUser.findById(Long.valueOf((String) fm.getKey(fieldCnValue)));
                            for (DSUser supervisor : user.getImmediateSupervisors()) {
                                assignable.addCandidateUser(supervisor.getName());
                            }
                        } else {
                		    DSUser user = DSUser.findById(fm.getEnumItem(fieldCnValue).getId().longValue());
                		    for (DSUser supervisor : user.getImmediateSupervisors()) {
                			assignable.addCandidateUser(supervisor.getName());
                		    }
                		}
			}
			else if (guid != null)
			{
				for (String g : guid.split(","))
				{
					assignable.addCandidateGroup(g);
					if (Boolean.parseBoolean(addToHistory))
						addToHistory(openExecution, doc, null, g);
					assigned = true;
				}
			}
			else if (user != null)
			{
				if (user.equals("author")) 
				{
					assignable.addCandidateUser(doc.getAuthor());
					if (Boolean.parseBoolean(addToHistory))
						addToHistory(openExecution, doc, doc.getAuthor(), null);
				}
				else
				{
					assignable.addCandidateUser(user);
					if (Boolean.parseBoolean(addToHistory))
						addToHistory(openExecution, doc, user, null);
				}
				assigned = true;
			}
			/**
			 * przypisanie zadania do konkretnego u?ytkownika wybranego za pomoc? pola "dsuser" (w tym mulitple) albo "dsdivision" (nie obsluguje multiple, dopisac!) w dokumencie
			 */
			else if (field != null)
			{
				FieldsManager fm = doc.getFieldsManager();
				Field userOrDivisionField = fm.getField(field);
				if (userOrDivisionField instanceof DSUserEnumField)
				{
					String userName = null;
					if (userOrDivisionField.isMultiple())
					{
						List<Long> userIds = (List<Long>)fm.getKey(field);
						for (Long userId : userIds)
						{
							userName = DSUser.findById(userId).getName();
							assignable.addCandidateUser(userName);
							if (Boolean.parseBoolean(addToHistory))
								addToHistory(openExecution, doc, userName, null);
						}
					}
					else
					{
						userName = fm.getEnumItemCn(field);
						//jesli jest null to niech lepiej idzie do autora niz w kosmos
						if(StringUtils.isEmpty(userName))
							userName = DSApi.context().getPrincipalName();
						assignable.addCandidateUser(userName);
						if (Boolean.parseBoolean(addToHistory))
							addToHistory(openExecution, doc, userName, null);
					}
				}
				else 
				{
					if (fieldCnSourceType != null && "username".equals(fieldCnSourceType)) {
						try {
							String username = fm.getEnumItemCn(field);
							assignable.addCandidateUser(username);
							if (Boolean.parseBoolean(addToHistory))
								addToHistory(openExecution, doc, username, null);
						} catch (EdmException e) {
							log.error("cant find any user to assignee for user field: " + field + ", go to admin");
							String admin = GlobalPreferences.getAdminUsername();
							assignable.addCandidateUser(admin);
							if (Boolean.parseBoolean(addToHistory))
								addToHistory(openExecution, doc, admin, null);
						}
					} else { //fieldCnSourceType="guid"
						if (mustContainsUser != null && mustContainsUser) {
							String guid = fm.getEnumItemCn(field);
							DSDivision div = DSDivision.find(guid);
							try {
								while (div.getOriginalUsers().length == 0) {
									div = div.getParent();
								}
								assignable.addCandidateGroup(div.getGuid());
								if (Boolean.parseBoolean(addToHistory))
									addToHistory(openExecution, doc, null, div.getGuid());
							} catch (EdmException e) {
								log.error("cant find any user to assignee for division field: " + field + ", go to admin");
								String admin = GlobalPreferences.getAdminUsername();
								assignable.addCandidateUser(admin);
								if (Boolean.parseBoolean(addToHistory))
									addToHistory(openExecution, doc, admin, null);
							}
						} else {
							String guid = fm.getEnumItemCn(field);
							assignable.addCandidateGroup(guid);
							if (Boolean.parseBoolean(addToHistory))
								addToHistory(openExecution, doc, null, guid);
						}
					}
				}
				assigned = true;
			}
			else if(fieldDataBaseUser != null){
				FieldsManager fm = doc.getFieldsManager();
				Field dataBaseField = fm.getField(fieldDataBaseUser);
				if (dataBaseField instanceof DataBaseEnumField){
					DSUser user = DSUser.findById(fm.getLongKey(dataBaseField.getCn()));
					assignable.addCandidateUser(user.getName());
			        AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
			        assigned = true;
				}
			}
			else if(fieldDataBaseDiv != null){
				FieldsManager fm = doc.getFieldsManager();
				Field dataBaseField = fm.getField(fieldDataBaseDiv);
				if (dataBaseField instanceof DataBaseEnumField){
					String id=fm.getStringKey(dataBaseField.getCn());
					String guid=((DataBaseEnumField)dataBaseField).getEnumItem(Integer.parseInt(id)).getCn();				
					assignable.addCandidateGroup(guid);
					if (Boolean.parseBoolean(addToHistory))
						addToHistory(openExecution, doc, null,guid);
			        assigned = true;
			        
			        //nadanie uprawnien do odczytu na ta grupe
			        PermissionBean perm=new PermissionBean(ObjectPermission.READ,guid, ObjectPermission.GROUP,"Dokumenty zwyk�y- odczyt");
					String groupName = perm.getGroupName();
					if (groupName == null) {
						groupName = perm.getSubject();
					}
					DSApi.context().grant(Document.find(docId), perm);
					DSApi.context().session().flush();
				}
			}
			else if (guidFromAdditionProperty != null) {
				String guids;
				if ((guids = Docusafe.getAdditionProperty(guidFromAdditionProperty)) != null) {
					for (String g : guids.split(","))
					{
						assignable.addCandidateGroup(g);
						if (Boolean.parseBoolean(addToHistory))
							addToHistory(openExecution, doc, null, g);
						assigned = true;
					}
				} else {
					throw new EdmException("Nie ustawiono dzia�u");
				}
			}
			else if (guidFromAdditionPropertyWithPermissionToWholeGroup != null) {
				String guids;
				if ((guids = Docusafe.getAdditionProperty(guidFromAdditionPropertyWithPermissionToWholeGroup)) != null) {
					for (String g : guids.split(","))
					{
						assignable.addCandidateGroup(g);
						if (Boolean.parseBoolean(addToHistory))
							addToHistory(openExecution, doc, null, g);
						assigned = true;

						java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
						//nadanie uprawnien na ta grupe
						perms.add(new PermissionBean(ObjectPermission.READ, g, ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
						perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, g, ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
						perms.add(new PermissionBean(ObjectPermission.MODIFY, g, ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
						perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, g, ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - modyfikacja"));
						perms.add(new PermissionBean(ObjectPermission.DELETE, g, ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));
						for (PermissionBean perm : perms) {
							if (perm.isCreateGroup()
									&& perm.getSubjectType().equalsIgnoreCase(
											ObjectPermission.GROUP)) {
								String groupName = perm.getGroupName();
								if (groupName == null) {
									groupName = perm.getSubject();
								}
							}
							DSApi.context().grant(Document.find(docId), perm);
							DSApi.context().session().flush();
						}
					}
				}
			}
            else if (StringUtils.isNotEmpty(roleNames)){
                String [] roles = TextUtils.splitNames(roleNames,",");
                assigned = AssigneeHandlerUtils.assignByRoleNameSet(doc, assignable, openExecution, roles);
            }
            else if (StringUtils.isNotEmpty(profileNames) || StringUtils.isNotEmpty(profileCodes)){
                if (StringUtils.isNotEmpty(profileCodes)){
                    //profileNames = StringUtils.isNotEmpty(profileNames) ? profileNames : profileCodes;
                    DocumentLogic logic = doc.getDocumentKind().logic();
                    if (logic instanceof ProfileCodeTranslator){
                        String translation = ((ProfileCodeTranslator) logic).translateProfilesCode(profileCodes);
                        //if (StringUtils.isNotEmpty(translation))
                        profileNames = translation;
                    }
                    else
                        throw new Exception("Error: Logic not implemented " + ProfileCodeTranslator.class.getName());
                }
                String [] profiles = TextUtils.splitNames(profileNames,",");
                assigned = AssigneeHandlerUtils.assignByProfileSet(doc, assignable, openExecution, profiles);
            }
            else if (StringUtils.isNotEmpty(permissionNames)){
                String [] permissions = TextUtils.splitNames(permissionNames,",");
                assigned = AssigneeHandlerUtils.assignByPermissionSet(doc, assignable, openExecution, permissions);
            }
			else if (!doc.getDocumentKind().logic().assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue))
			{
				String[] assigness = acceptationCn.split(",");
				log.debug("assignee operations for acceptationCn: {}, fieldCnValue: {}", acceptationCn, fieldCnValue);
				for (String ass : Arrays.asList(assigness))
				{
					if (assigned)
						break;

					List<String> users = new ArrayList<String>();
					List<String> divisions = new ArrayList<String>();
					if (AcceptanceDivisionImpl.findByCode(ass) != null && AcceptanceDivisionImpl.findByCode(ass).getOgolna() == 1)
						fieldValue = null;
					else if (fieldCnValue != null)
						fieldValue = doc.getFieldsManager().getEnumItem(fieldCnValue).getCn();

					for (AcceptanceCondition accept : AcceptanceCondition.find(ass, fieldValue))
					{
						if (accept.getUsername() != null && !users.contains(accept.getUsername()))
							users.add(accept.getUsername());
						if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
							divisions.add(accept.getDivisionGuid());
					}
					for (String user : users)
					{
						assignable.addCandidateUser(user);
						log.info("For accept " + ass + " and fieldValue " + fieldValue + " - candidateUser: " + user);
						if (Boolean.parseBoolean(addToHistory))
							addToHistory(openExecution, doc, user, null);
						assigned = true;
					}
					for (String division : divisions)
					{
						assignable.addCandidateGroup(division);
						log.info("For accept " + ass + " and fieldValue " + fieldValue + " - candidateGroup: " + division);
						if (Boolean.parseBoolean(addToHistory))
							addToHistory(openExecution, doc, null, division);
						assigned = true;
					}
				}
				if (!assigned)
					throw new Exception("Error: Not Assigned for " + acceptationCn);

			}
			
		}
		catch (EdmException e)
		{
			log.error(e.getMessage());
			throw e;
		}
		finally
		{
			if (DSApi.isContextOpen() && !isOpened)
				DSApi.close();
		}
		
	}

	/**
	 * @param openExecution
	 * @param doc
	 * @throws EdmException
	 * @throws UserNotFoundException
	 */
	public static void addToHistory(OpenExecution openExecution, OfficeDocument doc, String user, String guid) throws EdmException, UserNotFoundException {
		AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
		ahe.setSourceUser(DSApi.context().getPrincipalName());
		ahe.setProcessName(openExecution.getProcessDefinitionId());
		if (user == null) {
			ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
			ahe.setTargetGuid(guid);
		} else {
			ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
			ahe.setTargetUser(user);
		}
		DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
		if (divs != null && divs.length > 0)
			ahe.setSourceGuid(divs[0].getName());
		ahe.setCtime(new Date());
		ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		

		String status = null;
	    try
	    {
	       status = ((EnumerationField) doc.getFieldsManager().getField("STATUS")).getEnumItemByCn("acception").getTitle();
	    }
	    catch (Exception e)
	    {
		    try{
		    	log.warn(e.getMessage(), e);
		    	status = doc.getFieldsManager().getField("STATUS").getEnumItemByCn(openExecution.getActivity().getName()).getTitle();
	        } catch (Exception ex) {
	            try {
	            	log.warn(ex.getMessage(), ex);
	                status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
				} catch (Exception e2) {
					log.warn(e2.getMessage(), e2);
				}
	        }
	    }

	    if (status != null)
	        ahe.setStatus(status);
		
		doc.addAssignmentHistoryEntry(ahe);
	}

	
	public String getGuid()
	{
		return guid;
	}

	public void setGuid(String guid)
	{
		this.guid = guid;
	}

	public String getUser()
	{
		return user;
	}

	public void setUser(String user)
	{
		this.user = user;
	}

	public String getAcceptationCn()
	{
		return acceptationCn;
	}

	public void setAcceptationCn(String acceptationCn)
	{
		this.acceptationCn = acceptationCn;
	}

	public String getFieldCnValue()
	{
		return fieldCnValue;
	}

	public void setFieldCnValue(String fieldCnValue)
	{
		this.fieldCnValue = fieldCnValue;
	}

    public String getRoleNames() {
        return roleNames;
    }

    public String getProfileNames() {
        return profileNames;
    }

    public String getPermissionNames() {
        return permissionNames;
    }

    public void setRoleNames(String roleNames) {
        this.roleNames = roleNames;
    }

    public void setProfileNames(String profileNames) {
        this.profileNames = profileNames;
    }

    public void setPermissionNames(String permissionNames) {
        this.permissionNames = permissionNames;
    }

    public String getProfileCodes() {
        return profileCodes;
    }

    public void setProfileCodes(String profileCodes) {
        this.profileCodes = profileCodes;
    }

    //nieu?ywane
	protected void addPermission(OfficeDocument doc, Assignable assignable, OpenExecution openExecution) throws EdmException{
	    Set<String> assignUsersForPermission = new HashSet<String>();
	    Set<String> assignGroupsForPermission = new HashSet<String>();
	    
	    String procId = openExecution.getProcessInstance().getId();
	    
	    List<Task> tasks = Jbpm4Provider.getInstance().getTaskService().createTaskQuery().processInstanceId(procId).list();
	        for(Task task: tasks){
	            List<Participation> participations = Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(task.getId());
	            for(Participation p : participations){
	        	if (p.getUserId()!=null) {
	        	    assignUsersForPermission.add(p.getUserId());
	        	} else if (p.getGroupId() != null) {
	        	    assignGroupsForPermission.add(p.getGroupId());
	        	}
	            }
	        }
	        
	        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		    
		for (String name: assignUsersForPermission) {
			DSUser user = DSUser.findByUsername(name);
			String fullName = user.getLastname()+ " " +user.getFirstname();
			
			perms.add(new PermissionBean(ObjectPermission.READ, name, ObjectPermission.USER, fullName+" ("+name+")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, name, ObjectPermission.USER, fullName+" ("+name+")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, name, ObjectPermission.USER, fullName+" ("+name+")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, name, ObjectPermission.USER, fullName+" ("+name+")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, name, ObjectPermission.USER, fullName+" ("+name+")"));
		}
		for (String guid : assignGroupsForPermission) {
			DSDivision division = DSDivision.find(guid);
			DSUser[] users = division.getUsers();

			for (DSUser user : users) {
				if (!assignUsersForPermission.contains(user.getName())) {
					String name = user.getName();
					String fullName = user.getLastname() + " " + user.getFirstname();

					perms.add(new PermissionBean(ObjectPermission.READ, name, ObjectPermission.USER, fullName + " (" + name + ")"));
					perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, name, ObjectPermission.USER, fullName + " (" + name + ")"));
					perms.add(new PermissionBean(ObjectPermission.MODIFY, name, ObjectPermission.USER, fullName + " (" + name + ")"));
					perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, name, ObjectPermission.USER, fullName + " (" + name + ")"));
					perms.add(new PermissionBean(ObjectPermission.DELETE, name, ObjectPermission.USER, fullName + " (" + name + ")"));

				}
			}
		}
		
		DocumentLogic docLogic = doc.getDocumentKind().logic();
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(doc);
		perms.addAll(documentPermissions);
		if (docLogic instanceof AbstractDocumentLogic) 
		    ((AbstractDocumentLogic)docLogic).setUpPermission(doc, perms);
	}

    public interface ProfileCodeTranslator {
        String translateProfilesCode(String profilesCodes);
    }
}
