package pl.compan.docusafe.core.jbpm4;

import org.drools.lang.DRLParser.field_constraint_return;
import org.jbpm.api.activity.ActivityExecution;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.List;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import org.jbpm.api.task.Participation;
import org.jbpm.pvm.internal.task.TaskImpl;

import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Bardziej zaawansowany handler dekretuj�cy na wiele u�ytkownik�w / dzia��w
 * w zale�no�ci od przekazanych parametr�w.
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class FirstTaskAssignmentHandler implements AssignmentHandler {
	private static final Logger LOG = LoggerFactory
			.getLogger(FirstTaskAssignmentHandler.class);

                      
	
    
  /*  // usuwanie uprawnie�
    public void removePermission(OfficeDocument doc, Assignable assignable, OpenExecution openExecution) throws EdmException{
	    Set<String> assignUsersForPermission = new HashSet<String>();
	    Set<String> assignGroupsForPermission = new HashSet<String>();
	    
	    String procId = openExecution.getProcessInstance().getId();
	    
	    List<Task> tasks = Jbpm4Provider.getInstance().getTaskService().createTaskQuery().processInstanceId(procId).list();
	        for(Task task: tasks){
	            List<Participation> participations = Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(task.getId());
	            for(Participation p : participations){
	        	if (p.getUserId()!=null) {
	        	    assignUsersForPermission.add(p.getUserId());
	        	} else if (p.getGroupId() != null) {
	        	    assignGroupsForPermission.add(p.getGroupId());
	        	}
	            }
	        }
	        
	        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		    
		for (String name: assignUsersForPermission) {
			DSUser user = DSUser.findByUsername(name);
			String fullName = user.getLastname()+ " " +user.getFirstname();
			
			perms.remove(new PermissionBean(ObjectPermission.READ, name, ObjectPermission.USER, fullName+" ("+name+")"));
			perms.remove(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, name, ObjectPermission.USER, fullName+" ("+name+")"));
			perms.remove(new PermissionBean(ObjectPermission.MODIFY, name, ObjectPermission.USER, fullName+" ("+name+")"));
			perms.remove(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, name, ObjectPermission.USER, fullName+" ("+name+")"));
			perms.remove(new PermissionBean(ObjectPermission.DELETE, name, ObjectPermission.USER, fullName+" ("+name+")"));
		}
		for (String guid : assignGroupsForPermission) {
			DSDivision division = DSDivision.find(guid);
			DSUser[] users = division.getUsers();

			for (DSUser user : users) {
				if (!assignUsersForPermission.contains(user.getName())) {
					String name = user.getName();
					String fullName = user.getLastname() + " " + user.getFirstname();

					perms.remove(new PermissionBean(ObjectPermission.READ, name, ObjectPermission.USER, fullName + " (" + name + ")"));
					perms.remove(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, name, ObjectPermission.USER, fullName + " (" + name + ")"));
					perms.remove(new PermissionBean(ObjectPermission.MODIFY, name, ObjectPermission.USER, fullName + " (" + name + ")"));
					perms.remove(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, name, ObjectPermission.USER, fullName + " (" + name + ")"));
					perms.remove(new PermissionBean(ObjectPermission.DELETE, name, ObjectPermission.USER, fullName + " (" + name + ")"));

				}
			}
		}
		
		DocumentLogic docLogic = doc.getDocumentKind().logic();
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(doc);
		perms.addAll(documentPermissions);
		if (docLogic instanceof AbstractDocumentLogic) 
		    ((AbstractDocumentLogic)docLogic).setUpPermission(doc, perms);
	}
	
	public void execute(Assignable assignable, OpenExecution openExecution) throws Exception {

		assign(assignable , openExecution);
	}
	
	*/
	
	
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> arg2) throws Exception {
		if (signalName != null)
			execution.take(signalName);
		else 
			execution.takeDefaultTransition();
	}

    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
        LOG.info("assign !!");
        String username = (String) openExecution.getVariable(ASSIGN_USER_PARAM);
        String guid = (String) openExecution.getVariable(ASSIGN_DIVISION_GUID_PARAM);
        String assignee = (String) openExecution.getVariable(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY);
        List<String> targetUsernames = null;
        if(assignee != null){
            assignable.addCandidateUser(assignee);
        }
        else {
            targetUsernames = (List<String>) openExecution.getVariable(Jbpm4Constants.READ_ONLY_ASSIGNMENT_USERNAMES);
            List<String> targetGuids = (List<String>) openExecution.getVariable(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS);
            if(targetUsernames != null && targetUsernames.size() > 0)
            	username = targetUsernames.get(0);
            if(targetGuids != null && targetGuids.size() > 0)
            	guid = targetGuids.get(0);
        }
		if (AvailabilityManager.isAvailable("PAA")) {
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			Date date = (Date) Calendar.getInstance().getTime();
			DateUtils data2 = new DateUtils();
			String dataa = data2.formatCommonDateTime(date);
			if (targetUsernames != null) {
				for (String usernamePaa : targetUsernames) {
					assignable.addCandidateUser(usernamePaa);

					doc.addWorkHistoryEntry(Audit.create(DSApi.context()
							.getPrincipalName(), DSApi.context()
							.getPrincipalName(),
							"Dokument zosta� zdekretowany  w celu Zapoznania na u�ytkownika  : "
									+ username + " w dniu : " + dataa));

				}
			}
		}
		else if(username != null || guid != null)
        {
        	 
             LOG.info("trying to assign, execution params : assignee = '{}', guid = '{}'", username, guid);

             if(guid != null && guid.equalsIgnoreCase("null"))
             {
             	LOG.error("Przekaza� guid jako string null zmieniam na ROOT");
             	guid = null;
             	
             }
             if(username != null){
             	try {
                     DSUser.findByUsername(username);
                 } catch (UserNotFoundException ex) {
                     LOG.error("user '{}' not found - assigning 'admin'", username);
                     username = "admin";
                 }
             }
             if(guid != null){
             	try {
                     DSDivision.find(guid);
                 } catch (DivisionNotFoundException ex) {
                 	LOG.error("guid '{}' not found - assigning 'admin'", guid);
                     username = "admin";
                     guid = null;
                 }
             }
             if(guid != null && username != null){
             	((TaskImpl) assignable).addParticipation(username, guid, Participation.CANDIDATE);
             }
             else if (guid != null){
            	 assignable.addCandidateGroup(guid);
             }
             else if(username != null)
             {
             	((TaskImpl) assignable).addParticipation(username, DSDivision.ROOT_GUID, Participation.CANDIDATE);
             }
             else
             {
             	((TaskImpl) assignable).addParticipation("admin", DSDivision.ROOT_GUID, Participation.CANDIDATE);
             }
        }
        else
        {
	        if(assignee != null){
	            assignable.addCandidateUser(assignee);
	        } else {
	            targetUsernames = (List<String>) openExecution.getVariable(Jbpm4Constants.READ_ONLY_ASSIGNMENT_USERNAMES);
	            List<String> targetGuids = (List<String>) openExecution.getVariable(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS);
	            if(targetGuids != null){
	                for(String sguid: targetGuids){
	                    assignable.addCandidateGroup(sguid);
	                }
	            }
	            
	            if(targetUsernames != null){
	                for(String susername: targetUsernames){
	                    assignable.addCandidateUser(susername);
	                }
	            }
	        }
        }
    }
}
