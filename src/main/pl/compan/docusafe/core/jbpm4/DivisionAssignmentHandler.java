package pl.compan.docusafe.core.jbpm4;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import org.jfree.util.Log;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;

import java.util.Date;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class DivisionAssignmentHandler implements AssignmentHandler {

    /**
     * Nazwa pola dokumentu typu dsdivision - kluczem pola jest id dzia�u
     */
    public String divisionFieldCn;
    /**
     * Nazwa słownika, w którym wyszukiwane ma być pole dsdivision - kluczem pola jest id dzia�u
     */
    public String dictionaryCn;
    public String divisionCodeFieldCn;
    /**
     * Nazwa zmiennej procesu, która przechowuje id działu
     */
    public String divisionIdVariableName;
    /**
     * Kod dzia�u z kt�rego nale�y zacz�� wyszukiwanie u�ytkownika o okre�lonym profilu
     */
    public String divisionCodeFromAdds;
    /**
     * Kod dzia�u z kt�rego nale�y zacz�� wyszukiwanie u�ytkownika o okre�lonym profilu
     */
    public String divisionCodeIfNotSet;

    @Override
    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
        DSContextOpener contextOpener = DSContextOpener.openAsAdminIfNeed();

        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
            FieldsManager fm = doc.getFieldsManager();

            DSDivision division = getDivision(openExecution, fm);

            openExecution.setVariable(Jbpm4WorkflowFactory.REASSIGNMENT_TIME_VAR_NAME, new Date());
            boolean assigned = AssigneeHandlerUtils.assignToDivision(doc, assignable, openExecution, division);

            if (!assigned)
                throw new Exception("Not assigned to division " + division.getName());
        } catch (EdmException e) {
            Log.error(e.getMessage());
            throw e;
        } finally {
            contextOpener.closeIfNeed();
        }
    }

    public DSDivision getDivision(OpenExecution openExecution, FieldsManager fm) throws Exception {
        DSDivision division = null;

        if (StringUtils.isNotBlank(dictionaryCn)) {


            //todo


            Object dictKey = fm.getKey(dictionaryCn);
            Object dictField = fm.getField(dictionaryCn);

            //dict.getdivisionFieldCn);
        } else if (StringUtils.isNotBlank(divisionFieldCn)) {
            Long divisionId = Long.parseLong(fm.getKey(divisionFieldCn).toString());
            division = DSDivision.findById(divisionId);

        } else if (StringUtils.isNotBlank(divisionCodeFieldCn)) {
            String divisionCode = (String) fm.getKey(divisionCodeFieldCn);
            division = DSDivision.findByCode(divisionCode);

        } else if (StringUtils.isNotBlank(divisionIdVariableName)) {
            Long divisionId = getLongFromExecution(openExecution, divisionIdVariableName);
            if (divisionId == null)
                throw new Exception("Class of variable=" + divisionIdVariableName + " can not be casted to Long");
            division = DSDivision.findById(divisionId);

        } else if (StringUtils.isNotBlank(divisionCodeFromAdds)) {
            String code = Docusafe.getAdditionPropertyOrDefault(divisionCodeFromAdds, StringUtils.isNotBlank(divisionCodeIfNotSet) ? divisionCodeIfNotSet : "-1");
            division = DSDivision.findByCode((code));

        } else
            throw new Exception("No profile id source");


        if (division == null) throw new Exception("No division");
        return division;
    }

    public static Long getLongFromExecution(OpenExecution openExecution, String variableName) {
        Object profileIdObj = openExecution.getVariable(variableName);
        if (profileIdObj instanceof Integer)
            return ((Integer) profileIdObj).longValue();
        if (profileIdObj instanceof Long)
            return (Long) profileIdObj;
        return null;
    }
}
