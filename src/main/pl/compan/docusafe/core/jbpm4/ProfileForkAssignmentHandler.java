package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import org.jfree.util.Log;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * Przypisanie procesu-zadania do użytkowników przygotowanych przez {@code ProfilePrepareProcessListener}
 *
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ProfileForkAssignmentHandler implements AssignmentHandler {

    private String varName;

    @Override
    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
        DSContextOpener contextOpener = DSContextOpener.openAsAdminIfNeed();
        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);

            String assignee = (String) openExecution.getVariable(varName);
            ProfilePrepareProcessListener.AssigneeType assigneeType = ProfilePrepareProcessListener.getAssigneeType(assignee);
            String assigneeValue = ProfilePrepareProcessListener.getAssigneeValue(assignee);

            boolean assigned = false;
            switch (assigneeType) {
                case DIVISION_GUID:
                    assigned = AssigneeHandlerUtils.assignToDivision(doc, assignable, openExecution, assigneeValue);
                    break;
                case USER_NAME:
                    assigned = AssigneeHandlerUtils.assignToUser(doc, assignable, openExecution, assigneeValue);
                    break;
            }

            if (!assigned)
                throw new Exception("Not assigned");

        } catch (EdmException e) {
            Log.error(e.getMessage());
            throw e;
        } finally {
            contextOpener.closeIfNeed();
        }
    }

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName = varName;
    }
}
