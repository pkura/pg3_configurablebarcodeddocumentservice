package pl.compan.docusafe.core.jbpm4;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.dbutils.DbUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.process.form.ActivityController;
import pl.compan.docusafe.process.form.Button;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Klasa wspieraj�ca opcje znane z procesu r�cznego tj - dekretacja na mnie, przywracanie na list� zada� (po zamkni�ciu procesu) itp
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ManualProcessView extends ActivitiesExtensionProcessView {
    private final static Logger LOG = LoggerFactory.getLogger(ManualProcessView.class);

    public ManualProcessView(final InstantiationParameters ip) throws Exception {
        super(ip);
    }

    @Override
    protected Multimap<String, Object> provideActivities(Jbpm4ProcessInstance pi,
                                                                        ProcessActionContext context,
                                                                        ActivityController ac) {
//        LOG.info("provideActivities : {}, {}, {}", pi, context, pe);
        if((context.getActionType() == ProcessActionContext.ActionType.ARCHIVE_VIEW
            || context.getActionType() == ProcessActionContext.ActionType.SUMMARY_VIEW) && pi.isEnded()){
            //przywr�cenie na list� zada�
            Button restart = new Button();
            restart.setForceRemark(false);
            restart.setLabel("Przywr�� na list� zada�");
            restart.setName("reopen-wf");
//            LOG.info("isEnded : return {}", );
            LOG.info("return reopen-wf");
            Multimap<String, Object> ret = ArrayListMultimap.create();
            ret.put("activities", restart);
            return ret;
        } else if(context.getActionType() == ProcessActionContext.ActionType.ARCHIVE_VIEW && !pi.isEnded()) {
            //przeka� do mnie
            if(!iHaveThisDoc(pi.getId())){
                LOG.info("iDontHaveThisDoc !!!!");
                Button assignMe = new Button();
                assignMe.setForceRemark(false);
                assignMe.setLabel("Przeka� do mnie");
                assignMe.setName("assign-me");
                Multimap<String, Object> ret = ArrayListMultimap.create();
                ret.put("activities", assignMe);
                return ret;
            } else {
                LOG.info("iHaveThisDoc !!!!");
                return ArrayListMultimap.create();
            }
        } else {
            return super.provideActivities(pi, context, ac);
        }
    }

    private boolean iHaveThisDoc(String jbpmId){
        LOG.info("iHaveThisDoc : {}", jbpmId);
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement(
                    "select 1 from dsw_jbpm_tasklist, ds_document_to_jbpm4 "
                    + " where dsw_jbpm_tasklist.document_id = ds_document_to_jbpm4.document_id "
                    + " and ds_document_to_jbpm4.jbpm4_id = ?"
                    + " and assigned_resource = ?");
            ps.setString(1, jbpmId);
            ps.setString(2, DSApi.context().getPrincipalName());
            rs = ps.executeQuery();
            return rs.next();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            DSApi.context().closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }
    }
}

