package pl.compan.docusafe.core.jbpm4;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class MultipleExecutionsException extends IllegalStateException{
    public MultipleExecutionsException(String message, Throwable cause) {
        super(message, cause);
    }

    public MultipleExecutionsException(String s) {
        super(s);
    }
}
