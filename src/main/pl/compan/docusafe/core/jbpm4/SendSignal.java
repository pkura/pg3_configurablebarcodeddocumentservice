package pl.compan.docusafe.core.jbpm4;

import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

public class SendSignal implements ExternalActivityBehaviour
{
	String signalName = null;

	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Object dependExecutionId = execution.getVariable(Jbpm4Constants.PARENT_EXECUTION_ID_PROPERTY);

		try
		{
			if (dependExecutionId != null)
				Jbpm4Provider.getInstance().getExecutionService().signalExecutionById((String) dependExecutionId, signalName);
			execution.takeDefaultTransition();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	@Override
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> arg2) throws Exception
	{
		if (signalName != null)
			execution.take(signalName);
		else 
			execution.takeDefaultTransition();
	}
}
