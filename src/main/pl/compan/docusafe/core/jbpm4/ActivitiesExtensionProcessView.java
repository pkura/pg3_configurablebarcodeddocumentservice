package pl.compan.docusafe.core.jbpm4;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.process.*;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.process.form.ActivityController;
import pl.compan.docusafe.process.form.AttachmentInfo;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.jbpm4.ProcessExtensionsCache;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.ExtendedRenderBean;
import pl.compan.docusafe.web.common.RenderBean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Widok wy�wietlaj�cy dost�pne akcje dla procesu w przypadku bazuj�c na pliku xml.
 * (patrz https://www2.hosted-projects.com/trac/com-pan/docusafe/wiki/jbpm4)
 *
 * Klasa wsp�pracuje tylko z procesami jbpm4 (Jbpm4ProcessInstance)
 *
 * Dodatkowe parametry:
 *
 * archive-view-show - czy wszystkie procesy powi�zane z dokumentem maj� si� wy�wietla� w repozytorium,
 * domy�lnie false
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ActivitiesExtensionProcessView extends AbstractProcessView {

    protected final static Logger log = LoggerFactory.getLogger(ActivitiesExtensionProcessView.class);

    /** Domy�lny szablon - zwraca wszystkie akcje dost�pne w danej chiwli dla procesu na podstawie xml'a activities */
    protected final static String DEFAULT_TEMPLATE = "jbpm4-default-process-view.vm";
    protected final static String NEW_DEFAULT_TEMPLATE = "jbpm4-new-default-process-view.vm";
    /** Szablon dla administratora - poza tym co w DEFAULT_TEMPLATE, wy�wietla tak�e dane procesu (zmienne itp) */
    private final static String ADMIN_TEMPLATE = "jbpm4-admin-process-view.vm";

    private final boolean archiveViewShow;

    public ActivitiesExtensionProcessView(final InstantiationParameters ip) throws Exception{
        archiveViewShow = Boolean.parseBoolean(
                ip.getParamOrDefault(InstantiationParameters.Param.ARCHIVE_VIEW_SHOW, "false"));
    }

    public ExtendedRenderBean render(ProcessInstance pi, ProcessActionContext context, String defaultTemplate) throws EdmException {
        return render((Jbpm4ProcessInstance) pi, context, defaultTemplate);
    }

    public ExtendedRenderBean render(ProcessInstance pi, ProcessActionContext context) throws EdmException {
        return render((Jbpm4ProcessInstance) pi, context, DEFAULT_TEMPLATE);
    }

    private ExtendedRenderBean render(Jbpm4ProcessInstance pi, ProcessActionContext context, String defaultTemplate) throws EdmException {
        log.info("Jbpm4DefaultProcessView.render");
        ProcessExtensions pe = ServiceManager.getService(
                ProcessExtensionsCache.class,
                ProcessExtensionsCache.SERVICE_ID)
            .provide(pi.getProcessDefinitionId());

        ExtendedRenderBean ret;
        switch(context.getActionType()){
            case ADMIN_VIEW:
                ret = prepareAdminViewBean(pi);
                break;
            case ARCHIVE_VIEW:
                ret = archiveViewShow ? new ExtendedRenderBean(pi, defaultTemplate)
                            : new ExtendedRenderBean(pi, EMPTY_TEMPLATE);
                break;
            case USER_VIEW:
            case SUMMARY_VIEW:
                ret = new ExtendedRenderBean(pi, defaultTemplate);
                break;
            default:
                log.error("unsupported action type '{}' - empty template", context.getActionType());
                ret = new ExtendedRenderBean(pi, EMPTY_TEMPLATE);
                break;
        }
        //noinspection StringEquality
        if(ret.getTemplate() != EMPTY_TEMPLATE){//w tym przypadku mo�na tak por�wnywa�

            ret.putAllValues(provideActivities(pi, context, pe.getActivityController()).asMap());
        }

        return ret;
    }

    protected Multimap<String, Object> provideActivities(Jbpm4ProcessInstance pi,
                                                                        ProcessActionContext context,
                                                                        ActivityController ac) {
        Multimap<String, Object> ret = ac.getActivities(pi);
        if(!ret.get("messageButtons").isEmpty()){
            ret.put("email-link", Docusafe.getBaseUrl()+"/office/common/send-email.action");
            ret.putAll("attachments", getAttachmentsForMessageButtons(pi));
        }
        return ret;
    }

    private List<AttachmentInfo> getAttachmentsForMessageButtons(SingleStateProcessInstance pi) {
        List<AttachmentInfo> infos = new ArrayList<AttachmentInfo>();
        try
        {
            Long documentId = (Long) Jbpm4Provider.getInstance().getExecutionService().getVariable(pi.getId().toString(), String.valueOf(Jbpm4Constants.DOCUMENT_ID_PROPERTY));
            OfficeDocument doc = OfficeDocument.find(documentId);
            for(Attachment at : doc.getAttachments())
            {
                infos.add(new AttachmentInfo(at.getTitle(), at.getId()));
            }
        }
        catch(EdmException e)
        {
            log.error(e.getMessage(),e);
        }
        return infos;
    }

    private ExtendedRenderBean prepareAdminViewBean(Jbpm4ProcessInstance pi) {
        ExtendedRenderBean ret;
        ret = new ExtendedRenderBean(pi, ADMIN_TEMPLATE);
        List<Task> tasks = Jbpm4Provider.getInstance().getTaskService().createTaskQuery().processInstanceId(pi.getId()).list();
        List<TaskBean> taskBeans = Lists.newArrayListWithCapacity(tasks.size());
        for(Task task: tasks){
            TaskBean bean = new TaskBean();
            bean.name = task.getName();
            bean.assignee = StringUtils.defaultIfEmpty(task.getAssignee(), "");
            StringBuilder sb = new StringBuilder();
            List<Participation> participations = Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(task.getId());
            for(Participation p : participations){
                sb.append(getUserDescription(p.getUserId()))
                        .append(getDivisionDescription(p.getGroupId()))
                        .append('[')
                        .append(p.getType())
                        .append("]<br/>");
            }
            bean.participants = sb.toString();
            taskBeans.add(bean);
        }
        ret.putValue("processName", pi.getProcessName());
        try {
            ret.putValue("processState", pi.getSingleState());
        } catch (MultipleExecutionsException ex){
            ret.putValue("processState", "<wiele stan�w>");
        }
        ret.putValue("tasks", taskBeans);
        return ret;
    }

    private String getDivisionDescription(String groupId) {
        if (Strings.isNullOrEmpty(groupId)) {
            return "";
        } else {
            StringBuilder description = new StringBuilder();
            try {
                DSDivision division = DSDivision.find(groupId);
                for (DSUser user : division.getUsers()) {
                    if (description.length() > 100) {
                        description.append("...");
                        break;
                    }

                    if (description.length()==0) {
                        description.append("{");
                    } else {
                        description.append(", ");
                    }
                    description.append(user.getName());
                }
                if (description.length()>0) {
                    description.append("}");
                }
                return division.getName()+" ("+groupId+") "+description;
            } catch (EdmException e) {
                log.error(e.getMessage(),e);
                return groupId;
            }
        }
    }

    private String getUserDescription(String userId) {
        if (Strings.isNullOrEmpty(userId)) {
            return "";
        } else {
            try {
                DSUser user = DSUser.findByUsername(userId);
                return user.getFirstnameLastnameName();
            } catch (EdmException e) {
                log.error(e.getMessage(),e);
                return userId;
            }
        }
    }

    public static class TaskBean {
        String name;
        String assignee;
        String participants;

        public String getAssignee() {
            return assignee;
        }

        public String getName() {
            return name;
        }

        public String getParticipants() {
            return participants;
        }
    }
}
