package pl.compan.docusafe.core.jbpm4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPSClient;
import org.jbpm.api.listener.EventListenerExecution;
import org.jfree.util.Log;
import com.google.common.collect.Maps;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class GetOcrAttachementFromFtpListaner extends AbstractEventListener
{
	private final static Logger LOG = LoggerFactory.getLogger(SendAttachementToFtpListaner.class);
	private OfficeDocument doc;

	public void notify(EventListenerExecution event) throws Exception
	{
		String ftpServer = Docusafe.getAdditionProperty("ocrFtpServer");
		String ftpLogin = Docusafe.getAdditionProperty("ocrFtpLogin");
		String ftpPass = Docusafe.getAdditionProperty("ocrFtpPassword");

		if (ftpServer == null)
			throw new EdmException("Brak parametru 'ocrFtpServer' w adds.properties");
		if (ftpLogin == null)
			throw new EdmException("Brak parametru 'ocrFtpServerLogin' w adds.properties");
		if (ftpPass == null)
			throw new EdmException("Brak parametru 'ocrFtpServerPassword' w adds.properties");

		Long docId = Long.valueOf(event.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		LOG.debug("docId: {}", docId);
		try
		{
			LOG.debug("1.GetOcr. CONTEXT: {}", DSApi.isContextOpen());
			if (!DSApi.isContextOpen())
				DSApi.openAdmin();
			
			setDoc(OfficeDocument.find(docId));

			LOG.debug("DocId: {}", getDoc().getId());
			boolean sended = false;
			for (Attachment att : doc.getAttachments())
			{
				sended = download(ftpServer, ftpLogin, ftpPass, att.getMostRecentRevision());
				break; // tylko jeden
			}
			if (DSApi.isContextOpen())
				DSApi.close();
			LOG.debug("2.GetOcr. CONTEXT: {}", DSApi.isContextOpen());
			if (sended)
				progress(event);
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
		finally
		{
			LOG.debug("3.GetOcr. CONTEXT: {}", DSApi.isContextOpen());
			if (!DSApi.isContextOpen())
				DSApi.openAdmin();
		}

	}

	public void progress(EventListenerExecution prmEventListenerExecution)
	{
		org.jbpm.api.Configuration.getProcessEngine().getExecutionService().signalExecutionById(prmEventListenerExecution.getId(), "go on");
		//org.jbpm.api.Configuration.getProcessEngine().getExecutionService().
	}

	private boolean download(String ftpServer, String ftpLogin, String ftpPass, AttachmentRevision attRev) throws EdmException
	{
		//FTPClient client = new FTPClient();
		FTPSClient client = new FTPSClient();
		FileInputStream fis = null;
		FileOutputStream fos = null;
		boolean result = false;
		try
		{
			client.connect(ftpServer);
			client.login(ftpLogin, ftpPass);
			LOG.debug("getPassivePort: {}", client.getPassivePort());
			LOG.debug("client.getPassiveHost(): {}", client.getPassiveHost());
			LOG.debug("DefaultPort: {}", client.getDefaultPort());
			LOG.debug("getLocalPort: {}", client.getLocalPort());
			LOG.debug("getRemotePort: {}", client.getRemotePort());
			client.changeWorkingDirectory("Export");
			if (client.listFiles().length == 0)
				return result;
			try
			{
				File temp = new File("C:\\" + attRev.getOriginalFilename().substring(0, attRev.getOriginalFilename().length() - 4) + ".xml"); // File.createTempFile("docusafe_",
																														// "_tmp");
				fos = new FileOutputStream(temp);
				LOG.debug("retrieveFileName: {}", attRev.getOriginalFilename().substring(0, attRev.getOriginalFilename().length() - 4) + ".xml");
				if (client.retrieveFile(attRev.getOriginalFilename().substring(0, attRev.getOriginalFilename().length() - 4) + ".xml", fos))
				{
					LOG.debug("MAMY PLIK!!");
					result = createDoc(temp);
				}
				else
				{
					LOG.debug("Deltetd file: {}", temp.delete());
				}

				fos.close();
				client.logout();
			}
			catch (Exception e)
			{
				LOG.error(e.getMessage(), e);
			}
		}
		catch (IOException e)
		{
			LOG.error(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (fis != null)
				{
					fis.close();
				}
				client.disconnect();
			}
			catch (IOException e)
			{
				LOG.error(e.getMessage(), e);
			}
		}
		return result;
	}

	private boolean createDoc(File file) throws IOException, DocumentException, ParseException, EdmException
	{
		/**
		 * Wczytanie zocreowanego pliku
		 */
		SAXReader reader = new SAXReader();
		Document doc = reader.read(file);
		Element root = doc.getRootElement();
		Element client = root.element("_Liberty2");
		Element ocrDoc = client.element("_FAKTURA");
		Map<String, String> docValues = new HashMap<String, String>();
		for (Iterator i = ocrDoc.elements().iterator(); i.hasNext();)
		{
			Element elem = (Element) i.next();
			LOG.debug("ELEM: {}, data: {}", elem.getName().substring(1, elem.getName().length()), (String) elem.getData());
			docValues.put(elem.getName().substring(1, elem.getName().length()), (String) elem.getData());
		}

		Map<String, Object> toReload = Maps.newHashMap();
		Map<String, Object> senderValues = Maps.newHashMap();
		senderValues.put("dictionaryType", Person.DICTIONARY_SENDER);
		FieldsManager fm = getDoc().getFieldsManager();

		for (String cn : docValues.keySet())
		{
			LOG.debug("CN: {}, value: {}", cn, docValues.get(cn));
			if (cn.startsWith("SENDER_"))
			{
				if (cn.equals("SENDER_NRKONTA"))
				{
					senderValues.put("LPARAM", docValues.get(cn));
					toReload.put("NR_KONTA", docValues.get(cn));
				}
				senderValues.put(cn.replace("SENDER_", ""), docValues.get(cn));
			}

			else if (cn.equals("DATA_WYSTAWIENIA"))
				toReload.put(cn, DateUtils.sqlDateFormat.parse(docValues.get(cn)));
			else
				toReload.put(cn, docValues.get(cn));
		}

		Person person = Person.findByNip((String) senderValues.get("NIP"));
		if (person == null)
		{
			person = new Person();
			DSApi.context().begin();
			person.fromMapUpper(senderValues);
			person.setDictionaryGuid("rootdivision");
			person.create();
			DSApi.context().commit();
		}
		DSApi.context().begin();
		toReload.put("SENDER", person.getId());
		// przeladowanie wartosci
		getDoc().getDocumentKind().setOnly(getDoc().getId(), toReload);
		DSApi.context().commit();
		DSApi.context().begin();
		TaskSnapshot.updateByDocument(getDoc());
		DSApi.context().commit();
		return true;
	}

	public OfficeDocument getDoc()
	{
		return doc;
	}

	public void setDoc(OfficeDocument doc)
	{
		this.doc = doc;
	}

}
