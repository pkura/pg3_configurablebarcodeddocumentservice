package pl.compan.docusafe.core.jbpm4;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Tworzy proces do wiadomo�ci i wysy�a go do u�ytkownika.
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ReadOnlyAssigner implements ExternalActivityBehaviour {
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ReadOnlyAssigner.class);

    private String fieldValue = null;
    private String user;
    private String guid;
    private String acceptationCn;
    private String fieldCnValue;
    private String field;
    private String dataBaseEnumField;
    private String acceptationCnFromDocumentAcceptance;
    private Boolean allUsersFromDocumentAcceptance;
    /**
     * nazwa zmiennej procesowej zawieraj�cej username(y)
     */
    private String userFromVariable;

    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {
    }

    public void execute(ActivityExecution activityExecution) throws Exception {
        long documentId = (Long) activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);

        if (AvailabilityManager.isAvailable("mosty")) {
            log.error("MOSTY AVAILABLE");
            if (activityExecution.hasVariable("user")) {
                String usr = (String) activityExecution.getVariable("user");
                log.error("!!!!!!!!!!!!!!!!!FROM ACTIVITY:" + usr);
                if (usr != null) user = usr;
            }
        }


        boolean wasOpened = false;
        try {
            if (!DSApi.isContextOpen()) {
                wasOpened = true;
                DSApi.openAdmin();
            }

            OfficeDocument od = OfficeDocument.find(documentId);
            LinkedList<String> guids = new LinkedList<String>();
            LinkedList<String> users = new LinkedList<String>();
            if (StringUtils.isNotBlank(guid)) {
                LinkedList<String> guidAsList = new LinkedList<String>();
                guidAsList.add(guid);
                od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, guidAsList));
            } else if (user != null) {
                if (user.equals("author")) {
                    od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, od.getAuthor()));
                } else {
                    od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, user));
                }
            } else if (StringUtils.isNotBlank(field)) {
                String toUser = od.getFieldsManager().getEnumItemCn(field);
                od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, toUser));
            } else if (StringUtils.isNotBlank(dataBaseEnumField)) {
            	EnumItem dataBaseField = od.getFieldsManager().getEnumItem(dataBaseEnumField);
            	DSUser user = DSUser.findByUsername(dataBaseField.getRefValue());
				od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, user.getName()));
            } else if (StringUtils.isNotBlank(acceptationCn)) {
                String[] assigness = acceptationCn.split(",");
                for (String ass : Arrays.asList(assigness)) {
                    if (AcceptanceDivisionImpl.findByCode(ass) != null && AcceptanceDivisionImpl.findByCode(ass).getOgolna() == 1)
                        fieldValue = null;
                    else if (fieldCnValue != null)
                        fieldValue = od.getFieldsManager().getEnumItem(fieldCnValue).getCn();

                    for (AcceptanceCondition accept : AcceptanceCondition.find(ass, fieldValue)) {
                        if (accept.getUsername() != null) {
                            users.add(accept.getUsername());
                        }
                        if (accept.getDivisionGuid() != null) {
                            guids.add(accept.getDivisionGuid());
                        }
                    }
                }
            }
            //pobranie listy u�ytkownik�w z historii akceptacji dokumentu
            //ograniczone nazw� akceptacji
            //dla ka�dego usera oddzielny task
            else if (StringUtils.isNotBlank(acceptationCnFromDocumentAcceptance)) {
                for (DocumentAcceptance acc : DocumentAcceptance.find(documentId, acceptationCnFromDocumentAcceptance)) {
                    Set<String> userSet = new HashSet<String>();
                    if (acc.getUsername() != null) {
                        if (userSet.add(acc.getUsername())) {
                            od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, acc.getUsername()));
                        }
                    }
                }

            } else if (allUsersFromDocumentAcceptance != null && allUsersFromDocumentAcceptance) {
                Set<String> userSet = new HashSet<String>();
                for (DocumentAcceptance acc : DocumentAcceptance.find(documentId)) {
                    if (acc.getUsername() != null) {
                        if (userSet.add(acc.getUsername())) {
                            od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic()
                                    .startProcess(od, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, acc.getUsername()));
                        }
                    }
                }
            } else if (userFromVariable != null) {
                Object variable = activityExecution.getVariable(userFromVariable);
                if (variable != null) {
                    Iterable userList = Lists.newArrayList();

                    if (variable instanceof String) {
                        userList = Splitter.on(",").omitEmptyStrings().split(variable.toString());
                    } else if (variable instanceof Iterable) {
                        userList = (Iterable) variable;
                    } else {
                        log.error("unsupported");
                    }

                    for (Object username : userList) {
                        od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, username));
                    }

                    activityExecution.removeVariable(userFromVariable);
                } else {
                    log.error("Variable " + userFromVariable + " is null");
                }
            }

            if (!guids.isEmpty()) {
                od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, guids));
            }
            if (!users.isEmpty()) {
                od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_USERNAMES, users));
            }
        } catch (Exception e) {
        	log.error(e.getMessage(), e);
        }
        finally {
            if (DSApi.isContextOpen() && wasOpened)
                DSApi.close();

            if (activityExecution.getActivity().getDefaultOutgoingTransition() != null)
                activityExecution.takeDefaultTransition();
        }


    }

    private String getUser() {
        return user;
    }

    private void setUser(String user) {
        this.user = user;
    }
}
