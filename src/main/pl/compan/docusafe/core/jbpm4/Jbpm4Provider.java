package pl.compan.docusafe.core.jbpm4;

import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.hibernate.SessionFactory;
import org.jbpm.api.*;
import org.jbpm.pvm.internal.type.variable.DoubleVariable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;

/**
 *  Klasa dost�pu do JBPMa. Nale�y maksymalnie ograniczy� wyciek API jbpm'owego do Docusafe'a
 *    - nowsze wersje jbpm'a prawdopodobnie b�d� modyfikowa� API
 */
public class Jbpm4Provider {
    private static final Logger log = LoggerFactory.getLogger(Jbpm4Provider.class);

    private Configuration jbpmConfiguration;
    private ProcessEngine processEngine = null;
    private Boolean supported;

    //Na razie singleton
    private final static Jbpm4Provider INSTANCE = new Jbpm4Provider();

    public static Jbpm4Provider getInstance(){
        return INSTANCE;
    }

    public RepositoryService getRepositoryService(){
        return processEngine.getRepositoryService();
    }

    public ExecutionService getExecutionService(){
        return processEngine.getExecutionService();
    }

    public TaskService getTaskService(){
        return processEngine.getTaskService();
    }

    public IdentityService getIdentityService(){
        return processEngine.getIdentityService();
    }

    public HistoryService getHistoryService(){
        return processEngine.getHistoryService();
    }

    public ManagementService getManagementService ()
    {
	    return processEngine.getManagementService();
    }
//    public static Collection<URL> getMappingFiles(){
//        return Arrays.asList(
//            Jbpm4Provider.class.getResource("jbpm.execution.hbm.xml"),
//            Jbpm4Provider.class.getResource("jbpm.history.hbm.xml"),
//            Jbpm4Provider.class.getResource("jbpm.identity.hbm.xml"),
//            Jbpm4Provider.class.getResource("jbpm.repository.hbm.xml"),
//            Jbpm4Provider.class.getResource("jbpm.task.hbm.xml")
//            );
//    }

    private File hibernateConfigFile(){
        return new File(Docusafe.getHome(), "jbpm4.hibernate.cfg.xml");
    }

    public boolean isJbpm4Supported(){
        if(supported == null){
            supported = hibernateConfigFile().isFile();
        }
        return supported;
    }

    public boolean isInitialized(){
        return processEngine != null;
    }

    /**
     * Metoda inicjalizuj�ca jbpm4 - powinna by� wywo�ywana tylko w podczas startu systemu (i.e. nie u�ywa�)
     * @param hsf
     * @throws EdmException
     */
    public void initialize(SessionFactory hsf) throws EdmException {
        Preconditions.checkState(!isInitialized(), "jbpm4 already initialized");
        Preconditions.checkState(isJbpm4Supported(), "jbpm4 is not supported");
        
        InputStream is = null;
        try{
            DSApi.openAdmin();
            is = Jbpm4Provider.class.getResourceAsStream("jbpm.cfg.xml");
            String xmlString = IOUtils.toString(is, "utf8").replace("$hibernate.config.file",hibernateConfigFile().toURI().toURL().toString());
            log.info("xml config string = \n" + xmlString);

            jbpmConfiguration = new Configuration()
                .setXmlString(xmlString)
                .setHibernateSessionFactory(hsf);

            processEngine = jbpmConfiguration.buildProcessEngine();
        } catch(Exception e){
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(is);
            DSApi.close();
        }
    }
}
