package pl.compan.docusafe.core.jbpm4;

import java.sql.Statement;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.office.OfficeDocument;

/** Custom Activity, Przygotowuje do ponowienia akceptacji.
 * Robi to zaczynaj�c od ustalenia statusu, do kt�rego nale�y przej��. <br/>
 * <p>Je�li fieldCn == null pr�buje pobra� zmienn� correctionState.
 * <p>Je�li fieldCn != null pr�buje pobra� zmienn� cState, <br/>
 * a w ostateczno�ci state=fm.getEnumItemCn(fieldCn);
 * <p>
 * Usuwa DocumentAcceptance oraz zawarto�� pola fieldCn z tabeli dockindu,
 * a nast�pnie idzie do czynno�ci okre�lonej w state. */
@SuppressWarnings("serial")
public class CorrectionAssigner implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(CorrectionAssigner.class);
	private String fieldCn;

	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		String state = null;
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		if (fieldCn == null && execution.getVariable("correctionState") != null)
		{
			state = (String) execution.getVariable("correctionState");
			execution.removeVariable("correctionState");
			
			if(AvailabilityManager.isAvailable("correctionAssigner.removeUser"))
				execution.removeVariable("user");
		}
		else
		{
			if (execution.hasVariable("cState") && execution.hasVariable("user")){
			    state = execution.getVariable("cState").toString();
			    execution.setVariable("correctionState", state);
			} else {
			    state = fm.getEnumItemCn(fieldCn);
			    String user = fm.getEnumItem(fieldCn).getTitle();
			    execution.setVariable("user", user);
			    execution.setVariable("correctionState", state);
			}
		}
		clearAcceptance(docId, state);
		if (fieldCn != null)
		{
			Statement stat = null;
			try
			{
				stat = DSApi.context().createStatement();
				stat.executeUpdate("UPDATE " + fm.getDocumentKind().getTablename() + " SET " + fieldCn.toLowerCase() + " = null WHERE DOCUMENT_ID = " + docId);
			}
			catch (Exception e)
			{
				throw new EdmException(e);
			}
			finally
			{
				DSApi.context().closeStatement(stat);
			}
		}
		log.info("execution.take(state): {}", state);
		execution.take(state);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{

	}

	/** usuwa okre�lon� DocumentAcceptance (dokonan� akceptacj� dokumentu) */
	public static void clearAcceptance(long docId, String state) throws EdmException
	{
		clearAcceptance(null, docId, state);
	}
	
	/** usuwa okre�lon� DocumentAcceptance */
	public static void clearAcceptance(Long accId, long docId, String state) throws EdmException
	{
		if (accId == null) {
			DocumentAcceptance acceptationState = DocumentAcceptance.find(docId, state, null);
			Long acceptationStateId = null;
			String acceptationStateCn = null;
			if (acceptationState != null) {
				acceptationStateId = acceptationState.getId();
				acceptationStateCn = acceptationState.getAcceptanceCn();
			}
			for (DocumentAcceptance docAcc : DocumentAcceptance.find(docId))
			{
				if (acceptationStateCn != null && acceptationStateId != null && (docAcc.getId() >= acceptationStateId || (acceptationStateCn != "taskCorrectionForFork" && docAcc.getAcceptanceCn() == acceptationStateCn)))
					docAcc.delete();
			}
		} else {
			DocumentAcceptance acceptationToExec = DocumentAcceptance.findById(accId);
			if (acceptationToExec != null) {
				Long mpkId = acceptationToExec.getObjectId();

				if (mpkId != null) {
					for (DocumentAcceptance docAcc : DocumentAcceptance.find(docId))
						if (docAcc.getId() >= acceptationToExec.getId() && (docAcc.getObjectId() == null || docAcc.getObjectId().equals(mpkId)))
							docAcc.delete();
				} else {
					for (DocumentAcceptance docAcc : DocumentAcceptance.find(docId))
						if (docAcc.getId() >= acceptationToExec.getId())
							docAcc.delete();
				}
			}
		}
	}
}
