package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.imgwwaw.WniosekZakupowyLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FinishListener extends AbstractEventListener
{

	protected static Logger log = LoggerFactory.getLogger(FinishListener.class);
	
	@Override
	public void notify(EventListenerExecution execution) throws Exception
	{
		boolean isOpened = false;
		try {
			isOpened = DSApi.openContextIfNeeded();
			OfficeDocument doc = Jbpm4Utils.getDocument(execution);
			doc.addAssignmentHistoryEntry(
	                new AssignmentHistoryEntry(DSApi.context().getPrincipalName(),
	                        execution.getProcessDefinitionId(), AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION, AssignmentHistoryEntry.EntryType.JBPM_FINISH)
	            );
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		} finally {
			if (isOpened) {
				DSApi.closeContextIfNeeded(isOpened);
			}
		}
		
	}

}
