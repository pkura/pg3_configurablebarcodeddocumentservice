package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ManualAssignmentHandler implements AssignmentHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger LOG = LoggerFactory
			.getLogger(ManualAssignmentHandler.class);

	/*
	 * public final static String ASSIGN_USER_PARAM = "assignee"; public final
	 * static String ASSIGN_USER_COMMAND = "command"; public final static String
	 * ASSIGNED_USER_NAME = ""
	 */
	public void assign(Assignable assignable, OpenExecution openExecution)
			throws Exception {
		// String username = (String)
		// openExecution.getVariable(ASSIGN_USER_PARAM);
		Long docId = Long.valueOf(openExecution
				.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY)
				+ "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();

		String author;// = (String) openExecution.getVariable("DSAUTHOR");
		//String userId;// = (String) openExecution.getVariable("DSUSER");
		//S/tring command;// = (String)
						// openExecution.getVariable("DOCDESCRIPTION");

		if (fm.getValue("DSUSER") != null) {
			author = fm.getKey("DSUSER").toString();
			DSUser user = (DSUser)UserImpl.find(new Long(author));
    		assignable.addCandidateUser(user.getName());	
		} else {
			author = doc.getAuthor();
			assignable.addCandidateUser(author);
		}

	}
}
