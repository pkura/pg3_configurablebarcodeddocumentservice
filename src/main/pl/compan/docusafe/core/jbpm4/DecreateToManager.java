package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.bialystok.DivisionBoss;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * klasa uzywana przez proces jbpm "manager decretation"
 * jest to proces dekretacji na kierownika dzialu (w przypadku braku, na kierownika w dziale wyzszym)
 * @author bkaliszewski
 *
 */
public class DecreateToManager implements AssignmentHandler
{
	private static Logger log = LoggerFactory.getLogger(DecreateToManager.class);
	

 	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception
	{
		OfficeDocument document = Jbpm4Utils.getDocument(openExecution);
		
		String divisionName = (String)document.getFieldsManager().getValue("RECIPIENT_HERE");
		
		DSDivision division = DSDivision.findByName(divisionName);
		
		DSDivision managerDivision = DivisionBoss.findManager(division);

		// bierzemy uzytkownika-kierownika
		DSUser manager = managerDivision.getOriginalUsers()[0];
		
		// dekretuj
		assignable.setAssignee(manager.getName());
		log.info("ustawiam assignee na "+manager.getName());
		
		//TODO: sprawdzi� to
		TaskSnapshot.updateByDocumentId(document.getId(),"anyType");
	}
}