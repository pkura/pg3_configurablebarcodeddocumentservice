/**
 * 
 */
package pl.compan.docusafe.core.jbpm4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**Wysy�a procesy do wiadomo�ci (read-only)
 * z u�yciem profili
 * 
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 11-02-2014
 * ds_trunk_2014
 * ReadOnlyByProfile.java
 */
public class ReadOnlyByProfile implements ExternalActivityBehaviour{

	private static final StringManager sm =
	        GlobalPreferences.loadPropertiesFile(ReadOnlyByProfile.class.getPackage().getName(),null);
	
	protected static Logger log = LoggerFactory.getLogger(ReadOnlyByProfile.class);
	
	/**
     * Nazwa adds-a, kt�ry okre�la id profilu
     */
    public String profileIdAdds;
    
    /**
     * Kod dzia�u z kt�rego nale�y zacz�� wyszukiwanie u�ytkownika o okre�lonym profilu
     */
    public String divisionCodeFromAdds;
    
    /**
     * Kod dzia�u z kt�rego nale�y zacz�� wyszukiwanie u�ytkownika o okre�lonym profilu
     */
    public String divisionCodeIfNotSet;
    /**
     * Zakres przeszukiwania. Okre�la liczbe przeszukiwanych poziom�w do root'a
     * -1 do root
     */
    public Integer scopeLvl = 0;
    
    /**
     * Zako�cz przeszukiwanie na poziomie, na kt�rym znaleziono u�ytkowanika o podanym profilu
     */
    public Boolean endOnFirst = true;
    
    /**
     * Okre�la czy w momencie nie znalezienia w �adnym zakresie u�ytkownika o danym profilu maj�
     * zosta� przypisani wszyscy z peirwszego napotkanego dzia�u, w ktorymkto� sie znajduje
     */
    public Boolean assignToDivisionIfNoProfile = false;

	@Override
	public void execute(ActivityExecution activity) throws Exception {
		boolean isOpened = false;
		try {
			isOpened = DSApi.openContextIfNeeded();
			List<String> userNames = new ArrayList<String>();
			long documentId = (Long) activity.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);
			OfficeDocument officeDocument = OfficeDocument.find(documentId);
			FieldsManager fm = officeDocument.getFieldsManager();
			long divisionId = getDivisionId(fm);
			long profileId  = getProfileId();
			
			putUsersFromProfile(userNames, divisionId, profileId, endOnFirst, scopeLvl, assignToDivisionIfNoProfile);
			
			//Tworzy proces do wiadomo�ci 
			officeDocument.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic()
				.startProcess(officeDocument, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_USERNAMES, userNames));
			
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		} finally {
			if (isOpened) {
				DSApi.closeContextIfNeeded(isOpened);
			}
		}
	}
    
    public long getDivisionId(FieldsManager fm) throws Exception {
        Long divisionId = null;
       if (StringUtils.isNotBlank(divisionCodeFromAdds)) {
        	String code = "";
        	if(StringUtils.isNotBlank(divisionCodeIfNotSet)) {
        		code = Docusafe.getAdditionPropertyOrDefault(divisionCodeFromAdds, divisionCodeIfNotSet);
        	} else {
        		code = Docusafe.getAdditionPropertyOrDefault(divisionCodeFromAdds, "-1");
        	}
        	DSDivision division = DSDivision.findByCode((code));
        	divisionId = division.getId();
        } else {
        	 throw new Exception("No profile id source");
        }

        if (divisionId==null) throw new Exception("No division id");
        return divisionId;
    }

    public Long getProfileId () throws Exception {
        Long profileId;
        if (StringUtils.isNotBlank(profileIdAdds)){
            profileId = (long)Docusafe.getIntAdditionPropertyOrDefault(profileIdAdds, -1);
            if (profileId < 0) throw new Exception("No adds=" + profileIdAdds);
        } else {
        	throw new Exception("No profile id source");
        }

        if (profileId==null) throw new Exception("No profile id");
        return profileId;
    }

    public static boolean putUsersFromProfile(List<String> outUsernames, Long divisionId, Long profileId, boolean endOnFirst, int counter, boolean ifNotFoundNotThrowException) throws Exception {
        String sqlQuery = "select u.NAME " +
                "from DS_DIVISION d " +
                "join DS_USER_TO_DIVISION ud on d.ID = ud.DIVISION_ID " +
                "join DS_USER u on ud.USER_ID = u.ID " +
                "join ds_profile_to_user pu on u.ID = pu.user_id " +
                "where pu.profile_id = ? and d.id = ?";

        PreparedStatement ps;
        ResultSet rs;

        ps = DSApi.context().prepareStatement(sqlQuery);
        ps.setLong(1, profileId);
        ps.setLong(2, divisionId);
        rs = ps.executeQuery();

        List<String> resultList = new ArrayList<String>();
        while (rs.next()) {
            resultList.add(rs.getString(1));
        }

        outUsernames.addAll(resultList);
        if (endOnFirst && !outUsernames.isEmpty())
            return true;
        resultList.clear();

        if (counter == 0)
            return endOfAssignmentToUsersFromProfile(ifNotFoundNotThrowException, !outUsernames.isEmpty());

        DSDivision div = DSDivision.findById(divisionId);
        div = div.getParent();
        if (div == null || div.isRoot())
            return endOfAssignmentToUsersFromProfile(ifNotFoundNotThrowException, !outUsernames.isEmpty());

        return putUsersFromProfile(outUsernames, div.getId()/*div.getGuid()*/, profileId, endOnFirst, counter - 1, ifNotFoundNotThrowException);
    }

    public static boolean endOfAssignmentToUsersFromProfile(boolean ifNotFoundNotThrowException, boolean success) throws Exception {
        if (success)
            return true;
        if (ifNotFoundNotThrowException)
            return false;
        throw new Exception(sm.get("NieZnalezionoUzytkownikaWZasiegu"));
    }

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		
	}
	
}
