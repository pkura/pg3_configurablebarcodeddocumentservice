package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;
import org.jbpm.api.model.OpenExecution;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class Jbpm4SignalListener extends AbstractEventListener{
    private String signal;


    public void notify(EventListenerExecution eventListenerExecution) throws Exception {
        Jbpm4Provider.getInstance().getExecutionService().signalExecutionById(
                eventListenerExecution.getProcessInstance().getId(),
                signal);
    }
}
