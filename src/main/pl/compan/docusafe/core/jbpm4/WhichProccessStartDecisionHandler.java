package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WhichProccessStartDecisionHandler  implements DecisionHandler
{
	protected static Logger log = LoggerFactory.getLogger(WhichProccessStartDecisionHandler.class);
	
	private String documentCns;
	private String [] cns;
	
	@Override
	public String decide(OpenExecution execution)
	{
		cns = documentCns.split(",");
		
		long documentId = (Long) execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);
		
		try
		{
			OfficeDocument od = OfficeDocument.find(documentId);
			for (String cn : cns)
			{
				if (od.getDocumentKind().getCn().equals(cn))
					return cn;
			}
			return "other";
		}
		catch (DocumentNotFoundException e)
		{
			log.error(e.getMessage(), e);
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		
		return null;
	}

}
