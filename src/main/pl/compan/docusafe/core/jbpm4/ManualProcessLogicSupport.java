package pl.compan.docusafe.core.jbpm4;

import org.apache.commons.lang.ObjectUtils;
import org.jbpm.api.task.Task;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.dockinds.process.ProcessAction;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ManualProcessLogicSupport extends Jbpm4ProcessLogicSupport{
    private final static Logger LOG = LoggerFactory.getLogger(ManualProcessLogicSupport.class);

    public ManualProcessLogicSupport(InstantiationParameters ip) {
        super(ip);
    }

    @Override
    public void startProcess(OfficeDocument od, Map<String, ?> params) throws EdmException {
        super.startProcess(od, params);

        String username = ObjectUtils.toString(params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM),"x");
        String guid = ObjectUtils.toString(params.get(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM),"x");

        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setCtimeAndCdate(new Date());
        ahe.setObjective("start-process");
        ahe.setProcessName(getProcessName());
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        String divisions = "";
		for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
		{
			if (!divisions.equals(""))
				divisions = divisions + " / " + a.getName();
			else
				divisions = a.getName();
		}
		if(divisions.isEmpty())
			divisions="x";
		ahe.setSourceGuid(divisions);
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        ahe.setType(AssignmentHistoryEntry.EntryType.JBPM_INIT);
        od.addAssignmentHistoryEntry(ahe);
    }

    @Override
    protected void process(Jbpm4ProcessInstance pi, ProcessAction pa) throws EdmException {
        LOG.info("process action '{}'", pa.getActionName());
        if(pa.getActionName().equals("reopen-wf")){
            if(!pi.isEnded()){
                pa.addActionError("Proces r�czny nie zosta� zako�czony, nie mo�na utworzy� nowego procesu");
            } else {
                restartProcess(pa);
            }
            TaskSnapshot.updateByDocumentId(pa.getDocumentId(), "anyType");
        } else if(pa.getActionName().equals("assign-me")) {
            if(pi.isEnded()){
                pa.addActionError("Proces r�czny zosta� zako�czony.");
            } else {
                Task task = Jbpm4Provider.getInstance().getTaskService().createTaskQuery().processInstanceId(pi.getId()).uniqueResult();
                Jbpm4WorkflowFactory.assignMe(task.getId(), pa.getDocumentId());
            }
            TaskSnapshot.updateByDocumentId(pa.getDocumentId(), "anyType");
        } else {
            super.process(pi, pa);
        }
    }

    private void restartProcess(ProcessAction pa) throws EdmException {
        eraseOldProcessEntry(getProcessName(), pa.getDocumentId());
        startProcess(pa.getDocument(),
                Collections.singletonMap(
                        ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM,
                        DSApi.context().getPrincipalName()));
        pa.addActionMessage("Utworzono proces r�czny");

        OfficeDocument doc = OfficeDocument.find(pa.getDocumentId());
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setTargetUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(getProcessName());
        ahe.setType(AssignmentHistoryEntry.JBPM4_RESTART);
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        ahe.setCtime(new Date());
        doc.addAssignmentHistoryEntry(ahe);

        TaskSnapshot.updateByDocumentId(pa.getDocumentId(), "anyType");
    }

    private static void eraseOldProcessEntry(String processName, long documentId) throws EdmException {
        PreparedStatement ps = null;
        int ret;
        try{
            ps = DSApi.context().prepareStatement("DELETE FROM ds_document_to_jbpm4 WHERE document_id = ? AND process_name = ?");
            ps.setLong(1, documentId);
            ps.setString(2, processName);
            int rows = ps.executeUpdate();
            if(rows > 1){
                throw new EdmException("Wiele proces�w r�cznych :  " + rows);
            }
//            log.info("found " + ret + " processes for name = " + processName + " and id = " + documentId);
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }
}