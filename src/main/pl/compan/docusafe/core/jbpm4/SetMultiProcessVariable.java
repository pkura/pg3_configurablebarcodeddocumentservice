package pl.compan.docusafe.core.jbpm4;

import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetMultiProcessVariable implements ExternalActivityBehaviour 
{
	String variableNames;
	String variableValues;
	String variablesType;
	
	private static final Logger log = LoggerFactory.getLogger(SetMultiProcessVariable.class);

	/* 
	<custom class="pl.compan.docusafe.core.jbpm4.SetMultiProcessVariable" g="129,948,148,52" name="prepareForCorrectionJoin">
		<field name="variableNames">
			<string value="correction,count"/>
		</field>
		<field name="variableValues">
			<string value="11,1"/>
		</field>
		<field name="variablesType">
			<string value="integer"/>
		</field>
		<transition g="-20,-22" name="to join" to="join"/>
	</custom>
	 */
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		String[] names = variableNames.split(",");
		if (variableValues == null) {
			for (String name : names) {
				execution.removeVariable(name);
			}
		} else {
			String[] values = variableValues.split(",");
			
			if (values.length != names.length) {
				log.error("B��d w ustawianiu zmiennych procesu, liczba warto�ci jest r�na od liczby kluczy, proces: "+execution.getId());
				execution.takeDefaultTransition();
				return;
			}
			
			if (variablesType == null  || variablesType.equalsIgnoreCase("string")) {
				for (int i = 0; i < values.length; i++) {
					if (values[i].equals("")) {
						execution.removeVariable(names[i]);
					} else {
						execution.setVariable(names[i], values[i]);
					}
				}
			} else if (variablesType.equalsIgnoreCase("integer")) {
				for (int i = 0; i < values.length; i++) {
					if (values[i].equals("")) {
						execution.removeVariable(names[i]);
					} else {
						try {
							execution.setVariable(names[i], Integer.valueOf(values[i]));
						} catch (NumberFormatException e) {
							log.error("B��d podczas parsowania liczby w trakcie ustawianiu zmiennych procesu: " + execution.getId());
						}
					}
				}
			}
		}
		
		execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub
		
	}


}
