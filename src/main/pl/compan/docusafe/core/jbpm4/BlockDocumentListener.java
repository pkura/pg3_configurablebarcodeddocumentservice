package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Ustawia flag� zablokowany na procesowanym dokumencie.
 * <br/><br/><br/>
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 28.08.13
 * Time: 16:01
 */
public class BlockDocumentListener extends AbstractEventListener {

    private final static Logger LOG = LoggerFactory.getLogger(BlockDocumentListener.class);

    private boolean openTransaction = false;

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        boolean wasOpenedContext = false;
        try{
            if (!DSApi.isContextOpen()) {
                wasOpenedContext = true;
                DSApi.openAdmin();
            }

            if (openTransaction) {
                DSApi.context().begin();
            }

            Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            Document doc = Document.find(docId);
            doc.setBlocked(Boolean.TRUE);

            if (openTransaction) {
                DSApi.context().commit();
            }
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);

            if (openTransaction) {
                DSApi.context().setRollbackOnly();
            }
        } finally {
            if (DSApi.isContextOpen() && wasOpenedContext)
                try {
                    DSApi.close();
                } catch (EdmException e) {
                    LOG.error(e.getMessage(), e);
                }
        }
    }
}
