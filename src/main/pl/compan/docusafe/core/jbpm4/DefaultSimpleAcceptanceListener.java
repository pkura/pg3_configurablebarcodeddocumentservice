package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class DefaultSimpleAcceptanceListener extends AbsSimpleAcceptanceListener {

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        OfficeDocument doc = getOfficeDocument(execution);

        if(getDoNotInformLogic()==null){
            setDoNotInformLogic("true");
        }

        if(getDoNotSaveAcceptance()==null){
            setDoNotSaveAcceptance("false");
        }

        if(getDoNotUpdateAssignmentHistory()==null){
            setDoNotUpdateAssignmentHistory("false");
        }

        if (!isDoNotUpdateAssignmentHistory()){
            if (getAcception()==null){
                String statusCn = doc.getFieldsManager().getEnumItem("STATUS").getCn();
                setAcception(statusCn);
            }
        }

        executeTask(execution, doc);
    }


    protected String doNotUpdateAssignmentHistory;
    protected String type;
    protected String acception;

    protected String doNotInformLogic;
    protected String simpleAcception;

    protected String doNotSaveAcceptance;

    protected String clearVariables;


    public String getAcception() {
        return acception;
    }

    public void setAcception(String acception) {
        this.acception = acception;
    }

    public String getSimpleAcception() {
        return simpleAcception;
    }

    public void setSimpleAcception(String simpleAcception) {
        this.simpleAcception = simpleAcception;
    }

    public String getDoNotUpdateAssignmentHistory() {
        return doNotUpdateAssignmentHistory;
    }

    public void setDoNotUpdateAssignmentHistory(String doNotUpdateAssignmentHistory) {
        this.doNotUpdateAssignmentHistory = doNotUpdateAssignmentHistory;
    }

    public boolean isDoNotUpdateAssignmentHistory (){
        return doNotUpdateAssignmentHistory!=null && "true".equalsIgnoreCase(doNotUpdateAssignmentHistory) ? true : false;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDoNotInformLogic() {
        return doNotInformLogic;
    }

    public void setDoNotInformLogic(String doNotInformLogic) {
        this.doNotInformLogic = doNotInformLogic;
    }

    public boolean isDoNotInformLogic(){
        return doNotInformLogic!=null && "true".equalsIgnoreCase(doNotInformLogic) ? true : false;
    }


    public String getDoNotSaveAcceptance() {
        return doNotSaveAcceptance;
    }

    public void setDoNotSaveAcceptance(String doNotSaveAcceptance) {
        this.doNotSaveAcceptance = doNotSaveAcceptance;
    }

    public boolean isDoNotSaveAcceptance() {
        return doNotSaveAcceptance!=null && "true".equalsIgnoreCase(doNotSaveAcceptance) ? true : false;
    }


    public String getClearVariables() {
        return clearVariables;
    }

    public void setClearVariables(String clearVariables) {
        this.clearVariables = clearVariables;
    }
}
