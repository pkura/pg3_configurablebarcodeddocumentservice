package pl.compan.docusafe.core.jbpm4;

import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

public class SetProcessVarable implements ExternalActivityBehaviour 
{
	public static final String INTEGER_TYPE = "int";
	
	String variableName;
	String variableValue;
	String variableType;

	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		if (variableValue!=null)
		{
			if (INTEGER_TYPE.equals(variableType)) {
				int value = Integer.parseInt(variableValue);
				execution.setVariable(variableName, value);
			} else {
				execution.setVariable(variableName, variableValue);
			}
		}
		else
			execution.removeVariable(variableName);
		
		execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub
		
	}


}
