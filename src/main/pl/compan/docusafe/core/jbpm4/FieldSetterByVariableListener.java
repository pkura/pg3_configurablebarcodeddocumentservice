package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Collections;

/**
 * User: kk
 * Date: 24.01.14
 * Time: 01:14
 */
public class FieldSetterByVariableListener extends AbstractEventListener
{
    private final static Logger LOG = LoggerFactory.getLogger(FieldSetterByVariableListener.class);

    private String field;
    private String valuedVariable;

    public void notify(EventListenerExecution eventListenerExecution) throws Exception
    {
        boolean hasOpen = true;
        if (!DSApi.isContextOpen())
        {
            hasOpen = false;
            DSApi.openAdmin();
        }
        Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
        doc.getDocumentKind().setOnly(docId, Collections.singletonMap(field, eventListenerExecution.getVariable(valuedVariable)), false);
        if (DSApi.isContextOpen() && !hasOpen)
            DSApi.close();
    }
}
