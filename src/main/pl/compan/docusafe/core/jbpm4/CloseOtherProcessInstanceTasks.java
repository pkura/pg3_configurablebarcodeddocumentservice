package pl.compan.docusafe.core.jbpm4;

import java.util.Map;

import org.jbpm.api.Execution;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Ko�czy dzieci rodzica tego executiona, z wyj�tkiem tego.
 * czyt. zamyka pozosta�e taski z foreach'a
 * @author KK
 *
 * Metoda jest b��dna, nale�y u�ywa� CloseOtherExecutions
 */
@Deprecated
public class CloseOtherProcessInstanceTasks implements ExternalActivityBehaviour {
	private static final Logger log = LoggerFactory.getLogger(CloseOtherProcessInstanceTasks.class);
	private static final long serialVersionUID = 1L;

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		try {
			OpenExecution parentExecution;
			if ((parentExecution = execution.getParent()) != null) {
				for (Execution exec : parentExecution.getExecutions()) {
					if (!exec.equals(execution)) {
						if (exec instanceof ActivityExecution) {
							((ActivityExecution)exec).end();
						}
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
		// TODO Auto-generated method stub

	}

}
