package pl.compan.docusafe.core.jbpm4;

import java.math.BigDecimal;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.parametrization.ukw.prezentacja.OgolneLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Listener ustawia pole w "field" na warto�� w "value"
 * 
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class SetMPKFieldListener extends AbstractEventListener {
	private final static Logger LOG = LoggerFactory.getLogger(SetMPKFieldListener.class);
	private String field;
	private String value;
	
	public static enum Type { 
		CENTRUMID,
		CENTRUMCODE,
		ACCOUNTNUMBER,
		AMOUNT,
		DOCUMENTID,
		ACCEPTINGCENTRUMID,
		SUBGROUPID,
		STORAGEPLACE,
		CUSTOMSAGENCYID,
		LOKALIZACJA,
		CLASS_TYPE_ID,
		CONNECTED_TYPE_ID,
		REMARK_ID,
		VAT_TYPE_ID,
		VAT_TAX_TYPE_ID,
		ACCEPTANCE_MODE,
		AMOUNTUSED,
		ANALYTICS_ID,
		DESCRIPTIONAMOUNT,
		ITEMNO,
		ADDITIONALID,
		INVENTORYID,
		ORDERID,
		GROUPID,
		PRODUCTIDN 	}


	public void notify(EventListenerExecution eventListenerExecution) throws Exception {
		boolean contextOpened = true;
		try
		{
		if (!DSApi.isContextOpen())
		{
			DSApi.openAdmin();
			contextOpened = false;
		}

		Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Long mpkId = (Long) eventListenerExecution.getVariable(OgolneLogic.MPK_VARIABLE_NAME);

		CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId);

		switch (SetMPKFieldListener.Type.valueOf(field)) {
		case CENTRUMID:
			mpk.setCentrumId(Integer.valueOf(value));
			break;
		case CENTRUMCODE:
			mpk.setCentrumCode(value);
			break;
		case ACCOUNTNUMBER:
			mpk.setAccountNumber(value);
			break;
		case AMOUNT:
			mpk.setAmount(BigDecimal.valueOf(Long.valueOf(value)));
			break;
		case DOCUMENTID:
			mpk.setDocumentId(Long.valueOf(value));
			break;
		case ACCEPTINGCENTRUMID:
			mpk.setAcceptingCentrumId(Integer.valueOf(value));
			break;
		case SUBGROUPID:
			mpk.setSubgroupId(Integer.valueOf(value));
			break;
		case STORAGEPLACE:
			mpk.setStoragePlace(value);
			break;
		case CUSTOMSAGENCYID:
			mpk.setCustomsAgencyId(Integer.valueOf(value));
			break;
		case LOKALIZACJA:
			mpk.setLocationId(Integer.valueOf(value));
			break;
		case CLASS_TYPE_ID:
			mpk.setClassTypeId(Integer.valueOf(value));
			break;
		case CONNECTED_TYPE_ID:
			mpk.setConnectedTypeId(Integer.valueOf(value));
			break;
		case AMOUNTUSED:
			mpk.setAmountUsed(BigDecimal.valueOf(Long.valueOf(value)));
			break;
		case ANALYTICS_ID:
			mpk.setAnalyticsId(Integer.valueOf(value));
			break;
		default:
			break;

		}
		}
		finally
		{
			if (!contextOpened && DSApi.isContextOpen())
				DSApi.close();
		}
	}
}
