package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.listener.EventListener;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Baza EventListener'�w jbpm'owych, bardzo mo�liwe, �e na razie niepotrzebna
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public abstract class AbstractEventListener implements EventListener {
    protected final static Logger LOG = LoggerFactory.getLogger(AbstractEventListener.class);

    {
        LOG.info("tworzenie " + this.getClass().getSimpleName());
    }

    
}