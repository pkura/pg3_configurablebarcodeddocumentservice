package pl.compan.docusafe.core.jbpm4;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;

/**
 * DecisionHandler kt�ryy zwraca:
 * "has-remarks" je�eli pismo, z kt�rym jest powi�zane ma uwagi
 * "no-remarks" je�li nie ma uwag
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class HasRemarksDecisionHandler implements DecisionHandler {
    public String decide(OpenExecution openExecution) {
        try {
        Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        return
            Documents.document(docId).getDocument().getRemarks().size() > 0 ?
                    "has-remarks" :
                    "no-remarks";
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }
}
