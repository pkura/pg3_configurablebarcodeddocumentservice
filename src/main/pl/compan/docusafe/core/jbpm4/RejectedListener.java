package pl.compan.docusafe.core.jbpm4;

import java.util.Date;
import org.jbpm.api.listener.EventListenerExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

public class RejectedListener extends AbstractEventListener{

	private static final Logger log = LoggerFactory.getLogger(RejectedListener.class);
	private String acception;
	public void notify(EventListenerExecution execution) throws Exception {

		log.error("execution w rejectedlistener" + execution.getVariables());
		OfficeDocument doc =Jbpm4Utils.getDocument(execution);
		
		//Przekazania do poprawy - infomracja dla użytkownika
		AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
		ahe.setSourceUser(DSApi.context().getPrincipalName());
	    ahe.setProcessName(execution.getProcessDefinitionId());
	    ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REJECTED);
	    String divisions = "";
		for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
		{
			if (!divisions.equals(""))
				divisions = divisions + " / " + a.getName();
			else
				divisions = a.getName();
		}
		ahe.setSourceGuid(divisions);
		try {
			String status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
			if (status != null)
				ahe.setStatus(status);
		} catch (Exception e){
			log.warn(e.getMessage());
		}
	    ahe.setCtime(new Date());
	    	
	    ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
	    doc.addAssignmentHistoryEntry(ahe);
	    
	    doc.getDocumentKind().logic().onRejectedListener(doc);
	}
}