package pl.compan.docusafe.core.jbpm4;

import java.util.Calendar;
import java.util.Date;

import org.jbpm.api.listener.EventListenerExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;

public class ReadOnlySetHistoryListener extends AbstractEventListener
{
	private static final Logger log = LoggerFactory.getLogger(ReadOnlySetHistoryListener.class);

	public void notify(EventListenerExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);

		// Potwierdzenie przeczytania - infomracja dla użytkownika
		AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
		ahe.setSourceUser(DSApi.context().getPrincipalName());
		ahe.setProcessName(execution.getProcessDefinitionId());
		ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_CONFIRMED);
		String divisions = "";
		for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
		{
			if (!divisions.equals(""))
				divisions = divisions + " / " + a.getName();
			else
				divisions = a.getName();
		}
		ahe.setSourceGuid(divisions);
		
		// kwestia wydzielania nowego, manualnego procesu z istniejącego
		// akceptacja w nowym procesie, pobierałaby nie właściwy STATUS dla historii dekretacji
		//ahe.setStatus(doc.getFieldsManager().getValue("STATUS").toString());
		if(doc.getFieldsManager().getValue("STATUS") != null)
			ahe.setStatus(doc.getFieldsManager().getValue("STATUS").toString());
		else
			ahe.setStatus("");
		
		ahe.setCtime(new Date());
		ahe.setFinished(true);
		ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_CC);
		doc.addAssignmentHistoryEntry(ahe);

        if(AvailabilityManager.isAvailable("addToWatchCC")) {
            DSApi.context().watch(URN.create(Document.find(docId)));
        }

		
		if (AvailabilityManager.isAvailable("PAA")){
		Date date = (Date) Calendar.getInstance().getTime();
		DateUtils data2 = new DateUtils();
    	String dataa = data2.formatCommonDateTime(date);
		doc.addWorkHistoryEntry(Audit.create(DSApi.context().getPrincipalName(),DSApi.context().getPrincipalName(),
                "Dokument został przeczytany przez użytkownika  : "+DSApi.context().getPrincipalName()+ " w dniu : " +dataa));
		}
	}
}