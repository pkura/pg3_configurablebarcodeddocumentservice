package pl.compan.docusafe.core.jbpm4;

import com.google.common.base.Function;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.jbpm.api.ProcessDefinition;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.jbpm.api.model.OpenExecution;
import org.jfree.util.Log;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.editor.BPMN2JPDLUtils;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa zawieraj�ca helpery do Jbpma
 * 
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 * 
 */
public class Jbpm4Utils
{
    private final static StringManager SM = GlobalPreferences.loadPropertiesFile(Jbpm4Utils.class.getPackage().getName(), null);
	private final static Logger LOG = LoggerFactory.getLogger(Jbpm4Utils.class);
	private final static Logger migrationLog = LoggerFactory.getLogger("migrationLog");
	public final static String JPDL = "jpdl";
	public final static String ACTIVITIES = "activities";
	public final static String BPMN = "bpmn";
	
	/**
	 * Metoda zwraca id dokumentu
	 * 
	 * @param openExecution
	 * @return
	 */
	public static Long getDocumentId(OpenExecution openExecution)
	{
		return Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
	}

	/**
	 * Zwraca Document z openExecution rzuca wyjatkiem
	 * @see Jbpm4Utils getDocumentId()
	 * @param openExecution
	 * @return
	 * @throws DocumentNotFoundException
	 * @throws EdmException
	 */
	public static OfficeDocument getDocument(OpenExecution openExecution) throws DocumentNotFoundException, EdmException
	{
		return (OfficeDocument) OfficeDocument.find(getDocumentId(openExecution), false);
	}
	
    public static String getVariable (Long executionId, String variableName) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String result = null;
		
		try {
			ps = DSApi.context().prepareStatement("select date_value_,double_value_,long_value_,string_value_,text_value_ from jbpm4_variable where EXECUTION_=? and KEY_=?");
			ps.setLong(1, executionId);
			ps.setString(2, variableName);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				if (rs.getDate(1) != null) {
					result = rs.getDate(1).toString();
				} else if (rs.getDouble(2) != 0) {
					result = String.valueOf(rs.getDouble(2));
				} else if (rs.getLong(3) != 0) {
					result = String.valueOf(rs.getLong(3));
				} else if (rs.getString(4) != null) {
					result = rs.getString(4);
				} else if (rs.getString(5) != null) {
					result = rs.getString(5);
				}
			}
			
			if (rs.next()) {
				LOG.error("Znaleziono kilka wpis�w dla zmiennej: "+variableName+" o executionId: "+ executionId.toString());
			}
			
		} catch (EdmException e) {
			LOG.error(e.toString());
		} catch (SQLException eS) {
			LOG.error(eS.toString());
		} finally {
			DSApi.context().closeStatement(ps);
			DbUtils.closeQuietly(rs);
		}
    	
    	return result;
    }
    
	public static Long getDbIdFromExecutionId(String executionId) {
		Pattern p = Pattern.compile("(\\w+)(.)(\\d+)(.)(\\d+)(.)(\\d+)");
		Matcher m = p.matcher(executionId);
		Long result = null;

		if (m.find()) {
			result = Long.valueOf(m.group(7));
		} else {
			p = Pattern.compile("(\\w+)(.)(\\d+)");
			m = p.matcher(executionId);
			if (m.find()) {
				result = Long.valueOf(m.group(3));
			} else {
				return null;
			}
		}
		
		return result;
	}

    /**
	 * 
	 * @param processName nazwa procesu.
	 * @param resourcesType typ zasobu zwi�zanego z procesem tj. plik bpmn, jpdl, activities.
	 * @return org.dom4j.Document reprezentuj�cy wybrany zas�b, b�d� null w przypadku nieznalezienia zasobu.
	 * @throws DocumentException
	 */
	public static Document getProcessResourceAsDocument(String processName, String resourcesType) throws DocumentException
	{	
		List<ProcessDefinition> pdefs = Jbpm4Provider.getInstance().getRepositoryService().createProcessDefinitionQuery().processDefinitionName(processName).list();
		if(pdefs == null || pdefs.isEmpty())
			return null;
		int latestProcessVersionIdx = pdefs.size()-1;
		ByteArrayInputStream  is = null;
		for(String resourceName : Jbpm4Provider.getInstance().getRepositoryService().getResourceNames(pdefs.get(latestProcessVersionIdx).getDeploymentId()))
		{
			if(resourceName.contains(resourcesType))
			{
				is = (ByteArrayInputStream) Jbpm4Provider.getInstance().getRepositoryService().getResourceAsStream(pdefs.get(latestProcessVersionIdx).getDeploymentId(), resourceName);
				break;
			}
		}
		return is == null ? null : new SAXReader().read(is);	
	}
	
	
	/**
	 * Metoda tworzy plik archiwum zip zawieraj�cy pliki bpmn.xml, jpdl.xml, activities.xml.
	 * 
	 * @param archiveName nazw� kt�r� przyjmie arichum zip oraz zwarte w nim pliki .bpmn.xml, jpdl.xml, activities.xml, w przypadku warto�ci null nazwa jest ustawiany na warto�� 'default_name'
	 * @param bpmnDocument org.dom4j.Document reprezentuj�cy plik bpmn, na podstawie kt�rego zostan� wygenerowane pliki jpdl oraz activities.
	 * @return java.io.File reprezentuj�cy archiwum zip z plikami bpmn, jpdl, activities.
	 * @throws FileNotFoundException 
	 * @throws IOException
	 * @throws EdmException
	 */
	public static File createJBPMZipArchive(String archiveName, Document bpmnDocument) throws IOException{
		String tempArchiveDir = Docusafe.getHome().getAbsolutePath() + File.separator + BPMN + File.separator;
		
		if(archiveName == null)
			archiveName = "default_name";
		Map<String, Document> processResourceDocuments = new HashMap<String, Document>();
		processResourceDocuments.put(Jbpm4Utils.BPMN, (Document)bpmnDocument.clone());
		processResourceDocuments.putAll(BPMN2JPDLUtils.bpmn2jpdl(bpmnDocument));
		File zipArchive = new File(tempArchiveDir + archiveName + ".zip");
	    ZipOutputStream zipOutputStream = null;
	    File archivedFile = null;
	    XMLWriter writer = null;
	    FileInputStream fileInputStream = null;
	    OutputFormat format = OutputFormat.createPrettyPrint();
	    format.setEncoding("UTF-8");
		try {
			zipOutputStream = new ZipOutputStream(new FileOutputStream(zipArchive));
		    int maxBufferSize = 1024 * 1024, bytesRead, bytesAvailable, bufferSize;
		    byte [] buffer;
			String archivedFileName = null;   
		    for(String fileType : processResourceDocuments.keySet()){
	//	    	archived file creation
		    	archivedFileName = archiveName + "." + fileType + ".xml";
		    	archivedFile = new File(tempArchiveDir + archivedFileName);
	//	    	jpdl process renaming
		    	if(fileType.equals(JPDL)){
					renameProcess(processResourceDocuments.get(fileType).getDocument().getRootElement(), archiveName);
		    	}
	//	    	creating xml file from dom4j.Document
				writer = new XMLWriter(new FileWriter(archivedFile), format);
	        	writer.write(processResourceDocuments.get(fileType));
	        	writer.close(); 
	//        	archive entry creation
	        	zipOutputStream.putNextEntry(new ZipEntry(archivedFileName));
	        	fileInputStream = new FileInputStream(archivedFile);
	        	bytesAvailable = fileInputStream.available();
	    		bufferSize = Math.min(bytesAvailable, maxBufferSize);
	    		buffer = new byte[bufferSize];
	    		bytesRead = fileInputStream.read(buffer, 0, bufferSize);
	    		while (bytesRead > 0) {
	    			zipOutputStream.write(buffer, 0, bufferSize);
	    			bytesAvailable = fileInputStream.available();
	    			bufferSize = Math.min(bytesAvailable, maxBufferSize);
	    			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
	    		}
	            fileInputStream.close();
		        zipOutputStream.closeEntry();
	//	        temporary file deletion
		        archivedFile.delete();
		    }
		    return zipArchive;
	    } catch (Exception e) {
	    	if(zipArchive != null && zipArchive.exists())
	    		zipArchive.delete();
			Log.error("ERROR: " + e.getMessage());
		} finally{
			zipOutputStream.close();
		}
		return null;
	}
	
	/**
	 * Metoda modyfikuj�ca defaultow� nazw� nadan� procesowi przez edytor bpmn
	 * 
	 * @param processElement
	 * @param newName
	 * @throws EdmException
	 */
	private static void renameProcess(Element processElement, String newName) throws EdmException{
		String elemName = processElement.getName().toLowerCase();
		if(elemName.equals("process")){
			processElement.attribute("name").setValue(newName);
		}else{
			throw new EdmException("ERROR: spodziewano si� elementu 'process' a znaleziono element '" + elemName + "'");
		}
	}

    public static List<String> validateXml(final Document jpdl, final Document activities)
    {
        List<String> errors = new LinkedList<String>();

        String transitionToPatch = "/*[name()='process']/*/*[name()='transition']/@to"; //jpdl
        String elementNamesPatch = "/*[name()='process']/*/@name"; //jpdl

        @SuppressWarnings("unchecked")
        Multimap<String,Node> transitionsMap = Multimaps.index(jpdl.selectNodes(transitionToPatch), new Function<Node, String>()
        {
            @Override
            public String apply(Node node)
            {
                return node.getText();
            }
        });

        @SuppressWarnings("unchecked")
        Multimap<String,Node> elementsMap = Multimaps.index(jpdl.selectNodes(elementNamesPatch), new Function<Node, String>()
        {
            @Override
            public String apply(Node node)
            {
                return node.getText();
            }
        });

        Set<String> dif = new HashSet<String>(transitionsMap.keySet());

        dif.removeAll(elementsMap.keySet());

        for(String key : dif)
        {
            for(Node n : transitionsMap.get(key))
            {
                errors.add(SM.getString("BrakElementuDocelowego",JPDL,key,n.getParent().asXML()));
            }
        }

        if(activities == null)
            return errors;

        String buttonsPatch     = "/*[name()='activities']/*[name()='state']/*[name()='button']/@name"; //activities
        String transtionsPatch  = "/*[name()='process']/*[name()='task']/*[name()='transition']/@name"; //jpdl

        @SuppressWarnings("unchecked")
        Multimap<String,Node> buttonsMap = Multimaps.index(activities.selectNodes(buttonsPatch), new Function<Node, String>() {
            @Override
            public String apply(Node node)
            {
                return node.getText();
            }
        });

        @SuppressWarnings("unchecked")
        Multimap<String,Node> transitionsForButtonsMap = Multimaps.index(jpdl.selectNodes(transtionsPatch), new Function<Node, String>() {
            @Override
            public String apply(Node node)
            {
                return node.getText();
            }
        });

        dif = new HashSet<String>(buttonsMap.keySet());
        dif.removeAll(transitionsForButtonsMap.keySet());

        for(String key : dif)
        {
            for(Node n : buttonsMap.get(key))
            {
                errors.add(SM.getString("BrakElementuDocelowego",ACTIVITIES,key,n.getParent().asXML()));
            }
        }

        return  errors;
    }

    public static List<String> validateZip(final File zipFile)
    {
        SAXReader saxReader = new SAXReader();
        Document jpdl = null;
        Document activities = null;
        ZipInputStream zis = null;

        try
        {
            zis = new ZipInputStream(new FileInputStream(zipFile));
            ZipEntry ze = null;
            while((ze = zis.getNextEntry())!=null)
            {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                IOUtils.copy(zis,baos);
                ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());

                if(ze.getName().contains(JPDL))
                {
                    jpdl = saxReader.read(bais);
                }
                else if(ze.getName().contains(ACTIVITIES))
                {
                    activities = saxReader.read(bais);
                }
                bais.close();
                baos.close();
                zis.closeEntry();
            }
        }
        catch (Exception e) { return Arrays.asList("Bledny plik"); }
        finally { IOUtils.closeQuietly(zis);}

        return validateXml(jpdl,activities);
    }



/***
 * Metoda migruje zadania ze starego (internal) Workflow do JBPM
 */
	public static void migrationWorkflow() {
		// TODO Auto-generated method stub
		
	}
}