package pl.compan.docusafe.core.jbpm4;

import com.google.common.collect.Sets;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.ocr.OcrUtils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.util.Map;
import java.util.Set;

public class SaveAttachmentOnDiskListener implements ExternalActivityBehaviour
{

	private final static Logger LOG = LoggerFactory.getLogger(SaveAttachmentOnDiskListener.class);
	final static Set<String> ocrExtension = Sets.newHashSet("tiff","tif","gif","pdf","jpg");
	
	@Override
	public void execute(ActivityExecution exec) throws Exception
	{
		LOG.info("Start saving attachment on disk");
		boolean sended = false;
		boolean isOpened = true;
		Long docId = null;
		if (!DSApi.isContextOpen())
		{
			isOpened = false;
			DSApi.openAdmin();
		}
		try
		{
			String ocrPut = Docusafe.getAdditionProperty("ocr-put");
			if (ocrPut == null)
				throw new EdmException("Brak parametru 'ocr-put' w adds.properties, okreslajacego miejsce skladowania zalocznikow do ocr");
			
			docId = Long.valueOf(exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			LOG.info("Saving atachment for document ID: {}:", docId);
			OfficeDocument doc = OfficeDocument.find(docId);
			if (doc.getAttachments().size() == 0)
				sended = true;
			for (Attachment att : doc.getAttachments())
			{
				sended = save(ocrPut, att.getMostRecentRevision(), docId);
				break; // tylko jeden
			}
		}
		catch (Exception e)
		{
			LOG.error("DocID: " + docId + " - " + e.getMessage(), "");
		}
		finally
		{
			LOG.info("Result of save attachment: {}", sended);
			if (DSApi.isContextOpen() && !isOpened)
				DSApi.close();
			if (sended)
				exec.take("success");
			else
				exec.take("repeat");
		}

	}
	
	private boolean save(String dirToSave, AttachmentRevision att, Long docId)
	{

		String oldType = OcrUtils.fileExtension(att.getOriginalFilename());
		if(!SaveAttachmentOnDiskListener.ocrExtension.contains(oldType.toLowerCase()))
		{
			LOG.warn("DocId: " + docId + ": " + oldType + " nie jest wspierane przez ocr");
			return true;
		}
		try
		{
			File tempFile = new File(dirToSave + att.getFileName().replace(".DAT", "."+oldType));
			att.saveToFile(tempFile);
		}
		catch (Exception e)
		{
			LOG.error("DocID: " + docId + " - " + e.getMessage(), "");
			return false;
		}
		return true;
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub

	}
}
