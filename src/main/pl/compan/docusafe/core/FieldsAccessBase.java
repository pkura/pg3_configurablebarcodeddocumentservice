package pl.compan.docusafe.core;

import pl.compan.docusafe.api.Fields;

/**
 * Implementacja cz�ci metod FieldAccess
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public abstract class FieldsAccessBase implements Fields {

    public final void clear() {
        throw new UnsupportedOperationException("business attributes cannot be cleared");
    }

    public final Object remove(Object key) {
        throw new UnsupportedOperationException("business attributes cannot be removed");
    }

    @Override
    public <T> T get(Object key, Class<T> clazz) throws ClassCastException {
        return clazz.cast(get(key));
    }
}
