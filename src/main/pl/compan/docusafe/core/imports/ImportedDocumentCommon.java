package pl.compan.docusafe.core.imports;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.EdmHibernateException;

import java.util.Date;
import java.util.List;
import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Criteria;

/**
 * Wsp�lne dane dla wszystkich typ�w importowanych dokument�w
 * 
 * @version $Id$
 */
public class ImportedDocumentCommon
{
    private Long id;
    private ImportedFileInfo importedFileInfo;

    /**
     * tutaj dodawa� nowe dane:
     */
    private Integer status;
    private Integer result;
    private String errorInfo;
    private Long documentId;
    private Date processedTime;
    private Integer numberInFile;
    
    public ImportedDocumentCommon()
    {

    }

    public synchronized void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List<ImportedDocumentCommon> find(Long importedFileInfoId, Integer status) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(ImportedDocumentCommon.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.eq("status", status));
            c.createCriteria("importedFileInfo").add(org.hibernate.criterion.Expression.eq("id", importedFileInfoId));

            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List<ImportedDocumentCommon> find(Long importedFileInfoId) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(ImportedDocumentCommon.class);

        try
        {
            c.createCriteria("importedFileInfo").add(org.hibernate.criterion.Expression.eq("id", importedFileInfoId));

            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Integer countDocuments(ImportedFileInfo importedFileInfo) throws EdmException
    {
        try
        {
            String q = "select count(*) as ile from ds_imported_document_info where imported_file_id = ?";
    	    java.sql.PreparedStatement ps = DSApi.context().prepareStatement(q);
            ps.setLong(1, importedFileInfo.getId());
            return countDocuments(ps);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

    public static Integer countProcessedDocuments(ImportedFileInfo importedFileInfo) throws EdmException
    {
        try
        {
            String q = "select count(*) as ile from ds_imported_document_info where imported_file_id = ? and (status = ? or status = ?)";
    	    java.sql.PreparedStatement ps = DSApi.context().prepareStatement(q);
            ps.setLong(1, importedFileInfo.getId());
            ps.setInt(2, ImportManager.STATUS_DONE);
            ps.setInt(3, ImportManager.STATUS_ERROR); // wiem ze te sa bledne od samego poczatku (ale tez je musze wliczyc jako przetworzone)
            return countDocuments(ps);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

    public static Integer countBadDocuments(ImportedFileInfo importedFileInfo) throws EdmException
    {
        try
        {
            String q = "select count(*) as ile from ds_imported_document_info where imported_file_id = ? and status = ? and result = ?";
    	    java.sql.PreparedStatement ps = DSApi.context().prepareStatement(q);
            ps.setLong(1, importedFileInfo.getId());
            ps.setInt(2, ImportManager.STATUS_DONE);
            ps.setInt(3, ImportManager.RESULT_ERROR);
            return countDocuments(ps);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
    }

    private static Integer countDocuments(java.sql.PreparedStatement ps) throws SQLException
    {
        java.sql.ResultSet rs = ps.executeQuery();
        rs.next();
        int resp = 0;
        if (rs.getString("ile")!=null)
        {
            resp = rs.getInt("ile");
        }
        rs.close();
        DSApi.context().closeStatement(ps);
        return resp;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public ImportedFileInfo getImportedFileInfo()
    {
        return importedFileInfo;
    }

    public void setImportedFileInfo(ImportedFileInfo importedFileInfo)
    {
        this.importedFileInfo = importedFileInfo;
    }

    public Integer getStatus()
    {
        return status;
    }

    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getResult()
    {
        return result;
    }

    public void setResult(Integer result)
    {
        this.result = result;
    }

    public String getErrorInfo()
    {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo)
    {
        this.errorInfo = errorInfo;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Date getProcessedTime()
    {
        return processedTime;
    }

    public void setProcessedTime(Date processedTime)
    {
        this.processedTime = processedTime;
    }

    public Integer getNumberInFile()
    {
        return numberInFile;
    }

    public void setNumberInFile(Integer numberInFile)
    {
        this.numberInFile = numberInFile;
    }

}
