package pl.compan.docusafe.core.imports;

import com.google.common.base.Function;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.api.Fields;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.EnumRefField;
import pl.compan.docusafe.core.dockinds.field.XmlEnumColumnField;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class KeyValueImports {
    private final static Logger LOG = LoggerFactory.getLogger(KeyValueImports.class);
    private KeyValueImports(){}

    public static Function<KeyValueEntity, ImportEntryStatus> dicInvoice(){
        return dicInvoiceImport;
    }

    public static Function<KeyValueEntity, ImportEntryStatus> contractIc() throws EdmException {
        return new ContractIcImport();
    }

    private final static Function<KeyValueEntity, ImportEntryStatus> dicInvoiceImport
            = new Function<KeyValueEntity, ImportEntryStatus>() {
        public ImportEntryStatus apply(KeyValueEntity keyValueEntity) {
            String number = keyValueEntity.string("NUMBER");
            try{
                if(DicInvoice.isNumberDuplicate(number)){
                    return ImportEntryStatus.DUPLICATE_NOT_ADDED;
                }
                DicInvoice entry = new DicInvoice();
                entry.setName(keyValueEntity.string("NAME"));
                entry.setNip(keyValueEntity.string("NIP"));
                entry.setNumerKontrahenta(number);
                entry.setsenderCountry(keyValueEntity.string("COUNTRY"));
                DSApi.context().session().save(entry);
            } catch (Exception ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
            return ImportEntryStatus.ADDED;
        }
    };

    private final static class ContractIcImport implements Function<KeyValueEntity, ImportEntryStatus> {

        private final File directory = new File(Docusafe.getHome(), "ic-import");
        private final DocumentKind dk;

        public ContractIcImport() throws EdmException {
            dk = DocumentKind.findByCn("contract_ic");
        }

        public ImportEntryStatus apply(KeyValueEntity keyValueEntity) {
            try {
                return applyUnsafe(keyValueEntity);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }

        public ImportEntryStatus applyUnsafe(KeyValueEntity keyValueEntity) throws Exception{
            String boxNumber = keyValueEntity.string("BOX_BARCODE");
            String docNumber = keyValueEntity.string("DOCUMENT_NUMBER");
            String dateString = keyValueEntity.string("DOCUMENT_DATE");

            String barcode = keyValueEntity.string("BARCODE");
            String subtypeString = keyValueEntity.string("SUBTYPE");
            String file = keyValueEntity.string("FILE");

            File attachment = new File(file);
            attachment = new File(directory, attachment.getName());
            if(!attachment.isFile()){
                throw new IllegalStateException(attachment.getAbsolutePath() + " doesnt exists");
            }

            PreparedStatement ps = null;
            ResultSet rs = null;
            if(!StringUtils.isBlank(barcode)){
                try {
                    ps = DSApi.context().prepareStatement("SELECT COUNT(1) FROM dsg_contract_ic WHERE barcode = ?");
                    ps.setString(1, barcode);
                    rs = ps.executeQuery();
                    rs.next();
                    int size = rs.getInt(1);
                    if(!rs.wasNull() && size >= 1){//duplicate
                        LOG.error("duplicate barcode : {} - not importing", barcode);
                        return ImportEntryStatus.DUPLICATE_NOT_ADDED;
                    }
                } finally {
                    DbUtils.closeQuietly(rs);
                    DSApi.context().closeStatement(ps);
                }
            } else {
                LOG.warn("brak barkodu dla pliku '{}' - nie mo�na wykry� duplikat�w", file);
            }

            LOG.info("data : {} | {} | {} | {} | {} | {}", new Object[]{boxNumber, docNumber, barcode, subtypeString, dateString, file});

            XmlEnumColumnField typeField = (XmlEnumColumnField) dk.getFieldByCn("TYPE");
            EnumRefField subtypeField = (EnumRefField) dk.getFieldByCn("SUBTYPE");

            EnumItem subtype = subtypeField.getEnumItemByCn(subtypeString);
            EnumItem type = typeField.getEnumItem((Integer)subtypeField.getEnumItemDiscriminator(subtype.getId()));

            LOG.info("type = {}; subtype = {}", type.getTitle(), subtype.getTitle());

            Document doc = new Document(subtype.getTitle(), subtype.getTitle());
            doc.setFolder(Folder.getRootFolder());
            DSApi.context().session().save(doc);

            Box box;
            try{
                box = Box.findByName(null, boxNumber);
            } catch (EntityNotFoundException ex) {
                box = new Box(boxNumber);
                DSApi.context().session().save(box);
            }
            doc.setBox(box);
            doc.setDocumentKind(dk);

            Fields f = Documents.document(doc.getId()).getFields();
            f.put("TYPE", type);
            f.put("SUBTYPE", subtype);
            f.put("DOCUMENT_NUMBER", docNumber);
            f.put("BARCODE",barcode);

            List<DicInvoice> dics = DSApi.context().session().createCriteria(DicInvoice.class).add(Restrictions.eq("numerKontrahenta", docNumber)).list();
            if(dics.isEmpty()){
                LOG.warn("brak kontrahenta o numerze " + docNumber);
            } else {
                f.put("COMPANY", dics.get(0));
            }

            if(!StringUtils.isBlank(dateString)){
                Date date = DateUtils.sqlDateFormat.parse(dateString);
                f.put("DOCUMENT_DATE",date);
            }

            Attachment att = new Attachment("skan.tiff");
            doc.addAttachment(att);
            DSApi.context().session().save(att);
            att.createRevision(attachment);

            dk.logic().archiveActions(doc, 1337);
            dk.logic().documentPermissions(doc);

            return ImportEntryStatus.ADDED;
        }
    };
}
