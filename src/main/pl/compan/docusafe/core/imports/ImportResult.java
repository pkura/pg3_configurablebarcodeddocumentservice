package pl.compan.docusafe.core.imports;

import com.google.common.collect.Lists;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ImportResult {
    private final static Logger LOG = LoggerFactory.getLogger(ImportResult.class);
    private final List<String> errorMessages = Lists.newArrayList();


    /**
     * Licznik wszystkich wpis�w, w��cznie z tymi kt�re spowodowa�y ewentualne b��dy w trakcie importu
     */
    private Long entriesCounter = new Long(0);
    private long added = 0;
    private long updated = 0;
    private long duplicate = 0;
    private long ignored = 0;
    private long errors = 0;
    private long invalid = 0;

    public void report(ImportEntryStatus status){
        ++entriesCounter;
        switch(status){
            case ADDED: ++added; break;
            case DUPLICATE_NOT_ADDED: ++duplicate; break;
            case UPDATED: ++updated; break;
            case IGNORED: ++ignored; break;
            case INVALID_INPUT: ++invalid; break;
        }
    }

    public void reportError(Exception ex){
        ++entriesCounter;
        errorMessages.add(ex.getMessage());
        ++errors;
    }

    public void reportError(String message){
        ++entriesCounter;
        errorMessages.add(message);
        ++errors;
    }

    public long getTotalCount(){
        return added + duplicate + errors + updated;
    }

    public List<String> getErrorMessages(){
        return errorMessages;
    }

    public long getAddedCount(){
        return added;
    }

    public long getDuplicateCount(){
        return duplicate;
    }

    public long getErrorCount(){
        return errors;
    }

    public long getUpdatedCount(){
        return updated;
    }

    public long getIgnoredCount(){
        return ignored;
    }

    public long getInvalidCount(){
        return invalid;
    }

    public Long getEntriesCounter(){
        return entriesCounter;
    }
}