package pl.compan.docusafe.core.imports;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.service.imports.ImportHandler;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class ImportManager
{
    private static final Log log = LogFactory.getLog(ImportManager.class);
    
    public static final Integer TYPE_AEGON_BIZ_SIMPLE = 1;
    public static final Integer TYPE_OGOLNY_XML = 2;
    public static final Integer TYPE_ALD_FAKTURY = 3;
    public static final Integer TYPE_AEGON_FINANSOWY = 4;
    public static final Integer TYPE_AEGON_PRAWNY = 5;
    public static final Integer TYPE_DAA = 6;
    public static final Integer TYPE_AEGON_PTE = 7;
    public static final Integer TYPE_EMPLOYEE = 8;
    public static final Integer TYPE_NATIONWIDE = 9;
    public static final Integer TYPE_NATIONWIDE_OUTGOING = 10;

    public static final Integer STATUS_WAITING = 1;
    public static final Integer STATUS_WORKING = 2;
    public static final Integer STATUS_DONE = 3;
    public static final Integer STATUS_ERROR = 4;
    public static final Integer STATUS_PARSE = 5;
    
    /**
     * status odnoszacy sie tylko do ImportedDocumentInfo (nie uzywany przy ImportedFileInfo)
     * oznacza, ze byl blad w danych pobranych z pliku ".csv"
     * i nie bylo/nie bedzie proby zaimportowania tego dokumentu
     */
    

    public static final Integer RESULT_OK = 1;
    public static final Integer RESULT_ERROR = 2;

    public static final String CSV_FILE_SEPARATOR = ";";

    public static boolean isCsvImport(Integer type, boolean includeBizSimple)
    {
        return (type.equals(TYPE_AEGON_BIZ_SIMPLE) && includeBizSimple) ||
            type.equals(TYPE_ALD_FAKTURY) ||
            type.equals(TYPE_AEGON_FINANSOWY) ||
            type.equals(TYPE_AEGON_PRAWNY) ||
            type.equals(TYPE_DAA)||
            type.equals(TYPE_AEGON_PTE);
    }
    
    public static boolean isCsvImport(Integer type)
    {
        return isCsvImport(type, true);
    }
    
    public static String getDockindCn(Integer type)
    {
        if (type.equals(TYPE_ALD_FAKTURY))
            return DocumentLogicLoader.ALD_KIND;
        else if (type.equals(TYPE_AEGON_FINANSOWY))
            return DocumentLogicLoader.DF_KIND;
        else if (type.equals(TYPE_AEGON_PRAWNY))
            return DocumentLogicLoader.DP_KIND;
        else if (type.equals(TYPE_DAA))
            return DocumentLogicLoader.DAA_KIND;
        else if (type.equals(TYPE_AEGON_PTE))
            return DocumentLogicLoader.DC_KIND;
        else if (type.equals(TYPE_EMPLOYEE))
            return DocumentLogicLoader.EMPLOYEE_KIND;
        else if (type.equals(TYPE_NATIONWIDE))
        	return DocumentLogicLoader.NATIONWIDE_KIND;
        else if (type.equals(TYPE_NATIONWIDE_OUTGOING))
        	return DocumentLogicLoader.NATIONWIDE_KIND;
        return "normal";
    }
    
    public static String getStatusString(Integer status)
    {
        if (STATUS_DONE.equals(status))
            return "zako�czone";
        else if (STATUS_ERROR.equals(status))
            return "b��dny plik";
        else if (STATUS_WAITING.equals(status))
            return "nierozpocz�te";
        else if (STATUS_WORKING.equals(status))
            return "w trakcie";
        else if (STATUS_PARSE.equals(status))
            return "parsuje";

        return null;
    }

    public static String getResultString(Integer result)
    {
        if (RESULT_ERROR.equals(result))
            return "B��d";
        else if (RESULT_OK.equals(result))
            return "OK";

        return null;
    }

    /**
	 *Sprawdza czy mo�emy importowa� z danej �cie�ki na podstawie addsa "import.documents.allowed_tiff_file"
	 *Importowa� mozna z tego katalogu oraz z katalog�w podrz�dnych
	 *Je�li nie podano w adds �adnego katalogu, zezwala na wszystkie
	 */
    public static boolean isTiffDirectoryAllowed(String directory, Integer importType) throws EdmException
    {
        String configFilename = Configuration.getProperty("import.documents.allowed_tiff_file");
        if (configFilename == null)
            return true;
        try
        {
            File config = new File(Docusafe.getHome(), configFilename);
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(config)));
            String line;
            while ((line = reader.readLine()) != null)
            {
                if (directory.toLowerCase().startsWith(line.toLowerCase()))
                    return true;
            }
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas czytania z pliku konfiguracyjnego z dozwolonymi katalogami importu");
        }
        return false;
    }

    public static void parseAegonBizSimple(File file, String tiffDirectory) throws EdmException
    {
        ImportedFileInfo importedFileInfo = new ImportedFileInfo();
        importedFileInfo.setTiffDirectory(tiffDirectory);
        importedFileInfo.setImportType(TYPE_AEGON_BIZ_SIMPLE);
        importedFileInfo.setFileName(file.getName());
        importedFileInfo.setStatus(STATUS_WAITING);
        
    	DSApi.context().begin();
    	importedFileInfo.create();
    	DSApi.context().commit();
       
        try
        {
        	DSApi.context().begin();
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line;
            int lineCount = 0;
            while ((line = reader.readLine()) != null)
            {
                lineCount++;
                if (line.trim().startsWith("#"))
                    continue;
                String[] fields = line.split("\\s*;\\s*");
                ImportedDocumentInfo importedDocumentInfo = new ImportedDocumentInfo();
                importedDocumentInfo.setStatus(STATUS_WAITING);
                importedDocumentInfo.setLineNumber(lineCount);

                if (fields.length >= 1)
                    importedDocumentInfo.setTiffName(fields[0]);
                if (fields.length >= 2)
                    importedDocumentInfo.setNrPolisy(fields[1]);
                if (fields.length != 5 )
                {
                    importedDocumentInfo.setStatus(STATUS_ERROR);
                    importedDocumentInfo.setResult(RESULT_ERROR);
                    importedDocumentInfo.setErrorInfo("Niepoprawnie opisany dokument - oczekiwano dok�adnie 5 warto�ci (by�o "+fields.length+")");
                }
                else
                {
                    Date date = parseDate(fields[2]);
                    //Date date = DateUtils.parseJsDate(fields[2]);
                    //Parsowal Date w formacie DD-MM-YYYY - zakladalismy format YYYY-MM-DD
                    //Przenies to pozniej do jakiejs bardziej ogolnej czesci kodu


                    if (date != null)
                        importedDocumentInfo.setDate(date);
                    else
                    {
                        importedDocumentInfo.setStatus(STATUS_ERROR);
                        importedDocumentInfo.setErrorInfo("Podana data jest niepoprawna");
                    }
                    try
                    {
                        importedDocumentInfo.setTypDokumentuId(Integer.parseInt(fields[3]));
                    }
                    catch (java.lang.NumberFormatException e)
                    {
                        importedDocumentInfo.setStatus(STATUS_ERROR);
                        importedDocumentInfo.setErrorInfo("Podany identyfikator typu dokumentu nie jest liczb�");
                    }
                    // TODO: zrobic wywalanie bledu jesli nie istnieje typ dokumentu o podanym id  i sprawdzac tez czy podany tiff istnieje
                    importedDocumentInfo.setBoxNumber(fields[4]);
                }

                importedDocumentInfo.setImportedFileInfo(importedFileInfo);
                importedFileInfo.getImportedDocuments().add(importedDocumentInfo);
                
                
                importedDocumentInfo.create();
                
            }
            DSApi.context().commit();
            
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas czytania z pliku z danymi do importu");
        }
        finally
        {
        	DSApi.context().setRollbackOnly();
        }
    }
    
    public static void addImportedFileInfo(File file, String tiffDirectory, Integer importType) throws EdmException
    {
    	try
    	{
	    	DSApi.context().begin();
	        ImportedFileInfo importedFileInfo = new ImportedFileInfo();
	        importedFileInfo.setTiffDirectory(tiffDirectory);
	        importedFileInfo.setImportType(importType);
	        importedFileInfo.setFileName(file.getName());
	        importedFileInfo.setStatus(STATUS_PARSE);
	        importedFileInfo.create();
	        DSApi.context().commit();
	        saveFileInDirectory(file, tiffDirectory);
    	}
    	catch (Exception e) 
    	{
			log.error("",e);
			DSApi.context().rollback();
		}
    }
    
    private static void saveFileInDirectory(File file, String tiffDirectory) throws EdmException
    {
    	try
    	{
			File tmp = new File(tiffDirectory+"/"+file.getName());
	        FileOutputStream os = new FileOutputStream(tmp);
	        FileInputStream stream = new FileInputStream(file);
	        byte[] buf = new byte[1024];
	        int count;
	        while ((count = stream.read(buf)) > 0)
	        {
	            os.write(buf, 0, count);
	        }
	        os.close();
    	}
    	catch (Exception e) 
    	{
			throw new EdmException(e);
		}
    }
    
    public static void parseCsvToDSI(ImportedFileInfo iFi)
    {
    	//DSIManager
    }

    public static void parseCsv(File file, String tiffDirectory, Integer importType) throws EdmException
    {
        String typeCn = null;
        if (!isCsvImport(importType, false))
        {
            throw new EdmException("Nieznany typ importu: " + importType);
        }
        
        typeCn = ImportManager.getDockindCn(importType);
        
        
        ImportedFileInfo importedFileInfo = new ImportedFileInfo();
        importedFileInfo.setTiffDirectory(tiffDirectory);
        importedFileInfo.setImportType(importType);
        importedFileInfo.setFileName(file.getName());
        importedFileInfo.setStatus(STATUS_WAITING);
        DSApi.context().begin();
        importedFileInfo.create();
        ImportedFileCsv importedFileCsv = new ImportedFileCsv();
        importedFileCsv.setInfo(importedFileInfo);
        //importedFileCsv.setTypeId(importType);
        importedFileCsv.create();        
        
        //�eby zadzia�a� update przy wstawianiu bloba(chyba lekko na wyrost)
        DSApi.context().commit();
        //DSApi.close();          
        //DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
        

        importedFileCsv.setCsvData(file);

        ImportHandler handler = ImportHandler.getInstance(typeCn);
        if (handler == null)
        {
            throw new EdmException("Nie znaleziono klasy handlera importu: " + typeCn);
        }
        
        try
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line;
            int lineCount = 0;
            while ((line = reader.readLine()) != null)
            {
                lineCount++;
                System.out.println("LINE "+ lineCount);
                if (line.trim().startsWith("#") || line.trim().length() == 0)
                    continue;
                ImportedDocumentInfo importedDocumentInfo = handler.createDocumentInfo(line, lineCount);

                if (importedDocumentInfo != null)
                {
                    try
                    {
                    	DSApi.context().begin();
                        importedDocumentInfo.setImportedFileInfo(importedFileInfo);
                        importedFileInfo.getImportedDocuments().add(importedDocumentInfo);
                        importedDocumentInfo.create();
                        DSApi.context().commit();
                    }
                    catch (EdmException e)
                    {
                    	DSApi.context().setRollbackOnly();
                    	DSApi._close();
                    	DSApi.openAdmin();
                        //je�eli w informacjach dla file info jest co� nie tak(np. data nie
                        //akceptowana przez serwer sql) to pomi� lini�
                    }
                }
            }
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas czytania z pliku z danymi do importu");
        }
    }
    
    

    public static void parseAndAddImportedFile(File file, String tiffDirectory, Integer importType) throws EdmException
    {
        if (TYPE_AEGON_BIZ_SIMPLE.equals(importType))
            parseAegonBizSimple(file, tiffDirectory);
        else
        {
            parseCsv(file, tiffDirectory, importType);
        }
    }

    /**Startuje import DSI zmieniaj�c status Beanow z 3 na 1 */
    public static void startDSIImport(Long importedFileId) throws EdmException, SQLException
    {
    	DSIManager manager = new DSIManager();
    	ImportedFileInfo importedFileInfo = ImportedFileInfo.find(importedFileId);
        if (!STATUS_WAITING.equals(importedFileInfo.getStatus()))
            throw new EdmException("Nie mo�na rozpocz�� importu podanego pliku");
    	manager.startUserImport(importedFileId.toString());
    	importedFileInfo.setStatus(STATUS_WORKING);
        log.info("importedFileInfo.setStatus(STATUS_WORKING)");
    }
    
    public static void startImport(Long importedFileId) throws EdmException
    {
        ImportedFileInfo importedFileInfo = ImportedFileInfo.find(importedFileId);

        if (!STATUS_WAITING.equals(importedFileInfo.getStatus()))
            throw new EdmException("Nie mo�na rozpocz�� importu podanego pliku");

        importedFileInfo.setStatus(STATUS_WORKING);
        log.info("importedFileInfo.setStatus(STATUS_WORKING)");

        log.info("Rozpocz�cie ustawiania status�w importowanych dokument�w");
        for (ImportedDocumentInfo importedDocumentInfo : importedFileInfo.getImportedDocuments())
        {
            if (STATUS_WAITING.equals(importedDocumentInfo.getStatus()))
                importedDocumentInfo.setStatus(STATUS_WORKING);
        }
        log.info("Zako�czenie ustawiania status�w importowanych dokument�w");
    }
    
    /**
     * Generuje raport importu DSI w formacie csv o podanym pliku do importu i wypisuje na strumien 'output'.
     *
     * @param importedFileId identyfikator obiektu ImportedFileInfo, dla ktorego bedzie wygenerowany raport
     * @param output strumien wyjsciowy, na ktory zostanie zapisany raport
     * @throws EdmException
     */
    public static void generateDSIReport(Long importedFileId, OutputStream output) throws Exception
    {
        ImportedFileInfo importedFileInfo = ImportedFileInfo.find(importedFileId);

        PrintWriter writer = new PrintWriter(output);
        DSIManager manager = new DSIManager();
        Collection<DSIBean> coll = manager.findDSIBeanByUserImportId(DSApi.context().session().connection(),importedFileInfo.getId().toString(), false);

        for (DSIBean bean : coll)
		{
			String tiffName = bean.getFilePath();
			if (tiffName == null)
                tiffName = "brak nazwy za��cznika";
			 writer.print(tiffName + CSV_FILE_SEPARATOR);
			 writer.print((bean.getImportStatus().equals(DSIManager.IMPORT_STATUS_OK)?"OK" : bean.getErrCode()) + CSV_FILE_SEPARATOR);
			 writer.print((bean.getImportStatus().equals(DSIManager.IMPORT_STATUS_OK) ? "Utworzono dokument o id="+bean.getDocumentId()
					 :" B��D ")+CSV_FILE_SEPARATOR);
			 writer.print("\n");			 
		}
        writer.close();
    }

    /**
     * Generuje raport w formacie csv o podanym pliku do importu i wypisuje na strumien 'output'.
     *
     * @param importedFileId identyfikator obiektu ImportedFileInfo, dla ktorego bedzie wygenerowany raport
     * @param output strumien wyjsciowy, na ktory zostanie zapisany raport
     * @throws EdmException
     */
    public static void generateReport(Long importedFileId, OutputStream output) throws EdmException
    {
        ImportedFileInfo importedFileInfo = ImportedFileInfo.find(importedFileId);

        PrintWriter writer = new PrintWriter(output);

        for (ImportedDocumentInfo importedDocumentInfo : importedFileInfo.getImportedDocuments())
        {
            String tiffName = importedDocumentInfo.getTiffName();
            if (tiffName == null)
                tiffName = "brak nazwy za��cznika";
            writer.print(tiffName + CSV_FILE_SEPARATOR);
            String result = ImportManager.getResultString(importedDocumentInfo.getResult());
            writer.print(result==null?"Rezultat nieznany":result + CSV_FILE_SEPARATOR);
            writer.println(RESULT_OK.equals(importedDocumentInfo.getResult()) ?
                "Utworzono dokument o id="+importedDocumentInfo.getDocumentId() : ((importedDocumentInfo.getErrorInfo()==null)?"Rezultat nieznany":importedDocumentInfo.getErrorInfo()));
        }

        writer.close();
    }

    public static SimpleDateFormat normalFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat reverseFormat = new SimpleDateFormat("dd-MM-yyyy");

    /**
     *  Metoda parsuje date w formacie YYYY-MM-DD lub DD-MM-YYYY - jesli jest blad to zwracamy null
     */
    public static Date parseDate(String param)
    {
        Date date = null;
        try
        {
            normalFormat.setLenient(false);
            date = normalFormat.parse(param);
        }
        catch (Exception e) { date = null; }
        if (date == null)
        {
            try
            {
                reverseFormat.setLenient(false);
                date = reverseFormat.parse(param);
            }
            catch (Exception e) { date = null; }
        }
        return date;
    }

    /**
     *  Metoda parsuje date w formacie YYYY-MM-DD - jesli jest blad to zwracamy null
     *  Zwraca null tak�e wtedy kiedy rok nie mie�ci si� w granicach: 1900 - 2050 
     */
    public static Date parseDateValidated(String param)
    {
        Date date = null;
        try
        {
            DateUtils.sqlDateFormat.setLenient(false);
            date = DateUtils.sqlDateFormat.parse(param);
        }
        catch (Exception e)
        {
            return null;
        }
        
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        Integer year = cal.get(Calendar.YEAR);
        // TODO: przemyslec kwestie ograniczenia dat 
        if (year < 1900 || year > 2050)
            return null;
        return date;
    }

}
