package pl.compan.docusafe.core.imports;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.ald.ALDLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.servlet.ProgressBarPool;

public class NWOutgoingImportsManager implements Runnable
{


		File file;
		OutputStream output;
		private StringManager sm = StringManager.getManager(NWOutgoingImportsManager.class.getPackage().getName());

		public NWOutgoingImportsManager(File f, OutputStream o)
		{
			file = f;
			output = o;
		}
		public void run()
		{

			boolean result=true;
	    	String nrFakt = "";
	    	OutOfficeDocument doc;
	        try
	        {
	        	DSApi.openAdmin();
	        	DSApi.context().begin();
	        	FileInputStream is = new FileInputStream(file);
	        	HSSFWorkbook workbook = new HSSFWorkbook(is);
	            HSSFSheet sheet = workbook.getSheetAt(0);

	        	PrintWriter writer = new PrintWriter(output);
	        	writer.println("Rozpoczeto import.");
	            int lineCount = 0, errorCount = 0;

	            for(int i = sheet.getFirstRowNum();i<=sheet.getLastRowNum();i++)
	            {
	            	ProgressBarPool.getActivitiesInstance().put("test", (i/sheet.getLastRowNum())*100+"");
	            	try
	            	{

						lineCount++;
						if(i==0) continue;
						DecimalFormat df = new DecimalFormat();


						doc = new OutOfficeDocument();
						Recipient person = new Recipient();
						nrFakt = getCellString(sheet.getRow(i).getCell((short)0));
						if(nrFakt.equals(""))
						{
							lineCount--;
							continue;
						}
						person.setOrganization(getCellString(sheet.getRow(i).getCell((short)1)));


						String adres = getCellString(sheet.getRow(i).getCell((short)2));
						Pattern p = Pattern.compile("[0-9]{2}-[0-9]{3}");
						Matcher m = p.matcher(adres);
						if(m.find()){
							person.setStreet(adres.substring(0, m.start()));
							person.setZip(m.group());
							person.setLocation(adres.substring(m.end()));
						}
						else
						{
							person.setStreet(adres);
						}



						//person.setStreet(getCellString(sheet.getRow(i).getCell((short)2)));
						doc.addRecipient(person);
						Sender s = new Sender();
						String temp = getCellString(sheet.getRow(i).getCell((short)3)).replaceAll("[^0-9]", "").trim();
						if(temp.length()>0)
						{
				            doc.setWeightG(Integer.parseInt(temp.substring(0, temp.length()>3 ? 3 : temp.length())));
							doc.setWeightKg(Integer.parseInt(temp.length()>3 ? temp.substring(3): "0"));
						}

						/*try
						{
							doc.setPostalRegNumber(df.getNumberInstance().format(sheet.getRow(i).getCell((short)6).getNumericCellValue()).replace(" ", ""));
						}
						catch(NullPointerException f){

						}*/

						/*temp = sheet.getRow(i).getCell((short)7).getStringCellValue().replaceAll("[^0-9,]","").replace(",",".").trim();
						if(temp.length()>0)
							doc.setStampFee(new BigDecimal(Float.parseFloat(temp)));
						*/

						Map address = GlobalPreferences.getAddress();

						s.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
			            s.setStreet((String) address.get(GlobalPreferences.STREET));
			            s.setZip((String) address.get(GlobalPreferences.ZIP));
			            s.setLocation((String) address.get(GlobalPreferences.LOCATION));

			            doc.setSender(s);

						doc.setInternal(false);
						doc.setSummary("zaimportowane");
						doc.setAssignedDivision(DSDivision.ROOT_GUID);
						doc.setCreatingUser(DSApi.context().getPrincipalName());
						doc.setDocumentDate(DateUtils.getCurrentTime());
						doc.setClerk(DSApi.context().getPrincipalName());
						doc.setCurrentAssignmentUsername(DSApi.context().getPrincipalName());
						doc.create();
						Remark rem = new Remark(getCellString(sheet.getRow(i).getCell((short)4)) +
								getCellString(sheet.getRow(i).getCell((short)5))+ "",
								DSApi.context().getPrincipalName());
						doc.addRemark(rem);
						doc.setDescription(rem.getContent());

						Long newDocumentId = doc.getId();

						long journalId = Journal.getMainOutgoing().getId();
						int sequence = Journal.TX_newEntry2(journalId, newDocumentId, GlobalPreferences.getCurrentDay());
						doc.bindToJournal(Journal.getMainOutgoing().getId(), sequence);

						WorkflowFactory.createNewProcess(doc, true, "Pismo zaimportowane");


						org.apache.commons.io.IOUtils.closeQuietly(is);
						//DSApi.context().session().flush();

						writer.println("OK. Zaimportowano pismo numer: " + nrFakt);


					}
	                catch(Exception e)
	                {
	                	//DSApi.context().setRollbackOnly();
	                	if(!nrFakt.equals(""))
	                	{
		                	writer.println("---Blad! Nie udalo sie zaimportowac pisma nr: " + nrFakt + " " + e.getMessage());

		                	result = false;
		                	errorCount++;
	                	}else lineCount--;
	                	continue;
	                }
	                finally
	                {

	                }

	            }
	            DSApi.context().commit();
	            writer.println("Calkowita liczba blednych importow: " + errorCount);
	            writer.println("Calkowita liczba pism: " + lineCount);
	            writer.close();

	            //return result;

		}
        catch(EdmException e)
        {
        	doc = null;
        	DSApi.context().setRollbackOnly();
        	//throw new EdmException("B��d podczas czytania pliku z pismami wychodz�cymi");
        }
        catch (IOException e)
        {
        	DSApi.context().setRollbackOnly();
            //throw new EdmException("B��d podczas czytania pliku z pismami wychodz�cymi");
        }
        finally
        {
        	DSApi._close();
        }

	}



	public class ALDParser implements Runnable
    {
		File file;
		OutputStream output;
		private StringManager sm = StringManager.getManager(NWOutgoingImportsManager.class.getPackage().getName());

		public ALDParser(File f, OutputStream o)
		{
			file = f;
			output = o;
		}
		public void run()
		{
			boolean result=true;
	    	String nrFakt = "";
	        try
	        {
	        	DSApi.context().begin();
	        	PrintWriter writer = new PrintWriter(output);
	        	writer.println("Rozpocz�to import.");
	            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "cp1250"));
	            String line;
	            int lineCount = 0, errorCount = 0;
	            String opis = "";
	            float kwota = 0;
	            String[] prevParts = {""};
	            int docsCount = 0;
	            boolean koniec = false;
	            while (!koniec)
	            {
	            	if((line = reader.readLine()) == null) {
	            		line = " ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;";
	            		koniec = true;
	            	}
	                try {
						lineCount++;
						if(lineCount%10==0) DSApi.context().session().clear();
						line = line.trim();
						if (line.startsWith("#")){
						    lineCount--;
							continue;
						}

						String[] parts = line.split(";");
						if(parts.length<13) {
							lineCount--;
							continue;
						}
						if(lineCount == 1) prevParts = parts;

						if(parts[12].replace("\"","").trim().compareTo(prevParts[12].replace("\"","").trim())!=0||koniec)
						{
							docsCount++;
							OutOfficeDocument doc = new OutOfficeDocument();
							Recipient person = new Recipient();
							doc.setDescription(opis);
							nrFakt = prevParts[1].replace("\"","").trim();
							String org = prevParts[7].replace("\"","").trim() + " " + prevParts[8].replace("\"","").trim();
							person.setOrganization(org.substring(0, org.length()<50?org.length()-1:49));
							person.setStreet(prevParts[9].replace("\"","").trim());
							person.setZip(prevParts[10].replace("\"","").trim());
							person.setLocation(prevParts[11].replace("\"","").trim());
							person.setNip(prevParts[12].replace("\"","").trim());
							doc.addRecipient(person);
							Sender s = new Sender();

							Map address = GlobalPreferences.getAddress();

							s.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
				            s.setStreet((String) address.get(GlobalPreferences.STREET));
				            s.setZip((String) address.get(GlobalPreferences.ZIP));
				            s.setLocation((String) address.get(GlobalPreferences.LOCATION));

				            doc.setSender(s);

							DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.ALD_KIND);
							doc.setDocumentKind(documentKind);
							doc.setInternal(false);
							doc.setSummary(opis.substring(0,opis.length()-1) + " - zaimportowane");
							doc.setAssignedDivision(DSDivision.ROOT_GUID);
							doc.setCreatingUser(DSApi.context().getPrincipalName());
							doc.setDocumentDate(DateUtils.getCurrentTime());
							doc.setClerk(DSApi.context().getPrincipalName());
							doc.setCurrentAssignmentUsername(DSApi.context().getPrincipalName());
							doc.create();


							Long newDocumentId = doc.getId();


							FieldsManager fm = documentKind.getFieldsManager(newDocumentId);
							fm.initialize();
							Map<String, Object> values = fm.getFieldValues();

							values.put(ALDLogic.NIP_DOSTAWCY_FIELD_CN, prevParts[12].replace("\"","").trim());
							//values.put(ALDLogic.NR_FAKTURY_FIELD_CN, prevParts[1].replace("\"","").trim());
							//values.put(ALDLogic.DATA_FIELD_CN, parseDate(prevParts[2].replace("\"","").trim()));
							values.put(ALDLogic.KWOTA_BRUTTO_FIELD_CN, kwota);
							values.put(ALDLogic.KATEGORIA_FIELD_CN, fm.getField(ALDLogic.KATEGORIA_FIELD_CN).getEnumItemByCn(ALDLogic.ADMINISTRACJA_CN).getId());
							values.put(ALDLogic.DATA_PLATNOSCI_FIELD_CN, parseDate(prevParts[3].replace("\"","").trim()));
							values.put(ALDLogic.NAZWA_KLIENTA_FIELD_CN, org.substring(0, org.length()<50?org.length()-1:49));
							values.put(ALDLogic.STATUS_FIELD_CN, ALDLogic.STATUS_ZAAKCEPTOWANY);
							doc.getDocumentKind().setOnly(newDocumentId, values);


							doc.setPrepState(OutOfficeDocument.PREP_STATE_FAIR_COPY);

			                documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_OUT_OFFICE);
			                documentKind.logic().documentPermissions(doc);
			                //long journalId = Journal.getMainOutgoing().getId();		// dodawanie do dziennika. Obecnie wy��czone.
							//int sequence = Journal.TX_newEntry2(journalId, newDocumentId, GlobalPreferences.getCurrentDay());
							//doc.bindToJournal(Journal.getMainOutgoing().getId(), sequence);

							WorkflowFactory.createNewProcess(doc, true, "Pismo zaimportowane");



							DSApi.context().session().flush();

							writer.println("OK. Zaimportowano faktur� numer: " + nrFakt);
							prevParts = parts;
							if(!koniec){
								opis = parts[1].replace("\"","").trim() + ",";
								kwota = Float.parseFloat((parts[4].replace(",", ".").trim()));
							}

						}
						else
						{
							opis += parts[1].replace("\"","").trim() + ",";
							kwota += Float.parseFloat((parts[4].replace(",", ".").trim()));
						}

					}
	                catch(EdmException e)
	                {
	                	writer.println("---B��d! Nie uda�o si� zaimportowa� faktury nr: " + nrFakt);
	                	result = false;
	                	errorCount++;
	                	continue;
	                }
	                catch(Exception e)
	                {
	                	writer.println("---B��d! Nie uda�o si� zaimportowa� faktury nr: " + nrFakt +e.getMessage());
	                	LogFactory.getLog("eprint").debug("", e);
	                	result = false;
	                	errorCount++;
	                	continue;
	                }

	            }
	            writer.println("Ca�kowita liczba b��dnych import�w: " + errorCount);
	            writer.println("Ca�kowita liczba faktur: " + lineCount);
	            writer.println("Ca�kowita liczba dokument�w: " + docsCount);
	            writer.close();
	            DSApi.context().commit();
	            //return result;

	        }
	        catch (EdmException e)
	        {
	        	DSApi.context().setRollbackOnly();
	        	//throw new EdmException("B��d podczas czytania pliku z pismami wychodz�cymi");
	        }
	        catch (IOException e)
	        {
	        	DSApi.context().setRollbackOnly();
	            //throw new EdmException("B��d podczas czytania pliku z pismami wychodz�cymi");
	        }
		}
    }
	private static Date parseDate(String d)
    {
    	Date result = null;
    	String data = d.substring(6);
    	data += "-" + d.substring(4, 6);
    	data += "-" + d.substring(0, 4);
    	result=DateUtils.nullSafeParseJsDate(data);
    	return result;

    }

    private static String getCellString(HSSFCell c)
    {
    	//LabelSSTRecord
    	String res;
    	if(c==null) return "";
    	switch (c.getCellType()) {
		case HSSFCell.CELL_TYPE_NUMERIC:
			res =  new Double(c.getNumericCellValue()).toString();
			break;
		case HSSFCell.CELL_TYPE_BLANK:
			res = "";
			break;
		case HSSFCell.CELL_TYPE_BOOLEAN:
			res = c.getBooleanCellValue()+"";
			break;
		case HSSFCell.CELL_TYPE_ERROR:
			res = c.getErrorCellValue() + "";
			break;
		case HSSFCell.CELL_TYPE_STRING:
			res = c.getStringCellValue()+"";
			break;
		//case HSSFCell.CELL_TYPE_FORMULA:
		//	res = c.getCellFormula()+ "";
		default:
			res = c.getStringCellValue()+"";
		}
    	if(res.length()>49) res = res.substring(0,48);
    	return res;
    }
}
