package pl.compan.docusafe.core.imports;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.reports.Report;
import pl.compan.docusafe.service.reports.ReportGenerator;

/**
 * Przechowuje informacje o pliku, zawierajacym dane o dokumentach do zaimportowania. Na podstawie statusu mo�na
 * sie dowiedziec czy proces importowania nie zostal rozpoczety, jest w trakcie przetwarzania czy juz zostal juz
 * zakonczony.
 *
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
 * @version $Id$
 */

public class ImportedFileBlob
{
	private static final Log log = LogFactory.getLog(ReportGenerator.class);

	private Long id;
	private ImportedFileInfo info;
    
    public static final String XML_ENCODING = "ISO-8859-2";
    
	public ImportedFileBlob()
	{

	}

	public synchronized void create() throws EdmException
	{
		try {
			DSApi.context().session().save(this);
		}
		catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static ImportedFileBlob find(Long id) throws EdmException
	{
		return Finder.find(ImportedFileBlob.class, id);
	}

	public static ImportedFileBlob findByFileInfo(ImportedFileInfo info) throws EdmException
	{
		java.sql.PreparedStatement ps = null;
		ResultSet rs = null;
		try 
		{
			String q = "select id, imported_file_id from ds_imported_file_blob where imported_file_id = ?";
			ps = DSApi.context().prepareStatement(q);
			ps.setLong(1, info.getId());
			rs = ps.executeQuery();
			if (rs.next()) 
			{
				Long id = rs.getLong(1);
				return (ImportedFileBlob) DSApi.context().session().load(ImportedFileBlob.class, id);
			}
			else 
			{
				throw new EdmException("Nie znaleziono bloba dla okre�lonego pliku importu");
			}
		}
		catch (SQLException e) {
			throw new EdmSQLException(e);
		}
		finally
		{
			if (rs!=null) { try { rs.close(); } catch (Exception e) { } }
			DSApi.context().closeStatement(ps);			
		}
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public ImportedFileInfo getInfo()
	{
		return info;
	}

	public void setInfo(ImportedFileInfo info)
	{
		this.info = info;
	}

	/**
	 * Zwraca tymczasowy plik XML z danymi z bloba
	 * @return
	 * @throws EdmException
	 */
	public File getXmlFile() throws EdmException
	{
		PreparedStatement pst = null;
		InputStream is = null;
		try {
			pst = DSApi.context().prepareStatement("select xml from " + DSApi.getTableName(ImportedFileBlob.class) + " where id = ?");
			pst.setLong(1, id);
			ResultSet rs = pst.executeQuery();

			if (!rs.next())
				throw new EdmException("Nie znaleziono bloba dla pliku importu nr " + id);

			Blob blob = rs.getBlob(1);
			if (blob != null) {
                is = blob.getBinaryStream();
                File file = File.createTempFile("docusafe_importdata_", ".xml");
                OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
                byte[] buffer = new byte[1024];
                int count;
                while ((count = is.read(buffer)) > 0)
                {
                    os.write(buffer, 0, count);
                }
                is.close();
                os.close();

                if (log.isDebugEnabled())
                    log.debug("file="+file);

                return file;
            }
            else
                return null;
		}
		catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
		catch (SQLException e) {
			throw new EdmSQLException(e);
		}
		catch (Exception e) {			
			throw new EdmException(e);
		}
		finally {
			DSApi.context().closeStatement(pst);
		}
	}

    /**
     * Zwraca tymczasowy plik XML z cz�ci� danych z bloba
     * @return
     * @throws EdmException
     */
    public File getXmlFilePart(int offset, int size) throws EdmException
    {
        PreparedStatement pst = null;
        InputStream is = null;
        try {
            pst = DSApi.context().prepareStatement("select xml from " + DSApi.getTableName(ImportedFileBlob.class) + " where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();

            if (!rs.next())
                throw new EdmException("Nie znaleziono bloba dla pliku importu nr " + id);

            Blob blob = rs.getBlob(1);
            if (blob != null) {
                is = blob.getBinaryStream();
                File file = File.createTempFile("docusafe_importdata_", ".xml");
                OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
                String content = "<?xml version='1.0' encoding='" + XML_ENCODING + "'?>";
                os.write(content.getBytes(XML_ENCODING));
                byte[] buffer = new byte[1024];
                int count;
                int toRead = size;
                int done = 0;
                is.skip(offset);
                while ((count = is.read(buffer, done, toRead)) > 0)
                {
                    os.write(buffer, 0, count);
                    toRead -= count;
                    done += count;
                }
                is.close();
                os.close();

                if (log.isDebugEnabled())
                    log.debug("file="+file);

                return file;
            }
            else
                return null;
        }
        catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e) {
            throw new EdmSQLException(e);
        }
        catch (Exception e) {           
            throw new EdmException(e);
        }
        finally {
        	DSApi.context().closeStatement(pst);
        }
    }

	/**
	 * Zapisuje w blobie istniej�cego obiektu zawarto�� danego pliku XML
	 * @param xmlFile - plik XML
	 */
	public void setXmlData(File xmlFile)
	{
		InputStream is = null;
		PreparedStatement ps = null;
		try {
			is = new FileInputStream(xmlFile);

			ps = DSApi.context().prepareStatement("update " + DSApi.getTableName(ImportedFileBlob.class) + " set xml = ? where id = ?");

			//if (is != null)
			
			ps.setBinaryStream(1, is, (int) xmlFile.length());
			ps.setLong(2, id);
			ps.execute();
            
            is.close();
			
		}
		catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		finally {
			DSApi.context().closeStatement(ps);
			if (is != null)
				try {
					is.close();
				}
				catch (Exception e) {
				}
		}
	}    
}
