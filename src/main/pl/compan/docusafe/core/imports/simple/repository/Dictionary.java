package pl.compan.docusafe.core.imports.simple.repository;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

@SuppressWarnings("serial")
public class Dictionary implements java.io.Serializable {
	private Integer id;
	private Boolean deleted;
	private String code;
	private String tablename;
	private Integer dictSource;
	private String cn;
	private Dictionary refDict;
	private Boolean controlDict;
    private String dockindCn;
	
	public static enum SourceType {
		MANUAL(1),
		SERVICE(2),
		REPOSITORY(3),
        SERVICE_ADHOC(4);
		
		public final int value;
		
		SourceType(int value) {
			this.value = value;
		}
	}
	
	private static Dictionary instance;
	
    public static synchronized Dictionary getInstance() {
        if (instance == null)
            instance = new Dictionary();
        return instance;
    }
	
	public Dictionary() {
		this.deleted = false;
		this.controlDict = false;
	}


	public Dictionary(Boolean deleted, String code, String tablename, Integer dictSource, String cn, Dictionary refDict,
			Boolean isControlDict) {
		this.deleted = deleted;
		this.code = code;
		this.tablename = tablename;
		this.dictSource = dictSource;
		this.cn = cn;
		this.refDict = refDict;
		this.controlDict = isControlDict;
	}

	public Integer getId() {
		return this.id;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public void setCode(Long code) {
		this.code = code.toString();
	}

	public String getTablename() {
		return this.tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	
	public Integer getDictSource() {
		return this.dictSource;
	}

	public void setDictSource(Integer dictSource) {
		this.dictSource = dictSource;
	}
	
	public void setDictSource(SourceType type) {
		this.dictSource = type.value;
	}

	public String getCn() {
		return this.cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public Dictionary getRefDict() {
		return this.refDict;
	}

	public void setRefDict(Dictionary refDict) {
		this.refDict = refDict;
	}

	public Boolean isControlDict() {
		return this.controlDict;
	}

	public void setControlDict(Boolean isControlDict) {
		this.controlDict = isControlDict;
	}

    public String getDockindCn() {
        return dockindCn;
    }

    public void setDockindCn(String dockindCn) {
        this.dockindCn = dockindCn;
    }

    public Dictionary findById(Long id) throws EdmException
    {
        return Finder.find(Dictionary.class, id);
    }

    public List<Dictionary> findAll(boolean deleted) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(Dictionary.class);

    	crit.add(Restrictions.eq("deleted", deleted));

    	return (List<Dictionary>) crit.list();
    }

    public List<Dictionary> findAll(boolean deleted, String dockindCn) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(Dictionary.class);

    	crit.add(Restrictions.eq("deleted", deleted));
    	crit.add(Restrictions.eq("dockindCn", dockindCn));

    	return (List<Dictionary>) crit.list();
    }

    public List<Dictionary> findRepositoryDictToDelete(String dockindCn, Collection<Integer> available) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(Dictionary.class);
    	
    	crit.add(Restrictions.eq("dictSource", SourceType.REPOSITORY.value));
    	crit.add(Restrictions.eq("dockindCn", dockindCn));
    	crit.add(Restrictions.not(Restrictions.in("id", available)));
    	
    	return (List<Dictionary>) crit.list();
    }

    public Dictionary find(String code) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(Dictionary.class);

    	crit.add(Restrictions.eq("code", code));

    	return (Dictionary) crit.uniqueResult();
    }

    public Dictionary find(String code, String dockindCn) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(Dictionary.class);

    	crit.add(Restrictions.eq("code", code));
    	crit.add(Restrictions.eq("dockindCn", dockindCn));

    	return (Dictionary) crit.uniqueResult();
    }

	public Dictionary findControlDict() throws EdmException{
		Criteria crit = DSApi.context().session().createCriteria(Dictionary.class);
		
		crit.add(Restrictions.eq("controlDict", true));
		
		return (Dictionary) crit.uniqueResult();
	}
    
	public void save() throws EdmException {
		DSApi.context().session().save(this);
		DSApi.context().session().flush();
	}

	public void delete() throws EdmException {
		DSApi.context().session().delete(this);
		DSApi.context().session().flush();
	}

}
