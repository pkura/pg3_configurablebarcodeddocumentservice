package pl.compan.docusafe.core.imports.simple;

import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Objects;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class DictionaryRepositorySqlBuilder {
	
	private RepositoryDictionarySchema dicSchema;
	
	public RepositoryDictionarySchema getDicSchema() {
		return dicSchema;
	}
	public void setDicSchema(RepositoryDictionarySchema dicSchema) {
		this.dicSchema = dicSchema;
	}
	
	public String buildCheckerSql() {
		return "SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('"+getDicSchema().getTableName()+"') AND type in (N'U')";
	}
	
	public String buildCreateTableSql() {
		StringBuilder code = new StringBuilder("CREATE TABLE ");
		code.append(getDicSchema().getTableName());
		code.append(" (\n");
		code.append("id int identity(1,1) not null,\n");
		code.append("available int not null");
		for (RepositoryField field : getDicSchema().getFields()) {
			code.append(",\n");
			String column = Objects.firstNonNull(field.getColumnName(),field.getFieldName());
			code.append(column);
			code.append(" ");
			code.append(field.getColumnType());
			code.append(" null");
		}
		code.append(");");
		
		return code.toString();
	}
	
	public String buildDropTableSql() {
		String code = new String("DROP TABLE ");
		code+=getDicSchema().getTableName();
		code+=";";
		return code;
	}
	
	public String buildCleanerSql() {
		String code = new String("DELETE FROM ");
		code+=getDicSchema().getTableName();
		code+=";";
		return code;
	}
	
	public String buildSingleInsertSql(){
		StringBuilder code = new StringBuilder("INSERT INTO ");
		code.append(getDicSchema().getTableName());
		code.append("(");
		StringBuilder columnQuery = new StringBuilder();
		Set<Long> ids = Sets.newHashSet();
		for (RepositoryField field : getDicSchema().getFields()) {
			ids.addAll(field.getIds());
			if (columnQuery.length()>0) {
				columnQuery.append(",");
			}
			columnQuery.append(Objects.firstNonNull(field.getColumnName(),field.getFieldName()));
		}
		columnQuery.append(",available");
		code.append(columnQuery);
		code.append(")");
		
		code.append(" values ");
		StringBuilder values = new StringBuilder();
		for (Long id : ids) {
			StringBuilder value = new StringBuilder();
			for (RepositoryField field : getDicSchema().getFields()) {
				if (value.length()>0) {
					value.append(",");
				}
				String v = field.getValue(id);
				if (v == null) {
					value.append("null");
				} else {
					value.append("'");
					value.append(v);
					value.append("'");
				}
			}
			value.append(",available=1");
			if (value.length()>0) {
				values.append("("+value+"),");
			}
		}
		
		if (values.length() > 0) {
			return code.append(StringUtils.removeEnd(values.toString(), ",")+";").toString();
		} else {
			return "";
		}
	}

	public Map<Long,String> buildUpdateSql(){
		Set<Long> ids = Sets.newHashSet();
		for (RepositoryField field : getDicSchema().getFields()) {
			ids.addAll(field.getIds());
		}

		Map<Long,String> sql = Maps.newHashMapWithExpectedSize(ids.size());	
		
		StringBuilder code = new StringBuilder("UPDATE ");
		code.append(getDicSchema().getTableName());
		code.append(" SET ");
		
		for (Long id : ids) {
			StringBuilder value = new StringBuilder();
			StringBuilder identity = new StringBuilder();
			for (RepositoryField field : getDicSchema().getFields()) {
				String column = Objects.firstNonNull(field.getColumnName(), field.getFieldName());
					if (value.length() > 0) {
						value.append(",");
					}
					value.append(column);
					value.append("=");
					String v = field.getValue(id);
					if (v == null) {
						value.append("null");
					} else {
						value.append("'");
						value.append(v);
						value.append("'");
					}
				
				if (field.isIdentity()) {
					identity.append(" WHERE "+column+"='"+field.getValue(id)+"'");
				}
			}
			value.append(",available=1");
			if (identity.length()>0) {
				value.append(identity).append(";");
				sql.put(id, code.toString()+value.toString());
			}
		}
		
		return sql;
	}
	
	public Map<Long,String> buildInsertSql(){
		Set<Long> ids = Sets.newHashSet();
		for (RepositoryField field : getDicSchema().getFields()) {
			ids.addAll(field.getIds());
		}

		Map<Long,String> sql = Maps.newHashMapWithExpectedSize(ids.size());	
		
		StringBuilder code = new StringBuilder("INSERT INTO ");
		code.append(getDicSchema().getTableName());
		code.append("(");
		StringBuilder columnQuery = new StringBuilder();
		for (RepositoryField field : getDicSchema().getFields()) {
			if (columnQuery.length()>0) {
				columnQuery.append(",");
			}
			columnQuery.append(Objects.firstNonNull(field.getColumnName(),field.getFieldName()));
		}
		columnQuery.append(",available");
		code.append(columnQuery);
		code.append(")");
		code.append(" VALUES ");

		for (Long id : ids) {
			StringBuilder value = new StringBuilder("(");
			for (RepositoryField field : getDicSchema().getFields()) {
				if (value.length()>1) {
					value.append(",");
				}
				String v = field.getValue(id);
				if (v == null) {
					value.append("null");
				} else {
					value.append("'");
					value.append(v);
					value.append("'");
				}
			}
			
			//available
			value.append(",1");

			value.append(");");
			
			sql.put(id, code.toString()+value.toString());
		}
		
		return sql;
	}
}
