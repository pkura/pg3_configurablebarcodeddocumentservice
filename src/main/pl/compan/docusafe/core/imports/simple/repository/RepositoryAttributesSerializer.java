package pl.compan.docusafe.core.imports.simple.repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dom4j.Document;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindDBUtils;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class RepositoryAttributesSerializer {

    private List<ReposiotryAttributeDict> dictionaries;

    public RepositoryAttributesSerializer(List<ReposiotryAttributeDict> dictionaries) {
        this.dictionaries = dictionaries;
    }

    public void synchronizeTables() throws EdmException {
        List<Document> hbmDocuments = Lists.newArrayList();
        Set<Long> processed = Sets.newHashSet();
        for (ReposiotryAttributeDict dict : dictionaries) {
            if (processed.add(dict.getCode())) {
                EnumHibernateBuilder enumBuilder = new EnumHibernateBuilder(dict.getTableName());
                hbmDocuments.add(enumBuilder.getHbmDocument());
            }
        }

        DockindDBUtils.runUpdates(hbmDocuments);
    }

    public void synchronizeValues() throws SQLException, EdmException {
        for (ReposiotryAttributeDict dict : dictionaries) {
            int updated = 0;
            int inserted = 0;
            Map<Long, String> insertSqls = Maps.newHashMapWithExpectedSize(dict.itemsSize());
            Map<Long, String> updateSqls = Maps.newHashMapWithExpectedSize(dict.itemsSize());

            for (EnumItem item : dict.getItems()) {
                insertSqls.put(item.getErpId(), item.getInsertSql(dict.getTableName()));
                updateSqls.put(item.getErpId(), item.getUpdateSql(dict.getTableName()));
            }

            PreparedStatement ps = null;

            try {
                Set<String> ids = Sets.newHashSet();
                for (Long id : updateSqls.keySet()) {
                    ids.add(id.toString());
                    if (!Strings.isNullOrEmpty(updateSqls.get(id))) {
                        ps = DSApi.context().prepareStatement(updateSqls.get(id));
                        int updateSingleRow = ps.executeUpdate();
                        updated += updateSingleRow;
                        DSApi.context().closeStatement(ps);
                        if (updateSingleRow == 0) {
                            ps = DSApi.context().prepareStatement(insertSqls.get(id));
                            ps.execute();
                            inserted++;
                            DSApi.context().closeStatement(ps);
                        }
                    }
                }
                dict.setInserted(inserted);
                dict.setUpdated(updated);

                ServicesUtils.disableDeprecatedItem(true, ids, dict.getTableName(), EnumHibernateBuilder.Field.CENTRUM.column, EnumHibernateBuilder.Field.REF_VALUE.column, dict.getCode());
            } catch (SQLException e) {
                DSApi.context().closeStatement(ps);
                throw e;
            }
        }
    }
}
