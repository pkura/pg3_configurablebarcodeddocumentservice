package pl.compan.docusafe.core.imports.simple;

import java.io.File;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import pl.compan.docusafe.boot.Docusafe;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class DictionarySchemaBuilder {
	private String xmlName = "dicitionariesRepository.xml";
	private Document xml;
	
	public String getXmlName() {
		return xmlName;
	}
	
	public void setXmlName(String xmlName) {
		this.xmlName = xmlName;
	}

	public Document getXml() {
		return xml;
	}

	public void setXml(Document xml) {
		this.xml = xml;
	}
	
	public DictionarySchemaBuilder() throws DocumentException {
		buildXmlElement();
	}

	public DictionarySchemaBuilder(String xmlName) throws DocumentException {
		this.setXmlName(xmlName);
		buildXmlElement();
	}

	public void buildXmlElement() throws DocumentException {
		File xml = new File(Docusafe.getHome() + "/"+ getXmlName());
		this.setXml(new SAXReader().read(xml));
	}
	
	public List<RepositoryDictionarySchema> parseXml() {
		Document doc = getXml();
		List<RepositoryDictionarySchema> schema = Lists.newArrayList();
		Element root = Preconditions.checkNotNull(doc.getRootElement().element("dictionaries"), "Brak roota - dicitionaries");

		List<Element> dictionariesRaw = Preconditions.checkNotNull(root.elements("dictionary"), "Brak dictionary");
		if (dictionariesRaw != null) {
			RepositoryDictionarySchema dicSchema = null;
			for (Element dicRaw : dictionariesRaw) {
				Boolean identityFound = false;
				dicSchema = new RepositoryDictionarySchema();

				String dicId = Preconditions.checkNotNull(Objects.firstNonNull(dicRaw.attributeValue("id"), dicRaw.attributeValue("name")), "B��d podczas parsowania id s�ownika");
				dicSchema.setDicId(dicId);
				
				dicSchema.setUseRepositoryClassInstances("true".equalsIgnoreCase(dicRaw.attributeValue("useRepositoryClassInstances")));
				
				String table = Preconditions.checkNotNull(dicRaw.attributeValue("table"), "B��d podczas parsowania nazwy tabeli");
				dicSchema.setTableName(table);
				
				boolean recreateTable = dicRaw.attributeValue("recreateTable") != null ? Boolean.valueOf(dicRaw.attributeValue("recreateTable")) : false;
				dicSchema.setRecreateTable(recreateTable);

				if (dicRaw.element("view") != null) {
					dicSchema.setViewName(dicRaw.element("view").attributeValue("name"));
				}
				if (dicRaw.element("materializedView") != null) {
					dicSchema.setMaterializedViewName(dicRaw.element("materializedView").attributeValue("name"));
				}
				
				Element fieldsRaw = Preconditions.checkNotNull(dicRaw.element("fields"), "B��d podczas parsowania p�l dla s�ownika: " + dicId);
				List<Element> fieldsRawList = Preconditions.checkNotNull(fieldsRaw.elements("field"), "B��d podczas parsowania p�l dla s�ownika: " + dicId);

				RepositoryField field = null;
				for (Element fieldRaw : fieldsRawList) {
					field = new RepositoryField();
					
					String fieldName = Preconditions.checkNotNull(fieldRaw.attributeValue("name"), "B�ad podczas parsowania pola");
					field.setFieldName(fieldName);
					
					field.setColumnName(fieldRaw.attributeValue("column"));
					
					
					
					String columnType = Preconditions.checkNotNull(fieldRaw.attributeValue("type"), "B�ad podczas parsowania typu columny");
					
					field.setColumnType(columnType);
					Boolean identity = fieldRaw.attributeValue("identity") != null ? Boolean.valueOf(fieldRaw.attributeValue("identity")) : false;
					if (identityFound && identity) {
						identity = null;
					} else {
						identityFound = identity;
					}
					field.setIdentity(Preconditions.checkNotNull(identity, "B�ad podczas parsowania typu pola, wykryto dwa pola typu field: "+field.getFieldName()));
					
					Preconditions.checkArgument(!"id".equalsIgnoreCase(Objects.firstNonNull(field.getColumnName(), field.getFieldName())), "Nie mo�na doda� kolumny id");
					Preconditions.checkArgument(!"available".equalsIgnoreCase(Objects.firstNonNull(field.getColumnName(), field.getFieldName())), "Nie mo�na doda� kolumny available");
					
					dicSchema.addField(field);
				}
				schema.add(dicSchema);
			}
		}

		return schema;
	}
	
	
}
