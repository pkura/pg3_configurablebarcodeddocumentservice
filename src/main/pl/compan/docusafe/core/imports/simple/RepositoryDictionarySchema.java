package pl.compan.docusafe.core.imports.simple;

import java.util.List;

import com.google.common.collect.Lists;

public class RepositoryDictionarySchema {

	private String dicId;
	private String tableName;
	private String viewName;
	private String materializedViewName;
	private boolean useRepositoryClassInstances;
	private boolean recreateTable;
	private List<RepositoryField> fields = Lists.newArrayList();

	public String getDicId() {
		return dicId;
	}
	public void setDicId(String dicId) {
		this.dicId = dicId;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getViewName() {
		return viewName;
	}
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}
	public String getMaterializedViewName() {
		return materializedViewName;
	}
	public void setMaterializedViewName(String materializedViewName) {
		this.materializedViewName = materializedViewName;
	}
	public boolean isRecreateTable() {
		return recreateTable;
	}
	public void setRecreateTable(boolean recreateTable) {
		this.recreateTable = recreateTable;
	}
	public List<RepositoryField> getFields() {
		return fields;
	}
	public void setFields(List<RepositoryField> fields) {
		this.fields = fields;
	}
	public void addField(RepositoryField field) {
		this.fields.add(field);
	}
	public RepositoryField getIdentityColumn() {
		for (RepositoryField field : fields) {
			if (field.isIdentity()) {
				return field;
			}
		}
		return null;
	}
	public RepositoryField getField(String fieldName) {
		for (RepositoryField field : fields) {
			if (field.getFieldName().equals(fieldName)) {
				return field;
			}
		}
		return null;
	}
	public boolean isUseRepositoryClassInstances() {
		return useRepositoryClassInstances;
	}
	public void setUseRepositoryClassInstances(boolean useRepositoryClassInstances) {
		this.useRepositoryClassInstances = useRepositoryClassInstances;
	}

	public String getInfo() {
		return getDicId()+"/"+getTableName();
	}
	
}