package pl.compan.docusafe.core.imports.simple;

import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class RepositoryField {
	private String fieldName;
	private String columnName;
	private String columnType;
	private Boolean identity;
	private Map<Long,String> values = Maps.newHashMap();
	
	private final static Set<String> NUMERIC_TYPES = Sets.newHashSet();
	static {
		NUMERIC_TYPES.add("int");
	}
	
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getColumnType() {
		return columnType;
	}
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}
	public Map<Long,String> getValues() {
		return values;
	}
	public void setValues(Map<Long,String> values) {
		this.values = values;
	}
	public String getValue(Long id) {
		return getValues().get(id);
	}
	public boolean setValue(Long id, String value) {
		return getValues().put(id, value) != null ? true : false;
	}
	public Set<Long> getIds() {
		return getValues().keySet();
	}
	public Boolean isIdentity() {
		return identity;
	}
	public void setIdentity(Boolean identity) {
		this.identity = identity;
	}
	public boolean isNumeric() {
		if (NUMERIC_TYPES.contains(getColumnType())) {
			return true;
		} else {
			return false;
		}
	}
	public Set<String> getIdentityValues() {
		Set<String> values = Sets.newHashSet();
		
		if (isIdentity()) {
			if (isNumeric()) {
				for (String id : getValues().values()) {
					while (id != StringUtils.removeStart(id, "0") && id.length()>1) {
						id = StringUtils.removeStart(id, "0");
					}
					values.add(id);
				}
			} else {
				values = Sets.newHashSet(getValues().values());
			}
		}
		
		return values;
	}
}
