package pl.compan.docusafe.core.imports.simple;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeEqExp;
import pl.compan.docusafe.core.db.criteria.NativeExps;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class ServicesUtils {

    public static int disableDeprecatedItem(boolean isIdNumeric, Collection<String> ids, String tableName, String identityColumnName) throws EdmException {
        return (int) disableDeprecatedItem(isIdNumeric, ids, tableName, identityColumnName, new NativeEqExp[0]);
    }

    public static int disableDeprecatedItem(boolean isIdNumeric, Collection<String> ids, String tableName, String identityColumnName, String groupColumn, Object groupValue) throws EdmException {
        return (int) disableDeprecatedItem(isIdNumeric, ids, tableName, identityColumnName, (NativeEqExp) NativeExps.eq("d."+groupColumn, groupValue));
    }

    public static long disableDeprecatedItem(boolean isIdNumeric, Collection<String> ids, String tableName, String identityColumnName, NativeEqExp... whereStatements) throws EdmException {
        Set<String> allIds = Sets.newHashSet();


        NativeCriteria nc = DSApi.context().createNativeCriteria(tableName, "d");
        nc.setProjection(NativeExps.projection().addProjection("d." + identityColumnName));

        for (NativeEqExp stat : whereStatements) {
            nc.add(stat);
        }

        CriteriaResult cr = nc.criteriaResult();
        while (cr.next()) {
            allIds.add(cr.getString(0, null));
        }

        return disableDeprecatedItem(isIdNumeric, ids, allIds, tableName, identityColumnName);
    }

    public static long disableDeprecatedItem(boolean isIdNumeric, Collection<String> processedIds, Collection<String> allIds, String tableName, String identityColumnName) throws EdmException {
        long updated = 0;
        if (!allIds.isEmpty()) {
            Collection<String> toDisable = CollectionUtils.subtract(allIds, processedIds);
            if (!toDisable.isEmpty()) {
                // dzielenie wynik�w na mniejsze, bo w sql "where ... in (...)"
                // nie
                // mo�e by� wi�cej ni� 2000 warto�ci
                List<List<String>> subLists = Lists.newArrayList();
                List<String> temp = Lists.newArrayList(toDisable);
                int i = 0;
                while (i + 1000 < toDisable.size() - 1) {
                    subLists.add(temp.subList(i, i + 1000));
                    i += 1000;
                }
                subLists.add(temp.subList(i, temp.size()));

                for (List<String> list : subLists) {
                    PreparedStatement ps = null;
                    try {
                        String idList = "";
                        if (isIdNumeric) {
                            idList = list.toString().substring(1, list.toString().length() - 1);
                        } else {
                            StringBuilder sb = new StringBuilder();
                            for (Object id : list) {
                                if (sb.length()>0) {
                                    sb.append(",");
                                }
                                sb.append("'");
                                sb.append(id.toString());
                                sb.append("'");
                            }
                            idList = sb.toString();
                        }
                        ps = DSApi.context().prepareStatement("update " + tableName + " set available=0 where "+identityColumnName +" in (" + idList + ");");
                        updated = ps.executeUpdate();
                        ps.close();

                    } catch (Exception e) {
                        throw new EdmException(e);
                    }
                    DbUtils.closeQuietly(ps);
                }
            }
        }

        return updated;
    }

	public static int disableDeprecatedItem(Collection<Long> found, String tableName) throws EdmException {
        return disableDeprecatedItem(found, tableName, false);
    }

	public static int disableDeprecatedItem(Collection<Long> found, String tableName, boolean delete) throws EdmException {
		Set<Long> allIds = Sets.newHashSet();
		int updated = 0;

		NativeCriteria nc = DSApi.context().createNativeCriteria(tableName, "d");
		nc.setProjection(NativeExps.projection().addProjection("d.id"));

		CriteriaResult cr = nc.criteriaResult();
		while (cr.next()) {
			allIds.add(cr.getLong(0, null));
		}
		
		if (!allIds.isEmpty()) {
			Collection<Long> toDisable = CollectionUtils.subtract(allIds, found);
			if (!toDisable.isEmpty()) {
				// dzielenie wynik�w na mniejsze, bo w sql "where ... in (...)"
				// nie
				// mo�e by� wi�cej ni� 2000 warto�ci
				List<List<Long>> subLists = Lists.newArrayList();
				List<Long> temp = Lists.newArrayList(toDisable);
				int i = 0;
				while (i + 1000 < toDisable.size() - 1) {
					subLists.add(temp.subList(i, i + 1000));
					i += 1000;
				}
				subLists.add(temp.subList(i, temp.size()));

				for (List<Long> list : subLists) {
					PreparedStatement ps = null;
					try {
						String idList = list.toString().substring(1, list.toString().length() - 1);
                        if (delete) {
                            ps = DSApi.context().prepareStatement("delete from " + tableName + " where id in (" + idList + ");");
                        } else {
						    ps = DSApi.context().prepareStatement("update " + tableName + " set available=0 where id in (" + idList + ");");
                        }
						updated = ps.executeUpdate();
						ps.close();

					} catch (Exception e) {
						throw new EdmException(e);
					}
					DbUtils.closeQuietly(ps);
				}
			}
		}

		return updated;
	}

	public static void deleteRowsInsertFromEnumView(String tableName, String viewName) throws SQLException, EdmException {
		String sql = "delete from "+tableName+";";
		String sql2 = "insert into "+tableName+" (id,cn,title,refValue,centrum,available) select id,cn,title,refValue,centrum,available from "+viewName+";";
		String sql3 = "SET IDENTITY_INSERT "+tableName+" ON; insert into "+tableName+" (id,cn,title,refValue,centrum,available) select id,cn,title,refValue,centrum,available from "+viewName+"; SET IDENTITY_INSERT "+tableName+" OFF; ";
		
		PreparedStatement deleteStatement;
		deleteStatement = DSApi.context().prepareStatement(sql);
		deleteStatement.execute();

		try {
			PreparedStatement insertStatement;
			insertStatement = DSApi.context().prepareStatement(sql2);
			insertStatement.execute();
		} catch (SQLException e) {
			PreparedStatement insertStatementWithIdentity;
			insertStatementWithIdentity = DSApi.context().prepareStatement(sql3);
			insertStatementWithIdentity.execute();
		}
	}
}
