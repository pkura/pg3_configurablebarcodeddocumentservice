package pl.compan.docusafe.core.imports.simple.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

@SuppressWarnings("serial")
public class DictionaryMap implements java.io.Serializable {

	private Integer id;
	private String controlValue;
    private String dockindCn;
	private Dictionary dictionary;
	private boolean mustShow;
	private boolean showAllReferenced;

	private static DictionaryMap instance;
	
    public static synchronized DictionaryMap getInstance() {
        if (instance == null)
            instance = new DictionaryMap();
        return instance;
    }
	
	public DictionaryMap() {
	}

	public DictionaryMap(String controlValue, Dictionary dictionary, boolean mustShow) {
		this.controlValue = controlValue;
		this.dictionary = dictionary;
		this.mustShow = mustShow;
	}

	public String getControlValue() {
		return this.controlValue;
	}

	public void setControlValue(String controlValue) {
		this.controlValue = controlValue;
	}

    public String getDockindCn() {
        return dockindCn;
    }

    public void setDockindCn(String dockindCn) {
        this.dockindCn = dockindCn;
    }

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

	public boolean isMustShow() {
		return this.mustShow;
	}

	public void setMustShow(boolean mustShow) {
		this.mustShow = mustShow;
	}
	
    public boolean isShowAllReferenced() {
		return showAllReferenced;
	}

	public void setShowAllReferenced(boolean showAllReferenced) {
		this.showAllReferenced = showAllReferenced;
	}

	public DictionaryMap findById(Long id) throws EdmException
    {
        return Finder.find(DictionaryMap.class, id);
    }
    
    public List<DictionaryMap> find(String controlValue) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(DictionaryMap.class);
    	
    	crit.add(Restrictions.eq("controlValue", controlValue));
    	
    	return (List<DictionaryMap>) crit.list();
    }

    public List<DictionaryMap> find(String controlValue, String dockindCn) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(DictionaryMap.class);

    	crit.add(Restrictions.eq("controlValue", controlValue));
    	crit.add(Restrictions.eq("dockindCn", dockindCn));

    	return (List<DictionaryMap>) crit.list();
    }
    
	public void save() throws EdmException {
		DSApi.context().session().save(this);
		DSApi.context().session().flush();
	}

	public void delete() throws EdmException {
		DSApi.context().session().delete(this);
		DSApi.context().session().flush();
	}	
}
