package pl.compan.docusafe.core.imports.simple.repository;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.XMLWriter;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class DockindUpdater {
	
	private static Logger log = LoggerFactory.getLogger(DockindUpdater.class);

	private List<ReposiotryAttributeDict> dictionaries;
	
	public DockindUpdater(List<ReposiotryAttributeDict> dictionariesWithValues, Long dictToOmit) {
		dictionaries = Lists.newArrayList();
		Set<Long> process = Sets.newHashSet();
		for (ReposiotryAttributeDict dict : dictionariesWithValues) {
			if ((dictToOmit == null || !dict.getCode().equals(dictToOmit)) && process.add(dict.getCode())) {
				dictionaries.add(new ReposiotryAttributeDict(dict));
			}
		}
	}

	public void updateDockindFields(String docKindCn, String dynamicDictCn, boolean reloadDockind) throws EdmException, IOException, SQLException {
		if(dynamicDictCn == null){
            return;
        }

        DocumentKind docKind = DocumentKind.findByCn(docKindCn);
		Document document = docKind.getXML();
		
		String dicXPath = "/*[name()='doctype']/*[name()='fields']/*[name()='field'][@cn='"+dynamicDictCn+"']";
		Element dicElem = (Element)document.selectSingleNode(dicXPath);
		
		String dicFieldXPath1 = dicXPath+"/*[name()='type']/*[name()='field'][@cn='";
		String dicFieldXPath2 = "']";
		
		String fieldsXPath = "/*[name()='doctype']/*[name()='fields']/*[name()='field'][@cn='"+dynamicDictCn+"']/*[name()='type']/*[name()='field']";
		List<Long> ids = getDictionaryFieldIds(dicElem, fieldsXPath);
		
		String dynamicDictTable = dicElem.element("type").attributeValue("tableName");
		
		
		Set<String> cnAdded = Sets.newHashSet();
		Set<String> cnRepo = Sets.newHashSet();
		for (ReposiotryAttributeDict dict : dictionaries) {
			StringBuilder fieldXpath = new StringBuilder(dicFieldXPath1);
			fieldXpath.append(dict.getCn()+"_1");
			fieldXpath.append(dicFieldXPath2);
			
			Node field = (Node)document.selectSingleNode(fieldXpath.toString());

			if (field == null) {
				Long id = findProperId(ids);
				
				addFieldToDictionary(dicElem, dict, id);
				
				cnAdded.add(dict.getCn()+"_1");
			}
			cnRepo.add(dict.getCn()+"_1");
		}
		updateVisibleCns(dicElem, cnRepo);
		
		updateDynamiDictionaryTable(dynamicDictTable, cnAdded);
		
		File file = File.createTempFile("dockind",".xml");
		Writer out = new OutputStreamWriter(new FileOutputStream(file), "UTF8");
		XMLWriter output = new XMLWriter(out);
		output.write(document);
		output.close();
		docKind.saveContent(file);

        if (reloadDockind) {
		    docKind.resetDockindInfo();
        }
	}

	private List<Long> getDictionaryFieldIds(Element dicElem, String fieldsXPath) {
		List<Long> ids = Lists.newArrayList();
		for (Element field : (Collection<Element>)dicElem.selectNodes(fieldsXPath)) {
			String id = field.attributeValue("id");
			if (StringUtils.isNumeric(id)) {
				ids.add(Long.valueOf(id));
			}
		}
		Collections.sort(ids);
		return ids;
	}
	
	//TODO
	private void updateDynamiDictionaryTable(String dynamicDictTable, Set<String> cnAdded) throws SQLException, EdmException {
		List<String> sqls = Lists.newArrayList();
		String header = "alter table "+dynamicDictTable;
		String prefix = " add ";
		String postfix = " int null;\n";
		for (String cn : cnAdded) {
			sqls.add(new StringBuilder(header).append(prefix).append(StringUtils.removeEnd(cn.toLowerCase(Locale.ENGLISH), "_1")).append(postfix).toString());
		}
		
		for (String sql : sqls) {
			PreparedStatement ps = null;

			try {
				ps = DSApi.context().prepareStatement(sql);
				ps.execute();
			} catch (SQLException e) {
				log.debug(e.getMessage(), e);
			} finally {
				if (ps != null) {
					DSApi.context().closeStatement(ps);
				}
			}
		}
	}

	private Long findProperId(List<Long> ids) {
		Long id = null;
		if (ids != null && !ids.isEmpty()) {
			long i = ids.get(0);
			do {
				if (!ids.contains(++i)) {
					id = i;
					ids.add(i);
					Collections.sort(ids);
				}
			} while (id == null);
		}
		if (id == null) {
			id = 1L;
		}
		return id;
	}

	private void addFieldToDictionary(Element dicElem, ReposiotryAttributeDict dict, Long id) {
		dicElem.element("type").addElement("field")
					.addAttribute("id",id.toString())
					.addAttribute("cn", dict.getCn()+"_1")
					.addAttribute("column", dict.getColumnName()+"_1")
					.addAttribute("name", dict.getTitle())
					.addAttribute("cssClass", "hide-empty-select")
					.addElement("type")
					.addAttribute("name", "dataBase")
					.addAttribute("auto", "true")
					.addAttribute("autocompleteRegExp", "%")
					.addAttribute("empty-on-start", "true")
					.addAttribute("repository-field", "true")
					.addAttribute("reload-at-each-dwr-get", "false")
					.addAttribute("ref-style", "cn-by-colon")
					.addAttribute("tableName", dict.getTableName());
	}

	private void updateVisibleCns(Element dicElem, Set<String> cnRepo) {
		String oldCnsRaw = dicElem.element("type").attributeValue("visible-fields");
		Iterable<String> oldCns = Splitter.on(',').omitEmptyStrings().trimResults().split(oldCnsRaw);
		Set<String> visibleCns = Sets.newHashSet();
		for (String cn : oldCns) {
			if (!cn.startsWith("DSD_REPO")) {
				visibleCns.add(cn);
			}
		}
		visibleCns.addAll(cnRepo);
		String cnsRaw = Joiner.on(",").skipNulls().join(visibleCns);
		dicElem.element("type").addAttribute("visible-fields", cnsRaw);
	}
	
}
