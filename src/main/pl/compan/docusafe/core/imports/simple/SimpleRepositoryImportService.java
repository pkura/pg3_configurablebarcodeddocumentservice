package pl.compan.docusafe.core.imports.simple;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.axis2.AxisFault;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.simple.DictionaryServicesRepositoryStub;
import pl.compan.docusafe.ws.simple.DictionaryServicesRepositoryStub.GetRepositoryClassInstances;
import pl.compan.docusafe.ws.simple.DictionaryServicesRepositoryStub.GetRepositoryClassInstancesResponse;
import pl.compan.docusafe.ws.simple.DictionaryServicesRepositoryStub.GetRepositoryInstances;
import pl.compan.docusafe.ws.simple.DictionaryServicesRepositoryStub.GetRepositoryInstancesResponse;
import pl.compan.docusafe.ws.simple.DictionaryServicesRepositoryStub.RepositoryClassInstance;
import pl.compan.docusafe.ws.simple.DictionaryServicesRepositoryStub.RepositoryInstance;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

public class SimpleRepositoryImportService extends ServiceDriver implements Service {
	private Timer timer;
	private Long period = 10l;
	private Boolean run = false;
	private boolean syncing = false;
	
	private Property[] properties;

	private static final String AVAILABS_SIMPLE_IMPORT_SERVICE_NAME = "service.disable.SimpleRepositoryImport";
	
	class PeriodSyncTimeProperty extends Property {
		public PeriodSyncTimeProperty() {
			super(SIMPLE, PERSISTENT, SimpleRepositoryImportService.this, "period", "Czestotliwo�c synchronizacji w sekundach", String.class);
		}

		protected Object getValueSpi() {
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (SimpleRepositoryImportService.this) {
				if (object != null) {
					period = Long.parseLong((String) object);
				} else {
					period = 1440L;
				}
			}
		}
	}
	
	class RunProperty extends Property {
		public RunProperty() {
			super(SIMPLE, PERSISTENT, SimpleRepositoryImportService.this, "run", "Synchronizacja [w�/wy�]", Boolean.class);
		}

		protected Object getValueSpi() {
			return run;
		}

		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (SimpleRepositoryImportService.this) {
				if (object != null) {
					run = (Boolean) object;
				} else {
					run = false;
				}
			}
		}
	}
	
	@Override
	public Property[] getProperties() {
		return properties;
	}
	
	public SimpleRepositoryImportService() {
		properties = new Property[] { new PeriodSyncTimeProperty() , new RunProperty() };
	}
	
	@Override
	protected void start() throws pl.compan.docusafe.service.ServiceException {
		if (AvailabilityManager.isAvailable(AVAILABS_SIMPLE_IMPORT_SERVICE_NAME)) {
			return;
		}
		
		
		timer = new Timer(true);
		timer.schedule(new Import(), 15*DateUtils.SECOND, period*DateUtils.SECOND);
		
	}

	@Override
	protected void stop() throws pl.compan.docusafe.service.ServiceException {
		if (timer != null) {
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop() {
		return !syncing;
	}
	
	class Import extends TimerTask {
		private static final String SERVICE_PATH = "/services/DictionaryServicesRepository";
		
		long periodCounter = 0;
		
		DictionaryServicesRepositoryStub stub;
		
		public DictionaryServicesRepositoryStub getStub() throws AxisFault, Exception {
			if (stub == null) {
				AxisClientConfigurator conf = new AxisClientConfigurator();
				stub = new DictionaryServicesRepositoryStub(conf.getAxisConfigurationContext());
				conf.setUpHttpParameters(stub, SERVICE_PATH);
			}
			return stub;
		}
		
		@Override
		public void run() {
			if (run) {
				synchronized(this) {
					try {
						syncing = true;
						DictionaryServicesRepositoryStub stub = getStub();
						stub._getServiceClient().cleanupTransport();
						
						DSApi.openAdmin();
	
						DictionarySchemaBuilder dictionariesBuilder = new DictionarySchemaBuilder();
						dictionariesBuilder.buildXmlElement();
						List<RepositoryDictionarySchema> dictionaries = dictionariesBuilder.parseXml();
						DictionaryRepositorySqlBuilder sync;
	
						for (RepositoryDictionarySchema dic : dictionaries) {
	
							console(Console.INFO, dic.getInfo() + ": rozpoczynam pobieranie s�ownika");
	
							int inserted = 0;
							int updated = 0;
							
							if (dic.isUseRepositoryClassInstances()) {
								if (!StringUtils.isNumeric(dic.getDicId())) {
									console(Console.ERROR, dic.getInfo() + ": nieprawid�owy atrybutId s�ownika - NaN");
									log.error(dic.getInfo() + ": nieprawid�owy atrybutId s�ownika - NaN");
									continue;
								}
	
								GetRepositoryClassInstances params = new GetRepositoryClassInstances();
								params.setRepAtrybutId(Long.valueOf(dic.getDicId()));
	
								GetRepositoryClassInstancesResponse response = stub.getRepositoryClassInstances(params);
								if (response != null && response.get_return() != null) {
									long i = 1;
									for (RepositoryClassInstance entry : response.get_return()) {
										RepositoryField field;
										if ((field = dic.getField("id")) != null) {
											field.setValue(i, entry.getId() + "");
										} else {
											console(Console.ERROR, dic.getInfo() + ": brak pola id");
											log.error(dic.getInfo() + ": brak pola id");
										}
										if ((field = dic.getField("value")) != null) {
											field.setValue(i, entry.getValue());
										} else {
											console(Console.ERROR, dic.getInfo() + ": brak pola value");
											log.error(dic.getInfo() + ": brak pola value");
										}
										i++;
									}
								}
							} else {
								for (RepositoryField field : dic.getFields()) {
									GetRepositoryInstances params = new GetRepositoryInstances();
									params.setRepKlasaNazwa(dic.getDicId());
									params.setRepAtrybutIdo(field.getFieldName());
									GetRepositoryInstancesResponse response = stub.getRepositoryInstances(params);
									if (response != null && response.get_return() != null) {
										for (RepositoryInstance entry : response.get_return()) {
											if (field.setValue(entry.getRepInstancjaId(), entry.getWartosc())) {
												console(Console.ERROR,
														dic.getInfo() + ": powtarzaj�ce si� RepInstancjaId: " + entry.getRepInstancjaId());
												log.error(dic.getInfo() + ": powtarzaj�ce si� RepInstancjaId: " + entry.getRepInstancjaId());
											}
										}
									}
								}
							}
	
							sync = new DictionaryRepositorySqlBuilder();
							sync.setDicSchema(dic);
	
							Set<String> ids = dic.getIdentityColumn().getIdentityValues();
	
							Map<Long, String> udpateSqlCodes = sync.buildUpdateSql();
							Map<Long, String> insertSqlCodes = sync.buildInsertSql();
							String checkerSqlCode = sync.buildCheckerSql();
							String dropTableSqlCode = sync.buildDropTableSql();
							String createTableSqlCode = sync.buildCreateTableSql();
	
	
							if (insertSqlCodes.size() > 0 && udpateSqlCodes.size() > 0) {
								DSApi.context().begin();
	
								PreparedStatement ps = null;
								try {
									ps = DSApi.context().prepareStatement(checkerSqlCode);
									ResultSet rs = ps.executeQuery();
									if (rs.next()) {
										if (dic.isRecreateTable()) {
											ps = DSApi.context().prepareStatement(dropTableSqlCode);
											ps.execute();
											DSApi.context().closeStatement(ps);
											ps = DSApi.context().prepareStatement(createTableSqlCode);
											ps.execute();
											DSApi.context().closeStatement(ps);
										}
									} else {
										ps = DSApi.context().prepareStatement(createTableSqlCode);
										ps.execute();
										DSApi.context().closeStatement(ps);
									}
	
									for (Long id : udpateSqlCodes.keySet()) {
										if (!Strings.isNullOrEmpty(udpateSqlCodes.get(id))) {
											ps = DSApi.context().prepareStatement(udpateSqlCodes.get(id));
											int updateSingleRow = ps.executeUpdate();
											updated += updateSingleRow;
											DSApi.context().closeStatement(ps);
											if (updateSingleRow == 0) {
												ps = DSApi.context().prepareStatement(insertSqlCodes.get(id));
												ps.execute();
												inserted++;
												DSApi.context().closeStatement(ps);
											}
										}
									}
	
									int disabled = ServicesUtils.disableDeprecatedItem(dic.getIdentityColumn().isNumeric(), ids, dic.getTableName(),
											Objects.firstNonNull(dic.getIdentityColumn().getColumnName(), dic.getIdentityColumn().getFieldName()));
	
									console(Console.INFO, dic.getInfo() + ": zablokowano (brak w serwisie): " + disabled);
									console(Console.INFO, dic.getInfo() + ": zaaktualizowano: " + updated);
									console(Console.INFO, dic.getInfo() + ": dodano: " + inserted);
	
									if (!Strings.isNullOrEmpty(dic.getViewName()) && !Strings.isNullOrEmpty(dic.getMaterializedViewName())) {
										ServicesUtils.deleteRowsInsertFromEnumView(dic.getMaterializedViewName(), dic.getViewName());
										try {
											DataBaseEnumField.reloadForTable(dic.getMaterializedViewName());
										} catch (Exception e) {
											console(Console.ERROR, dic.getInfo() + ": spowodowa� b��d, nie mo�na prze�adowa� pola Enum " + e.getMessage());
											log.error(e.getMessage(), e);
										}
										console(Console.INFO, dic.getInfo() + ": zmaterializowano widoki");
									}
									console(Console.INFO, dic.getInfo() + ": pomy�lnie pobrano s�ownik");
	
									DSApi.context().commit();
								} catch (Exception e) {
									DSApi.context().rollback();
									DSApi.context().closeStatement(ps);
									console(Console.ERROR, dic.getInfo() + ": spowodowa� b��d: " + e.getMessage());
									log.error(e.getMessage(), e);
								}
							} else {
								console(Console.INFO, dic.getInfo() + ": zako�czono pobieranie s�ownika, serwis nie zwraca danych");
							}
	
						}
	
					} catch (Exception e) {
						console(Console.ERROR, ": " + e.getMessage());
						log.error(e.getMessage(), e);
					} finally {
						syncing = false;
						try {
							DSApi.close();
						} catch (EdmException e) {
							console(Console.ERROR, ": BLAD " + e.getMessage());
							log.error(e.getMessage(), e);
						}
					}
				}
			}
		}
	}
}
