package pl.compan.docusafe.core.imports.simple.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ams.DictionaryServicesRepositoryStub.RepositoryAttributesContext;

public class SimpleRepositoryAttribute implements java.io.Serializable {

	private Integer id;
	private boolean available = false;
	
	private Integer contextId;
	private Integer objectId;
	private boolean used = false;
	private String tableName;
	
	private Integer czyAktywny;
	private Integer czyBlokada;
	private String nazwa;
	private Long obiektSystemuId;
	private Long repAtrybutId;
	private String repAtrybutIdo;
	private Long repDispAtrId;
	private Long repKlasaId;
	private Long repTypId;
	private String repTypProstyIdo;
	private Long repWartoscId;
	private Integer rodzaj;
	private Integer rodzajTypu;
	private Integer rodzajWartosci;
	private Integer typDlugosc;
	private String typFormat;
	private Integer typWartosci;
	private Date wartoscDate;
	private BigDecimal wartoscDecimal;
	private Long wartoscInstancjaId;
	private Integer wartoscLong;
	private String wartoscString;

	private static SimpleRepositoryAttribute instance;
	
    public static synchronized SimpleRepositoryAttribute getInstance() {
        if (instance == null)
            instance = new SimpleRepositoryAttribute();
        return instance;
    }
	
	public SimpleRepositoryAttribute() {
	}

	public SimpleRepositoryAttribute(Integer id, boolean available) {
		this.id = id;
		this.available = available;
	}

	public SimpleRepositoryAttribute(Integer id, boolean available, Integer czyAktywny, Integer czyBlokada, String nazwa,
			Long obiektSystemuId, Long repAtrybutId, String repAtrybutIdo, Long repDispAtrId, Long repKlasaId, Long repTypId,
			String repTypProstyIdo, Long repWartoscId, Integer rodzaj, Integer rodzajTypu, Integer rodzajWartosci, Integer typDlugosc,
			String typFormat, Integer typWartosci, Date wartoscDate, BigDecimal wartoscDecimal, Long wartoscInstancjaId,
			Integer wartoscLong, String wartoscString) {
		this.id = id;
		this.available = available;
		this.czyAktywny = czyAktywny;
		this.czyBlokada = czyBlokada;
		this.nazwa = nazwa;
		this.obiektSystemuId = obiektSystemuId;
		this.repAtrybutId = repAtrybutId;
		this.repAtrybutIdo = repAtrybutIdo;
		this.repDispAtrId = repDispAtrId;
		this.repKlasaId = repKlasaId;
		this.repTypId = repTypId;
		this.repTypProstyIdo = repTypProstyIdo;
		this.repWartoscId = repWartoscId;
		this.rodzaj = rodzaj;
		this.rodzajTypu = rodzajTypu;
		this.rodzajWartosci = rodzajWartosci;
		this.typDlugosc = typDlugosc;
		this.typFormat = typFormat;
		this.typWartosci = typWartosci;
		this.wartoscDate = wartoscDate;
		this.wartoscDecimal = wartoscDecimal;
		this.wartoscInstancjaId = wartoscInstancjaId;
		this.wartoscLong = wartoscLong;
		this.wartoscString = wartoscString;
	}

	public Integer getId() {
		return this.id;
	}
	
	public Long getLongId() {
		if (this.id != null) {
			return this.id.longValue();
		} else {
			return null;
		}
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isAvailable() {
		return this.available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Integer getCzyAktywny() {
		return this.czyAktywny;
	}

	public void setCzyAktywny(Integer czyAktywny) {
		this.czyAktywny = czyAktywny;
	}

	public Integer getCzyBlokada() {
		return this.czyBlokada;
	}

	public void setCzyBlokada(Integer czyBlokada) {
		this.czyBlokada = czyBlokada;
	}

	public String getNazwa() {
		return this.nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Long getObiektSystemuId() {
		return this.obiektSystemuId;
	}

	public void setObiektSystemuId(Long obiektSystemuId) {
		this.obiektSystemuId = obiektSystemuId;
	}

	public Long getRepAtrybutId() {
		return this.repAtrybutId;
	}

	public void setRepAtrybutId(Long repAtrybutId) {
		this.repAtrybutId = repAtrybutId;
	}

	public String getRepAtrybutIdo() {
		return this.repAtrybutIdo;
	}

	public void setRepAtrybutIdo(String repAtrybutIdo) {
		this.repAtrybutIdo = repAtrybutIdo;
	}

	public Long getRepDispAtrId() {
		return this.repDispAtrId;
	}

	public void setRepDispAtrId(Long repDispAtrId) {
		this.repDispAtrId = repDispAtrId;
	}

	public Long getRepKlasaId() {
		return this.repKlasaId;
	}

	public void setRepKlasaId(Long repKlasaId) {
		this.repKlasaId = repKlasaId;
	}

	public Long getRepTypId() {
		return this.repTypId;
	}

	public void setRepTypId(Long repTypId) {
		this.repTypId = repTypId;
	}

	public String getRepTypProstyIdo() {
		return this.repTypProstyIdo;
	}

	public void setRepTypProstyIdo(String repTypProstyIdo) {
		this.repTypProstyIdo = repTypProstyIdo;
	}

	public Long getRepWartoscId() {
		return this.repWartoscId;
	}

	public void setRepWartoscId(Long repWartoscId) {
		this.repWartoscId = repWartoscId;
	}

	public Integer getRodzaj() {
		return this.rodzaj;
	}

	public void setRodzaj(Integer rodzaj) {
		this.rodzaj = rodzaj;
	}

	public Integer getRodzajTypu() {
		return this.rodzajTypu;
	}

	public void setRodzajTypu(Integer rodzajTypu) {
		this.rodzajTypu = rodzajTypu;
	}

	public Integer getRodzajWartosci() {
		return this.rodzajWartosci;
	}

	public void setRodzajWartosci(Integer rodzajWartosci) {
		this.rodzajWartosci = rodzajWartosci;
	}

	public Integer getTypDlugosc() {
		return this.typDlugosc;
	}

	public void setTypDlugosc(Integer typDlugosc) {
		this.typDlugosc = typDlugosc;
	}

	public String getTypFormat() {
		return this.typFormat;
	}

	public void setTypFormat(String typFormat) {
		this.typFormat = typFormat;
	}

	public Integer getTypWartosci() {
		return this.typWartosci;
	}

	public void setTypWartosci(Integer typWartosci) {
		this.typWartosci = typWartosci;
	}

	public Date getWartoscDate() {
		return this.wartoscDate;
	}

	public void setWartoscDate(Date wartoscDate) {
		this.wartoscDate = wartoscDate;
	}

	public BigDecimal getWartoscDecimal() {
		return this.wartoscDecimal;
	}

	public void setWartoscDecimal(BigDecimal wartoscDecimal) {
		this.wartoscDecimal = wartoscDecimal;
	}

	public Long getWartoscInstancjaId() {
		return this.wartoscInstancjaId;
	}

	public void setWartoscInstancjaId(Long wartoscInstancjaId) {
		this.wartoscInstancjaId = wartoscInstancjaId;
	}

	public Integer getWartoscLong() {
		return this.wartoscLong;
	}

	public void setWartoscLong(Integer wartoscLong) {
		this.wartoscLong = wartoscLong;
	}

	public String getWartoscString() {
		return this.wartoscString;
	}

	public void setWartoscString(String wartoscString) {
		this.wartoscString = wartoscString;
	}

	public Integer getContextId() {
		return contextId;
	}

	public void setContextId(Integer contextId) {
		this.contextId = contextId;
	}

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}
	
	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public SimpleRepositoryAttribute findDict(Integer objectId, Long repKlasaId) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(SimpleRepositoryAttribute.class);
			criteria.add(Restrictions.eq("objectId", objectId));
			criteria.add(Restrictions.eq("repKlasaId", repKlasaId));
			criteria.add(Restrictions.eq("available", true));
			return (SimpleRepositoryAttribute) criteria.uniqueResult();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public SimpleRepositoryAttribute find(Long repKlasaId, Integer objectId, Integer contextId) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(SimpleRepositoryAttribute.class);
			criteria.add(Restrictions.eq("repKlasaId", repKlasaId));
			criteria.add(Restrictions.eq("objectId", objectId));
			criteria.add(Restrictions.eq("contextId", contextId));
			criteria.add(Restrictions.eq("available", true));
			return (SimpleRepositoryAttribute) criteria.uniqueResult();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}	
	
	public List<SimpleRepositoryAttribute> find(Integer objectId, Integer contextId) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(SimpleRepositoryAttribute.class);
			criteria.add(Restrictions.eq("objectId", objectId));
			criteria.add(Restrictions.eq("contextId", contextId));
			criteria.add(Restrictions.eq("available", true));
			return criteria.list();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public List<SimpleRepositoryAttribute> find(Integer objectId, Integer contextId, Long repAtrybutId) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(SimpleRepositoryAttribute.class);
			criteria.add(Restrictions.eq("objectId", objectId));
			criteria.add(Restrictions.eq("contextId", contextId));
			criteria.add(Restrictions.eq("repAtrybutId", repAtrybutId));
			return criteria.list();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public List<SimpleRepositoryAttribute> findAvailable(boolean available, Integer contextId) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(SimpleRepositoryAttribute.class);
            criteria.add(Restrictions.eq("contextId", contextId));
            criteria.add(Restrictions.eq("available", available));
			return criteria.list();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	public List<SimpleRepositoryAttribute> findAvailable(boolean available) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(SimpleRepositoryAttribute.class);
			criteria.add(Restrictions.eq("available", available));
			return criteria.list();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public List<SimpleRepositoryAttribute> findUsed(boolean used) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(SimpleRepositoryAttribute.class);
			criteria.add(Restrictions.eq("used", used));
			return criteria.list();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public void setAllFieldsFromServiceObject(RepositoryAttributesContext item) {
		this.setCzyAktywny(item.getCzyAktywny());
		this.setCzyBlokada(item.getCzyBlokada());
		this.setNazwa(item.getNazwa());
		this.setObiektSystemuId(item.getObiektSystemuId());
		this.setRepAtrybutId(item.getRepAtrybutId());
		this.setRepAtrybutIdo(item.getRepAtrybutIdo());
		this.setRepDispAtrId(item.getRepDispAtrId());
		this.setRepKlasaId(item.getRepKlasaId());
		this.setRepTypId(item.getRepTypId());
		this.setRepTypProstyIdo(item.getRepTypProstyIdo());
		this.setRepWartoscId(item.getRepWartoscId());
		this.setRodzaj(item.getRodzaj());
		this.setRodzajTypu(item.getRodzajTypu());
		this.setRodzajWartosci(item.getRodzajWartosci());
		this.setTypDlugosc(item.getTypDlugosc());
		this.setTypFormat(item.getTypFormat());
		this.setTypWartosci(item.getTypWartosci());
		this.setWartoscDate(item.getWartoscDate());
		this.setWartoscDecimal(item.getWartoscDecimal());
		this.setWartoscInstancjaId(item.getWartoscInstancjaId());
		this.setWartoscLong(item.getWartoscLong());
		this.setWartoscString(item.getWartoscString());
	}

	public void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
