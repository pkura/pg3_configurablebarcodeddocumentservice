package pl.compan.docusafe.core.imports.simple.repository;

import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

public class EnumHibernateBuilder {
	private static Logger log = LoggerFactory.getLogger(EnumHibernateBuilder.class);
	
	public static enum Field {
		AVAILABLE("available"),
		REF_VALUE("refValue"),
		CENTRUM("centrum"),
		TITLE("title"),
		CN("cn");
		
		public final String column;
		
		Field(String column) {
			this.column = column;
		}
	}
	
	private static final String INTEGER_HBM_TYPE = "integer";
	private static final String STRING_HBM_TYPE = "string";

	
	public static final String HB_ID = "id";
	public static final String HB_NAME = "name";
	public static final String HB_TYPE = "type";

	public static final String HB_ACCESS = "access";
	public static final String HB_FIELD = "field";
	public static final String HB_PROPERTY = "property";
	public static final String HB_CLASS_NAME = "ClassName";
	
	public static final String HB_UNSAVED_VALUE = "unsaved-value";
	public static final String HB_NULL = "null";
	public static final String HB_ANY = "any";
	public static final String HB_NONE = "none";
	public static final String HB_UNDEFINED = "undefined";
	public static final String HB_ID_VALUE = "id_value";
	
	public static final String HB_NODE = "node";
	public static final String HB_ELEMENT_NAME = "element-name";
	public static final String HB_ATTRIBUTE_NAME = "@attribute-name";
	public static final String HB_ELEMENT_ATTRIBUTE = "element/@attribute";
	
	public static final String HB_GENERATOR = "generator";
	public static final String HB_CLASS = "class";
	public static final String HB_NATIVE = "native";
			
	public static final String HB_PARAM = "param";
	public static final String HB_SEQUENCE = "sequence";
	
	public static final String HB_COLUMN = "column";
	public static final String HB_UPDATE = "update";
	public static final String HB_INSERT = "insert";
	public static final String HB_FORMULA = "formula";
	public static final String HB_LAZY = "lazy";
	public static final String HB_UNIQUE = "unique";
	public static final String HB_NOT_NULL = "not-null";
	public static final String HB_OPTIMISTIC_LOCK = "optimistic-lock";
	public static final String HB_GENERATED = "generated";
	public static final String HB_INDEX = "index";
	public static final String HB_UNIQUE_KEY = "unique-key";
	public static final String HB_LENGTH = "length";
	public static final String HB_PRECISION = "precision";
	public static final String HB_SCALE = "scale";
	
	public static final String FALSE = "false";
	public static final String TRUE = "true";
	
	public static final String HB_HIBERNATE_MAPPING = "hibernate-mapping";
	public static final String HB_DEFAULT_LAZY = "default-lazy";
	public static final String HB_TABLE = "table";
	public static final String DOCKIND_DEFAULT_TABLE_PREFIX = "dsg";
	public static final String SEQUENCE_POSTFIX = "id";
	public static final String HB_DEFAULT_ID_TYPE = "long";
	
	private static final List<FieldHbmProperites> enumFields = Lists.newArrayList();
	static {
		enumFields.add(new FieldHbmProperites(Field.CN.column, STRING_HBM_TYPE, 50));
		enumFields.add(new FieldHbmProperites(Field.TITLE.column, STRING_HBM_TYPE, 128));
		enumFields.add(new FieldHbmProperites(Field.CENTRUM.column, INTEGER_HBM_TYPE));
		enumFields.add(new FieldHbmProperites(Field.REF_VALUE.column, INTEGER_HBM_TYPE));
		enumFields.add(new FieldHbmProperites(Field.AVAILABLE.column, INTEGER_HBM_TYPE));
	}
	
	private Document hbmDocument;
	
	public EnumHibernateBuilder(String tableName) {
		setHbmDocument(DocumentHelper.createDocument());
		createHeader(tableName);
		addFields();
		log.error(hbmDocument.asXML());
	}
	
	public void createHeader(String tableName){
		getHbmDocument().addDocType(HB_HIBERNATE_MAPPING, "-//Hibernate/Hibernate Mapping DTD 3.0//EN", "http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd");
		getHbmDocument().addElement(HB_HIBERNATE_MAPPING)
			.addAttribute(HB_DEFAULT_LAZY, FALSE)
			.addElement(HB_CLASS)
			.addAttribute("entity-name", tableName)
			.addAttribute(HB_TABLE, tableName)
			.addElement(HB_ID)
			.addAttribute(HB_ACCESS, HB_FIELD)
			.addAttribute(HB_NAME, "id")
			.addAttribute(HB_TYPE, HB_DEFAULT_ID_TYPE)
			.addElement(HB_GENERATOR)
			.addAttribute(HB_CLASS, HB_NATIVE)
			.addElement(HB_PARAM)
			.addAttribute(HB_NAME, HB_SEQUENCE)
			.addText(tableName+"_"+SEQUENCE_POSTFIX);
	}
	
	public void addFields() {
		for (FieldHbmProperites field : enumFields) {
			addField(field);
		}
	}
	
	public void addField(FieldHbmProperites field) {
		Element hbmProperty = getHbmDocument().getRootElement().element(HB_CLASS).addElement(HB_PROPERTY);
		hbmProperty.addAttribute(HB_ACCESS, HB_FIELD);
		hbmProperty.addAttribute(HB_NOT_NULL, String.valueOf(field.isNotNull()));
		hbmProperty.addAttribute(HB_TYPE, field.getType());
		hbmProperty.addAttribute(HB_COLUMN, Objects.firstNonNull(field.getColumn(), field.getName()));
		hbmProperty.addAttribute(HB_NAME, field.getName());
	}

	/**
	 * @return the hbmDocument
	 */
	public Document getHbmDocument() {
		return hbmDocument;
	}

	/**
	 * @param hbmDocument the hbmDocument to set
	 */
	public void setHbmDocument(Document hbmDocument) {
		this.hbmDocument = hbmDocument;
	}
}

class FieldHbmProperites {
	private String type;
	private String name;
	private String column;
	private Integer lenght;
	private boolean notNull = false;
	
	private FieldHbmProperites() {
	}
	
	public FieldHbmProperites(String name, String type) {
		this.name = name;
		this.type = type;
	}
	
	public FieldHbmProperites(String name, String type, boolean notNull) {
		this.name = name;
		this.type = type;
		this.notNull = notNull;
	}
	
	public FieldHbmProperites(String name, String type, Integer lenght) {
		this.name = name;
		this.type = type;
		this.lenght = lenght;
	}
	
	public FieldHbmProperites(String name, String type, Integer lenght, boolean notNull) {
		this.name = name;
		this.type = type;
		this.lenght = lenght;
		this.notNull = notNull;
	}
	
	public FieldHbmProperites(String name, String column, String type, Integer lenght, boolean notNull) {
		this.name = name;
		this.column = column;
		this.type = type;
		this.lenght = lenght;
		this.notNull = notNull;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public Integer getLenght() {
		return lenght;
	}

	public void setLenght(Integer lenght) {
		this.lenght = lenght;
	}

	public boolean isNotNull() {
		return notNull;
	}

	public void setNotNull(boolean notNull) {
		this.notNull = notNull;
	}
	
}
