package pl.compan.docusafe.core.imports.simple.repository;


import org.apache.commons.lang.StringEscapeUtils;
import pl.compan.docusafe.core.imports.simple.repository.EnumHibernateBuilder.Field;

import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class ReposiotryAttributeDict {
	private String title;
	private Long code;
	private String cn;
	private String tableName;
	private String columnName;
	private int updated;
	private int inserted;
	public EdmException exception;
	private List<EnumItem> items = Lists.newArrayList();
	
	public ReposiotryAttributeDict(){
	}
	
	public ReposiotryAttributeDict(ReposiotryAttributeDict dict) {
		this.title = dict.getTitle();
		this.code = dict.getCode();
		this.cn = dict.getCn();
		this.tableName = dict.getTableName();
		this.columnName = dict.getColumnName();
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public int getUpdated() {
		return updated;
	}
	public void setUpdated(int updated) {
		this.updated = updated;
	}
	public int getInserted() {
		return inserted;
	}
	public void setInserted(int inserted) {
		this.inserted = inserted;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public List<EnumItem> getItems() {
		return items;
	}
	public void setItems(List<EnumItem> items) {
		this.items = items;
	}
	public void addItem(Long erpId, String cn, String title, Long repAtrybutId) {
		EnumItem item = new EnumItem(erpId, cn, title, repAtrybutId);
		items.add(item);
	}
	public int itemsSize() {
		return items.size();
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
}

class EnumItem {
	private Long erpId;
	private Long repAtrybutId;
	private String cn;
	private String title;
	
	Map<Field,Object> fields;
	
	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public Long getRepAtrybutId() {
		return repAtrybutId;
	}

	public void setRepAtrybutId(Long repAtrybutId) {
		this.repAtrybutId = repAtrybutId;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public EnumItem(Long erpId, String cn, String title, Long repAtrybutId) {
		fields = Maps.newHashMap();
		
		this.erpId = erpId;
		fields.put(Field.CENTRUM, erpId);
		
		this.cn = cn;
		fields.put(Field.CN, cn);
		
		this.title = title;
		fields.put(Field.TITLE, title);
		
		this.repAtrybutId = repAtrybutId;
		fields.put(Field.REF_VALUE, repAtrybutId);
		
		fields.put(Field.AVAILABLE, 1);
	}
	
	public String getUpdateSql(String tableName) {
		StringBuilder code = new StringBuilder("UPDATE ");
		code.append(tableName);
		code.append(" SET ");

		int i = 0;
		for (Field field : fields.keySet()) {
			if (i++>0) {
				code.append(",");
			}
			
			code.append(field.column);
			code.append("=");
			if (fields.get(field) == null) {
				code.append("null");
			} else {
				if (fields.get(field) instanceof String) {
					code.append("'");
                    code.append(StringEscapeUtils.escapeSql(fields.get(field).toString()));
					code.append("'");
				} else {
					code.append(fields.get(field));
				}
			}
		}
		code.append(" WHERE ");
		code.append(Field.CENTRUM.column);
		code.append("=");
		code.append(fields.get(Field.CENTRUM));
		code.append(" AND ");
		code.append(Field.REF_VALUE.column);
		code.append("=");
		code.append(fields.get(Field.REF_VALUE));
		code.append(";");
		
		return code.toString();
	}
	
	public String getInsertSql(String tableName) {
		int i;
		StringBuilder code = new StringBuilder("INSERT INTO ");
		code.append(tableName);
		code.append(" (");
		
		i = 0;
		for (Field field : fields.keySet()) {
			if (i++>0) {
				code.append(",");
			}
			code.append(field.column);
		}
		code.append(") VALUES (");
		
		i = 0;
		for (Field field : fields.keySet()) {
			if (i++>0) {
				code.append(",");
			}
			if (fields.get(field) == null) {
				code.append("null");
			} else {
				if (fields.get(field) instanceof String) {
					code.append("'");
					code.append(StringEscapeUtils.escapeSql(fields.get(field).toString()));
					code.append("'");
				} else {
					code.append(fields.get(field));
				}
			}
		}
		code.append(");");
		
		return code.toString();
	}
	
}