package pl.compan.docusafe.core.imports;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.ald.ALDLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class SimpleImportsManager {

	public final static Logger log = LoggerFactory.getLogger(SimpleImportsManager.class);

	public static StringManager sm = StringManager.getManager(SimpleImportsManager.class.getPackage().getName());
	/**
     * Funkcja wczytuje z pliku dane o pismach wychodzących i tworzy pisma wychodzące w systemie. Jeśli
     * w kilku kolejnych wierszach znajduje się ten sam numer klienta kolejne faktury są dodawane do jednego
     * pisma. Numery faktur są dodawane w opisie pisma, a kwoty się sumują.
     * @param file - plik z danymi o pismach wychodzących
     * @param output - strumień wyjściowy do którego zapisywany jest raport
     * @return zwraca wartość logiczną mówiącą czy import się powiódł
     * @throws EdmException
     */
    public static boolean parseAndSaveNWOutgoingFile(File file, OutputStream output, String dataPrzyjecia) throws EdmException
    {
    	PrintStream writer = null;
    	try
    	{
    		
    		writer = new PrintStream(output,false,"UTF-8");

    		InputStream inp = new FileInputStream(file);

    	    Workbook wb = WorkbookFactory.create(inp);
    	    Sheet sheet = wb.getSheetAt(0);
    	    Iterator<Row> rows = sheet.rowIterator();

    	    OutOfficeDocumentDelivery delivery = null;
			for(OutOfficeDocumentDelivery ood : OutOfficeDocumentDelivery.list())
			{
				if(ood.getName().equalsIgnoreCase("poczta"))
				{
					delivery = ood;
					break;
				}
			}
			
			if(delivery == null)
				delivery = new OutOfficeDocumentDelivery("poczta");
    	    
    	    int lp = 0;

    	    DSApi.context().begin();

    	    while(rows.hasNext())
    	    {
    	    	try
    	    	{
    	    	Row row = rows.next();

    	    	//sprawdzenie numeru
    	    	try
            	{
    	    		if(row.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC)
    	    		{
    	    			lp = ((Double) row.getCell(0).getNumericCellValue()).intValue();
    	    		}
    	    		else if(row.getCell(0).getCellType() == Cell.CELL_TYPE_STRING)
    	    		{
    	    			lp = Integer.parseInt(row.getCell(0).getStringCellValue().trim());
    	    		}
    	    		else
    	    		{
    	    			continue;
    	    		}
				}
            	catch (Exception e)
            	{
					continue;
				}
            	//sprawdzenie numeru : end

            	String organization = row.getCell(1).getStringCellValue();

            	String adres = row.getCell(2).getStringCellValue(); //uzywany tylko do rozbicia
            	Pattern p = Pattern.compile("[0-9]{2}-[0-9]{3}");
            	Matcher m = p.matcher(adres);
            	String street = "";
            	String zip = "";
            	String location = "";

            	if(m.find())
            	{
            		street = adres.substring(0, m.start());
            		zip = m.group();
            		location = adres.substring(m.end());
				}
				else
				{
					street = adres;
				}

            	String weight = row.getCell(3).getStringCellValue().replaceAll("[^0-9]", "").trim();
            	
            	String numerNadawczy = null; 
            		
            	if(row.getCell(6) != null && row.getCell(6).getCellType() == Cell.CELL_TYPE_NUMERIC)
            	{            		
            		//numerNadawczy = //row.getCell(6).getNumericCellValue() != null ? ((Double)row.getCell(6).getNumericCellValue()).intValue() : null;
            		numerNadawczy = String.valueOf(row.getCell(6).getNumericCellValue()).equals("null") ? null : String.valueOf(row.getCell(6).getNumericCellValue());
            	}
            	else if(row.getCell(6) != null)
            	{
            		numerNadawczy = row.getCell(6).getStringCellValue().replace(" ", "");
            	}
            	
            	
            	
            	String oplata = row.getCell(7).getCellType() == Cell.CELL_TYPE_NUMERIC ? ((Double)row.getCell(7).getNumericCellValue()).toString() :  row.getCell(7).getStringCellValue().replaceAll("[^0-9,]","").replace(",",".").trim();
            	String uwagi = row.getCell(4) != null ? row.getCell(4).getStringCellValue() : "";
            	String adnotacje = row.getCell(5) != null ? row.getCell(5).getStringCellValue() : "";

            	OutOfficeDocument doc = new OutOfficeDocument();
            	doc.setDocumentKind(DocumentKind.findByCn(DocumentKind.NORMAL_KIND));

            		Recipient person = new Recipient();
            		person.setStreet(street);
            		person.setZip(zip);
            		person.setLocation(location);
            		person.setOrganization(organization);

            	doc.addRecipient(person);

            	doc.setWeightG(Integer.parseInt(weight.substring(0, weight.length()>3 ? 3 : weight.length())));
				doc.setWeightKg(Integer.parseInt(weight.length()>3 ? weight.substring(3): "0"));

				DecimalFormat df = new DecimalFormat();
				try
				{
					//doc.setPostalRegNumber(df.getNumberInstance().format(numerNadawczy).replace(" ", ""));
					doc.setPostalRegNumber(numerNadawczy);
				}
				catch(NullPointerException f) {}


				if(oplata.length() > 0)
				{
					doc.setStampFee(new BigDecimal(Float.parseFloat(oplata)));
				}

				Map address = GlobalPreferences.getAddress();
				Sender sender = new Sender();

					sender.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
					sender.setStreet((String) address.get(GlobalPreferences.STREET));
					sender.setZip((String) address.get(GlobalPreferences.ZIP));
					sender.setLocation((String) address.get(GlobalPreferences.LOCATION));

	            doc.setSender(sender);

	            doc.setInternal(false);
				doc.setSummary("zaimportowane");
				doc.setAssignedDivision(DSDivision.ROOT_GUID);
				doc.setCreatingUser(DSApi.context().getPrincipalName());
				
				doc.setDelivery(delivery);
				doc.setDocumentDate(DateUtils.getCurrentTime());
				
				
				doc.setClerk(DSApi.context().getPrincipalName());
				doc.setCurrentAssignmentUsername(DSApi.context().getPrincipalName());
				doc.create();

				Remark rem = new Remark(uwagi+" "+adnotacje , DSApi.context().getPrincipalName());
				doc.addRemark(rem);

				StringBuffer description = new StringBuffer();
				
				if(numerNadawczy != null && numerNadawczy.length() > 0)
				{
					description.append(sm.getString("listPolecony"));
				}
				else
				{
					description.append(sm.getString("listZwykly"));
				}
				
				description.append(' ');
				description.append(rem.getContent());
				
				doc.setDescription(description.toString());
				doc.setSummary(description.toString());
				//doc.setDescription(rem.getContent());

				Long newDocumentId = doc.getId();

				long journalId = Journal.getMainOutgoing().getId();
				//int sequence = Journal.TX_newEntry2(journalId, newDocumentId, GlobalPreferences.getCurrentDay());
				int sequence = Journal.TX_newEntry2(journalId, newDocumentId, DateUtils.parseJsDate(dataPrzyjecia));
				
				doc.bindToJournal(Journal.getMainOutgoing().getId(), sequence, DateUtils.parseJsDate(dataPrzyjecia));

				doc.setPrepState(OutOfficeDocument.PREP_STATE_FAIR_COPY);
				
				LoggerFactory.getLogger("tomekl").debug("docid {}",doc.getId());
				
				WorkflowFactory.createNewProcess(doc, true, "Pismo zaimportowane");

            	writer.println("Dodano do importu pismo nr "+lp);

    	    	}
    	    	catch (Exception e)
    	    	{
    	    		e.printStackTrace();
    	    		writer.println("Blad w pismie nr "+lp);
    	    		throw new EdmException("Blad w pismie nr "+lp,e);
				}
    	    }

    	    DSApi.context().commit();

    	    writer.println("Zakonczono import pism");

    	    org.apache.commons.io.IOUtils.closeQuietly(inp);


    	}
    	catch (Exception e)
    	{
    		DSApi.context()._rollback();
    		if(writer != null)
    		{
    			writer.println("Import nieudany");
    			writer.flush();
    		    writer.close();
    		    writer = null;
    		}
			//e.printStackTrace();
    		log.error(e.getMessage(),e);
			return false;
		}

    	if(writer != null)
    	{
    		writer.flush();
    		writer.close();
    	}

    	return true;
    }

    public static boolean parseAndSaveFaxFile(File file, OutputStream output, File dir, Long journalId) throws EdmException {
		log.info("rozpocz�to import fax�w - plik - {}, katalog - {}", file.toURI(), dir==null?"brak":dir.toURI());

		PrintStream writer;
		try{
			writer = new PrintStream(output,false,"UTF-8");
		} catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		Connection conn = null;
		try {
    		DSApi.context().begin();
			log.debug("otwarto kontekst");
			conn = Docusafe.getDataSource().getConnection();
			conn.setAutoCommit(false);

    		Journal mainJournal = null;
    		Journal bindJournal = Journal.find(journalId);
    		if (!bindJournal.isMain()) {
				mainJournal = Journal.getMainIncoming();
			}

    		InputStream is = new FileInputStream(file);
    		POIFSFileSystem fs = new POIFSFileSystem( is );
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);

            //Iterator<HSSFRow> rows = sheet.rowIterator();
            Iterator<Row> rows = sheet.rowIterator();
            while (rows.hasNext()) {
				log.info("parsowanie wiersza");
            	//HSSFRow row = (HSSFRow) rows.next();
				Row row = rows.next();

				Integer nrKo = null;
            	try {
					nrKo = ((Double) row.getCell(0).getNumericCellValue()).intValue();
				} catch (Exception e) {
					log.debug("error:" + e.getMessage(), e);
					continue;
				}

            	if(nrKo < 1){
            		continue;
				}

				Date dataPisma;
            	try {
					 dataPisma = row.getCell(1) != null ? row.getCell(1).getDateCellValue() : null; //
				} catch (Exception e) {
					writer.println("[2," + (row.getRowNum()+1) + "] blad podczas wczytywania daty pisma");
					throw e;
				}


            	String opisPisma = row.getCell(2) != null ? row.getCell(2).getRichStringCellValue().getString() : ""; //
            	String imie = row.getCell(3) != null ? row.getCell(3).getRichStringCellValue().getString() : "";
            	String nazwisko = row.getCell(4) != null ? row.getCell(4).getRichStringCellValue().getString() : "";

            	String instytucja = row.getCell(5) != null ? row.getCell(5).getRichStringCellValue().getString() : "";

            	String ulica = row.getCell(6) != null ? row.getCell(6).getRichStringCellValue().getString() : "";
            	//String nrdomu = row.getCell(7) != null ? ""+((Double) row.getCell(7).getNumericCellValue()).intValue() : "";
            	//String nrlokalu = row.getCell(8) != null ? ""+((Double) row.getCell(8).getNumericCellValue()).intValue() : "";

            	String kod = "00-000";
            	Cell kodCell = row.getCell(7);
            	if(kodCell != null){
					if(kodCell.getCellType() ==  HSSFCell.CELL_TYPE_NUMERIC)
					{
						DecimalFormat df = new DecimalFormat("00000");
						String tmp = df.format(Math.abs(kodCell.getNumericCellValue()));
						kod = String.format("%s-%s", tmp.substring(0, 2), tmp.substring(2,5));						
					}
					else if(kodCell.getCellType() == HSSFCell.CELL_TYPE_STRING)
					{
						kod = kodCell.getRichStringCellValue().getString();
					}
				}

            	String miasto = row.getCell(8) != null ? row.getCell(8).getRichStringCellValue().getString() : "";

				String nrNadawczy = null;
				if(row.getCell(9) != null)
				{
					if(row.getCell(9).getCellType() ==  HSSFCell.CELL_TYPE_NUMERIC)
					{
						nrNadawczy = "" + (int)row.getCell(9).getNumericCellValue();
					}
					else if(row.getCell(9).getCellType() == HSSFCell.CELL_TYPE_STRING)
					{
						nrNadawczy = row.getCell(9).toString();
					}
				}

            	String odbiorcaAdres = row.getCell(10) != null ? row.getCell(10).getRichStringCellValue().getString() : "";
            	String odbiorcaImieNazwisko = row.getCell(11) != null ? row.getCell(11).getRichStringCellValue().getString() : "";
            	String odbiorcaDzial = row.getCell(12) != null ? row.getCell(12).getRichStringCellValue().getString() : "";
            	String skan = row.getCell(13) != null ? row.getCell(13).getRichStringCellValue().getString() : null;

            	LoggerFactory.getLogger("tomekl").debug(nrKo+" "+dataPisma+" "+opisPisma+" "+imie+" "+nazwisko+" "+instytucja+" "+ulica+" "+kod+" "+miasto+" "+nrNadawczy+" "+odbiorcaAdres+" "+odbiorcaImieNazwisko+" "+odbiorcaDzial+" "+skan);

            	InOfficeDocument doc = new InOfficeDocument();

            	doc.setTitle(opisPisma);
            	doc.setSummary(opisPisma);
            	doc.setDescription(opisPisma);

            	doc.setAssignedDivision(DSDivision.ROOT_GUID);
    			doc.setDocumentDate(dataPisma);
    			doc.setIncomingDate(dataPisma);
    			



    			doc.setCreatingUser(DSApi.context().getPrincipalName());
    			doc.setClerk(DSApi.context().getPrincipalName());
				doc.setCurrentAssignmentUsername(DSApi.context().getPrincipalName());

				doc.setDocumentKind(DocumentKind.findByCn(DocumentKind.NORMAL_KIND));
            	doc.setPostalRegNumber(nrNadawczy);

            	/*Sender sender = new Sender();
            	sender.setAnonymous(true);
            	doc.setSender(sender);*/

            	try{
					addSender(imie,nazwisko,instytucja,ulica,kod,miasto,doc);
					addRecipient(odbiorcaImieNazwisko, odbiorcaAdres, odbiorcaDzial, doc);
					/*if(StringUtils.isNotBlank(skan)){
						addFaxAttachment(skan, dir, doc);
					}*/
				} catch(Exception e) {
					LoggerFactory.getLogger("tomekl").debug(e.getMessage(),e);
					writer.println(e.getMessage());
					throw e;
				}
            	
            	doc.create();
            	doc.getDocumentKind().set(doc.getId(), new HashMap<String, Object>());
            	doc.getDocumentKind().logic().archiveActions(doc, DocumentLogic.TYPE_OUT_OFFICE);
            	
            	try
            	{
            		if(StringUtils.isNotBlank(skan))
						addFaxAttachment(skan, dir, doc);
            	}
            	catch (Exception e) 
            	{
            		
					e.printStackTrace();
					throw e;
				}
            	
            	//dzienniki
    			int sequence = Journal.TX_uncommitedNewEntry(bindJournal.getId(), doc.getId(), GlobalPreferences.getCurrentDay(), conn);
    			if (sequence != nrKo) {
					writer.println("Niezgodnosc numerow ko. Podano numer : " + nrKo + " , spodziewano sie " + sequence);
					writer.println("Import zostanie cofniety.");
					throw new Exception("Niezgodnosc numerow ko");
				}

    			doc.bindToJournal(bindJournal.getId(), sequence);

    			if (mainJournal != null) {
					int sequencee = Journal.TX_uncommitedNewEntry(mainJournal.getId(), doc.getId(), GlobalPreferences.getCurrentDay(), conn);
					doc.bindToJournal(mainJournal.getId(), sequencee);
				}

            	WorkflowFactory.createNewProcess(doc, true, "Pismo zaimportowane");
            	writer.println("Dodano dokument o numerze ko "+nrKo+" id : "+doc.getId());
            }

			DSApi.context().commit();
			conn.commit();
    	} catch (Exception e) {
			log.error(e.getMessage(), e);
			try{
				conn.rollback();
			} catch(Exception er){
				log.error(er.getMessage(),er);
			}
			DSApi.context()._rollback();
			return false;
		} finally {
			DbUtils.closeQuietly(conn);
			try {
				writer.close();
				output.flush();
				output.close();
			} catch (Exception e) {
			}
		}

    	return true;
    }

    public static boolean parseAndSaveALDOutgoingFile(File file, OutputStream output) throws EdmException
    {
    	boolean result=true;
    	String nrFakt = "";
        try
        {
        	DSApi.context().begin();
        	PrintWriter writer = new PrintWriter(output);
        	writer.println("Rozpocz�to import.");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "cp1250"));
            String line;
            int lineCount = 0, errorCount = 0;
            String opis = "";
            float kwota = 0;
            String[] prevParts = {""};
            int docsCount = 0;
            boolean koniec = false;
            while (!koniec)
            {
            	if((line = reader.readLine()) == null) {
            		line = " ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;";
            		koniec = true;
            	}
                try {
					lineCount++;
					if(lineCount%10==0) DSApi.context().session().clear();
					line = line.trim();
					if (line.startsWith("#")){
					    lineCount--;
						continue;
					}

					String[] parts = line.split(";");
					if(parts.length<13) {
						lineCount--;
						continue;
					}
					if(lineCount == 1) prevParts = parts;

					if(parts[12].replace("\"","").trim().compareTo(prevParts[12].replace("\"","").trim())!=0||koniec)
					{
						docsCount++;
						OutOfficeDocument doc = new OutOfficeDocument();
						Recipient person = new Recipient();
						doc.setDescription(opis);
						nrFakt = prevParts[1].replace("\"","").trim();
						String org = prevParts[7].replace("\"","").trim() + " " + prevParts[8].replace("\"","").trim();
						person.setOrganization(org.substring(0, org.length()<50?org.length()-1:49));
						person.setStreet(prevParts[9].replace("\"","").trim());
						person.setZip(prevParts[10].replace("\"","").trim());
						person.setLocation(prevParts[11].replace("\"","").trim());
						person.setNip(prevParts[12].replace("\"","").trim());
						doc.addRecipient(person);
						Sender s = new Sender();

						Map address = GlobalPreferences.getAddress();

						s.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
			            s.setStreet((String) address.get(GlobalPreferences.STREET));
			            s.setZip((String) address.get(GlobalPreferences.ZIP));
			            s.setLocation((String) address.get(GlobalPreferences.LOCATION));

			            doc.setSender(s);

						DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.ALD_KIND);
						doc.setDocumentKind(documentKind);
						doc.setInternal(false);
						doc.setSummary(opis.substring(0,opis.length()-1) + " - zaimportowane");
						doc.setAssignedDivision(DSDivision.ROOT_GUID);
						doc.setCreatingUser(DSApi.context().getPrincipalName());
						doc.setDocumentDate(DateUtils.getCurrentTime());
						doc.setClerk(DSApi.context().getPrincipalName());
						doc.setCurrentAssignmentUsername(DSApi.context().getPrincipalName());
						doc.create();


						Long newDocumentId = doc.getId();

						DSApi.context().session().flush();
					//	FieldsManager fm = documentKind.getFieldsManager(newDocumentId);
					//	fm.initialize();
						Map<String, Object> values = new HashMap<String, Object>();//fm.getFieldValues();

						values.put(ALDLogic.NIP_DOSTAWCY_FIELD_CN, prevParts[12].replace("\"","").trim());
						//values.put(ALDLogic.NR_FAKTURY_FIELD_CN, prevParts[1].replace("\"","").trim());
						//values.put(ALDLogic.DATA_FIELD_CN, parseDate(prevParts[2].replace("\"","").trim()));
						values.put(ALDLogic.KWOTA_BRUTTO_FIELD_CN, kwota);
						//values.put(ALDLogic.KATEGORIA_FIELD_CN, fm.getField(ALDLogic.KATEGORIA_FIELD_CN).getEnumItemByCn(ALDLogic.KATEGORIA_FIELD_CN).getId());
						values.put(ALDLogic.KATEGORIA_FIELD_CN, documentKind.getFieldByCn(ALDLogic.KATEGORIA_FIELD_CN).getEnumItemByCn(ALDLogic.ADMINISTRACJA_CN).getId());
						values.put(ALDLogic.DATA_PLATNOSCI_FIELD_CN, parseDate(prevParts[3].replace("\"","").trim()));
						values.put(ALDLogic.NAZWA_KLIENTA_FIELD_CN, org.substring(0, org.length()<50?org.length()-1:49));
						values.put(ALDLogic.STATUS_FIELD_CN, ALDLogic.STATUS_ZAAKCEPTOWANY);
						doc.getDocumentKind().setOnly(newDocumentId, values);


						doc.setPrepState(OutOfficeDocument.PREP_STATE_FAIR_COPY);

		                documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_OUT_OFFICE);
		                documentKind.logic().documentPermissions(doc);
		                //long journalId = Journal.getMainOutgoing().getId();		// dodawanie do dziennika. Obecnie wy��czone.
						//int sequence = Journal.TX_newEntry2(journalId, newDocumentId, GlobalPreferences.getCurrentDay());
						//doc.bindToJournal(Journal.getMainOutgoing().getId(), sequence);

						WorkflowFactory.createNewProcess(doc, true, "Pismo zaimportowane");



						DSApi.context().session().flush();

						writer.println("OK. Zaimportowano faktur� numer: " + nrFakt);
						prevParts = parts;
						if(!koniec){
							opis = parts[1].replace("\"","").trim() + ",";
							kwota = Float.parseFloat((parts[4].replace(",", ".").trim()));
						}

					}
					else
					{
						opis += parts[1].replace("\"","").trim() + ",";
						kwota += Float.parseFloat((parts[4].replace(",", ".").trim()));
					}

				}
                catch(EdmException e)
                {
                	LogFactory.getLog("eprint").error("", e);
                	writer.println("---B��d! Nie uda�o si� zaimportowa� faktury nr: " + nrFakt);
                	result = false;
                	errorCount++;
                	continue;
                }
                catch(Exception e)
                {
                	LogFactory.getLog("eprint").error("", e);
                	writer.println("---B��d! Nie uda�o si� zaimportowa� faktury nr: " + nrFakt +e.getMessage());
                	LogFactory.getLog("eprint").debug("", e);
                	result = false;
                	errorCount++;
                	continue;
                }

            }
            writer.println("Ca�kowita liczba b��dnych import�w: " + errorCount);
            writer.println("Ca�kowita liczba faktur: " + lineCount);
            writer.println("Ca�kowita liczba dokument�w: " + docsCount);
            writer.close();
            DSApi.context().commit();
            return result;

        }
        catch (EdmException e)
        {
        	DSApi.context().setRollbackOnly();
        	LogFactory.getLog("eprint").error("", e);
        	throw new EdmException("B��d podczas czytania pliku z pismami wychodz�cymi");
        }
        catch (IOException e)
        {
        	DSApi.context().setRollbackOnly();
        	LogFactory.getLog("eprint").error("", e);
            throw new EdmException("B��d podczas czytania pliku z pismami wychodz�cymi");
        }


    }

	private static void addFaxAttachment(String skan, File dir, InOfficeDocument doc) throws EdmException {
		if(dir == null){
			throw new EdmException("Nie podano katalogu z za��cznikami");
		}
		Attachment attachment = new Attachment(skan);
		doc.createAttachment(attachment);
		File fax = new File(dir.getAbsoluteFile() + File.separator + skan);
		if(!fax.exists()){
			throw new EdmException("Plik:" + fax.getAbsolutePath()+" nie isnieje");
		}
		attachment.setType(Attachment.ATTACHMENT_TYPE_DOCUMENT);
		attachment.createRevision(fax).setOriginalFilename(skan);
	}

	private static void addRecipient(String odbiorcaImieNazwisko, String odbiorcaAdres, String odbiorcaDzial, InOfficeDocument doc) throws EdmException {
		Recipient rec = new Recipient();
		processNameSurname(odbiorcaImieNazwisko, rec);
		processInstAddr(odbiorcaAdres, rec);
		rec.setOrganizationDivision(odbiorcaDzial);
		doc.addRecipient(rec);
		DSApi.context().session().save(rec);
	}

	private static void addSender( String imie, String nazwisko, String instytucja, String ulica, String kod, String miasto, InOfficeDocument doc) throws EdmException
	{
		Sender sender = new Sender();
		sender.setFirstname(imie);
		sender.setLastname(nazwisko);
		sender.setOrganization(instytucja);
		sender.setStreet(ulica);
		sender.setZip(kod);
		sender.setLocation(miasto);
		doc.setSender(sender);
		DSApi.context().session().save(sender);
	}

	private static void addSender(String imieNazwisko, String instytucja, InOfficeDocument doc) throws EdmException {
		Sender sender = new Sender();
		processNameSurname(imieNazwisko, sender);
		processInstAddr(instytucja, sender);
		doc.setSender(sender);
		DSApi.context().session().save(sender);
	}

    private static Date parseDate(String d)
    {
    	Date result = null;
    	String data = d.substring(6);
    	data += "-" + d.substring(4, 6);
    	data += "-" + d.substring(0, 4);
    	result=DateUtils.nullSafeParseJsDate(data);
    	return result;

    }

    private static void processInstAddr(String odbiorcaAdres, Person rec) {
		if (!StringUtils.isBlank(odbiorcaAdres)) {
			String[] instAddr = StringUtils.split(odbiorcaAdres, ",", 2); // dzieli na {instytucja, adres}
			rec.setOrganization(instAddr[0]);
			if (instAddr.length > 1) {
				String[] addr = instAddr[1].split(",");
				rec.setStreet(addr[0]);
				if (addr.length > 1) {
					rec.setLocation(addr[1]);
					if (addr.length > 2) {
						rec.setCountry(addr[2]);
					}
				}
			}
		}
	}

	private static void processNameSurname(String odbiorcaImieNazwisko, Person rec) throws EdmException {
		if (!StringUtils.isBlank(odbiorcaImieNazwisko)) {
			String[] nameSurname = odbiorcaImieNazwisko.split(" ");
			if (nameSurname.length < 2) {
				throw new EdmException(odbiorcaImieNazwisko + " - nie poprawnie sformatowane imi� i nazwisko");
			}
			rec.setFirstname(nameSurname[0]);
			rec.setLastname(nameSurname[1]);
		}
	}
}
