package pl.compan.docusafe.core.imports;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.service.imports.dsi.DSIManager;

import java.util.Date;
import java.util.List;
import java.sql.SQLException;

import javax.persistence.PrePersist;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.HibernateException;
import org.hibernate.Criteria;
/* User: Administrator, Date: 2007-01-29 15:43:43 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class ImportedDocumentInfo
{
    private Long id;
    private ImportedFileInfo importedFileInfo;
    /**
     * numer linii w pliku ".csv" na podstawie ktorego utworzono importedFileInfo
     */
    private Integer lineNumber;
    private String tiffName;
    private String nrPolisy;
    private Date date;
    private Integer typDokumentuId;
    private String boxNumber;
    private Integer status;
    private Integer result;
    private String errorInfo;
    /**
     * identyfikator dokumentu, ktory zostal zaimportowany na podstawie tego obiektu
     */
    private Long documentId;
    /**
     * data przetworzenia tego importu (czyli data, kiedy status zmieniono na STATUS_DONE)
     */
    private Date processedTime;

    public ImportedDocumentInfo()
    {

    }

    public synchronized void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static List<ImportedDocumentInfo> find(Long importedFileInfoId, Integer status) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(ImportedDocumentInfo.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.eq("status", status));
            c.createCriteria("importedFileInfo").add(org.hibernate.criterion.Expression.eq("id", importedFileInfoId));

            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static ImportedDocumentInfo find(Long id) throws EdmException
    {
        return (ImportedDocumentInfo) DSApi.context().session().load(ImportedDocumentInfo.class, id);
    }

    public static Integer countDocuments(ImportedFileInfo importedFileInfo) throws EdmException
    {
    	java.sql.PreparedStatement ps = null;
        try
        {
        	/**ostatnim importem w starej strukturze to TYPE_AEGON_PTE
			 * wszystkie nast�pne musz� by� prze DSI w przyszlosci przerobic stare importy na DSI */
        	if(importedFileInfo.getImportType() > 7)
        	{
        		ps = DSApi.context().prepareStatement("select count(*) as ile from dsi_import_table where user_import_id = ?");
        		ps.setString(1, importedFileInfo.getId().toString());
        	}
        	else
        	{
        		String tableName = "ds_imported_document_info";
                if (importedFileInfo.getImportType().equals(ImportManager.TYPE_OGOLNY_XML))
                    tableName = "ds_imported_document_common";
                String q = "select count(*) as ile from " + tableName + " where imported_file_id = ?";
        	    ps = DSApi.context().prepareStatement(q);
                ps.setLong(1, importedFileInfo.getId());
        	}           
            return countDocuments(ps);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	DbUtils.closeQuietly(ps);
        }
    }

    public static Integer countProcessedDocuments(ImportedFileInfo importedFileInfo) throws EdmException
    {
    	java.sql.PreparedStatement ps = null;
        try
        {
        	/**ostatnim importem w starej strukturze to TYPE_AEGON_PTE
			 * wszystkie nast�pne musz� by� prze DSI w przyszlosci przerobic stare importy na DSI */
        	if(importedFileInfo.getImportType() > 7)
        	{
        		ps = DSApi.context().prepareStatement("select count(*) as ile from dsi_import_table where user_import_id = ? and import_status = ? ");
        		ps.setString(1, importedFileInfo.getId().toString());
        		ps.setInt(2, DSIManager.IMPORT_STATUS_OK);
        	}
        	else
        	{
	            String tableName = "ds_imported_document_info";
	            if (importedFileInfo.getImportType().equals(ImportManager.TYPE_OGOLNY_XML))
	                tableName = "ds_imported_document_common";
	            String q = "select count(*) as ile from " + tableName + " where imported_file_id = ? and (status = ? or status = ?)";
	    	    ps = DSApi.context().prepareStatement(q);
	            ps.setLong(1, importedFileInfo.getId());
	            ps.setInt(2, ImportManager.STATUS_DONE);
	            ps.setInt(3, ImportManager.STATUS_ERROR); // wiem ze te sa bledne od samego poczatku (ale tez je musze wliczyc jako przetworzone)
        	}
            return countDocuments(ps);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	DbUtils.closeQuietly(ps);
        }
    }

    public static Integer countBadDocuments(ImportedFileInfo importedFileInfo) throws EdmException
    {
    	java.sql.PreparedStatement ps = null;
        try
        {
        	/**ostatnim importem w starej strukturze to TYPE_AEGON_PTE
			 * wszystkie nast�pne musz� by� prze DSI w przyszlosci przerobic stare importy na DSI */
        	if(importedFileInfo.getImportType() > 7)
        	{
        		ps = DSApi.context().prepareStatement("select count(*) as ile from dsi_import_table where user_import_id = ? and import_status = ? ");
        		ps.setString(1, importedFileInfo.getId().toString());
        		ps.setInt(2, DSIManager.IMPORT_STATUS_ERROR);
        	}
        	else
        	{
	            String tableName = "ds_imported_document_info";
	            if (importedFileInfo.getImportType().equals(ImportManager.TYPE_OGOLNY_XML))
	                tableName = "ds_imported_document_common";
	            String q = "select count(*) as ile from " + tableName + " where imported_file_id = ? and status = ? and result = ?";
	    	    ps = DSApi.context().prepareStatement(q);
	            ps.setLong(1, importedFileInfo.getId());
	            ps.setInt(2, ImportManager.STATUS_DONE);
	            ps.setInt(3, ImportManager.RESULT_ERROR);
        	}
            return countDocuments(ps);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	DbUtils.closeQuietly(ps);
        }
    }

    private static Integer countDocuments(java.sql.PreparedStatement ps) throws SQLException
    {
        java.sql.ResultSet rs = ps.executeQuery();
        rs.next();
        int resp = 0;
        if (rs.getString("ile")!=null)
        {
            resp = rs.getInt("ile");
        }
        rs.close();
        DSApi.context().closeStatement(ps);
        return resp;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public ImportedFileInfo getImportedFileInfo()
    {
        return importedFileInfo;
    }

    public void setImportedFileInfo(ImportedFileInfo importedFileInfo)
    {
        this.importedFileInfo = importedFileInfo;
    }

    public Integer getLineNumber()
    {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber)
    {
        this.lineNumber = lineNumber;
    }

    public String getTiffName()
    {
        return tiffName;
    }

    public void setTiffName(String tiffName)
    {
        this.tiffName = tiffName;
    }

    public String getNrPolisy()
    {
        return nrPolisy;
    }

    public void setNrPolisy(String nrPolisy)
    {
        this.nrPolisy = nrPolisy;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public Integer getTypDokumentuId()
    {
        return typDokumentuId;
    }

    public void setTypDokumentuId(Integer typDokumentuId)
    {
        this.typDokumentuId = typDokumentuId;
    }

    public String getBoxNumber()
    {
        return boxNumber;
    }

    public void setBoxNumber(String boxNumber)
    {
        this.boxNumber = boxNumber;
    }

    public Integer getStatus()
    {
        return status;
    }

    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getResult()
    {
        return result;
    }

    public void setResult(Integer result)
    {
        this.result = result;
    }

    public String getErrorInfo()
    {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo)
    {
        this.errorInfo = errorInfo;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Date getProcessedTime()
    {
        return processedTime;
    }

    public void setProcessedTime(Date processedTime)
    {
        this.processedTime = processedTime;
    }
}

