package pl.compan.docusafe.core.imports;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.reports.Report;
import pl.compan.docusafe.service.reports.ReportGenerator;

/**
 * Przechowuje informacje o pliku, zawierajacym dane o dokumentach do
 * zaimportowania. Na podstawie statusu można sie dowiedziec czy proces
 * importowania nie zostal rozpoczety, jest w trakcie przetwarzania czy juz
 * zostal juz zakonczony.
 * 
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
 * @version $Id$
 */

public class ImportedFileCsv
{
	private static final Log log = LogFactory.getLog(ReportGenerator.class);

	private Long id;
	// private Integer typeId;
	private ImportedFileInfo info;

	public ImportedFileCsv()
	{

	}

	public synchronized void create() throws EdmException
	{
		try
		{
			DSApi.context().session().save(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public static ImportedFileCsv find(Long id) throws EdmException
	{
		return Finder.find(ImportedFileCsv.class, id);
	}

	public static ImportedFileCsv findByFileInfo(Long infoId) throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String q = "select id, imported_file_id from ds_imported_file_csv where imported_file_id = ?";
			ps = DSApi.context().prepareStatement(q);
			ps.setLong(1, infoId);
			rs = ps.executeQuery();
			if (rs.next())
			{
				Long id = rs.getLong(1);
				return (ImportedFileCsv) DSApi.context().session().load(ImportedFileCsv.class, id);
			}
			else
			{
				throw new EdmException("Nie znaleziono bloba dla określonego pliku importu");
			}
		}
		catch (SQLException e)
		{
			throw new EdmSQLException(e);
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (Exception e)
				{
				}
			}
			DSApi.context().closeStatement(ps);
		}
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public ImportedFileInfo getInfo()
	{
		return info;
	}

	public void setInfo(ImportedFileInfo info)
	{
		this.info = info;
	}

	/**
	 * Zwraca tymczasowy plik z danymi z bloba
	 * 
	 * @return
	 * @throws EdmException
	 */
	public File getCsvFile() throws EdmException
	{
		PreparedStatement pst = null;
		InputStream is = null;
		try
		{
			pst = DSApi.context().prepareStatement("select csv from " + DSApi.getTableName(ImportedFileCsv.class) + " where id = ?");
			pst.setLong(1, id);
			ResultSet rs = pst.executeQuery();

			if (!rs.next())
				throw new EdmException("Nie znaleziono bloba dla pliku importu nr " + id);

			Blob blob = rs.getBlob(1);
			if (blob != null)
			{
				is = blob.getBinaryStream();
				File file = File.createTempFile("docusafe_importdata_", ".csv");
				OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
				byte[] buffer = new byte[1024];
				int count;
				while ((count = is.read(buffer)) > 0)
				{
					os.write(buffer, 0, count);
				}
				is.close();
				os.close();

				if (log.isDebugEnabled())
					log.debug("file=" + file);

				return file;
			}
			else
				return null;
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
		catch (SQLException e)
		{
			throw new EdmSQLException(e);
		}
		catch (Exception e)
		{
			throw new EdmException(e);
		}
		finally
		{
			DSApi.context().closeStatement(pst);

		}
	}

	/**
	 * Zapisuje w blobie istniejącego obiektu zawartość danego pliku CSV
	 * 
	 * @param csvFile
	 *            - plik CSV
	 */
	public void setCsvData(File csvFile)
	{
		InputStream is = null;
		PreparedStatement ps = null;
		try
		{
			is = new FileInputStream(csvFile);
			ps = DSApi.context().prepareStatement("update " + DSApi.getTableName(ImportedFileCsv.class) + " set csv = ? where id = ?");
			ps.setBinaryStream(1, is, (int) csvFile.length());
			ps.setLong(2, id);
			ps.execute();
			is.close();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
			if (is != null)
				try{is.close();}catch (Exception e)	{}
		}
	}

	public ArrayList<String> getLines()
	{
		try
		{
			File file = this.getCsvFile();
			ArrayList<String> lines = new ArrayList<String>();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), Docusafe.getAdditionProperty(Configuration.ADDITION_IMPORT_ENCODING)));
			String line;
			while ((line = reader.readLine()) != null)
			{
				lines.add(line);
			}
			return lines;
		}
		catch (Exception e)
		{
			log.error("Blad w czasie inicjowania pliku CSV", e);
		}
		return null;
	}
}
