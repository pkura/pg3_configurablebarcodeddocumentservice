package pl.compan.docusafe.core.imports;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.users.auth.AuthUtil;

/* User: Administrator, Date: 2007-01-29 15:49:00 */

/**
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wliz�o</a>
 * @version $Id$
 * 
 * Przer�bka klasy ImportManager parsuj�cej pliki CSV do cel�w parsowania plik�w
 * XML z danymi o importowanych dokumentach
 */

public class XmlImportManager
{
	/**
	 * Importowanie dokument�w za pomoc� og�lnego XML-a
	 * */	
	public static void parseAndAddImportedFile(File file, String tiffDirectory, Integer importType) throws EdmException
	{
		try {
			ImportedFileInfo importedFileInfo = new ImportedFileInfo();
			importedFileInfo.setTiffDirectory(tiffDirectory);
			importedFileInfo.setImportType(importType);
			importedFileInfo.setFileName(file.getName());
			importedFileInfo.setStatus(ImportManager.STATUS_WAITING);

			importedFileInfo.create();

			ImportedFileBlob importedFileBlob = new ImportedFileBlob();
			importedFileBlob.setInfo(importedFileInfo);
			importedFileBlob.create();
			
			//�eby zadzia�a� update przy wstawianiu bloba(chyba lekko na wyrost)
			DSApi.context().commit();
			DSApi.close();			
			DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
			DSApi.context().begin();
			
			importedFileBlob.setXmlData(file);

			SAXReader reader = new SAXReader();
			org.dom4j.Document document = reader.read(file);
            
			Element root = document.getRootElement();
			if (!root.getName().equals("import"))
				throw new EdmException("Podany plik nie jest prawid�owym plikiem importu");

			Iterator itr = root.elements("document").iterator();
			int docCount = 0;
			while (itr.hasNext()) {
				itr.next();
				ImportedDocumentCommon importedDocumentCommon = new ImportedDocumentCommon();
                importedDocumentCommon.setStatus(ImportManager.STATUS_WAITING);
				importedDocumentCommon.setNumberInFile(docCount);
				
				importedDocumentCommon.setImportedFileInfo(importedFileInfo);
				//importedFileInfo.getImportedDocuments().add(importedDocumentInfo);
				importedDocumentCommon.create();
				++docCount;
			}
		} catch (DocumentException e) {
			throw new EdmException("B��d podczas czytania z pliku z danymi do importu");
		} catch (Exception e) {
		}
		
	}

	public static void startImport(Long importedFileId) throws EdmException
	{
		ImportedFileInfo importedFileInfo = ImportedFileInfo.find(importedFileId);

		if (!ImportManager.STATUS_WAITING.equals(importedFileInfo.getStatus()))
			throw new EdmException("Nie mo�na rozpocz�� importu podanego pliku");

		importedFileInfo.setStatus(ImportManager.STATUS_WORKING);
		List<ImportedDocumentCommon> importedDocuments = ImportedDocumentCommon.find(importedFileId, ImportManager.STATUS_WAITING);

		for (ImportedDocumentCommon importedDocumentCommon : importedDocuments) {
			if (ImportManager.STATUS_WAITING.equals(importedDocumentCommon.getStatus()))
				importedDocumentCommon.setStatus(ImportManager.STATUS_WORKING);
		}
	}

	/**
	 * Generuje raport w formacie csv o podanym pliku do importu i wypisuje na
	 * strumien 'output'.
	 * 
	 * @param importedFileId
	 *            identyfikator obiektu ImportedFileInfo, dla ktorego bedzie
	 *            wygenerowany raport
	 * @param output
	 *            strumien wyjsciowy, na ktory zostanie zapisany raport
	 * @throws EdmException
	 */
	public static void generateReport(Long importedFileId, OutputStream output) throws EdmException
	{
		PrintWriter writer = new PrintWriter(output);
        
		for (ImportedDocumentCommon importedDocumentCommon : ImportedDocumentCommon.find(importedFileId)) {
            Document doc = DSApi.context().load(Document.class, importedDocumentCommon.getDocumentId());
			writer.print(doc.getTitle() + ImportManager.CSV_FILE_SEPARATOR);
			writer.print(ImportManager.getResultString(importedDocumentCommon.getResult()) + ImportManager.CSV_FILE_SEPARATOR);
			writer.println(ImportManager.RESULT_OK.equals(importedDocumentCommon.getResult()) ? "Utworzono dokument o id=" + importedDocumentCommon.getDocumentId() : importedDocumentCommon.getErrorInfo());
		}

		writer.close();
	}

}
