package pl.compan.docusafe.core.imports;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.*;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Plik xls z nag��wkiem s�u��cy do importu typu KeyValue
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class XlsWithHeaderFileProvider implements Iterable<KeyValueEntity>{
    private final static Logger LOG = LoggerFactory.getLogger(XlsWithHeaderFileProvider.class);

    private final File xls;

    /**
     * obiekt zajmuj�cy si� zmian� headera na odpowiedni (zamian� alias�w),
     * mo�e wyrzuca� wyj�tki - wtedy gdy header nie spe�nia wymaga�
     */
    private Function<String[], String[]> headerFilter = Functions.identity();

    public XlsWithHeaderFileProvider(File xls) {
        if(!xls.getName().endsWith(".xls") && !xls.getName().endsWith(".xlsx")){
            throw new IllegalArgumentException("Ten plik nie jest plikiem xls");
        }
        this.xls = xls;
    }

    public Iterator<KeyValueEntity> iterator() {
        return new XlsIterator(xls, headerFilter);
    }

    public Function<String[], String[]> getHeaderFilter() {
        return headerFilter;
    }

    public void setHeaderFilter(Function<String[], String[]> headerFilter) {
        this.headerFilter = headerFilter;
    }
}

class XlsIterator implements Iterator<KeyValueEntity> {
    private final static Logger LOG = LoggerFactory.getLogger(XlsIterator.class);
    private final Iterator<Row> rowIterator;
    private final String[] header;

    XlsIterator(File xls, Function<String[], String[]> headerFilter){
        FileInputStream stream = null;
        try {
            stream = new FileInputStream(xls);
            Workbook wb = WorkbookFactory.create(stream);
            if(wb.getNumberOfSheets() < 1){
                throw new IllegalArgumentException("pusty plik xls");
            } else {
                Sheet sheet = wb.getSheetAt(0);
                rowIterator = sheet.rowIterator();
                if(!rowIterator.hasNext()){
                    throw new IllegalArgumentException("pusty plik xls");
                } else {
                    header = headerFilter.apply(readHeader(rowIterator.next()));
                }
            }            
        } catch(Exception ex) {
            if(ex instanceof RuntimeException) {
                throw (RuntimeException) ex;
            } else {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        } finally {
            IOUtils.closeQuietly(stream);
        }
    }

    private String[] readHeader(Row row){
        LOG.info("last cell num" + row.getLastCellNum());
        String[] ret = new String[row.getLastCellNum()];
        for(int i = 0; i < ret.length; ++i){            
            Cell cell = row.getCell(i);
            if(cell == null){
                ret[i] = "";
            } else {
                switch (cell.getCellType()){
                    case Cell.CELL_TYPE_NUMERIC:
                        ret[i] = String.valueOf(cell.getNumericCellValue());
                        break;
                    default:
                        ret[i] = cell.getStringCellValue();
                        break;
                }
            ret[i] = cell.getStringCellValue();
            }
        }
        LOG.info("header = " + Arrays.toString(ret));
        return ret;
    }

    public boolean hasNext() {
        return rowIterator.hasNext();
    }

    private KeyValueEntity kveFromRow(Row row){
        Map<String, String> values = new HashMap<String, String>();

        for(short i = 0; i < row.getLastCellNum(); ++i){
            Cell cell = row.getCell(i, Row.RETURN_BLANK_AS_NULL);
            if(cell != null){
                if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
                    values.put(header[i], cell.getNumericCellValue() + "");
                } else {
                    values.put(header[i], cell.getStringCellValue());
                }
            }
        }

        return KeyValueEntity.fromMap(values);
    }

    public KeyValueEntity next() {
        return kveFromRow(rowIterator.next());
    }

    public void remove() {
        throw new UnsupportedOperationException("remove not supported");
    }
}
