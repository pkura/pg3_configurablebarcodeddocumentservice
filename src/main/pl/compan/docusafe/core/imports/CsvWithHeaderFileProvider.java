package pl.compan.docusafe.core.imports;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.util.CsvParser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.*;
import java.util.*;

/**
 * Plik csv z nag��wkiem s�u��cy do importu typu KeyValue
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class CsvWithHeaderFileProvider implements Iterable<KeyValueEntity>{
    private final static Logger LOG = LoggerFactory.getLogger(CsvWithHeaderFileProvider.class);

    private final File csv;

    /**
     * obiekt zajmuj�cy si� zmian� headera na odpowiedni (zamian� alias�w),
     * mo�e wyrzuca� wyj�tki - wtedy gdy header nie spe�nia wymaga�
     */
    private Function<String[], String[]> headerFilter = Functions.identity();

    public CsvWithHeaderFileProvider(File csv) throws Exception{
        if(!csv.getName().endsWith(".csv")){
            throw new IllegalArgumentException("Ten plik nie jest plikiem csv");
        }
        this.csv = csv;
    }


    public Iterator<KeyValueEntity> iterator() {
        try {
            return new CsvIterator(csv, headerFilter);
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public Function<String[], String[]> getHeaderFilter() {
        return headerFilter;
    }

    public void setHeaderFilter(Function<String[], String[]> headerFilter) {
        this.headerFilter = headerFilter;
    }
}

/**
 * Iterator pliku csv
 */
class CsvIterator implements Iterator<KeyValueEntity> {
    private final static Logger LOG = LoggerFactory.getLogger(CsvWithHeaderFileProvider.class);
    private final String[] header;

    private long count = 0;

    private final BufferedReader reader;
    /** zmienna s�u��ca do okre�lenia czy jest co� jeszcze do wczytania - wykorzystywana w metodzie hasNext() */
    private String[] next = null;

    CsvIterator(File csv, Function<String[], String[]> headerFilter) throws Exception{
        reader = new BufferedReader(new InputStreamReader(new FileInputStream(csv), "utf8"));
        //parser = new CSVParser(';','"','\\');
        
        header = headerFilter.apply(CsvParser.parseLine(reader.readLine()));
    }

    public boolean hasNext() {
        if(next == null){
            String line = "";
            try{
                line = reader.readLine();
                if(line == null) return false;
                next = CsvParser.parseLine(line);
            } catch (Exception ex){
//                LOG.error(ex.getMessage(), ex);
                IOUtils.closeQuietly(reader);
//                next = null;
                throw new RuntimeException(ex.getMessage() + " w linii \"" + line + "\"", ex);
            }
            if(next == null){//jak next == null to znaczy, �e plik si� sko�czy� (tak przynajmniej jest napisane w dokumentacji CSVParsera)
                LOG.info("csv count = " + count);
                IOUtils.closeQuietly(reader);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public KeyValueEntity next() {
        try {
            if (hasNext()) {
                ++ count;
                return prepare(next);
            } else {
                throw new NoSuchElementException("csv file has no more records");
            }
        } finally {
            next = null;
        }
    }

    KeyValueEntity prepare(String[] input) {
        if(input.length != header.length) {
            return KeyValueEntity.error("Liczba p�l nie jest zgodna z liczb� etykiet z pierwszej linii - linia  " + Arrays.asList(input));
        }
        Map<String, String> map = new HashMap<String, String>();
        for(int i = 0; i < header.length; ++i) {
            map.put(header[i], input[i]);
        }
        return KeyValueEntity.fromMap(map);
    }

    public void remove() {
        throw new UnsupportedOperationException("removing is not supported");
    }
}
