package pl.compan.docusafe.core.imports;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Prosta klasa (opakowanie mapy) do import�w
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class KeyValueEntity {
    private final static Logger LOG = LoggerFactory.getLogger(KeyValueEntity.class);
    private final Map<String, String> values = new HashMap<String, String>();
    private String errorMessage = null;

    public static KeyValueEntity fromMap(Map<String, String> map){
        KeyValueEntity ret = new KeyValueEntity();
        ret.values.putAll(map);
        return ret;
    }

    public static KeyValueEntity error(String error){
        KeyValueEntity ret = new KeyValueEntity();
        ret.errorMessage = "error";
        return ret;
    }

    public boolean isValid(){
        return errorMessage == null;
    }

    public String getErrorMessage(){
        return errorMessage;
    }

    private KeyValueEntity(){

    }

    public String string(String key){
        return values.get(key);
    }
}