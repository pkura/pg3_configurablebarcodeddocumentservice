package pl.compan.docusafe.core.imports;

/**
* Status importu
* @author Micha� Sankowski <michal.sankowski@docusafe.pl>
*/
public enum ImportEntryStatus {

    /** Dodano nowy wpis */
    ADDED,
    /** Uaktualniono wpis */
    UPDATED,
    /** Nie dodano - duplikat */
    DUPLICATE_NOT_ADDED,
    /** Niew�a�ciwe dane - zignorowano */
    INVALID_INPUT,
    /** Po prostu zignorowano - np pusty wpis */
    IGNORED;

}
