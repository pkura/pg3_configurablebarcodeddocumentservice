package pl.compan.docusafe.core.imports;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.Callable;

/**
 * Import dowolnych warto�ci typu klucz warto��
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class KeyValueImport implements Callable<ImportResult> {
    private final static Logger LOG = LoggerFactory.getLogger(KeyValueImport.class);

    public final static TransactionPolicy TRANSACTION_PER_ENTRY = TransactionPolicy.TRANSACTION_PER_ENTRY;
    public final static TransactionPolicy TRANSACTION_FOR_ALL = TransactionPolicy.TRANSACTION_FOR_ALL;
    public final static TransactionPolicy EXTERNAL_TRANSACTION = TransactionPolicy.EXTERNAL_TRANSACTION;

    private final Iterable<KeyValueEntity> entities;
    private final Predicate<KeyValueEntity> validator;
    private final Function<KeyValueEntity, ImportEntryStatus> consumer;
    private final TransactionPolicy transactionPolicy;

    /**
     * Strategia rozpoczynania ko�czenia transakcji podczas importu
     */
    enum TransactionPolicy {
        /**
         * Transakcja dla ka�dego wpisu - wszystkie wpisy przed pierwszym b��dnym s� commitowane
         */
        TRANSACTION_PER_ENTRY,
        /**
         * Jedna transakcja dla ca�ego importu - je�li jest gdzie� b��d to ca�o�� jest rollbackowana
         */
        TRANSACTION_FOR_ALL,

        /**
         * Funkcja CALL jest wywo�ywana w trakcie transakcji
         */
        EXTERNAL_TRANSACTION,
    }

    /**
     * Tworzy obiekt imports
     *
     * @param provider  - dostawca warto�ci do importu
     * @param validator - obiekt sprawdzaj�cy poprawno��
     * @param consumer  - konsument wprowadzanych warto�ci
     */
    public KeyValueImport(Iterable<KeyValueEntity> provider,
                          Predicate<KeyValueEntity> validator,
                          Function<KeyValueEntity, ImportEntryStatus> consumer,
                          TransactionPolicy transactionPolicy) {
        this.consumer = consumer;
        this.entities = provider;
        this.validator = validator;
        this.transactionPolicy = transactionPolicy;
    }

    /**
     * Tworzy obiekt imports
     *
     * @param provider  - dostawca warto�ci do importu
     * @param validator - obiekt sprawdzaj�cy poprawno��
     * @param consumer  - konsument wprowadzanych warto�ci
     */
    public KeyValueImport(Iterable<KeyValueEntity> provider,
                          Predicate<KeyValueEntity> validator,
                          Function<KeyValueEntity, ImportEntryStatus> consumer) {
        this.consumer = consumer;
        this.entities = provider;
        this.validator = validator;
        this.transactionPolicy = TRANSACTION_PER_ENTRY;
    }

    public KeyValueImport(Iterable<KeyValueEntity> provider,
                          Function<KeyValueEntity, ImportEntryStatus> consumer) {
        this(provider, Predicates.<KeyValueEntity>alwaysTrue(), consumer);
    }

    /**
     * Uruchamia import - wymaga otwartego kontekstu i braku transakcji
     */
    public ImportResult call() throws Exception {
        switch(transactionPolicy){
            case TRANSACTION_PER_ENTRY:
                return transactionPerEntryImportCall();
            case TRANSACTION_FOR_ALL:
                return transactionForAllImportCall();
            case EXTERNAL_TRANSACTION:
                return externalTransactionImportCall();
            default:
                throw new RuntimeException("unknown enum");
        }
    }

    private ImportResult externalTransactionImportCall() throws EdmException {
        ImportResult importResult = new ImportResult();
        for (KeyValueEntity entity : entities) {
            ImportEntryStatus result;
            if (entity.isValid() && validator.apply(entity)) {
                result = consumer.apply(entity);
            } else {
                result = ImportEntryStatus.INVALID_INPUT;
            }
            importResult.report(result);
        }
        return importResult;
    }

    private ImportResult transactionForAllImportCall() throws EdmException {
        ImportResult importResult = new ImportResult();
        importBlock:{
            DSApi.context().begin();
            for (KeyValueEntity entity : entities) {
                if(!entity.isValid()) {
                    importResult.reportError(entity.getErrorMessage());
                } else try {
                    ImportEntryStatus result;
                    if (validator.apply(entity)) {
                        result = consumer.apply(entity);
                    } else {
                        result = ImportEntryStatus.INVALID_INPUT;
                    }
                    importResult.report(result);
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                    importResult.reportError(ex);
                    DSApi.context().rollback();
                    break importBlock; //ko�czymy import przy pierwszym napotkanym b��dzie
                }
            }
            DSApi.context().commit();
        }
        return importResult;
    }

    private ImportResult transactionPerEntryImportCall() throws EdmException {
        int cleanCounter = 0;
        ImportResult importResult = new ImportResult();
        for (KeyValueEntity entity : entities) {
            if(!entity.isValid()) {
                importResult.reportError(entity.getErrorMessage());
            } else try {
                ImportEntryStatus result;
                if (validator.apply(entity)) {
                    DSApi.context().begin();
                    result = consumer.apply(entity);
                    DSApi.context().commit();
                } else {
                    result = ImportEntryStatus.INVALID_INPUT;
                }
                importResult.report(result);
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
                importResult.reportError(ex);
                DSApi.context().rollback();
            }
            ++ cleanCounter;
            if(cleanCounter >= 100){
                cleanCounter = 0;
                DSApi.context().session().flush();
                DSApi.context().session().clear();
            }
        }
        return importResult;
    }
}