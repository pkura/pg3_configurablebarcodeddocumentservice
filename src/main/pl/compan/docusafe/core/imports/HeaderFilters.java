package pl.compan.docusafe.core.imports;

import com.google.common.base.*;

import java.util.*;

/**
 * Klasa do tworzenia filtr�w nag��wk�w
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class HeaderFilters {
    private HeaderFilters(){}

    public static HeaderFilterBuilder buildFilter(){
        return new HeaderFilterBuilder();
    }

    public static class HeaderFilterBuilder {
        HeaderFilterBuilder(){}

        HashSet<String> requiredValues = new HashSet<String>();
        HashSet<String> possibleValues = null;
        Map<String, String> replaceMap = new HashMap<String, String>();

        public HeaderFilterBuilder possibleKeys(String... keys){
            Preconditions.checkState(possibleValues == null, "Possible keys already set");
            possibleValues = new HashSet<String>(Arrays.asList(keys));
            return this;
        }

        public HeaderFilterBuilder requiredKeys(String... keys){
            Preconditions.checkState(requiredValues.isEmpty(), "Required keys already set");
            requiredValues = new HashSet<String>(Arrays.asList(keys));
            return this;
        }

        public HeaderFilterBuilder alias(String key, String... aliases){
            for(String alias:aliases){
                Preconditions.checkState(!replaceMap.containsKey(alias), "Alias %s already set", alias);
                replaceMap.put(alias, key);
            }
            return this;
        }

        public Function<String[], String[]> create(){
            Function<String[], String[]> ret = null;
            if(!requiredValues.isEmpty() || possibleValues != null){
                Validator validator = new Validator();
                if(possibleValues != null){
                    validator.possibleValues = Predicates.in(possibleValues);
                }
                validator.requiredValues = requiredValues;
                ret = validator;
            }
            if(!replaceMap.isEmpty()){
                Aliaser aliaser = new Aliaser();
                aliaser.replaceMap = replaceMap;
                if(ret != null){
                    ret = Functions.compose(ret, aliaser);
                }
            }

            return ret;
        }
    }

    private static class Validator implements Function<String[], String[]>{
        Predicate<String> possibleValues = Predicates.alwaysTrue();
        Set<String> requiredValues;

        public String[] apply(String[] from) {
            Set<String> restRequired = new HashSet<String>(requiredValues);
            for(int i = 0; i < from.length; ++i){
                from[i] = from[i].trim();
                if(!possibleValues.apply(from[i])){
                    throw new IllegalArgumentException("Nieznana warto��: '" + from[i] + "'");
                }
                restRequired.remove(from[i]);
            }
            if(!restRequired.isEmpty()){
                throw new IllegalArgumentException("Brak wymaganej kolumny:" + restRequired.toString() + " w�r�d  " + Arrays.asList(from));
            }
            return from;
        }
    }

    private static class Aliaser implements Function<String[], String[]>{
        Map<String, String> replaceMap;

        public String[] apply(String[] from) {
            String[] ret = new String[from.length];
            for(int i = 0; i < ret.length; ++ i){
                if(replaceMap.containsKey(from[i])){
                    ret[i] = replaceMap.get(from[i]);
                } else {
                    ret[i] = from[i];
                }
            }
            return ret;
        }
    }
}
