package pl.compan.docusafe.core.imports;

import pl.compan.docusafe.core.office.workflow.jbpm.ProcessDefinition;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.EdmHibernateException;

import java.util.List;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;

import org.hibernate.HibernateException;
import org.hibernate.Criteria;
/* User: Administrator, Date: 2007-01-29 15:42:25 */

/**
 * Przechowuje informacje o pliku, zawierajacym dane o dokumentach do zaimportowania. Na podstawie statusu mo�na
 * sie dowiedziec czy proces importowania nie zostal rozpoczety, jest w trakcie przetwarzania czy juz zostal juz
 * zakonczony.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class ImportedFileInfo
{
    private Long id;
    private Integer importType;
    private Date ctime;
    private String creatingUser;
    private String fileName;
    private String tiffDirectory;
    private Integer status;
    /**
     * data skonczenia przetwarzania wszystkich dokumentow z tego pliku (czyli data, w ktorej
     * status zmieniono na STATUS_DONE)
     */
    private Date endTime;
    /**
     * Zbior informacji o dokumentach, ktore znajdowaly sie w tym pliku.
     */
    private Set<ImportedDocumentInfo> importedDocuments;

    public ImportedFileInfo()
    {

    }

    public synchronized void create() throws EdmException
    {
        if (ctime == null)
            ctime = new Date();
        if (creatingUser == null )
            creatingUser = DSApi.context().getPrincipalName();

        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static ImportedFileInfo find(Long id) throws EdmException
    {
        return Finder.find(ImportedFileInfo.class, id);
    }

    public static List<ImportedFileInfo> list() throws EdmException
    {
        return (List<ImportedFileInfo>) Finder.list(ImportedFileInfo.class, "ctime desc");
    }

    public static List<ImportedFileInfo> findByStatus(Integer status) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(ImportedFileInfo.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.eq("status", status));

            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getImportType()
    {
        return importType;
    }
    
    public String getDocumentKind()
    {
    	return ImportManager.getDockindCn(this.getImportType());
    }

    public void setImportType(Integer importType)
    {
        this.importType = importType;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getTiffDirectory()
    {
        return tiffDirectory;
    }

    public void setTiffDirectory(String tiffDirectory)
    {
        this.tiffDirectory = tiffDirectory;
    }

    public Integer getStatus()
    {
        return status;
    }

    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Set<ImportedDocumentInfo> getImportedDocuments()
    {
        if (importedDocuments == null)
            importedDocuments = new HashSet<ImportedDocumentInfo>();
        return importedDocuments;
    }

    public void setImportedDocuments(Set<ImportedDocumentInfo> importedDocuments)
    {
        this.importedDocuments = importedDocuments;
    }

    public Date getEndTime()
    {
        return endTime;
    }

    public void setEndTime(Date endTime)
    {
        this.endTime = endTime;
    }
}
