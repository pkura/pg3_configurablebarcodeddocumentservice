package pl.compan.docusafe.core;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/* User: Administrator, Date: 2005-06-27 09:30:05 */

/**
 * Klasa s�u��ca do budowania zapyta� na podstawie formularzy WWW
 * wype�nianych przez u�ytkownika.  Wspieranych jest kilka rodzaj�w
 * warunk�w, jakie mo�na przedstawi� przy pomocy formularza (r�wno��,
 * prefiks napisu, wi�kszo��, mniejszo��).  Mo�liwe jest te� dodanie
 * podwarunku typu OR (podwarunek typu AND nie jest potrzebny, poniewa�
 * g��wne zapytanie u�ywa AND do ��czenia swoich kryteri�w).  Warunki
 * AND zagnie�d�one w OR nie s� mo�liwe.
 * <p>
 * Klienci klasy nie powinni bezpo�rednio u�ywa� metod tworz�cych wyra�enia
 * (np. addLike()), ale powinni mie� oddane do u�ytku specjalizowane metody
 * implementowane przez klasy dziedzicz�ce (np. summary(String)).
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: FormQuery.java,v 1.16 2010/02/16 13:00:02 wkuty Exp $
 */
public class FormQuery
{
    private static final Logger log = LoggerFactory.getLogger(FormQuery.class);
    private List<Object> expressions = new LinkedList<Object>();
    private int offset;
    private int limit;
    private List<OrderBy> orderFields = new LinkedList<OrderBy>();

    public FormQuery()
    {
    }

    public FormQuery(int offset, int limit)
    {
        this.offset = offset;
        this.limit = limit;
    }

    public static class OrderBy
    {
        private String attribute;
        private boolean ascending;

        public OrderBy(String attribute, boolean ascending)
        {
            this.attribute = attribute;
            this.ascending = ascending;
        }

        public String getAttribute()
        {
            return attribute;
        }

        public boolean isAscending()
        {
            return ascending;
        }
    }

    /**
     * Klasa bazowa dla prostego warunku (np. r�wno��).
     */
    protected static class Expr implements Serializable
    {
        private String attribute;

        public Expr()
        {
        }

        public Expr(String attribute)
        {
            this.attribute = attribute;
        }

        public String getAttribute()
        {
            return attribute;
        }
    }

    public static class Eq extends Expr
    {
        private Object value;

        public Eq(String attribute, Object value)
        {
            super(attribute);
            this.value = value;
        }

        public Object getValue()
        {
            return value;
        }
    }

    private static class Contains extends Expr{
        private final String textQuery;
        public Contains(String column, String textQuery) {
            super(column);
            this.textQuery = textQuery;
        }
        public String getTextQuery() {
            return textQuery;
        }
    }
    
    public static class In extends Expr
    {
        private Object[] value;

        public In(String attribute, Object[] value)
        {
            super(attribute);
            this.value = value;
        }

        public Object[] getValue()
        {
            return value;
        }
    }
    
    public static class NotIn extends Expr
    {
        private Object[] value;

        public NotIn(String attribute, Object[] value)
        {
            super(attribute);
            this.value = value;
        }

        public Object[] getValue()
        {
            return value;
        }
    }

    public static class Like extends Expr
    {
        private String value;
        /**
         * Ignore case - ten zapis obniza wydajnosc - ignoreCase nalezy robic na poziomie bazy
         * @deprecated
         */
        @Deprecated
        private boolean ignoreCase;
        public Like(String attribute, String value)
        {
			this(attribute, value, true);
//            this.value = value;
//            this.ignoreCase = true;
        }
        
        public Like(String attribute, String value, boolean ignoreCase)
        {
            super(attribute);
            this.value = value;
            this.ignoreCase = ignoreCase;
			if(ignoreCase){
				log.debug("!UPPER!" + attribute + " , " + value, new Throwable());
			}
        }

        public String getValue()
        {
            return value;
        }

		public boolean isIgnoreCase() {
			return ignoreCase;
		}
        
    }

    public static class Ge extends Expr
    {
        private Object value;

        public Ge(String attribute, Object value)
        {
            super(attribute);
            this.value = value;
        }

        public Object getValue()
        {
            return value;
        }
    }

    public static class Le extends Expr
    {
        private Object value;

        public Le(String attribute, Object value)
        {
            super(attribute);
            this.value = value;
        }

        public Object getValue()
        {
            return value;
        }
    }

    protected static class Disjunction
    {
        private FormQuery query;

        public Disjunction(FormQuery query)
        {
            this.query = query;
        }

        public FormQuery getQuery()
        {
            return query;
        }
    }
    
    /**
     * Dodaje warunek LIKE.  Obecno�� znaku '%' w value nie jest
     * sprawdzana
     * @param attribute Nazwa por�wnywanego atrybutu
     * @param value String po LIKE, jego warto�� nie b�dzie zmieniana w �aden spos�b
     */
    public FormQuery addLike(String attribute, String value)
    {
        if (attribute == null)
            throw new NullPointerException("attribute");
        if (value == null)
            throw new NullPointerException("value");
        expressions.add(new Like(attribute, value));
        return this;
    }
    
    public FormQuery addLike(String attribute, String value, boolean ignoreCase)
    {
        if (attribute == null)
            throw new NullPointerException("attribute");
        if (value == null)
            throw new NullPointerException("value");
        expressions.add(new Like(attribute, value, ignoreCase));
        return this;
    }

    public FormQuery addEq(String attribute, Object value)
    {
        if (attribute == null)
            throw new NullPointerException("attribute");
        expressions.add(new Eq(attribute, value));        
        return this;
    }


    
    public FormQuery addIn(String attribute, Object[] value)
    {
        if (attribute == null)
            throw new NullPointerException("attribute");
        expressions.add(new In(attribute, value));
        return this;
    }
    
    public FormQuery addNotIn(String attribute, Object[] value)
    {
        if (attribute == null)
            throw new NullPointerException("attribute");
        expressions.add(new NotIn(attribute, value));
        return this;
    }

    public FormQuery addGe(String attribute, Object value)
    {
        if (attribute == null)
            throw new NullPointerException("attribute");
        if (value == null)
            throw new NullPointerException("value");
        expressions.add(new Ge(attribute, value));
        return this;
    }

    public FormQuery addLe(String attribute, Object value)
    {
        if (attribute == null)
            throw new NullPointerException("attribute");
        if (value == null)
            throw new NullPointerException("value");
        expressions.add(new Le(attribute, value));
        return this;
    }

	/**
	 * Uwaga: Nale�y przekaza� nowy obiekt FormQuery do addDisjunction
	 * z samymi ograniczeniami dla Disjunction
	 *
	 * @param query
	 */
    protected void addDisjunction(FormQuery query)
    {
        if (query == null)
            throw new NullPointerException("query");
        Disjunction disjunction = new Disjunction(query);
        expressions.add(disjunction);
    } 

    public int getOffset()
    {
        return offset;
    }

    public int getLimit()
    {
        return limit;
    }

    public void orderAsc(String attribute) throws EdmException
    {
//        if (attribute == null)
//            throw new NullPointerException("attribute");
        if (validSortAttribute(attribute))
            orderFields.add(new OrderBy(attribute, true));
    }

    public void orderDesc(String attribute) throws EdmException
    {
//        if (attribute == null)
//            throw new NullPointerException("attribute");
        if (validSortAttribute(attribute))
            orderFields.add(new OrderBy(attribute, false));
    }

    protected boolean validSortAttribute(String attribute)
    {
        String[] sortAttributes = sortAttributes();
        if (sortAttributes == null)
            return true;
        for (int i=0; i < sortAttributes.length; i++)
        {
            if (attribute.equals(sortAttributes[i]))
                return true;
        }
        return false;
    }

    protected String[] sortAttributes()
    {
        return null;
    }

    public OrderBy[] getOrder()
    {
        if (orderFields == null || orderFields.size() == 0)
            return new OrderBy[0];
        return (OrderBy[]) orderFields.toArray(new OrderBy[orderFields.size()]);
    }

    public void visitQuery(QueryVisitor visitor) throws EdmException
    {
		for (int i=0; i < expressions.size(); i++)
        {
            Object object = expressions.get(i);
            if (object instanceof Expr)
            {
                if (object instanceof Eq)
                {
                    visitor.visitEq((Eq) object);
                }
                else if (object instanceof In)
                {
                    visitor.visitIn((In) object);
                }
                else if (object instanceof NotIn)
                {
                	visitor.visitNotIn((NotIn) object);
                }
                else if (object instanceof Like)
                {
                    visitor.visitLike((Like) object);
                }
                else if (object instanceof Ge)
                {
                    visitor.visitGe((Ge) object);
                }
                else if (object instanceof Le)
                {
                    visitor.visitLe((Le) object);
                }
                else
                {
                    throw new IllegalArgumentException("Nieznana klasa Expr: "+
                        (object != null ? object.getClass().getName() : "<null>"));
                }
            }
            else if (object instanceof Disjunction)
            {
                visitor.startDisjunction();

                ((Disjunction) object).getQuery().visitQuery(visitor);

                visitor.endDisjunction();
            }
        }
    }


    /**
     * Interfejs klasy typu Visitor, kt�rej metody b�d� wywo�ywane
     * dla ka�dego warunku dodanego do zapytania.  Zaleca si� dziedziczenie
     * po klasie QueryVisitorAdapter, poniewa� w interfejsie QueryVisitor
     * mog� si� w przysz�o�ci pojawi� nowe metody.
     */
    public interface QueryVisitor
    {
        void visitEq(Eq expr) throws EdmException;
        void visitIn(In expr) throws EdmException;
        void visitNotIn(NotIn expr) throws EdmException;
        void visitLike(Like expr) throws EdmException;
        void visitGe(Ge expr) throws EdmException;
        void visitLe(Le expr) throws EdmException;
        void startDisjunction() throws EdmException;
        void endDisjunction() throws EdmException;
    }

    public static class QueryVisitorAdapter implements QueryVisitor
    {
        public void visitEq(Eq expr) throws EdmException
        {
        }

        public void visitLike(Like expr) throws EdmException
        {
        }

        public void visitGe(Ge expr) throws EdmException
        {
        }

        public void visitLe(Le expr) throws EdmException
        {
        }

        public void startDisjunction() throws EdmException
        {
		}

        public void endDisjunction() throws EdmException
        {
		}

		public void visitIn(In expr) throws EdmException 
		{			
		}
		
		public void visitNotIn(NotIn expr) throws EdmException
		{
		}
    }

    public String toString()
    {
        return getClass().getName()+"[offset="+offset+" limit="+limit+
            " expressions="+expressions+" orderFields="+orderFields+"]";
    }

	public List<Object> getExpressions() {
		return expressions;
	}

	public void setExpressions(List<Object> expressions) {
		this.expressions = expressions;
	}

    
}
