package pl.compan.docusafe.logic;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.ImageUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.tiff.Tiff;

import com.asprise.util.tiff.TIFFWriter;

/**
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class AttachmentsManager
{
	private static Logger log = LoggerFactory.getLogger(AttachmentsManager.class);
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(AttachmentsManager.class.getPackage().getName(),null);
    /**
     * Sprawdza czy aktualnie zalogowany u�ytkownik mo�e dokona� podzia�u danego za��cznika (najnowszej
     * jego wersji) na mniejsze za��czniki.
     */
    public static boolean canSplit(Attachment attachment) throws EdmException
    {
        return (DSApi.context().hasPermission(DSPermission.PISMO_DZIELENIE_ZALACZNIKA) &&                 
                (attachment.getMostRecentRevision() != null) && (attachment.getMostRecentRevision().getMime().indexOf("image/tiff") >= 0));
    }
    
    /**
     * Dzielenie za��cznika w formacie TIFF na pliki w formacie PNG zawieraj�ce pojedyncze strony.
     * Oryginalny za��cznik nie jest usuwany, a utworzone pliki s� dodawane do tego samego dokumentu
     * w postaci nowych za��cznik�w.
     * 
     * @param attachment
     * @throws EdmException
     */
    public static void splitAttachment(Attachment attachment) throws EdmException
    {
        if (!DSApi.context().hasPermission(DSPermission.PISMO_DZIELENIE_ZALACZNIKA))
            throw new EdmException(sm.getString("NiePosiadaszUprawnienDoDzieleniaZalacznikow"));
        if (attachment.getMostRecentRevision() == null)
            throw new EdmException(sm.getString("WybranyZalacznikNieZawieraZadnejWersji"));
        if (!(attachment.getMostRecentRevision().getMime().indexOf("image/tiff") >= 0))
            throw new EdmException(sm.getString("MoznaDzielicTylkoZalacznikiWformacieTIFF"));
        
        // pobranie pliku do podzia�u
        File file;
        try
        {                                   
            file = attachment.getMostRecentRevision().saveToTempFile();
        }
        catch (IOException e)
        {
            throw new EdmException(sm.getString("BladPodczasPobieraniaZalacznikaDoPodzialu"), e);
        }
        
        // dzielenia TIFF-a na za��czniki z pojedynczymi stronami w formacie PNG
        File[] files = ImageUtils.splitTIFFToPNG(file);  
        
        // dodawanie utworzonych plik�w jako za��czniki do dokumentu
        for (int i=0; files != null && i < files.length; i++)
        {
            Attachment newAttachment = new Attachment(sm.getString("PodzielonyZalacznik")+" "+(i+1));
            attachment.getDocument().createAttachment(newAttachment);
            String filename = files[i].getName();
            String ext;
            if (filename.indexOf('.') > 0)
                ext = filename.substring(filename.indexOf('.')+1);
            else
                ext = "png";

            newAttachment.createRevision(files[i]).setOriginalFilename(attachment.getMostRecentRevision().getOriginalFilename()+"_"+(i+1)+"."+ext);
        }
        
    }
    
    /**
     * ��czy pliki odpowiadaj�ce podanym wersjom za��cznikami w jeden plik w formacie TIFF.
     * Zak�adane jest, �e przekazane pliki s� w akceptowanym formacie graficznym (png,bmp,gif,jpg,tiff).
     * 
     * @param revisionIds   identyfikatory wersji za��cznik�w, kt�re po��czy� w jeden plik
     * @param deleteSources   true <=> za��czniki zawieraj�ce podane wersje za��cznik�w zostan� usuni�te     
     */
    public static File concatenateRevisions(Long[] revisionIds, boolean deleteSources) throws EdmException
    {
        try
        {
            Tiff tiff = new Tiff();
    
            for (int i=0; i < revisionIds.length; i++)
            {
                AttachmentRevision revision = AttachmentRevision.find(revisionIds[i]);
                File tmp = File.createTempFile("docusafe_", ".tmp");
                    //"."+(revision.getMime().substring(revision.getMime().indexOf('/')+1)));
                revision.saveToFile(tmp);
             //   if (event.getLog().isDebugEnabled())
             //       event.getLog().debug("utworzono plik "+tmp+" typu "+
             //           revision.getMime());
                tiff.add(tmp, revision.getMime());
    
                if (deleteSources && revision.getAttachment().isDocumentType())
                {
                    revision.getAttachment().getDocument().scrubAttachment(
                        revision.getAttachment().getId());
                }            
            }
    
            File output = tiff.write(
                TIFFWriter.TIFF_CONVERSION_TO_BLACK_WHITE,
                TIFFWriter.TIFF_COMPRESSION_GROUP4);
            
            return output;
        }
        catch (IOException e)
        {
            throw new EdmException(sm.getString("BladPodczasKonwersjiZalacznikowGraficznychDoPlikuTIFF"), e);
        }
    }
    
    /**
     * Sprawdza czy w zalacznikach dokumentu znajduje sie jakis obraz akceptowany przez przegladarke docusafe.
     * @param documentId id dokumentu
     * @return true jezeli posiada
     * @throws SQLException 
     */
	public static boolean isAnyImageInAttachments(long documentId) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try	{
			ps = DSApi.context().prepareStatement(
                    "SELECT 1 FROM DS_ATTACHMENT_REVISION rev, DS_ATTACHMENT atta "
                         + " WHERE atta.DOCUMENT_ID = ?"
                         + " AND rev.ATTACHMENT_ID = atta.ID "
                         + " AND atta.DELETED = ? "
                         + " AND rev.MIME like 'image/%'");
			ps.setLong(1, documentId);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			return rs.next();
    	} catch (Exception ex) {
    		log.error(ex.getMessage(),ex);
		} finally {
            DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps); 		
    	}
    	return false;
    }
	/**
     * Sprawdza czy w zalacznikach dokumentu znajduje sie jakis pdf akceptowany przez przegladarke docusafe.
     * @param documentId id dokumentu
     * @return true jezeli posiada
     * @throws SQLException 
     */
	public static boolean isAnyPdfInAttachments(long documentId) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DSApi.context().prepareStatement("SELECT 1 FROM DS_ATTACHMENT_REVISION rev, DS_ATTACHMENT atta"
					+ " WHERE atta.DOCUMENT_ID = ? "
                    + " AND rev.ATTACHMENT_ID = atta.ID "
                    + " AND atta.DELETED = ? "
                    + " AND rev.MIME = 'application/pdf'");
			ps.setLong(1, documentId);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			return rs.next();
    	} catch (Exception ex) {
    		log.error(ex.getMessage(), ex);
		} finally {
            DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
    	}
    	return false;
    }
}
