package pl.compan.docusafe.logic;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowManager;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.ImageUtils;
import pl.compan.docusafe.util.StringManager;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;

/**
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class DocumentsManager
{
    /**
     * Tworzy nowe pismo kancelaryjne na podstawie pisma 'document'. Jedynym za��cznikiem nowego pisma
     * b�dzie plik uzyskany z po��czenia najnowszych wersji ze zbioru za��cznik�w o identyfikatorach
     * z tablicy 'attachmentIds'. 
     * Dla nowego dokumentu jest tworzony nowy proces (zadanie trafia zawsze do u�ytkownika wykonuj�cego t� 
     * operacje - koordynatorzy s� ignorowani) i nadawany nowy numer KO (opr�cz pisma wychodz�cego).
     * 
     * @param document
     * @param attachmentIds
     */
    public static OfficeDocument createDocument(OfficeDocument document, Long[] attachmentIds) throws EdmException
    {
    	final StringManager sm = 
            GlobalPreferences.loadPropertiesFile(DocumentsManager.class.getPackage().getName(),null);
        Long[] revisionIds = new Long[attachmentIds.length];
        int count = 0;
        String tmp = null;
        StringBuilder message = new StringBuilder();//attachmentIds.length == 1 ? "Z za��cznika" : "Z za��cznik�w ");

        for (Long id : attachmentIds)
        {
            Attachment attachment = Attachment.find(id);
            if (attachment.getMostRecentRevision() == null || !ImageUtils.canConvertToTiff(attachment.getMostRecentRevision().getMime()))
                throw new EdmException(sm.getString("WybranoConajmniejJedenZalacznikKtoryNieJestPlikiemGraficznymWceluWygenerowania"));
            revisionIds[count] = attachment.getMostRecentRevision().getId();
            count++;
            message.append(count > 1 ? ", " : "");
            message.append("'"+attachment.getTitle()+"'");
        }
        //message.append(" utworzono nowy dokument o ");
        File file = null;
        try {
        	file = AttachmentsManager.concatenateRevisions(revisionIds, false);
        }
        catch (Exception e) {
        	throw new EdmException("Niew�a�ciwy format pliku. Prosz� ponownie zeskanowa� za��cznik.");
        }
         
        OfficeDocument newDoc = createDocument(document, file, "skan.tiff");
        document = OfficeDocument.findOfficeDocument(document.getId());
        
        if (attachmentIds.length == 1)
        {
            if (newDoc.getOfficeNumber() != null)
                tmp = sm.getString("ZzalacznikaUtworzonoNowyDokumentOnumerzeKO",message.toString(),newDoc.getOfficeNumber());
            else
                tmp = sm.getString("ZzalacznikaUtworzonoNowyDokumentOnumerzeID",message.toString(),newDoc.getId());
        }
        else
        {
            if (newDoc.getOfficeNumber() != null)
                tmp = sm.getString("ZzalacznikowUtworzonoNowyDokumentOnumerzeKO",message.toString(),newDoc.getOfficeNumber());
            else
                tmp = sm.getString("ZzalacznikowUtworzonoNowyDokumentOnumerzeID",message.toString(),newDoc.getId());
        }
        
       // message.append(newDoc.getOfficeNumber() != null ? " numerze KO "+newDoc.getOfficeNumber() : " ID "+newDoc.getId());        
        document.addWorkHistoryEntry(
                Audit.create("newDocument",
                    DSApi.context().getPrincipalName(),
                    tmp,
                    "CREATE_INTO", document.getId()));
        
        if (document.getOfficeNumber() != null)
            tmp = sm.getString("DokumentPowstalZdokumentuOnumerzeKO",document.getOfficeNumber());
        else
            tmp = sm.getString("DokumentPowstalZdokumentuOnumerzeID",document.getId());
        //message = new StringBuilder("Dokument powsta� z dokumentu o ");
       // message.append(document.getOfficeNumber() != null ? " numerze KO "+document.getOfficeNumber() : " ID "+document.getId());        
        
        newDoc.addWorkHistoryEntry(
                Audit.create("newDocument",
                        DSApi.context().getPrincipalName(),
                        tmp,
                        "CREATE_FROM", document.getId()));
        for (Long id : attachmentIds)
        {
            document.scrubAttachment(id);
        }
		
		if(document.getDocumentKind() != null){
			document.getDocumentKind().logic().archiveActions(newDoc, DocumentKindsManager.getType(newDoc));
		}
        return newDoc;
    }
    
    /**
     * Tworzy nowe pismo kancelaryjne zawieraj�ce dane takie same jak 'document' i posiadaj�cy jeden za��cznik 'file'.
     * Dla nowego dokumentu jest tworzony nowy proces - zadanie trafia do u�ytkownika wykonuj�cego t� operacje.
     * Jest to prawie klonowanie z tak� tylko r�nic�, �e nadawany jest nowy numer KO (opr�cz pisma wychodz�cego).
     * 
     * @param document
     * @param file   plik dla za��cznika
     * @param filename   oryginalna nazwa pliku dla za��cznika
     */
    private static OfficeDocument createDocument(OfficeDocument document, File file, String filename) throws EdmException
    {
    	final StringManager sm = 
            GlobalPreferences.loadPropertiesFile(DocumentsManager.class.getPackage().getName(),null);
        OfficeDocument newDocument = (OfficeDocument) document.cloneObject(new Long[]{});
        
        // na wypadek gdyby oryginalny dokument by� podpisany podpisem elektronicznym 
        newDocument.setBlocked(false);
        
        if (document.getContainingCase() != null)        
            newDocument.removeFromCase();        
        
        
        // dodawanie za��cznika
        Attachment attachment = new Attachment("Skan");
        newDocument.createAttachment(attachment);
        attachment.createRevision(file).setOriginalFilename(filename);
        
        // przenoszenie atrybut�w biznesowych
        DocumentKind documentKind = newDocument.getDocumentKind();
        if (documentKind != null)
        {            
            FieldsManager fm = documentKind.getFieldsManager(document.getId());
            Map<String,Object> values = fm.getFieldValues();
            documentKind.set(newDocument.getId(), values);
        }           
        
        // przydzielanie nowego numeru z dziennika i zmieniam tw�rc�/date przyj�cia
        Long newDocumentId = newDocument.getId();
        newDocument.setAuthor(DSApi.context().getPrincipalName());
        newDocument.setCtime(new Date());
        if (newDocument.getType() == DocumentType.INCOMING)
        {
            ((InOfficeDocument) newDocument).setCreatingUser(DSApi.context().getPrincipalName());
            
            // ustawiam incomingDate na aktualn� dat� - w identyczny spos�b jak w przyj�ciu pisma
            Calendar currentDay = Calendar.getInstance();
            currentDay.setTime(GlobalPreferences.getCurrentDay());

            Date breakOfDay = DateUtils.endOfDay(currentDay.getTime());

            // je�eli faktyczna data jest p�niejsza ni� otwarty
            // dzie�, pozostawiam dat� i ustawiam godzin� 23:59:59.999
            if (new Date().after(breakOfDay))
            {
                currentDay.setTime(breakOfDay);
            }
            else
            {
                Calendar now = Calendar.getInstance();
                currentDay.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
                currentDay.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
                currentDay.set(Calendar.SECOND, now.get(Calendar.SECOND));
            }
            ((InOfficeDocument) newDocument).setIncomingDate(currentDay.getTime());
            bindToIncomingJournal(newDocumentId);
        }
        else if (newDocument.getType() == DocumentType.OUTGOING)
        {
            ((OutOfficeDocument) newDocument).setCreatingUser(DSApi.context().getPrincipalName());
            // dla wychodz�ych numeru KO nie przydzielamy od razu
            //bindToOutgoingJournal(newDocumentId);
            newDocument.setOfficeNumber(null);
            newDocument.setOfficeNumberYear(null);
        }
        else if (newDocument.getType() == DocumentType.INTERNAL)
        {
            ((OutOfficeDocument) newDocument).setCreatingUser(DSApi.context().getPrincipalName());
            bindToInternalJournal(newDocumentId);
        }
        else
            throw new EdmException(sm.getString("NapotkanoNieznanyTypDokumentuKancelaryjnego"));
        
        // tworzenie nowego zadania
        newDocument = OfficeDocument.findOfficeDocument(newDocumentId);
        WorkflowFactory.createNewProcess(newDocument, false);  
        
        return newDocument;
    }
    
    /**
     * Przypisanie pisma przychodz�cego do g��wnego dziennika pism przychodz�cych.
     * @param documentId
     * @throws EdmException
     */
    private static void bindToIncomingJournal(Long documentId) throws EdmException
    {
        Date entryDate = GlobalPreferences.getCurrentDay();
        Long journalId = Journal.getMainIncoming().getId();
        Integer sequenceId = Journal.TX_newEntry2(journalId, documentId, entryDate);
        InOfficeDocument document = InOfficeDocument.findInOfficeDocument(documentId);
        document.bindToJournal(journalId, sequenceId);
    }
    
    /**
     * Przypisanie pisma wewn�trznego do g��wnego dziennika pism wewn�trznych.
     * @param documentId
     * @throws EdmException
     */
    private static void bindToInternalJournal(Long documentId) throws EdmException
    {
        Date entryDate = GlobalPreferences.getCurrentDay();
        Long journalId = Journal.getMainInternal().getId();
        
//        DSApi.context().commit();
//        DSApi.close();

        Integer sequenceId = Journal.TX_newEntry2(journalId, documentId, entryDate);
        
//        DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
//        DSApi.context().begin();
        
        OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(documentId);
        
        document.bindToJournal(journalId, sequenceId);
    }
    
    /**
     * Przypisanie pisma wychodz�cego do g��wnego dziennika pism wychodz�cych.
     * @param documentId
     * @throws EdmException
     */
    private static void bindToOutgoingJournal(Long documentId) throws EdmException
    {
        Date entryDate = GlobalPreferences.getCurrentDay();
        Long journalId = Journal.getMainOutgoing().getId();
        
        DSApi.context().commit();
        DSApi.close(); 

        Integer sequenceId = Journal.TX_newEntry2(journalId, documentId, entryDate);
        
        DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
        DSApi.context().begin();        
        
        OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(documentId);
        
        document.bindToJournal(journalId, sequenceId);
    }
}
