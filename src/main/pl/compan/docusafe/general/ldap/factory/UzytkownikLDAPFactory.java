package pl.compan.docusafe.general.ldap.factory;

import pl.compan.docusafe.boot.Docusafe;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class UzytkownikLDAPFactory {
	
	private LDAPConnectionFactory ldapFactory = null;

	private LDAPConnectionFactory getLDAPFactory() {
		if (ldapFactory == null) {
			ldapFactory = new LDAPConnectionFactory();
		}
		return ldapFactory;
	}
	
	
	/**
	 * Metoda zwraca użytkownika wyszukanego po identyfikatorze
	 * 
	 * @return
	 */
	public SearchResult getUzytkownikaPoIdentyfikatorze(String filtr) {
		try {
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            String addsFilter = Docusafe.getAdditionPropertyOrDefault("general.ldap.uzytkownik.filter","(objectclass=top)");
            NamingEnumeration<SearchResult> renum = getLDAPFactory().getContext().search("",
					"(&(uidnumber=" + filtr + ")"+ addsFilter +")", controls);

			while (renum.hasMoreElements()) {
				SearchResult element = (SearchResult) renum.nextElement();
				return element;
			}
			return null;

		} catch (NamingException e) {
			return null;
		}
	}

	
}
