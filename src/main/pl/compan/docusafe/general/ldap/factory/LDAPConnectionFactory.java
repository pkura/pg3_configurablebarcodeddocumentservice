package pl.compan.docusafe.general.ldap.factory;

import javax.naming.directory.DirContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.users.ad.ActiveDirectoryManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class LDAPConnectionFactory {

	public static final Logger log = LoggerFactory.getLogger(LDAPConnectionFactory.class);

	private ActiveDirectoryManager activeDirectoryManager = null;
	private String activeDirectoryUrl = "";

	

	/**
	 * Zwraca obiekt ActiveDirectoryManager do LDAP. Do poprawnego działania
	 * niezbędne jest ustawienie w pliku adds.properties, zmiennej do połączenia
	 * z LDAP: active.directory.url.
	 * 
	 * @return
	 */
	public ActiveDirectoryManager getActiveDirectoryManagerContext() {

		if (Docusafe.getAdditionProperty("active.directory.url") != null) {
			activeDirectoryUrl = Docusafe
					.getAdditionProperty("active.directory.url");
		} else {
			log.error("Nie ustawiono zmiennej w adds.properties: active.directory.url.");
			return null;
		}

		activeDirectoryManager = new ActiveDirectoryManager(activeDirectoryUrl);

		return activeDirectoryManager;
	}

	/**
	 * Pobranie kontekstu otwartego jako anonymous.
	 * 
	 * @return
	 */
	public DirContext getContext() {
		try {
			DirContext context = getActiveDirectoryManagerContext().LdapConnectAnnonymous();
			return context;
		} catch (Exception e) {
			return null;
		}

	}
}
