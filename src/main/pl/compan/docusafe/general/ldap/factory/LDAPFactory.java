package pl.compan.docusafe.general.ldap.factory;

import java.util.ArrayList;
import java.util.List;

import javax.naming.directory.SearchResult;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.general.canonical.model.Pracownik;
import pl.compan.docusafe.general.mapers.LDAPToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class LDAPFactory {


    private static final Logger log = LoggerFactory.getLogger(LDAPFactory.class);
	/**
	 * Metoda dla podanej listy obiektow Pracownik odwoluje si� do LDAP.
	 * Tam na podstawie powi�zania LDAP<->WS(ERP) wstawia login z LDAP do obiektu.
	 * @param listaPracownikow
	 * @return
	 */
	public List<Pracownik> aktualizujLoginZLDAPDlaPracownikow(List<Pracownik> listaPracownikow)
	{
		List<Pracownik> listaWynikowa = new ArrayList<Pracownik>();
		
		UzytkownikLDAPFactory uzytkownikLDAPFactory = new UzytkownikLDAPFactory();
		LDAPToCanonicalMapper lDAPToCanonicalMapper = new LDAPToCanonicalMapper();
		
		if (Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_LDAP_IDENTYFIKATOR_ERP_NAZWA")==null)
		{
			log.error("Nie ustawiono w pliku adds.properties warto�ci IMPORT_PRACOWNIKOW_LDAP_IDENTYFIKATOR_ERP_NAZWA.");
		}
		else
		{		
            String prefixDomeny = Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_LDAP_IDENTYFIKATOR_ERP_NAZWA");

            // Aktualizacja praconwik�w o login z domeny
            for (Pracownik pracownik : listaPracownikow)
            {
                log.info("pracowownik {}", pracownik);
                if (pracownik.getOperatorDoPorownywania()!=null)
                {
                    SearchResult result = uzytkownikLDAPFactory.getUzytkownikaPoIdentyfikatorze(pracownik.getOperatorDoPorownywania());
                    log.info("result {}", result);
                    if (result!=null)
                    {
                        pracownik = lDAPToCanonicalMapper.MapResultToPracownik(result, pracownik);
                        // Pracownikowi dajemy status logowania domenowego
                        pracownik.setLogowanieDomenowe(true);

                        // Dodajemy do pracownika prefix domeny
                        pracownik.setPrefixDomeny(prefixDomeny);

                        // Domena mo�e zwraca� zamiast pustego loginu dan� typu "!U342342", kt�ra nie jest loginem - dlatego tych pracownik�w eliminujemy
                        if (!pracownik.getLogin().contains("!"))
                        {
                            listaWynikowa.add(pracownik);
                        }
                    }
                }
            }
		}
		return listaWynikowa;
	}
}
