package pl.compan.docusafe.general.canonical.factory;

import java.util.List;

import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.dto.JednostkaOrganizacyjnaDTO;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class JednostkaOrganizacyjnaFactory {

	public static final Logger log = LoggerFactory.getLogger(JednostkaOrganizacyjnaFactory.class);
		
	
	/**
	 * Metoda zwraca wczytan� z bazy danych list� jednostek organizacyjnych.
	 * @return
	 */
	public List<JednostkaOrganizacyjna> pobierzJednostkiOrganizacyjneZBD() {
		JednostkaOrganizacyjnaDTO jednostkaOrganizacyjnaDTO = new JednostkaOrganizacyjnaDTO();
		return jednostkaOrganizacyjnaDTO.pobierzJednostkiOrganizacyjneZBD_OC();		
	}
	
	/**
	 * Metoda zwraca list� JednostkaOrganizacyjna z WebSerwisu [WS]
	 * Metoda u�ywa dowolnego, skonfigurowanego webserwisu Simple DictionaryService - GetOrganizationalUnit
	 * @return
	 */
	public List<JednostkaOrganizacyjna>  PobierzJednostkiOrganizacyjneWS()
	{
		JednostkaOrganizacyjnaDTO jednostkaOrganizacyjnaDTO = new JednostkaOrganizacyjnaDTO();
		
		return jednostkaOrganizacyjnaDTO.PobierzJednostkiOrganizacyjneWS();		
	}
	
	/**
	 * Metoda zwraca JednostkaOrganizacyjna po idm z BD
	 * @param idm
	 * @return
	 */
	public JednostkaOrganizacyjna PobierzJednostkePoIdmDB(String idm)
	{
		JednostkaOrganizacyjnaDTO jednostkaOrganizacyjnaDTO = new JednostkaOrganizacyjnaDTO();
		
		return jednostkaOrganizacyjnaDTO.pobierzJednostkeOrganizacyjnaPoIdmDB(idm);
	}
}
