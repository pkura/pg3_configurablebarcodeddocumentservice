package pl.compan.docusafe.general.canonical.factory;

import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.TypyWnioskowORezerwacje;
import pl.compan.docusafe.general.dto.TypyWnioskowORezerwacjeDTO;
import pl.compan.docusafe.general.hibernate.dao.TypyWnioskowORezerwacjeDBDAO;
import pl.compan.docusafe.general.hibernate.model.TypyWnioskowORezerwacjeDB;

public class TypyWnioskowORezerwacjeFactory {

	/**
	 * Metoda pobiera liste typow wnioskow o rezerwacje z BD
	 * 
	 */
	public List<TypyWnioskowORezerwacje> pobierzTypyWnioskowORezerwacjeZBD() {
		
		TypyWnioskowORezerwacjeDTO typyWnioskowORezerwacje = new TypyWnioskowORezerwacjeDTO();
		
		return typyWnioskowORezerwacje.pobierzTypyWnioskowZBD();
	}
	/**
	 * Metoda pobiera liste typow wnioskow o rezerwacje z WS
	 */
	public List<TypyWnioskowORezerwacje> pobierzTypyWnioskowORezerwacjeZWS() {
		
		TypyWnioskowORezerwacjeDTO typyWnioskowORezerwacje = new TypyWnioskowORezerwacjeDTO();
		
		return typyWnioskowORezerwacje.pobierzTypyWnioskowZWS();
	}
	
	/**
	 * Metoda aktualizuje typy wnioskow o rezerwacje
	 * 
	 * @param TypyWnioskowORezerwacjeNowe
	 * @param TypyWnioskowORezerwacjeStare
	 */
	public List<TypyWnioskowORezerwacje> aktualizujTypyWnioskowORezerwacje(
			List<TypyWnioskowORezerwacje> TypyWnioskowORezerwacjeNowe, List<TypyWnioskowORezerwacje> TypyWnioskowORezerwacjeStare) {
		for (TypyWnioskowORezerwacje tw : TypyWnioskowORezerwacjeNowe) {
			try {
				List<TypyWnioskowORezerwacjeDB> listTwDB = TypyWnioskowORezerwacjeDBDAO.findByErpID(tw.getErpId());
				if (listTwDB.isEmpty()) {
					tw.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (TypyWnioskowORezerwacjeDB twDB : listTwDB) {
						tw.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						tw.setId(twDB.getId());
					}
				}
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		return TypyWnioskowORezerwacjeNowe;
	}
	
	/**
	 * Metoda zapisuje typy wnioskow o rezerwacje do bazy danych
	 * @param
	 */
	
	public void ZapiszTypyWnioskowORezerwacjeDoDB(List<TypyWnioskowORezerwacje> listaTypyWnioskowORezerwacje) {
		
		TypyWnioskowORezerwacjeDTO typyWnioskowORezerwacje = new TypyWnioskowORezerwacjeDTO();
		
		typyWnioskowORezerwacje.zapisz(listaTypyWnioskowORezerwacje);
	}
}
