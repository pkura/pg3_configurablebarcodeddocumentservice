package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.OkresBudzetowy;
import pl.compan.docusafe.general.dto.OkresBudzetowyDTO;
import pl.compan.docusafe.general.hibernate.dao.OkresBudzetowyDBDAO;
import pl.compan.docusafe.general.hibernate.model.OkresBudzetowyDB;

public class OkresBudzetowyFactory {
	/**
	 * Metoda pobiera liste komorek budzetowych z BD
	 * 
	 */
	public List<OkresBudzetowy> pobierzOkresBudzetowyZBD() {

		OkresBudzetowyDTO okresBudzetowy = new OkresBudzetowyDTO();

		return okresBudzetowy.pobierzOkresBudzetowyZBD();
	}
	/**
	 * Metoda pobiera liste komorek budzetowych z WS
	 * @throws RemoteException 
	 */
	public List<OkresBudzetowy> pobierzOkresBudzetowyZWS() throws RemoteException {

		OkresBudzetowyDTO okresBudzetowy = new OkresBudzetowyDTO();

		return okresBudzetowy.pobierzOkresBudzetowyZWS();
	}

	/**
	 * Metoda aktualizuje budzety komorek
	 * 
	 * @param okresBudzetowyNowe
	 * @param okresBudzetowyStare
	 */
	public List<OkresBudzetowy> aktualizujOkresBudzetowy(
			List<OkresBudzetowy> okresBudzetowyNowe, List<OkresBudzetowy> okresBudzetowyStare) {
		HashSet<Long> okresySet=new HashSet<Long>();
		for (OkresBudzetowy bk : okresBudzetowyNowe) {
			try {
				List<OkresBudzetowyDB> okresBudzetowy =OkresBudzetowyDBDAO.findByErpID(bk.getErpId());
				if (okresBudzetowy.isEmpty()) {
					bk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (OkresBudzetowyDB bkDB : okresBudzetowy) {
						bk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						bk.setId(bkDB.getId());
					}
				}
				bk.setAvailable(true);
				okresySet.add(bk.getErpId());
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		for (OkresBudzetowy bk : okresBudzetowyStare) {
			if(okresySet.contains(bk.getErpId()))continue;
			bk.setAvailable(false);
			bk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			okresBudzetowyNowe.add(bk);
		}
		return okresBudzetowyNowe;
	}

	/**
	 * Metoda zapisuje budzety komorek do bazy danych
	 * @param
	 */

	public void zapiszOkresyBudzetowyDoDB(List<OkresBudzetowy> listaOkresBudzetowy) {

		OkresBudzetowyDTO okresBudzetowy = new OkresBudzetowyDTO();

		okresBudzetowy.Zapisz(listaOkresBudzetowy);
	}
}
