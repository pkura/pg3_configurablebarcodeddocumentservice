package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.model.Kontrahent;
import pl.compan.docusafe.general.hibernate.model.KontrahentDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;

public class KontrahentDBFactory {

	public List<Kontrahent> pobierzKontrahentaZBD() {
		List<Kontrahent> listaKontrahent= new ArrayList<Kontrahent>();

		List<KontrahentDB> listaKontrahentDB;
		try {
			listaKontrahentDB = KontrahentDB.list();

			for (KontrahentDB kontrahentDB : listaKontrahentDB) {
				Kontrahent kontrahent = HibernateToCanonicalMapper
						.MapKontrahentDBToKontrahent(kontrahentDB);
				listaKontrahent.add(kontrahent);
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaKontrahent;
	}

	public boolean Zapisz(Kontrahent kontrahent)
	{

		KontrahentDB kontrahentDB = new KontrahentDB();
		try {
			DSApi.context().begin();
			switch(kontrahent.getStanObiektu())
			{
			case NOWY:
				kontrahentDB = CanonicalToHibernateMapper.MapKontrahentNaKontrahentDB(kontrahentDB, kontrahent);	
				kontrahentDB.save();
				break;			
			case ZMODYFIKOWANY:
				List<KontrahentDB> kontrahentIdm = KontrahentDB.findByNrKonta(kontrahent.getNrKonta());
				List<KontrahentDB> erpId = KontrahentDB.findByErpId(kontrahent.getErpId());
				if(!kontrahentIdm.isEmpty()){
					for(KontrahentDB kDB:kontrahentIdm){
						kontrahentDB = CanonicalToHibernateMapper.MapKontrahentNaKontrahentDB(kDB, kontrahent);
						kontrahentDB.save();	
					}
				} else if(!erpId.isEmpty()){
					for(KontrahentDB kk:erpId){
						kontrahentDB = CanonicalToHibernateMapper.MapKontrahentNaKontrahentDB(kk, kontrahent);
						kontrahentDB.save();
					}
				}
				break;
			}
			DSApi.context().commit();
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
}

