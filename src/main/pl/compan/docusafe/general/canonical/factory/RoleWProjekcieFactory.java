package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.RoleWProjekcie;
import pl.compan.docusafe.general.dto.RoleWProjekcieDTO;
import pl.compan.docusafe.general.hibernate.dao.RoleWProjekcieDBDAO;
import pl.compan.docusafe.general.hibernate.model.RoleWProjekcieDB;

public class RoleWProjekcieFactory {
	/**
	 * Metoda pobiera liste rol w projekcie z BD
	 * 
	 */
	public List<RoleWProjekcie> pobierzRoleWProjekcieZBD() {

		RoleWProjekcieDTO roleWProjekcie = new RoleWProjekcieDTO();

		return roleWProjekcie.pobierzRoleWProjekcieZBD();
	}
	/**
	 * Metoda pobiera liste rol w projekcie z WS
	 * @throws RemoteException 
	 */
	public List<RoleWProjekcie> pobierzRoleWProjekcieZWS() throws RemoteException {

		RoleWProjekcieDTO roleWProjekcie = new RoleWProjekcieDTO();

		return roleWProjekcie.pobierzRoleWProjekcieZWS();
	}

	/**
	 * Metoda aktualizuje role w projekcie
	 * 
	 * @param RoleWProjekcieNowe
	 * @param RoleWProjekcieStare
	 */
	public List<RoleWProjekcie> aktualizujRoleWProjekcie(
			List<RoleWProjekcie> RoleWProjekcieNowe, List<RoleWProjekcie> RoleWProjekcieStare) {
		HashSet<Double> rolSet=new HashSet<Double>();
		for (RoleWProjekcie r : RoleWProjekcieNowe) {
			try {
				List<RoleWProjekcieDB> roleWProjekcieDB = RoleWProjekcieDBDAO.findByRolaKontraktId(r.getBp_rola_kontrakt_id());
				if (roleWProjekcieDB.isEmpty()) {
					r.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (RoleWProjekcieDB rDB : roleWProjekcieDB) {
						r.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						r.setId(rDB.getId());
					}
				}
				r.setAvailable(true);
				rolSet.add(r.getBp_rola_kontrakt_id());
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		for(RoleWProjekcie r:RoleWProjekcieStare){
			if(rolSet.contains(r.getBp_rola_kontrakt_id()))continue;
			r.setAvailable(false);
			r.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			RoleWProjekcieNowe.add(r);
		}
		return RoleWProjekcieNowe;
	}

	/**
	 * Metoda zapisuje role w projekcie do bazy danych
	 * @param
	 */

	public void ZapiszRoleWProjekcieDoDB(List<RoleWProjekcie> listaRoleWProjekcie) {

		RoleWProjekcieDTO roleWProjekcie = new RoleWProjekcieDTO();

		roleWProjekcie.zapisz(listaRoleWProjekcie);
	}
}
