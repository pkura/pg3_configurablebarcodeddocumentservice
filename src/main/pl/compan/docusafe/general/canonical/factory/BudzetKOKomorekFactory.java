package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.BudzetKOKomorek;
import pl.compan.docusafe.general.dto.BudzetKOKomorekDTO;
import pl.compan.docusafe.general.hibernate.dao.BudzetKOKomorkaDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudzetKOKomorekDB;


public class BudzetKOKomorekFactory {
	/**
	 * Metoda pobiera liste budzetow z BD
	 * 
	 */
	public List<BudzetKOKomorek> pobierzBudzetyKoKomorekZBD() {
		
		BudzetKOKomorekDTO kontrahentKonto = new BudzetKOKomorekDTO();
		
		return kontrahentKonto.pobierzBudzetyZBD();
	}
	/**
	 * Metoda pobiera liste budzetow z WS
	 * @throws RemoteException 
	 */
	public List<BudzetKOKomorek> pobierzBudzetyKoKomorekZWS() throws RemoteException {
		
		BudzetKOKomorekDTO kontrahentKonto = new BudzetKOKomorekDTO();
		
		return kontrahentKonto.pobierzBudzetyKoKomorekZWS();
	}
	
	/**
	 * Metoda aktualizuje budzety
	 * 
	 * @param kontoNowe
	 * @param kontoStare
	 */
	public List<BudzetKOKomorek> aktualizujBudzetyKoKomorek(
			List<BudzetKOKomorek> kontoNowe, List<BudzetKOKomorek> kontoStare) {
		HashSet<Long> noweSet=new HashSet<Long>();
		for (BudzetKOKomorek kk : kontoNowe) {
			try {
				List<BudzetKOKomorekDB> konto = BudzetKOKomorkaDBDAO.findByErpId(kk.getErpId());
				if (konto.isEmpty()) {
					kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (BudzetKOKomorekDB kkDB : konto) {
						kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						kk.setId(kkDB.getId());
					}
				}
				noweSet.add(kk.getErpId());
				kk.setAvailable(true);
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		for(BudzetKOKomorek kk : kontoStare){
			if(noweSet.contains(kk.getErpId()))continue;
			kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			kk.setAvailable(false);
			kontoNowe.add(kk);
		}
		
		
		
		return kontoNowe;
	}
	
	/**
	 * Metoda zapisuje budzetow do bazy danych
	 * @param
	 */
	
	public void ZapiszBudzetyKoKomorekDoDB(List<BudzetKOKomorek> listaBudzetowKoKomorek) {
		
		BudzetKOKomorekDTO kontrahentKonto = new BudzetKOKomorekDTO();
		
		kontrahentKonto.zapisz(listaBudzetowKoKomorek);
	}
}
