package pl.compan.docusafe.general.canonical.factory;

import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.ProduktyFaktury;
import pl.compan.docusafe.general.hibernate.model.CostInvoiceProductDB;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;

public class ProduktyFakturyFactory {
	public List<ProduktyFaktury> polaczProduktyFaktury(List<ProduktyFaktury> nowa, List<ProduktyFaktury> stara){
		HashSet<Long> produktSet=new HashSet<Long>();
		for(ProduktyFaktury pf:nowa){
			try {
				List<CostInvoiceProductDB> costInvoiceProduct = CostInvoiceProductDB.findByERPID(pf.getWytworId());
				if(costInvoiceProduct.isEmpty()){
					pf.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					pf.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
				}
				//rozliczenie za media jest nie widoczne, uzupelniane automatycznie w przypadku faktury rozliczajacej media
				if(StringUtils.startsWith(pf.getCn(),InvoiceGeneralConstans.MEDIA))
					pf.setAktywna(false);
				else pf.setAktywna(true);
				produktSet.add(pf.getWytworId());
			} catch (EdmException e) {
				e.printStackTrace();
			}
		}
		for(ProduktyFaktury pf:stara){
			if(produktSet.contains(pf.getWytworId()))continue;
			pf.setAktywna(false);
			pf.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			nowa.add(pf);
			
		}
		
		return nowa;
	}
}
