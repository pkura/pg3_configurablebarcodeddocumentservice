package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.general.canonical.model.ProduktyFaktury;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.CostInvoiceProduct;

public class ProduktyFakturyWSFactory {
	public List<ProduktyFaktury>  PobierzProduktyFaktury() throws RemoteException
	{
		List<ProduktyFaktury> listaProduktyFaktury = new ArrayList<ProduktyFaktury>();


		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<CostInvoiceProduct> listCostInvoiceProduct = dictionaryServiceFactory.getCostInvoiceProduct();

		for (CostInvoiceProduct unit : listCostInvoiceProduct)
		{
			ProduktyFaktury pf = WSToCanonicalMapper.MapCostInvoiceProductToProduktyFaktury(unit);

			listaProduktyFaktury.add(pf);
		}

		return listaProduktyFaktury;

	}
}
