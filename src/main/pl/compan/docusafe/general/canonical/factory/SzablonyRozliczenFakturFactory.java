package pl.compan.docusafe.general.canonical.factory;

import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.SzablonyRozliczenFaktur;
import pl.compan.docusafe.general.dto.SzablonyRozliczenFakturDTO;
import pl.compan.docusafe.general.hibernate.dao.SzablonyRozliczenFakturDBDAO;
import pl.compan.docusafe.general.hibernate.model.SzablonyRozliczenFakturDB;

public class SzablonyRozliczenFakturFactory {
	/**
	 * Metoda pobiera liste szablonow z BD
	 * 
	 */
	public List<SzablonyRozliczenFaktur> pobierzSzablonyRozliczenFakturZBD() {
		
		SzablonyRozliczenFakturDTO kontrahentKonto = new SzablonyRozliczenFakturDTO();
		
		return kontrahentKonto.pobierzSzablonyRozliczenFakturZBD();
	}
	/**
	 * Metoda pobiera liste szablonow z WS
	 */
	public List<SzablonyRozliczenFaktur> pobierzSzablonyRozliczenFakturZWS() {
		
		SzablonyRozliczenFakturDTO kontrahentKonto = new SzablonyRozliczenFakturDTO();
		
		return kontrahentKonto.pobierzSzablonyRozliczenFakturZWS();
	}
	
	/**
	 * Metoda aktualizuje szablony
	 * 
	 * @param kontoNowe
	 * @param kontoStare
	 */
	public List<SzablonyRozliczenFaktur> aktualizujSzablonyRozliczenFaktur(
			List<SzablonyRozliczenFaktur> kontoNowe, List<SzablonyRozliczenFaktur> kontoStare) {
		for (SzablonyRozliczenFaktur kk : kontoNowe) {
			try {
				List<SzablonyRozliczenFakturDB> konto = SzablonyRozliczenFakturDBDAO.findByErpId(kk.getErpId());
				if (konto.isEmpty()) {
					kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (SzablonyRozliczenFakturDB kkDB : konto) {
						kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						kk.setId(kkDB.getId());
					}
				}
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		return kontoNowe;
	}
	
	/**
	 * Metoda zapisuje szablony do bazy danych
	 * @param
	 */
	
	public void ZapiszSzablonyRozliczenFakturDoDB(List<SzablonyRozliczenFaktur> szablonyRozliczenFaktur) {
		
		SzablonyRozliczenFakturDTO kontrahentKonto = new SzablonyRozliczenFakturDTO();
		
		kontrahentKonto.zapisz(szablonyRozliczenFaktur);
	}
}
