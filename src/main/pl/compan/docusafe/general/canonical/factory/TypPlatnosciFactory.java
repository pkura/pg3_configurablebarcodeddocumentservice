package pl.compan.docusafe.general.canonical.factory;

import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.TypPlatnosci;
import pl.compan.docusafe.general.hibernate.model.PaymentTermsDB;

public class TypPlatnosciFactory {

	public List<TypPlatnosci> polaczTypyPlatnosci(List<TypPlatnosci> nowy, List<TypPlatnosci> stary){
		HashSet<String> typySet=new HashSet<String>();
		for(TypPlatnosci tp:nowy){
			try {
				List<PaymentTermsDB> paymentTerms = PaymentTermsDB.findByCn(tp.getCn());
				if(paymentTerms.isEmpty()){
					tp.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					tp.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
				}
				tp.isAktywna(true);
				typySet.add(tp.getCn());
			} catch (EdmException e) {
				e.printStackTrace();
			}

		}
		for(TypPlatnosci tp:stary){
			if(typySet.contains(tp.getCn()))continue;
			tp.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			tp.isAktywna(false);
			nowy.add(tp);
		}
		return nowy;
	}
}
