package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.model.ProduktyFaktury;
import pl.compan.docusafe.general.hibernate.model.CostInvoiceProductDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;

public class ProduktyFakturyDBFactory {
	public List<ProduktyFaktury> pobierzProduktyFakturyZBD() {
		List<ProduktyFaktury> listaProduktyFaktury = new ArrayList<ProduktyFaktury>();

		List<CostInvoiceProductDB> listCostInvoiceProductDB;
		try {
			listCostInvoiceProductDB = CostInvoiceProductDB.list();

			for (CostInvoiceProductDB cost : listCostInvoiceProductDB) {
				ProduktyFaktury pf = HibernateToCanonicalMapper
						.MapCostInvoiceProductDBToProduktyFaktury(cost);
				listaProduktyFaktury.add(pf);
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaProduktyFaktury;
	}

	public boolean Zapisz(ProduktyFaktury pf)
	{

		CostInvoiceProductDB cost = new CostInvoiceProductDB();
		try {
			switch(pf.getStanObiektu())
			{
			case NOWY:
				cost = CanonicalToHibernateMapper.MapProduktyFakturyToCostInvoiceProductDB(cost, pf);	
				cost.save();
				break;			
			case ZMODYFIKOWANY:
				List<CostInvoiceProductDB> costInvoiceProduct = CostInvoiceProductDB.findByERPID(pf.getWytworId());
				for(CostInvoiceProductDB cpDB:costInvoiceProduct){
					cost = CanonicalToHibernateMapper.MapProduktyFakturyToCostInvoiceProductDB(cpDB, pf);
					cost.save();
				}
				break;
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
}
