package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.RodzajePlanowZakupu;
import pl.compan.docusafe.general.dto.RodzajePlanowZakupuDTO;
import pl.compan.docusafe.general.hibernate.dao.RodzajePlanowZakupuDBDAO;
import pl.compan.docusafe.general.hibernate.model.RodzajePlanowZakupuDB;

public class RodzajePlanowZakupuFactory {
	/**
	 * Metoda pobiera liste rodzajow planow zakupu z BD
	 * 
	 */
	public List<RodzajePlanowZakupu> pobierzRodzajePlanowZakupuZBD() {

		RodzajePlanowZakupuDTO rodzajePlanowZakupu = new RodzajePlanowZakupuDTO();

		return rodzajePlanowZakupu.pobierzRodzajePlanowZakupuZBD();
	}
	/**
	 * Metoda pobiera liste liste rodzajow planow zakupu z WS
	 * @throws RemoteException 
	 */
	public List<RodzajePlanowZakupu> pobierzRodzajePlanowZakupuZWS() throws RemoteException {

		RodzajePlanowZakupuDTO rodzajePlanowZakupu = new RodzajePlanowZakupuDTO();

		return rodzajePlanowZakupu.pobierzRodzajePlanowZakupuZWS();
	}

	/**
	 * Metoda aktualizuje rodzaje planow zakupu
	 * 
	 * @param RodzajePlanowZakupuNowe
	 * @param RodzajePlanowZakupuStare
	 */
	public List<RodzajePlanowZakupu> aktualizujRodzajePlanowZakupu(
			List<RodzajePlanowZakupu> RodzajePlanowZakupuNowe, List<RodzajePlanowZakupu> RodzajePlanowZakupuStare) {
		for (RodzajePlanowZakupu rpz : RodzajePlanowZakupuNowe) {
			try {
				List<RodzajePlanowZakupuDB> rodzajePlanowZakupu = RodzajePlanowZakupuDBDAO.findByErpID(rpz.getErpId());
				if (rodzajePlanowZakupu.isEmpty()) {
					rpz.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (RodzajePlanowZakupuDB rpzDB : rodzajePlanowZakupu) {
						rpz.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						rpz.setId(rpzDB.getId());
					}
				}
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		return RodzajePlanowZakupuNowe;
	}

	/**
	 * Metoda zapisuje rodzaje planow zakupu do bazy danych
	 * @param
	 */

	public void ZapiszRodzajePlanowZakupuDoDB(List<RodzajePlanowZakupu> listaRodzajePlanowZakupu) {

		RodzajePlanowZakupuDTO rodzajePlanowZakupu = new RodzajePlanowZakupuDTO();

		rodzajePlanowZakupu.zapisz(listaRodzajePlanowZakupu);
	}
}
