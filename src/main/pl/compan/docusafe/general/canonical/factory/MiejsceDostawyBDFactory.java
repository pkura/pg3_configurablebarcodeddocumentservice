package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.model.MiejsceDostawy;
import pl.compan.docusafe.general.hibernate.model.DeliveryLocationDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;

public class MiejsceDostawyBDFactory {
	public List<MiejsceDostawy> pobierzMiejsceDostawyZBD() {
		List<MiejsceDostawy> listaMiejscDostawy = new ArrayList<MiejsceDostawy>();

		List<DeliveryLocationDB> listaMiejscDostawyDB;
		try {
			listaMiejscDostawyDB = DeliveryLocationDB.list();

			for (DeliveryLocationDB dl : listaMiejscDostawyDB) {
				MiejsceDostawy md = HibernateToCanonicalMapper
						.MapDeliveryLocationDBToMiejsceDostawy(dl);
				listaMiejscDostawy.add(md);
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaMiejscDostawy;
	}

	public boolean Zapisz(MiejsceDostawy miejsceDostawy)
	{
		DeliveryLocationDB deliveryLocationDB = new DeliveryLocationDB();
		try {
			switch(miejsceDostawy.getStanObiektu())
			{
			case NOWY:
				deliveryLocationDB = CanonicalToHibernateMapper.MapMiejsceDostawyNaDeliveryLocationDB(deliveryLocationDB, miejsceDostawy);	
				deliveryLocationDB.save();
				break;			
			case ZMODYFIKOWANY:
				List<DeliveryLocationDB> deliveryLocation = DeliveryLocationDB.findByCn(miejsceDostawy.getCn());
				for(DeliveryLocationDB dl:deliveryLocation){
					deliveryLocationDB = CanonicalToHibernateMapper.MapMiejsceDostawyNaDeliveryLocationDB(dl, miejsceDostawy);
					deliveryLocationDB.save();
				}
				break;
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
}
