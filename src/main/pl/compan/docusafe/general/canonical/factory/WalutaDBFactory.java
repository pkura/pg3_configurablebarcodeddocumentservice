package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.canonical.model.Waluta;
import pl.compan.docusafe.general.hibernate.dao.DSDivisionFactory;
import pl.compan.docusafe.general.hibernate.model.CurrencyDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;

public class WalutaDBFactory {

	public List<Waluta> pobierzWaluteZBD() {
		List<Waluta> listaWaluta = new ArrayList<Waluta>();

		List<CurrencyDB> listaWalutaDB;
		try {
			listaWalutaDB = CurrencyDB.list();

			for (CurrencyDB waluta : listaWalutaDB) {
				Waluta w = HibernateToCanonicalMapper
						.MapCurrencyDBToWaluta(waluta);
				listaWaluta.add(w);
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaWaluta;
	}

	public boolean Zapisz(Waluta waluta)
	{
		CurrencyDB currencyDB = new CurrencyDB();
		try {
			switch(waluta.getStanObiektu())
			{
			case NOWY:
				currencyDB = CanonicalToHibernateMapper.MapWalutaNaCurrencyDB(currencyDB, waluta);	
				currencyDB.save();
				break;			
			case ZMODYFIKOWANY:
				List<CurrencyDB> currency = CurrencyDB.findByCn(waluta.getCn());
				for(CurrencyDB cc:currency){
					currencyDB = CanonicalToHibernateMapper.MapWalutaNaCurrencyDB(cc, waluta);
					currencyDB.save();
				}
				break;
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
}
