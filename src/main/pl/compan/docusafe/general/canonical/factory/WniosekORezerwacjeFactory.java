package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.WniosekORezerwacje;
import pl.compan.docusafe.general.dto.WniosekORezerwacjeDTO;
import pl.compan.docusafe.general.hibernate.dao.WniosekORezerwacjeDBDAO;
import pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjeDB;

public class WniosekORezerwacjeFactory {
	/**
	 * Metoda pobiera liste WniosekORezerwacje z BD
	 * 
	 */
	public List<WniosekORezerwacje> pobierzWniosekORezerwacjeZBD() {
		
		WniosekORezerwacjeDTO wnDTO = new WniosekORezerwacjeDTO();
		
		return wnDTO.pobierzWniosekORezerwacjeZBD();
	}
	/**
	 * Metoda pobiera liste wnioskow o rezerwacje z WS
	 * @throws RemoteException 
	 */
	public List<WniosekORezerwacje> pobierzWnioskiORezerwacjeZWS() throws RemoteException {
		
		WniosekORezerwacjeDTO wnDTO = new WniosekORezerwacjeDTO();
		
		return wnDTO.pobierzWnioskiORezerwacjeZWS();
	}
	
	/**
	 * Metoda aktualizuje wnioski o rezerwacje
	 * 
	 * @param WnioskiORezerwacjeNowe
	 * @param WnioskiORezerwacjeStare
	 */
	public List<WniosekORezerwacje> aktualizujWnioskiORezerwacje(
			List<WniosekORezerwacje> WnioskiORezerwacjeNowe, List<WniosekORezerwacje> WnioskiORezerwacjeStare) {
		HashSet<Long> wnSet=new HashSet<Long>();
		for (WniosekORezerwacje wn : WnioskiORezerwacjeNowe) {
			try {
				List<WniosekORezerwacjeDB> listWniosekORezerwacje = WniosekORezerwacjeDBDAO.findby_bd_rezerwacja_id(wn.getBdRezerwacjaId());
				if (listWniosekORezerwacje.isEmpty()) {
					wn.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (WniosekORezerwacjeDB wnDB : listWniosekORezerwacje) {
						wn.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						wn.setId(wnDB.getId());
					}
				}
				//dostepny
				wn.setAvailable(true);
				wnSet.add(wn.getBdRezerwacjaId());
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		for (WniosekORezerwacje wn : WnioskiORezerwacjeStare) {
			if(wnSet.contains(wn.getBdRezerwacjaId()))continue;
			wn.setAvailable(false);
			wn.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			WnioskiORezerwacjeNowe.add(wn);
		}
		return WnioskiORezerwacjeNowe;
	}
	
	/**
	 * Metoda zapisuje wnioski o rezerwacje do bazy danych
	 * @param
	 */
	
	public void zapiszWniosekORezerwacjeDoDB(List<WniosekORezerwacje> listaWniosekORezerwacje) {
		
		WniosekORezerwacjeDTO wniosekORezerwacje = new WniosekORezerwacjeDTO();
		
		wniosekORezerwacje.Zapisz(listaWniosekORezerwacje);
	}
}
