package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetEtap;
import pl.compan.docusafe.general.dto.ProjektBudzetEtapDTO;
import pl.compan.docusafe.general.hibernate.dao.ProjectBudgetItemDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetEtapDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ProjektBudzetEtapFactory {
	public static final Logger log = LoggerFactory.getLogger(ProjektBudzetEtapFactory.class);

	/**
	 * Metoda pobiera projekty budzetow etapy z BD
	 * 
	 * @return
	 */
	public List<ProjektBudzetEtap> pobierzProjektBudzetEtapZBD() {

		ProjektBudzetEtapDTO projektBudzetEtapDTO = new ProjektBudzetEtapDTO();

		return projektBudzetEtapDTO.pobierzProjektBudzetEtapZBD();		
	}

	/**
	 * Metoda pobiera projekty budzetow etapy z WS
	 * 
	 * @return
	 * @throws RemoteException 
	 */
	public List<ProjektBudzetEtap>  PobierzProjektBudzetEtapWS() throws RemoteException
	{
		ProjektBudzetEtapDTO projektBudzetEtapDTO = new ProjektBudzetEtapDTO();

		return projektBudzetEtapDTO.PobierzProjektBudzetEtap();		
	}
	/**
	 * Metoda aktualizuje projekty budzetow etap
	 * @param ProjektBudzetEtapNowa
	 * @param ProjektBudzetEtapStara
	 * @return
	 */
	public List<ProjektBudzetEtap> aktualizujProjektBudzetEtap(List<ProjektBudzetEtap> ProjektBudzetEtapNowa, List<ProjektBudzetEtap> ProjektBudzetEtapStara){
		HashSet<Long> etapSet=new HashSet<Long>();
		for(ProjektBudzetEtap projektBudzetEtap:ProjektBudzetEtapNowa){
			try {
				List<ProjektBudzetEtapDB> projectBudgetItem = ProjectBudgetItemDBDAO.findByErpID(projektBudzetEtap.getErpId());
				if(projectBudgetItem.isEmpty()){
					projektBudzetEtap.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for(ProjektBudzetEtapDB pb:projectBudgetItem){
						projektBudzetEtap.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						projektBudzetEtap.setId(pb.getId());
					}
				}
				projektBudzetEtap.setAvailable(true);
				etapSet.add(projektBudzetEtap.getErpId());
			} catch (EdmException e) {
				log.error("", e);
			}
		}
		for(ProjektBudzetEtap pbe:ProjektBudzetEtapStara){
			if(etapSet.contains(pbe.getErpId()))continue;
			pbe.setAvailable(false);
			pbe.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			ProjektBudzetEtapNowa.add(pbe);
		}
		return ProjektBudzetEtapNowa;
	}

	/**
	 * Metoda zapisuje projekty budzetow etap do bazy danych
	 * 
	 * @param listaProjektBudzetEtap
	 */
	public void zapiszProjektBudzetEtapDoBD(List<ProjektBudzetEtap> listaProjektBudzetEtap) {

		ProjektBudzetEtapDTO projektBudzetEtapDTO = new ProjektBudzetEtapDTO();

		projektBudzetEtapDTO.Zapisz(listaProjektBudzetEtap);
	}
}
