package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.ZleceniaProdukcyjne;
import pl.compan.docusafe.general.dto.ZleceniaProdukcyjneDTO;
import pl.compan.docusafe.general.hibernate.dao.ProductionOrderDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProductionOrderDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZleceniaProdukcyjneFactory {
	public static final Logger log = LoggerFactory.getLogger(ZleceniaProdukcyjneFactory.class);

	/**
	 * Metoda zlecenia produkcyjne z BD
	 * 
	 * @return
	 */
	public List<ZleceniaProdukcyjne> pobierzZleceniaProdukcyjneZBD() {

		ZleceniaProdukcyjneDTO zleceniaProdukcyjneDTO = new ZleceniaProdukcyjneDTO();

		return zleceniaProdukcyjneDTO.pobierzZleceniaProdukcyjneZBD();		
	}

	/**
	 * Metoda zlecenia produkcyjne z WS
	 * 
	 * @return
	 * @throws RemoteException 
	 */
	public List<ZleceniaProdukcyjne>  PobierzZleceniaProdukcyjneWS() throws RemoteException
	{
		ZleceniaProdukcyjneDTO zleceniaProdukcyjneDTO = new ZleceniaProdukcyjneDTO();

		return zleceniaProdukcyjneDTO.PobierzZleceniaProdukcyjne();		
	}
	/**
	 * Metoda aktualizuje zlecenia produkcyjne
	 * @param ZleceniaProdukcyjneNowa
	 * @param ZleceniaProdukcyjneStara
	 * @return
	 */
	public List<ZleceniaProdukcyjne> aktualizujZleceniaProdukcyjne(List<ZleceniaProdukcyjne> ZleceniaProdukcyjneNowa, List<ZleceniaProdukcyjne> ZleceniaProdukcyjneStara){
		HashSet<Long> zlecSet=new HashSet<Long>();
		for(ZleceniaProdukcyjne zleceniaProdukcyjne:ZleceniaProdukcyjneNowa){
			try {
				List<ProductionOrderDB> productionOrderDB = ProductionOrderDBDAO.findByErpID(zleceniaProdukcyjne.getErpId());
				if(productionOrderDB.isEmpty()){
					zleceniaProdukcyjne.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for(ProductionOrderDB p:productionOrderDB){
						zleceniaProdukcyjne.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						zleceniaProdukcyjne.setId(p.getId());
					}
				}
				zleceniaProdukcyjne.setAvailable(true);
				zlecSet.add(zleceniaProdukcyjne.getErpId());
			} catch (EdmException e) {
				log.error("", e);
			}
		}
		for(ZleceniaProdukcyjne zp:ZleceniaProdukcyjneStara){
			if(zlecSet.contains(zp.getErpId()))continue;
			zp.setAvailable(false);
			zp.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			ZleceniaProdukcyjneNowa.add(zp);
		}
		return ZleceniaProdukcyjneNowa;
	}

	/**
	 * Metoda zapisuje zlecenia produkcyjne do bazy danych
	 * 
	 * @param listaZleceniaProdukcyjne
	 */
	public void zapiszZleceniaProdukcyjneDoBD(List<ZleceniaProdukcyjne> listaZleceniaProdukcyjne) {

		ZleceniaProdukcyjneDTO zleceniaProdukcyjneDTO = new ZleceniaProdukcyjneDTO();

		zleceniaProdukcyjneDTO.Zapisz(listaZleceniaProdukcyjne);
	}
}
