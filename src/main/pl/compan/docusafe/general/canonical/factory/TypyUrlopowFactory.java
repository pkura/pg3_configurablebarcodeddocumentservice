package pl.compan.docusafe.general.canonical.factory;

import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.TypyUrlopow;
import pl.compan.docusafe.general.dto.TypyUrlopowDTO;
import pl.compan.docusafe.general.hibernate.dao.TypyUrlopowDBDAO;
import pl.compan.docusafe.general.hibernate.model.TypyUrlopowDB;

public class TypyUrlopowFactory {
	/**
	 * Metoda pobiera liste typow urlopow z BD
	 * 
	 */
	public List<TypyUrlopow> pobierzTypyUrlopowZBD() {
		
		TypyUrlopowDTO typyUrlopow = new TypyUrlopowDTO();
		
		return typyUrlopow.pobierzTypyUrlopowZBD();
	}
	/**
	 * Metoda pobiera liste typow urlopow z WS
	 */
	public List<TypyUrlopow> pobierzTypyUrlopoweZWS() {
		
		TypyUrlopowDTO typyUrlopow = new TypyUrlopowDTO();
		
		return typyUrlopow.PobierzTypyUrlopow();
	}
	
	/**
	 * Metoda aktualizuje typy urlopow
	 * 
	 * @param TypyUrlopowNowe
	 * @param TypyUrlopowStare
	 */
	public List<TypyUrlopow> aktualizujTypyUrlopow(
			List<TypyUrlopow> TypyUrlopowNowe, List<TypyUrlopow> TypyUrlopowStare) {
		for (TypyUrlopow tu : TypyUrlopowNowe) {
			try {
				List<TypyUrlopowDB> listTypyUrlopow = TypyUrlopowDBDAO.findByErpID(tu.getErpId());
				if (listTypyUrlopow.isEmpty()) {
					tu.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (TypyUrlopowDB tuDB : listTypyUrlopow) {
						tu.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						tu.setId(tuDB.getId());
					}
				}
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		return TypyUrlopowNowe;
	}
	
	/**
	 * Metoda zapisuje typy urlopow do bazy danych
	 * @param
	 */
	
	public void ZapiszTypyUrlopowoDB(List<TypyUrlopow> listaTypyUrlopow) {
		
		TypyUrlopowDTO typyUrlopow = new TypyUrlopowDTO();
		
		typyUrlopow.Zapisz(listaTypyUrlopow);
	}
}
