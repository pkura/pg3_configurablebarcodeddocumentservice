package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.Kontrahent;
import pl.compan.docusafe.general.hibernate.dao.KontrahentKontoDBDAO;
import pl.compan.docusafe.general.hibernate.model.KontrahentDB;
import pl.compan.docusafe.general.hibernate.model.KontrahentKontoDB;

public class KontrahentFactory {
	public List<Kontrahent> sprawdzKontrahent(List<Kontrahent> nowy, List<Kontrahent> stary){

		List<Kontrahent> aktualny = new ArrayList<Kontrahent>();
		for(Kontrahent kontrahent:nowy){
			try {
				List<KontrahentDB> kontrahentDB = KontrahentDB.findByErpId(kontrahent.getErpId());
				List<KontrahentKontoDB> kontrahentKontoDB = KontrahentKontoDBDAO.findByDostawcaId(kontrahent.getErpId());
				if(!kontrahentKontoDB.isEmpty()){
					//ustawianie konta dla kontrahenta
					for(KontrahentKontoDB konto:kontrahentKontoDB){
						Kontrahent kont = new Kontrahent();
						if(kontrahentDB.isEmpty()){
							kont.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
							kont.setNazwa(kontrahent.getNazwa());
							kont.setKodpocztowy(kontrahent.getKodpocztowy());
							kont.setMiejscowosc(kontrahent.getMiejscowosc());
							kont.setNip(kontrahent.getNip());
							kont.setNrDomu(kontrahent.getNrDomu());
							kont.setMiejscowosc(kontrahent.getMiejscowosc());
							kont.setNrMieszkania(kontrahent.getNrMieszkania());
							kont.setUlica(kontrahent.getUlica());
							kont.setNowy(kontrahent.getNowy());
							kont.setErpId(kontrahent.getErpId());
							kont.setKraj(kontrahent.getKraj());
							kont.setNrKonta(konto.getNumer_konta());
							aktualny.add(kont);
						} else {
							kont.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
							kont.setNazwa(kontrahent.getNazwa());
							kont.setKodpocztowy(kontrahent.getKodpocztowy());
							kont.setMiejscowosc(kontrahent.getMiejscowosc());
							kont.setNip(kontrahent.getNip());
							kont.setNrDomu(kontrahent.getNrDomu());
							kont.setMiejscowosc(kontrahent.getMiejscowosc());
							kont.setNrMieszkania(kontrahent.getNrMieszkania());
							kont.setUlica(kontrahent.getUlica());
							kont.setNowy(kontrahent.getNowy());
							kont.setErpId(kontrahent.getErpId());
							kont.setKraj(kontrahent.getKraj());
							kont.setNrKonta(konto.getNumer_konta());
							aktualny.add(kont);
						}
					} 
				}else if(kontrahentDB.isEmpty()){
					kontrahent.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
					aktualny.add(kontrahent);
				} else {
					kontrahent.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
					aktualny.add(kontrahent);
				}
			} catch (EdmException e) {
				e.printStackTrace();
			}
		}
		return aktualny;
	}
}
