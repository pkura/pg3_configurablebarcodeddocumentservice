package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.OkresBudzetowy;
import pl.compan.docusafe.general.canonical.model.ProjectAccountPiatki;
import pl.compan.docusafe.general.dto.ProjectAccountPiatkiDTO;
import pl.compan.docusafe.general.hibernate.dao.ProjectAccountPiatkiDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProjectAccountPiatkiDB;

public class ProjectAccountPiatkiFactory {
	/**
	 * Metoda pobiera liste piatek z BD
	 * 
	 */
	public List<ProjectAccountPiatki> pobierzPiatkiZBD() {
		
		ProjectAccountPiatkiDTO piatkiDTO = new ProjectAccountPiatkiDTO();
		
		return piatkiDTO.pobierzPiatkiZBD();
	}
	/**
	 * Metoda pobiera liste piatek z WS
	 * @throws RemoteException 
	 */
	public List<ProjectAccountPiatki> pobierzPiatkiZWS() throws RemoteException {
		
		ProjectAccountPiatkiDTO piatkiDTO = new ProjectAccountPiatkiDTO();
		
		return piatkiDTO.pobierzPiatkiZWS();
	}
	
	/**
	 * Metoda aktualizuje piatki
	 * 
	 * @param kontoNowe
	 * @param kontoStare
	 */
	public List<ProjectAccountPiatki> aktualizujProjectAccountPiatki(
			List<ProjectAccountPiatki> kontoNowe, List<ProjectAccountPiatki> kontoStare) {
		HashSet<String> piatkiSet=new HashSet<String>();
		for (ProjectAccountPiatki kk : kontoNowe) {
			try {
				List<ProjectAccountPiatkiDB> konto = ProjectAccountPiatkiDBDAO.findByKontoIDoIKontraktIDIRepAtrybutIdo(kk.getKonto_ido(),kk.getKontrakt_id(),kk.getRep_atrybut_ido());
				if (konto.isEmpty()) {
					kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (ProjectAccountPiatkiDB kkDB : konto) {
						kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						kk.setId(kkDB.getId());
					}
				}
				kk.setAvailable(true);
				piatkiSet.add(kk.getKonto_ido()+kk.getKontrakt_id().toString()+kk.getRep_atrybut_ido());
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		for (ProjectAccountPiatki bk : kontoStare) {
			if(piatkiSet.contains(bk.getKonto_ido()+bk.getKontrakt_id().toString()+bk.getRep_atrybut_ido()))continue;
			bk.setAvailable(false);
			bk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			kontoNowe.add(bk);
		}
		return kontoNowe;
	}
	
	/**
	 * Metoda zapisuje piatki do bazy danych
	 * @param
	 */
	
	public void zapiszPiatkiDoDB(List<ProjectAccountPiatki> listaBudzetowKoKomorek) {
		
		ProjectAccountPiatkiDTO kontrahentKonto = new ProjectAccountPiatkiDTO();
		
		kontrahentKonto.zapisz(listaBudzetowKoKomorek);
	}
}
