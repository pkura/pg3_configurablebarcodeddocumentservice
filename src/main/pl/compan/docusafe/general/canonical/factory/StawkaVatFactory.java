package pl.compan.docusafe.general.canonical.factory;

import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.StawkaVat;
import pl.compan.docusafe.general.hibernate.model.VatRateDB;

public class StawkaVatFactory {
	public List<StawkaVat> polaczStawkaVat(List<StawkaVat> nowa, List<StawkaVat> stara){
		HashSet<Long> vatSet=new HashSet<Long>();
		for(StawkaVat sv:nowa){
			try {
				List<VatRateDB> vatRate = VatRateDB.findByErpID(sv.getErp_id());
				if(vatRate.isEmpty()){
					sv.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					sv.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
				}
				sv.setAktywna(true);
				vatSet.add(sv.getErp_id());
			} catch (EdmException e) {
				e.printStackTrace();
			}
		}
		for(StawkaVat sv:stara){
			if(vatSet.contains(sv.getErp_id()))continue;
			sv.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			sv.setAktywna(false);
			stara.add(sv);
		}
		return nowa;
	}
}
