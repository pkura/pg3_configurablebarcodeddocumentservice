package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.BudzetPozycjaSimple;
import pl.compan.docusafe.general.dto.BudzetPozycjaSimpleDTO;
import pl.compan.docusafe.general.hibernate.dao.BudzetPozycjaSimpleDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudzetPozycjaSimpleDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class BudzetPozycjeSimpleFactory {

	/**
	 * Metoda pobiera pozycje budzetow z BD
	 * 
	 */
	public static final Logger log = LoggerFactory.getLogger(BudzetPozycjeSimpleFactory.class);
	public List<BudzetPozycjaSimple> pobierzBudzetPozycjeSimpleZBD() {
		
		BudzetPozycjaSimpleDTO kontrahentKonto = new BudzetPozycjaSimpleDTO();
		
		return kontrahentKonto.pobierzBudzetyPozycjeZBD();
	}
	/**
	 * Metoda pobiera pozycje budzetow z WS
	 * @throws RemoteException 
	 */
	public List<BudzetPozycjaSimple> pobierzBudzetPozycjeSimpleZWS() throws RemoteException {
		
		BudzetPozycjaSimpleDTO kontrahentKonto = new BudzetPozycjaSimpleDTO();
		
		return kontrahentKonto.pobierzBudzetyPozycjeSimpleZWS();
	}
	
	/**
	 * Metoda aktualizuje pozycje budzetow
	 * 
	 * @param kontoNowe
	 * @param kontoStare
	 */
	public List<BudzetPozycjaSimple> aktualizujBudzetyKoKomorek(
			List<BudzetPozycjaSimple> kontoNowe, List<BudzetPozycjaSimple> kontoStare) {
		HashSet<Long> budzetySet=new HashSet<Long>();
		for (BudzetPozycjaSimple kk : kontoNowe) {
			try {
				BudzetPozycjaSimpleDB konto = BudzetPozycjaSimpleDBDAO.findByErpIdUnique(kk.getErpId());
				if (konto==null) {
					kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
						kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						kk.setId(konto.getId());
				}
				kk.setAvailable(true);
				budzetySet.add(kk.getErpId());
			} catch (EdmException e) {
				log.error(e.getMessage(),e);
			}
		}
		//dodanie nieaktualnych z bazy danych
		for (BudzetPozycjaSimple kk : kontoStare) {
			if(budzetySet.contains(kk.getErpId()))continue;
			kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			kk.setAvailable(false);
			kontoNowe.add(kk);
		}
		
		return kontoNowe;
	}
	
	/**
	 * Metoda zapisuje budzety do bazy danych
	 * @param
	 */
	
	public void ZapiszBudzetyKoKomorekDoDB(List<BudzetPozycjaSimple> listaBudzetowKoKomorek) {
		
		BudzetPozycjaSimpleDTO kontrahentKonto = new BudzetPozycjaSimpleDTO();
		
		kontrahentKonto.zapisz(listaBudzetowKoKomorek);
	}
}
