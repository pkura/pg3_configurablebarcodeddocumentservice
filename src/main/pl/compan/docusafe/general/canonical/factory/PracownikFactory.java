package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.canonical.model.Pracownik;
import pl.compan.docusafe.general.canonical.model.PracownikWJednostkachOrganizacyjnych;
import pl.compan.docusafe.general.dto.PracownikDTO;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PracownikFactory {

	public static final Logger log = LoggerFactory.getLogger(PracownikFactory.class);

	/**
	 * Metoda pobiera pracownikow z WS
	 * 
	 * @return
	 */
	public List<Pracownik> PobierzPracownikowWS() {

		PracownikDTO pracownikDTO = new PracownikDTO();

		return pracownikDTO.pobierzPracownikowWS();
	}

	/**
	 * Metoda pobiera pracownik�w z BD
	 * 
	 * @return
	 */
	public List<Pracownik> PobierzPracownikowDB() {

		PracownikDTO pracownikDTO = new PracownikDTO();

		return pracownikDTO.pobierzPracownikowDB();

	}

	/**
	 * Metoda pobiera dwie listy pracownikow: now� oraz star�. Nast�pnie listy
	 * te s� ��czone w jedn� - now� list�.
	 * U�ytkownicy z nowej listy s� wprowadzani jako nowi u�ytkownicy,
	 * U�ytkownicy, kt�rzy s� w obydwu listach s� aktualizowani,
	 * U�ytkownicy ze starej listy, kt�rzy nie wyst�puj� w nowej li�cie s� dezaktywowani.
	 * 
	 * @param listaPracownikowNowa
	 * @param listaPracownikowStara
	 * @return
	 */
	public List<Pracownik> aktualizujListePracownikow(List<Pracownik> listaPracownikowNowa, List<Pracownik> listaPracownikowStara) {

		List<Pracownik> aktualnaListaPracownikow = new ArrayList<Pracownik>();

		// Zamiana list na HashMapy
		// Mapa zawieraj�ca now� list� pracownik�w, kluczem jest
		// operatorDoPorownywania
		HashMap<String, Pracownik> _nowaMap = new HashMap<String, Pracownik>();

		for (Pracownik pracownik : listaPracownikowNowa) {
            log.info("nowa {}", pracownik);
			if (pracownik != null) {
				if (pracownik.getOperatorDoPorownywania() != null) {
					try {
						_nowaMap.put(pracownik.getOperatorDoPorownywania(), pracownik);
					} catch (Exception exc) {
						log.debug("",exc);
					}
				} else {
					log.debug("Pracownik {} {} - Brak operatora do por�wnania.", pracownik.getImie(), pracownik.getNazwisko());
				}
			}
		}

		// Mapa zawieraj�ca star� list� pracownik�w, kluczem jest
		// operatorDoPorownywania
		HashMap<String, Pracownik> _staraMap = new HashMap<String, Pracownik>();
		for (Pracownik pracownik : listaPracownikowStara) {
            log.info("stara {}", pracownik);
			if (pracownik != null) {
				_staraMap.put(pracownik.getOperatorDoPorownywania(), pracownik);
			}
		}

		// Mapa zawieraj�ca finaln� - po��czon� - list� u�ytkownik�w
		HashMap<String, Pracownik> _polaczonaMap = new HashMap<String, Pracownik>();

		// Przegl�damy wszystkie elementy _nowaMap i aktualizujemy

		Iterator<String> _nowaMapIterator = _nowaMap.keySet().iterator();

		while (_nowaMapIterator.hasNext()) {

			String key = _nowaMapIterator.next();

			Pracownik nowyPracownik = _nowaMap.get(key);

			// Sprawdzenie, czy taki pracownik ju� jest w starej strukturze
			Pracownik staryPracownik = _staraMap.get(key);

			// Pracownika nie ma
			if (staryPracownik == null) {
				// Dodajemy pracownika do aktualnej listy
				nowyPracownik.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);

				_polaczonaMap.put(nowyPracownik.getOperatorDoPorownywania(),
						nowyPracownik);

			} else {
				// Pracownik jest - aktualizujemy co najwy�ej - inaczej m�wi�c
				PracownikDTO pracownikDTO = new PracownikDTO();
				staryPracownik = pracownikDTO.aktualizujDaneWSNaDB(nowyPracownik, staryPracownik);

				staryPracownik.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
				_polaczonaMap.put(staryPracownik.getOperatorDoPorownywania(),
						staryPracownik);
				// Skoro ju� obs�u�yli�my t� jednostk�, to usuwamy j� z mapy
				// starej
				_staraMap.remove(staryPracownik.getOperatorDoPorownywania());
			}
		}
		// U�ytkownicy ze starej listy, nie wyst�puj�cy w nowej li�cie s� dezaktywowani
		
		Iterator<String> _staraMapIterator = _staraMap.keySet().iterator();

		while (_staraMapIterator.hasNext()) {

			String key = _staraMapIterator.next();

			Pracownik staryPracownik = _staraMap.get(key);
			
			staryPracownik.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.USUNIETY);
			
			_polaczonaMap.put(staryPracownik.getOperatorDoPorownywania(),
					staryPracownik);
		}
			
		// Tworzymy list� z po��czonej hashmapy
		
		Iterator<String> _poloczonaMapIterator = _polaczonaMap.keySet().iterator();
		
		while (_poloczonaMapIterator.hasNext()) {

			String key = _poloczonaMapIterator.next();

			Pracownik polaczonyPracownik = _polaczonaMap.get(key);
			log.info("polaczona {}", polaczonyPracownik);
			aktualnaListaPracownikow.add(polaczonyPracownik);
		}
		
		
		return aktualnaListaPracownikow;
	}

	/**
	 * Metoda zapisuje pracownik�w do bazy danych
	 *
	 * @param listaPracownikowAktualna
	 */
	public void zapiszListePracownikowDoBD(List<Pracownik> listaPracownikow) {
		PracownikDTO pracownikDTO = new PracownikDTO();
		pracownikDTO.zapiszListePracownikowDoBD(listaPracownikow);
	}

	/**
	 * Metoda dodaje dodatkowe powi�zania dla u�ytkownik�w w strukturze organizacyjnej
	 * @param listaPracownikowAktualna
	 * @return
	 */
	public List<Pracownik> aktualizujPrzpisanieDoKomorekOrganizacyjnych(
			List<Pracownik> listaPracownikowAktualna) {
		
		// Pobranie listy powi�za� pracownik<->jednostka organizacyjna
		PracownikDTO pracownikDTO = new PracownikDTO();
		
		List<PracownikWJednostkachOrganizacyjnych> listaPracownikowWJO = 
				pracownikDTO.pobierzPowiazaniePracownikowDoJednostekOrganizacyjnychWS();
		
		HashMap<String, Pracownik> _hashMapaPracownikow = new HashMap<String, Pracownik>(); 
		
		// Upychamy pracownik�w do szybkiego wyszukiwania
		
		for(Pracownik pracownik : listaPracownikowAktualna)
		{
			_hashMapaPracownikow.put(pracownik.getZewnetrznyIdm(), pracownik);
		}
		
		// Dla ka�dego pracownik badamy, czy ma przypisan� tak� jednostk� organizacyjn�
		
		for (PracownikWJednostkachOrganizacyjnych pwjo : listaPracownikowWJO) {
			try {
				String pracownikIdm = pwjo.getPracownik().getZewnetrznyIdm();
				String joIdn = pwjo.getJednostkaOrganizacyjna().getIdn();
                if(StringUtils.isEmpty(joIdn)){
                    log.debug("joIdm jest null nie przypisuje {}", pwjo.getPracownik().getLogin());
                    continue;
                }
				Pracownik pracownik = _hashMapaPracownikow.get(pracownikIdm);

				boolean joPrzypisana = false;

				if (pracownik != null && pracownik.getJednostkiOrganizacyjne() != null) {
					for (JednostkaOrganizacyjna jo : pracownik.getJednostkiOrganizacyjne()) {
						if (jo.getIdn() != null && jo.getIdn().equals(joIdn)) {
							joPrzypisana = true;
						}
					}

					if (!joPrzypisana) {
						// Przypisujemy now� jednostk� organizacyjn�
						// Jednostka organizacyjna
						JednostkaOrganizacyjnaFactory jednostkaOrganizacyjnaFactory = new JednostkaOrganizacyjnaFactory();
						JednostkaOrganizacyjna jednostkaOrganizacyjna = jednostkaOrganizacyjnaFactory.PobierzJednostkePoIdmDB(joIdn);
						pracownik.getJednostkiOrganizacyjne().add(jednostkaOrganizacyjna);
					}
				}
			} catch (Exception exc) {
				log.debug("", exc);
			}
		}
		
		// Przypisanie do u�ytkownik�w dodatkowych jednostek organizacyjnych
		return listaPracownikowAktualna;
	}

}
