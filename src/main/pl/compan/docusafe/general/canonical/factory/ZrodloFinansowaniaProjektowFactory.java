package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.ZrodloFinansowaniaProjektow;
import pl.compan.docusafe.general.dto.ZrodloFinansowaniaProjektowDTO;
import pl.compan.docusafe.general.hibernate.dao.SourcesOfProjectFundingDBDAO;
import pl.compan.docusafe.general.hibernate.model.SourcesOfProjectFundingDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZrodloFinansowaniaProjektowFactory {
	public static final Logger log = LoggerFactory.getLogger(ZrodloFinansowaniaProjektowFactory.class);

	/**
	 * Metoda pobiera zrodlo finansowania projektow z BD
	 * 
	 * @return
	 */
	public List<ZrodloFinansowaniaProjektow> pobierzZrodloFinansowaniaProjektowZBD() {

		ZrodloFinansowaniaProjektowDTO zrodloFinansowaniaProjektowDTO = new ZrodloFinansowaniaProjektowDTO();

		return zrodloFinansowaniaProjektowDTO.pobierzZrodloFinansowaniaProjektowZBD();		
	}

	/**
	 * Metoda pobiera zrodlo finansowania projektow z WS
	 * 
	 * @return
	 * @throws RemoteException 
	 */
	public List<ZrodloFinansowaniaProjektow>  PobierzZrodloFinansowaniaProjektowWS() throws RemoteException
	{
		ZrodloFinansowaniaProjektowDTO zrodloFinansowaniaProjektowDTO = new ZrodloFinansowaniaProjektowDTO();

		return zrodloFinansowaniaProjektowDTO.PobierzZrodloFinansowaniaProjektow();		
	}
	/**
	 * Metoda aktualizuje zrodlo finansowania projektow
	 * @param ZrodloFinansowaniaProjektowNowa
	 * @param ZrodloFinansowaniaProjektowStara
	 * @return
	 */
	public List<ZrodloFinansowaniaProjektow> aktualizujZrodloFinansowaniaProjektow(List<ZrodloFinansowaniaProjektow> ZrodloFinansowaniaProjektowNowa, List<ZrodloFinansowaniaProjektow> ZrodloFinansowaniaProjektowStara){
		HashSet<Long> sofSet=new HashSet<Long>();
		for(ZrodloFinansowaniaProjektow zrodloFinansowaniaProjektow:ZrodloFinansowaniaProjektowNowa){
			try {
				List<SourcesOfProjectFundingDB> sourcesOfProjectFundingDB = SourcesOfProjectFundingDBDAO.findByErpID(zrodloFinansowaniaProjektow.getErpId());
				if(sourcesOfProjectFundingDB.isEmpty()){
					zrodloFinansowaniaProjektow.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for(SourcesOfProjectFundingDB sop:sourcesOfProjectFundingDB){
						zrodloFinansowaniaProjektow.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						zrodloFinansowaniaProjektow.setId(sop.getId());
					}
				}
				zrodloFinansowaniaProjektow.setAvailable(true);
				sofSet.add(zrodloFinansowaniaProjektow.getErpId());
			} catch (EdmException e) {
				log.error("", e);
			}
		}
		for(ZrodloFinansowaniaProjektow zfp:ZrodloFinansowaniaProjektowStara){
			if(sofSet.contains(zfp.getErpId()))continue;
			zfp.setAvailable(false);
			zfp.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			ZrodloFinansowaniaProjektowNowa.add(zfp);
		}
		return ZrodloFinansowaniaProjektowNowa;
	}

	/**
	 * Metoda zapisuje zrodlo finansowania projektow do bazy danych
	 * 
	 * @param listaZrodloFinansowaniaProjektow
	 */
	public void zapiszZrodloFinansowaniaProjektowDoBD(List<ZrodloFinansowaniaProjektow> listaZrodloFinansowaniaProjektow) {

		ZrodloFinansowaniaProjektowDTO zrodloFinansowaniaProjektowDTO = new ZrodloFinansowaniaProjektowDTO();

		zrodloFinansowaniaProjektowDTO.Zapisz(listaZrodloFinansowaniaProjektow);
	}
}
