package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.general.canonical.model.JednostkaMiary;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitOfMeasure;

public class JednostkaMiaryWSFactory {
	public List<JednostkaMiary>  PobierzJednostkaMiary() throws RemoteException
	{
		List<JednostkaMiary> listaJednostkaMiary = new ArrayList<JednostkaMiary>();


		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<UnitOfMeasure> listUnitOfMeasure = dictionaryServiceFactory.getUnitOfMeasure();

		for (UnitOfMeasure unit : listUnitOfMeasure)
		{
			JednostkaMiary jm = WSToCanonicalMapper.MapUnitOfMeasureToJednostkaMiary(unit);

			listaJednostkaMiary.add(jm);
		}

		return listaJednostkaMiary;

	}
}
