package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.model.TypPlatnosci;
import pl.compan.docusafe.general.hibernate.model.PaymentTermsDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;

public class TypPlatnosciDBFactory {
	public List<TypPlatnosci> pobierzTypPlatnosciZBD() {
		List<TypPlatnosci> listaTypPlatnosci = new ArrayList<TypPlatnosci>();

		List<PaymentTermsDB> listaPaymentTermsDB;
		try {
			listaPaymentTermsDB = PaymentTermsDB.list();

			for (PaymentTermsDB pt : listaPaymentTermsDB) {
				TypPlatnosci tp = HibernateToCanonicalMapper
						.MapPaymentTermsToTypPlatnosci(pt);
				listaTypPlatnosci.add(tp);
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaTypPlatnosci;
	}

	public boolean Zapisz(TypPlatnosci tp)
	{

		PaymentTermsDB pt = new PaymentTermsDB();
		try {
			switch(tp.getStanObiektu())
			{
			case NOWY:
				pt = CanonicalToHibernateMapper.MapTypPlatnosciNaPaymentTerms(pt, tp);	
				pt.save();
				break;			
			case ZMODYFIKOWANY:
				List<PaymentTermsDB> paymentTerms = PaymentTermsDB.findByCnRefVal(tp.getCn(), tp.getObiektPowiazany());
				for(PaymentTermsDB ptDB:paymentTerms){
					pt = CanonicalToHibernateMapper.MapTypPlatnosciNaPaymentTerms(ptDB, tp);
					pt.save();
				}
				break;
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
}
