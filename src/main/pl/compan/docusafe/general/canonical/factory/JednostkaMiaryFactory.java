package pl.compan.docusafe.general.canonical.factory;

import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.JednostkaMiary;
import pl.compan.docusafe.general.hibernate.model.UnitOfMeasureDB;

public class JednostkaMiaryFactory {
	public List<JednostkaMiary> sprawdzJednostkaMiary(List<JednostkaMiary> nowy, List<JednostkaMiary> stary){
		HashSet<Long> jmSet=new HashSet<Long>();
		for(JednostkaMiary jednostkaMiary:nowy){
			try {
				List<UnitOfMeasureDB> unitOfMeasure = UnitOfMeasureDB.findByErpID(jednostkaMiary.getErpId());
				if(unitOfMeasure.isEmpty()){
					jednostkaMiary.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					jednostkaMiary.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
				}
				jednostkaMiary.setAktywna(true);
				jmSet.add(jednostkaMiary.getErpId());
			} catch (EdmException e) {
				e.printStackTrace();
			}

		}
		for(JednostkaMiary jednostkaMiary:nowy){
			if(jmSet.contains(jednostkaMiary.getErpId()))continue;
			jednostkaMiary.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			jednostkaMiary.setAktywna(false);
			nowy.add(jednostkaMiary);
		}
		return nowy;
	}
}
