package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.RoleWBudzecie;
import pl.compan.docusafe.general.dto.RoleWBudzecieDTO;
import pl.compan.docusafe.general.hibernate.dao.RoleWBudzecieDBDAO;
import pl.compan.docusafe.general.hibernate.model.RoleWBudzecieDB;

public class RoleWBudzecieFactory {
	/**
	 * Metoda pobiera liste rol w budzecie z BD
	 * 
	 */
	public List<RoleWBudzecie> pobierzRoleWBudzecieZBD() {

		RoleWBudzecieDTO roleWBudzecie = new RoleWBudzecieDTO();

		return roleWBudzecie.pobierzRoleWBudzecieZBD();
	}
	/**
	 * Metoda pobiera liste rol w budzecie z WS
	 * @throws RemoteException 
	 */
	public List<RoleWBudzecie> pobierzRoleWBudzecieZWS() throws RemoteException {

		RoleWBudzecieDTO roleWBudzecie = new RoleWBudzecieDTO();

		return roleWBudzecie.pobierzRoleWBudzecieZWS();
	}

	/**
	 * Metoda aktualizuje role w budzecie
	 * 
	 * @param RoleWBudzecieNowe
	 * @param RoleWBudzecieStare
	 */
	public List<RoleWBudzecie> aktualizujRoleWBudzecie(
			List<RoleWBudzecie> RoleWBudzecieNowe, List<RoleWBudzecie> RoleWBudzecieStare) {
		HashSet<String> roleSet=new HashSet<String>();
		for (RoleWBudzecie r : RoleWBudzecieNowe) {
			try {
				List<RoleWBudzecieDB> roleWBudzecieDB = RoleWBudzecieDBDAO.findByBudzetIdRolaOsoba(r.getBd_budzet_ko_id(), r.getBp_rola_id(), r.getOsoba_id());
				if (roleWBudzecieDB.isEmpty()) {
					r.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (RoleWBudzecieDB rDB : roleWBudzecieDB) {
						r.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						r.setId(rDB.getId());
					}
				}
				r.setAvailable(true);
				roleSet.add(r.getBd_budzet_ko_id()+":"+r.getBp_rola_id()+":"+r.getOsoba_id());
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		for (RoleWBudzecie r : RoleWBudzecieStare) {
			if(roleSet.contains(r.getBd_budzet_ko_id()+":"+r.getBp_rola_id()+":"+r.getOsoba_id()))continue;
			r.setAvailable(false);
			r.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			RoleWBudzecieNowe.add(r);
		}
		return RoleWBudzecieNowe;
	}

	/**
	 * Metoda zapisuje role w budzecie do bazy danych
	 * @param
	 */

	public void ZapiszRoleWBudzecieDoDB(List<RoleWBudzecie> listaRoleWBudzecie) {

		RoleWBudzecieDTO roleWBudzecie = new RoleWBudzecieDTO();

		roleWBudzecie.zapisz(listaRoleWBudzecie);
	}
}
