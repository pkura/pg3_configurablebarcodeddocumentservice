package pl.compan.docusafe.general.canonical.factory;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import pl.compan.docusafe.core.imports.simple.repository.DictionaryMap;
import pl.compan.docusafe.general.canonical.model.GeneralDictionary;
import pl.compan.docusafe.general.canonical.model.GeneralSimpleRepositoryAttribute;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;

public class GeneralCacheHandler {
	
	private static final Logger log = LoggerFactory.getLogger(GeneralCacheHandler.class);
	
	public enum DictionaryCacheType {
		CONTROL_DICT,ALL_DICTS;
	}
	
	private static final LoadingCache<DictionaryCacheType, List<GeneralDictionary>> dictionaryCache = CacheBuilder.newBuilder()
		  .softValues()
		  .expireAfterWrite(1, TimeUnit.HOURS)
		  .build(new DictionaryCacheLoader());

	private static final LoadingCache<String, List<GeneralDictionary>> dictionaryToDockindCache = CacheBuilder.newBuilder()
		  .softValues()
		  .expireAfterWrite(1, TimeUnit.HOURS)
		  .build(new DictionaryToDockindCacheLoader());
	
	private static final LoadingCache<DictionaryMapProperties, List<DictionaryMap>> dictionaryMapCache = CacheBuilder.newBuilder()
			.softValues()
			.expireAfterWrite(1, TimeUnit.HOURS)
			.build(new DictionaryMapCacheLoader());
	
	private static final LoadingCache<RepositoryProperties, List<GeneralSimpleRepositoryAttribute>> repositoryCache = CacheBuilder.newBuilder()
			  .softValues()
			  .expireAfterWrite(1, TimeUnit.HOURS)
			  .build(new RepositoryCacheLoader());

	private static class DictionaryCacheLoader extends CacheLoader<DictionaryCacheType, List<GeneralDictionary>> {
		
		@Override
		public List<GeneralDictionary> load(DictionaryCacheType type) throws Exception {
			GeneralDictionary instance = GeneralDictionary.getInstance();
			
			if (type == DictionaryCacheType.CONTROL_DICT) {
				return Lists.newArrayList(instance.findControlDict());
			}
			if (type == DictionaryCacheType.ALL_DICTS) {
				return instance.findAll(false);
			}
			return Lists.newArrayList();
		}
	}

	private static class DictionaryToDockindCacheLoader extends CacheLoader<String, List<GeneralDictionary>> {

		@Override
		public List<GeneralDictionary> load(String dockindCn) throws Exception {
			GeneralDictionary instance = GeneralDictionary.getInstance();

			return instance.findAll(false, dockindCn);
		}
	}
	
	private static class DictionaryMapCacheLoader extends CacheLoader<DictionaryMapProperties, List<DictionaryMap>> {

		@Override
		public List<DictionaryMap> load(DictionaryMapProperties mapProperty) throws Exception {
			DictionaryMap instance = DictionaryMap.getInstance();

			return instance.find(mapProperty.getControlValue(), mapProperty.getDockindCn());
		}
		
	}
	
	private static class RepositoryCacheLoader extends CacheLoader<RepositoryProperties, List<GeneralSimpleRepositoryAttribute>> {
		
		@Override
		public List<GeneralSimpleRepositoryAttribute> load(RepositoryProperties property) throws Exception {
			GeneralSimpleRepositoryAttribute instance = GeneralSimpleRepositoryAttribute.getInstance();
			if (property.getRepKlasaId() == null) {
				return instance.find(property.getObjectId(), property.getContextId());
			} else {
				return Lists.newArrayList(instance.find(property.getRepKlasaId(), property.getObjectId(), property.getContextId()));
			}
		}
	}

	public static List<GeneralDictionary> getAvailableDicts() {
		try {
			return dictionaryCache.get(DictionaryCacheType.ALL_DICTS);
		} catch (ExecutionException e) {
			log.error(e.getMessage(),e);
		}

		return Lists.newArrayList();
	}

	public static List<GeneralDictionary> getAvailableDicts(String dockindCn) {
		try {
			return dictionaryToDockindCache.get(dockindCn);
		} catch (ExecutionException e) {
			log.error(e.getMessage(),e);
		}

		return Lists.newArrayList();
	}

	public static GeneralDictionary getControlDict() {
		try {
			List<GeneralDictionary> results = dictionaryCache.get(DictionaryCacheType.CONTROL_DICT);
			if (!results.isEmpty()) {
				return results.get(0);
			}
		} catch (ExecutionException e) {
			log.error(e.getMessage(),e);
		}
		
		return null;
	}
	
	public static List<DictionaryMap> getDictionariesMappings(Integer controlValue, String dockindCn) {
		if (controlValue != null) {
			return getDictionariesMappings(controlValue.toString(), dockindCn);
		}
		
		return Lists.newArrayList();
	}
	
	public static List<DictionaryMap> getDictionariesMappings(String controlValue, String dockindCn) {
		if (controlValue != null && dockindCn != null) {
			try {
				return dictionaryMapCache.get(new DictionaryMapProperties(controlValue, dockindCn));
			} catch (ExecutionException e) {
				log.error(e.getMessage(),e);
			}
		}
		
		return Lists.newArrayList();
	}
	
	public static GeneralSimpleRepositoryAttribute getRepostioryDict(Long repKlasaId, Integer objectId, Integer contextId) {
		List<GeneralSimpleRepositoryAttribute> results;
		try {
			results = repositoryCache.get(new RepositoryProperties(repKlasaId, objectId, contextId));
			if (!results.isEmpty()) {
				return results.get(0);
			}
		} catch (ExecutionException e) {
			log.error(e.getMessage(),e);
		}
		
		return null;
	}
	
	public static List<GeneralSimpleRepositoryAttribute> getRepositoryDicts(Integer objectId, Integer contextId) {
		try {
			return repositoryCache.get(new RepositoryProperties(null, objectId, contextId));
		} catch (ExecutionException e) {
			log.error(e.getMessage(),e);
		}
		
		return Lists.newArrayList();
	}
	
	public static void cleanUpCache() {
		repositoryCache.invalidateAll();
		dictionaryMapCache.invalidateAll();
		dictionaryCache.invalidateAll();
		dictionaryToDockindCache.invalidateAll();
	}
	
}
class RepositoryProperties {
	private Long repKlasaId;
	private Integer objectId;
	private Integer contextId;

	public RepositoryProperties(Long repKlasaId, Integer objectId, Integer contextId) {
		this.repKlasaId = repKlasaId;
		this.objectId = objectId;
		this.contextId = contextId;
	}
	public Long getRepKlasaId() {
		return repKlasaId;
	}
	public void setRepKlasaId(Long repKlasaId) {
		this.repKlasaId = repKlasaId;
	}
	public Integer getObjectId() {
		return objectId;
	}
	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

    public Integer getContextId() {
        return contextId;
    }

    public void setContextId(Integer contextId) {
        this.contextId = contextId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RepositoryProperties that = (RepositoryProperties) o;

        if (contextId != null ? !contextId.equals(that.contextId) : that.contextId != null) return false;
        if (objectId != null ? !objectId.equals(that.objectId) : that.objectId != null) return false;
        if (repKlasaId != null ? !repKlasaId.equals(that.repKlasaId) : that.repKlasaId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = repKlasaId != null ? repKlasaId.hashCode() : 0;
        result = 31 * result + (objectId != null ? objectId.hashCode() : 0);
        result = 31 * result + (contextId != null ? contextId.hashCode() : 0);
        return result;
    }
}

class DictionaryMapProperties {
    private String controlValue;
    private String dockindCn;

    DictionaryMapProperties(String controlValue, String dockindCn) {
        this.controlValue = controlValue;
        this.dockindCn = dockindCn;
    }

    public String getControlValue() {
        return controlValue;
    }

    public void setControlValue(String controlValue) {
        this.controlValue = controlValue;
    }

    public String getDockindCn() {
        return dockindCn;
    }

    public void setDockindCn(String dockindCn) {
        this.dockindCn = dockindCn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DictionaryMapProperties that = (DictionaryMapProperties) o;

        if (!controlValue.equals(that.controlValue)) return false;
        if (!dockindCn.equals(that.dockindCn)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = controlValue.hashCode();
        result = 31 * result + dockindCn.hashCode();
        return result;
    }
}
