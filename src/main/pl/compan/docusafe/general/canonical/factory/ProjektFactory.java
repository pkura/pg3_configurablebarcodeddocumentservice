package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.Projekt;
import pl.compan.docusafe.general.dto.ProjektDTO;
import pl.compan.docusafe.general.hibernate.dao.ProjectDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProjectDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ProjektFactory {
	public static final Logger log = LoggerFactory.getLogger(ProjektFactory.class);

	/**
	 * Metoda pobiera projekty z BD
	 * 
	 * @return
	 */
	public List<Projekt> pobierzProjektZBD() {

		ProjektDTO projektDTO = new ProjektDTO();

		return projektDTO.pobierzProjektZBD();		
	}

	/**
	 * Metoda pobiera projekty z WS
	 * 
	 * @return
	 * @throws RemoteException 
	 */
	public List<Projekt>  PobierzProjektWS() throws RemoteException
	{
		ProjektDTO projektDTO = new ProjektDTO();

		return projektDTO.PobierzProjekt();		
	}
	/**
	 * Metoda aktualizuje projekty
	 * @param ProjektNowa
	 * @param ProjektStara
	 * @return
	 */
	public List<Projekt> aktualizujProjekt(List<Projekt> ProjektNowa, List<Projekt> ProjektStara){
		HashSet<Long> projSet=new HashSet<Long>();
		for(Projekt projekt:ProjektNowa){
			try {
				List<ProjectDB> project = ProjectDBDAO.findByErpID(projekt.getErpId());
				if(project.isEmpty()){
					projekt.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for(ProjectDB p:project){
						projekt.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						projekt.setId(p.getId());
					}
				}
				projekt.setAvailable(true);
				projSet.add(projekt.getErpId());
			} catch (EdmException e) {
				log.error("", e);
			}
		}
		for(Projekt projekt:ProjektStara){
			if(projSet.contains(projekt.getErpId()))continue;
			projekt.setAvailable(false);
			projekt.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			ProjektNowa.add(projekt);
		}
		return ProjektNowa;
	}

	/**
	 * Metoda zapisuje projekty do bazy danych
	 * 
	 * @param listaProjekt
	 */
	public void zapiszProjektDoBD(List<Projekt> listaProjekt) {

		ProjektDTO projektDTO = new ProjektDTO();

		projektDTO.Zapisz(listaProjekt);
	}
}
