package pl.compan.docusafe.general.canonical.factory;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.model.JednostkaMiary;
import pl.compan.docusafe.general.dto.JednostkaMiaryDTO;
import pl.compan.docusafe.general.hibernate.model.UnitOfMeasureDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;

public class JednostkaMiaryDBFactory {
	
	/**
	 * Zwraca liste jednostek organizacyjnych z bazy danych
	 * @return
	 */
	public List<JednostkaMiary> pobierzJednostkaMiaryZBD() {
		JednostkaMiaryDTO jednostkaMiaryDTO = new JednostkaMiaryDTO();
		
		return jednostkaMiaryDTO.pobierzJednostkaMiaryZBD();
	}

	public boolean Zapisz(JednostkaMiary jednostkaMiary)
	{
		UnitOfMeasureDB unitOfMeasureDB = new UnitOfMeasureDB();
		try {
			switch(jednostkaMiary.getStanObiektu())
			{
			case NOWY:
				unitOfMeasureDB = CanonicalToHibernateMapper.MapJednostkaMiaryNaUnitOfMeasureDB(unitOfMeasureDB, jednostkaMiary);	
				unitOfMeasureDB.save();
				break;			
			case ZMODYFIKOWANY:
				List<UnitOfMeasureDB> jmErpID = UnitOfMeasureDB.findByErpID(jednostkaMiary.getErpId());
				for(UnitOfMeasureDB uom:jmErpID){
					unitOfMeasureDB = CanonicalToHibernateMapper.MapJednostkaMiaryNaUnitOfMeasureDB(uom, jednostkaMiary);
					unitOfMeasureDB.save();
				}
				break;
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
}
