package pl.compan.docusafe.general.canonical.factory;

import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.MiejsceDostawy;
import pl.compan.docusafe.general.hibernate.model.DeliveryLocationDB;

public class MiejsceDostawyFactory {
	public List<MiejsceDostawy> polaczMiejsceDostawy(List<MiejsceDostawy> nowa, List<MiejsceDostawy> stara){
		HashSet<String> delSet=new HashSet<String>();
		for(MiejsceDostawy md:nowa){
			try {
				List<DeliveryLocationDB> deliveryLocation = DeliveryLocationDB.findByCn(md.getCn());
				if(deliveryLocation.isEmpty()){
					md.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					md.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
				}
				md.isAktywna(true);
				delSet.add(md.getCn());
			} catch (EdmException e) {
				e.printStackTrace();
			}

		}
		for(MiejsceDostawy md:stara){
			if(delSet.contains(md.getCn()))continue;
			md.isAktywna(false);
			md.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			nowa.add(md);
		}
		return nowa;
	}
}
