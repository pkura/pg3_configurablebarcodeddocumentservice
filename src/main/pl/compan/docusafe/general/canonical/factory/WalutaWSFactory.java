package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.canonical.model.Waluta;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Currency;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.OrganizationalUnit;

public class WalutaWSFactory {
	/**
	 * Metoda zwraca list� walut z WebSerwisu [WS]
	 * Metoda u�ywa dowolnego, skonfigurowanego webserwisu Simple DictionaryService - getCurrency
	 * @return
	 * @throws RemoteException 
	 */
	public List<Waluta>  PobierzWalute() throws RemoteException
	{
		List<Waluta> listaWalut = new ArrayList<Waluta>();
		
		
		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<Currency> listaCurrency = dictionaryServiceFactory.getCurrency();
				
		for (Currency unit : listaCurrency)
		{
			Waluta waluta = WSToCanonicalMapper.MapCurrencyToWaluta(unit);
			
			listaWalut.add(waluta);
		}
		
		return listaWalut;
		
	}
}
