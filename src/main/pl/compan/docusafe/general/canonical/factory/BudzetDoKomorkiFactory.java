package pl.compan.docusafe.general.canonical.factory;

import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.BudzetDoKomorki;
import pl.compan.docusafe.general.dto.BudzetDoKomorkiDTO;
import pl.compan.docusafe.general.hibernate.dao.BudzetDoKomorkiDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudzetDoKomorkiDB;


public class BudzetDoKomorkiFactory {
	/**
	 * Metoda pobiera liste  z BD
	 * 
	 */
	public List<BudzetDoKomorki> pobierzBudzetyDoKomorekZBD() {
		
		BudzetDoKomorkiDTO kontrahentKonto = new BudzetDoKomorkiDTO();
		
		return kontrahentKonto.pobierzBudzetyDoKomorekZBD();
	}
	/**
	 * Metoda pobiera liste z WS
	 */
	public List<BudzetDoKomorki> pobierzBudzetyDoKomorekZWS() {
		
		BudzetDoKomorkiDTO kontrahentKonto = new BudzetDoKomorkiDTO();
		
		return kontrahentKonto.pobierzBudzetyDoKomorkiZWS();
	}
	
	/**
	 * Metoda aktualizuje 
	 * 
	 * @param kontoNowe
	 * @param kontoStare
	 */
	public List<BudzetDoKomorki> aktualizujBudzetyDoKomorki(
			List<BudzetDoKomorki> kontoNowe, List<BudzetDoKomorki> kontoStare) {
		for (BudzetDoKomorki kk : kontoNowe) {
			try {
				List<BudzetDoKomorkiDB> konto = BudzetDoKomorkiDBDAO.findByBudzetId(kk.getBudzet_id());
				if (konto.isEmpty()) {
					kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (BudzetDoKomorkiDB kkDB : konto) {
						kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						kk.setId(kkDB.getId());
					}
				}
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		return kontoNowe;
	}
	
	/**
	 * Metoda zapisuje budzetow do bazy danych
	 * @param
	 */
	
	public void zapiszBudzetDoKomorkiDoDB(List<BudzetDoKomorki> listaBudzetowKoKomorek) {
		
		BudzetDoKomorkiDTO kontrahentKonto = new BudzetDoKomorkiDTO();
		
		kontrahentKonto.zapisz(listaBudzetowKoKomorek);
	}
}
