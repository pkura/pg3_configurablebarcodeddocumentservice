package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.general.canonical.model.TypPlatnosci;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PaymentTerms;

public class TypPlatnosciWSFactory {
	public List<TypPlatnosci>  PobierzTypPlatnosci() throws RemoteException
	{
		List<TypPlatnosci> listaTypPtatnosci = new ArrayList<TypPlatnosci>();
		
		
		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<PaymentTerms> listaPaymentTerms = dictionaryServiceFactory.getPaymentTerms();
				
		for (PaymentTerms unit : listaPaymentTerms)
		{
			TypPlatnosci tp = WSToCanonicalMapper.MapPaymentTermsToTypPlatnosci(unit);
			
			listaTypPtatnosci.add(tp);
		}
		
		return listaTypPtatnosci;
		
	}
}
