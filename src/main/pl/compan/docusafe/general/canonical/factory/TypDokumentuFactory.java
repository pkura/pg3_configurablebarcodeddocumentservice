package pl.compan.docusafe.general.canonical.factory;

import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.TypDokumentu;
import pl.compan.docusafe.general.hibernate.model.PurchaseDocumentTypeDB;

public class TypDokumentuFactory {
	public List<TypDokumentu> polaczTypDokumentu(List<TypDokumentu> nowy, List<TypDokumentu> stary){
		HashSet<Long> tdSet=new HashSet<Long>();
		for(TypDokumentu td:nowy){
			try {
				List<PurchaseDocumentTypeDB> paymentTerms = PurchaseDocumentTypeDB.findByDokzakID(td.getTypdokzak_id());
				if(paymentTerms.isEmpty()){
					td.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					td.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
				}
				td.setAktywna(true);
				tdSet.add(td.getTypdokzak_id());
			} catch (EdmException e) {
				e.printStackTrace();
			}

		}
		for(TypDokumentu td:stary){
			if(tdSet.contains(td.getTypdokzak_id()))continue;
			td.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			td.setAktywna(false);
			nowy.add(td);
		}
		return nowy;
	}
}
