package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.general.canonical.model.MiejsceDostawy;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.DeliveryLocation;

public class MiejsceDostawyWSFactory {
	public List<MiejsceDostawy>  PobierzMiejsceDostawy() throws RemoteException
	{
		List<MiejsceDostawy> listaMiejscDostawy = new ArrayList<MiejsceDostawy>();
		
		
		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<DeliveryLocation> listaDeliveryLocation = dictionaryServiceFactory.getDeliveryLocation();
				
		for (DeliveryLocation unit : listaDeliveryLocation)
		{
			MiejsceDostawy miejsceDostawy = WSToCanonicalMapper.MapDeliveryLocationToMiejsceDostawy(unit);
			
			listaMiejscDostawy.add(miejsceDostawy);
		}
		
		return listaMiejscDostawy;
		
	}
}

