package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.dao.JednostkaOrganizacyjnaDAO;
import pl.compan.docusafe.general.canonical.dao.WalutaDAO;
import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.canonical.model.Waluta;
import pl.compan.docusafe.general.hibernate.model.CurrencyDB;
import pl.compan.docusafe.general.mapers.MapperUtils;

public class WalutaFactory {


	public List<Waluta> polaczWaluty(List<Waluta> walutaNowa, List<Waluta> walutaStara){
		HashSet<String> walutaNowaSet=new HashSet<String>();
		for(Waluta wal:walutaNowa){
			try {
				List<CurrencyDB> currency = CurrencyDB.findByCn(wal.getCn());
				if(currency.isEmpty()){
					wal.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					wal.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
				}
				wal.isAktywna(true);
				walutaNowaSet.add(wal.getCn());
			} catch (EdmException e) {
				e.printStackTrace();
			}

		}//dodanie walut, ktore byly, a nie wystepuja juz w serwisie
		for(Waluta wal:walutaStara){
			if(walutaNowaSet.contains(wal.getCn()))continue;
			wal.isAktywna(false);
			wal.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			walutaNowa.add(wal);
		}
		
		return walutaNowa;
	}
}
