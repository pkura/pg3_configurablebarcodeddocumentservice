package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.general.canonical.model.StawkaVat;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.VatRate;

public class StawkaVatWSFactory {
	public List<StawkaVat>  PobierzStawkaVat() throws RemoteException
	{
		List<StawkaVat> listaStawkaVat = new ArrayList<StawkaVat>();
		
		
		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<VatRate> listVatRate = dictionaryServiceFactory.getVatRate();
				
		for (VatRate unit : listVatRate)
		{
			StawkaVat sv = WSToCanonicalMapper.MapVatRateToStawkaVat(unit);
			
			listaStawkaVat.add(sv);
		}
		
		return listaStawkaVat;
		
	}
}
