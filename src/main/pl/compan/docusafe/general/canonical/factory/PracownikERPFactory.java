package pl.compan.docusafe.general.canonical.factory;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.Pracownik;
import pl.compan.docusafe.general.dto.PracownikERPDTO;
import pl.compan.docusafe.general.hibernate.dao.PracownikDBDAO;
import pl.compan.docusafe.general.hibernate.model.PracownikDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PracownikERPFactory {
	public static final Logger log = LoggerFactory.getLogger(PracownikERPFactory.class);

	/**
	 * Metoda pobiera pracownikow z BD
	 * 
	 * @return
	 */
	public List<Pracownik> pobierzPracownikZBD() {

		PracownikERPDTO pracownikERPDTO = new PracownikERPDTO();

		return pracownikERPDTO.pobierzPracownikowZBD();		
	}

	/**
	 * Metoda pobiera pracownikow z WS
	 * 
	 * @return
	 */
	public List<Pracownik>  PobierzPracownikWS()
	{
		PracownikERPDTO pracownikERPDTO = new PracownikERPDTO();

		return pracownikERPDTO.PobierzPracownikow();		
	}
	/**
	 * Metoda aktualizuje pracownikow
	 * @param PracownikNowa
	 * @param PracownikStara
	 * @return
	 */
	public List<Pracownik> aktualizujPracownikow(List<Pracownik> PracownikNowa, List<Pracownik> PracownikStara){

		for(Pracownik pracownik:PracownikNowa){
			try {
				List<PracownikDB> pracownikDB = PracownikDBDAO.findByNREWID(pracownik.getNrewid());
				if(pracownikDB.isEmpty()){
					pracownik.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for(PracownikDB p:pracownikDB){
						pracownik.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						pracownik.setId(p.getId());
					}
				}
			} catch (EdmException e) {
				log.error("", e);
			}
		}
		return PracownikNowa;
	}

	/**
	 * Metoda zapisuje pracownikow do bazy danych
	 * 
	 * @param listaPracownikow
	 */
	public void zapiszPracownikowDoBD(List<Pracownik> listaPracownikow) {

		PracownikERPDTO pracownikERPDTO = new PracownikERPDTO();

		pracownikERPDTO.Zapisz(listaPracownikow);
	}
}
