package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.Zrodlo;
import pl.compan.docusafe.general.dto.ZrodloDTO;
import pl.compan.docusafe.general.hibernate.dao.ZrodloDBDAO;
import pl.compan.docusafe.general.hibernate.model.ZrodloDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZrodloFactory {
	public static final Logger log = LoggerFactory.getLogger(ZrodloFactory.class);

	/**
	 * Metoda pobiera zrodlo z BD
	 * 
	 * @return
	 */
	public List<Zrodlo> pobierzZrodloZBD() {

		ZrodloDTO zrodloDTO = new ZrodloDTO();

		return zrodloDTO.pobierzZrodloZBD();		
	}

	/**
	 * Metoda pobiera zrodlo z WS
	 * 
	 * @return
	 * @throws RemoteException 
	 */
	public List<Zrodlo>  PobierzZrodloWS() throws RemoteException
	{
		ZrodloDTO zrodloDTO = new ZrodloDTO();

		return zrodloDTO.PobierzZrodlo();		
	}
	/**
	 * Metoda aktualizuje zrodlo 
	 * @param zrodloNowa
	 * @param zrodloStara
	 * @return
	 */
	public List<Zrodlo> aktualizujZrodlo(List<Zrodlo> zrodloNowa, List<Zrodlo> zrodloStara){
		HashSet<Long> sofSet=new HashSet<Long>();
		for(Zrodlo zrodlo:zrodloNowa){
			try {
				List<ZrodloDB> zrodloDB = ZrodloDBDAO.findByErpID(zrodlo.getErpId());
				if(zrodloDB.isEmpty()){
					zrodlo.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for(ZrodloDB z:zrodloDB){
						zrodlo.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						zrodlo.setId(z.getId());
					}
				}
				zrodlo.setAvailable(true);
				sofSet.add(zrodlo.getErpId());
			} catch (EdmException e) {
				log.error("", e);
			}
		}
		for(Zrodlo z:zrodloStara){
			if(sofSet.contains(z.getErpId()))continue;
			z.setAvailable(false);
			z.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			zrodloNowa.add(z);
		}
		return zrodloNowa;
	}

	/**
	 * Metoda zapisuje zrodlo do bazy danych
	 * 
	 * @param listaZrodlo
	 */
	public void zapiszZrodloDoBD(List<Zrodlo> listaZrodlo) {

		ZrodloDTO zrodloDTO = new ZrodloDTO();

		zrodloDTO.Zapisz(listaZrodlo);
	}
}
