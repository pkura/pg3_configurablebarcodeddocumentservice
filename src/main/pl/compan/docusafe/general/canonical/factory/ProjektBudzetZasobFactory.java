package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetZasob;
import pl.compan.docusafe.general.dto.ProjektBudzetuZasobDTO;
import pl.compan.docusafe.general.hibernate.dao.ProjectBudgetItemResourceDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetZasobDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ProjektBudzetZasobFactory {
	public static final Logger log = LoggerFactory.getLogger(ProjektBudzetZasobFactory.class);

	/**
	 * Metoda pobiera zasoby projektow z budzetow z BD
	 * 
	 * @return
	 */
	public List<ProjektBudzetZasob> pobierzProjektBudzetZasobZBD() {

		ProjektBudzetuZasobDTO projektBudzetuZasobDTO = new ProjektBudzetuZasobDTO();

		return projektBudzetuZasobDTO.pobierzProjektBudzetuZasobZBD();		
	}

	/**
	 * Metoda pobiera zasoby projektow z budzetow z WS
	 * 
	 * @return
	 * @throws RemoteException 
	 */
	public List<ProjektBudzetZasob>  PobierzProjektBudzetZasobWS() throws RemoteException
	{
		ProjektBudzetuZasobDTO projektBudzetuZasobDTO = new ProjektBudzetuZasobDTO();

		return projektBudzetuZasobDTO.PobierzProjektBudzetZasob();		
	}
	/**
	 * Metoda aktualizuje zasoby projektow z budzetow
	 * @param ProjektBudzetZasobNowa
	 * @param ProjektBudzetZasobStara
	 * @return
	 */
	public List<ProjektBudzetZasob> aktualizujProjektBudzetZasob(List<ProjektBudzetZasob> ProjektBudzetZasobNowa, List<ProjektBudzetZasob> ProjektBudzetZasobStara){
		HashSet<Long> budzetySet=new HashSet<Long>();
		for(ProjektBudzetZasob projektBudzetZasob:ProjektBudzetZasobNowa){
			try {
				List<ProjektBudzetZasobDB> projectBudgetItemResourceDB = ProjectBudgetItemResourceDBDAO.findByErpID(projektBudzetZasob.getErpId());
				if(projectBudgetItemResourceDB.isEmpty()){
					projektBudzetZasob.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for(ProjektBudzetZasobDB pb:projectBudgetItemResourceDB){
						projektBudzetZasob.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						projektBudzetZasob.setId(pb.getId());
					}
				}
				projektBudzetZasob.setAvailable(true);
				budzetySet.add(projektBudzetZasob.getErpId());
			} catch (EdmException e) {
				log.error("", e);
			}
		}
		for(ProjektBudzetZasob projektBudzetZasob:ProjektBudzetZasobNowa){
			if(budzetySet.contains(projektBudzetZasob.getErpId()))continue;
			projektBudzetZasob.setAvailable(false);
			projektBudzetZasob.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			ProjektBudzetZasobNowa.add(projektBudzetZasob);
			
		}
		return ProjektBudzetZasobNowa;
	}

	/**
	 * Metoda zapisuje zasoby projektow z budzetow do bazy danych
	 * 
	 * @param listaProjektBudzetZasob
	 */
	public void zapiszProjektBudzetZasobDoBD(List<ProjektBudzetZasob> listaProjektBudzetZasob) {

		ProjektBudzetuZasobDTO projektBudzetZasobDTO = new ProjektBudzetuZasobDTO();

		projektBudzetZasobDTO.Zapisz(listaProjektBudzetZasob);
	}
}
