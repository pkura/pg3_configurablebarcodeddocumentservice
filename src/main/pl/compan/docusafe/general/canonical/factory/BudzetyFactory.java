package pl.compan.docusafe.general.canonical.factory;

import java.util.List;

import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.Budzet;
import pl.compan.docusafe.general.dto.BudzetDTO;
import pl.compan.docusafe.general.hibernate.dao.BudgetViewDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudgetViewDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BudzetyFactory {
	
	public static final Logger log = LoggerFactory.getLogger(BudzetyFactory.class);

	/**
	 * Metoda pobiera budzety z BD
	 * 
	 * @return
	 */
	public List<Budzet> pobierzBudzetZBD() {

		BudzetDTO budzetDTO = new BudzetDTO();

		return budzetDTO.pobierzBudzetZBD();		
	}

	/**
	 * Metoda pobiera budzety z WS
	 * 
	 * @return
	 */
	public List<Budzet>  PobierzBudzetWS()
	{
		BudzetDTO budzetDTO = new BudzetDTO();

		return budzetDTO.PobierzBudzety();		
	}
	/**
	 * Metoda aktualizuje budzety
	 * @param BudzetNowa
	 * @param BudzetStara
	 * @return
	 */
	public List<Budzet> aktualizujBudzet(List<Budzet> BudzetNowa, List<Budzet> BudzetStara){

		for(Budzet budzet:BudzetNowa){
			BudgetViewDB budgetView = BudgetViewDBDAO.findByBudzetIdAndZrodloId(budzet.getBudzetId(), budzet.getZrodloId());
			if(budgetView == null){
				budzet.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
			} else {
				budzet.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
				budzet.setId(budgetView.getId());
			}
		}
		return BudzetNowa;
	}

	/**
	 * Metoda zapisuje budzety do bazy danych
	 * 
	 * @param listaBudzet
	 */
	public void zapiszBudzetDoBD(List<Budzet> listaBudzet) {

		BudzetDTO budzetDTO = new BudzetDTO();

		budzetDTO.Zapisz(listaBudzet);
	}
}
