package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.general.canonical.model.TypDokumentu;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PurchaseDocumentType;

public class TypDokumentuWSFactory {
	public List<TypDokumentu>  PobierzTypDokumentu() throws RemoteException
	{
		List<TypDokumentu> listaTypDokumentu = new ArrayList<TypDokumentu>();
		
		
		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<PurchaseDocumentType> listPurchaseDocumentType = dictionaryServiceFactory.getPurchaseDocumentType();
				
		for (PurchaseDocumentType unit : listPurchaseDocumentType)
		{
			TypDokumentu td = WSToCanonicalMapper.MapPurchaseDocumentTypeToTypDostawy(unit);
			
			listaTypDokumentu.add(td);
		}
		
		return listaTypDokumentu;
		
	}
}
