package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.model.TypDokumentu;
import pl.compan.docusafe.general.canonical.model.TypPlatnosci;
import pl.compan.docusafe.general.hibernate.model.PaymentTermsDB;
import pl.compan.docusafe.general.hibernate.model.PurchaseDocumentTypeDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;

public class TypDokumentuDBFactory {
	public List<TypDokumentu> pobierzTypDokumentuZBD() {
		List<TypDokumentu> listaTypDokumentu = new ArrayList<TypDokumentu>();

		List<PurchaseDocumentTypeDB> listPurchaseDocumentTypeDB;
		try {
			listPurchaseDocumentTypeDB = PurchaseDocumentTypeDB.list();

			for (PurchaseDocumentTypeDB pd : listPurchaseDocumentTypeDB) {
				TypDokumentu td = HibernateToCanonicalMapper
						.MapPurchaseDocumentTypeDBToTypDokumentu(pd);
				listaTypDokumentu.add(td);
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaTypDokumentu;
	}

	public boolean Zapisz(TypDokumentu td)
	{

		PurchaseDocumentTypeDB pd = new PurchaseDocumentTypeDB();
		try {
			switch(td.getStanObiektu())
			{
			case NOWY:
				pd = CanonicalToHibernateMapper.MapTypDokumentuToPurchaseDocumentTypeDB(pd, td);	
				pd.save();
				break;			
			case ZMODYFIKOWANY:
				List<PurchaseDocumentTypeDB> purchaseDocumentType = PurchaseDocumentTypeDB.findByDokzakID(td.getTypdokzak_id());
				for(PurchaseDocumentTypeDB pdDB:purchaseDocumentType){
					pd = CanonicalToHibernateMapper.MapTypDokumentuToPurchaseDocumentTypeDB(pdDB, td);
					pd.save();
				}
				break;
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
}
