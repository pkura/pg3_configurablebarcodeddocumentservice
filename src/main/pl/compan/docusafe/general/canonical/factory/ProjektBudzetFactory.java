package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.ProjektBudzet;
import pl.compan.docusafe.general.dto.ProjektBudzetDTO;
import pl.compan.docusafe.general.hibernate.dao.ProjektBudzetDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ProjektBudzetFactory {
	public static final Logger log = LoggerFactory.getLogger(ProjektBudzetFactory.class);

	/**
	 * Metoda pobiera projekty budzetow z BD
	 * 
	 * @return
	 */
	public List<ProjektBudzet> pobierzProjektBudzetZBD() {

		ProjektBudzetDTO projektBudzetDTO = new ProjektBudzetDTO();

		return projektBudzetDTO.pobierzProjektBudzetZBD();		
	}

	/**
	 * Metoda pobiera projekty budzetow z WS
	 * 
	 * @return
	 * @throws RemoteException 
	 */
	public List<ProjektBudzet>  PobierzProjektBudzetWS() throws RemoteException
	{
		ProjektBudzetDTO projektBudzetDTO = new ProjektBudzetDTO();

		return projektBudzetDTO.PobierzProjektBudzet();		
	}
	/**
	 * Metoda aktualizuje projekty budzetow
	 * @param ProjektBudzetNowa
	 * @param ProjektBudzetStara
	 * @return
	 */
	public List<ProjektBudzet> aktualizujProjektBudzet(List<ProjektBudzet> ProjektBudzetNowa, List<ProjektBudzet> ProjektBudzetStara){
		HashSet<Long> budzetySet=new HashSet<Long>();
		for(ProjektBudzet projektBudzet:ProjektBudzetNowa){
			try {
				List<ProjektBudzetDB> projectBD = ProjektBudzetDBDAO.findByErpID(projektBudzet.getErpId());
				if(projectBD.isEmpty()){
					projektBudzet.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for(ProjektBudzetDB p:projectBD){
						projektBudzet.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						projektBudzet.setId(p.getId());
					}
				}
				projektBudzet.setAvailable(true);
				budzetySet.add(projektBudzet.getErpId());
			} catch (EdmException e) {
				log.error("", e);
			}
		}
		for(ProjektBudzet projektBudzet:ProjektBudzetStara){
			if(budzetySet.contains(projektBudzet.getErpId()))continue;
			projektBudzet.setAvailable(false);
			projektBudzet.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			ProjektBudzetNowa.add(projektBudzet);
		}
		return ProjektBudzetNowa;
	}

	/**
	 * Metoda zapisuje projekty budzetow do bazy danych
	 * 
	 * @param listaProjekt
	 */
	public void zapiszProjektBudzetDoBD(List<ProjektBudzet> listaProjektBudzet) {

		ProjektBudzetDTO projektBudzetDTO = new ProjektBudzetDTO();

		projektBudzetDTO.Zapisz(listaProjektBudzet);
	}
}
