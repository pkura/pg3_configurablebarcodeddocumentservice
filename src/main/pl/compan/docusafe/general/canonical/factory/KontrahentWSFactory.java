package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.general.canonical.model.Kontrahent;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Supplier;

public class KontrahentWSFactory {
	public List<Kontrahent>  PobierzKontrahentow()
	{
		List<Kontrahent> listaKontrahentow = new ArrayList<Kontrahent>();
		
		
		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<Supplier> listSupplier = dictionaryServiceFactory.getSupplier();
				
		for (Supplier unit : listSupplier)
		{
			Kontrahent tp = WSToCanonicalMapper.MapSupplierToKontrahent(unit);
			
			listaKontrahentow.add(tp);
		}
		
		return listaKontrahentow;
		
	}
}
