package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.NumeryZamowien;
import pl.compan.docusafe.general.dto.NumeryZamowienDTO;
import pl.compan.docusafe.general.hibernate.dao.NumeryZamowienDBDAO;
import pl.compan.docusafe.general.hibernate.model.NumeryZamowienDB;

public class NumeryZamowienFactory {

	/**
	 * Metoda pobiera liste numerow zamowien z BD
	 * 
	 */
	public List<NumeryZamowien> pobierzNumeryZamowienZBD() {
		
		NumeryZamowienDTO kontrahentKonto = new NumeryZamowienDTO();
		
		return kontrahentKonto.pobierzNumeryZamowienZBD();
	}
	/**
	 * Metoda pobiera liste numerow zamowien z WS
	 * @throws RemoteException 
	 */
	public List<NumeryZamowien> pobierzNumeryZamowienZWS() throws RemoteException {
		
		NumeryZamowienDTO kontrahentKonto = new NumeryZamowienDTO();
		
		return kontrahentKonto.pobierzNumeryZamowienZWS();
	}
	
	/**
	 * Metoda aktualizuje numery zamowien
	 * 
	 * @param numeryZamowienNowe
	 * @param numeryZamowienStare
	 */
	public List<NumeryZamowien> aktualizujNumeryZamowien(
			List<NumeryZamowien> numeryZamowienNowe, List<NumeryZamowien> numeryZamowienStare) {
		HashSet<Long> zamSet=new HashSet<Long>();
		for (NumeryZamowien kk : numeryZamowienNowe) {
			try {
				List<NumeryZamowienDB> konto = NumeryZamowienDBDAO.findByErpID(kk.getErpId());
				if (konto.isEmpty()) {
					kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (NumeryZamowienDB kkDB : konto) {
						kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						kk.setId(kkDB.getId());
					}
				}
				
				kk.setAvailable(true);
				zamSet.add(kk.getErpId());
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		for (NumeryZamowien kk : numeryZamowienStare) {
			if(zamSet.contains(kk.getErpId()))continue;
			kk.setAvailable(false);
			kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			numeryZamowienNowe.add(kk);
		}
		return numeryZamowienNowe;
	}
	
	/**
	 * Metoda zapisuje numery zamowien do bazy danych
	 * @param
	 */
	
	public void zapiszNumeryZamowienDoDB(List<NumeryZamowien> listaBudzetowKoKomorek) {
		
		NumeryZamowienDTO kontrahentKonto = new NumeryZamowienDTO();
		
		kontrahentKonto.zapisz(listaBudzetowKoKomorek);
	}
}
