package pl.compan.docusafe.general.canonical.factory;

import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.KontrahentKonto;
import pl.compan.docusafe.general.dto.KontrahentKontoDTO;
import pl.compan.docusafe.general.hibernate.dao.KontrahentKontoDBDAO;
import pl.compan.docusafe.general.hibernate.model.KontrahentKontoDB;

public class KontrahentKontoFactory {

	/**
	 * Metoda pobiera liste kont bankowych z BD
	 * 
	 */
	public List<KontrahentKonto> pobierzKontaBankoweZBD() {
		
		KontrahentKontoDTO kontrahentKonto = new KontrahentKontoDTO();
		
		return kontrahentKonto.pobierzKontoZBD();
	}
	/**
	 * Metoda pobiera liste kont bankowych z WS
	 */
	public List<KontrahentKonto> pobierzKontaBankoweZWS() {
		
		KontrahentKontoDTO kontrahentKonto = new KontrahentKontoDTO();
		
		return kontrahentKonto.pobierzKontaZWS();
	}
	
	/**
	 * Metoda aktualizuje konta bankowe
	 * 
	 * @param kontoNowe
	 * @param kontoStare
	 */
	public List<KontrahentKonto> aktualizujKontaBankowe(
			List<KontrahentKonto> kontoNowe, List<KontrahentKonto> kontoStare) {
		for (KontrahentKonto kk : kontoNowe) {
			try {
				List<KontrahentKontoDB> konto = KontrahentKontoDBDAO.findByErpID(kk.getErpId());
				if (konto.isEmpty()) {
					kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (KontrahentKontoDB kkDB : konto) {
						kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						kk.setId(kkDB.getId());
					}
				}
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		return kontoNowe;
	}
	
	/**
	 * Metoda zapisuje konta bankowe do bazy danych
	 * @param
	 */
	
	public void ZapiszKontaBankoweDoDB(List<KontrahentKonto> listaKontrahentKonto) {
		
		KontrahentKontoDTO kontrahentKonto = new KontrahentKontoDTO();
		
		kontrahentKonto.zapisz(listaKontrahentKonto);
	}
}
