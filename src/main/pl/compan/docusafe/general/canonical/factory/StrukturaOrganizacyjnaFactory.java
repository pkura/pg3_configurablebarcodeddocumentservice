package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class StrukturaOrganizacyjnaFactory {
    public static final Logger log = LoggerFactory.getLogger(StrukturaOrganizacyjnaFactory.class);
	/**
	 * Metoda pobiera dwie struktury organizacyjne: nowa oraz stara. Wynikiem
	 * jest po��czona struktura organizacyjna.
	 * 
	 * @param nowa
	 *            - struktura organizacyjna nowa
	 * @param stara
	 *            - struktura organizacyjna aktualna / stara
	 * @return
	 */
	public List<JednostkaOrganizacyjna> PolaczStrukturyOrganizacyjne(
			List<JednostkaOrganizacyjna> nowa,
			List<JednostkaOrganizacyjna> stara) {
        log.info("PolaczStrukturyOrganizacyjne init");
		List<JednostkaOrganizacyjna> polaczona = new ArrayList<JednostkaOrganizacyjna>();

		// Mapa zawieraj�ca now� struktur�, kluczem jest Idm - zewn�trzny
		HashMap<String, JednostkaOrganizacyjna> _nowaMap = new HashMap<String, JednostkaOrganizacyjna>();
		for (JednostkaOrganizacyjna jednostka : nowa) {
            log.info("nowa {}", jednostka);
			_nowaMap.put(jednostka.getIdn(), jednostka);
		}

		// Mapa zawieraj�ca star� aktualnie obowi�zujac� struktur� z bazy danych
		// - kluczem Idm - zewn�trzny
		HashMap<String, JednostkaOrganizacyjna> _staraMap = new HashMap<String, JednostkaOrganizacyjna>();
		for (JednostkaOrganizacyjna jednostka : stara) {
            log.info("stara {}", jednostka);
			_staraMap.put(jednostka.getIdn(), jednostka);
		}

		// Mapa zawieraj�ca finaln� - po��czon� - struktur�
		HashMap<String, JednostkaOrganizacyjna> _polaczonaMap = new HashMap<String, JednostkaOrganizacyjna>();

		// Przegl�damy wszystkie elementy _nowaMap i aktualizujemy

		Iterator<String> _nowaMapIterator = _nowaMap.keySet().iterator();

		while (_nowaMapIterator.hasNext()) {

			String key = _nowaMapIterator.next();

			JednostkaOrganizacyjna nowaJO = _nowaMap.get(key);

			// Sprawdzenie, czy taka jednostka ju� jest w starej strukturze
			JednostkaOrganizacyjna staraJO = _staraMap.get(key);
			
			// Jednostki nie ma
			if (staraJO == null) {
				// Dodajemy t� jednostk� do tablicy struktury
				nowaJO.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				// Dla nowych generujemy guid automatycznie, za wyj�tkiem tych, kt�re s� rootdivision (maj� pustego rodzica)
				if (nowaJO.getRodzicIdn() == null)
				{
					nowaJO.setGuid(DSDivision.ROOT_GUID);				
				}
				else
				{
					UUID guid = UUID.randomUUID();
					nowaJO.setGuid(guid.toString());
				}

				nowaJO.setAktywna(true);
				
				_polaczonaMap.put(nowaJO.getIdn(), nowaJO);

			} else {
				// Jednostka jest - aktualizujemy co najwy�ej - inaczej m�wi�c
				// zast�pujemy nowym obiektem
				staraJO.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
				staraJO.setAktywna(true);
				_polaczonaMap.put(staraJO.getIdn(), staraJO);
				// Skoro ju� obs�u�yli�my t� jednostk�, to usuwamy j� z mapy
				// starej
				_staraMap.remove(staraJO.getIdn());
			}
		}

		// Teraz pozosta�o�ci mapy starej przej�ymy i zaktualizujemy aktualn�
		// map'e

		Iterator<String> _staraMapIterator = _staraMap.keySet().iterator();

		while (_staraMapIterator.hasNext()) {

			String key = _staraMapIterator.next();

			// Ka�dy element wrzucamy do po��czonej mapy

			JednostkaOrganizacyjna _staraJO = _staraMap.get(key);
			_staraJO.setAktywna(false);
			
			_polaczonaMap.put(key, _staraJO);
		}

		Iterator<String> _polaczonaMapIterator = _polaczonaMap.keySet()
				.iterator();

		while (_polaczonaMapIterator.hasNext()) {

			String key = _polaczonaMapIterator.next();

			JednostkaOrganizacyjna _polaczonaJO = _polaczonaMap.get(key);
            log.info("polaczona {}", _polaczonaJO);
			polaczona.add(_polaczonaJO);
		}

        log.info("koniec PolaczStrukturyOrganizacyjne");
		return polaczona;
	}

}
