package pl.compan.docusafe.general.canonical.factory;

import java.util.List;

import org.jfree.util.Log;


import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.BudzetKOKomorek;
import pl.compan.docusafe.general.canonical.model.NumeryZamowien;
import pl.compan.docusafe.general.canonical.model.ZamowienieSimplePozycje;
import pl.compan.docusafe.general.dto.NumeryZamowienDTO;
import pl.compan.docusafe.general.dto.ZamowienieSimplePozycjeDTO;
import pl.compan.docusafe.general.hibernate.dao.BudzetKOKomorkaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.ZamowienieSimplePozycjeDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudzetKOKomorekDB;
import pl.compan.docusafe.general.hibernate.model.ZamowienieSimplePozycjeDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZamowienieSimplePozycjeFactory {
	public static final Logger log = LoggerFactory.getLogger(JednostkaOrganizacyjnaFactory.class);
		
	
	/**
	 * Metoda zwraca wczytan� z bazy danych list� pozycji zamowien.
	 * @return
	 */
	public List<ZamowienieSimplePozycje> pobierzZamowienieSimplePozycjeZBD() {
		ZamowienieSimplePozycjeDTO jednostkaOrganizacyjnaDTO = new ZamowienieSimplePozycjeDTO();
		return jednostkaOrganizacyjnaDTO.pobierzZamPozycjeZBD();		
	}

	/**
	 * Metoda aktualizuje pozycjie zamowien
	 * 
	 * @param kontoNowe
	 * @param kontoStare
	 */
	public List<ZamowienieSimplePozycje> aktualizujZamowienieSimplePozycje(
			List<ZamowienieSimplePozycje> kontoNowe, List<ZamowienieSimplePozycje> kontoStare) {
		for (ZamowienieSimplePozycje kk : kontoNowe) {
			try {
				List<ZamowienieSimplePozycjeDB> konto = ZamowienieSimplePozycjeDBDAO.findByZamDosPozId(kk.getZamdospoz_id());
				if (konto.isEmpty()) {
					kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (ZamowienieSimplePozycjeDB kkDB : konto) {
						kk.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						kk.setId(kkDB.getId());
					}
				}
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		return kontoNowe;
	}
	
	
	/**
	 * Metoda zapisuje numery zamowien do bazy danych
	 * @param
	 */
	
	public void zapiszZamowienieSimplePozycjeDoDB(List<ZamowienieSimplePozycje> listaBudzetowKoKomorek) {
		
		ZamowienieSimplePozycjeDTO kontrahentKonto = new ZamowienieSimplePozycjeDTO();
		
		kontrahentKonto.zapisz(listaBudzetowKoKomorek);
	}

}
