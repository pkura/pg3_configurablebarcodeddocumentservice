package pl.compan.docusafe.general.canonical.factory;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.model.StawkaVat;
import pl.compan.docusafe.general.canonical.model.TypDokumentu;
import pl.compan.docusafe.general.hibernate.model.PurchaseDocumentTypeDB;
import pl.compan.docusafe.general.hibernate.model.VatRateDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;

public class StawkaVatDBFactory {
	public List<StawkaVat> pobierzStawkaVatZBD() {
		List<StawkaVat> listaStawkaVat = new ArrayList<StawkaVat>();

		List<VatRateDB> listVatRateDB;
		try {
			listVatRateDB = VatRateDB.list();

			for (VatRateDB vr : listVatRateDB) {
				StawkaVat sv = HibernateToCanonicalMapper
						.MapVatRateDBToStawkaVat(vr);
				listaStawkaVat.add(sv);
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaStawkaVat;
	}

	public boolean Zapisz(StawkaVat sv)
	{

		VatRateDB vr = new VatRateDB();
		try {
			switch(sv.getStanObiektu())
			{
			case NOWY:
				vr = CanonicalToHibernateMapper.MapStawkaVatToVatRateDB(vr, sv);	
				vr.save();
				break;			
			case ZMODYFIKOWANY:
				List<VatRateDB> vatRate = VatRateDB.findByErpID(sv.getErp_id());
				for(VatRateDB vrDB:vatRate){
					vr = CanonicalToHibernateMapper.MapStawkaVatToVatRateDB(vrDB, sv);
					vr.save();
				}
				break;
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
}
