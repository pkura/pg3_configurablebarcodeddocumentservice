package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.PlanZakupow;
import pl.compan.docusafe.general.dto.PlanZakupowDTO;
import pl.compan.docusafe.general.hibernate.dao.PlanZakupowDBDAO;
import pl.compan.docusafe.general.hibernate.model.PlanZakupowDB;

public class PlanZakupowFactory {
	/**
	 * Metoda pobiera liste planow zakupow z BD
	 * 
	 */
	public List<PlanZakupow> pobierzPlanZakupowZBD() {
		
		PlanZakupowDTO planZakupow = new PlanZakupowDTO();
		
		return planZakupow.pobierzPlanZakupowZBD();
	}
	/**
	 * Metoda pobiera liste planow zakupow z WS
	 * @throws RemoteException 
	 */
	public List<PlanZakupow> pobierzPlanZakupowZWS() throws RemoteException {
		
		PlanZakupowDTO planZakupow = new PlanZakupowDTO();
		
		return planZakupow.pobierzPlanZakupowZWS();
	}
	
	/**
	 * Metoda aktualizuje plany zakupu
	 * 
	 * @param PlanZakupowNowe
	 * @param PlanZakupowStare
	 */
	public List<PlanZakupow> aktualizujPlanZakupow(
			List<PlanZakupow> planZakupowNowe, List<PlanZakupow> planZakupowStare) {
		HashSet<Long> planSet=new HashSet<Long>();
		for (PlanZakupow pz : planZakupowNowe) {
			try {
				List<PlanZakupowDB> planZakupow = PlanZakupowDBDAO.findByErpID(pz.getErpId());
				if (planZakupow.isEmpty()) {
					pz.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for (PlanZakupowDB pzDB : planZakupow) {
						pz.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						pz.setId(pzDB.getId());
					}
				}
				pz.setAvailable(true);
				planSet.add(pz.getErpId());
			} catch (EdmException e) {
				Log.error(e);
			}
		}
		for (PlanZakupow pz : planZakupowStare) {
			if(planSet.contains(pz.getErpId()))continue;
			pz.setAvailable(false);
			pz.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			planZakupowNowe.add(pz);
		}
		return planZakupowNowe;
	}
	
	/**
	 * Metoda zapisuje plany zakupu do bazy danych
	 * @param
	 */
	
	public void ZapiszPlanZakupowDoDB(List<PlanZakupow> listaPlanZakupow) {
		
		PlanZakupowDTO planZakupow = new PlanZakupowDTO();
		
		planZakupow.zapisz(listaPlanZakupow);
	}
}
