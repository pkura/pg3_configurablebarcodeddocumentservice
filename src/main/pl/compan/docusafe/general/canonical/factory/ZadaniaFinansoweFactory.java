package pl.compan.docusafe.general.canonical.factory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils;
import pl.compan.docusafe.general.canonical.model.ZadaniaFinansowe;
import pl.compan.docusafe.general.dto.ZadaniaFinansoweDTO;
import pl.compan.docusafe.general.hibernate.dao.FinancialTaskDBDAO;
import pl.compan.docusafe.general.hibernate.model.FinancialTaskDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZadaniaFinansoweFactory {

	public static final Logger log = LoggerFactory.getLogger(ZadaniaFinansoweFactory.class);

	/**
	 * Metoda pobiera zadania finansowe z BD
	 * 
	 * @return
	 */
	public List<ZadaniaFinansowe> pobierzZadaniaFinansoweZBD() {

		ZadaniaFinansoweDTO zadaniaFinansoweDTO = new ZadaniaFinansoweDTO();

		return zadaniaFinansoweDTO.pobierzZadaniaFinansoweZBD();		
	}

	/**
	 * Metoda pobiera zadania finansowe z WS
	 * 
	 * @return
	 * @throws RemoteException 
	 */
	public List<ZadaniaFinansowe>  PobierzZadaniaFinansoweWS() throws RemoteException
	{
		ZadaniaFinansoweDTO zadaniaFinansoweDTO = new ZadaniaFinansoweDTO();

		return zadaniaFinansoweDTO.PobierzZadaniaFinansowe();		
	}
	/**
	 * Metoda aktualizuje ZadaniaFinansowe
	 * @param ZadaniaFinansoweNowa
	 * @param ZadaniaFinansoweStara
	 * @return
	 */
	public List<ZadaniaFinansowe> aktualizujZadaniaFinansowe(List<ZadaniaFinansowe> ZadaniaFinansoweNowa, List<ZadaniaFinansowe> ZadaniaFinansoweStara){
		HashSet<Long> zdSet=new HashSet<Long>();
		for(ZadaniaFinansowe zf:ZadaniaFinansoweNowa){
			try {
				List<FinancialTaskDB> financialTask = FinancialTaskDBDAO.findByErpID(zf.getErpId());
				if(financialTask.isEmpty()){
					zf.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.NOWY);
				} else {
					for(FinancialTaskDB ft:financialTask){
						zf.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
						zf.setId(ft.getId());
					}
				}
				zf.setAvailable(1);
				zdSet.add(zf.getErpId());
			} catch (EdmException e) {
				log.error("", e);
			}
		}
		for(ZadaniaFinansowe zf:ZadaniaFinansoweStara){
			if(zdSet.contains(zf.getErpId()))continue;
			zf.setAvailable(0);
			zf.setStanObiektu(ObjectUtils.STATUS_OBIEKTU.ZMODYFIKOWANY);
			ZadaniaFinansoweNowa.add(zf);
		}
		return ZadaniaFinansoweNowa;
	}

	/**
	 * Metoda zapisuje zadania finansowe do bazy danych
	 * 
	 * @param listaZadaniaFinansowe
	 */
	public void zapiszZadaniaFinansoweDoBD(List<ZadaniaFinansowe> listaZadaniaFinansowe) {

		ZadaniaFinansoweDTO zadaniaFinansoweDTO = new ZadaniaFinansoweDTO();

		zadaniaFinansoweDTO.Zapisz(listaZadaniaFinansowe);
	}
}
