package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.ProjektBudzetZasob;

public class ProjektBudzetZasobDAO {
private static ProjektBudzetZasobDAO projektBudzetZasobDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt ProjektBudzetZasobDAO
	 * @return
	 */
	public static ProjektBudzetZasobDAO getInstance()
	{
		if (projektBudzetZasobDAO==null)
		{
			projektBudzetZasobDAO = new ProjektBudzetZasobDAO();			
		}
		
		return projektBudzetZasobDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu ProjektBudzetZasob
	 * @return
	 */
	public ProjektBudzetZasob getProjektBudzetZasob()
	{
		ProjektBudzetZasob projektBudzetZasob = new ProjektBudzetZasob();
		return projektBudzetZasob;
	}
}
