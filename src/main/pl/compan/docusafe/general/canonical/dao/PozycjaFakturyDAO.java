package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.PozycjaFaktury;

public class PozycjaFakturyDAO {

	private static PozycjaFakturyDAO fakturaPozycjaDAO;
	
	public static PozycjaFakturyDAO getInstance(){
		if (fakturaPozycjaDAO==null){
			fakturaPozycjaDAO = new PozycjaFakturyDAO();			
		}
		
		return fakturaPozycjaDAO;
	}
	

	public PozycjaFaktury getFakturaPozycja(){
		PozycjaFaktury fakturaPozycja = new PozycjaFaktury();
		return fakturaPozycja;
	}

}
