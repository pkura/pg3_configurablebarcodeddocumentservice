package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.PlanZakupow;

public class PlanZakupowDAO {
	private static PlanZakupowDAO planZakupowDAO;

	/**
	 * Metoda tworzy jedyne obiekt PlanZakupowDAO
	 */
	public static PlanZakupowDAO getInstance() {
		if (planZakupowDAO == null) {
			planZakupowDAO = new PlanZakupowDAO();
		}
		return planZakupowDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt PlanZakupow
	 */

	public PlanZakupow getPlanZakupow() {
		PlanZakupow planZakupow = new PlanZakupow();
		return planZakupow;
	}
}
