package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.TypyUrlopow;

public class TypyUrlopowDAO {
	private static TypyUrlopowDAO typyUrlopowDAO;

	/**
	 * Metoda tworzy jedyne obiekt TypyUrlopowDAO
	 */
	public static TypyUrlopowDAO getInstance() {
		if (typyUrlopowDAO == null) {
			typyUrlopowDAO = new TypyUrlopowDAO();
		}
		return typyUrlopowDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt TypyUrlopow
	 */

	public TypyUrlopow getTypyUrlopow() {
		TypyUrlopow typyUrlopow = new TypyUrlopow();
		return typyUrlopow;
	}
}
