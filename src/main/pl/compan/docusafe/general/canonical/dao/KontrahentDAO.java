package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.Kontrahent;

public class KontrahentDAO {
	private static KontrahentDAO kontrahentDAO;

	/**
	 * Metoda tworzy jedyny obiekt KontrahentDAO
	 * @return
	 */
	public static KontrahentDAO getInstance()
	{
		if (kontrahentDAO==null)
		{
			kontrahentDAO = new KontrahentDAO();			
		}

		return kontrahentDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt typu Kontrahent
	 * @return
	 */
	public Kontrahent getKontrahent()
	{
		Kontrahent kontrahent = new Kontrahent();
		return kontrahent;
	}
}
