package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.canonical.model.Waluta;

public class WalutaDAO {
	
private static WalutaDAO walutaDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt WalutaDAO
	 * @return
	 */
	public static WalutaDAO getInstance()
	{
		if (walutaDAO==null)
		{
			walutaDAO = new WalutaDAO();			
		}
		
		return walutaDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu Waluta
	 * @return
	 */
	public Waluta getWaluta()
	{
		Waluta waluta = new Waluta();
		return waluta;
	}
	
}
