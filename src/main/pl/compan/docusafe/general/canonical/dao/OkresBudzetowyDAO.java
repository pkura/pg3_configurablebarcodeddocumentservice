package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.OkresBudzetowy;

public class OkresBudzetowyDAO {
	private static OkresBudzetowyDAO okresBudzetowyDAO;

	/**
	 * Metoda tworzy jedyne obiekt okresBudzetowyDAO
	 */
	public static OkresBudzetowyDAO getInstance() {
		if (okresBudzetowyDAO == null) {
			okresBudzetowyDAO = new OkresBudzetowyDAO();
		}
		return okresBudzetowyDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt OkresBudzetowy
	 */

	public OkresBudzetowy getOkresBudzetowy() {
		OkresBudzetowy okresBudzetowy = new OkresBudzetowy();
		return okresBudzetowy;
	}
}
