package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.MiejsceDostawy;

public class MiejsceDostawyDAO {
private static MiejsceDostawyDAO miejsceDostawyDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt MiejsceDostawyDAO
	 * @return
	 */
	public static MiejsceDostawyDAO getInstance()
	{
		if (miejsceDostawyDAO==null)
		{
			miejsceDostawyDAO = new MiejsceDostawyDAO();			
		}
		
		return miejsceDostawyDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu MiejsceDostawy
	 * @return
	 */
	public MiejsceDostawy getMiejsceDostawy()
	{
		MiejsceDostawy miejsceDostawy = new MiejsceDostawy();
		return miejsceDostawy;
	}
	
}
