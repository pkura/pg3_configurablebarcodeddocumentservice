package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.RodzajePlanowZakupu;

public class RodzajePlanowZakupuDAO {
	private static RodzajePlanowZakupuDAO rodzajePlanowZakupuDAO;

	/**
	 * Metoda tworzy jedyne obiekt RodzajePlanowZakupuDAO
	 */
	public static RodzajePlanowZakupuDAO getInstance() {
		if (rodzajePlanowZakupuDAO == null) {
			rodzajePlanowZakupuDAO = new RodzajePlanowZakupuDAO();
		}
		return rodzajePlanowZakupuDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt RodzajePlanowZakupu
	 */

	public RodzajePlanowZakupu getRodzajePlanowZakupu() {
		RodzajePlanowZakupu rodzajePlanowZakupu = new RodzajePlanowZakupu();
		return rodzajePlanowZakupu;
	}
}
