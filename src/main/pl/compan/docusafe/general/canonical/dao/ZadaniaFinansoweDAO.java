package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.ZadaniaFinansowe;

public class ZadaniaFinansoweDAO {
private static ZadaniaFinansoweDAO zadaniaFinansoweDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt ZadaniaFinansoweDAO
	 * @return
	 */
	public static ZadaniaFinansoweDAO getInstance()
	{
		if (zadaniaFinansoweDAO==null)
		{
			zadaniaFinansoweDAO = new ZadaniaFinansoweDAO();			
		}
		
		return zadaniaFinansoweDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu ZadaniaFinansowe
	 * @return
	 */
	public ZadaniaFinansowe getZadaniaFinansowe()
	{
		ZadaniaFinansowe zadaniaFinansowe = new ZadaniaFinansowe();
		return zadaniaFinansowe;
	}
	
}
