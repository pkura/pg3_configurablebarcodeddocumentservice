package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.ProjektBudzet;

public class ProjektBudzetDAO {
private static ProjektBudzetDAO projektBudzetDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt ProjektBudzetDAO
	 * @return
	 */
	public static ProjektBudzetDAO getInstance()
	{
		if (projektBudzetDAO==null)
		{
			projektBudzetDAO = new ProjektBudzetDAO();			
		}
		
		return projektBudzetDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu ProjektBudzet
	 * @return
	 */
	public ProjektBudzet getProjektBudzet()
	{
		ProjektBudzet projektBudzet = new ProjektBudzet();
		return projektBudzet;
	}
}
