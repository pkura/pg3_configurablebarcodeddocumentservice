package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.SzablonyRozliczenFaktur;

public class SzablonyRozliczenFakturDAO {
	private static SzablonyRozliczenFakturDAO szablonyRozliczenFakturDAO;

	/**
	 * Metoda tworzy jedyne obiekt SzablonyRozliczenFakturDAO
	 */
	public static SzablonyRozliczenFakturDAO getInstance() {
		if (szablonyRozliczenFakturDAO == null) {
			szablonyRozliczenFakturDAO = new SzablonyRozliczenFakturDAO();
		}
		return szablonyRozliczenFakturDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt SzablonyRozliczenFaktur
	 */

	public SzablonyRozliczenFaktur getSzablonyRozliczenFaktur() {
		SzablonyRozliczenFaktur szablonyRozliczenFaktur = new SzablonyRozliczenFaktur();
		return szablonyRozliczenFaktur;
	}
}
