package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.RoleWProjekcie;

public class RoleWProjekcieDAO {
	private static RoleWProjekcieDAO roleWProjekcieDAO;

	/**
	 * Metoda tworzy jedyne obiekt RoleWProjekcieDAO
	 */
	public static RoleWProjekcieDAO getInstance() {
		if (roleWProjekcieDAO == null) {
			roleWProjekcieDAO = new RoleWProjekcieDAO();
		}
		return roleWProjekcieDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt RoleWProjekcie
	 */

	public RoleWProjekcie getRoleWProjekcie() {
		RoleWProjekcie roleWProjekcie = new RoleWProjekcie();
		return roleWProjekcie;
	}
}
