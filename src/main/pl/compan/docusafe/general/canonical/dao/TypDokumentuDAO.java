package pl.compan.docusafe.general.canonical.dao;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.TypDokumentuDBFactory;
import pl.compan.docusafe.general.canonical.model.TypDokumentu;
import pl.compan.docusafe.general.canonical.model.TypPlatnosci;
import pl.compan.docusafe.general.hibernate.model.PaymentTermsDB;
import pl.compan.docusafe.general.hibernate.model.PurchaseDocumentTypeDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class TypDokumentuDAO {
	protected static Logger log = LoggerFactory.getLogger(TypDokumentuDAO.class);

	private static TypDokumentuDAO typDokumentuDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt TypDokumentuDAO
	 * @return
	 */
	public static TypDokumentuDAO getInstance()
	{
		if (typDokumentuDAO==null)
		{
			typDokumentuDAO = new TypDokumentuDAO();			
		}
		
		return typDokumentuDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu TypDokumentu
	 * @return
	 */
	public TypDokumentu getTypDokumentu()
	{
		TypDokumentu typDokumentu = new TypDokumentu();
		return typDokumentu;
	}
	
	
	public static TypDokumentu find(Long id){
		try {
			List<PurchaseDocumentTypeDB> listDB=PurchaseDocumentTypeDB.findByDokzakID(id);
			TypDokumentu tp=new TypDokumentu();
			for(PurchaseDocumentTypeDB db:listDB){
				tp.setId(db.getId());
				tp.setNazwa(db.getTitle());
				tp.setCn(db.getCn());
				tp.setCzywal(db.getCzywal());
				tp.setSymbol_waluty(db.getSymbol_waluty());
				tp.setTypdokzak_id(db.getTypdokzak_id());
				tp.setAktywna(Boolean.parseBoolean(db.getAvailable().toString()));
				tp.setTypdokIdn(db.getTypdokIdn());
				tp.setTypdokIds(db.getCn());
			}
			return tp;
		} catch (EdmException e) {
			log.debug("blad dostepu do bazy",e);
			e.printStackTrace();
		}

		return null;
	}
	
}
