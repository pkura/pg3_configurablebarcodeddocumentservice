package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.WniosekORezerwacje;


public class WniosekORezerwacjeDAO {
	private static WniosekORezerwacjeDAO wniosekORezerwacjeDBDAO;

	/**
	 * Metoda tworzy jedyny obiekt WniosekORezerwacjeDAO
	 * @return
	 */
	public static WniosekORezerwacjeDAO getInstance()
	{
		if (wniosekORezerwacjeDBDAO==null)
		{
			wniosekORezerwacjeDBDAO = new WniosekORezerwacjeDAO();			
		}

		return wniosekORezerwacjeDBDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt typu Kontrahent
	 * @return
	 */
	public WniosekORezerwacje getWniosekORezerwacje()
	{
		WniosekORezerwacje wniosekORezerwacje = new WniosekORezerwacje();
		return wniosekORezerwacje;
	}
}
