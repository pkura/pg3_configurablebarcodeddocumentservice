package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;

public class JednostkaOrganizacyjnaDAO {

private static JednostkaOrganizacyjnaDAO jednostkaOrganizacyjnaDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt JednostkaOrganizacyjnaDAO
	 * @return
	 */
	public static JednostkaOrganizacyjnaDAO getInstance()
	{
		if (jednostkaOrganizacyjnaDAO==null)
		{
			jednostkaOrganizacyjnaDAO = new JednostkaOrganizacyjnaDAO();			
		}
		
		return jednostkaOrganizacyjnaDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu JednostkaOrganizacyjna
	 * @return
	 */
	public JednostkaOrganizacyjna getJednostkaOrganizacyjna()
	{
		JednostkaOrganizacyjna jednostkaOrganizacyjna = new JednostkaOrganizacyjna();
		return jednostkaOrganizacyjna;
	}
	
	
	
}
