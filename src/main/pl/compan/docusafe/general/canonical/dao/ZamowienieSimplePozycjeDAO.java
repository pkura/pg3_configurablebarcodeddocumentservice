package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.ZamowienieSimplePozycje;


public class ZamowienieSimplePozycjeDAO {

private static ZamowienieSimplePozycjeDAO zamowienieSimplePozycjeDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt ZamowienieSimplePozycjeDAO
	 * @return
	 */
	public static ZamowienieSimplePozycjeDAO getInstance()
	{
		if (zamowienieSimplePozycjeDAO==null)
		{
			zamowienieSimplePozycjeDAO = new ZamowienieSimplePozycjeDAO();			
		}
		
		return zamowienieSimplePozycjeDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu ZamowienieSimplePozycje
	 * @return
	 */
	public ZamowienieSimplePozycje getZamowienieSimplePozycje()
	{
		ZamowienieSimplePozycje zadaniaFinansowe = new ZamowienieSimplePozycje();
		return zadaniaFinansowe;
	}
}
