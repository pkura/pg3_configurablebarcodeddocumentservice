package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.model.TypPlatnosci;
import pl.compan.docusafe.general.hibernate.model.PaymentTermsDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class TypPlatnosciDAO {
	private static TypPlatnosciDAO typPlatnosciDAO;

	protected static Logger log = LoggerFactory.getLogger(TypPlatnosciDAO.class);
	/**
	 * Metoda tworzy jedyny obiekt TypPlatnosciDAO
	 * @return
	 */
	public static TypPlatnosciDAO getInstance()
	{
		if (typPlatnosciDAO==null)
		{
			typPlatnosciDAO = new TypPlatnosciDAO();			
		}

		return typPlatnosciDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt typu TypPlatnosci
	 * @return
	 */
	public TypPlatnosci getTypPlatnosci()
	{
		TypPlatnosci typPlatnosci = new TypPlatnosci();
		return typPlatnosci;
	}


	public static TypPlatnosci find(Long id){
		try {
			PaymentTermsDB db=PaymentTermsDB.find(id);
			TypPlatnosci tp=new TypPlatnosci();
			tp.setId(db.getId());
			tp.setNazwa(db.getTitle());
			tp.setCn(db.getCn());
			tp.setObiektPowiazany(db.getRefValue());
			
			return tp;
		} catch (EdmException e) {
			log.debug("blad dostepu do bazy",e);
			e.printStackTrace();
		}

		return null;
	}
}
