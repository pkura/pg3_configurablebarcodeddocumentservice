package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.Zrodlo;

public class ZrodloDAO {
private static ZrodloDAO zrodloDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt ZrodloDAO
	 * @return
	 */
	public static ZrodloDAO getInstance()
	{
		if (zrodloDAO==null)
		{
			zrodloDAO = new ZrodloDAO();			
		}
		
		return zrodloDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu Zrodlo
	 * @return
	 */
	public Zrodlo getZrodlo()
	{
		Zrodlo zrodlo = new Zrodlo();
		return zrodlo;
	}
}
