package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.ProjektBudzetEtap;

public class ProjektBudzetEtapDAO {
private static ProjektBudzetEtapDAO projektBudzetDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt ProjektBudzetDAO
	 * @return
	 */
	public static ProjektBudzetEtapDAO getInstance()
	{
		if (projektBudzetDAO==null)
		{
			projektBudzetDAO = new ProjektBudzetEtapDAO();			
		}
		
		return projektBudzetDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu ProjektBudzet
	 * @return
	 */
	public ProjektBudzetEtap getProjektBudzet()
	{
		ProjektBudzetEtap projektBudzet = new ProjektBudzetEtap();
		return projektBudzet;
	}
}
