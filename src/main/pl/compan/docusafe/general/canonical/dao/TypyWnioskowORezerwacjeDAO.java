package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.TypyWnioskowORezerwacje;

public class TypyWnioskowORezerwacjeDAO {
	private static TypyWnioskowORezerwacjeDAO typyWnioskowORezerwacjeDAO;

	/**
	 * Metoda tworzy jedyne obiekt TypyWnioskowORezerwacjeDAO
	 */
	public static TypyWnioskowORezerwacjeDAO getInstance() {
		if (typyWnioskowORezerwacjeDAO == null) {
			typyWnioskowORezerwacjeDAO = new TypyWnioskowORezerwacjeDAO();
		}
		return typyWnioskowORezerwacjeDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt TypyWnioskowORezerwacje
	 */

	public TypyWnioskowORezerwacje getTypyWnioskowORezerwacje() {
		TypyWnioskowORezerwacje typyWnioskowORezerwacje = new TypyWnioskowORezerwacje();
		return typyWnioskowORezerwacje;
	}
}
