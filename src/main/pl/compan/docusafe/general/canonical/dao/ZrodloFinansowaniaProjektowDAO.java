package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.ZrodloFinansowaniaProjektow;

public class ZrodloFinansowaniaProjektowDAO {
private static ZrodloFinansowaniaProjektowDAO zrodloFinansowaniaProjektowDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt ZrodloFinansowaniaProjektowDAO
	 * @return
	 */
	public static ZrodloFinansowaniaProjektowDAO getInstance()
	{
		if (zrodloFinansowaniaProjektowDAO==null)
		{
			zrodloFinansowaniaProjektowDAO = new ZrodloFinansowaniaProjektowDAO();			
		}
		
		return zrodloFinansowaniaProjektowDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu ZrodloFinansowaniaProjektow
	 * @return
	 */
	public ZrodloFinansowaniaProjektow getZrodloFinansowaniaProjektow()
	{
		ZrodloFinansowaniaProjektow zrodloFinansowaniaProjektow = new ZrodloFinansowaniaProjektow();
		return zrodloFinansowaniaProjektow;
	}
}
