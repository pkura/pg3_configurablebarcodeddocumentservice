package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.WydatekStrukturalny;

public class WydatekStrukturalnyDAO {

	private static WydatekStrukturalnyDAO wydatekStrukturalnyDAO;
	
	public static WydatekStrukturalnyDAO getInstance(){
		if(wydatekStrukturalnyDAO==null)wydatekStrukturalnyDAO= new WydatekStrukturalnyDAO();
		return wydatekStrukturalnyDAO;
	}
	public WydatekStrukturalny getWydatekStrukturalny(){
		return new WydatekStrukturalny();
	}
}
