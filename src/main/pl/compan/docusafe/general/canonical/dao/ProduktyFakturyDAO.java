package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.ProduktyFaktury;

public class ProduktyFakturyDAO {
	private static ProduktyFakturyDAO produktyFakturyDAO;

	public static ProduktyFakturyDAO getInstance() {
		if (produktyFakturyDAO == null) {
			produktyFakturyDAO = new ProduktyFakturyDAO();
		}

		return produktyFakturyDAO;
	}

	public ProduktyFaktury getProduktyFaktury() {
		ProduktyFaktury produktyFaktury = new ProduktyFaktury();
		return produktyFaktury;
	}
}
