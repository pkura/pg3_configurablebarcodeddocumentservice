package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.ZleceniaProdukcyjne;

public class ZleceniaProdukcyjneDAO {
private static ZleceniaProdukcyjneDAO zleceniaProdukcyjneDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt ZleceniaProdukcyjneDAO
	 * @return
	 */
	public static ZleceniaProdukcyjneDAO getInstance()
	{
		if (zleceniaProdukcyjneDAO==null)
		{
			zleceniaProdukcyjneDAO = new ZleceniaProdukcyjneDAO();			
		}
		
		return zleceniaProdukcyjneDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu ZleceniaProdukcyjne
	 * @return
	 */
	public ZleceniaProdukcyjne getZleceniaProdukcyjne()
	{
		ZleceniaProdukcyjne zleceniaProdukcyjne = new ZleceniaProdukcyjne();
		return zleceniaProdukcyjne;
	}
}
