package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.RoleWBudzecie;

public class RoleWBudzecieDAO {
	private static RoleWBudzecieDAO roleWBudzecieDAO;

	/**
	 * Metoda tworzy jedyne obiekt RoleWBudzecieDAO
	 */
	public static RoleWBudzecieDAO getInstance() {
		if (roleWBudzecieDAO == null) {
			roleWBudzecieDAO = new RoleWBudzecieDAO();
		}
		return roleWBudzecieDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt RoleWBudzecie
	 */

	public RoleWBudzecie getRoleWBudzecie() {
		RoleWBudzecie roleWBudzecie = new RoleWBudzecie();
		return roleWBudzecie;
	}
}
