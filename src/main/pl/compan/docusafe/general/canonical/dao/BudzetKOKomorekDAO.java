package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.BudzetKOKomorek;

public class BudzetKOKomorekDAO {
	private static BudzetKOKomorekDAO budzetKOKomorekDAO;

	/**
	 * Metoda tworzy jedyne obiekt BudzetKOKomorekDAO
	 */
	public static BudzetKOKomorekDAO getInstance() {
		if (budzetKOKomorekDAO == null) {
			budzetKOKomorekDAO = new BudzetKOKomorekDAO();
		}
		return budzetKOKomorekDAO;
	}

	/**
	 * Metoda tworzy nowy obiekt BudzetKOKomorek
	 */

	public BudzetKOKomorek getBudzetKOKomorek() {
		BudzetKOKomorek budzetKOKomorek = new BudzetKOKomorek();
		return budzetKOKomorek;
	}
}
