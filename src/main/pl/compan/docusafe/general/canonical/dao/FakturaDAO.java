package pl.compan.docusafe.general.canonical.dao;

import java.util.Calendar;

import org.apache.commons.lang.NotImplementedException;

import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.handlers.ExportToErpHandler;
import pl.compan.docusafe.events.handlers.PikaMailHandler;
import pl.compan.docusafe.events.handlers.ExportToErpHandler.RodzajExportu;
import pl.compan.docusafe.general.canonical.model.Faktura;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FakturaDAO {
	private static Logger log = LoggerFactory.getLogger(FakturaDAO.class);
	
	private static FakturaDAO fakturaDAO;
	
	public static FakturaDAO getInstance(){
		if (fakturaDAO==null){
			fakturaDAO = new FakturaDAO();			
		}
		
		return fakturaDAO;
	}
	
	private FakturaDAO(){}

	public Faktura getFaktura(){
		Faktura faktura = new Faktura();
		return faktura;
	}
	
	public void wyslijDoERP(Faktura faktura) throws DocusafeEventException{
		ExportToErpHandler.createEvent(RodzajExportu.FAKTURA, faktura.getId());
	}
	
	public void Zwaliduj(Faktura faktura)
	{
		throw new NotImplementedException();		
	}

}
