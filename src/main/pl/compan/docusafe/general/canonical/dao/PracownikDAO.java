package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.Pracownik;

public  class PracownikDAO {

	private static PracownikDAO pracownikDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt PracownikDAO
	 * @return
	 */
	public static PracownikDAO getInstance()
	{
		if (pracownikDAO==null)
		{
			pracownikDAO = new PracownikDAO();			
		}
		
		return pracownikDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu Pracownik
	 * @return
	 */
	public Pracownik getPracownik()
	{
		Pracownik pracownik = new Pracownik();
		return pracownik;
	}
	
}
