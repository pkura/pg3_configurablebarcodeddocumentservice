package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.Projekt;

public class ProjektDAO {
private static ProjektDAO projektDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt ProjektDAO
	 * @return
	 */
	public static ProjektDAO getInstance()
	{
		if (projektDAO==null)
		{
			projektDAO = new ProjektDAO();			
		}
		
		return projektDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu Projekt
	 * @return
	 */
	public Projekt getProjekt()
	{
		Projekt projekt = new Projekt();
		return projekt;
	}
}
