package pl.compan.docusafe.general.canonical.dao;

import pl.compan.docusafe.general.canonical.model.StawkaVat;

public class StawkaVatDAO {
private static StawkaVatDAO stawkaVatDAO;
	
	/**
	 * Metoda tworzy jedyny obiekt StawkaVatDAO
	 * @return
	 */
	public static StawkaVatDAO getInstance()
	{
		if (stawkaVatDAO==null)
		{
			stawkaVatDAO = new StawkaVatDAO();			
		}
		
		return stawkaVatDAO;
	}
	
	/**
	 * Metoda tworzy nowy obiekt typu StawkaVat
	 * @return
	 */
	public StawkaVat getStawkaVat()
	{
		StawkaVat stawkaVat = new StawkaVat();
		return stawkaVat;
	}
}
