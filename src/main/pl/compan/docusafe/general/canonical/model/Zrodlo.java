package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class Zrodlo {

	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Long id;
	private Long erpId;
	private Double identyfikator_num;
	private String idm;
	private String zrodlo_nazwa;
	private boolean available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}

	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public Double getIdentyfikator_num() {
		return identyfikator_num;
	}

	public void setIdentyfikator_num(Double identyfikator_num) {
		this.identyfikator_num = identyfikator_num;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getZrodlo_nazwa() {
		return zrodlo_nazwa;
	}

	public void setZrodlo_nazwa(String zrodlo_nazwa) {
		this.zrodlo_nazwa = zrodlo_nazwa;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
}
