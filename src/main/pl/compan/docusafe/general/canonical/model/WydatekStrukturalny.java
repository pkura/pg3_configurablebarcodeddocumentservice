package pl.compan.docusafe.general.canonical.model;

import java.math.BigDecimal;

public class WydatekStrukturalny {
	
	private Long id;
	private Long WydatekStrukturalny;
	private BigDecimal kwotaBrutto;
	private Long obszar;
	private Long kod;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getWydatekStrukturalny() {
		return WydatekStrukturalny;
	}
	public void setWydatekStrukturalny(Long wydatekStrukturalny) {
		WydatekStrukturalny = wydatekStrukturalny;
	}
	public BigDecimal getKwotaBrutto() {
		return kwotaBrutto;
	}
	public void setKwotaBrutto(BigDecimal kwotaBrutto) {
		this.kwotaBrutto = kwotaBrutto;
	}
	public Long getObszar() {
		return obszar;
	}
	public void setObszar(Long obszar) {
		this.obszar = obszar;
	}
	public Long getKod() {
		return kod;
	}
	public void setKod(Long kod) {
		this.kod = kod;
	}
}
