package pl.compan.docusafe.general.canonical.model;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

@SuppressWarnings("serial")
public class GeneralDictionary implements java.io.Serializable {
	private Integer id;
	private Boolean deleted;
	private String code;
	private String tablename;
	private Integer dictSource;
	private String cn;
	private GeneralDictionary refDict;
	private Boolean controlDict;
    private String dockindCn;
	
	public static enum SourceType {
		MANUAL(1),
		SERVICE(2),
		REPOSITORY(3);
		
		public final int value;
		
		SourceType(int value) {
			this.value = value;
		}
	}
	
	private static GeneralDictionary instance;
	
    public static synchronized GeneralDictionary getInstance() {
        if (instance == null)
            instance = new GeneralDictionary();
        return instance;
    }
	
	public GeneralDictionary() {
		this.deleted = false;
		this.controlDict = false;
	}


	public GeneralDictionary(Boolean deleted, String code, String tablename, Integer dictSource, String cn, GeneralDictionary refDict,
			Boolean isControlDict) {
		this.deleted = deleted;
		this.code = code;
		this.tablename = tablename;
		this.dictSource = dictSource;
		this.cn = cn;
		this.refDict = refDict;
		this.controlDict = isControlDict;
	}

	public Integer getId() {
		return this.id;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public void setCode(Long code) {
		this.code = code.toString();
	}

	public String getTablename() {
		return this.tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	
	public Integer getDictSource() {
		return this.dictSource;
	}

	public void setDictSource(Integer dictSource) {
		this.dictSource = dictSource;
	}
	
	public void setDictSource(SourceType type) {
		this.dictSource = type.value;
	}

	public String getCn() {
		return this.cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public GeneralDictionary getRefDict() {
		return this.refDict;
	}

	public void setRefDict(GeneralDictionary refDict) {
		this.refDict = refDict;
	}

	public Boolean isControlDict() {
		return this.controlDict;
	}

	public void setControlDict(Boolean isControlDict) {
		this.controlDict = isControlDict;
	}

    public String getDockindCn() {
        return dockindCn;
    }

    public void setDockindCn(String dockindCn) {
        this.dockindCn = dockindCn;
    }

    public GeneralDictionary findById(Long id) throws EdmException
    {
        return Finder.find(GeneralDictionary.class, id);
    }

    public List<GeneralDictionary> findAll(boolean deleted) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(GeneralDictionary.class);

    	crit.add(Restrictions.eq("deleted", deleted));

    	return (List<GeneralDictionary>) crit.list();
    }

    public List<GeneralDictionary> findAll(boolean deleted, String dockindCn) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(GeneralDictionary.class);

    	crit.add(Restrictions.eq("deleted", deleted));
    	crit.add(Restrictions.eq("dockindCn", dockindCn));

    	return (List<GeneralDictionary>) crit.list();
    }

    public List<GeneralDictionary> findRepositoryDictToDelete(String dockindCn, Collection<Integer> available) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(GeneralDictionary.class);
    	
    	crit.add(Restrictions.eq("dictSource", SourceType.REPOSITORY.value));
    	crit.add(Restrictions.eq("dockindCn", dockindCn));
    	crit.add(Restrictions.not(Restrictions.in("id", available)));
    	
    	return (List<GeneralDictionary>) crit.list();
    }

    public GeneralDictionary find(String code) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(GeneralDictionary.class);

    	crit.add(Restrictions.eq("code", code));

    	return (GeneralDictionary) crit.uniqueResult();
    }

    public GeneralDictionary find(String code, String dockindCn) throws EdmException{
    	Criteria crit = DSApi.context().session().createCriteria(GeneralDictionary.class);

    	crit.add(Restrictions.eq("code", code));
    	crit.add(Restrictions.eq("dockindCn", dockindCn));

    	return (GeneralDictionary) crit.uniqueResult();
    }

	public GeneralDictionary findControlDict() throws EdmException{
		Criteria crit = DSApi.context().session().createCriteria(GeneralDictionary.class);
		
		crit.add(Restrictions.eq("controlDict", true));
		
		return (GeneralDictionary) crit.uniqueResult();
	}
    
	public void save() throws EdmException {
		DSApi.context().session().save(this);
		DSApi.context().session().flush();
	}

	public void delete() throws EdmException {
		DSApi.context().session().delete(this);
		DSApi.context().session().flush();
	}

}
