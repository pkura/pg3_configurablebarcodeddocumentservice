package pl.compan.docusafe.general.canonical.model;



import pl.compan.docusafe.general.ObjectUtils;

public class StawkaVat{
	public enum RODZAJ_JEDNOSTKI_ORGANIZACYJNEJ { DOCUSAFE, ERP };

	/**
	 * Oznaczenie stanu obiektu (nowy, zmodyfikowany, usuni�ty)
	 */
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Long id;
	private String cn;
	private String nazwa;
	private Boolean aktywna;
	private Boolean czy_zwolniona;
	private Boolean nie_podlega;
	private Long erp_id;
	private String idm;

	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public boolean isAktywna() {
		return aktywna;
	}

	public void isAktywna(boolean aktywna) {
		this.aktywna = aktywna;
	}
	public Boolean getCzy_zwolniona() {
		return czy_zwolniona;
	}
	public void setCzy_zwolniona(Boolean czy_zwolniona) {
		this.czy_zwolniona = czy_zwolniona;
	}
	public Boolean getNie_podlega() {
		return nie_podlega;
	}
	public void setNie_podlega(Boolean nie_podlega) {
		this.nie_podlega = nie_podlega;
	}
	public Long getErp_id() {
		return erp_id;
	}
	public void setErp_id(Long erp_id) {
		this.erp_id = erp_id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Boolean getAktywna() {
		return aktywna;
	}
	public void setAktywna(Boolean aktywna) {
		this.aktywna = aktywna;
	}
	public String getIdm() {
		return idm;
	}
	public void setIdm(String idm) {
		this.idm = idm;
	}
}
