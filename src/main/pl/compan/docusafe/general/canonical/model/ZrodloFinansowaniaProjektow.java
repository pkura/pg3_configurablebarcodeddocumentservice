package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class ZrodloFinansowaniaProjektow {
	
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Long id;
	private String refValue;
	private Boolean available;
	private Double kontrakt_id;
	private Long erpId;
	private String kontrakt_idm;
	private Integer rodzaj;
	private Double wsp_proc;
	private Double zrodlo_id;
	private Double zrodlo_identyfikator_num;
	private String zrodlo_idn;
	private String zrodlo_nazwa;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRefValue() {
		return refValue;
	}
	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	public Double getKontrakt_id() {
		return kontrakt_id;
	}
	public void setKontrakt_id(Double kontrakt_id) {
		this.kontrakt_id = kontrakt_id;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
	public String getKontrakt_idm() {
		return kontrakt_idm;
	}
	public void setKontrakt_idm(String kontrakt_idm) {
		this.kontrakt_idm = kontrakt_idm;
	}
	public Integer getRodzaj() {
		return rodzaj;
	}
	public void setRodzaj(Integer rodzaj) {
		this.rodzaj = rodzaj;
	}
	public Double getWsp_proc() {
		return wsp_proc;
	}
	public void setWsp_proc(Double wsp_proc) {
		this.wsp_proc = wsp_proc;
	}
	public Double getZrodlo_id() {
		return zrodlo_id;
	}
	public void setZrodlo_id(Double zrodlo_id) {
		this.zrodlo_id = zrodlo_id;
	}
	public Double getZrodlo_identyfikator_num() {
		return zrodlo_identyfikator_num;
	}
	public void setZrodlo_identyfikator_num(Double zrodlo_identyfikator_num) {
		this.zrodlo_identyfikator_num = zrodlo_identyfikator_num;
	}
	public String getZrodlo_idn() {
		return zrodlo_idn;
	}
	public void setZrodlo_idn(String zrodlo_idn) {
		this.zrodlo_idn = zrodlo_idn;
	}
	public String getZrodlo_nazwa() {
		return zrodlo_nazwa;
	}
	public void setZrodlo_nazwa(String zrodlo_nazwa) {
		this.zrodlo_nazwa = zrodlo_nazwa;
	}
	
}
