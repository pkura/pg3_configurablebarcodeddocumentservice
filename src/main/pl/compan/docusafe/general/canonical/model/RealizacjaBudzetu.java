package pl.compan.docusafe.general.canonical.model;

import java.math.BigDecimal;

public class RealizacjaBudzetu {
	private Integer id;
	private String nazwaBudzetu;
	private String nazwaPozycji;
	private String planPozycji;
	private String wykonaniePozycji;
	private BigDecimal pozostalo;
	
	public RealizacjaBudzetu(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNazwaBudzetu() {
		return nazwaBudzetu;
	}

	public void setNazwaBudzetu(String nazwaBudzetu) {
		this.nazwaBudzetu = nazwaBudzetu;
	}

	public String getNazwaPozycji() {
		return nazwaPozycji;
	}

	public void setNazwaPozycji(String nazwaPozycji) {
		this.nazwaPozycji = nazwaPozycji;
	}

	public String getPlanPozycji() {
		return planPozycji;
	}

	public void setPlanPozycji(String planPozycji) {
		this.planPozycji = planPozycji;
	}

	public String getWykonaniePozycji() {
		return wykonaniePozycji;
	}

	public void setWykonaniePozycji(String wykonaniePozycji) {
		this.wykonaniePozycji = wykonaniePozycji;
	}

	public BigDecimal getPozostalo() {
		return pozostalo;
	}

	public void setPozostalo(BigDecimal pozostalo) {
		this.pozostalo = pozostalo;
	}
	
}
