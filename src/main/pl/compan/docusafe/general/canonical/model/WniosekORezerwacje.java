package pl.compan.docusafe.general.canonical.model;

import java.math.BigDecimal;

import pl.compan.docusafe.general.ObjectUtils;

public class WniosekORezerwacje {

	private Long id;
	private Long bdBudzetKoId;
	private String bdBudzetKoIdm;
	private Long bdPlanZamId;
	private String bdPlanZamIdm;
	private Long bdRezerwacjaId;
	private String bdRezerwacjaIdm;
	private Long bdRezerwacjaPozId;
	private Long bdRodzajKoId;
	private Long bdRodzajPlanZamId;
	private Long bdStrBudzetId;
	private String bdTypRezerwacjaIdn;
	private Long bkBdOkrSzablonRelId;
	private Long bkBdSzablonPozId;
	
	/* na tescie tego nie ma
	@Column(name = "bk_czy_archiwalny")
	private Double bkCzyArchiwalny;*/
	
	private String bkNazwa;
	private String bkPozNazwa;
	private Integer bkPozNrpoz;
	private BigDecimal bkPozPKoszt;
	private BigDecimal bkPozPKosztZrodla;
	private BigDecimal bkPozRKoszt;
	private BigDecimal bkPozRKosztZrodla;
	private BigDecimal bkPozRezKoszt;
	private BigDecimal bkPozRezKosztZrodla;
	
	/* na tescie tego nie ma
	@Column(name = "bk_status")
	private Integer bkStatus;*/
	
	private Long bkWytworId;
	private Long budzetId;
	private String budzetIdm;
	private BigDecimal cena;
	private Boolean czyObslugaPzp;
	private BigDecimal ilosc;
	private BigDecimal koszt;
	private Long magazynId;
	private String nazwa;
	private Integer nrpoz;
	private Long okrrozlId;
	private Long pzpBdOkrSzablonRelId;
	private Long pzpBdSzablonPozId;
	
	/* na tescie tego nie ma
	@Column(name = "pzp_czy_archiwalny")
	private Double pzpCzyArchiwalny;*/
	
	private Long pzpJmId;
	private String pzpJmIdn;
	private String pzpNazwa;
	private String pzpPozNazwa;
	private Integer pzpPozNrpoz;
	private BigDecimal pzpPozPIlosc;
	private BigDecimal pzpPozPKoszt;
	private BigDecimal pzpPozRIlosc;
	private BigDecimal pzpPozRKoszt;
	private BigDecimal pzpPozRezIlosc;
	private BigDecimal pzpPozRezKoszt;
	
	/* na tescie tego nie ma
	@Column(name = "pzp_status")
	private Integer pzpStatus;*/
	
	private Long pzpWytworId;
	private String pzpWytworIdm;
	private Long rezJmId;
	private String rezJmIdn;
	private Boolean rezerwacjaCzyArchiwalny;
	private Integer rezerwacjaStatus;
	private Long vatstawId;
	private String wytworEditIdm;
	private String wytworEditNazwa;
	private Long wytworId;
	private Long zadanieId;
	private String zadanieIdn;
	private String zadanieNazwa;
	private Long zrodloId;
	private String zrodloIdn;
	private String zrodloNazwa;
	
	private Boolean available;
	
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBdBudzetKoId() {
		return bdBudzetKoId;
	}
	public void setBdBudzetKoId(Long bdBudzetKoId) {
		this.bdBudzetKoId = bdBudzetKoId;
	}
	public String getBdBudzetKoIdm() {
		return bdBudzetKoIdm;
	}
	public void setBdBudzetKoIdm(String bdBudzetKoIdm) {
		this.bdBudzetKoIdm = bdBudzetKoIdm;
	}
	public Long getBdPlanZamId() {
		return bdPlanZamId;
	}
	public void setBdPlanZamId(Long bdPlanZamId) {
		this.bdPlanZamId = bdPlanZamId;
	}
	public String getBdPlanZamIdm() {
		return bdPlanZamIdm;
	}
	public void setBdPlanZamIdm(String bdPlanZamIdm) {
		this.bdPlanZamIdm = bdPlanZamIdm;
	}
	public Long getBdRezerwacjaId() {
		return bdRezerwacjaId;
	}
	public void setBdRezerwacjaId(Long bdRezerwacjaId) {
		this.bdRezerwacjaId = bdRezerwacjaId;
	}
	public String getBdRezerwacjaIdm() {
		return bdRezerwacjaIdm;
	}
	public void setBdRezerwacjaIdm(String bdRezerwacjaIdm) {
		this.bdRezerwacjaIdm = bdRezerwacjaIdm;
	}
	public Long getBdRezerwacjaPozId() {
		return bdRezerwacjaPozId;
	}
	public void setBdRezerwacjaPozId(Long bdRezerwacjaPozId) {
		this.bdRezerwacjaPozId = bdRezerwacjaPozId;
	}
	public Long getBdRodzajKoId() {
		return bdRodzajKoId;
	}
	public void setBdRodzajKoId(Long bdRodzajKoId) {
		this.bdRodzajKoId = bdRodzajKoId;
	}
	public Long getBdRodzajPlanZamId() {
		return bdRodzajPlanZamId;
	}
	public void setBdRodzajPlanZamId(Long bdRodzajPlanZamId) {
		this.bdRodzajPlanZamId = bdRodzajPlanZamId;
	}
	public Long getBdStrBudzetId() {
		return bdStrBudzetId;
	}
	public void setBdStrBudzetId(Long bdStrBudzetId) {
		this.bdStrBudzetId = bdStrBudzetId;
	}
	public String getBdTypRezerwacjaIdn() {
		return bdTypRezerwacjaIdn;
	}
	public void setBdTypRezerwacjaIdn(String bdTypRezerwacjaIdn) {
		this.bdTypRezerwacjaIdn = bdTypRezerwacjaIdn;
	}
	public Long getBkBdOkrSzablonRelId() {
		return bkBdOkrSzablonRelId;
	}
	public void setBkBdOkrSzablonRelId(Long bkBdOkrSzablonRelId) {
		this.bkBdOkrSzablonRelId = bkBdOkrSzablonRelId;
	}
	public Long getBkBdSzablonPozId() {
		return bkBdSzablonPozId;
	}
	public void setBkBdSzablonPozId(Long bkBdSzablonPozId) {
		this.bkBdSzablonPozId = bkBdSzablonPozId;
	}
	public String getBkNazwa() {
		return bkNazwa;
	}
	public void setBkNazwa(String bkNazwa) {
		this.bkNazwa = bkNazwa;
	}
	public String getBkPozNazwa() {
		return bkPozNazwa;
	}
	public void setBkPozNazwa(String bkPozNazwa) {
		this.bkPozNazwa = bkPozNazwa;
	}
	public Integer getBkPozNrpoz() {
		return bkPozNrpoz;
	}
	public void setBkPozNrpoz(Integer bkPozNrpoz) {
		this.bkPozNrpoz = bkPozNrpoz;
	}
	public BigDecimal getBkPozPKoszt() {
		return bkPozPKoszt;
	}
	public void setBkPozPKoszt(BigDecimal bkPozPKoszt) {
		this.bkPozPKoszt = bkPozPKoszt;
	}
	public BigDecimal getBkPozPKosztZrodla() {
		return bkPozPKosztZrodla;
	}
	public void setBkPozPKosztZrodla(BigDecimal bkPozPKosztZrodla) {
		this.bkPozPKosztZrodla = bkPozPKosztZrodla;
	}
	public BigDecimal getBkPozRKoszt() {
		return bkPozRKoszt;
	}
	public void setBkPozRKoszt(BigDecimal bkPozRKoszt) {
		this.bkPozRKoszt = bkPozRKoszt;
	}
	public BigDecimal getBkPozRKosztZrodla() {
		return bkPozRKosztZrodla;
	}
	public void setBkPozRKosztZrodla(BigDecimal bkPozRKosztZrodla) {
		this.bkPozRKosztZrodla = bkPozRKosztZrodla;
	}
	public BigDecimal getBkPozRezKoszt() {
		return bkPozRezKoszt;
	}
	public void setBkPozRezKoszt(BigDecimal bkPozRezKoszt) {
		this.bkPozRezKoszt = bkPozRezKoszt;
	}
	public BigDecimal getBkPozRezKosztZrodla() {
		return bkPozRezKosztZrodla;
	}
	public void setBkPozRezKosztZrodla(BigDecimal bkPozRezKosztZrodla) {
		this.bkPozRezKosztZrodla = bkPozRezKosztZrodla;
	}
	public Long getBkWytworId() {
		return bkWytworId;
	}
	public void setBkWytworId(Long bkWytworId) {
		this.bkWytworId = bkWytworId;
	}
	public Long getBudzetId() {
		return budzetId;
	}
	public void setBudzetId(Long budzetId) {
		this.budzetId = budzetId;
	}
	public String getBudzetIdm() {
		return budzetIdm;
	}
	public void setBudzetIdm(String budzetIdm) {
		this.budzetIdm = budzetIdm;
	}
	public BigDecimal getCena() {
		return cena;
	}
	public void setCena(BigDecimal cena) {
		this.cena = cena;
	}
	public Boolean getCzyObslugaPzp() {
		return czyObslugaPzp;
	}
	public void setCzyObslugaPzp(Boolean czyObslugaPzp) {
		this.czyObslugaPzp = czyObslugaPzp;
	}
	public BigDecimal getIlosc() {
		return ilosc;
	}
	public void setIlosc(BigDecimal ilosc) {
		this.ilosc = ilosc;
	}
	public BigDecimal getKoszt() {
		return koszt;
	}
	public void setKoszt(BigDecimal koszt) {
		this.koszt = koszt;
	}
	public Long getMagazynId() {
		return magazynId;
	}
	public void setMagazynId(Long magazynId) {
		this.magazynId = magazynId;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public Integer getNrpoz() {
		return nrpoz;
	}
	public void setNrpoz(Integer nrpoz) {
		this.nrpoz = nrpoz;
	}
	public Long getOkrrozlId() {
		return okrrozlId;
	}
	public void setOkrrozlId(Long okrrozlId) {
		this.okrrozlId = okrrozlId;
	}
	public Long getPzpBdOkrSzablonRelId() {
		return pzpBdOkrSzablonRelId;
	}
	public void setPzpBdOkrSzablonRelId(Long pzpBdOkrSzablonRelId) {
		this.pzpBdOkrSzablonRelId = pzpBdOkrSzablonRelId;
	}
	public Long getPzpBdSzablonPozId() {
		return pzpBdSzablonPozId;
	}
	public void setPzpBdSzablonPozId(Long pzpBdSzablonPozId) {
		this.pzpBdSzablonPozId = pzpBdSzablonPozId;
	}
	public Long getPzpJmId() {
		return pzpJmId;
	}
	public void setPzpJmId(Long pzpJmId) {
		this.pzpJmId = pzpJmId;
	}
	public String getPzpJmIdn() {
		return pzpJmIdn;
	}
	public void setPzpJmIdn(String pzpJmIdn) {
		this.pzpJmIdn = pzpJmIdn;
	}
	public String getPzpNazwa() {
		return pzpNazwa;
	}
	public void setPzpNazwa(String pzpNazwa) {
		this.pzpNazwa = pzpNazwa;
	}
	public String getPzpPozNazwa() {
		return pzpPozNazwa;
	}
	public void setPzpPozNazwa(String pzpPozNazwa) {
		this.pzpPozNazwa = pzpPozNazwa;
	}
	public Integer getPzpPozNrpoz() {
		return pzpPozNrpoz;
	}
	public void setPzpPozNrpoz(Integer pzpPozNrpoz) {
		this.pzpPozNrpoz = pzpPozNrpoz;
	}
	public BigDecimal getPzpPozPIlosc() {
		return pzpPozPIlosc;
	}
	public void setPzpPozPIlosc(BigDecimal pzpPozPIlosc) {
		this.pzpPozPIlosc = pzpPozPIlosc;
	}
	public BigDecimal getPzpPozPKoszt() {
		return pzpPozPKoszt;
	}
	public void setPzpPozPKoszt(BigDecimal pzpPozPKoszt) {
		this.pzpPozPKoszt = pzpPozPKoszt;
	}
	public BigDecimal getPzpPozRIlosc() {
		return pzpPozRIlosc;
	}
	public void setPzpPozRIlosc(BigDecimal pzpPozRIlosc) {
		this.pzpPozRIlosc = pzpPozRIlosc;
	}
	public BigDecimal getPzpPozRKoszt() {
		return pzpPozRKoszt;
	}
	public void setPzpPozRKoszt(BigDecimal pzpPozRKoszt) {
		this.pzpPozRKoszt = pzpPozRKoszt;
	}
	public BigDecimal getPzpPozRezIlosc() {
		return pzpPozRezIlosc;
	}
	public void setPzpPozRezIlosc(BigDecimal pzpPozRezIlosc) {
		this.pzpPozRezIlosc = pzpPozRezIlosc;
	}
	public BigDecimal getPzpPozRezKoszt() {
		return pzpPozRezKoszt;
	}
	public void setPzpPozRezKoszt(BigDecimal pzpPozRezKoszt) {
		this.pzpPozRezKoszt = pzpPozRezKoszt;
	}
	public Long getPzpWytworId() {
		return pzpWytworId;
	}
	public void setPzpWytworId(Long pzpWytworId) {
		this.pzpWytworId = pzpWytworId;
	}
	public String getPzpWytworIdm() {
		return pzpWytworIdm;
	}
	public void setPzpWytworIdm(String pzpWytworIdm) {
		this.pzpWytworIdm = pzpWytworIdm;
	}
	public Long getRezJmId() {
		return rezJmId;
	}
	public void setRezJmId(Long rezJmId) {
		this.rezJmId = rezJmId;
	}
	public String getRezJmIdn() {
		return rezJmIdn;
	}
	public void setRezJmIdn(String rezJmIdn) {
		this.rezJmIdn = rezJmIdn;
	}
	public Boolean getRezerwacjaCzyArchiwalny() {
		return rezerwacjaCzyArchiwalny;
	}
	public void setRezerwacjaCzyArchiwalny(Boolean rezerwacjaCzyArchiwalny) {
		this.rezerwacjaCzyArchiwalny = rezerwacjaCzyArchiwalny;
	}
	public Integer getRezerwacjaStatus() {
		return rezerwacjaStatus;
	}
	public void setRezerwacjaStatus(Integer rezerwacjaStatus) {
		this.rezerwacjaStatus = rezerwacjaStatus;
	}
	public Long getVatstawId() {
		return vatstawId;
	}
	public void setVatstawId(Long vatstawId) {
		this.vatstawId = vatstawId;
	}
	public String getWytworEditIdm() {
		return wytworEditIdm;
	}
	public void setWytworEditIdm(String wytworEditIdm) {
		this.wytworEditIdm = wytworEditIdm;
	}
	public String getWytworEditNazwa() {
		return wytworEditNazwa;
	}
	public void setWytworEditNazwa(String wytworEditNazwa) {
		this.wytworEditNazwa = wytworEditNazwa;
	}
	public Long getWytworId() {
		return wytworId;
	}
	public void setWytworId(Long wytworId) {
		this.wytworId = wytworId;
	}
	public Long getZadanieId() {
		return zadanieId;
	}
	public void setZadanieId(Long zadanieId) {
		this.zadanieId = zadanieId;
	}
	public String getZadanieIdn() {
		return zadanieIdn;
	}
	public void setZadanieIdn(String zadanieIdn) {
		this.zadanieIdn = zadanieIdn;
	}
	public String getZadanieNazwa() {
		return zadanieNazwa;
	}
	public void setZadanieNazwa(String zadanieNazwa) {
		this.zadanieNazwa = zadanieNazwa;
	}
	public Long getZrodloId() {
		return zrodloId;
	}
	public void setZrodloId(Long zrodloId) {
		this.zrodloId = zrodloId;
	}
	public String getZrodloIdn() {
		return zrodloIdn;
	}
	public void setZrodloIdn(String zrodloIdn) {
		this.zrodloIdn = zrodloIdn;
	}
	public String getZrodloNazwa() {
		return zrodloNazwa;
	}
	public void setZrodloNazwa(String zrodloNazwa) {
		this.zrodloNazwa = zrodloNazwa;
	}
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
	
	
}
