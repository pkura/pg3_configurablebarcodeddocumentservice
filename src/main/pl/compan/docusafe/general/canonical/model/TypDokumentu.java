package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class TypDokumentu {
	public enum RODZAJ_JEDNOSTKI_ORGANIZACYJNEJ { DOCUSAFE, ERP };

	/**
	 * Oznaczenie stanu obiektu (nowy, zmodyfikowany, usuni�ty)
	 */
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private long id;
	private String cn;
	private String nazwa;
	private boolean aktywna;
	private boolean czywal;
	private String typdokIdn;
	private String typdokIds;
	private Long typdokzak_id;
	private String symbol_waluty;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Boolean isAktywna() {
		return aktywna;
	}

	public void setAktywna(Boolean aktywna) {
		this.aktywna = aktywna;
	}

	public Boolean getCzywal() {
		return czywal;
	}

	public void setCzywal(Boolean czywal) {
		this.czywal = czywal;
	}

	public String getTypdokIdn() {
		return typdokIdn;
	}

	public void setTypdokIdn(String typdokIdn) {
		this.typdokIdn = typdokIdn;
	}

	public Long getTypdokzak_id() {
		return typdokzak_id;
	}

	public void setTypdokzak_id(Long typdokzak_id) {
		this.typdokzak_id = typdokzak_id;
	}
	
	public String getSymbol_waluty() {
		return symbol_waluty;
	}

	public void setSymbol_waluty(String symbol_waluty) {
		this.symbol_waluty = symbol_waluty;
	}
	public String getTypdokIds() {
		return typdokIds;
	}
	public void setTypdokIds(String typdokIds) {
		this.typdokIds = typdokIds;
	}
}
