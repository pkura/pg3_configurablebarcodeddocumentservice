package pl.compan.docusafe.general.canonical.model;

import java.math.BigDecimal;

public class PozycjaFaktury {
	private Long idPozycji;
	private ZadaniaProjektowe zadaniaProjektowe;
	private ZadaniaFinansowe zadaniaFinansowe;
	private Integer kwalifikacja;
	private ZrodloFinansowaniaProjektow zrodlo;
	private StawkaVat stawkaVat;
	private BudzetPozycja budzetPole;
	private BigDecimal kwotaNetto;
	private BigDecimal kwotaVat;
	private BigDecimal kwotaBrutto;
	private OPK opk;
	// brak na formatce private KosztRodzajowy kosztyRodzajowe;
	private PrzeznaczenieZakupu przeznaczenieZakupu;
	private ProduktyFaktury produktFaktury;
	private Projekt projekt;
	private ZleceniaProdukcyjne zlecenie;
	private Long pozycjaZamowieniaId;
	private WniosekORezerwacje wniosekORezerwacje;
	private Projekt projektZawOZrodlo;
	private Zrodlo zrodloZawezajaceProj;
	
	public PozycjaFaktury(){}


	public StawkaVat getStawkaVat() {
		return stawkaVat;
	}

	public void setStawkaVat(StawkaVat stawkaVat) {
		this.stawkaVat = stawkaVat;
	}

	public BigDecimal getKwotaNetto() {
		return kwotaNetto;
	}

	public void setKwotaNetto(BigDecimal kwotaNetto) {
		this.kwotaNetto = kwotaNetto;
	}

	public BigDecimal getKwotaVat() {
		return kwotaVat;
	}

	public void setKwotaVat(BigDecimal kwotaVat) {
		this.kwotaVat = kwotaVat;
	}

	public BigDecimal getKwotaBrutto() {
		return kwotaBrutto;
	}

	public void setKwotaBrutto(BigDecimal kwotaBrutto) {
		this.kwotaBrutto = kwotaBrutto;
	}

	public PrzeznaczenieZakupu getPrzeznaczenieZakupu() {
		return przeznaczenieZakupu;
	}

	public void setPrzeznaczenieZakupu(PrzeznaczenieZakupu przeznaczenieZakupu) {
		this.przeznaczenieZakupu = przeznaczenieZakupu;
	}

	public ZadaniaProjektowe getZadaniaProjektowe() {
		return zadaniaProjektowe;
	}

	public void setZadaniaProjektowe(ZadaniaProjektowe zadaniaProjektowe) {
		this.zadaniaProjektowe = zadaniaProjektowe;
	}

	public Integer getKwalifikacja() {
		return kwalifikacja;
	}

	public void setKwalifikacja(Integer kwalifikacja) {
		this.kwalifikacja = kwalifikacja;
	}
	
	public ZrodloFinansowaniaProjektow getZrodlo() {
		return zrodlo;
	}

	public void setZrodlo(ZrodloFinansowaniaProjektow zrodlo) {
		this.zrodlo = zrodlo;
	}

	public BudzetPozycja getBudzetPole() {
		return budzetPole;
	}

	public void setBudzetPole(BudzetPozycja budzetPole) {
		this.budzetPole = budzetPole;
	}

	public OPK getOpk() {
		return opk;
	}

	public void setOpk(OPK opk) {
		this.opk = opk;
	}

/*	public KosztRodzajowy getKosztyRodzajowe() {
		return kosztyRodzajowe;
	}

	public void setKosztyRodzajowe(KosztRodzajowy kosztyRodzajowe) {
		this.kosztyRodzajowe = kosztyRodzajowe;
	}*/


	public ProduktyFaktury getProduktFaktury() {
		return produktFaktury;
	}

	public void setProduktFaktury(ProduktyFaktury produktFaktury) {
		this.produktFaktury = produktFaktury;
	}

	public ZadaniaFinansowe getZadaniaFinansowe() {
		return zadaniaFinansowe;
	}

	public void setZadaniaFinansowe(ZadaniaFinansowe zadaniaFinansowe) {
		this.zadaniaFinansowe = zadaniaFinansowe;
	}
	
	public Projekt getProjekt() {
		return projekt;
	}

	public void setProjekt(Projekt projekt) {
		this.projekt = projekt;
	}

	public ZleceniaProdukcyjne getZlecenie() {
		return zlecenie;
	}

	public void setZlecenie(ZleceniaProdukcyjne zlecenie) {
		this.zlecenie = zlecenie;
	}

	public Long getPozycjaZamowieniaId() {
		return pozycjaZamowieniaId;
	}

	public void setPozycjaZamowieniaId(Long pozycjaZamowieniaId) {
		this.pozycjaZamowieniaId = pozycjaZamowieniaId;
	}

	public WniosekORezerwacje getWniosekORezerwacje() {
		return wniosekORezerwacje;
	}

	public void setWniosekORezerwacje(WniosekORezerwacje wniosekORezerwacje) {
		this.wniosekORezerwacje = wniosekORezerwacje;
	}

	public Projekt getProjektZawOZrodlo() {
		return projektZawOZrodlo;
	}

	public void setProjektZawOZrodlo(Projekt projektZawOZrodlo) {
		this.projektZawOZrodlo = projektZawOZrodlo;
	}

	public Zrodlo getZrodloZawezajaceProj() {
		return zrodloZawezajaceProj;
	}

	public void setZrodloZawezajaceProj(Zrodlo zrodloZawezajaceProj) {
		this.zrodloZawezajaceProj = zrodloZawezajaceProj;
	}


	public Long getIdPozycji() {
		return idPozycji;
	}


	public void setIdPozycji(Long idPozycji) {
		this.idPozycji = idPozycji;
	}
}
