package pl.compan.docusafe.general.canonical.model;

public class PrzeznaczenieZakupu{
	private Long id;
	private String cn;
	private String title;
	
	public PrzeznaczenieZakupu(){}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}
