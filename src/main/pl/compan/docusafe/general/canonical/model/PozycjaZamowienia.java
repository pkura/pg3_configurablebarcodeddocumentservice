package pl.compan.docusafe.general.canonical.model;

import java.math.BigDecimal;

public class PozycjaZamowienia {

	private  Long ID;
	private  Long DOCUMENT_ID_ZAMOWIENIA;
	private  Long DOCUMENT_ID_FAKTURY;
	private  Long POZZAM_ID;
	private     Long POZYCJAID;
	private    Long OKRES_BUDZETOWY;
	private   Long KOMORKA_BUDZETOWA;
	private   Long IDENTYFIKATOR_PLANU;
	private   Long IDENTYFIKATOR_BUDZETU;
	private    String POZYCJA_PLANU_ZAKUPOW;
	private    String POZYCJA_BUDZETU;
	private   Long PROJEKT;
	private   Long PRODUKT;
	private    String OPIS;
	private   BigDecimal ILOSC;
	private   BigDecimal ILOSC_ZREAL;
	private   Long JM;
	private   Long STAWKA_VAT;
	private   BigDecimal NETTO;
	private   BigDecimal BRUTTO;
	private   String UWAGI ;


	public Long getID() {
		return ID;
	}
	public void setID(Long iD) {
		ID = iD;
	}
	public Long getPOZYCJAID() {
		return POZYCJAID;
	}
	public void setPOZYCJAID(Long pOZYCJAID) {
		POZYCJAID = pOZYCJAID;
	}
	public Long getOKRES_BUDZETOWY() {
		return OKRES_BUDZETOWY;
	}
	public void setOKRES_BUDZETOWY(Long oKRES_BUDZETOWY) {
		OKRES_BUDZETOWY = oKRES_BUDZETOWY;
	}
	public Long getKOMORKA_BUDZETOWA() {
		return KOMORKA_BUDZETOWA;
	}
	public void setKOMORKA_BUDZETOWA(Long kOMORKA_BUDZETOWA) {
		KOMORKA_BUDZETOWA = kOMORKA_BUDZETOWA;
	}
	public Long getIDENTYFIKATOR_PLANU() {
		return IDENTYFIKATOR_PLANU;
	}
	public void setIDENTYFIKATOR_PLANU(Long iDENTYFIKATOR_PLANU) {
		IDENTYFIKATOR_PLANU = iDENTYFIKATOR_PLANU;
	}
	public Long getIDENTYFIKATOR_BUDZETU() {
		return IDENTYFIKATOR_BUDZETU;
	}
	public void setIDENTYFIKATOR_BUDZETU(Long iDENTYFIKATOR_BUDZETU) {
		IDENTYFIKATOR_BUDZETU = iDENTYFIKATOR_BUDZETU;
	}
	public String getPOZYCJA_PLANU_ZAKUPOW() {
		return POZYCJA_PLANU_ZAKUPOW;
	}
	public void setPOZYCJA_PLANU_ZAKUPOW(String pOZYCJA_PLANU_ZAKUPOW) {
		POZYCJA_PLANU_ZAKUPOW = pOZYCJA_PLANU_ZAKUPOW;
	}
	public String getPOZYCJA_BUDZETU() {
		return POZYCJA_BUDZETU;
	}
	public void setPOZYCJA_BUDZETU(String pOZYCJA_BUDZETU) {
		POZYCJA_BUDZETU = pOZYCJA_BUDZETU;
	}
	public Long getPROJEKT() {
		return PROJEKT;
	}
	public void setPROJEKT(Long pROJEKT) {
		PROJEKT = pROJEKT;
	}
	public Long getPRODUKT() {
		return PRODUKT;
	}
	public void setPRODUKT(Long pRODUKT) {
		PRODUKT = pRODUKT;
	}
	public String getOPIS() {
		return OPIS;
	}
	public void setOPIS(String oPIS) {
		OPIS = oPIS;
	}
	public BigDecimal getILOSC() {
		return ILOSC;
	}
	public void setILOSC(BigDecimal iLOSC) {
		ILOSC = iLOSC;
	}
	public Long getJM() {
		return JM;
	}
	public void setJM(Long jM) {
		JM = jM;
	}
	public Long getSTAWKA_VAT() {
		return STAWKA_VAT;
	}
	public void setSTAWKA_VAT(Long sTAWKA_VAT) {
		STAWKA_VAT = sTAWKA_VAT;
	}
	public BigDecimal getNETTO() {
		return NETTO;
	}
	public void setNETTO(BigDecimal nETTO) {
		NETTO = nETTO;
	}
	public BigDecimal getBRUTTO() {
		return BRUTTO;
	}
	public void setBRUTTO(BigDecimal bRUTTO) {
		BRUTTO = bRUTTO;
	}
	public String getUWAGI() {
		return UWAGI;
	}
	public void setUWAGI(String uWAGI) {
		UWAGI = uWAGI;
	}

	public BigDecimal getILOSC_ZREAL() {
		return ILOSC_ZREAL;
	}
	public void setILOSC_ZREAL(BigDecimal iLOSC_ZREAL) {
		ILOSC_ZREAL = iLOSC_ZREAL;
	}
	public Long getDOCUMENT_ID_ZAMOWIENIA() {
		return DOCUMENT_ID_ZAMOWIENIA;
	}
	public void setDOCUMENT_ID_ZAMOWIENIA(Long dOCUMENT_ID_ZAMOWIENIA) {
		DOCUMENT_ID_ZAMOWIENIA = dOCUMENT_ID_ZAMOWIENIA;
	}
	public Long getDOCUMENT_ID_FAKTURY() {
		return DOCUMENT_ID_FAKTURY;
	}
	public void setDOCUMENT_ID_FAKTURY(Long dOCUMENT_ID_FAKTURY) {
		DOCUMENT_ID_FAKTURY = dOCUMENT_ID_FAKTURY;
	}


}
