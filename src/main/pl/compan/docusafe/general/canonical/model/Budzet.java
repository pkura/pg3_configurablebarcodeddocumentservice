package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class Budzet {
	
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Long id;
	private double bdBudzetId;
	private String bdBudzetIdm;
	private double bdBudzetRodzajId;
	private double bdBudzetRodzajZadaniaId;
	private double bdBudzetRodzajZrodlaId;
	private double bdGrupaRodzajId;
	private String bdRodzaj;
	private String bdStrBudzetIdn;
	private String bdStrBudzetIds;
	private double bdSzablonPozId;
	private double bdZadanieId;
	private String bdZadanieIdn;
	private String budWOkrNazwa;
	private double budzetId;
	private String budzetIdm;
	private String budzetIds;
	private String budzetNazwa;
	private double czyAktywna;
	private String dokBdStrBudzetIdn;
	private String dokKomNazwa;
	private double dokPozLimit;
	private String dokPozNazwa;
	private int dokPozNrpoz;
	private double dokPozPKoszt;
	private double dokPozRKoszt;
	private double dokPozRezKoszt;
	private double okrrozlId;
	private double pozycjaPodrzednaId;
	private String strBudNazwa;
	private int wspolczynnikPozycji;
	private double wytworId;
	private String zadanieNazwa;
	private double zadaniePKoszt;
	private double zadanieRKoszt;
	private double zadanieRezKoszt;
	private double zrodloId;
	private String zrodloIdn;
	private String zrodloNazwa;
	private double zrodloPKoszt;
	private double zrodloRKoszt;
	private double zrodloRezKoszt;
	private boolean available;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getBdBudzetId() {
		return bdBudzetId;
	}
	public void setBdBudzetId(double bdBudzetId) {
		this.bdBudzetId = bdBudzetId;
	}
	public String getBdBudzetIdm() {
		return bdBudzetIdm;
	}
	public void setBdBudzetIdm(String bdBudzetIdm) {
		this.bdBudzetIdm = bdBudzetIdm;
	}
	public double getBdBudzetRodzajId() {
		return bdBudzetRodzajId;
	}
	public void setBdBudzetRodzajId(double bdBudzetRodzajId) {
		this.bdBudzetRodzajId = bdBudzetRodzajId;
	}
	public double getBdBudzetRodzajZadaniaId() {
		return bdBudzetRodzajZadaniaId;
	}
	public void setBdBudzetRodzajZadaniaId(double bdBudzetRodzajZadaniaId) {
		this.bdBudzetRodzajZadaniaId = bdBudzetRodzajZadaniaId;
	}
	public double getBdBudzetRodzajZrodlaId() {
		return bdBudzetRodzajZrodlaId;
	}
	public void setBdBudzetRodzajZrodlaId(double bdBudzetRodzajZrodlaId) {
		this.bdBudzetRodzajZrodlaId = bdBudzetRodzajZrodlaId;
	}
	public double getBdGrupaRodzajId() {
		return bdGrupaRodzajId;
	}
	public void setBdGrupaRodzajId(double bdGrupaRodzajId) {
		this.bdGrupaRodzajId = bdGrupaRodzajId;
	}
	public String getBdRodzaj() {
		return bdRodzaj;
	}
	public void setBdRodzaj(String bdRodzaj) {
		this.bdRodzaj = bdRodzaj;
	}
	public String getBdStrBudzetIdn() {
		return bdStrBudzetIdn;
	}
	public void setBdStrBudzetIdn(String bdStrBudzetIdn) {
		this.bdStrBudzetIdn = bdStrBudzetIdn;
	}
	public String getBdStrBudzetIds() {
		return bdStrBudzetIds;
	}
	public void setBdStrBudzetIds(String bdStrBudzetIds) {
		this.bdStrBudzetIds = bdStrBudzetIds;
	}
	public double getBdSzablonPozId() {
		return bdSzablonPozId;
	}
	public void setBdSzablonPozId(double bdSzablonPozId) {
		this.bdSzablonPozId = bdSzablonPozId;
	}
	public double getBdZadanieId() {
		return bdZadanieId;
	}
	public void setBdZadanieId(double bdZadanieId) {
		this.bdZadanieId = bdZadanieId;
	}
	public String getBdZadanieIdn() {
		return bdZadanieIdn;
	}
	public void setBdZadanieIdn(String bdZadanieIdn) {
		this.bdZadanieIdn = bdZadanieIdn;
	}
	public String getBudWOkrNazwa() {
		return budWOkrNazwa;
	}
	public void setBudWOkrNazwa(String budWOkrNazwa) {
		this.budWOkrNazwa = budWOkrNazwa;
	}
	public double getBudzetId() {
		return budzetId;
	}
	public void setBudzetId(double budzetId) {
		this.budzetId = budzetId;
	}
	public String getBudzetIdm() {
		return budzetIdm;
	}
	public void setBudzetIdm(String budzetIdm) {
		this.budzetIdm = budzetIdm;
	}
	public String getBudzetIds() {
		return budzetIds;
	}
	public void setBudzetIds(String budzetIds) {
		this.budzetIds = budzetIds;
	}
	public String getBudzetNazwa() {
		return budzetNazwa;
	}
	public void setBudzetNazwa(String budzetNazwa) {
		this.budzetNazwa = budzetNazwa;
	}
	public double getCzyAktywna() {
		return czyAktywna;
	}
	public void setCzyAktywna(double czyAktywna) {
		this.czyAktywna = czyAktywna;
	}
	public String getDokBdStrBudzetIdn() {
		return dokBdStrBudzetIdn;
	}
	public void setDokBdStrBudzetIdn(String dokBdStrBudzetIdn) {
		this.dokBdStrBudzetIdn = dokBdStrBudzetIdn;
	}
	public String getDokKomNazwa() {
		return dokKomNazwa;
	}
	public void setDokKomNazwa(String dokKomNazwa) {
		this.dokKomNazwa = dokKomNazwa;
	}
	public double getDokPozLimit() {
		return dokPozLimit;
	}
	public void setDokPozLimit(double dokPozLimit) {
		this.dokPozLimit = dokPozLimit;
	}
	public String getDokPozNazwa() {
		return dokPozNazwa;
	}
	public void setDokPozNazwa(String dokPozNazwa) {
		this.dokPozNazwa = dokPozNazwa;
	}
	public int getDokPozNrpoz() {
		return dokPozNrpoz;
	}
	public void setDokPozNrpoz(int dokPozNrpoz) {
		this.dokPozNrpoz = dokPozNrpoz;
	}
	public double getDokPozPKoszt() {
		return dokPozPKoszt;
	}
	public void setDokPozPKoszt(double dokPozPKoszt) {
		this.dokPozPKoszt = dokPozPKoszt;
	}
	public double getDokPozRKoszt() {
		return dokPozRKoszt;
	}
	public void setDokPozRKoszt(double dokPozRKoszt) {
		this.dokPozRKoszt = dokPozRKoszt;
	}
	public double getDokPozRezKoszt() {
		return dokPozRezKoszt;
	}
	public void setDokPozRezKoszt(double dokPozRezKoszt) {
		this.dokPozRezKoszt = dokPozRezKoszt;
	}
	public double getOkrrozlId() {
		return okrrozlId;
	}
	public void setOkrrozlId(double okrrozlId) {
		this.okrrozlId = okrrozlId;
	}
	public double getPozycjaPodrzednaId() {
		return pozycjaPodrzednaId;
	}
	public void setPozycjaPodrzednaId(double pozycjaPodrzednaId) {
		this.pozycjaPodrzednaId = pozycjaPodrzednaId;
	}
	public String getStrBudNazwa() {
		return strBudNazwa;
	}
	public void setStrBudNazwa(String strBudNazwa) {
		this.strBudNazwa = strBudNazwa;
	}
	public int getWspolczynnikPozycji() {
		return wspolczynnikPozycji;
	}
	public void setWspolczynnikPozycji(int wspolczynnikPozycji) {
		this.wspolczynnikPozycji = wspolczynnikPozycji;
	}
	public double getWytworId() {
		return wytworId;
	}
	public void setWytworId(double wytworId) {
		this.wytworId = wytworId;
	}
	public String getZadanieNazwa() {
		return zadanieNazwa;
	}
	public void setZadanieNazwa(String zadanieNazwa) {
		this.zadanieNazwa = zadanieNazwa;
	}
	public double getZadaniePKoszt() {
		return zadaniePKoszt;
	}
	public void setZadaniePKoszt(double zadaniePKoszt) {
		this.zadaniePKoszt = zadaniePKoszt;
	}
	public double getZadanieRKoszt() {
		return zadanieRKoszt;
	}
	public void setZadanieRKoszt(double zadanieRKoszt) {
		this.zadanieRKoszt = zadanieRKoszt;
	}
	public double getZadanieRezKoszt() {
		return zadanieRezKoszt;
	}
	public void setZadanieRezKoszt(double zadanieRezKoszt) {
		this.zadanieRezKoszt = zadanieRezKoszt;
	}
	public double getZrodloId() {
		return zrodloId;
	}
	public void setZrodloId(double zrodloId) {
		this.zrodloId = zrodloId;
	}
	public String getZrodloIdn() {
		return zrodloIdn;
	}
	public void setZrodloIdn(String zrodloIdn) {
		this.zrodloIdn = zrodloIdn;
	}
	public String getZrodloNazwa() {
		return zrodloNazwa;
	}
	public void setZrodloNazwa(String zrodloNazwa) {
		this.zrodloNazwa = zrodloNazwa;
	}
	public double getZrodloPKoszt() {
		return zrodloPKoszt;
	}
	public void setZrodloPKoszt(double zrodloPKoszt) {
		this.zrodloPKoszt = zrodloPKoszt;
	}
	public double getZrodloRKoszt() {
		return zrodloRKoszt;
	}
	public void setZrodloRKoszt(double zrodloRKoszt) {
		this.zrodloRKoszt = zrodloRKoszt;
	}
	public double getZrodloRezKoszt() {
		return zrodloRezKoszt;
	}
	public void setZrodloRezKoszt(double zrodloRezKoszt) {
		this.zrodloRezKoszt = zrodloRezKoszt;
	}
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
}
