package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class RodzajePlanowZakupu {

	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Long id;
	private Double cena;
	private boolean czy_aktywna;
	private Long komorkaRealizujaca;
	private String nazwa;
	private Long erpId;
	private int nrpoz;
	private String opis;
	private Double p_ilosc;
	private Double p_koszt;
	private String parent;
	private Long parentId;
	private String parentIdm;
	//TODO na serwisie jest i r_ilosc i rez_ilosc, ale ktore to wlasciwe, to do wyjasnienia
	private Double r_ilosc;
	private Double r_koszt;
	private Double rez_ilosc;
	private Double rez_koszt;
	private Long wytwor_id;
	private String wytwor_idm;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public boolean getCzy_aktywna() {
		return czy_aktywna;
	}
	public void setCzy_aktywna(boolean czy_aktywna) {
		this.czy_aktywna = czy_aktywna;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
	public int getNrpoz() {
		return nrpoz;
	}
	public void setNrpoz(int nrpoz) {
		this.nrpoz = nrpoz;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public double getP_ilosc() {
		return p_ilosc;
	}
	public void setP_ilosc(double p_ilosc) {
		this.p_ilosc = p_ilosc;
	}
	public double getP_koszt() {
		return p_koszt;
	}
	public void setP_koszt(double p_koszt) {
		this.p_koszt = p_koszt;
	}
	public double getRez_ilosc() {
		return rez_ilosc;
	}
	public void setRez_ilosc(double r_ilosc) {
		this.rez_ilosc = r_ilosc;
	}
	public double getRez_koszt() {
		return rez_koszt;
	}
	public void setRez_koszt(double r_koszt) {
		this.rez_koszt = r_koszt;
	}
	public long getWytwor_id() {
		return wytwor_id;
	}
	public Long getKomorkaRealizujaca() {
		return komorkaRealizujaca;
	}
	public void setKomorkaRealizujaca(Long komorkaRealizujaca) {
		this.komorkaRealizujaca = komorkaRealizujaca;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getParentIdm() {
		return parentIdm;
	}
	public void setParentIdm(String parentIdm) {
		this.parentIdm = parentIdm;
	}
	public Double getR_ilosc() {
		return r_ilosc;
	}
	public void setR_ilosc(Double r_ilosc) {
		this.r_ilosc = r_ilosc;
	}
	public Double getR_koszt() {
		return r_koszt;
	}
	public void setR_koszt(Double r_koszt) {
		this.r_koszt = r_koszt;
	}
	public String getWytwor_idm() {
		return wytwor_idm;
	}
	public void setWytwor_idm(String wytwor_idm) {
		this.wytwor_idm = wytwor_idm;
	}
	public void setCena(Double cena) {
		this.cena = cena;
	}
	public void setP_ilosc(Double p_ilosc) {
		this.p_ilosc = p_ilosc;
	}
	public void setP_koszt(Double p_koszt) {
		this.p_koszt = p_koszt;
	}
	public void setRez_ilosc(Double rez_ilosc) {
		this.rez_ilosc = rez_ilosc;
	}
	public void setRez_koszt(Double rez_koszt) {
		this.rez_koszt = rez_koszt;
	}
	public void setWytwor_id(Long wytwor_id) {
		this.wytwor_id = wytwor_id;
	}
}
