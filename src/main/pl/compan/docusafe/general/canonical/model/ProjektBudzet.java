package pl.compan.docusafe.general.canonical.model;

import java.util.Date;

import pl.compan.docusafe.general.ObjectUtils;

public class ProjektBudzet {

	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Long id;
	private Long erpId;
	private String idm;
	private String nazwa;
	private Date p_datako;
	private Date p_datapo;
	private String parent;
	private Long parent_id;
	private String parent_idm;
/*	private String cn;
	private String title;
	private Integer centrum;	
	private String refValue;
	private Integer available;*/
	//private Double projekt_id;
	private Boolean stan;
	private Boolean status;
	private Boolean available;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
	public Date getP_datako() {
		return p_datako;
	}
	public void setP_datako(Date p_datako) {
		this.p_datako = p_datako;
	}
	public Date getP_datapo() {
		return p_datapo;
	}
	public void setP_datapo(Date p_datapo) {
		this.p_datapo = p_datapo;
	}
	public Boolean getStan() {
		return stan;
	}
	public void setStan(Boolean stan) {
		this.stan = stan;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getIdm() {
		return idm;
	}
	public void setIdm(String idm) {
		this.idm = idm;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public Long getParent_id() {
		return parent_id;
	}
	public void setParent_id(Long parent_id) {
		this.parent_id = parent_id;
	}
	public String getParent_idm() {
		return parent_idm;
	}
	public void setParent_idm(String parent_idm) {
		this.parent_idm = parent_idm;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
}
