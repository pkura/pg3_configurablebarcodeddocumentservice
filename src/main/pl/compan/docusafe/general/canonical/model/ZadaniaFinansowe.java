package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class ZadaniaFinansowe {
	
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Long id;
	private String cn;
	private String title;
	private Integer centrum;	
	private String refValue;
	private Integer available;
	private Double identyfikator_num;
	private Long erpId;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getCentrum() {
		return centrum;
	}
	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}
	public String getRefValue() {
		return refValue;
	}
	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}
	public Integer getAvailable() {
		return available;
	}
	public void setAvailable(Integer available) {
		this.available = available;
	}
	public Double getIdentyfikator_num() {
		return identyfikator_num;
	}
	public void setIdentyfikator_num(Double identyfikator_num) {
		this.identyfikator_num = identyfikator_num;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
	
}
