package pl.compan.docusafe.general.canonical.model;

import java.math.BigDecimal;

import pl.compan.docusafe.general.ObjectUtils;

public class BudzetPozycjaSimple {

	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Long id;
	private Long bd_nad_rodzaj_ko_id;
	private Boolean czy_aktywna;
	private Long erpId;
	private String nazwa;
	private Long nrpoz;
	private String opis;
	private BigDecimal p_koszt;
	private String parent;
	private Long parentId;
	private String parentIdm;
	private BigDecimal r_koszt;
	private BigDecimal rez_koszt;
	private Long wytworId;
	private String wytworIdm;
	private Boolean available;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBd_nad_rodzaj_ko_id() {
		return bd_nad_rodzaj_ko_id;
	}
	public void setBd_nad_rodzaj_ko_id(Long bd_nad_rodzaj_ko_id) {
		this.bd_nad_rodzaj_ko_id = bd_nad_rodzaj_ko_id;
	}
	public Boolean getCzy_aktywna() {
		return czy_aktywna;
	}
	public void setCzy_aktywna(Boolean czy_aktywna) {
		this.czy_aktywna = czy_aktywna;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public Long getNrpoz() {
		return nrpoz;
	}
	public void setNrpoz(Long nrpoz) {
		this.nrpoz = nrpoz;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public BigDecimal getP_koszt() {
		return p_koszt;
	}
	public void setP_koszt(BigDecimal p_koszt) {
		this.p_koszt = p_koszt;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getParentIdm() {
		return parentIdm;
	}
	public void setParentIdm(String parentIdm) {
		this.parentIdm = parentIdm;
	}
	public BigDecimal getR_koszt() {
		return r_koszt;
	}
	public void setR_koszt(BigDecimal r_koszt) {
		this.r_koszt = r_koszt;
	}
	public BigDecimal getRez_koszt() {
		return rez_koszt;
	}
	public void setRez_koszt(BigDecimal rez_koszt) {
		this.rez_koszt = rez_koszt;
	}
	public Long getWytworId() {
		return wytworId;
	}
	public void setWytworId(Long wytworId) {
		this.wytworId = wytworId;
	}
	public String getWytworIdm() {
		return wytworIdm;
	}
	public void setWytworIdm(String wytworIdm) {
		this.wytworIdm = wytworIdm;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
	
	
	
	
}
