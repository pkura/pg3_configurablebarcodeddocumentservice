package pl.compan.docusafe.general.canonical.model;

import java.util.Date;

import pl.compan.docusafe.general.ObjectUtils;

public class OkresBudzetowy {

	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private boolean czy_archiwalny;
	private Date Data_obow_do;
	private Date Data_obow_od;
	private Long id;
	private Long erpId;
	private String idm;
	private String nazwa;
	private int wersja;
	private Boolean available;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public boolean getCzy_archiwalny() {
		return czy_archiwalny;
	}
	public void setCzy_archiwalny(boolean czy_archiwalny) {
		this.czy_archiwalny = czy_archiwalny;
	}
	public Date getData_obow_do() {
		return Data_obow_do;
	}
	public void setData_obow_do(Date data_obow_do) {
		Data_obow_do = data_obow_do;
	}
	public Date getData_obow_od() {
		return Data_obow_od;
	}
	public void setData_obow_od(Date data_obow_od) {
		Data_obow_od = data_obow_od;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
	public String getIdm() {
		return idm;
	}
	public void setIdm(String idm) {
		this.idm = idm;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public int getWersja() {
		return wersja;
	}
	public void setWersja(int wersja) {
		this.wersja = wersja;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
}
