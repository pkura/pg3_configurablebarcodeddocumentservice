package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class KontrahentKonto {
	
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	Long id;
	Long bank_id;
	String bank_idn;
	String bank_nazwa;
	Boolean czyIban;
	Long dostawca_id;
	String dostawca_idn;
	Long erpId;
	String idm;
	int kolejnosc_uzycia;
	String nazwa;
	String numer_konta;
	String symbol_waluty;
	Long typKurs_id;
	String typKurs_idn;
	Long waluta_id;
	Long zrodKursWal_id;
	String zrodKursWal_idn;
	String zrodKursWal_nazwa;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBank_id() {
		return bank_id;
	}
	public void setBank_id(Long bank_id) {
		this.bank_id = bank_id;
	}
	public String getBank_idn() {
		return bank_idn;
	}
	public void setBank_idn(String bank_idn) {
		this.bank_idn = bank_idn;
	}
	public String getBank_nazwa() {
		return bank_nazwa;
	}
	public void setBank_nazwa(String bank_nazwa) {
		this.bank_nazwa = bank_nazwa;
	}
	public Boolean getCzyIban() {
		return czyIban;
	}
	public void setCzyIban(Boolean czyIban) {
		this.czyIban = czyIban;
	}
	public Long getDostawca_id() {
		return dostawca_id;
	}
	public void setDostawca_id(Long dostawca_id) {
		this.dostawca_id = dostawca_id;
	}
	public String getDostawca_idn() {
		return dostawca_idn;
	}
	public void setDostawca_idn(String dostawca_idn) {
		this.dostawca_idn = dostawca_idn;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
	public String getIdm() {
		return idm;
	}
	public void setIdm(String idm) {
		this.idm = idm;
	}
	public int getKolejnosc_uzycia() {
		return kolejnosc_uzycia;
	}
	public void setKolejnosc_uzycia(int kolejnosc_uzycia) {
		this.kolejnosc_uzycia = kolejnosc_uzycia;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String getNumer_konta() {
		return numer_konta;
	}
	public void setNumer_konta(String numer_konta) {
		this.numer_konta = numer_konta;
	}
	public String getSymbol_waluty() {
		return symbol_waluty;
	}
	public void setSymbol_waluty(String symbol_waluty) {
		this.symbol_waluty = symbol_waluty;
	}
	public Long getTypKurs_id() {
		return typKurs_id;
	}
	public void setTypKurs_id(Long typKurs_id) {
		this.typKurs_id = typKurs_id;
	}
	public String getTypKurs_idn() {
		return typKurs_idn;
	}
	public void setTypKurs_idn(String typKurs_idn) {
		this.typKurs_idn = typKurs_idn;
	}
	public Long getWaluta_id() {
		return waluta_id;
	}
	public void setWaluta_id(Long waluta_id) {
		this.waluta_id = waluta_id;
	}
	public Long getZrodKursWal_id() {
		return zrodKursWal_id;
	}
	public void setZrodKursWal_id(Long zrodKursWal_id) {
		this.zrodKursWal_id = zrodKursWal_id;
	}
	public String getZrodKursWal_idn() {
		return zrodKursWal_idn;
	}
	public void setZrodKursWal_idn(String zrodKursWal_idn) {
		this.zrodKursWal_idn = zrodKursWal_idn;
	}
	public String getZrodKursWal_nazwa() {
		return zrodKursWal_nazwa;
	}
	public void setZrodKursWal_nazwa(String zrodKursWal_nazwa) {
		this.zrodKursWal_nazwa = zrodKursWal_nazwa;
	}
}
