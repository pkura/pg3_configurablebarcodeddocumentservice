package pl.compan.docusafe.general.canonical.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.core.users.sql.UserImpl;

public class Faktura {
	private Long id;
	
	private Integer status;
	private Date dataWplywu; 
	
	private Recipient adresat;
	
	private BigDecimal kwotaNetto; 
	private BigDecimal kwotaBrutto;
	private BigDecimal kwotaVat;
	
	private Waluta waluta;
	private Date terminPlatnosci;

	private Attachment zalacznik; 
	
	private Integer rodzajFaktury;
	private Boolean fakturaZaKosztyTrwale;
	private Boolean fakturaNaWydziale;
	
	private DivisionImpl pelnomocnikDoSprawFinansowych;
	private String numerFaktury;
	private String kodKreskowy;
	
	private Kontrahent wystawcaFaktury;
	
	private Date dataWystawienia;
	private BigDecimal kwotaWWalucie;
	private BigDecimal kurs;
	
	private TypPlatnosci typPlatnosci;
	private String nazwaTowaruUslugi;
	private String dodatkoweInformacje;
	
	private Boolean wydatekJestStrukturalny;
	private List<WydatekStrukturalny> wydatkiStrukturalne;
	
	private Boolean skierujDoUzupelnieniaOpisu;
	private UserImpl osobaUzupelniajacaOpisFaktury;
	private String numerPostepowaniaPrzetargowego;
	
	private Integer trybPostepowania;
	private Integer wynikaZPostepowaniaPrzetargowego;
	
	private UserImpl pracownikKsiegowosci;
	private UserImpl glownyKsiegowy;
	
	private Kontrahent dostawca;
	
	private Boolean zamowienieNaMagazyn;
	
	private Boolean czyKorekta;
	private String korektaNumer;
	
	private TypDokumentu typFaktury;
	
	private Long numerDokumentuKorygowanego;
	
	private Journal dziennik;
	private String numerPrzesylkiRejestrowanej;
	private MiejsceDostawy stanowiskoDostaw;
	
	private String opisMerytoryczny;
	private String przeznaczenieIUwagi;
	private Boolean czySzablon;
	private SzablonyRozliczenFaktur szablon;
	
	private List<BudzetPozycja> pozycjeBudzetowe;

	private List<PozycjaFaktury> pozycjeFaktury;

	private BigDecimal kwotaPozostalaNetto;
	private BigDecimal kwotaPozostala;
	private BigDecimal kwotaPozostalaVAT;
	private Boolean potwierdzamOdbiorZgodnieZUmowa;
	private Boolean potwierdzamOdbiorZgodnieZFaktura;
	
	private String numerKontaBankowego;
	private Integer sposobWyplaty;
	private Boolean czyZZamowienia;
	private Long documentIdZamowienia;
	private List<PozycjaZamowienia> pozycjeZZamowienia;
	private Boolean ZAAKCEPTOWANOPOZZAM;
	private Boolean czyRozliczenieMediow;
	private Boolean czyZWniosku;
	private Boolean zaakceptowanoWnioskiORez;
    private ZamowienieSimplePozycje zamowieniePozycje;
	private NumeryZamowien zamowienie;
	private Boolean wygenerowanoMetryczke;
	
	public Faktura(){}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getDataWplywu() {
		return dataWplywu;
	}

	public void setDataWplywu(Date dataWplywu) {
		this.dataWplywu = dataWplywu;
	}

	public Recipient getAdresat() {
		return adresat;
	}

	public void setAdresat(Recipient adresat) {
		this.adresat = adresat;
	}

	public BigDecimal getKwotaNetto() {
		return kwotaNetto;
	}

	public void setKwotaNetto(BigDecimal kwotaNetto) {
		this.kwotaNetto = kwotaNetto;
	}

	public BigDecimal getKwotaBrutto() {
		return kwotaBrutto;
	}

	public void setKwotaBrutto(BigDecimal kwotaBrutto) {
		this.kwotaBrutto = kwotaBrutto;
	}

	public BigDecimal getKwotaVat() {
		return kwotaVat;
	}

	public void setKwotaVat(BigDecimal kwotaVat) {
		this.kwotaVat = kwotaVat;
	}

	public Waluta getWaluta() {
		return waluta;
	}

	public void setWaluta(Waluta waluta) {
		this.waluta = waluta;
	}

	public Date getTerminPlatnosci() {
		return terminPlatnosci;
	}

	public void setTerminPlatnosci(Date terminPlatnosci) {
		this.terminPlatnosci = terminPlatnosci;
	}

	public Attachment getZalacznik() {
		return zalacznik;
	}

	public void setZalacznik(Attachment zalacznik) {
		this.zalacznik = zalacznik;
	}

	public Integer getRodzajFaktury() {
		return rodzajFaktury;
	}

	public void setRodzajFaktury(Integer rodzajFaktury) {
		this.rodzajFaktury = rodzajFaktury;
	}

	public Boolean getFakturaNaWydziale() {
		return fakturaNaWydziale;
	}

	public void setFakturaNaWydziale(Boolean fakturaNaWydziale) {
		this.fakturaNaWydziale = fakturaNaWydziale;
	}

	public DivisionImpl getPelnomocnikDoSprawFinansowych() {
		return pelnomocnikDoSprawFinansowych;
	}

	public void setPelnomocnikDoSprawFinansowych(
			DivisionImpl pelnomocnikDoSprawFinansowych) {
		this.pelnomocnikDoSprawFinansowych = pelnomocnikDoSprawFinansowych;
	}

	public String getNumerFaktury() {
		return numerFaktury;
	}

	public void setNumerFaktury(String numerFaktury) {
		this.numerFaktury = numerFaktury;
	}

	public String getKodKreskowy() {
		return kodKreskowy;
	}

	public void setKodKreskowy(String kodKreskowy) {
		this.kodKreskowy = kodKreskowy;
	}

	public Kontrahent getWystawcaFaktury() {
		return wystawcaFaktury;
	}

	public void setWystawcaFaktury(Kontrahent wystawcaFaktury) {
		this.wystawcaFaktury = wystawcaFaktury;
	}

	public Date getDataWystawienia() {
		return dataWystawienia;
	}

	public void setDataWystawienia(Date dataWystawienia) {
		this.dataWystawienia = dataWystawienia;
	}

	public BigDecimal getKwotaWWalucie() {
		return kwotaWWalucie;
	}

	public void setKwotaWWalucie(BigDecimal kwotaWWalucie) {
		this.kwotaWWalucie = kwotaWWalucie;
	}

	public BigDecimal getKurs() {
		return kurs;
	}

	public void setKurs(BigDecimal kurs) {
		this.kurs = kurs;
	}

	public TypPlatnosci getTypPlatnosci() {
		return typPlatnosci;
	}

	public void setTypPlatnosci(TypPlatnosci typPlatnosci) {
		this.typPlatnosci = typPlatnosci;
	}

	public String getNazwaTowaruUslugi() {
		return nazwaTowaruUslugi;
	}

	public void setNazwaTowaruUslugi(String nazwaTowaruUslugi) {
		this.nazwaTowaruUslugi = nazwaTowaruUslugi;
	}

	public String getDodatkoweInformacje() {
		return dodatkoweInformacje;
	}

	public void setDodatkoweInformacje(String dodatkoweInformacje) {
		this.dodatkoweInformacje = dodatkoweInformacje;
	}

	public Boolean getWydatekJestStrukturalny() {
		return wydatekJestStrukturalny;
	}

	public void setWydatekJestStrukturalny(Boolean wydatekJestStrukturalny) {
		this.wydatekJestStrukturalny = wydatekJestStrukturalny;
	}

	public List<WydatekStrukturalny> getWydatkiStrukturalne() {
		return wydatkiStrukturalne;
	}

	public void setWydatkiStrukturalne(List<WydatekStrukturalny> wydatkiStrukturalne) {
		this.wydatkiStrukturalne = wydatkiStrukturalne;
	}

	public Boolean getSkierujDoUzupelnieniaOpisu() {
		return skierujDoUzupelnieniaOpisu;
	}

	public void setSkierujDoUzupelnieniaOpisu(Boolean skierujDoUzupelnieniaOpisu) {
		this.skierujDoUzupelnieniaOpisu = skierujDoUzupelnieniaOpisu;
	}

	public UserImpl getOsobaUzupelniajacaOpisFaktury() {
		return osobaUzupelniajacaOpisFaktury;
	}

	public void setOsobaUzupelniajacaOpisFaktury(
			UserImpl osobaUzupelniajacaOpisFaktury) {
		this.osobaUzupelniajacaOpisFaktury = osobaUzupelniajacaOpisFaktury;
	}

	public String getNumerPostepowaniaPrzetargowego() {
		return numerPostepowaniaPrzetargowego;
	}

	public void setNumerPostepowaniaPrzetargowego(
			String numerPostepowaniaPrzetargowego) {
		this.numerPostepowaniaPrzetargowego = numerPostepowaniaPrzetargowego;
	}

	public Integer getTrybPostepowania() {
		return trybPostepowania;
	}

	public void setTrybPostepowania(Integer trybPostepowania) {
		this.trybPostepowania = trybPostepowania;
	}

	public Integer getWynikaZPostepowaniaPrzetargowego() {
		return wynikaZPostepowaniaPrzetargowego;
	}

	public void setWynikaZPostepowaniaPrzetargowego(
			Integer wynikaZPostepowaniaPrzetargowego) {
		this.wynikaZPostepowaniaPrzetargowego = wynikaZPostepowaniaPrzetargowego;
	}

	public UserImpl getPracownikKsiegowosci() {
		return pracownikKsiegowosci;
	}

	public void setPracownikKsiegowosci(UserImpl pracownikKsiegowosci) {
		this.pracownikKsiegowosci = pracownikKsiegowosci;
	}

	public UserImpl getGlownyKsiegowy() {
		return glownyKsiegowy;
	}

	public void setGlownyKsiegowy(UserImpl glownyKsiegowy) {
		this.glownyKsiegowy = glownyKsiegowy;
	}

	public Kontrahent getDostawca() {
		return dostawca;
	}

	public void setDostawca(Kontrahent dostawca) {
		this.dostawca = dostawca;
	}

	public Boolean getZamowienieNaMagazyn() {
		return zamowienieNaMagazyn;
	}

	public void setZamowienieNaMagazyn(Boolean zamowienieNaMagazyn) {
		this.zamowienieNaMagazyn = zamowienieNaMagazyn;
	}

	public Boolean getCzyKorekta() {
		return czyKorekta;
	}

	public void setCzyKorekta(Boolean czyKorekta) {
		this.czyKorekta = czyKorekta;
	}

	public TypDokumentu getTypFaktury() {
		return typFaktury;
	}

	public void setTypFaktury(TypDokumentu typFaktury) {
		this.typFaktury = typFaktury;
	}

	public Long getNumerDokumentuKorygowanego() {
		return numerDokumentuKorygowanego;
	}

	public void setNumerDokumentuKorygowanego(Long numerDokumentuKorygowanego) {
		this.numerDokumentuKorygowanego = numerDokumentuKorygowanego;
	}

	public Journal getDziennik() {
		return dziennik;
	}

	public void setDziennik(Journal dziennik) {
		this.dziennik = dziennik;
	}

	public String getNumerPrzesylkiRejestrowanej() {
		return numerPrzesylkiRejestrowanej;
	}

	public void setNumerPrzesylkiRejestrowanej(String numerPrzesylkiRejestrowanej) {
		this.numerPrzesylkiRejestrowanej = numerPrzesylkiRejestrowanej;
	}

	public MiejsceDostawy getStanowiskoDostaw() {
		return stanowiskoDostaw;
	}

	public void setStanowiskoDostaw(MiejsceDostawy stanowiskoDostaw) {
		this.stanowiskoDostaw = stanowiskoDostaw;
	}

	public String getOpisMerytoryczny() {
		return opisMerytoryczny;
	}

	public void setOpisMerytoryczny(String opisMerytoryczny) {
		this.opisMerytoryczny = opisMerytoryczny;
	}

	public String getPrzeznaczenieIUwagi() {
		return przeznaczenieIUwagi;
	}

	public void setPrzeznaczenieIUwagi(String przeznaczenieIUwagi) {
		this.przeznaczenieIUwagi = przeznaczenieIUwagi;
	}

	public BigDecimal getKwotaPozostala() {
		return kwotaPozostala;
	}

	public void setKwotaPozostala(BigDecimal kwotaPozostala) {
		this.kwotaPozostala = kwotaPozostala;
	}

	public Boolean getPotwierdzamOdbiorZgodnieZUmowa() {
		return potwierdzamOdbiorZgodnieZUmowa;
	}

	public void setPotwierdzamOdbiorZgodnieZUmowa(
			Boolean potwierdzamOdbiorZgodnieZUmowa) {
		this.potwierdzamOdbiorZgodnieZUmowa = potwierdzamOdbiorZgodnieZUmowa;
	}

	public Boolean getPotwierdzamOdbiorZgodnieZFaktura() {
		return potwierdzamOdbiorZgodnieZFaktura;
	}

	public void setPotwierdzamOdbiorZgodnieZFaktura(
			Boolean potwierdzamOdbiorZgodnieZFaktura) {
		this.potwierdzamOdbiorZgodnieZFaktura = potwierdzamOdbiorZgodnieZFaktura;
	}

	public List<PozycjaFaktury> getPozycjeFaktury() {
		return pozycjeFaktury;
	}

	public void setPozycjeFaktury(List<PozycjaFaktury> pozycjeFaktury) {
		this.pozycjeFaktury = pozycjeFaktury;
	}

	public List<BudzetPozycja> getPozycjeBudzetowe() {
		return pozycjeBudzetowe;
	}

	public void setPozycjeBudzetowe(List<BudzetPozycja> pozycjeBudzetowe) {
		this.pozycjeBudzetowe = pozycjeBudzetowe;
	}

	public Boolean getFakturaZaKosztyTrwale() {
		return fakturaZaKosztyTrwale;
	}

	public void setFakturaZaKosztyTrwale(Boolean fakturaZaKosztyTrwale) {
		this.fakturaZaKosztyTrwale = fakturaZaKosztyTrwale;
	}

	public String getNumerKontaBankowego() {
		return numerKontaBankowego;
	}

	public void setNumerKontaBankowego(String numerKontaBankowego) {
		this.numerKontaBankowego = numerKontaBankowego;
	}

	public BigDecimal getKwotaPozostalaNetto() {
		return kwotaPozostalaNetto;
	}

	public void setKwotaPozostalaNetto(BigDecimal kwotaPozostalaNetto) {
		this.kwotaPozostalaNetto = kwotaPozostalaNetto;
	}

	public BigDecimal getKwotaPozostalaVAT() {
		return kwotaPozostalaVAT;
	}

	public void setKwotaPozostalaVAT(BigDecimal kwotaPozostalaVAT) {
		this.kwotaPozostalaVAT = kwotaPozostalaVAT;
	}

	public String getKorektaNumer() {
		return korektaNumer;
	}

	public void setKorektaNumer(String korektaNumer) {
		this.korektaNumer = korektaNumer;
	}

	public Integer getSposobWyplaty() {
		return sposobWyplaty;
	}

	public void setSposobWyplaty(Integer sposobWyplaty) {
		this.sposobWyplaty = sposobWyplaty;
	}

	public Boolean getCzySzablon() {
		return czySzablon;
	}

	public void setCzySzablon(Boolean czySzablon) {
		this.czySzablon = czySzablon;
	}

	public SzablonyRozliczenFaktur getSzablon() {
		return szablon;
	}

	public void setSzablon(SzablonyRozliczenFaktur szablon) {
		this.szablon = szablon;
	}

	public Boolean getCzyZZamowienia() {
		return czyZZamowienia;
	}

	public void setCzyZZamowienia(Boolean czyZZamowienia) {
		this.czyZZamowienia = czyZZamowienia;
	}

	public Long getDocumentIdZamowienia() {
		return documentIdZamowienia;
	}

	public void setDocumentIdZamowienia(Long documentIdZamowienia) {
		this.documentIdZamowienia = documentIdZamowienia;
	}

	public List<PozycjaZamowienia> getPozycjeZZamowienia() {
		return pozycjeZZamowienia;
	}

	public void setPozycjeZZamowienia(List<PozycjaZamowienia> pozycjeZZamowienia) {
		this.pozycjeZZamowienia = pozycjeZZamowienia;
	}

	public Boolean getZAAKCEPTOWANOPOZZAM() {
		return ZAAKCEPTOWANOPOZZAM;
	}

	public void setZAAKCEPTOWANOPOZZAM(Boolean zAAKCEPTOWANOPOZZAM) {
		ZAAKCEPTOWANOPOZZAM = zAAKCEPTOWANOPOZZAM;
	}

	public Boolean getCzyRozliczenieMediow() {
		return czyRozliczenieMediow;
	}

	public void setCzyRozliczenieMediow(Boolean czyRozliczenieMediow) {
		this.czyRozliczenieMediow = czyRozliczenieMediow;
	}

	public Boolean getCzyZWniosku() {
		return czyZWniosku;
	}

	public void setCzyZWniosku(Boolean czyZWniosku) {
		this.czyZWniosku = czyZWniosku;
	}

	public Boolean getZaakceptowanoWnioskiORez() {
		return zaakceptowanoWnioskiORez;
	}

	public void setZaakceptowanoWnioskiORez(Boolean zaakceptowanoWnioskiORez) {
		this.zaakceptowanoWnioskiORez = zaakceptowanoWnioskiORez;
	}

	public ZamowienieSimplePozycje getZamowieniePozycje() {
		return zamowieniePozycje;
	}

	public void setZamowieniePozycje(ZamowienieSimplePozycje zamowieniePozycje) {
		this.zamowieniePozycje = zamowieniePozycje;
	}

	public NumeryZamowien getZamowienie() {
		return zamowienie;
	}

	public void setZamowienie(NumeryZamowien zamowienie) {
		this.zamowienie = zamowienie;
	}

	public Boolean getWygenerowanoMetryczke() {
		return wygenerowanoMetryczke;
	}

	public void setWygenerowanoMetryczke(Boolean wygenerowanoMetryczke) {
		this.wygenerowanoMetryczke = wygenerowanoMetryczke;
	}



}