package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class Kontrahent {
	
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private long id;
	private String imie;
	private String nazwisko;
	private String nazwa;
	private String ulica;
	private String nrDomu;
	private String nrMieszkania;
	private String miejscowosc;
	private String nip;
	private String regon;
	private String kraj;
	private String email;
	private String faks;
	private String telefon;
	private String kodpocztowy;
	private String nrKonta;
	private Long erpId;
	private boolean nowy;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}
	
	public String getNrDomu() {
		return nrDomu;
	}
	
	public void setNrDomu(String nrDomu) {
		this.nrDomu=nrDomu;
	}
	
	public String getNrMieszkania() {
		return nrMieszkania;
	}
	
	public void setNrMieszkania(String nrMieszkania) {
		this.nrMieszkania=nrMieszkania;
	}
	
	public String getMiejscowosc() {
		return miejscowosc;
	}

	public void setMiejscowosc(String miejscowosc) {
		this.miejscowosc = miejscowosc;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getRegon() {
		return regon;
	}

	public void setRegon(String regon) {
		this.regon = regon;
	}

	public String getKraj() {
		return kraj;
	}

	public void setKraj(String kraj) {
		this.kraj = kraj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFaks() {
		return faks;
	}

	public void setFaks(String faks) {
		this.faks = faks;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getKodpocztowy() {
		return kodpocztowy;
	}

	public void setKodpocztowy(String kodpocztowy) {
		this.kodpocztowy = kodpocztowy;
	}

	public void setNrKonta(String nrKonta) {
		this.nrKonta = nrKonta;
	}

	public String getNrKonta() {
		return nrKonta;
	}
	
	public Long getErpId() {
		return erpId;
	}
	
	public void setErpId(Long erpId) {
		this.erpId=erpId;
	}
	
	public Boolean getNowy() {
		return nowy;
	}
	
	public void setNowy(Boolean nowy) {
		this.nowy=nowy;
	}
}
