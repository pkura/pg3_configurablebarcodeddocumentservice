package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;


public class BudzetDoKomorki {
	private Long id;
    private Long budzet_id;
    private String budzet_idn;
    private Long budzet_nad_id;
    private String budzet_nad_idn;
    private Long okresId;
    private Boolean czyWiodaca;
    private Long komorkaId;
    private String komorkaIdn;
    private String nazwaKomorkiBudzetowej;
    private String nazwaKomorkiOrg;
    
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
    
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBudzet_id() {
		return budzet_id;
	}
	public void setBudzet_id(Long budzet_id) {
		this.budzet_id = budzet_id;
	}
	public String getBudzet_idn() {
		return budzet_idn;
	}
	public void setBudzet_idn(String budzet_idn) {
		this.budzet_idn = budzet_idn;
	}
	public Long getBudzet_nad_id() {
		return budzet_nad_id;
	}
	public void setBudzet_nad_id(Long budzet_nad_id) {
		this.budzet_nad_id = budzet_nad_id;
	}
	public String getBudzet_nad_idn() {
		return budzet_nad_idn;
	}
	public void setBudzet_nad_idn(String budzet_nad_idn) {
		this.budzet_nad_idn = budzet_nad_idn;
	}
	public Long getOkresId() {
		return okresId;
	}
	public void setOkresId(Long okresId) {
		this.okresId = okresId;
	}
	public Boolean getCzyWiodaca() {
		return czyWiodaca;
	}
	public void setCzyWiodaca(Boolean czyWiodaca) {
		this.czyWiodaca = czyWiodaca;
	}
	public Long getKomorkaId() {
		return komorkaId;
	}
	public void setKomorkaId(Long komorkaId) {
		this.komorkaId = komorkaId;
	}
	public String getKomorkaIdn() {
		return komorkaIdn;
	}
	public void setKomorkaIdn(String komorkaIdn) {
		this.komorkaIdn = komorkaIdn;
	}
	public String getNazwaKomorkiBudzetowej() {
		return nazwaKomorkiBudzetowej;
	}
	public void setNazwaKomorkiBudzetowej(String nazwaKomorkiBudzetowej) {
		this.nazwaKomorkiBudzetowej = nazwaKomorkiBudzetowej;
	}
	public String getNazwaKomorkiOrg() {
		return nazwaKomorkiOrg;
	}
	public void setNazwaKomorkiOrg(String nazwaKomorkiOrg) {
		this.nazwaKomorkiOrg = nazwaKomorkiOrg;
	}
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}

    
    
    
}
