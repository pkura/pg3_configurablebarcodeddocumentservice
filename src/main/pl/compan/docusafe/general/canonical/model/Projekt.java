package pl.compan.docusafe.general.canonical.model;

import java.util.Date;
import pl.compan.docusafe.general.ObjectUtils;

public class Projekt {

	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Long id;
	private String cn;
	private String title;
	private Integer centrum;	
	private String refValue;
	private Boolean available;
	private Double identyfikator_num;
	private Long erpId;
	private Date p_datako;
	private Date p_datapo;
	private Double typ_kontrakt_id;
	private String typ_kontrakt_idn;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getCentrum() {
		return centrum;
	}
	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}
	public String getRefValue() {
		return refValue;
	}
	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}
	public Double getIdentyfikator_num() {
		return identyfikator_num;
	}
	public void setIdentyfikator_num(Double identyfikator_num) {
		this.identyfikator_num = identyfikator_num;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
	public Date getP_datako() {
		return p_datako;
	}
	public void setP_datako(Date p_datako) {
		this.p_datako = p_datako;
	}
	public Date getP_datapo() {
		return p_datapo;
	}
	public void setP_datapo(Date p_datapo) {
		this.p_datapo = p_datapo;
	}
	public Double getTyp_kontrakt_id() {
		return typ_kontrakt_id;
	}
	public void setTyp_kontrakt_id(Double typ_kontrakt_id) {
		this.typ_kontrakt_id = typ_kontrakt_id;
	}
	public String getTyp_kontrakt_idn() {
		return typ_kontrakt_idn;
	}
	public void setTyp_kontrakt_idn(String typ_kontrakt_idn) {
		this.typ_kontrakt_idn = typ_kontrakt_idn;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
}
