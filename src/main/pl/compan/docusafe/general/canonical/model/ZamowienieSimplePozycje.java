package pl.compan.docusafe.general.canonical.model;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import pl.compan.docusafe.general.ObjectUtils;

public class ZamowienieSimplePozycje {

		private Long id;
		private Long supplier_order_id;
		private String supplier_order_idm;
		private String wytwor_idm;
		private BigDecimal ilosc;
		private BigDecimal iloscZreal;
		private Integer nrpoz;
		private BigDecimal cena;
		private BigDecimal cenabrutto;
		private BigDecimal koszt;
		private BigDecimal kwotnett;
		private BigDecimal kwotvat;
		private Long zamdospoz_id;
		private Long budzetId;
		private String budzetIdm;
		private Long bdBudzetKoId;
		private String bdBudzetKoIdm;
		private Long bdRodzajKoId;
		private Long bdPlanZamId;
		private String bdPlanZamIdm;
		private Long bdRodzajPlanZamId;
		private Long bdRezerwacjaId;
		private String bdRezerwacjaIdm;
		private Long bdRezerwacjaPozId;
		private Long kontraktId;
		private Long projektId;
		private Long etapId;
		private Long zasobId;
		
		private Long vatStawId;
		private Long walutaId;
		private Long komorkaOrganizacyjaERPId;
		
		public BigDecimal getIloscZreal() {
			return iloscZreal;
		}
		public void setIloscZreal(BigDecimal iloscZreal) {
			this.iloscZreal = iloscZreal;
		}
		public Long getWalutaId() {
			return walutaId;
		}
		public void setWalutaId(Long walutaId) {
			this.walutaId = walutaId;
		}
		private ObjectUtils.STATUS_OBIEKTU stanObiektu;
		
		public ZamowienieSimplePozycje(Element el, String idm, Long erpId) {
			this.supplier_order_id = erpId;
			this.supplier_order_idm = idm;
			if(el.element("wytwor_id")!=null && el.element("wytwor_id").element("idm")!=null && !StringUtils.isBlank(el.element("wytwor_id").element("idm").getTextTrim()))
				this.wytwor_idm = el.element("wytwor_id").element("idm").getTextTrim();
			if(el.element("ilosc")!=null && !StringUtils.isBlank(el.element("ilosc").getTextTrim())){
				this.ilosc = new BigDecimal(el.element("ilosc").getTextTrim()).setScale(4,BigDecimal.ROUND_HALF_UP);
			}
			if(el.element("ilzreal")!=null && !StringUtils.isBlank(el.element("ilzreal").getTextTrim())){
				this.iloscZreal = new BigDecimal(el.element("ilzreal").getTextTrim()).setScale(4,BigDecimal.ROUND_HALF_UP);
			}
			if(el.element("nrpoz")!=null && !StringUtils.isBlank(el.element("nrpoz").getTextTrim())) this.nrpoz = Integer.parseInt(el.element("nrpoz").getTextTrim());
			if(el.element("cena")!=null && !StringUtils.isBlank(el.element("cena").getTextTrim())){
				this.cena = new BigDecimal(el.element("cena").getTextTrim()).setScale(4,BigDecimal.ROUND_HALF_UP);}
			if(el.element("cenabrutto")!=null && !StringUtils.isBlank(el.element("cenabrutto").getTextTrim())){
				this.cenabrutto = new BigDecimal(el.element("cenabrutto").getTextTrim()).setScale(4,BigDecimal.ROUND_HALF_UP);
			}
			if (el.element("koszt") != null && !StringUtils.isBlank(el.element("koszt").getTextTrim())) {
				this.koszt = new BigDecimal(el.element("koszt").getTextTrim());
			}
			if(el.element("komorka_id")!=null && !StringUtils.isBlank(el.element("komorka_id").element("id").getTextTrim())){
				this.komorkaOrganizacyjaERPId=Long.parseLong(el.element("komorka_id").element("id").getTextTrim());
			}
			if(el.element("kwotnett")!=null && !StringUtils.isBlank(el.element("kwotnett").getTextTrim()))this.kwotnett = new BigDecimal(el.element("kwotnett").getTextTrim()).setScale(4,BigDecimal.ROUND_HALF_UP);
			if(el.element("kwotvat")!=null && !StringUtils.isBlank(el.element("kwotvat").getTextTrim()))this.kwotvat = new BigDecimal(el.element("kwotvat").getTextTrim()).setScale(4,BigDecimal.ROUND_HALF_UP);
			this.zamdospoz_id = Long.parseLong(el.element("zamdospoz_id").getTextTrim());
			if (el.element("budzet_id") != null && el.element("budzet_id").element("id") != null && !StringUtils.isBlank(el.element("budzet_id").element("id").getTextTrim())) {
				this.budzetId = Long.parseLong(el.element("budzet_id").element("id").getTextTrim());
			}

			if(el.element("vatstaw_id")!=null && !StringUtils.isBlank(el.element("vatstaw_id").element("id").getTextTrim())){
				this.vatStawId=Long.parseLong(el.element("vatstaw_id").element("id").getTextTrim());
			}

			if(el.element("waluta_id")!=null && !StringUtils.isBlank(el.element("waluta_id").element("id").getTextTrim())){
				this.walutaId=Long.parseLong(el.element("waluta_id").element("id").getTextTrim());
			}

			if (el.element("budzet_id") != null && el.element("budzet_id").element("idm") != null && !StringUtils.isBlank(el.element("budzet_id").element("idm").getTextTrim())) {
				this.budzetIdm = el.element("budzet_id").element("idm").getTextTrim();
			}
			if (el.element("bd_budzet_ko_id") != null && el.element("bd_budzet_ko_id").element("id") != null && !StringUtils.isBlank(el.element("bd_budzet_ko_id").element("id").getTextTrim())) {
				this.bdBudzetKoId = Long.parseLong(el.element("bd_budzet_ko_id").element("id").getTextTrim());
			}
			if (el.element("bd_budzet_ko_id") != null && el.element("bd_budzet_ko_id").element("idm") != null && !StringUtils.isBlank(el.element("bd_budzet_ko_id").element("idm").getTextTrim())) {
				this.bdBudzetKoIdm = el.element("bd_budzet_ko_id").element("idm").getTextTrim();
			}
			if (el.element("bd_rodzaj_ko_id") != null && !StringUtils.isBlank(el.element("bd_rodzaj_ko_id").getTextTrim())) {
				this.bdRodzajKoId = Long.parseLong(el.element("bd_rodzaj_ko_id").getTextTrim());
			}
			if (el.element("bd_plan_zam_id") != null && el.element("bd_plan_zam_id").element("id") != null &&!StringUtils.isBlank(el.element("bd_plan_zam_id").element("id").getTextTrim())) {
				this.bdPlanZamId = Long.parseLong(el.element("bd_plan_zam_id").element("id").getTextTrim());
			}
			if (el.element("bd_plan_zam_id") != null && el.element("bd_plan_zam_id").element("idm") != null &&!StringUtils.isBlank(el.element("bd_plan_zam_id").element("idm").getTextTrim())) {
				this.bdPlanZamIdm = el.element("bd_plan_zam_id").element("idm").getTextTrim();
			}
			if (el.element("bd_rodzaj_plan_zam_id") != null &&!StringUtils.isBlank(el.element("bd_rodzaj_plan_zam_id").getTextTrim())) {
				this.bdRodzajPlanZamId = Long.parseLong(el.element("bd_rodzaj_plan_zam_id").getTextTrim());
			}
			if (el.element("bd_rezerwacja_id") != null && el.element("bd_rezerwacja_id").element("id") != null &&!StringUtils.isBlank(el.element("bd_rezerwacja_id").element("id").getTextTrim())) {
				this.bdRezerwacjaId = Long.parseLong(el.element("bd_rezerwacja_id").element("id").getTextTrim());
			}
			if (el.element("bd_rezerwacja_id") != null && el.element("bd_rezerwacja_id").element("idm") != null &&!StringUtils.isBlank(el.element("bd_rezerwacja_id").element("idm").getTextTrim())) {
				this.bdRezerwacjaIdm = el.element("bd_rezerwacja_id").element("idm").getTextTrim();
			}
			if (el.element("bd_rezerwacja_poz_id") != null && !StringUtils.isBlank(el.element("bd_rezerwacja_poz_id").getTextTrim())) {
				this.bdRezerwacjaPozId = Long.parseLong(el.element("bd_rezerwacja_poz_id").getTextTrim());
			}
			if (el.element("kontrakt_id") != null&&!StringUtils.isBlank(el.element("kontrakt_id").getTextTrim())) {
				this.kontraktId = Long.parseLong(el.element("kontrakt_id").getTextTrim());
			}
			if (el.element("projekt_id") != null && !StringUtils.isBlank(el.element("projekt_id").getTextTrim())) {
				this.projektId = Long.parseLong(el.element("projekt_id").getTextTrim());
			}
			if (el.element("etap_id") != null && !StringUtils.isBlank(el.element("etap_id").getTextTrim())) {
				this.etapId = Long.parseLong(el.element("etap_id").getTextTrim());
			}
			if (el.element("zasob_id") != null && !StringUtils.isBlank(el.element("zasob_id").getTextTrim())) {
				this.zasobId = Long.parseLong(el.element("zasob_id").getTextTrim());
			}




		}
		public ZamowienieSimplePozycje() {
			
		}


		public String getSupplier_order_idm() {
			return supplier_order_idm;
		}
		public void setSupplier_order_idm(String supplier_order_idm) {
			this.supplier_order_idm = supplier_order_idm;
		}
		public String getWytwor_idm() {
			return wytwor_idm;
		}
		public void setWytwor_idm(String wytwor_idm) {
			this.wytwor_idm = wytwor_idm;
		}
		public Long getBudzetId() {
			return budzetId;
		}
		public void setBudzetId(Long budzetId) {
			this.budzetId = budzetId;
		}
		public String getBudzetIdm() {
			return budzetIdm;
		}
		public void setBudzetIdm(String budzetIdm) {
			this.budzetIdm = budzetIdm;
		}
		public Long getBdBudzetKoId() {
			return bdBudzetKoId;
		}
		public void setBdBudzetKoId(Long bdBudzetKoId) {
			this.bdBudzetKoId = bdBudzetKoId;
		}
		public String getBdBudzetKoIdm() {
			return bdBudzetKoIdm;
		}
		public void setBdBudzetKoIdm(String bdBudzetKoIdm) {
			this.bdBudzetKoIdm = bdBudzetKoIdm;
		}
		public Long getBdRodzajKoId() {
			return bdRodzajKoId;
		}
		public void setBdRodzajKoId(Long bdRodzajKoId) {
			this.bdRodzajKoId = bdRodzajKoId;
		}
		public Long getBdPlanZamId() {
			return bdPlanZamId;
		}
		public void setBdPlanZamId(Long bdPlanZamId) {
			this.bdPlanZamId = bdPlanZamId;
		}
		public String getBdPlanZamIdm() {
			return bdPlanZamIdm;
		}
		public void setBdPlanZamIdm(String bdPlanZamIdm) {
			this.bdPlanZamIdm = bdPlanZamIdm;
		}
		public Long getBdRodzajPlanZamId() {
			return bdRodzajPlanZamId;
		}
		public void setBdRodzajPlanZamId(Long bdRodzajPlanZamId) {
			this.bdRodzajPlanZamId = bdRodzajPlanZamId;
		}
		public Long getBdRezerwacjaId() {
			return bdRezerwacjaId;
		}
		public void setBdRezerwacjaId(Long bdRezerwacjaId) {
			this.bdRezerwacjaId = bdRezerwacjaId;
		}
		public String getBdRezerwacjaIdm() {
			return bdRezerwacjaIdm;
		}
		public void setBdRezerwacjaIdm(String bdRezerwacjaIdm) {
			this.bdRezerwacjaIdm = bdRezerwacjaIdm;
		}
		public Long getBdRezerwacjaPozId() {
			return bdRezerwacjaPozId;
		}
		public void setBdRezerwacjaPozId(Long bdRezerwacjaPozId) {
			this.bdRezerwacjaPozId = bdRezerwacjaPozId;
		}
		public Long getKontraktId() {
			return kontraktId;
		}
		public void setKontraktId(Long kontraktId) {
			this.kontraktId = kontraktId;
		}
		public Long getProjektId() {
			return projektId;
		}
		public void setProjektId(Long projektId) {
			this.projektId = projektId;
		}
		public Long getEtapId() {
			return etapId;
		}
		public void setEtapId(Long etapId) {
			this.etapId = etapId;
		}
		public Long getZasobId() {
			return zasobId;
		}
		public void setZasobId(Long zasobId) {
			this.zasobId = zasobId;
		}
		public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
			return stanObiektu;
		}
		public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
			this.stanObiektu = stanObiektu;
		}
		public BigDecimal getIlosc() {
			return ilosc;
		}
		public void setIlosc(BigDecimal ilosc) {
			this.ilosc = ilosc;
		}
		public Integer getNrpoz() {
			return nrpoz;
		}
		public void setNrpoz(Integer nrpoz) {
			this.nrpoz = nrpoz;
		}
		public BigDecimal getCena() {
			return cena;
		}
		public void setCena(BigDecimal cena) {
			this.cena = cena;
		}
		public BigDecimal getCenabrutto() {
			return cenabrutto;
		}
		public void setCenabrutto(BigDecimal cenabrutto) {
			this.cenabrutto = cenabrutto;
		}
		public BigDecimal getKoszt() {
			return koszt;
		}
		public void setKoszt(BigDecimal koszt) {
			this.koszt = koszt;
		}
		public BigDecimal getKwotnett() {
			return kwotnett;
		}
		public void setKwotnett(BigDecimal kwotnett) {
			this.kwotnett = kwotnett;
		}
		public BigDecimal getKwotvat() {
			return kwotvat;
		}
		public void setKwotvat(BigDecimal kwotvat) {
			this.kwotvat = kwotvat;
		}
		public void setSupplier_order_id(Long supplier_order_id) {
			this.supplier_order_id = supplier_order_id;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Long getZamdospoz_id() {
			return zamdospoz_id;
		}
		public void setZamdospoz_id(Long zamdospoz_id) {
			this.zamdospoz_id = zamdospoz_id;
		}
		public Long getSupplier_order_id() {
			return supplier_order_id;
		}
		public Long getVatStawId() {
			return vatStawId;
		}
		public void setVatStawId(Long vatStawId) {
			this.vatStawId = vatStawId;
		}
		public Long getKomorkaOrganizacyjaERPId() {
			return komorkaOrganizacyjaERPId;
		}
		public void setKomorkaOrganizacyjaERPId(Long komorkaOrganizacyjaERPId) {
			this.komorkaOrganizacyjaERPId = komorkaOrganizacyjaERPId;
		}
		

		
}
