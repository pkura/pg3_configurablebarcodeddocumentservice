package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;

public class OPK {
	
	private Long id;
	private Long erp_id;
	private Long ds_division_id;
	private Integer isCost;
	private Integer isDepartment;
	private String opk;
	
	public OPK(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getErp_id() {
		return erp_id;
	}

	public void setErp_id(Long erp_id) {
		this.erp_id = erp_id;
	}

	public Long getDs_division_id() {
		return ds_division_id;
	}

	public void setDs_division_id(Long ds_division_id) {
		this.ds_division_id = ds_division_id;
	}

	public Integer getIsCost() {
		return isCost;
	}

	public void setIsCost(Integer isCost) {
		this.isCost = isCost;
	}

	public Integer getIsDepartment() {
		return isDepartment;
	}

	public void setIsDepartment(Integer isDepartment) {
		this.isDepartment = isDepartment;
	}

	public String getOpk() {
		return opk;
	}

	public void setOpk(String opk) {
		this.opk = opk;
	}
	public String getTitle(){
		try {
			return getOpk() + " : " + DSDivision.findById(getDs_division_id().intValue()).getName();
		} catch (DivisionNotFoundException e) {
			System.out.println("B��d: DivisionNotFoundException");
		} catch (EdmException e) {
			System.out.println("B��d: EdmException");
		}
		return getOpk();
	}
}
