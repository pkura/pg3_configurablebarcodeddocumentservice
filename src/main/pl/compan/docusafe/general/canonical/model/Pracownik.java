package pl.compan.docusafe.general.canonical.model;

import java.util.List;

import pl.compan.docusafe.general.ObjectUtils;

public class Pracownik {

	/**
	 * Imi� pracownika
	 */
	private String imie;
	/**
	 * Nazwisko pracownika
	 */
	private String nazwisko;
	/**
	 * Identyfikator zewn�rzny Idm
	 */
	private String zewnetrznyIdm;
	/**
	 * Identyfikator zewn�trzny Id
	 */
	private Long zewnetrznyId;
	/**
	 * Pesel
	 */
	private String pesel;
	/**
	 * JednostkaOrganizacyjna
	 */
	private List<JednostkaOrganizacyjna> jednostkiOrganizacyjne;
	/**
	 * Login
	 */
	private String login;
	/**
	 * Identyfikator
	 */
	private Long id;
	/**
	 * Has�o u�ytkownika - wykorzystywane tylko sporadycznie
	 */
	private String haslo;	
	/**
	 * W tym polu powinien si� zawsze znale�� operator, kt�r b�dzie wsp�lny dla
	 * r�nych modeli. Przyk�adowo, w jednym wdro�eniu mo�e to by� PESEL a w
	 * innym numer GUID. *
	 */
	private String operatorDoPorownywania;

	/**
	 * Identyfikator GUID
	 */
	private String guid;
	/**
	 * Oznaczenie stanu obiektu (nowy, zmodyfikowany, usuni�ty)
	 */
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;

	/**
	 * Prefix u�ywany do logowania domenowego
	 */
	private String prefixDomeny;
	
	/**
	 * Zmienna okre�la czy zachodzi logowanie domenowe.
	 */
	private boolean isLogowanieDomenowe;
	
	/**
	 * Zmienna okre�la adres email
	 */
	private String adresEmail;
	
	/**
	 * Numer ewidencyjny pracownika
	 */
	private int nrewid;
	private boolean czyAktywna;
	/**
	 * Konstruktor tej klasy nie powinien by� u�ywany bezpo�rednio. W celu
	 * utworzenia nowej instancji Pracownik u�yj
	 * PracownikDAO.getInstance().getPracownik()
	 */
	public Pracownik() {
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getZewnetrznyIdm() {
		return zewnetrznyIdm;
	}

	public void setZewnetrznyIdm(String zewnetrznyIdm) {
		this.zewnetrznyIdm = zewnetrznyIdm;
	}

	public Long getZewnetrznyId() {
		return zewnetrznyId;
	}
	
	public void setZewnetrznyId(Long zewnetrznyId) {
		this.zewnetrznyId = zewnetrznyId;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public List<JednostkaOrganizacyjna> getJednostkiOrganizacyjne() {
		return jednostkiOrganizacyjne;
	}

	public void setJednostkiOrganizacyjne(
			List<JednostkaOrganizacyjna> jednostkiOrganizacyjne) {
		this.jednostkiOrganizacyjne = jednostkiOrganizacyjne;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOperatorDoPorownywania() {
		return operatorDoPorownywania;
	}

	public void setOperatorDoPorownywania(String operatorDoPorownywania) {
		this.operatorDoPorownywania = operatorDoPorownywania;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}

	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}

	public enum PRACOWNIK_POLA_MAPUJACE {
		PESEL, GUID
	}

	public String getPrefixDomeny() {
		return prefixDomeny;
	}

	public void setPrefixDomeny(String prefixDomeny) {
		this.prefixDomeny = prefixDomeny;
	}

	public boolean isLogowanieDomenowe() {
		return isLogowanieDomenowe;
	}

	public void setLogowanieDomenowe(boolean isLogowanieDomenowe) {
		this.isLogowanieDomenowe = isLogowanieDomenowe;
	}

	public String getAdresEmail() {
		return adresEmail;
	}

	public void setAdresEmail(String adresEmail) {
		this.adresEmail = adresEmail;
	};

    public int getNrewid() {
		return nrewid;
	}

	public void setNrewid(int nrewid) {
		this.nrewid = nrewid;
	}

	public boolean isCzyAktywna() {
		return czyAktywna;
	}

	public void setCzyAktywna(boolean czyAktywna) {
		this.czyAktywna = czyAktywna;
	}

	@Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Pracownik{");
        sb.append("imie='").append(imie).append('\'');
        sb.append(", nazwisko='").append(nazwisko).append('\'');
        sb.append(", zewnetrznyIdm='").append(zewnetrznyIdm).append('\'');
        sb.append(", zewnetrznyId=").append(zewnetrznyId);
        sb.append(", pesel='").append(pesel).append('\'');
        sb.append(", jednostkiOrganizacyjne=").append(jednostkiOrganizacyjne);
        sb.append(", login='").append(login).append('\'');
        sb.append(", id=").append(id);
        sb.append(", haslo='").append(haslo).append('\'');
        sb.append(", operatorDoPorownywania='").append(operatorDoPorownywania).append('\'');
        sb.append(", guid='").append(guid).append('\'');
        sb.append(", stanObiektu=").append(stanObiektu);
        sb.append(", prefixDomeny='").append(prefixDomeny).append('\'');
        sb.append(", isLogowanieDomenowe=").append(isLogowanieDomenowe);
        sb.append(", adresEmail='").append(adresEmail).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
