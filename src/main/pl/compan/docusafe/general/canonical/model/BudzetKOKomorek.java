package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class BudzetKOKomorek {

	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Long id;
	private String idm;
	private String nazwa;
	private Long okresId;
	private String okresIdm;
	private Long erpId;
	private Long budzetId;
	private String budzetIdn;
	private Boolean czyAgregat;
	private Boolean available;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIdm() {
		return idm;
	}
	public void setIdm(String idm) {
		this.idm = idm;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public Long getOkresId() {
		return okresId;
	}
	public void setOkresId(Long okresId) {
		this.okresId = okresId;
	}
	public String getOkresIdm() {
		return okresIdm;
	}
	public void setOkresIdm(String okresIdm) {
		this.okresIdm = okresIdm;
	}
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
	public Long getBudzetId() {
		return budzetId;
	}
	public void setBudzetId(Long budzetId) {
		this.budzetId = budzetId;
	}
	public String getBudzetIdn() {
		return budzetIdn;
	}
	public void setBudzetIdn(String budzetIdn) {
		this.budzetIdn = budzetIdn;
	}
	public Boolean getCzyAgregat() {
		return czyAgregat;
	}
	public void setCzyAgregat(Boolean czyAgregat) {
		this.czyAgregat = czyAgregat;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}

	
}
