package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;


public class JednostkaMiary {

	private Long id;
	private String cn;
	private String title;
	private Integer centrum;	
	private String refValue;
	private Boolean aktywna;
	private String jmtyp_idn;
	private Integer precyzjajm;
	private Long erpId;

	private ObjectUtils.STATUS_OBIEKTU stanObiektu;

	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getCentrum() {
		return centrum;
	}
	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}
	public String getRefValue() {
		return refValue;
	}
	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}
	public Boolean getAktywna() {
		return aktywna;
	}
	public void setAktywna(Boolean aktywna) {
		this.aktywna = aktywna;
	}
	public String getJmtyp_idn() {
		return jmtyp_idn;
	}
	public void setJmtyp_idn(String jmtyp_idn) {
		this.jmtyp_idn = jmtyp_idn;
	}
	public Integer getPrecyzjajm() {
		return precyzjajm;
	}
	public void setPrecyzjajm(Integer precyzjajm) {
		this.precyzjajm = precyzjajm;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
}
