package pl.compan.docusafe.general.canonical.model;

import java.util.List;

public class PracownikWJednostkachOrganizacyjnych {

	/**
	 * Pracownik
	 */
	private Pracownik pracownik;
	/**
	 * Lista jednostek powiązanych z pracownikiem
	 */
	private JednostkaOrganizacyjna jednostkaOrganizacyjna;
	
	
	public Pracownik getPracownik() {
		return pracownik;
	}
	public void setPracownik(Pracownik pracownik) {
		this.pracownik = pracownik;
	}
	public JednostkaOrganizacyjna getJednostkaOrganizacyjna() {
		return jednostkaOrganizacyjna;
	}
	public void setJednostkaOrganizacyjna(
			JednostkaOrganizacyjna jednostkaOrganizacyjna) {
		this.jednostkaOrganizacyjna = jednostkaOrganizacyjna;
	}
	
}
