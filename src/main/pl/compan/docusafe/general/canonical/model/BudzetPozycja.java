package pl.compan.docusafe.general.canonical.model;

import java.math.BigDecimal;

import javax.persistence.Column;

import pl.compan.docusafe.core.users.DSUser;

public class BudzetPozycja {
	private Long id;
	private Long okresBudzetowy;
	private BudzetKOKomorek budzet;
	private BudzetPozycjaSimple pozycjaZBudzetu;
	private Projekt projekt;
	private BigDecimal kwotaNetto;
	private BigDecimal kwotaVat;
	DSUser dysponent;
	private Long akceptacja;
	private Long budzetProjektu;
	private Long etap;
	private Long zasob;
	
	public BudzetPozycja(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOkresBudzetowy() {
		return okresBudzetowy;
	}

	public void setOkresBudzetowy(Long okresBudzetowy) {
		this.okresBudzetowy = okresBudzetowy;
	}

	public BigDecimal getKwotaNetto() {
		return kwotaNetto;
	}

	public void setKwotaNetto(BigDecimal kwotaNetto) {
		this.kwotaNetto = kwotaNetto;
	}

	public BigDecimal getKwotaVat() {
		return kwotaVat;
	}

	public void setKwotaVat(BigDecimal kwotaVat) {
		this.kwotaVat = kwotaVat;
	}

	public DSUser getDysponent() {
		return dysponent;
	}

	public void setDysponent(DSUser dysponent) {
		this.dysponent = dysponent;
	}
	
	public Projekt getProjekt() {
		return projekt;
	}

	public void setProjekt(Projekt projekt) {
		this.projekt = projekt;
	}

	public Long getAkceptacja() {
		return akceptacja;
	}

	public void setAkceptacja(Long akceptacja) {
		this.akceptacja = akceptacja;
	}

	public Long getBudzetProjektu() {
		return budzetProjektu;
	}

	public void setBudzetProjektu(Long budzetProjektu) {
		this.budzetProjektu = budzetProjektu;
	}

	public Long getEtap() {
		return etap;
	}

	public void setEtap(Long etap) {
		this.etap = etap;
	}

	public Long getZasob() {
		return zasob;
	}

	public void setZasob(Long zasob) {
		this.zasob = zasob;
	}

	public BudzetPozycjaSimple getPozycjaZBudzetu() {
		return pozycjaZBudzetu;
	}

	public void setPozycjaZBudzetu(BudzetPozycjaSimple pozycjaZBudzetu) {
		this.pozycjaZBudzetu = pozycjaZBudzetu;
	}

	public BudzetKOKomorek getBudzet() {
		return budzet;
	}

	public void setBudzet(BudzetKOKomorek budzet) {
		this.budzet = budzet;
	}
	
}
