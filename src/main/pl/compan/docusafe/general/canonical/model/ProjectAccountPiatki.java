package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class ProjectAccountPiatki {

	Long id;
	String konto_ido;
	Long kontrakt_id;
	String kontrakt_idm;
	String rep_atrybut_ido;	
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Boolean available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKonto_ido() {
		return konto_ido;
	}

	public void setKonto_ido(String konto_ido) {
		this.konto_ido = konto_ido;
	}

	public Long getKontrakt_id() {
		return kontrakt_id;
	}

	public void setKontrakt_id(Long kontrakt_id) {
		this.kontrakt_id = kontrakt_id;
	}

	public String getKontrakt_idm() {
		return kontrakt_idm;
	}

	public void setKontrakt_idm(String kontrakt_idm) {
		this.kontrakt_idm = kontrakt_idm;
	}

	public String getRep_atrybut_ido() {
		return rep_atrybut_ido;
	}

	public void setRep_atrybut_ido(String rep_atrybut_ido) {
		this.rep_atrybut_ido = rep_atrybut_ido;
	}

	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}

	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
	
	
}
