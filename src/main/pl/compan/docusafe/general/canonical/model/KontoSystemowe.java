package pl.compan.docusafe.general.canonical.model;

public class KontoSystemowe {

	/**
	 * Login systemowy
	 */
	String login;
	/**
	 * Powi�zany obiekt Pracownik
	 */
	Pracownik pracownik;
	/**
	 * Status konta aktywne/nie aktywne
	 */
	boolean aktywne;
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public Pracownik getPracownik() {
		return pracownik;
	}
	public void setPracownik(Pracownik pracownik) {
		this.pracownik = pracownik;
	}
	public boolean isAktywne() {
		return aktywne;
	}
	public void setAktywne(boolean aktywne) {
		this.aktywne = aktywne;
	}
}
