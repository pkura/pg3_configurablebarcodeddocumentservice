package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class SzablonyRozliczenFaktur { 
	
private Long id;
private Long erpId;
private String idm;
private String nazwa;
private String okresIdm;
private Long okresId;
private ObjectUtils.STATUS_OBIEKTU stanObiektu;

public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public Long getErpId() {
	return erpId;
}
public void setErpId(Long erpId) {
	this.erpId = erpId;
}
public String getIdm() {
	return idm;
}
public void setIdm(String idm) {
	this.idm = idm;
}
public String getNazwa() {
	return nazwa;
}
public void setNazwa(String nazwa) {
	this.nazwa = nazwa;
}
public String getOkresIdm() {
	return okresIdm;
}
public void setOkresIdm(String okresIdm) {
	this.okresIdm = okresIdm;
}
public Long getOkresId() {
	return okresId;
}
public void setOkresId(Long okresId) {
	this.okresId = okresId;
}
public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
	return stanObiektu;
}
public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
	this.stanObiektu = stanObiektu;
}

}
