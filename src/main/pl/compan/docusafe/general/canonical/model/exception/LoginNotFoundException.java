package pl.compan.docusafe.general.canonical.model.exception;

public class LoginNotFoundException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3231783110678695923L;

	public LoginNotFoundException(String username)
	    {
	        super("Obiekt "+username+" nie ma ustawionego loginu.");
	    }
}

