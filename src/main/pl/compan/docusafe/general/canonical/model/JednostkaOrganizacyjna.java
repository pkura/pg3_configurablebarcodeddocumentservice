package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

/**
 * Klasa odwzorowuj�ca Jednostk� Organizacyjn�
 * 
 * @author
 * 
 */
public class JednostkaOrganizacyjna {

	public enum RODZAJ_JEDNOSTKI_ORGANIZACYJNEJ {
		DOCUSAFE, ERP
	};

	// Identyfikator kom�rki
	private long id;
	// Przechowuje identyfikator rodzica
	private Long rodzicId;
	// JO mo�e by� nieaktywna, nie u�ywane kom�rki s� dezaktywowane a nie
	// usuwane
	private boolean aktywna;
	// JO Kadrowa
	private boolean kadrowa;
	// JO Kosztowa
	private boolean kosztowa;
	// JO - oddzia�
	private boolean oddzial;
	// Opis kom�rki organizacyjnej
	private String opis;
	// Nazwa JO
	private String nazwa;
	// Kod kom�rki
	private String kod;
	// Zmienna okre�la czy miejscem powstania jednostki by� ERP czy system
	// DocuSafe (np. wprowadzona z palca)
	private RODZAJ_JEDNOSTKI_ORGANIZACYJNEJ rodzajJednostki;
	// Oznaczenie stanu obiektu (nowy, zmodyfikowany, usuni�ty)
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	// Zmienna pozwala na wpisanie ID innego systemu
	private String idn;
	// Przechowuje zewn�trzny identyfikator rodzica
	private String rodzicIdn;
	// Pole Guid
	private String guid;


	public boolean isAktywna() {
		return aktywna;
	}

	public void setAktywna(boolean aktywna) {
		this.aktywna = aktywna;
	}

	public boolean isKadrowa() {
		return kadrowa;
	}

	public void setKadrowa(boolean kadrowa) {
		this.kadrowa = kadrowa;
	}

	public boolean isKosztowa() {
		return kosztowa;
	}

	public void setKosztowa(boolean kosztowa) {
		this.kosztowa = kosztowa;
	}

	public boolean isOddzial() {
		return oddzial;
	}

	public void setOddzial(boolean oddzial) {
		this.oddzial = oddzial;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getRodzicIdn() {
		return rodzicIdn;
	}

	public void setRodzicIdn(String rodzicIdn) {
		this.rodzicIdn = rodzicIdn;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	public RODZAJ_JEDNOSTKI_ORGANIZACYJNEJ getRodzajJednostki() {
		return rodzajJednostki;
	}

	public void setRodzajJednostki(
			RODZAJ_JEDNOSTKI_ORGANIZACYJNEJ rodzajJednostki) {
		this.rodzajJednostki = rodzajJednostki;
	}

	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}

	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}

	public String getIdn() {
		return idn;
	}

	public void setIdn(String idn) {
		this.idn = idn;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Long getRodzicId() {
		return rodzicId;
	}

	public void setRodzicId(Long rodzicId) {
		this.rodzicId = rodzicId;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("JednostkaOrganizacyjna{");
        sb.append("id=").append(id);
        sb.append(", rodzicId=").append(rodzicId);
        sb.append(", aktywna=").append(aktywna);
        sb.append(", kadrowa=").append(kadrowa);
        sb.append(", kosztowa=").append(kosztowa);
        sb.append(", oddzial=").append(oddzial);
        sb.append(", opis='").append(opis).append('\'');
        sb.append(", nazwa='").append(nazwa).append('\'');
        sb.append(", kod='").append(kod).append('\'');
        sb.append(", rodzajJednostki=").append(rodzajJednostki);
        sb.append(", stanObiektu=").append(stanObiektu);
        sb.append(", idn='").append(idn).append('\'');
        sb.append(", rodzicIdn='").append(rodzicIdn).append('\'');
        sb.append(", guid='").append(guid).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
