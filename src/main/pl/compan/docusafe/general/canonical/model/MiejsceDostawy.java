package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class MiejsceDostawy{
	public enum RODZAJ_JEDNOSTKI_ORGANIZACYJNEJ { DOCUSAFE, ERP };

	/**
	 * Oznaczenie stanu obiektu (nowy, zmodyfikowany, usuni�ty)
	 */
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private long id;
	private String cn;
	private String nazwa;
	private boolean aktywna;

	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public boolean isAktywna() {
		return aktywna;
	}

	public void isAktywna(boolean aktywna) {
		this.aktywna = aktywna;
	}
}
