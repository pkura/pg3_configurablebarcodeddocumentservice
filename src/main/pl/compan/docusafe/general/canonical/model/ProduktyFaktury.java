package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class ProduktyFaktury {

	private long id;
	private long wytworId;
	private String cn;
	private String jmIdn;
	private String klasyWytwIdn;
	private String nazwa;
	private int rodzTowaru;
	private String vatStawIds;
	private Boolean aktywna;
	private ObjectUtils.STATUS_OBIEKTU stanObiektu;

	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getWytworId() {
		return wytworId;
	}
	public void setWytworId(long wytworId) {
		this.wytworId = wytworId;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getJmIdn() {
		return jmIdn;
	}
	public void setJmIdn(String jmIdn) {
		this.jmIdn = jmIdn;
	}
	public String getKlasyWytwIdn() {
		return klasyWytwIdn;
	}
	public void setKlasyWytwIdn(String klasyWytwIdn) {
		this.klasyWytwIdn = klasyWytwIdn;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public int getRodzTowaru() {
		return rodzTowaru;
	}
	public void setRodzTowaru(int rodzTowaru) {
		this.rodzTowaru = rodzTowaru;
	}
	public String getVatStawIds() {
		return vatStawIds;
	}
	public void setVatStawIds(String vatStawIds) {
		this.vatStawIds = vatStawIds;
	}
	public Boolean getAktywna() {
		return aktywna;
	}
	public void setAktywna(Boolean aktywna) {
		this.aktywna = aktywna;
	}
}

