package pl.compan.docusafe.general.canonical.model;

import pl.compan.docusafe.general.ObjectUtils;

public class PlanZakupow {

	private ObjectUtils.STATUS_OBIEKTU stanObiektu;
	private Long id;
	private Long okres_id;
	private String okres_idm;
	private Long budzet_id;
	private String budzet_idn;
	private Boolean czyAgregat;
//	private boolean czy_ceny_brutto;
//	private boolean czy_kwoty_tys;
	private Long erpId;
	private String idm;
	private String nazwa;
//	private double kwota_pzp;
//	private double pozostalo_pzp;
	//private int status;
	private Boolean available;
	
	public ObjectUtils.STATUS_OBIEKTU getStanObiektu() {
		return stanObiektu;
	}
	public void setStanObiektu(ObjectUtils.STATUS_OBIEKTU stanObiektu) {
		this.stanObiektu = stanObiektu;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOkres_id() {
		return okres_id;
	}
	public void setOkres_id(Long budzet_id) {
		this.okres_id = budzet_id;
	}
	public String getOkres_idm() {
		return okres_idm;
	}
	public void setOkres_idm(String budzet_idm) {
		this.okres_idm = budzet_idm;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String getIdm() {
		return idm;
	}
	public void setIdm(String idm) {
		this.idm = idm;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
	public Long getBudzet_id() {
		return budzet_id;
	}
	public void setBudzet_id(Long budzet_id) {
		this.budzet_id = budzet_id;
	}
	public String getBudzet_idn() {
		return budzet_idn;
	}
	public void setBudzet_idn(String budzet_idn) {
		this.budzet_idn = budzet_idn;
	}
	public Boolean getCzyAgregat() {
		return czyAgregat;
	}
	public void setCzyAgregat(Boolean czyAgregat) {
		this.czyAgregat = czyAgregat;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
}
