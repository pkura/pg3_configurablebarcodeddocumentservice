package pl.compan.docusafe.general.dictionaryLogic;

import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.general.validator.Validator;


/**
 * @author Jurek
 *
 */
public class KontrahentLogic extends DwrDictionaryBase {
	private static final String KRAJ_CN_PL = "PL";
	
	private static final String FIELD_KRAJ = "WYSTAWCA_KRAJ";
	private static final String FIELD_NIP = "WYSTAWCA_NIP";
	private static final String FIELD_NRKONTA = "WYSTAWCA_NRKONTA";
	private static final String FIELD_KODPOCZTOWY = "WYSTAWCA_KODPOCZTOWY";
	private static final String TABLENAME_KRAJE = "dsr_country";
	@Override
	public long add(Map<String, FieldData> values) throws EdmException {
		int ret = validate(values);
		if(ret!=0)
			return ret;
		return super.add(values);
	}

	@Override
	public int update(Map<String, FieldData> values) throws EdmException {
		int ret = validate(values);
		if(ret!=0)
			return ret;
		return super.update(values);
	}

	private int validate(Map<String, FieldData> values) throws EdmException{
		EnumValues krajFromValues = ((FieldData) values.get(FIELD_KRAJ)).getEnumValuesData();
		EnumItem krajItem = DataBaseEnumField.getEnumItemForTable(TABLENAME_KRAJE, 
				Integer.valueOf(krajFromValues.getSelectedId()));
		FieldData nip = values.get(FIELD_NIP);
		FieldData nrKonta = values.get(FIELD_NRKONTA);
		FieldData kodPocztowy = values.get(FIELD_KODPOCZTOWY);
		if(krajItem.getCn().trim().equals(KRAJ_CN_PL)){
			if(nip!=null){
				if(!Validator.isValidNip(nip.getStringData()))
					return -2;	
			}
			if(nrKonta!=null){
				String nrKontaTekst = nrKonta.getStringData();
				nrKontaTekst = nrKontaTekst.toUpperCase().replaceAll("[\\p{Punct}\\p{Space}]*","");
				if(!nrKontaTekst.matches("[A-Z]{2}.*"))
					nrKontaTekst = "PL"+nrKontaTekst;
				if(!Validator.isValidIban(nrKontaTekst))
					return -3;
			}
			if(kodPocztowy!=null){
				String kodPocztowyTekst = kodPocztowy.getStringData();
				if(!Validator.isValidZip(kodPocztowyTekst))
					return -4;
			}	
		}else{
			String regex="\\d+";
			if(!nip.getStringData().matches(regex))
				return -2;
			if(nrKonta==null || !nrKonta.getStringData().matches(regex))
				return -3;
			if(!kodPocztowy.getStringData().matches(regex))
				return -4;
			
		}
		return 0;
	}
}
