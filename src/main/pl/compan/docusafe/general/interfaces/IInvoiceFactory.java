package pl.compan.docusafe.general.interfaces;

public interface IInvoiceFactory {

	/**
	 * Metoda aktualizuje wszystkie s�owniki faktury.
	 * Implementacja metody powinna finalnie spowodowa�, �e u�ywane przez formularz faktury s�owniki, b�d� mia�y
	 * aktualne widoki.
	 * @return
	 */
	boolean UpdateDictionaries();	
	
	
}
