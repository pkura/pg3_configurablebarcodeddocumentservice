package pl.compan.docusafe.general.hibernate.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ds_division_erp")
public class OpkDB implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	private Long id;
	
	@Column(name="erp_id")
	private Long erp_id;
	
	@Column(name="ds_division_id")
	private Long ds_division_id;
	
	@Column(name="is_cost")
	private Integer isCost;
	
	@Column(name="is_department")
	private Integer isDepartment;
	
	@Column(name="opk")
	private String opk;
	
	public OpkDB(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getErp_id() {
		return erp_id;
	}

	public void setErp_id(Long idds_division) {
		this.erp_id = idds_division;
	}

	public Integer getIsCost() {
		return isCost;
	}

	public void setIsCost(Integer isCost) {
		this.isCost = isCost;
	}

	public Integer getIsDepartment() {
		return isDepartment;
	}

	public void setIsDepartment(Integer isDepartment) {
		this.isDepartment = isDepartment;
	}

	public Long getDs_division_id() {
		return ds_division_id;
	}

	public void setDs_division_id(Long ds_division_id) {
		this.ds_division_id = ds_division_id;
	}

	public String getOpk() {
		return opk;
	}

	public void setOpk(String opk) {
		this.opk = opk;
	}
	
}
