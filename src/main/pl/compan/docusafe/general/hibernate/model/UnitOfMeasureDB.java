package pl.compan.docusafe.general.hibernate.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

@Entity
@Table(name = "dsg_jednostki_miary")

public class UnitOfMeasureDB{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "cn", nullable = false)
	private String cn;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "centrum", nullable = true)
	private Integer centrum;	

	@Column(name = "refValue", nullable = true)
	private String refValue;

	@Column(name = "available", nullable = true)
	private Integer available;

	@Column(name = "jmtyp_idn", nullable = true)
	private String jmtyp_idn;

	@Column(name = "precyzjajm", nullable = true)
	private Integer precyzjajm;

	@Column(name = "erpId", nullable = true)
	private Long erpId;

	public static List<UnitOfMeasureDB> list() throws EdmException
	{
		return DSApi.context().session().createCriteria(UnitOfMeasureDB.class)
				.addOrder(Order.asc("title"))
				.list();	
	}

	public static UnitOfMeasureDB find(Long id) throws EdmException
	{
		UnitOfMeasureDB status = (UnitOfMeasureDB) DSApi.getObject(UnitOfMeasureDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(UnitOfMeasureDB.class, id);

		return status;
	}

	public static List<UnitOfMeasureDB> findByErpID(Long id) throws EdmException
	{
		return DSApi.context().session().createCriteria(UnitOfMeasureDB.class).add(Restrictions.eq("erpId", id)).list();
	}
	
	public static List<UnitOfMeasureDB> findByIdn(String idn) throws EdmException
	{
		return DSApi.context().session().createCriteria(UnitOfMeasureDB.class).add(Restrictions.eq("cn", idn)).list();
	}
	public final void save() throws EdmException
	{
		try
		{
			DSApi.context().session().saveOrUpdate(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public String getJmtyp_idn() {
		return jmtyp_idn;
	}

	public void setJmtyp_idn(String jmtyp_idn) {
		this.jmtyp_idn = jmtyp_idn;
	}

	public Integer getPrecyzjajm() {
		return precyzjajm;
	}

	public void setPrecyzjajm(Integer precyzjajm) {
		this.precyzjajm = precyzjajm;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(UnitOfMeasureDB.class)+" set available='0'");
		ps.executeUpdate();
		
	}
}
