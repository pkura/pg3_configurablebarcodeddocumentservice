package pl.compan.docusafe.general.hibernate.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_invoice_general_budzety_pozycja_simple")
public class BudzetPozycjaSimpleDB {

	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "bd_nad_rodzaj_ko_id")
	private Long bd_nad_rodzaj_ko_id;
	
	@Column(name = "czyAktywna")
	private Boolean czyAktywna;
	
	@Column(name = "erpId")
	private Long erpId;
	
	@Column(name = "nazwa")
	private String nazwa;
	
	@Column(name = "nrpoz")
	private Long nrpoz;
	
	@Column(name = "opis")
	private String opis;
	
	@Column(name = "p_koszt")
	private BigDecimal p_koszt;
	
	@Column(name = "parent")
	private String parent;
	
	@Column(name = "parent_id")
	private Long parentId;	
	
	@Column(name = "parent_idm")
	private String parent_idm;
	
	@Column(name = "r_koszt")
	private BigDecimal r_koszt;
	
	@Column(name = "rez_koszt")
	private BigDecimal rez_koszt;
	
	@Column(name = "wytwor_id")
	private Long wytwor_id;
	
	@Column(name = "wytwor_idm")
	private String wytwor_idm;
	
	@Column(name = "available")
	private Boolean available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBd_nad_rodzaj_ko_id() {
		return bd_nad_rodzaj_ko_id;
	}

	public void setBd_nad_rodzaj_ko_id(Long bd_nad_rodzaj_ko_id) {
		this.bd_nad_rodzaj_ko_id = bd_nad_rodzaj_ko_id;
	}

	public Boolean getCzyAktywna() {
		return czyAktywna;
	}

	public void setCzyAktywna(Boolean czyAktywna) {
		this.czyAktywna = czyAktywna;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Long getNrpoz() {
		return nrpoz;
	}

	public void setNrpoz(Long nrpoz) {
		this.nrpoz = nrpoz;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public BigDecimal getP_koszt() {
		return p_koszt;
	}

	public void setP_koszt(BigDecimal p_koszt) {
		this.p_koszt = p_koszt;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParent_idm() {
		return parent_idm;
	}

	public void setParent_idm(String parent_idm) {
		this.parent_idm = parent_idm;
	}

	public BigDecimal getR_koszt() {
		return r_koszt;
	}

	public void setR_koszt(BigDecimal r_koszt) {
		this.r_koszt = r_koszt;
	}

	public BigDecimal getRez_koszt() {
		return rez_koszt;
	}

	public void setRez_koszt(BigDecimal rez_koszt) {
		this.rez_koszt = rez_koszt;
	}

	public Long getWytwor_id() {
		return wytwor_id;
	}

	public void setWytwor_id(Long wytwor_id) {
		this.wytwor_id = wytwor_id;
	}

	public String getWytwor_idm() {
		return wytwor_idm;
	}

	public void setWytwor_idm(String wytwor_idm) {
		this.wytwor_idm = wytwor_idm;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}		
	
	
	
}
