package pl.compan.docusafe.general.hibernate.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_projekt_budzet_etap")
public class ProjektBudzetEtapDB {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "erpId", nullable = true)
	private Long erpId;
	
	@Column(name = "nazwa", nullable = false)
	private String nazwa;
	
	@Column(name = "nrpoz", nullable = true)
	private Integer nrpoz;

	@Column(name = "p_datako")
	private Date p_datako;
	
	@Column(name = "p_datapo")
	private Date p_datapo;
	
	@Column(name = "parent", nullable = false)
	private String parent;
	
	@Column(name = "parent_id", nullable = false)
	private Long parent_id;
	
	@Column(name = "parent_idm", nullable = false)
	private String parent_idm;
	
/*	@Column(name = "cn", nullable = false)
	private String cn;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "centrum", nullable = true)
	private Integer centrum;	

	@Column(name = "refValue", nullable = true)
	private String refValue;

	@Column(name = "available", nullable = true)
	private Integer available;*/
	
	
/*	@Column(name = "budzet_id")
	private Double budzet_id;*/
	
	@Column(name="available")
	private Boolean available;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNrpoz() {
		return nrpoz;
	}

	public void setNrpoz(Integer nrpoz) {
		this.nrpoz = nrpoz;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public Date getP_datako() {
		return p_datako;
	}

	public void setP_datako(Date p_datako) {
		this.p_datako = p_datako;
	}

	public Date getP_datapo() {
		return p_datapo;
	}

	public void setP_datapo(Date p_datapo) {
		this.p_datapo = p_datapo;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public Long getParent_id() {
		return parent_id;
	}

	public void setParent_id(Long parent_id) {
		this.parent_id = parent_id;
	}

	public String getParent_idm() {
		return parent_idm;
	}

	public void setParent_idm(String parent_idm) {
		this.parent_idm = parent_idm;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

}
