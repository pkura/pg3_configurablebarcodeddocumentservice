package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_rodzaje_planow_zak")
public class RodzajePlanowZakupuDB {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
		
	@Column(name = "cena")
	private double cena;
	
	@Column(name = "czy_aktywna")
	private boolean czy_aktywna;

	@Column(name = "erpId")
	private Long erpId;
	
	@Column(name = "komorkaRealizujaca")
	private Long komorkaRealizujaca;
	
	@Column(name = "nazwa")
	private String nazwa;
	
	@Column(name = "nrpoz")
	private int nrpoz;
	
	@Column(name = "opis")
	private String opis;
	
	@Column(name = "p_ilosc")
	private double p_ilosc;
	
	@Column(name = "p_koszt")
	private double p_koszt;
	
	@Column(name = "parent")
	private String parent;
	
	@Column(name = "parentId")
	private Long parentId;
	
	@Column(name = "parentIdm")
	private String parentIdm;
	
	
	@Column(name = "r_ilosc")
	private double r_ilosc;
	
	@Column(name = "r_koszt")
	private double r_koszt;

	@Column(name = "rez_ilosc")
	private double rez_ilosc;
	
	@Column(name = "rez_koszt")
	private double rez_koszt;
	
	@Column(name = "wytwor_id")
	private long wytwor_id;
	
	@Column(name = "wytwor_idm")
	private String wytwor_idm;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public boolean getCzy_aktywna() {
		return czy_aktywna;
	}

	public void setCzy_aktywna(boolean czy_aktywna) {
		this.czy_aktywna = czy_aktywna;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public int getNrpoz() {
		return nrpoz;
	}

	public void setNrpoz(int nrpoz) {
		this.nrpoz = nrpoz;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public double getP_ilosc() {
		return p_ilosc;
	}

	public void setP_ilosc(double p_ilosc) {
		this.p_ilosc = p_ilosc;
	}

	public double getP_koszt() {
		return p_koszt;
	}

	public void setP_koszt(double p_koszt) {
		this.p_koszt = p_koszt;
	}

	public double getR_ilosc() {
		return r_ilosc;
	}

	public void setR_ilosc(double r_ilosc) {
		this.r_ilosc = r_ilosc;
	}

	public double getR_koszt() {
		return r_koszt;
	}

	public void setR_koszt(double r_koszt) {
		this.r_koszt = r_koszt;
	}

	public long getWytwor_id() {
		return wytwor_id;
	}

	public void setWytwor_id(long wytwor_id) {
		this.wytwor_id = wytwor_id;
	}

	public Long getKomorkaRealizujaca() {
		return komorkaRealizujaca;
	}

	public void setKomorkaRealizujaca(Long komorkaRealizujaca) {
		this.komorkaRealizujaca = komorkaRealizujaca;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParentIdm() {
		return parentIdm;
	}

	public void setParentIdm(String parentIdm) {
		this.parentIdm = parentIdm;
	}

	public double getRez_ilosc() {
		return rez_ilosc;
	}

	public void setRez_ilosc(double rez_ilosc) {
		this.rez_ilosc = rez_ilosc;
	}

	public double getRez_koszt() {
		return rez_koszt;
	}

	public void setRez_koszt(double rez_koszt) {
		this.rez_koszt = rez_koszt;
	}

	public String getWytwor_idm() {
		return wytwor_idm;
	}

	public void setWytwor_idm(String wytwor_idm) {
		this.wytwor_idm = wytwor_idm;
	}
}
