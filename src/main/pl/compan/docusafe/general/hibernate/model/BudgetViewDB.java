package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

@Entity
@Table(name="DSG_budzety")
public class BudgetViewDB {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "bd_budzet_id", nullable = false)
	private double bdBudzetId;
	
	@Column(name = "bd_budzet_idm")
	private String bdBudzetIdm;
	
	@Column(name = "bd_budzet_rodzaj_id")
	private double bdBudzetRodzajId;
	
	@Column(name = "bd_budzet_rodzaj_zadania_id")
	private double bdBudzetRodzajZadaniaId;
	
	@Column(name = "bd_budzet_rodzaj_zrodla_id")
	private double bdBudzetRodzajZrodlaId;
	
	@Column(name = "bd_grupa_rodzaj_id")
	private double bdGrupaRodzajId;
	
	@Column(name = "bd_rodzaj")
	private String bdRodzaj;
	
	@Column(name = "bd_str_budzet_idn")
	private String bdStrBudzetIdn;
	
	@Column(name = "bd_str_budzet_ids")
	private String bdStrBudzetIds;
	
	@Column(name = "bd_szablon_poz_id")
	private double bdSzablonPozId;
	
	@Column(name = "bd_zadanie_id")
	private double bdZadanieId;
	
	@Column(name = "bd_zadanie_idn")
	private String bdZadanieIdn;
	
	@Column(name = "bud_w_okr_nazwa")
	private String budWOkrNazwa;
	
	@Column(name = "budzet_id")
	private double budzetId;
	
	@Column(name = "budzet_idm")
	private String budzetIdm;
	
	@Column(name = "budzet_ids")
	private String budzetIds;
	
	@Column(name = "budzet_nazwa")
	private String budzetNazwa;
	
	@Column(name = "czy_aktywna")
	private double czyAktywna;
	
	@Column(name = "dok_bd_str_budzet_idn")
	private String dokBdStrBudzetIdn;
	
	@Column(name = "dok_kom_nazwa")
	private String dokKomNazwa;
	
	@Column(name = "dok_poz_limit")
	private double dokPozLimit;
	
	@Column(name = "dok_poz_nazwa")
	private String dokPozNazwa;
	
	@Column(name = "dok_poz_nrpoz")
	private int dokPozNrpoz;
	
	@Column(name = "dok_poz_p_koszt")
	private double dokPozPKoszt;
	
	@Column(name = "dok_poz_r_koszt")
	private double dokPozRKoszt;
	
	@Column(name = "dok_poz_rez_koszt")
	private double dokPozRezKoszt;
	
	@Column(name = "okrrozl_id")
	private double okrrozlId;
	
	@Column(name = "pozycja_podrzedna_id")
	private double pozycjaPodrzednaId;
	
	@Column(name = "str_bud_nazwa")
	private String strBudNazwa;
	
	@Column(name = "wspolczynnik_pozycji")
	private int wspolczynnikPozycji;
	
	@Column(name = "wytwor_id")
	private double wytworId;
	
	@Column(name = "zadanie_nazwa")
	private String zadanieNazwa;
	
	@Column(name = "zadanie_p_koszt")
	private double zadaniePKoszt;
	
	@Column(name = "zadanie_r_koszt")
	private double zadanieRKoszt;
	
	@Column(name = "zadanie_rez_koszt")
	private double zadanieRezKoszt;
	
	@Column(name = "zrodlo_id")
	private double zrodloId;
	
	@Column(name = "zrodlo_idn")
	private String zrodloIdn;
	
	@Column(name = "zrodlo_nazwa")
	private String zrodloNazwa;
	
	@Column(name = "zrodlo_p_koszt")
	private double zrodloPKoszt;
	
	@Column(name = "zrodlo_r_koszt")
	private double zrodloRKoszt;
	
	@Column(name = "zrodlo_rez_koszt")
	private double zrodloRezKoszt;
	
	@Column(name = "available")
	private boolean available;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getBdBudzetId() {
		return bdBudzetId;
	}

	public void setBdBudzetId(double bdBudzetId) {
		this.bdBudzetId = bdBudzetId;
	}

	public String getBdBudzetIdm() {
		return bdBudzetIdm;
	}

	public void setBdBudzetIdm(String bdBudzetIdm) {
		this.bdBudzetIdm = bdBudzetIdm;
	}

	public double getBdBudzetRodzajId() {
		return bdBudzetRodzajId;
	}

	public void setBdBudzetRodzajId(double bdBudzetRodzajId) {
		this.bdBudzetRodzajId = bdBudzetRodzajId;
	}

	public double getBdBudzetRodzajZadaniaId() {
		return bdBudzetRodzajZadaniaId;
	}

	public void setBdBudzetRodzajZadaniaId(double bdBudzetRodzajZadaniaId) {
		this.bdBudzetRodzajZadaniaId = bdBudzetRodzajZadaniaId;
	}

	public double getBdBudzetRodzajZrodlaId() {
		return bdBudzetRodzajZrodlaId;
	}

	public void setBdBudzetRodzajZrodlaId(double bdBudzetRodzajZrodlaId) {
		this.bdBudzetRodzajZrodlaId = bdBudzetRodzajZrodlaId;
	}

	public double getBdGrupaRodzajId() {
		return bdGrupaRodzajId;
	}

	public void setBdGrupaRodzajId(double bdGrupaRodzajId) {
		this.bdGrupaRodzajId = bdGrupaRodzajId;
	}

	public String getBdRodzaj() {
		return bdRodzaj;
	}

	public void setBdRodzaj(String bdRodzaj) {
		this.bdRodzaj = bdRodzaj;
	}

	public String getBdStrBudzetIdn() {
		return bdStrBudzetIdn;
	}

	public void setBdStrBudzetIdn(String bdStrBudzetIdn) {
		this.bdStrBudzetIdn = bdStrBudzetIdn;
	}

	public String getBdStrBudzetIds() {
		return bdStrBudzetIds;
	}

	public void setBdStrBudzetIds(String bdStrBudzetIds) {
		this.bdStrBudzetIds = bdStrBudzetIds;
	}

	public double getBdSzablonPozId() {
		return bdSzablonPozId;
	}

	public void setBdSzablonPozId(double bdSzablonPozId) {
		this.bdSzablonPozId = bdSzablonPozId;
	}

	public double getBdZadanieId() {
		return bdZadanieId;
	}

	public void setBdZadanieId(double bdZadanieId) {
		this.bdZadanieId = bdZadanieId;
	}

	public String getBdZadanieIdn() {
		return bdZadanieIdn;
	}

	public void setBdZadanieIdn(String bdZadanieIdn) {
		this.bdZadanieIdn = bdZadanieIdn;
	}

	public String getBudWOkrNazwa() {
		return budWOkrNazwa;
	}

	public void setBudWOkrNazwa(String budWOkrNazwa) {
		this.budWOkrNazwa = budWOkrNazwa;
	}

	public double getBudzetId() {
		return budzetId;
	}

	public void setBudzetId(double budzetId) {
		this.budzetId = budzetId;
	}

	public String getBudzetIdm() {
		return budzetIdm;
	}

	public void setBudzetIdm(String budzetIdm) {
		this.budzetIdm = budzetIdm;
	}

	public String getBudzetIds() {
		return budzetIds;
	}

	public void setBudzetIds(String budzetIds) {
		this.budzetIds = budzetIds;
	}

	public String getBudzetNazwa() {
		return budzetNazwa;
	}

	public void setBudzetNazwa(String budzetNazwa) {
		this.budzetNazwa = budzetNazwa;
	}

	public double getCzyAktywna() {
		return czyAktywna;
	}

	public void setCzyAktywna(double czyAktywna) {
		this.czyAktywna = czyAktywna;
	}

	public String getDokBdStrBudzetIdn() {
		return dokBdStrBudzetIdn;
	}

	public void setDokBdStrBudzetIdn(String dokBdStrBudzetIdn) {
		this.dokBdStrBudzetIdn = dokBdStrBudzetIdn;
	}

	public String getDokKomNazwa() {
		return dokKomNazwa;
	}

	public void setDokKomNazwa(String dokKomNazwa) {
		this.dokKomNazwa = dokKomNazwa;
	}

	public double getDokPozLimit() {
		return dokPozLimit;
	}

	public void setDokPozLimit(double dokPozLimit) {
		this.dokPozLimit = dokPozLimit;
	}

	public String getDokPozNazwa() {
		return dokPozNazwa;
	}

	public void setDokPozNazwa(String dokPozNazwa) {
		this.dokPozNazwa = dokPozNazwa;
	}

	public int getDokPozNrpoz() {
		return dokPozNrpoz;
	}

	public void setDokPozNrpoz(int dokPozNrpoz) {
		this.dokPozNrpoz = dokPozNrpoz;
	}

	public double getDokPozPKoszt() {
		return dokPozPKoszt;
	}

	public void setDokPozPKoszt(double dokPozPKoszt) {
		this.dokPozPKoszt = dokPozPKoszt;
	}

	public double getDokPozRKoszt() {
		return dokPozRKoszt;
	}

	public void setDokPozRKoszt(double dokPozRKoszt) {
		this.dokPozRKoszt = dokPozRKoszt;
	}

	public double getDokPozRezKoszt() {
		return dokPozRezKoszt;
	}

	public void setDokPozRezKoszt(double dokPozRezKoszt) {
		this.dokPozRezKoszt = dokPozRezKoszt;
	}

	public double getOkrrozlId() {
		return okrrozlId;
	}

	public void setOkrrozlId(double okrrozlId) {
		this.okrrozlId = okrrozlId;
	}

	public double getPozycjaPodrzednaId() {
		return pozycjaPodrzednaId;
	}

	public void setPozycjaPodrzednaId(double pozycjaPodrzednaId) {
		this.pozycjaPodrzednaId = pozycjaPodrzednaId;
	}

	public String getStrBudNazwa() {
		return strBudNazwa;
	}

	public void setStrBudNazwa(String strBudNazwa) {
		this.strBudNazwa = strBudNazwa;
	}

	public int getWspolczynnikPozycji() {
		return wspolczynnikPozycji;
	}

	public void setWspolczynnikPozycji(int wspolczynnikPozycji) {
		this.wspolczynnikPozycji = wspolczynnikPozycji;
	}

	public double getWytworId() {
		return wytworId;
	}

	public void setWytworId(double wytworId) {
		this.wytworId = wytworId;
	}

	public String getZadanieNazwa() {
		return zadanieNazwa;
	}

	public void setZadanieNazwa(String zadanieNazwa) {
		this.zadanieNazwa = zadanieNazwa;
	}

	public double getZadaniePKoszt() {
		return zadaniePKoszt;
	}

	public void setZadaniePKoszt(double zadaniePKoszt) {
		this.zadaniePKoszt = zadaniePKoszt;
	}

	public double getZadanieRKoszt() {
		return zadanieRKoszt;
	}

	public void setZadanieRKoszt(double zadanieRKoszt) {
		this.zadanieRKoszt = zadanieRKoszt;
	}

	public double getZadanieRezKoszt() {
		return zadanieRezKoszt;
	}

	public void setZadanieRezKoszt(double zadanieRezKoszt) {
		this.zadanieRezKoszt = zadanieRezKoszt;
	}

	public double getZrodloId() {
		return zrodloId;
	}

	public void setZrodloId(double zrodloId) {
		this.zrodloId = zrodloId;
	}

	public String getZrodloIdn() {
		return zrodloIdn;
	}

	public void setZrodloIdn(String zrodloIdn) {
		this.zrodloIdn = zrodloIdn;
	}

	public String getZrodloNazwa() {
		return zrodloNazwa;
	}

	public void setZrodloNazwa(String zrodloNazwa) {
		this.zrodloNazwa = zrodloNazwa;
	}

	public double getZrodloPKoszt() {
		return zrodloPKoszt;
	}

	public void setZrodloPKoszt(double zrodloPKoszt) {
		this.zrodloPKoszt = zrodloPKoszt;
	}

	public double getZrodloRKoszt() {
		return zrodloRKoszt;
	}

	public void setZrodloRKoszt(double zrodloRKoszt) {
		this.zrodloRKoszt = zrodloRKoszt;
	}

	public double getZrodloRezKoszt() {
		return zrodloRezKoszt;
	}

	public void setZrodloRezKoszt(double zrodloRezKoszt) {
		this.zrodloRezKoszt = zrodloRezKoszt;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

}
