package pl.compan.docusafe.general.hibernate.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@Entity
@Table(name="dsg_invoice_general_multiple")
public class FakturaMulti {
	private static final Logger log = LoggerFactory.getLogger(FakturaMulti.class);

	@Id
	@Column(name="ID")
	private Long ID;
	@Column(name="DOCUMENT_ID")
	private Long DOCUMENT_ID;
	@Column(name="FIELD_CN")
	private String FIELD_CN;
	@Column(name="FIELD_VAL")
	private String FIELD_VAL;


	public static List<FakturaMulti> list(Long documentId,String Cn) throws EdmException {
		return DSApi.context().session().createCriteria(FakturaMulti.class).add(Restrictions.eq("DOCUMENT_ID", documentId)).add(Restrictions.eq("FIELD_CN", Cn)).list();
	}
	public Long getID() {
		return ID;
	}
	public void setID(Long iD) {
		ID = iD;
	}
	public Long getDOCUMENT_ID() {
		return DOCUMENT_ID;
	}
	public void setDOCUMENT_ID(Long DOCUMENT_ID) {
		this.DOCUMENT_ID = DOCUMENT_ID;
	}
	public String getFIELD_CN() {
		return FIELD_CN;
	}
	public void setFIELD_CN(String fIELD_CN) {
		FIELD_CN = fIELD_CN;
	}
	public String getFIELD_VAL() {
		return FIELD_VAL;
	}
	public void setFIELD_VAL(String fIELD_VAL) {
		FIELD_VAL = fIELD_VAL;
	}

}
