package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_projectAccount")
public class ProjectAccountPiatkiDB {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "konto_ido", nullable = false)
	private String konto_ido;

	@Column(name = "kontrakt_id", nullable = true)
	private Long kontrakt_id;

	@Column(name = "kontrakt_idm", nullable = true)
	private String kontrakt_idm;

	@Column(name = "rep_atrybut_ido", nullable = true)
	private String rep_atrybut_ido;
	
	@Column(name = "available", nullable = true)
	private Boolean available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKonto_ido() {
		return konto_ido;
	}

	public void setKonto_ido(String konto_ido) {
		this.konto_ido = konto_ido;
	}

	public Long getKontrakt_id() {
		return kontrakt_id;
	}

	public void setKontrakt_id(Long kontrakt_id) {
		this.kontrakt_id = kontrakt_id;
	}

	public String getKontrakt_idm() {
		return kontrakt_idm;
	}

	public void setKontrakt_idm(String kontrakt_idm) {
		this.kontrakt_idm = kontrakt_idm;
	}

	public String getRep_atrybut_ido() {
		return rep_atrybut_ido;
	}

	public void setRep_atrybut_ido(String rep_atrybut_ido) {
		this.rep_atrybut_ido = rep_atrybut_ido;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}	
	
	
	
	
	
}
