package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="dsg_szablony_rozl_faktury")
public class SzablonyRozliczenFakturDB {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "erpId")
    private Long erpId;
	
	@Column(name = "idm")
    private String idm;
	
	@Column(name = "nazwa")
	private String nazwa;
	
	@Column(name = "okres_rozl")
    private String okres_rozl;
	
	@Column(name = "okres_id")
	private Long okresId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getOkres_rozl() {
		return okres_rozl;
	}

	public void setOkres_rozl(String okres_rozl) {
		this.okres_rozl = okres_rozl;
	}

	public Long getOkresId() {
		return okresId;
	}

	public void setOkresId(Long okresId) {
		this.okresId = okresId;
	}
	
	
}
