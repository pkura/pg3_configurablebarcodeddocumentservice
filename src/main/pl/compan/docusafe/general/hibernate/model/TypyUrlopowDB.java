package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_typy_urlopow")
public class TypyUrlopowDB {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO )
	private Long id;
	
	@Column(name = "idm")
	private String idm;
	
	@Column(name = "opis")
	private String opis;
	
	@Column(name = "opis_dlugi")
	private String opis_dlugi;
	
	@Column(name = "erpId")
	private Long erpId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getOpis_dlugi() {
		return opis_dlugi;
	}

	public void setOpis_dlugi(String opis_dlugi) {
		this.opis_dlugi = opis_dlugi;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
}
