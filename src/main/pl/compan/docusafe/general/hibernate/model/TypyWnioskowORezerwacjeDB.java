package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_typy_wnioskow_o_rez")
public class TypyWnioskowORezerwacjeDB {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "nazwa")
	private String nazwa;
	
	@Column(name = "idm")
	private String idm;
	
	@Column(name = "erpId")
	private Long erpId;
	
	@Column(name = "czy_aktywny")
	private boolean czy_aktywny;
	
	@Column(name = "czy_kontrola_ceny")
	private boolean czy_kontrola_ceny;
	
	@Column(name = "czy_kontrola_ilosci")
	private boolean czy_kontrola_ilosci;
	
	@Column(name = "czy_kontrola_wartosci")
	private boolean czy_kontrola_wartosci;
	
	@Column(name = "czy_obsluga_pzp")
	private boolean czy_obsluga_pzp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public boolean getCzy_aktywny() {
		return czy_aktywny;
	}

	public void setCzy_aktywny(boolean czy_aktywny) {
		this.czy_aktywny = czy_aktywny;
	}

	public boolean getCzy_kontrola_ceny() {
		return czy_kontrola_ceny;
	}

	public void setCzy_kontrola_ceny(boolean czy_kontrola_ceny) {
		this.czy_kontrola_ceny = czy_kontrola_ceny;
	}

	public boolean getCzy_kontrola_ilosci() {
		return czy_kontrola_ilosci;
	}

	public void setCzy_kontrola_ilosci(boolean czy_kontrola_ilosci) {
		this.czy_kontrola_ilosci = czy_kontrola_ilosci;
	}

	public boolean getCzy_kontrola_wartosci() {
		return czy_kontrola_wartosci;
	}

	public void setCzy_kontrola_wartosci(boolean czy_kontrola_wartosci) {
		this.czy_kontrola_wartosci = czy_kontrola_wartosci;
	}

	public boolean getCzy_obsluga_pzp() {
		return czy_obsluga_pzp;
	}

	public void setCzy_obsluga_pzp(boolean czy_obsluga_pzp) {
		this.czy_obsluga_pzp = czy_obsluga_pzp;
	}
}
