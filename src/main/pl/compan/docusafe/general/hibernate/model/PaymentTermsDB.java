package pl.compan.docusafe.general.hibernate.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.google.common.collect.Lists;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;


@Entity
@Table(name = "dsg_payment_terms")
public class PaymentTermsDB {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "cn", nullable = false)
	private String cn;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "centrum", nullable = true)
	private Integer centrum;	

	@Column(name = "refValue", nullable = true)
	private String refValue;

	@Column(name = "available", nullable = true)
	private Integer available;

	public PaymentTermsDB() {
	}

	public static List<PaymentTermsDB> list() throws EdmException
	{
		return DSApi.context().session().createCriteria(PaymentTermsDB.class)
				.addOrder(Order.asc("title"))
				.list();	

	}

	public static PaymentTermsDB find(Long id) throws EdmException
	{
		PaymentTermsDB status = (PaymentTermsDB) DSApi.getObject(PaymentTermsDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(PaymentTermsDB.class, id);

		return status;
	}

	public static List<PaymentTermsDB> findByCn(String _cn) throws EdmException
	{
		return DSApi.context().session().createCriteria(PaymentTermsDB.class).add(Restrictions.eq("cn", _cn)).list();
	}
	
	public static List<PaymentTermsDB> findByCnRefVal(String _cn, String refVal) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(PaymentTermsDB.class);
		criteria.add(Restrictions.eq("cn", _cn));
		criteria.add(Restrictions.eq("refValue", refVal));
		return criteria.list();
	}

	public static List<PaymentTermsDB> findByCn(Collection<?> cn, boolean notIn) throws EdmException
	{
		List<PaymentTermsDB> list = Lists.newArrayList();
		try{
			Criteria criteria = DSApi.context().session().createCriteria(PaymentTermsDB.class);

			if(cn != null){
				if(notIn){
					criteria.add(Restrictions.not(Restrictions.in("cn", cn)));
				} else {
					criteria.add(Restrictions.in("cn", cn));
				}
			} else {
				return list;
			}

			list = criteria.list();

			return list;
		}catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public final void save() throws EdmException
	{
		try
		{
			DSApi.context().session().save(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(PaymentTermsDB.class)+" set available='0'");
		ps.executeUpdate();
		
	}
}

