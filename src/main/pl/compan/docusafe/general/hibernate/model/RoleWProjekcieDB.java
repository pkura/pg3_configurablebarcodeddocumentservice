package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_role_in_project")
public class RoleWProjekcieDB {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "bp_rola_id")
	private double bp_rola_id;
	
	@Column(name = "bp_rola_idn")
	private String bp_rola_idn;
	
	@Column(name = "bp_rola_kontrakt_id")
	private double bp_rola_kontrakt_id;
	
	@Column(name = "czy_blokada_prac")
	private boolean czy_blokada_prac;
	
	@Column(name = "czy_blokada_roli")
	private boolean czy_blokada_roli;
	
	@Column(name = "kontrakt_id")
	private double kontrakt_id;
	
	@Column(name = "kontrakt_idm")
	private String kontrakt_idm;
	
	@Column(name = "pracownik_id")
	private double pracownik_id;
	
	@Column(name = "pracownik_idn")
	private String pracownik_idn;
	
	@Column(name = "pracownik_nrewid")
	private int pracownik_nrewid;
	
	@Column(name = "osobaGuid")
	private String osobaGuid;
	
	@Column(name = "osoba_id")
	private Long osobaId;
	
	@Column(name = "available")
	private Boolean available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getBp_rola_id() {
		return bp_rola_id;
	}

	public void setBp_rola_id(double bp_rola_id) {
		this.bp_rola_id = bp_rola_id;
	}

	public String getBp_rola_idn() {
		return bp_rola_idn;
	}

	public void setBp_rola_idn(String bp_rola_idn) {
		this.bp_rola_idn = bp_rola_idn;
	}

	public double getBp_rola_kontrakt_id() {
		return bp_rola_kontrakt_id;
	}

	public void setBp_rola_kontrakt_id(double bp_rola_kontrakt_id) {
		this.bp_rola_kontrakt_id = bp_rola_kontrakt_id;
	}

	public boolean getCzy_blokada_prac() {
		return czy_blokada_prac;
	}

	public void setCzy_blokada_prac(boolean czy_blokada_prac) {
		this.czy_blokada_prac = czy_blokada_prac;
	}

	public boolean getCzy_blokada_roli() {
		return czy_blokada_roli;
	}

	public void setCzy_blokada_roli(boolean czy_blokada_roli) {
		this.czy_blokada_roli = czy_blokada_roli;
	}

	public double getKontrakt_id() {
		return kontrakt_id;
	}

	public void setKontrakt_id(double kontrakt_id) {
		this.kontrakt_id = kontrakt_id;
	}

	public String getKontrakt_idm() {
		return kontrakt_idm;
	}

	public void setKontrakt_idm(String kontrakt_idm) {
		this.kontrakt_idm = kontrakt_idm;
	}

	public double getPracownik_id() {
		return pracownik_id;
	}

	public void setPracownik_id(double pracownik_id) {
		this.pracownik_id = pracownik_id;
	}

	public String getPracownik_idn() {
		return pracownik_idn;
	}

	public void setPracownik_idn(String pracownik_idn) {
		this.pracownik_idn = pracownik_idn;
	}

	public int getPracownik_nrewid() {
		return pracownik_nrewid;
	}

	public void setPracownik_nrewid(int pracownik_nrewid) {
		this.pracownik_nrewid = pracownik_nrewid;
	}

	public String getOsobaGuid() {
		return osobaGuid;
	}

	public void setOsobaGuid(String osobaGuid) {
		this.osobaGuid = osobaGuid;
	}

	public Long getOsobaId() {
		return osobaId;
	}

	public void setOsobaId(Long osobaId) {
		this.osobaId = osobaId;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

}
