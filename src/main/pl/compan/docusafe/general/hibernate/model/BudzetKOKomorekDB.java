package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_budzetyKO_komorek")
public class BudzetKOKomorekDB {
	
	
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "idm")
	private String idm;
	
	@Column(name = "nazwa")
	private String nazwa;
	
	@Column(name = "okres_id")
	private Long okresId;
	
	@Column(name = "okres_idm")
	private String okresIdm;

	@Column(name = "budzet_id")
	private Long budzet_id;
	
	@Column(name = "budzet_idn")
	private String budzet_idn;
	
	@Column(name = "erpId")
	private Long erpId;

	@Column(name = "czyAgregat")
	private Boolean czyAgregat;
	
	@Column(name="available")
	private Boolean available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}


	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public Long getErpId() {
		return erpId;
	}

	public Long getOkresId() {
		return okresId;
	}

	public void setOkresId(Long okresId) {
		this.okresId = okresId;
	}

	public String getOkresIdm() {
		return okresIdm;
	}

	public void setOkresIdm(String okresIdm) {
		this.okresIdm = okresIdm;
	}

	public Boolean getCzyAgregat() {
		return czyAgregat;
	}

	public void setCzyAgregat(Boolean czyAgregat) {
		this.czyAgregat = czyAgregat;
	}

	public Long getBudzet_id() {
		return budzet_id;
	}

	public void setBudzet_id(Long budzet_id) {
		this.budzet_id = budzet_id;
	}

	public String getBudzet_idn() {
		return budzet_idn;
	}

	public void setBudzet_idn(String budzet_idn) {
		this.budzet_idn = budzet_idn;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
	
	
}
