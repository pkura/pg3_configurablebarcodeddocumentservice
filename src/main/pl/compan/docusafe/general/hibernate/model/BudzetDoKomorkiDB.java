package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dsg_budzetDokomorki")
public class BudzetDoKomorkiDB {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "budzet_id")
    private Long budzet_id;
	
	@Column(name = "budzet_idn")
    private String budzet_idn;
	
	@Column(name = "budzet_nad_id")
    private Long budzet_nad_id;
	
	@Column(name = "budzet_nad_idn")
	private String budzet_nad_idn;
    
	@Column(name = "okres_id")
	private Long okresId;
	
	@Column(name = "czyWiodaca")
    private Boolean czyWiodaca;
	
	@Column(name = "komorka_id")
    private Long komorkaId;
	
	@Column(name = "komorka_idn")
    private String komorkaIdn;
	
	@Column(name = "nazwaKomorkiBudzetowej")
    private String nazwaKomorkiBudzetowej;
	
	@Column(name = "nazwaKomorkiPrganizacyjnej")
    private String nazwaKomorkiOrg;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBudzet_id() {
		return budzet_id;
	}

	public void setBudzet_id(Long budzet_id) {
		this.budzet_id = budzet_id;
	}

	public String getBudzet_idn() {
		return budzet_idn;
	}

	public void setBudzet_idn(String budzet_idn) {
		this.budzet_idn = budzet_idn;
	}

	public Long getBudzet_nad_id() {
		return budzet_nad_id;
	}

	public void setBudzet_nad_id(Long budzet_nad_id) {
		this.budzet_nad_id = budzet_nad_id;
	}

	public String getBudzet_nad_idn() {
		return budzet_nad_idn;
	}

	public void setBudzet_nad_idn(String budzet_nad_idn) {
		this.budzet_nad_idn = budzet_nad_idn;
	}

	public Long getOkresId() {
		return okresId;
	}

	public void setOkresId(Long okresId) {
		this.okresId = okresId;
	}

	public Boolean getCzyWiodaca() {
		return czyWiodaca;
	}

	public void setCzyWiodaca(Boolean czyWiodaca) {
		this.czyWiodaca = czyWiodaca;
	}

	public Long getKomorkaId() {
		return komorkaId;
	}

	public void setKomorkaId(Long komorkaId) {
		this.komorkaId = komorkaId;
	}

	public String getKomorkaIdn() {
		return komorkaIdn;
	}

	public void setKomorkaIdn(String komorkaIdn) {
		this.komorkaIdn = komorkaIdn;
	}

	public String getNazwaKomorkiBudzetowej() {
		return nazwaKomorkiBudzetowej;
	}

	public void setNazwaKomorkiBudzetowej(String nazwaKomorkiBudzetowej) {
		this.nazwaKomorkiBudzetowej = nazwaKomorkiBudzetowej;
	}

	public String getNazwaKomorkiOrg() {
		return nazwaKomorkiOrg;
	}

	public void setNazwaKomorkiOrg(String nazwaKomorkiOrg) {
		this.nazwaKomorkiOrg = nazwaKomorkiOrg;
	}
	
	
	
}
