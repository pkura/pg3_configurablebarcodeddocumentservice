package pl.compan.docusafe.general.hibernate.model;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;


@Entity
@Table(name = "dsg_document_types")
public class PurchaseDocumentTypeDB implements Serializable{
	private static final long serialVersionUID = 1L;

	public static final String ENUM_VIEW_TABLE = "simple_erp_per_docusafe_typDokumentu_view";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "typdok_ids", nullable = false)
	private String cn;

	@Column(name = "nazwa", nullable = false)
	private String title;

	@Column(name = "available", nullable = true)
	private Integer available;

	@Column(name = "czywal", nullable = true)
	private Boolean czywal;

	@Column(name = "typdok_idn", nullable = true)
	private String typdokIdn;

	@Column(name = "typdokzak_id", nullable = false)
	private Long typdokzak_id;

	@Column(name = "symbol_waluty", nullable = false)
	private String symbol_waluty;
	
	public PurchaseDocumentTypeDB() {
	}

	public static List<PurchaseDocumentTypeDB> list() throws EdmException
	{
		return DSApi.context().session().createCriteria(PurchaseDocumentTypeDB.class)
				.addOrder(Order.asc("title"))
				.list();	
	}

	public static PurchaseDocumentTypeDB find(Long id) throws EdmException
	{
		PurchaseDocumentTypeDB status = (PurchaseDocumentTypeDB) DSApi.getObject(PurchaseDocumentTypeDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(PurchaseDocumentTypeDB.class, id);

		return status;
	}
	
	public static List<PurchaseDocumentTypeDB> findByDokzakID(Long id) throws EdmException
	{
		return DSApi.context().session().createCriteria(PurchaseDocumentTypeDB.class).add(Restrictions.eq("typdokzak_id", id)).list();
	}
	
	 public static List<Long> getDocumentTypesId() throws EdmException {
	    	List<PurchaseDocumentTypeDB> documentTypeList = null;
			
			try {
				Criteria criteria = DSApi.context().session().createCriteria(PurchaseDocumentTypeDB.class);
				criteria.add(Restrictions.eq("available", 1));
				documentTypeList = (List<PurchaseDocumentTypeDB>) criteria.list();
			} catch (HibernateException e) {
				throw new EdmHibernateException(e);
			}
			List<Long> objectIds = new LinkedList<Long>();
			
			for (PurchaseDocumentTypeDB type : documentTypeList) {
				objectIds.add((long) type.getTypdokzak_id());
			}
			return objectIds;
	    }

	public final void save() throws EdmException
	{
		try
		{
			DSApi.context().session().save(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public Boolean getCzywal() {
		return czywal;
	}

	public void setCzywal(Boolean czywal) {
		this.czywal = czywal;
	}

	public String getTypdokIdn() {
		return typdokIdn;
	}

	public void setTypdokIdn(String typdokIdn) {
		this.typdokIdn = typdokIdn;
	}

	public Long getTypdokzak_id() {
		return typdokzak_id;
	}

	public void setTypdokzak_id(Long typdokzak_id) {
		this.typdokzak_id = typdokzak_id;
	}

	public String getSymbol_waluty() {
		return symbol_waluty;
	}

	public void setSymbol_waluty(String symbol_waluty) {
		this.symbol_waluty = symbol_waluty;
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(PurchaseDocumentTypeDB.class)+" set available='0'");
		ps.executeUpdate();
		
	}

}
