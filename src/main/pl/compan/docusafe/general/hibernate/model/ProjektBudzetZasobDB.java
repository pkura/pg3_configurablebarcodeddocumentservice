package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_project_budzet_zasob")
public class ProjektBudzetZasobDB {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

/*	@Column(name = "cn", nullable = false)
	private String cn;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "centrum", nullable = true)
	private Integer centrum;	

	@Column(name = "refValue", nullable = true)
	private String refValue;

	@Column(name = "available", nullable = true)
	private Integer available;*/

	@Column(name = "czy_glowny")
	private Boolean czy_glowny;
	
	@Column(name = "erpId", nullable = true)
	private Long erpId;
	
	@Column(name= "nazwa", nullable = true)
	private String nazwa;
	
	@Column(name = "nrpoz", nullable = true)
	private Integer nrpoz;
	
	@Column(name = "p_cena")
	private Double p_cena;
	
	@Column(name = "p_ilosc")
	private Double p_ilosc;
	
	@Column(name = "p_wartosc")
	private Double p_wartosc;

	@Column(name = "parent", nullable = true)
	private String parent;
	
	@Column(name = "parent_id", nullable = true)
	private Long parent_id;
	
	@Column(name = "r_ilosc")
	private Double r_ilosc;
	
	@Column(name = "r_wartosc")
	private Double r_wartosc;
	
	@Column(name = "rodzaj_zasobu")
	private Integer rodzaj_zasobu;
	
	@Column(name = "zasob_glowny_id")
	private Double zasob_glowny_id;
	
/*	@Column(name = "etap_id")
	private Double etap_id;*/
	
	@Column(name="available")
	private Boolean available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNrpoz() {
		return nrpoz;
	}

	public void setNrpoz(Integer nrpoz) {
		this.nrpoz = nrpoz;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public Boolean getCzy_glowny() {
		return czy_glowny;
	}

	public void setCzy_glowny(Boolean czy_glowny) {
		this.czy_glowny = czy_glowny;
	}

	public Double getP_cena() {
		return p_cena;
	}

	public void setP_cena(Double p_cena) {
		this.p_cena = p_cena;
	}

	public Double getP_ilosc() {
		return p_ilosc;
	}

	public void setP_ilosc(Double p_ilosc) {
		this.p_ilosc = p_ilosc;
	}

	public Double getP_wartosc() {
		return p_wartosc;
	}

	public void setP_wartosc(Double p_wartosc) {
		this.p_wartosc = p_wartosc;
	}

	public Double getR_ilosc() {
		return r_ilosc;
	}

	public void setR_ilosc(Double r_ilosc) {
		this.r_ilosc = r_ilosc;
	}

	public Double getR_wartosc() {
		return r_wartosc;
	}

	public void setR_wartosc(Double r_wartosc) {
		this.r_wartosc = r_wartosc;
	}

	public Integer getRodzaj_zasobu() {
		return rodzaj_zasobu;
	}

	public void setRodzaj_zasobu(Integer rodzaj_zasobu) {
		this.rodzaj_zasobu = rodzaj_zasobu;
	}

	public Double getZasob_glowny_id() {
		return zasob_glowny_id;
	}

	public void setZasob_glowny_id(Double zasob_glowny_id) {
		this.zasob_glowny_id = zasob_glowny_id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public Long getParent_id() {
		return parent_id;
	}

	public void setParent_id(Long parent_id) {
		this.parent_id = parent_id;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}


}
