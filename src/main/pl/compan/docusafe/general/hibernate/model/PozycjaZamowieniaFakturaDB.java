package pl.compan.docusafe.general.hibernate.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dsg_invoice_general_poz_zam")
public class PozycjaZamowieniaFakturaDB {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private  Long ID;
	
	@Column(name="DOCUMENT_ID_ZAMOWIENIA")
	private  Long DOCUMENT_ID_ZAMOWIENIA;
	
	@Column(name="DOCUMENT_ID")
	private  Long DOCUMENT_ID;
	
	@Column(name="ZAMPOZ_ID")
	private  Long ZAMPOZ_ID;
	
	@Column(name="POZYCJAID_ZAM")
	private     Long POZYCJAID_ZAM;
	
	@Column(name="OKRES_BUDZETOWY_ZAM")
	private    Long OKRES_BUDZETOWY_ZAM;
	
	@Column(name="KOMORKA_BUDZETOWA_ZAM")
	private   Long KOMORKA_BUDZETOWA_ZAM;
	
	@Column(name="IDENTYFIKATOR_PLANU_ZAM")
	private   Long IDENTYFIKATOR_PLANU_ZAM;
	
	@Column(name="IDENTYFIKATOR_BUDZETU_ZAM")
	private   Long IDENTYFIKATOR_BUDZETU_ZAM;
	
	@Column(name="POZYCJA_PLANU_ZAKUPOW_ZAM")
	private    String POZYCJA_PLANU_ZAKUPOW_ZAM;
	
	@Column(name="POZYCJA_BUDZETU_ZAM")
	private    String POZYCJA_BUDZETU_ZAM;
	
	@Column(name="PROJEKT_ZAM")
	private   Long PROJEKT_ZAM;
	
	@Column(name="PRODUKT_ZAM")
	private   Long PRODUKT_ZAM;
	
	@Column(name="OPIS_ZAM")
	private    String OPIS_ZAM;
	
	@Column(name="ILOSC_ZAM")
	private   BigDecimal ILOSC_ZAM;
	
	@Column(name="ILOSC_ZREAL_ZAM")
	private   BigDecimal ILOSC_ZREAL_ZAM;
	
	@Column(name="JM_ZAM")
	private   Long JM_ZAM;
	
	@Column(name="STAWKA_VAT_ZAM")
	private   Long STAWKA_VAT_ZAM;
	
	@Column(name="NETTO_ZAM")
	private   BigDecimal NETTO_ZAM;
	
	@Column(name="BRUTTO_ZAM")
	private   BigDecimal BRUTTO_ZAM;
	
	@Column(name="UWAGI_ZAM")
	private   String UWAGI_ZAM ;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public Long getDOCUMENT_ID_ZAMOWIENIA() {
		return DOCUMENT_ID_ZAMOWIENIA;
	}

	public void setDOCUMENT_ID_ZAMOWIENIA(Long dOCUMENT_ID_ZAMOWIENIA) {
		DOCUMENT_ID_ZAMOWIENIA = dOCUMENT_ID_ZAMOWIENIA;
	}

	public Long getDOCUMENT_ID() {
		return DOCUMENT_ID;
	}

	public void setDOCUMENT_ID(Long dOCUMENT_ID) {
		DOCUMENT_ID = dOCUMENT_ID;
	}

	public Long getPOZYCJAID() {
		return POZYCJAID_ZAM;
	}

	public void setPOZYCJAID(Long pOZYCJAID) {
		POZYCJAID_ZAM = pOZYCJAID;
	}

	public Long getOKRES_BUDZETOWY() {
		return OKRES_BUDZETOWY_ZAM;
	}

	public void setOKRES_BUDZETOWY(Long oKRES_BUDZETOWY) {
		OKRES_BUDZETOWY_ZAM = oKRES_BUDZETOWY;
	}

	public Long getKOMORKA_BUDZETOWA() {
		return KOMORKA_BUDZETOWA_ZAM;
	}

	public void setKOMORKA_BUDZETOWA(Long kOMORKA_BUDZETOWA) {
		KOMORKA_BUDZETOWA_ZAM = kOMORKA_BUDZETOWA;
	}

	public Long getIDENTYFIKATOR_PLANU() {
		return IDENTYFIKATOR_PLANU_ZAM;
	}

	public void setIDENTYFIKATOR_PLANU(Long iDENTYFIKATOR_PLANU) {
		IDENTYFIKATOR_PLANU_ZAM = iDENTYFIKATOR_PLANU;
	}

	public Long getIDENTYFIKATOR_BUDZETU() {
		return IDENTYFIKATOR_BUDZETU_ZAM;
	}

	public void setIDENTYFIKATOR_BUDZETU(Long iDENTYFIKATOR_BUDZETU) {
		IDENTYFIKATOR_BUDZETU_ZAM = iDENTYFIKATOR_BUDZETU;
	}

	public String getPOZYCJA_PLANU_ZAKUPOW() {
		return POZYCJA_PLANU_ZAKUPOW_ZAM;
	}

	public void setPOZYCJA_PLANU_ZAKUPOW(String pOZYCJA_PLANU_ZAKUPOW) {
		POZYCJA_PLANU_ZAKUPOW_ZAM = pOZYCJA_PLANU_ZAKUPOW;
	}

	public String getPOZYCJA_BUDZETU() {
		return POZYCJA_BUDZETU_ZAM;
	}

	public void setPOZYCJA_BUDZETU(String pOZYCJA_BUDZETU) {
		POZYCJA_BUDZETU_ZAM = pOZYCJA_BUDZETU;
	}

	public Long getPROJEKT() {
		return PROJEKT_ZAM;
	}

	public void setPROJEKT(Long pROJEKT) {
		PROJEKT_ZAM = pROJEKT;
	}

	public String getOPIS() {
		return OPIS_ZAM;
	}

	public void setOPIS(String oPIS) {
		OPIS_ZAM = oPIS;
	}

	public BigDecimal getILOSC() {
		return ILOSC_ZAM;
	}

	public void setILOSC(BigDecimal iLOSC) {
		ILOSC_ZAM = iLOSC;
	}

	public BigDecimal getILOSC_ZREAL() {
		return ILOSC_ZREAL_ZAM;
	}

	public void setILOSC_ZREAL(BigDecimal iLOSC_ZREAL) {
		ILOSC_ZREAL_ZAM = iLOSC_ZREAL;
	}

	public Long getJM() {
		return JM_ZAM;
	}

	public void setJM(Long jM) {
		JM_ZAM = jM;
	}

	public Long getSTAWKA_VAT() {
		return STAWKA_VAT_ZAM;
	}

	public void setSTAWKA_VAT(Long sTAWKA_VAT) {
		STAWKA_VAT_ZAM = sTAWKA_VAT;
	}

	public BigDecimal getNETTO() {
		return NETTO_ZAM;
	}

	public void setNETTO(BigDecimal nETTO) {
		NETTO_ZAM = nETTO;
	}

	public BigDecimal getBRUTTO() {
		return BRUTTO_ZAM;
	}

	public void setBRUTTO(BigDecimal bRUTTO) {
		BRUTTO_ZAM = bRUTTO;
	}

	public String getUWAGI() {
		return UWAGI_ZAM;
	}

	public void setUWAGI(String uWAGI) {
		UWAGI_ZAM = uWAGI;
	}

	public Long getZAMPOZ_ID() {
		return ZAMPOZ_ID;
	}

	public void setZAMPOZ_ID(Long zAMPOZ_ID) {
		ZAMPOZ_ID = zAMPOZ_ID;
	}

	public Long getPRODUKT() {
		return PRODUKT_ZAM;
	}

	public void setPRODUKT(Long pRODUKT) {
		PRODUKT_ZAM = pRODUKT;
	}
	
	
	
	
}
