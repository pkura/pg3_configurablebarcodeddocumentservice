package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_role_in_budget")
public class RoleWBudzecieDB {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "bp_rola_id")
	private double bp_rola_id;
	
	@Column(name = "bp_rola_idn")
	private String bp_rola_idn;
	
	@Column(name = "bd_budzet_ko_id")
	private double bd_budzet_ko_id;
	
	@Column(name = "budzet_id")
	private double budzet_id;
	
	@Column(name = "bd_budzet_ko_idm")
	private String bd_budzet_ko_idm;
	
	@Column(name = "pracownik_id")
	private double pracownik_id;
	
	@Column(name = "nazwa")
	private String nazwa;
	
	@Column(name = "pracownik_nrewid")
	private int pracownik_nrewid;
	
	@Column(name = "osobaGuid")
	private String osobaGuid;
	
	@Column(name = "osoba_id")
	private Long osoba_id;
	
	@Column(name = "available")
	private Boolean available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getBp_rola_id() {
		return bp_rola_id;
	}

	public void setBp_rola_id(double bp_rola_id) {
		this.bp_rola_id = bp_rola_id;
	}

	public String getBp_rola_idn() {
		return bp_rola_idn;
	}

	public void setBp_rola_idn(String bp_rola_idn) {
		this.bp_rola_idn = bp_rola_idn;
	}

	public double getBd_budzet_ko_id() {
		return bd_budzet_ko_id;
	}

	public void setBd_budzet_ko_id(double bd_budzet_ko_id) {
		this.bd_budzet_ko_id = bd_budzet_ko_id;
	}

	public double getBudzet_id() {
		return budzet_id;
	}

	public void setBudzet_id(double budzet_id) {
		this.budzet_id = budzet_id;
	}

	public String getBd_budzet_ko_idm() {
		return bd_budzet_ko_idm;
	}

	public void setBd_budzet_ko_idm(String bd_budzet_ko_idm) {
		this.bd_budzet_ko_idm = bd_budzet_ko_idm;
	}

	public double getPracownik_id() {
		return pracownik_id;
	}

	public void setPracownik_id(double pracownik_id) {
		this.pracownik_id = pracownik_id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public int getPracownik_nrewid() {
		return pracownik_nrewid;
	}

	public void setPracownik_nrewid(int pracownik_nrewid) {
		this.pracownik_nrewid = pracownik_nrewid;
	}

	public String getOsobaGuid() {
		return osobaGuid;
	}

	public void setOsobaGuid(String osobaGuid) {
		this.osobaGuid = osobaGuid;
	}

	public Long getOsoba_id() {
		return osoba_id;
	}

	public void setOsoba_id(Long osoba_id) {
		this.osoba_id = osoba_id;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
}
