package pl.compan.docusafe.general.hibernate.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DSG_INVOICE_GENERAL_MPK_REAL")
public class RealizacjaBudzetuDB {
	@Id
	private Integer id;
	
	@Column(name="NAZWABUDZETU")
	private String nazwaBudzetu;
	
	@Column(name="NAZWAPOZYCJI")
	private String nazwaPozycji;
	
	@Column(name="PLANPOZYCJI")
	private String planPozycji;
	
	@Column(name="WYKONANIEPOZYCJI")
	private String wykonaniePozycji;
	
	@Column(name="POZOSTALO")
	private BigDecimal pozostalo;
	
	public RealizacjaBudzetuDB(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNazwaBudzetu() {
		return nazwaBudzetu;
	}

	public void setNazwaBudzetu(String nazwaBudzetu) {
		this.nazwaBudzetu = nazwaBudzetu;
	}

	public String getNazwaPozycji() {
		return nazwaPozycji;
	}

	public void setNazwaPozycji(String nazwaPozycji) {
		this.nazwaPozycji = nazwaPozycji;
	}

	public String getPlanPozycji() {
		return planPozycji;
	}

	public void setPlanPozycji(String planPozycji) {
		this.planPozycji = planPozycji;
	}

	public String getWykonaniePozycji() {
		return wykonaniePozycji;
	}

	public void setWykonaniePozycji(String wykonaniePozycji) {
		this.wykonaniePozycji = wykonaniePozycji;
	}

	public BigDecimal getPozostalo() {
		return pozostalo;
	}

	public void setPozostalo(BigDecimal pozostalo) {
		this.pozostalo = pozostalo;
	}
}
