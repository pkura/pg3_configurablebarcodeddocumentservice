package pl.compan.docusafe.general.hibernate.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_okresy_budzetowe")
public class OkresBudzetowyDB {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "czy_archiwalny")
	private boolean czy_archiwalny;
	
	@Column(name = "Data_obow_do")
	private Date Data_obow_do;
	
	@Column(name = "Data_obow_od")
	private Date Data_obow_od;
	
	@Column(name = "erpId")
	private Long erpId;
	
	@Column(name = "idm")
	private String idm;
	
	@Column(name = "nazwa")
	private String nazwa;

	@Column(name = "wersja")
	private int wersja;

	@Column(name = "available")
	private Boolean available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean getCzy_archiwalny() {
		return czy_archiwalny;
	}

	public void setCzy_archiwalny(boolean czy_archiwalny) {
		this.czy_archiwalny = czy_archiwalny;
	}

	public Date getData_obow_do() {
		return Data_obow_do;
	}

	public void setData_obow_do(Date data_obow_do) {
		Data_obow_do = data_obow_do;
	}

	public Date getData_obow_od() {
		return Data_obow_od;
	}

	public void setData_obow_od(Date data_obow_od) {
		Data_obow_od = data_obow_od;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public int getWersja() {
		return wersja;
	}

	public void setWersja(int wersja) {
		this.wersja = wersja;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
	
}
