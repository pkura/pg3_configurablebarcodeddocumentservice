package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_pracownicyERP")
public class PracownikDB {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "imie")
	private String imie;
	
	@Column(name = "nazwisko")
	private String nazwisko;
	
	@Column(name = "pracownik_idn")
	private String pracownik_idn;
	
	@Column(name = "pracownik_id")
	private Long pracownik_id;
	
	@Column(name = "pesel")
	private String pesel;
	
	@Column(name = "guid")
	private String guid;
	
	@Column(name = "nrewid")
	private int nrewid;
	
	@Column(name = "czy_aktywny_osoba")
	private boolean czy_aktywny_osoba;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public int getNrewid() {
		return nrewid;
	}

	public void setNrewid(int nrewid) {
		this.nrewid = nrewid;
	}

	public String getPracownik_idn() {
		return pracownik_idn;
	}

	public void setPracownik_idn(String pracownik_idn) {
		this.pracownik_idn = pracownik_idn;
	}

	public Long getPracownik_id() {
		return pracownik_id;
	}

	public void setPracownik_id(Long pracownik_id) {
		this.pracownik_id = pracownik_id;
	}

	public boolean isCzy_aktywny_osoba() {
		return czy_aktywny_osoba;
	}

	public void setCzy_aktywny_osoba(boolean czy_aktywny_osoba) {
		this.czy_aktywny_osoba = czy_aktywny_osoba;
	}
}
