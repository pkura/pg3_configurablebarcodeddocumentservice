package pl.compan.docusafe.general.hibernate.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.google.common.collect.Lists;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.general.ObjectUtils;


@Entity
@Table(name = "dsg_currency")

public class CurrencyDB{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "cn", nullable = false)
	private String cn;
	
	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "centrum", nullable = true)
	private Integer centrum;	

	@Column(name = "refValue", nullable = true)
	private String refValue;

	@Column(name = "available", nullable = true)
	private Integer available;

	public CurrencyDB() {
	}

	public static List<CurrencyDB> list() throws EdmException
	{
		return DSApi.context().session().createCriteria(CurrencyDB.class)
				.addOrder(Order.asc("title"))
				.list();	

	}

	public static CurrencyDB find(Long id) throws EdmException
	{
		CurrencyDB status = (CurrencyDB) DSApi.getObject(CurrencyDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(CurrencyDB.class, id);

		return status;
	}


	public static List<CurrencyDB> findByCn(String _cn) throws EdmException
	{
		return DSApi.context().session().createCriteria(CurrencyDB.class).add(Restrictions.eq("cn", _cn)).list();
	}

	public static List<CurrencyDB> findByCn(Collection<?> countryIdn, boolean notIn) throws EdmException
	{
		List<CurrencyDB> list = Lists.newArrayList();
		try{
			Criteria criteria = DSApi.context().session().createCriteria(CurrencyDB.class);

			if(countryIdn != null){
				if(notIn){
					criteria.add(Restrictions.not(Restrictions.in("cn", countryIdn)));
				} else {
					criteria.add(Restrictions.in("cn", countryIdn));
				}
			} else {
				return list;
			}

			list = criteria.list();

			return list;
		}catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public final void save() throws EdmException
	{
		try
		{
			DSApi.context().session().save(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}
	
	public final void update() throws EdmException
	{
		try
		{
			DSApi.context().session().update(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(CurrencyDB.class)+" set available='0'");
		ps.executeUpdate();
		
	}
}
