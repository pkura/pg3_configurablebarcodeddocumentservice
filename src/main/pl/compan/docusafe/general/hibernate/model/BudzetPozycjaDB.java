package pl.compan.docusafe.general.hibernate.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@Entity
@Table(name = "DSG_INVOICE_GENERAL_MPK")
public class BudzetPozycjaDB {

	private static final Logger log = LoggerFactory.getLogger(BudzetPozycjaDB.class);
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "OKRESBUDZET")
	private Long okresBudzetowy;

	/*@ManyToOne
	@JoinTable(
			name="dsg_invoice_general_budzet", 
			joinColumns={@JoinColumn(name="id", referencedColumnName="BUDZETID")},
			inverseJoinColumns={@JoinColumn(name="cn", referencedColumnName="id")})
	private BudgetViewDB budzetId;*/

	@Column(name="BUDZETID")
	private Long budzetId;

	@Column(name = "POZYCJAID")
	private Long pozycjaId;

	@Column(name = "PROJEKT")
	private Long projekt;

	@Column(name = "BUDZET_PROJ")
	private Long budzetProjektuId;

	@Column(name = "ETAP")
	private Long etapId;

	@Column(name = "ZASOB")
	private Long zasobId;

	@Column(name = "KWOTANETTO")
	private BigDecimal kwotaNetto;

	@Column(name = "KWOTAVAT")
	private BigDecimal kwotaVat;

	@Column(name = "DYSPONENT")
	private Long dysponent;

	@Column(name = "AKCEPTACJA")
	private Long akceptacja;

	@Column(name="STAWKA_BUDZET")
	private Long stawkaVat;
	
	/**
	 * 
	 * @param IdBudzetu
	 * @param documentId
	 * @return zwraca pierwszy pasujacy budzet(kazdej fakturze moze byc prztypisany tylko raz dany budzet)
	 * @throws EdmException
	 */
	public static BudzetPozycjaDB find(Long IdBudzetu,Long documentId) throws EdmException {
		try
		{

			List<FakturaMulti> multi=FakturaMulti.list(documentId,InvoiceGeneralConstans.MPK_CN);
			DSApi.context().session().flush();
			Criteria c = DSApi.context().session().createCriteria(BudzetPozycjaDB.class);
			
			c.add(Restrictions.eq("budzetId", IdBudzetu));
			
			for(Object db : c.list()){
				for(Object mult:multi){
					if(((FakturaMulti)mult).getFIELD_CN().equals(InvoiceGeneralConstans.MPK_CN)){
						if(((BudzetPozycjaDB)db).getId().compareTo(Long.parseLong(((FakturaMulti)mult).getFIELD_VAL()))==0){
							if(((FakturaMulti)mult).getDOCUMENT_ID().compareTo(documentId)==0){
								return (BudzetPozycjaDB)db;
							}
						}
					}
				}
			}

		} catch(Exception e){
			log.error("", e);
			return null;
		}
		return null;
	}
	
	public static void save(BudzetPozycjaDB budzetPozycjaDB){
		try {
			DSApi.context().session().save(budzetPozycjaDB);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public BudzetPozycjaDB() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOkresBudzetowy() {
		return okresBudzetowy;
	}

	public void setOkresBudzetowy(Long okresBudzetowy) {
		this.okresBudzetowy = okresBudzetowy;
	}
		public Long getBudzetId() {
		return budzetId;
	}

	public void setBudzetId(Long budzetId) {
		this.budzetId = budzetId;
	}

	public BigDecimal getKwotaNetto() {
		return kwotaNetto;
	}

	public void setKwotaNetto(BigDecimal kwotaNetto) {
		this.kwotaNetto = kwotaNetto;
	}

	public BigDecimal getKwotaVat() {
		return kwotaVat;
	}

	public void setKwotaVat(BigDecimal kwotaVat) {
		this.kwotaVat = kwotaVat;
	}

	public Long getProjekt() {
		return projekt;
	}

	public void setProjekt(Long projekt) {
		this.projekt = projekt;
	}

	public Long getDysponent() {
		return dysponent;
	}

	public void setDysponent(Long dysponent) {
		this.dysponent = dysponent;
	}

	public Long getAkceptacja() {
		return akceptacja;
	}

	public void setAkceptacja(Long akceptacja) {
		this.akceptacja = akceptacja;
	}

	public Long getBudzetProjektuId() {
		return budzetProjektuId;
	}

	public void setBudzetProjektuId(Long budzetProjektuId) {
		this.budzetProjektuId = budzetProjektuId;
	}

	public Long getEtapId() {
		return etapId;
	}

	public void setEtapId(Long etapId) {
		this.etapId = etapId;
	}

	public Long getZasobId() {
		return zasobId;
	}

	public void setZasobId(Long zasobId) {
		this.zasobId = zasobId;
	}

	public Long getPozycjaId() {
		return pozycjaId;
	}

	public void setPozycjaId(Long pozycjaId) {
		this.pozycjaId = pozycjaId;
	}

	public Long getStawkaVat() {
		return stawkaVat;
	}

	public void setStawkaVat(Long stawkaVat) {
		this.stawkaVat = stawkaVat;
	}

}
