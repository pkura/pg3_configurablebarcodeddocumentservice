package pl.compan.docusafe.general.hibernate.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dsg_invoice_supplier_order_position")
public class ZamowienieSimplePozycjeDB {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name="supplier_order_id")
	private Long supplier_order_id;
	
	@Column(name="supplier_order_idm")
	private String supplier_order_idm;
	
	@Column(name="wytwor_idm")
	private String wytwor_idm;
	
	@Column(name="ilosc")
	private BigDecimal ilosc;
	
	@Column(name="nrpoz")
	private Integer nrpoz;
	
	@Column(name="cena")
	private BigDecimal cena;
	
	@Column(name="cenabrutto")
	private BigDecimal cenabrutto;
	
	@Column(name="koszt")
	private BigDecimal koszt;
	
	@Column(name="kwotnett")
	private BigDecimal kwotnett;
	
	@Column(name="kwotvat")
	private BigDecimal kwotvat;
	
	@Column(name="zamdospoz_id")
	private Long zamdospoz_id;
	
	@Column(name="budzet_Id")
	private Long budzetId;
	
	@Column(name="budzet_Idm")
	private String budzetIdm;
	
	@Column(name="bd_budzet_ko_id")
	private Long bdBudzetKoId;
	
	@Column(name="bd_Budzet_Ko_Idm")
	private String bdBudzetKoIdm;
	
	@Column(name="bd_Rodzaj_Ko_Id")
	private Long bdRodzajKoId;
	
	@Column(name="bd_Plan_Zam_Id")
	private Long bdPlanZamId;
	
	@Column(name="bd_Plan_Zam_Idm")
	private String bdPlanZamIdm;
	
	@Column(name="bd_Rodzaj_Plan_Zam_Id")
	private Long bdRodzajPlanZamId;
	
	@Column(name="bd_Rezerwacja_Id")
	private Long bdRezerwacjaId;
	
	@Column(name="bd_Rezerwacja_Idm")
	private String bdRezerwacjaIdm;
	
	@Column(name="bd_Rezerwacja_Poz_Id")
	private Long bdRezerwacjaPozId;
	
	@Column(name="kontrakt_Id")
	private Long kontraktId;
	
	@Column(name="projekt_Id")
	private Long projektId;
	
	@Column(name="etap_Id")
	private Long etapId;
	
	@Column(name="zasob_Id")
	private Long zasobId;

	@Column(name="vatStawId")
	private Long vatStawId;
	
	@Column(name="walutaId")
	private Long walutaId;
	
	@Column(name="iloscZreal")
	private BigDecimal iloscZreal;
	
	@Column(name="komorkaOrganizacyjaERPId")
	private Long komorkaOrganizacyjaERPId;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSupplier_order_id() {
		return supplier_order_id;
	}

	public void setSupplier_order_id(Long supplier_order_id) {
		this.supplier_order_id = supplier_order_id;
	}

	public String getSupplier_order_idm() {
		return supplier_order_idm;
	}

	public void setSupplier_order_idm(String supplier_order_idm) {
		this.supplier_order_idm = supplier_order_idm;
	}

	public String getWytwor_idm() {
		return wytwor_idm;
	}

	public void setWytwor_idm(String wytwor_idm) {
		this.wytwor_idm = wytwor_idm;
	}

	public BigDecimal getIlosc() {
		return ilosc;
	}

	public void setIlosc(BigDecimal ilosc) {
		this.ilosc = ilosc;
	}

	public Integer getNrpoz() {
		return nrpoz;
	}

	public void setNrpoz(Integer nrpoz) {
		this.nrpoz = nrpoz;
	}

	public BigDecimal getCena() {
		return cena;
	}

	public void setCena(BigDecimal cena) {
		this.cena = cena;
	}

	public BigDecimal getCenabrutto() {
		return cenabrutto;
	}

	public void setCenabrutto(BigDecimal cenabrutto) {
		this.cenabrutto = cenabrutto;
	}

	public BigDecimal getKoszt() {
		return koszt;
	}

	public void setKoszt(BigDecimal koszt) {
		this.koszt = koszt;
	}

	public BigDecimal getKwotnett() {
		return kwotnett;
	}

	public void setKwotnett(BigDecimal kwotnett) {
		this.kwotnett = kwotnett;
	}

	public BigDecimal getKwotvat() {
		return kwotvat;
	}

	public void setKwotvat(BigDecimal kwotvat) {
		this.kwotvat = kwotvat;
	}

	public Long getZamdospoz_id() {
		return zamdospoz_id;
	}

	public void setZamdospoz_id(Long zamdospoz_id) {
		this.zamdospoz_id = zamdospoz_id;
	}

	public Long getBudzetId() {
		return budzetId;
	}

	public void setBudzetId(Long budzetId) {
		this.budzetId = budzetId;
	}

	public String getBudzetIdm() {
		return budzetIdm;
	}

	public void setBudzetIdm(String budzetIdm) {
		this.budzetIdm = budzetIdm;
	}

	public Long getBdBudzetKoId() {
		return bdBudzetKoId;
	}

	public void setBdBudzetKoId(Long bdBudzetKoId) {
		this.bdBudzetKoId = bdBudzetKoId;
	}

	public String getBdBudzetKoIdm() {
		return bdBudzetKoIdm;
	}

	public void setBdBudzetKoIdm(String bdBudzetKoIdm) {
		this.bdBudzetKoIdm = bdBudzetKoIdm;
	}

	public Long getBdRodzajKoId() {
		return bdRodzajKoId;
	}

	public void setBdRodzajKoId(Long bdRodzajKoId) {
		this.bdRodzajKoId = bdRodzajKoId;
	}

	public Long getBdPlanZamId() {
		return bdPlanZamId;
	}

	public void setBdPlanZamId(Long bdPlanZamId) {
		this.bdPlanZamId = bdPlanZamId;
	}

	public String getBdPlanZamIdm() {
		return bdPlanZamIdm;
	}

	public void setBdPlanZamIdm(String bdPlanZamIdm) {
		this.bdPlanZamIdm = bdPlanZamIdm;
	}

	public Long getBdRodzajPlanZamId() {
		return bdRodzajPlanZamId;
	}

	public void setBdRodzajPlanZamId(Long bdRodzajPlanZamId) {
		this.bdRodzajPlanZamId = bdRodzajPlanZamId;
	}

	public Long getBdRezerwacjaId() {
		return bdRezerwacjaId;
	}

	public void setBdRezerwacjaId(Long bdRezerwacjaId) {
		this.bdRezerwacjaId = bdRezerwacjaId;
	}

	public String getBdRezerwacjaIdm() {
		return bdRezerwacjaIdm;
	}

	public void setBdRezerwacjaIdm(String bdRezerwacjaIdm) {
		this.bdRezerwacjaIdm = bdRezerwacjaIdm;
	}

	public Long getBdRezerwacjaPozId() {
		return bdRezerwacjaPozId;
	}

	public void setBdRezerwacjaPozId(Long bdRezerwacjaPozId) {
		this.bdRezerwacjaPozId = bdRezerwacjaPozId;
	}

	public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	public Long getProjektId() {
		return projektId;
	}

	public void setProjektId(Long projektId) {
		this.projektId = projektId;
	}

	public Long getEtapId() {
		return etapId;
	}

	public void setEtapId(Long etapId) {
		this.etapId = etapId;
	}

	public Long getZasobId() {
		return zasobId;
	}

	public void setZasobId(Long zasobId) {
		this.zasobId = zasobId;
	}

	public Long getVatStawId() {
		return vatStawId;
	}

	public void setVatStawId(Long vatStawId) {
		this.vatStawId = vatStawId;
	}

	public Long getWalutaId() {
		return walutaId;
	}

	public void setWalutaId(Long walutaId) {
		this.walutaId = walutaId;
	}

	public BigDecimal getIloscZreal() {
		return iloscZreal;
	}

	public void setIloscZreal(BigDecimal iloscZreal) {
		this.iloscZreal = iloscZreal;
	}

	public Long getKomorkaOrganizacyjaERPId() {
		return komorkaOrganizacyjaERPId;
	}

	public void setKomorkaOrganizacyjaERPId(Long komorkaOrganizacyjaERPId) {
		this.komorkaOrganizacyjaERPId = komorkaOrganizacyjaERPId;
	}
	
	
	
}
