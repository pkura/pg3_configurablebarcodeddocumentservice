package pl.compan.docusafe.general.hibernate.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.validator.Validator;


@Entity
@Table(name = "DSG_KONTRAHENT")
public class KontrahentDB {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "imie", nullable = true)
	private String imie;

	@Column(name = "nazwisko", nullable = true)
	private String nazwisko;

	@Column(name = "nazwa", nullable = true)
	private String nazwa;

	@Column(name = "ulica", nullable = true)
	private String ulica;
	
	@Column(name = "nrDomu", nullable = true)
	private String nrDomu;
	
	@Column(name = "nrMieszkania", nullable = true)
	private String nrMieszkania;

	@Column(name = "kodpocztowy", nullable = true)
	private String kodpocztowy;

	@Column(name = "miejscowosc", nullable = true)
	private String miejscowosc;

	@Column(name = "nip", nullable = true)
	private String nip;

	@Column(name = "regon", nullable = true)
	private String regon;

	@Column(name = "kraj", nullable = true)
	private String kraj;

	@Column(name = "email", nullable = true)
	private String email;

	@Column(name = "faks", nullable = true)
	private String faks;

	@Column(name = "telefon", nullable = true)
	private String telefon;

	@Column(name = "nrKonta", nullable = true)
	private String nrKonta;
	
	@Column(name = "erpId", nullable = false)
	private Long erpId;

	@Column(name = "nowy", nullable = true)
	private Boolean nowy;
	
	public KontrahentDB() {
	}

	/**
	 * Metoda wyszukuje kontrahenta po danym numerze NIP.
	 * 
	 * @param nip
	 * @return
	 * @throws EdmException
	 */
	public static List<KontrahentDB> findByNip(String nip) throws EdmException {
		return DSApi.context().session().createCriteria(KontrahentDB.class)
				.add(Restrictions.like("nip", "%" + nip + "%").ignoreCase())
				.list();
	}
	
	public static List<KontrahentDB> findByErpId(Long id) throws EdmException {
		return 	DSApi.context().session().createCriteria(KontrahentDB.class).add(Restrictions.eq("erpId", id)).list();
	
	}
	public static KontrahentDB findByErpIdUnique(Long id) throws EdmException {
		return 	(KontrahentDB) DSApi.context().session().createCriteria(KontrahentDB.class).add(Restrictions.eq("erpId", id)).uniqueResult();
	
	}
	
	public static KontrahentDB findByIdUnique(Long id) throws EdmException {
		return 	(KontrahentDB) DSApi.context().session().createCriteria(KontrahentDB.class).add(Restrictions.eq("id", id)).uniqueResult();
	}
	public static List<KontrahentDB> findByNrKonta(String nrKonta) throws EdmException {
		return 	DSApi.context().session().createCriteria(KontrahentDB.class).add(Restrictions.eq("nrKonta", nrKonta)).list();
	
	}

	public static List<KontrahentDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(KontrahentDB.class)
				.addOrder(Order.asc("nazwisko")).addOrder(Order.asc("imie"))
				.addOrder(Order.asc("nazwa")).list();

	}

	public static KontrahentDB find(Long id) throws EdmException {
		KontrahentDB status = (KontrahentDB) DSApi.getObject(KontrahentDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(KontrahentDB.class, id);

		return status;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof KontrahentDB))
			return false;

		final KontrahentDB doc = (KontrahentDB) o;

		if (id != null ? !(id.equals(doc.id)) : doc.id != null)
			return false;

		return true;
	}

	public int hashCode() {
		return (id != null ? id.hashCode() : 0);
	}

	public final void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(KontrahentDB kontrahentDB) throws EdmException {
		try {
			DSApi.context().session().merge(kontrahentDB);
			DSApi.context().session().clear();
			DSApi.context().session().update(kontrahentDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	private boolean validate() throws EdmException {
		if (!Validator.isValidNip(nip)) {
			throw new EdmException("Nieprawid�owy NIP. Sprawd� poprawno��.");
		} else if (!Validator.isValidIban(nrKonta)) {
			throw new EdmException(
					"Nieprawid�owy numer konta. Sprawd� poprawno��.");
		} else if (!Validator.isValidZip(kodpocztowy)) {
			throw new EdmException(
					"Nieprawid�owy kod pocztowy. Sprawd� poprawno��.");
		}

		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getUlica() {
		return ulica;
	}
	
	public String getNrDomu() {
		return nrDomu;
	}
	
	public void setNrDomu(String nrDomu) {
		this.nrDomu=nrDomu;
	}
	
	public String getNrMieszkania() {
		return nrMieszkania;
	}
	
	public void setNrMieszkania(String nrMieszkania) {
		this.nrMieszkania=nrMieszkania;
	}
	
	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getMiejscowosc() {
		return miejscowosc;
	}

	public void setMiejscowosc(String miejscowosc) {
		this.miejscowosc = miejscowosc;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getRegon() {
		return regon;
	}

	public void setRegon(String regon) {
		this.regon = regon;
	}

	public String getKraj() {
		return kraj;
	}

	public void setKraj(String kraj) {
		this.kraj = kraj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFaks() {
		return faks;
	}

	public void setFaks(String faks) {
		this.faks = faks;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getKodpocztowy() {
		return kodpocztowy;
	}

	public void setKodpocztowy(String kodpocztowy) {
		this.kodpocztowy = kodpocztowy;
	}

	public void setNrKonta(String nrKonta) {
		this.nrKonta = nrKonta;
	}

	public String getNrKonta() {
		return nrKonta;
	}
	
	public Long getErpId() {
		return erpId;
	}
	public void setErpIid(Long erpId) {
		this.erpId=erpId;
	}
	
	public Boolean getNowy() {
		return nowy;
	}
	
	public void setNowy(Boolean nowy) {
		this.nowy=nowy;
	}


}