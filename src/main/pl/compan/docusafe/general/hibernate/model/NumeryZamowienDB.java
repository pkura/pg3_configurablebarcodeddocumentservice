package pl.compan.docusafe.general.hibernate.model;

import java.math.BigDecimal;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_invoice_supplier_order_header")
public class NumeryZamowienDB {

	
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "dat_dok")
	private Date dat_dok;
	
	@Column(name = "dat_odb")
	private Date dat_odb;
	
	@Column(name = "dostawca_id")
	private Long dostawca_id;
	
	@Column(name = "dostawca_idn")
	private String dostawca_idn;
	
	@Column(name = "erpId")
	private Long erpId;
	
	@Column(name = "idm")
	private String idm;
	
	@Column(name = "opis_wewn")
	private String opis_wewn;
	
	@Column(name = "uwagi")
	private String uwagi;
	
	@Column(name = "wartdok")
	private BigDecimal wartdok;
	
	@Column(name="available")
	private Boolean available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDat_dok() {
		return dat_dok;
	}

	public void setDat_dok(Date dat_dok) {
		this.dat_dok = dat_dok;
	}

	public Date getDat_odb() {
		return dat_odb;
	}

	public void setDat_odb(Date dat_odb) {
		this.dat_odb = dat_odb;
	}

	public Long getDostawca_id() {
		return dostawca_id;
	}

	public void setDostawca_id(Long dostawca_id) {
		this.dostawca_id = dostawca_id;
	}

	public String getDostawca_idn() {
		return dostawca_idn;
	}

	public void setDostawca_idn(String dostawca_idn) {
		this.dostawca_idn = dostawca_idn;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getOpis_wewn() {
		return opis_wewn;
	}

	public void setOpis_wewn(String opis_wewn) {
		this.opis_wewn = opis_wewn;
	}

	public String getUwagi() {
		return uwagi;
	}

	public void setUwagi(String uwagi) {
		this.uwagi = uwagi;
	}

	public BigDecimal getWartdok() {
		return wartdok;
	}

	public void setWartdok(BigDecimal wartdok) {
		this.wartdok = wartdok;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}


	
	
	
	
}
