package pl.compan.docusafe.general.hibernate.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

@Entity
@Table(name = "dsg_vat_rate")

public class VatRateDB{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "cn", nullable = false)
	private String cn;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "centrum", nullable = true)
	private Integer centrum;	

	@Column(name = "refValue", nullable = true)
	private String refValue;

	@Column(name = "available", nullable = true)
	private Integer available;

	@Column(name = "czy_zwolniona", nullable = true)
	private Boolean czy_zwolniona;

	@Column(name = "nie_podlega", nullable = true)
	private Boolean nie_podlega;

	@Column(name = "erp_id", nullable = true)
	private Long erp_id;
	
	@Column(name = "idm", nullable = true)
	private String idm;

	public static List<VatRateDB> list() throws EdmException
	{
		return DSApi.context().session().createCriteria(VatRateDB.class)
				.addOrder(Order.asc("title"))
				.list();	
	}

	public static VatRateDB find(Long id) throws EdmException
	{
		VatRateDB status = (VatRateDB) DSApi.getObject(VatRateDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(VatRateDB.class, id);

		return status;
	}

	public static List<VatRateDB> findByErpID(Long id) throws EdmException
	{
		return DSApi.context().session().createCriteria(VatRateDB.class).add(Restrictions.eq("erp_id", id)).list();
	}

	public static VatRateDB findByErpIDUnique(Long id) throws EdmException
	{
		return (VatRateDB) DSApi.context().session().createCriteria(VatRateDB.class).add(Restrictions.eq("erp_id", id)).uniqueResult();
	}
	public final void save() throws EdmException
	{
		try
		{
			DSApi.context().session().saveOrUpdate(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public VatRateDB() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public Boolean getCzy_zwolniona() {
		return czy_zwolniona;
	}

	public void setCzy_zwolniona(Boolean czy_zwolniona) {
		this.czy_zwolniona = czy_zwolniona;
	}

	public Boolean getNie_podlega() {
		return nie_podlega;
	}

	public void setNie_podlega(Boolean nie_podlega) {
		this.nie_podlega = nie_podlega;
	}

	public Long getErp_id() {
		return erp_id;
	}

	public void setErp_id(Long erp_id) {
		this.erp_id = erp_id;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(VatRateDB.class)+" set available='0'");
		ps.executeUpdate();
	}
}
