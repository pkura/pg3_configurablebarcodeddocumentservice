package pl.compan.docusafe.general.hibernate.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;

	@Entity
	@Table(name = InvoiceGeneralConstans.TABELA_WNIOSKI_O_REZERWACJE_POZYCJE)
	public class WniosekORezerwacjePozycjaDB {
		
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		@Column(name = "id")
		private Long id;
		@Column(name="rezerwacja_okres_budzet")
		private Long rezerwacja_okres_budzet;
		@Column(name="rezerwacja_budzetID")
		private Long rezerwacja_budzetID;
		@Column(name="rezerwacja_id")
		private Long rezerwacjaId;
		@Column(name = "pozycje_rez_rezerwacja_idm", length=100)
		private String pozycje_rez_rezerwacja_idm;
		@Column(name = "nr_poz_rez")
		private Integer nrPozRez;
		@Column(name = "wytwor_rez", length=100)
		private String wytworRez;
		@Column(name = "wytwor_nazwa_rez", length=150)
		private String wytworNazwaRez;
		@Column(name = "ilosc_rez")
		private BigDecimal iloscRez;
		@Column(name = "cena_rez")
		private BigDecimal cenaRez;
		@Column(name = "cena_brutto_rez")
		private BigDecimal cenaBruttoRez;
		@Column(name = "koszt_rez")
		private BigDecimal kosztRez;
		@Column(name = "kwota_netto_rez")
		private BigDecimal kwotaNettoRez;
		@Column(name = "kwota_vat_rez")
		private BigDecimal kwotaVatRez;
		@Column(name="rezerwacja_stawka")
		private Long stawkaVat;
		@Column(name = "KOSZT_REZ_BRUTTO")
		private BigDecimal kosztRezBrutto;
		@Column(name = "document_id")
		private Long document_id;
		public WniosekORezerwacjePozycjaDB(){}
		@Column(name="ZRODLO_WNIOSEK")
		private Long zrodloEprId;

		public WniosekORezerwacjePozycjaDB(Long rezerwacja_okres_budzet,
				Long rezerwacja_budzetID,Long rezerwacjaId, String pozycje_rez_rezerwacja_idm,
				Integer nrPozRez, String wytworRez, String wytworNazwaRez,
				BigDecimal iloscRez, BigDecimal cenaRez,
				BigDecimal cenaBruttoRez, BigDecimal kosztRez,
				BigDecimal kwotaNettoRez, BigDecimal kwotaVatRez,
				Long stawkaVat, BigDecimal kwotaVatRezBrutto,Long documentId, Long zrodlo) {
			super();
			this.rezerwacja_okres_budzet = rezerwacja_okres_budzet;
			this.rezerwacja_budzetID = rezerwacja_budzetID;
			this.rezerwacjaId=rezerwacjaId;
			this.pozycje_rez_rezerwacja_idm = pozycje_rez_rezerwacja_idm;
			this.nrPozRez = nrPozRez;
			this.wytworRez = wytworRez;
			this.wytworNazwaRez = wytworNazwaRez;
			this.iloscRez = iloscRez;
			this.cenaRez = cenaRez;
			this.cenaBruttoRez = cenaBruttoRez;
			this.kosztRez = kosztRez;
			this.kwotaNettoRez = kwotaNettoRez;
			this.kwotaVatRez = kwotaVatRez;
			this.stawkaVat = stawkaVat;
			this.kosztRezBrutto = kwotaVatRezBrutto;
			this.document_id=documentId;
			this.zrodloEprId=zrodlo;
		}





		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Integer getNrPozRez() {
			return nrPozRez;
		}
		public void setNrPozRez(Integer nrPozRez) {
			this.nrPozRez = nrPozRez;
		}
		public String getWytworRez() {
			return wytworRez;
		}
		public void setWytworRez(String wytworRez) {
			this.wytworRez = wytworRez;
		}
		public String getWytworNazwaRez() {
			return wytworNazwaRez;
		}
		public void setWytworNazwaRez(String wytworNazwaRez) {
			this.wytworNazwaRez = wytworNazwaRez;
		}
		public BigDecimal getIloscRez() {
			return iloscRez;
		}
		public void setIloscRez(BigDecimal iloscRez) {
			this.iloscRez = iloscRez;
		}
		public BigDecimal getCenaRez() {
			return cenaRez;
		}
		public void setCenaRez(BigDecimal cenaRez) {
			this.cenaRez = cenaRez;
		}
		public BigDecimal getCenaBruttoRez() {
			return cenaBruttoRez;
		}
		public void setCenaBruttoRez(BigDecimal cenaBruttoRez) {
			this.cenaBruttoRez = cenaBruttoRez;
		}
		public BigDecimal getKosztRez() {
			return kosztRez;
		}
		public void setKosztRez(BigDecimal kosztRez) {
			this.kosztRez = kosztRez;
		}
		public BigDecimal getKwotaNettoRez() {
			return kwotaNettoRez;
		}
		public void setKwotaNettoRez(BigDecimal kwotaNettoRez) {
			this.kwotaNettoRez = kwotaNettoRez;
		}
		public BigDecimal getKwotaVatRez() {
			return kwotaVatRez;
		}
		public void setKwotaVatRez(BigDecimal kwotaVatRez) {
			this.kwotaVatRez = kwotaVatRez;
		}

		public Long getRezerwacja_okres_budzet() {
			return rezerwacja_okres_budzet;
		}


		public void setRezerwacja_okres_budzet(Long rezerwacja_okres_budzet) {
			this.rezerwacja_okres_budzet = rezerwacja_okres_budzet;
		}


		public Long getRezerwacja_budzetID() {
			return rezerwacja_budzetID;
		}


		public void setRezerwacja_budzetID(Long rezerwacja_budzetID) {
			this.rezerwacja_budzetID = rezerwacja_budzetID;
		}

		public Long getStawkaVat() {
			return stawkaVat;
		}


		public void setStawkaVat(Long stawkaVat) {
			this.stawkaVat = stawkaVat;
		}

		public Long getRezerwacjaId() {
			return rezerwacjaId;
		}


		public void setRezerwacjaId(Long rezerwacjaId) {
			this.rezerwacjaId = rezerwacjaId;
		}


		public BigDecimal getKosztRezBrutto() {
			return kosztRezBrutto;
		}


		public void setKosztRezBrutto(BigDecimal kosztRezBrutto) {
			this.kosztRezBrutto = kosztRezBrutto;
		}


		public String getPozycje_rez_rezerwacja_idm() {
			return pozycje_rez_rezerwacja_idm;
		}


		public void setPozycje_rez_rezerwacja_idm(String pozycje_rez_rezerwacja_idm) {
			this.pozycje_rez_rezerwacja_idm = pozycje_rez_rezerwacja_idm;
		}


		public Long getDocument_id() {
			return document_id;
		}


		public void setDocument_id(Long document_id) {
			this.document_id = document_id;
		}

		public Long getZrodloEprId() {
			return zrodloEprId;
		}

		public void setZrodloEprId(Long zrodloEprId) {
			this.zrodloEprId = zrodloEprId;
		}




		
	}


