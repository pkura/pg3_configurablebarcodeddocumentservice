package pl.compan.docusafe.general.hibernate.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_zrod_finan_proj")
public class SourcesOfProjectFundingDB implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "refValue", nullable = true)
	private String refValue;

	@Column(name = "available", nullable = true)
	private Boolean available;

	@Column(name = "kontrakt_id", nullable = true)
	private Double kontrakt_id;

	@Column(name = "erpId", nullable = true)
	private Long erpId;

	@Column(name = "kontrakt_idm", nullable = true)
	private String kontrakt_idm;

	@Column(name = "rodzaj", nullable = true)
	private Integer rodzaj;

	@Column(name = "wsp_proc", nullable = true)
	private Double wsp_proc;

	@Column(name = "zrodlo_id", nullable = true)
	private Double zrodlo_id;

	@Column(name = "zrodlo_identyfikator_num", nullable = true)
	private Double zrodlo_identyfikator_num;

	@Column(name = "zrodlo_idn", nullable = true)
	private String zrodlo_idn;

	@Column(name = "zrodlo_nazwa", nullable = true)
	private String zrodlo_nazwa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public Double getKontrakt_id() {
		return kontrakt_id;
	}

	public void setKontrakt_id(Double kontrakt_id) {
		this.kontrakt_id = kontrakt_id;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public String getKontrakt_idm() {
		return kontrakt_idm;
	}

	public void setKontrakt_idm(String kontrakt_idm) {
		this.kontrakt_idm = kontrakt_idm;
	}

	public Integer getRodzaj() {
		return rodzaj;
	}

	public void setRodzaj(Integer rodzaj) {
		this.rodzaj = rodzaj;
	}

	public Double getWsp_proc() {
		return wsp_proc;
	}

	public void setWsp_proc(Double wsp_proc) {
		this.wsp_proc = wsp_proc;
	}

	public Double getZrodlo_id() {
		return zrodlo_id;
	}

	public void setZrodlo_id(Double zrodlo_id) {
		this.zrodlo_id = zrodlo_id;
	}

	public Double getZrodlo_identyfikator_num() {
		return zrodlo_identyfikator_num;
	}

	public void setZrodlo_identyfikator_num(Double zrodlo_identyfikator_num) {
		this.zrodlo_identyfikator_num = zrodlo_identyfikator_num;
	}

	public String getZrodlo_idn() {
		return zrodlo_idn;
	}

	public void setZrodlo_idn(String zrodlo_idn) {
		this.zrodlo_idn = zrodlo_idn;
	}

	public String getZrodlo_nazwa() {
		return zrodlo_nazwa;
	}

	public void setZrodlo_nazwa(String zrodlo_nazwa) {
		this.zrodlo_nazwa = zrodlo_nazwa;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
}
