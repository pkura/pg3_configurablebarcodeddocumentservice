package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dsg_przez_zakup")
public class PrzeznaczenieZakupuDB{

	@Id
	private Long id;
	
	private String cn;
	
	private String title;
	
	public PrzeznaczenieZakupuDB(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}
