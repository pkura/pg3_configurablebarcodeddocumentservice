package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_zrod_finan")
public class ZrodloDB {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable=false)
	private Long id;
	
	@Column(name = "erpId")
	private Long erpId;
	
	@Column(name = "identyfikator_num")
	private Double identyfikator_num;
	
	@Column(name = "idm")
	private String idm;
	
	@Column(name = "zrodlo_nazwa")
	private String zrodlo_nazwa;
	
	@Column(name = "available")
	private boolean available;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public Double getIdentyfikator_num() {
		return identyfikator_num;
	}

	public void setIdentyfikator_num(Double identyfikator_num) {
		this.identyfikator_num = identyfikator_num;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getZrodlo_nazwa() {
		return zrodlo_nazwa;
	}

	public void setZrodlo_nazwa(String zrodlo_nazwa) {
		this.zrodlo_nazwa = zrodlo_nazwa;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
}
