package pl.compan.docusafe.general.hibernate.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

@Entity
@Table(name = "dsg_INVOICE_GENERAL_WYD_STR_POZ")
public class WydatekStrukturalnyDB implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "WYDATEK_STRUKT", nullable = true)
	private Long wydatekStrukt;

	@Column(name = "KWOTA_BRUTTO_WYD", nullable = true)
	private BigDecimal kwotaBrutto;
	
	@Column(name = "KOD", nullable = true)
	private Long kod;
	
	@Column(name = "OBSZAR", nullable = true)
	private Long obszar;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getWydatekStrukt() {
		return wydatekStrukt;
	}

	public void setWydatekStrukt(Long wydatekStrukt) {
		this.wydatekStrukt = wydatekStrukt;
	}

	public BigDecimal getKwotaBrutto() {
		return kwotaBrutto;
	}

	public void setKwotaBrutto(BigDecimal kwotaBrutto) {
		this.kwotaBrutto = kwotaBrutto;
	}
	
	public Long getKod() {
		return kod;
	}

	public void setKod(Long kod) {
		this.kod = kod;
	}

	public Long getObszar() {
		return obszar;
	}

	public void setObszar(Long obszar) {
		this.obszar = obszar;
	}

	public static WydatekStrukturalnyDB find(Long id) throws EdmException
	{
		WydatekStrukturalnyDB db = (WydatekStrukturalnyDB) DSApi.getObject(WydatekStrukturalnyDB.class, id);

		if (db == null)
			throw new EntityNotFoundException(PurchaseDocumentTypeDB.class, id);

		return db;
	}
	
}
