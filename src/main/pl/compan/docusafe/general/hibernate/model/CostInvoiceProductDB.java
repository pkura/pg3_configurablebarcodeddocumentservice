package pl.compan.docusafe.general.hibernate.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

@Entity
@Table(name = "dsg_product")
public class CostInvoiceProductDB {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "cn", nullable = false)
	private String cn;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "available", nullable = true)
	private Integer available;

	@Column(name = "wytworId", nullable = true)
	private Long wytworId;

	@Column(name = "jmIdn", nullable = true)
	private String jmIdn;

	@Column(name = "rodzTowaru", nullable = false)
	private Integer rodzTowaru;

	@Column(name = "klasyWytwIdn", nullable = false)
	private String klasyWytwIdn;

	@Column(name = "vatStawIds", nullable = false)
	private String vatStawIds;

	public CostInvoiceProductDB() {
	}

	public static List<CostInvoiceProductDB> list() throws EdmException
	{
		return DSApi.context().session().createCriteria(CostInvoiceProductDB.class)
				.addOrder(Order.asc("title"))
				.list();	
	}

	public static CostInvoiceProductDB find(Long id) throws EdmException
	{
		CostInvoiceProductDB status = (CostInvoiceProductDB) DSApi.getObject(CostInvoiceProductDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(CostInvoiceProductDB.class, id);

		return status;
	}
	/**
	 * Wyszukuje po id produktu z erpa
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public static List<CostInvoiceProductDB> findByERPID(Long id) throws EdmException
	{
		return DSApi.context().session().createCriteria(CostInvoiceProductDB.class).add(Restrictions.eq("wytworId", id)).list();
	}
	public static CostInvoiceProductDB findByCN(String cn) throws EdmException
	{
		return (CostInvoiceProductDB) DSApi.context().session().createCriteria(CostInvoiceProductDB.class).add(Restrictions.eq("cn", cn)).uniqueResult();
	}
	public static CostInvoiceProductDB findByERPIDUnique(Long id) throws EdmException
	{
		return (CostInvoiceProductDB) DSApi.context().session().createCriteria(CostInvoiceProductDB.class).add(Restrictions.eq("wytworId", id)).uniqueResult();
	}
	public static List<CostInvoiceProductDB> findByProductIDM(String id) throws EdmException
	{
		return DSApi.context().session().createCriteria(CostInvoiceProductDB.class).add(Restrictions.eq("cn", id)).list();
	}
	
	public final void save() throws EdmException
	{
		try
		{
			DSApi.context().session().save(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public Long getWytworId() {
		return wytworId;
	}

	public void setWytworId(Long wytworId) {
		this.wytworId = wytworId;
	}

	public String getJmIdn() {
		return jmIdn;
	}

	public void setJmIdn(String jmIdn) {
		this.jmIdn = jmIdn;
	}

	public Integer getRodzTowaru() {
		return rodzTowaru;
	}

	public void setRodzTowaru(Integer rodzTowaru) {
		this.rodzTowaru = rodzTowaru;
	}

	public String getKlasyWytwIdn() {
		return klasyWytwIdn;
	}

	public void setKlasyWytwIdn(String klasyWytwIdn) {
		this.klasyWytwIdn = klasyWytwIdn;
	}

	public String getVatStawIds() {
		return vatStawIds;
	}

	public void setVatStawIds(String vatStawIds) {
		this.vatStawIds = vatStawIds;
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(CostInvoiceProductDB.class)+" set available='0'");
		ps.executeUpdate();
	}

}

