package pl.compan.docusafe.general.hibernate.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.FilterJoinTable;
import org.hibernate.annotations.WhereJoinTable;

import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.core.users.sql.UserImpl;

@Entity
@Table(name="dsg_invoice_general")
public class FakturaDB {	
	@Id
	@Column(name="DOCUMENT_ID")
	private Long id;
	
	private Integer status;
	
	@Column(name="data_wplywu")
	private Date dataWplywu; 
	
	@ManyToOne
	@JoinColumn(name="adresat")
	private Recipient adresat;
	
	@Column(name="netto")
	private BigDecimal kwotaNetto;
	
	@Column(name="brutto")
	private BigDecimal kwotaBrutto;
	
	@Column(name="vat")
	private BigDecimal kwotaVat;
	
	@ManyToOne
	@JoinColumn(name="waluta")
	private CurrencyDB waluta;
	
	@Column(name="termin_platnosci")
	private Date terminPlatnosci;
	
	@OneToOne
	@JoinColumn(name="faktura_skan")
	private Attachment zalacznik; 
	
	@Column(name="RODZAJFAKTURY")
	private Integer rodzajFaktury;
	
	@Column(name="FAKTKOSZTTRWALE")
	private Boolean fakturaZaKosztyTrwale;
	
	@Column(name="FAKTNAWYDZIALE")
	private Boolean fakturaNaWydziale;
	
	@ManyToOne
	@JoinColumn(name="PELNOMOCNIK")
	private DivisionImpl pelnomocnikDoSprawFinansowych;
	
	@Column(name="numer_faktury")
	private String numerFaktury;
	
	@Column(name="BARCODE")
	private String kodKreskowy;
	
	@ManyToOne
	@JoinColumn(name="wystawca_faktury")
	private KontrahentDB wystawcaFaktury;
	
	@Column(name="data_wystawienia")
	private Date dataWystawienia;
	
	@Column(name="kwotaWwalucie")
	private BigDecimal kwotaWWalucie;
	
	private BigDecimal kurs;
	
	@ManyToOne
	@JoinColumn(name="typPlatnosci")
	private PaymentTermsDB typPlatnosci;
	
	@Column(name="nazwaTowUsl")
	private String nazwaTowaruUslugi;
	
	@Column(name="DODATKOWEINFO")
	private String dodatkoweInformacje;
	
	@Column(name="WYDATEKSTRUKT")
	private Boolean wydatekJestStrukturalny;
	
	@OneToMany
	@JoinTable(
			name="dsg_invoice_general_multiple", 
			joinColumns={@JoinColumn(name="DOCUMENT_ID", referencedColumnName="DOCUMENT_ID")},
			inverseJoinColumns={@JoinColumn(name="FIELD_VAL", referencedColumnName="id")})
	@WhereJoinTable(clause="FIELD_CN='WYDATEKSTRUKTPOZ'")
	private Set<WydatekStrukturalnyDB> wydatekStrukturalnyPoz;
	
	@Column(name="SKIERUJUZUPOPIS")
	private Boolean skierujDoUzupelnieniaOpisu;
	
	@ManyToOne
	@JoinColumn(name="OSOBAUZUPOPIS")
	private UserImpl osobaUzupelniajacaOpisFaktury;
	
	@Column(name="NUMERPOSTPRZETARG")
	private String numerPostepowaniaPrzetargowego;
	
	@Column(name="TRYBPOSTEP")
	private Integer trybPostepowania;
	
	@Column(name="WynikaZPostepPrzetarg")
	private Integer wynikaZPostepowaniaPrzetargowego;
	
	@ManyToOne
	@JoinColumn(name="PracKsiegowosc")
	private UserImpl pracownikKsiegowosci;
	
	@ManyToOne
	@JoinColumn(name="GlownyKsiegowy")
	private UserImpl glownyKsiegowy;
	
	@ManyToOne
	@JoinColumn(name="Dostawca")
	private KontrahentDB dostawca;
	
	@Column(name="korekta")
	private Boolean czyKorekta;
	
	@Column(name="korekta_faktura")
	private String korektaNumer;
	
	@ManyToOne
	@JoinColumn(name="typ_faktury_wal", referencedColumnName="typdokzak_id")
	private PurchaseDocumentTypeDB typFakturyWal;
	
	@ManyToOne
	@JoinColumn(name="typ_faktury_pln", referencedColumnName="typdokzak_id")
	private PurchaseDocumentTypeDB typFakturyPln;
		
	@ManyToOne
	@JoinColumn(name="STANOWISKO_DOSTAW")
	private DeliveryLocationDB stanowiskoDostaw;
	
	@Column(name="opisMeryt")
	private String opisMerytoryczny;
	
	@Column(name="PrzeznIUwagi")
	private String przeznaczenieIUwagi;
	
	@OneToMany
	@JoinTable(
			name="dsg_invoice_general_multiple", 
			joinColumns={@JoinColumn(name="DOCUMENT_ID", referencedColumnName="DOCUMENT_ID")},
			inverseJoinColumns={@JoinColumn(name="FIELD_VAL", referencedColumnName="id")})
	@WhereJoinTable(clause="FIELD_CN='MPK'")
	private Set<BudzetPozycjaDB> pozycjeBudzetowe;
	
	@OneToMany
	@JoinTable(
			name="dsg_invoice_general_multiple", 
			joinColumns={@JoinColumn(name="DOCUMENT_ID", referencedColumnName="DOCUMENT_ID")},
			inverseJoinColumns={@JoinColumn(name="FIELD_VAL", referencedColumnName="id")})
	@WhereJoinTable(clause="FIELD_CN='POZYCJE_FAKTURY'")
	private Set<FakturaPozycjaDB> pozycjeFaktury;
	
	@Column(name="KwotaPozost")
	private BigDecimal kwotaPozostala;
	
	@Column(name="potwOdZgZUmow")
	private Boolean potwierdzamOdbiorZgodnieZUmowa;
	
	@Column(name="potwOdZgZFakt")
	private Boolean potwierdzamOdbiorZgodnieZFaktura;
	
	@Column(name="NRKONTAWYSTAWCY")
	private String numerKontaBankowego;
	
	@Column(name = "sposobWyplaty")
	private Integer sposobWyplaty;
	
	@Column(name="czySzablon")
	private Boolean czySzablonRozliczenFaktury;
	
	@Column(name="Szablon")
	private Long szablonErpId;

	@Column(name="czyZZamowienia")
	private Boolean czyZZamowienia;
	
	@Column(name="ZAAKCEPTOWANOPOZZAM")
	private Boolean ZAAKCEPTOWANOPOZZAM;

	@Column(name="czyRozliczenieMediow")
	private Boolean czyRozliczenieMediow;
	
	@Column(name="CZYZWNIOSKU")
	private Boolean czZWniosku;
	
	@Column(name="ZaakceptowanoWnioskiORez")
	private Boolean zaakceptowanoWnioskiORez;
	
	@OneToMany
	@JoinTable(
			name="dsg_invoice_general_multiple", 
			joinColumns={@JoinColumn(name="DOCUMENT_ID", referencedColumnName="DOCUMENT_ID")},
			inverseJoinColumns={@JoinColumn(name="FIELD_VAL", referencedColumnName="id")})
	@WhereJoinTable(clause="FIELD_CN='WNIOSKI_O_REZERWACJE'")
	private Set<WniosekORezerwacjeDB> wnioskiORezerwacjeDB;	

	@OneToMany
	@JoinTable(
			name="dsg_invoice_general_multiple", 
			joinColumns={@JoinColumn(name="DOCUMENT_ID", referencedColumnName="DOCUMENT_ID")},
			inverseJoinColumns={@JoinColumn(name="FIELD_VAL", referencedColumnName="id")})
	@WhereJoinTable(clause="FIELD_CN='POZYCJE_REZERWACJI'")
	private Set<WniosekORezerwacjePozycjaDB> pozycjeRezerwacjiDB;	
	
	@OneToMany
	@JoinTable(
			name="dsg_invoice_general_multiple", 
			joinColumns={@JoinColumn(name="DOCUMENT_ID", referencedColumnName="DOCUMENT_ID")},
			inverseJoinColumns={@JoinColumn(name="FIELD_VAL", referencedColumnName="id")})
	@WhereJoinTable(clause="FIELD_CN='POZYCJE_ZAMOW'")
	private Set<ZamowienieSimplePozycjeDB> pozycjeZamowieniaDB;		
	
	@ManyToOne
	@JoinColumn(name="nrZamowienia")
	NumeryZamowienDB zamowienie;
	
	@Column(name="wygenerowanoMetryczke")
	private Boolean wygenerowanoMetryczke;
	
	public FakturaDB(){}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getDataWplywu() {
		return dataWplywu;
	}

	public void setDataWplywu(Date dataWplywu) {
		this.dataWplywu = dataWplywu;
	}

	public Recipient getAdresat() {
		return adresat;
	}

	public void setAdresat(Recipient adresat) {
		this.adresat = adresat;
	}

	public BigDecimal getKwotaNetto() {
		return kwotaNetto;
	}

	public void setKwotaNetto(BigDecimal kwotaNetto) {
		this.kwotaNetto = kwotaNetto;
	}

	public BigDecimal getKwotaBrutto() {
		return kwotaBrutto;
	}

	public void setKwotaBrutto(BigDecimal kwotaBrutto) {
		this.kwotaBrutto = kwotaBrutto;
	}

	public BigDecimal getKwotaVat() {
		return kwotaVat;
	}

	public void setKwotaVat(BigDecimal kwotaVat) {
		this.kwotaVat = kwotaVat;
	}

	public CurrencyDB getWaluta() {
		return waluta;
	}

	public void setWaluta(CurrencyDB waluta) {
		this.waluta = waluta;
	}

	public Date getTerminPlatnosci() {
		return terminPlatnosci;
	}

	public void setTerminPlatnosci(Date terminPlatnosci) {
		this.terminPlatnosci = terminPlatnosci;
	}

	public Attachment getZalacznik() {
		return zalacznik;
	}

	public void setZalacznik(Attachment zalacznik) {
		this.zalacznik = zalacznik;
	}

	public Integer getRodzajFaktury() {
		return rodzajFaktury;
	}

	public void setRodzajFaktury(Integer rodzajFaktury) {
		this.rodzajFaktury = rodzajFaktury;
	}

	public Boolean getFakturaNaWydziale() {
		return fakturaNaWydziale;
	}

	public void setFakturaNaWydziale(Boolean fakturaNaWydziale) {
		this.fakturaNaWydziale = fakturaNaWydziale;
	}

	public DivisionImpl getPelnomocnikDoSprawFinansowych() {
		return pelnomocnikDoSprawFinansowych;
	}

	public void setPelnomocnikDoSprawFinansowych(
			DivisionImpl pelnomocnikDoSprawFinansowych) {
		this.pelnomocnikDoSprawFinansowych = pelnomocnikDoSprawFinansowych;
	}

	public String getNumerFaktury() {
		return numerFaktury;
	}

	public void setNumerFaktury(String numerFaktury) {
		this.numerFaktury = numerFaktury;
	}

	public String getKodKreskowy() {
		return kodKreskowy;
	}

	public void setKodKreskowy(String kodKreskowy) {
		this.kodKreskowy = kodKreskowy;
	}

	public KontrahentDB getWystawcaFaktury() {
		return wystawcaFaktury;
	}

	public void setWystawcaFaktury(KontrahentDB wystawcaFaktury) {
		this.wystawcaFaktury = wystawcaFaktury;
	}

	public Date getDataWystawienia() {
		return dataWystawienia;
	}

	public void setDataWystawienia(Date dataWystawienia) {
		this.dataWystawienia = dataWystawienia;
	}

	public BigDecimal getKwotaWWalucie() {
		return kwotaWWalucie;
	}

	public void setKwotaWWalucie(BigDecimal kwotaWWalucie) {
		this.kwotaWWalucie = kwotaWWalucie;
	}

	public BigDecimal getKurs() {
		return kurs;
	}

	public void setKurs(BigDecimal kurs) {
		this.kurs = kurs;
	}

	public PaymentTermsDB getTypPlatnosci() {
		return typPlatnosci;
	}

	public void setTypPlatnosci(PaymentTermsDB typPlatnosci) {
		this.typPlatnosci = typPlatnosci;
	}

	public String getNazwaTowaruUslugi() {
		return nazwaTowaruUslugi;
	}

	public void setNazwaTowaruUslugi(String nazwaTowaruUslugi) {
		this.nazwaTowaruUslugi = nazwaTowaruUslugi;
	}

	public String getDodatkoweInformacje() {
		return dodatkoweInformacje;
	}

	public void setDodatkoweInformacje(String dodatkoweInformacje) {
		this.dodatkoweInformacje = dodatkoweInformacje;
	}

	public Boolean getWydatekJestStrukturalny() {
		return wydatekJestStrukturalny;
	}

	public void setWydatekJestStrukturalny(Boolean wydatekJestStrukturalny) {
		this.wydatekJestStrukturalny = wydatekJestStrukturalny;
	}

	public Set<WydatekStrukturalnyDB> getWydatekStrukturalnyPoz() {
		return wydatekStrukturalnyPoz;
	}

	public void setWydatekStrukturalnyPoz(
			Set<WydatekStrukturalnyDB> wydatekStrukturalnyPoz) {
		this.wydatekStrukturalnyPoz = wydatekStrukturalnyPoz;
	}

	public Boolean getSkierujDoUzupelnieniaOpisu() {
		return skierujDoUzupelnieniaOpisu;
	}

	public void setSkierujDoUzupelnieniaOpisu(Boolean skierujDoUzupelnieniaOpisu) {
		this.skierujDoUzupelnieniaOpisu = skierujDoUzupelnieniaOpisu;
	}

	public UserImpl getOsobaUzupelniajacaOpisFaktury() {
		return osobaUzupelniajacaOpisFaktury;
	}

	public void setOsobaUzupelniajacaOpisFaktury(
			UserImpl osobaUzupelniajacaOpisFaktury) {
		this.osobaUzupelniajacaOpisFaktury = osobaUzupelniajacaOpisFaktury;
	}

	public String getNumerPostepowaniaPrzetargowego() {
		return numerPostepowaniaPrzetargowego;
	}

	public void setNumerPostepowaniaPrzetargowego(
			String numerPostepowaniaPrzetargowego) {
		this.numerPostepowaniaPrzetargowego = numerPostepowaniaPrzetargowego;
	}

	public Integer getTrybPostepowania() {
		return trybPostepowania;
	}

	public void setTrybPostepowania(Integer trybPostepowania) {
		this.trybPostepowania = trybPostepowania;
	}

	public Integer getWynikaZPostepowaniaPrzetargowego() {
		return wynikaZPostepowaniaPrzetargowego;
	}

	public void setWynikaZPostepowaniaPrzetargowego(
			Integer wynikaZPostepowaniaPrzetargowego) {
		this.wynikaZPostepowaniaPrzetargowego = wynikaZPostepowaniaPrzetargowego;
	}

	public UserImpl getPracownikKsiegowosci() {
		return pracownikKsiegowosci;
	}

	public void setPracownikKsiegowosci(UserImpl pracownikKsiegowosci) {
		this.pracownikKsiegowosci = pracownikKsiegowosci;
	}

	public UserImpl getGlownyKsiegowy() {
		return glownyKsiegowy;
	}

	public void setGlownyKsiegowy(UserImpl glownyKsiegowy) {
		this.glownyKsiegowy = glownyKsiegowy;
	}

	public KontrahentDB getDostawca() {
		return dostawca;
	}

	public void setDostawca(KontrahentDB dostawca) {
		this.dostawca = dostawca;
	}

/*	public Boolean getZamowienieNaMagazyn() {
		return zamowienieNaMagazyn;
	}

	public void setZamowienieNaMagazyn(Boolean zamowienieNaMagazyn) {
		this.zamowienieNaMagazyn = zamowienieNaMagazyn;
	}*/

	public Boolean getCzyKorekta() {
		return czyKorekta;
	}

	public void setCzyKorekta(Boolean czyKorekta) {
		this.czyKorekta = czyKorekta;
	}

	public PurchaseDocumentTypeDB getTypFakturyWal() {
		return typFakturyWal;
	}

	public void setTypFakturyWal(PurchaseDocumentTypeDB typFakturyWal) {
		this.typFakturyWal = typFakturyWal;
	}

	public PurchaseDocumentTypeDB getTypFakturyPln() {
		return typFakturyPln;
	}

	public void setTypFakturyPln(PurchaseDocumentTypeDB typFakturyPln) {
		this.typFakturyPln = typFakturyPln;
	}

/*	public Journal getDziennik() {
		return dziennik;
	}

	public void setDziennik(Journal dziennik) {
		this.dziennik = dziennik;
	}*/

/*	public String getNumerPrzesylkiRejestrowanej() {
		return numerPrzesylkiRejestrowanej;
	}

	public void setNumerPrzesylkiRejestrowanej(String numerPrzesylkiRejestrowanej) {
		this.numerPrzesylkiRejestrowanej = numerPrzesylkiRejestrowanej;
	}*/

	public DeliveryLocationDB getStanowiskoDostaw() {
		return stanowiskoDostaw;
	}

	public void setStanowiskoDostaw(DeliveryLocationDB stanowiskoDostaw) {
		this.stanowiskoDostaw = stanowiskoDostaw;
	}

	public String getOpisMerytoryczny() {
		return opisMerytoryczny;
	}

	public void setOpisMerytoryczny(String opisMerytoryczny) {
		this.opisMerytoryczny = opisMerytoryczny;
	}

	public String getPrzeznaczenieIUwagi() {
		return przeznaczenieIUwagi;
	}

	public void setPrzeznaczenieIUwagi(String przeznaczenieIUwagi) {
		this.przeznaczenieIUwagi = przeznaczenieIUwagi;
	}

	public BigDecimal getKwotaPozostala() {
		return kwotaPozostala;
	}

	public void setKwotaPozostala(BigDecimal kwotaPozostala) {
		this.kwotaPozostala = kwotaPozostala;
	}

	public Boolean getPotwierdzamOdbiorZgodnieZUmowa() {
		return potwierdzamOdbiorZgodnieZUmowa;
	}

	public void setPotwierdzamOdbiorZgodnieZUmowa(
			Boolean potwierdzamOdbiorZgodnieZUmowa) {
		this.potwierdzamOdbiorZgodnieZUmowa = potwierdzamOdbiorZgodnieZUmowa;
	}

	public Boolean getPotwierdzamOdbiorZgodnieZFaktura() {
		return potwierdzamOdbiorZgodnieZFaktura;
	}

	public void setPotwierdzamOdbiorZgodnieZFaktura(
			Boolean potwierdzamOdbiorZgodnieZFaktura) {
		this.potwierdzamOdbiorZgodnieZFaktura = potwierdzamOdbiorZgodnieZFaktura;
	}

	public Set<FakturaPozycjaDB> getPozycjeFaktury() {
		return pozycjeFaktury;
	}

	public void setPozycjeFaktury(Set<FakturaPozycjaDB> pozycjeFaktury) {
		this.pozycjeFaktury = pozycjeFaktury;
	}

	public Set<BudzetPozycjaDB> getPozycjeBudzetowe() {
		return pozycjeBudzetowe;
	}

	public void setPozycjeBudzetowe(Set<BudzetPozycjaDB> pozycjeBudzetowe) {
		this.pozycjeBudzetowe = pozycjeBudzetowe;
	}

	public Boolean getFakturaZaKosztyTrwale() {
		return fakturaZaKosztyTrwale;
	}

	public void setFakturaZaKosztyTrwale(Boolean fakturaZaKosztyTrwale) {
		this.fakturaZaKosztyTrwale = fakturaZaKosztyTrwale;
	}

	public String getNumerKontaBankowego() {
		return numerKontaBankowego;
	}

	public void setNumerKontaBankowego(String numerKontaBankowego) {
		this.numerKontaBankowego = numerKontaBankowego;
	}

	public String getKorektaNumer() {
		return korektaNumer;
	}

	public void setKorektaNumer(String korektaNumer) {
		this.korektaNumer = korektaNumer;
	}

	public Integer getSposobWyplaty() {
		return sposobWyplaty;
	}

	public void setSposobWyplaty(Integer sposobWyplaty) {
		this.sposobWyplaty = sposobWyplaty;
	}

	public Boolean getCzySzablonRozliczenFaktury() {
		return czySzablonRozliczenFaktury;
	}

	public void setCzySzablonRozliczenFaktury(Boolean czySzablonRozliczenFaktury) {
		this.czySzablonRozliczenFaktury = czySzablonRozliczenFaktury;
	}

	public Long getSzablonErpId() {
		return szablonErpId;
	}

	public void setSzablonErpId(Long szablonErpId) {
		this.szablonErpId = szablonErpId;
	}

	public Boolean getCzyZZamowienia() {
		return czyZZamowienia;
	}

	public void setCzyZZamowienia(Boolean czyZZamowienia) {
		this.czyZZamowienia = czyZZamowienia;
	}

	public Boolean getZAAKCEPTOWANOPOZZAM() {
		return ZAAKCEPTOWANOPOZZAM;
	}

	public void setZAAKCEPTOWANOPOZZAM(Boolean zAAKCEPTOWANOPOZZAM) {
		ZAAKCEPTOWANOPOZZAM = zAAKCEPTOWANOPOZZAM;
	}

	public Boolean getCzyRozliczenieMediow() {
		return czyRozliczenieMediow;
	}

	public void setCzyRozliczenieMediow(Boolean czyRozliczenieMediow) {
		this.czyRozliczenieMediow = czyRozliczenieMediow;
	}

	public Boolean getCzZWniosku() {
		return czZWniosku;
	}

	public void setCzZWniosku(Boolean czZWniosku) {
		this.czZWniosku = czZWniosku;
	}

	public Boolean getZaakceptowanoWnioskiORez() {
		return zaakceptowanoWnioskiORez;
	}

	public void setZaakceptowanoWnioskiORez(Boolean zaakceptowanoWnioskiORez) {
		this.zaakceptowanoWnioskiORez = zaakceptowanoWnioskiORez;
	}

	public Set<WniosekORezerwacjeDB> getWnioskiORezerwacjeDB() {
		return wnioskiORezerwacjeDB;
	}

	public void setWnioskiORezerwacjeDB(
			Set<WniosekORezerwacjeDB> wnioskiORezerwacjeDB) {
		this.wnioskiORezerwacjeDB = wnioskiORezerwacjeDB;
	}

	public Set<WniosekORezerwacjePozycjaDB> getPozycjeRezerwacjiDB() {
		return pozycjeRezerwacjiDB;
	}

	public void setPozycjeRezerwacjiDB(
			Set<WniosekORezerwacjePozycjaDB> pozycjeRezerwacjiDB) {
		this.pozycjeRezerwacjiDB = pozycjeRezerwacjiDB;
	}


	public NumeryZamowienDB getZamowienie() {
		return zamowienie;
	}

	public void setZamowienie(NumeryZamowienDB zamowienie) {
		this.zamowienie = zamowienie;
	}

	public Set<ZamowienieSimplePozycjeDB> getPozycjeZamowieniaDB() {
		return pozycjeZamowieniaDB;
	}

	public void setPozycjeZamowieniaDB(
			Set<ZamowienieSimplePozycjeDB> pozycjeZamowieniaDB) {
		this.pozycjeZamowieniaDB = pozycjeZamowieniaDB;
	}

	public Boolean getWygenerowanoMetryczke() {
		return wygenerowanoMetryczke;
	}

	public void setWygenerowanoMetryczke(Boolean wygenerowanoMetryczke) {
		this.wygenerowanoMetryczke = wygenerowanoMetryczke;
	}

}