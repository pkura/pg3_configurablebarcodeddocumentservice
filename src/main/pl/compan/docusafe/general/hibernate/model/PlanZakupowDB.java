package pl.compan.docusafe.general.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_plany_zakupow")
public class PlanZakupowDB {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "okres_id")
	private Long okres_id;
	
	@Column(name = "okres_idm")
	private String okres_idm;
	
	@Column(name = "budzet_id")
	private Long budzet_id;
	
	@Column(name = "budzet_idn")
	private String budzet_idn;
	
/*	@Column(name = "czy_ceny_brutto")
	private boolean czy_ceny_brutto;
	
	@Column(name = "czy_kwoty_tys")
	private boolean czy_kwoty_tys;*/
	
	@Column(name = "czyAgregat")
	private Boolean czyAgregat;
	
	@Column(name = "erpId")
	private Long erpId;

	@Column(name = "idm")
	private String idm;
	
	@Column(name = "nazwa")
	private String nazwa;
	
/*	@Column(name = "kwota_pzp")
	private double kwota_pzp;
	
	@Column(name = "pozostalo_pzp")
	private double pozostalo_pzp;
	*/
/*	@Column(name = "status")
	private int status;
*/
	@Column(name = "available")
	private Boolean available;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public Long getOkres_id() {
		return okres_id;
	}

	public void setOkres_id(Long okres_id) {
		this.okres_id = okres_id;
	}

	public String getOkres_idm() {
		return okres_idm;
	}

	public void setOkres_idm(String okres_idm) {
		this.okres_idm = okres_idm;
	}

	public Boolean getCzyAgregat() {
		return czyAgregat;
	}

	public void setCzyAgregat(Boolean czyAgregat) {
		this.czyAgregat = czyAgregat;
	}

	public Long getBudzet_id() {
		return budzet_id;
	}

	public void setBudzet_id(Long budzet_id) {
		this.budzet_id = budzet_id;
	}

	public String getBudzet_idn() {
		return budzet_idn;
	}

	public void setBudzet_idn(String budzet_idn) {
		this.budzet_idn = budzet_idn;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
}
