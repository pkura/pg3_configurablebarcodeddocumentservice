package pl.compan.docusafe.general.hibernate.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dsg_projekty")
public class ProjectDB implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "cn", nullable = false)
	private String cn;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "centrum", nullable = true)
	private Integer centrum;	

	@Column(name = "refValue", nullable = true)
	private String refValue;

	@Column(name = "available", nullable = true)
	private Boolean available;

	@Column(name = "identyfikator_num", nullable = true)
	private Double identyfikator_num;

	@Column(name = "erpId", nullable = true)
	private Long erpId;
	
	@Column(name = "p_datako")
	private Date p_datako;
	
	@Column(name = "p_datapo")
	private Date p_datapo;
	
	@Column(name = "typ_kontrakt_id")
	private Double typ_kontrakt_id;
	
	@Column(name ="typ_kontrakt_idn")
	private String typ_kontrakt_idn;

	public ProjectDB() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public Double getIdentyfikator_num() {
		return identyfikator_num;
	}

	public void setIdentyfikator_num(Double identyfikator_num) {
		this.identyfikator_num = identyfikator_num;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public Date getP_datako() {
		return p_datako;
	}

	public void setP_datako(Date p_datako) {
		this.p_datako = p_datako;
	}

	public Date getP_datapo() {
		return p_datapo;
	}

	public void setP_datapo(Date p_datapo) {
		this.p_datapo = p_datapo;
	}

	public Double getTyp_kontrakt_id() {
		return typ_kontrakt_id;
	}

	public void setTyp_kontrakt_id(Double typ_kontrakt_id) {
		this.typ_kontrakt_id = typ_kontrakt_id;
	}

	public String getTyp_kontrakt_idn() {
		return typ_kontrakt_idn;
	}

	public void setTyp_kontrakt_idn(String typ_kontrakt_idn) {
		this.typ_kontrakt_idn = typ_kontrakt_idn;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
}
