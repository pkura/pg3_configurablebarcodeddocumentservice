/**
 * 
 */
package pl.compan.docusafe.general.hibernate.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;

/**
 * @author Tomasz Nowak <tomasz.nowak@docusafe.pl>
 *
 */
@Entity
@Table(name = InvoiceGeneralConstans.TABELA_WNIOSKI_O_REZERWACJE)
public class WniosekORezerwacjeDB{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "bd_budzet_ko_id")
	private Long bdBudzetKoId;
	
	@Column(name = "bd_budzet_ko_idm")
	private String bdBudzetKoIdm;
	
	@Column(name = "bd_plan_zam_id")
	private Long bdPlanZamId;
	
	@Column(name = "bd_plam_zam_idm")
	private String bdPlanZamIdm;
	
	@Column(name = "bd_rezerwacja_id")
	private Long bdRezerwacjaId;
	
	@Column(name = "bd_rezerwacja_idm")
	private String bdRezerwacjaIdm;
	
	@Column(name = "bd_rezerwacja_poz_id")
	private Long bdRezerwacjaPozId;
	
	@Column(name = "bd_rodzaj_ko_id")
	private Long bdRodzajKoId;
	
	@Column(name = "bd_rodzaj_plan_zam_id")
	private Long bdRodzajPlanZamId;
	
	@Column(name = "bd_str_budzet_id")
	private Long bdStrBudzetId;
	
	@Column(name = "bd_typ_rezerwacja_idn")
	private String bdTypRezerwacjaIdn;
	
	@Column(name = "bk_bd_okr_szablon_rel_id")
	private Long bkBdOkrSzablonRelId;
	
	@Column(name = "bk_bd_szablon_poz_id")
	private Long bkBdSzablonPozId;
	
	/* na tescie tego nie ma
	@Column(name = "bk_czy_archiwalny")
	private Double bkCzyArchiwalny;*/
	
	@Column(name = "bk_nazwa")
	private String bkNazwa;
	
	@Column(name = "bk_poz_nazwa")
	private String bkPozNazwa;
	
	@Column(name = "bk_poz_nrpoz")
	private Integer bkPozNrpoz;
	
	@Column(name = "bk_poz_p_koszt")
	private BigDecimal bkPozPKoszt;
	
	@Column(name = "bk_poz_p_koszt_zrodla")
	private BigDecimal bkPozPKosztZrodla;
	
	@Column(name = "bk_poz_r_koszt")
	private BigDecimal bkPozRKoszt;
	
	@Column(name = "bk_poz_r_koszt_zrodla")
	private BigDecimal bkPozRKosztZrodla;
	
	@Column(name = "bk_poz_rez_koszt")
	private BigDecimal bkPozRezKoszt;
	
	@Column(name = "bk_poz_rez_koszt_zrodla")
	private BigDecimal bkPozRezKosztZrodla;
	
	/* na tescie tego nie ma
	@Column(name = "bk_status")
	private Integer bkStatus;*/
	
	@Column(name = "bk_wytwor_id")
	private Long bkWytworId;
	
	@Column(name = "budzet_id")
	private Long budzetId;
	
	@Column(name = "budzet_idm")
	private String budzetIdm;
	
	@Column(name = "cena")
	private BigDecimal cena;
	
	@Column(name = "czy_obsluga_pzp")
	private Boolean czyObslugaPzp;
	
	@Column(name = "ilosc")
	private BigDecimal ilosc;
	
	@Column(name = "koszt")
	private BigDecimal koszt;
	
	@Column(name = "magazyn_id")
	private Long magazynId;
	
	@Column(name = "nazwa")
	private String nazwa;
	
	@Column(name = "nrpoz")
	private Integer nrpoz;
	
	@Column(name = "okrrozl_id")
	private Long okrrozlId;
	
	@Column(name = "pzp_bd_okr_szablon_rel_id")
	private Long pzpBdOkrSzablonRelId;
	
	@Column(name = "pzp_bd_szablon_poz_id")
	private Long pzpBdSzablonPozId;
	
	/* na tescie tego nie ma
	@Column(name = "pzp_czy_archiwalny")
	private Double pzpCzyArchiwalny;*/
	
	@Column(name = "pzp_jm_id")
	private Long pzpJmId;
	
	@Column(name = "pzp_jm_idn")
	private String pzpJmIdn;
	
	@Column(name = "pzp_nazwa")
	private String pzpNazwa;
	
	@Column(name = "pzp_poz_nazwa")
	private String pzpPozNazwa;
	
	@Column(name = "pzp_poz_nrpoz")
	private Integer pzpPozNrpoz;
	
	@Column(name = "pzp_poz_p_ilosc")
	private BigDecimal pzpPozPIlosc;
	
	@Column(name = "pzp_poz_p_koszt")
	private BigDecimal pzpPozPKoszt;
	
	@Column(name = "pzp_poz_r_ilosc")
	private BigDecimal pzpPozRIlosc;
	
	@Column(name = "pzp_poz_r_koszt")
	private BigDecimal pzpPozRKoszt;
	
	@Column(name = "pzp_poz_rez_ilosc")
	private BigDecimal pzpPozRezIlosc;
	
	@Column(name = "pzp_poz_rez_koszt")
	private BigDecimal pzpPozRezKoszt;
	
	/* na tescie tego nie ma
	@Column(name = "pzp_status")
	private Integer pzpStatus;*/
	
	@Column(name = "pzp_wytwor_id")
	private Long pzpWytworId;
	
	@Column(name = "pzp_wytwor_idm")
	private String pzpWytworIdm;
	
	@Column(name = "rez_jm_id")
	private Long rezJmId;
	
	@Column(name = "rez_jm_idn")
	private String rezJmIdn;
	
	@Column(name = "rezerwacja_czy_archiwalny")
	private Boolean rezerwacjaCzyArchiwalny;
	
	@Column(name = "rezerwacja_status")
	private Integer rezerwacjaStatus;
	
	@Column(name = "vatstaw_id")
	private Long vatstawId;
	
	@Column(name = "wytwor_edit_idm")
	private String wytworEditIdm;
	
	@Column(name = "wytwor_edit_nazwa")
	private String wytworEditNazwa;
	
	@Column(name = "wytwor_id")
	private Long wytworId;
	
	@Column(name = "zadanie_id")
	private Long zadanieId;
	
	@Column(name = "zadanie_idn")
	private String zadanieIdn;
	
	@Column(name = "zadanie_nazwa")
	private String zadanieNazwa;
	
	@Column(name = "zrodlo_id")
	private Long zrodloId;
	
	@Column(name = "zrodlo_idn")
	private String zrodloIdn;
	
	@Column(name = "zrodlo_nazwa")
	private String zrodloNazwa;
	
	@Column(name = "available")
	private Boolean available;	
	
	public WniosekORezerwacjeDB() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBdBudzetKoId() {
		return bdBudzetKoId;
	}

	public void setBdBudzetKoId(Long bdBudzetKoId) {
		this.bdBudzetKoId = bdBudzetKoId;
	}

	public String getBdBudzetKoIdm() {
		return bdBudzetKoIdm;
	}

	public void setBdBudzetKoIdm(String bdBudzetKoIdm) {
		this.bdBudzetKoIdm = bdBudzetKoIdm;
	}

	public Long getBdPlanZamId() {
		return bdPlanZamId;
	}

	public void setBdPlanZamId(Long bdPlanZamId) {
		this.bdPlanZamId = bdPlanZamId;
	}

	public String getBdPlanZamIdm() {
		return bdPlanZamIdm;
	}

	public void setBdPlanZamIdm(String bdPlanZamIdm) {
		this.bdPlanZamIdm = bdPlanZamIdm;
	}

	public Long getBdRezerwacjaId() {
		return bdRezerwacjaId;
	}

	public void setBdRezerwacjaId(Long bdRezerwacjaId) {
		this.bdRezerwacjaId = bdRezerwacjaId;
	}

	public String getBdRezerwacjaIdm() {
		return bdRezerwacjaIdm;
	}

	public void setBdRezerwacjaIdm(String bdRezerwacjaIdm) {
		this.bdRezerwacjaIdm = bdRezerwacjaIdm;
	}

	public Long getBdRezerwacjaPozId() {
		return bdRezerwacjaPozId;
	}

	public void setBdRezerwacjaPozId(Long bdRezerwacjaPozId) {
		this.bdRezerwacjaPozId = bdRezerwacjaPozId;
	}

	public Long getBdRodzajKoId() {
		return bdRodzajKoId;
	}

	public void setBdRodzajKoId(Long bdRodzajKoId) {
		this.bdRodzajKoId = bdRodzajKoId;
	}

	public Long getBdRodzajPlanZamId() {
		return bdRodzajPlanZamId;
	}

	public void setBdRodzajPlanZamId(Long bdRodzajPlanZamId) {
		this.bdRodzajPlanZamId = bdRodzajPlanZamId;
	}

	public Long getBdStrBudzetId() {
		return bdStrBudzetId;
	}

	public void setBdStrBudzetId(Long bdStrBudzetId) {
		this.bdStrBudzetId = bdStrBudzetId;
	}

	public String getBdTypRezerwacjaIdn() {
		return bdTypRezerwacjaIdn;
	}

	public void setBdTypRezerwacjaIdn(String bdTypRezerwacjaIdn) {
		this.bdTypRezerwacjaIdn = bdTypRezerwacjaIdn;
	}

	public Long getBkBdOkrSzablonRelId() {
		return bkBdOkrSzablonRelId;
	}

	public void setBkBdOkrSzablonRelId(Long bkBdOkrSzablonRelId) {
		this.bkBdOkrSzablonRelId = bkBdOkrSzablonRelId;
	}

	public Long getBkBdSzablonPozId() {
		return bkBdSzablonPozId;
	}

	public void setBkBdSzablonPozId(Long bkBdSzablonPozId) {
		this.bkBdSzablonPozId = bkBdSzablonPozId;
	}

	public String getBkNazwa() {
		return bkNazwa;
	}

	public void setBkNazwa(String bkNazwa) {
		this.bkNazwa = bkNazwa;
	}

	public String getBkPozNazwa() {
		return bkPozNazwa;
	}

	public void setBkPozNazwa(String bkPozNazwa) {
		this.bkPozNazwa = bkPozNazwa;
	}

	public Integer getBkPozNrpoz() {
		return bkPozNrpoz;
	}

	public void setBkPozNrpoz(Integer bkPozNrpoz) {
		this.bkPozNrpoz = bkPozNrpoz;
	}

	public BigDecimal getBkPozPKoszt() {
		return bkPozPKoszt;
	}

	public void setBkPozPKoszt(BigDecimal bkPozPKoszt) {
		this.bkPozPKoszt = bkPozPKoszt;
	}

	public BigDecimal getBkPozPKosztZrodla() {
		return bkPozPKosztZrodla;
	}

	public void setBkPozPKosztZrodla(BigDecimal bkPozPKosztZrodla) {
		this.bkPozPKosztZrodla = bkPozPKosztZrodla;
	}

	public BigDecimal getBkPozRKoszt() {
		return bkPozRKoszt;
	}

	public void setBkPozRKoszt(BigDecimal bkPozRKoszt) {
		this.bkPozRKoszt = bkPozRKoszt;
	}

	public BigDecimal getBkPozRKosztZrodla() {
		return bkPozRKosztZrodla;
	}

	public void setBkPozRKosztZrodla(BigDecimal bkPozRKosztZrodla) {
		this.bkPozRKosztZrodla = bkPozRKosztZrodla;
	}

	public BigDecimal getBkPozRezKoszt() {
		return bkPozRezKoszt;
	}

	public void setBkPozRezKoszt(BigDecimal bkPozRezKoszt) {
		this.bkPozRezKoszt = bkPozRezKoszt;
	}

	public BigDecimal getBkPozRezKosztZrodla() {
		return bkPozRezKosztZrodla;
	}

	public void setBkPozRezKosztZrodla(BigDecimal bkPozRezKosztZrodla) {
		this.bkPozRezKosztZrodla = bkPozRezKosztZrodla;
	}

	public Long getBkWytworId() {
		return bkWytworId;
	}

	public void setBkWytworId(Long bkWytworId) {
		this.bkWytworId = bkWytworId;
	}

	public Long getBudzetId() {
		return budzetId;
	}

	public void setBudzetId(Long budzetId) {
		this.budzetId = budzetId;
	}

	public String getBudzetIdm() {
		return budzetIdm;
	}

	public void setBudzetIdm(String budzetIdm) {
		this.budzetIdm = budzetIdm;
	}

	public BigDecimal getCena() {
		return cena;
	}

	public void setCena(BigDecimal cena) {
		this.cena = cena;
	}

	public Boolean getCzyObslugaPzp() {
		return czyObslugaPzp;
	}

	public void setCzyObslugaPzp(Boolean czyObslugaPzp) {
		this.czyObslugaPzp = czyObslugaPzp;
	}

	public BigDecimal getIlosc() {
		return ilosc;
	}

	public void setIlosc(BigDecimal ilosc) {
		this.ilosc = ilosc;
	}

	public BigDecimal getKoszt() {
		return koszt;
	}

	public void setKoszt(BigDecimal koszt) {
		this.koszt = koszt;
	}

	public Long getMagazynId() {
		return magazynId;
	}

	public void setMagazynId(Long magazynId) {
		this.magazynId = magazynId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Integer getNrpoz() {
		return nrpoz;
	}

	public void setNrpoz(Integer nrpoz) {
		this.nrpoz = nrpoz;
	}

	public Long getOkrrozlId() {
		return okrrozlId;
	}

	public void setOkrrozlId(Long okrrozlId) {
		this.okrrozlId = okrrozlId;
	}

	public Long getPzpBdOkrSzablonRelId() {
		return pzpBdOkrSzablonRelId;
	}

	public void setPzpBdOkrSzablonRelId(Long pzpBdOkrSzablonRelId) {
		this.pzpBdOkrSzablonRelId = pzpBdOkrSzablonRelId;
	}

	public Long getPzpBdSzablonPozId() {
		return pzpBdSzablonPozId;
	}

	public void setPzpBdSzablonPozId(Long pzpBdSzablonPozId) {
		this.pzpBdSzablonPozId = pzpBdSzablonPozId;
	}

	public Long getPzpJmId() {
		return pzpJmId;
	}

	public void setPzpJmId(Long pzpJmId) {
		this.pzpJmId = pzpJmId;
	}

	public String getPzpJmIdn() {
		return pzpJmIdn;
	}

	public void setPzpJmIdn(String pzpJmIdn) {
		this.pzpJmIdn = pzpJmIdn;
	}

	public String getPzpNazwa() {
		return pzpNazwa;
	}

	public void setPzpNazwa(String pzpNazwa) {
		this.pzpNazwa = pzpNazwa;
	}

	public String getPzpPozNazwa() {
		return pzpPozNazwa;
	}

	public void setPzpPozNazwa(String pzpPozNazwa) {
		this.pzpPozNazwa = pzpPozNazwa;
	}

	public Integer getPzpPozNrpoz() {
		return pzpPozNrpoz;
	}

	public void setPzpPozNrpoz(Integer pzpPozNrpoz) {
		this.pzpPozNrpoz = pzpPozNrpoz;
	}

	public BigDecimal getPzpPozPIlosc() {
		return pzpPozPIlosc;
	}

	public void setPzpPozPIlosc(BigDecimal pzpPozPIlosc) {
		this.pzpPozPIlosc = pzpPozPIlosc;
	}

	public BigDecimal getPzpPozPKoszt() {
		return pzpPozPKoszt;
	}

	public void setPzpPozPKoszt(BigDecimal pzpPozPKoszt) {
		this.pzpPozPKoszt = pzpPozPKoszt;
	}

	public BigDecimal getPzpPozRIlosc() {
		return pzpPozRIlosc;
	}

	public void setPzpPozRIlosc(BigDecimal pzpPozRIlosc) {
		this.pzpPozRIlosc = pzpPozRIlosc;
	}

	public BigDecimal getPzpPozRKoszt() {
		return pzpPozRKoszt;
	}

	public void setPzpPozRKoszt(BigDecimal pzpPozRKoszt) {
		this.pzpPozRKoszt = pzpPozRKoszt;
	}

	public BigDecimal getPzpPozRezIlosc() {
		return pzpPozRezIlosc;
	}

	public void setPzpPozRezIlosc(BigDecimal pzpPozRezIlosc) {
		this.pzpPozRezIlosc = pzpPozRezIlosc;
	}

	public BigDecimal getPzpPozRezKoszt() {
		return pzpPozRezKoszt;
	}

	public void setPzpPozRezKoszt(BigDecimal pzpPozRezKoszt) {
		this.pzpPozRezKoszt = pzpPozRezKoszt;
	}

	public Long getPzpWytworId() {
		return pzpWytworId;
	}

	public void setPzpWytworId(Long pzpWytworId) {
		this.pzpWytworId = pzpWytworId;
	}

	public String getPzpWytworIdm() {
		return pzpWytworIdm;
	}

	public void setPzpWytworIdm(String pzpWytworIdm) {
		this.pzpWytworIdm = pzpWytworIdm;
	}

	public Long getRezJmId() {
		return rezJmId;
	}

	public void setRezJmId(Long rezJmId) {
		this.rezJmId = rezJmId;
	}

	public String getRezJmIdn() {
		return rezJmIdn;
	}

	public void setRezJmIdn(String rezJmIdn) {
		this.rezJmIdn = rezJmIdn;
	}

	public Boolean getRezerwacjaCzyArchiwalny() {
		return rezerwacjaCzyArchiwalny;
	}

	public void setRezerwacjaCzyArchiwalny(Boolean rezerwacjaCzyArchiwalny) {
		this.rezerwacjaCzyArchiwalny = rezerwacjaCzyArchiwalny;
	}

	public Integer getRezerwacjaStatus() {
		return rezerwacjaStatus;
	}

	public void setRezerwacjaStatus(Integer rezerwacjaStatus) {
		this.rezerwacjaStatus = rezerwacjaStatus;
	}

	public Long getVatstawId() {
		return vatstawId;
	}

	public void setVatstawId(Long vatstawId) {
		this.vatstawId = vatstawId;
	}

	public String getWytworEditIdm() {
		return wytworEditIdm;
	}

	public void setWytworEditIdm(String wytworEditIdm) {
		this.wytworEditIdm = wytworEditIdm;
	}

	public String getWytworEditNazwa() {
		return wytworEditNazwa;
	}

	public void setWytworEditNazwa(String wytworEditNazwa) {
		this.wytworEditNazwa = wytworEditNazwa;
	}

	public Long getWytworId() {
		return wytworId;
	}

	public void setWytworId(Long wytworId) {
		this.wytworId = wytworId;
	}

	public Long getZadanieId() {
		return zadanieId;
	}

	public void setZadanieId(Long zadanieId) {
		this.zadanieId = zadanieId;
	}

	public String getZadanieIdn() {
		return zadanieIdn;
	}

	public void setZadanieIdn(String zadanieIdn) {
		this.zadanieIdn = zadanieIdn;
	}

	public String getZadanieNazwa() {
		return zadanieNazwa;
	}

	public void setZadanieNazwa(String zadanieNazwa) {
		this.zadanieNazwa = zadanieNazwa;
	}

	public Long getZrodloId() {
		return zrodloId;
	}

	public void setZrodloId(Long zrodloId) {
		this.zrodloId = zrodloId;
	}

	public String getZrodloIdn() {
		return zrodloIdn;
	}

	public void setZrodloIdn(String zrodloIdn) {
		this.zrodloIdn = zrodloIdn;
	}

	public String getZrodloNazwa() {
		return zrodloNazwa;
	}

	public void setZrodloNazwa(String zrodloNazwa) {
		this.zrodloNazwa = zrodloNazwa;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
    


}
