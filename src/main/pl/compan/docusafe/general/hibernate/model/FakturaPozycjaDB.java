package pl.compan.docusafe.general.hibernate.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

@Entity
@Table(name="DSG_INVOICE_GENERAL_POZFAKT")
public class FakturaPozycjaDB {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@Column(name="POZYCJAFAKPOLEBUDZET")
	private Long pozycjaBudzetu;

	@ManyToOne
	@JoinColumn(name="STAWKA")
	private VatRateDB stawkaVat;
	
	@Column(name="KWOTANETTO")
	private BigDecimal kwotaNetto;
	
	@Column(name="KWOTAVAT")
	private BigDecimal kwotaVat;
	
	@Column(name="KWOTABRUTTO")
	private BigDecimal kwotaBrutto;
	
	@ManyToOne
	@JoinColumn(name="OPK", referencedColumnName="erp_id")
	private OpkDB opk;
	
	@ManyToOne
	@JoinColumn(name="PRZEZNACZENIE")
	private PrzeznaczenieZakupuDB przeznaczenieZakupu;
	
	@ManyToOne
	@JoinColumn(name="POZFAKTURY")
	private CostInvoiceProductDB produktyFaktury;

	@ManyToOne
	@JoinColumn(name="POZYCJA_PROJEKT", referencedColumnName="erpId")
	private ProjectDB funduszProjektowy;
	
	@ManyToOne
	@JoinColumn(name="ZLECENIE", referencedColumnName="erpId")
	private ProductionOrderDB zlecenie;
	
	@ManyToOne
	@JoinColumn(name="ZRODLO", referencedColumnName="erpId")
	private SourcesOfProjectFundingDB zrodloFinansowania;
	
	@Column(name="pozycjaZamowieniaId")
	private Long pozycjaZamowieniaId;
	
	@Column(name="documentId")
	private Long documentId;
	
	@ManyToOne
	@JoinColumn(name="wniosekORezerwacje", referencedColumnName="id")
	private WniosekORezerwacjeDB wniosekORezerwacje;
	
	@ManyToOne
	@JoinColumn(name="ZRODLOZAWPROJ", referencedColumnName="erpId")
	private ZrodloDB ZrodloZawProj;
	
	@ManyToOne
	@JoinColumn(name="POZYCJAPROJDOZROD", referencedColumnName="erpId")
	private ProjectDB PozycjaProjDdoZrod;
	
	
	public FakturaPozycjaDB(){}

    public FakturaPozycjaDB find(Long key) throws EdmException {
        return (FakturaPozycjaDB) DSApi.context().session().get(FakturaPozycjaDB.class, key);
    }

	public VatRateDB getStawkaVat() {
		return stawkaVat;
	}

	public void setStawkaVat(VatRateDB stawkaVat) {
		this.stawkaVat = stawkaVat;
	}

	public BigDecimal getKwotaNetto() {
		return kwotaNetto;
	}

	public void setKwotaNetto(BigDecimal kwotaNetto) {
		this.kwotaNetto = kwotaNetto;
	}

	public BigDecimal getKwotaVat() {
		return kwotaVat;
	}

	public void setKwotaVat(BigDecimal kwotaVat) {
		this.kwotaVat = kwotaVat;
	}

	public BigDecimal getKwotaBrutto() {
		return kwotaBrutto;
	}

	public void setKwotaBrutto(BigDecimal kwotaBrutto) {
		this.kwotaBrutto = kwotaBrutto;
	}

	public OpkDB getOpk() {
		return opk;
	}

	public void setOpk(OpkDB opk) {
		this.opk = opk;
	}

	public CostInvoiceProductDB getProduktyFaktury() {
		return produktyFaktury;
	}

	public void setProduktyFaktury(CostInvoiceProductDB produktyFaktury) {
		this.produktyFaktury = produktyFaktury;
	}

	public PrzeznaczenieZakupuDB getPrzeznaczenieZakupu() {
		return przeznaczenieZakupu;
	}

	public void setPrzeznaczenieZakupu(PrzeznaczenieZakupuDB przeznaczenieZakupu) {
		this.przeznaczenieZakupu = przeznaczenieZakupu;
	}

	public ProjectDB getFunduszProjektowy() {
		return funduszProjektowy;
	}

	public void setFunduszProjektowy(ProjectDB funduszProjektowy) {
		this.funduszProjektowy = funduszProjektowy;
	}

	public Long getPozycjaBudzetu() {
		return pozycjaBudzetu;
	}

	public void setPozycjaBudzetu(Long pozycjaBudzetu) {
		this.pozycjaBudzetu = pozycjaBudzetu;
	}
	
	public ProductionOrderDB getZlecenie() {
		return zlecenie;
	}

	public void setZlecenie(ProductionOrderDB zlecenie) {
		this.zlecenie = zlecenie;
	}

	public SourcesOfProjectFundingDB getZrodloFinansowania() {
		return zrodloFinansowania;
	}

	public void setZrodloFinansowania(SourcesOfProjectFundingDB zrodloFinansowania) {
		this.zrodloFinansowania = zrodloFinansowania;
	}

	public Long getPozycjaZamowieniaId() {
		return pozycjaZamowieniaId;
	}

	public void setPozycjaZamowieniaId(Long pozycjaZamowieniaId) {
		this.pozycjaZamowieniaId = pozycjaZamowieniaId;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public WniosekORezerwacjeDB getWniosekORezerwacje() {
		return wniosekORezerwacje;
	}

	public void setWniosekORezerwacje(WniosekORezerwacjeDB wniosekORezerwacje) {
		this.wniosekORezerwacje = wniosekORezerwacje;
	}

	public ZrodloDB getZrodloZawProj() {
		return ZrodloZawProj;
	}

	public void setZrodloZawProj(ZrodloDB zrodloZawProj) {
		ZrodloZawProj = zrodloZawProj;
	}

	public ProjectDB getPozycjaProjDdoZrod() {
		return PozycjaProjDdoZrod;
	}

	public void setPozycjaProjDdoZrod(ProjectDB pozycjaProjDdoZrod) {
		PozycjaProjDdoZrod = pozycjaProjDdoZrod;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
