package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;

public class StrukturaOrganizacyjnaDBFactory {

	/**
	 * Metoda pobiera list� JednostkaOrganizacyjna i zapisuje ja do bazy. Na
	 * pocz�tku nast�puje zmiana wszystkich "obcych" jendostek na status
	 * "nie aktywny" - nie usuwamy �adnych jednostek Nast�pnie ka�dy element
	 * listy ma status: NOWY, ZMODYFIKOWANY NOWY: a) budujemy obiekt DSDivision,
	 * b) zapisujemy do bazy danych ZMODYFIKOWANY: a) wczytujemy obiekt z bazy
	 * danych o danym zewnetrznym Id b) modyfikujemy dane (poza id i idrodzica)
	 * c) zapisujemy do bazy danych W ostatnim kroku pobieramy ka�dy obiekt i
	 * aktualizujemy jego parentId
	 * 
	 * Jako parametr podajemy liste JednostkaOrganizacyjna z aktualnymi
	 * nastepujacymi danymi: - // Identyfikator kom�rki private long id; //
	 * Przechowuje identyfikator rodzica private Long rodzicId; // JO mo�e by�
	 * nieaktywna, nie u�ywane kom�rki s� dezaktywowane a nie // usuwane private
	 * boolean aktywna; // JO Kadrowa private boolean kadrowa; // JO Kosztowa
	 * private boolean kosztowa; // JO - oddzia� private boolean oddzial; //
	 * Opis kom�rki organizacyjnej private String opis; // Nazwa JO private
	 * String nazwa; // Kod kom�rki private String kod; // Zmienna okre�la czy
	 * miejscem powstania jednostki by� ERP czy system // DocuSafe (np.
	 * wprowadzona z palca) private RODZAJ_JEDNOSTKI_ORGANIZACYJNEJ
	 * rodzajJednostki; // Oznaczenie stanu obiektu (nowy, zmodyfikowany,
	 * usuni�ty) private ObjectUtils.STATUS_OBIEKTU stanObiektu; // Zmienna
	 * pozwala na wpisanie ID innego systemu private String idn; // Przechowuje
	 * zewn�trzny identyfikator rodzica private String rodzicIdn; // Pole Guid
	 * private String guid;
	 * 
	 * @param aktualnaStruktura
	 */
	public void ZapiszStruktureDoBD(List<JednostkaOrganizacyjna> aktualnaStruktura) {
		// Zmiana wszystkich "obcych" jendostek na status "nie aktywny" - nie
		// usuwamy �adnych jednostek

		DSDivisionFactory dsFactory = new DSDivisionFactory();

		List<DSDivision> listaDivision;
		try {

			DSApi.openContextIfNeeded();
			DSApi.context().begin();

			listaDivision = DSDivision.findDivisionWithExternalId();

			for (DSDivision dv : listaDivision) {
				dv.setHidden(true);
				Persister.update(dv);
			}
			// Zapisujemy w bazie zmiany
			DSApi.context().commit();

			// Otwieramy nowy kontekst
			DSApi.context().begin();

			// Dla ka�dej JednostkiOrganizacyjnej wstawiam do bazy danych nowy
			// obiekt lub go aktualizuj�
			for (JednostkaOrganizacyjna jo : aktualnaStruktura) {
				if (jo.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					DSDivision nowyDS = dsFactory.getDSDivisionInstance();
					nowyDS = CanonicalToHibernateMapper
							.MapJednostkaOrganizacyjnaNaDSDivision(jo, nowyDS);
					Persister.create(nowyDS);
				} else if (jo.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
					// Wyszukujemy w bazie na podstawie externalId
					DSDivision aktualnyDS = DSDivision.findByExternalId(jo
							.getIdn());
				
					// Odkrywamy jednostki
					aktualnyDS.setHidden(false);
					
					aktualnyDS = CanonicalToHibernateMapper
							.MapJednostkaOrganizacyjnaNaDSDivision(jo,
									aktualnyDS);
								
					
					Persister.update(aktualnyDS);
				}
			}

			// Zapisujemy nowe i zmodyfikowane kom�rki
			DSApi.context().commit();

			// Otwieramy nowy kontekst
			DSApi.context().begin();

			// Teraz lec� jeszcze raz po li�cie i aktualizuj� w bazie danych
			// parentId
			for (JednostkaOrganizacyjna jo : aktualnaStruktura) {

				if (jo.getRodzicIdn() != null) {
					// Wyszukaj obiekt w bazie na podstawie externalId
					DSDivision obiekt = DSDivision
							.findByExternalId(jo.getIdn());

					// Wyszukaj rodzica
					DSDivision rodzic = DSDivision.findByExternalId(jo
							.getRodzicIdn());

					if (obiekt != null && rodzic != null) {
						if (obiekt.getGuid() == null) {
							Log.debug("obiekt ma guid=null");
						}
						// aktualizacja rodzica
						obiekt.setParent(rodzic);
						Persister.update(obiekt);
					}
				}
			}

			// Zapisujemy zmiany
			DSApi.context().commit();

		} catch (DivisionNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
