package pl.compan.docusafe.general.hibernate.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.BudzetPozycjaSimpleDB;

public class BudzetPozycjaSimpleDBDAO {

	
	public static List<BudzetPozycjaSimpleDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(BudzetPozycjaSimpleDB.class)
				.list();
	}
	
	public static List<BudzetPozycjaSimpleDB> findByErpId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(BudzetPozycjaSimpleDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	public static BudzetPozycjaSimpleDB findByErpIdUnique(Long id)
			throws EdmException {
		return (BudzetPozycjaSimpleDB) DSApi.context().session().createCriteria(BudzetPozycjaSimpleDB.class)
				.add(Restrictions.eq("erpId", id)).uniqueResult();
	}
	
	public static List<BudzetPozycjaSimpleDB> findByParent_id(Long parent_id)
			throws EdmException {
		return DSApi.context().session().createCriteria(BudzetPozycjaSimpleDB.class)
				.add(Restrictions.eq("parent_id", parent_id)).list();
	}
	
	public static BudzetPozycjaSimpleDB findById(Long id) throws EdmException {
		BudzetPozycjaSimpleDB status = (BudzetPozycjaSimpleDB) DSApi.getObject(
				BudzetPozycjaSimpleDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(BudzetPozycjaSimpleDB.class, id);

		return status;
	}
	
	public static void save(BudzetPozycjaSimpleDB budzetKO) throws EdmException {
		try {
			DSApi.context().session().save(budzetKO);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(BudzetPozycjaSimpleDB budzetKO) throws EdmException {
		try {
			DSApi.context().session().merge(budzetKO);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(BudzetPozycjaSimpleDB.class)+" set available='0'");
		ps.executeUpdate();
	}
}
