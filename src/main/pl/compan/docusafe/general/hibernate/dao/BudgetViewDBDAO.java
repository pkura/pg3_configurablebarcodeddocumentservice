package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.general.hibernate.model.BudgetViewDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BudgetViewDBDAO {
	private static Logger log = LoggerFactory.getLogger(BudgetViewDBDAO.class);

	public static BudgetViewDB findModelById(Long id) {
		BudgetViewDB wynik = null;
		boolean contextOpened = false;

		try {
			contextOpened = DSApi.openContextIfNeeded();
			Criteria criteria = DSApi.context().session().createCriteria(BudgetViewDB.class);
			criteria.add(Restrictions.idEq(id));
			wynik = (BudgetViewDB) criteria.uniqueResult();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}

		return wynik;
	}
	
	public static List<BudgetViewDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(BudgetViewDB.class)
				.addOrder(Order.asc("bdBudzetIdm")).list();
	}

	public static BudgetViewDB findByBudzetIdAndZrodloId(double bdBudzetId, double zrodloId) {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(BudgetViewDB.class);
			criteria.add(Restrictions.eq("bdBudzetId", bdBudzetId));
			criteria.add(Restrictions.eq("zrodloId", zrodloId));
			return (BudgetViewDB) criteria.setMaxResults(1).uniqueResult();
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}
	public static void save(BudgetViewDB budgetViewDB) throws EdmException {
		try {
			DSApi.context().session().save(budgetViewDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(BudgetViewDB budgetViewDB) throws EdmException {
		try {
			DSApi.context().session().merge(budgetViewDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
