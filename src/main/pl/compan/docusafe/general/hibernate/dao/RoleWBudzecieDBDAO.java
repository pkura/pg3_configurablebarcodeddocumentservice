package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.RoleWBudzecieDB;

public class RoleWBudzecieDBDAO {

	public static List<RoleWBudzecieDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(RoleWBudzecieDB.class)
				.list();
	}

	/**
	 * wyszukuje role w budzecie po bp_budzet_ko_id, bp_rola_id, osoba_id
	 * @param budzetId
	 * @param rolaId
	 * @param osobaId
	 * @return
	 * @throws EdmException
	 */
	public static List<RoleWBudzecieDB> findByBudzetIdRolaOsoba(double budzetId, double rolaId, long osobaId)
			throws EdmException {
		return DSApi.context().session().createCriteria(RoleWBudzecieDB.class)
				.add(Restrictions.eq("bd_budzet_ko_id", budzetId))
				.add(Restrictions.eq("bp_rola_id", rolaId))
				.add(Restrictions.eq("osoba_id", osobaId)).list();
	}

	public static RoleWBudzecieDB find(Long id) throws EdmException {
		RoleWBudzecieDB status = (RoleWBudzecieDB) DSApi.getObject(
				RoleWBudzecieDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(RoleWBudzecieDB.class, id);

		return status;
	}

	public static void save(RoleWBudzecieDB roleWBudzecieDB) throws EdmException {
		try {
			DSApi.context().session().save(roleWBudzecieDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void update(RoleWBudzecieDB roleWBudzecieDB) throws EdmException {
		try {
			DSApi.context().session().merge(roleWBudzecieDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws EdmException {
		List<RoleWBudzecieDB>role= list();
		for(RoleWBudzecieDB rola:role){
			rola.setAvailable(false);
			update(rola);
		}
	}
}
