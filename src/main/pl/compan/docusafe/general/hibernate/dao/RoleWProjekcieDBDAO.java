package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.RoleWProjekcieDB;

public class RoleWProjekcieDBDAO {

	public static List<RoleWProjekcieDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(RoleWProjekcieDB.class)
				.list();
	}
	
	public static List<RoleWProjekcieDB> findByRolaKontraktId(double id)
			throws EdmException {
		return DSApi.context().session().createCriteria(RoleWProjekcieDB.class)
				.add(Restrictions.eq("bp_rola_kontrakt_id", id)).list();
	}
	
	public static RoleWProjekcieDB find(Long id) throws EdmException {
		RoleWProjekcieDB status = (RoleWProjekcieDB) DSApi.getObject(
				RoleWProjekcieDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(RoleWProjekcieDB.class, id);

		return status;
	}
	
	public static void save(RoleWProjekcieDB roleWProjekcieDB) throws EdmException {
		try {
			DSApi.context().session().save(roleWProjekcieDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(RoleWProjekcieDB roleWProjekcieDB) throws EdmException {
		try {
			DSApi.context().session().merge(roleWProjekcieDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
