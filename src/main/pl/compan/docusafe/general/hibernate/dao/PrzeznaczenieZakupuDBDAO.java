package pl.compan.docusafe.general.hibernate.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.hibernate.model.PrzeznaczenieZakupuDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PrzeznaczenieZakupuDBDAO{
	private static Logger log = LoggerFactory.getLogger(PrzeznaczenieZakupuDBDAO.class);
	
	public static PrzeznaczenieZakupuDB findModelById(Long id) {
		PrzeznaczenieZakupuDB wynik = null;
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			Criteria criteria = DSApi.context().session().createCriteria(PrzeznaczenieZakupuDB.class);
			criteria.add(Restrictions.idEq(id));
			wynik = (PrzeznaczenieZakupuDB) criteria.uniqueResult();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
        
        return wynik;
	}
	
	public static void save(PrzeznaczenieZakupuDB przeznaczenieZakupu){
		try {
			DSApi.context().session().save(przeznaczenieZakupu);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	public static void update(PrzeznaczenieZakupuDB przeznaczenieZakupu){
		try {
			DSApi.context().session().update(przeznaczenieZakupu);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
}