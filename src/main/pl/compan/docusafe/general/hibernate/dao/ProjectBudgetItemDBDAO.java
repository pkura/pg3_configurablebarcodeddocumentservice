package pl.compan.docusafe.general.hibernate.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetEtapDB;

public class ProjectBudgetItemDBDAO {
	public static List<ProjektBudzetEtapDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(ProjektBudzetEtapDB.class)
				.addOrder(Order.asc("nazwa")).list();
	}

	public static ProjektBudzetEtapDB find(Long id) throws EdmException {
		ProjektBudzetEtapDB status = (ProjektBudzetEtapDB) DSApi.getObject(
				ProjektBudzetEtapDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(ProjektBudzetEtapDB.class, id);

		return status;
	}

	public static List<ProjektBudzetEtapDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(ProjektBudzetEtapDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}

	public static void save(ProjektBudzetEtapDB projectBudgetItemDB) throws EdmException {
		try {
			DSApi.context().session().save(projectBudgetItemDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(ProjektBudzetEtapDB projectBudgetItemDB) throws EdmException {
		try {
			DSApi.context().session().merge(projectBudgetItemDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(ProjektBudzetEtapDB.class)+" set available='0'");
		ps.executeUpdate();
		
	}
}
