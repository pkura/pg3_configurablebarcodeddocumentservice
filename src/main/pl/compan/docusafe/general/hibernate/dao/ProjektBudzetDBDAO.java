package pl.compan.docusafe.general.hibernate.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetDB;

public class ProjektBudzetDBDAO {
	public static List<ProjektBudzetDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(ProjektBudzetDB.class)
				.addOrder(Order.asc("nazwa")).list();
	}

	public static ProjektBudzetDB find(Long id) throws EdmException {
		ProjektBudzetDB status = (ProjektBudzetDB) DSApi.getObject(
				ProjektBudzetDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(ProjektBudzetDB.class, id);

		return status;
	}

	public static List<ProjektBudzetDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(ProjektBudzetDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}

	public static void save(ProjektBudzetDB projectBugdetDB) throws EdmException {
		try {
			DSApi.context().session().save(projectBugdetDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(ProjektBudzetDB projectBugdetDB) throws EdmException {
		try {
			DSApi.context().session().merge(projectBugdetDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(ProjektBudzetDB.class)+" set available='0'");
		ps.executeUpdate();
		
	}
}
