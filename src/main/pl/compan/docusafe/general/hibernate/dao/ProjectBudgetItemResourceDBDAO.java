package pl.compan.docusafe.general.hibernate.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.BudzetPozycjaSimpleDB;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetZasobDB;

public class ProjectBudgetItemResourceDBDAO {
	public static List<ProjektBudzetZasobDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(ProjektBudzetZasobDB.class)
				.addOrder(Order.asc("nazwa")).list();
	}

	public static ProjektBudzetZasobDB find(Long id) throws EdmException {
		ProjektBudzetZasobDB status = (ProjektBudzetZasobDB) DSApi.getObject(
				ProjektBudzetZasobDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(ProjektBudzetZasobDB.class, id);

		return status;
	}

	public static List<ProjektBudzetZasobDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(ProjektBudzetZasobDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}

	public static void save(ProjektBudzetZasobDB projectBudgetItemResourceDB) throws EdmException {
		try {
			DSApi.context().session().save(projectBudgetItemResourceDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(ProjektBudzetZasobDB projectBudgetItemResourceDB) throws EdmException {
		try {
			DSApi.context().session().merge(projectBudgetItemResourceDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(ProjektBudzetZasobDB.class)+" set available='0'");
		ps.executeUpdate();
		
	}
}
