package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.general.hibernate.model.SzablonyRozliczenFakturDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SzablonyRozliczenFakturDBDAO {

	private static Logger log = LoggerFactory.getLogger(BudzetDoKomorkiDBDAO.class);

	public static SzablonyRozliczenFakturDB findSzablonById(Long id) {
		SzablonyRozliczenFakturDB wynik = null;
		boolean contextOpened = false;

		try {
			contextOpened = DSApi.openContextIfNeeded();
			Criteria criteria = DSApi.context().session().createCriteria(SzablonyRozliczenFakturDB.class);
			criteria.add(Restrictions.idEq(id));
			wynik = (SzablonyRozliczenFakturDB) criteria.uniqueResult();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}

		return wynik;
	}
	
	public static List<SzablonyRozliczenFakturDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(SzablonyRozliczenFakturDB.class)
				.addOrder(Order.asc("erpId")).list();
	}


	public static void save(SzablonyRozliczenFakturDB budgetViewDB) throws EdmException {
		try {
			DSApi.context().session().save(budgetViewDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static List<SzablonyRozliczenFakturDB> findByErpId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(SzablonyRozliczenFakturDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	
	public static void update(SzablonyRozliczenFakturDB budgetViewDB) throws EdmException {
		try {
			DSApi.context().session().merge(budgetViewDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
