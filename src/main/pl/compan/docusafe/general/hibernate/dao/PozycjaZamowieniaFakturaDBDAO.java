package pl.compan.docusafe.general.hibernate.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.general.hibernate.model.PozycjaZamowieniaFakturaDB;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PozycjaZamowieniaFakturaDBDAO implements InvoiceGeneralConstans {
	private static Logger log = LoggerFactory.getLogger(PozycjaZamowieniaFakturaDBDAO.class);
	
	public static List<PozycjaZamowieniaFakturaDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(PozycjaZamowieniaFakturaDB.class)
				.list();
	}
	
	public static List<PozycjaZamowieniaFakturaDB> findByDocumentId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(PozycjaZamowieniaFakturaDB.class)
				.add(Restrictions.eq("DOCUMENT_ID", id)).list();
	}
	public static PozycjaZamowieniaFakturaDB findById(Long id)
			throws EdmException {
		return (PozycjaZamowieniaFakturaDB) DSApi.context().session().createCriteria(PozycjaZamowieniaFakturaDB.class)
				.add(Restrictions.eq("ID", id)).uniqueResult();
	}
	
	public static List<PozycjaZamowieniaFakturaDB> findByDocumentIdAndDocZamId(Long documentId,Long zamId)
			throws EdmException {
		return DSApi.context().session().createCriteria(PozycjaZamowieniaFakturaDB.class)
				.add(Restrictions.eq("DOCUMENT_ID", documentId)).add(Restrictions.eq("DOCUMENT_ID_ZAMOWIENIA", zamId)).list();
	}
	
	public static void save(PozycjaZamowieniaFakturaDB planZakupowDB) throws EdmException {
		try {
			DSApi.context().session().save(planZakupowDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(PozycjaZamowieniaFakturaDB pozycjaZamowieniaFakturDB) throws EdmException {
		try {
			DSApi.context().begin();
			DSApi.context().session().merge(pozycjaZamowieniaFakturDB);
			DSApi.context().session().clear();
			DSApi.context().session().update(pozycjaZamowieniaFakturDB);
			DSApi.context().session().flush();
			DSApi.context().commit();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void remove(PozycjaZamowieniaFakturaDB fpDB) {
			try {
				DSApi.context().prepareStatement("delete from "+TABELA_POZYCJE_ZAMOWIENIA+" where id="+fpDB.getID().toString()).execute();
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
			}

		
	}
}
