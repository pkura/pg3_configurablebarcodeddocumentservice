package pl.compan.docusafe.general.hibernate.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.SourcesOfProjectFundingDB;

public class SourcesOfProjectFundingDBDAO {
	public static List<SourcesOfProjectFundingDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(SourcesOfProjectFundingDB.class)
				.addOrder(Order.asc("zrodlo_nazwa")).list();
	}

	public static SourcesOfProjectFundingDB find(Long id) throws EdmException {
		SourcesOfProjectFundingDB status = (SourcesOfProjectFundingDB) DSApi.getObject(
				SourcesOfProjectFundingDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(SourcesOfProjectFundingDB.class, id);

		return status;
	}

	public static List<SourcesOfProjectFundingDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(SourcesOfProjectFundingDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	
	public static SourcesOfProjectFundingDB findByErpIDUnique(Long id)
			throws EdmException {
		return (SourcesOfProjectFundingDB) DSApi.context().session().createCriteria(SourcesOfProjectFundingDB.class)
				.add(Restrictions.eq("erpId", id)).uniqueResult();
	}
	public static SourcesOfProjectFundingDB findByZrodloIdUnique(Double id)
			throws EdmException {
		return (SourcesOfProjectFundingDB) DSApi.context().session().createCriteria(SourcesOfProjectFundingDB.class)
				.add(Restrictions.eq("zrodlo_id", id)).uniqueResult();
	}
	public static void save(SourcesOfProjectFundingDB sourcesOfProjectFundingDB) throws EdmException {
		try {
			DSApi.context().session().save(sourcesOfProjectFundingDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(SourcesOfProjectFundingDB sourcesOfProjectFundingDB) throws EdmException {
		try {
			DSApi.context().session().merge(sourcesOfProjectFundingDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(SourcesOfProjectFundingDB.class)+" set available='0'");
		ps.executeUpdate();
		
	}
}
