package pl.compan.docusafe.general.hibernate.dao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.hibernate.model.RealizacjaBudzetuDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RealizacjaBudzetuDBDAO{
	private static Logger log = LoggerFactory.getLogger(RealizacjaBudzetuDBDAO.class);
	
	public static RealizacjaBudzetuDB findModelById(Long id) {
		RealizacjaBudzetuDB wynik = null;
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			Criteria criteria = DSApi.context().session().createCriteria(RealizacjaBudzetuDB.class);
			criteria.add(Restrictions.idEq(id));
			wynik = (RealizacjaBudzetuDB) criteria.uniqueResult();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
        
        return wynik;
	}
	
	public static void save(RealizacjaBudzetuDB realizacjaBudzetu){
		try {
			DSApi.context().session().save(realizacjaBudzetu);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	public static void update(RealizacjaBudzetuDB realizacjaBudzetu){
		try {
			DSApi.context().session().update(realizacjaBudzetu);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
}