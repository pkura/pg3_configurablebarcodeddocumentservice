package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.ProductionOrderDB;

public class ProductionOrderDBDAO {
	
	public static List<ProductionOrderDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(ProductionOrderDB.class)
				.addOrder(Order.asc("title")).list();
	}

	public static ProductionOrderDB find(Long id) throws EdmException {
		ProductionOrderDB status = (ProductionOrderDB) DSApi.getObject(
				ProductionOrderDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(ProductionOrderDB.class, id);

		return status;
	}

	public static List<ProductionOrderDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(ProductionOrderDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}

	public static void save(ProductionOrderDB productionOrderDB) throws EdmException {
		try {
			DSApi.context().session().save(productionOrderDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(ProductionOrderDB productionOrderDB) throws EdmException {
		try {
			DSApi.context().session().merge(productionOrderDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws EdmException {
		List<ProductionOrderDB> zlecenia=list();
		for(ProductionOrderDB zlDB:zlecenia){
			zlDB.setAvailable(false);
			update(zlDB);
		}

	}
}
