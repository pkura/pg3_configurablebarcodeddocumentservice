package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.TypyUrlopowDB;

public class TypyUrlopowDBDAO {
	public static List<TypyUrlopowDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(TypyUrlopowDB.class)
				.addOrder(Order.asc("opis")).list();
	}

	public static TypyUrlopowDB find(Long id) throws EdmException {
		TypyUrlopowDB status = (TypyUrlopowDB) DSApi.getObject(
				TypyUrlopowDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(TypyUrlopowDB.class, id);

		return status;
	}

	public static List<TypyUrlopowDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(TypyUrlopowDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}

	public static void save(TypyUrlopowDB typyUrlopowDB) throws EdmException {
		try {
			DSApi.context().session().save(typyUrlopowDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(TypyUrlopowDB typyUrlopowDB) throws EdmException {
		try {
			DSApi.context().session().merge(typyUrlopowDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
