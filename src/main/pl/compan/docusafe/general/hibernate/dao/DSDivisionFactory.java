package pl.compan.docusafe.general.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa
 * 
 * @author Gubari Nanokata
 * 
 */
public class DSDivisionFactory {

	private static Logger log = LoggerFactory
			.getLogger(DSDivisionFactory.class);

	/**
	 * Metoda zwraca wszystkie DSDivision z bazy danych Docusafe
	 * 
	 * @return
	 */
	public List<DSDivision> getDsDivisions() {
		try {
			DSDivision[] tablicaDivision = DSDivision.getAllDivisions();

			List<DSDivision> _listaDivision = MapDSDivisionArrayToList(tablicaDivision);

			return _listaDivision;

		} catch (EdmException e) {
			// TODO Auto-generated catch block
			log.debug(e);
		}

		return null;
	}

	/**
	 * Metoda zamienia tablic� obiekt�w na list� obiekt�w
	 * 
	 * @param tablica
	 * @return
	 */
	public List<DSDivision> MapDSDivisionArrayToList(DSDivision[] tablica) {
		List<DSDivision> lista = new ArrayList<DSDivision>();

		for (DSDivision obj : tablica) {
			lista.add(obj);
		}

		return lista;
	}

	/**
	 * Metoda zwraca instancj� obiektu DSDivision zainicjowan� minimaln� porcj�
	 * danych
	 * 
	 * @return
	 */
	public DSDivision getDSDivisionInstance() {
		// Tworzymy nowy obiekt
		DSDivision dv = new DivisionImpl();
		return dv;
	}		
}
