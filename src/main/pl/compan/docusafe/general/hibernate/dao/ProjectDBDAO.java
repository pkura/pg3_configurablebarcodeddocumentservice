package pl.compan.docusafe.general.hibernate.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.ProjectDB;

public class ProjectDBDAO {

	public static List<ProjectDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(ProjectDB.class)
				.addOrder(Order.asc("title")).list();
	}

	public static ProjectDB find(Long id) throws EdmException {
		ProjectDB status = (ProjectDB) DSApi.getObject(
				ProjectDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(ProjectDB.class, id);

		return status;
	}

	public static List<ProjectDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(ProjectDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	public static ProjectDB findByErpIDUnique(Long id)
			throws EdmException {
		return (ProjectDB) DSApi.context().session().createCriteria(ProjectDB.class)
				.add(Restrictions.eq("erpId", id)).uniqueResult();
	}
	public static void save(ProjectDB projectDB) throws EdmException {
		try {
			DSApi.context().session().save(projectDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(ProjectDB projectDB) throws EdmException {
		try {
			DSApi.context().session().merge(projectDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(ProjectDB.class)+" set available='0'");
		ps.executeUpdate();
		
	}
}
