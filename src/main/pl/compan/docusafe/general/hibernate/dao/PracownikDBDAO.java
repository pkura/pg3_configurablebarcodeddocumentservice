package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.PracownikDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PracownikDBDAO {
	private static Logger log = LoggerFactory.getLogger(PracownikDBDAO.class);

	public static List<PracownikDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(PracownikDB.class)
				.addOrder(Order.asc("imie")).list();
	}

	public static PracownikDB find(Long id) throws EdmException {
		PracownikDB status = (PracownikDB) DSApi.getObject(
				PracownikDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(PracownikDB.class, id);

		return status;
	}

	public static List<PracownikDB> findByNREWID(int id)
			throws EdmException {
		return DSApi.context().session().createCriteria(PracownikDB.class)
				.add(Restrictions.eq("nrewid", id)).list();
	}

	public static void save(PracownikDB pracownikDB) throws EdmException {
		try {
			DSApi.context().session().save(pracownikDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(PracownikDB pracownikDB) throws EdmException {
		try {
			DSApi.context().session().merge(pracownikDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
