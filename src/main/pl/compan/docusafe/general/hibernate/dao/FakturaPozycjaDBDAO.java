package pl.compan.docusafe.general.hibernate.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.durables.Durable;

import pl.compan.docusafe.general.hibernate.model.FakturaPozycjaDB;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class FakturaPozycjaDBDAO implements InvoiceGeneralConstans {
	private static Logger log = LoggerFactory.getLogger(FakturaPozycjaDBDAO.class);
	
	public static FakturaPozycjaDB findModelById(Long id) {
		FakturaPozycjaDB wynik = null;
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			Criteria criteria = DSApi.context().session().createCriteria(FakturaPozycjaDB.class);
			criteria.add(Restrictions.idEq(id));
			wynik = (FakturaPozycjaDB) criteria.uniqueResult();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
        
        return wynik;
	}
	
	public static ResultSet findValuesById(Long id){
		ResultSet wynik = null;
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			wynik = DSApi.context().prepareStatement("SELECT * FROM DSG_INVOICE_GENERAL_POZFAKT WHERE id="+id).executeQuery();
			wynik.next();
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
        
        return wynik;
	}
	
	public static ResultSet findRepoValues(Long id, String tableName){
		ResultSet wynik = null;
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			wynik = DSApi.context().prepareStatement("SELECT * FROM "+tableName+" WHERE id="+id).executeQuery();
			wynik.next();
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
        
        return wynik;
	}
	
	public static FakturaPozycjaDB findBypozycjaZamowieniaIdIDocumentId( Long id,Long documentId)
			throws EdmException {
		return (FakturaPozycjaDB) DSApi.context().session().createCriteria(FakturaPozycjaDB.class)
				.add(Restrictions.eq("pozycjaZamowieniaId", id)).add(Restrictions.eq("documentId", documentId)).uniqueResult();
	}

	public static List<FakturaPozycjaDB> findByDocumentId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(FakturaPozycjaDB.class)
				.add(Restrictions.eq("documentId", id)).list();
	}
	
	public static void save(FakturaPozycjaDB fakturaPozycja){
		try {
			DSApi.context().session().save(fakturaPozycja);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	public static void update(FakturaPozycjaDB fakturaPozycja){
		try {
			DSApi.context().session().update(fakturaPozycja);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
	}
	public static void remove(FakturaPozycjaDB fakturaPozycja){
		try {
			DSApi.context().prepareStatement("delete from "+TABELA_POZYCJE_FAKTURY+" where id="+fakturaPozycja.getId()).execute();

		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}
	}

	public static void remove(Long id) {
		try {
			DSApi.context().prepareStatement("delete from "+TABELA_POZYCJE_FAKTURY+" where id="+id.toString()).execute();
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}

	}

	
}
