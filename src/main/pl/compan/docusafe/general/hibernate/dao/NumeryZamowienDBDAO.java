package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.NumeryZamowienDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class NumeryZamowienDBDAO {
	public static final Logger log = LoggerFactory.getLogger(NumeryZamowienDBDAO.class);
	
	public static List<NumeryZamowienDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(NumeryZamowienDB.class)
				.list();
	}
	
	public static List<NumeryZamowienDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(NumeryZamowienDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	public static NumeryZamowienDB findByErpIDUnique(Long id)
			throws EdmException {
		return (NumeryZamowienDB) DSApi.context().session().createCriteria(NumeryZamowienDB.class)
				.add(Restrictions.eq("erpId", id)).uniqueResult();
	}
	
	public static List<NumeryZamowienDB> findByDostawcaId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(NumeryZamowienDB.class)
				.add(Restrictions.eq("dostawca_id", id)).list();
	}
	
	public static NumeryZamowienDB find(Long id) throws EdmException {
		NumeryZamowienDB status = (NumeryZamowienDB) DSApi.getObject(
				NumeryZamowienDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(NumeryZamowienDB.class, id);

		return status;
	}
	
	public static void save(NumeryZamowienDB numeryZamowienDB) throws EdmException {
		try {
			DSApi.context().session().save(numeryZamowienDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(NumeryZamowienDB numeryZamowienDB) throws EdmException {
		try {
			DSApi.context().session().merge(numeryZamowienDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() {
		try {
			List<NumeryZamowienDB> wnioski=list();
			for(NumeryZamowienDB wnDB:wnioski){
				wnDB.setAvailable(false);
				update(wnDB);
			}
		} catch (EdmException e) {
			log.error("",e);
		}
		
	}
}
