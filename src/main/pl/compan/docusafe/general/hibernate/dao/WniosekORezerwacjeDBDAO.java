package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjeDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Tomasz Nowak <tomasz.nowak@docusafe.pl>
 *
 */
public class WniosekORezerwacjeDBDAO {
	public static final Logger log = LoggerFactory.getLogger(WniosekORezerwacjeDBDAO.class);
	
	public static List<WniosekORezerwacjeDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(WniosekORezerwacjeDB.class)
				.addOrder(Order.asc("bdRezerwacjaIdm")).list();
	}

	public static WniosekORezerwacjeDB find(Long id) throws EdmException {
		WniosekORezerwacjeDB status = (WniosekORezerwacjeDB) DSApi.getObject(
				WniosekORezerwacjeDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(WniosekORezerwacjeDB.class, id);

		return status;
	}

	public static List<WniosekORezerwacjeDB> findby_bd_rezerwacja_id(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(WniosekORezerwacjeDB.class)
				.add(Restrictions.eq("bdRezerwacjaId", id)).list();
	}
	public static WniosekORezerwacjeDB findby_bd_rezerwacja_id_unique(Long id)
			throws EdmException {
		return (WniosekORezerwacjeDB) DSApi.context().session().createCriteria(WniosekORezerwacjeDB.class)
				.add(Restrictions.eq("bdRezerwacjaId", id)).uniqueResult();
	}
	
	public static void save(WniosekORezerwacjeDB requestForReservationDB) throws EdmException {
		try {
			DSApi.context().session().save(requestForReservationDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(WniosekORezerwacjeDB requestForReservationDB) throws EdmException {
		try {
			DSApi.context().session().merge(requestForReservationDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() {
		try {
			List<WniosekORezerwacjeDB> wnioski=list();
			for(WniosekORezerwacjeDB wnDB:wnioski){
				wnDB.setAvailable(false);
				update(wnDB);
			}
		} catch (EdmException e) {
			log.error("",e);
		}
		
	}
}
