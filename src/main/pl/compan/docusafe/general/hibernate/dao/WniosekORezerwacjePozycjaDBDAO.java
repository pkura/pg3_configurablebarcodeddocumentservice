package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjePozycjaDB;

public class WniosekORezerwacjePozycjaDBDAO {
	
	public static List<WniosekORezerwacjePozycjaDB> findby_Document_id(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(WniosekORezerwacjePozycjaDB.class)
				.add(Restrictions.eq("document_id", id)).list();
	}
	
}
