package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DSUserFactory {
	private static Logger log = LoggerFactory.getLogger(DSUserFactory.class);

	/**
	 * Metoda zwraca wszystkie DSUser z bazy danych Docusafe
	 * 
	 * @return
	 */
	public List<DSUser> getDSUsers() {
		try {
			List<DSUser> listaDsUser = DSUser.list(DSUser.SORT_NONE);
			
			return listaDsUser;

		} catch (EdmException e) {
			// TODO Auto-generated catch block
			log.debug(e);
		}
		return null;
	}

	

	/**
	 * Metoda zwraca instancj� obiektu DSUser zainicjowan� minimaln� porcj�
	 * danych
	 * 
	 * @return
	 */
	public DSUser getDSUserInstance() {
		// Tworzymy nowy obiekt
		DSUser du = new UserImpl();
		return du;
	}		
}
