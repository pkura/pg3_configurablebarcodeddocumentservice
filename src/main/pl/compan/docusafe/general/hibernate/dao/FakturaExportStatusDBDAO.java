package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.hibernate.model.FakturaExportStatusDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FakturaExportStatusDBDAO {
	private static Logger log = LoggerFactory.getLogger(FakturaExportStatusDBDAO.class);

	public static void save(FakturaExportStatusDB fakturaExportStatusDB){
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();

			DSApi.context().session().save(fakturaExportStatusDB);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
	}
	
	public static void update(FakturaExportStatusDB fakturaExportStatusDB){
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			DSApi.context().session().update(fakturaExportStatusDB);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
	}
	
	public static List<FakturaExportStatusDB> list(){
		List wynik = null;
		
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			wynik = DSApi.context().session().createCriteria(FakturaExportStatusDB.class).list();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} 
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
		
		return wynik;
	}
	
	public static void create(Long id, String guid){
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			FakturaExportStatusDB fakturaExportStatusDB = new FakturaExportStatusDB();
			fakturaExportStatusDB.setDocumentId(id);
			fakturaExportStatusDB.setGuid(guid);
			
			FakturaExportStatusDBDAO.save(fakturaExportStatusDB);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
	}
	
	public static void delete(FakturaExportStatusDB fakturaExportStatusDB){
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();

			DSApi.context().begin();
			DSApi.context().session().delete(fakturaExportStatusDB);
			DSApi.context().commit();
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
	}
}
