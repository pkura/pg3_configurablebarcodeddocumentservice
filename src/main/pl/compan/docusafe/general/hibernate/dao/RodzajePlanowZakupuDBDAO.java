package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.RodzajePlanowZakupuDB;

public class RodzajePlanowZakupuDBDAO {

	public static List<RodzajePlanowZakupuDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(RodzajePlanowZakupuDB.class)
				.list();
	}
	
	public static List<RodzajePlanowZakupuDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(RodzajePlanowZakupuDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	
	public static RodzajePlanowZakupuDB find(Long id) throws EdmException {
		RodzajePlanowZakupuDB status = (RodzajePlanowZakupuDB) DSApi.getObject(
				RodzajePlanowZakupuDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(RodzajePlanowZakupuDB.class, id);

		return status;
	}
	
	public static void save(RodzajePlanowZakupuDB rodzajePlanowZakupuDB) throws EdmException {
		try {
			DSApi.context().session().save(rodzajePlanowZakupuDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(RodzajePlanowZakupuDB rodzajePlanowZakupuDB) throws EdmException {
		try {
			DSApi.context().session().merge(rodzajePlanowZakupuDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
