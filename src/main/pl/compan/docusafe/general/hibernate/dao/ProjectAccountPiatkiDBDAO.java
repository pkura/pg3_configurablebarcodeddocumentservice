package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.ProjectAccountPiatkiDB;

public class ProjectAccountPiatkiDBDAO {

	public static List<ProjectAccountPiatkiDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(ProjectAccountPiatkiDB.class)
				.list();
	}
	
	public static List<ProjectAccountPiatkiDB> findByKontraktId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(ProjectAccountPiatkiDB.class)
				.add(Restrictions.eq("kontrakt_id", id)).list();
	}
	public static List<ProjectAccountPiatkiDB> findByKontraktIdAndAvailable(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(ProjectAccountPiatkiDB.class)
				.add(Restrictions.eq("kontrakt_id", id)).add(Restrictions.eq("available", true)).list();
	}
	public static List<ProjectAccountPiatkiDB> findByKontoIDoIKontraktIDIRepAtrybutIdo(String id,Long kontraktId,String repAtrybut)
			throws EdmException {
		return DSApi.context().session().createCriteria(ProjectAccountPiatkiDB.class)
				.add(Restrictions.eq("konto_ido", id)).add(Restrictions.eq("kontrakt_id", kontraktId)).add(Restrictions.eq("rep_atrybut_ido",repAtrybut)).list();
	}
	
	public static ProjectAccountPiatkiDB findById(Long id) throws EdmException {
		ProjectAccountPiatkiDB status = (ProjectAccountPiatkiDB) DSApi.getObject(
				ProjectAccountPiatkiDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(ProjectAccountPiatkiDB.class, id);

		return status;
	}
	
	public static void save(ProjectAccountPiatkiDB budzetKO) throws EdmException {
		try {
			DSApi.context().session().save(budzetKO);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(ProjectAccountPiatkiDB budzetKO) throws EdmException {
		try {
			DSApi.context().session().merge(budzetKO);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
