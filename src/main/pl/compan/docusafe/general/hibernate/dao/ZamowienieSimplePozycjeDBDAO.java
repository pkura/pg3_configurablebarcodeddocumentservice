package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.general.hibernate.model.ZamowienieSimplePozycjeDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZamowienieSimplePozycjeDBDAO {

	private static Logger log = LoggerFactory.getLogger(ZamowienieSimplePozycjeDBDAO.class);

	public static ZamowienieSimplePozycjeDB findById(Long id) {
		ZamowienieSimplePozycjeDB wynik = null;
		boolean contextOpened = false;

		try {
			contextOpened = DSApi.openContextIfNeeded();
			Criteria criteria = DSApi.context().session().createCriteria(ZamowienieSimplePozycjeDB.class);
			criteria.add(Restrictions.idEq(id));
			wynik = (ZamowienieSimplePozycjeDB) criteria.uniqueResult();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}

		return wynik;
	}
	
	public static List<ZamowienieSimplePozycjeDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(ZamowienieSimplePozycjeDB.class)
				.list();
	}

	public static List<ZamowienieSimplePozycjeDB> findByZamDosPozId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(ZamowienieSimplePozycjeDB.class)
				.add(Restrictions.eq("zamdospoz_id", id)).list();
	}
	public static ZamowienieSimplePozycjeDB findByZamDosPozIdUnique(Long id)
			throws EdmException {
		return (ZamowienieSimplePozycjeDB) DSApi.context().session().createCriteria(ZamowienieSimplePozycjeDB.class)
				.add(Restrictions.eq("zamdospoz_id", id)).uniqueResult();
	}
	public static List<ZamowienieSimplePozycjeDB> findBySupplierOrderId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(ZamowienieSimplePozycjeDB.class)
				.add(Restrictions.eq("supplier_order_id", id)).list();
	}
	
	public static void save(ZamowienieSimplePozycjeDB budgetViewDB) throws EdmException {
		try {
			DSApi.context().session().save(budgetViewDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	
	public static void update(ZamowienieSimplePozycjeDB budgetViewDB) throws EdmException {
		try {
			DSApi.context().session().merge(budgetViewDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
