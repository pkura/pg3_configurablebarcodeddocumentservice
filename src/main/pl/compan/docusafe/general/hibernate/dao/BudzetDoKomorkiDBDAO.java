package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.general.hibernate.model.BudzetDoKomorkiDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BudzetDoKomorkiDBDAO {
	private static Logger log = LoggerFactory.getLogger(BudzetDoKomorkiDBDAO.class);

	public static BudzetDoKomorkiDB findModelById(Long id) {
		BudzetDoKomorkiDB wynik = null;
		boolean contextOpened = false;

		try {
			contextOpened = DSApi.openContextIfNeeded();
			Criteria criteria = DSApi.context().session().createCriteria(BudzetDoKomorkiDB.class);
			criteria.add(Restrictions.idEq(id));
			wynik = (BudzetDoKomorkiDB) criteria.uniqueResult();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}

		return wynik;
	}
	
	public static List<BudzetDoKomorkiDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(BudzetDoKomorkiDB.class)
				.addOrder(Order.asc("budzet_id")).list();
	}


	public static void save(BudzetDoKomorkiDB budgetViewDB) throws EdmException {
		try {
			DSApi.context().session().save(budgetViewDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static List<BudzetDoKomorkiDB> findByBudzetId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(BudzetDoKomorkiDB.class)
				.add(Restrictions.eq("budzet_id", id)).list();
	}
	
	public static void update(BudzetDoKomorkiDB budgetViewDB) throws EdmException {
		try {
			DSApi.context().session().merge(budgetViewDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
