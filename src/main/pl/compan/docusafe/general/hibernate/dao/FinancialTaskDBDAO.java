package pl.compan.docusafe.general.hibernate.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.FinancialTaskDB;

public class FinancialTaskDBDAO {
	
	public static List<FinancialTaskDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(FinancialTaskDB.class)
				.addOrder(Order.asc("title")).list();
	}

	public static FinancialTaskDB find(Long id) throws EdmException {
		FinancialTaskDB status = (FinancialTaskDB) DSApi.getObject(
				FinancialTaskDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(FinancialTaskDB.class, id);

		return status;
	}

	public static List<FinancialTaskDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(FinancialTaskDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}

	public static void save(FinancialTaskDB financialTaskDB) throws EdmException {
		try {
			DSApi.context().session().save(financialTaskDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(FinancialTaskDB financialTaskDB) throws EdmException {
		try {
			DSApi.context().session().merge(financialTaskDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(FinancialTaskDB.class)+" set available='0'");
		ps.executeUpdate();
		
	}
}
