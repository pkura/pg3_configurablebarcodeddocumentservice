package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.general.hibernate.model.BudzetKOKomorekDB;
import pl.compan.docusafe.general.hibernate.model.FakturaDB;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FakturaDBDAO {
	private static Logger log = LoggerFactory.getLogger(FakturaDB.class);
	
	public static FakturaDB findModelById(Long id) {
		FakturaDB wynik = null;
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			Criteria criteria = DSApi.context().session().createCriteria(FakturaDB.class);
			criteria.add(Restrictions.idEq(id));
			wynik = (FakturaDB) criteria.uniqueResult();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
        
        return wynik;
	}
	
	public static void save(FakturaDB faktura){
		try {
			DSApi.context().session().save(faktura);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
	}
	
	public static void update(FakturaDB faktura) throws EdmException {
		try {
			DSApi.context().session().merge(faktura);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static List<FakturaDB> findByBarcode(String barc) {
		List<FakturaDB> wynik = null;
		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			Criteria criteria = DSApi.context().session().createCriteria(FakturaDB.class);
			criteria.add(Restrictions.eq(InvoiceGeneralConstans.BARCODE_FAKTURADB_CN,barc));
			wynik = (List<FakturaDB>) criteria.list();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
        
        return wynik;
	}
}
