package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.BudzetKOKomorekDB;

public class BudzetKOKomorkaDBDAO {

	public static List<BudzetKOKomorekDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(BudzetKOKomorekDB.class)
				.list();
	}
	
	public static List<BudzetKOKomorekDB> findByErpId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(BudzetKOKomorekDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	
	public static List<BudzetKOKomorekDB> findByBudzetIdN(String idn)
			throws EdmException {
		return DSApi.context().session().createCriteria(BudzetKOKomorekDB.class)
				.add(Restrictions.eq("budzet_idn", idn)).list();
	}
	public static List<BudzetKOKomorekDB> findByBudzetId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(BudzetKOKomorekDB.class)
				.add(Restrictions.eq("budzet_id", id)).list();
	}	
	public static BudzetKOKomorekDB findByBudzetIdUnique(Long id)
			throws EdmException {
		return (BudzetKOKomorekDB) DSApi.context().session().createCriteria(BudzetKOKomorekDB.class)
				.add(Restrictions.eq("budzet_id", id)).uniqueResult();
	}	
	public static BudzetKOKomorekDB findById(Long id) throws EdmException {
		BudzetKOKomorekDB status = (BudzetKOKomorekDB) DSApi.getObject(
				BudzetKOKomorekDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(BudzetKOKomorekDB.class, id);

		return status;
	}
	
	public static void save(BudzetKOKomorekDB budzetKO) throws EdmException {
		try {
			DSApi.context().session().save(budzetKO);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(BudzetKOKomorekDB budzetKO) throws EdmException {
		try {
			DSApi.context().session().merge(budzetKO);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws EdmException {
		List<BudzetKOKomorekDB> budzety=list();
		for(BudzetKOKomorekDB budDB:budzety){
			budDB.setAvailable(false);
			update(budDB);
		}
		
	}
}
