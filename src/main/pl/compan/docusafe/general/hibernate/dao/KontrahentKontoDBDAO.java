package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.KontrahentKontoDB;

public class KontrahentKontoDBDAO {

	public static List<KontrahentKontoDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(KontrahentKontoDB.class)
				.list();
	}
	
	public static List<KontrahentKontoDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(KontrahentKontoDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	
	public static List<KontrahentKontoDB> findByDostawcaId(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(KontrahentKontoDB.class)
				.add(Restrictions.eq("dostawca_id", id)).list();
	}
	
	public static KontrahentKontoDB find(Long id) throws EdmException {
		KontrahentKontoDB status = (KontrahentKontoDB) DSApi.getObject(
				KontrahentKontoDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(KontrahentKontoDB.class, id);

		return status;
	}
	
	public static void save(KontrahentKontoDB kontrahentKontoDB) throws EdmException {
		try {
			DSApi.context().session().save(kontrahentKontoDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(KontrahentKontoDB kontrahentKontoDB) throws EdmException {
		try {
			DSApi.context().session().merge(kontrahentKontoDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
