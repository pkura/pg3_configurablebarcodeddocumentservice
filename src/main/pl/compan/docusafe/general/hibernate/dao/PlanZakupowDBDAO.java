package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.PlanZakupowDB;

public class PlanZakupowDBDAO {

	public static List<PlanZakupowDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(PlanZakupowDB.class)
				.list();
	}
	
	public static List<PlanZakupowDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(PlanZakupowDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	
	public static PlanZakupowDB findByErpIDUnique(Long id)
			throws EdmException {
		return (PlanZakupowDB) DSApi.context().session().createCriteria(PlanZakupowDB.class)
				.add(Restrictions.eq("erpId", id)).uniqueResult();
	}
	public static PlanZakupowDB find(Long id) throws EdmException {
		PlanZakupowDB status = (PlanZakupowDB) DSApi.getObject(
				PlanZakupowDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(PlanZakupowDB.class, id);

		return status;
	}
	
	public static void save(PlanZakupowDB planZakupowDB) throws EdmException {
		try {
			DSApi.context().session().save(planZakupowDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(PlanZakupowDB planZakupowDB) throws EdmException {
		try {
			DSApi.context().session().merge(planZakupowDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws EdmException {
		List<PlanZakupowDB>plany=list();
		for(PlanZakupowDB plan:plany){
			plan.setAvailable(false);
			update(plan);
		}
	}
}
