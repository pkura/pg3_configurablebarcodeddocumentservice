package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.OkresBudzetowyDB;
import pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjeDB;

public class OkresBudzetowyDBDAO {
	public static List<OkresBudzetowyDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(OkresBudzetowyDB.class)
				.list();
	}
	
	public static List<OkresBudzetowyDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(OkresBudzetowyDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	
	public static OkresBudzetowyDB find(Long id) throws EdmException {
		OkresBudzetowyDB status = (OkresBudzetowyDB) DSApi.getObject(
				OkresBudzetowyDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(OkresBudzetowyDB.class, id);

		return status;
	}
	
	public static void save(OkresBudzetowyDB okresBudzetowyDB) throws EdmException {
		try {
			DSApi.context().session().save(okresBudzetowyDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(OkresBudzetowyDB okresBudzetowyDB) throws EdmException {
		try {
			DSApi.context().session().merge(okresBudzetowyDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableOnFalseToAll() throws EdmException {
		List<OkresBudzetowyDB> okresy=list();
		for(OkresBudzetowyDB wnDB:okresy){
			wnDB.setAvailable(false);
			update(wnDB);
		}
		
	}
}
