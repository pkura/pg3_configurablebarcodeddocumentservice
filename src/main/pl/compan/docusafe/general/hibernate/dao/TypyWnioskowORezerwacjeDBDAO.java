package pl.compan.docusafe.general.hibernate.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.TypyWnioskowORezerwacjeDB;

public class TypyWnioskowORezerwacjeDBDAO {

	public static List<TypyWnioskowORezerwacjeDB> list() throws EdmException{
		return DSApi.context().session().createCriteria(TypyWnioskowORezerwacjeDB.class)
				.list();
	}
	
	public static List<TypyWnioskowORezerwacjeDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(TypyWnioskowORezerwacjeDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	
	public static TypyWnioskowORezerwacjeDB find(Long id) throws EdmException {
		TypyWnioskowORezerwacjeDB status = (TypyWnioskowORezerwacjeDB) DSApi.getObject(
				TypyWnioskowORezerwacjeDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(TypyWnioskowORezerwacjeDB.class, id);

		return status;
	}
	
	public static void save(TypyWnioskowORezerwacjeDB typyWnioskowORezerwacjeDB) throws EdmException {
		try {
			DSApi.context().session().save(typyWnioskowORezerwacjeDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(TypyWnioskowORezerwacjeDB typyWnioskowORezerwacjeDB) throws EdmException {
		try {
			DSApi.context().session().merge(typyWnioskowORezerwacjeDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
