package pl.compan.docusafe.general.hibernate.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.general.hibernate.model.ZrodloDB;

public class ZrodloDBDAO {
	public static List<ZrodloDB> list() throws EdmException {
		return DSApi.context().session().createCriteria(ZrodloDB.class)
				.addOrder(Order.asc("zrodlo_nazwa")).list();
	}

	public static ZrodloDB find(Long id) throws EdmException {
		ZrodloDB status = (ZrodloDB) DSApi.getObject(
				ZrodloDB.class, id);

		if (status == null)
			throw new EntityNotFoundException(ZrodloDB.class, id);

		return status;
	}

	public static List<ZrodloDB> findByErpID(Long id)
			throws EdmException {
		return DSApi.context().session().createCriteria(ZrodloDB.class)
				.add(Restrictions.eq("erpId", id)).list();
	}
	
	public static ZrodloDB findByErpIDUnique(Long id)
			throws EdmException {
		return (ZrodloDB) DSApi.context().session().createCriteria(ZrodloDB.class)
				.add(Restrictions.eq("erpId", id)).uniqueResult();
	}

	public static void save(ZrodloDB zrodloDB) throws EdmException {
		try {
			DSApi.context().session().save(zrodloDB);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public static void update(ZrodloDB zrodloDB) throws EdmException {
		try {
			DSApi.context().session().merge(zrodloDB);
			DSApi.context().session().flush();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static void setAvailableToFalseToAll() throws SQLException, EdmException {
		//zawsze lepsze niz po kazdym wierszu(szybsze)
		PreparedStatement ps=DSApi.context().prepareStatement("update "+DSApi.getTableName(ZrodloDB.class)+" set available='0'");
		ps.executeUpdate();
		
	}
}
