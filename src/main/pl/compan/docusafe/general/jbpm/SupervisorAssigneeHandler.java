package pl.compan.docusafe.general.jbpm;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.wssk.AHEUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Asygnacja na przelozonego uzytkownika wybranego na formularzu
 */
public class SupervisorAssigneeHandler implements AssignmentHandler {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(SupervisorAssigneeHandler.class);
	
	String field;
	
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
		
		String user = doc.getFieldsManager().getStringKey(field);
		Long userId = null;
		try{
			userId = Long.parseLong(user);
		} catch (NumberFormatException nfe){
			log.info("podane ID użytkownika (" +user+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
			throw new IllegalStateException("podane ID użytkownika (" +user+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
		}
		UserImpl userDS = (UserImpl) DSUser.findById(userId);
		
		DSUser supervisor = userDS.getSupervisor();
		if (supervisor == null ) {
			log.info("Nie znaleziono przełożonego użytkownika o ID: " + user);
			DSUser admin = DSUser.findByUsername("admin");
			log.info("Dekretacja na: " + admin.getName());
			assignable.addCandidateUser(admin.getName());
			AHEUtils.addToHistory(openExecution, doc, admin.getName(), null, null, log);
		} else {
			log.info("Dekretacja na: " + supervisor.getName());
			assignable.addCandidateUser(supervisor.getName());
			AHEUtils.addToHistory(openExecution, doc, supervisor.getName(), null, null, log);
		}
	}
}

