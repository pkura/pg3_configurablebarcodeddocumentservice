package pl.compan.docusafe.general.jbpm;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.general.canonical.dao.FakturaDAO;
import pl.compan.docusafe.general.canonical.model.Faktura;
import pl.compan.docusafe.general.hibernate.dao.FakturaDBDAO;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.ws.client.simple.general.RequestServiceFactory.SimpleErpStatusFaktury;

public class WyslijDoERP implements ExternalActivityBehaviour{

	public final static String PROCESS_EXECUTION_NAME = "wyslij_do_erp";
	
	public enum MozliweSygnaly{
		blad_wprowadzenia_do_ERP,
		wprowadzono_do_ERP,
		end;
	}
	
	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Document document = Jbpm4Utils.getDocument(execution);
		
		Faktura faktura = HibernateToCanonicalMapper.MapFakturaDBToFaktura(FakturaDBDAO.findModelById(document.getId()),document.getId());
		FakturaDAO.getInstance().wyslijDoERP(faktura);
		
		execution.waitForSignal();
	}

	@Override
	public void signal(ActivityExecution activityExecution, String signal, Map<String, ?> arg2) throws Exception {
		SimpleErpStatusFaktury statusFaktury = SimpleErpStatusFaktury.valueOf(signal);
		
		switch(statusFaktury){
			case FAULT:
			case INVALID:
				activityExecution.setVariable(activityExecution.getVariable("variableName")+"", MozliweSygnaly.blad_wprowadzenia_do_ERP.toString());
				break;
			case FINISHED:
				activityExecution.setVariable(activityExecution.getVariable("variableName")+"", MozliweSygnaly.wprowadzono_do_ERP.toString());
				break;
		}

		activityExecution.takeDefaultTransition();
	}

}
