package pl.compan.docusafe.general;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.general.interfaces.IInvoiceFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.wssk.WsskService;

/**
 * Klasa jest odpowiedzialna za zainicjowanie w projekcie w�a�ciwej fabryki 
 * @author
 *
 */
public class ProjectFactoriesFactory {

	private static IInvoiceFactory iInvoiceFactory;
	private final static Logger log = LoggerFactory
			.getLogger(ProjectFactoriesFactory.class);
	
	
	
	public static String FACTORY_NAME_INVOICE = "factories.invoice_factory.class";
	
	
	private void createInvoiceObject(){
		iInvoiceFactory = (IInvoiceFactory)createObject(FACTORY_NAME_INVOICE);
	}
	
	/**
	 * Metoda zwraca instancj� obiektu klasy, kt�rej nazwa jest podana jako parametr.
	 * Nale�y poda� nazw� z pliku adds.properties i pami�ta�, �e nazwa klasy w tym pliku
	 * musi by� podana z ca�ym pakietem, np.
	 * factories.invoice_factory.class = pl.compan.docusafe.general.factories.ws.WSInvoiceFactory
	 * @param addsName
	 * @return
	 */
	private Object createObject(String addsName)
	{
		String factoryClassInvoiceName = Docusafe.getAdditionProperty(addsName);		
		
		try {
			 Object obj = Class.forName(factoryClassInvoiceName).newInstance();
			 return obj;
		} catch (InstantiationException e) {			
			log.debug("InstantiationException: "+factoryClassInvoiceName+". " + e.getMessage());			
		} catch (IllegalAccessException e) {
			log.debug("IllegalAccessException: "+factoryClassInvoiceName+". " + e.getMessage());	
		} catch (ClassNotFoundException e) {
			log.debug("ClassNotFoundException: "+factoryClassInvoiceName+". " + e.getMessage());	
		}	
		return null;
	}
	
	/**
	 * Metoda zwraca obiekt Factory dla Faktury
	 * @return
	 */
	public IInvoiceFactory getInvoiceFactory()
	{
		if (iInvoiceFactory==null){
			createInvoiceObject();
		}
		return iInvoiceFactory;
	}
	
}
