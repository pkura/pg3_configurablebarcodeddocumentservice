package pl.compan.docusafe.general.encryption;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.Security;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.modes.CFBBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jfree.util.Log;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.AvailabilityManager;


public class AESEncryptor 
{
    public static String ECB_MODE = "ECB";
    public static String CBC_MODE = "CBC";
    public static String CFB_MODE = "CFB";
    private KeyParameter key;
    private  BufferedBlockCipher cipher; 
    
    public AESEncryptor() {
		initCipher();
	}
    
    public void initCipher() {
        
        initCipher(AdditionManager.getProperty("aes.encryption.mode"));
    }
    public void initCipher(String mode)
    {
        BlockCipher blockCipher;
        if (CBC_MODE.equalsIgnoreCase(mode))
        {
            blockCipher = new CBCBlockCipher(new AESEngine()); 
        }
        else if (CFB_MODE.equalsIgnoreCase(mode))
        {
            blockCipher = new CFBBlockCipher(new AESEngine(),64); 
        }
        else
        {
            //ECB jest standardowy
            blockCipher = new AESEngine();
            Log.error("ECB Mode");
        }
        
        cipher = new PaddedBufferedBlockCipher(blockCipher);
    }

    /**
     * Ustawiamy wartosc klucza
     * @param key 
     */
    public void setUpKey(byte[] key)
    {
        this.key = new KeyParameter( key );
    }
    
    /**
     * Ustawiamy klucz na podstawie 
     * @param base64key 
     */
    public void setUpKeyByBase64(String base64key)
    {
    	byte[] bytearray = DatatypeConverter.parseBase64Binary(base64key);
    	setUpKey(bytearray);
        //setUpKey(Base64.decode(base64key));
    }
    
    public void setUpKey(String key) {
    	MessageDigest md = null;
    	try {
    		Security.addProvider(new BouncyCastleProvider());
    		md = MessageDigest.getInstance(AdditionManager.getProperty("aes.encryption.type"));
    		md.update(key.getBytes("UTF-8"));
        	byte[] digest = md.digest();        	
        	setUpKey(digest);
		} 
    	catch (Exception e) 
		{
			Log.error("B��d w tworzeniu klucza", e);
		}
    	
    }
    
    public final static Integer BUF_SIZE = 1024;
    
    /**
     * Szyfrowanie polaczone z kopiowanie z IS do OS. Metoda zamyka Streamy
     * @param is
     * @param os
     * @throws IOException
     * @throws CryptoException 
     */
    public synchronized void encrypt(InputStream is, OutputStream os) throws IOException, CryptoException
    {
        try
        {
            if (is.available()>0)
            {
                byte[] b = new byte[is.available()];
                is.read(b);
                byte[] enc = encrypt(b);
                IOUtils.write(enc, os);
            }
            //IOUtils.toByteArray(is)
        }
        finally
        {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(os);
        }
    }
    
    public synchronized void decrypt(InputStream is, OutputStream os) throws IOException, CryptoException
    {
        try
        {
            if (is.available()>0)
            {
                byte[] b = new byte[is.available()];
                is.read(b);
                byte[] enc = decrypt(b);
                IOUtils.write(enc, os);
            }
            //IOUtils.toByteArray(is)
        }
        finally
        {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(os);
        }
    }
    /**
     * Zakodowanie ciagu znakow
     * @param data
     * @return
     * @throws CryptoException 
     */
    public synchronized byte[] encrypt( byte[] data )
    throws CryptoException {
        if( data == null || data.length == 0 ){
            return new byte[0];
        }
        
        cipher.init( true, key );
        return callCipher( data );
    }
    
    /**
     * Zakodowanie Stringa
     * @param data
     * @return
     * @throws CryptoException 
     */
    public byte[] encryptString( String data )
    throws CryptoException {
        if( data == null || data.length() == 0 ){
            return new byte[0];
        }
        
        return encrypt( data.getBytes() );
    }
    
    /**
     * Dekodowanie
     * @param data
     * @return
     * @throws CryptoException 
     */
    public synchronized byte[] decrypt( byte[] data )
    throws CryptoException {
        if( data == null || data.length == 0 ){
            return new byte[0];
        }
        
        cipher.init( false, key );
        return callCipher( data );
    }
    

    /**
     * Dekodowanie String
     * @param data
     * @return
     * @throws CryptoException 
     */
    public String decryptString( byte[] data )
    throws CryptoException {
        if( data == null || data.length == 0 ){
            return "";
        }
        
        return new String( decrypt( data ) );
    }
    
    /**
     * Wywolanie szyfru
     * @param data
     * @return
     * @throws CryptoException 
     */
    private byte[] callCipher( byte[] data )
    throws CryptoException {
        int    size =
                cipher.getOutputSize( data.length );
        byte[] result = new byte[ size ];
        int    olen = cipher.processBytes( data, 0,
                data.length, result, 0 );
        olen += cipher.doFinal( result, olen );
        
        if( olen < size ){
            byte[] tmp = new byte[ olen ];
            System.arraycopy(
                    result, 0, tmp, 0, olen );
            result = tmp;
        }
        
        return result;
    }
}
