package pl.compan.docusafe.general.validator;
/**
 * Klasa s�u��ca do walidacji numeru NIP, IBAN
 * @author Jerzy Pir�g
 *
 */
public class Validator {
	/**
     * Weryfikacje numeru NIP
     * @param nip
     * @return true je�li prawid�owy
     */
	public static boolean isValidNip(String nip){
		String nipOczyszczony = nip.replaceAll("\\D","");
		if(nipOczyszczony.length()!=10)
			return false;
		int[] weights = {6, 5, 7, 2, 3, 4, 5, 6, 7};
		String[] aNip = nipOczyszczony.split("");
		try {
	        int sum = 0;
	        for (int i = 0; i < weights.length; i++) {
	            sum += Integer.parseInt(aNip[i + 1]) * weights[i];
	        }
	        return (sum % 11) == Integer.parseInt(aNip[10]);
	    } catch (NumberFormatException e) {
	        return false;
	    }
	}
	/**
     * Weryfikacje numeru IBAN
     * @param iban
     * @return true je�li prawid�owy
     */
    public static boolean isValidIban(String iban) {
        //
        // 0. Zamiana na wielkie litery i usuni�cie �mieci i spacji
        iban = iban.toUpperCase().replaceAll("[\\p{Punct}\\p{Space}]*","");
        if (!iban.matches("^[A-Z]{2}[0-9]{12,}"))
            return false;
//		if (iban.length() < 16)
//			return false;
        // 1. Pierwsze 4 znaki na koniec
        iban = iban.substring(4, iban.length()) + iban.substring(0 ,4);
        // 2. Litery na cyfry
        for (int i = 0; i < iban.length(); i++) {
            char c = iban.charAt(i);
            if (Character.isUpperCase(c)) {
                int code = Character.getNumericValue(c);
                iban = iban.substring(0, i) + code
                        + iban.substring(i + 1, iban.length());
            }
        }
        // 3. Modulo 97
        int mod = 0;
        int isize = iban.length();
        for (int i = 0; i < isize; i = i + 6) {
            try {
                mod = Integer.parseInt("" + mod + iban.substring(i, i + 6), 10) % 97;
            } catch (StringIndexOutOfBoundsException e) {
                return false;
            }
        }
        return (mod == 1);
    }
    
    /**
     * Weryfikacja numeru IBAN
     * @param iban
     * @return true je�li prawid�owy
     */
    public static boolean isValidIban(long iban) {
        return isValidIban(new Long(iban).toString());
    }
    
    /**
     * Weryfikacje kodu pocztowego
     * @param zip
     * @return true je�li prawid�owy
     */
	public static boolean isValidZip(String zip){
		zip = zip.trim();
		if(zip.length()!=6)
			return false;
		
		String  regex = "[0-9]{2}( |-)?[0-9]{3}";
		if(zip.matches(regex))
			return true;
		else
			return false;
	}
	
}
