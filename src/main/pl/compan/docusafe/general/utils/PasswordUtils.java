package pl.compan.docusafe.general.utils;

import pl.compan.docusafe.general.mapers.utils.strings.PolishStringUtils;

public class PasswordUtils {

	/**
	 * Prosta metoda do generowania loginu z dw�ch parametr�w.
	 * Parametry moga mie� polskie znaki. Algorytm: 3 znaki param1 + 3 znaki param2
	 * Usuni�cie polskich znak�w i kod�w UTF8
	 * @param param1
	 * @param param2
	 * @return
	 */
	public String generujLogin(String param1, String param2)
	{
		String login1 = param1.substring(0, 3);
		String login2 =param2.substring(0, 3);
		String login = login1 + login2;
		
		PolishStringUtils psu = new PolishStringUtils();
		
		// Zamiana polskich znak�w
		login = psu.zamienPolskieZnaki(login);				
		
		// Zamiana kod�w UTF 
		login = psu.zamienKodyUTF8PolskichZnakow(login);
		
		return login;
	}
}
