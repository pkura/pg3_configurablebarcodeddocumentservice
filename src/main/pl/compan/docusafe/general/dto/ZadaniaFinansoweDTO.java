package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.ZadaniaFinansowe;
import pl.compan.docusafe.general.hibernate.dao.FinancialTaskDBDAO;
import pl.compan.docusafe.general.hibernate.model.FinancialTaskDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.FinancialTask;

public class ZadaniaFinansoweDTO {
	
	public static final Logger log = LoggerFactory.getLogger(ZadaniaFinansoweDTO.class);

	/**
	 * Pobiera zadania finansowe z bazy danych
	 * 
	 * @return
	 */
	public List<ZadaniaFinansowe> pobierzZadaniaFinansoweZBD() {
		List<ZadaniaFinansowe> listaZadaniaFinansowe = new ArrayList<ZadaniaFinansowe>();

		List<FinancialTaskDB> listaFinancialTaskDB;
		try {
			listaFinancialTaskDB = FinancialTaskDBDAO.list();

			for (FinancialTaskDB financialTaskDB : listaFinancialTaskDB) {
				ZadaniaFinansowe zadaniaFinansowe = HibernateToCanonicalMapper
						.MapFinancialTaskDBToZadaniaFinansowe(financialTaskDB);
				listaZadaniaFinansowe.add(zadaniaFinansowe);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaZadaniaFinansowe;
	}

	/**
	 * Metoda zwraca liste ZadaniaFinansowe z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * GetFinancialTask
	 * @throws RemoteException 
	 */
	public List<ZadaniaFinansowe> PobierzZadaniaFinansowe() throws RemoteException {
		List<ZadaniaFinansowe> listaZadaniaFinansowe = new ArrayList<ZadaniaFinansowe>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<FinancialTask> listFinancialTask = dictionaryServiceFactory
				.getFinancialTask();

		for (FinancialTask unit : listFinancialTask) {
			ZadaniaFinansowe zf = WSToCanonicalMapper
					.MapFinancialTaskToZadaniaFinansowe(unit);

			listaZadaniaFinansowe.add(zf);
		}
		return listaZadaniaFinansowe;
	}

	/**
	 * Metoda zapisuje liste ZadaniaFinansowe do bazy danych
	 * 
	 * @param listaZadaniaFinansowe
	 */
	public boolean Zapisz(List<ZadaniaFinansowe> listaZadaniaFinansowe) {

		try {
			for(ZadaniaFinansowe zadaniaFinansowe:listaZadaniaFinansowe)
			{
				FinancialTaskDB financialTaskDB = CanonicalToHibernateMapper.MapZadaniaFinansoweNaFinancialTaskDB(zadaniaFinansowe);

				if (zadaniaFinansowe.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					FinancialTaskDBDAO.save(financialTaskDB);
				}
				else
					if (zadaniaFinansowe.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						FinancialTaskDBDAO.update(financialTaskDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
