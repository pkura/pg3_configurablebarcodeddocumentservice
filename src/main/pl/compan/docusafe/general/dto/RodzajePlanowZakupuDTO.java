package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.RodzajePlanowZakupu;
import pl.compan.docusafe.general.hibernate.dao.RodzajePlanowZakupuDBDAO;
import pl.compan.docusafe.general.hibernate.model.RodzajePlanowZakupuDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PurchasePlanKind;

public class RodzajePlanowZakupuDTO {
	
public static final Logger log = LoggerFactory.getLogger(RodzajePlanowZakupuDTO.class);
	
	/**
	 * Metoda pobiera rodzaje planow zakupu z bazy danych
	 */
	public List<RodzajePlanowZakupu> pobierzRodzajePlanowZakupuZBD() {

		List<RodzajePlanowZakupu> listaRodzajePlanowZakupu = new ArrayList<RodzajePlanowZakupu>();

		List<RodzajePlanowZakupuDB> listaRodzajePlanowZakupuDB;
		try {
			listaRodzajePlanowZakupuDB = RodzajePlanowZakupuDBDAO.list();
			for (RodzajePlanowZakupuDB ppz : listaRodzajePlanowZakupuDB) {
				RodzajePlanowZakupu rodzajePlanowZakupu = HibernateToCanonicalMapper
						.MapRodzajePlanowZakupuDBToRodzajePlanowZakupu(ppz);
				listaRodzajePlanowZakupu.add(rodzajePlanowZakupu);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaRodzajePlanowZakupu;
	}
	
	/**
	 * Metoda zwraca liste rodzajow planow zakupu  z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - GetPurchasePlanKind
	 * @throws RemoteException 
	 */
	public List<RodzajePlanowZakupu> pobierzRodzajePlanowZakupuZWS() throws RemoteException {
		List<RodzajePlanowZakupu> listaRodzajePlanowZakupu = new ArrayList<RodzajePlanowZakupu>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<PurchasePlanKind> purchasePlanKind = dictionaryService
				.getPurchasePlanKind();

		for (PurchasePlanKind ppk : purchasePlanKind) {
			RodzajePlanowZakupu rodzajePlanowZakupu = WSToCanonicalMapper.MapPurchasePlanKindToRodzajePlanowZakupu(ppk);
			listaRodzajePlanowZakupu.add(rodzajePlanowZakupu);
		}
		return listaRodzajePlanowZakupu;
	}
	
	/**
	 * Metoda zapisuje rodzaje planow zakupu do bazy danych
	 * 
	 * @param listaRodzajePlanowZakupu
	 */
	public boolean zapisz(List<RodzajePlanowZakupu> listaRodzajePlanowZakupu) {
		try {
			for (RodzajePlanowZakupu rpz : listaRodzajePlanowZakupu) {
				RodzajePlanowZakupuDB rodzajePlanowZakupuDB = CanonicalToHibernateMapper.MapRodzajePlanowZakupuToRodzajePlanowZakupuDB(rpz);

				if (rpz.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					RodzajePlanowZakupuDBDAO.save(rodzajePlanowZakupuDB);
				} else {
					if (rpz.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						RodzajePlanowZakupuDBDAO.update(rodzajePlanowZakupuDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
