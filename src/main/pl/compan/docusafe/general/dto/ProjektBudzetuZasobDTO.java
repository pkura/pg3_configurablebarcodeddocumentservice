package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetZasob;
import pl.compan.docusafe.general.hibernate.dao.ProjectBudgetItemResourceDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetZasobDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectBudgetItemResource;

public class ProjektBudzetuZasobDTO {
	public static final Logger log = LoggerFactory.getLogger(ProjektBudzetuZasobDTO.class);

	/**
	 * Pobiera projekty budzetow zasob z bazy danych
	 * 
	 * @return
	 */
	public List<ProjektBudzetZasob> pobierzProjektBudzetuZasobZBD() {
		List<ProjektBudzetZasob> listaProjektBudzetZasob = new ArrayList<ProjektBudzetZasob>();

		List<ProjektBudzetZasobDB> listaProjectBudgetItemResourceDB;
		try {
			listaProjectBudgetItemResourceDB = ProjectBudgetItemResourceDBDAO.list();

			for (ProjektBudzetZasobDB projectBudgetItemResourceDB : listaProjectBudgetItemResourceDB) {
				ProjektBudzetZasob projektBudzetZasob = HibernateToCanonicalMapper
						.MapProjectBudgetItemResourceDBToProjektBudzetZasob(projectBudgetItemResourceDB);
				listaProjektBudzetZasob.add(projektBudzetZasob);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaProjektBudzetZasob;
	}

	/**
	 * Metoda zwraca liste ProjektBudzetZasob z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * ProjectBudgetItemResource
	 * @throws RemoteException 
	 */
	public List<ProjektBudzetZasob> PobierzProjektBudzetZasob() throws RemoteException {
		List<ProjektBudzetZasob> listaProjektBudzetZasob = new ArrayList<ProjektBudzetZasob>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<ProjectBudgetItemResource> listProjectBudgetItemResource = dictionaryServiceFactory
				.getProjectBudgetItemResource();

		for (ProjectBudgetItemResource unit : listProjectBudgetItemResource) {
			ProjektBudzetZasob projektBudzetZasob = WSToCanonicalMapper
					.MapProjectBudgetItemResourceToProjektBudzetZasob(unit);

			listaProjektBudzetZasob.add(projektBudzetZasob);
		}
		return listaProjektBudzetZasob;
	}

	/**
	 * Metoda zapisuje liste ProjektBudzetZasob do bazy danych
	 * 
	 * @param listaProjektBudzet
	 */
	public boolean Zapisz(List<ProjektBudzetZasob> listaProjektBudzetZasob) {

		try {
			for(ProjektBudzetZasob projektBudzetZasob:listaProjektBudzetZasob)
			{
				ProjektBudzetZasobDB projectBudgetItemResourceDB = CanonicalToHibernateMapper.MapProjektBudzetZasobNaProjectBudgetItemResourceDB(projektBudzetZasob);

				if (projektBudzetZasob.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					ProjectBudgetItemResourceDBDAO.save(projectBudgetItemResourceDB);
				}
				else
					if (projektBudzetZasob.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						ProjectBudgetItemResourceDBDAO.update(projectBudgetItemResourceDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
