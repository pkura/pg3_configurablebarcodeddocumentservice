package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetEtap;
import pl.compan.docusafe.general.hibernate.dao.ProjectBudgetItemDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetEtapDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectBudgetItem;

public class ProjektBudzetEtapDTO {
	
	public static final Logger log = LoggerFactory.getLogger(ProjektBudzetEtapDTO.class);

	/**
	 * Pobiera projekty budzetow etapy z bazy danych
	 * 
	 * @return
	 */
	public List<ProjektBudzetEtap> pobierzProjektBudzetEtapZBD() {
		List<ProjektBudzetEtap> listaProjektBudzetEtap = new ArrayList<ProjektBudzetEtap>();

		List<ProjektBudzetEtapDB> listaProjectBudgetItemDB;
		try {
			listaProjectBudgetItemDB = ProjectBudgetItemDBDAO.list();

			for (ProjektBudzetEtapDB projectBudgetItemDB : listaProjectBudgetItemDB) {
				ProjektBudzetEtap projektBudzetEtap = HibernateToCanonicalMapper
						.MapProjectBudgetItemDBToProjektBudzet(projectBudgetItemDB);
				listaProjektBudzetEtap.add(projektBudzetEtap);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaProjektBudzetEtap;
	}

	/**
	 * Metoda zwraca liste ProjektBudzetEtap z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * GetProjectBudgetItem
	 * @throws RemoteException 
	 */
	public List<ProjektBudzetEtap> PobierzProjektBudzetEtap() throws RemoteException {
		List<ProjektBudzetEtap> listaProjektBudzetEtap = new ArrayList<ProjektBudzetEtap>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<ProjectBudgetItem> listProjectBudgetItem = dictionaryServiceFactory
				.getProjectBudgetItem();

		for (ProjectBudgetItem unit : listProjectBudgetItem) {
			ProjektBudzetEtap projektBudzet = WSToCanonicalMapper
					.MapProjectBudgetItemToProjektBudzet(unit);

			listaProjektBudzetEtap.add(projektBudzet);
		}
		return listaProjektBudzetEtap;
	}

	/**
	 * Metoda zapisuje liste ProjektBudzetEtap do bazy danych
	 * 
	 * @param listaProjektBudzet
	 */
	public boolean Zapisz(List<ProjektBudzetEtap> listaProjektBudzetEtap) {

		try {
			for(ProjektBudzetEtap projektBudzetEtap:listaProjektBudzetEtap)
			{
				ProjektBudzetEtapDB projectBudgetItemDB = CanonicalToHibernateMapper.MapProjektBudzetNaProjectBudgetItemDB(projektBudzetEtap);

				if (projektBudzetEtap.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					ProjectBudgetItemDBDAO.save(projectBudgetItemDB);
				}
				else
					if (projektBudzetEtap.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						ProjectBudgetItemDBDAO.update(projectBudgetItemDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
