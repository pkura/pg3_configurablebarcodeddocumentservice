package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.WniosekORezerwacje;
import pl.compan.docusafe.general.hibernate.dao.WniosekORezerwacjeDBDAO;
import pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjeDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.RequestForReservation;

public class WniosekORezerwacjeDTO {
	public static final Logger log = LoggerFactory.getLogger(WniosekORezerwacjeDTO.class);

	/**
	 * Pobiera wnioski o rezerwacje z bazy danych
	 * 
	 * @return
	 */
	public List<WniosekORezerwacje> pobierzWniosekORezerwacjeZBD() {
		List<WniosekORezerwacje> listaWniosekORezerwacje = new ArrayList<WniosekORezerwacje>();

		List<WniosekORezerwacjeDB> listaTypyUrlopowDB;
		try {
			listaTypyUrlopowDB = WniosekORezerwacjeDBDAO.list();

			for (WniosekORezerwacjeDB wnDB : listaTypyUrlopowDB) {
				WniosekORezerwacje wn = HibernateToCanonicalMapper
						.mapWniosekORezerwacjeDBToWniosekORezerwacje(wnDB);
				listaWniosekORezerwacje.add(wn);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaWniosekORezerwacje;
	}

	/**
	 * Metoda zwraca liste WniosekORezerwacje z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * GetRequestForReservation
	 * @throws RemoteException 
	 */
	public List<WniosekORezerwacje> pobierzWnioskiORezerwacjeZWS() throws RemoteException {
		List<WniosekORezerwacje> listaWniosekORezerwacje = new ArrayList<WniosekORezerwacje>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<RequestForReservation> listReq = dictionaryServiceFactory
				.getRequestForReservation();

		for (RequestForReservation req : listReq) {
			WniosekORezerwacje wn = WSToCanonicalMapper
					.MapRequestForReservationToWniosekORezerwacje(req);

			listaWniosekORezerwacje.add(wn);
		}
		return listaWniosekORezerwacje;
	}

	/**
	 * Metoda zapisuje liste wnioskow o rezerwacje do bazy danych
	 * 
	 * @param listaWniosekORezerwacje
	 */
	public boolean Zapisz(List<WniosekORezerwacje> listaWniosekORezerwacje) {

		try {
			for(WniosekORezerwacje wn:listaWniosekORezerwacje)
			{
				WniosekORezerwacjeDB wnDB = CanonicalToHibernateMapper.MapWniosekORezerwacjeToWniosekORezerwacjeDB(wn);

				if (wn.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					WniosekORezerwacjeDBDAO.save(wnDB);
				}
				else
					if (wn.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						WniosekORezerwacjeDBDAO.update(wnDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
