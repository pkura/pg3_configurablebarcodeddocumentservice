package pl.compan.docusafe.general.dto;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.BudzetDoKomorki;
import pl.compan.docusafe.general.hibernate.dao.BudzetDoKomorkiDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudzetDoKomorkiDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.BudgetUnitOrganizationUnit;

public class BudzetDoKomorkiDTO {
	public static final Logger log = LoggerFactory.getLogger(BudzetDoKomorkiDTO.class);
	
	
	/**
	 * Metoda pobiera konta bankowe z bazy danych
	 */
	public List<BudzetDoKomorki> pobierzBudzetyDoKomorekZBD() {

		List<BudzetDoKomorki> listaBudDoKom = new ArrayList<BudzetDoKomorki>();

		List<BudzetDoKomorkiDB> listaBudzetowDoKomDB;
		try {
			listaBudzetowDoKomDB = BudzetDoKomorkiDBDAO.list();
			for (BudzetDoKomorkiDB budzetKoDB : listaBudzetowDoKomDB) {
				BudzetDoKomorki budzetDoKomorki = HibernateToCanonicalMapper
						.MapBudzetDOKomorkiDBToBudzetDoKomorki(budzetKoDB);
				listaBudDoKom.add(budzetDoKomorki);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaBudDoKom;
	}
	
	/**
	 * Metoda zwraca liste budzetDoKomorki z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - getBudgetUnitOrganizationUnit
	 */
	public List<BudzetDoKomorki> pobierzBudzetyDoKomorkiZWS() {
		List<BudzetDoKomorki> listaKont = new ArrayList<BudzetDoKomorki>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<BudgetUnitOrganizationUnit> unitBudgetKos = dictionaryService
				.getBudgetUnitOrganizationUnit();

		for (BudgetUnitOrganizationUnit sba : unitBudgetKos) {
			BudzetDoKomorki kontrahentKonto = WSToCanonicalMapper.MapBudgetUnitOrganizationalUnitToBudzetDoKomorki(sba);
			listaKont.add(kontrahentKonto);
		}
		return listaKont;
	}
	
	/**
	 * Metoda zapisuje budzetdoKomorki do bazy danych
	 * 
	 * @param BudzetDoKomorki
	 */
	public boolean zapisz(List<BudzetDoKomorki> listaKontrahentKonto) {
		try {
			for (BudzetDoKomorki kk : listaKontrahentKonto) {
				BudzetDoKomorkiDB budzetKoKomorekDB = CanonicalToHibernateMapper.MapBudzetDoKomorkiToBudzetDoKomorkiDB(kk);

				if (kk.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					BudzetDoKomorkiDBDAO.save(budzetKoKomorekDB);
				} else {
					if (kk.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						BudzetDoKomorkiDBDAO.update(budzetKoKomorekDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
