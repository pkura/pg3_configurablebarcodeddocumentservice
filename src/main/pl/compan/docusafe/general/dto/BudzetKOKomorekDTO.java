package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.BudzetKOKomorek;
import pl.compan.docusafe.general.hibernate.dao.BudzetKOKomorkaDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudzetKOKomorekDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitBudgetKo;

public class BudzetKOKomorekDTO {
public static final Logger log = LoggerFactory.getLogger(BudzetKOKomorekDTO.class);
	
	
	/**
	 * Metoda pobiera konta bankowe z bazy danych
	 */
	public List<BudzetKOKomorek> pobierzBudzetyZBD() {

		List<BudzetKOKomorek> listaBudzetowKo = new ArrayList<BudzetKOKomorek>();

		List<BudzetKOKomorekDB> listaBudzetowKoDB;
		try {
			listaBudzetowKoDB = BudzetKOKomorkaDBDAO.list();
			for (BudzetKOKomorekDB budzetKoDB : listaBudzetowKoDB) {
				BudzetKOKomorek kontrahentKonto = HibernateToCanonicalMapper
						.MapBudzetKOKomorekDBToBudzetKOKomorek(budzetKoDB);
				listaBudzetowKo.add(kontrahentKonto);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaBudzetowKo;
	}
	
	/**
	 * Metoda zwraca liste kont z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - GetSupplierBankAccount
	 * @throws RemoteException 
	 */
	public List<BudzetKOKomorek> pobierzBudzetyKoKomorekZWS() throws RemoteException {
		List<BudzetKOKomorek> listaKont = new ArrayList<BudzetKOKomorek>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<UnitBudgetKo> unitBudgetKos = dictionaryService
				.getUnitBudgetKo();

		for (UnitBudgetKo sba : unitBudgetKos) {
			BudzetKOKomorek kontrahentKonto = WSToCanonicalMapper.MapUnitBudgetKoToBudzetKOKomorek(sba);
			listaKont.add(kontrahentKonto);
		}
		return listaKont;
	}
	
	/**
	 * Metoda zapisuje konta kontrahentow do bazy danych
	 * 
	 * @param kontrahentKonto
	 */
	public boolean zapisz(List<BudzetKOKomorek> listaKontrahentKonto) {
		try {
			for (BudzetKOKomorek kk : listaKontrahentKonto) {
				BudzetKOKomorekDB budzetKoKomorekDB = CanonicalToHibernateMapper.MapbudzetKoKomorekTobudzetKoKomorekDB(kk);

				if (kk.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					BudzetKOKomorkaDBDAO.save(budzetKoKomorekDB);
				} else {
					if (kk.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						BudzetKOKomorkaDBDAO.update(budzetKoKomorekDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
