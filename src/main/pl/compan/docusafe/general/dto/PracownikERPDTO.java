package pl.compan.docusafe.general.dto;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.Pracownik;
import pl.compan.docusafe.general.hibernate.dao.PracownikDBDAO;
import pl.compan.docusafe.general.hibernate.model.PracownikDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Person;

public class PracownikERPDTO {
	public static final Logger log = LoggerFactory.getLogger(PracownikERPDTO.class);

	/**
	 * Pobiera pracownikow z bazy danych
	 * 
	 * @return
	 */
	public List<Pracownik> pobierzPracownikowZBD() {
		List<Pracownik> listaPracownik = new ArrayList<Pracownik>();

		List<PracownikDB> listaPracownikDB;
		try {
			listaPracownikDB = PracownikDBDAO.list();

			for (PracownikDB pracownikDB : listaPracownikDB) {
				Pracownik pracownik = HibernateToCanonicalMapper
						.MapPracownikDBToPracownik(pracownikDB);
				listaPracownik.add(pracownik);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaPracownik;
	}

	/**
	 * Metoda zwraca liste Pracownikow z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * GetPerson
	 */
	public List<Pracownik> PobierzPracownikow() {
		List<Pracownik> listaPracownikow = new ArrayList<Pracownik>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<Person> listPerson = dictionaryServiceFactory
				.getPersons();

		for (Person unit : listPerson) {
			Pracownik pracownik = WSToCanonicalMapper
					.MapPersontoPracownik(unit);

			listaPracownikow.add(pracownik);
		}
		return listaPracownikow;
	}

	/**
	 * Metoda zapisuje liste Pracownikow do bazy danych
	 * 
	 * @param listaPracownikow
	 */
	public boolean Zapisz(List<Pracownik> listaPracownikow) {

		try {
			for(Pracownik pracownik:listaPracownikow)
			{
				PracownikDB pracownikDB = CanonicalToHibernateMapper.MapPracownikNaPracownikDB(pracownik);

				if (pracownik.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					PracownikDBDAO.save(pracownikDB);
				}
				else
					if (pracownik.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						PracownikDBDAO.update(pracownikDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
