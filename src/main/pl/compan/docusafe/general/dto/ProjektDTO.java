package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.Projekt;
import pl.compan.docusafe.general.hibernate.dao.ProjectDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProjectDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Project;

public class ProjektDTO {
	
	public static final Logger log = LoggerFactory.getLogger(ProjektDTO.class);

	/**
	 * Pobiera projekty z bazy danych
	 * 
	 * @return
	 */
	public List<Projekt> pobierzProjektZBD() {
		List<Projekt> listaProjekt = new ArrayList<Projekt>();

		List<ProjectDB> listaProjectDB;
		try {
			listaProjectDB = ProjectDBDAO.list();

			for (ProjectDB projectDB : listaProjectDB) {
				Projekt projekt = HibernateToCanonicalMapper
						.MapProjectDBToProjekt(projectDB);
				listaProjekt.add(projekt);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaProjekt;
	}

	/**
	 * Metoda zwraca liste Projekt z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * GetProject
	 * @throws RemoteException 
	 */
	public List<Projekt> PobierzProjekt() throws RemoteException {
		List<Projekt> listaProjekt = new ArrayList<Projekt>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<Project> listProject = dictionaryServiceFactory
				.getProject();

		for (Project unit : listProject) {
			Projekt projekt = WSToCanonicalMapper
					.MapProjectToProjekt(unit);

			listaProjekt.add(projekt);
		}
		return listaProjekt;
	}

	/**
	 * Metoda zapisuje liste Projekt do bazy danych
	 * 
	 * @param listaProjekt
	 */
	public boolean Zapisz(List<Projekt> listaProjekt) {

		try {
			for(Projekt projekt:listaProjekt)
			{
				ProjectDB projectDB = CanonicalToHibernateMapper.MapProjektNaProjectDB(projekt);

				if (projekt.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					ProjectDBDAO.save(projectDB);
				}
				else
					if (projekt.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						ProjectDBDAO.update(projectDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
