package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.OkresBudzetowy;
import pl.compan.docusafe.general.hibernate.dao.OkresBudzetowyDBDAO;
import pl.compan.docusafe.general.hibernate.model.OkresBudzetowyDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitBudget;

public class OkresBudzetowyDTO {
	public static final Logger log = LoggerFactory.getLogger(OkresBudzetowyDTO.class);

	/**
	 * Pobiera budzety komorek z bazy danych
	 * @return
	 */
	public List<OkresBudzetowy> pobierzOkresBudzetowyZBD() {
		List<OkresBudzetowy> listaOkresBudzetowy = new ArrayList<OkresBudzetowy>();

		List<OkresBudzetowyDB> listaOkresBudzetowyDB;
		try {
			listaOkresBudzetowyDB = OkresBudzetowyDBDAO.list();

			for (OkresBudzetowyDB okresBudzetowyDB : listaOkresBudzetowyDB) {
				OkresBudzetowy okresBudzetowy = HibernateToCanonicalMapper
						.mapOkresBudzetowyDBToOkresBudzetowy(okresBudzetowyDB);
				listaOkresBudzetowy.add(okresBudzetowy);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaOkresBudzetowy;
	}

	/**
	 * Metoda zwraca liste OkresBudzetowy z WebSerwisu
	 * Metoda u�ywa dowolnego, skonfigurowanego webserwisu Simple DictionaryService - GetUnitBudget
	 * @throws RemoteException 
	 */
	public List<OkresBudzetowy> pobierzOkresBudzetowyZWS() throws RemoteException {
		List<OkresBudzetowy> listaOkresBudzetowy = new ArrayList<OkresBudzetowy>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<UnitBudget> listUnitBudget = dictionaryServiceFactory
				.getUnitBudget();

		for (UnitBudget unit : listUnitBudget) {
			OkresBudzetowy okresBudzetowy = WSToCanonicalMapper
					.mapUnitBudgetToOkresBudzetowy(unit);

			listaOkresBudzetowy.add(okresBudzetowy);
		}
		return listaOkresBudzetowy;
	}

	/**
	 * Metoda zapisuje liste OkresBudzetowy do bazy danych
	 * @param listaOkresBudzetowy
	 */
	public boolean Zapisz(List<OkresBudzetowy> listaOkresBudzetowy) {

		try {
			for(OkresBudzetowy okresBudzetowy:listaOkresBudzetowy)
			{
				OkresBudzetowyDB okresBudzetowyDB = CanonicalToHibernateMapper.mapOkresBudzetowyNaOkresBudzetowyDB(okresBudzetowy);

				if (okresBudzetowy.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					OkresBudzetowyDBDAO.save(okresBudzetowyDB);
				}
				else
					if (okresBudzetowy.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						OkresBudzetowyDBDAO.update(okresBudzetowyDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
