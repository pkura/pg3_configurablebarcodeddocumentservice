package pl.compan.docusafe.general.dto;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.canonical.model.Pracownik;
import pl.compan.docusafe.general.canonical.model.PracownikWJednostkachOrganizacyjnych;
import pl.compan.docusafe.general.hibernate.dao.DSUserFactory;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Employee;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Person;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PersonAgreement;

public class PracownikDTO {

	public static final Logger log = LoggerFactory
			.getLogger(PracownikDTO.class);

	/**
	 * Metoda pobiera wszystkich Employee[WS] i zwraca jako List<Pracownik>
	 * [CAN]
	 * 
	 * @return
	 */
	public List<Pracownik> pobierzPracownikowWS() {
		try {
			List<Pracownik> listaPracownikow = new ArrayList<Pracownik>();

			DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();

			String nazwaUslugi = Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_NAZWA_USLUGI");

			if (nazwaUslugi.equals("getEmployee")) {
				List<Employee> listaEmployee = dictionaryServiceFactory.getEmployees();

				// Dla ka�dego elementu mapujemy element z WS na element
				// Canonical
				for (Employee employee : listaEmployee) {
					Pracownik pracownik = WSToCanonicalMapper.MapEmployeeToPracownik(employee);
					listaPracownikow.add(pracownik);
				}
			} else if (nazwaUslugi.equals("getPerson")) {
				List<Person> listaPerson = dictionaryServiceFactory.getPersons();

				// Dla ka�dego elementu mapujemy element z WS na element
				// Canonical
				for (Person person : listaPerson) {
					Pracownik pracownik = WSToCanonicalMapper.MapPersonToPracownik(person);
					listaPracownikow.add(pracownik);

				}
			}
			return listaPracownikow;
		} catch (Exception exc) {
			log.error("B��d podczas pobierania u�ytkownik�w z systemu ERP");
			log.debug("",exc);
		}
		return null;
	}

	public List<Pracownik> pobierzPracownikowDB() {
		List<Pracownik> lista = null;
		try {
			DSApi.openContextIfNeeded();
			DSApi.context().begin();
			lista = pobierzUzytkownikowZBD();
			DSApi.context().commit();
		} catch (Exception e) {
			log.error("", e);
			DSApi.context()._rollback();
		} finally {
			DSApi._close();
		}
		return lista;
	}

	/**
	 * Metoda zwraca list� Jednostek Organizacyjnych z bazy danych DocuSafe
	 * 
	 * @return
	 */
	public List<Pracownik> pobierzUzytkownikowZBD() {

		List<Pracownik> listaPracownikow = new ArrayList<Pracownik>();

		DSUserFactory factory = new DSUserFactory();
		List<DSUser> listaDSUser = factory.getDSUsers();

		for (DSUser dsuser : listaDSUser) {
			Pracownik pracownik = HibernateToCanonicalMapper
					.MapDsuserToPracownik(dsuser);
			listaPracownikow.add(pracownik);
		}

		return listaPracownikow;
	}

	/**
	 * Metoda stosuje logik�, kt�ra mapuje inteligentnie dane pracownika z WS na
	 * pracownika z BD - metoda nie nadpisuje: has�o, login
	 * 
	 * @param pracownikWS
	 * @return
	 */
	public Pracownik aktualizujDaneWSNaDB(Pracownik pracownikWS,
			Pracownik pracownikDB) {
		pracownikDB.setImie(pracownikWS.getImie());
		pracownikDB.setNazwisko(pracownikWS.getNazwisko());
		pracownikDB.setJednostkiOrganizacyjne(pracownikWS
				.getJednostkiOrganizacyjne());
		pracownikDB.setOperatorDoPorownywania(pracownikWS
				.getOperatorDoPorownywania());
		pracownikDB.setPesel(pracownikWS.getPesel());

		return pracownikDB;
	}

	/**
	 * Metoda zapisuje list� pracownik�w do bazy danych
	 * 
	 * @param listaPracownikowAktualna
	 */
	public void zapiszListePracownikowDoBD(List<Pracownik> listaPracownikow) {

		try {
			DSApi.openContextIfNeeded();			

			CanonicalToHibernateMapper mapper = new CanonicalToHibernateMapper();
			for (Pracownik pracownik : listaPracownikow) {
				
				try {
					if (pracownik != null) {

						DSUser dsuser = null;

						dsuser = mapper.mapPracownikToDSUser(pracownik);

						if (dsuser == null) {							
							continue;
						}

						if (dsuser != null) {
							if (pracownik.getStanObiektu() == STATUS_OBIEKTU.USUNIETY) {
								dsuser.setDeleted(true);
							}

							if (pracownik.getStanObiektu() == STATUS_OBIEKTU.NOWY
									|| pracownik.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY
									|| pracownik.getStanObiektu() == STATUS_OBIEKTU.USUNIETY) {
								// metoda mapuj�ca tworzy obiekt - je�eli go
								// niema,
								// wi�c k��da instancja obiektu ma zosta�
								// zaktualizowana
								try {
									DSApi.context().begin();
									Persister.update(dsuser);
									DSApi.context().commit();
								} catch (EdmException edm) {
									log.debug(edm);
								}
							}
						}
					}
				} catch (Exception pe) {
					log.debug(pe.getStackTrace());
				}					
			}
			
			// Mapujemy jednostki organizacyjne
			for (Pracownik pracownik : listaPracownikow) {
				for (JednostkaOrganizacyjna jednostkaOrganizacyjna : pracownik
						.getJednostkiOrganizacyjne()) {
					
					// Aktualizujemy u�ytkownik�w do dsdivision
					DSApi.openContextIfNeeded();
					DSApi.context().begin();
					
					DSDivision dsDivision;

					if (jednostkaOrganizacyjna != null) {
						try {
							dsDivision = null;
							if (jednostkaOrganizacyjna.getIdn()!=null)
							{
								dsDivision = DSDivision.findByExternalId(jednostkaOrganizacyjna.getIdn());
							}
							DSUser dsUser = null;
							
							if (pracownik.getLogin()!=null)
							{
								dsUser = UserImpl.findByUsername(pracownik.getLogin());
							}
							if (dsDivision != null && dsUser != null) {
								dsDivision.addUser(dsUser);
							}
						} catch (DivisionNotFoundException e) {
							log.debug("Nie ma takiej jednostki DSDivision="
									+ jednostkaOrganizacyjna.getIdn());
						} catch (EdmException e) {
							log.debug(e);
						}
					}
				
					// Zakomitowanie zmian
					DSApi.context().commit();
				}
			}		

		} catch (Exception e) {
			log.error("", e);
			DSApi.context()._rollback();
		} finally {
			DSApi._close();
		}
	}
	
	/**
	 * Metoda zwraca powi�zania pracownik�w w jednostkach organizacyjnych
	 * @return
	 */
	public List<PracownikWJednostkachOrganizacyjnych> pobierzPowiazaniePracownikowDoJednostekOrganizacyjnychWS()
	{	
		try {
			List<PracownikWJednostkachOrganizacyjnych> listaPracownikowWJO = new ArrayList<PracownikWJednostkachOrganizacyjnych>();

			DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();

			List<PersonAgreement> listaPersonAgreement = dictionaryServiceFactory.getPersonAgreements();

				// Dla ka�dego elementu mapujemy element z WS na element
				// Canonical
				for (PersonAgreement personAgreement : listaPersonAgreement) {
					PracownikWJednostkachOrganizacyjnych pracownikWJednostkachOrganizacyjnych = 
							WSToCanonicalMapper.MapPersonAgreementToPracownikWJednostkachOrganizacyjnych(personAgreement);
					listaPracownikowWJO.add(pracownikWJednostkachOrganizacyjnych);
				}
			
			return listaPracownikowWJO;
		} catch (Exception exc) {
			log.error("B��d podczas pobierania u�ytkownik�w z systemu ERP");
			log.debug(exc);
		}
		return null;
	}

}
