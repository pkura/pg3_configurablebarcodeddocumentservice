package pl.compan.docusafe.general.dto;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.Budzet;
import pl.compan.docusafe.general.hibernate.dao.BudgetViewDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudgetViewDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.BudgetView;

public class BudzetDTO {
	public static final Logger log = LoggerFactory.getLogger(BudzetDTO.class);

	/**
	 * Pobiera budzety z bazy danych
	 * 
	 * @return
	 */
	public List<Budzet> pobierzBudzetZBD() {
		List<Budzet> listaBudzet = new ArrayList<Budzet>();

		List<BudgetViewDB> listaBudgetViewDB;
		try {
			listaBudgetViewDB = BudgetViewDBDAO.list();

			for (BudgetViewDB budgetViewDB : listaBudgetViewDB) {
				Budzet budzet = HibernateToCanonicalMapper
						.MapBudgetViewDBToBudzet(budgetViewDB);
				listaBudzet.add(budzet);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaBudzet;
	}

	/**
	 * Metoda zwraca liste budzety z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * GetBudgetView
	 */
	public List<Budzet> PobierzBudzety() {
		List<Budzet> listaBudzet = new ArrayList<Budzet>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<BudgetView> listBudgetView = dictionaryServiceFactory
				.getBudgetView();

		for (BudgetView unit : listBudgetView) {
			Budzet budzet = WSToCanonicalMapper
					.MapBudgetViewToBudzet(unit);

			listaBudzet.add(budzet);
		}
		return listaBudzet;
	}

	/**
	 * Metoda zapisuje liste budzety do bazy danych
	 * 
	 * @param listaBudzet
	 */
	public boolean Zapisz(List<Budzet> listaBudzet) {

		try {
			for(Budzet budzet:listaBudzet)
			{
				BudgetViewDB budgetView = CanonicalToHibernateMapper.MapBudzetNaBudgetViewDB(budzet);

				if (budzet.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					BudgetViewDBDAO.save(budgetView);
				}
				else
					if (budzet.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						BudgetViewDBDAO.update(budgetView);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
