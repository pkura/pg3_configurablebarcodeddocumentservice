package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.RoleWProjekcie;
import pl.compan.docusafe.general.hibernate.dao.RoleWProjekcieDBDAO;
import pl.compan.docusafe.general.hibernate.model.RoleWProjekcieDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.RolesInProjects;

public class RoleWProjekcieDTO {
public static final Logger log = LoggerFactory.getLogger(RoleWProjekcieDTO.class);
	
	/**
	 * Metoda pobiera role w projekcie z bazy danych
	 */
	public List<RoleWProjekcie> pobierzRoleWProjekcieZBD() {

		List<RoleWProjekcie> listaRoleWProjekcie = new ArrayList<RoleWProjekcie>();

		List<RoleWProjekcieDB> listaRoleWProjekcieDB;
		try {
			listaRoleWProjekcieDB = RoleWProjekcieDBDAO.list();
			for (RoleWProjekcieDB r : listaRoleWProjekcieDB) {
				RoleWProjekcie roleWProjekcie = HibernateToCanonicalMapper
						.MapRoleWProjekcieDBToRoleWProjekcie(r);
				listaRoleWProjekcie.add(roleWProjekcie);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaRoleWProjekcie;
	}
	
	/**
	 * Metoda zwraca liste rol w projekcie z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - GetRolesInProjects
	 * @throws RemoteException 
	 */
	public List<RoleWProjekcie> pobierzRoleWProjekcieZWS() throws RemoteException {
		List<RoleWProjekcie> listaRoleWProjekcie = new ArrayList<RoleWProjekcie>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<RolesInProjects> rolesInProjects = dictionaryService
				.getRoleInProject();

		for (RolesInProjects r : rolesInProjects) {
			RoleWProjekcie roleWProjekcie = WSToCanonicalMapper.MapRolesInProjectsToRoleWProjekcie(r);
			listaRoleWProjekcie.add(roleWProjekcie);
		}
		return listaRoleWProjekcie;
	}
	
	/**
	 * Metoda zapisuje role w projekcie do bazy danych
	 * 
	 * @param listaRoleWProjekcie
	 */
	public boolean zapisz(List<RoleWProjekcie> listaRoleWProjekcie) {
		try {
			for (RoleWProjekcie r : listaRoleWProjekcie) {
				RoleWProjekcieDB roleWProjekcieDB = CanonicalToHibernateMapper.MapRoleWProjekcieToRoleWProjekcieDB(r);

				if (r.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					RoleWProjekcieDBDAO.save(roleWProjekcieDB);
				} else {
					if (r.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						RoleWProjekcieDBDAO.update(roleWProjekcieDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
