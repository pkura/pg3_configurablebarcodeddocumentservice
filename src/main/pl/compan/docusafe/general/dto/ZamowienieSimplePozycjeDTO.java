package pl.compan.docusafe.general.dto;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.ZamowienieSimplePozycje;
import pl.compan.docusafe.general.hibernate.dao.ZamowienieSimplePozycjeDBDAO;
import pl.compan.docusafe.general.hibernate.model.ZamowienieSimplePozycjeDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class ZamowienieSimplePozycjeDTO {

	public static final Logger log = LoggerFactory.getLogger(KontrahentKontoDTO.class);
	/**
	 * Metoda pobiera pozycje zamowien simple z bazy danych
	 */
	public List<ZamowienieSimplePozycje> pobierzZamPozycjeZBD() {

		List<ZamowienieSimplePozycje> listaBudzetowKo = new ArrayList<ZamowienieSimplePozycje>();

		List<ZamowienieSimplePozycjeDB> listaBudzetowKoDB;
		try {
			listaBudzetowKoDB = ZamowienieSimplePozycjeDBDAO.list();
			for (ZamowienieSimplePozycjeDB budzetKoDB : listaBudzetowKoDB) {
				ZamowienieSimplePozycje kontrahentKonto = HibernateToCanonicalMapper
						.MapZamowienieSimplePozycjeDBToZamowienieSimplePozycje(budzetKoDB);
				listaBudzetowKo.add(kontrahentKonto);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaBudzetowKo;
	}
	
	/**
	 * Metoda zapisuje ZamowienieSimplePozycje do bazy danych
	 * 
	 * @param kontrahentKonto
	 */
	public boolean zapisz(List<ZamowienieSimplePozycje> listaKontrahentKonto) {
		try {
			for (ZamowienieSimplePozycje kk : listaKontrahentKonto) {
				ZamowienieSimplePozycjeDB budzetKoKomorekDB = CanonicalToHibernateMapper.MapZamowienieSimplePozycjeToZamowienieSimplePozycjeDB(kk);

				if (kk.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					ZamowienieSimplePozycjeDBDAO.save(budzetKoKomorekDB);
				} else {
					if (kk.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						ZamowienieSimplePozycjeDBDAO.update(budzetKoKomorekDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
