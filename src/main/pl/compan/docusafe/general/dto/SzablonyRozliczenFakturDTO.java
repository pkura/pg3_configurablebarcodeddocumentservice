package pl.compan.docusafe.general.dto;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.SzablonyRozliczenFaktur;
import pl.compan.docusafe.general.hibernate.dao.SzablonyRozliczenFakturDBDAO;
import pl.compan.docusafe.general.hibernate.model.SzablonyRozliczenFakturDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PatternsOfCostSharing;

public class SzablonyRozliczenFakturDTO {

public static final Logger log = LoggerFactory.getLogger(SzablonyRozliczenFakturDTO.class);
	
	
	/**
	 * Metoda pobiera SzablonyRozliczenFaktur z bazy danych
	 */
	public List<SzablonyRozliczenFaktur> pobierzSzablonyRozliczenFakturZBD() {

		List<SzablonyRozliczenFaktur> listaBudDoKom = new ArrayList<SzablonyRozliczenFaktur>();

		List<SzablonyRozliczenFakturDB> listaBudzetowDoKomDB;
		try {
			listaBudzetowDoKomDB = SzablonyRozliczenFakturDBDAO.list();
			for (SzablonyRozliczenFakturDB budzetKoDB : listaBudzetowDoKomDB) {
				SzablonyRozliczenFaktur budzetDoKomorki = HibernateToCanonicalMapper
						.MapSzablonyRozliczenFakturDBDBToSzablonyRozliczenFaktur(budzetKoDB);
				listaBudDoKom.add(budzetDoKomorki);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaBudDoKom;
	}
	
	/**
	 * Metoda zwraca liste SzablonyRozliczenFaktur z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - 
	 */
	public List<SzablonyRozliczenFaktur> pobierzSzablonyRozliczenFakturZWS() {
		List<SzablonyRozliczenFaktur> listaKont = new ArrayList<SzablonyRozliczenFaktur>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<PatternsOfCostSharing> unitBudgetKos = dictionaryService
				.getPatternsOfCostSharing();

		for (PatternsOfCostSharing sba : unitBudgetKos) {
			SzablonyRozliczenFaktur kontrahentKonto = WSToCanonicalMapper.MapPatternsOfCostSharingToSzablonyRozliczenFaktur(sba);
			listaKont.add(kontrahentKonto);
		}
		return listaKont;
	}
	
	/**
	 * Metoda zapisuje SzablonyRozliczenFaktur do bazy danych
	 * 
	 * @param SzablonyRozliczenFaktur
	 */
	public boolean zapisz(List<SzablonyRozliczenFaktur> listaKontrahentKonto) {
		try {
			for (SzablonyRozliczenFaktur kk : listaKontrahentKonto) {
				SzablonyRozliczenFakturDB budzetKoKomorekDB = CanonicalToHibernateMapper.MapSzablonyRozliczenFakturToSzablonyRozliczenFakturDB(kk);

				if (kk.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					SzablonyRozliczenFakturDBDAO.save(budzetKoKomorekDB);
				} else {
					if (kk.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						SzablonyRozliczenFakturDBDAO.update(budzetKoKomorekDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
