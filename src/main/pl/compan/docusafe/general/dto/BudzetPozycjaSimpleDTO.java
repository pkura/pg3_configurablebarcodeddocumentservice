package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.BudzetPozycjaSimple;
import pl.compan.docusafe.general.hibernate.dao.BudzetPozycjaSimpleDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudzetPozycjaSimpleDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitBudgetKoKind;

public class BudzetPozycjaSimpleDTO {

public static final Logger log = LoggerFactory.getLogger(KontrahentKontoDTO.class);
	
	
	/**
	 * Metoda pobiera budzet pozycje z bazy danych
	 */
	public List<BudzetPozycjaSimple> pobierzBudzetyPozycjeZBD() {

		List<BudzetPozycjaSimple> listaBudzetowKo = new ArrayList<BudzetPozycjaSimple>();

		List<BudzetPozycjaSimpleDB> listaBudzetowKoDB;
		try {
			listaBudzetowKoDB = BudzetPozycjaSimpleDBDAO.list();
			for (BudzetPozycjaSimpleDB budzetKoDB : listaBudzetowKoDB) {
				BudzetPozycjaSimple kontrahentKonto = HibernateToCanonicalMapper
						.MapBudzetPozycjaSimpleDBToBudzetPozycjaSimple(budzetKoDB);
				listaBudzetowKo.add(kontrahentKonto);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaBudzetowKo;
	}
	
	/**
	 * Metoda zwraca liste kont z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - GetUnitBudgetKoKind
	 * @throws RemoteException 
	 */
	public List<BudzetPozycjaSimple> pobierzBudzetyPozycjeSimpleZWS() throws RemoteException {
		List<BudzetPozycjaSimple> listaKont = new ArrayList<BudzetPozycjaSimple>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<UnitBudgetKoKind> unitBudgetKos = dictionaryService
				.getUnitBudgetKoKind();

		for (UnitBudgetKoKind sba : unitBudgetKos) {
			BudzetPozycjaSimple kontrahentKonto = WSToCanonicalMapper.MapUnitBudgetKoKindToBudzetPozycjaSimple(sba);
			listaKont.add(kontrahentKonto);
		}
		return listaKont;
	}
	
	/**
	 * Metoda zapisuje budzet pozycje do bazy danych
	 * 
	 * @param kontrahentKonto
	 */
	public boolean zapisz(List<BudzetPozycjaSimple> listaKontrahentKonto) {
		try {
			for (BudzetPozycjaSimple kk : listaKontrahentKonto) {
				BudzetPozycjaSimpleDB budzetKoKomorekDB = CanonicalToHibernateMapper.MapbudzetPozycjeSimpleToBudzetPozycjeSimpleDB(kk);

				if (kk.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					BudzetPozycjaSimpleDBDAO.save(budzetKoKomorekDB);
				} else {
					if (kk.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						BudzetPozycjaSimpleDBDAO.update(budzetKoKomorekDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
