package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.model.JednostkaMiary;
import pl.compan.docusafe.general.hibernate.model.UnitOfMeasureDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitOfMeasure;

public class JednostkaMiaryDTO {
	
	public static final Logger log = LoggerFactory.getLogger(JednostkaMiaryDTO.class);

	/**
	 * Pobiera jednostki miary z bazy danych
	 * @return
	 */
	public List<JednostkaMiary> pobierzJednostkaMiaryZBD() {
		List<JednostkaMiary> listaJednostkaMiary= new ArrayList<JednostkaMiary>();

		List<UnitOfMeasureDB> listaUnitOfMeasureDB;
		try {
			listaUnitOfMeasureDB = UnitOfMeasureDB.list();

			for (UnitOfMeasureDB unitOfMeasureDB : listaUnitOfMeasureDB) {
				JednostkaMiary jednostkaMiary = HibernateToCanonicalMapper
						.MapUnitOfMeasureDBToJednostkaMiary(unitOfMeasureDB);
				listaJednostkaMiary.add(jednostkaMiary);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaJednostkaMiary;
	}
	
	/**
	 * Metoda zwraca liste JednostkaMiary z WebSerwisu
	 * Metoda u�ywa dowolnego, skonfigurowanego webserwisu Simple DictionaryService - GetUnitOfMeasure
	 * @throws RemoteException 
	 */
	public List<JednostkaMiary>  PobierzJednostkaMiary() throws RemoteException
	{
		List<JednostkaMiary> listaJednostkaMiary = new ArrayList<JednostkaMiary>();


		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<UnitOfMeasure> listUnitOfMeasure = dictionaryServiceFactory.getUnitOfMeasure();

		for (UnitOfMeasure unit : listUnitOfMeasure)
		{
			JednostkaMiary jm = WSToCanonicalMapper.MapUnitOfMeasureToJednostkaMiary(unit);

			listaJednostkaMiary.add(jm);
		}

		return listaJednostkaMiary;

	}
	
	/**
	 * Metoda zapisuje liste jednostkaMiary do bazy danych
	 * @param JednostkaMiary
	 */
	public boolean Zapisz(JednostkaMiary jednostkaMiary)
	{
		UnitOfMeasureDB unitOfMeasureDB = new UnitOfMeasureDB();
		try {
			switch(jednostkaMiary.getStanObiektu())
			{
			case NOWY:
				unitOfMeasureDB = CanonicalToHibernateMapper.MapJednostkaMiaryNaUnitOfMeasureDB(unitOfMeasureDB, jednostkaMiary);	
				unitOfMeasureDB.save();
				break;			
			case ZMODYFIKOWANY:
				List<UnitOfMeasureDB> jmErpID = UnitOfMeasureDB.findByErpID(jednostkaMiary.getErpId());
				for(UnitOfMeasureDB uom:jmErpID){
					unitOfMeasureDB = CanonicalToHibernateMapper.MapJednostkaMiaryNaUnitOfMeasureDB(uom, jednostkaMiary);
					unitOfMeasureDB.save();
				}
				break;
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
