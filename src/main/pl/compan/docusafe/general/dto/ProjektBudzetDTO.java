package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.ProjektBudzet;
import pl.compan.docusafe.general.hibernate.dao.ProjektBudzetDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectBugdet;

public class ProjektBudzetDTO {
	
	public static final Logger log = LoggerFactory.getLogger(ProjektBudzetDTO.class);

	/**
	 * Pobiera projekty budzetow  z bazy danych
	 * 
	 * @return
	 */
	public List<ProjektBudzet> pobierzProjektBudzetZBD() {
		List<ProjektBudzet> listaProjektBudzet = new ArrayList<ProjektBudzet>();

		List<ProjektBudzetDB> listaProjectBugdetDB;
		try {
			listaProjectBugdetDB = ProjektBudzetDBDAO.list();

			for (ProjektBudzetDB projectBugdetDB : listaProjectBugdetDB) {
				ProjektBudzet projektBudzet = HibernateToCanonicalMapper
						.MapProjectBugdetDBToProjektBudzet(projectBugdetDB);
				listaProjektBudzet.add(projektBudzet);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaProjektBudzet;
	}

	/**
	 * Metoda zwraca liste ProjektBudzet z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * GetProjectBugdet
	 * @throws RemoteException 
	 */
	public List<ProjektBudzet> PobierzProjektBudzet() throws RemoteException {
		List<ProjektBudzet> listaProjektBudzet = new ArrayList<ProjektBudzet>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<ProjectBugdet> listProjectBugdet = dictionaryServiceFactory
				.getProjectBugdet();

		for (ProjectBugdet unit : listProjectBugdet) {
			ProjektBudzet projektBudzet = WSToCanonicalMapper
					.MapProjectBugdetToProjektBudzet(unit);

			listaProjektBudzet.add(projektBudzet);
		}
		return listaProjektBudzet;
	}

	/**
	 * Metoda zapisuje liste ProjektBudzet do bazy danych
	 * 
	 * @param listaProjektBudzet
	 */
	public boolean Zapisz(List<ProjektBudzet> listaProjektBudzet) {

		try {
			for(ProjektBudzet projektBudzet:listaProjektBudzet)
			{
				ProjektBudzetDB projectBudgetDB = CanonicalToHibernateMapper.MapProjektBudzetNaProjectProjectBugdetDB(projektBudzet);

				if (projektBudzet.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					ProjektBudzetDBDAO.save(projectBudgetDB);
				}
				else
					if (projektBudzet.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						ProjektBudzetDBDAO.update(projectBudgetDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
