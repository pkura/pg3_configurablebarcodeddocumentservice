package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.PlanZakupow;
import pl.compan.docusafe.general.hibernate.dao.PlanZakupowDBDAO;
import pl.compan.docusafe.general.hibernate.model.PlanZakupowDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PurchasePlan;

public class PlanZakupowDTO {
	
public static final Logger log = LoggerFactory.getLogger(PlanZakupowDTO.class);
	
	/**
	 * Metoda pobiera plan zakupow z bazy danych
	 */
	public List<PlanZakupow> pobierzPlanZakupowZBD() {

		List<PlanZakupow> listaPlanowZakupu = new ArrayList<PlanZakupow>();

		List<PlanZakupowDB> listaPlanZakupowDB;
		try {
			listaPlanZakupowDB = PlanZakupowDBDAO.list();
			for (PlanZakupowDB pz : listaPlanZakupowDB) {
				PlanZakupow planZakupow = HibernateToCanonicalMapper
						.MapPlanZakupowoDBToPlanZakupow(pz);
				listaPlanowZakupu.add(planZakupow);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaPlanowZakupu;
	}
	
	/**
	 * Metoda zwraca liste planow zakupu z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - GetPurchasePlan
	 * @throws RemoteException 
	 */
	public List<PlanZakupow> pobierzPlanZakupowZWS() throws RemoteException {
		List<PlanZakupow> listaPlanZakupow= new ArrayList<PlanZakupow>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<PurchasePlan> purchasePlan = dictionaryService
				.getPurchasePlan();

		for (PurchasePlan pp : purchasePlan) {
			PlanZakupow planZakupow = WSToCanonicalMapper.MapPurchasePlanToPlanZakupow(pp);
			listaPlanZakupow.add(planZakupow);
		}
		return listaPlanZakupow;
	}
	
	/**
	 * Metoda zapisuje plan zakupow do bazy danych
	 * 
	 * @param listaPlanZakupow
	 */
	public boolean zapisz(List<PlanZakupow> listaPlanZakupow) {
		try {
			for (PlanZakupow pz : listaPlanZakupow) {
				PlanZakupowDB planZakupowDB = CanonicalToHibernateMapper.MapPlanZakupowToPlanZakupowDB(pz);

				if (pz.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					PlanZakupowDBDAO.save(planZakupowDB);
				} else {
					if (pz.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						PlanZakupowDBDAO.update(planZakupowDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
