package pl.compan.docusafe.general.dto;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.hibernate.dao.DSDivisionFactory;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.OrganizationalUnit;

public class JednostkaOrganizacyjnaDTO {

	public static final Logger log = LoggerFactory.getLogger(JednostkaOrganizacyjnaDTO.class);
	
	
	/**
	 * Metoda zwraca list� Jednostek Organizacyjnych z bazy danych DocuSafe
	 * Metoda otwiera dodatkowo kontekst bazy danych
	 * @return
	 */
	public List<JednostkaOrganizacyjna> pobierzJednostkiOrganizacyjneZBD_OC()
	{ 
		List<JednostkaOrganizacyjna> lista = null;
		try {
			DSApi.openContextIfNeeded();
			DSApi.context().begin();
			lista =  pobierzJednostkiOrganizacyjneZBD();	
			DSApi.context().commit();
		} catch (Exception e){
			log.error("",e);
			DSApi.context()._rollback();			
		} finally {
			DSApi._close();			
		}
		return lista;		
	}	
	
	/**
	 * Metoda zwraca list� Jednostek Organizacyjnych z bazy danych DocuSafe
	 * @return
	 */
	public List<JednostkaOrganizacyjna> pobierzJednostkiOrganizacyjneZBD() {
		List<JednostkaOrganizacyjna> listaJednostekOrganizacyjnych = new ArrayList<JednostkaOrganizacyjna>();

		DSDivisionFactory factory = new DSDivisionFactory();
		List<DSDivision> listaDsDivision = factory.getDsDivisions();

		for (DSDivision division : listaDsDivision) {
			JednostkaOrganizacyjna jo = HibernateToCanonicalMapper
					.MapDSDivisionToJednostkaOrganizacyjna(division);
			listaJednostekOrganizacyjnych.add(jo);
		}

		return listaJednostekOrganizacyjnych;
	}	
	
	/**
	 * Metoda zwraca list� JednostkaOrganizacyjna z WebSerwisu [WS]
	 * Metoda u�ywa dowolnego, skonfigurowanego webserwisu Simple DictionaryService - GetOrganizationalUnit
	 * @return
	 */
	public List<JednostkaOrganizacyjna>  PobierzJednostkiOrganizacyjneWS()
	{
		List<JednostkaOrganizacyjna> listaJednostekOrganizacyjnych = new ArrayList<JednostkaOrganizacyjna>();
		
		// Pobranie z WS liczby wszystkich element�w struktury organizacyjnej
		
		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<OrganizationalUnit> listaOrganizationalUnits = dictionaryServiceFactory.getOrganizationUnits();
				
		String value = Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_ERP_JEDNOSTKA_ORGANIZACYJNA_CZY_IMPORTOWAC_NIEAKTYWNE");
		
		boolean importujNieaktywne = false;
		
		if (value!=null && value.equals("true"))
		{
			importujNieaktywne = true;
		}
		
		// Dla ka�dego elementu mapujemy element z WS na element Canonical
		for (OrganizationalUnit unit : listaOrganizationalUnits)
		{
			boolean dodaj = true;
			
			if (unit.getCzy_aktywna()==0.0 && !importujNieaktywne)
			{
				dodaj = false;
			}
			
			if (dodaj)
			{
				JednostkaOrganizacyjna jednostkaOrganizacyjna = WSToCanonicalMapper.MapOrganizationalUnitToJednostkaOrganizacyjna(unit);
				
				listaJednostekOrganizacyjnych.add(jednostkaOrganizacyjna);
			}
		}
		
		return listaJednostekOrganizacyjnych;
		
	}
	
	/**
	 * Metoda pobiera jednostk� organizacyjn� z bazy danych po jej Idm
	 * @return
	 */
	public JednostkaOrganizacyjna pobierzJednostkeOrganizacyjnaPoIdmDB(String idm)
	{
		JednostkaOrganizacyjna jednostkaOrganizacyjna = null;
			
		try {
			DSApi.openContextIfNeeded();
			DSApi.context().begin();
			DSDivision division  =  DSDivision.findByExternalId(idm);	
			jednostkaOrganizacyjna = HibernateToCanonicalMapper.MapDSDivisionToJednostkaOrganizacyjna(division);
			DSApi.context().commit();
		} catch (Exception e){
			log.debug(e);					
		} finally {
			DSApi._close();			
		}
		return jednostkaOrganizacyjna;
		
		
		
	}
}
