package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.ProjectAccountPiatki;
import pl.compan.docusafe.general.hibernate.dao.ProjectAccountPiatkiDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProjectAccountPiatkiDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectAccount;

public class ProjectAccountPiatkiDTO {

	public static final Logger log = LoggerFactory.getLogger(BudzetDoKomorkiDTO.class);
	
	
	/**
	 * Metoda pobiera piatki z bazy danych
	 */
	public List<ProjectAccountPiatki> pobierzPiatkiZBD() {

		List<ProjectAccountPiatki> listaBudDoKom = new ArrayList<ProjectAccountPiatki>();

		List<ProjectAccountPiatkiDB> listaBudzetowDoKomDB;
		try {
			listaBudzetowDoKomDB = ProjectAccountPiatkiDBDAO.list();
			for (ProjectAccountPiatkiDB budzetKoDB : listaBudzetowDoKomDB) {
				ProjectAccountPiatki budzetDoKomorki = HibernateToCanonicalMapper
						.MapProjectAccountPiatkiDBToProjectAccountPiatki(budzetKoDB);
				listaBudDoKom.add(budzetDoKomorki);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaBudDoKom;
	}
	
	/**
	 * Metoda zwraca liste ProjectAccountPiatki z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - getProjectAccount
	 * @throws RemoteException 
	 */
	public List<ProjectAccountPiatki> pobierzPiatkiZWS() throws RemoteException {
		List<ProjectAccountPiatki> listaKont = new ArrayList<ProjectAccountPiatki>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<ProjectAccount> unitBudgetKos = dictionaryService
				.getProjectAccount();

		for (ProjectAccount sba : unitBudgetKos) {
			ProjectAccountPiatki kontrahentKonto = WSToCanonicalMapper.MapProjectAccountPiatkiToProjectAccountPiatki(sba);
			listaKont.add(kontrahentKonto);
		}
		return listaKont;
	}
	
	/**
	 * Metoda zapisuje ProjectAccountPiatki do bazy danych
	 * 
	 * @param ProjectAccountPiatki
	 */
	public boolean zapisz(List<ProjectAccountPiatki> listaKontrahentKonto) {
		try {
			for (ProjectAccountPiatki kk : listaKontrahentKonto) {
				ProjectAccountPiatkiDB budzetKoKomorekDB = CanonicalToHibernateMapper.MapProjectAccountPiatkiToProjectAccountPiatkiDB(kk);

				if (kk.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					ProjectAccountPiatkiDBDAO.save(budzetKoKomorekDB);
				} else {
					if (kk.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						ProjectAccountPiatkiDBDAO.update(budzetKoKomorekDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
