package pl.compan.docusafe.general.dto;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.TypyUrlopow;
import pl.compan.docusafe.general.hibernate.dao.TypyUrlopowDBDAO;
import pl.compan.docusafe.general.hibernate.model.TypyUrlopowDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.AbsenceType;

public class TypyUrlopowDTO {
	public static final Logger log = LoggerFactory.getLogger(TypyUrlopowDTO.class);

	/**
	 * Pobiera typy urlopow z bazy danych
	 * 
	 * @return
	 */
	public List<TypyUrlopow> pobierzTypyUrlopowZBD() {
		List<TypyUrlopow> listaTypyUrlopow = new ArrayList<TypyUrlopow>();

		List<TypyUrlopowDB> listaTypyUrlopowDB;
		try {
			listaTypyUrlopowDB = TypyUrlopowDBDAO.list();

			for (TypyUrlopowDB typyUrlopowDB : listaTypyUrlopowDB) {
				TypyUrlopow typyUrlopow = HibernateToCanonicalMapper
						.MapTypyUrlopowDBToTypyUrlopow(typyUrlopowDB);
				listaTypyUrlopow.add(typyUrlopow);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaTypyUrlopow;
	}

	/**
	 * Metoda zwraca liste typow urlopu z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * GetAbsenceType
	 */
	public List<TypyUrlopow> PobierzTypyUrlopow() {
		List<TypyUrlopow> listaTypyUrlopow = new ArrayList<TypyUrlopow>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<AbsenceType> listAbsenceType = dictionaryServiceFactory
				.getAbsenceType();

		for (AbsenceType unit : listAbsenceType) {
			TypyUrlopow typyUrlopow = WSToCanonicalMapper
					.MapAbsenceTypeToTypyUrlopow(unit);

			listaTypyUrlopow.add(typyUrlopow);
		}
		return listaTypyUrlopow;
	}

	/**
	 * Metoda zapisuje liste typow urlopu do bazy danych
	 * 
	 * @param listaTypyUrlopow
	 */
	public boolean Zapisz(List<TypyUrlopow> listaTypyUrlopow) {

		try {
			for(TypyUrlopow typyUrlopow:listaTypyUrlopow)
			{
				TypyUrlopowDB typyUrlopowDB = CanonicalToHibernateMapper.MapTypyUrlopowNaTypyUrlopowDB(typyUrlopow);

				if (typyUrlopow.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					TypyUrlopowDBDAO.save(typyUrlopowDB);
				}
				else
					if (typyUrlopow.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						TypyUrlopowDBDAO.update(typyUrlopowDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
