package pl.compan.docusafe.general.dto;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.KontrahentKonto;
import pl.compan.docusafe.general.hibernate.dao.KontrahentKontoDBDAO;
import pl.compan.docusafe.general.hibernate.model.KontrahentKontoDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.SupplierBankAccount;

public class KontrahentKontoDTO {

	public static final Logger log = LoggerFactory.getLogger(KontrahentKontoDTO.class);
	
	
	/**
	 * Metoda pobiera konta bankowe z bazy danych
	 */
	public List<KontrahentKonto> pobierzKontoZBD() {

		List<KontrahentKonto> listaKont = new ArrayList<KontrahentKonto>();

		List<KontrahentKontoDB> listaKontDB;
		try {
			listaKontDB = KontrahentKontoDBDAO.list();
			for (KontrahentKontoDB kontoDB : listaKontDB) {
				KontrahentKonto kontrahentKonto = HibernateToCanonicalMapper
						.MapKontrahentKontoDBToKontrahentKonto(kontoDB);
				listaKont.add(kontrahentKonto);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaKont;
	}
	
	/**
	 * Metoda zwraca liste kont z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - GetSupplierBankAccount
	 */
	public List<KontrahentKonto> pobierzKontaZWS() {
		List<KontrahentKonto> listaKont = new ArrayList<KontrahentKonto>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<SupplierBankAccount> supplierBankAccount = dictionaryService
				.getSupplierBankAccount();

		for (SupplierBankAccount sba : supplierBankAccount) {
			KontrahentKonto kontrahentKonto = WSToCanonicalMapper.MapSupplierBankAccountToKontrahentKonto(sba);
			listaKont.add(kontrahentKonto);
		}
		return listaKont;
	}
	
	/**
	 * Metoda zapisuje konta kontrahentow do bazy danych
	 * 
	 * @param kontrahentKonto
	 */
	public boolean zapisz(List<KontrahentKonto> listaKontrahentKonto) {
		try {
			for (KontrahentKonto kk : listaKontrahentKonto) {
				KontrahentKontoDB kontrahentKontoDB = CanonicalToHibernateMapper.MapKontrahentKontoToKontrahentKontoDB(kk);

				if (kk.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					KontrahentKontoDBDAO.save(kontrahentKontoDB);
				} else {
					if (kk.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						KontrahentKontoDBDAO.update(kontrahentKontoDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
