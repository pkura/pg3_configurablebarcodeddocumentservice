package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.RoleWBudzecie;
import pl.compan.docusafe.general.hibernate.dao.RoleWBudzecieDBDAO;
import pl.compan.docusafe.general.hibernate.model.RoleWBudzecieDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.RolesInUnitBudgetKo;

public class RoleWBudzecieDTO {
	public static final Logger log = LoggerFactory.getLogger(RoleWBudzecieDTO.class);

	/**
	 * Metoda pobiera role w budzecie z bazy danych
	 */
	public List<RoleWBudzecie> pobierzRoleWBudzecieZBD() {

		List<RoleWBudzecie> listaRoleWBudzecie = new ArrayList<RoleWBudzecie>();

		List<RoleWBudzecieDB> listaRoleWBudzecieDB;
		try {
			listaRoleWBudzecieDB = RoleWBudzecieDBDAO.list();
			for (RoleWBudzecieDB r : listaRoleWBudzecieDB) {
				RoleWBudzecie roleWBudzecie = HibernateToCanonicalMapper
						.MapRoleWBudzecieDBToRoleWBudzecie(r);
				listaRoleWBudzecie.add(roleWBudzecie);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaRoleWBudzecie;
	}

	/**
	 * Metoda zwraca liste rol w budzecie z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - GetRolesInUnitBudgetKo
	 * @throws RemoteException 
	 */
	public List<RoleWBudzecie> pobierzRoleWBudzecieZWS() throws RemoteException {
		List<RoleWBudzecie> listaRoleWBudzecie = new ArrayList<RoleWBudzecie>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<RolesInUnitBudgetKo> rolesInUnitBudgetKo = dictionaryService
				.getRolesInUnitBudgetKo();

		for (RolesInUnitBudgetKo r : rolesInUnitBudgetKo) {
			RoleWBudzecie roleWBudzecie = WSToCanonicalMapper.MapRolesInUnitBudgetKoToRoleWBudzecie(r);
			listaRoleWBudzecie.add(roleWBudzecie);
		}
		return listaRoleWBudzecie;
	}

	/**
	 * Metoda zapisuje role w budzecie do bazy danych
	 * 
	 * @param listaRoleWBudzecie
	 */
	public boolean zapisz(List<RoleWBudzecie> listaRoleWBudzecie) {
		try {
			for (RoleWBudzecie r : listaRoleWBudzecie) {
				RoleWBudzecieDB roleWBudzecieDB = CanonicalToHibernateMapper.MapRoleWBudzecieToRoleWBudzecieDB(r);

				if (r.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					RoleWBudzecieDBDAO.save(roleWBudzecieDB);
				} else {
					if (r.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						RoleWBudzecieDBDAO.update(roleWBudzecieDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
