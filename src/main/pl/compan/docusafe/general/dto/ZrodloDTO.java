package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.Zrodlo;
import pl.compan.docusafe.general.hibernate.dao.ZrodloDBDAO;
import pl.compan.docusafe.general.hibernate.model.ZrodloDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.FundingSource;

public class ZrodloDTO {
	public static final Logger log = LoggerFactory.getLogger(ZrodloDTO.class);

	/**
	 * Pobiera zrodlo z bazy danych
	 * 
	 * @return
	 */
	public List<Zrodlo> pobierzZrodloZBD() {
		List<Zrodlo> listaZrodlo = new ArrayList<Zrodlo>();

		List<ZrodloDB> listaZrodloDB;
		try {
			listaZrodloDB = ZrodloDBDAO.list();

			for (ZrodloDB zrodloDB : listaZrodloDB) {
				Zrodlo zrodlo = HibernateToCanonicalMapper
						.MapZrodloDBToZrodlo(zrodloDB);
				listaZrodlo.add(zrodlo);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaZrodlo;
	}

	/**
	 * Metoda zwraca liste Zrodlo z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * GetFundingSource
	 * @throws RemoteException 
	 */
	public List<Zrodlo> PobierzZrodlo() throws RemoteException {
		List<Zrodlo> listaZrodlo = new ArrayList<Zrodlo>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<FundingSource> listFundingSource = dictionaryServiceFactory
				.getFundingSource();

		for (FundingSource unit : listFundingSource) {
			Zrodlo zrodlo = WSToCanonicalMapper
					.MapFundingSourceToZrodlo(unit);

			listaZrodlo.add(zrodlo);
		}
		return listaZrodlo;
	}

	/**
	 * Metoda zapisuje liste Zrodlo do bazy danych
	 * 
	 * @param listaZrodlo
	 */
	public boolean Zapisz(List<Zrodlo> listaZrodlo) {

		try {
			for(Zrodlo zrodlo:listaZrodlo)
			{
				ZrodloDB zrodloDB = CanonicalToHibernateMapper.MapZrodloNaZrodloDB(zrodlo);

				if (zrodlo.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					ZrodloDBDAO.save(zrodloDB);
				}
				else
					if (zrodlo.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						ZrodloDBDAO.update(zrodloDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
