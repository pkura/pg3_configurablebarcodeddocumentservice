package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.ZrodloFinansowaniaProjektow;
import pl.compan.docusafe.general.hibernate.dao.SourcesOfProjectFundingDBDAO;
import pl.compan.docusafe.general.hibernate.model.SourcesOfProjectFundingDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.SourcesOfProjectFunding;

public class ZrodloFinansowaniaProjektowDTO {
	public static final Logger log = LoggerFactory.getLogger(ZrodloFinansowaniaProjektowDTO.class);

	/**
	 * Pobiera zrodlo finansowania projektow z bazy danych
	 * 
	 * @return
	 */
	public List<ZrodloFinansowaniaProjektow> pobierzZrodloFinansowaniaProjektowZBD() {
		List<ZrodloFinansowaniaProjektow> listaZrodloFinansowaniaProjektow = new ArrayList<ZrodloFinansowaniaProjektow>();

		List<SourcesOfProjectFundingDB> listaSourcesOfProjectFundingDB;
		try {
			listaSourcesOfProjectFundingDB = SourcesOfProjectFundingDBDAO.list();

			for (SourcesOfProjectFundingDB sourcesOfProjectFundingDB : listaSourcesOfProjectFundingDB) {
				ZrodloFinansowaniaProjektow zrodloFinansowaniaProjektow = HibernateToCanonicalMapper
						.MapSourcesOfProjectFundingDBToZrodloFinansowaniaProjektow(sourcesOfProjectFundingDB);
				listaZrodloFinansowaniaProjektow.add(zrodloFinansowaniaProjektow);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaZrodloFinansowaniaProjektow;
	}

	/**
	 * Metoda zwraca liste ZrodloFinansowaniaProjektow z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * GetSourcesOfProjectFunding
	 * @throws RemoteException 
	 */
	public List<ZrodloFinansowaniaProjektow> PobierzZrodloFinansowaniaProjektow() throws RemoteException {
		List<ZrodloFinansowaniaProjektow> listaZrodloFinansowaniaProjektow = new ArrayList<ZrodloFinansowaniaProjektow>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<SourcesOfProjectFunding> listSourcesOfProjectFunding = dictionaryServiceFactory
				.getSourcesOfProjectFunding();

		for (SourcesOfProjectFunding unit : listSourcesOfProjectFunding) {
			ZrodloFinansowaniaProjektow zrodloFinansowaniaProjektow = WSToCanonicalMapper
					.MapSourcesOfProjectFundingToZrodloFinansowaniaProjektow(unit);

			listaZrodloFinansowaniaProjektow.add(zrodloFinansowaniaProjektow);
		}
		return listaZrodloFinansowaniaProjektow;
	}

	/**
	 * Metoda zapisuje liste ZrodloFinansowaniaProjektow do bazy danych
	 * 
	 * @param listaZrodloFinansowaniaProjektow
	 */
	public boolean Zapisz(List<ZrodloFinansowaniaProjektow> listaZrodloFinansowaniaProjektow) {

		try {
			for(ZrodloFinansowaniaProjektow zrodloFinansowaniaProjektow:listaZrodloFinansowaniaProjektow)
			{
				SourcesOfProjectFundingDB sourcesOfProjectFundingDB = CanonicalToHibernateMapper.MapZrodloFinansowaniaProjektowNaSourcesOfProjectFundingDB(zrodloFinansowaniaProjektow);

				if (zrodloFinansowaniaProjektow.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					SourcesOfProjectFundingDBDAO.save(sourcesOfProjectFundingDB);
				}
				else
					if (zrodloFinansowaniaProjektow.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						SourcesOfProjectFundingDBDAO.update(sourcesOfProjectFundingDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
