package pl.compan.docusafe.general.dto;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.TypyWnioskowORezerwacje;
import pl.compan.docusafe.general.hibernate.dao.TypyWnioskowORezerwacjeDBDAO;
import pl.compan.docusafe.general.hibernate.model.TypyWnioskowORezerwacjeDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ApplicationForReservationType;

public class TypyWnioskowORezerwacjeDTO {

	public static final Logger log = LoggerFactory.getLogger(TypyWnioskowORezerwacjeDTO.class);
	
	
	/**
	 * Metoda pobiera typy wnioskow o rezerwacje z bazy danych
	 */
	public List<TypyWnioskowORezerwacje> pobierzTypyWnioskowZBD() {

		List<TypyWnioskowORezerwacje> listaTypowWnioskow = new ArrayList<TypyWnioskowORezerwacje>();

		List<TypyWnioskowORezerwacjeDB> listaTypyWnioskowORezerwacjeDB;
		try {
			listaTypyWnioskowORezerwacjeDB = TypyWnioskowORezerwacjeDBDAO.list();
			for (TypyWnioskowORezerwacjeDB typ : listaTypyWnioskowORezerwacjeDB) {
				TypyWnioskowORezerwacje typyWnioskowORezerwacje = HibernateToCanonicalMapper
						.MapTypyWnioskowORezerwacjeDBToTypyWnioskowORezerwacje(typ);
				listaTypowWnioskow.add(typyWnioskowORezerwacje);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaTypowWnioskow;
	}
	
	/**
	 * Metoda zwraca liste typow wnioskow o rezerwacje z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - GetApplicationForReservationType
	 */
	public List<TypyWnioskowORezerwacje> pobierzTypyWnioskowZWS() {
		List<TypyWnioskowORezerwacje> listaTypowWnioskow = new ArrayList<TypyWnioskowORezerwacje>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<ApplicationForReservationType> applicationForREservationType = dictionaryService
				.getApplicationForReservationType();

		for (ApplicationForReservationType app : applicationForREservationType) {
			TypyWnioskowORezerwacje typWnioskowORezerwacje = WSToCanonicalMapper.MapApplicationForReservationTypeToTypyWnioskowORezerwacje(app);
			listaTypowWnioskow.add(typWnioskowORezerwacje);
		}
		return listaTypowWnioskow;
	}
	
	/**
	 * Metoda zapisuje typy wnioskow o rezerwacje do bazy danych
	 * 
	 * @param listaTypowWnioskow
	 */
	public boolean zapisz(List<TypyWnioskowORezerwacje> listaTypowWnioskow) {
		try {
			for (TypyWnioskowORezerwacje tw : listaTypowWnioskow) {
				TypyWnioskowORezerwacjeDB typyWnioskowORezerwacjeDB = CanonicalToHibernateMapper.MapTypyWnioskowORezerwacjeToTypyWnioskowORezerwacjeDB(tw);

				if (tw.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					TypyWnioskowORezerwacjeDBDAO.save(typyWnioskowORezerwacjeDB);
				} else {
					if (tw.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						TypyWnioskowORezerwacjeDBDAO.update(typyWnioskowORezerwacjeDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
