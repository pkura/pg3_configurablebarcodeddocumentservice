package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.ZleceniaProdukcyjne;
import pl.compan.docusafe.general.hibernate.dao.ProductionOrderDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProductionOrderDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProductionOrder;

public class ZleceniaProdukcyjneDTO {
	public static final Logger log = LoggerFactory.getLogger(ZleceniaProdukcyjneDTO.class);

	/**
	 * Pobiera zlecenia produkcyjne zasob z bazy danych
	 * 
	 * @return
	 */
	public List<ZleceniaProdukcyjne> pobierzZleceniaProdukcyjneZBD() {
		List<ZleceniaProdukcyjne> listaZleceniaProdukcyjne = new ArrayList<ZleceniaProdukcyjne>();

		List<ProductionOrderDB> listaProductionOrderDB;
		try {
			listaProductionOrderDB = ProductionOrderDBDAO.list();

			for (ProductionOrderDB productionOrderDB : listaProductionOrderDB) {
				ZleceniaProdukcyjne zleceniaProdukcyjne = HibernateToCanonicalMapper
						.MapProductionOrderDBToZleceniaProdukcyjne(productionOrderDB);
				listaZleceniaProdukcyjne.add(zleceniaProdukcyjne);
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return listaZleceniaProdukcyjne;
	}

	/**
	 * Metoda zwraca liste ZleceniaProdukcyjne z WebSerwisu Metoda u�ywa
	 * dowolnego, skonfigurowanego webserwisu Simple DictionaryService -
	 * GetProductionOrder
	 * @throws RemoteException 
	 */
	public List<ZleceniaProdukcyjne> PobierzZleceniaProdukcyjne() throws RemoteException {
		List<ZleceniaProdukcyjne> listaZleceniaProdukcyjne = new ArrayList<ZleceniaProdukcyjne>();

		DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
		List<ProductionOrder> listProductionOrder = dictionaryServiceFactory
				.getProductionOrder();

		for (ProductionOrder unit : listProductionOrder) {
			ZleceniaProdukcyjne zleceniaProdukcyjne = WSToCanonicalMapper
					.MapProductionOrderToZleceniaProdukcyjne(unit);

			listaZleceniaProdukcyjne.add(zleceniaProdukcyjne);
		}
		return listaZleceniaProdukcyjne;
	}

	/**
	 * Metoda zapisuje liste ZleceniaProdukcyjne do bazy danych
	 * 
	 * @param listaZleceniaProdukcyjne
	 */
	public boolean Zapisz(List<ZleceniaProdukcyjne> listaZleceniaProdukcyjne) {

		try {
			for(ZleceniaProdukcyjne zleceniaProdukcyjne:listaZleceniaProdukcyjne)
			{
				ProductionOrderDB productionOrderDB = CanonicalToHibernateMapper.MapZleceniaProdukcyjneNaProductionOrderDB(zleceniaProdukcyjne);

				if (zleceniaProdukcyjne.getStanObiektu() == STATUS_OBIEKTU.NOWY)
				{				
					ProductionOrderDBDAO.save(productionOrderDB);
				}
				else
					if (zleceniaProdukcyjne.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY)
					{
						ProductionOrderDBDAO.update(productionOrderDB);
					}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return true;
	}
}
