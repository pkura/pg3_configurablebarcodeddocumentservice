package pl.compan.docusafe.general.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.model.NumeryZamowien;
import pl.compan.docusafe.general.hibernate.dao.NumeryZamowienDBDAO;
import pl.compan.docusafe.general.hibernate.model.NumeryZamowienDB;
import pl.compan.docusafe.general.mapers.CanonicalToHibernateMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.general.mapers.WSToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.SupplierOrderHeader;

public class NumeryZamowienDTO {
	
	public static final Logger log = LoggerFactory.getLogger(KontrahentKontoDTO.class);
	
	
	/**
	 * Metoda pobiera numery zamowien z bazy danych
	 */
	public List<NumeryZamowien> pobierzNumeryZamowienZBD() {

		List<NumeryZamowien> listaKont = new ArrayList<NumeryZamowien>();

		List<NumeryZamowienDB> listaKontDB;
		try {
			listaKontDB = NumeryZamowienDBDAO.list();
			for (NumeryZamowienDB kontoDB : listaKontDB) {
				NumeryZamowien kontrahentKonto = HibernateToCanonicalMapper
						.MapNumeryZamowienDBToNumeryZamowien(kontoDB);
				listaKont.add(kontrahentKonto);
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return listaKont;
	}
	
	/**
	 * Metoda zwraca liste numery zamowien z WS Metoda u�ywa dowolnego, skonfigurowanego
	 * webserwisu Simple DictionaryService - getSupplierOrderHeader
	 * @throws RemoteException 
	 */
	public List<NumeryZamowien> pobierzNumeryZamowienZWS() throws RemoteException {
		List<NumeryZamowien> listaKont = new ArrayList<NumeryZamowien>();

		DictionaryServiceFactory dictionaryService = new DictionaryServiceFactory();
		List<SupplierOrderHeader> supplierBankAccount = dictionaryService
				.getSupplierOrderHeader();

		for (SupplierOrderHeader sba : supplierBankAccount) {
			NumeryZamowien kontrahentKonto = WSToCanonicalMapper.MapSupplierOrderHeaderToNumeryZamowien(sba);
			listaKont.add(kontrahentKonto);
		}
		return listaKont;
	}
	
	/**
	 * Metoda zapisuje numery zamowien do bazy danych
	 * 
	 * @param NumeryZamowien
	 */
	public boolean zapisz(List<NumeryZamowien> listaKontrahentKonto) {
		try {
			for (NumeryZamowien kk : listaKontrahentKonto) {
				NumeryZamowienDB kontrahentKontoDB = CanonicalToHibernateMapper.MapNumeryZamowienToNumeryZamowienDB(kk);

				if (kk.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
					NumeryZamowienDBDAO.save(kontrahentKontoDB);
				} else {
					if (kk.getStanObiektu() == STATUS_OBIEKTU.ZMODYFIKOWANY) {
						NumeryZamowienDBDAO.update(kontrahentKontoDB);
					}
				}
			}
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		return true;
	}
}
