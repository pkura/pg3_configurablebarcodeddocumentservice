package pl.compan.docusafe.general.mapers;

import javax.naming.NamingException;
import javax.naming.directory.SearchResult;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.general.canonical.factory.PracownikFactory;
import pl.compan.docusafe.general.canonical.model.Pracownik;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class LDAPToCanonicalMapper {

	public static final Logger log = LoggerFactory
			.getLogger(LDAPToCanonicalMapper.class);
	
	/**
	 * Metoda mapuje login z domeny na pracownika
	 * @param ldapResult
	 * @param pracownik
	 * @return
	 */
	public  Pracownik MapResultToPracownik(SearchResult ldapResult, Pracownik pracownik)
	{
		String ldapLoginField = Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_LDAP_IDENTYFIKATOR_ERP_NAZWA");
		String ldapEmailField = Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_LDAP_EMAIL");
		String login = "";
		String email = "";
		
		// Uzupełnienie loginu użytkownika
		if (ldapLoginField !=null && (!ldapLoginField.equals("")))
		{
			try {
				login = getStringAttr(ldapResult,ldapLoginField);
				pracownik.setLogin(login);
				
			} catch (NamingException e) {
				login = "Wystąpił błąd";
			}							
		}
		else
		{
			pracownik = null;
		}
		
		// Uzupełnienie adresu e-mail dla pracownika
		if (ldapEmailField !=null && (!ldapEmailField.equals("")))
		{
			try {				
				email = getStringAttr(ldapResult,ldapEmailField);
				if (email!=null)
				{
					pracownik.setAdresEmail(email);
				}
				
			} catch (NamingException e) {
				log.debug("LDAP Mapowanie: użytkownik "+ pracownik.getLogin()+" nie posiada adresu e-mail");
			}							
		}
		
		
		return pracownik;
	}
	
	/**
	 * Metoda wspomagająca mapowanie
	 * @param element
	 * @param name
	 * @return
	 * @throws NamingException
	 */
	private String getStringAttr(SearchResult element,String name) throws NamingException
	{
		if(element.getAttributes().get(name) != null)
		{
			return (String) element.getAttributes().get(name) .get();
		}
		return null;
	}

}
