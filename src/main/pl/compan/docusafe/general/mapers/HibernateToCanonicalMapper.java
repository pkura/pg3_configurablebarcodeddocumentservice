package pl.compan.docusafe.general.mapers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.ReferenceToHimselfException;
import pl.compan.docusafe.general.canonical.dao.BudzetDoKomorkiDAO;
import pl.compan.docusafe.general.canonical.dao.BudzetKOKomorekDAO;
import pl.compan.docusafe.general.canonical.dao.BudzetPozycjaDAO;
import pl.compan.docusafe.general.canonical.dao.BudzetyDAO;
import pl.compan.docusafe.general.canonical.dao.NumeryZamowienDAO;
import pl.compan.docusafe.general.canonical.dao.OkresBudzetowyDAO;
import pl.compan.docusafe.general.canonical.dao.FakturaDAO;
import pl.compan.docusafe.general.canonical.dao.JednostkaMiaryDAO;
import pl.compan.docusafe.general.canonical.dao.JednostkaOrganizacyjnaDAO;
import pl.compan.docusafe.general.canonical.dao.KontrahentDAO;
import pl.compan.docusafe.general.canonical.dao.KontrahentKontoDAO;
import pl.compan.docusafe.general.canonical.dao.MiejsceDostawyDAO;
import pl.compan.docusafe.general.canonical.dao.OpkDAO;
import pl.compan.docusafe.general.canonical.dao.PlanZakupowDAO;
import pl.compan.docusafe.general.canonical.dao.PozycjaFakturyDAO;
import pl.compan.docusafe.general.canonical.dao.PracownikDAO;
import pl.compan.docusafe.general.canonical.dao.ProduktyFakturyDAO;
import pl.compan.docusafe.general.canonical.dao.ProjectAccountPiatkiDAO;
import pl.compan.docusafe.general.canonical.dao.ProjektBudzetDAO;
import pl.compan.docusafe.general.canonical.dao.ProjektBudzetEtapDAO;
import pl.compan.docusafe.general.canonical.dao.ProjektBudzetZasobDAO;
import pl.compan.docusafe.general.canonical.dao.ProjektDAO;
import pl.compan.docusafe.general.canonical.dao.PrzeznaczenieZakupuDAO;
import pl.compan.docusafe.general.canonical.dao.RealizacjaBudzetuDAO;
import pl.compan.docusafe.general.canonical.dao.RodzajePlanowZakupuDAO;
import pl.compan.docusafe.general.canonical.dao.RoleWBudzecieDAO;
import pl.compan.docusafe.general.canonical.dao.RoleWProjekcieDAO;
import pl.compan.docusafe.general.canonical.dao.StawkaVatDAO;
import pl.compan.docusafe.general.canonical.dao.SzablonyRozliczenFakturDAO;
import pl.compan.docusafe.general.canonical.dao.TypDokumentuDAO;
import pl.compan.docusafe.general.canonical.dao.TypPlatnosciDAO;
import pl.compan.docusafe.general.canonical.dao.TypyUrlopowDAO;
import pl.compan.docusafe.general.canonical.dao.TypyWnioskowORezerwacjeDAO;
import pl.compan.docusafe.general.canonical.dao.WalutaDAO;
import pl.compan.docusafe.general.canonical.dao.WniosekORezerwacjeDAO;
import pl.compan.docusafe.general.canonical.dao.WydatekStrukturalnyDAO;
import pl.compan.docusafe.general.canonical.dao.ZadaniaFinansoweDAO;
import pl.compan.docusafe.general.canonical.dao.ZamowienieSimplePozycjeDAO;
import pl.compan.docusafe.general.canonical.dao.ZleceniaProdukcyjneDAO;
import pl.compan.docusafe.general.canonical.dao.ZrodloDAO;
import pl.compan.docusafe.general.canonical.dao.ZrodloFinansowaniaProjektowDAO;
import pl.compan.docusafe.general.canonical.model.Budzet;
import pl.compan.docusafe.general.canonical.model.BudzetDoKomorki;
import pl.compan.docusafe.general.canonical.model.BudzetKOKomorek;
import pl.compan.docusafe.general.canonical.model.BudzetPozycja;
import pl.compan.docusafe.general.canonical.model.BudzetPozycjaSimple;
import pl.compan.docusafe.general.canonical.model.BudzetPozycjaSimpleDAO;
import pl.compan.docusafe.general.canonical.model.NumeryZamowien;
import pl.compan.docusafe.general.canonical.model.OkresBudzetowy;
import pl.compan.docusafe.general.canonical.model.Faktura;
import pl.compan.docusafe.general.canonical.model.JednostkaMiary;
import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.canonical.model.Kontrahent;
import pl.compan.docusafe.general.canonical.model.KontrahentKonto;
import pl.compan.docusafe.general.canonical.model.MiejsceDostawy;
import pl.compan.docusafe.general.canonical.model.OPK;
import pl.compan.docusafe.general.canonical.model.PlanZakupow;
import pl.compan.docusafe.general.canonical.model.PozycjaFaktury;
import pl.compan.docusafe.general.canonical.model.PozycjaZamowienia;
import pl.compan.docusafe.general.canonical.model.Pracownik;
import pl.compan.docusafe.general.canonical.model.ProduktyFaktury;
import pl.compan.docusafe.general.canonical.model.ProjectAccountPiatki;
import pl.compan.docusafe.general.canonical.model.Projekt;
import pl.compan.docusafe.general.canonical.model.ProjektBudzet;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetEtap;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetZasob;
import pl.compan.docusafe.general.canonical.model.PrzeznaczenieZakupu;
import pl.compan.docusafe.general.canonical.model.RealizacjaBudzetu;
import pl.compan.docusafe.general.canonical.model.RodzajePlanowZakupu;
import pl.compan.docusafe.general.canonical.model.RoleWBudzecie;
import pl.compan.docusafe.general.canonical.model.RoleWProjekcie;
import pl.compan.docusafe.general.canonical.model.StawkaVat;
import pl.compan.docusafe.general.canonical.model.SzablonyRozliczenFaktur;
import pl.compan.docusafe.general.canonical.model.TypDokumentu;
import pl.compan.docusafe.general.canonical.model.TypPlatnosci;
import pl.compan.docusafe.general.canonical.model.TypyUrlopow;
import pl.compan.docusafe.general.canonical.model.TypyWnioskowORezerwacje;
import pl.compan.docusafe.general.canonical.model.Waluta;
import pl.compan.docusafe.general.canonical.model.WniosekORezerwacje;
import pl.compan.docusafe.general.canonical.model.WydatekStrukturalny;
import pl.compan.docusafe.general.canonical.model.ZadaniaFinansowe;
import pl.compan.docusafe.general.canonical.model.ZamowienieSimplePozycje;
import pl.compan.docusafe.general.canonical.model.ZleceniaProdukcyjne;
import pl.compan.docusafe.general.canonical.model.Zrodlo;
import pl.compan.docusafe.general.canonical.model.ZrodloFinansowaniaProjektow;
import pl.compan.docusafe.general.hibernate.dao.BudgetViewDBDAO;
import pl.compan.docusafe.general.hibernate.dao.BudzetKOKomorkaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.BudzetPozycjaSimpleDBDAO;
import pl.compan.docusafe.general.hibernate.dao.ProjectAccountPiatkiDBDAO;
import pl.compan.docusafe.general.hibernate.dao.SzablonyRozliczenFakturDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudgetViewDB;
import pl.compan.docusafe.general.hibernate.model.BudzetDoKomorkiDB;
import pl.compan.docusafe.general.hibernate.model.BudzetKOKomorekDB;
import pl.compan.docusafe.general.hibernate.model.BudzetPozycjaDB;
import pl.compan.docusafe.general.hibernate.model.BudzetPozycjaSimpleDB;
import pl.compan.docusafe.general.hibernate.model.NumeryZamowienDB;
import pl.compan.docusafe.general.hibernate.model.OkresBudzetowyDB;
import pl.compan.docusafe.general.hibernate.model.CostInvoiceProductDB;
import pl.compan.docusafe.general.hibernate.model.CurrencyDB;
import pl.compan.docusafe.general.hibernate.model.DeliveryLocationDB;
import pl.compan.docusafe.general.hibernate.model.FakturaDB;
import pl.compan.docusafe.general.hibernate.model.FakturaPozycjaDB;
import pl.compan.docusafe.general.hibernate.model.FinancialTaskDB;
import pl.compan.docusafe.general.hibernate.model.KontrahentDB;
import pl.compan.docusafe.general.hibernate.model.KontrahentKontoDB;
import pl.compan.docusafe.general.hibernate.model.OpkDB;
import pl.compan.docusafe.general.hibernate.model.PaymentTermsDB;
import pl.compan.docusafe.general.hibernate.model.PlanZakupowDB;
import pl.compan.docusafe.general.hibernate.model.PracownikDB;
import pl.compan.docusafe.general.hibernate.model.ProductionOrderDB;
import pl.compan.docusafe.general.hibernate.model.ProjectAccountPiatkiDB;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetEtapDB;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetZasobDB;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetDB;
import pl.compan.docusafe.general.hibernate.model.ProjectDB;
import pl.compan.docusafe.general.hibernate.model.PrzeznaczenieZakupuDB;
import pl.compan.docusafe.general.hibernate.model.PurchaseDocumentTypeDB;
import pl.compan.docusafe.general.hibernate.model.RealizacjaBudzetuDB;
import pl.compan.docusafe.general.hibernate.model.RodzajePlanowZakupuDB;
import pl.compan.docusafe.general.hibernate.model.RoleWBudzecieDB;
import pl.compan.docusafe.general.hibernate.model.RoleWProjekcieDB;
import pl.compan.docusafe.general.hibernate.model.SourcesOfProjectFundingDB;
import pl.compan.docusafe.general.hibernate.model.SzablonyRozliczenFakturDB;
import pl.compan.docusafe.general.hibernate.model.TypyUrlopowDB;
import pl.compan.docusafe.general.hibernate.model.TypyWnioskowORezerwacjeDB;
import pl.compan.docusafe.general.hibernate.model.UnitOfMeasureDB;
import pl.compan.docusafe.general.hibernate.model.VatRateDB;
import pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjeDB;
import pl.compan.docusafe.general.hibernate.model.WydatekStrukturalnyDB;
import pl.compan.docusafe.general.hibernate.model.ZamowienieSimplePozycjeDB;
import pl.compan.docusafe.general.hibernate.model.ZrodloDB;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * Klasa mapuje obiekty Hibernatowe [HB] na obiekty modelu kanonicznego [CAN]
 * 
 * @author Gubari Nanokata
 * 
 */
public class HibernateToCanonicalMapper {

	/**
	 * Metoda mapuje obiekt DSDivision [HB] na JednostkaOrganizacyjna [CAN]
	 * 
	 * @param division
	 * @return
	 */
	private static final Logger log = LoggerFactory.getLogger(HibernateToCanonicalMapper.class);

	public static JednostkaOrganizacyjna MapDSDivisionToJednostkaOrganizacyjna(
			DSDivision dv) {
		JednostkaOrganizacyjna jo = JednostkaOrganizacyjnaDAO.getInstance()
				.getJednostkaOrganizacyjna();

		// Id
		jo.setId(dv.getId());
		// Guid
		jo.setGuid(dv.getGuid());
		// Nazwa
		jo.setNazwa(dv.getName());
		// Kod
		jo.setKod(dv.getCode());
		// ID Rodzica
		try {
			if (dv.getParent() != null) {
				jo.setRodzicId(dv.getParent().getId());
			}
		} catch (DivisionNotFoundException e) {
			jo.setRodzicId(null);
		} catch (ReferenceToHimselfException e) {
			jo.setRodzicId(null);
		} catch (EdmException e) {
			Log.debug(e);
			jo.setRodzicId(null);
		}

		// Aktywna/Ukryta
		jo.setAktywna(dv.isHidden());

		// ID z innego systemu
		jo.setIdn(dv.getExternalId());

		return jo;
	}

	public static Waluta MapCurrencyDBToWaluta(CurrencyDB c) {
		Waluta w = WalutaDAO.getInstance().getWaluta();

		// Aktywna
		w.isAktywna(c.getAvailable()!=null?MapperUtils.getBooleanFromInt(c.getAvailable()):false);
		// Id
		w.setId(c.getId());
		w.setCn(c.getCn());
		// Nazwa
		w.setNazwa(c.getTitle());
		return w;
	}

	public static TypPlatnosci MapPaymentTermsToTypPlatnosci(PaymentTermsDB pt) {
		TypPlatnosci tp = TypPlatnosciDAO.getInstance().getTypPlatnosci();

		// Aktywna
		tp.isAktywna(MapperUtils.getBooleanFromInt(pt.getAvailable()));
		// Id
		tp.setId(pt.getId());
		tp.setCn(pt.getCn());
		// Nazwa
		tp.setNazwa(pt.getTitle());
		//refValue
		tp.setObiektPowiazany(pt.getRefValue());
		return tp;
	}

	public static Kontrahent MapKontrahentDBToKontrahent(
			KontrahentDB kontrahentDB) {
		Kontrahent kontrahent = KontrahentDAO.getInstance().getKontrahent();

		kontrahent.setId((MapperUtils.getLongFromDouble(kontrahentDB.getId())));
		kontrahent.setNazwa(kontrahentDB.getNazwa());
		kontrahent.setKodpocztowy(kontrahentDB.getKodpocztowy());
		kontrahent.setMiejscowosc(kontrahentDB.getMiejscowosc());
		kontrahent.setNip(kontrahentDB.getNip());
		kontrahent.setNrDomu(kontrahentDB.getNrDomu());
		kontrahent.setMiejscowosc(kontrahentDB.getMiejscowosc());
		kontrahent.setNrMieszkania(kontrahentDB.getNrMieszkania());
		kontrahent.setUlica(kontrahentDB.getUlica());
		if(kontrahentDB.getNowy() == null)
			kontrahent.setNowy(false);
		kontrahent.setErpId(kontrahentDB.getErpId());
		kontrahent.setKraj(kontrahentDB.getKraj());
		kontrahent.setNrKonta(kontrahentDB.getNrKonta());
		return kontrahent;
	}

	public static MiejsceDostawy MapDeliveryLocationDBToMiejsceDostawy(
			DeliveryLocationDB dl) {
		MiejsceDostawy md = MiejsceDostawyDAO.getInstance().getMiejsceDostawy();

		// Aktywna
		if(dl.getAvailable()!=null)md.isAktywna(MapperUtils.getBooleanFromInt(dl.getAvailable()));
		md.setCn(dl.getCn());
		// Id
		md.setId(dl.getId());
		// Nazwa
		md.setNazwa(dl.getTitle());

		return md;
	}

	public static TypDokumentu MapPurchaseDocumentTypeDBToTypDokumentu(
			PurchaseDocumentTypeDB pd) {
		TypDokumentu td = TypDokumentuDAO.getInstance().getTypDokumentu();

		// Aktywna
		td.setAktywna(MapperUtils.getBooleanFromInt(pd.getAvailable()));
		td.setCn(pd.getCn());
		// Id
		td.setId(pd.getId());
		// Nazwa
		td.setNazwa(pd.getTitle());
		td.setCzywal(pd.getCzywal());
		td.setSymbol_waluty(pd.getSymbol_waluty());
		td.setTypdokzak_id(pd.getTypdokzak_id());
		td.setTypdokIdn(pd.getTypdokIdn());
		return td;
	}

	public static StawkaVat MapVatRateDBToStawkaVat(VatRateDB vr) {
		StawkaVat sv = StawkaVatDAO.getInstance().getStawkaVat();

		// Aktywna
		sv.setAktywna(MapperUtils.getBooleanFromInt(vr.getAvailable()));
		sv.setCn(vr.getCn());
		// Id
		sv.setId(vr.getId());
		// Nazwa
		sv.setNazwa(vr.getTitle());
		sv.setCzy_zwolniona(vr.getCzy_zwolniona());
		sv.setErp_id(vr.getErp_id());
		sv.setNie_podlega(vr.getNie_podlega());
		sv.setIdm(vr.getIdm());
		return sv;
	}

	public static PrzeznaczenieZakupu mapPrzeznaczenieZakupuDBToPrzeznaczenieZakupu(PrzeznaczenieZakupuDB przeznaczenieZakupuDB){
		PrzeznaczenieZakupu przeznaczenieZakupu = PrzeznaczenieZakupuDAO.getInstance().getPrzeznaczenieZakupu();

		przeznaczenieZakupu.setCn(przeznaczenieZakupuDB.getCn());
		przeznaczenieZakupu.setId(przeznaczenieZakupuDB.getId());
		przeznaczenieZakupu.setTitle(przeznaczenieZakupuDB.getTitle());

		return przeznaczenieZakupu;
	}

	public static Faktura MapFakturaDBToFaktura(FakturaDB fakturaDB,Long documentId){
		Faktura faktura = FakturaDAO.getInstance().getFaktura();

		faktura.setAdresat(fakturaDB.getAdresat());
		faktura.setCzyKorekta(fakturaDB.getCzyKorekta());
		faktura.setKorektaNumer(fakturaDB.getKorektaNumer());
		faktura.setDataWplywu(fakturaDB.getDataWplywu());
		faktura.setDataWystawienia(fakturaDB.getDataWystawienia());
		faktura.setDodatkoweInformacje(fakturaDB.getDodatkoweInformacje());

		faktura.setDostawca((fakturaDB.getDostawca()!=null)?MapKontrahentDBToKontrahent(fakturaDB.getDostawca()):null);

		//faktura.setDziennik(fakturaDB.getDziennik());
		faktura.setFakturaNaWydziale(fakturaDB.getFakturaNaWydziale());
		faktura.setGlownyKsiegowy(fakturaDB.getGlownyKsiegowy());
		faktura.setId(fakturaDB.getId());
		faktura.setKodKreskowy(fakturaDB.getKodKreskowy());
		faktura.setKurs(fakturaDB.getKurs());
		faktura.setKwotaBrutto(fakturaDB.getKwotaBrutto());
		faktura.setKwotaNetto(fakturaDB.getKwotaNetto());
		faktura.setKwotaPozostala(fakturaDB.getKwotaPozostala());
		faktura.setKwotaVat(fakturaDB.getKwotaVat());
		faktura.setKwotaWWalucie(fakturaDB.getKwotaWWalucie());
		faktura.setNazwaTowaruUslugi(fakturaDB.getNazwaTowaruUslugi());
		faktura.setNumerDokumentuKorygowanego(faktura
				.getNumerDokumentuKorygowanego());
		faktura.setNumerFaktury(fakturaDB.getNumerFaktury());
		faktura.setNumerPostepowaniaPrzetargowego(fakturaDB
				.getNumerPostepowaniaPrzetargowego());
		//	faktura.setNumerPrzesylkiRejestrowanej(fakturaDB
		//			.getNumerPrzesylkiRejestrowanej());
		faktura.setOpisMerytoryczny(fakturaDB.getOpisMerytoryczny());
		faktura.setOsobaUzupelniajacaOpisFaktury(fakturaDB
				.getOsobaUzupelniajacaOpisFaktury());
		faktura.setPelnomocnikDoSprawFinansowych(faktura
				.getPelnomocnikDoSprawFinansowych());
		faktura.setPotwierdzamOdbiorZgodnieZFaktura(fakturaDB
				.getPotwierdzamOdbiorZgodnieZFaktura());
		faktura.setPotwierdzamOdbiorZgodnieZUmowa(fakturaDB
				.getPotwierdzamOdbiorZgodnieZUmowa());
		faktura.setPracownikKsiegowosci(fakturaDB.getPracownikKsiegowosci());
		faktura.setPrzeznaczenieIUwagi(fakturaDB.getPrzeznaczenieIUwagi());
		faktura.setRodzajFaktury(fakturaDB.getRodzajFaktury());
		faktura.setSkierujDoUzupelnieniaOpisu(fakturaDB.getSkierujDoUzupelnieniaOpisu());

		faktura.setStanowiskoDostaw((fakturaDB.getStanowiskoDostaw()!=null)?MapDeliveryLocationDBToMiejsceDostawy(fakturaDB.getStanowiskoDostaw()):null);

		faktura.setStatus(fakturaDB.getStatus());
		faktura.setTerminPlatnosci(fakturaDB.getTerminPlatnosci());
		faktura.setTrybPostepowania(fakturaDB.getTrybPostepowania());

		//Mapowanie typu faktury
		TypDokumentu typFaktury = null;

		if(fakturaDB.getTypFakturyPln()!=null)
			typFaktury = MapPurchaseDocumentTypeDBToTypDokumentu(fakturaDB
					.getTypFakturyPln());
		else if(fakturaDB.getTypFakturyWal()!=null)
			typFaktury = MapPurchaseDocumentTypeDBToTypDokumentu(fakturaDB
					.getTypFakturyWal());

		faktura.setTypFaktury(typFaktury);
		////

		faktura.setTypPlatnosci((fakturaDB.getTypPlatnosci()!=null)?MapPaymentTermsToTypPlatnosci(fakturaDB.getTypPlatnosci()):null);

		faktura.setWaluta((fakturaDB.getWaluta()!=null)?MapCurrencyDBToWaluta(fakturaDB.getWaluta()):null);

		faktura.setWydatekJestStrukturalny(fakturaDB
				.getWydatekJestStrukturalny());
		List<WydatekStrukturalny> wydatki = new ArrayList<WydatekStrukturalny>();
		for(WydatekStrukturalnyDB WydatekStrukturalnyDB: fakturaDB.getWydatekStrukturalnyPoz()){
			wydatki.add(MapFakturaWydatekDBToFakturaWydatek(WydatekStrukturalnyDB,documentId));
		}
		faktura.setWydatkiStrukturalne(wydatki);
		faktura.setWynikaZPostepowaniaPrzetargowego(fakturaDB.getWynikaZPostepowaniaPrzetargowego());

		faktura.setWystawcaFaktury((fakturaDB.getWystawcaFaktury()!=null)?MapKontrahentDBToKontrahent(fakturaDB.getWystawcaFaktury()):null);

		faktura.setZalacznik(fakturaDB.getZalacznik());
		//	faktura.setZamowienieNaMagazyn(fakturaDB.getZamowienieNaMagazyn());

		List<PozycjaFaktury> pozycjeFaktury = new ArrayList<PozycjaFaktury>();
		for(FakturaPozycjaDB fakturaPozycjaDB: fakturaDB.getPozycjeFaktury()){
			pozycjeFaktury.add(MapFakturaPozycjaDBToFakturaPozycja(fakturaPozycjaDB,documentId));
		}
		faktura.setPozycjeFaktury(pozycjeFaktury);
//		faktura.setNumerKontaBankowego(fakturaDB.getNumerKontaBankowego());
		faktura.setSposobWyplaty(fakturaDB.getSposobWyplaty());
		faktura.setCzySzablon(fakturaDB.getCzySzablonRozliczenFaktury());
		if(fakturaDB.getCzySzablonRozliczenFaktury()!=null&&fakturaDB.getCzySzablonRozliczenFaktury()){
			try {
				faktura.setSzablon(MapSzablonyRozliczenFakturDBDBToSzablonyRozliczenFaktur(SzablonyRozliczenFakturDBDAO.findByErpId(fakturaDB.getSzablonErpId()).get(0)));
			} catch (EdmException e) {
				log.debug("",e);
				e.printStackTrace();
			}
		}
		faktura.setCzyZZamowienia(fakturaDB.getCzyZZamowienia());
		if(fakturaDB.getZamowienie()!=null){
				faktura.setZamowienie(MapNumeryZamowienDBToNumeryZamowien(fakturaDB.getZamowienie()));
		}
		faktura.setCzyZWniosku(fakturaDB.getCzZWniosku());
		
		faktura.setCzyRozliczenieMediow(fakturaDB.getCzyRozliczenieMediow());
		faktura.setWygenerowanoMetryczke(fakturaDB.getWygenerowanoMetryczke());
		
		return faktura;
	}

	public static OPK MapOpkDBToOPK(OpkDB opkDB){
		OPK opk = OpkDAO.getInstance().getOpk();

		opk.setId(opkDB.getId());
		opk.setDs_division_id(opkDB.getDs_division_id());
		opk.setErp_id(opkDB.getErp_id());
		opk.setIsCost(opkDB.getIsCost());
		opk.setIsDepartment(opkDB.getIsDepartment());
		opk.setOpk(opkDB.getOpk());

		return opk;
	}

	public static PozycjaFaktury MapFakturaPozycjaDBToFakturaPozycja(FakturaPozycjaDB fakturaPozycjaDB,Long documentId) {
		PozycjaFaktury fakturaPozycja = PozycjaFakturyDAO.getInstance().getFakturaPozycja();

		fakturaPozycja.setIdPozycji(fakturaPozycjaDB.getId());
		fakturaPozycja.setKwotaBrutto(fakturaPozycjaDB.getKwotaBrutto());
		fakturaPozycja.setKwotaNetto(fakturaPozycjaDB.getKwotaNetto());
		fakturaPozycja.setKwotaVat(fakturaPozycjaDB.getKwotaVat());
		fakturaPozycja.setOpk((fakturaPozycjaDB.getOpk()!=null)?MapOpkDBToOPK(fakturaPozycjaDB.getOpk()):null);
		fakturaPozycja.setBudzetPole((fakturaPozycjaDB.getPozycjaBudzetu()!=null)?MapBudzetPozycjaDBToBudzetPozycja(fakturaPozycjaDB.getPozycjaBudzetu(), documentId):null);
		fakturaPozycja.setStawkaVat((fakturaPozycjaDB.getStawkaVat()!=null)?MapVatRateDBToStawkaVat(fakturaPozycjaDB.getStawkaVat()):null);
		fakturaPozycja.setProduktFaktury((fakturaPozycjaDB.getProduktyFaktury()!=null)?MapCostInvoiceProductDBToProduktyFaktury(fakturaPozycjaDB.getProduktyFaktury()):null);
		fakturaPozycja.setPrzeznaczenieZakupu((fakturaPozycjaDB.getPrzeznaczenieZakupu()!=null)?mapPrzeznaczenieZakupuDBToPrzeznaczenieZakupu(fakturaPozycjaDB.getPrzeznaczenieZakupu()):null);
		fakturaPozycja.setProjekt((fakturaPozycjaDB.getFunduszProjektowy()!=null)?MapProjectDBToProjekt(fakturaPozycjaDB.getFunduszProjektowy()):null);
		fakturaPozycja.setZlecenie((fakturaPozycjaDB.getZlecenie()!=null)?MapProductionOrderDBToZleceniaProdukcyjne(fakturaPozycjaDB.getZlecenie()):null);
		//zrodla 0 to pozycja niedostepna dla danego funduszu
		fakturaPozycja.setZrodlo((fakturaPozycjaDB.getZrodloFinansowania()!=null&&fakturaPozycjaDB.getZrodloFinansowania().getZrodlo_id()!=0)?MapSourcesOfProjectFundingDBToZrodloFinansowaniaProjektow(fakturaPozycjaDB.getZrodloFinansowania()):null);
		fakturaPozycja.setWniosekORezerwacje(fakturaPozycjaDB.getWniosekORezerwacje()!=null?mapWniosekORezerwacjeDBToWniosekORezerwacje(fakturaPozycjaDB.getWniosekORezerwacje()):null);
		fakturaPozycja.setPozycjaZamowieniaId(fakturaPozycjaDB.getPozycjaZamowieniaId()!=null?fakturaPozycjaDB.getPozycjaZamowieniaId():null);
	//	fakturaPozycja.setProjektZawOZrodlo(fakturaPozycjaDB.getPozycjaProjDdoZrod()!=null?MapProjectDBToProjekt(fakturaPozycjaDB.getPozycjaProjDdoZrod()):null);
	//	fakturaPozycja.setZrodloZawezajaceProj(fakturaPozycjaDB.getZrodloZawProj()!=null?MapZrodloDBToZrodlo(fakturaPozycjaDB.getZrodloZawProj()):null);

		
		return fakturaPozycja;
	}
	public static WydatekStrukturalny MapFakturaWydatekDBToFakturaWydatek(WydatekStrukturalnyDB wydDB,Long documentId) {
		WydatekStrukturalny ws=WydatekStrukturalnyDAO.getInstance().getWydatekStrukturalny();

		ws.setId(wydDB.getId());
		ws.setKwotaBrutto(wydDB.getKwotaBrutto());
		ws.setWydatekStrukturalny(wydDB.getWydatekStrukt());
		ws.setKod(wydDB.getKod());
		ws.setObszar(wydDB.getObszar());
		
		return ws;
	}
	public static BudzetPozycja MapBudzetPozycjaDBToBudzetPozycja(Long budzetId,Long documentId)  {
		BudzetPozycja budzetPozycja = BudzetPozycjaDAO.getInstance().getBudzetPozycja();
		BudzetPozycjaDB budzetPozycjaDB = null;
		try{
			DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(InvoiceGeneralConstans.WIDOK_MPK_BUDZET);
			EnumItem ei=d.getEnumItem(Integer.parseInt(budzetId.toString()));

			if(ei!=null&&ei.getCn()!=null&&!ei.getCn().equals("")){
				budzetPozycjaDB=BudzetPozycjaDB.find(Long.parseLong(ei.getId().toString()), documentId);
			}


			if(budzetPozycjaDB!=null){
				List<BudzetKOKomorekDB> budzetyKom=BudzetKOKomorkaDBDAO.findByErpId(budzetPozycjaDB.getBudzetId());
				if(budzetyKom.size()>0){
					budzetPozycja.setBudzet(MapBudzetKOKomorekDBToBudzetKOKomorek(budzetyKom.get(budzetyKom.size()-1)));
				}

				budzetPozycja.setKwotaNetto(budzetPozycjaDB.getKwotaNetto());
				budzetPozycja.setKwotaVat(budzetPozycjaDB.getKwotaVat());
				budzetPozycja.setOkresBudzetowy(budzetPozycjaDB.getOkresBudzetowy());	

				List<BudzetPozycjaSimpleDB> budzetyPoz=BudzetPozycjaSimpleDBDAO.findByErpId(budzetPozycjaDB.getPozycjaId());
				if(budzetyPoz.size()>0){
					budzetPozycja.setPozycjaZBudzetu(MapBudzetPozycjaSimpleDBToBudzetPozycjaSimple(budzetyPoz.get(budzetyPoz.size()-1)));
				}

				budzetPozycja.setAkceptacja(budzetPozycjaDB.getAkceptacja());
				//budzetPozycja.setProjekt(MapProjectDBToProjekt(budzetPozycjaDB.getProjekt()))
				budzetPozycja.setBudzetProjektu(budzetPozycjaDB.getBudzetProjektuId());
				budzetPozycja.setEtap(budzetPozycjaDB.getEtapId());
				budzetPozycja.setZasob(budzetPozycjaDB.getZasobId());
			}
		}
		catch(EdmException e){
			log.debug("",e);
			e.printStackTrace();
		}
		return budzetPozycja;
	}
	/**
	 * Metoda mapuje obiekt BudgetView na Budzet
	 * @param budgetView
	 * @return
	 */
	public static Budzet MapBudgetViewDBToBudzet(BudgetViewDB budgetView) {
		Budzet budzet = BudzetyDAO.getInstance().getBudzet();
		if(budgetView!=null){
			budzet.setAvailable(budgetView.isAvailable());
			budzet.setBdBudzetId(budgetView.getBdBudzetId());
			budzet.setBdBudzetIdm(budgetView.getBdBudzetIdm());
			budzet.setBdBudzetRodzajId(budgetView.getBdBudzetRodzajId());
			budzet.setBdBudzetRodzajZadaniaId(budgetView.getBdBudzetRodzajZadaniaId());
			budzet.setBdBudzetRodzajZrodlaId(budgetView.getBdBudzetRodzajZrodlaId());
			budzet.setBdGrupaRodzajId(budgetView.getBdGrupaRodzajId());
			budzet.setBdRodzaj(budgetView.getBdRodzaj());
			budzet.setBdStrBudzetIdn(budgetView.getBdStrBudzetIdn());
			budzet.setBdStrBudzetIds(budgetView.getBdStrBudzetIds());
			budzet.setBdSzablonPozId(budgetView.getBdSzablonPozId());
			budzet.setBdZadanieId(budgetView.getBdZadanieId());
			budzet.setBdZadanieIdn(budgetView.getBdZadanieIdn());
			budzet.setBudWOkrNazwa(budgetView.getBudWOkrNazwa());
			budzet.setBudzetId(budgetView.getBudzetId());
			budzet.setBudzetIdm(budgetView.getBudzetIdm());
			budzet.setBudzetIds(budgetView.getBudzetIds());
			budzet.setBudzetNazwa(budgetView.getBudzetNazwa());
			budzet.setCzyAktywna(budgetView.getCzyAktywna());
			budzet.setDokBdStrBudzetIdn(budgetView.getDokBdStrBudzetIdn());
			budzet.setDokKomNazwa(budgetView.getDokKomNazwa());
			budzet.setDokPozLimit(budgetView.getDokPozLimit());
			budzet.setDokPozNazwa(budgetView.getDokPozNazwa());
			budzet.setDokPozNrpoz(budgetView.getDokPozNrpoz());
			budzet.setDokPozPKoszt(budgetView.getDokPozPKoszt());
			budzet.setDokPozRezKoszt(budgetView.getDokPozRezKoszt());
			budzet.setDokPozRKoszt(budgetView.getDokPozPKoszt());
			budzet.setId(budgetView.getId());
			budzet.setOkrrozlId(budgetView.getOkrrozlId());
			budzet.setPozycjaPodrzednaId(budgetView.getPozycjaPodrzednaId());
			budzet.setStrBudNazwa(budgetView.getStrBudNazwa());
			budzet.setWspolczynnikPozycji(budgetView.getWspolczynnikPozycji());
			budzet.setWytworId(budgetView.getWytworId());
			budzet.setZadanieNazwa(budgetView.getZadanieNazwa());
			budzet.setZadaniePKoszt(budgetView.getZadaniePKoszt());
			budzet.setZadanieRezKoszt(budgetView.getZadanieRezKoszt());
			budzet.setZadanieRKoszt(budgetView.getZadanieRKoszt());
			budzet.setZrodloId(budgetView.getZrodloId());
			budzet.setZrodloIdn(budgetView.getZrodloIdn());
			budzet.setZrodloNazwa(budgetView.getZrodloNazwa());
			budzet.setZrodloPKoszt(budgetView.getZrodloPKoszt());
			budzet.setZrodloRezKoszt(budgetView.getZrodloRezKoszt());
			budzet.setZrodloRKoszt(budgetView.getZrodloRKoszt());
		}
		return budzet;
	}

	public static RealizacjaBudzetu MapRealizacjaBudzetuDBToRealizacjaBudzetu(
			RealizacjaBudzetuDB realizacjaBudzetuDB) {
		RealizacjaBudzetu realizacjaBudzetu = RealizacjaBudzetuDAO
				.getInstance().getRealizacjaBudzetu();
		if(realizacjaBudzetuDB!=null){
			realizacjaBudzetu.setId(realizacjaBudzetuDB.getId());
			realizacjaBudzetu
			.setNazwaBudzetu(realizacjaBudzetuDB.getNazwaBudzetu());
			realizacjaBudzetu
			.setNazwaPozycji(realizacjaBudzetuDB.getNazwaPozycji());
			realizacjaBudzetu.setPlanPozycji(realizacjaBudzetuDB.getPlanPozycji());
			realizacjaBudzetu.setPozostalo(realizacjaBudzetuDB.getPozostalo());
			realizacjaBudzetu.setWykonaniePozycji(realizacjaBudzetuDB
					.getWykonaniePozycji());
		}
		return realizacjaBudzetu;
	}

	public static ProduktyFaktury MapCostInvoiceProductDBToProduktyFaktury(
			CostInvoiceProductDB cost) {
		ProduktyFaktury pf = ProduktyFakturyDAO.getInstance()
				.getProduktyFaktury();
		if(cost!=null){
			pf.setAktywna(MapperUtils.getBooleanFromInt(cost.getAvailable()));
			pf.setCn(cost.getCn());
			pf.setId(cost.getId());
			pf.setNazwa(cost.getTitle());
			pf.setKlasyWytwIdn(cost.getKlasyWytwIdn());
			pf.setJmIdn(cost.getJmIdn());
			pf.setRodzTowaru(cost.getRodzTowaru());
			pf.setVatStawIds(cost.getVatStawIds());
			//erp_id
			pf.setWytworId(cost.getWytworId());
		}
		return pf;
	}

	public static JednostkaMiary MapUnitOfMeasureDBToJednostkaMiary(
			UnitOfMeasureDB uom) {
		JednostkaMiary jm = JednostkaMiaryDAO.getInstance().getJednostkaMiary();
		if(uom!=null){
			jm.setAktywna(MapperUtils.getBooleanFromInt(uom.getAvailable()));
			jm.setCn(uom.getCn());
			jm.setId(uom.getId());
			jm.setErpId(uom.getErpId());
			jm.setJmtyp_idn(uom.getJmtyp_idn());
			jm.setPrecyzjajm(uom.getPrecyzjajm());
			jm.setTitle(uom.getTitle());
		}
		return jm;
	}

	/**
	 * Metoda mapuje obiekt DSUser [DB] na Pracownik [CAN]
	 * 
	 * @param dsuser
	 * @return
	 */
	public static Pracownik MapDsuserToPracownik(DSUser dsuser) {
		if (dsuser==null)
		{
			return null;
		}
		try {
			Pracownik pracownik = new Pracownik();
			// Imi�
			if (dsuser.getFirstname()!=null)
			{
				pracownik.setImie(dsuser.getFirstname());
			}
			
			// Email
			if (dsuser.getEmail()!=null)
			{
				pracownik.setAdresEmail(dsuser.getEmail());
			}
			
			// Nazwisko
			if (dsuser.getLastname()!=null)
			{
				pracownik.setNazwisko(dsuser.getLastname());
			}
			
			// Pesel
			if (dsuser.getPesel()!=null)
			{
				pracownik.setPesel(dsuser.getPesel());
			}
			
			// Login
			pracownik.setLogin(dsuser.getName());
			// Prefix domeny
			if (dsuser.getDomainPrefix()!=null)
			{
				pracownik.setPrefixDomeny(dsuser.getDomainPrefix());
			}
			// Czy logowanie domenowe
			pracownik.setLogowanieDomenowe(dsuser.isAdUser());
			
			// Jednostki organizacyjne - wiele
			List<DSDivision> listaDzialow;
			try {
				listaDzialow = dsuser.getDivisionsAsList();

				List<JednostkaOrganizacyjna> listaJednostekOrganizacyjnych = new ArrayList<JednostkaOrganizacyjna>();

				for (DSDivision dsdivision : listaDzialow) {
					JednostkaOrganizacyjna jednostkaOrganizacyjna = HibernateToCanonicalMapper
							.MapDSDivisionToJednostkaOrganizacyjna(dsdivision);

					listaJednostekOrganizacyjnych.add(jednostkaOrganizacyjna);
				}

				pracownik
				.setJednostkiOrganizacyjne(listaJednostekOrganizacyjnych);
			} catch (EdmException e) {
				Log.debug(e);
			}
			
			// Id
			if (dsuser.getId()!=null)
			{
				pracownik.setId(dsuser.getId());
			}
			
			// Identyfikator mapuj�cy
			boolean mapuj = false;
			if (Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_MAPUJ") != null) {
				mapuj = new Boolean(
						Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_MAPUJ"));
			}

			if (mapuj) {
				// W pliku adds.properties ustawiamy jedno z dostepnych p�l,
				// kt�re
				// b�dzie okre�la� wsp�lny identyfikator
				// dla mapowanych modeli dla obiektu Pracownik
				// IMPORT_PRACOWNIKOW_MAPUJ = true - w og�le u�ywaj mapowania
				// IMPORT_PRACOWNIKOW_POLE_MAPUJACE_NAZWA = PESEL/GUID kt�rym
				// polem
				// mapujemy

				if (Docusafe
						.getAdditionProperty("IMPORT_PRACOWNIKOW_POLE_MAPUJACE_NAZWA") == null) {
					throw new EdmException(
							"Brak ustawionego adds'a IMPORT_PRACOWNIKOW_POLE_MAPUJACE_NAZWA");
				}

				String nazwaPolaMapujacego = Docusafe
						.getAdditionProperty("IMPORT_PRACOWNIKOW_POLE_MAPUJACE_NAZWA");

				if (nazwaPolaMapujacego.equals("PESEL")) {
					pracownik.setOperatorDoPorownywania(dsuser.getPesel());
				} else 
					if (nazwaPolaMapujacego.equals("PRACOWNIK_NR_EWID"))					
				{
					pracownik.setOperatorDoPorownywania(dsuser.getPracow_nrewid_erp());
				}
			}

			return pracownik;
		} catch (Exception exc) {
			Log.debug(exc);
		}
		return null;
	}


	/**
	 * Metoda mapuje obiekt FinancialTaskDB na ZadaniaFinansowe
	 * @param ft
	 * @return
	 */
	public static ZadaniaFinansowe MapFinancialTaskDBToZadaniaFinansowe(
			FinancialTaskDB ft) {
		ZadaniaFinansowe zf = ZadaniaFinansoweDAO.getInstance().getZadaniaFinansowe();

		zf.setAvailable(ft.getAvailable());
		zf.setCentrum(ft.getCentrum());
		zf.setCn(ft.getCn());
		zf.setErpId(ft.getErpId());
		zf.setId(ft.getId());
		zf.setIdentyfikator_num(ft.getIdentyfikator_num());
		zf.setRefValue(ft.getRefValue());
		zf.setTitle(ft.getTitle());
		return zf;
	}

	/**
	 * Metoda mapuje obiekt ProjectDB na Projekt
	 * @param p
	 * @return
	 */
	public static Projekt MapProjectDBToProjekt(
			ProjectDB p) {
		Projekt projekt = ProjektDAO.getInstance().getProjekt();

		projekt.setAvailable(p.getAvailable());
		projekt.setCentrum(p.getCentrum());
		projekt.setCn(p.getCn());
		projekt.setErpId(p.getErpId());
		projekt.setId(p.getId());
		projekt.setIdentyfikator_num(p.getIdentyfikator_num());
		projekt.setP_datako(p.getP_datako());
		projekt.setP_datapo(p.getP_datapo());
		projekt.setTitle(p.getTitle());
		projekt.setTyp_kontrakt_id(p.getTyp_kontrakt_id());
		projekt.setTyp_kontrakt_idn(p.getTyp_kontrakt_idn());
		return projekt;
	}

	/**
	 * Metoda mapuje obiekt ProjektBudzetEtapDB na ProjektBudzet
	 * @param pb
	 * @return
	 */
	public static ProjektBudzetEtap MapProjectBudgetItemDBToProjektBudzet(
			ProjektBudzetEtapDB pb) {
		ProjektBudzetEtap projektBudzet = ProjektBudzetEtapDAO.getInstance().getProjektBudzet();

		projektBudzet.setId(pb.getId());
		projektBudzet.setErpId(pb.getErpId());
		projektBudzet.setNazwa(pb.getNazwa());
		projektBudzet.setNrpoz(pb.getNrpoz());
		projektBudzet.setP_datako(pb.getP_datako());
		projektBudzet.setP_datapo(pb.getP_datapo());
		projektBudzet.setParent(pb.getParent());
		projektBudzet.setParent_id(pb.getParent_id());
		projektBudzet.setParent_idm(pb.getParent_idm());
		projektBudzet.setAvailable(pb.getAvailable());
		
		return projektBudzet;
	}

	/**
	 * Metoda mapuje obiekt ProjektBudzetDB na ProjektBudzet
	 * @param pb
	 * @return
	 */
	public static ProjektBudzet MapProjectBugdetDBToProjektBudzet(
			ProjektBudzetDB pb) {
		ProjektBudzet projektBudzet = ProjektBudzetDAO.getInstance().getProjektBudzet();
		
		projektBudzet.setId(pb.getId());
		projektBudzet.setErpId(pb.getErpId());
		projektBudzet.setIdm(pb.getIdm());
		projektBudzet.setNazwa(pb.getNazwa());
		projektBudzet.setP_datako(pb.getP_datako());
		projektBudzet.setP_datapo(pb.getP_datapo());
		projektBudzet.setParent(pb.getParent());
		projektBudzet.setParent_id(pb.getParent_id());
		projektBudzet.setParent_idm(pb.getParent_idm());
		projektBudzet.setStan(pb.getStan());
		projektBudzet.setStatus(pb.getStatus());
		projektBudzet.setAvailable(pb.getAvailable());
		
		return projektBudzet;
	}

	/**
	 * Metoda mapuje obiekt ProjectBudgetItemResourceDB na ProjektBudzetZasob
	 * @param pb
	 * @return
	 */
	public static ProjektBudzetZasob MapProjectBudgetItemResourceDBToProjektBudzetZasob(
			ProjektBudzetZasobDB pb) {
		ProjektBudzetZasob projektBudzetZasob = ProjektBudzetZasobDAO.getInstance().getProjektBudzetZasob();

		projektBudzetZasob.setId(pb.getId());
		projektBudzetZasob.setCzy_glowny(pb.getCzy_glowny());
		projektBudzetZasob.setErpId(pb.getErpId());
		projektBudzetZasob.setNazwa(pb.getNazwa());
		projektBudzetZasob.setNrpoz(pb.getNrpoz());
		projektBudzetZasob.setP_cena(pb.getP_cena());
		projektBudzetZasob.setP_ilosc(pb.getP_ilosc());
		projektBudzetZasob.setP_wartosc(pb.getP_wartosc());
		projektBudzetZasob.setParent(pb.getParent());
		projektBudzetZasob.setParent_id(pb.getParent_id());
		projektBudzetZasob.setR_ilosc(pb.getR_ilosc());
		projektBudzetZasob.setR_wartosc(pb.getR_wartosc());
		projektBudzetZasob.setRodzaj_zasobu(pb.getRodzaj_zasobu());
		projektBudzetZasob.setZasob_glowny_id(pb.getZasob_glowny_id());
		projektBudzetZasob.setAvailable(pb.getAvailable());
		
		return projektBudzetZasob;
	}

	/**
	 * Metoda mapuje obiekt SourcesOfProjectFunding na ZrodloFinansowaniaProjektow
	 * @param sop
	 * @return
	 */
	public static ZrodloFinansowaniaProjektow MapSourcesOfProjectFundingDBToZrodloFinansowaniaProjektow(
			SourcesOfProjectFundingDB sop) {
		ZrodloFinansowaniaProjektow zrodloFinansowaniaProjektow = ZrodloFinansowaniaProjektowDAO.getInstance().getZrodloFinansowaniaProjektow();

		zrodloFinansowaniaProjektow.setAvailable(sop.getAvailable());
		zrodloFinansowaniaProjektow.setErpId(sop.getErpId());
		zrodloFinansowaniaProjektow.setId(sop.getId());
		zrodloFinansowaniaProjektow.setKontrakt_id(sop.getKontrakt_id());
		zrodloFinansowaniaProjektow.setKontrakt_idm(sop.getKontrakt_idm());
		zrodloFinansowaniaProjektow.setRodzaj(sop.getRodzaj());
		zrodloFinansowaniaProjektow.setWsp_proc(sop.getWsp_proc());
		zrodloFinansowaniaProjektow.setZrodlo_id(sop.getZrodlo_id());
		zrodloFinansowaniaProjektow.setZrodlo_identyfikator_num(sop.getZrodlo_identyfikator_num());
		zrodloFinansowaniaProjektow.setZrodlo_idn(sop.getZrodlo_idn());
		zrodloFinansowaniaProjektow.setZrodlo_nazwa(sop.getZrodlo_nazwa());
		return zrodloFinansowaniaProjektow;
	}

	/**
	 * Metoda mapuje obiekt ProductionOrderDB na ZleceniaProdukcyjne
	 * @param po
	 * @return
	 */
	public static ZleceniaProdukcyjne MapProductionOrderDBToZleceniaProdukcyjne(
			ProductionOrderDB po) {
		ZleceniaProdukcyjne zleceniaProdukcyjne = ZleceniaProdukcyjneDAO.getInstance().getZleceniaProdukcyjne();

		zleceniaProdukcyjne.setAvailable(po.getAvailable());
		zleceniaProdukcyjne.setErpId(po.getErpId());
		zleceniaProdukcyjne.setId(po.getId());
		zleceniaProdukcyjne.setCn(po.getCn());
		zleceniaProdukcyjne.setTitle(po.getTitle());
		return zleceniaProdukcyjne;
	}
	
	/**
	 * Metoda mapuje obiekt KontrahentKontoDB na KontrahentKonto
	 * @param kk
	 * @return
	 */
	public static KontrahentKonto MapKontrahentKontoDBToKontrahentKonto(KontrahentKontoDB kk) {
		KontrahentKonto kontrahentKonto = KontrahentKontoDAO.getInstance().getKontahentKonto();
		
		kontrahentKonto.setBank_id(kk.getBank_id());
		kontrahentKonto.setBank_idn(kk.getBank_idn());
		kontrahentKonto.setBank_nazwa(kk.getBank_nazwa());
		kontrahentKonto.setCzyIban(kk.getCzyIban());
		kontrahentKonto.setDostawca_id(kk.getDostawca_id());
		kontrahentKonto.setDostawca_idn(kk.getDostawca_idn());
		kontrahentKonto.setErpId(kk.getErpId());
		kontrahentKonto.setId(kk.getId());
		kontrahentKonto.setIdm(kk.getIdm());
		kontrahentKonto.setKolejnosc_uzycia(kk.getKolejnosc_uzycia());
		kontrahentKonto.setNazwa(kk.getNazwa());
		kontrahentKonto.setNumer_konta(kk.getNumer_konta());
		kontrahentKonto.setSymbol_waluty(kk.getSymbol_waluty());
		kontrahentKonto.setTypKurs_id(kk.getTypKurs_id());
		kontrahentKonto.setTypKurs_idn(kk.getTypKurs_idn());
		kontrahentKonto.setWaluta_id(kk.getWaluta_id());
		kontrahentKonto.setZrodKursWal_id(kk.getZrodKursWal_id());
		kontrahentKonto.setZrodKursWal_idn(kk.getZrodKursWal_idn());
		kontrahentKonto.setZrodKursWal_nazwa(kk.getZrodKursWal_nazwa());
		
		return kontrahentKonto;
	}
	
	/**
	 * Metoda mapuje obiekt TypyWnioskowORezerwacjeDB na TypyWnioskowORezerwacje
	 * @param typDB
	 */
	public static TypyWnioskowORezerwacje MapTypyWnioskowORezerwacjeDBToTypyWnioskowORezerwacje(TypyWnioskowORezerwacjeDB typDB) {
		TypyWnioskowORezerwacje typyWnioskowORezerwacje = TypyWnioskowORezerwacjeDAO.getInstance().getTypyWnioskowORezerwacje();
		
		typyWnioskowORezerwacje.setCzy_aktywny(typDB.getCzy_aktywny());
		typyWnioskowORezerwacje.setCzy_kontrola_ceny(typDB.getCzy_kontrola_ceny());
		typyWnioskowORezerwacje.setCzy_kontrola_ilosci(typDB.getCzy_kontrola_ilosci());
		typyWnioskowORezerwacje.setCzy_kontrola_wartosci(typDB.getCzy_kontrola_wartosci());
		typyWnioskowORezerwacje.setErpId(typDB.getErpId());
		typyWnioskowORezerwacje.setCzy_obsluga_pzp(typDB.getCzy_obsluga_pzp());
		typyWnioskowORezerwacje.setId(typDB.getId());
		typyWnioskowORezerwacje.setIdm(typDB.getIdm());
		typyWnioskowORezerwacje.setNazwa(typDB.getNazwa());
		
		return typyWnioskowORezerwacje;
	}
	
	/**
	 * Metoda mapuje obiekt OkresBudzetowyDB na OkresBudzetowy
	 * @param budDB
	 * @return
	 */
	public static OkresBudzetowy mapOkresBudzetowyDBToOkresBudzetowy(OkresBudzetowyDB budDB) {
		OkresBudzetowy bud = OkresBudzetowyDAO.getInstance().getOkresBudzetowy();
		
		bud.setCzy_archiwalny(budDB.getCzy_archiwalny());
		bud.setData_obow_do(budDB.getData_obow_do());
		bud.setData_obow_od(budDB.getData_obow_od());
		bud.setErpId(budDB.getErpId());
		bud.setId(budDB.getId());
		bud.setIdm(budDB.getIdm());
		bud.setNazwa(budDB.getNazwa());
		bud.setWersja(budDB.getWersja());
		bud.setAvailable(budDB.getAvailable());
		
		return bud;
	}
	
	/**
	 * Metoda mapuje obiekt PlanZakupowoDB na PlanZakupow
	 * @param pl
	 * @return
	 */
	public static PlanZakupow MapPlanZakupowoDBToPlanZakupow(PlanZakupowDB pl) {
		PlanZakupow pz = PlanZakupowDAO.getInstance().getPlanZakupow();
		
		pz.setId(pl.getId());
		pz.setOkres_id(pl.getOkres_id());
		pz.setOkres_idm(pl.getOkres_idm());
		pz.setBudzet_id(pl.getBudzet_id());
		pz.setBudzet_idn(pl.getBudzet_idn());
		pz.setCzyAgregat(pl.getCzyAgregat());
		pz.setErpId(pl.getErpId());
		pz.setIdm(pl.getIdm());
		pz.setNazwa(pl.getNazwa());
		pz.setAvailable(pl.getAvailable());
		
		return pz;
	}
	
	/**
	 * Metoda mapuje obiekt RodzajePlanowZakupuDB na RodzajePlanowZakupu
	 * @param rpz
	 * @return
	 */
	public static RodzajePlanowZakupu MapRodzajePlanowZakupuDBToRodzajePlanowZakupu(RodzajePlanowZakupuDB rpz) {
		RodzajePlanowZakupu rp = RodzajePlanowZakupuDAO.getInstance().getRodzajePlanowZakupu();
		
		rp.setId(rpz.getId());
		rp.setCena(rpz.getCena());
		rp.setCzy_aktywna(rpz.getCzy_aktywna());
		rp.setErpId(rpz.getErpId());
		rp.setKomorkaRealizujaca(rpz.getKomorkaRealizujaca());
		rp.setNazwa(rpz.getNazwa());
		rp.setNrpoz(rpz.getNrpoz());
		rp.setOpis(rpz.getOpis());
		rp.setP_ilosc(rpz.getP_ilosc());
		rp.setP_koszt(rpz.getP_koszt());
		rp.setParent(rpz.getParent());
		rp.setParentId(rpz.getParentId());
		rp.setParentIdm(rpz.getParentIdm());
		rp.setRez_ilosc(rpz.getRez_ilosc());
		rp.setRez_koszt(rpz.getRez_koszt());
		rp.setR_ilosc(rpz.getR_ilosc());
		rp.setR_koszt(rpz.getR_koszt());
		rp.setWytwor_id(rpz.getWytwor_id());
		rp.setWytwor_idm(rpz.getWytwor_idm());
		
		return rp;
	}

	public static BudzetKOKomorek MapBudzetKOKomorekDBToBudzetKOKomorek(
			BudzetKOKomorekDB budzetKoDB) {
		BudzetKOKomorek bud = BudzetKOKomorekDAO.getInstance().getBudzetKOKomorek();

		bud.setId(budzetKoDB.getId());
		bud.setIdm(budzetKoDB.getIdm());
		bud.setNazwa(budzetKoDB.getNazwa());
		bud.setBudzetId(budzetKoDB.getBudzet_id());
		bud.setBudzetIdn(budzetKoDB.getBudzet_idn());
		bud.setOkresIdm(budzetKoDB.getOkresIdm());
		bud.setOkresId(budzetKoDB.getOkresId());
		bud.setErpId(budzetKoDB.getErpId());
		bud.setCzyAgregat(budzetKoDB.getCzyAgregat());
		bud.setAvailable(budzetKoDB.getAvailable());
		
		return bud;

	}
	
	/**
	 * Metoda mapuje obiekt RoleWProjektachDB na RoleWProjektach
	 * @param r
	 * @return
	 */
	public static RoleWProjekcie MapRoleWProjekcieDBToRoleWProjekcie(RoleWProjekcieDB r){
		RoleWProjekcie rol = RoleWProjekcieDAO.getInstance().getRoleWProjekcie();
		
		rol.setBp_rola_id(r.getBp_rola_id());
		rol.setBp_rola_idn(r.getBp_rola_idn());
		rol.setBp_rola_kontrakt_id(r.getBp_rola_kontrakt_id());
		rol.setCzy_blokada_prac(r.getCzy_blokada_prac());
		rol.setCzy_blokada_roli(r.getCzy_blokada_roli());
		rol.setId(r.getId());
		rol.setKontrakt_id(r.getKontrakt_id());
		rol.setKontrakt_idm(r.getKontrakt_idm());
		rol.setOsobaGuid(r.getOsobaGuid());
		rol.setOsobaId(r.getOsobaId());
		rol.setPracownik_id(r.getPracownik_id());
		rol.setPracownik_idn(r.getPracownik_idn());
		rol.setPracownik_nrewid(r.getPracownik_nrewid());
		rol.setAvailable(r.getAvailable());
		
		return rol;
	}
	
	/**
	 * Metoda mapuje obiekt RoleWBudzecieDB na RoleWBudzecie
	 * @param r
	 * @return
	 */
	public static RoleWBudzecie MapRoleWBudzecieDBToRoleWBudzecie(RoleWBudzecieDB r){
		RoleWBudzecie rola = RoleWBudzecieDAO.getInstance().getRoleWBudzecie();
		
		rola.setBd_budzet_ko_id(r.getBd_budzet_ko_id());
		rola.setBd_budzet_ko_idm(r.getBd_budzet_ko_idm());
		rola.setBp_rola_id(r.getBp_rola_id());
		rola.setBp_rola_idn(r.getBp_rola_idn());
		rola.setBudzet_id(r.getBudzet_id());
		rola.setId(r.getId());
		rola.setNazwa(r.getNazwa());
		rola.setOsoba_id(r.getOsoba_id());
		rola.setOsobaGuid(r.getOsobaGuid());
		rola.setPracownik_id(r.getPracownik_id());
		rola.setPracownik_nrewid(r.getPracownik_nrewid());
		rola.setAvailable(r.getAvailable());
		
		return rola;
	}
	
	
	public static BudzetPozycjaSimple MapBudzetPozycjaSimpleDBToBudzetPozycjaSimple(
			BudzetPozycjaSimpleDB budDB) {
		BudzetPozycjaSimple bud = BudzetPozycjaSimpleDAO.getInstance().getBudzetPozycjaSimple();

		bud.setId(budDB.getId());
		bud.setBd_nad_rodzaj_ko_id(budDB.getBd_nad_rodzaj_ko_id());
		bud.setCzy_aktywna(budDB.getCzyAktywna());
		bud.setErpId(budDB.getErpId());
		bud.setNazwa(budDB.getNazwa());
		bud.setNrpoz(budDB.getNrpoz());
		bud.setOpis(budDB.getOpis());
		bud.setP_koszt(budDB.getP_koszt());
		bud.setParent(budDB.getParent());
		bud.setParentId(budDB.getParentId());
		bud.setParentIdm(bud.getParentIdm());
		bud.setR_koszt(budDB.getR_koszt());
		bud.setRez_koszt(budDB.getRez_koszt());
		bud.setWytworId(budDB.getWytwor_id());
		bud.setWytworIdm(budDB.getWytwor_idm());
		bud.setAvailable(budDB.getAvailable());
		
		return bud;

	}

	public static BudzetDoKomorki MapBudzetDOKomorkiDBToBudzetDoKomorki(
			BudzetDoKomorkiDB budDoKoDB) {
		BudzetDoKomorki bud = BudzetDoKomorkiDAO.getInstance().getBudzetDoKomorki();
		
		bud.setId(budDoKoDB.getId());
		
		bud.setBudzet_id(budDoKoDB.getBudzet_id());
		bud.setBudzet_idn(budDoKoDB.getBudzet_idn());
		bud.setBudzet_nad_id(budDoKoDB.getBudzet_nad_id());
		bud.setBudzet_nad_idn(budDoKoDB.getBudzet_nad_idn());
		bud.setOkresId(budDoKoDB.getOkresId());
		bud.setCzyWiodaca(budDoKoDB.getCzyWiodaca());
		bud.setKomorkaId(budDoKoDB.getKomorkaId());
		bud.setKomorkaIdn(budDoKoDB.getKomorkaIdn());
		bud.setNazwaKomorkiBudzetowej(budDoKoDB.getNazwaKomorkiBudzetowej());
		bud.setNazwaKomorkiOrg(budDoKoDB.getNazwaKomorkiOrg());
		
		return bud;
		
	}

	public static SzablonyRozliczenFaktur MapSzablonyRozliczenFakturDBDBToSzablonyRozliczenFaktur(
			SzablonyRozliczenFakturDB szabDB) {
		SzablonyRozliczenFaktur szab=SzablonyRozliczenFakturDAO.getInstance().getSzablonyRozliczenFaktur();
		
		szab.setId(szabDB.getId());
		
		szab.setIdm(szabDB.getIdm());
		szab.setErpId(szabDB.getErpId());
		szab.setNazwa(szabDB.getNazwa());
		szab.setOkresId(szabDB.getOkresId());
		szab.setOkresIdm(szabDB.getOkres_rozl());
		
		return szab;
	}

	public static ProjectAccountPiatki MapProjectAccountPiatkiDBToProjectAccountPiatki(
			ProjectAccountPiatkiDB piatkiDB) {
		ProjectAccountPiatki piatki=ProjectAccountPiatkiDAO.getInstance().getProjectAccountPiatki();
		
		piatki.setId(piatkiDB.getId());
		
		piatki.setKonto_ido(piatkiDB.getKonto_ido());
		piatki.setKontrakt_id(piatkiDB.getKontrakt_id());
		piatki.setKontrakt_idm(piatkiDB.getKontrakt_idm());
		piatki.setRep_atrybut_ido(piatkiDB.getRep_atrybut_ido());
		piatki.setAvailable(piatkiDB.getAvailable());
		return piatki;
	}

	public static NumeryZamowien MapNumeryZamowienDBToNumeryZamowien(
			NumeryZamowienDB nzDB) {
		NumeryZamowien nz=NumeryZamowienDAO.getInstance().getNumeryZamowien();
		nz.setId(nzDB.getId());
		
		nz.setDat_dok(nzDB.getDat_dok());
		nz.setDat_odb(nzDB.getDat_odb());
		nz.setErpId(nzDB.getErpId());
		nz.setDostawca_id(nzDB.getDostawca_id());
		nz.setDostawca_idn(nzDB.getDostawca_idn());
		nz.setIdm(nzDB.getIdm());
		nz.setOpis_wewn(nzDB.getOpis_wewn());
		nz.setUwagi(nzDB.getUwagi());
		nz.setWartdok(nzDB.getWartdok());
		nz.setAvailable(nzDB.getAvailable());
		
		return nz;
	}

	public static ZamowienieSimplePozycje MapZamowienieSimplePozycjeDBToZamowienieSimplePozycje(
			ZamowienieSimplePozycjeDB zspDB) {
	ZamowienieSimplePozycje zsp=ZamowienieSimplePozycjeDAO.getInstance().getZamowienieSimplePozycje();
	
	zsp.setId(zspDB.getId());
	zsp.setBdBudzetKoId(zspDB.getBdBudzetKoId());
	zsp.setBdBudzetKoIdm(zspDB.getBdBudzetKoIdm());
	zsp.setBdPlanZamId(zspDB.getBdPlanZamId());
	zsp.setBdPlanZamIdm(zspDB.getBdPlanZamIdm());
	zsp.setBdRezerwacjaId(zspDB.getBdRezerwacjaId());
	zsp.setBdRezerwacjaIdm(zspDB.getBdRezerwacjaIdm());
	zsp.setBdRodzajKoId(zspDB.getBdRodzajKoId());
	zsp.setBdRodzajPlanZamId(zspDB.getBdRodzajPlanZamId());
	zsp.setBudzetId(zspDB.getBudzetId());
	zsp.setBudzetIdm(zspDB.getBudzetIdm());
	zsp.setCena(zspDB.getCena());
	zsp.setCenabrutto(zspDB.getCenabrutto());
	zsp.setEtapId(zspDB.getEtapId());
	zsp.setIlosc(zspDB.getIlosc());
	zsp.setKontraktId(zspDB.getKontraktId());
	zsp.setKoszt(zspDB.getKoszt());
	zsp.setKwotnett(zspDB.getKwotnett());
	zsp.setKwotvat(zspDB.getKwotvat());
	zsp.setNrpoz(zspDB.getNrpoz());
	zsp.setProjektId(zspDB.getProjektId());
	zsp.setSupplier_order_id(zspDB.getSupplier_order_id());
	zsp.setSupplier_order_idm(zspDB.getSupplier_order_idm());
	zsp.setWytwor_idm(zspDB.getWytwor_idm());
	zsp.setZamdospoz_id(zspDB.getZamdospoz_id());
	zsp.setZasobId(zspDB.getZasobId());
	zsp.setIloscZreal(zspDB.getIloscZreal());
	zsp.setWalutaId(zspDB.getWalutaId());
	zsp.setVatStawId(zspDB.getVatStawId());
	zsp.setKomorkaOrganizacyjaERPId(zspDB.getKomorkaOrganizacyjaERPId());
	
	return zsp;
	
	}
	
	/**
	 * Metoda mapuje obiekt PracownikDB na Pracownik
	 * @param pDB
	 * @return
	 */
	public static Pracownik MapPracownikDBToPracownik(PracownikDB pDB){
		Pracownik p = PracownikDAO.getInstance().getPracownik();
		
		p.setCzyAktywna(pDB.isCzy_aktywny_osoba());
		p.setGuid(pDB.getGuid());
		p.setId(pDB.getId());
		p.setImie(pDB.getImie());
		p.setNazwisko(pDB.getNazwisko());
		p.setNrewid(pDB.getNrewid());
		p.setPesel(pDB.getPesel());
		p.setZewnetrznyId(pDB.getPracownik_id());
		p.setZewnetrznyIdm(pDB.getPracownik_idn());
		
		return p;
	}
	
	/**
	 * Metoda mapuje obiekt TypyUrlopowDB na TypyUrlopow
	 * @param tu
	 * @return
	 */
	public static TypyUrlopow MapTypyUrlopowDBToTypyUrlopow(TypyUrlopowDB tu){
		TypyUrlopow t = TypyUrlopowDAO.getInstance().getTypyUrlopow();
		
		t.setErpId(tu.getErpId());
		t.setId(tu.getId());
		t.setIdm(tu.getIdm());
		tu.setOpis(tu.getOpis());
		tu.setOpis_dlugi(tu.getOpis_dlugi());
		
		return t;
	}

	public static WniosekORezerwacje mapWniosekORezerwacjeDBToWniosekORezerwacje(
			WniosekORezerwacjeDB wnDB) {
		WniosekORezerwacje wn=WniosekORezerwacjeDAO.getInstance().getWniosekORezerwacje();
		wn.setId(wnDB.getId());
		
		wn.setBdBudzetKoId(wnDB.getBdBudzetKoId());
		wn.setBdBudzetKoIdm(wnDB.getBdBudzetKoIdm());
		wn.setBdPlanZamId(wnDB.getBdPlanZamId());
		wn.setBdPlanZamIdm(wnDB.getBdPlanZamIdm());
		wn.setBdRezerwacjaId(wnDB.getBdRezerwacjaId());
		wn.setBdRezerwacjaIdm(wnDB.getBdRezerwacjaIdm());
		wn.setBdRezerwacjaPozId(wnDB.getBdRezerwacjaPozId());
		wn.setBdRodzajKoId(wnDB.getBdRodzajKoId());
		wn.setBdRodzajPlanZamId(wnDB.getBdRodzajPlanZamId());
		wn.setBdStrBudzetId(wnDB.getBdStrBudzetId());
		wn.setBdTypRezerwacjaIdn(wnDB.getBdTypRezerwacjaIdn());
		wn.setBkBdOkrSzablonRelId(wnDB.getBkBdOkrSzablonRelId());
		wn.setBkBdSzablonPozId(wnDB.getBkBdSzablonPozId());
		/* nie ma tego na tescie
		    	setBkCzyArchiwalny(item.getBk_czy_archiwalny());*/
		wn.setBkNazwa(wnDB.getBkNazwa());
		wn.setBkPozNazwa(wnDB.getBkPozNazwa());
		wn.setBkPozNrpoz(wnDB.getBkPozNrpoz());
		wn.setBkPozPKoszt(wnDB.getBkPozPKoszt());
		wn.setBkPozPKosztZrodla(wnDB.getBkPozPKosztZrodla());
		wn.setBkPozRKoszt(wnDB.getBkPozRKoszt());
		wn.setBkPozRKosztZrodla(wnDB.getBkPozRezKosztZrodla());
		wn.setBkPozRezKoszt(wnDB.getBkPozRezKoszt());
		wn.setBkPozRezKosztZrodla(wnDB.getBkPozRezKosztZrodla());
		/* nie ma tego na tescie
		    	setBkStatus(item.getBk_status());*/
		wn.setBkWytworId(wnDB.getBkWytworId());
		wn.setBudzetId(wnDB.getBudzetId());
		wn.setBudzetIdm(wnDB.getBudzetIdm());
		wn.setCena(wnDB.getCena());
		wn.setCzyObslugaPzp(wnDB.getCzyObslugaPzp());
		wn.setIlosc(wnDB.getIlosc());
		wn.setKoszt(wnDB.getKoszt());
		wn.setMagazynId(wnDB.getMagazynId());
		wn.setNazwa(wnDB.getNazwa());
		wn.setNrpoz(wnDB.getNrpoz());
		wn.setOkrrozlId(wnDB.getOkrrozlId());
		wn.setPzpBdOkrSzablonRelId(wnDB.getPzpBdOkrSzablonRelId());
		wn.setPzpBdSzablonPozId(wnDB.getPzpBdSzablonPozId());
		/* nie ma tego na tescie
		    	setPzpCzyArchiwalny(item.getPzp_czy_archiwalny());*/
		wn.setPzpJmId(wnDB.getPzpJmId());
		wn.setPzpJmIdn(wnDB.getPzpJmIdn());
		wn.setPzpNazwa(wnDB.getPzpNazwa());
		wn.setPzpPozNazwa(wnDB.getPzpPozNazwa());
		wn.setPzpPozNrpoz(wnDB.getPzpPozNrpoz());
		wn.setPzpPozPIlosc(wnDB.getPzpPozPIlosc());
		wn.setPzpPozPKoszt(wnDB.getPzpPozPKoszt());
		wn.setPzpPozRIlosc(wnDB.getPzpPozRIlosc());
		wn.setPzpPozRKoszt(wnDB.getPzpPozRezKoszt());
		wn.setPzpPozRezIlosc(wnDB.getPzpPozRezIlosc());
		wn.setPzpPozRezKoszt(wnDB.getPzpPozRezKoszt());
		/* nie ma tego na tescie
		    	setPzpStatus(item.getPzp_status());*/
		wn.setPzpWytworId(wnDB.getPzpWytworId());
		wn.setPzpWytworIdm(wnDB.getPzpWytworIdm());		
		wn.setRezJmId(wnDB.getRezJmId());
		wn.setRezJmIdn(wnDB.getRezJmIdn());
		wn.setRezerwacjaCzyArchiwalny(wnDB.getRezerwacjaCzyArchiwalny());
		wn.setRezerwacjaStatus(wnDB.getRezerwacjaStatus());
		wn.setVatstawId(wnDB.getVatstawId());
		wn.setWytworEditIdm(wnDB.getWytworEditIdm());
		wn.setWytworEditNazwa(wnDB.getWytworEditNazwa());
		wn.setWytworId(wnDB.getWytworId());
		wn.setZadanieId(wnDB.getZadanieId());			
		wn.setZadanieIdn(wnDB.getZadanieIdn());		
		wn.setZadanieNazwa(wnDB.getZadanieNazwa());	
		wn.setZrodloId(wnDB.getZrodloId());
		wn.setZrodloIdn(wnDB.getZrodloIdn());			
		wn.setZrodloNazwa(wnDB.getZrodloNazwa());		
		wn.setAvailable(wnDB.getAvailable());
		return wn;
	}
	
	public static Zrodlo MapZrodloDBToZrodlo(ZrodloDB zDB){
		Zrodlo z = ZrodloDAO.getInstance().getZrodlo();
		
		z.setErpId(zDB.getErpId());
		z.setId(zDB.getId());
		z.setIdentyfikator_num(zDB.getIdentyfikator_num());
		z.setIdm(zDB.getIdm());
		z.setZrodlo_nazwa(zDB.getZrodlo_nazwa());
		z.setAvailable(zDB.isAvailable());
		
		return z;
	}
}
