package pl.compan.docusafe.general.mapers;

import java.util.ArrayList;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.dao.BudzetDoKomorkiDAO;
import pl.compan.docusafe.general.canonical.dao.BudzetKOKomorekDAO;
import pl.compan.docusafe.general.canonical.dao.BudzetyDAO;
import pl.compan.docusafe.general.canonical.dao.NumeryZamowienDAO;
import pl.compan.docusafe.general.canonical.dao.OkresBudzetowyDAO;
import pl.compan.docusafe.general.canonical.dao.JednostkaMiaryDAO;
import pl.compan.docusafe.general.canonical.dao.JednostkaOrganizacyjnaDAO;
import pl.compan.docusafe.general.canonical.dao.KontrahentDAO;
import pl.compan.docusafe.general.canonical.dao.KontrahentKontoDAO;
import pl.compan.docusafe.general.canonical.dao.MiejsceDostawyDAO;
import pl.compan.docusafe.general.canonical.dao.PlanZakupowDAO;
import pl.compan.docusafe.general.canonical.dao.PracownikDAO;
import pl.compan.docusafe.general.canonical.dao.ProduktyFakturyDAO;
import pl.compan.docusafe.general.canonical.dao.ProjectAccountPiatkiDAO;
import pl.compan.docusafe.general.canonical.dao.ProjektBudzetDAO;
import pl.compan.docusafe.general.canonical.dao.ProjektBudzetEtapDAO;
import pl.compan.docusafe.general.canonical.dao.ProjektBudzetZasobDAO;
import pl.compan.docusafe.general.canonical.dao.ProjektDAO;
import pl.compan.docusafe.general.canonical.dao.RodzajePlanowZakupuDAO;
import pl.compan.docusafe.general.canonical.dao.RoleWBudzecieDAO;
import pl.compan.docusafe.general.canonical.dao.RoleWProjekcieDAO;
import pl.compan.docusafe.general.canonical.dao.StawkaVatDAO;
import pl.compan.docusafe.general.canonical.dao.SzablonyRozliczenFakturDAO;
import pl.compan.docusafe.general.canonical.dao.TypDokumentuDAO;
import pl.compan.docusafe.general.canonical.dao.TypPlatnosciDAO;
import pl.compan.docusafe.general.canonical.dao.TypyUrlopowDAO;
import pl.compan.docusafe.general.canonical.dao.TypyWnioskowORezerwacjeDAO;
import pl.compan.docusafe.general.canonical.dao.WalutaDAO;
import pl.compan.docusafe.general.canonical.dao.WniosekORezerwacjeDAO;
import pl.compan.docusafe.general.canonical.dao.ZadaniaFinansoweDAO;
import pl.compan.docusafe.general.canonical.dao.ZleceniaProdukcyjneDAO;
import pl.compan.docusafe.general.canonical.dao.ZrodloDAO;
import pl.compan.docusafe.general.canonical.dao.ZrodloFinansowaniaProjektowDAO;
import pl.compan.docusafe.general.canonical.factory.JednostkaOrganizacyjnaFactory;
import pl.compan.docusafe.general.canonical.model.Budzet;
import pl.compan.docusafe.general.canonical.model.BudzetDoKomorki;
import pl.compan.docusafe.general.canonical.model.BudzetKOKomorek;
import pl.compan.docusafe.general.canonical.model.BudzetPozycjaSimple;
import pl.compan.docusafe.general.canonical.model.BudzetPozycjaSimpleDAO;
import pl.compan.docusafe.general.canonical.model.NumeryZamowien;
import pl.compan.docusafe.general.canonical.model.OkresBudzetowy;
import pl.compan.docusafe.general.canonical.model.JednostkaMiary;
import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.canonical.model.Kontrahent;
import pl.compan.docusafe.general.canonical.model.KontrahentKonto;
import pl.compan.docusafe.general.canonical.model.MiejsceDostawy;
import pl.compan.docusafe.general.canonical.model.PlanZakupow;
import pl.compan.docusafe.general.canonical.model.Pracownik;
import pl.compan.docusafe.general.canonical.model.PracownikWJednostkachOrganizacyjnych;
import pl.compan.docusafe.general.canonical.model.ProduktyFaktury;
import pl.compan.docusafe.general.canonical.model.ProjectAccountPiatki;
import pl.compan.docusafe.general.canonical.model.Projekt;
import pl.compan.docusafe.general.canonical.model.ProjektBudzet;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetEtap;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetZasob;
import pl.compan.docusafe.general.canonical.model.RodzajePlanowZakupu;
import pl.compan.docusafe.general.canonical.model.RoleWBudzecie;
import pl.compan.docusafe.general.canonical.model.RoleWProjekcie;
import pl.compan.docusafe.general.canonical.model.StawkaVat;
import pl.compan.docusafe.general.canonical.model.SzablonyRozliczenFaktur;
import pl.compan.docusafe.general.canonical.model.TypDokumentu;
import pl.compan.docusafe.general.canonical.model.TypPlatnosci;
import pl.compan.docusafe.general.canonical.model.TypyUrlopow;
import pl.compan.docusafe.general.canonical.model.TypyWnioskowORezerwacje;
import pl.compan.docusafe.general.canonical.model.Waluta;
import pl.compan.docusafe.general.canonical.model.WniosekORezerwacje;
import pl.compan.docusafe.general.canonical.model.ZadaniaFinansowe;
import pl.compan.docusafe.general.canonical.model.ZleceniaProdukcyjne;
import pl.compan.docusafe.general.canonical.model.Zrodlo;
import pl.compan.docusafe.general.canonical.model.ZrodloFinansowaniaProjektow;
import pl.compan.docusafe.general.utils.PasswordUtils;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.AbsenceType;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ApplicationForReservationType;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.BudgetUnitOrganizationUnit;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.BudgetView;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.CostInvoiceProduct;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Currency;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.DeliveryLocation;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Employee;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.FinancialTask;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.FundingSource;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.OrganizationalUnit;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PatternsOfCostSharing;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PaymentTerms;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Person;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PersonAgreement;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProductionOrder;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Project;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectAccount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectBudgetItem;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectBudgetItemResource;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectBugdet;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PurchaseDocumentType;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PurchasePlan;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PurchasePlanKind;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.RolesInProjects;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.RolesInUnitBudgetKo;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.SourcesOfProjectFunding;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Supplier;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.SupplierBankAccount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.SupplierOrderHeader;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitBudget;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitBudgetKo;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitBudgetKoKind;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitOfMeasure;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.VatRate;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.RequestForReservation;

/**
 * Klasa odpowiedzialna za mapowanie pomi�dzy obiektami z us�ugi [WS] - Simple a
 * modelem kanonicznym [Canonical]
 * 
 * @author Gubari Nanokata
 * 
 */
public class WSToCanonicalMapper {

	/**
	 * Metoda mapuje OrganizationalUnit [WS] na JednostkaOrganizacyjna
	 * [Canonical]
	 * 
	 * @param ou
	 * @return
	 */
	public static JednostkaOrganizacyjna MapOrganizationalUnitToJednostkaOrganizacyjna(
			OrganizationalUnit ou) {
		JednostkaOrganizacyjna jo = JednostkaOrganizacyjnaDAO.getInstance()
				.getJednostkaOrganizacyjna();
		try {
			jo.setAktywna(MapperUtils.getBooleanFromDouble(ou.getCzy_aktywna()));
			jo.setKadrowa(MapperUtils.getBooleanFromDouble(ou.getCzy_kadrowa()));
			jo.setKosztowa(MapperUtils.getBooleanFromDouble(ou
					.getCzy_kosztowa()));
			jo.setNazwa(ou.getNazwa());
			jo.setRodzicIdn(ou.getKomorka_nad_idn());
			jo.setIdn(ou.getIdm());
			return jo;
		} catch (Exception exc) {
			Log.debug(exc);
		}
		return null;
	}

	/**
	 * Metoda mapuje Currency [WS] na Waluta [Canonical]
	 * 
	 * @param c
	 * @return
	 */
	public static Waluta MapCurrencyToWaluta(Currency c) {
		Waluta w = WalutaDAO.getInstance().getWaluta();

		w.setId((MapperUtils.getLongFromDouble(c.getId())));
		w.setNazwa(c.getNazwa());
		w.setCn(c.getIdm());
		w.isAktywna(true);
		return w;
	}

	/**
	 * Metoda mapuje PaymentTerms [WS] na TypPlatnosci [Canonical]
	 * 
	 * @param pt
	 * @return
	 */
	public static TypPlatnosci MapPaymentTermsToTypPlatnosci(PaymentTerms pt) {
		TypPlatnosci tp = TypPlatnosciDAO.getInstance().getTypPlatnosci();

		tp.setId((MapperUtils.getLongFromDouble(pt.getId())));
		tp.setNazwa(pt.getIdm());
		tp.setCn(pt.getIdm());
		tp.setObiektPowiazany(pt.getPrzeznaczenie());
		tp.isAktywna(true);
		return tp;
	}

	/**
	 * Metoda mapuje Supplier [WS] na Kontrahent [Canonical]
	 * 
	 * @param supp
	 * @return
	 */
	public static Kontrahent MapSupplierToKontrahent(Supplier supp) {
		Kontrahent kontrahent = KontrahentDAO.getInstance().getKontrahent();

		kontrahent.setId(MapperUtils.getLongFromDouble(supp.getId()));
		kontrahent.setNazwa(supp.getDostawca_nazwa());
		kontrahent.setKodpocztowy(supp.getDostawca_kodpoczt());
		kontrahent.setMiejscowosc(supp.getDostawca_miasto());
		kontrahent.setNip(supp.getDostawca_nip());
		kontrahent.setNrDomu(supp.getDostawca_nrdomu());
		kontrahent.setMiejscowosc(supp.getDostawca_miasto());
		kontrahent.setNrMieszkania(supp.getDostawca_nrmieszk());
		kontrahent.setUlica(supp.getDostawca_ulica());
		kontrahent.setErpId(MapperUtils.getLongFromDouble(supp.getId()));
		kontrahent.setKraj(String.valueOf(MapperUtils.getLongFromDouble(supp.getKlasdost_id())));
		kontrahent.setNowy(true);
		return kontrahent;
	}

	/**
	 * Metoda mapuje DeliveryLocation [WS] na MiejsceDostawy [Canonical]
	 * 
	 * @param dl
	 * @return
	 */
	public static MiejsceDostawy MapDeliveryLocationToMiejsceDostawy(
			DeliveryLocation dl) {
		MiejsceDostawy md = MiejsceDostawyDAO.getInstance().getMiejsceDostawy();

		md.setId((MapperUtils.getLongFromDouble(dl.getId())));
		md.setNazwa(dl.getNazwa());
		md.setCn(MapperUtils.getStringFromDouble(dl.getId()));
		md.isAktywna(MapperUtils.getBooleanFromDouble(dl.getCzy_aktywna()));
		return md;
	}

	/**
	 * Metoda mapuje PurchaseDocumentType [WS] na TypDokumentu [Canonical]
	 * 
	 * @param pdt
	 * @return
	 */
	public static TypDokumentu MapPurchaseDocumentTypeToTypDostawy(
			PurchaseDocumentType pdt) {
		TypDokumentu td = TypDokumentuDAO.getInstance().getTypDokumentu();

		td.setId(MapperUtils.getLongFromDouble(pdt.getId()));
		td.setTypdokzak_id(MapperUtils.getLongFromDouble(pdt.getId()));
		td.setNazwa(pdt.getNazwa());
		td.setCn(pdt.getTypdok_ids());
		td.setTypdokIdn(pdt.getIdm());
		td.setSymbol_waluty(pdt.getSymbolwaluty());
		td.setCzywal(MapperUtils.getBooleanFromDouble(pdt.getCzywal()));
		td.setAktywna(MapperUtils.getBooleanFromDouble(pdt.getCzy_aktywny()));
		return td;
	}

	/**
	 * Metoda mapuje VatRate [WS] na StawkaVat [Canonical]
	 * 
	 * @param vr
	 * @return
	 */
	public static StawkaVat MapVatRateToStawkaVat(VatRate vr) {
		StawkaVat sv = StawkaVatDAO.getInstance().getStawkaVat();

		sv.setId(MapperUtils.getLongFromDouble(vr.getId()));
		sv.setErp_id(MapperUtils.getLongFromDouble(vr.getId()));
		sv.setNazwa(vr.getNazwa());
		sv.setCn(MapperUtils.getStringFromDouble(vr.getProcent()));
		sv.setCzy_zwolniona(MapperUtils.getBooleanFromDouble(vr
				.getCzy_zwolniona()));
		sv.setNie_podlega(MapperUtils.getBooleanFromDouble(vr.getNie_podlega()));
		sv.setAktywna(MapperUtils.getBooleanFromDouble(vr.getCzy_aktywna()));
		sv.setIdm(vr.getIdm());
		
		return sv;
	}

	/**
	 * Metoda mapuje CostInvoiceProduct [WS] na ProduktyFaktury [Canonical]
	 * 
	 * @param cost
	 * @return
	 */
	public static ProduktyFaktury MapCostInvoiceProductToProduktyFaktury(
			CostInvoiceProduct cost) {
		ProduktyFaktury pf = ProduktyFakturyDAO.getInstance()
				.getProduktyFaktury();

		pf.setId(MapperUtils.getLongFromDouble(cost.getId()));
		pf.setWytworId(MapperUtils.getLongFromDouble(cost.getId()));
		pf.setNazwa(cost.getNazwa());
		pf.setCn(cost.getIdm());
		pf.setJmIdn(cost.getJm_idn());
		pf.setKlasyWytwIdn(cost.getKlaswytw_idn());
		pf.setRodzTowaru(cost.getRodztowaru());
		pf.setVatStawIds(cost.getVatstaw_ids());
		pf.setAktywna(true);
		return pf;
	}

	/**
	 * Metoda mapuje UnitOfMeasure [WS] na JednostkaMiary [Canonical]
	 * 
	 * @param uom
	 * @return
	 */
	public static JednostkaMiary MapUnitOfMeasureToJednostkaMiary(
			UnitOfMeasure uom) {
		JednostkaMiary jm = JednostkaMiaryDAO.getInstance().getJednostkaMiary();

		jm.setId(MapperUtils.getLongFromDouble(uom.getId()));
		jm.setErpId(MapperUtils.getLongFromDouble(uom.getId()));
		jm.setTitle(uom.getNazwa());
		jm.setCn(uom.getIdm());
		jm.setAktywna(true);
		jm.setJmtyp_idn(uom.getJmtyp_idn());
		jm.setPrecyzjajm(uom.getPrecyzjajm());
		return jm;
	}
	
	

	/**
	 * Metoda mapuje obiekt z [WS] Employee na obiekt kanoniczny
	 * Pracownik [CAN]
	 * 
	 * @param employee
	 * @return
	 */
	public static Pracownik MapEmployeeToPracownik(Employee employee) {
		Pracownik pracownik = PracownikDAO.getInstance().getPracownik();

		try {
			// Imi�
			if (employee.getImie()!=null)
			{
				pracownik.setImie(employee.getImie());
			};
			// Zewn�rzny identyfikator
			
				pracownik.setZewnetrznyId(MapperUtils.getLongFromDouble(employee	
					.getId()));			
			pracownik.setZewnetrznyIdm(MapperUtils.getStringFromInt(employee
					.getIdm()));
			// Nazwisko
			if (employee.getNazwisko()!=null)
			{
				pracownik.setNazwisko(employee.getNazwisko());
			}
			// Pesel
			if (employee.getPesel()!=null)
			{
				pracownik.setPesel(employee.getPesel());
			}
			
			// Jednostka organizacyjna
			JednostkaOrganizacyjnaFactory jednostkaOrganizacyjnaFactory = new JednostkaOrganizacyjnaFactory();
			JednostkaOrganizacyjna jednostkaOrganizacyjna = jednostkaOrganizacyjnaFactory
					.PobierzJednostkePoIdmDB(employee.getKomorka_org());
			List<JednostkaOrganizacyjna> listaJednostekOrganizacyjnych = new ArrayList<JednostkaOrganizacyjna>();
			listaJednostekOrganizacyjnych.add(jednostkaOrganizacyjna);
			pracownik.setJednostkiOrganizacyjne(listaJednostekOrganizacyjnych);

			// Czy generujemy has�o dla u�ytkownika
			boolean generujHaslo = false;
			if (Docusafe
					.getAdditionProperty("IMPORT_PRACOWNIKOW_GENERUJ_HASLO") != null) {
				generujHaslo = new Boolean(
						Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_GENERUJ_HASLO"));
			}

			if (generujHaslo) {
				pracownik.setHaslo(employee.getPesel());
			}

			// Czy generujemy login
			boolean generujLogin = false;
			if (Docusafe
					.getAdditionProperty("IMPORT_PRACOWNIKOW_GENERUJ_LOGIN") != null) {
				generujLogin = new Boolean(
						Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_GENERUJ_LOGIN"));
			}

			if (generujLogin) {
				
				PasswordUtils pu = new PasswordUtils();
				
				String login = pu.generujLogin(employee.getImie(),  employee.getPesel());
				
				pracownik.setLogin(login);		
				
			}

			boolean mapuj = false;
			if (Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_MAPUJ") != null) {
				mapuj = new Boolean(
						Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_MAPUJ"));
			}

			if (mapuj) {
				// W pliku adds.properties ustawiamy jedno z dostepnych p�l,
				// kt�re b�dzie okre�la� wsp�lny identyfikator
				// dla mapowanych modeli dla obiektu Pracownik
				// IMPORT_PRACOWNIKOW_MAPUJ = true - w og�le u�ywaj mapowania
				// IMPORT_PRACOWNIKOW_POLE_MAPUJACE_NAZWA = PESEL/GUID kt�rym
				// polem mapujemy

				if (Docusafe
						.getAdditionProperty("IMPORT_PRACOWNIKOW_POLE_MAPUJACE_NAZWA") == null) {
					throw new EdmException(
							"Brak ustawionego adds'a IMPORT_PRACOWNIKOW_POLE_MAPUJACE_NAZWA");
				}

				String nazwaPolaMapujacego = Docusafe
						.getAdditionProperty("IMPORT_PRACOWNIKOW_POLE_MAPUJACE_NAZWA");

				if (nazwaPolaMapujacego.equals("PESEL")) {
					if (employee.getPesel()!=null)
					{
					pracownik.setOperatorDoPorownywania(employee.getPesel());
					}
				} else 
				if (nazwaPolaMapujacego.equals("PRACOWNIK_NR_EWID")) {
					Integer idm = new Integer(employee.getIdm());
					pracownik.setOperatorDoPorownywania(idm.toString());
				}
			}
			return pracownik;
		} catch (Exception exc) {
			Log.debug(exc);
		}
		return null;
	}

	

	/**
	 * Metoda mapuje obiekt z WS Person [BD] na obiekt kanoniczny
	 * Pracownik [CAN]
	 * 
	 * @param person
	 * @return
	 */
	public static Pracownik MapPersonToPracownik(Person person) {
		
		Pracownik pracownik = PracownikDAO.getInstance().getPracownik();

		try {
			// Imi�
			if (person.getImie()!=null)	
			{
				pracownik.setImie(person.getImie());
			}
			
			// Zewn�rzny identyfikator
			pracownik.setZewnetrznyId(MapperUtils.getLongFromDouble(person.getPracownik_id()));
			// Identyfikator pracownik nr ewid (wykorzystywany do mapowania w LDAP)
			
			String poleZIdentyfikatoremLdapAd = Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_POLE_Z_IDENTYFIKATOREM_LDAP_AD");
			if (poleZIdentyfikatoremLdapAd.equals("pracownik_idn"))
			{
				pracownik.setZewnetrznyIdm(MapperUtils.getStringFromInt(person.getPracownik_nrewid()));
				pracownik.setOperatorDoPorownywania(MapperUtils.getStringFromInt(person.getPracownik_nrewid()));
			} else
				if (poleZIdentyfikatoremLdapAd.equals("guid"))
				{
					pracownik.setZewnetrznyIdm(person.getGuid());
					pracownik.setOperatorDoPorownywania(person.getGuid());
				} else
					if (poleZIdentyfikatoremLdapAd.equals("pesel"))
					{
						pracownik.setZewnetrznyIdm(person.getPesel());
						pracownik.setOperatorDoPorownywania(person.getPesel());
					}
			
			// Nazwisko
			if (person.getNazwisko()!=null)
			{
				pracownik.setNazwisko(person.getNazwisko());
			}
			// Pesel
			if (person.getPesel()!=null)
			{
				pracownik.setPesel(person.getPesel());
			}
			// GUID
			if (person.getGuid()!=null)
			{
				pracownik.setGuid(person.getGuid());
			}
			// Jednostka organizacyjna
			JednostkaOrganizacyjnaFactory jednostkaOrganizacyjnaFactory = new JednostkaOrganizacyjnaFactory();
			JednostkaOrganizacyjna jednostkaOrganizacyjna = jednostkaOrganizacyjnaFactory
					.PobierzJednostkePoIdmDB(person.getKomorka_idn());

			List<JednostkaOrganizacyjna> listaJednostekOrganizacyjnych = new ArrayList<JednostkaOrganizacyjna>();

			if (jednostkaOrganizacyjna != null) {
				listaJednostekOrganizacyjnych.add(jednostkaOrganizacyjna);
			}
			pracownik.setJednostkiOrganizacyjne(listaJednostekOrganizacyjnych);

			// Czy generujemy has�o dla u�ytkownika
			boolean generujHaslo = false;
			if (Docusafe
					.getAdditionProperty("IMPORT_PRACOWNIKOW_GENERUJ_HASLO") != null) {
				generujHaslo = new Boolean(
						Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_GENERUJ_HASLO"));
			}
			
			if (generujHaslo) {
				pracownik.setHaslo(person.getPesel());
			}

			// Czy generujemy login
			boolean generujLogin = false;
			if (Docusafe
					.getAdditionProperty("IMPORT_PRACOWNIKOW_GENERUJ_LOGIN") != null) {
				generujLogin = new Boolean(
						Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_GENERUJ_LOGIN"));
			}

			if (generujLogin) {
				
				PasswordUtils pu = new PasswordUtils();
				
				String login = pu.generujLogin(person.getImie(),  person.getPesel());
				
				pracownik.setLogin(login);				
			}			
			
			return pracownik;
		} catch (Exception exc) {
			Log.debug(exc);
		}
		return null;
	}
	
	
	/**
	 * Metoda mapuje BudgetView [WS] na Budzet [Canonical]
	 * 
	 * @param budgetView
	 * @return
	 */
	public static Budzet MapBudgetViewToBudzet(BudgetView budgetView) {
		Budzet budzet = BudzetyDAO.getInstance().getBudzet();

		budzet.setAvailable(MapperUtils.getBooleanFromDouble(budgetView.getCzy_aktywna()));
		budzet.setBdBudzetId(budgetView.getBd_budzet_id());
		budzet.setBdBudzetIdm(budgetView.getBd_budzet_idm());
		budzet.setBdBudzetRodzajId(budgetView.getBd_budzet_rodzaj_id());
		budzet.setBdBudzetRodzajZadaniaId(budgetView.getBd_budzet_rodzaj_zadania_id());
		budzet.setBdBudzetRodzajZrodlaId(budgetView.getBd_budzet_rodzaj_zrodla_id());
		budzet.setBdGrupaRodzajId(budgetView.getBd_grupa_rodzaj_id());
		budzet.setBdRodzaj(budgetView.getBd_rodzaj());
		budzet.setBdStrBudzetIdn(budgetView.getBd_str_budzet_idn());
		budzet.setBdStrBudzetIds(budgetView.getBd_str_budzet_ids());
		budzet.setBdSzablonPozId(budgetView.getBd_szablon_poz_id());
		budzet.setBdZadanieId(budgetView.getBd_zadanie_id());
		budzet.setBdZadanieIdn(budgetView.getBd_zadanie_idn());
		budzet.setBudWOkrNazwa(budgetView.getBud_w_okr_nazwa());
		budzet.setBudzetId(budgetView.getBudzet_id());
		budzet.setBudzetIdm(budgetView.getBudzet_idm());
		budzet.setBudzetIds(budgetView.getBudzet_ids());
		budzet.setBudzetNazwa(budgetView.getBudzet_nazwa());
		budzet.setCzyAktywna(budgetView.getCzy_aktywna());
		budzet.setDokBdStrBudzetIdn(budgetView.getDok_bd_str_budzet_idn());
		budzet.setDokKomNazwa(budgetView.getDok_kom_nazwa());
		budzet.setDokPozLimit(budgetView.getDok_poz_limit());
		budzet.setDokPozNazwa(budgetView.getDok_poz_nazwa());
		budzet.setDokPozNrpoz(budgetView.getDok_poz_nrpoz());
		budzet.setDokPozPKoszt(budgetView.getDok_poz_p_kosz());
		budzet.setDokPozRezKoszt(budgetView.getDok_poz_rez_koszt());
		budzet.setDokPozRKoszt(budgetView.getDok_poz_r_koszt());
		budzet.setOkrrozlId(budgetView.getOkrrozl_id());
		budzet.setPozycjaPodrzednaId(budgetView.getPozycja_podrzedna_id());
		budzet.setStrBudNazwa(budgetView.getStr_bud_nazwa());
		budzet.setWspolczynnikPozycji(budgetView.getWspolczynnik_pozycji());
		budzet.setWytworId(budgetView.getWytwor_id());
		budzet.setZadanieNazwa(budgetView.getZadanie_nazwa());
		budzet.setZadaniePKoszt(budgetView.getZadanie_p_koszt());
		budzet.setZadanieRezKoszt(budgetView.getZadanie_rez_koszt());
		budzet.setZadanieRKoszt(budgetView.getZadanie_r_koszt());
		budzet.setZrodloId(budgetView.getZrodlo_id());
		budzet.setZrodloIdn(budgetView.getZrodlo_idn());
		budzet.setZrodloNazwa(budgetView.getZrodlo_nazwa());
		budzet.setZrodloPKoszt(budgetView.getZrodlo_p_koszt());
		budzet.setZrodloRezKoszt(budgetView.getZrodlo_rez_koszt());
		budzet.setZrodloRKoszt(budgetView.getZrodlo_r_koszt());
		return budzet;
	}
	
	/**
	 * Metoda mapuje FinancialTask [WS] na ZadaniaFinansowe [Canonical]
	 * 
	 * @param ft
	 * @return
	 */
	public static ZadaniaFinansowe MapFinancialTaskToZadaniaFinansowe(FinancialTask ft) {
		ZadaniaFinansowe zf = ZadaniaFinansoweDAO.getInstance().getZadaniaFinansowe();

		zf.setAvailable(1);
		zf.setCn(ft.getIdm());
		zf.setErpId(MapperUtils.getLongFromDouble(ft.getId()));
		zf.setIdentyfikator_num(MapperUtils.getDoubleFromString(ft.getIdentyfikator_num()));
		zf.setTitle(ft.getZrodlo_nazwa());
		return zf;
	}
	
	/**
	 * Metoda mapuje Project [WS] na Projekt [Canonical]
	 * 
	 * @param p
	 * @return
	 */
	public static Projekt MapProjectToProjekt(Project p) {
		Projekt projekt = ProjektDAO.getInstance().getProjekt();

		projekt.setAvailable(true);
		projekt.setCn(p.getIdm());
		projekt.setErpId(MapperUtils.getLongFromDouble(p.getId()));
		projekt.setIdentyfikator_num(p.getIdentyfikator_num());
		projekt.setTitle(p.getNazwa());
		projekt.setP_datako(p.getP_datako());
		projekt.setP_datapo(p.getP_datapo());
		projekt.setTyp_kontrakt_id(p.getTyp_kontrakt_id());
		projekt.setTyp_kontrakt_idn(p.getTyp_kontrakt_idn());
		return projekt;
	}
	
	/**
	 * Metoda mapuje ProjectBudgetItem [WS] na ProjektBudzet [Canonical]
	 * 
	 * @param pb
	 * @return
	 */
	public static ProjektBudzetEtap MapProjectBudgetItemToProjektBudzet(ProjectBudgetItem pb) {
		ProjektBudzetEtap projektBudzet = ProjektBudzetEtapDAO.getInstance().getProjektBudzet();

		projektBudzet.setErpId(MapperUtils.getLongFromDouble(pb.getId()));
		projektBudzet.setNazwa(pb.getNazwa());
		projektBudzet.setNrpoz(pb.getNrpoz());
		projektBudzet.setP_datako(pb.getP_datako());
		projektBudzet.setP_datapo(pb.getP_datapo());
		projektBudzet.setParent(pb.getParent());
		projektBudzet.setParent_id(MapperUtils.getLongFromDouble(pb.getParent_id()));
		projektBudzet.setParent_idm(pb.getParent_idm());
		projektBudzet.setAvailable(true);
		
		return projektBudzet;
	}
	
	/**
	 * Metoda mapuje ProjectBudgetItem [WS] na ProjektBudzet [Canonical]
	 * 
	 * @param pb
	 * @return
	 */
	public static ProjektBudzet MapProjectBugdetToProjektBudzet(ProjectBugdet pb) {
		ProjektBudzet projektBudzet = ProjektBudzetDAO.getInstance().getProjektBudzet();
		
		projektBudzet.setErpId(MapperUtils.getLongFromDouble(pb.getId()));
		projektBudzet.setIdm(pb.getIdm());
		projektBudzet.setNazwa(pb.getNazwa());
		projektBudzet.setP_datako(pb.getP_datako());
		projektBudzet.setP_datapo(pb.getP_datapo());;
		projektBudzet.setParent(pb.getParent());
		projektBudzet.setParent_id(MapperUtils.getLongFromDouble(pb.getParent_id()));
		projektBudzet.setParent_idm(pb.getParent_idm());
		projektBudzet.setStan(MapperUtils.getBooleanFromInt(pb.getStan()));
		projektBudzet.setStatus(MapperUtils.getBooleanFromInt(pb.getStatus()));
		projektBudzet.setAvailable(true);
		
		return projektBudzet;
	}

	/**
	 * Metoda mapuje ProjectBudgetItemResource [WS] na ProjektBudzetZasob [Canonical]
	 * 
	 * @param pb
	 * @return
	 */
	public static ProjektBudzetZasob MapProjectBudgetItemResourceToProjektBudzetZasob(ProjectBudgetItemResource pb) {
		ProjektBudzetZasob projektBudzetZasob = ProjektBudzetZasobDAO.getInstance().getProjektBudzetZasob();

		projektBudzetZasob.setCzy_glowny(MapperUtils.getBooleanFromDouble(pb.getCzy_glowny()));
		projektBudzetZasob.setErpId(MapperUtils.getLongFromDouble(pb.getId()));
		projektBudzetZasob.setNazwa(pb.getNazwa());
		projektBudzetZasob.setNrpoz(pb.getNrpoz());
		projektBudzetZasob.setP_cena(pb.getP_cena());
		projektBudzetZasob.setP_ilosc(pb.getP_ilosc());
		projektBudzetZasob.setP_wartosc(pb.getP_wartosc());
		projektBudzetZasob.setParent(pb.getParent());
		projektBudzetZasob.setParent_id(MapperUtils.getLongFromDouble(pb.getParent_id()));
		projektBudzetZasob.setR_ilosc(pb.getR_ilosc());
		projektBudzetZasob.setR_wartosc(pb.getR_wartosc());
		projektBudzetZasob.setRodzaj_zasobu(pb.getRodzaj_zasobu());
		projektBudzetZasob.setZasob_glowny_id(pb.getZasob_glowny_id());
		projektBudzetZasob.setAvailable(true);
		
		return projektBudzetZasob;
	}
	
	/**
	 * Metoda mapuje SourcesOfProjectFunding [WS] na ZrodloFinansowaniaProjektow [Canonical]
	 * 
	 * @param sop
	 * @return
	 */
	public static ZrodloFinansowaniaProjektow MapSourcesOfProjectFundingToZrodloFinansowaniaProjektow(SourcesOfProjectFunding sop) {
		ZrodloFinansowaniaProjektow zrodloFinansowaniaProjektow = ZrodloFinansowaniaProjektowDAO.getInstance().getZrodloFinansowaniaProjektow();

		zrodloFinansowaniaProjektow.setAvailable(MapperUtils.getBooleanFromDouble(sop.getCzy_aktywny()));
		zrodloFinansowaniaProjektow.setErpId(MapperUtils.getLongFromDouble(sop.getId()));
		zrodloFinansowaniaProjektow.setKontrakt_id(sop.getKontrakt_id());
		zrodloFinansowaniaProjektow.setKontrakt_idm(sop.getKontrakt_idm());
		zrodloFinansowaniaProjektow.setRodzaj(sop.getRodzaj());
		zrodloFinansowaniaProjektow.setWsp_proc(sop.getWsp_proc());
		zrodloFinansowaniaProjektow.setZrodlo_id(sop.getZrodlo_id());
		zrodloFinansowaniaProjektow.setZrodlo_identyfikator_num(sop.getZrodlo_identyfikator_num());
		zrodloFinansowaniaProjektow.setZrodlo_idn(sop.getZrodlo_idn());
		zrodloFinansowaniaProjektow.setZrodlo_nazwa(sop.getZrodlo_nazwa());
		return zrodloFinansowaniaProjektow;
	}
	
	/**
	 * Metoda mapuje ProductionOrder [WS] na ZleceniaProdukcyjne [Canonical]
	 * 
	 * @param po
	 * @return
	 */
	public static ZleceniaProdukcyjne MapProductionOrderToZleceniaProdukcyjne(ProductionOrder po) {
		ZleceniaProdukcyjne zleceniaProdukcyjne = ZleceniaProdukcyjneDAO.getInstance().getZleceniaProdukcyjne();

		zleceniaProdukcyjne.setAvailable(true);
		zleceniaProdukcyjne.setErpId(MapperUtils.getLongFromDouble(po.getId()));
		zleceniaProdukcyjne.setCn(po.getIdm());
		zleceniaProdukcyjne.setTitle(po.getNazwa());
		return zleceniaProdukcyjne;
	}

	
	/**
	 * Metoda mapuje PersonAgreement na PracownikWJednostkachOrganizacyjnych
	 * @param personAgreement
	 * @return
	 */
	public static PracownikWJednostkachOrganizacyjnych MapPersonAgreementToPracownikWJednostkachOrganizacyjnych(
			PersonAgreement personAgreement) {
		PracownikWJednostkachOrganizacyjnych pracownikWJednostkachOrganizacyjnych = new PracownikWJednostkachOrganizacyjnych();
		
		Pracownik pracownik = new Pracownik();
		
		Integer idm = new Integer(personAgreement.getPracownik_nrewid());
		pracownik.setZewnetrznyIdm(idm.toString());
		pracownik.setOperatorDoPorownywania(idm.toString());
		pracownikWJednostkachOrganizacyjnych.setPracownik(pracownik);
		
		JednostkaOrganizacyjna jednostkaOrganizacyjna = new JednostkaOrganizacyjna();
		
		jednostkaOrganizacyjna.setIdn(personAgreement.getKomorka_idn());
		pracownikWJednostkachOrganizacyjnych.setJednostkaOrganizacyjna(jednostkaOrganizacyjna);		
		
		return pracownikWJednostkachOrganizacyjnych;
	}

	/**
	 * Metoda mapuje SupplierBankAccount na KontrahentKonto
	 * @param sba
	 */
	public static KontrahentKonto MapSupplierBankAccountToKontrahentKonto(SupplierBankAccount sba) {
		KontrahentKonto kontrahentKonto = KontrahentKontoDAO.getInstance().getKontahentKonto();

		kontrahentKonto.setBank_id(MapperUtils.getLongFromDouble(sba.getBank_id()));
		kontrahentKonto.setBank_idn(sba.getBank_idn());
		kontrahentKonto.setBank_nazwa(sba.getBank_nazwa());
		kontrahentKonto.setCzyIban(MapperUtils.getBooleanFromDouble(sba.getCzy_iban()));
		kontrahentKonto.setDostawca_id(MapperUtils.getLongFromDouble(sba.getDostawca_id()));
		kontrahentKonto.setDostawca_idn(sba.getDostawca_idn());
		kontrahentKonto.setErpId(MapperUtils.getLongFromDouble(sba.getId()));
		kontrahentKonto.setIdm(sba.getIdm());
		kontrahentKonto.setKolejnosc_uzycia(sba.getKolejnosc_uzycia());
		kontrahentKonto.setNazwa(sba.getNazwa());
		kontrahentKonto.setNumer_konta(sba.getNumer_konta());
		kontrahentKonto.setSymbol_waluty(sba.getSymbolwaluty());
		kontrahentKonto.setTypKurs_id(MapperUtils.getLongFromDouble(sba.getTypkurs_id()));
		kontrahentKonto.setTypKurs_idn(sba.getTypkurs_idn());
		kontrahentKonto.setWaluta_id(MapperUtils.getLongFromDouble(sba.getWaluta_id()));
		kontrahentKonto.setZrodKursWal_id(MapperUtils.getLongFromDouble(sba.getZrodkurswal_id()));
		kontrahentKonto.setZrodKursWal_idn(sba.getZrodkurswal_idn());
		kontrahentKonto.setZrodKursWal_nazwa(sba.getZrodkurswal_nazwa());

		return kontrahentKonto;
	}
	/**
	* Metoda mapuje ApplicationForReservationType na TypyWnioskowORezerwacje
	* @param app
 	*/
	public static TypyWnioskowORezerwacje MapApplicationForReservationTypeToTypyWnioskowORezerwacje(ApplicationForReservationType app) {
		TypyWnioskowORezerwacje typy = TypyWnioskowORezerwacjeDAO.getInstance().getTypyWnioskowORezerwacje();
		
		typy.setCzy_aktywny(MapperUtils.getBooleanFromDouble(app.getCzy_aktywny()));
		typy.setCzy_kontrola_ceny(MapperUtils.getBooleanFromDouble(app.getCzy_kontrola_ceny()));
		typy.setCzy_kontrola_ilosci(MapperUtils.getBooleanFromDouble(app.getCzy_kontrola_ilosci()));
		typy.setCzy_kontrola_wartosci(MapperUtils.getBooleanFromDouble(app.getCzy_kontrola_wartosci()));
		typy.setCzy_obsluga_pzp(MapperUtils.getBooleanFromDouble(app.getCzy_obsluga_pzp()));
		typy.setErpId(MapperUtils.getLongFromDouble(app.getId()));
		typy.setNazwa(app.getNazwa());
		typy.setIdm(app.getIdm());
		
		return typy;
	}
	
	/**
	 * Metoda mapuje UnitBudget na OkresBudzetowy
	 * @param unit
	 * @return
	 */
	public static OkresBudzetowy mapUnitBudgetToOkresBudzetowy(UnitBudget unit) {
		OkresBudzetowy budKo = OkresBudzetowyDAO.getInstance().getOkresBudzetowy();
	
		budKo.setCzy_archiwalny(MapperUtils.getBooleanFromDouble(unit.getCzy_archiwalny()));
		budKo.setData_obow_do(unit.getData_obow_do());
		budKo.setData_obow_od(unit.getData_obow_od());
		budKo.setErpId(MapperUtils.getLongFromDouble(unit.getId()));
		budKo.setIdm(unit.getIdm());
		budKo.setNazwa(unit.getNazwa());
		budKo.setWersja(unit.getWersja());
		budKo.setAvailable(true);
		
		return budKo;
	}
	
	/**
	 * Metoda mapuje PurchasePlan na PlanZakupow
	 * @param pp
	 * @return
	 */
	public static PlanZakupow MapPurchasePlanToPlanZakupow(PurchasePlan pp) {
		PlanZakupow pz = PlanZakupowDAO.getInstance().getPlanZakupow();
		
		pz.setBudzet_id(MapperUtils.getLongFromDouble(pp.getBd_str_budzet_id()));
		pz.setBudzet_idn(pp.getBd_str_budzet_idn());
		pz.setOkres_id(MapperUtils.getLongFromDouble(pp.getBudzet_id()));
		pz.setOkres_idm(pp.getBudzet_idm());
		pz.setCzyAgregat(MapperUtils.getBooleanFromInt(pp.getCzy_agregat()));
		pz.setErpId(MapperUtils.getLongFromDouble(pp.getId()));
		pz.setIdm(pp.getIdm());
		pz.setNazwa(pp.getNazwa());
		pz.setAvailable(true);
		
		return pz;
	}
	
	/**
	 * Metoda mapuje obiekt PurchasePlanKind na RodzajePlanowZakupu
	 * @param ppk
	 * @return
	 */
	public static RodzajePlanowZakupu MapPurchasePlanKindToRodzajePlanowZakupu(PurchasePlanKind ppk) {
		RodzajePlanowZakupu ppz = RodzajePlanowZakupuDAO.getInstance().getRodzajePlanowZakupu();
		
		ppz.setCena(ppk.getCena());
		ppz.setCzy_aktywna(MapperUtils.getBooleanFromDouble(ppk.getCzy_aktywna()));
		ppz.setErpId(MapperUtils.getLongFromDouble(ppk.getId()));
		ppz.setKomorkaRealizujaca(MapperUtils.getLongFromDouble(ppk.getKomorka_realizujaca()));
		ppz.setNazwa(ppk.getNazwa());
		ppz.setNrpoz(ppk.getNrpoz());
		ppz.setOpis(ppk.getOpis());
		ppz.setP_ilosc(ppk.getP_ilosc());
		ppz.setP_koszt(ppk.getP_koszt());
		ppz.setParent(ppk.getParent());
		ppz.setParentId(ppz.getParentId());
		ppz.setParentIdm(ppz.getParentIdm());
		ppz.setR_ilosc(ppk.getR_ilosc());
		ppz.setR_koszt(ppk.getR_koszt());
		ppz.setRez_ilosc(ppk.getRez_ilosc());
		ppz.setRez_koszt(ppk.getRez_koszt());
		ppz.setWytwor_id(MapperUtils.getLongFromDouble(ppk.getWytwor_id()));
		ppz.setWytwor_idm(ppk.getWytwor_idm());
		return ppz;
	}

	public static BudzetKOKomorek MapUnitBudgetKoToBudzetKOKomorek(
			UnitBudgetKo unit) {
		BudzetKOKomorek budKo = BudzetKOKomorekDAO.getInstance().getBudzetKOKomorek();
		
		budKo.setIdm(unit.getIdm());
		budKo.setNazwa(unit.getNazwa());
		budKo.setBudzetId(MapperUtils.getLongFromDouble(unit.getBd_str_budzet_id()));
		budKo.setBudzetIdn(unit.getBd_str_budzet_idn());
		budKo.setOkresId(MapperUtils.getLongFromDouble(unit.getBudzet_id()));
		budKo.setOkresIdm(unit.getBudzet_idm());
		budKo.setErpId(MapperUtils.getLongFromDouble(unit.getId()));
		budKo.setCzyAgregat(MapperUtils.getBooleanFromInt(unit.getCzy_agregat()));
		budKo.setAvailable(true);
		
		return budKo;
	}
	/**
	 * Metoda mapuje obiekt RolesInProjects na RoleWProjekcie
	 * @param r
	 */
	public static RoleWProjekcie MapRolesInProjectsToRoleWProjekcie(RolesInProjects r) {
		RoleWProjekcie role = RoleWProjekcieDAO.getInstance().getRoleWProjekcie();
		
		role.setBp_rola_id(r.getBp_rola_id());
		role.setBp_rola_idn(r.getBp_rola_idn());
		role.setBp_rola_kontrakt_id(r.getBp_rola_kontrakt_id());
		role.setCzy_blokada_prac(MapperUtils.getBooleanFromDouble(r.getCzy_blokada_prac()));
		role.setCzy_blokada_roli(MapperUtils.getBooleanFromDouble(r.getCzy_blokada_roli()));
		role.setKontrakt_id(r.getKontrakt_id());
		role.setKontrakt_idm(r.getKontrakt_idm());
		role.setOsobaGuid(r.getOsoba_guid());
		role.setPracownik_id(r.getPracownik_id());
		role.setPracownik_idn(r.getPracownik_idn());
		role.setPracownik_nrewid(r.getPracownik_nrewid());
		role.setOsobaId(MapperUtils.getLongFromDouble(r.getOsoba_id()));
		role.setAvailable(true);
		
		return role;
	}
	/**
	 * Metoda mapuje obiekt RolesInUnitBudgetKo na RoleWBudzecie
	 * @param r
	 * @return
	 */
	public static RoleWBudzecie MapRolesInUnitBudgetKoToRoleWBudzecie(RolesInUnitBudgetKo r){
		RoleWBudzecie role = RoleWBudzecieDAO.getInstance().getRoleWBudzecie();
		
		role.setBd_budzet_ko_id(r.getBd_budzet_ko_id());
		role.setBd_budzet_ko_idm(r.getBd_budzet_ko_idm());
		role.setBp_rola_id(r.getBd_rola_id());
		role.setBp_rola_idn(r.getBd_rola_idn());
		role.setBudzet_id(r.getBudzet_id());
		role.setNazwa(r.getNazwa());
		role.setOsoba_id(MapperUtils.getLongFromDouble(r.getOsoba_id()));
		role.setOsobaGuid(r.getOsoba_guid());
		role.setPracownik_id(r.getPracownik_id());
		role.setPracownik_nrewid(r.getPracownik_nrewid());
		role.setAvailable(true);
		
		return role;
	}

	public static BudzetPozycjaSimple MapUnitBudgetKoKindToBudzetPozycjaSimple(
			UnitBudgetKoKind sba) {
		
		BudzetPozycjaSimple bud = BudzetPozycjaSimpleDAO.getInstance().getBudzetPozycjaSimple();
		
		bud.setBd_nad_rodzaj_ko_id(MapperUtils.getLongFromDouble(sba.getBd_nad_rodzaj_ko_id()));
		bud.setCzy_aktywna(MapperUtils.getBooleanFromDouble(sba.getCzy_aktywna()));
		bud.setErpId(MapperUtils.getLongFromDouble(sba.getId()));
		bud.setNazwa(sba.getNazwa());
		bud.setNrpoz(MapperUtils.getLongFromString(Integer.toString(sba.getNrpoz())));
		bud.setOpis(sba.getOpis());
		bud.setP_koszt(MapperUtils.getMoneyfromDouble(sba.getP_koszt()));
		bud.setParent(sba.getParent());
		bud.setParentId(MapperUtils.getLongFromDouble(sba.getParent_id()));
		bud.setParentIdm(sba.getParent_idm());
		bud.setR_koszt(MapperUtils.getMoneyfromDouble(sba.getR_koszt()));
		bud.setRez_koszt(MapperUtils.getMoneyfromDouble(sba.getRez_koszt()));
		bud.setWytworId(MapperUtils.getLongFromDouble(sba.getWytwor_id()));
		bud.setWytworIdm(sba.getWytwor_idm());
		bud.setAvailable(true);
		
		return bud;
	}

	public static BudzetDoKomorki MapBudgetUnitOrganizationalUnitToBudzetDoKomorki(
			BudgetUnitOrganizationUnit sba) {
		BudzetDoKomorki bud=BudzetDoKomorkiDAO.getInstance().getBudzetDoKomorki();
		
		bud.setBudzet_id(MapperUtils.getLongFromDouble(sba.getBd_str_budzet_id()));
		bud.setBudzet_idn(sba.getBd_str_budzet_nad_idn());
		bud.setBudzet_nad_id(MapperUtils.getLongFromDouble(sba.getBd_str_budzet_nad_id()));
		bud.setBudzet_nad_idn(sba.getBd_str_budzet_nad_idn());
		bud.setOkresId(MapperUtils.getLongFromDouble(sba.getBudzet_id()));
		bud.setCzyWiodaca(MapperUtils.getBooleanFromDouble(sba.getCzy_wiodaca()));
		bud.setKomorkaId(MapperUtils.getLongFromDouble(sba.getKomorka_id()));
		bud.setKomorkaIdn(sba.getKomorka_idn());
		bud.setNazwaKomorkiBudzetowej(sba.getNazwa_komorki_budzetowej());
		bud.setNazwaKomorkiOrg(sba.getNazwa_komorki_organizacyjnej());
		
		return bud;
	}

	public static SzablonyRozliczenFaktur MapPatternsOfCostSharingToSzablonyRozliczenFaktur(
			PatternsOfCostSharing sba) {
		SzablonyRozliczenFaktur szab= SzablonyRozliczenFakturDAO.getInstance().getSzablonyRozliczenFaktur();
		
		szab.setErpId(MapperUtils.getLongFromDouble(sba.getId()));
		szab.setIdm(sba.getIdm());
		szab.setNazwa(sba.getNazwa());
		szab.setOkresId(MapperUtils.getLongFromDouble(sba.getOkrrozl_id()));
		szab.setOkresIdm(MapperUtils.getStringFromInt(sba.getOkres_rozl()));
		
		return szab;
	}

	public static ProjectAccountPiatki MapProjectAccountPiatkiToProjectAccountPiatki(
			ProjectAccount sba) {
		ProjectAccountPiatki piat= ProjectAccountPiatkiDAO.getInstance().getProjectAccountPiatki();
		
		piat.setKonto_ido(sba.getKonto_ido());
		piat.setKontrakt_id(MapperUtils.getLongFromDouble(sba.getKontrakt_id()));
		piat.setKontrakt_idm(sba.getKontrakt_idm());
		piat.setRep_atrybut_ido(sba.getRep_atrybut_ido());
		piat.setAvailable(true);
		return piat;
	}

	public static NumeryZamowien MapSupplierOrderHeaderToNumeryZamowien(
			SupplierOrderHeader soh) {
		NumeryZamowien nz=NumeryZamowienDAO.getInstance().getNumeryZamowien();
		
		nz.setDat_dok(soh.getDatdok());
		nz.setDat_odb(soh.getDatodb());
		nz.setErpId(MapperUtils.getLongFromDouble(soh.getId()));
		nz.setIdm(soh.getIdm());
		nz.setDostawca_id(MapperUtils.getLongFromDouble(soh.getDostawca_id()));
		nz.setDostawca_idn(soh.getDostawca_idn());
		nz.setOpis_wewn(soh.getOpis_wewn());
		nz.setUwagi(soh.getUwagi());
		nz.setWartdok(MapperUtils.getMoneyfromDouble(soh.getWartdok()));
		return nz;
		
	}
	/**
	 * Metoda mapuje obiekt GetPerson na Pracownikow
	 */
	public static Pracownik MapPersontoPracownik(Person p){
		Pracownik pp = PracownikDAO.getInstance().getPracownik();
		
		pp.setGuid(p.getGuid());
		pp.setImie(p.getImie());
		pp.setNazwisko(p.getNazwisko());
		pp.setNrewid(p.getPracownik_nrewid());
		pp.setPesel(p.getPesel());
		pp.setZewnetrznyId(MapperUtils.getLongFromDouble(p.getId()));
		pp.setZewnetrznyIdm(p.getIdm());
		pp.setCzyAktywna(MapperUtils.getBooleanFromDouble(p.getCzy_aktywny_osoba()));
		
		return pp;
	}
	
	/**
	 * Metoda mapuje obiekt AbsenceType na TypyUrlopow
	 * @param at
	 * @return
	 */
	public static TypyUrlopow MapAbsenceTypeToTypyUrlopow(AbsenceType at){
		TypyUrlopow tu = TypyUrlopowDAO.getInstance().getTypyUrlopow();
		
		tu.setErpId(MapperUtils.getLongFromDouble(at.getID()));
		tu.setIdm(at.getIDM());
		tu.setOpis(at.getOpis());
		tu.setOpis_dlugi(at.getOpis_dlugi());
		
		return tu;
	}
	
	/**
	 * 
	 * Metoda mapuje obiekt RequestForReservation na WniosekORezerwacje
	 * @param req
	 * @return
	 */
	public static WniosekORezerwacje MapRequestForReservationToWniosekORezerwacje(RequestForReservation req){
		WniosekORezerwacje wn=WniosekORezerwacjeDAO.getInstance().getWniosekORezerwacje();
	    
			wn.setBdBudzetKoId(MapperUtils.getLongFromDouble(req.getBd_budzet_ko_id()));
	    	wn.setBdBudzetKoIdm(req.getBd_budzet_ko_idm());
	    	wn.setBdPlanZamId(MapperUtils.getLongFromDouble(req.getBd_plan_zam_id()));
	    	wn.setBdPlanZamIdm(req.getBd_plan_zam_idm());
	    	wn.setBdRezerwacjaId(MapperUtils.getLongFromDouble(req.getBd_rezerwacja_id()));
	    	wn.setBdRezerwacjaIdm(req.getBd_rezerwacja_idm());
	    	wn.setBdRezerwacjaPozId(MapperUtils.getLongFromDouble(req.getBd_rezerwacja_poz_id()));
	    	wn.setBdRodzajKoId(MapperUtils.getLongFromDouble(req.getBd_rodzaj_ko_id()));
	    	wn.setBdRodzajPlanZamId(MapperUtils.getLongFromDouble(req.getBd_rodzaj_plan_zam_id()));
	    	wn.setBdStrBudzetId(MapperUtils.getLongFromDouble(req.getBd_str_budzet_id()));
	    	wn.setBdTypRezerwacjaIdn(req.getBd_typ_rezerwacja_idn());
	    	wn.setBkBdOkrSzablonRelId(MapperUtils.getLongFromDouble(req.getBk_bd_okr_szablon_rel_id()));
	    	wn.setBkBdSzablonPozId(MapperUtils.getLongFromDouble(req.getBk_bd_szablon_poz_id()));
	    	/* nie ma tego na tescie
	    	setBkCzyArchiwalny(item.getBk_czy_archiwalny());*/
	    	wn.setBkNazwa(req.getBk_nazwa());
	    	wn.setBkPozNazwa(req.getBk_poz_nazwa());
	    	wn.setBkPozNrpoz(req.getBk_poz_nrpoz());
	    	wn.setBkPozPKoszt(MapperUtils.getMoneyfromDouble(req.getBk_poz_p_koszt()));
	    	wn.setBkPozPKosztZrodla(MapperUtils.getMoneyfromDouble(req.getBk_poz_p_koszt_zrodla()));
	    	wn.setBkPozRKoszt(MapperUtils.getMoneyfromDouble(req.getBk_poz_r_koszt()));
	    	wn.setBkPozRKosztZrodla(MapperUtils.getMoneyfromDouble(req.getBk_poz_r_koszt_zrodla()));
	    	wn.setBkPozRezKoszt(MapperUtils.getMoneyfromDouble(req.getBk_poz_rez_koszt()));
	    	wn.setBkPozRezKosztZrodla(MapperUtils.getMoneyfromDouble(req.getBk_poz_rez_koszt_zrodla()));
	    	/* nie ma tego na tescie
	    	setBkStatus(item.getBk_status());*/
	    	wn.setBkWytworId(MapperUtils.getLongFromDouble(req.getBk_wytwor_id()));
	    	wn.setBudzetId(MapperUtils.getLongFromDouble(req.getBudzet_id()));
	    	wn.setBudzetIdm(req.getBudzet_idm());
	    	wn.setCena(MapperUtils.getMoneyfromDouble(req.getCena()));
	    	wn.setCzyObslugaPzp(MapperUtils.getBooleanFromDouble(req.getCzy_obsluga_pzp()));
	    	wn.setIlosc(MapperUtils.getMoneyfromDouble(req.getIlosc()));
	    	wn.setKoszt(MapperUtils.getMoneyfromDouble(req.getKoszt()));
	    	wn.setMagazynId(MapperUtils.getLongFromDouble(req.getMagazyn_id()));
	    	wn.setNazwa(req.getNazwa());
	    	wn.setNrpoz(req.getNrpoz());
	    	wn.setOkrrozlId(MapperUtils.getLongFromDouble(req.getOkrrozl_id()));
	    	wn.setPzpBdOkrSzablonRelId(MapperUtils.getLongFromDouble(req.getPzp_bd_okr_szablon_rel_id()));
	    	wn.setPzpBdSzablonPozId(MapperUtils.getLongFromDouble(req.getPzp_bd_szablon_poz_id()));
	    	/* nie ma tego na tescie
	    	setPzpCzyArchiwalny(item.getPzp_czy_archiwalny());*/
	    	wn.setPzpJmId(MapperUtils.getLongFromDouble(req.getPzp_jm_id()));
	    	wn.setPzpJmIdn(req.getPzp_jm_idn());
	    	wn.setPzpNazwa(req.getPzp_nazwa());
	    	wn.setPzpPozNazwa(req.getPzp_poz_nazwa());
	    	wn.setPzpPozNrpoz(req.getPzp_poz_nrpoz());
	    	wn.setPzpPozPIlosc(MapperUtils.getMoneyfromDouble(req.getPzp_poz_p_ilosc()));
	    	wn.setPzpPozPKoszt(MapperUtils.getMoneyfromDouble(req.getPzp_poz_p_koszt()));
	    	wn.setPzpPozRIlosc(MapperUtils.getMoneyfromDouble(req.getPzp_poz_r_ilosc()));
	    	wn.setPzpPozRKoszt(MapperUtils.getMoneyfromDouble(req.getPzp_poz_r_koszt()));
	    	wn.setPzpPozRezIlosc(MapperUtils.getMoneyfromDouble(req.getPzp_poz_rez_ilosc()));
	    	wn.setPzpPozRezKoszt(MapperUtils.getMoneyfromDouble(req.getPzp_poz_rez_koszt()));
	    	/* nie ma tego na tescie
	    	setPzpStatus(item.getPzp_status());*/
	    	wn.setPzpWytworId(MapperUtils.getLongFromDouble(req.getPzp_wytwor_id()));
	    	wn.setPzpWytworIdm(req.getPzp_wytwor_idm());		
	    	wn.setRezJmId(MapperUtils.getLongFromDouble(req.getRez_jm_id()));
	    	wn.setRezJmIdn(req.getRez_jm_idn());
	    	wn.setRezerwacjaCzyArchiwalny(MapperUtils.getBooleanFromDouble(req.getRezerwacja_czy_archiwalny()));
	    	wn.setRezerwacjaStatus(req.getRezerwacja_status());
	    	wn.setVatstawId(MapperUtils.getLongFromDouble(req.getVatstaw_id()));
	    	wn.setWytworEditIdm(req.getWytwor_edit_idm());
	    	wn.setWytworEditNazwa(req.getWytwor_edit_nazwa());
	    	wn.setWytworId(MapperUtils.getLongFromDouble(req.getWytwor_id()));
	    	wn.setZadanieId(MapperUtils.getLongFromDouble(req.getZadanie_id()));			
	    	wn.setZadanieIdn(req.getZadanie_idn());		
	    	wn.setZadanieNazwa(req.getZadanie_nazwa());	
	    	wn.setZrodloId(MapperUtils.getLongFromDouble(req.getZrodlo_id()));
	    	wn.setZrodloIdn(req.getZrodlo_idn());			
	    	wn.setZrodloNazwa(req.getZrodlo_nazwa());		

		return wn;
	}
	
	public static Zrodlo MapFundingSourceToZrodlo(FundingSource fs){
		Zrodlo z = ZrodloDAO.getInstance().getZrodlo();
		
		z.setErpId(MapperUtils.getLongFromDouble(fs.getId()));
		z.setIdentyfikator_num(fs.getIdentyfikator_num());
		z.setIdm(fs.getIdm());
		z.setZrodlo_nazwa(fs.getZrodlo_nazwa());
	
		return z;
	}
	
}
