package pl.compan.docusafe.general.mapers.utils.strings;

/**
 * Klasa zawiera metody modyfikuj�ce stringi
 * @author Gubari Nanokata
 *
 */
public class PolishStringUtils {
	
	/**
	 * Metoda zamienia polskie znaki na ich odpowiedniki niepolskie
	 * @param tekst
	 * @return
	 */
	public String zamienPolskieZnaki(String tekst) {
		
		// �
		tekst = tekst.replace("�", "a");
		// �
		tekst = tekst.replace("�", "c");
		// �
		tekst = tekst.replace("�", "e");
		// �
		tekst = tekst.replace("�", "l");
		// �
		tekst = tekst.replace("�", "n");
		// �
		tekst = tekst.replace("�", "o");
		// �
		tekst = tekst.replace("�", "s");
		// �
		tekst = tekst.replace("�", "z");
		// �
		tekst = tekst.replace("�", "z");
		// �
		tekst = tekst.replace("�", "A");
		// �
		tekst = tekst.replace("�", "C");
		// �
		tekst = tekst.replace("�", "E");
		// �
		tekst = tekst.replace("�", "L");
		// �
		tekst = tekst.replace("�", "N");
		// �
		tekst = tekst.replace("�", "O");
		// �
		tekst = tekst.replace("�", "S");
		// �
		tekst = tekst.replace("�", "Z");
		// �
		tekst = tekst.replace("�", "Z");
				
		return tekst;
	}
	
	/**
	 * Metoda zamienia kody UTF-8 polskich znak�w na ich odpowiedniki niepolskie
	 * @param tekst
	 * @return
	 */
	public String zamienKodyUTF8PolskichZnakow(String tekst) {
		
		// �
		tekst = tekst.replace("\u0105", "a");
		// �
		tekst = tekst.replace("\u0107", "c");
		// �
		tekst = tekst.replace("\u0119", "e");
		// �
		tekst = tekst.replace("\u0142", "l");
		// �
		tekst = tekst.replace("\u0144", "n");
		// �
		tekst = tekst.replace("\u00F3", "o");
		// �
		tekst = tekst.replace("\u015B", "s");
		// �
		tekst = tekst.replace("\u017A", "z");
		// �
		tekst = tekst.replace("\u017C", "z");
		// �
		tekst = tekst.replace("\u0104", "A");
		// �
		tekst = tekst.replace("\u0106", "C");
		// �
		tekst = tekst.replace("\u0118", "E");
		// �
		tekst = tekst.replace("\u0141", "L");
		// �
		tekst = tekst.replace("\u0143", "N");
		// �
		tekst = tekst.replace("\u00D3", "O");
		// �
		tekst = tekst.replace("\u015A", "S");
		// �
		tekst = tekst.replace("\u0179", "Z");
		// �
		tekst = tekst.replace("\u017B", "Z");
				
		return tekst;
	}

}
