package pl.compan.docusafe.general.mapers;

import java.math.BigDecimal;

public class MapperUtils {

	/**
	 * Metoda parsuje warto�� double do boolean
	 * @param d
	 * @return
	 */
	public static boolean getBooleanFromDouble(double d)
	{
		Double dObj = new Double(d);
		Boolean b = Boolean.parseBoolean(getStringFilter(dObj.toString()));
		return b.booleanValue();
	}
	
	/**
	 * Metoda parsuje warto�� int do booolean
	 * @param i
	 * @return
	 */
	public static boolean getBooleanFromInt(int i)
	{
		Integer iObj = new Integer(i);
		Boolean b = Boolean.parseBoolean(getStringFilter(iObj.toString()));
		return b.booleanValue();
	}
	
	/**
	 * Metoda parsuje warto�� double do String
	 * @param d
	 * @return
	 */
	public static String getStringFromDouble(double d)
	{
		Double dObj = new Double(d);		
		return dObj.toString();
	}
	
	/**
	 * Metoda parsuje warto�� int do String
	 * @param i
	 * @return
	 */
	public static String getStringFromInt(int i)
	{
		Integer dObj = new Integer(i);		
		return dObj.toString();
	}
	
	
	/**
	 * Metoda zamienia typ double do long
	 * @param d
	 * @return
	 */
	public static long getLongFromDouble(double d)
	{
		Double dObj = new Double(d);
		Long l = (new Double(dObj)).longValue();
		return l;
	}
	
	/**
	 * Metoda zabmienia najcz�ciej wymagane transformacje zwracane przez s�owniki 
	 * @param d
	 * @return
	 */
	private static String getStringFilter(String d)
	{
		d = d.replace("0.0", "false");
		d = d.replace("1.0", "true");
		d = d.replace("0", "false");		
		d = d.replace("1", "true");
		return d;
	}
	
	public static int getIntFromBoolean(boolean b)
	{
		Boolean bObj = new Boolean(b);
		int i;
		if(bObj){
			i=1;
		} else {
			i=0;
		}
		return i;
	}
	
	public static Long getLongFromString(String s)
	{
		String sObj = new String(s);		
		return Long.valueOf(s);
	}
	
	public static Double getDoubleFromString(String s)
	{
		String sObj = new String(s);		
		return Double.valueOf(s);
	}
	
	public static BigDecimal getMoneyfromDouble(Double d){
		BigDecimal bd=new BigDecimal(d);
		bd.setScale(4, BigDecimal.ROUND_HALF_DOWN);
		return bd;
	}
	
}
