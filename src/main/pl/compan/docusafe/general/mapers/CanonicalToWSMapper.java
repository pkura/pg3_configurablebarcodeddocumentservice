package pl.compan.docusafe.general.mapers;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.general.canonical.model.Faktura;
import pl.compan.docusafe.general.canonical.model.GeneralSimpleRepositoryAttribute;
import pl.compan.docusafe.general.canonical.model.Kontrahent;
import pl.compan.docusafe.general.canonical.model.PozycjaFaktury;
import pl.compan.docusafe.general.canonical.model.WniosekORezerwacje;
import pl.compan.docusafe.general.hibernate.dao.FakturaPozycjaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.PozycjaZamowieniaFakturaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.ZamowienieSimplePozycjeDBDAO;
import pl.compan.docusafe.general.hibernate.model.CostInvoiceProductDB;
import pl.compan.docusafe.general.hibernate.model.PozycjaZamowieniaFakturaDB;
import pl.compan.docusafe.general.hibernate.model.UnitOfMeasureDB;
import pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjeDB;
import pl.compan.docusafe.general.hibernate.model.ZamowienieSimplePozycjeDB;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.simple.general.dokzak.DokzakTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.DokzakpozTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.DokzakpozZfTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.DokzakpozZfiTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.DokzakpozycjeTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.DostfaktTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.FkDkrSzablonyKosztowTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.IDENTITYNATURETYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.IDNIDTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.IDSIDTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.ObjectFactory;
import pl.compan.docusafe.ws.simple.general.dokzak.ObjectTypeManager;
import pl.compan.docusafe.ws.simple.general.dokzak.RepWartoscTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.RepWartosciTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.SYMBOLWALUTYIDTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.SysDokZalacznikTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.SysDokZalacznikiTYPE;

public class CanonicalToWSMapper {

	private final static Logger log = LoggerFactory.getLogger(CanonicalToWSMapper.class);
	private static final String TABLENAME_KRAJE = "dsr_country";
	
	public static DokzakTYPE MapFakturaToDokzakTYPE(Faktura faktura){
		ObjectFactory factory = new ObjectFactory();
        DokzakTYPE dokzakTYPE = factory.createDokzakTYPE();

        //Pola obowiazkowe  -  START
        
        //Typ platnosci
        IDNIDTYPE warplatId = ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.T, null, faktura.getTypPlatnosci().getCn().trim());
        dokzakTYPE.getContent().add(factory.createDokzakTYPEWarplatId(warplatId));
        
        //Stanowisko dostaw
        //Trzeba wysylac idki bo dla idnow przechodza niektore np. BIURO REKTORA
        IDNIDTYPE stanowiskoDostawId = ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, new BigDecimal(Double.parseDouble(faktura.getStanowiskoDostaw().getCn())), null);
        dokzakTYPE.getContent().add(factory.createDokzakTYPEStdostId(stanowiskoDostawId));
        //zamowienie
        if(faktura.getCzyZZamowienia()!=null&&faktura.getCzyZZamowienia()&&faktura.getZamowienie()!=null){
        	dokzakTYPE.getContent().add(factory.createDokzakTYPEZamdostId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(faktura.getZamowienie().getErpId()))));
        }
        //atrybutvat_id
        //narazie nie wysylamy bo nie mamy slownika i dlatego nie przechodzi RACHUNEK
        //dokzakTYPE.getContent().add(factory.createDokzakTYPEAtrybutvatId(ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, new BigDecimal(1), null)));
        
        //Uzytkownik
        String uzytkownikErpIdn = Docusafe.getAdditionProperty("dokzak.erp_uzytkownik.name");
        IDNIDTYPE uzytkId = ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.T,null,uzytkownikErpIdn);
        dokzakTYPE.getContent().add(factory.createDokzakTYPEUzytkId(uzytkId));
        
        //Waluta
        SYMBOLWALUTYIDTYPE symbolWaluty = factory.createSYMBOLWALUTYIDTYPE();
        symbolWaluty.setSymbolwaluty(faktura.getWaluta().getCn());
        symbolWaluty.setType(IDENTITYNATURETYPE.T);
        dokzakTYPE.getContent().add(factory.createDokzakTYPEWalutaId(symbolWaluty));
        
        //Typ dokumentu zakupu
        IDNIDTYPE typDokumentuId = ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.T, null, faktura.getTypFaktury().getTypdokIdn());
        dokzakTYPE.getContent().add(factory.createDokzakTYPETypdokzakId(typDokumentuId));
        
        //Dostawca faktury
        dokzakTYPE.getContent().add(factory.createDokzakTYPEDostfaktId(CanonicalToWSMapper.dodajKontrahenta(faktura, factory)));
         
        
        //Numer konta bankowego
        if(faktura.getWystawcaFaktury().getNrKonta() != null)
            dokzakTYPE.getContent().add(factory.createDokzakTYPEKontobankoweId(ObjectTypeManager.createIDMIDTYPE(factory, faktura.getWystawcaFaktury().getNrKonta())));
        
        //Data wystawienia faktury
        dokzakTYPE.getContent().add(factory.createDokzakTYPEDatdok(faktura.getDataWystawienia()));

        //Data operacji - termin platnosci
        dokzakTYPE.getContent().add(factory.createDokzakTYPEDatoper(faktura.getDataWplywu()));

        dokzakTYPE.getContent().add(factory.createDokzakTYPEKurs(faktura.getKurs()));
        
        dokzakTYPE.getContent().add(factory.createDokzakTYPEWartdok(faktura.getKwotaNetto()));
        dokzakTYPE.getContent().add(factory.createDokzakTYPEWartwalbaz(faktura.getKwotaWWalucie()));
        
        //Uwagi
        dokzakTYPE.getContent().add(factory.createDokzakTYPEDokUwagi(CanonicalToWSMapper.createUwagi(faktura)));
        
        dokzakTYPE.getContent().add(factory.createDokzakTYPEDatodb(faktura.getDataWplywu()));
        
        //Opakowanie opis(podajemy nasz kod kreskowy)
        dokzakTYPE.getContent().add(factory.createDokzakTYPEOpisopak(faktura.getKodKreskowy()));
        //Numer faktury
        dokzakTYPE.getContent().add(factory.createDokzakTYPEDokobcy(faktura.getNumerFaktury()));
        
        //Data wplywu
        dokzakTYPE.getContent().add(factory.createDokzakTYPEDatprzyj(faktura.getDataWplywu()));
        dokzakTYPE.getContent().add(factory.createDokzakTYPEDatplatn(faktura.getTerminPlatnosci()));
        
        //Pola obowiazkowe  -  END
        		
        //Pola nieobowiazkowe  -  STAR
        
        dokzakTYPE.getContent().add(factory.createDokzakTYPEKwvat(faktura.getKwotaVat()));
        dokzakTYPE.getContent().add(factory.createDokzakTYPEKwotvatwalbaz(faktura.getKwotaVat()));
        
        //Pozycje faktury
        dokzakTYPE.getContent().add(factory.createDokzakTYPEDokzakpozycje(CanonicalToWSMapper.createPozycjeFaktury(faktura, factory)));

        //Zalaczniki
        if(faktura.getZalacznik() != null)
        	dokzakTYPE.getContent().add(factory.createDokzakTYPESysDokZalaczniki(CanonicalToWSMapper.createZalaczniki(faktura, factory)));


        //Pola nieobowiazkowe  -  END
        
        return dokzakTYPE;
	}
	
	private static RepWartosciTYPE createRepWartosciDlaPozycji(Faktura faktura, PozycjaFaktury pozycja, ObjectFactory factory){
		RepWartosciTYPE repWartosciTYPE = factory.createRepWartosciTYPE();
		
		try {
			List<GeneralSimpleRepositoryAttribute> listaSlownikow = GeneralSimpleRepositoryAttribute.find(faktura.getTypFaktury().getTypdokzak_id().intValue(), 6);
		
			ResultSet resultSet = FakturaPozycjaDBDAO.findValuesById(pozycja.getIdPozycji().longValue());
			
			for(GeneralSimpleRepositoryAttribute slownik: listaSlownikow){
				RepWartoscTYPE repWartoscTYPE = factory.createRepWartoscTYPE();
				repWartoscTYPE.setRepAtrybutId(new BigDecimal(slownik.getRepAtrybutId()));
				if(resultSet.getLong(slownik.getTableName().substring(4).toUpperCase())!=0){//0 - pozycja jest niedostepna dla danego funduszu, ustawiane na logice
					Integer erpId = FakturaPozycjaDBDAO.findRepoValues(resultSet.getLong(slownik.getTableName().substring(4).toUpperCase()), slownik.getTableName().substring(4)).getInt("centrum");
				
					repWartoscTYPE.setWartoscInstancjaId(new BigDecimal(erpId));
				
					repWartosciTYPE.getRepWartosc().add(repWartoscTYPE);
				}
			}
		
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		
		return repWartosciTYPE;
	}

	private static DokzakpozycjeTYPE createPozycjeFaktury(Faktura faktura, ObjectFactory factory){
		DokzakpozycjeTYPE dokzakpozycjeTYPE = factory.createDokzakpozycjeTYPE();
		
		int i = 1;
        //szablony
		if(faktura.getCzySzablon()!=null&&faktura.getCzySzablon()){
			DokzakpozTYPE dokzakpozTYPE = factory.createDokzakpozTYPE();
			dokzakpozTYPE.setSzablonId(ObjectTypeManager.createFkDkrSzablonyKosztowTYPE(factory, faktura.getSzablon().getErpId()));
			dokzakpozycjeTYPE.getDokzakpoz().add(dokzakpozTYPE);
			dokzakpozTYPE.setNrpoz(i++);
		}
		else{

		
			for(PozycjaFaktury pozycja: faktura.getPozycjeFaktury()){
				DokzakpozTYPE dokzakpozTYPE = factory.createDokzakpozTYPE();

				//Stawka VAT
				IDSIDTYPE idsidtype = factory.createIDSIDTYPE();
				idsidtype.setType(IDENTITYNATURETYPE.N);
				idsidtype.setId(new BigDecimal(pozycja.getStawkaVat().getErp_id()));
				dokzakpozTYPE.setVatstawId(idsidtype);

				if(pozycja.getZlecenie()!=null && pozycja.getZlecenie().getErpId()!=null&&pozycja.getZlecenie().getErpId()!=0){//0 jesli jest pozycja niedostepna(wybrano fundusz projektowy)
					dokzakpozTYPE.setZlecprodId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(pozycja.getZlecenie().getErpId())));
				}

				if(pozycja.getOpk() != null && pozycja.getOpk().getErp_id() != null)
					dokzakpozTYPE.setKomorkaId(ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, new BigDecimal(pozycja.getOpk().getErp_id().longValue()), null));

				if(pozycja.getProjekt() != null&&pozycja.getProjekt().getErpId()!=null&&pozycja.getProjekt().getErpId()!=0){//0 jesli jest pozycja niedostepna(wybrano zlecenie)
					//dokzakpozTYPE.setProjektId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(pozycja.getBudzetPole().getBudzet().getBdBudzetId())));
					dokzakpozTYPE.setKontraktId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(pozycja.getProjekt().getErpId())));
				}

				if(faktura.getRodzajFaktury() == InvoiceGeneralConstans.RodzajeFaktury.FAKTPROJEKT.zwrocId()){
					if(pozycja.getProjekt()!=null)dokzakpozTYPE.setProjektId(ObjectTypeManager.createIDMIDTYPE(factory, new  BigDecimal(pozycja.getProjekt().getErpId())));
					//if(pozycja.get)dokzakpozTYPE.setEtapId(value);
					//dokzakpozTYPE.setZasobId(value);
				}

				if(pozycja.getBudzetPole() != null){
					dokzakpozTYPE.setBudzetId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(pozycja.getBudzetPole().getOkresBudzetowy())));
					dokzakpozTYPE.setBdBudzetKoId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(pozycja.getBudzetPole().getBudzet().getErpId())));

					if(pozycja.getBudzetPole().getPozycjaZBudzetu()!=null)dokzakpozTYPE.setBdRodzajKoId(new BigDecimal(pozycja.getBudzetPole().getPozycjaZBudzetu().getErpId()));
				}

				if(pozycja.getProduktFaktury() != null)
					dokzakpozTYPE.setWytworId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(pozycja.getProduktFaktury().getWytworId())));
				if(faktura.getCzyRozliczenieMediow()!=null&&faktura.getCzyRozliczenieMediow()){
					try {
						CostInvoiceProductDB media=CostInvoiceProductDB.findByCN(InvoiceGeneralConstans.MEDIA);
						if(media!=null){
							dokzakpozTYPE.setWytworId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(media.getWytworId())));
						}
						else log.error("Nie wybrano poprawnie wytowru na MEDIA");
					} catch (EdmException e) {
						log.error("", e);
					}
				}

				if(pozycja.getPrzeznaczenieZakupu() != null && pozycja.getPrzeznaczenieZakupu().getCn() != null)
					dokzakpozTYPE.setPrzeznzak(pozycja.getPrzeznaczenieZakupu().getCn());

				dokzakpozTYPE.setKwotnett(pozycja.getKwotaNetto());
				dokzakpozTYPE.setKwotvat(pozycja.getKwotaVat());
				if(pozycja.getOpk()!=null)
					dokzakpozTYPE.setKomorkaId(ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, new BigDecimal(pozycja.getOpk().getErp_id().longValue()),null));
				dokzakpozTYPE.setJmId(ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.T,null,"szt."));
				dokzakpozTYPE.setIlosc(BigDecimal.ONE);
				if(pozycja.getWniosekORezerwacje()!=null&&pozycja.getWniosekORezerwacje().getRezJmIdn()!=null&&(faktura.getCzyZWniosku()!=null&&faktura.getCzyZWniosku())){
					dokzakpozTYPE.setJmId(ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.T,null,pozycja.getWniosekORezerwacje().getRezJmIdn()));
					dokzakpozTYPE.setIlosc(pozycja.getWniosekORezerwacje().getIlosc());
				}
				if(faktura.getCzyZZamowienia()!=null&&faktura.getCzyZZamowienia()&&pozycja.getPozycjaZamowieniaId()!=null){
					try {
						List<PozycjaZamowieniaFakturaDB> pozZamDBList=PozycjaZamowieniaFakturaDBDAO.findByDocumentIdAndDocZamId(faktura.getId(), pozycja.getPozycjaZamowieniaId());
						if(pozZamDBList.size()>0){
							PozycjaZamowieniaFakturaDB pozZamDB=pozZamDBList.get(0);
							UnitOfMeasureDB unit =UnitOfMeasureDB.find(pozZamDB.getJM());
							if(unit!=null)
								dokzakpozTYPE.setJmId(ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.T,null,unit.getCn()));
							dokzakpozTYPE.setIlosc(pozZamDB.getILOSC_ZREAL());
						}
					} catch (EdmException e) {
						log.error("", e);
					}
				}
				dokzakpozTYPE.setNrpoz(i++);
				dokzakpozTYPE.setRodzwytw("0");
				if(faktura.getCzyRozliczenieMediow()!=null&&faktura.getCzyRozliczenieMediow()){
					try {
						CostInvoiceProductDB media=CostInvoiceProductDB.findByCN(InvoiceGeneralConstans.MEDIA);
						if(media!=null){
							dokzakpozTYPE.setRodztowaru(media.getRodzTowaru()+"");
						}
						else log.error("Nie wybrano poprawnie wytowru na MEDIA");
					} catch (EdmException e) {
						log.error("", e);
					}
				}		
				else if(pozycja.getProduktFaktury()!=null)
					dokzakpozTYPE.setRodztowaru(pozycja.getProduktFaktury().getRodzTowaru()+"");
				
				dokzakpozTYPE.setRepWartosci(createRepWartosciDlaPozycji(faktura, pozycja, factory));

				if(pozycja.getZrodlo()!= null
					//jesli wystarczy blad zamiast zawezania po zrodle to nie potrzebne	&&(faktura.getCzyZWniosku()==null||(faktura.getCzyZWniosku()!=null&&!faktura.getCzyZWniosku()))
						){
					DokzakpozZfiTYPE zfiTYPE = factory.createDokzakpozZfiTYPE();
					List<DokzakpozZfTYPE> pozycjeFinansowania = zfiTYPE.getDokzakpozZf();
					DokzakpozZfTYPE pozycjaFinansowania = factory.createDokzakpozZfTYPE();
					pozycjaFinansowania.setZrodloId(ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, new BigDecimal(pozycja.getZrodlo().getZrodlo_id().longValue()), null));
					pozycjaFinansowania.setProcent(new BigDecimal(100));
					pozycjaFinansowania.setKwota(pozycja.getKwotaNetto());
                    if(pozycja.getWniosekORezerwacje()!=null)
                        pozycjaFinansowania.setRodzaj(1);
                    else if(pozycja.getProjekt()!=null)
						pozycjaFinansowania.setRodzaj(2);
					else
						pozycjaFinansowania.setRodzaj(1);
					pozycjeFinansowania.add(pozycjaFinansowania);
					dokzakpozTYPE.setDokzakpozZfi(zfiTYPE);
				}
				if(pozycja.getWniosekORezerwacje()!=null&&(faktura.getCzyZWniosku()!=null&&faktura.getCzyZWniosku())){
					WniosekORezerwacje wniosek=pozycja.getWniosekORezerwacje();
					if(wniosek.getBdPlanZamId()!=null&&(!(wniosek.getBdPlanZamId().compareTo(Long.parseLong("0"))==0)))dokzakpozTYPE.setBdPlanZamId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(wniosek.getBdPlanZamId())));
					if(wniosek.getBdRezerwacjaId()!=null)dokzakpozTYPE.setBdRezerwacjaId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(wniosek.getBdRezerwacjaId())));
					if(wniosek.getBdRezerwacjaPozId()!=null)dokzakpozTYPE.setBdRezerwacjaPozId(new BigDecimal(wniosek.getBdRezerwacjaPozId()));
					if(wniosek.getBdRodzajPlanZamId()!=null&&(!(wniosek.getBdRodzajPlanZamId().compareTo(Long.parseLong("0"))==0)))dokzakpozTYPE.setBdRodzajPlanZamId(new BigDecimal(wniosek.getBdRodzajPlanZamId()));
				}
				if(pozycja.getPozycjaZamowieniaId()!=null&&(faktura.getCzyZZamowienia()!=null&&faktura.getCzyZZamowienia())){
					try {
						ZamowienieSimplePozycjeDB zamDB=ZamowienieSimplePozycjeDBDAO.findByZamDosPozIdUnique(pozycja.getPozycjaZamowieniaId());
						if(zamDB!=null){
							if(zamDB.getBdPlanZamId()!=null&&(!(zamDB.getBdPlanZamId().compareTo(Long.parseLong("0"))==0)))dokzakpozTYPE.setBdPlanZamId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(zamDB.getBdPlanZamId())));
							if(zamDB.getBdRezerwacjaId()!=null)dokzakpozTYPE.setBdRezerwacjaId(ObjectTypeManager.createIDMIDTYPE(factory, new BigDecimal(zamDB.getBdRezerwacjaId())));
							if(zamDB.getBdRezerwacjaPozId()!=null)dokzakpozTYPE.setBdRezerwacjaPozId(new BigDecimal(zamDB.getBdRezerwacjaPozId()));
							if(zamDB.getBdRodzajPlanZamId()!=null&&(!(zamDB.getBdRodzajPlanZamId().compareTo(Long.parseLong("0"))==0)))dokzakpozTYPE.setBdRodzajPlanZamId(new BigDecimal(zamDB.getBdRodzajPlanZamId()));
						}
						
						
					} catch (EdmException e) {
						log.error("", e);
					}
					
				}
				
				dokzakpozycjeTYPE.getDokzakpoz().add(dokzakpozTYPE);
			}
		}
		return dokzakpozycjeTYPE;
	}
	
	private static DostfaktTYPE dodajKontrahenta(Faktura faktura, ObjectFactory factory){
		DostfaktTYPE dostfaktTYPE = factory.createDostfaktTYPE();
		Kontrahent kontrahent = faktura.getWystawcaFaktury();
		String kontr = kontrahent.getNazwa();
		String kontNazwa = "";
		String kontNazwaRoz = "";
		
		if (kontr.length()>=140)
		{
			kontr = kontr.substring(0,140);
			kontNazwa = kontr.substring(0,60);
			kontNazwaRoz = kontr.substring(60,140);
		} else
			if ((kontr.length()>=60) && (kontr.length()<140))
			{
				kontNazwa = kontr.substring(0,60);
				kontNazwaRoz = kontr.substring(60,kontr.length());

			}
			else if (kontr.length()<60)
			{
				kontNazwa = kontr;

			}

		dostfaktTYPE.setNazwa(kontNazwa);
		dostfaktTYPE.setNazwaRozw(kontNazwaRoz);
		dostfaktTYPE.setNip(kontrahent.getNip());
		dostfaktTYPE.setKodpoczt(kontrahent.getKodpocztowy());
		dostfaktTYPE.setMiasto(kontrahent.getMiejscowosc());
		dostfaktTYPE.setUlica(kontrahent.getUlica());
		dostfaktTYPE.setNrdomu(kontrahent.getNrDomu());
		dostfaktTYPE.setNrmieszk(kontrahent.getNrMieszkania());
		EnumItem krajCN = null;
		try{
			krajCN = DataBaseEnumField.getEnumItemForTable(TABLENAME_KRAJE, 
					Integer.valueOf(kontrahent.getKraj()));
		}catch(EdmException e){
			log.error("", e);
		}
		dostfaktTYPE.setKrajId(ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.T, null, krajCN.getCn()));

		return dostfaktTYPE;
	}
	
	private static SysDokZalacznikiTYPE createZalaczniki(Faktura faktura, ObjectFactory factory){
		SysDokZalacznikiTYPE sysDokZalacznikiTYPE = factory.createSysDokZalacznikiTYPE();
		
		if(faktura.getZalacznik() != null){
			String sciezka = "";

            if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu")!=null){
                String urlSystemu = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
                sciezka  = urlSystemu;
            }
            else
            {
                sciezka  = Docusafe.getBaseUrl();
            }

            sciezka += "/repository/view-attachment-revision.do?id="+faktura.getZalacznik().getMostRecentRevision().getId();

            SysDokZalacznikTYPE sysDokZalacznikTYPE = factory.createSysDokZalacznikTYPE();
            sysDokZalacznikTYPE.setNazwa("Skan faktury");
            sysDokZalacznikTYPE.setSciezka(sciezka);
            sysDokZalacznikTYPE.setType("URL");

            //dodanie do listy zalaczników
            sysDokZalacznikiTYPE.getSysDokZalacznik().add(sysDokZalacznikTYPE);
		}

        return sysDokZalacznikiTYPE;
	}
	
	private static String createUwagi(Faktura faktura){
		String wynik = "";
		
		//Nr KO
		try {
			wynik += "Nr KO: "+OfficeDocument.find(faktura.getId()).getOfficeNumber()+"; ";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		if(faktura.getNazwaTowaruUslugi()!=null && !faktura.getNazwaTowaruUslugi().isEmpty())
			wynik += "Nazwa towaru/uslugi: "+faktura.getNazwaTowaruUslugi()+"; ";
		
		if(faktura.getDodatkoweInformacje()!=null && !faktura.getDodatkoweInformacje().isEmpty())
			wynik += "Dodatkowe info: "+faktura.getDodatkoweInformacje()+"; ";
		
		if(faktura.getOpisMerytoryczny()!=null && !faktura.getOpisMerytoryczny().isEmpty())
			wynik += "Opis merytoryczny: "+faktura.getOpisMerytoryczny()+"; ";
		
		if(faktura.getPrzeznaczenieIUwagi()!=null && !faktura.getPrzeznaczenieIUwagi().isEmpty())
			wynik += "Przeznaczenie i uwagi: "+faktura.getPrzeznaczenieIUwagi()+"; ";
		
		if(faktura.getNumerPostepowaniaPrzetargowego()!=null && !faktura.getNumerPostepowaniaPrzetargowego().isEmpty())
			wynik += "Numer postepowania przetargowego: "+faktura.getNumerPostepowaniaPrzetargowego()+"; ";
		
        if(faktura.getCzyKorekta()!=null&&faktura.getCzyKorekta()){
        	if(faktura.getKorektaNumer()!=null&&!faktura.getKorektaNumer().equals(""))wynik+="Numer faktury korygowanej: "+faktura.getKorektaNumer()+"; ";
        }
        
		return wynik;
	}
	
}
