package pl.compan.docusafe.general.mapers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;


import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.general.canonical.dao.StawkaVatDAO;
import pl.compan.docusafe.general.canonical.dao.TypDokumentuDAO;
import pl.compan.docusafe.general.canonical.dao.TypPlatnosciDAO;
import pl.compan.docusafe.general.canonical.dao.WydatekStrukturalnyDAO;
import pl.compan.docusafe.general.canonical.model.BudzetKOKomorek;
import pl.compan.docusafe.general.canonical.model.BudzetPozycja;
import pl.compan.docusafe.general.canonical.model.BudzetPozycjaSimple;
import pl.compan.docusafe.general.canonical.model.Faktura;
import pl.compan.docusafe.general.canonical.model.OPK;
import pl.compan.docusafe.general.canonical.model.PozycjaFaktury;
import pl.compan.docusafe.general.canonical.model.Projekt;
import pl.compan.docusafe.general.canonical.model.PrzeznaczenieZakupu;
import pl.compan.docusafe.general.canonical.model.StawkaVat;
import pl.compan.docusafe.general.canonical.model.SzablonyRozliczenFaktur;
import pl.compan.docusafe.general.canonical.model.TypDokumentu;
import pl.compan.docusafe.general.canonical.model.TypPlatnosci;
import pl.compan.docusafe.general.canonical.model.Waluta;
import pl.compan.docusafe.general.canonical.model.WydatekStrukturalny;
import pl.compan.docusafe.general.canonical.model.ZadaniaProjektowe;
import pl.compan.docusafe.general.canonical.model.ZleceniaProdukcyjne;
import pl.compan.docusafe.general.canonical.model.Zrodlo;
import pl.compan.docusafe.general.canonical.model.ZrodloFinansowaniaProjektow;
import pl.compan.docusafe.general.hibernate.dao.BudzetKOKomorkaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.ProjectDBDAO;
import pl.compan.docusafe.general.hibernate.dao.SourcesOfProjectFundingDBDAO;
import pl.compan.docusafe.general.hibernate.dao.SzablonyRozliczenFakturDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudzetKOKomorekDB;
import pl.compan.docusafe.general.hibernate.model.KontrahentDB;
import pl.compan.docusafe.general.hibernate.model.ProjectDB;
import pl.compan.docusafe.general.hibernate.model.SourcesOfProjectFundingDB;
import pl.compan.docusafe.general.hibernate.model.SzablonyRozliczenFakturDB;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa odpowiedzialna za mapowanie FieldsManagera 
 * na model
 * todo: pola nie obslugiwane:StanowiskoDostaw,Dziennik,TYP_FAKTURY_WAL,TYP_FAKTURY_PLN,
 * WYSTAWCA, ADRESAT,WYDATKISTRUKTENUM,OSOBAUZUPOPIS,
 * ZAMOWIENIAPOZ,ZAMOWIENIE,DOSTAWCA,GLOWNYKSIEG,PRACKSIEGOWOSC,WYNIKAZPOSTEPPRZETARG,TRYBPOSTEP,PRACOWNIK,
 * 
 * 
 * mozna wczytywac/zapisywac wartosci z/do DWR ( oprocz slownikow, je tylko zaczytuje, nie zapisze ich zmian)
 * wartosci z FM mozna tylko przeksztalcic do modelu faktury ( oprocz slownikow )
 * 
 * Pelnomocnik - tylko wczytuje
 */
public class FMToModelMapper implements InvoiceGeneralConstans{
	protected static Logger log = LoggerFactory.getLogger(FMToModelMapper.class);





/**
 * otwiera(jesli juz nie jest otwarty) kontekst
 * zwraca obiekt faktury z dwr-owych values 
 * zamyka kontekst jesli musial byc otworzony
 * */
	public static Faktura zwrocModelZValuesDWR(Map<String, FieldData> values){
		boolean ctx=false;
		try {
			ctx=DSApi.openContextIfNeeded();
			return zwrocModelZValues(values,true);
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
			//e.printStackTrace();
		}
		finally{
			DSApi.closeContextIfNeeded(ctx);
		}
		return null;
		
	}
	/**
	 * otwiera(jesli juz nie jest otwarty) kontekst
	 * zwraca obiekt faktury z values z fm 
	 * zamyka kontekst jesli musial byc otworzony
	 * */
	public static Faktura zwrocModelZValues(Map<String, Object> values){
		boolean ctx=false;
		try {
			ctx=DSApi.openContextIfNeeded();
			return zwrocModelZValues(zamienMapaObjectNaField(values),false);
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		//	e.printStackTrace();
		}
		finally{
			DSApi.closeContextIfNeeded(ctx);
		}
		return null;
	}

	private static Faktura zwrocModelZValues(Map<String, FieldData> values,boolean DWR) {
		Faktura f=new Faktura();
		boolean ctx=false;
		try {
			String przedrostek="";
			if(DWR)przedrostek="DWR_";


			//defaultStatus
			f.setStatus(StatusFaktury.valueOf(STATUS_UTW_DOKUMENTU_CN).zwrocId());
			if(values.containsKey(przedrostek+STATUS_CN)){
				if(values.get(przedrostek+STATUS_CN)!=null){
					if(DWR){ 
						if(values.get(przedrostek+STATUS_CN).getEnumValuesData().getSelectedOptions().size()>0)
						{
							f.setStatus(Integer.parseInt(values.get(przedrostek+STATUS_CN).getEnumValuesData().getSelectedOptions().get(0)));
						}
					}
					else{
						f.setStatus(values.get(przedrostek+STATUS_CN).getIntegerData());
					}

				}
			}
			
			//czy zamowienie
			if(values.containsKey(przedrostek+CZYZZAMOWIENIA_CN)){
				if(values.get(przedrostek+CZYZZAMOWIENIA_CN)!=null){
					f.setCzyZZamowienia(values.get(przedrostek+CZYZZAMOWIENIA_CN).getBooleanData());
				}	
			}
			//czy z wniosku
			if(values.containsKey(przedrostek+CZYZWNIOSKU_CN)){
				if(values.get(przedrostek+CZYZWNIOSKU_CN)!=null){
					f.setCzyZWniosku(values.get(przedrostek+CZYZWNIOSKU_CN).getBooleanData());
				}	
			}
			//czy rozliczenie mediow
			if(values.containsKey(przedrostek+CZYROZLICZENIEMEDIOW_CN)){
				if(values.get(przedrostek+CZYROZLICZENIEMEDIOW_CN)!=null){
					f.setCzyRozliczenieMediow(values.get(przedrostek+CZYROZLICZENIEMEDIOW_CN).getBooleanData());
				}	
			}
			//zaakceptowano zamowienie
			if(values.containsKey(przedrostek+ZAAKCEPTOWANOPOZZAM_CN)){
				if(values.get(przedrostek+ZAAKCEPTOWANOPOZZAM_CN)!=null){
					f.setZAAKCEPTOWANOPOZZAM(values.get(przedrostek+ZAAKCEPTOWANOPOZZAM_CN).getBooleanData());
				}	
			}
			//zaakceptowano wnioski o rezerwacje
			if(values.containsKey(przedrostek+ZAAKCEPTOWANO_WNIOSKI_O_REZERWACJE_CN)){
				if(values.get(przedrostek+ZAAKCEPTOWANO_WNIOSKI_O_REZERWACJE_CN)!=null){
					f.setZaakceptowanoWnioskiORez(values.get(przedrostek+ZAAKCEPTOWANO_WNIOSKI_O_REZERWACJE_CN).getBooleanData());
				}	
			}
			//numer zamowienia
			if(values.containsKey(przedrostek+NRZAMOWIENIA_CN)){
				if(values.get(przedrostek+NRZAMOWIENIA_CN)!=null){
					if(DWR){
						if(values.get(przedrostek+NRZAMOWIENIA_CN).getEnumValuesData()!=null&&values.get(przedrostek+NRZAMOWIENIA_CN).getEnumValuesData().getSelectedOptions()!=null&&
								values.get(przedrostek+NRZAMOWIENIA_CN).getEnumValuesData().getSelectedOptions().size()>0&&values.get(przedrostek+NRZAMOWIENIA_CN).getEnumValuesData().getSelectedOptions().get(0)!=null&&
										!values.get(przedrostek+NRZAMOWIENIA_CN).getEnumValuesData().getSelectedOptions().get(0).toString().equals(""))
						{	
							DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_NUMERY_ZAMOWIEN);
							String widokId=values.get(przedrostek+NRZAMOWIENIA_CN).getEnumValuesData().getSelectedOptions().get(0);
							Long erpId=Long.parseLong(d.getEnumItem(widokId).getCn());
							
							f.setDocumentIdZamowienia(erpId);
						}
					}
					else{
						DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_NUMERY_ZAMOWIEN);
						String widokId=values.get(przedrostek+NRZAMOWIENIA_CN).getIntegerData().toString();
						Long erpId=Long.parseLong(d.getEnumItem(widokId).getCn());
						f.setDocumentIdZamowienia(erpId);
					}
				}

			}
			//data wplywu
			if(values.containsKey(przedrostek+DATA_WPLYWU_CN)){
				if(values.get(przedrostek+DATA_WPLYWU_CN)!=null){
					f.setDataWplywu(values.get(przedrostek+DATA_WPLYWU_CN).getDateData());
				}
			}
			/*		//adresat
		if(values.containsKey(przedrostek+ADRESAT_CN)){
			if(values.get(przedrostek+ADRESAT_CN)!=null){
				if(values.get(przedrostek+ADRESAT_CN).getDictionaryData().get("id")!=null){
					pl.compan.docusafe.core.office.Recipient p = new  Recipient();
					p.createBasePerson(ADRESAT_CN, values.get(przedrostek+ADRESAT_CN).getDictionaryData());
					f.setAdresat(p);
				}
			}
		}*/
			//kwota netto
			if(values.containsKey(przedrostek+KWOTA_NETTO_CN)){
				if(values.get(przedrostek+KWOTA_NETTO_CN)!=null){
					f.setKwotaNetto(values.get(przedrostek+KWOTA_NETTO_CN).getMoneyData());
				}
			}
			//kwota brutto
			if(values.containsKey(przedrostek+KWOTA_BRUTTO_CN)){
				if(values.get(przedrostek+KWOTA_BRUTTO_CN)!=null){
					f.setKwotaBrutto(values.get(przedrostek+KWOTA_BRUTTO_CN).getMoneyData());
				}
			}
			//kwota vat
			if(values.containsKey(przedrostek+KWOTA_VAT_CN)){
				if(values.get(przedrostek+KWOTA_VAT_CN)!=null){
					f.setKwotaVat(values.get(przedrostek+KWOTA_VAT_CN).getMoneyData());
				}
			}
			//waluta
			if(values.containsKey(przedrostek+WALUTA_CN)){
				if(values.get(przedrostek+WALUTA_CN)!=null){
					if(DWR){
						if(values.get(przedrostek+WALUTA_CN).getEnumValuesData().getSelectedOptions().size()>0&&!values.get(przedrostek+WALUTA_CN).getEnumValuesData().getSelectedOptions().get(0).toString().equals(""))
						{
							Waluta w=new Waluta();
							w.setId(Long.parseLong(values.get(przedrostek+WALUTA_CN).getEnumValuesData().getSelectedOptions().get(0)));
							DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_SIMPLE_WALUTA);
							for(EnumItem ei: d.getAvailableItems()){
								if(ei.getId()==Integer.parseInt(values.get(przedrostek+WALUTA_CN).getEnumValuesData().getSelectedOptions().get(0))){
									w.setNazwa(ei.getTitle());
								}
							}
							f.setWaluta(w);
						}
					}
					else{
						Waluta w=new Waluta();
						w.setId(Long.parseLong(values.get(przedrostek+WALUTA_CN).getIntegerData().toString()));
						DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_SIMPLE_WALUTA);
						for(EnumItem ei: d.getAvailableItems()){
							if(ei.getId()==values.get(przedrostek+WALUTA_CN).getIntegerData()){
								w.setNazwa(ei.getTitle());
							}
						}
						f.setWaluta(w);
					}
				}

			}
			//TERMIN_PLATNOSCI
			if(values.containsKey(przedrostek+TERMIN_PLATNOSCI_CN)){
				if(values.get(przedrostek+TERMIN_PLATNOSCI_CN)!=null){
					f.setTerminPlatnosci(values.get(przedrostek+TERMIN_PLATNOSCI_CN).getDateData());
				}
			}

			//rodzajfaktury
			if(values.containsKey(przedrostek+RODZAJFAKTURY_CN)){
				if(values.get(przedrostek+RODZAJFAKTURY_CN)!=null){
					if(DWR){ 
						if(values.get(przedrostek+RODZAJFAKTURY_CN).getEnumValuesData().getSelectedOptions().size()>0&&!values.get(przedrostek+RODZAJFAKTURY_CN).getEnumValuesData().getSelectedOptions().get(0).toString().equals(""))
						{
							f.setRodzajFaktury(Integer.parseInt(values.get(przedrostek+RODZAJFAKTURY_CN).getEnumValuesData().getSelectedOptions().get(0)));
						}
					}
					else{
						f.setRodzajFaktury(values.get(przedrostek+RODZAJFAKTURY_CN).getIntegerData());
					}
				}
			}
			//FAKTURA ZA koszty trwale
			if(values.containsKey(przedrostek+FAKTZAKOSZTYTRWALE_CN)){
				if(values.get(przedrostek+FAKTZAKOSZTYTRWALE_CN)!=null){
					f.setFakturaNaWydziale((values.get(przedrostek+FAKTZAKOSZTYTRWALE_CN).getBooleanData()));
				}
			}
			
			//FAKTNAWYDZIALE
			if(values.containsKey(przedrostek+FAKTNAWYDZIALE_CN)){
				if(values.get(przedrostek+FAKTNAWYDZIALE_CN)!=null){
					f.setFakturaNaWydziale((values.get(przedrostek+FAKTNAWYDZIALE_CN).getBooleanData()));
				}
			}
			//PELNOMOCNIK
			if(values.containsKey(przedrostek+PELNOMOCNIK_CN)){
				if(values.get(przedrostek+PELNOMOCNIK_CN)!=null){
					if(DWR){
						if(values.get(przedrostek+PELNOMOCNIK_CN).getEnumValuesData().getSelectedOptions().size()>0&&values.get(przedrostek+PELNOMOCNIK_CN).getEnumValuesData().getSelectedOptions().get(0)!=null&&!values.get(przedrostek+PELNOMOCNIK_CN).getEnumValuesData().getSelectedOptions().get(0).equals("")){	
							f.setPelnomocnikDoSprawFinansowych((DivisionImpl)DivisionImpl.findById(Long.parseLong(values.get(przedrostek+PELNOMOCNIK_CN).getEnumValuesData().getSelectedOptions().get(0))));
						}
					}
					else if(values.get(przedrostek+PELNOMOCNIK_CN).getIntegerData()!=null){
						f.setPelnomocnikDoSprawFinansowych((DivisionImpl)DivisionImpl.findById(Long.parseLong(values.get(przedrostek+PELNOMOCNIK_CN).toString())));
					}
				}
			}
			//NR_FAKTURY
			if(values.containsKey(przedrostek+NR_FAKTURY_CN)){
				if(values.get(przedrostek+NR_FAKTURY_CN)!=null&&!values.get(przedrostek+NR_FAKTURY_CN).toString().equals("")){
					f.setNumerFaktury(values.get(przedrostek+NR_FAKTURY_CN).getStringData());
				}
			}
			
			//czy korekta faktury
			if(values.containsKey(przedrostek+KOREKTA_CN)){
				if(values.get(przedrostek+KOREKTA_CN)!=null){
					f.setCzyKorekta(values.get(przedrostek+KOREKTA_CN).getBooleanData());
				}	
			}
			//Numer faktury korygowanej
			if(values.containsKey(przedrostek+KOREKTA_NUMER_CN)){
				if(values.get(przedrostek+KOREKTA_NUMER_CN)!=null){
					f.setKorektaNumer(values.get(przedrostek+KOREKTA_NUMER_CN).getStringData());
				}	
			}
			
			//BARCODE
			if(values.containsKey(przedrostek+BARCODE_CN)){
				if(values.get(przedrostek+BARCODE_CN)!=null){
					f.setKodKreskowy(values.get(przedrostek+BARCODE_CN).getStringData());
				}
			}

				//SENDER
		//wystawca
			if(values.containsKey(przedrostek+WYSTAWCA_CN)){
				if(values.get(przedrostek+WYSTAWCA_CN)!=null){
					if(DWR){
						if(values.get(przedrostek+WYSTAWCA_CN).getDictionaryData().get("id")!=null&&values.get(przedrostek+WYSTAWCA_CN).getDictionaryData().get("id").getData()!=null&&!values.get(przedrostek+WYSTAWCA_CN).getDictionaryData().get("id").getData().equals("")){
							KontrahentDB kontDB=KontrahentDB.findByIdUnique(Long.parseLong(values.get(przedrostek+WYSTAWCA_CN).getDictionaryData().get("id").getData().toString()));
							f.setWystawcaFaktury(HibernateToCanonicalMapper.MapKontrahentDBToKontrahent(kontDB));
						}
					}
					else{//TODO nie ma dla nie dwr
						
					}
				}
			}

			//DATA_WYSTAWIENIA
			if(values.containsKey(przedrostek+DATA_WYSTAWIENIA_CN)){
				if(values.get(przedrostek+DATA_WYSTAWIENIA_CN)!=null){
					f.setDataWystawienia(values.get(przedrostek+DATA_WYSTAWIENIA_CN).getDateData());
				}
			}
			//KWOTA_W_WALUCIE
			if(values.containsKey(przedrostek+KWOTA_W_WALUCIE_CN)){
				if(values.get(przedrostek+KWOTA_W_WALUCIE_CN)!=null){
					f.setKwotaWWalucie(values.get(przedrostek+KWOTA_W_WALUCIE_CN).getMoneyData());
				}
			}
			//KURS
			if(values.containsKey(przedrostek+KURS_CN)){
				if(values.get(przedrostek+KURS_CN)!=null){
					f.setKurs(values.get(przedrostek+KURS_CN).getMoneyData());
				}
			}
			//TYPPLATNOSCI
			if(values.containsKey(przedrostek+TYPPLATNOSCI_CN)){
				if(values.get(przedrostek+TYPPLATNOSCI_CN)!=null){
					if(DWR){
						if(values.get(przedrostek+TYPPLATNOSCI_CN).getEnumValuesData().getSelectedOptions().size()>0&&!values.get(przedrostek+TYPPLATNOSCI_CN).getEnumValuesData().getSelectedOptions().get(0).toString().equals(""))
						{
							TypPlatnosci typ;
							typ=TypPlatnosciDAO.find(Long.parseLong(values.get(przedrostek+TYPPLATNOSCI_CN).getEnumValuesData().getSelectedOptions().get(0)));
							f.setTypPlatnosci(typ);
						}
					}
					else{
						TypPlatnosci typ;
						typ=TypPlatnosciDAO.find(Long.parseLong(values.get(przedrostek+TYPPLATNOSCI_CN).getIntegerData().toString()));
						f.setTypPlatnosci(typ);
					}



				}
			}
			//TYP faktury PLN
			if(values.containsKey(przedrostek+TYP_FAKTURY_PLN_CN)){
				if(values.get(przedrostek+TYP_FAKTURY_PLN_CN)!=null){
					if(DWR){
						if(values.get(przedrostek+TYP_FAKTURY_PLN_CN).getEnumValuesData().getSelectedOptions().size()>0&&!values.get(przedrostek+TYP_FAKTURY_PLN_CN).getEnumValuesData().getSelectedOptions().get(0).toString().equals(""))
						{
							/*TypDokumentu typ;
							typ=TypDokumentuDAO.find(Long.parseLong(values.get(przedrostek+TYP_FAKTURY_PLN_CN).getEnumValuesData().getSelectedOptions().get(0)));
							f.setTypFaktury(typ);*/
						}
					}
					else{
						/*TypDokumentu typ;
						typ=TypDokumentuDAO.find(Long.parseLong(values.get(przedrostek+TYP_FAKTURY_PLN_CN).getIntegerData().toString()));
						f.setTypFaktury(typ);*/
					}



				}
			}
			//TYP faktury wal 
			if(values.containsKey(przedrostek+TYP_FAKTURY_WAL_CN)){
				if(values.get(przedrostek+TYP_FAKTURY_WAL_CN)!=null){
					if(DWR){
						if(values.get(przedrostek+TYP_FAKTURY_WAL_CN).getEnumValuesData().getSelectedOptions().size()>0&&!values.get(przedrostek+TYP_FAKTURY_WAL_CN).getEnumValuesData().getSelectedOptions().get(0).toString().equals(""))
						{
							TypDokumentu typ;
							typ=TypDokumentuDAO.find(Long.parseLong(values.get(przedrostek+TYP_FAKTURY_WAL_CN).getEnumValuesData().getSelectedOptions().get(0)));
							f.setTypFaktury(typ);
						}
					}
					else{
						TypDokumentu typ;
						typ=TypDokumentuDAO.find(Long.parseLong(values.get(przedrostek+TYP_FAKTURY_WAL_CN).getIntegerData().toString()));
						f.setTypFaktury(typ);
					}



				}
			}
			//NAZWATOWUSL
			if(values.containsKey(przedrostek+NAZWATOWUSL_CN)){
				if(values.get(przedrostek+NAZWATOWUSL_CN)!=null){
					f.setNazwaTowaruUslugi(values.get(przedrostek+NAZWATOWUSL_CN).getStringData());
				}	
			}

			//DodatkoweInfo
			if(values.containsKey(przedrostek+DODATKOWEINFO_CN)){
				if(values.get(przedrostek+DODATKOWEINFO_CN)!=null){
					f.setDodatkoweInformacje(values.get(przedrostek+DODATKOWEINFO_CN).getStringData());
				}	
			}
			//WYDATEKSTRUKT
			if(values.containsKey(przedrostek+WYDATEKSTRUKT_CN)){
				if(values.get(przedrostek+WYDATEKSTRUKT_CN)!=null){
					f.setWydatekJestStrukturalny(values.get(przedrostek+WYDATEKSTRUKT_CN).getBooleanData());
				}	
			}
			//WYDATKISTRUKTPOZ
			if(values.containsKey(przedrostek+WYDATEKSTRUKTPOZ_CN)){
				List<WydatekStrukturalny> pozycje=new ArrayList<WydatekStrukturalny>();
				Map<String,FieldData> slownik=values.get(przedrostek+WYDATEKSTRUKTPOZ_CN).getDictionaryData();
				int licznik=1;
				String zarostek="_"+Integer.toString(licznik);
				boolean czyJest=true;
				String slownikPrzedrostek;
				if(DWR)slownikPrzedrostek=WYDATEKSTRUKTPOZ_CN+"_";
				else slownikPrzedrostek="";
				for(int i=0;i<slownik.size()&&czyJest;i++){
					WydatekStrukturalny ws=WydatekStrukturalnyDAO.getInstance().getWydatekStrukturalny();
					czyJest=false;
					//ustaic id
					if(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KWOTA_BRUTTO_WYD_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KWOTA_BRUTTO_WYD_CN+zarostek).getMoneyData()!=null){
							ws.setKwotaBrutto(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KWOTA_BRUTTO_WYD_CN+zarostek).getMoneyData());
							czyJest=true;
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KWOTA_BRUTTO_WYD_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KWOTA_BRUTTO_WYD_CN+zarostek).getStringData().equals("")){
								ws.setKwotaBrutto( new BigDecimal(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KWOTA_BRUTTO_WYD_CN+zarostek).getStringData()));
								czyJest=true;}
						}
					}
					if(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN+zarostek).getEnumValuesData()!=null){
							if(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN+zarostek).getEnumValuesData().getSelectedOptions()!=null
									&&slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN+zarostek).getEnumValuesData().getSelectedOptions().size()>0
									&&slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN+zarostek).getEnumValuesData().getSelectedOptions().get(0)!=null
									&&!slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN+zarostek).getEnumValuesData().getSelectedOptions().get(0).toString().equals(""))
							{
								ws.setWydatekStrukturalny(Long.parseLong(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN+zarostek).getEnumValuesData().getSelectedOptions().get(0)));
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN+zarostek).getStringData().equals("")){
								ws.setWydatekStrukturalny(Long.parseLong(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN+zarostek).getStringData()));
								czyJest=true;
							}
						}

					}
					if(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KOD_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KOD_CN+zarostek).getEnumValuesData()!=null){
							if(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KOD_CN+zarostek).getEnumValuesData().getSelectedOptions()!=null
									&&slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KOD_CN+zarostek).getEnumValuesData().getSelectedOptions().size()>0
									&&slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KOD_CN+zarostek).getEnumValuesData().getSelectedOptions().get(0)!=null
									&&!slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KOD_CN+zarostek).getEnumValuesData().getSelectedOptions().get(0).toString().equals(""))
							{
								ws.setKod(Long.parseLong(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KOD_CN+zarostek).getEnumValuesData().getSelectedOptions().get(0)));
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KOD_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KOD_CN+zarostek).getStringData().equals("")){
								ws.setKod(Long.parseLong(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_KOD_CN+zarostek).getStringData()));
								czyJest=true;
							}
						}

					}
					if(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_OBSZAR_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_OBSZAR_CN+zarostek).getEnumValuesData()!=null){
							if(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_OBSZAR_CN+zarostek).getEnumValuesData().getSelectedOptions()!=null
									&&slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_OBSZAR_CN+zarostek).getEnumValuesData().getSelectedOptions().size()>0
									&&slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_OBSZAR_CN+zarostek).getEnumValuesData().getSelectedOptions().get(0)!=null
									&&!slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_OBSZAR_CN+zarostek).getEnumValuesData().getSelectedOptions().get(0).toString().equals(""))
							{
								ws.setObszar(Long.parseLong(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_OBSZAR_CN+zarostek).getEnumValuesData().getSelectedOptions().get(0)));
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_OBSZAR_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_OBSZAR_CN+zarostek).getStringData().equals("")){
								ws.setObszar(Long.parseLong(slownik.get(slownikPrzedrostek+WYDATEKSTRUKTPOZ_OBSZAR_CN+zarostek).getStringData()));
								czyJest=true;
							}
						}

					}
			
					if(czyJest)pozycje.add(ws);
					licznik++;
					zarostek="_"+Integer.toString(licznik);
				}
				f.setWydatkiStrukturalne(pozycje);
			}
			//SKIERUJUZUPOPIS
			if(values.containsKey(przedrostek+SKIERUJUZUPOPIS_CN)){
				if(values.get(przedrostek+SKIERUJUZUPOPIS_CN)!=null){
					f.setSkierujDoUzupelnieniaOpisu(values.get(przedrostek+SKIERUJUZUPOPIS_CN).getBooleanData());
				}	
			}
			//ZAMNAMAG
			if(values.containsKey(przedrostek+ZAMNAMAG_CN)){
				if(values.get(przedrostek+ZAMNAMAG_CN)!=null){
					f.setZamowienieNaMagazyn(values.get(przedrostek+ZAMNAMAG_CN).getBooleanData());
				}	
			}
			//kwota pozostala
			if(values.containsKey(przedrostek+KWOTAPOZOST_CN)){
				if(values.get(przedrostek+KWOTAPOZOST_CN)!=null){
					f.setKwotaPozostala(values.get(przedrostek+KWOTAPOZOST_CN).getMoneyData());
				}
			}
			//kwota pozostala netto
			if(values.containsKey(przedrostek+KWOTAPOZOSTNETTO_CN)){
				if(values.get(przedrostek+KWOTAPOZOSTNETTO_CN)!=null){
					f.setKwotaPozostalaNetto(values.get(przedrostek+KWOTAPOZOSTNETTO_CN).getMoneyData());
				}
			}
			//kwota pozostala vat
			if(values.containsKey(przedrostek+KWOTAPOZOSTVAT_CN)){
				if(values.get(przedrostek+KWOTAPOZOSTVAT_CN)!=null){
					f.setKwotaPozostalaVAT(values.get(przedrostek+KWOTAPOZOSTVAT_CN).getMoneyData());
				}
			}
			//OPISMERYT
			if(values.containsKey(przedrostek+OPISMERYT_CN)){
				if(values.get(przedrostek+OPISMERYT_CN)!=null){
					f.setOpisMerytoryczny(values.get(przedrostek+OPISMERYT_CN).getStringData());
				}	
			}
			//PRZEZNIUWAGI
			if(values.containsKey(przedrostek+PRZEZNIUWAGI_CN)){
				if(values.get(przedrostek+PRZEZNIUWAGI_CN)!=null){
					f.setPrzeznaczenieIUwagi(values.get(przedrostek+PRZEZNIUWAGI_CN).getStringData());
				}	
			}
			//NUMERPOSTEPPRZETARG
			if(values.containsKey(przedrostek+NUMERPOSTPRZETARG_CN)){
				if(values.get(przedrostek+NUMERPOSTPRZETARG_CN)!=null){
					f.setNumerPostepowaniaPrzetargowego(values.get(przedrostek+NUMERPOSTPRZETARG_CN).getStringData());
				}	
			}
			//WYNIKAZPOSTEPPRZETARG
			if(values.containsKey(przedrostek+WYNIKAZPOSTEPPRZETARG_CN)){
				if(values.get(przedrostek+WYNIKAZPOSTEPPRZETARG_CN)!=null){
					if(DWR){ 
						if(values.get(przedrostek+WYNIKAZPOSTEPPRZETARG_CN).getEnumValuesData().getSelectedOptions().size()>0&&!values.get(przedrostek+WYNIKAZPOSTEPPRZETARG_CN).getEnumValuesData().getSelectedOptions().get(0).toString().equals(""))
						{
							f.setWynikaZPostepowaniaPrzetargowego(Integer.parseInt(values.get(przedrostek+WYNIKAZPOSTEPPRZETARG_CN).getEnumValuesData().getSelectedOptions().get(0)));
						}
					}
					else{
						f.setWynikaZPostepowaniaPrzetargowego(values.get(przedrostek+WYNIKAZPOSTEPPRZETARG_CN).getIntegerData());
					}

				}
			}
			if(values.containsKey(przedrostek+POZYCJE_FAKTURY_CN)){
				List<PozycjaFaktury> pozycje=new ArrayList<PozycjaFaktury>();
				Map<String,FieldData> slownik=values.get(przedrostek+POZYCJE_FAKTURY_CN).getDictionaryData();

				int licznik=1;
				String zarostek="_"+Integer.toString(licznik);
				boolean czyJest=true;
				String slownikPrzedrostek;
				if(DWR)slownikPrzedrostek=POZYCJE_FAKTURY_CN+"_";
				else slownikPrzedrostek="";
				for(int i=0;i<slownik.size()&&czyJest;i++){
					PozycjaFaktury pf=new PozycjaFaktury();
					czyJest=false;

					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ID_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ID_CN+zarostek).getLongData()!=null){
							pf.setIdPozycji(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ID_CN+zarostek).getLongData());
							czyJest=true;
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ID_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ID_CN+zarostek).getStringData().equals("")){
								pf.setIdPozycji(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ID_CN+zarostek).getLongData());
								czyJest=true;}
						}
					}
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POLE_BUDZETU_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POLE_BUDZETU_CN+zarostek).getEnumValuesData()!=null){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POLE_BUDZETU_CN+zarostek).getEnumValuesData().getSelectedId()!=null
									&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POLE_BUDZETU_CN+zarostek).getEnumValuesData().getSelectedId().equals("")){
								BudzetPozycja bp=new BudzetPozycja();
								DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_MPK_BUDZET);
								EnumItem ei=d.getEnumItem(Integer.parseInt(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POLE_BUDZETU_CN+zarostek).getEnumValuesData().getSelectedId()));
								if(ei!=null&&ei.getCn()!=null&&!ei.getCn().equals("")){
									List<BudzetKOKomorekDB>lista=BudzetKOKomorkaDBDAO.findByErpId(Long.valueOf(ei.getId()));
									if(lista.size()>0){
										BudzetKOKomorek bko=HibernateToCanonicalMapper.MapBudzetKOKomorekDBToBudzetKOKomorek(lista.get(lista.size()-1));
										bp.setBudzet(bko);
										pf.setBudzetPole(bp);
										czyJest=true;
									}
								}
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POLE_BUDZETU_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POLE_BUDZETU_CN+zarostek).getStringData().equals("")){
								DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_MPK_BUDZET);
								BudzetPozycja bp=new BudzetPozycja();
								EnumItem ei=d.getEnumItem(Integer.parseInt(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POLE_BUDZETU_CN+zarostek).getStringData()));
								if(ei!=null&&ei.getCn()!=null&&!ei.getCn().equals("")){
									List<BudzetKOKomorekDB>lista=BudzetKOKomorkaDBDAO.findByErpId(Long.valueOf(ei.getId()));
									if(lista.size()>0){
										BudzetKOKomorek bko=HibernateToCanonicalMapper.MapBudzetKOKomorekDBToBudzetKOKomorek(lista.get(lista.size()-1));
										bp.setBudzet(bko);
										pf.setBudzetPole(bp);
										czyJest=true;
									}
								}
							}
						}
					}

					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_CN+zarostek).getEnumValuesData()!=null){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_CN+zarostek).getEnumValuesData().getSelectedId()!=null
									&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_CN+zarostek).getEnumValuesData().getSelectedId().equals("")){
								ZadaniaProjektowe zp=new ZadaniaProjektowe();
								zp.setId(Long.parseLong(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_CN+zarostek).getEnumValuesData().getSelectedId()));
								pf.setZadaniaProjektowe(zp);
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_CN+zarostek).getStringData().equals("")){
								ZadaniaProjektowe zp=new ZadaniaProjektowe();
								zp.setId(Long.parseLong(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_CN+zarostek).getStringData()));
								pf.setZadaniaProjektowe(zp);
								czyJest=true;
							}

						}
					}
				
						if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZRODLO_CN+zarostek)!=null){
							if(DWR&&slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZRODLO_CN+zarostek).getEnumValuesData()!=null){
								if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZRODLO_CN+zarostek).getEnumValuesData().getSelectedId()!=null
										&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZRODLO_CN+zarostek).getEnumValuesData().getSelectedId().equals("")){
									DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_POZYCJE_ZRODLO);
									EnumItem ei=d.getEnumItem(Integer.parseInt(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZRODLO_CN+zarostek).getEnumValuesData().getSelectedId()));
									if(ei!=null&&ei.getCn()!=null&&!ei.getCn().equals("")){
										SourcesOfProjectFundingDB sopfDB=SourcesOfProjectFundingDBDAO.findByErpIDUnique(Long.valueOf(ei.getId()));
										if(sopfDB!=null){
											ZrodloFinansowaniaProjektow bko=HibernateToCanonicalMapper.MapSourcesOfProjectFundingDBToZrodloFinansowaniaProjektow(sopfDB);
											pf.setZrodlo(bko);
											czyJest=true;
										}
									}
								}
							}
							else if(!DWR){
								if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZRODLO_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZRODLO_CN+zarostek).getStringData().equals("")){
									DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_POZYCJE_ZRODLO);
									EnumItem ei=d.getEnumItem(Integer.parseInt(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZRODLO_CN+zarostek).getStringData()));
									if(ei!=null&&ei.getCn()!=null&&!ei.getCn().equals("")){
										SourcesOfProjectFundingDB sopfDB=SourcesOfProjectFundingDBDAO.findByErpIDUnique(Long.valueOf(ei.getId()));
										if(sopfDB!=null){
											ZrodloFinansowaniaProjektow bko=HibernateToCanonicalMapper.MapSourcesOfProjectFundingDBToZrodloFinansowaniaProjektow(sopfDB);
											pf.setZrodlo(bko);
											czyJest=true;
										}
									}
								}
							}
						}
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZLECENIE_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZLECENIE_CN+zarostek).getEnumValuesData()!=null){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZLECENIE_CN+zarostek).getEnumValuesData().getSelectedId()!=null
									&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZLECENIE_CN+zarostek).getEnumValuesData().getSelectedId().equals("")){
								ZleceniaProdukcyjne zl=new ZleceniaProdukcyjne();
								zl.setId(Long.parseLong(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZLECENIE_CN+zarostek).getEnumValuesData().getSelectedId()));
								pf.setZlecenie(zl);
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZLECENIE_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZLECENIE_CN+zarostek).getStringData().equals("")){
								ZleceniaProdukcyjne zl=new ZleceniaProdukcyjne();
								zl.setId(Long.parseLong(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZLECENIE_CN+zarostek).getStringData()));
								pf.setZlecenie(zl);
								czyJest=true;
							}

						}
					}
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PROJEKT_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PROJEKT_CN+zarostek).getEnumValuesData()!=null){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PROJEKT_CN+zarostek).getEnumValuesData().getSelectedId()!=null
									&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PROJEKT_CN+zarostek).getEnumValuesData().getSelectedId().equals("")){
								Projekt pr=null;
								Long projId=Long.parseLong(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PROJEKT_CN+zarostek).getEnumValuesData().getSelectedId());
								ProjectDB projDB=ProjectDBDAO.findByErpIDUnique(projId);
								if(projDB!=null)pr=HibernateToCanonicalMapper.MapProjectDBToProjekt(projDB);
								pf.setProjekt(pr);
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PROJEKT_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PROJEKT_CN+zarostek).getStringData().equals("")){
								Projekt pr=null;
								Long projId=Long.parseLong(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PROJEKT_CN+zarostek).getStringData());
								ProjectDB projDB=ProjectDBDAO.findByErpIDUnique(projId);
								if(projDB!=null)pr=HibernateToCanonicalMapper.MapProjectDBToProjekt(projDB);
								pf.setProjekt(pr);
								czyJest=true;
							}

						}
					}
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWALIFIKACJA_CN+zarostek)!=null){
						if(DWR){
							EnumValues ev=slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWALIFIKACJA_CN+zarostek).getEnumValuesData();
							if(ev.getSelectedId()!=null&&!ev.getSelectedId().equals("")){
								pf.setKwalifikacja(Integer.parseInt(ev.getSelectedId()));
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWALIFIKACJA_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWALIFIKACJA_CN+zarostek).getStringData().equals("")){
								pf.setKwalifikacja(Integer.parseInt(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWALIFIKACJA_CN+zarostek).getStringData()));
								czyJest=true;
							}
						}

					}
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_FINANSOWE_CN+zarostek)!=null){
						if(DWR){
							EnumValues ev=slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_FINANSOWE_CN+zarostek).getEnumValuesData();
							if(ev.getSelectedId()!=null&&!ev.getSelectedId().equals("")){
								Zrodlo zrodlo=new Zrodlo();
								zrodlo.setId(Long.parseLong(ev.getSelectedId().toString()));
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_FINANSOWE_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_FINANSOWE_CN+zarostek).getStringData().equals("")){
								Zrodlo zrodlo=new Zrodlo();
								zrodlo.setId(Long.parseLong(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_FINANSOWE_CN+zarostek).getStringData()));
								czyJest=true;
							}
						}
					}
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_STAWKA_CN+zarostek)!=null){
						if(DWR){
							EnumValues ev=slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_STAWKA_CN+zarostek).getEnumValuesData();
							if(ev.getSelectedId()!=null&&!ev.getSelectedId().equals("")){
								if(ev!=null){
									StawkaVat sv=StawkaVatDAO.getInstance().getStawkaVat();
									sv.setId(Long.parseLong(ev.getSelectedId().toString()));
									pf.setStawkaVat(sv);
									czyJest=true;
								}
							}}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_STAWKA_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_STAWKA_CN+zarostek).getStringData().equals("")){
								StawkaVat sv=StawkaVatDAO.getInstance().getStawkaVat();
								sv.setId(Long.parseLong(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_STAWKA_CN+zarostek).getStringData()));
								pf.setStawkaVat(sv);
								czyJest=true;
							}

						}
					}
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTANETTO_CN+zarostek)!=null){
						if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTANETTO_CN+zarostek).getMoneyData()!=null){
							pf.setKwotaNetto(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTANETTO_CN+zarostek).getMoneyData());
							czyJest=true;
						}
					}
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTAVAT_CN+zarostek)!=null){
						if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTAVAT_CN+zarostek).getMoneyData()!=null){
							pf.setKwotaVat(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTAVAT_CN+zarostek).getMoneyData());
							czyJest=true;
						}
					}
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTABRUTTO_CN+zarostek)!=null){
						if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTABRUTTO_CN+zarostek).getMoneyData()!=null){
							pf.setKwotaBrutto(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTABRUTTO_CN+zarostek).getMoneyData());
							czyJest=true;
						}
					}
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_OPK_CN+zarostek)!=null){
						if(DWR){
							EnumValues ev=slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_OPK_CN+zarostek).getEnumValuesData();
							if(ev.getSelectedId()!=null&&!ev.getSelectedId().equals("")){
								OPK opk=new OPK();
								opk.setId(Long.parseLong(ev.getSelectedId().toString()));
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_OPK_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_OPK_CN+zarostek).getStringData().equals("")){
								OPK opk=new OPK();
								opk.setId(Long.parseLong(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_OPK_CN+zarostek).getStringData()));
								czyJest=true;
							}
						}
					}
					/*if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PRZENZACZENIE_CN+zarostek)!=null){
							PrzeznaczenieZakupu
							czyJest=true;
						}*/
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POZFAKTURY_CN+zarostek)!=null){
						if(DWR){
							EnumValues ev=slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POZFAKTURY_CN+zarostek).getEnumValuesData();
							if(ev.getSelectedId()!=null&&!ev.getSelectedId().equals("")){

								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POZFAKTURY_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_POZFAKTURY_CN+zarostek).getStringData().equals("")){

								czyJest=true;
							}
						}
					}
					if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PRZENZACZENIE_CN+zarostek)!=null){
						if(DWR){
							EnumValues ev=slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PRZENZACZENIE_CN+zarostek).getEnumValuesData();
							if(ev.getSelectedId()!=null&&!ev.getSelectedId().equals("")){
								PrzeznaczenieZakupu pz=new PrzeznaczenieZakupu();
								pz.setId(Long.parseLong(ev.getSelectedId().toString()));
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PRZENZACZENIE_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PRZENZACZENIE_CN+zarostek).getStringData().equals("")){
								PrzeznaczenieZakupu pz=new PrzeznaczenieZakupu();
								pz.setId(Long.parseLong(slownik.get(slownikPrzedrostek+POZYCJE_FAKTURY_PRZENZACZENIE_CN+zarostek).getStringData()));
								czyJest=true;
							}
						}

					}
					if(czyJest)
						pozycje.add(pf);
					licznik++;
					zarostek="_"+Integer.toString(licznik);
				}
				f.setPozycjeFaktury(pozycje);
			}

			if(values.containsKey(przedrostek+MPK_CN)){
				List<BudzetPozycja> pozycje=new ArrayList<BudzetPozycja>();
				Map<String,FieldData> slownik=values.get(przedrostek+MPK_CN).getDictionaryData();

				int licznik=1;
				String zarostek="_"+Integer.toString(licznik);
				boolean czyJest=true;
				String slownikPrzedrostek="";
				if(DWR)slownikPrzedrostek=MPK_CN+"_";
				else slownikPrzedrostek="";
				for(int i=0;i<slownik.size()&&czyJest;i++){
					czyJest=false;
					BudzetPozycja bp=new BudzetPozycja();
					if(slownik.get(slownikPrzedrostek+MPK_ID_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+MPK_ID_CN+zarostek).getIntegerData()!=null){
							bp.setId(Long.parseLong(slownik.get(slownikPrzedrostek+MPK_ID_CN+zarostek).getIntegerData().toString()));
							czyJest=true;
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+MPK_ID_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+MPK_ID_CN+zarostek).getStringData().equals("")){
								bp.setId(Long.parseLong(slownik.get(slownikPrzedrostek+MPK_ID_CN+zarostek).getStringData()));
								czyJest=true;
							}
						}
					}
					if(slownik.get(slownikPrzedrostek+MPK_OKRESBUDZETOWY_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+MPK_OKRESBUDZETOWY_CN+zarostek).getEnumValuesData()!=null){
							if(slownik.get(slownikPrzedrostek+MPK_OKRESBUDZETOWY_CN+zarostek).getEnumValuesData().getSelectedId()!=null
									&&!slownik.get(slownikPrzedrostek+MPK_OKRESBUDZETOWY_CN+zarostek).getEnumValuesData().getSelectedId().equals("")){
								bp.setOkresBudzetowy(Long.parseLong(slownik.get(slownikPrzedrostek+MPK_OKRESBUDZETOWY_CN+zarostek).getEnumValuesData().getSelectedId()));
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+MPK_OKRESBUDZETOWY_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+MPK_OKRESBUDZETOWY_CN+zarostek).getStringData().equals("")){
								bp.setOkresBudzetowy(Long.parseLong(slownik.get(slownikPrzedrostek+MPK_OKRESBUDZETOWY_CN+zarostek).getStringData()));
								czyJest=true;
							}							
						}
					}
					if(slownik.get(slownikPrzedrostek+MPK_BUDZET_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+MPK_BUDZET_CN+zarostek).getEnumValuesData()!=null){
							if(slownik.get(slownikPrzedrostek+MPK_BUDZET_CN+zarostek).getEnumValuesData().getSelectedId()!=null
									&&!slownik.get(slownikPrzedrostek+MPK_BUDZET_CN+zarostek).getEnumValuesData().getSelectedId().equals("")){
								DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_MPK_BUDZET);
								EnumItem ei=d.getEnumItem(Integer.parseInt(slownik.get(slownikPrzedrostek+MPK_BUDZET_CN+zarostek).getEnumValuesData().getSelectedId()));
								if(ei!=null&&ei.getCn()!=null&&!ei.getCn().equals("")){
									List<BudzetKOKomorekDB>lista=BudzetKOKomorkaDBDAO.findByErpId(Long.valueOf(ei.getId()));
									if(lista.size()>0){
										BudzetKOKomorek bko=HibernateToCanonicalMapper.MapBudzetKOKomorekDBToBudzetKOKomorek(lista.get(lista.size()-1));
										bp.setBudzet(bko);
										czyJest=true;
									}
								}
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+MPK_BUDZET_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+MPK_BUDZET_CN+zarostek).getStringData().equals("")){
								DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_MPK_BUDZET);
								EnumItem ei=d.getEnumItem(Integer.parseInt(slownik.get(slownikPrzedrostek+MPK_BUDZET_CN+zarostek).getStringData()));
								if(ei!=null&&ei.getCn()!=null&&!ei.getCn().equals("")){
									List<BudzetKOKomorekDB>lista=BudzetKOKomorkaDBDAO.findByErpId(Long.valueOf(ei.getId()));
									if(lista.size()>0){
										BudzetKOKomorek bko=HibernateToCanonicalMapper.MapBudzetKOKomorekDBToBudzetKOKomorek(lista.get(lista.size()-1));
										bp.setBudzet(bko);
										czyJest=true;
									}
								}
							}
						}
					}
					if(slownik.get(slownikPrzedrostek+MPK_POZYCJA_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+MPK_POZYCJA_CN+zarostek).getEnumValuesData()!=null){
							if(slownik.get(slownikPrzedrostek+MPK_POZYCJA_CN+zarostek).getEnumValuesData().getSelectedId()!=null
									&&!slownik.get(slownikPrzedrostek+MPK_POZYCJA_CN+zarostek).getEnumValuesData().getSelectedId().equals("")){
								BudzetPozycjaSimple pb=new BudzetPozycjaSimple();
								pb.setId(Long.parseLong(slownik.get(slownikPrzedrostek+MPK_POZYCJA_CN+zarostek).getEnumValuesData().getSelectedId()));
								bp.setPozycjaZBudzetu(pb);
								czyJest=true;

							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+MPK_POZYCJA_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+MPK_POZYCJA_CN+zarostek).getStringData().equals("")){
								BudzetPozycjaSimple pb=new BudzetPozycjaSimple();
								pb.setId(Long.parseLong(slownik.get(slownikPrzedrostek+MPK_POZYCJA_CN+zarostek).getStringData()));
								bp.setPozycjaZBudzetu(pb);
								czyJest=true;
							}
						}
					}
					if(slownik.get(slownikPrzedrostek+MPK_KWOTANETTO_CN+zarostek)!=null){
						if(slownik.get(slownikPrzedrostek+MPK_KWOTANETTO_CN+zarostek).getMoneyData()!=null){
							bp.setKwotaNetto(slownik.get(slownikPrzedrostek+MPK_KWOTANETTO_CN+zarostek).getMoneyData());
							czyJest=true;
						}
					}
					if(slownik.get(slownikPrzedrostek+MPK_KWOTAVAT_CN+zarostek)!=null){
						if(slownik.get(slownikPrzedrostek+MPK_KWOTAVAT_CN+zarostek).getMoneyData()!=null){
							bp.setKwotaVat(slownik.get(slownikPrzedrostek+MPK_KWOTAVAT_CN+zarostek).getMoneyData());
							czyJest=true;
						}						
					}
					if(slownik.get(slownikPrzedrostek+MPK_DYSPONENT_CN+zarostek)!=null){
						if(DWR){
							EnumValues ev=slownik.get(slownikPrzedrostek+MPK_DYSPONENT_CN+zarostek).getEnumValuesData();
							if(ev.getSelectedOptions().size()>0&&ev.getSelectedOptions().get(0)!=null&&!ev.getSelectedOptions().get(0).equals("")){	
								bp.setDysponent(UserImpl.find(Long.parseLong(ev.getSelectedOptions().get(0))));
								czyJest=true;
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+MPK_DYSPONENT_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+MPK_DYSPONENT_CN+zarostek).getStringData().equals("")){
								bp.setDysponent(UserImpl.find(Long.parseLong(slownik.get(slownikPrzedrostek+MPK_DYSPONENT_CN+zarostek).getStringData())));
								czyJest=true;
							}
						}
					}
					if(slownik.get(slownikPrzedrostek+MPK_PROJEKT_CN+zarostek)!=null){
						if(DWR&&slownik.get(slownikPrzedrostek+MPK_PROJEKT_CN+zarostek).getEnumValuesData()!=null){
							if(slownik.get(slownikPrzedrostek+MPK_PROJEKT_CN+zarostek).getEnumValuesData().getSelectedId()!=null
									&&!slownik.get(slownikPrzedrostek+MPK_PROJEKT_CN+zarostek).getEnumValuesData().getSelectedId().equals("")){
								Projekt pr=null;
								Long projId=Long.parseLong(slownik.get(slownikPrzedrostek+MPK_PROJEKT_CN+zarostek).getEnumValuesData().getSelectedId());
								ProjectDB projDB=ProjectDBDAO.findByErpIDUnique(projId);
								if(projDB!=null)pr=HibernateToCanonicalMapper.MapProjectDBToProjekt(projDB);
								bp.setProjekt(pr);
								czyJest=true;			
							}
						}
						else if(!DWR){
							if(slownik.get(slownikPrzedrostek+MPK_PROJEKT_CN+zarostek).getData()!=null&&!slownik.get(slownikPrzedrostek+MPK_PROJEKT_CN+zarostek).getStringData().equals("")){
								Projekt pr=null;
								Long projId=Long.parseLong(slownik.get(slownikPrzedrostek+MPK_PROJEKT_CN+zarostek).getStringData());
								ProjectDB projDB=ProjectDBDAO.findByErpIDUnique(projId);
								if(projDB!=null)pr=HibernateToCanonicalMapper.MapProjectDBToProjekt(projDB);
								bp.setProjekt(pr);
								czyJest=true;		
							}
						}
					}

					if(czyJest)
						pozycje.add(bp);
					licznik++;
					zarostek="_"+Integer.toString(licznik);
				}
				f.setPozycjeBudzetowe(pozycje);
			}
			//Sposob wyplaty
			if(values.containsKey(przedrostek+SPOSOBWYPLATY_CN)){
				if(values.get(przedrostek+SPOSOBWYPLATY_CN)!=null){
					if(DWR){ 
						if(values.get(przedrostek+SPOSOBWYPLATY_CN).getEnumValuesData()!=null&&values.get(przedrostek+SPOSOBWYPLATY_CN).getEnumValuesData().getSelectedOptions().size()>0&&!values.get(przedrostek+SPOSOBWYPLATY_CN).getEnumValuesData().getSelectedOptions().get(0).toString().equals(""))
						{
							f.setSposobWyplaty(Integer.parseInt(values.get(przedrostek+SPOSOBWYPLATY_CN).getEnumValuesData().getSelectedOptions().get(0)));
						}
					}
					else{
						f.setSposobWyplaty(values.get(przedrostek+SPOSOBWYPLATY_CN).getIntegerData());
					}
				}
			}
			
			//czy szablony
			if(values.containsKey(przedrostek+CZYSZABLON_CN)){
				if(values.get(przedrostek+CZYSZABLON_CN)!=null){
					f.setCzySzablon(values.get(przedrostek+CZYSZABLON_CN).getBooleanData());
				}	
			}
			//szablony
			if(values.containsKey(przedrostek+SZABLONY_CN)){
				if(values.get(przedrostek+SZABLONY_CN)!=null){
					if(DWR){ 
						if(values.get(przedrostek+SZABLONY_CN).getEnumValuesData()!=null&&values.get(przedrostek+SZABLONY_CN).getEnumValuesData().getSelectedId()!=null&&!values.get(przedrostek+SZABLONY_CN).getEnumValuesData().getSelectedId().equals(""))
						{
							String szablonErpId=values.get(przedrostek+SZABLONY_CN).getEnumValuesData().getSelectedId();
							List<SzablonyRozliczenFakturDB> szabDBList= SzablonyRozliczenFakturDBDAO.findByErpId(Long.parseLong(szablonErpId));
							if(szabDBList.size()>0){
								SzablonyRozliczenFaktur szab=HibernateToCanonicalMapper.MapSzablonyRozliczenFakturDBDBToSzablonyRozliczenFaktur(szabDBList.get(0));
								f.setSzablon(szab);
							}
							
						}
					}
					else{
						f.setRodzajFaktury(values.get(przedrostek+SZABLONY_CN).getIntegerData());
					}
				}
			}



		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		//	e.printStackTrace();	

		} catch (Exception e) {
			log.error(e.getMessage(),e);
		//	e.printStackTrace();	

		}
		return f;
	}

/**
 * 
 * @param f
 * @param values
 * przepisuje wartosic(nie wszystkie) z ,pdelu faktury do dwr-owych values
 */
	public static void przepiszWartosciZFakturyDoValuesDWR(Faktura f,Map<String, FieldData> values)
	{
		boolean ctx=false;
		try {
			ctx=DSApi.openContextIfNeeded();
			przepiszWartosciZFakturyDoValues(f, values,true);
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		//	e.printStackTrace();
		}
		finally{
			DSApi.closeContextIfNeeded(ctx);
		}	
	}
	/*	public static void przepiszWartosciZFakturyDoValues(Faktura f,Map<String,Object> values) {

		Map<String,FieldData> mapa=zamienMapaObjectNaField(values);
		przepiszWartosciZFakturyDoValues(f,mapa,false);
		zamienMapaFieldNaObject(values,mapa);
	}*/

	//nie dziala dla dwr=false
	private static void przepiszWartosciZFakturyDoValues(Faktura f,Map<String, FieldData> values,boolean DWR) {
		String przedrostek="";
		if(DWR)przedrostek+="DWR_";
		try {
			DataBaseEnumField d;
			EnumValues ev;
			List<String> selected=new ArrayList<String>();
			selected.add(Integer.toString(f.getStatus()));
			ev=new EnumValues(zwrocMapeStatusow(),selected);
			if(DWR){
				values.put(przedrostek+STATUS_CN,new FieldData(Type.ENUM, ev));
			}
			else{
				values.put(przedrostek+STATUS_CN, new FieldData(Type.INTEGER,ev.getSelectedId()));
			}

			values.put(przedrostek+DATA_WPLYWU_CN,new FieldData(Type.DATE,f.getDataWplywu()));
			//	values.put(przedrostek+ADRESAT_CN,new FieldData(Type.DICTIONARY,toMapR(f.getAdresat())));
			values.put(przedrostek+KWOTA_NETTO_CN,new FieldData(Type.MONEY, f.getKwotaNetto()));
			values.put(przedrostek+KWOTA_BRUTTO_CN,new FieldData(Type.MONEY, f.getKwotaBrutto()));
			values.put(przedrostek+KWOTA_VAT_CN,new FieldData(Type.MONEY, f.getKwotaVat()));
			values.put(przedrostek+FAKTNAWYDZIALE_CN, new FieldData(Type.BOOLEAN,f.getFakturaNaWydziale()));

			if(f.getWaluta()!=null){
				d=DataBaseEnumField.getEnumFiledForTable(WIDOK_SIMPLE_WALUTA);
				ev=d.getEnumValuesForForcedPush(f.getWaluta().getId().toString());
				if(DWR)
					values.put(przedrostek+WALUTA_CN, new FieldData(Type.ENUM,ev));
				else
					values.put(przedrostek+WALUTA_CN, new FieldData(Type.INTEGER,ev.getSelectedId()));
			}
			values.put(przedrostek+TERMIN_PLATNOSCI_CN,new FieldData(Type.DATE,f.getTerminPlatnosci()));

			if(f.getRodzajFaktury()!=null){
				selected=new ArrayList<String>();
				selected.add(Integer.toString(f.getRodzajFaktury()));
				ev=new EnumValues(zwrocMapeRodzajowFaktury(),selected);
				if(DWR)
					values.put(przedrostek+RODZAJFAKTURY_CN,new FieldData(Type.ENUM, ev));
				else
					values.put(przedrostek+RODZAJFAKTURY_CN,new FieldData(Type.INTEGER,ev.getSelectedId() ));
				/*			if(f.getPelnomocnikDoSprawFinansowych().getId()!=null){
				ev=d.getEnumValuesForForcedPush(f.getPelnomocnikDoSprawFinansowych().getId().toString());
				values.put(przedrostek+PELNOMOCNIK_CN, new FieldData(Type.ENUM,ev));
			}*/
			}
			values.put(przedrostek+FAKTZAKOSZTYTRWALE_CN, new FieldData(Type.BOOLEAN,f.getFakturaZaKosztyTrwale()));
			values.put(przedrostek+NR_FAKTURY_CN, new FieldData(Type.STRING,f.getNumerFaktury()));
			values.put(przedrostek+BARCODE_CN, new FieldData(Type.STRING,f.getKodKreskowy()));
			//values.put(przedrostek+SENDER_CN, new FieldData(Type.DICTIONARY,toMapS(f.getWystawcaFaktury())));
			values.put(przedrostek+DATA_WYSTAWIENIA_CN,new FieldData(Type.DATE,f.getDataWystawienia()));
			values.put(przedrostek+KWOTA_W_WALUCIE_CN,new FieldData(Type.MONEY, f.getKwotaWWalucie()));
			values.put(przedrostek+KURS_CN,new FieldData(Type.MONEY, f.getKurs()));

			if(f.getTypPlatnosci()!=null){
				d=DataBaseEnumField.getEnumFiledForTable(WIDOK_SIMPLE_TYPPLATNOSCI);
				ev=d.getEnumValuesForForcedPush(Long.toString(f.getTypPlatnosci().getId()));
				if(DWR)
					values.put(przedrostek+TYPPLATNOSCI_CN, new FieldData(Type.ENUM,ev));
				else
					values.put(przedrostek+TYPPLATNOSCI_CN, new FieldData(Type.INTEGER,ev.getSelectedId()));
			}
			if(f.getNazwaTowaruUslugi()!=null){
				values.put(przedrostek+NAZWATOWUSL_CN,new FieldData(Type.STRING,f.getNazwaTowaruUslugi()));
			}
			if(f.getDodatkoweInformacje()!=null){
				values.put(przedrostek+DODATKOWEINFO_CN,new FieldData(Type.STRING,f.getDodatkoweInformacje()));
			}
			if(f.getKorektaNumer()!=null){
				values.put(przedrostek+KOREKTA_NUMER_CN,new FieldData(Type.STRING,f.getKorektaNumer()));
			}
			values.put(przedrostek+KOREKTA_CN, new FieldData(Type.BOOLEAN,f.getCzyKorekta()));
			values.put(przedrostek+WYDATEKSTRUKT_CN, new FieldData(Type.BOOLEAN,f.getWydatekJestStrukturalny()));
			values.put(przedrostek+SKIERUJUZUPOPIS_CN, new FieldData(Type.BOOLEAN,f.getSkierujDoUzupelnieniaOpisu()));
			values.put(przedrostek+ZAMNAMAG_CN, new FieldData(Type.BOOLEAN,f.getZamowienieNaMagazyn()));
			values.put(przedrostek+KWOTAPOZOST_CN,new FieldData(Type.MONEY,f.getKwotaPozostala()));
			values.put(przedrostek+KWOTAPOZOSTVAT_CN,new FieldData(Type.MONEY,f.getKwotaPozostalaVAT()));
			values.put(przedrostek+KWOTAPOZOSTNETTO_CN,new FieldData(Type.MONEY,f.getKwotaPozostalaNetto()));
			values.put(przedrostek+ZAAKCEPTOWANOPOZZAM_CN, new FieldData(Type.BOOLEAN,f.getZAAKCEPTOWANOPOZZAM()));
			values.put(przedrostek+ZAAKCEPTOWANO_WNIOSKI_O_REZERWACJE_CN, new FieldData(Type.BOOLEAN,f.getZaakceptowanoWnioskiORez()));
			values.put(przedrostek+CZYROZLICZENIEMEDIOW_CN, new FieldData(Type.BOOLEAN,f.getCzyRozliczenieMediow()));
			
			if(f.getPrzeznaczenieIUwagi()!=null){
				values.put(przedrostek+PRZEZNIUWAGI_CN,new FieldData(Type.STRING,f.getPrzeznaczenieIUwagi()));
			}
			if(f.getOpisMerytoryczny()!=null){
				values.put(przedrostek+OPISMERYT_CN,new FieldData(Type.STRING,f.getOpisMerytoryczny()));
			}
			if(f.getNumerPostepowaniaPrzetargowego()!=null){
				values.put(przedrostek+NUMERPOSTPRZETARG_CN,new FieldData(Type.STRING,f.getNumerPostepowaniaPrzetargowego()));
			}
			if(f.getWynikaZPostepowaniaPrzetargowego()!=null&&!f.getWynikaZPostepowaniaPrzetargowego().equals("")){
				selected=new ArrayList<String>();
				selected.add(Integer.toString(f.getWynikaZPostepowaniaPrzetargowego()));
				Map<String,String> mapa=new LinkedHashMap<String, String>();
				mapa.put("0", "NIE");
				mapa.put("1", "TAK");
				ev=new EnumValues(mapa,selected);
				if(DWR){
					values.put(przedrostek+WYNIKAZPOSTEPPRZETARG_CN,new FieldData(Type.ENUM, ev));
				}
				else{
					values.put(przedrostek+WYNIKAZPOSTEPPRZETARG_CN, new FieldData(Type.INTEGER,ev.getSelectedId()));
				}
			}
			if(f.getSposobWyplaty() != null){
				selected=new ArrayList<String>();
				selected.add(Integer.toString(f.getSposobWyplaty()));
				ev=new EnumValues(zwrocMapeSposobowWyplaty(),selected);
				if(DWR)
					values.put(przedrostek+SPOSOBWYPLATY_CN,new FieldData(Type.ENUM, ev));
				else
					values.put(przedrostek+SPOSOBWYPLATY_CN,new FieldData(Type.INTEGER,ev.getSelectedId() ));
			}

			/*if(f.getPozycjeFaktury()!=null&&f.getPozycjeFaktury().size()>0){
				Map<String,FieldData> slownik=values.get(przedrostek+POZYCJE_FAKTURY_CN).getDictionaryData();
				String slownikPrzedrostek=POZYCJE_FAKTURY_CN+"_";
				int licznik=1;
				String zarostek="_"+Integer.toString(licznik);


				for(PozycjaFaktury pf:f.getPozycjeFaktury()){
					if(pf.getIdPozycji()!=null){
						slownik.put(slownikPrzedrostek+POZYCJE_FAKTURY_ID_CN+zarostek,new FieldData(Type.INTEGER, pf.getIdPozycji()));
					}
					if(pf.getZadaniaProjektowe()!=null){
						slownik.put(slownikPrzedrostek+POZYCJE_FAKTURY_ZADANIA_CN+zarostek,new FieldData(Type.LONG,pf.getZadaniaProjektowe().getId()));
					}
					if(pf.getKwalifikacja()!=null){
						d=DataBaseEnumField.getEnumFiledForTable(WIDOK_SIMPLE_VAT);
						ev=d.getEnumValuesForForcedPush(pf.getKwalifikacja().toString());
						slownik.put(slownikPrzedrostek+POZYCJE_FAKTURY_KWALIFIKACJA_CN+zarostek, new FieldData(Type.ENUM,ev));
					}
					if(pf.getZrodlo()!=null){				
						slownik.put(slownikPrzedrostek+POZYCJE_FAKTURY_ZRODLO_CN+zarostek,new FieldData(Type.LONG,pf.getZrodlo().getId()));
					}
					if(pf.getKwotaNetto()!=null){
						slownik.put(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTANETTO_CN+zarostek,new FieldData(Type.MONEY,pf.getKwotaNetto()));
					}
					if(pf.getKwotaVat()!=null){
						slownik.put(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTAVAT_CN+zarostek, new FieldData(Type.MONEY,pf.getKwotaVat()));
					}
					if(pf.getKwotaBrutto()!=null){
						slownik.put(slownikPrzedrostek+POZYCJE_FAKTURY_KWOTABRUTTO_CN+zarostek, new FieldData(Type.MONEY,pf.getKwotaBrutto()));
					}
					if(pf.getOpk()!=null){
						slownik.put(slownikPrzedrostek+POZYCJE_FAKTURY_OPK_CN+zarostek, new FieldData(Type.LONG,pf.getOpk().getId()));
					}
					if(pf.getPrzeznaczenieZakupu()!=null){
						slownik.put(slownikPrzedrostek+POZYCJE_FAKTURY_PRZENZACZENIE_CN+zarostek, new FieldData(Type.LONG,pf.getPrzeznaczenieZakupu().getId()));
					}
					if(pf.getPozycjaZBudzetu()!=null){
						slownik.put(slownikPrzedrostek+POZYCJE_FAKTURY_POZFAKTURY_CN+zarostek,new FieldData(Type.LONG,pf.getPozycjaZBudzetu().getId()));
					}
					licznik++;
					zarostek="_"+Integer.toString(licznik);
				}
				FieldData fd= new FieldData();
				fd.setDictionaryData(slownik);
				values.put(przedrostek+POZYCJE_FAKTURY_CN,fd);

			}*/


		} catch (EdmException e) {
			log.error(e.getMessage(),e);
			//e.printStackTrace();
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			//e.printStackTrace();	

		}

	}

	public static Map<String,String> zwrocMapeStatusow(){
		Map<String,String> tmp=new LinkedHashMap<String, String>();
		for(StatusFaktury s:StatusFaktury.values()){
			tmp.put(s.name(),Integer.toString( s.zwrocId()));
		}
		return tmp;
	}
	public static Map<String,String> zwrocMapeRodzajowFaktury(){
		Map<String,String> tmp=new LinkedHashMap<String, String>();
		for(RodzajeFaktury s:RodzajeFaktury.values()){
			tmp.put(s.name(),Integer.toString( s.zwrocId()));
		}
		return tmp;
	}

	public static Map<String, String> zwrocMapeSposobowWyplaty() {
		Map<String,String> tmp = new LinkedHashMap<String,String>();
		for(SposobyWyplaty s: SposobyWyplaty.values()){
			tmp.put(s.name(), Integer.toString(s.zwrocId()));
		}
		return tmp;
	}
	
	public static Map<String, FieldData> toMapR( Recipient p) {
		Map<String, FieldData> map = new HashMap<String, FieldData>();

		map.put("title", new FieldData(Type.STRING,p.getTitle()));
		map.put("firstname", new FieldData(Type.STRING,p.getFirstname()));
		map.put("lastname", new FieldData(Type.STRING,p.getLastname()));
		map.put("organization", new FieldData(Type.STRING,p.getOrganization()));
		map.put("organizationDivision", new FieldData(Type.STRING,p.getOrganizationDivision()));
		map.put("street", new FieldData(Type.STRING,p.getStreet()));
		map.put("zip",  new FieldData(Type.STRING,p.getZip()));
		map.put("location",  new FieldData(Type.STRING,p.getLocation()));
		map.put("country",  new FieldData(Type.STRING,p.getCountry()));
		map.put("pesel",  new FieldData(Type.STRING,p.getPesel()));
		map.put("nip",  new FieldData(Type.STRING,p.getNip()));
		map.put("email",  new FieldData(Type.STRING,p.getEmail()));
		map.put("fax", new FieldData(Type.STRING,p.getFax()));
		map.put("remarks",  new FieldData(Type.STRING,p.getRemarks()));
		map.put("dictionaryType",  new FieldData(Type.STRING,p.getDictionaryType()));
		map.put("dictionaryGuid",  new FieldData(Type.STRING,p.getDictionaryGuid()));
		map.put("id", new FieldData(Type.LONG,p.getId()));
		map.put("adresSkrytkiEpuap",  new FieldData(Type.STRING,p.getAdresSkrytkiEpuap()));
		map.put("identifikatorEpuap",  new FieldData(Type.STRING,p.getIdentifikatorEpuap()));
		map.put("lparam",  new FieldData(Type.STRING,p.getLparam()));
		map.put("wparam",  new FieldData(Type.STRING,p.getLparam()));
		map.put("phoneNumber",  new FieldData(Type.STRING,p.getPhoneNumber()));
		map.put("externalID",  new FieldData(Type.STRING,p.getExternalID()));
		return map;
	}
	public static Map<String, FieldData> toMapS( Sender p) {
		Map<String, FieldData> map = new HashMap<String, FieldData>();

		map.put("title", new FieldData(Type.STRING,p.getTitle()));
		map.put("firstname", new FieldData(Type.STRING,p.getFirstname()));
		map.put("lastname", new FieldData(Type.STRING,p.getLastname()));
		map.put("organization", new FieldData(Type.STRING,p.getOrganization()));
		map.put("organizationDivision", new FieldData(Type.STRING,p.getOrganizationDivision()));
		map.put("street", new FieldData(Type.STRING,p.getStreet()));
		map.put("zip",  new FieldData(Type.STRING,p.getZip()));
		map.put("location",  new FieldData(Type.STRING,p.getLocation()));
		map.put("country",  new FieldData(Type.STRING,p.getCountry()));
		map.put("pesel",  new FieldData(Type.STRING,p.getPesel()));
		map.put("nip",  new FieldData(Type.STRING,p.getNip()));
		map.put("email",  new FieldData(Type.STRING,p.getEmail()));
		map.put("fax", new FieldData(Type.STRING,p.getFax()));
		map.put("remarks",  new FieldData(Type.STRING,p.getRemarks()));
		map.put("dictionaryType",  new FieldData(Type.STRING,p.getDictionaryType()));
		map.put("dictionaryGuid",  new FieldData(Type.STRING,p.getDictionaryGuid()));
		map.put("id", new FieldData(Type.LONG,p.getId()));
		map.put("adresSkrytkiEpuap",  new FieldData(Type.STRING,p.getAdresSkrytkiEpuap()));
		map.put("identifikatorEpuap",  new FieldData(Type.STRING,p.getIdentifikatorEpuap()));
		map.put("lparam",  new FieldData(Type.STRING,p.getLparam()));
		map.put("wparam",  new FieldData(Type.STRING,p.getLparam()));
		map.put("phoneNumber",  new FieldData(Type.STRING,p.getPhoneNumber()));
		map.put("externalID",  new FieldData(Type.STRING,p.getExternalID()));
		return map;
	}



	private static Map<String,FieldData> zamienMapaObjectNaField(Map<String,Object> values){
		Map<String,FieldData> dwr=new LinkedHashMap<String, FieldData>();


		for(String key:values.keySet()){
			if(values.get(key)!=null){
				Type t = null;
				if(values.get(key) instanceof String ){
					t=Type.STRING;
				}
				else if(values.get(key) instanceof Long ){
					t=Type.LONG;
				}
				else if(values.get(key) instanceof Integer ){
					t=Type.INTEGER;
				}
				else if(values.get(key) instanceof Date ){
					t=Type.DATE;
				}
				else if(values.get(key) instanceof EnumValues ){
					t=Type.ENUM;
				}
				else if(values.get(key) instanceof Attachment){
					t=Type.ATTACHMENT;
				}
				else if(values.get(key) instanceof BigDecimal){
					t=Type.MONEY;
				}
				else if(values.get(key) instanceof Boolean){
					t=Type.BOOLEAN;
				}
				else if(values.get(key) instanceof HashMap ){
					LinkedHashMap<String, HashMap<String,FieldData>> slowniki=new LinkedHashMap<String, HashMap<String,FieldData>>();
					LinkedHashMap<String, Integer> licznikiPozycji=new LinkedHashMap<String, Integer>(); 
					for(String dictKey:((HashMap<String,?>)values.get(key)).keySet()){

						if(key.equals("M_DICT_VALUES")){
							String kluczDict=dictKey.substring(0, dictKey.lastIndexOf("_"));
							if(slowniki.containsKey(kluczDict)){
								slowniki.get(kluczDict).putAll(dodajLicznik((HashMap<String,FieldData>)((HashMap<String,?>)values.get(key)).get(dictKey),licznikiPozycji.get(kluczDict)));
								licznikiPozycji.put(kluczDict,licznikiPozycji.get(kluczDict)+1);
							}
							else{							
								slowniki.put(kluczDict,dodajLicznik((HashMap<String,FieldData>)((HashMap<String,?>)values.get(key)).get(dictKey),1));
								licznikiPozycji.put(kluczDict, 2);
							}
						}else{
							dwr.put(dictKey, new FieldData(Type.DICTIONARY,((HashMap<String,?>)values.get(key)).get(dictKey)));
						}
					}
					if(slowniki.size()>0){
						for(String doWstawienia:slowniki.keySet()){
							dwr.put(doWstawienia, new FieldData(Type.DICTIONARY,slowniki.get(doWstawienia)));
						}
					}
				}

				if(t!=null)
					dwr.put(key, new FieldData(t,values.get(key)));
			}

		}
		return dwr;
	}

	private static void zamienMapaFieldNaObject(Map<String,Object>values,Map<String,FieldData> valuesDWR){

		for(String key:valuesDWR.keySet()){
			values.put(key, (Object)valuesDWR.get(key));
		}
	}

	private static HashMap<String,FieldData> dodajLicznik(HashMap<String,FieldData> pozycje,int numer){

		HashMap<String,FieldData> nowePoz=new LinkedHashMap<String, FieldData>();

		for(String key:pozycje.keySet()){
			nowePoz.put(key+"_"+numer,pozycje.get(key));
		}
		return nowePoz;
	}


}
