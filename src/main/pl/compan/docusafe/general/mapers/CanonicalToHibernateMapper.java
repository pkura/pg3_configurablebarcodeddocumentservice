package pl.compan.docusafe.general.mapers;

import java.sql.Date;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.general.ObjectUtils.STATUS_OBIEKTU;
import pl.compan.docusafe.general.canonical.dao.WniosekORezerwacjeDAO;
import pl.compan.docusafe.general.canonical.dao.ZamowienieSimplePozycjeDAO;
import pl.compan.docusafe.general.canonical.model.Budzet;
import pl.compan.docusafe.general.canonical.model.BudzetDoKomorki;
import pl.compan.docusafe.general.canonical.model.BudzetKOKomorek;
import pl.compan.docusafe.general.canonical.model.BudzetPozycjaSimple;
import pl.compan.docusafe.general.canonical.model.NumeryZamowien;
import pl.compan.docusafe.general.canonical.model.OkresBudzetowy;
import pl.compan.docusafe.general.canonical.model.Faktura;
import pl.compan.docusafe.general.canonical.model.JednostkaMiary;
import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.canonical.model.Kontrahent;
import pl.compan.docusafe.general.canonical.model.KontrahentKonto;
import pl.compan.docusafe.general.canonical.model.MiejsceDostawy;
import pl.compan.docusafe.general.canonical.model.PlanZakupow;
import pl.compan.docusafe.general.canonical.model.Pracownik;
import pl.compan.docusafe.general.canonical.model.ProduktyFaktury;
import pl.compan.docusafe.general.canonical.model.ProjectAccountPiatki;
import pl.compan.docusafe.general.canonical.model.Projekt;
import pl.compan.docusafe.general.canonical.model.ProjektBudzet;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetEtap;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetZasob;
import pl.compan.docusafe.general.canonical.model.RodzajePlanowZakupu;
import pl.compan.docusafe.general.canonical.model.RoleWBudzecie;
import pl.compan.docusafe.general.canonical.model.RoleWProjekcie;
import pl.compan.docusafe.general.canonical.model.StawkaVat;
import pl.compan.docusafe.general.canonical.model.SzablonyRozliczenFaktur;
import pl.compan.docusafe.general.canonical.model.TypDokumentu;
import pl.compan.docusafe.general.canonical.model.TypPlatnosci;
import pl.compan.docusafe.general.canonical.model.TypyUrlopow;
import pl.compan.docusafe.general.canonical.model.TypyWnioskowORezerwacje;
import pl.compan.docusafe.general.canonical.model.Waluta;
import pl.compan.docusafe.general.canonical.model.WniosekORezerwacje;
import pl.compan.docusafe.general.canonical.model.ZadaniaFinansowe;
import pl.compan.docusafe.general.canonical.model.ZamowienieSimplePozycje;
import pl.compan.docusafe.general.canonical.model.ZleceniaProdukcyjne;
import pl.compan.docusafe.general.canonical.model.Zrodlo;
import pl.compan.docusafe.general.canonical.model.ZrodloFinansowaniaProjektow;
import pl.compan.docusafe.general.canonical.model.exception.LoginNotFoundException;
import pl.compan.docusafe.general.hibernate.model.BudgetViewDB;
import pl.compan.docusafe.general.hibernate.model.BudzetDoKomorkiDB;
import pl.compan.docusafe.general.hibernate.model.BudzetKOKomorekDB;
import pl.compan.docusafe.general.hibernate.model.BudzetPozycjaSimpleDB;
import pl.compan.docusafe.general.hibernate.model.NumeryZamowienDB;
import pl.compan.docusafe.general.hibernate.model.OkresBudzetowyDB;
import pl.compan.docusafe.general.hibernate.model.CostInvoiceProductDB;
import pl.compan.docusafe.general.hibernate.model.CurrencyDB;
import pl.compan.docusafe.general.hibernate.model.DeliveryLocationDB;
import pl.compan.docusafe.general.hibernate.model.FakturaDB;
import pl.compan.docusafe.general.hibernate.model.FinancialTaskDB;
import pl.compan.docusafe.general.hibernate.model.KontrahentDB;
import pl.compan.docusafe.general.hibernate.model.KontrahentKontoDB;
import pl.compan.docusafe.general.hibernate.model.PaymentTermsDB;
import pl.compan.docusafe.general.hibernate.model.PlanZakupowDB;
import pl.compan.docusafe.general.hibernate.model.PracownikDB;
import pl.compan.docusafe.general.hibernate.model.ProductionOrderDB;
import pl.compan.docusafe.general.hibernate.model.ProjectAccountPiatkiDB;
import pl.compan.docusafe.general.hibernate.model.ProjectDB;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetDB;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetEtapDB;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetZasobDB;
import pl.compan.docusafe.general.hibernate.model.PurchaseDocumentTypeDB;
import pl.compan.docusafe.general.hibernate.model.RodzajePlanowZakupuDB;
import pl.compan.docusafe.general.hibernate.model.RoleWBudzecieDB;
import pl.compan.docusafe.general.hibernate.model.RoleWProjekcieDB;
import pl.compan.docusafe.general.hibernate.model.SourcesOfProjectFundingDB;
import pl.compan.docusafe.general.hibernate.model.SzablonyRozliczenFakturDB;
import pl.compan.docusafe.general.hibernate.model.TypyUrlopowDB;
import pl.compan.docusafe.general.hibernate.model.TypyWnioskowORezerwacjeDB;
import pl.compan.docusafe.general.hibernate.model.UnitOfMeasureDB;
import pl.compan.docusafe.general.hibernate.model.VatRateDB;
import pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjeDB;
import pl.compan.docusafe.general.hibernate.model.ZamowienieSimplePozycjeDB;
import pl.compan.docusafe.general.hibernate.model.ZrodloDB;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa odpowiedzialna za mapowanie modelu kanonicznego [Canonical] na
 * hibernate [HBM]
 * 
 * @author Gubari Nanokata
 * 
 */
public class CanonicalToHibernateMapper {

	public static final Logger log = LoggerFactory.getLogger(CanonicalToHibernateMapper.class);
	
	
	/**
	 * Metoda mapuje obiekt JednostkaOrganizacyjna [CAN] na DSDiviwion [HBM]
	 * Obiekt DSDivision nie ma mo�liwo�ci ustawienia ID, dlatego trzeba go
	 * poda� jako parametr - w przypadku aktualizacji danych - wczytujemy go z
	 * bazy i nast�pnie mapujemy na jo. Mapper nie mapuje setParent - rodzica -
	 * nale�y go wczyta� z bazy danych i przypi��.
	 * 
	 * @param jo
	 * @return
	 */
	public static DSDivision MapJednostkaOrganizacyjnaNaDSDivision(
			JednostkaOrganizacyjna jo, DSDivision dv) {
		// Aktywna
		dv.setHidden(!jo.isAktywna());
		// Id - nie mapujemy - mapowanie odbywa si� na wczytanym (z ID) obiektem

		// Nazwa
		dv.setName(jo.getNazwa());

		// Zewn�trzny identyfikator
		dv.setExternalId(jo.getIdn());

		// Kod
		dv.setCode(jo.getKod());
		// Opis
		dv.setDescription(jo.getOpis());

		// Guid
		dv.setGuid(jo.getGuid());

		// Division typ
		dv.setDivisionType(DSDivision.TYPE_DIVISION);

		return dv;
	}

	public static CurrencyDB MapWalutaNaCurrencyDB(CurrencyDB c, Waluta w) {
		// Aktywna
		c.setAvailable(MapperUtils.getIntFromBoolean(w.isAktywna()));
		// cn
		c.setCn(w.getCn());
		// Nazwa
		c.setTitle(w.getNazwa());
		return c;
	}

	public static PaymentTermsDB MapTypPlatnosciNaPaymentTerms(
			PaymentTermsDB pt, TypPlatnosci tp) {
		// Aktywna
		pt.setAvailable(MapperUtils.getIntFromBoolean(tp.isAktywna()));
		// cn
		pt.setCn(tp.getCn());
		// Nazwa
		pt.setTitle(tp.getNazwa());
		// Obiekt powiazany
		pt.setRefValue(tp.getObiektPowiazany());
		return pt;
	}
	
	public static KontrahentDB MapKontrahentNaKontrahentDB(KontrahentDB kontrahentDB, Kontrahent kontrahent)
	{

		kontrahentDB.setNazwa(kontrahent.getNazwa());
		kontrahentDB.setKodpocztowy(kontrahent.getKodpocztowy());
		kontrahentDB.setMiejscowosc(kontrahent.getMiejscowosc());
		kontrahentDB.setNip(kontrahent.getNip());
		kontrahentDB.setNrDomu(kontrahent.getNrDomu());
		kontrahentDB.setMiejscowosc(kontrahent.getMiejscowosc());
		kontrahentDB.setNrMieszkania(kontrahent.getNrMieszkania());
		kontrahentDB.setUlica(kontrahent.getUlica());
		kontrahentDB.setNowy(kontrahent.getNowy());
		kontrahentDB.setErpIid(kontrahent.getErpId());
		kontrahentDB.setKraj(kontrahent.getKraj());
		kontrahentDB.setNrKonta(kontrahent.getNrKonta());
		return kontrahentDB;
	}

	public static DeliveryLocationDB MapMiejsceDostawyNaDeliveryLocationDB(
			DeliveryLocationDB dl, MiejsceDostawy md) {
		dl.setCn(md.getCn());
		// Nazwa
		dl.setTitle(md.getNazwa());
		dl.setAvailable(MapperUtils.getIntFromBoolean(md.isAktywna()));
		return dl;
	}

	public static PurchaseDocumentTypeDB MapTypDokumentuToPurchaseDocumentTypeDB(
			PurchaseDocumentTypeDB pd, TypDokumentu td) {

		// Aktywna
		pd.setAvailable(MapperUtils.getIntFromBoolean(td.isAktywna()));
		pd.setCn(td.getCn());
		// Nazwa
		pd.setTitle(td.getNazwa());
		pd.setCzywal(td.getCzywal());
		pd.setSymbol_waluty(td.getSymbol_waluty());
		pd.setTypdokzak_id(td.getTypdokzak_id());
		pd.setTypdokIdn(td.getTypdokIdn());
		return pd;
	}

	public static VatRateDB MapStawkaVatToVatRateDB(VatRateDB vr, StawkaVat sv) {
		// Aktywna
		vr.setAvailable(MapperUtils.getIntFromBoolean(sv.getAktywna()));
		vr.setCn(sv.getCn());
		// Nazwa
		vr.setTitle(sv.getNazwa());
		vr.setCzy_zwolniona(sv.getCzy_zwolniona());
		vr.setErp_id(sv.getErp_id());
		vr.setNie_podlega(sv.getNie_podlega());
		vr.setIdm(sv.getIdm());
		return vr;
	}

	public static CostInvoiceProductDB MapProduktyFakturyToCostInvoiceProductDB(
			CostInvoiceProductDB cp, ProduktyFaktury pf) {
		cp.setAvailable(MapperUtils.getIntFromBoolean(pf.getAktywna()));
		cp.setCn(pf.getCn());
		cp.setTitle(pf.getNazwa());
		cp.setKlasyWytwIdn(pf.getKlasyWytwIdn());
		cp.setJmIdn(pf.getJmIdn());
		cp.setRodzTowaru(pf.getRodzTowaru());
		cp.setVatStawIds(pf.getVatStawIds());
		// erp_id
		cp.setWytworId(pf.getWytworId());
		return cp;
	}

	public static UnitOfMeasureDB MapJednostkaMiaryNaUnitOfMeasureDB(
			UnitOfMeasureDB uom, JednostkaMiary jm) {
		uom.setAvailable(MapperUtils.getIntFromBoolean(jm.getAktywna()));
		uom.setCn(jm.getCn());
		uom.setErpId(jm.getErpId());
		uom.setJmtyp_idn(jm.getJmtyp_idn());
		uom.setPrecyzjajm(jm.getPrecyzjajm());
		uom.setTitle(jm.getTitle());
		return uom;
	}

	public static FakturaDB MapFakturaToFakturaDB(Faktura faktura) {
		FakturaDB fakturaDB = new FakturaDB();

		fakturaDB.setAdresat(faktura.getAdresat());
		fakturaDB.setCzyKorekta(faktura.getCzyKorekta());
		fakturaDB.setKorektaNumer(faktura.getKorektaNumer());
		fakturaDB.setDataWplywu(new Date(faktura.getDataWplywu().getTime()));
		fakturaDB.setDataWystawienia(new Date(faktura.getDataWystawienia()
				.getTime()));
		fakturaDB.setDodatkoweInformacje(faktura.getDodatkoweInformacje());
//		fakturaDB.setDziennik(faktura.getDziennik());
		fakturaDB.setFakturaNaWydziale(faktura.getFakturaNaWydziale());
		fakturaDB.setGlownyKsiegowy(faktura.getGlownyKsiegowy());
		fakturaDB.setId(faktura.getId());
		fakturaDB.setKodKreskowy(faktura.getKodKreskowy());
		fakturaDB.setKurs(faktura.getKurs());
		fakturaDB.setKwotaBrutto(faktura.getKwotaBrutto());
		fakturaDB.setKwotaNetto(faktura.getKwotaNetto());
		fakturaDB.setKwotaPozostala(faktura.getKwotaPozostala());
		fakturaDB.setKwotaVat(faktura.getKwotaVat());
		fakturaDB.setKwotaWWalucie(faktura.getKwotaWWalucie());
		fakturaDB.setNazwaTowaruUslugi(faktura.getNazwaTowaruUslugi());
		fakturaDB.setKorektaNumer(faktura.getKorektaNumer());
		fakturaDB.setNumerFaktury(faktura.getNumerFaktury());
		fakturaDB.setNumerPostepowaniaPrzetargowego(faktura
				.getNumerPostepowaniaPrzetargowego());
//		fakturaDB.setNumerPrzesylkiRejestrowanej(faktura
//				.getNumerPrzesylkiRejestrowanej());
		fakturaDB.setOpisMerytoryczny(faktura.getOpisMerytoryczny());
		fakturaDB.setOsobaUzupelniajacaOpisFaktury(faktura
				.getOsobaUzupelniajacaOpisFaktury());
		fakturaDB.setPelnomocnikDoSprawFinansowych(faktura
				.getPelnomocnikDoSprawFinansowych());
		fakturaDB.setPotwierdzamOdbiorZgodnieZFaktura(faktura
				.getPotwierdzamOdbiorZgodnieZFaktura());
		fakturaDB.setPotwierdzamOdbiorZgodnieZUmowa(faktura
				.getPotwierdzamOdbiorZgodnieZUmowa());
		fakturaDB.setPracownikKsiegowosci(faktura.getPracownikKsiegowosci());
		fakturaDB.setPrzeznaczenieIUwagi(faktura.getPrzeznaczenieIUwagi());
		fakturaDB.setRodzajFaktury(faktura.getRodzajFaktury());
		fakturaDB.setSkierujDoUzupelnieniaOpisu(faktura
				.getSkierujDoUzupelnieniaOpisu());
		fakturaDB.setStanowiskoDostaw(MapMiejsceDostawyNaDeliveryLocationDB(
				new DeliveryLocationDB(), faktura.getStanowiskoDostaw()));
		fakturaDB.setStatus(faktura.getStatus());
		fakturaDB.setTerminPlatnosci(new Date(faktura.getTerminPlatnosci()
				.getTime()));
		fakturaDB.setTrybPostepowania(faktura.getTrybPostepowania());

		if (faktura.getTypFaktury().getCzywal()) {
			fakturaDB.setTypFakturyWal(MapTypDokumentuToPurchaseDocumentTypeDB(
					new PurchaseDocumentTypeDB(), faktura.getTypFaktury()));
			fakturaDB.setTypFakturyPln(null);
		} else {
			fakturaDB.setTypFakturyPln(MapTypDokumentuToPurchaseDocumentTypeDB(
					new PurchaseDocumentTypeDB(), faktura.getTypFaktury()));
			fakturaDB.setTypFakturyWal(null);
		}

		fakturaDB.setTypPlatnosci(MapTypPlatnosciNaPaymentTerms(
				new PaymentTermsDB(), faktura.getTypPlatnosci()));
		fakturaDB.setWaluta(MapWalutaNaCurrencyDB(new CurrencyDB(),
				faktura.getWaluta()));
		fakturaDB.setWydatekJestStrukturalny(faktura
				.getWydatekJestStrukturalny());
	//	fakturaDB.setWydatekStrukturalnyPoz(new Set(faktura.getWydatkiStrukturalne()));
		fakturaDB.setWynikaZPostepowaniaPrzetargowego(faktura
				.getWynikaZPostepowaniaPrzetargowego());
		fakturaDB.setWystawcaFaktury(MapKontrahentNaKontrahentDB(
				new KontrahentDB(), faktura.getWystawcaFaktury()));
		fakturaDB.setZalacznik(faktura.getZalacznik());
//		fakturaDB.setZamowienieNaMagazyn(faktura.getZamowienieNaMagazyn());
		fakturaDB.setSposobWyplaty(faktura.getSposobWyplaty());
		fakturaDB.setCzyRozliczenieMediow(faktura.getCzyRozliczenieMediow());
		fakturaDB.setCzyZZamowienia(faktura.getCzyZZamowienia());
		fakturaDB.setWygenerowanoMetryczke(faktura.getWygenerowanoMetryczke());
		
		return fakturaDB;
	}

	/**
	 * Metoda mapuje obiekt Pracownik [CAN] na DSUser [HB]
	 * W zwi�zku z zale�no�ciami w systemie obiekt dsUser nie mo�e istnie� w oderwaniu od bazy danych, st�d te�
	 * w przypadku mapowania. Obiekt Pracownik musi mie� ustawiony login.
	 * NOWY - zostaje utworzony automatycznie w bazie nowy u�ytkownik
	 * MODYFIKACJA - zostaje wczytany z bazy danych obiekt i nadpisany nowymi w�a�ciwo�ciami z mapowania
	 * 
	 * @param pracownik
	 * @return
	 * @throws LoginNotFoundException - obiekt Pracownik musi mie� ustawiony login
	 */
	public DSUser mapPracownikToDSUser(Pracownik pracownik) {

		DSUser dsUser = null;
				
		if (pracownik.getLogin()==null || pracownik.getLogin().equals("admin"))
		{
			return null;
		}
		
		// Pracownik nowy - tworzymy pusty obiekt, pracownik istnieje -
		// wczytujemy go
		if (pracownik.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
			try {
				try {
					 DSUser.findByUsername(pracownik
							.getLogin());
					 return null;
				} catch (UserNotFoundException unfe) {
					dsUser = UserFactory.getInstance().createUser(
							pracownik.getLogin(), pracownik.getImie(),
							pracownik.getNazwisko());
					dsUser = uzupelnijDanePracownika(pracownik, dsUser);
				}
			} catch (Exception e) {
				log.debug(e);
			}
		} else {
			try {
				dsUser = UserImpl.findByUsername(pracownik.getLogin());
				// Imi�
				if (pracownik.getImie()!=null)
				{
					dsUser.setFirstname(pracownik.getImie());
				}
								
				// Nazwisko
				if (pracownik.getNazwisko()!=null)
				{
					dsUser.setLastname(pracownik.getNazwisko());
				}
				
				//Uzupe�nienie pozosta�ych danych
				dsUser = uzupelnijDanePracownika(pracownik, dsUser);
				
			} catch (UserNotFoundException e) {
				log.debug("Pr�ba wyszukania u�ytkownika "
						+ pracownik.getLogin() + " zako�czona niepowodzeniem.");
				return null;
			}
			catch (Exception exc){
				log.debug(exc);
			}
		}

		if (dsUser != null) {
			// Pesel
			if (pracownik.getPesel() != null) {
				dsUser.setPesel(pracownik.getPesel());
			} else {
				dsUser.setPesel("00000000000");
			}
			// Stan obiektu
			if (pracownik.getStanObiektu() == STATUS_OBIEKTU.NOWY) {
				// Czy generujemy has�o dla u�ytkownika
				boolean generujHaslo = false;
				if (Docusafe
						.getAdditionProperty("IMPORT_PRACOWNIKOW_GENERUJ_HASLO") != null) {
					generujHaslo = new Boolean(
							Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_GENERUJ_HASLO"));
				}
				// Generowanie has�a
				if (generujHaslo) {
					try {
						dsUser.setPassword(UserFactory.hashPassword(pracownik
								.getHaslo()));
						// TODO: has�o mog�o by� ju� wykorzystane

					} catch (Exception e) {
						log.debug(e);
					}
				}
			}
		}
		return dsUser;
	}

	private DSUser uzupelnijDanePracownika(Pracownik pracownik, DSUser dsUser) {
		// Numer ewidencyjny ERP
		if (pracownik.getZewnetrznyIdm()!=null)
		{
			dsUser.setPracow_nrewid_erp(pracownik.getZewnetrznyIdm());
		}
		// Prefix domeny
		if (pracownik.getPrefixDomeny()!=null)
		{
			dsUser.setDomainPrefix(pracownik.getPrefixDomeny());
		}
		
		// Email
		if (pracownik.getAdresEmail()!=null)
		{
			dsUser.setEmail(pracownik.getAdresEmail());			
		}
		
		// Logowanie domenowe lub nie
		dsUser.setAdUser(pracownik.isLogowanieDomenowe());
		
		return dsUser;
	}

	
	/**
	 * Metoda mapuje obiekt budzet [CAN] na BudgetViewDB [HB]
	 * 
	 * @param budzet
	 * @return
	 */
	public static BudgetViewDB MapBudzetNaBudgetViewDB(Budzet budzet) {
		BudgetViewDB budzetView = new BudgetViewDB();
		
		budzetView.setId(budzet.getId());
		budzetView.setAvailable(budzet.isAvailable());
		budzetView.setBdBudzetId(budzet.getBdBudzetId());
		budzetView.setBdBudzetIdm(budzet.getBdBudzetIdm());
		budzetView.setBdBudzetRodzajId(budzet.getBdBudzetRodzajId());
		budzetView.setBdBudzetRodzajZadaniaId(budzet.getBdBudzetRodzajZadaniaId());
		budzetView.setBdBudzetRodzajZrodlaId(budzet.getBdBudzetRodzajZrodlaId());
		budzetView.setBdGrupaRodzajId(budzet.getBdGrupaRodzajId());
		budzetView.setBdRodzaj(budzet.getBdRodzaj());
		budzetView.setBdStrBudzetIdn(budzet.getBdStrBudzetIdn());
		budzetView.setBdStrBudzetIds(budzet.getBdStrBudzetIds());
		budzetView.setBdSzablonPozId(budzet.getBdSzablonPozId());
		budzetView.setBdZadanieId(budzet.getBdZadanieId());
		budzetView.setBdZadanieIdn(budzet.getBdZadanieIdn());
		budzetView.setBudWOkrNazwa(budzet.getBudWOkrNazwa());
		budzetView.setBudzetId(budzet.getBudzetId());
		budzetView.setBudzetIdm(budzet.getBudzetIdm());
		budzetView.setBudzetIds(budzet.getBudzetIds());
		budzetView.setBudzetNazwa(budzet.getBudzetNazwa());
		budzetView.setCzyAktywna(budzet.getCzyAktywna());
		budzetView.setDokBdStrBudzetIdn(budzet.getDokBdStrBudzetIdn());
		budzetView.setDokKomNazwa(budzet.getDokKomNazwa());
		budzetView.setDokPozLimit(budzet.getDokPozLimit());
		budzetView.setDokPozNazwa(budzet.getDokPozNazwa());
		budzetView.setDokPozNrpoz(budzet.getDokPozNrpoz());
		budzetView.setDokPozPKoszt(budzet.getDokPozPKoszt());
		budzetView.setDokPozRezKoszt(budzet.getDokPozRezKoszt());
		budzetView.setDokPozRKoszt(budzet.getDokPozPKoszt());
		budzetView.setOkrrozlId(budzet.getOkrrozlId());
		budzetView.setPozycjaPodrzednaId(budzet.getPozycjaPodrzednaId());
		budzetView.setStrBudNazwa(budzet.getStrBudNazwa());
		budzetView.setWspolczynnikPozycji(budzet.getWspolczynnikPozycji());
		budzetView.setWytworId(budzet.getWytworId());
		budzetView.setZadanieNazwa(budzet.getZadanieNazwa());
		budzetView.setZadaniePKoszt(budzet.getZadaniePKoszt());
		budzetView.setZadanieRezKoszt(budzet.getZadanieRezKoszt());
		budzetView.setZadanieRKoszt(budzet.getZadanieRKoszt());
		budzetView.setZrodloId(budzet.getZrodloId());
		budzetView.setZrodloIdn(budzet.getZrodloIdn());
		budzetView.setZrodloNazwa(budzet.getZrodloNazwa());
		budzetView.setZrodloPKoszt(budzet.getZrodloPKoszt());
		budzetView.setZrodloRezKoszt(budzet.getZrodloRezKoszt());
		budzetView.setZrodloRKoszt(budzet.getZrodloRKoszt());
		return budzetView;
	}
	
	/**
	 * Metoda mapuje obiekt ZadaniaFinansowe [CAN] na SupplierOrderHeaderDB [HB]
	 * 
	 * @param dz
	 * @return
	 */
	public static FinancialTaskDB MapZadaniaFinansoweNaFinancialTaskDB(ZadaniaFinansowe zf) {
		FinancialTaskDB ft = new FinancialTaskDB();
		
		ft.setAvailable(zf.getAvailable());
		ft.setCentrum(zf.getCentrum());
		ft.setCn(zf.getCn());
		ft.setErpId(zf.getErpId());
		ft.setId(zf.getId());
		ft.setIdentyfikator_num(zf.getIdentyfikator_num());
		ft.setTitle(zf.getTitle());
		return ft;
	}
	
	/**
	 * Metoda mapuje obiekt Projekt [CAN] na ProjectDB [HB]
	 * 
	 * @param p
	 * @return
	 */
	public static ProjectDB MapProjektNaProjectDB(Projekt p) {
		ProjectDB project = new ProjectDB();
		
		project.setAvailable(p.getAvailable());
		project.setCentrum(p.getCentrum());
		project.setCn(p.getCn());
		project.setErpId(p.getErpId());
		project.setId(p.getId());
		project.setIdentyfikator_num(p.getIdentyfikator_num());
		project.setTitle(p.getTitle());
		project.setP_datako(p.getP_datako());
		project.setP_datapo(p.getP_datapo());
		project.setTyp_kontrakt_id(p.getTyp_kontrakt_id());
		project.setTyp_kontrakt_idn(p.getTyp_kontrakt_idn());
		return project;
	}
	
	/**
	 * Metoda mapuje obiekt ProjektBudzetEtap [CAN] na ProjektBudzetEtapDB [HB]
	 * 
	 * @param pb
	 * @return
	 */
	public static ProjektBudzetEtapDB MapProjektBudzetNaProjectBudgetItemDB(ProjektBudzetEtap pb) {
		ProjektBudzetEtapDB projectBudget = new ProjektBudzetEtapDB();
		
		projectBudget.setId(pb.getId());
		projectBudget.setErpId(pb.getErpId());
		projectBudget.setNazwa(pb.getNazwa());
	    projectBudget.setNrpoz(pb.getNrpoz());
	    projectBudget.setP_datako(pb.getP_datako());
		projectBudget.setP_datapo(pb.getP_datapo());
	    projectBudget.setParent(pb.getParent());
	    projectBudget.setParent_id(pb.getParent_id());
	    projectBudget.setParent_idm(pb.getParent_idm());   
	    projectBudget.setAvailable(pb.getAvailable());
	    
		return projectBudget;
	}
	
	/**
	 * Metoda mapuje obiekt ProjektBudzet [CAN] na ProjectBudgetDB [HB]
	 * 
	 * @param pb
	 * @return
	 */
	public static ProjektBudzetDB MapProjektBudzetNaProjectProjectBugdetDB(ProjektBudzet pb) {
		ProjektBudzetDB projectBudget = new ProjektBudzetDB();
		

		projectBudget.setId(pb.getId());	
		projectBudget.setErpId(pb.getErpId());
		projectBudget.setIdm(pb.getIdm());
		projectBudget.setNazwa(pb.getNazwa());
		projectBudget.setP_datako(pb.getP_datako());
		projectBudget.setP_datapo(pb.getP_datapo());
		projectBudget.setParent(pb.getParent());
		projectBudget.setParent_id(pb.getParent_id());
		projectBudget.setParent_idm(pb.getParent_idm());
		projectBudget.setStan(pb.getStan());
		projectBudget.setStatus(pb.getStatus());
		projectBudget.setAvailable(pb.getAvailable());
		
		return projectBudget;
	}
	
	/**
	 * Metoda mapuje obiekt ProjektBudzetZasob [CAN] na ProjectBudgetItemResourceDB [HB]
	 * 
	 * @param pb
	 * @return
	 */
	public static ProjektBudzetZasobDB MapProjektBudzetZasobNaProjectBudgetItemResourceDB(ProjektBudzetZasob pb) {
		ProjektBudzetZasobDB projektBudzetZasobDB = new ProjektBudzetZasobDB();
		
		projektBudzetZasobDB.setCzy_glowny(pb.getCzy_glowny());
		projektBudzetZasobDB.setErpId(pb.getErpId());
		projektBudzetZasobDB.setNazwa(pb.getNazwa());
		projektBudzetZasobDB.setId(pb.getId());
		projektBudzetZasobDB.setNrpoz(pb.getNrpoz());
		projektBudzetZasobDB.setP_cena(pb.getP_cena());
		projektBudzetZasobDB.setP_ilosc(pb.getP_ilosc());
		projektBudzetZasobDB.setP_wartosc(pb.getP_wartosc());
		projektBudzetZasobDB.setParent(pb.getParent());
		projektBudzetZasobDB.setParent_id(pb.getParent_id());
		projektBudzetZasobDB.setR_ilosc(pb.getR_ilosc());
		projektBudzetZasobDB.setR_wartosc(pb.getR_wartosc());
		projektBudzetZasobDB.setRodzaj_zasobu(pb.getRodzaj_zasobu());
		projektBudzetZasobDB.setZasob_glowny_id(pb.getZasob_glowny_id());
		projektBudzetZasobDB.setAvailable(pb.getAvailable());

		return projektBudzetZasobDB;
	}
	
	/**
	 * Metoda mapuje obiekt ZrodloFinansowaniaProjektow [CAN] na SourcesOfProjectFundingDB [HB]
	 * 
	 * @param zf
	 * @return
	 */
	public static SourcesOfProjectFundingDB MapZrodloFinansowaniaProjektowNaSourcesOfProjectFundingDB(ZrodloFinansowaniaProjektow zf) {
		SourcesOfProjectFundingDB sourcesOfProjectFundingDB = new SourcesOfProjectFundingDB();
		
		sourcesOfProjectFundingDB.setAvailable(zf.getAvailable());
		sourcesOfProjectFundingDB.setErpId(zf.getErpId());
		sourcesOfProjectFundingDB.setId(zf.getId());
		sourcesOfProjectFundingDB.setKontrakt_id(zf.getKontrakt_id());
		sourcesOfProjectFundingDB.setKontrakt_idm(zf.getKontrakt_idm());
		sourcesOfProjectFundingDB.setRodzaj(zf.getRodzaj());
		sourcesOfProjectFundingDB.setWsp_proc(zf.getWsp_proc());
		sourcesOfProjectFundingDB.setZrodlo_id(zf.getZrodlo_id());
		sourcesOfProjectFundingDB.setZrodlo_identyfikator_num(zf.getZrodlo_identyfikator_num());
		sourcesOfProjectFundingDB.setZrodlo_idn(zf.getZrodlo_idn());
		sourcesOfProjectFundingDB.setZrodlo_nazwa(zf.getZrodlo_nazwa());
		return sourcesOfProjectFundingDB;
	}
	
	/**
	 * Metoda mapuje obiekt ZleceniaProdukcyjne [CAN] na ProductionOrderDB [HB]
	 * 
	 * @param zf
	 * @return
	 */
	public static ProductionOrderDB MapZleceniaProdukcyjneNaProductionOrderDB(ZleceniaProdukcyjne zp) {
		ProductionOrderDB productionOrderDB = new ProductionOrderDB();
		
		productionOrderDB.setAvailable(zp.getAvailable());
		productionOrderDB.setErpId(zp.getErpId());
		productionOrderDB.setId(zp.getId());
		productionOrderDB.setCn(zp.getCn());
		productionOrderDB.setTitle(zp.getTitle());
		return productionOrderDB;
	}
	
	/**
	 * Metoda mapuje obiekt KontrahentKonto [CAN] na KontrahentKontoDB [HB]
	 * @param kk
	 */
	public static KontrahentKontoDB MapKontrahentKontoToKontrahentKontoDB(KontrahentKonto kk) {
		KontrahentKontoDB kontrahentKontoDB = new KontrahentKontoDB();
		
		kontrahentKontoDB.setBank_id(kk.getBank_id());
		kontrahentKontoDB.setBank_idn(kk.getBank_idn());
		kontrahentKontoDB.setBank_nazwa(kk.getBank_nazwa());
		kontrahentKontoDB.setCzyIban(kk.getCzyIban());
		kontrahentKontoDB.setDostawca_id(kk.getDostawca_id());
		kontrahentKontoDB.setDostawca_idn(kk.getDostawca_idn());
		kontrahentKontoDB.setErpId(kk.getErpId());
		kontrahentKontoDB.setId(kk.getId());
		kontrahentKontoDB.setIdm(kk.getIdm());
		kontrahentKontoDB.setKolejnosc_uzycia(kk.getKolejnosc_uzycia());
		kontrahentKontoDB.setNazwa(kk.getNazwa());
		kontrahentKontoDB.setNumer_konta(kk.getNumer_konta());
		kontrahentKontoDB.setSymbol_waluty(kk.getSymbol_waluty());
		kontrahentKontoDB.setTypKurs_id(kk.getTypKurs_id());
		kontrahentKontoDB.setTypKurs_idn(kk.getTypKurs_idn());
		kontrahentKontoDB.setWaluta_id(kk.getWaluta_id());
		kontrahentKontoDB.setZrodKursWal_id(kk.getZrodKursWal_id());
		kontrahentKontoDB.setZrodKursWal_idn(kk.getZrodKursWal_idn());
		kontrahentKontoDB.setZrodKursWal_nazwa(kk.getZrodKursWal_nazwa());
		
		return kontrahentKontoDB;
	}
	
	/**
	 * Metoda mapuje obiekt TypyWnioskowORezerwacje na TypyWnioskowORezerwacjeDB
	 * @param typy
	 * @return
	 */
	public static TypyWnioskowORezerwacjeDB MapTypyWnioskowORezerwacjeToTypyWnioskowORezerwacjeDB(TypyWnioskowORezerwacje typy) {
		TypyWnioskowORezerwacjeDB typyWnioskowORezerwacjeDB = new TypyWnioskowORezerwacjeDB();
		
		typyWnioskowORezerwacjeDB.setCzy_aktywny(typy.getCzy_aktywny());
		typyWnioskowORezerwacjeDB.setCzy_kontrola_ceny(typy.getCzy_kontrola_ceny());
		typyWnioskowORezerwacjeDB.setCzy_kontrola_ilosci(typy.getCzy_kontrola_ilosci());
		typyWnioskowORezerwacjeDB.setCzy_kontrola_wartosci(typy.getCzy_kontrola_wartosci());
		typyWnioskowORezerwacjeDB.setCzy_obsluga_pzp(typy.getCzy_obsluga_pzp());
		typyWnioskowORezerwacjeDB.setErpId(typy.getErpId());
		typyWnioskowORezerwacjeDB.setId(typy.getId());
		typyWnioskowORezerwacjeDB.setIdm(typy.getIdm());
		typyWnioskowORezerwacjeDB.setNazwa(typy.getNazwa());
		
		return typyWnioskowORezerwacjeDB;
	}
	
	/**
	 * Metoda mapuje obiekt OkresBudzetowy na OkresBudzetowyDB
	 * @param bud
	 * @return
	 */
	public static OkresBudzetowyDB mapOkresBudzetowyNaOkresBudzetowyDB(OkresBudzetowy bud) {
		OkresBudzetowyDB budDB = new OkresBudzetowyDB();
		
		budDB.setCzy_archiwalny(bud.getCzy_archiwalny());
		budDB.setData_obow_do(bud.getData_obow_do());
		budDB.setData_obow_od(bud.getData_obow_od());
		budDB.setErpId(bud.getErpId());
		budDB.setId(bud.getId());
		budDB.setIdm(bud.getIdm());
		budDB.setNazwa(bud.getNazwa());
		budDB.setWersja(bud.getWersja());
		budDB.setAvailable(bud.getAvailable());
		
		return budDB;
	}
	
	/**
	 * Metoda mapuje obiekt PlanZakupow na PlanZakupowDB
	 * @param pz
	 * @return
	 */
	public static PlanZakupowDB MapPlanZakupowToPlanZakupowDB(PlanZakupow pz) {
		PlanZakupowDB pzDB = new PlanZakupowDB();
		
		pzDB.setId(pz.getId());
		pzDB.setOkres_id(pz.getOkres_id());
		pzDB.setOkres_idm(pz.getOkres_idm());
		pzDB.setBudzet_id(pz.getBudzet_id());
		pzDB.setBudzet_idn(pz.getBudzet_idn());
		pzDB.setCzyAgregat(pz.getCzyAgregat());
		pzDB.setErpId(pz.getErpId());
		pzDB.setIdm(pz.getIdm());
		pzDB.setNazwa(pz.getNazwa());
		pzDB.setAvailable(pz.getAvailable());
		
		return pzDB;
	}
	
	/**
	 * Metoda mapuje obiekt RodzajePlanowZakupu na RodzajePlanowZakupuDB
	 * @param rpz
	 * @return
	 */
	public static RodzajePlanowZakupuDB MapRodzajePlanowZakupuToRodzajePlanowZakupuDB(RodzajePlanowZakupu rpz) {
		RodzajePlanowZakupuDB ro = new RodzajePlanowZakupuDB();
		
		ro.setCena(rpz.getCena());
		ro.setCzy_aktywna(rpz.getCzy_aktywna());
		ro.setErpId(rpz.getErpId());
		ro.setKomorkaRealizujaca(rpz.getKomorkaRealizujaca());
		ro.setId(rpz.getId());
		ro.setNazwa(rpz.getNazwa());
		ro.setNrpoz(rpz.getNrpoz());
		ro.setOpis(rpz.getOpis());
		ro.setP_ilosc(rpz.getP_ilosc());
		ro.setP_koszt(rpz.getP_koszt());
		ro.setParent(rpz.getParent());
		ro.setParentId(rpz.getParentId());
		ro.setParentIdm(rpz.getParentIdm());
		ro.setR_ilosc(rpz.getR_ilosc());
		ro.setR_koszt(rpz.getR_koszt());
		ro.setRez_ilosc(rpz.getRez_ilosc());
		ro.setRez_koszt(rpz.getRez_koszt());
		ro.setWytwor_id(rpz.getWytwor_id());
		ro.setWytwor_idm(rpz.getWytwor_idm());
		
		return ro;
	}

	public static BudzetKOKomorekDB MapbudzetKoKomorekTobudzetKoKomorekDB(
			BudzetKOKomorek bud) {
		BudzetKOKomorekDB budDB = new BudzetKOKomorekDB();
		
		budDB.setId(bud.getId());
		budDB.setIdm(bud.getIdm());
		budDB.setNazwa(bud.getNazwa());
		budDB.setCzyAgregat(bud.getCzyAgregat());
		budDB.setBudzet_id(bud.getBudzetId());
		budDB.setBudzet_idn(bud.getBudzetIdn());
		budDB.setErpId(bud.getErpId());
		budDB.setOkresId(bud.getOkresId());
		budDB.setOkresIdm(bud.getOkresIdm());
		budDB.setAvailable(bud.getAvailable());
		
		return budDB;
	}
	
	/**
	 * Metoda mapuje obiekt RoleWProjekcie na RoleWProjekcieDB
	 * @param r
	 * @return
	 */
	public static RoleWProjekcieDB MapRoleWProjekcieToRoleWProjekcieDB(RoleWProjekcie r){
		RoleWProjekcieDB rol = new RoleWProjekcieDB();
		
		rol.setBp_rola_id(r.getBp_rola_id());
		rol.setBp_rola_idn(r.getBp_rola_idn());
		rol.setBp_rola_kontrakt_id(r.getBp_rola_kontrakt_id());
		rol.setCzy_blokada_prac(r.getCzy_blokada_prac());
		rol.setCzy_blokada_roli(r.getCzy_blokada_roli());
		rol.setId(r.getId());
		rol.setKontrakt_id(r.getKontrakt_id());
		rol.setKontrakt_idm(r.getKontrakt_idm());
		rol.setOsobaGuid(r.getOsobaGuid());
		rol.setOsobaId(r.getOsobaId());
		rol.setPracownik_id(r.getPracownik_id());
		rol.setPracownik_idn(r.getPracownik_idn());
		rol.setPracownik_nrewid(r.getPracownik_nrewid());
		rol.setAvailable(r.getAvailable());
		
		return rol;
	}
	
	/**
	 * Metoda mapuje obiekt RoleWBudzecie na RoleWBudzecieDB
	 * @param r
	 * @return
	 */
	public static RoleWBudzecieDB MapRoleWBudzecieToRoleWBudzecieDB(RoleWBudzecie r){
		RoleWBudzecieDB role = new RoleWBudzecieDB();
		
		role.setBd_budzet_ko_id(r.getBd_budzet_ko_id());
		role.setBd_budzet_ko_idm(r.getBd_budzet_ko_idm());
		role.setBp_rola_id(r.getBp_rola_id());
		role.setBp_rola_idn(r.getBp_rola_idn());
		role.setBudzet_id(r.getBudzet_id());
		role.setId(r.getId());
		role.setNazwa(r.getNazwa());
		role.setOsoba_id(r.getOsoba_id());
		role.setOsobaGuid(r.getOsobaGuid());
		role.setPracownik_id(r.getPracownik_id());
		role.setPracownik_nrewid(r.getPracownik_nrewid());
		role.setAvailable(r.getAvailable());
		
		return role;
	}

	public static BudzetPozycjaSimpleDB MapbudzetPozycjeSimpleToBudzetPozycjeSimpleDB(
			BudzetPozycjaSimple kk) {
		
		BudzetPozycjaSimpleDB pozDB=new BudzetPozycjaSimpleDB();
		
		pozDB.setId(kk.getId());
		pozDB.setBd_nad_rodzaj_ko_id(kk.getBd_nad_rodzaj_ko_id());
		pozDB.setCzyAktywna(kk.getCzy_aktywna());
		pozDB.setErpId(kk.getErpId());
		pozDB.setNazwa(kk.getNazwa());
		pozDB.setNrpoz(kk.getNrpoz());
		pozDB.setOpis(kk.getOpis());
		pozDB.setP_koszt(kk.getP_koszt());
		pozDB.setParent(kk.getParent());
		pozDB.setParentId(kk.getParentId());
		pozDB.setParent_idm(kk.getParentIdm());
		pozDB.setR_koszt(kk.getR_koszt());
		pozDB.setRez_koszt(kk.getRez_koszt());
		pozDB.setWytwor_id(kk.getWytworId());
		pozDB.setWytwor_idm(kk.getWytworIdm());
		pozDB.setAvailable(kk.getAvailable());
		
		return pozDB;
	}

	public static BudzetDoKomorkiDB MapBudzetDoKomorkiToBudzetDoKomorkiDB(
			BudzetDoKomorki kk) {
		BudzetDoKomorkiDB budDB=new BudzetDoKomorkiDB();
		
		budDB.setId(kk.getId());	
		budDB.setBudzet_id(kk.getBudzet_id());
		budDB.setBudzet_idn(kk.getBudzet_idn());
		budDB.setBudzet_nad_id(kk.getBudzet_nad_id());
		budDB.setBudzet_idn(kk.getBudzet_nad_idn());
		budDB.setOkresId(kk.getOkresId());
		budDB.setCzyWiodaca(kk.getCzyWiodaca());
		budDB.setKomorkaId(kk.getKomorkaId());
		budDB.setKomorkaIdn(kk.getKomorkaIdn());
		budDB.setNazwaKomorkiBudzetowej(kk.getNazwaKomorkiBudzetowej());
		budDB.setNazwaKomorkiOrg(kk.getNazwaKomorkiOrg());
		
		
		return budDB;
	}

	public static SzablonyRozliczenFakturDB MapSzablonyRozliczenFakturToSzablonyRozliczenFakturDB(
			SzablonyRozliczenFaktur kk) {
		SzablonyRozliczenFakturDB szabDB=new SzablonyRozliczenFakturDB();
		
		szabDB.setId(kk.getId());
		
		szabDB.setErpId(kk.getErpId());
		szabDB.setIdm(kk.getIdm());
		szabDB.setNazwa(kk.getNazwa());
		szabDB.setOkres_rozl(kk.getOkresIdm());
		szabDB.setOkresId(kk.getOkresId());
		return szabDB;
	}

	public static ProjectAccountPiatkiDB MapProjectAccountPiatkiToProjectAccountPiatkiDB(
			ProjectAccountPiatki kk) {
		ProjectAccountPiatkiDB piatkiDB=new ProjectAccountPiatkiDB();
		
		piatkiDB.setId(kk.getId());
		
		piatkiDB.setKonto_ido(kk.getKonto_ido());
		piatkiDB.setKontrakt_id(kk.getKontrakt_id());
		piatkiDB.setKontrakt_idm(kk.getKontrakt_idm());
		piatkiDB.setRep_atrybut_ido(kk.getRep_atrybut_ido());
		piatkiDB.setAvailable(kk.getAvailable());
		return piatkiDB;
	}

	public static NumeryZamowienDB MapNumeryZamowienToNumeryZamowienDB(
			NumeryZamowien nz) {
		NumeryZamowienDB nzDB=new NumeryZamowienDB();
		nzDB.setId(nz.getId());
		nzDB.setDat_dok(nz.getDat_dok());
		nzDB.setDat_odb(nz.getDat_odb());
		nzDB.setDostawca_id(nz.getDostawca_id());
		nzDB.setDostawca_idn(nz.getDostawca_idn());
		nzDB.setErpId(nz.getErpId());
		nzDB.setIdm(nz.getIdm());
		nzDB.setOpis_wewn(nz.getOpis_wewn());
		nzDB.setUwagi(nz.getUwagi());
		nzDB.setWartdok(nz.getWartdok());
		nzDB.setAvailable(nz.getAvailable());
		
		return nzDB;
		
	}

	public static ZamowienieSimplePozycjeDB MapZamowienieSimplePozycjeToZamowienieSimplePozycjeDB(
			ZamowienieSimplePozycje zsp) {
		ZamowienieSimplePozycjeDB zspDB=new ZamowienieSimplePozycjeDB();
		
		zspDB.setId(zsp.getId());
		zspDB.setBdBudzetKoId(zsp.getBdBudzetKoId());
		zspDB.setBdBudzetKoIdm(zsp.getBdBudzetKoIdm());
		zspDB.setBdPlanZamId(zsp.getBdPlanZamId());
		zspDB.setBdPlanZamIdm(zsp.getBdPlanZamIdm());
		zspDB.setBdRezerwacjaId(zsp.getBdRezerwacjaId());
		zspDB.setBdRezerwacjaIdm(zsp.getBdRezerwacjaIdm());
		zspDB.setBdRodzajKoId(zsp.getBdRodzajKoId());
		zspDB.setBdRodzajPlanZamId(zsp.getBdRodzajPlanZamId());
		zspDB.setBudzetId(zsp.getBudzetId());
		zspDB.setBudzetIdm(zsp.getBudzetIdm());
		zspDB.setCena(zsp.getCena());
		zspDB.setCenabrutto(zsp.getCenabrutto());
		zspDB.setEtapId(zsp.getEtapId());
		zspDB.setIlosc(zsp.getIlosc());
		zspDB.setKontraktId(zsp.getKontraktId());
		zspDB.setKoszt(zsp.getKoszt());
		zspDB.setKwotnett(zsp.getKwotnett());
		zspDB.setKwotvat(zsp.getKwotvat());
		zspDB.setNrpoz(zsp.getNrpoz());
		zspDB.setProjektId(zsp.getProjektId());
		zspDB.setSupplier_order_id(zsp.getSupplier_order_id());
		zspDB.setSupplier_order_idm(zsp.getSupplier_order_idm());
		zspDB.setWytwor_idm(zsp.getWytwor_idm());
		zspDB.setZamdospoz_id(zsp.getZamdospoz_id());
		zspDB.setZasobId(zsp.getZasobId());
		zspDB.setWalutaId(zsp.getWalutaId());
		zspDB.setIloscZreal(zsp.getIloscZreal());
		zspDB.setVatStawId(zsp.getVatStawId());
		zspDB.setKomorkaOrganizacyjaERPId(zsp.getKomorkaOrganizacyjaERPId());
		
		return zspDB;
	}
	
	public static PracownikDB MapPracownikNaPracownikDB(Pracownik p){
		PracownikDB pDB = new PracownikDB();
		
		pDB.setCzy_aktywny_osoba(p.isCzyAktywna());
		pDB.setGuid(p.getGuid());
		pDB.setId(p.getId());
		pDB.setImie(p.getImie());
		pDB.setNazwisko(p.getNazwisko());
		pDB.setNrewid(p.getNrewid());
		pDB.setPesel(p.getPesel());
		pDB.setPracownik_id(p.getZewnetrznyId());
		pDB.setPracownik_idn(p.getZewnetrznyIdm());
		
		return pDB;
	}
	
	public static TypyUrlopowDB MapTypyUrlopowNaTypyUrlopowDB(TypyUrlopow tu){
		TypyUrlopowDB t = new TypyUrlopowDB();
		
		t.setErpId(tu.getErpId());
		t.setId(tu.getId());
		t.setIdm(tu.getIdm());
		t.setOpis(tu.getOpis());
		t.setOpis_dlugi(tu.getOpis_dlugi());
		
		return t;
	}

	public static WniosekORezerwacjeDB MapWniosekORezerwacjeToWniosekORezerwacjeDB(
			WniosekORezerwacje wn) {
		WniosekORezerwacjeDB wnDB=new WniosekORezerwacjeDB();
		
		wnDB.setId(wn.getId());
		wnDB.setBdBudzetKoId(wn.getBdBudzetKoId());
		wnDB.setBdBudzetKoIdm(wn.getBdBudzetKoIdm());
		wnDB.setBdPlanZamId(wn.getBdPlanZamId());
		wnDB.setBdPlanZamIdm(wn.getBdPlanZamIdm());
		wnDB.setBdRezerwacjaId(wn.getBdRezerwacjaId());
		wnDB.setBdRezerwacjaIdm(wn.getBdRezerwacjaIdm());
		wnDB.setBdRezerwacjaPozId(wn.getBdRezerwacjaPozId());
		wnDB.setBdRodzajKoId(wn.getBdRodzajKoId());
		wnDB.setBdRodzajPlanZamId(wn.getBdRodzajPlanZamId());
		wnDB.setBdStrBudzetId(wn.getBdStrBudzetId());
		wnDB.setBdTypRezerwacjaIdn(wn.getBdTypRezerwacjaIdn());
		wnDB.setBkBdOkrSzablonRelId(wn.getBkBdOkrSzablonRelId());
		wnDB.setBkBdSzablonPozId(wn.getBkBdSzablonPozId());
		/* nie ma tego na tescie
		    	setBkCzyArchiwalny(item.getBk_czy_archiwalny());*/
		wnDB.setBkNazwa(wn.getBkNazwa());
		wnDB.setBkPozNazwa(wn.getBkPozNazwa());
		wnDB.setBkPozNrpoz(wn.getBkPozNrpoz());
		wnDB.setBkPozPKoszt(wn.getBkPozPKoszt());
		wnDB.setBkPozPKosztZrodla(wn.getBkPozPKosztZrodla());
		wnDB.setBkPozRKoszt(wn.getBkPozRKoszt());
		wnDB.setBkPozRKosztZrodla(wn.getBkPozRezKosztZrodla());
		wnDB.setBkPozRezKoszt(wn.getBkPozRezKoszt());
		wnDB.setBkPozRezKosztZrodla(wn.getBkPozRezKosztZrodla());
		/* nie ma tego na tescie
		    	setBkStatus(item.getBk_status());*/
		wnDB.setBkWytworId(wn.getBkWytworId());
		wnDB.setBudzetId(wn.getBudzetId());
		wnDB.setBudzetIdm(wn.getBudzetIdm());
		wnDB.setCena(wn.getCena());
		wnDB.setCzyObslugaPzp(wn.getCzyObslugaPzp());
		wnDB.setIlosc(wn.getIlosc());
		wnDB.setKoszt(wn.getKoszt());
		wnDB.setMagazynId(wn.getMagazynId());
		wnDB.setNazwa(wn.getNazwa());
		wnDB.setNrpoz(wn.getNrpoz());
		wnDB.setOkrrozlId(wn.getOkrrozlId());
		wnDB.setPzpBdOkrSzablonRelId(wn.getPzpBdOkrSzablonRelId());
		wnDB.setPzpBdSzablonPozId(wn.getPzpBdSzablonPozId());
		/* nie ma tego na tescie
		    	setPzpCzyArchiwalny(item.getPzp_czy_archiwalny());*/
		wnDB.setPzpJmId(wn.getPzpJmId());
		wnDB.setPzpJmIdn(wn.getPzpJmIdn());
		wnDB.setPzpNazwa(wn.getPzpNazwa());
		wnDB.setPzpPozNazwa(wn.getPzpPozNazwa());
		wnDB.setPzpPozNrpoz(wn.getPzpPozNrpoz());
		wnDB.setPzpPozPIlosc(wn.getPzpPozPIlosc());
		wnDB.setPzpPozPKoszt(wn.getPzpPozPKoszt());
		wnDB.setPzpPozRIlosc(wn.getPzpPozRIlosc());
		wnDB.setPzpPozRKoszt(wn.getPzpPozRezKoszt());
		wnDB.setPzpPozRezIlosc(wn.getPzpPozRezIlosc());
		wnDB.setPzpPozRezKoszt(wn.getPzpPozRezKoszt());
		/* nie ma tego na tescie
		    	setPzpStatus(item.getPzp_status());*/
		wnDB.setPzpWytworId(wn.getPzpWytworId());
		wnDB.setPzpWytworIdm(wn.getPzpWytworIdm());		
		wnDB.setRezJmId(wn.getRezJmId());
		wnDB.setRezJmIdn(wn.getRezJmIdn());
		wnDB.setRezerwacjaCzyArchiwalny(wn.getRezerwacjaCzyArchiwalny());
		wnDB.setRezerwacjaStatus(wn.getRezerwacjaStatus());
		wnDB.setVatstawId(wn.getVatstawId());
		wnDB.setWytworEditIdm(wn.getWytworEditIdm());
		wnDB.setWytworEditNazwa(wn.getWytworEditNazwa());
		wnDB.setWytworId(wn.getWytworId());
		wnDB.setZadanieId(wn.getZadanieId());			
		wnDB.setZadanieIdn(wn.getZadanieIdn());		
		wnDB.setZadanieNazwa(wn.getZadanieNazwa());	
		wnDB.setZrodloId(wn.getZrodloId());
		wnDB.setZrodloIdn(wn.getZrodloIdn());			
		wnDB.setZrodloNazwa(wn.getZrodloNazwa());		
		wnDB.setAvailable(wn.getAvailable());
		
		return wnDB;
	}
	
	public static ZrodloDB MapZrodloNaZrodloDB(Zrodlo z){
		ZrodloDB zdb = new ZrodloDB();
		
		zdb.setAvailable(z.isAvailable());
		zdb.setErpId(z.getErpId());
		zdb.setId(z.getId());
		zdb.setIdentyfikator_num(z.getIdentyfikator_num());
		zdb.setIdm(z.getIdm());
		zdb.setZrodlo_nazwa(z.getZrodlo_nazwa());
		
		return zdb;
	}
}
