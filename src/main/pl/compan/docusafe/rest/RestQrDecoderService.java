package pl.compan.docusafe.rest;


import com.google.common.base.Optional;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.compan.docusafe.service.barcodes.recognizer.ZxingUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(produces = "application/json")
public class RestQrDecoderService {

    private final static Logger log = LoggerFactory.getLogger(RestQrDecoderService.class);
    private HashMap<DecodeHintType, Object> hints;

    @Autowired
    HttpServletRequest request;

    @RequestMapping(value = "/qr_decode", method = RequestMethod.POST, headers = "content-type!=multipart/form-data")
    @ResponseBody
    public Map<String, Object> decode(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        request.getHeader("content-range");//Content-Range:bytes 737280-819199/845769
        request.getHeader("content-length"); //845769
        request.getHeader("content-disposition"); // Content-Disposition:attachment; filename="Screenshot%20from%202012-12-19%2017:28:01.png"
        try {
            ServletInputStream is = request.getInputStream();//actual content.
            return qrDecode(is);
        } catch (IOException e) {
            log.error("[decode] error", e);
            throw new InternalServerError(e);
        }
    }

    @RequestMapping(value = "/qr_decode", method = RequestMethod.POST, headers = "content-type=multipart/form-data")
    @ResponseBody
    public Map<String, Object> decodeMultipart(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @RequestParam("file") final MultipartFile multiPart) {

        try {
            InputStream is = multiPart.getInputStream();
            return qrDecode(is);
        } catch (IOException e) {
            log.error("[decodeMultipart] error", e);
            throw new InternalServerError(e);
        }
    }

    private Map<DecodeHintType, Object> getHints() {
        if(hints == null) {
            hints = new HashMap<DecodeHintType, Object>();
            hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
            hints.put(DecodeHintType.POSSIBLE_FORMATS, Arrays.asList(BarcodeFormat.QR_CODE));
        }

        return hints;
    }

    private Map<String, Object> qrDecode(InputStream is) throws IOException {
        File file = streamToFile(is);

        Optional<String> barcode = ZxingUtils.decodeBarcode(file, getHints());

        file.delete();

        Map<String, Object> response = new HashMap<String, Object>();

        if(barcode.isPresent()) {
            response.put("found", true);
            response.put("barcode", barcode.get());
        } else {
            response.put("found", false);
        }

        return response;
    }

    private File streamToFile(InputStream is) throws IOException {
        File tmp = File.createTempFile("qr_decode", null);
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(tmp);
            IOUtils.copy(is, os);
        } finally {
            is.close();
            if(os != null) {
                os.close();
            }
        }
        return tmp;
    }

}
