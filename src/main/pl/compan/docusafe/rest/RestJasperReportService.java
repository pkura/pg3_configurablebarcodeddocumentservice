package pl.compan.docusafe.rest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.compan.docusafe.api.user.office.JasperReportDto;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.spring.user.office.JasperReportServiceImpl;

import java.util.List;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletResponse;

/**
 * Klasa zawiera metody zwi�zane z generowaniem raport�w z Jasper Servera.
 */
@Controller
@RequestMapping(produces = {"application/json","application/xml"} ,
        consumes= {"application/json", "application/xml"})
public class RestJasperReportService {

    @Autowired
    JasperReportServiceImpl reportService;

    @Autowired
    RestRequestHandler requestHandler;
    
    @Autowired
    ResponseHandler responseHandler;


    /**
     * Metoda zwraca dost�pne raporty
     * 
     * @ds.Request(value="/jasper-report", method = RequestMethod.GET, produces = {"application/json","application/xml"} )
     * @return lista
     * @throws Exception
     */
    @RequestMapping(value="/jasper-report", method = RequestMethod.GET,
            produces = {"application/json","application/xml"} )
    public @ResponseBody ListDtos<KeyElement> getReportLists() throws Exception {
        List<KeyElement> list = requestHandler.openContext(new Callable<List<KeyElement>>() {
            @Override
            public List<KeyElement> call() throws Exception {
                return reportService.getReportsList();
            }
        });

        responseHandler.setUpStatusCodeForDtos(list);
		return new ListDtos<KeyElement>(list);

    }

    /**
     * Metoda zwraca dost�pne typy raport�w
     * 
     * @ds.Request(value="/jasper-report", method = RequestMethod.GET, produces = {"application/json","application/xml"} )
     * @return
     */
    @RequestMapping(value="/jasper-report/typntne", method = RequestMethod.GET,
            produces = {"application/json","application/xml"} )
    public @ResponseBody ListDtos<KeyElement> getReportType() throws Exception {
        List<KeyElement> list = requestHandler.openContext(new Callable<List<KeyElement>>() {
            @Override
            public List<KeyElement> call() throws Exception {
                return reportService.getReportsType();
            }
        });

        responseHandler.setUpStatusCodeForDtos(list);
        return new ListDtos<KeyElement>(list);

    }

    /**
     *
     *	Metoda dodaje raport
     *
     * @ds.Request(value="/jasper-report", method = RequestMethod.PUT, produces = {"application/json","application/xml"} )
     * @param <code>jasperReport</code> obiekt raportu kt�ry ma zosta� dodany
     * @return Informacje czy uda�o si� doda� raport
     * @throws Exception
     */
    @RequestMapping(value="/jasper-report", method = RequestMethod.PUT,
    		produces = MediaType.TEXT_HTML_VALUE )
    public @ResponseBody String addReport(@RequestBody final JasperReportDto jasperReport) throws Exception {
        try{
	    	String result = requestHandler.openContext(new Callable<String>() {
	            @Override
	            public String call() throws Exception {
	                return reportService.addReport(jasperReport);
	            }
	        });
	    	responseHandler.setUpStatusCodeFor(result);
	        return result;
        } catch (Exception e) {
        	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
        return null;
    }
}
