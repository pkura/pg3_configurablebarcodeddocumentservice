package pl.compan.docusafe.rest;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.opensymphony.webwork.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.auth.FormCallbackHandler;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.NetUtil;
import pl.compan.docusafe.web.common.LogoutAction;
import pl.compan.docusafe.web.filter.AuthFilter;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Service restowy do logowania i wylogowania
 */
@Controller
@RequestMapping(produces = "application/json")
public class RestAuthenticationService2 {
    private final static Logger log = LoggerFactory.getLogger(RestAuthenticationService2.class);

    @Autowired
    HttpServletRequest request;

    @RequestMapping(value="/login2", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity login(@RequestBody Credentials credentials){
        try {
            credentials.password = credentials.password == null ? "" : credentials.password;
            LoginContext lc = getLoginContext(credentials.username, credentials.password);
            lc.login();

            Subject subject = getSubject();
            HttpSession session = request.getSession();
            if(subject != null){
                session.removeAttribute(AuthFilter.SUBJECT_KEY);
                session.removeAttribute(AuthFilter.LANGUAGE_KEY);
                session.invalidate();
            }

            session = request.getSession(true);
            session.setAttribute(AuthFilter.SUBJECT_KEY, lc.getSubject());
            session.setAttribute(AuthFilter.LANGUAGE_KEY, GlobalPreferences.getUserLocal(lc.getSubject()));
            setStrutsRequest();
        } catch(Exception e) {
            log.error("[login] error", e);
            return new ResponseEntity<String>("Nieudane logowanie: " + e.getMessage(), HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }

    @RequestMapping(value="/logout2", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity logout() throws LoginException {
        LogoutAction.logout(request, "DocuSafe", this.toString());
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value="/is_logged_in", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
//    ResponseEntity<String> isLoggedIn(){
    Map<String, Object> isLoggedIn(){
        Subject subject = getSubject();

        Map<String, Object> response = new HashMap<String, Object>();

        if(subject != null) {
            Optional<UserPrincipal> userPrincipal = getUserPrincipal(subject);

            if(userPrincipal.isPresent()) {
                response.put("is_logged_in", true);
                response.put("username", userPrincipal.get().getName());
//                return new ResponseEntity<String>(userPrincipal.get().getName(), HttpStatus.OK);
            } else {
                response.put("is_logged_in", false);
//                return new ResponseEntity<String>(HttpStatus.OK);
            }
        } else {
            response.put("is_logged_in", false);
//            return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
        }

        return response;
    }

    private LoginContext getLoginContext(String login, String password) throws LoginException {
        return new LoginContext("DocuSafe",
                new FormCallbackHandler(login, password, NetUtil.getClientAddr(request).getIp()));
    }

    public void setStrutsRequest(){
        ServletActionContext.setRequest(request);
    }

    public void open(){
        Preconditions.checkNotNull(getSubject(), "Brak użytkownika!");
        try {
            DSApi.open(getSubject());
        } catch (EdmException e) {
            throw new IllegalStateException("Wystąpił wewnętrzny problem!");
        }
    }

    protected Subject getSubject() {
        return (Subject)request.getSession().getAttribute(AuthFilter.SUBJECT_KEY);
    }

    public void close(){
        DSApi._close();
    }

    public static Optional<UserPrincipal> getUserPrincipal(Subject subject)
    {
        Set users = subject.getPrincipals(UserPrincipal.class);

        if (users != null && users.size() > 0)
            return Optional.of((UserPrincipal) users.iterator().next());
        else
            return Optional.absent();
    }

    public static class Credentials {
        public String username;
        public String password;
    }

}
