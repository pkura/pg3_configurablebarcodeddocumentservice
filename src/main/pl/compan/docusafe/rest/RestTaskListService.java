package pl.compan.docusafe.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.compan.docusafe.api.user.office.TaskList;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.tasklist.TaskListHelper;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HttpMethod;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Klasa udost�pniaj�ca liste zada� u�ytkownika poprzez us�ugi RESTowe
 */
@Controller
@RequestMapping()
public class RestTaskListService {

    private static final Logger log = LoggerFactory.getLogger(RestTaskListService.class);
    @Autowired
    RestRequestHandler requestHandler;
	
    @Autowired
    ResponseHandler responseHandler;

    /**
     * Zwraca zadania u�ytkownika lub dzia�u
     *
     * @ds.Request (value = "/tasklist/{username}/user/{offset}/offset", method = RequestMethod.GET)
     * 
     * @param userName nazwa u�ytkownika
     * @param offset numer strony do zwr�cenia
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/tasklist/{username}/user/{offset}/offset", method = RequestMethod.GET)
    public @ResponseBody TaskList getUserTaskList(@PathVariable("username") final String userName,
                                                  @PathVariable("offset") final Integer offset) throws Exception {
    	List<Map<String, String>> list = null;
    	try{
	        list = requestHandler.openContext(new Callable<List<Map<String, String>>>() {
	            @Override
	            public List<Map<String, String>> call() throws Exception {
	                    return TaskListHelper.instance().getTasks(userName, offset);
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(list);
	        return new TaskList(list);
    	} catch (Exception e) {
    		log.error("", e);
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
        return null;
    }

}
