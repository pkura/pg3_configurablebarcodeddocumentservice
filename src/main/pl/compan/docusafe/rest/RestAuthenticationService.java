package pl.compan.docusafe.rest;

import com.google.common.base.Preconditions;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.catalina.Session;
import org.jasig.cas.client.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.FormCallbackHandler;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.NetUtil;
import pl.compan.docusafe.web.filter.AuthFilter;
import pl.compan.docusafe.web.filter.CasUtils;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Entity;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * Service restowy do logowania i wylogowania
 * @RequestMapping(produces = {"application/json", "application/xml"}, consumes= {"application/json", "application/xml"})

 */
@Controller
@RequestMapping(produces = {"application/json", "application/xml"}, consumes = "*")//only produce json
public class RestAuthenticationService {

    private final static Logger log = LoggerFactory.getLogger(RestAuthenticationService.class);

    @Autowired
    HttpServletRequest request;
    
    @Autowired
	ResponseHandler responseHandler;

    /**
     * Logowanie zwyk�e.
     *
     * @ds.Request(value="/login", method = RequestMethod.POST, params = {"username","password"})
     @ResponseStatus(HttpStatus.ACCEPTED)
     * @param login
     * @param password
     * @return
     * */
    @RequestMapping(value="/login", method = RequestMethod.POST, params = {"username","password"})
    public @ResponseBody void login(@RequestParam("username") final String login, @RequestParam("password") String password){
        try{
            //@TODO tu mo�na doda� sprawdzanie po ip czy dany adres ma dost�p do serwisu
            loginUser(AuthUtil.getBasicLoginContext(login, password, request));
            responseHandler.setHeaders(HttpStatus.ACCEPTED);
        }catch(Exception e){
            log.error("", e);
            responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
            throw new IllegalStateException("Nieudane logowanie: " + e.getMessage());
        }
    }

    /**
     * Logowanie zapomoc� certyfiaktu
     * @ds.Request(value="/login-certificate", method = RequestMethod.POST)
     * @param certificate
     */
    @RequestMapping(value="/login-certificate", method = RequestMethod.POST)
    public @ResponseBody void loginCertificate(@RequestBody final KeyElement certificate) {
        try{
            loginUser(AuthUtil.loginByCertificate(certificate.getValue(), request));
            responseHandler.setHeaders(HttpStatus.ACCEPTED);
        }catch(Exception e){
            log.error("", e);
            responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
            throw new IllegalStateException("Nieudane logowanie: " + e.getMessage());
        }
    }
    /**
     * Logowanie poprzez Cas-proxy
     *
     * @ds.Request(value="/login-cas", method = RequestMethod.POST, params = {"t"})
     * @ResponseStatus(HttpStatus.ACCEPTED)
     * @param login
     * @return
     * */
    @RequestMapping(value="/login-cas", method = RequestMethod.POST, params = {"t"})
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @ResponseBody void login(@RequestParam("t") final String ticket){
        try{
            String username = CasUtils.checkProxyTicket(ticket);
            loginUser(AuthUtil.getLoginContext(username, "", request));
        }catch(Exception e){
            log.error("", e);
            throw new IllegalStateException("Nieudane logowanie: " + e.getMessage());
        }
    }

    @RequestMapping(value="/logout-cas", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void logutCas(){
        try {
            URL url = new URL(CasUtils.getCasServerLogoutUrl());
            URLConnection urlConnection = url.openConnection();
            Map<String, List<String>> headerFields = urlConnection.getHeaderFields();
            for(Map.Entry<String, List<String>> e : headerFields.entrySet()){
                log.error("{} -> {}",e.getKey(), e.getValue());
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    urlConnection.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                log.error(inputLine);
            in.close();

        }catch (Exception e){
            log.error("", e);
            throw new IllegalStateException("Nieudane wylogowanie: " + e.getMessage());
        }
    }
    /**
     * Loguje u�ytkownika
     * @param login
     * @param password
     * @throws LoginException
     */
    private void loginUser(LoginContext lc) throws LoginException {
        lc.login(); // jak nie wyrzuci b��dem to przesz�o
        //Subject subject = getSubject();
        HttpSession session = request.getSession();
        session.setAttribute(AuthFilter.SUBJECT_KEY, lc.getSubject());
        setStrutsRequest();
    }

    /**
     * Wylogowanie u�ytkownika
     * @RequestMapping(value = "/logout", method = RequestMethod.POST)
     * @ResponseStatus(HttpStatus.ACCEPTED)
     */
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @ResponseBody void logout(){
        request.getSession().removeAttribute(AuthFilter.SUBJECT_KEY);
        request.getSession().invalidate();
        setStrutsRequest();
        request.getSession(true);
    }

    public void setStrutsRequest(){
        ServletActionContext.setRequest(request);
    }

    public void open(){
        Preconditions.checkNotNull(getSubject(), "Brak u�ytkownika!");
        try {
            DSApi.open(getSubject());
        } catch (EdmException e) {
            throw new IllegalStateException("Wyst�pi� wewn�trzny problem!");
        }
    }

    private Subject getSubject() {
        return (Subject)request.getSession().getAttribute(AuthFilter.SUBJECT_KEY);
    }

    public void close(){
        DSApi._close();
    }
}
