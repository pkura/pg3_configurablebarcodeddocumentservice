package pl.compan.docusafe.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.compan.docusafe.api.user.office.IntegerDto;
import pl.compan.docusafe.api.user.office.ListIds;
import pl.compan.docusafe.parametrization.archiwum.PublicDocument;
import pl.compan.docusafe.rest.handlers.ResponseHandler;

/**
 * Usługi restowe związane z obsługą archiwaliów publicznych
 */
@Controller
@RequestMapping(produces = { "application/json", "application/xml" })
public class RestPublishedArchivesService {

    @Autowired
    RestRequestHandler requestHandler;
    @Autowired
    ResponseHandler responseHandler;
    @Autowired
    HttpServletRequest request;

    /**
     * Metoda zwraca liczbę dokumentów będących archiwaliami publicznie
     * dostępnymi.
     * 
     * @ds.Request(value = "/published/cnt", method = RequestMethod.GET)
     * @return Całkowita liczba dokumentów będących archiwaliami publicznie
     *         dostępnymi.
     */
    @RequestMapping(value = "/published/cnt", method = RequestMethod.GET)
    public @ResponseBody
    IntegerDto countDocuments() {
	int count = 0;
	try {
	    count = this.requestHandler.openContext(new Callable<Integer>() {

		@Override
		public Integer call() throws Exception {
		    return PublicDocument.list().size();
		}
	    });
	} catch (Exception e) {
	    this.responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
	}
	return new IntegerDto(count);
    }

    /**
     * Lista identyfikatorów dokumentów będących archiwaliami publicznie
     * dostępnymi, od pozycji {from} do {to} na liście zbiorczej.
     * 
     * @ds.Request(value = "/published/list/{from}/{to}/ids", method =
     *                   RequestMethod.GET)
     */
    @RequestMapping(value = "/published/list/{from}/{to}/ids", method = RequestMethod.GET)
    public @ResponseBody
    ListIds listDocumentIds(@PathVariable final int from, @PathVariable final int to) throws Exception {
	List<Long> listOfIds = null;
	try {
	    listOfIds = this.requestHandler.openContext(new Callable<List<Long>>() {

		@Override
		public List<Long> call() throws Exception {
		    List<PublicDocument> listOfDocs = PublicDocument.list(from, to);
		    ArrayList<Long> result = new ArrayList<Long>();
		    for (PublicDocument doc : listOfDocs) {
			result.add(doc.getDocumentId());
		    }
		    return result;
		}
	    });
	} catch (Exception e) {
	    this.responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
	    listOfIds = new ArrayList<Long>();
	}
	return new ListIds(listOfIds);
    }
}
