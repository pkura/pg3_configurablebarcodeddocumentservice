package pl.compan.docusafe.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.compan.docusafe.web.common.LogoutAction;

import javax.security.auth.login.LoginException;

@Controller
public class RestConnectionService {
    @RequestMapping(value="/ping", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity ping() throws LoginException {
        return new ResponseEntity<String>("PONG", HttpStatus.OK);
    }
}
