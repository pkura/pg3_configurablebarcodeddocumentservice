package pl.compan.docusafe.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.compan.docusafe.events.rxjava.RxJavaCore;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@Controller
@RequestMapping(produces = "application/json")
public class RestRxJavaService {

    private static final Logger log = LoggerFactory.getLogger(RestRxJavaService.class);

    @Autowired
    ResponseHandler responseHandler;

    @RequestMapping(value="/rxjava", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity test(){

        try {
            new RxJavaCore().test();
        } catch(Throwable ex) {
            log.error("[test] error", ex);
            throw new InternalServerError(ex);
        }

        return responseHandler.handle("OK");

    }


}
