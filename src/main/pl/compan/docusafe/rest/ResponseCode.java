package pl.compan.docusafe.rest;

import org.apache.commons.lang3.StringUtils;

/**
 * Kody odpowiedzi
 */
public enum ResponseCode {
    SUCCESS(1), ERROR(-2);
    int i;

    ResponseCode(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    @Override
    public String toString() {
        return  name();
    }
}
