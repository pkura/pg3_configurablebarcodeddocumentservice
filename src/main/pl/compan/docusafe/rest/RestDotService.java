package pl.compan.docusafe.rest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import pl.compan.docusafe.api.user.office.DocumentService;
import pl.compan.docusafe.core.cmis.model.CmisPrepend;
import pl.compan.docusafe.core.graphs.GraphGenerator;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.Callable;

    /**
     * Service udostępniający dane związane z logami DOT
     */
    @Controller
    @RequestMapping(produces = {"application/json", "application/xml"}, consumes= {"application/json", "application/xml"})
    public class RestDotService implements Serializable {
        private static final Logger log = LoggerFactory.getLogger(RestDotService.class);
        public static final String COOKIE="cookie";

        @Autowired
        RestRequestHandler requestHandler;

        @Autowired
        HttpServletRequest request;

        @Autowired
        DocumentService documentService;
		
        @Autowired
        ResponseHandler responseHandler;

        /**
         * Zwraca DOT dla dokumentu
         * @ds.Request (value="/dot/{documentId}/document", method = RequestMethod.GET )
         * @param documentId
         * @return DOT w Base64
         * @throws Exception
         */
        @RequestMapping(value = "/dot/{documentId}/document", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.GET)
        public @ResponseBody
        String getLogByDocumentId(@PathVariable final String documentId) throws Exception {

        	String result = null;
            try {
	            result = requestHandler.openContext(new Callable<String>() {
	                @Override
	                public String call() throws Exception {
	                    Long docId = CmisPrepend.lastId(documentId);
	                    return new GraphGenerator().generateGraphFromDocIdBase64(docId, true, false);
	                }
	            });
        	responseHandler.setUpStatusCodeFor(result);
	        } catch (Exception e) {
	        	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
			}
	        return result;
        }
}
