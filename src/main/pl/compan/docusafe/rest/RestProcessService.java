package pl.compan.docusafe.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import pl.compan.docusafe.api.user.office.*;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * US�ugi zwi�zane z procesami
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
@Controller
@RequestMapping(produces = {"application/json", "application/xml"})
public class RestProcessService {

    private static final Logger log = LoggerFactory.getLogger(RestProcessService.class);

    @Autowired
    ProcessService processService;

    @Autowired
    RestRequestHandler requestHandler;

    @Autowired
    HttpServletRequest request;
    
    @Autowired
    ResponseHandler responseHandler;

    /**
     * Methoda zwraca informacje o dost�pnych akcjach na dokumencie
     * @ds.Request(value = "/process/{documentId/document/actions", method= RequestMethod.GET)
     * @param documentId id dokumentu
     * @throws Exception
     */
    @RequestMapping(value = "/process/{documentId}/document/actions", method= RequestMethod.GET)
    public @ResponseBody ListDtos<KeyElement> getProcessActions(@PathVariable final String documentId) throws Exception {
    	List<KeyElement> results = null;
    	try {
	        results = requestHandler.openContext(new Callable<List<KeyElement>>() {
	            @Override
	            public List<KeyElement> call() throws Exception {
	                return processService.getProcessActions(documentId);
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(results);      
		} catch (Exception e) {
			log.error("Ocurred exception for Process.getProcessActions -> {}", e);
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	    	return new ListDtos<KeyElement>(results);
    }

    /**
     * Metoda wykonuje akcj� procesow� na dokumencie
     * @ds.Request(value = "/process/{documentId}/documentId/{processAction}/execute", method= RequestMethod.POST)
     *
     * @param documentId
     * @param processAction
     * @throws Exception
     */
    @RequestMapping(value = "/process/{documentId}/documentId/{processAction}/execute", method= RequestMethod.POST)
    public @ResponseBody void executeProcessAction(@PathVariable final String documentId, @PathVariable final String processAction) throws Exception {
    	try {
        requestHandler.openTransaction(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                processService.executeProcessAction(documentId, processAction);
	                return null;
	            }
	        });
	    	responseHandler.setHeaders(HttpStatus.ACCEPTED);
    	} catch (Exception e) {
    		log.error("Ocurred exception for Process.getProcessActions -> {}", e);
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    }

    /**
     * Metoda s�u�y do dekretacji r�cznej dokumentu
     *
     * @ds.Request(value = "/process/{documentId}/manual-assign", method= RequestMethod.POST)
     * @param assigns
     * @param documentId
     * @throws Exception
     */
    @RequestMapping(value = "/process/{documentId}/manual-assign", method= RequestMethod.POST)
    public @ResponseBody void assignMultiManualAssignment(final @RequestBody ListDtos<ManualAssignmentDto> assigns, final @PathVariable String documentId) throws Exception {
    	try {
	    	requestHandler.openContext(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                processService.manualMutliAssign(assigns.getItems(), documentId);
	                return null;
	            }
	        });
	    	responseHandler.setHeaders(HttpStatus.ACCEPTED);
    	} catch (Exception e) {
    		log.error("Ocurred exception for Process.getProcessActions -> {}", e);
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    }

    /**
     * Interfejs s�u�y do zwracania listy id dokument�w z uruchomionym procesem
     *
     * @ds.Request(value = "/process/{processName}/documents", method = RequestMethod.GET)
     *
     * @param processName
     * @throws Exception
     */
    @RequestMapping(value = "/process/{processName}/documents", method = RequestMethod.GET)
    public @ResponseBody ListIds getDocumentsForProcess(@PathVariable final String processName) throws Exception {
        List<Long> results = requestHandler.openContext(new Callable<List<Long>>() {
            @Override
            public List<Long> call() throws Exception {
                return processService.getDocumentsForProcess(processName);
            }
        });
        responseHandler.setUpStatusCodeForIds(results);
        return new ListIds(results);
    }

    /**
     * Interfejs s�u�y do zwracania nazwy procesu dla podanego dokumentu
     *
     * @ds.Request(value = "/document/{documentId}/processes", method = RequestMethod.GET)
     *
     * @param documentId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/document/{documentId}/processes", method = RequestMethod.GET)
    public @ResponseBody ListDtos<KeyElement> getProcessForDocument(@PathVariable final String documentId) throws Exception{
        List<KeyElement> results = requestHandler.openContext(new Callable<List<KeyElement>>() {
            @Override
            public List<KeyElement> call() throws Exception {
                return processService.getProcessForDocument(documentId);
            }
        });
        responseHandler.setUpStatusCodeForDtos(results);
        return new ListDtos<KeyElement>(results);
    }

    /**
     * Interfejs zwraca diagram w jakim obecnym stanie znajduje si� dokument
     *
     * @ds.Request(value = "/process/{processId}/diagram", method= RequestMethod.GET)
     *
     * @param processInstanceId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/process/{processId}/diagram", method= RequestMethod.GET, produces = "text/html;charset=UTF-8")
    public @ResponseBody String getProcessInstanceDiagram(@PathVariable final String processId) throws Exception{
    	String s = null;
    	try {
	        s = requestHandler.openContext(new Callable<String>() {
	            @Override
	            public String call() throws Exception {
	                return processService.getProcessInstance(processId);
	            }
	        });
	    	responseHandler.setUpStatusCodeFor(s);
		} catch (Exception e) {
			log.error("Ocurred exception for Process.getProcessInstanceDiagram -> {}", e);
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    	return s;
    }

    /**
     * Interfejs zwraca definicje proceesu
     * @param processName
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/process/{processName}/definition", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    public @ResponseBody String getProcessDefinition(@PathVariable final String processName) throws Exception {
    	String s = null;
    	try {
			s = requestHandler.openContext(new Callable<String>() {
	            @Override
	            public String call() throws Exception {
	                return processService.getProcessDefinition(processName);
	            }
	        });
			responseHandler.setUpStatusCodeFor(s);
    	} catch (Exception e) {
			log.error("Ocurred exception for Process.getProcessDefinition -> {}", e);
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
        return s;
    }

    @RequestMapping(value = "/process/{processName}/definition", method = RequestMethod.POST)
    public @ResponseBody void loadProcessDefinition (@RequestBody final KeyElement base64, @PathVariable final String processName) throws Exception {
    	try {
	    	requestHandler.openContext(new Callable<String>() {
	            @Override
	            public String call() throws Exception {
	                return processService.loadProcessDefinition(base64.getValue(), processName);
	            }
	        });	
			responseHandler.setHeaders(HttpStatus.OK);
    	} catch (Exception e) {
			log.error("Ocurred exception for Process.loadProcessDefinition -> {}", e);
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    }

}
