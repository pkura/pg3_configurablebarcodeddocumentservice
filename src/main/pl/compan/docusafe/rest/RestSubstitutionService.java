package pl.compan.docusafe.rest;

import com.google.common.base.Optional;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.SubstitutionService;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.DSApiUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Klasa udost�pniaj�ca metody na REST API zwi�zane z zastepstwami u�ytkownik�w
 */
@Controller
@RequestMapping(produces = {"application/json","application/xml"} ,
                consumes= {"application/json", "application/xml"})
public class RestSubstitutionService implements Serializable{


    private static final Logger log = LoggerFactory.getLogger(RestSubstitutionService.class);

    @Autowired
    SubstitutionService substitutionService;

    @Autowired
    RestRequestHandler requestHandler;

    @Autowired
	ResponseHandler responseHandler;
    
    @Autowired
    HttpServletRequest request;

    /**
     * Zwraca aktualne zast�pstwa
     *    
     * @ds.Request (value="/substitution", method = RequestMethod.GET)
     * @return Lista zast�pstw
     * @throws Exception
     */
    @RequestMapping(value="/substitution", method = RequestMethod.GET, params = {})
    public @ResponseBody ListDtos<SubstitutionDto> getAllCurrent() throws Exception {
        List<SubstitutionDto> list = requestHandler.openContext(new Callable<List<SubstitutionDto>>() {
            @Override
            public List<SubstitutionDto> call() throws EdmException {
                return substitutionService.getAllCurrentSubstitutions();
            }
        });
        responseHandler.setUpStatusCodeForDtos(list);
		return new ListDtos<SubstitutionDto>(list);
        
    }

    /**
     * Zwraca zast�pstwo dla danego zadanego czasu ( parametr 'time' w �adaniu )
     *
     * @ds.Request(value="/substitution", method = RequestMethod.GET, params = {"time"})
     * @param time data dla jakiego zawracane s� zastepstwa
     * @return Lista zast�pstw
     * @throws Exception
     */
    @RequestMapping(value="/substitution", method = RequestMethod.GET, params = {"time"})
    public @ResponseBody ListDtos<SubstitutionDto> getCurrentAtTime(
            @RequestParam("time") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final Date time) throws Exception {
        List<SubstitutionDto> list = requestHandler.openContext(new Callable<List<SubstitutionDto>>() {
            @Override
            public List<SubstitutionDto> call() throws EdmException {
                return substitutionService.getSubstitutionsAtTime(new DateTime(time));
            }
        });
        responseHandler.setUpStatusCodeForDtos(list);
		return new ListDtos<SubstitutionDto>(list);
    }

    /**
     * Zwraca zast�pstwo o podanym Id
     *
     * @ds.Request (value="/substitution/{substitutionId}", method = RequestMethod.GET)
     * @param substitutionId id zast�pstwa
     * @return SubstitutionDto - zast�pstwo
     * @throws Exception
     */
    @RequestMapping(value="/substitution/{substitutionId}", method = RequestMethod.GET)
    public @ResponseBody SubstitutionDto getById(@PathVariable final long substitutionId) throws Exception {
        return requestHandler.openContext(new Callable<SubstitutionDto>() {
            @Override
            public SubstitutionDto call() throws EdmException {
                Optional<SubstitutionDto> ret = substitutionService.getById(substitutionId);
                if (ret.isPresent()) {
                    return ret.get();
                } else {
                    throw new ResourceNotFoundException("Substitution " + substitutionId + " not found");
                }
            }
        });
    }

    /**
     * Tworzy zast�pstwo
     *
     * @ds.Request(value="/substitution", method = RequestMethod.POST)
     * @param substitutionDto obiekt tworzonego zast�pstwa
     * @param request wstrzykiwany obiekt zast�pstwa
     * @return zwraca uri do zast�pstwa
     * @throws Exception Ewentualny b�ad zwraca w postaci not_modified flagi + pow�d w nag�owku w parametrze "reason"
     */
    @RequestMapping(value="/substitution", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.POST)
    public @ResponseBody String create(@RequestBody final SubstitutionDto substitutionDto) throws Exception {
        long id = 0;
        try{
            id = requestHandler.openTransaction(new Callable<Long>() {
                @Override
                public Long call() throws Exception {
                    return substitutionService.create(substitutionDto);
                }
            });
            responseHandler.setHeaders(HttpStatus.CREATED);
        }catch(Exception e){
            log.error("B��d", e);
            responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return request.getRequestURI() + "/" + id;
    }

    /**
     * Anuluje zast�pstwo z podanym id
     *
     * @ds.Request (value="/substitution/{substitutionId}", method = RequestMethod.DELETE)
     * @param substitutionId id zast�pstwa kt�re zostanie usuni�te
     * @throws Exception zwraca b��d gdy nie znajdzie zast�pstwa o podanym id
     */
    @RequestMapping(value="/substitution/{substitutionId}", method = RequestMethod.DELETE)
    public @ResponseBody void delete(@PathVariable final long substitutionId) throws Exception {
    	try {
	        requestHandler.openTransaction(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                Optional<SubstitutionDto> sub = substitutionService.getById(substitutionId);
	                if (!sub.isPresent()) {
	                    throw new ResourceNotFoundException("Substitution " + substitutionId + " not found");
	                } else {
	                    substitutionService.cancel(sub.get());
	                }
	                return null;
	            }
	        });
	        responseHandler.setHeaders(HttpStatus.ACCEPTED);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    }
}
