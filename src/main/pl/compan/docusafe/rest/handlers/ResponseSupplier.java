package pl.compan.docusafe.rest.handlers;

import org.springframework.http.ResponseEntity;

abstract public class ResponseSupplier<T> {
    abstract public ResponseEntity<T> get() throws Throwable;
}
