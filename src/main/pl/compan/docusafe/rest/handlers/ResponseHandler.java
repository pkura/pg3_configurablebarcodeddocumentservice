package pl.compan.docusafe.rest.handlers;

import com.google.common.base.Optional;

import com.google.common.base.Supplier;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import pl.compan.docusafe.api.user.office.OfficeDocumentDto;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.rest.InternalServerError;
import pl.compan.docusafe.rest.RestRequestHandler;
import pl.compan.docusafe.rest.helpers.MultiOptional;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

@Service
public class ResponseHandler {

    private final static Logger log = LoggerFactory.getLogger(ResponseHandler.class);

	@Autowired
    HttpServletResponse response;
	
    public ResponseEntity handle(Object object) {
        return new ResponseEntity(object, HttpStatus.OK);
    }

    public <T> ResponseEntity handle(Optional<T> optional) {
        if(optional.isPresent()) {
            T entity = optional.get();
            if(entity instanceof MultiOptional) {
                return handle((MultiOptional)entity);
            } else {
                return new ResponseEntity<T>(entity, HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
        }
    }

    public <T> ResponseEntity<T> handle(MultiOptional<T> multiOptional) {
        if(multiOptional.isPresent()) {
            return new ResponseEntity<T>(multiOptional.get(), HttpStatus.OK);
        } else if(multiOptional.isAbsent()) {
            return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
        } else if(multiOptional.isDenied()) {
            return new ResponseEntity<T>(HttpStatus.FORBIDDEN);
        } else {
            throw new InternalServerError("Unknown state for multi optional: "+multiOptional.getStateString());
        }
    }

    public <T> ResponseEntity<T> handle(ResponseSupplier<T> callback) {
        try {
            return callback.get();
        } catch (Throwable ex) {
            throw new InternalServerError("500 Internal Server Error", ex);
        }
    }

//    public <T extends Collection> ResponseEntity<T> handleList(Optional<T> optionalList) {
//        if(optionalList.isPresent()) {
//            return new ResponseEntity<T>(optionalList.get(), HttpStatus.OK);
//        } else {
//            return new ResponseEntity(new ArrayList(), HttpStatus.OK);
//        }
//    }
    public ResponseEntity handleList(Optional optionalList) {
        if(optionalList.isPresent()) {
            return new ResponseEntity(optionalList.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity(new ArrayList(), HttpStatus.OK);
        }
    }

    /**
     * Method sets Status Code for fetched ids data by DAOs
     * @param list
     */
	public void setUpStatusCodeForIds(List<Long> list) {
		if(CollectionUtils.isEmpty(list)){
        	response.setStatus(HttpStatus.NOT_FOUND.value());
        	response.addHeader(RestRequestHandler.REASON, "Nie istnieje �adany obiekt!");
        } else {
        	response.setStatus(HttpStatus.OK.value());
        }
	}

	/**
	 * Method sets Status Code and add Reasno header which point on why validate does not pass
	 * @param httpStatus
	 * @param reason
	 */
	public void setHeaders(HttpStatus httpStatus, String reason) {
		response.setStatus(httpStatus.value());
		if(StringUtils.isNotEmpty(reason)){
			response.addHeader(RestRequestHandler.REASON, reason);
		} else {
			response.addHeader(RestRequestHandler.REASON, "Nie istnieje �adany obiekt!");
		}
	}
	
	/**
	 * Method sets Status Code
	 * @param httpStatus
	 * @param reason
	 */
	public void setHeaders(HttpStatus httpStatus) {
		response.setStatus(httpStatus.value());
	}

	/**
	 * Method sets Status Code for fetched Dtos data by DAOs
	 * @param list
	 */
	public <T> void setUpStatusCodeForDtos(List<T> list) {
		if(!CollectionUtils.isEmpty(list)){
        	response.setStatus(HttpStatus.OK.value());
        } else {
        	response.setStatus(HttpStatus.NOT_FOUND.value());
        	response.addHeader(RestRequestHandler.REASON, "Nie istnieje �adany obiekt!");
        }
	}

	/**
	 * Method sets Status Code for fetched result string by DAOs
	 * @param result
	 */
	public void setUpStatusCodeFor(String result) {
		if(StringUtils.isEmpty(result)){
        	response.setStatus(HttpStatus.NOT_FOUND.value());
        	response.addHeader(RestRequestHandler.REASON, "Nie istnieje �adany obiekt!");
        } else {
        	response.setStatus(HttpStatus.OK.value());            
        }
	}

	/**
	 * Method sets Status Code for fetched boolresult boolean data by DAOs
	 * @param boolResult
	 */
	public void setUpStatusCodeFor(boolean boolResult) {
        if(boolResult){
        	response.setStatus(HttpStatus.OK.value());
        } else {
        	response.setStatus(HttpStatus.NOT_MODIFIED.value());
        	response.addHeader(RestRequestHandler.REASON, "Nie istnieje �adany obiekt!");
        }
	}

	/**
	 * Method sets Status Code and add Reasno header which point on why error occured
	 * @param ex
	 */
	public void setUpStatusCodeBy(Exception ex) {
		String reason;
		if(ex.getMessage() != null){
			reason = ex.getMessage();
		} else {
			reason = "";
		}
		this.setHeaders(HttpStatus.BAD_REQUEST, reason);
	}
}
