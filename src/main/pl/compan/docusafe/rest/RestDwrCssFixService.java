package pl.compan.docusafe.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;

/**
 * <p>
 *     Don't even ask...
 * </p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
@Controller
@RequestMapping
public class RestDwrCssFixService {

    private static final Logger log = LoggerFactory.getLogger(RestDwrCssFixService.class);

    @Autowired
    RestTemplate restTemplate;

    public static boolean isProcessed = false;
    public static String responseBody = null;

    @RequestMapping(value = "dwr/fix.css", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<String> get(HttpServletRequest request, @RequestParam(required = false) Boolean reload, @RequestParam(defaultValue = "", required = false) String pathPrefix) {
        if(responseBody != null && Boolean.FALSE.equals(reload)) {
            return new ResponseEntity<String>(responseBody, HttpStatus.OK);
        } else {
            String main = getMainCss(request);

            if(main != null) {
                responseBody = extractData(main, pathPrefix);
                return new ResponseEntity<String>(responseBody, HttpStatus.OK);
            } else {
                return new ResponseEntity<String>("500 Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

    }

    private String extractData(String main, String pathPrefix) {
        int i = main.indexOf("/* Potrzebne do wyswietlania ikonek jQueryUI */");
        int j = main.indexOf("/* ORG");
        if(i < 0 || j < 0) {
            return null;
        }

        String ret = main.substring(i, j);
        return ret.replaceAll("\\/img", pathPrefix+"/img");
    }

    private String getMainCss(HttpServletRequest request) {
        String path = request.getRequestURL().toString();
        String targetPath = path.replaceFirst("/api/dwr/fix.css$", "/main.css");

        if(targetPath.equals(path)) {
            log.error("[getMainCss] targetPath is same as path, path = {}", path);
            return null;
        }

        String cookie = request.getHeader("COOKIE");

        HttpHeaders headers = new HttpHeaders();
        headers.set("COOKIE", cookie);

        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<String> response = restTemplate.exchange(targetPath, HttpMethod.GET, entity, String.class);

        if(response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        } else {
            return null;
        }

    }

}
