package pl.compan.docusafe.rest;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.compan.docusafe.api.user.*;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.ListIds;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.resources.Resource;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.rest.views.RichUserView;
import pl.compan.docusafe.util.DSApiUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Klasa zawiera us�ugi dost�pne poprzez interfejs REST zwi�zane z obs�ug� u�ytkownika
 */
@Controller
@RequestMapping()
public class RestUserService implements Serializable {
    private final static Logger LOG = LoggerFactory.getLogger(RestUserService.class);

    @Autowired
    private UserService userService;

    @Autowired
    private DivisionService divisionService;

    @Autowired
    private SubstitutionService substitutionService;

    @Autowired
    private RestRequestHandler requestHandler;

    @Autowired
    private ResponseHandler responseHandler;
    
    @Autowired
    HttpServletRequest request;
    
    /**
     * Metoda zwraca wszystkich u�ytkownik�w w systemie
     *
     * @ds.Request (value="/user", method = RequestMethod.GET )
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/user", method = RequestMethod.GET, produces={"application/json;charset=UTF-8","application/xml;charset=UTF-8"})
    public @ResponseBody ListDtos<UserDto> getAll() throws Exception {
        List<UserDto> list = requestHandler.openContext(new Callable<List<UserDto>>() {
            @Override
            public List<UserDto> call() throws EdmException {
                return userService.getAllUsers();
            }
        });
        
        responseHandler.setUpStatusCodeForDtos(list);
        return new ListDtos<UserDto>(list);
    }

    /**
     * Metoda zwraca wszystkich u�ytkownik�w w systemie wraz z dzia�ami, grupami do kt�rych nale��
     * 
     * @ds.Request (value="/user2", method = RequestMethod.GET)
     * @param HttpServletRequest request u�ywany do bodowania odpowiedzi
     * @return list u�ykownik�w
     * @throws Exception
     */
    @RequestMapping(value="/user2", method = RequestMethod.GET, produces = {"application/json"}, consumes = "*")
    public @ResponseBody
    ResponseEntity getAllWithDivision() throws Exception {
        return responseHandler.handleList(new DSTransaction().executeOpenContext(request, new DSTransaction.Function<List<RichUserView>>() {
            @Override
            public ImmutableList<RichUserView> apply() throws Exception {
                return RichUserView.getAll();
            }
        }));
    }


    /**
     * Zwraca u�ytkownika po przekazanym parametrze
     * 
     * @ds.Request (value="/user/{userId}/name", method = RequestMethod.GET)
     * @param userId id, name
     * @return u�ytkownik
     * @throws Exception
     */
    @RequestMapping(value="/user/{userId}", method = RequestMethod.GET)
    public @ResponseBody UserDto get(@PathVariable final String userId) throws Exception {
        return requestHandler.openContext(new Callable<UserDto>() {
            @Override
            public UserDto call() throws EdmException {
                Optional<UserDto> ret = Optional.absent();
                if(org.apache.commons.lang.StringUtils.isNumeric(userId))
                    ret = userService.getById(Long.valueOf(userId));
                if (ret.isPresent()) {
                    return ret.get();
                } else {
                    throw new ResourceNotFoundException("User '" + userId + "' not found");
                }
            }
        });
    }

    /**
     * Zwraca u�ytkownika po przekazanym parametrze
     *
     * @ds.Request (value="/user/{userId}/name", method = RequestMethod.GET)
     * @param userId name lub externalName/personNumber
     * @return u�ytkownik
     * @throws Exception
     */
    @RequestMapping(value="/user/{userId}/name", method = RequestMethod.GET)
    public @ResponseBody UserDto getByName(@PathVariable final String userId) throws Exception {
        return requestHandler.openContext(new Callable<UserDto>() {
            @Override
            public UserDto call() throws EdmException {
                Optional<UserDto> ret = userService.getByNameOrExternalName(userId);
                if (ret.isPresent()) {
                    return ret.get();
                } else {
                    throw new ResourceNotFoundException("User '" + userId + "' not found");
                }
            }
        });
    }


    /**
     * Tworzy u�ytkownika i zwraca jego uri do api
     *
     * @ds.Request (value="/user", method = RequestMethod.POST)
     *  
     * @param userDto u�ytkownik, kt�ry ma zosta� utworzony
     * @param request request u�ywany do bodowania odpowiedzi
     * @return uri wskazuj�ce na nowego u�ytkownika
     * @throws Ewentualny b�ad zwraca w postaci not_modified flagi + pow�d w nag�owku w parametrze "reason"
     */
    @RequestMapping(value="/user", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    public @ResponseBody String create(@RequestBody final UserDto userDto) throws Exception {
        long id = 0;
        try{
            id = requestHandler.openTransaction(new Callable<Long>() {
                @Override
                public Long call() throws Exception {
                    return userService.create(userDto);
                }
            });
            responseHandler.setHeaders(HttpStatus.CREATED);
            return request.getRequestURI() + "/" + id;
        }catch(Exception e){
            LOG.error("", e);
            responseHandler.setHeaders(HttpStatus.NOT_MODIFIED, e.getMessage());
        }

        return Long.toString(id);
    }

    /**
     * Lista dzia��w do kt�rych nale�y u�ytkownik
     *
     * @ds.Request (value="/user/{userId}/division", method = RequestMethod.GET)
     * @param userId id, name lub externalName/personNumber
     * @return lista dzia��w u�ytkownika
     * @throws Exception
     */
    @RequestMapping(value="/user/{userId}/division", method = RequestMethod.GET)
    public @ResponseBody ListDtos<DivisionDto> getDivisions(@PathVariable final String userId) throws Exception {
        List<DivisionDto> list = requestHandler.openContext(new Callable<List<DivisionDto>>() {
            @Override
            public List<DivisionDto> call() throws EdmException {
                Optional<UserDto> user = userService.getByIdOrNameOrExternalName(userId);
                if (user.isPresent()) {
                    return divisionService.getUserDivisions(user.get());
                } else {
                    throw new ResourceNotFoundException("User '" + userId + "' not found");
                }
            }
        });
        responseHandler.setUpStatusCodeForDtos(list);
        return new ListDtos<DivisionDto>(list);
    }

    /**
     * Zwraca aktualne zast�pstwo u�ytkownika
     * @ds.Request (value="/user/{userId}/substitute", method = RequestMethod.GET)
     *
     * @param userId id, name lub externalName/personNumber
     * @return zast�pstwo u�ytkownika
     * @throws Exception
     */
    @RequestMapping(value="/user/{userId}/substitute", method = RequestMethod.GET)
    public @ResponseBody SubstitutionDto getSubstitute(@PathVariable final String userId) throws Exception {
        return requestHandler.openContext(new Callable<SubstitutionDto>() {
            @Override
            public SubstitutionDto call() throws EdmException {
                Optional<UserDto> user = userService.getByIdOrNameOrExternalName(userId);
                if (user.isPresent()) {
                    Optional<SubstitutionDto> ret = substitutionService.getUserSubstitution(user.get());
                    if (ret.isPresent()) {
                        return ret.get();
                    } else {
                        throw new ResourceNotFoundException("User '" + userId + "' currently does not have substitute");
                    }
                } else {
                    throw new ResourceNotFoundException("User '" + userId + "' not found");
                }
            }
        });
    }

    /**
     * Zwraca list� zast�pstw u�ytkownika
     *
     * @ds.Request (value="/user/{userId}/substitution", method = RequestMethod.GET)
     * @param userId id, name lub externalName/personNumber
     * @return lista zast�pstw u�ytkownika
     * @throws Exception
     */
    @RequestMapping(value="/user/{userId}/substitution", method = RequestMethod.GET)
    public @ResponseBody ListDtos<SubstitutionDto> getSubstitutionsByUser(@PathVariable final String userId) throws Exception {
        List<SubstitutionDto> list = requestHandler.openContext(new Callable<List<SubstitutionDto>>() {
            @Override
            public List<SubstitutionDto> call() throws EdmException {
                Optional<UserDto> user = userService.getByIdOrNameOrExternalName(userId);
                if (user.isPresent()) {
                    return substitutionService.getSubstitutedByUser(user.get());
                } else {
                    throw new ResourceNotFoundException("User '" + userId + "' not found");
                }
            }
        });
        responseHandler.setUpStatusCodeForDtos(list);
        return new ListDtos<SubstitutionDto>(list);
    }

    /**
     * Aktualizuje u�ytkownika o podanym id, poprzez parametry przekazany w RequestBody userDto
     *
     * @ds.Request (value="/user/{userId}", method = RequestMethod.PUT)
     * @param userId id, name lub externalName/personNumber
     * @param userDto parametry do akutalizacji
     */
    @RequestMapping(value="/user/{userId}", method = RequestMethod.PUT)
    public @ResponseBody void update(@PathVariable final String userId,
                       @RequestBody final UserDto userDto) throws Exception {
        if(!userId.equals(userDto.getId().toString())
                && !userId.equals(userDto.getUsername())
                	&& !userId.equals(userDto.getPersonNumber())){
            throw new IllegalArgumentException("id in path and in body does not match");
        }
        try {
	        requestHandler.openTransaction(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                userService.update(userDto);
	                return null;
	            }
	        });
	        responseHandler.setHeaders(HttpStatus.ACCEPTED);
        } catch (Exception e) {
        	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    }

    /**
     * Usuwa z systemu u�ytkownika
     * 
     * @ds.Request (value="/user/{userId}", method = RequestMethod.DELETE)
     * @param userId id, name lub externalName/personNumber
     * @throws Exception
     */
    @RequestMapping(value="/user/{userId}", method = RequestMethod.DELETE)
    public @ResponseBody void delete(@PathVariable final String userId) throws Exception {
    	try {
	        requestHandler.openTransaction(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                Optional<UserDto> user = userService.getByIdOrNameOrExternalName(userId);
	                if (!user.isPresent()) {
	                    throw new ResourceNotFoundException("User '" + userId + "' not found");
	                } else {
	                    userService.delete(user.get());
	                }
	                return null;
	            }
	        });
	        responseHandler.setHeaders(HttpStatus.ACCEPTED);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    }

    @RequestMapping(value="/roles/list", method=RequestMethod.GET)
    public @ResponseBody ListDtos<KeyElement> getAvailableOfficeRoles() throws Exception{
        List<KeyElement> roles = requestHandler.openContext(new Callable<List<KeyElement>>() {
            @Override
            public List<KeyElement> call() throws Exception {
                return userService.getAvailableOfficeRoles();
            }
        });
        responseHandler.setUpStatusCodeForDtos(roles);
        return new ListDtos<KeyElement>(roles);
    }

    /**
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/user/{username}/roles", method=RequestMethod.GET)
    public @ResponseBody ListDtos<KeyElement> getUserRoles(@PathVariable final String username) throws Exception{
        List<KeyElement> roles = requestHandler.openContext(new Callable<List<KeyElement>>() {
            @Override
            public List<KeyElement> call() throws Exception {
                return userService.getUserOfficeRoles(username);
            }
        });
        responseHandler.setUpStatusCodeForDtos(roles);
        return new ListDtos<KeyElement>(roles);
    }

    /**
     *
     * @param username
     * @throws Exception
     */
    @RequestMapping(value="/user/{username}/roles", method=RequestMethod.POST)
    public @ResponseBody void updateUserRole(@PathVariable final String username, final @RequestBody ListIds roles) throws Exception{
    	try {
	        requestHandler.openTransaction(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                userService.updateUserOfficeRoles(roles.getItems(), username);
	                return null;
	            }
	        });
	        responseHandler.setHeaders(HttpStatus.ACCEPTED);
    	}catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    }
}
