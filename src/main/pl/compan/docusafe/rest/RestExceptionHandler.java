package pl.compan.docusafe.rest;

import com.opensymphony.webwork.ServletActionContext;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.rest.exceptions.BadRequestException;
import pl.compan.docusafe.rest.exceptions.NotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 */
@ControllerAdvice
public class RestExceptionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);


    @ExceptionHandler(EdmException.class)
    public void serverEdmException(HttpServletResponse resp, Exception ex) throws Exception{
       resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
       LOG.error(ex.getMessage(), ex);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public void accessDeniedException(HttpServletResponse resp, Exception ex) throws Exception{
       resp.sendError(HttpServletResponse.SC_FORBIDDEN, ex.getMessage());
       LOG.error(ex.getMessage(), ex);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public void resourceNotFound(HttpServletResponse resp, Exception ex) throws Exception {
        resp.sendError(HttpServletResponse.SC_NOT_FOUND, ex.getMessage());
    }

    @ExceptionHandler(NotFoundException.class)
    public void notFoundException(HttpServletResponse resp, Exception ex) throws Exception {
        resp.sendError(HttpServletResponse.SC_NOT_FOUND, ex.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public void illegalArgumentException(HttpServletResponse resp, Exception ex) throws Exception {
        resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
        LOG.info(ex.getMessage(), ex);
    }

    @ExceptionHandler(IllegalStateException.class)
    public void illegalStateException(HttpServletResponse resp, Exception ex) throws Exception {
        resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
        //LOG.info(ex.getMessage(), ex);
    }

    @ExceptionHandler(BadRequestException.class)
    public void badRequestException(HttpServletResponse resp, Exception ex) throws Exception {
        resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
        LOG.error(ex.getMessage(), ex);
    }

    @ExceptionHandler(RuntimeException.class)
    public void serverRuntimeException(HttpServletResponse resp, Exception ex) throws Exception{
        resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
        LOG.error(ex.getMessage(), ex);
    }

    @ExceptionHandler(InternalServerError.class)
    public void internalServerErrorException(HttpServletResponse resp, Exception ex) throws Exception{
        resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
        LOG.error(ex.getMessage(), ex);
    }

//    @ExceptionHandler(Exception.class)
//    public void serverError(HttpServletResponse resp, Exception ex) throws IOException {
//        resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
//                "Unexpected error " + ex.getMessage());
//        LOG.error(ex.getMessage(), ex);
//    }

    @ExceptionHandler(DivisionNotFoundException.class)
    public void serverDivisionNotFound(HttpServletResponse resp, Exception ex) throws Exception{
        resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
        LOG.error(ex.getMessage(), ex);
    }

}
