package pl.compan.docusafe.rest;

import com.google.common.base.Throwables;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Klasa pomocznicza do obs�ugi zdarze� zwi�zanych z REST API
 */
@Controller
public class RestRequestHandler implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(RestRequestHandler.class);
    /** @var pole ustawiane w nag��wku odpowiedzi opisuj�ce rodzaj b��du*/
    public static final String REASON = "Reason";
    @Autowired
    private RestAuthenticationService authenticationService;

    /**
     * Kopiuje obiekt REquestu Springowy do Requestu Strutsowego/WebWorkowego
     * w celu wywo�ania akcji z formatek.
     *
     * @param run
     * @param <T>
     * @return
     * @throws Exception
     */
    public <T> T inRequestContext(Callable<T> run) throws Exception {
        try {
            authenticationService.setStrutsRequest();
            return run.call();
        } catch (EdmException ex){
            throw new EdmException(ex.getMessage());
        }
    }

    public <T> T openContext(Callable<T> run) throws Exception {
        try {
            authenticationService.open();
            return run.call();
        } catch (EdmException ex){
            log.error("", ex);
            throw new EdmException(ex.getMessage());
        } finally {
            DSApi._close();
        }
    }

    public <T> T openTransaction(Callable<T> run) throws Exception {
        T t = null;
        try {
            authenticationService.open();
            DSApi.context().begin();
            t = run.call();
            DSApi.context().commit();
        } catch (Exception ex){
        	if (DSApi.context().inTransaction()){
        		DSApi.context()._rollback();
        	}
            throw new EdmException(ex.getMessage());
        } finally {
            DSApi._close();
        }

        return t;
    }

    /**
     * Zwraca Encj� odpowiedzi b��dnej wraz z podanym powodem
     * @param type
     * @param reason
     * @param status
     * @param <T>
     * @return
     */
    public <T> ResponseEntity<T> getErrorResponse(Class<T> type, String reason, HttpStatus status){
        HttpHeaders headers = new HttpHeaders();
        headers.set(REASON, reason);
        return new ResponseEntity<T>(headers, status);
    }

     public static void executeAction(String actionName, EventActionSupport eventAction) throws Exception {
        eventAction.execute(actionName);
        if(eventAction.hasActionErrors()){
            String s = StringUtils.join(eventAction.getActionErrors().iterator(), ",");
            throw new IllegalStateException(s);
        }

    }
}
