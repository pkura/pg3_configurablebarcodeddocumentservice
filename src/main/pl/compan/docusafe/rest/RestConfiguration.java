package pl.compan.docusafe.rest;

import org.apache.solr.client.solrj.SolrServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.ModelAndView;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.rest.converter.BufferedImageHttpMessageConverter;
import pl.compan.docusafe.rest.solr.ContainerSolrCloudServer;
import pl.compan.docusafe.rest.solr.ContainerSolrServer;
import pl.compan.docusafe.rest.solr.DocumentSolrCloudServer;
import pl.compan.docusafe.rest.solr.DocumentSolrServer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <h1>RestConfiguration</h1>
 * Class is used to configuration spring container. You can define new bean definition.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"pl.compan.docusafe.rest","pl.compan.docusafe.spring","pl.compan.docusafe.core.office.search"})
public class RestConfiguration extends WebMvcConfigurerAdapter {

    public static final Logger log = LoggerFactory.getLogger(RestConfiguration.class);

    @Bean
    public MultipartResolver multipartResolver(){
        return new CommonsMultipartResolver();
    }

    @Bean(name = "documentSolrServer")
    public SolrServer documentSolrServer() throws MalformedURLException {
        String documentSolrServerUrl = AdditionManager.getPropertyOrDefault("solr.documentsServerUrl", "http://localhost:8983/solr/documents");
        String documentsCloudHostnames = AdditionManager.getProperty("solr.documentsCloudHostnames");
        if(documentsCloudHostnames != null) {
            DocumentSolrCloudServer documentSolrCloudServer = new DocumentSolrCloudServer(documentsCloudHostnames);
            documentSolrCloudServer.setDefaultCollection("documents");
            return documentSolrCloudServer;
        } else {
            return new DocumentSolrServer(documentSolrServerUrl);
        }
    }

    @Bean(name = "containerSolrServer")
    public SolrServer containerSolrServer() throws MalformedURLException {
        String containerSolrServerUrl = AdditionManager.getPropertyOrDefault("solr.containersServerUrl", "http://localhost:8983/solr/containers");
        String containerCloudHostnames = AdditionManager.getProperty("solr.containerCloudHostnames");
        if(containerCloudHostnames != null) {
            ContainerSolrCloudServer containerSolrCloudServer = new ContainerSolrCloudServer(containerCloudHostnames);
            containerSolrCloudServer.setDefaultCollection("containers");
            return containerSolrCloudServer;
        } else {
            return new ContainerSolrServer(containerSolrServerUrl);
        }
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        // Simple strategy: only path extension is taken into account
        configurer.favorPathExtension(false)
                .ignoreAcceptHeader(false)
                .favorParameter(true)
                .useJaf(false)
                .defaultContentType(new MediaType("application", "json", Charset.forName("UTF-8")))
                .mediaType("html", new MediaType("text", "html", Charset.forName("UTF-8")))
                .mediaType("xml", new MediaType("application", "xml", Charset.forName("UTF-8")))
                .mediaType("txt", new MediaType("text", "plain", Charset.forName("UTF-8")));
        log.error("The configurer is a " + configurer.getClass());
    }

    @Bean(name="contentNegotiatingViewResolver")
    public ViewResolver getContentNegotiatingViewResolver(
            ContentNegotiationManager manager) {
        ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();
        resolver.setContentNegotiationManager(manager);
        log.error("Created ContentNegotiatingViewResolver");
        return resolver;
    }

    @Bean
    public RestTemplate getRestTemplat(){
        return new RestTemplate();
    }
    /**
     * Replaces use of {@link MvcConfiguringPostProcessor}.
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {

        // List is initially empty. Create and configure what we need.
    	StringHttpMessageConverter shmc = new StringHttpMessageConverter();
    	shmc.setSupportedMediaTypes(Arrays.asList(
                new MediaType("text", "html", Charset.forName("UTF-8")),
                new MediaType("text", "plain", Charset.forName("UTF-8"))
        ));
    	shmc.setWriteAcceptCharset(true);
    	converters.add(shmc);
    	
        MappingJacksonHttpMessageConverter jmc = new MappingJacksonHttpMessageConverter();
        jmc.setSupportedMediaTypes(Arrays.asList(new MediaType("application", "json", Charset.forName("UTF-8"))));
        jmc.setPrettyPrint(true);
        converters.add(jmc);

        Jaxb2RootElementHttpMessageConverter j2 = new Jaxb2RootElementHttpMessageConverter();
        j2.setSupportedMediaTypes(Arrays.asList(new MediaType("application", "xml", Charset.forName("UTF-8"))));
        converters.add(j2);

        BufferedImageHttpMessageConverter bi = new BufferedImageHttpMessageConverter();
        converters.add(bi);
        return;
    }
    
    // This is for AutoWire annotation, to make able inject response object
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(httpServletResponseInScopeInterceptor());
    }

    private ThreadLocal<HttpServletResponse> localResponse = new ThreadLocal<HttpServletResponse>();

    @Bean
    public HandlerInterceptorAdapter httpServletResponseInScopeInterceptor() {
        return new HandlerInterceptorAdapter() {
            @Override
            public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
                localResponse.set(response);
                return true;
            }

            @Override
            public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
                localResponse.remove();
            }
        };
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public HttpServletResponse httpServletResponse() throws Exception {
        return localResponse.get();
    }
}
