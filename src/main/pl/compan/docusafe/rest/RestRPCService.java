package pl.compan.docusafe.rest;


import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.RpcRequest;
import pl.compan.docusafe.api.user.office.XmlMapMapper;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MarshallerUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Us�ugi zwi�zane z protoko�e json-rpc i xml-rpc
 */
@Controller
public class RestRPCService implements Serializable{
    private static final Logger log = LoggerFactory.getLogger(RestRPCService.class);
    public static final String COOKIE="cookie";

    @Autowired
    HttpServletRequest request;
    
    @Autowired
    ResponseHandler responseHandler;

    /**
     * 
     * Akcja zwiazana z protocolem RPC, wykorzystuj�ca jason'a jako typ transferu danych.
     *  
     * @ds.Request(value = "/jsonrpc", produces = "application/json", consumes= {"application/json"}, method = RequestMethod.POST)
     * @param <code>RpcRequest</code> obj ��dania rpc
     * @return <code>ResponseEntity</code> przechowywuj�cy dane wynikowe rpc w postaci json
     */
    @RequestMapping(value = "/jsonrpc", produces = {MediaType.TEXT_HTML_VALUE,MediaType.TEXT_PLAIN_VALUE}, consumes= {"application/json"}, method = RequestMethod.POST)
    public @ResponseBody String rpcJson(@RequestBody final RpcRequest obj){
        log.info("Execute json-rpc {}", obj);
        HttpHeaders headers = getHeaders(MediaType.APPLICATION_JSON);
        RestTemplate template = new RestTemplate();

        RpcMethod rpcMethod =RpcMethod.findRpcMethod(obj.getMethodName());
	     HttpEntity<?> e;
        if(rpcMethod.isNameEqual("Template.addTemplateForDocumentKind")){
            e = setBodyRequest2(obj, rpcMethod, headers);
            setTemplate(template);
        } else {
        	e = setBodyRequest(obj, rpcMethod, headers);
        }

        Map<String, Object> vars = getJsonParamsUrlFromRpcRequest(obj, rpcMethod);
        
        ResponseEntity<String> result = null;
        try {
			result = template.exchange(
		            getUrl(rpcMethod.getUrl()), //adres methody
		            rpcMethod.getHttpMethod(),
		            e, //body request
		            String.class,
		            vars
		    );
		    
		     responseHandler.setHeaders(result.getStatusCode());
		     return result.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("", ex);
            responseHandler.setHeaders(HttpStatus.BAD_REQUEST, ex.getResponseHeaders().getFirst(RestRequestHandler.REASON));
		}
        
        return null;
    }


    /**
     * Us�uga zwi�zana z protoko�em xml-rpc
     * 
     * @ds.Request (value = "/xmlrpc",produces = {"application/xml"}, consumes= {"application/xml"}, method = RequestMethod.POST)
     * @param <code>RpcRequest</code> obj ��dania rpc
     * @return <code>ResponseEntity</code> przechowywuj�cy dane wynikowe rpc w postaci xml
     */
    @RequestMapping(value = "/xmlrpc", produces = MediaType.TEXT_HTML_VALUE, consumes= {"application/xml"}, method = RequestMethod.POST)
    public @ResponseBody String rpcXml(@RequestBody final RpcRequest obj){
        log.info("executing xml-rpc {}", obj);
        HttpHeaders headers = getHeaders(MediaType.APPLICATION_XML);
        RestTemplate template = new RestTemplate();

        RpcMethod rpcMethod =RpcMethod.findRpcMethod(obj.getMethodName());
        HttpEntity<String> e = setBodyRequest(obj, rpcMethod, headers);
        
        Map<String, Object> vars = getXmlParamsUrlFromRpcRequest(obj, rpcMethod);
        ResponseEntity<String> result = null;
        try {
		     result = template.exchange(
		            getUrl(rpcMethod.getUrl()), //adres methody
		            rpcMethod.getHttpMethod(),
		            e, //body request
		            String.class,
		            vars
		    );
		     responseHandler.setHeaders(result.getStatusCode());
		    return result.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("", ex);
            responseHandler.setHeaders(HttpStatus.BAD_REQUEST, ex.getResponseHeaders().getFirst(RestRequestHandler.REASON));
		}
        
        return null;
    }

	/**
     * Methoda zwraca body request wysy�any w PUT, POST lub PATCH w zale�no�ci czy dana methoda posiada taki
     *
     * @param obj
     * @param rpcMethod
     * @return
     */
    private HttpEntity<String> setBodyRequest(RpcRequest rpc, RpcMethod rpcMethod, HttpHeaders headers) {
        HttpEntity<String> np = null;

        if(rpcMethod.hasRequestBody()){
            String n = rpc.getParams().iterator().next();
            np = new HttpEntity<String>(n,headers);
        } else {
            np = new HttpEntity<String>("",headers);
        }

        return np;
    }
    
    private HttpEntity<MultiValueMap<String, Object>> setBodyRequest2(RpcRequest rpc, RpcMethod rpcMethod, HttpHeaders headers) {
    	HttpEntity<MultiValueMap<String, Object>> np = null;
    	headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        if(rpcMethod.hasRequestBody()){
        		String n = rpc.getParams().iterator().next();
        		try {
        			ObjectMapper mapper = new ObjectMapper();
        			Map<String,String> map = mapper.readValue(n,Map.class);
					File file = FileUtils.decodeByBase64ToFile(map.get("file"));
	                MultiValueMap<String, Object> mvm = new LinkedMultiValueMap<String, Object>();
	                mvm.add("file", new FileSystemResource(file)); // MultipartFile
	                np = new HttpEntity<MultiValueMap<String, Object>>(mvm, headers);
	                return np;
				} catch (Exception e) {}
        } else {
            np = new HttpEntity<MultiValueMap<String, Object>>(null,headers);
        }

        return  np;
    }
    /**
     * 
     * Metoda zwraca parametry potrzebne do konstrukcji URL'a w postaci mapy o ile s�. W przeciwnym przypadku zwraca pust� mape. 
     * Dla Json
     * 
     * @param obj
     * @param rpcMethod
     * @return LinkedHashMap<String, Object>
     */
    private Map<String, Object> getJsonParamsUrlFromRpcRequest(
			final RpcRequest obj, RpcMethod rpcMethod) {
		
    	Map<String, Object> vars = new LinkedHashMap<String, Object>();
    	ObjectMapper mapper = new ObjectMapper();
    	Map<String,Object> var = null;
    	TypeReference<LinkedHashMap<String,Object>> typeRef = new TypeReference<LinkedHashMap<String,Object>>() {};
    	
		try {
			if(!rpcMethod.hasRequestBody()){
					for (String stringJson : obj.getParams()) {
						var = mapper.readValue(stringJson, typeRef);
						if(var.size() > 1){
							vars.putAll(var);
						}else {
							String key = var.keySet().iterator().next();
							Object value = var.get(key);
							vars.put(key, value );
						}
					} 
			} else {
				Iterator<String> iterator = obj.getParams().iterator();
				iterator.next();
				for (;iterator.hasNext();) {
					String jsonString = iterator.next();
					var = mapper.readValue(jsonString, typeRef);
					String key = var.keySet().iterator().next();
					Object value = var.get(key);
					key = StringUtils.remove(key, ":.+");
					vars.put(key, value );
				}
			}
		} catch (JsonParseException e) {
			log.error("Parser do not transform ", e);
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("Json mapping error ", e);
			e.printStackTrace();
		} catch (IOException e) {
			log.error("IO ", e);
			e.printStackTrace();
		}
		
		return vars;
	}
    
    /**
     * 
     * Metoda zwraca parametry potrzebne do konstrukcji URL'a w postaci mapy o ile s�. W przeciwnym przypadku zwraca pust� mape. 
     * Dla Xml
     * 
     * @param obj
     * @param rpcMethod
     * @return
     * @throws JAXBException 
     */
    private Map<String, Object> getXmlParamsUrlFromRpcRequest(RpcRequest obj,
 			RpcMethod rpcMethod) {
    	
    	Map<String, Object> vars = new LinkedHashMap<String, Object>();
    	Map<String,Object> var = null;
    	
		try {
			if(!rpcMethod.hasRequestBody()){
					for (String stringXml : obj.getParams()) {
						XmlMapMapper xmlMap = (XmlMapMapper) MarshallerUtils.unmarshall(new XmlMapMapper(), stringXml);
						var = xmlMap.getItems();
						if(var.size() > 1){
							vars.putAll(var);
						}else {
							String key = var.keySet().iterator().next();
							Object value = var.get(key);
							vars.put(key, value );
						}
					} 
			} else {
				Iterator<String> iterator = obj.getParams().iterator();
				iterator.next();
				for (;iterator.hasNext();) {
					String stringXml = iterator.next();
					XmlMapMapper xmlMap = (XmlMapMapper) MarshallerUtils.unmarshall(new XmlMapMapper(), stringXml);
					var = xmlMap.getItems();
					String key = var.keySet().iterator().next();
					Object value = var.get(key);
					vars.put(key, value );
				}
			}
		} catch (JAXBException jaxbe) {
			log.error("XML Parser do not transform ", jaxbe);
		}
		
		return vars;
 	}

    private HttpHeaders getHeaders(MediaType mediaType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType(mediaType.getType(), mediaType.getSubtype()));
        String requestedMediaType = (String) request.getHeader("requestedMediaType");
        if (mediaType.toString().contains(requestedMediaType)){
        	headers.setAccept(Arrays.asList(new MediaType(mediaType.getType(), mediaType.getSubtype())));
        } else {
        	headers.setAccept(Arrays.asList(MediaType.parseMediaType(request.getHeader("Accept"))));
        }

        String cookie = request.getHeader(COOKIE);
        if(StringUtils.isNotEmpty(cookie)){
            headers.add("cookie", cookie);
        }
        
        String ssl = request.getHeader("Certificate-ssl");
        if(StringUtils.isNotEmpty(ssl)){
            headers.add("Certificate-ssl", ssl);
        }

        return headers;
    }

    private static String getUrl(String uri){
        return Docusafe.getBaseUrl() + "/api" + uri;
    }

	private void setTemplate(RestTemplate template) {
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(new FormHttpMessageConverter());
		messageConverters.add(new StringHttpMessageConverter());
		messageConverters.add(new MappingJacksonHttpMessageConverter());
		template.setMessageConverters(messageConverters);
	}
    
}
