package pl.compan.docusafe.rest.views;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "field-view")
@XmlType(namespace="pl.compan.docusafe.rest.views", name = "field-view")
@XmlAccessorType(XmlAccessType.FIELD)
public class FieldView {

    public String value;

    public FieldView(){
    }

    public FieldView(Object value){
        this.value = value == null ? "" : value.toString();
    }

}
