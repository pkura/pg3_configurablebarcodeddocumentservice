package pl.compan.docusafe.rest.views;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;

import java.util.Map;

public class RichDocumentView extends DocumentView {
    public String content;
    public String barcode;

    public RichDocumentView(Map<String, Object> values) {
        super(values);
        this.content = (String)values.get("content");
        this.barcode = (String)values.get("barcode");
    }

    public RichDocumentView(Document document) throws EdmException {
        super(document);
    }
}
