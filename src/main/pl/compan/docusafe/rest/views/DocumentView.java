package pl.compan.docusafe.rest.views;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Date;
import java.util.List;
import java.util.Map;

@XmlRootElement(name = "doc-view")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class DocumentView {

    public Long id;
    public String title;
    public String author;
    public Date ctime;
    public Date mtime;

    @XmlElement(name="doc-kind-view")
    public DocumentKindView kind;

    @XmlElement(name="id")
    public List<Long> attachments;

    public Map<String, FieldView> fields;

	public DocumentView() {
	}

    public DocumentView(Document document) throws EdmException {
        this.id = document.getId();
        this.title = document.getTitle();
        this.author = document.getAuthor();
        this.kind = new DocumentKindView(document.getDocumentKind());
        this.ctime = document.getCtime();
        this.mtime = document.getMtime();
        this.fields = (new FieldsView(document)).fields;
        List<Long> list = FluentIterable.from(document.getAttachments()).transform(new Function<Attachment, Long>() {
            @Nullable
            @Override
            public Long apply(Attachment attachment) {
                return attachment.getId();
            }
        }).toList();
        this.attachments = list;
    }

    public DocumentView(Map<String, Object> values) {
        id = Long.valueOf((String) values.get("id"));
        title = (String)values.get("title");
        author = (String)values.get("author");

        ctime = (Date)values.get("ctime");
        mtime = (Date)values.get("mtime");

        kind = new DocumentKindView();
        kind.name = (String)values.get("documentKind");
    }

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public DocumentKindView getKind() {
		return kind;
	}

	public List<Long> getAttachments() {
		return attachments;
	}
    
    

}
