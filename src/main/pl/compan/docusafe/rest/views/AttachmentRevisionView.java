package pl.compan.docusafe.rest.views;

import pl.compan.docusafe.core.base.AttachmentRevision;

import java.util.Date;

public class AttachmentRevisionView {

    public final Long id;
    public final Integer revision;
    public final Date ctime;
    public final String author;
    public final String originalFilename;
    public final Integer size;
    public final String mime;
    public final String lparam;
    public final Long wparam;
    public final Integer onDisc;
    public final Long contentRefId;
    public final Boolean editable;
    public final String verificationCode;

    public AttachmentRevisionView(AttachmentRevision revision) {
        this.id = revision.getId();
        this.revision = revision.getRevision();
        this.ctime = revision.getCtime();
        this.author = revision.getAuthor();
        this.originalFilename = revision.getOriginalFilename();
        this.size = revision.getSize();
        this.mime = revision.getMime();
        this.lparam = revision.getLparam();
        this.wparam = revision.getWparam();
        this.onDisc = revision.getOnDisc();
        this.contentRefId = revision.getContentRefId();
        this.editable = revision.getEditable();
        this.verificationCode = revision.getVerificationCode();
    }

}
