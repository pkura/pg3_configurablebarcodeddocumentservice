package pl.compan.docusafe.rest.views;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.regtype.RegistryEntry;

import java.util.Date;
import java.util.Set;

public class AttachmentView {

    public final Long id;
    public final String title;
    public final String description;
    public final Date ctime;
    public final String author;
    public final Set<AttachmentRevisionView> revisions;
    public final boolean deleted;
    public final String barcode;
    public final String lparam;
    public final Long wparam;
    public final String cn;

    public final String fieldCn;

    public final Integer type;
    public final RegistryEntry registryEntry;
    public final String uuid;

    public final boolean editable;


    public AttachmentView(Attachment attachment) {
        this.id = attachment.getId();
        this.title = attachment.getTitle();
        this.description = attachment.getDescription();
        this.ctime = attachment.getCtime();
        this.author = attachment.getAuthor();
        this.deleted = attachment.isDeleted();
        this.barcode = attachment.getBarcode();
        this.lparam = attachment.getLparam();
        this.wparam = attachment.getWparam();
        this.cn = attachment.getCn();
        this.fieldCn = attachment.getFieldCn();
        this.type = attachment.getType();
        this.registryEntry = attachment.getRegistryEntry();
        this.uuid = attachment.getUuid();
        this.editable = attachment.isEditable();

        this.revisions = FluentIterable.from(attachment.getRevisions()).transform(new Function<AttachmentRevision, AttachmentRevisionView>() {
            @Override
            public AttachmentRevisionView apply(AttachmentRevision revision) {
                return new AttachmentRevisionView(revision);
            }
        }).toSet();
    }
}
