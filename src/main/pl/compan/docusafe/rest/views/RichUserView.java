package pl.compan.docusafe.rest.views;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

import javax.annotation.Nullable;
import java.util.List;

public class RichUserView {
    public Long id;
    public String firstname;
    public String lastname;
    public String username;
    public String fullname;
    public List<DivisionView> divisions;

    public RichUserView(DSUser user) {
        id = user.getId();
        firstname = user.getFirstname();
        lastname = user.getLastname();
        username = user.getName();
        fullname = lastname+" "+firstname;
        divisions = FluentIterable.from(user.getDivisionsAsListSafe()).transform(new Function<DSDivision, DivisionView>() {
            @Override
            public DivisionView apply(DSDivision division) {
                return new DivisionView(division);
            }
        }).toList();
    }

    public static ImmutableList<RichUserView> getAll() throws EdmException {
        return FluentIterable.from(DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME)).transform(new Function<DSUser, RichUserView>() {
            @Override
            public RichUserView apply(DSUser user) {
                return new RichUserView(user);
            }
        }).toList();
    }
}
