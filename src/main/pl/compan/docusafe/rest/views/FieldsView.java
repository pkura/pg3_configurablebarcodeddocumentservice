package pl.compan.docusafe.rest.views;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;

import java.util.*;

public class FieldsView {

    public Map<String, FieldView> fields;

    public FieldsView(){
    }

    public FieldsView(Document doc) throws EdmException {
        FieldsManager fm = doc.getFieldsManager();

        fields = new HashMap<String, FieldView>();
        putFieldViews(fields, fm);
    }

    protected void putFieldViews(Map<String, FieldView> fields, FieldsManager fm) throws EdmException {
        Set<Map.Entry<String, Object>> fieldValues = fm.getFieldValues().entrySet();
        for(Map.Entry<String, Object> entry: fieldValues) {
            fields.put(entry.getKey(), new FieldView(entry.getValue()));
        }
    }

}
