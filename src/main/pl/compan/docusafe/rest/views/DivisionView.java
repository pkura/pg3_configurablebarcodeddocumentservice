package pl.compan.docusafe.rest.views;

import com.google.common.base.Optional;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.sql.DivisionImpl;

public class DivisionView {
    public String code;
    public Long id;
    public Long parentId;
    public String guid;
    public String name;
    public String divisionType;

    public DivisionView(DSDivision division) {
        id = division.getId();
        guid = division.getGuid();
        name = division.getName();
        code = division.getCode();
        divisionType = division.getDivisionType();

        Optional<DivisionImpl> parent = division.getParentSafe();
        parentId = parent.isPresent() ? parent.get().getId() : null;
    }
}
