package pl.compan.docusafe.rest.views;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import pl.compan.docusafe.core.dockinds.DocumentKind;

@XmlRootElement(name="doc-kind-view")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class DocumentKindView {

    public String name;
    public String cn;

	public DocumentKindView() {
	}
    
    public DocumentKindView(DocumentKind kind) {
        this.name = kind.getName();
        this.cn = kind.getCn();
    }

    public String getName() {
  		return name;
  	}

  	public void setName(String name) {
  		this.name = name;
  	}

  	public String getCn() {
  		return cn;
  	}

  	public void setCn(String cn) {
  		this.cn = cn;
  	}
    
}
