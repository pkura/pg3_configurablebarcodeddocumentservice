package pl.compan.docusafe.rest.exceptions;

@Deprecated
abstract public class HttpResponseException extends RuntimeException {
    public HttpResponseException(String s) {
        super(s);
    }

    public HttpResponseException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpResponseException(Throwable cause) {
        super(cause);
    }
}
