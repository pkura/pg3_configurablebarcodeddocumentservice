/**
 * Pakiet zawiera opis metod jakie s� dost�pne na interfejsie RESTOWYM.<br/>
 * Przyk�ad opisu dostepu do danej metody:<br/><br/>
 *
 *  &#64;RequestMapping(value="/login", method=POST, params={"username","password"})<br/>
 *  informuje i� dany link znajduje si� pod adresem: <code>http://host:port/api/login </code><br/>
 *<br/>
 *  i b�dzie zawiera� parametry:<br/>
 *      - username<br/>
 *      - password<br/>
 *<br/>
 *  parametr <code>method=POST</code> okre�la �e metoda na interfejsie przyjmuje tylko i wy��cznie<br/>
 *  rodzaj �adania POST.<br/>
 *<br/>
 *  &#64;ResponseStatus(value=ACCEPTED) - okre�la rodzaj odpowiedzi po poprawnym wykonaniu �adania.<br/>
 */
package pl.compan.docusafe.rest;
