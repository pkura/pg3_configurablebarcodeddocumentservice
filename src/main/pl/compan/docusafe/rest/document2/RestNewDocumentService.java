package pl.compan.docusafe.rest.document2;

import com.google.common.base.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.templating.DocumentSnapshotSerializer;
import pl.compan.docusafe.rest.handlers.ResponseHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping(produces = "application/json")
public class RestNewDocumentService {

    @Autowired
    ResponseHandler responseHandler;

    @RequestMapping(value = "/document2/{documentId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity get(@PathVariable final Long documentId, HttpServletRequest request) throws EdmException {

        return responseHandler.handle(new DSTransaction().executeOpenContext(request, new DSTransaction.Function<Object>() {
            @Override
            public Object apply() throws Exception {
                Document document = Document.find(documentId);

                Map<String, Object> mapFromDocument = DocumentSnapshotSerializer.getMapFromDocument(document);

                return mapFromDocument;
            }
        }));
    }

}
