package pl.compan.docusafe.rest;

        import org.springframework.http.HttpStatus;
        import org.springframework.web.bind.annotation.ResponseStatus;
        import pl.compan.docusafe.core.EdmException;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerError extends RuntimeException {
    public InternalServerError(Throwable e) {
        super("500 - " + e.getMessage(), e);
    }

    public InternalServerError(String msg) {
        super(msg);
    }
    public InternalServerError(String msg, Throwable ex) {
        super(msg, ex);
    }
}