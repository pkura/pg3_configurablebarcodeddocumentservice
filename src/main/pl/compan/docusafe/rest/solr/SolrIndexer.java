package pl.compan.docusafe.rest.solr;

import org.apache.solr.client.solrj.SolrServer;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import rx.Observable;
import rx.Subscriber;

public abstract class SolrIndexer<T> {

    private static final Logger log = LoggerFactory.getLogger(SolrIndexer.class);

    public void execute(Integer limit) throws EdmException {
        Observable<T> obs = AllObservable.get(getClazz(), limit);
        subscribe(obs);
    }

    public void execute() throws EdmException {
        execute(Integer.MAX_VALUE);
    }

    protected abstract Class<T> getClazz();

    protected void subscribe(Observable<? extends T> obs) {
        obs.subscribe(new Subscriber<T>() {
            @Override
            public final void onCompleted() {
                log.debug("[onCompleted] completed, commiting");

                try {
                    commit();
                } catch (Exception ex) {
                    log.debug("[onCompleted] error, cannot commit", ex);
                }
            }

            @Override
            public final void onError(Throwable e) {
                log.error("[onError] error", e);
                try {
                    rollback();
                } catch (Exception ex) {
                    log.error("[onError] Cannot rollback", ex);
                }
            }

            @Override
            public final void onNext(T obj) {
                try {
                    updateIndex(obj);
                } catch (Exception ex) {
                    log.error("[onNext] cannot index obj = {}", obj, ex);
                }
            }
        });
    }

    protected abstract void commit() throws Exception;

    protected abstract void rollback() throws Exception;

    protected abstract void updateIndex(T obj) throws Exception;

    protected abstract void deleteIndexByQuery() throws Exception;

    protected void deleteIndex() throws Exception {
        log.debug("[deleteIndex] begin");
        try {
            deleteIndexByQuery();
            commit();
        } catch(Exception ex) {
            log.error("[deleteIndex] error", ex);
            rollback();
            throw new Exception("[deleteIndex] delete query failed", ex);
        }
        log.debug("[deleteIndex] end");
    }


}
