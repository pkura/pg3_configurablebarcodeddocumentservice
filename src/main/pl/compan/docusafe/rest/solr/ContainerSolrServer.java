package pl.compan.docusafe.rest.solr;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;

public class ContainerSolrServer extends HttpSolrServer {
    public ContainerSolrServer(String baseURL) {
        super(baseURL);
    }
}
