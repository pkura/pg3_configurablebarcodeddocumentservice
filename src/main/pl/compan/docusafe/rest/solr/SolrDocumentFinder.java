package pl.compan.docusafe.rest.solr;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.inject.Named;
import java.util.Iterator;

@Service
public class SolrDocumentFinder {

    private static final Logger log = LoggerFactory.getLogger(SolrDocumentIndexer.class);

    private SolrServer solr;

    @Autowired
    public SolrDocumentFinder(@Named(value = "documentSolrServer") SolrServer solr) {
        this.solr = solr;
    }

    public QueryResponse search(String queryString) throws SolrServerException {
        SolrQuery query = new SolrQuery();

//        query.setQuery("id:291");
        query.setQuery(queryString);
       Integer size =  Integer.parseInt(Docusafe.getAdditionPropertyOrDefault("solr.HighlightSnippets", "2"));
        query = query.setHighlight(true).setHighlightSnippets(size).setParam("hl.fl", "content");

        QueryResponse response = solr.query(query);

        return response;

//        Iterator<SolrDocument> it = response.getResults().iterator();

//        while(it.hasNext()) {
//            SolrDocument doc = it.next();
//
//            log.debug("id = {}, allowedUsers = {}", doc.get("id"), doc.get("allowedUsers"));
//            log.debug("    classes: id = {}, allowedUsers = {}", doc.get("id").getClass(), doc.get("allowedUsers").getClass());
//            log.debug("    hightlighting = {}", response.getHighlighting());
////            log.debug("doc --> {}", doc);
//        }
    }

}
