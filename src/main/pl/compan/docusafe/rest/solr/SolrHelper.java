package pl.compan.docusafe.rest.solr;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class SolrHelper {

    public static final SolrServer getSolr() {
        String url = "http://localhost:8983/solr/";
        return new HttpSolrServer(url);
    }

    public static Map<String,Object> getPartialUpdate( Object value ) {
        Map<String, Object> partialUpdate = new HashMap<String, Object>();
        partialUpdate.put("set", value );
        return partialUpdate;
    }


}
