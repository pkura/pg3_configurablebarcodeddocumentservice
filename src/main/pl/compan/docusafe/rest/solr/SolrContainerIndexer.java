package pl.compan.docusafe.rest.solr;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.compan.docusafe.core.jackrabbit.PrivilegesProvider;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.inject.Named;

@Service
public class SolrContainerIndexer extends SolrIndexer<Container> {

    private static final String DISCRIMINATOR_FOLDER = "FOLDER";
    private static final String DISCRIMINATOR_CASE = "CASE";

    private static final Logger log = LoggerFactory.getLogger(SolrContainerIndexer.class);

    @Autowired
    @Named(value = "containerSolrServer")
    protected SolrServer solr;

    @Override
    protected Class<Container> getClazz() {
        return Container.class;
    }

    @Override
    protected void commit() throws Exception {
        solr.commit();
    }

    @Override
    protected void rollback() throws Exception {
        solr.rollback();
    }

    @Override
    protected void updateIndex(Container container) throws Exception {
        log.debug("[updateIndex] indexing container.id = {}", container.getId());
        SolrInputDocument solrDocument = new SolrInputDocument();

        solrDocument.addField("id", container.getId());
        solrDocument.addField("officeId", container.getOfficeId());
        solrDocument.addField("archivedDate", container.getArchivedDate());
        solrDocument.addField("archiveStatus", container.getArchiveStatus().ordinal());
        solrDocument.addField("year", container.getYear());
        solrDocument.addField("author", container.getAuthor());
        solrDocument.addField("ctime", container.getCtime());
        
        // pole do uprawnien
        solrDocument.addField("divisionguid", container.getDivisionGuid());
        solrDocument.addField("clerk", container.getClerk());

        Rwa rwa = container.getRwa();
        solrDocument.addField("rwaId", rwa.getId());
        solrDocument.addField("symbol", rwa.getDigit1());
        solrDocument.addField("achome", rwa.getDigit2());

        if(container instanceof OfficeFolder) {
            solrDocument.addField("discriminator", DISCRIMINATOR_FOLDER);

            OfficeFolder officeFolder = (OfficeFolder) container;

            solrDocument.addField("name", officeFolder.getName());
        } else if(container instanceof OfficeCase) {
            solrDocument.addField("discriminator", DISCRIMINATOR_CASE);

            OfficeCase officeCase = (OfficeCase)container;

            solrDocument.addField("title", officeCase.getTitle());
            solrDocument.addField("description", officeCase.getDescription());
            solrDocument.addField("openDate", officeCase.getOpenDate());
            solrDocument.addField("finishDate", officeCase.getFinishDate());
            if(officeCase.getStatus() != null){
            	solrDocument.addField("status", officeCase.getStatus().getName());
            }
        }

        UpdateResponse response = solr.add(solrDocument);
    }

    protected void deleteIndexByQuery() throws Exception {
        solr.deleteByQuery("*:*");
    }
}
