package pl.compan.docusafe.rest.solr;

import org.apache.solr.client.solrj.impl.CloudSolrServer;

import java.net.MalformedURLException;

public class ContainerSolrCloudServer extends CloudSolrServer{
    public ContainerSolrCloudServer(String zkHost) throws MalformedURLException {
        super(zkHost);
    }
}
