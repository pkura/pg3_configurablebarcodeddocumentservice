package pl.compan.docusafe.rest.solr;

import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;

import java.net.MalformedURLException;

public class DocumentSolrCloudServer extends CloudSolrServer {
    public DocumentSolrCloudServer(String zkHost) throws MalformedURLException {
        super(zkHost);
    }
}
