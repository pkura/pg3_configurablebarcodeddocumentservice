package pl.compan.docusafe.rest.solr;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by user on 2015-02-27.
 */
public class MaxAttachmentRevisionID {
    private static final Logger log = LoggerFactory.getLogger(MaxAttachmentRevisionID.class);
    private static final String luceneIndexNode = "LuceneIndex", maxRevisionIdKey = "MaxRevisionId";
    private static MaxAttachmentRevisionID instance;

    public static MaxAttachmentRevisionID getInstance(){
        if(instance == null){
            instance = new MaxAttachmentRevisionID();
        }
        return instance;
    }


    public Long getValue() throws EdmException {
       // DSApi.open();
        Long value = DSApi.context().systemPreferences().node(luceneIndexNode).getLong(maxRevisionIdKey, 0l);
        //DSApi.close();
        return value;
    }
    public void saveValue() throws EdmException {
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement("SELECT MAX(ID) FROM DS_ATTACHMENT_REVISION");
            ResultSet rs = ps.executeQuery();
            rs.next();
            DSApi.context().systemPreferences().node(luceneIndexNode).putLong(maxRevisionIdKey, rs.getLong(1));
        } catch (SQLException e) {
            log.debug(e.getMessage(), e);
        } finally {
            if (ps != null) {
                DSApi.context().closeStatement(ps);
            }
        }
    }
}
