package pl.compan.docusafe.rest.solr;

import org.apache.commons.dbutils.DbUtils;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.document.DocumentSmartFinder;
import pl.compan.docusafe.core.jackrabbit.PrivilegesProvider;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.lucene.NewLuceneService;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.expression.Expression;

import javax.inject.Named;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class SolrDocumentIndexer extends SolrIndexer<Document> {

    private static final Logger log = LoggerFactory.getLogger(SolrDocumentIndexer.class);

    @Autowired
    @Named(value = "documentSolrServer")
    protected SolrServer solr;
    private boolean indexAll;

    public SolrDocumentIndexer() {
    }

    @Override
    protected Class<Document> getClazz() {
        return Document.class;
    }

    @Override
    protected void commit() throws Exception {
        solr.commit();
        MaxAttachmentRevisionID.getInstance().saveValue();
    }

    @Override
    protected void rollback() throws Exception {
        solr.rollback();
    }

    public void updateIndex(Document document) throws Exception {
        log.debug("[updateIndex] indexing doc.id = {}", document.getId());
        SolrInputDocument solrDocument = new SolrInputDocument();

        solrDocument.addField("id", document.getId());
        solrDocument.addField("id_long", document.getId());
        solrDocument.addField("title", document.getTitle());
        solrDocument.addField("description", document.getDescription());
        solrDocument.addField("documentKindId", document.getDocumentKind().getId());
        solrDocument.addField("documentKind", document.getDocumentKind().getName());
        solrDocument.addField("ctime", document.getCtime());

        try{
        	solrDocument.addField("ctimeString", DateUtils.formatSqlDate(document.getCtime()));
        }
        catch(Exception e){}

        solrDocument.addField("mtime", document.getMtime());
        solrDocument.addField("archivedDate", document.getArchivedDate());
        solrDocument.addField("author", document.getAuthor());
        solrDocument.addField("isArchived", document.getIsArchived());
        solrDocument.addField("lastRemark", document.getLastRemark());
        solrDocument.addField("deleted", document.isDeleted());

        addChangelogData(document, solrDocument);

        Long maxAttachmentRevisionId = null;
        if(!this.indexAll){
            maxAttachmentRevisionId = MaxAttachmentRevisionID.getInstance().getValue();
        }
        String content = getContent(document, maxAttachmentRevisionId);
        if(content == null) {
        	try{
        		content = DocumentSmartFinder.instance().find(document.getId()).getFieldValue("content").toString();
        	}
        	catch(Exception e){
        		log.error(e.getMessage(), e);
        	}
        }
        solrDocument.setField("content", content);

        try{
        	solrDocument.addField("authorName", DSUser.findByUsername(document.getAuthor()).getFirstnameLastnameName());
        }
        catch(Exception e){}

        if(document instanceof InOfficeDocument) {
            solrDocument.addField("documentDate", ((InOfficeDocument) document).getDocumentDate());

            try{
            	solrDocument.addField("documentDateString", DateUtils.formatSqlDate(document.getCtime()));
            }
            catch(Exception e){}

            solrDocument.addField("postalRegNumber", ((InOfficeDocument) document).getPostalRegNumber());
        }

        PrivilegesProvider provider = new PrivilegesProvider(document);

        solrDocument.addField("allowedUsers", provider.getReadUsers());
        solrDocument.addField("allowedGroups", provider.getReadGroups());

        if(document instanceof OfficeDocument) {
            solrDocument.addField("caseDocumentId", ((OfficeDocument)document).getCaseDocumentId());
            solrDocument.addField("barcode", ((OfficeDocument) document).getBarcode());
            solrDocument.addField("barcode_keyword", ((OfficeDocument) document).getBarcode());
            solrDocument.addField("officeNumber", ((OfficeDocument) document).getOfficeNumber());
            try{
            	DSUser user = DSUser.findByUsername(((OfficeDocument) document).getCurrentAssignmentUsername());
            	solrDocument.addField("currentAssignmentUsername", user.getLastnameFirstnameName());
            }
            catch(Exception e){}
        }

        UpdateResponse response = solr.add(solrDocument);
    }

    protected void addChangelogData(Document document, SolrInputDocument solrDocument) throws EdmException, SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement("select username, what from ds_document_changelog where document_id = ?");
            ps.setLong(1, document.getId());
            rs = ps.executeQuery();

            if(rs.next()) {
                String accessedBy = rs.getString(1);
                String accessedAs = rs.getString(2);

                solrDocument.addField("accessedAs", accessedAs);
                solrDocument.addField("accessedBy", accessedBy);
            }

        } finally {
            DbUtils.closeQuietly(ps);
            DbUtils.closeQuietly(rs);
        }
    }

    @Override
    protected void deleteIndexByQuery() throws Exception {
        solr.deleteByQuery("*:*");
    }

    /**
     * <p>
     *     Returns concatenated text attachments.
     * </p>
     *
     * @param document
     * @return concateneated text attachments
     */
    private String getContent(Document document, Long maxAttachmentRevisionId) throws EdmException, IOException {
        return NewLuceneService.getTextFromAttachments(document, false, maxAttachmentRevisionId);
    }

    public void setIndexAll(boolean indexAll) {
        this.indexAll = indexAll;
    }
}
