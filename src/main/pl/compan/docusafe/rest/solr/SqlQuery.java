package pl.compan.docusafe.rest.solr;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.apache.commons.lang3.StringUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class SqlQuery {

    private static final Logger log = LoggerFactory.getLogger(SqlQuery.class);

//	private static final String DATE_FORMAT = "yyyy-MM-ddTHH:mm:ss.000Z";
//	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
//	private static final String DATE_SUFFIX = "T00:00:00.000+00:00";

    public static enum PropertyType {
        STRING,
        URI,
        HTML,
        ID,
        DECIMAL,
        INTEGER,
        DATETIME,
        BOOLEAN
    }

    public enum OrderType {
        ASC("ASC"),
        DESC("DESC");

        private String name;

        private OrderType(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }
    }

    public static final OrderType ORDER_DESC = OrderType.DESC;
    public static final OrderType ORDER_ASC = OrderType.ASC;

    private List<String> selectList = new ArrayList<String>();
    private String from;
    private String where = "";
    private String orderBy;
    private OrderType orderType = ORDER_ASC;

    private List<String> list = new ArrayList<String>();

    public SqlQuery(){}

    public SqlQuery(String from) {
        this.from = from;
    }

    public String toString()
    {
        return "SELECT "+(selectList.isEmpty() ? "*" : StringUtils.join(selectList, ", "))+" FROM "+from+(where.equals("") ? "" : " WHERE "+where)+(orderBy != null ? " ORDER BY "+orderBy+" "+orderType.getName() : "");
    }

    public SqlQuery orderBy(String field, OrderType type)
    {
        orderBy = field;
        orderType = type;
        return this;
    }

    public SqlQuery orderBy(String field)
    {
        orderBy = field;
        return this;
    }

    public SqlQuery stack(String element)
    {
        list.add(element);
        return this;
    }

    /**
     * Returns copy of {@code list} and clears {@code list}.
     * @return
     */
    public List<String> clearStack()
    {
        ArrayList<String> ret = (ArrayList<String>) ((ArrayList)list).clone();
        list.clear();
        return ret;
    }

    public List<String> getStack()
    {
        return list;
    }

    public String orStack()
    {
        return opStack("OR");
    }

    public String andStack()
    {
        return opStack("AND");
    }

    private String opStack(String op)
    {
        List<String> list = clearStack();
        return opWhere(op, list);
    }

    private String opWhere(String op, List<String> list)
    {
        if(! list.isEmpty())
        {
            where += "(" + StringUtils.join(list, " "+op+" ") + ")";
        }
        return where;
    }

    public SqlQuery select(String... fields)
    {
        selectList.addAll(Arrays.asList(fields));
        return this;
    }

    public String and(String... xs)
    {
        return opWhere("AND", Arrays.asList(xs));
    }

    // === statics ===

    public static String in(String key, List<String> values,
                            final PropertyType type)
    {
        Collection<String> newValues = Collections2.transform(values, new Function<String, String>() {
            @Override
            public String apply(String x) {
                return toSqlString(x, type);
            }
        });

        return key + " IN (" + StringUtils.join(newValues, ", ") + ")";
    }

    public static String in(String key, String[] formValues, PropertyType type)
    {
        return in(key, Arrays.asList(formValues), type);
    }

    // operations: =, >=, >, <=, <, <>
    private static String op(String op, String key, String formValue, PropertyType type)
    {
        String sqlValue = toSqlString(formValue, type);
        return key + " " + op + " " + sqlValue;
    }

    public static String eq(String key, String formValue, PropertyType type)
    {
        return op("=", key, formValue, type);
    }

    public static String neq(String key, String formValue, PropertyType type)
    {
        return op("<>", key, formValue, type);
    }

    public static String gte(String key, String formValue, PropertyType type)
    {
        return op(">=", key, formValue, type);
    }

    public static String gt(String key, String formValue, PropertyType type)
    {
        return op(">", key, formValue, type);
    }

    public static String lte(String key, String formValue, PropertyType type)
    {
        return op("<=", key, formValue, type);
    }

    public static String lt(String key, String formValue, PropertyType type)
    {
        return op("<", key, formValue, type);
    }

    private static String toSqlString(String value, PropertyType type)
    {
        if(type == PropertyType.STRING || type == PropertyType.URI || type == PropertyType.HTML
                || type == PropertyType.ID)
        {
            return "'"+value+"'";
        }
        else if(type == PropertyType.DECIMAL || type == PropertyType.INTEGER)
        {
            return value;
        }
        else if(type == PropertyType.DATETIME)
        {
            return "TIMESTAMP '"+value+"'";
        }
        else if(type == PropertyType.BOOLEAN)
        {
            return value.equals("true") ? "true" : "false";
        }
        else
        {
            log.error("[toSqlString] unsupported type = {}, from value = {}", type, value);
            return null;
        }
    }

}

