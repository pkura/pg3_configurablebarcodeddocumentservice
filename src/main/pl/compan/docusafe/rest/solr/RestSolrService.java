package pl.compan.docusafe.rest.solr;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@Controller
@RequestMapping(produces = "application/json")
public class RestSolrService {

    private static final Logger log = LoggerFactory.getLogger(RestSolrService.class);

    @Autowired
    private ResponseHandler responseHandler;

    @Autowired
    private SolrDocumentIndexer documentIndexer;

    @Autowired
    private SolrDocumentFinder documentFinder;

    @Autowired
    private SolrContainerFinder containerFinder;

    @Autowired
    private SolrContainerIndexer containerIndexer;

    @RequestMapping(value="/solr/documents", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity createDocumentsIndex(@RequestParam(value = "limit", required = false) final Integer limit, @RequestParam(value = "indexall", required = false) final Boolean indexAll) {

        try {
            if(indexAll == null){
                documentIndexer.setIndexAll(false);
            }else {
                documentIndexer.setIndexAll(indexAll);
            }
            documentIndexer.execute(limit == null ? Integer.MAX_VALUE : limit);

            String response = "OK";

            return responseHandler.handle(response);
        } catch (Throwable e) {
            log.error("/solr error", e);
            return new ResponseEntity<String>(ExceptionUtils.getStackTrace(e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value="/solr/documents", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity searchDocuments(@RequestParam(value = "query", required = true) final String query) throws SolrServerException {

        try {
            QueryResponse search = documentFinder.search(query);
            return responseHandler.handle(search.getResults().getNumFound());
        } catch(Throwable ex) {
            log.error("POST /solr error", ex);
            return new ResponseEntity<String>(ExceptionUtils.getStackTrace(ex), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value="/solr/documents", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity searchDocuments() throws SolrServerException {

        try {
            documentIndexer.deleteIndex();
            return responseHandler.handle("OK");
        } catch(Throwable ex) {
            log.error("POST /solr error", ex);
            return new ResponseEntity<String>(ExceptionUtils.getStackTrace(ex), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value="/solr/containers", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity createContainerIndex(@RequestParam(value = "limit", required = false) final Integer limit) {

        try {
            containerIndexer.execute(limit == null ? Integer.MAX_VALUE : limit);

            String response = "OK";

            return responseHandler.handle(response);
        } catch (Throwable e) {
            log.error("/solr error", e);
            return new ResponseEntity<String>(ExceptionUtils.getStackTrace(e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value="/solr/containers", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity deleteContainerIndex() {

        try {
            containerIndexer.deleteIndex();
            return responseHandler.handle("OK");
        } catch (Throwable e) {
            log.error("/solr error", e);
            return new ResponseEntity<String>(ExceptionUtils.getStackTrace(e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value="/solr/containers", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity searchContainers(@RequestParam(value = "query", required = true) final String query) throws SolrServerException {

        try {
//            containerFinder.search(query);
            return responseHandler.handle("NOT IMPLEMENTED");
        } catch(Throwable ex) {
            log.error("POST /solr error", ex);
            return new ResponseEntity<String>(ExceptionUtils.getStackTrace(ex), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
