package pl.compan.docusafe.rest.solr;

import org.apache.commons.lang.NotImplementedException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import rx.Observable;
import rx.Subscriber;

import java.util.Iterator;
import java.util.List;

public class AllObservable {

    public static <U> Observable<U> get(Class<U> clazz) throws EdmException {
        return get(clazz, Integer.MAX_VALUE);
    }

    public static <U> Observable<U> get(final Class<U> clazz, final int maxLimit) throws EdmException {

        return Observable.create(new Observable.OnSubscribe<U>() {
            @Override
            public void call(final Subscriber<? super U> subscriber) {

                try {
                    new DSTransaction().executeOpenAdmin(new DSTransaction.Function<Object>() {
                        @Override
                        public Object apply() throws Exception {
                            Iterator<U> it = new HibernateBatchIterator<U>(clazz, maxLimit);

                            while (it.hasNext()) {
                                U next = it.next();
                                subscriber.onNext(next);
                            }

                            subscriber.onCompleted();

                            return null;
                        }
                    });
                } catch (Exception e) {
                    subscriber.onError(e);
                }

            }
        });
    }

    private static class HibernateBatchIterator<T> implements Iterator<T> {

        private static final Logger log = LoggerFactory.getLogger(HibernateBatchIterator.class);

        private static final int DELTA_OFFSET = 50;
        private Class<T> clazz;
        private List<T> list;
        private Iterator<T> listIterator;
        private int limit = DELTA_OFFSET;
        private boolean stop = false;
        private int offset = 0;
        private int maxLimit = Integer.MAX_VALUE;
        private int counter = 0;

        public HibernateBatchIterator(Class<T> clazz) throws EdmException {
            this.clazz = clazz;
            init();
        }

        public HibernateBatchIterator(Class<T> clazz, int maxLimit) throws EdmException {
            this.clazz = clazz;
            this.maxLimit = maxLimit;
            init();
        }

        public void init() throws EdmException {
            Criteria criteria = DSApi.context().session().createCriteria(clazz);
            criteria.setMaxResults(limit);
            criteria.setFirstResult(offset);
            criteria.addOrder(Order.desc("id"));
            list = criteria.list();

            if(list.size() == 0) {
                stop = true;
            } else {
                listIterator = list.iterator();
            }

        }

        @Override
        public boolean hasNext() {
            if(stop) {
                return false;
            }

            if(counter > maxLimit) {
                stop = true;
                return false;
            }

            if(listIterator.hasNext()) {
                return true;
            } else {
                this.offset += DELTA_OFFSET;
                try {
                    init();
                    return hasNext();
                } catch (EdmException e) {
                    log.error("[hasNext] error", e);
                    return false;
                }
            }
        }

        @Override
        public T next() {
            counter++;
            return listIterator.next();
        }

        @Override
        public void remove() {
            throw new NotImplementedException("Method remove is not implemented");
        }
    }

}
