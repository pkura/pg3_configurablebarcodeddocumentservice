package pl.compan.docusafe.rest.solr;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.queryparser.classic.QueryParser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class LuceneQuery {

    private static final Logger log = LoggerFactory.getLogger(LuceneQuery.class);

    /**
     * Escape query parameters?
     */
    private final boolean escape;

    private Mode mode;

    private String leftBracket;
    private String rightBracket;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public static enum Mode {
        QUOTE,
        PARANTHESES
    }

    public static enum PropertyType {
        STRING,
        URI,
        HTML,
        ID,
        DECIMAL,
        INTEGER,
        DATETIME,
        BOOLEAN;
    }
    public enum OrderType {
        ASC("ASC"),
        DESC("DESC");

        private String name;

        private OrderType(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }

    }
    public static final OrderType ORDER_DESC = OrderType.DESC;
    public static final OrderType ORDER_ASC = OrderType.ASC;
    private List<String> selectList = new ArrayList<String>();

    private String from;
    private String where = "";
    private String orderBy;
    private OrderType orderType = ORDER_ASC;
    private List<String> list = new ArrayList<String>();

    public LuceneQuery(){
        this(Mode.PARANTHESES);
    }

    public LuceneQuery(Mode mode) {
        this(mode, true);
    }

    public LuceneQuery(Mode mode, boolean escape) {
        this.mode = mode;
        this.escape = escape;
        calcBracket();
    }

    @Deprecated
    private void calcBracket() {
        if(mode == Mode.PARANTHESES) {
            leftBracket = "(";
            rightBracket = ")";
        } else if(mode == Mode.QUOTE) {
            leftBracket = "\"";
            rightBracket = "\"";
        }
    }

    private String wrapInBracket(String value, Mode mode) {
        if(mode == Mode.PARANTHESES) {
            return "("+value+")";
        } else if(mode == Mode.QUOTE) {
            return "\""+value+"\"";
        } else {
            return value;
        }
    }

    public String toString()
    {
        return where.equals("") ? "*:*" : where;
    }

    public LuceneQuery stack(String element)
    {
        list.add(element);
        return this;
    }

    /**
     * Returns copy of {@code list} and clears {@code list}.
     * @return
     */
    public List<String> clearStack()
    {
        ArrayList<String> ret = (ArrayList<String>) ((ArrayList)list).clone();
        list.clear();
        return ret;
    }

    public List<String> getStack()
    {
        return list;
    }

    public String orStack()
    {
        return opStack("OR");
    }

    public String andStack()
    {
        return opStack("AND");
    }

    private String opStack(String op)
    {
        List<String> list = clearStack();
        return opWhere(op, list);
    }

    private String opWhere(String op, List<String> list)
    {
        if(! list.isEmpty())
        {
            where += "(" + StringUtils.join(list, " "+op+" ") + ")";
        }
        return where;
    }

    public LuceneQuery select(String... fields)
    {
        selectList.addAll(Arrays.asList(fields));
        return this;
    }

    public String and(String... xs)
    {
        return opWhere("AND", Arrays.asList(xs));
    }

    // === statics ===

    public String in(final String key, Collection<String> values,
                            final PropertyType type)
    {
        Collection<String> newValues = Collections2.transform(values, new Function<String, String>() {
            @Override
            public String apply(String x) {
                return key+":"+toSqlString(x, type, mode);
            }
        });

        return "(" + StringUtils.join(newValues, " OR ") + ")";
    }

    public String in(String key, String[] formValues, PropertyType type)
    {
        return in(key, Arrays.asList(formValues), type);
    }

    // operations: =, >=, >, <=, <, <>
    private String op(String op, String key, String formValue, PropertyType type)
    {
        return op(op, key, formValue, type, this.mode);
    }

    private String op(String op, String key, String formValue, PropertyType type, Mode mode) {
        String sqlValue = toSqlString(formValue, type, mode);
        return key + "" + op + "" + sqlValue;
    }

    public String eq(String key, Integer formValue, PropertyType type) {
        return eq(key, String.valueOf(formValue), type);
    }

    public String eq(String key, String formValue, PropertyType type, Mode mode) {
        return op(":", key, formValue, type, mode);
    }

    public String eq(String key, String formValue, PropertyType type)
    {
        return op(":", key, formValue, type);
    }

    public String eqLike(String key, String formValue, PropertyType type)
    {
        return eqLike(key, formValue, type, true);
    }

    public String eqLike(String key, String formValue, PropertyType type, boolean startString)
    {
        String result = op(":", key, formValue, type).replaceFirst("\\)$", "*)");
        
        if(!startString){
        	result = result.replaceFirst("\\(", "(*");
        }
        
        return result;
    }
    
    public String neq(String key, String formValue, PropertyType type)
    {
        return op("<>", key, formValue, type);
    }


    // dates
    public String gte(String key, Date formValue, PropertyType type) {
        return wrapInBracket(escape(key)+":["+toDateString(formValue)+" TO *]", mode);
    }

    public String gt(String key, Date formValue, PropertyType type) {
        return wrapInBracket(escape(key)+":["+toDateString(formValue)+" TO *]", mode);
    }

    public String lte(String key, Date formValue, PropertyType type) {
        return wrapInBracket(escape(key)+":[* TO "+toDateString(formValue)+"]", mode);
    }

    public String lt(String key, Date formValue, PropertyType type) {
        return wrapInBracket(escape(key)+":[* TO "+toDateString(formValue)+"]", mode);
    }

    public String gte(String key, String formValue, PropertyType type)
    {
        return op(":", key, rangeFrom(formValue, type, mode), type);
    }

    public String gt(String key, String formValue, PropertyType type)
    {
        return op(":", key, rangeFrom(formValue, type, mode), type);
    }

    public String lte(String key, String formValue, PropertyType type)
    {
        return op(":", key, rangeTo(formValue, type, mode), type);
    }

    public String lt(String key, String formValue, PropertyType type)
    {
        return op(":", key, rangeTo(formValue, type, mode), type);
    }

    protected String rangeFrom(String from, PropertyType type, Mode mode) {
        return range(from, "*", type, mode);
    }

    protected String rangeTo(String to, PropertyType type, Mode mode) {
        return range("*", to, type, mode);
    }

    protected String range(String from, String to, PropertyType type, Mode mode) {
        String fromEscaped;
        String toEscaped;
        if(type == PropertyType.DATETIME) {
            // already escaped
            fromEscaped = from;
            toEscaped = to;
        } else {
            fromEscaped = escape(from);
            toEscaped = escape(to);
        }
        return "["+fromEscaped+" TO "+toEscaped+"]";
    }

    public String empty(String key) {
        return "-"+key+":[* TO *]";
    }

    private String toSqlString(String value, PropertyType type, Mode mode) {
        if(type == PropertyType.STRING || type == PropertyType.URI || type == PropertyType.HTML
                || type == PropertyType.ID)
        {
            return wrapInBracket(escape(value), mode);
        }
        else if(type == PropertyType.DECIMAL || type == PropertyType.INTEGER)
        {
            return escape(value);
        }
        else if(type == PropertyType.DATETIME)
        {
            return checkDate(value, type, mode);
        }
        else if(type == PropertyType.BOOLEAN)
        {
            return value.equals("true") ? "true" : "false";
        }
        else
        {
            log.error("[toSqlString] unsupported type = {}, from value = {}", type, value);
            return null;
        }
    }

    private String checkDate(String value, PropertyType type, Mode mode) {
        // verify if date can be parsed
        // to prevent LuceneInjection
        try {
            dateFormat.parse(value);
            return value;
        } catch (ParseException e) {
            log.error("[toSqlString] value = {}, type = {}, mode = {}", value, type, mode, e);
            return "cannot_parse_date";
        }
    }

    public String toDateString(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        dateFormat.setTimeZone(tz);
        return dateFormat.format(date);
    }

    private String escape(String elem) {
        return escape ? QueryParser.escape(elem) : elem;
    }
}

