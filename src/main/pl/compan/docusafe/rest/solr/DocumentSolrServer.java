package pl.compan.docusafe.rest.solr;

import org.apache.solr.client.solrj.impl.HttpSolrServer;

public class DocumentSolrServer extends HttpSolrServer {
    public DocumentSolrServer(String baseURL) {
        super(baseURL);
    }
}
