package pl.compan.docusafe.rest;


import com.google.common.base.Optional;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.document.DocumentSmartFinder;
import pl.compan.docusafe.core.office.container.ContainerSmartFinder;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.rest.handlers.ResponseSupplier;
import pl.compan.docusafe.rest.views.DocumentView;
import pl.compan.docusafe.rest.views.RichDocumentView;
import pl.compan.docusafe.spring.SmartSearchDocumentResponse;
import pl.compan.docusafe.spring.SmartSearchService;
import pl.compan.docusafe.util.Base64;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(produces = "application/json")
public class RestSmartSearchService {

    private final static Logger log = LoggerFactory.getLogger(RestSmartSearchService.class);

    @Autowired
    SmartSearchService smartSearch;

    @Autowired
    ResponseHandler responseHandler;

    private String decodeBase64(String base64) throws UnsupportedEncodingException {
        return new String(Base64.decode(base64.getBytes()), "UTF-8");
    }

    @RequestMapping(value="/smartsearch/document/barcode={barcodeBase64}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<SmartSearchDocumentResponse> searchDocumentByBarcode(@PathVariable final String barcodeBase64, HttpServletRequest request) throws UnsupportedEncodingException {

        final String barcode = decodeBase64(barcodeBase64);
        Optional<SmartSearchDocumentResponse> searchResponse = new DSTransaction().executeOpenContext(request, new DSTransaction.Function<SmartSearchDocumentResponse>() {
            @Override
            public SmartSearchDocumentResponse apply() throws Exception {
                try {
                    return smartSearch.searchSingleDocument(barcode);
                } catch (EdmException e) {
                    throw new InternalServerError(e);
                }
            }
        });

        if(searchResponse.isPresent()) {
            return new ResponseEntity<SmartSearchDocumentResponse>(searchResponse.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<SmartSearchDocumentResponse>(HttpStatus.NOT_FOUND);
        }

    }

    @RequestMapping(value="/smartsearch/document/barcodePrefix={barcodePrefixBase64}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<RichDocumentView>> searchDocumentsByBarcodePrefix(@PathVariable final String barcodePrefixBase64, HttpServletRequest request) throws UnsupportedEncodingException {
        final String barcodePrefix = decodeBase64(barcodePrefixBase64);
        Optional<List<RichDocumentView>> searchResponse = new DSTransaction().executeOpenContext(request, new DSTransaction.Function<List<RichDocumentView>>() {
            @Override
            public List<RichDocumentView> apply() throws Exception {
                try {
                    return smartSearch.findByBarcodePrefix(barcodePrefix);
                } catch (EdmException e) {
                    throw new InternalServerError(e);
                }
            }
        });

        return responseHandler.handle(searchResponse);
    }

    @RequestMapping(value="/smartsearch/document", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<RichDocumentView>> searchDocument(@RequestParam(value = "base64Query", required = false) final String base64Query,
                                                              @RequestParam(defaultValue = "20") final Integer limit,
                                                              @RequestParam(defaultValue = "0") final Integer offset,
                                                              HttpServletRequest request) throws SolrServerException, UnsupportedEncodingException {

        return responseHandler.handle(new ResponseSupplier<List<RichDocumentView>>() {
            @Override
            public ResponseEntity<List<RichDocumentView>> get() throws Throwable {
                try {
                    String query = decodeBase64(base64Query);

                    List<RichDocumentView> resp = DocumentSmartFinder.instance().find(query, limit, offset);

                    return new ResponseEntity<List<RichDocumentView>>(resp, HttpStatus.OK);
                } catch(SolrServerException ex) {
                    log.error("[searchDocument] solr error", ex);
                    return new ResponseEntity("Błąd połączenia z wyszukiwarką pełnotekstową", HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        });
    }

    @RequestMapping(value="/smartsearch/container", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<Map<String, Object>>> searchContainer(@RequestParam(value = "base64Query", required = false) final String base64Query,
                                                              @RequestParam(defaultValue = "20") final Integer limit,
                                                              @RequestParam(defaultValue = "0") final Integer offset,
                                                              HttpServletRequest request) throws SolrServerException, UnsupportedEncodingException {

        return responseHandler.handle(new ResponseSupplier<List<Map<String, Object>>>() {
            @Override
            public ResponseEntity<List<Map<String, Object>>> get() throws Throwable {
                try {
                    String query = decodeBase64(base64Query);

                    List<Map<String, Object>> resp = ContainerSmartFinder.instance().find(query, limit, offset);

                    return new ResponseEntity<List<Map<String, Object>>>(resp, HttpStatus.OK);
                } catch(SolrServerException ex) {
                    log.error("[searchContainer] solr error", ex);
                    return new ResponseEntity("Błąd połączenia z wyszukiwarką pełnotekstową", HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        });

    }


}