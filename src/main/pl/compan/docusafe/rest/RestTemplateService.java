package pl.compan.docusafe.rest;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.TemplateDto;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.repository.RepositoryProvider;
import pl.compan.docusafe.core.repository.SimpleFileSystemRepository;
import pl.compan.docusafe.core.templating.DocumentSnapshot;
import pl.compan.docusafe.rest.helpers.SpringUploadHelper;
import pl.compan.docusafe.spring.user.office.TemplateServiceImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.annotation.Nullable;
import javax.mail.Multipart;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.concurrent.Callable;

@Controller
@RequestMapping(produces = {"application/json","application/xml"})
public class RestTemplateService implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(RestTemplateService.class);

    @Autowired
    RestRequestHandler requestHandler;

    @Autowired
    SpringUploadHelper springUploadHelper;

    @RequestMapping(value = "/template", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.GET)
    public @ResponseBody String getTemplates(@RequestBody OfficeCaseDto template) {
        JsonObject obj = new JsonObject();
        obj.addProperty("name", template.getTitle());
        obj.addProperty("age", template.getDescription());
        return obj.toString();
    }

    @RequestMapping(value = "/template/dockind={dockind}", method = RequestMethod.GET)
    @ResponseBody public ListDtos<TemplateDto> getAvailableTemplateByDocKindCn(@PathVariable final String dockind) throws Exception {
        List<TemplateDto> list = requestHandler.openContext(new Callable<List<TemplateDto>>() {
            @Override
            public List<TemplateDto> call() throws EdmException {
                return getTemplateService().getListOfAvailableTemplates(dockind);
            }
        });
		return new ListDtos<TemplateDto>(list);
    }

    @RequestMapping(value = "/template/documentId={documentId}", method = RequestMethod.GET)
    public @ResponseBody ListDtos<TemplateDto> getAvailableTemplate(@PathVariable  final long documentId) throws Exception {
        List<TemplateDto> list = requestHandler.openContext(new Callable<List<TemplateDto>>() {
            @Override
            public List<TemplateDto> call() throws EdmException {
                Document doc = Document.find(documentId);
                String dockind = doc.getDocumentKind().getCn();
                return getTemplateService().getListOfAvailableTemplates(dockind);
            }
        });
		return new ListDtos<TemplateDto>(list);
    }

    @RequestMapping(value = "/template/dockind={dockind}/{rtf:.+}", method = RequestMethod.GET, produces = {"application/rtf"})
    public @ResponseBody void getClearRtf(@PathVariable final String dockind, @PathVariable final String rtf, final HttpServletResponse response) throws Exception {
            requestHandler.openContext(new Callable<ResponseEntity>() {
            @Override
            public ResponseEntity call() throws Exception {
                List<TemplateDto> listOfAvailableTemplates = getTemplateService().getListOfAvailableTemplates(dockind);
                boolean isAvailableForUser = FluentIterable.from(listOfAvailableTemplates)
                        .anyMatch(new Predicate<TemplateDto>() {
                                      @Override
                                      public boolean apply(@Nullable TemplateDto templateDto) {
                                          return templateDto.getTemplateName().equals(rtf) ? true : false;
                                      }
                                  }
                        );
                boolean isUserTemplate = FluentIterable.from(listOfAvailableTemplates)
                        .anyMatch(new Predicate<TemplateDto>() {
                                      @Override
                                      public boolean apply(@Nullable TemplateDto templateDto) {
                                          return templateDto.getTemplateName().equals(rtf) && templateDto.getIsUserTemplate() == true ? true : false;
                                      }
                                  }
                        );
                if(!isAvailableForUser) {
                	response.setStatus(HttpStatus.NOT_FOUND.value());
                }
                SimpleFileSystemRepository repo;
                if(isUserTemplate) {
                    String username = DSApi.context().getDSUser().getName();
                    repo = RepositoryProvider.getUserTemplateRepository(username, dockind);
                } else {
                    repo = RepositoryProvider.getGlobalTemplateRepository(dockind);
                }
                final InputStream inputStream = repo.get(rtf);
                IOUtils.copy(inputStream, response.getOutputStream());
                response.flushBuffer();
                response.setStatus(HttpStatus.OK.value());
				return null;
            }
        });
        return;
    }


    @RequestMapping(value = "/template/documentId={documentId}/{rtf:.+}", method = RequestMethod.GET, produces = {"application/rtf"})
    public @ResponseBody void getFilledRtf(@PathVariable  final String rtf, final HttpServletResponse response, @PathVariable final long documentId) throws Exception {
            requestHandler.openContext(new Callable<ResponseEntity>() {
            @Override
            public ResponseEntity call() throws Exception {
                Document doc = Document.find(documentId);
                String documentKindCn = doc.getDocumentKind().getCn();
                List<TemplateDto> listOfAvailableTemplates = getTemplateService().getListOfAvailableTemplates(documentKindCn);
                boolean isAvailableForUser = FluentIterable.from(listOfAvailableTemplates)
                        .anyMatch(new Predicate<TemplateDto>() {
                                      @Override
                                      public boolean apply(@Nullable TemplateDto templateDto) {
                                          return templateDto.getTemplateName().equals(rtf) ? true : false;
                                      }
                                  }
                        );
                boolean isUserTemplate = FluentIterable.from(listOfAvailableTemplates)
                        .anyMatch(new Predicate<TemplateDto>() {
                                      @Override
                                      public boolean apply(@Nullable TemplateDto templateDto) {
                                          return templateDto.getTemplateName().equals(rtf) && templateDto.getIsUserTemplate() == true ? true : false;
                                      }
                                  }
                        );
                if(!isAvailableForUser) {
                	response.setStatus(HttpStatus.NOT_FOUND.value());
//                    return new ResponseEntity(HttpStatus.NOT_FOUND);
                }
                DocumentSnapshot documentSnapshot = getTemplateService().createSnapshot(documentId, null);
                final SimpleFileSystemRepository documentRepository = RepositoryProvider.getDocumentRepository(documentId, documentSnapshot.getId());
                JsonElement jsonElement = getTemplateService().createFile(documentSnapshot, documentId, rtf, documentKindCn, isUserTemplate);
                JsonObject jsonObject = (JsonObject) jsonElement;
                String documentName = jsonObject.get("name").getAsString();
                final InputStream inputStream = documentRepository.get(documentName);
                IOUtils.copy(inputStream, response.getOutputStream());
                response.setContentType("application/rtf");
                response.flushBuffer();
                response.setStatus(HttpStatus.OK.value());
//                return new ResponseEntity(HttpStatus.OK);
                return null;
            }

            ;
        });
        return;
    }

    @RequestMapping(value = "/template/dockind={dockind}/{templateName:.+}", method = RequestMethod.POST, produces = {"text/plain"})
    public ResponseEntity<String> addTemplate(@PathVariable final String dockind, @PathVariable final String templateName, @RequestParam("file") MultipartFile multipartFile, final HttpServletResponse response) throws Exception {
        return springUploadHelper.uploadAction(multipartFile, new Function<File, ResponseEntity<String>>() {
            @Override
            public ResponseEntity<String> apply(File file) {
                try {
                    File newFile = new File(file.getParent(), templateName);
                    newFile.delete();
                    boolean b = file.renameTo(newFile);
                    newFile.deleteOnExit();
                    if(!b) {
                        return new ResponseEntity<String>("Cannot rename file to "+templateName, HttpStatus.INTERNAL_SERVER_ERROR);
                    } else {
                        getTemplateService().addGlobalTemplate(newFile, dockind);
                        return new ResponseEntity<String>("OK", HttpStatus.OK);
                    }
                } catch (Exception ex) {
                    log.error("[addTemplate] error", ex);
                    return new ResponseEntity<String>("Error: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        });
    }

//    @RequestMapping(value = "/template2/dockind={dockind}/{templateName:.+}", method = RequestMethod.POST, produces = {"text/plain"})
//    public String addTemplate2(@PathVariable final String dockind, @PathVariable final String templateName, final HttpServletRequest request) throws Exception {
//        ServletInputStream is = null;
//        FileOutputStream fos = null;
//        try {
//            is = request.getInputStream();
//
//            File tmp = File.createTempFile("template_upload", null);
//            fos = new FileOutputStream(tmp);
//            IOUtils.copy(is, fos);
//
//            getTemplateService().addGlobalTemplate(tmp, templateName);
//            return "OK";
//        } finally {
//            IOUtils.closeQuietly(is);
//            IOUtils.closeQuietly(fos);
//        }
//    }

    @RequestMapping(value = "/template/dockind={dockind}/{templateName:.+}", method = RequestMethod.DELETE, produces = {"text/plain"})
    public ResponseEntity<String> deleteTemplate(@PathVariable final String dockind, @PathVariable final String templateName, final HttpServletResponse response) throws Exception {
        getTemplateService().deleteGlobalTemplate(dockind, templateName);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    private TemplateServiceImpl getTemplateService() {
        return Docusafe.getBean(TemplateServiceImpl.class);
    }
}