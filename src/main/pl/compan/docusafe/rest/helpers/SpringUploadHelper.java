package pl.compan.docusafe.rest.helpers;

import com.google.common.base.Function;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.compan.docusafe.rest.InternalServerError;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class SpringUploadHelper {

    private static final Logger log = LoggerFactory.getLogger(SpringUploadHelper.class);

    public <T> T uploadAction(MultipartFile multipartFile, Function<File, T> callback) {
        if (!multipartFile.isEmpty()) {
            try {
                File sourceFile = multipartToFile(multipartFile);
                return callback.apply(sourceFile);
            } catch (Exception e) {
                log.error("UPLOAD_ERROR", e);
                throw new InternalServerError("Upload error => " + e.getMessage());
            }
        } else {
            throw new InternalServerError("Error, not file uploaded (multipart is empty)");
        }
    }

    public File multipartToFile(MultipartFile multipartFile) throws IOException {
        byte[] bytes = multipartFile.getBytes();

        File sourceFile = File.createTempFile(multipartFile.getOriginalFilename(), null);
        sourceFile.deleteOnExit();

        BufferedOutputStream stream = null;
        try {
            stream = new BufferedOutputStream(new FileOutputStream(sourceFile));
            stream.write(bytes);
        } finally {
            if(stream != null) {
                stream.close();
            }
        }

        return sourceFile;
    }

}
