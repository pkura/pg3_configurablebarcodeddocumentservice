package pl.compan.docusafe.rest.helpers;

import com.google.common.base.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

public class MultiOptional<T> {

    private enum State {
        PRESENT,
        DENIED,
        ABSENT,
        UNKNOWN
    }

    private T reference;
    private State state = State.UNKNOWN;

    private MultiOptional(T reference) {
        this.reference = checkNotNull(reference);
        this.state = State.PRESENT;
    }

    private MultiOptional(State state) {
        this.state = state;
    }

    public static <T> MultiOptional<T> of(T obj) {
        return new MultiOptional<T>(obj);
    }

    public static <T> MultiOptional<T> of(Optional<T> optional) {
        if(optional.isPresent()) {
            return MultiOptional.of(optional.get());
        } else {
            return MultiOptional.<T>absent();
        }
    }

    public static MultiOptional ofTryUnwrap(Optional optional) {
        if(optional.isPresent()) {
            Object get = optional.get();
            if(get instanceof MultiOptional) {
                return (MultiOptional)get;
            } else {
                return MultiOptional.of(optional);
            }
        } else {
            return MultiOptional.of(optional);
        }
    }

    public static <T> MultiOptional<T> of(MultiOptional<T> multiOptional) {
        return multiOptional;
    }

    public static <T> MultiOptional<T> unwrapFromOptional(Optional<MultiOptional<T>> optional) {
        if(optional.isPresent()) {
            return optional.get();
        } else {
            return MultiOptional.<T>absent();
        }
    }

    public static <T> MultiOptional<T> absent() {
        return new MultiOptional<T>(State.ABSENT);
    }

    public static <T> MultiOptional<T> denied() {
        return new MultiOptional<T>(State.DENIED);
    }

    public static <T, U> MultiOptional<U> transform(MultiOptional<T> from) {
        return new MultiOptional<U>(from.getState());
    }

    public boolean isPresent() {
        return state.equals(State.PRESENT);
    }

    public boolean isAbsent() {
        return state.equals(State.ABSENT);
    }

    public boolean isDenied() {
        return state.equals(State.DENIED);
    }

    public T get() {
        if(! isPresent()) {
            throw new IllegalStateException("Optional.get() cannot be called on an absent value");
        }
        return this.reference;
    }

    public String getStateString() {
        return this.state.toString();
    }

    private State getState() {
        return this.state;
    }
}
