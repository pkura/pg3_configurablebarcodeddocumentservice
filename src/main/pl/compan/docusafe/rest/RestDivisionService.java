package pl.compan.docusafe.rest;

import com.google.common.base.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionService;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.UserService;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.DSApiUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.Callable;

/**
 *  * Klasa zawiera us�ugi dost�pne poprzez interfejs REST zwi�zane z obs�ug� struktury organizacyjnej
 */
@Controller
@RequestMapping(produces = {"application/json","application/xml"} ,
                consumes= {"application/json", "application/xml"})
public class RestDivisionService implements Serializable {
        private final static Logger LOG = LoggerFactory.getLogger(RestDivisionService.class);

    @Autowired
    private DivisionService divisionService;

    @Autowired
    private UserService userService;

    @Autowired
    RestRequestHandler requestHandler;
    
    @Autowired
    ResponseHandler responseHandler;

    /**
     * Zwraca wszystkie dzia�y ze struktury organizacyjnej, kt�re nie s� usuni�te.
     *
     * @return lista dzia��w
     * @throws Exception
     */
    @RequestMapping(value="/division", method = RequestMethod.GET, params = {}, consumes="*")
    public @ResponseBody ListDtos<DivisionDto> getAll() throws Exception {
        List<DivisionDto> list = requestHandler.openContext(new Callable<List<DivisionDto>>() {
            @Override
            public List<DivisionDto> call() throws EdmException {
                return divisionService.getAllDivisions();
            }
        });
        responseHandler.setUpStatusCodeForDtos(list);
		return new ListDtos<DivisionDto>(list);
        
    }

    /**
     * Zwraca dzia� o przekazanym Id
     * @RequestMapping(value="/division/{divisionId}", method = RequestMethod.GET)
     * @param divisionId id dzia�u
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/division/{divisionId}", method = RequestMethod.GET)
    public @ResponseBody DivisionDto getById(@PathVariable final long divisionId) throws Exception {
    	DivisionDto divisionDto = null;
    	try {
	        divisionDto = requestHandler.openContext(new Callable<DivisionDto>() {
	            @Override
	            public DivisionDto call() throws EdmException {
	                Optional<DivisionDto> ret;
	                ret = divisionService.getById(divisionId);
	                if (ret.isPresent()) {
	                    return ret.get();
	                } else {
	                    throw new ResourceNotFoundException("Division '" + divisionId + "' not found");
	                }
	            }
	        });
	        responseHandler.setHeaders(HttpStatus.OK);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    	return divisionDto;
    }

    /**
     * Zwraca dzia� po przekazanym numerze guid
     * @RequestMapping(value="/division", method = RequestMethod.GET, params = "guid")
     * @param guid guid danego dzia�u
     * @return dzia�
     * @throws Exception
     */
    @RequestMapping(value="/division", method = RequestMethod.GET, params = "guid")
    public @ResponseBody DivisionDto getByGuid(@RequestParam("guid") final String guid) throws Exception {
    	DivisionDto divisionDto = null;
    	try {
			divisionDto = requestHandler.openContext(new Callable<DivisionDto>() {
	            @Override
	            public DivisionDto call() throws EdmException {
	                Optional<DivisionDto> ret;
	                ret = divisionService.getByGuid(guid);
	                if(ret.isPresent()){
	                    return ret.get();
	                } else {
	                    throw new ResourceNotFoundException("Division with guid '" + guid + "' not found");
	                }
	            }
	        });
	        responseHandler.setHeaders(HttpStatus.OK);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    	return divisionDto;
    }

    /**
     * Zwraca list� u�ytkownik�w znajduj�cych si� w danym dziale
     @RequestMapping(value="/division/{divisionId}/user", method = RequestMethod.GET)
     * @param divisionId id dzia�u
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/division/{divisionId}/user", method = RequestMethod.GET)
    public @ResponseBody ListDtos<UserDto> getUsers(@PathVariable final long divisionId) throws Exception {
    	List<UserDto> list = null;
    	try {
	        list = requestHandler.openContext(new Callable<List<UserDto>>() {
	            @Override
	            public List<UserDto> call() throws EdmException {
	                Optional<DivisionDto> division;
	                division = divisionService.getById(divisionId);
	                if (division.isPresent()) {
	                    return divisionService.getUsers(division.get());
	                } else {
	                    throw new ResourceNotFoundException("Division '" + divisionId + "' not found");
	                }
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(list);
			return new ListDtos<UserDto>(list);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new ListDtos<UserDto>(list);
    }

    /**
     * Dodaje u�ytkownika do wybranego dzia�u
     * 
     * @RequestMapping(value="/division/{divisionId}/user", method = RequestMethod.POST)
     * @param divisionId id dzia�u
     * @param user dane u�ytkownika znajduj�ce si� w RequestBody
     * @param request obiekt requestu wstrzykiwany w metod�
     * @return zwraca uri do dzia�u
     * @throws Exception
     */
    @RequestMapping(value="/division/{divisionId}/user", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.POST)
    public @ResponseBody String addUserToDivision(@PathVariable final long divisionId,
                                                  @RequestBody final UserDto user,
                                                  HttpServletRequest request) throws Exception {
        
    	try {
    		requestHandler.openTransaction(new Callable<Object>() {
	            @Override
	            public Object call() throws EdmException {
	                Optional<DivisionDto> division;
	                division = divisionService.getById(divisionId);
		                if (division.isPresent()) {
		                    divisionService.addUser(division.get(), user);
		                    return null;
		                } else {
		                    throw new ResourceNotFoundException("Division '" + divisionId + "' not found");
		                }
		            }
		        });
    		responseHandler.setHeaders(HttpStatus.CREATED);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
        return request.getRequestURI() + "/" + divisionId;
    }

    /**
     * Usuwa u�ytkownika z danego dzia�u
     *
     @RequestMapping(value="/division/{divisionId}/user/{userId}", method = RequestMethod.DELETE)
     * @param divisionId id dzia�u do usuni�cia
     * @param userId
     * @throws Exception
     */
    @RequestMapping(value="/division/{divisionId}/user/{userId}", method = RequestMethod.DELETE)
    public @ResponseBody void removeUserFromDivision(@PathVariable("divisionId") final long divisionId,
                                       @PathVariable("userId") final String userId) throws Exception {
        try {
	    	requestHandler.openTransaction(new Callable<Object>() {
	            @Override
	            public Object call() throws EdmException {
	                Optional<DivisionDto> division = divisionService.getById(divisionId);
	                Optional<UserDto> user = userService.getByIdOrNameOrExternalName(userId);
	                if (division.isPresent() && user.isPresent()) {
	                    return divisionService.removeUser(division.get(), user.get());
	                } else if (user.isPresent()) {
	                    throw new IllegalArgumentException("Division '" + divisionId + "' not found");
	                } else {
	                    throw new IllegalArgumentException("User '" + userId + "' not found");
	                }
	            }
	        });
	    	responseHandler.setHeaders(HttpStatus.ACCEPTED);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    }

    /**
     * Zwraca poddzia�y z danego dzia�u
     *
     @RequestMapping(value="/division/{divisionId}/division", method = RequestMethod.GET)
     * @param divisionId id dzia�u
     * @return lista poddzia��w danego dzia�u
     * @throws Exception
     */
    @RequestMapping(value="/division/{divisionId}/division", method = RequestMethod.GET)
    public @ResponseBody ListDtos<DivisionDto> getSubdivisions(@PathVariable final long divisionId) throws Exception {
    	List<DivisionDto> list = null;
    	try {
	        list = requestHandler.openContext(new Callable<List<DivisionDto>>() {
	            @Override
	            public List<DivisionDto> call() throws EdmException {
	                Optional<DivisionDto> division;
	                division = divisionService.getById(divisionId);
	                if (division.isPresent()) {
	                    return divisionService.getSubdivisions(division.get());
	                } else {
	                    throw new ResourceNotFoundException("Division '" + divisionId + "' not found");
	                }
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(list);
			return new ListDtos<DivisionDto>(list);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new ListDtos<DivisionDto>(list);
    }

    /**
     * Tworzy nowy dzia� i zwraca uri do niego
     *     @RequestMapping(value="/division", method = RequestMethod.POST)
     * @param divisionDto dzia� kt�y ma zostac utworzony
     * @param request wstrzykiwany obiekt request
     * @return uri wskazuj�ce na dzia�
     */
    @RequestMapping(value="/division", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.POST)
    public @ResponseBody String create(@RequestBody final DivisionDto divisionDto, HttpServletRequest request) throws Exception {
        long id = 0;
        try{
        	id = requestHandler.openTransaction(new Callable<Long>() {
		            @Override
		            public Long call() throws Exception {
		                return divisionService.create(divisionDto);
		            }
        		});
        	responseHandler.setHeaders(HttpStatus.CREATED);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
        return request.getRequestURI() + "/" + id;
    }

    /**
     * Aktualizacja danych dzia�u
     * @RequestMapping(value="/division/{divisionId}", method = RequestMethod.PUT)
     * @param divisionId id dzia�u do aktualizacji
     * @param divisionDto dane kt�re zostan� zmienione
     * @throws Exception
     */
    @RequestMapping(value="/division/{divisionId}", method = RequestMethod.PUT)
    public @ResponseBody void update(@PathVariable final long divisionId,
                       @RequestBody final DivisionDto divisionDto) throws Exception {
        if(divisionId != divisionDto.getId()){
            throw new IllegalArgumentException("id in path and in body does not match");
        }
        try {
	        requestHandler.openTransaction(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                divisionService.update(divisionDto);
	                return null;
	            }
	        });
	    	responseHandler.setHeaders(HttpStatus.ACCEPTED);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    }

    /**
     * Usuni�cie dzia�u
     * @RequestMapping(value="/division/{divisionId}", method = RequestMethod.DELETE)
     * @param divisionId id dzia�u
     * @throws Exception
     */
    @RequestMapping(value="/division/{divisionId}", method = RequestMethod.DELETE)
    public @ResponseBody void delete(@PathVariable final long divisionId) throws Exception {
    	try {
	        requestHandler.openTransaction(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                Optional<DivisionDto> division = divisionService.getById(divisionId);
	                if (!division.isPresent()) {
	                    throw new ResourceNotFoundException("Division '" + divisionId + "' not found");
	                } else {
	
	                    divisionService.delete(division.get());
	                }
	                return null;
	            }
	        });
	    	responseHandler.setHeaders(HttpStatus.ACCEPTED);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    }
}
