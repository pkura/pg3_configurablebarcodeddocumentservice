package pl.compan.docusafe.rest;

import com.google.common.base.Optional;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.UserService;
import pl.compan.docusafe.api.user.office.DocumentService;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cmis.model.CmisPrepend;
import pl.compan.docusafe.core.xes.XesLogData;
import pl.compan.docusafe.core.xes.XesXmlFactory;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Service udostępniający dane związane z logami Xes loga
 */
@Controller
@RequestMapping(produces = {"application/json", "application/xml"}, consumes= {"application/json", "application/xml"})
public class RESTXesService implements Serializable{
    private static final Logger log = LoggerFactory.getLogger(RESTXesService.class);
    public static final String COOKIE="cookie";

    @Autowired
    RestRequestHandler requestHandler;

    @Autowired
    HttpServletRequest request;

    @Autowired
    DocumentService documentService;
	
    @Autowired
    ResponseHandler responseHandler;

	@Autowired
	UserService userService;

    /**
     * Zwraca XesLog Data po dokumencie
     * @ds.Request (value="/xeslog/{documentId}/document", method = RequestMethod.GET )
     * @param documentId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/xeslog/{documentId}/document", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.GET)
    public @ResponseBody String getLogByDocumentId(@PathVariable final String documentId) throws Exception {

        String result = null;
        try{
        	result = requestHandler.openContext(new Callable<String>() {        
	            @Override
	            public String call() throws Exception {
	                Long docId = CmisPrepend.lastId(documentId);
	                return new XesXmlFactory().getXesLogBase64Document(docId, true);
	            }
	        });
        	responseHandler.setUpStatusCodeFor(result);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return result;
    }

    /**
     * Zwraca XesLog Data po użytkowniku
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/xeslog/{userId}/user", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.GET,params = {"time"})
    public @ResponseBody String getLogByUserId(@PathVariable final String userId,
                                                       @RequestParam("time") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final Date time) throws Exception {
    	String result = null;
        try{
	    	result = requestHandler.openContext(new Callable<String>() {
	            @Override
	            public String call() throws Exception {
					Optional<UserDto> user = userService.getByNameOrExternalName(userId);
					if(!user.isPresent()){
						throw new EdmException("Taki użykownik nie istnieje!");
					}
	                return new XesXmlFactory().getXesLogBase64User(user.get().getId(), time, true);
	            }
	        });
	    	responseHandler.setUpStatusCodeFor(result);
        } catch (Exception e) {
        	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return result;
    }

    /**
     * Zwraca XesLog Data po użytkowniku dla podanego zakresu
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/xeslog/{userId}/userRange", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.GET,params = {"starttime", "endtime"})
    public @ResponseBody String getLogByUserId(@PathVariable final String userId,
                                               @RequestParam("starttime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final Date starttime,
                                               @RequestParam("endtime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final Date endtime) throws Exception {
    	String result = null;
        try{
        	result = requestHandler.openContext(new Callable<String>() {
	            @Override
	            public String call() throws Exception {
					Optional<UserDto> user = userService.getByNameOrExternalName(userId);
					if(!user.isPresent()){
						throw new EdmException("Taki użykownik nie istnieje!");
					}
	                return new XesXmlFactory().getXesLogBase64User(user.get().getId(), starttime, endtime, true);
	            }
	        });
	    	responseHandler.setUpStatusCodeFor(result);
        } catch (Exception e) {
        	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return result;
    }
}
