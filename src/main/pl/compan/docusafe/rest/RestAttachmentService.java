package pl.compan.docusafe.rest;


import com.google.common.base.Optional;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.rest.exceptions.BadRequestException;
import pl.compan.docusafe.rest.exceptions.NotFoundException;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.rest.helpers.MultiOptional;
import pl.compan.docusafe.rest.views.AttachmentView;
import pl.compan.docusafe.spring.user.AttachmentService;
import pl.compan.docusafe.spring.user.AttachmentServiceInContext;
import pl.compan.docusafe.web.archive.repository.ViewAttachmentRevisionAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
@Controller
public class RestAttachmentService {

    @Autowired
    RestRequestHandler requestHandler;

    @Autowired
    ResponseHandler responseHandler;

    @Autowired
    AttachmentService attachmentService;

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value="/attachmentrevision", method = RequestMethod.GET, params = {})
    public @ResponseBody
    ResponseEntity getNewestRevisionsByDocument(@RequestParam final Long documentId, final HttpServletRequest request) throws Exception {
        return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
    }

    @RequestMapping(value="/attachment/{attachmentId}", method = RequestMethod.GET, params = {})
    public @ResponseBody
    ResponseEntity<AttachmentView> getAttachment(@PathVariable final Long attachmentId, final HttpServletRequest request) throws Exception {
        return responseHandler.handle(attachmentService.inDSContext(request).getAttachmentView(attachmentId));
    }

    @RequestMapping(value="/attachment", method = RequestMethod.GET, params = {})
    public @ResponseBody
    ResponseEntity getAttachments(@RequestParam final Long documentId, final HttpServletRequest request) throws Exception {
        return responseHandler.handle(attachmentService.inDSContext(request).getAttachmentViewsByDocumentId(documentId));
    }

    @RequestMapping(value="/attachmentrevision/{revisionId}/raw", method = RequestMethod.GET, params = {})
    public @ResponseBody
    void getRevisionAsRaw(@PathVariable final Long revisionId, @RequestParam(required = false) final String download, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
            MultiOptional<AttachmentRevision> revOpt = attachmentService.inDSContext(request).getAttachmentRevision(revisionId);

            if(revOpt.isAbsent()) {
                response.setStatus(404);
            } else if(revOpt.isDenied()) {
                response.setStatus(403);
            } else if(revOpt.isPresent()) {
                try {
                    attachmentService.inDSContext(request).putRevisionStreamInResponse(revOpt.get(), download, response);
                } catch (IOException ex) {
                    throw new InternalServerError(ex);
                }
            }
    }

    @RequestMapping(value="/attachmentrevision/{revisionId}/{targetExt}", method = RequestMethod.GET, params = {})
    public @ResponseBody
    void getConvertedRevision(@PathVariable final Long revisionId, @PathVariable final String targetExt, final HttpServletRequest request, final HttpServletResponse response) throws Exception {

        MultiOptional<AttachmentRevision> revOpt = attachmentService.inDSContext(request).getAttachmentRevision(revisionId);

        if (revOpt.isPresent()) {
            AttachmentRevision attachmentRevision = revOpt.get();
            ViewAttachmentRevisionAction.createResponse(revisionId, null, attachmentRevision.getAttachment().getId(), targetExt, request, response, null);
        } else if (revOpt.isAbsent()) {
            response.setStatus(404);
        } else if (revOpt.isDenied()) {
            response.setStatus(403);
        } else {
            throw new InternalServerError("Unknown status: "+revOpt.getStateString());
        }
    }


    @RequestMapping(value="/documentconverter/extensions", method = RequestMethod.GET, params = {})
    public @ResponseBody
    List<String> getAvailableConversionExtensions() throws Exception {
        return AdditionManager.getPropertyAsList("app.documentconverter.extensions");
    }

    /**
     * <p>
     *     Metoda zwraca pierwsz� stron� attachment revisionu o id=revisionId.
     *     Dzia�a r�wnie� dla pdf. Gdy revision nie jest obrazkiem lub pdfem
     *     to zwracany jest status code 400 Bad Request. Gdy nie znaleziono revisionu
     *     zwracany jest status code 404 Not Found.
     * </p>
     *
     *
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     * @param revisionId
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value="/attachmentrevision/{revisionId}/image", method = RequestMethod.GET, params = {})
    public @ResponseBody
    void getFirstPageFromImageAttachment(@PathVariable final Long revisionId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Optional<AttachmentRevision> revisionOpt = new DSTransaction().executeOpenContext(request, new DSTransaction.Function<AttachmentRevision>() {
            @Override
            public AttachmentRevision apply() throws Exception {
                return AttachmentRevision.find(revisionId);
            }
        });

        if(revisionOpt.isPresent()) {
            InputStream stream = null;
            try {
                AttachmentRevision rev = revisionOpt.get();

                if(isImage(rev.getMime())) {
                    stream = rev.getStreamByPage(0, AttachmentRevision.PNG, false);
                    IOUtils.copy(stream, response.getOutputStream());
                    response.setContentType(AttachmentRevision.PNG);
                    response.flushBuffer();
                } else {
                    throw new BadRequestException("Requested AttachmentRevision is not image");
                }

            } finally {
                if(stream != null) {
                    stream.close();
                }
            }
        } else {
            throw new NotFoundException("AttachmentRevision not found");
        }

    }

    private boolean isImage(String contentType) {
        if(StringUtils.isEmpty(contentType)) {
            return false;
        }

        return StringUtils.split(contentType, "/")[0].equals("image");
    }
}
