package pl.compan.docusafe.rest;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicates;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.office.*;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.rest.views.DocumentView;
import pl.compan.docusafe.rest.views.RichDocumentView;
import pl.compan.docusafe.spring.SmartSearchService;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;


/**
 * Us�ugi restowe zwi�zane z obs�ug� dokument�w
 */
@Controller
@RequestMapping(produces = {"application/json", "application/xml"})
public class RestDocumentService {

    private static final Logger log = LoggerFactory.getLogger(RestDocumentService.class);

    @Autowired
    DocumentService documentService;

    @Autowired
    RestRequestHandler requestHandler;

    @Autowired
    ResponseHandler responseHandler;
    
    @Autowired
    HttpServletRequest request;
    

    /**
     * Metoda zwraca dokument o podanym id
     * rest u�ywany w wersji mobilnej
     * 
     * @ds.Request(value = "/document/{documentId}", method = RequestMethod.GET)
     * @param <code>documentId</code> id dokumentu
     * @param <code>request</code> potrzebny do zbudowania obiektu odpowiedzi
     * @return
     */
    @RequestMapping(value = "/document/{documentId}", method = RequestMethod.GET)
    public @ResponseBody DocumentView get(@PathVariable final Long documentId, HttpServletResponse response) {
        Optional<DocumentView> document = new DSTransaction().executeOpenContext(request, new DSTransaction.Function<DocumentView>() {
            @Override
            public DocumentView apply() throws Exception {
                return new DocumentView(Document.find(documentId));
            }
        });

        ResponseEntity responseEntity = responseHandler.handle(document);
        DocumentView docView = (DocumentView) responseEntity.getBody();
        response.setStatus(responseEntity.getStatusCode().value());
		return docView;
    }

    /**
     * Zwraca list� dokument�w
     * rest dla aplikacji mobilnej
     * 
     * @ds.Request(value = "/document", method = RequestMethod.GET, params=ids)
     * @param <code>ids</code> list identifikator�w dokument�w
     * @param <code>request</code> potrzebny do zbudowania obiektu odpowiedzi
     * @return
     */
    @RequestMapping(value = "/document", method = RequestMethod.GET, params = {"ids"})
    public @ResponseBody
    ResponseEntity getDocuments(@RequestParam(value = "ids", required = true) final Long[] ids) {
        Optional<ImmutableList<DocumentView>> documents = new DSTransaction().executeOpenContext(request, new DSTransaction.Function<ImmutableList<DocumentView>>() {
            @Override
            public ImmutableList<DocumentView> apply() throws Exception {
                return FluentIterable.from(Document.findSafe(Arrays.asList(ids))).transform(new Function<Document, DocumentView>() {
                    @Override
                    public DocumentView apply(Document document) {
                        try {
                            return new DocumentView(document);
                        } catch (EdmException ex) {
                            log.error("[get] error creating DocumentView, document.id = {}", document.getId(), ex);
                            return null;
                        }
                    }
                }).filter(Predicates.notNull()).toList();
            }
        });

        return responseHandler.handleList(documents);
    }

    /**
     * Walidacja barkod�w
     *
     * @ds.Request(value = "/document/barcode={barcode}/{algorithm}/validate", method = RequestMethod.POST)
     *
     * @param <code>algorithm</code> jakie b�dzie wykorzystany do walidacji kodu kreskowego
     * @param <code>barcode</code> kod kreskowy
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/document/barcode={barcode}/{algorithm}/validate", consumes = {"application/json", "application/xml"}, method = RequestMethod.POST)
    public @ResponseBody BooleanDto validateBarcode(@PathVariable final String algorithm, @PathVariable final String barcode) throws Exception {
    	Boolean isValidate = null;
    	try {
	        isValidate = requestHandler.openContext(new Callable<Boolean>() {
		            @Override
		            public Boolean call() throws Exception {
		                return documentService.validateBarcode(algorithm, barcode);
		            }
		        });
	        responseHandler.setUpStatusCodeFor(isValidate);
	        return new BooleanDto(isValidate);
    	} catch (Exception e) {
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new BooleanDto(isValidate);
    }

    /**
     * Wyszukiwanie dokumentu po barkodzie
     * 
     * @ds.Request(value = "/document/barcode={barcode}/find", method = RequestMethod.GET)
     * @param <code>barcode</code>
     * @return zwraca <code>id</code> dokumentu
     * @throws Exception
     */
    @RequestMapping(value = "/document/barcode={barcode}/find", method = RequestMethod.GET)
    public @ResponseBody ListIds findByBarcode(@PathVariable final String barcode) throws Exception {
    	List<Long> list = null;
    	try {
	        list = requestHandler.openContext(new Callable<List<Long>>() {
	            @Override
	            public List<Long> call() throws Exception {
	                return documentService.findByBarcode(barcode);
	            }
	        });
	        responseHandler.setUpStatusCodeForIds(list);      
		} catch (Exception e) {
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	    	return new ListIds(list);
    }
    /**
     * Wyszukiwanie dokumentu po barkodzie. parametr check okre�la czy ma walidowa� barkod przekazany.
     * Domy�lnie wywo�ywane jest metoda bez sprawdzania barkodu.
     *
     * @ds.Request(value = "/document/barcode={barcode}/find/{check}/check", method = RequestMethod.GET)
     *
     *
     * @param <code>barcode</code>
     * @param <code>check</code> czy ma sprawdza� barcode, obs�uga Algorytmu PG
     * @return zwraca id dokumentu
     * @throws Exception
     */
    @RequestMapping(value = "/document/barcode={barcode}/find/{check}/check", method = RequestMethod.GET)
    public @ResponseBody ListIds findByBarcode(@PathVariable final String barcode, @PathVariable String check) throws Exception {
        if(StringUtils.isNotEmpty(check)){
            if(!documentService.validateBarcode("PG",barcode)){
            	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, "Niepoprawny barkod!");
                return new ListIds();
            }
        }

        List<Long> longs = requestHandler.openContext(new Callable<List<Long>>() {
            @Override
            public List<Long> call() throws Exception {
                return documentService.findByBarcode(barcode);
            }
        });

        responseHandler.setUpStatusCodeForIds(longs);
        return new ListIds(longs);
    }


    /**
     * Wyszukiwanie dokumentu
     * 
     * @ds.Request(value = "/document/search", method = RequestMethod.POST)
     * @param <code>document</code> objekt documentu zawieraj�cy informacje o szukanym dokumencie
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/document/search", method = RequestMethod.POST)
    public @ResponseBody ListDtos<OfficeDocumentDto> search(@RequestBody final OfficeDocumentDto document) throws Exception {
    	List<OfficeDocumentDto> list = null;
    	try {
	        list = requestHandler.openContext(new Callable<List<OfficeDocumentDto>>() {
	            @Override
	            public List<OfficeDocumentDto> call() throws Exception {
	                return documentService.searchDocuments(document);
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(list);
			return new ListDtos<OfficeDocumentDto>(list);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new ListDtos<OfficeDocumentDto>(list);
    }

    /**
     * Us�uga umow�liwia wyszukiwanie fulltextsearch
     * 
     * @ds.Request (value = "/document/fulltextsearch", method = RequestMethod.POST)
     * @param <code>text</code>
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/document/fulltextsearch", method = RequestMethod.POST, consumes = {"application/json", "application/xml"})
    public @ResponseBody ListDtos<RichDocumentView> searchFullTextDocument(@RequestBody final FullTextSearchDto text) throws Exception {
        List<RichDocumentView> list = null;
        try {
        	list = requestHandler.openContext(new Callable<List<RichDocumentView>>() {  
	            @Override
	            public List<RichDocumentView> call() throws Exception {
	                return documentService.fullTextSearchDocument(text);
	            }
	        });
            responseHandler.setUpStatusCodeForDtos(list);
            return new ListDtos<RichDocumentView>(list);
        } catch (Exception ex){
        	log.error("[searchDocument] solr error", ex);
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
        return new ListDtos<RichDocumentView>(list);
    }

    /**
     * Zwraca obiekt s�ownika dokumentu
     * 
     * @ds.Request(value = "/document/dictionary", method = RequestMethod.GET, params={"documentId","dictionary","dictionaryId"})
     * @param <code>documentId</code> id dokumentu
     * @param <code>dictionary</code> nazwa s�ownika
     * @param <code>id</code> id s�ownika
     * @return
     * @throws EdmException
     */
    @RequestMapping(value = "/document/dictionary", method = RequestMethod.GET)
    public @ResponseBody ListDtos<FieldView> getDocumentDictionary(@RequestParam("documentId") final String documentId,
                                                  @RequestParam("dictionary") final String dictionary,
                                                  @RequestParam("dictionaryId") final Long id) throws Exception {

    	List<FieldView> list = null;
    	try {
	        list = requestHandler.openContext(new Callable<List<FieldView>>() {
	            @Override
	            public List<FieldView> call() throws Exception {
	                return documentService.getDocumentDictionaryObject(documentId, dictionary, id);
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(list);
			return new ListDtos<FieldView>(list);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new ListDtos<FieldView>(list);
    }

    /**
     * Wyszukiwanie w s�ownikach
     * 
     * @ds.Request(value = "/document/dictionary/search", method = RequestMethod.POST)
     * @param <code>dictionaryDtos</code> objekt zawieraj�cy informacje o szukanych s�ownikach
     * @return lista dost�pnych wpis�w s�ownika
     * @throws Exception
     */
    @RequestMapping(value = "/document/dictionary/search", method = RequestMethod.POST, consumes = {"application/json", "application/xml"})
    public @ResponseBody ListDtos<DictionaryDto> searchDictionary(@RequestBody final DictionaryDto dictionaryDtos) throws Exception {
        log.info("search {}", dictionaryDtos.toString());

        if(CollectionUtils.isEmpty(dictionaryDtos.getFieldValues())){
        	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, "Empty POST request Dictionary object!");
        	return new ListDtos<DictionaryDto>();
        }

        List<DictionaryDto> fields = null;
    	try {
	        fields = requestHandler.openContext(new Callable<List<DictionaryDto>>() {
	            @Override
	            public List<DictionaryDto> call() throws Exception {
	                return documentService.searchDictionary(dictionaryDtos);
	            }
	        });
	
	        responseHandler.setUpStatusCodeForDtos(fields);
			return new ListDtos<DictionaryDto>(fields);
		} catch (Exception e) {
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new ListDtos<DictionaryDto>(fields);
    }

    /**
     * Dodawanie do s�ownika dokumentu
     *
     * @ds.Request(value = "/document/dictionary/add", method = RequestMethod.POST)
     * @param <code>dictionaryDtos</code> objekt s�ownika dodawany do s�ownika dokumentu
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/document/dictionary/add", method = RequestMethod.POST, consumes = {"application/json", "application/xml"})
    public @ResponseBody ListDtos<DictionaryDto> add(@RequestBody final DictionaryDto dictionaryDtos) throws Exception {
        log.info("add {}", dictionaryDtos.toString());

        if(CollectionUtils.isEmpty(dictionaryDtos.getFieldValues())){
        	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, "Empty POST request Dictionary object!");
        	return new ListDtos<DictionaryDto>();
        }
            
        List<DictionaryDto> fields = null;
    	try {
	        fields = requestHandler.openContext(new Callable<List<DictionaryDto>>() {
	            @Override
	            public List<DictionaryDto> call() throws Exception {
	                return documentService.add(dictionaryDtos);
	            }
	        });

	        responseHandler.setUpStatusCodeForDtos(fields);
			return new ListDtos<DictionaryDto>(fields);
		} catch (Exception e) {
            log.error("", e);
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new ListDtos<DictionaryDto>(fields);
    }

    /**
     * Wyszukiwanie dokumentu po id i zwracanie go w formie xml
     * 
     * @ds.Request(value = "/document/{documentId}", method = RequestMethod.GET)
     * @param <code>documentId</code> id dokumentu
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/document/xml/{documentId}", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.GET)
    public @ResponseBody String getDocumentAsXml(@PathVariable final String documentId) throws Exception{
        log.info("getXml {}", documentId);

        String result = null;
        try {
	        result = requestHandler.openContext(new Callable<String>() {
	            @Override
	            public String call() throws Exception {
	                return documentService.getDocumentAsXml(documentId);
	            }
	        });
	        responseHandler.setUpStatusCodeFor(result);
	        return result;
        } catch (Exception e) {
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
        return result;
        
    }

    /**
     * Usuwanie dokumentu ze s�ownika dokumentu
     * 
     * @ds.Request(value = "/document/dictionary/delete", method = RequestMethod.POST)
     * @param <code>dictionaryDtos</code> objekt representuj�cy s�ownik
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/document/dictionary/delete", method = RequestMethod.POST, consumes = {"application/json", "application/xml"})
    public @ResponseBody BooleanDto remove(@RequestBody final DictionaryDto dictionaryDtos) throws Exception{
        log.info("remove {}", dictionaryDtos);
        boolean removed = requestHandler.openContext(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return documentService.remove(dictionaryDtos);
            }
        });

        responseHandler.setUpStatusCodeFor(removed);
        return new BooleanDto(removed);
            
    }

    /**
     * Aktualizowanie dokumentu na s�owniku
     * 
     * @ds.Request(value = "/document/dictionary/update", method = RequestMethod.POST)
     * @param <code>dictionaryDtos</code> objekt representuj�cy s�ownik
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/document/dictionary/update", method = RequestMethod.POST)
    public @ResponseBody BooleanDto update(@RequestBody final DictionaryDto dictionaryDtos) throws Exception {
        log.info("update {}", dictionaryDtos.toString());

        if(CollectionUtils.isEmpty(dictionaryDtos.getFieldValues())){
        	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, "Empty POST request Dictionary object!");
        	return new BooleanDto();
        }

        Boolean updated = requestHandler.openContext(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return documentService.update(dictionaryDtos);
            }
        });

        responseHandler.setUpStatusCodeFor(updated);
        return new BooleanDto(updated);
    }

    /**
     * Zwraca histori� dokumentu
     *
     * @ds.Request(value="/document/{documentId}/audit", method= RequestMethod.GET)
     * @param documentId
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/document/{documentId}/audit", method= RequestMethod.GET)
    public @ResponseBody ListDtos<WorkHistoryBean> getAudit(@PathVariable final String documentId) throws Exception {
    	List<WorkHistoryBean> results = null;
    	try {
	    	results = requestHandler.openContext(new Callable<List<WorkHistoryBean>>() {
	            @Override
	            public List<WorkHistoryBean> call() throws Exception {
	                return documentService.getAudit(documentId);
	            }
	        });

	        responseHandler.setUpStatusCodeForDtos(results);
			return new ListDtos<WorkHistoryBean>(results);
		} catch (Exception e) {
			responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new ListDtos<WorkHistoryBean>(results);
    }

    /**
     * Dodaje pismo do obserwowanych
     *
     * @ds.Request(value="/document/{documentId}/watch", method= RequestMethod.PUT)
     * @param documentId
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/document/{documentId}/watch", method= RequestMethod.PUT)
    public @ResponseBody void addWatch(@PathVariable final String documentId) throws Exception{
    	try{
	        requestHandler.openTransaction(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                documentService.watch(documentId);
	                return null;
	            }
	        });
	        responseHandler.setHeaders(HttpStatus.CREATED);
	    }catch(Exception e){
	        log.error("", e);
	        responseHandler.setHeaders(HttpStatus.NOT_MODIFIED, e.getMessage());
	    }
    }

    /**
     * Usuwa pismo z obserwowanych
     *
     * @ds.Request(value="/document/{documentId}/unwatch", method= RequestMethod.DELETE)
     * @param documentId
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/document/{documentId}/unwatch", method= RequestMethod.DELETE)
    public @ResponseBody void removeWatch(@PathVariable final String documentId) throws Exception{
    	try{
	        requestHandler.openTransaction(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                documentService.unwatch(documentId);
	                return  null;
	            }
	        });
	        responseHandler.setHeaders(HttpStatus.ACCEPTED);
	    }catch(Exception e){
	        log.error("", e);
	        responseHandler.setHeaders(HttpStatus.NOT_MODIFIED, e.getMessage());
	    }
    }


}
