package pl.compan.docusafe.rest;

import com.google.common.base.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionService;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.UserService;
import pl.compan.docusafe.api.user.office.JasperReportDto;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.DSApiUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.archive.repository.ViewAttachmentRevisionAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.concurrent.Callable;

/**
 *  * Klasa zawiera us�ugi dost�pne poprzez interfejs REST zwi�zane z obs�ug� konwersji dokument�w
 */
@Controller
@RequestMapping(produces = {"application/json","application/xml","text/html"} ,
        consumes= {"application/json", "application/xml"})
            public class RestConverterService implements Serializable {
            private final static Logger LOG = LoggerFactory.getLogger(RestConverterService.class);

        @Autowired
        RestRequestHandler requestHandler;
        
        @Autowired
        ResponseHandler responseHandler;

    /**
     *
     *	Metoda konwertuje za��cznik do pdf i zwraca go jako base64
     *
     * @ds.Request(value="/converter/{attachmentId}", method = RequestMethod.PUT, produces = {"application/json","application/xml"} )
     * @param <code>attachmentId</code> id za��cznika, kt�ry ma by� przekonwertowany
     * @return Informacja czy uda�o si� przekonwertowa� za��cznik
     * @throws Exception
     */
    @RequestMapping(value="/converter/{attachmentId}", method = RequestMethod.PUT)
    public @ResponseBody String convertAttachmentAsPdf(@PathVariable final Long attachmentId) throws Exception {
    	String result = null;
        try{
	        result = requestHandler.openContext(new Callable<String>() {
	            @Override
	            public String call() throws Exception {
	                return ViewAttachmentRevisionAction.convertAttachmentToPdfBase64(attachmentId);
	            }
	        });
        	responseHandler.setHeaders(HttpStatus.OK);        	
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
        return result;
    }

}



