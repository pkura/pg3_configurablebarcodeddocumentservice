package pl.compan.docusafe.rest;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.office.*;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.AssociativeDocumentStore;
import pl.compan.docusafe.core.office.AssociativeDocumentStore.AssociativeDocumentBean;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.users.auth.FormCallbackHandler;
import pl.compan.docusafe.rest.handlers.ResponseHandler;
import pl.compan.docusafe.util.DSApiUtils;
import pl.compan.docusafe.web.filter.AuthFilter;

import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Klasa zawiera us�ugi dost�pne poprzez interfejs REST zwi�zane z obs�ug� spraw
 */
@Controller
@RequestMapping(produces = {"application/json","application/xml"} ,
                consumes= {"application/json", "application/xml"})
public class RestOfficeCaseService implements Serializable{

    @Autowired
    OfficeCaseService officeCaseService;

    @Autowired
    RestRequestHandler requestHandler;
    
    @Autowired
    ResponseHandler responseHandler;

    /**
     * Us�uga zwraca wszystkie sprawy u�ytkownika
     *
     * @ds.Request(value = "/officecase/{username}/user", method = RequestMethod.GET)
     *
     * @param username id name lub externalName/personNumber
     * @return lista spraw
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/{username}/user", method = RequestMethod.GET)
    public @ResponseBody ListDtos<OfficeCaseDto> getUserOfficeCase(@PathVariable final String username) throws Exception {
    	List<OfficeCaseDto> list = null;
    	try {
	        list = requestHandler.openContext(new Callable<List<OfficeCaseDto>>() {
	            @Override
	            public List<OfficeCaseDto> call() throws Exception {
	                return officeCaseService.getUserOfficeCase(username);
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(list);
			return new ListDtos<OfficeCaseDto>(list);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new ListDtos<OfficeCaseDto>(list);
    }

    /**
     * Zwraca sprawy przydzielone do dzia�u
     *
     * @ds.Request(value = "/officecase/division", method = RequestMethod.GET, params = "guid")
     *
     * @param guid guid dzia�u
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/division", method = RequestMethod.GET, params = "guid")
    public @ResponseBody ListDtos<OfficeCaseDto> getDivisionOfficeCase(@RequestParam("guid") final String guid) throws Exception {
    	List<OfficeCaseDto> list = null;
    	try {
	    	list = requestHandler.openContext(new Callable<List<OfficeCaseDto>>() {
	            @Override
	            public List<OfficeCaseDto> call() throws EdmException {
	                return officeCaseService.getDivisionOfficeCase(guid);
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(list);
			return new ListDtos<OfficeCaseDto>(list);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new ListDtos<OfficeCaseDto>(list);
    }

    /**
     * Zwraca wszystkie sprawy dla dokumentu
     * 
     * @ds.Request(value = "/officecase/{documentId}/document", method = RequestMethod.GET)
     * @param <code>documentId</code>
     * @return
     * @throws <code>Exception</code>
     */
    @RequestMapping(value = "/officecase/{documentId}/document", method = RequestMethod.GET)
    public @ResponseBody ListDtos<AssociativeDocumentStore.AssociativeDocumentBean> getDocumentOfficeCase(@PathVariable final String documentId) throws Exception {
    	List<AssociativeDocumentBean>  list = null;
    	try {
	    	list = requestHandler.openContext(new Callable<List<AssociativeDocumentStore.AssociativeDocumentBean>>() {
	            @Override
	            public List<AssociativeDocumentStore.AssociativeDocumentBean> call() throws EdmException {
	                return officeCaseService.getDocumentOfficeCase(documentId);
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(list);
			return new ListDtos<AssociativeDocumentBean>(list);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new ListDtos<AssociativeDocumentBean>(list);
    }

    /**
     * Zwraca sprawy powi�zane z dan� spraw�
     *
     * @ds.Request(value = "/officecase/{officeCaseId}/getcases", method = RequestMethod.GET)
     * @param officeCaseId id sprawy, dla kt�rej chcemy zwr�ci� powi�zane sprawy
     * @return lista spraw
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/{officeCaseId}/getcases", method = RequestMethod.GET)
    public @ResponseBody ListDtos<OfficeCaseDto> getOfficeCaseToOfficeCase(@PathVariable final Long officeCaseId) throws Exception {
    	List<OfficeCaseDto>  list = null;
    	try {
	    	list = requestHandler.openContext(new Callable<List<OfficeCaseDto>>() {
	            @Override
	            public List<OfficeCaseDto> call() throws EdmException {
	                return officeCaseService.getOfficeCaseToOfficeCase(officeCaseId);
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(list);
			return new ListDtos<OfficeCaseDto>(list);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return new ListDtos<OfficeCaseDto>(list);
    }

    /**
     * Tworzy spraw�
     * 
     * @ds.Request(value = "/officecase/create", method = RequestMethod.POST)
     * @param <code>officeCaseDto</code> obiekt sprawy kt�ry zostanie utworzony
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/create", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.POST)
    public @ResponseBody String create(@RequestBody final OfficeCaseDto officeCaseDto) throws Exception {
        Long id = new Long(0);
        try{
        	id = requestHandler.inRequestContext(new Callable<Long>() {
	            @Override
	            public Long call() {
	                boolean ok = officeCaseService.createOfficeCase(officeCaseDto);
	                if (!ok)
	                    throw new IllegalArgumentException("Nie uda�o si� utworzy� sprawy!");
	                return officeCaseDto.getId();
	            }
	        });
        	responseHandler.setHeaders(HttpStatus.CREATED);
        } catch (Exception e) {
        	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
        return "/officecase/" + id + "/case";
    }

    /**
     * Dodaje dokument do sprawy.
     *
     * @ds.Request(value = "/officecase/{documentId}/document/{officeCaseId}/case/add", method = RequestMethod.PUT)
     * @param <code>documentId</code> id dokumentu
     * @param <code>officeCaseId</code> id sprawy
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/{documentId}/document/{officeCaseId}/case/add", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.PUT)
    public @ResponseBody String addDocumentToOfficeCase(@PathVariable final String documentId, @PathVariable final Long officeCaseId) throws Exception {
        String result = null;
        try {
        	result = requestHandler.openTransaction(new Callable<String>() {
	            @Override
	            public String call() throws EdmException {
	                    return officeCaseService.addDocumentToOfficeCase(documentId, officeCaseId, true);
	
	            }
        	});
        	responseHandler.setHeaders(HttpStatus.CREATED);
        } catch (Exception e) {
        	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return result;
        
    }

    /**
     * Usuwa dokument ze sprawy
     *
     * @ds.Request(value = "/officecase/{documentId}/document/{officeCaseId}/case/remove", method = RequestMethod.DELETE)
     * @param <code>documentId</code> id dokumentu
     * @param <code>officeCaseId</code> id sprawy
     * @return zwraca informacj� o usunieciu
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/{documentId}/document/{officeCaseId}/case/remove", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.DELETE)
    public @ResponseBody String removeDocumentFromCase(@PathVariable final String documentId, @PathVariable final Long officeCaseId) throws Exception {
        try{
            String result = requestHandler.openTransaction(new Callable<String>() {
                @Override
                public String call() throws EdmException {
                    return officeCaseService.removeDocumentFromOfficeCase(documentId, officeCaseId);
                }
            });

            responseHandler.setUpStatusCodeFor(result);
            return result;         
        }catch(Exception e){
        	responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
        }
		return new String();
    }

    /**
     * Zwraca obiekt sprawy o podanym identyfikatorze
     *
     * @ds.Request(value = "/officecase/{officeCaseId}/case", method = RequestMethod.GET)
     * @param <code>officeCaseId</code> identyfiaktor sprawy
     * @return <code>officeCase</code>
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/{officeCaseId}/case", method = RequestMethod.GET)
    public @ResponseBody OfficeCaseDto getOfficeCase(@PathVariable final String officeCaseId) throws Exception {
    	OfficeCaseDto officeCaseDto;
    	try{
	    	officeCaseDto = requestHandler.openContext(new Callable<OfficeCaseDto>() {
	            @Override
	            public OfficeCaseDto call() throws Exception {
	                return officeCaseService.getOfficeCase(officeCaseId);
	            }
	        });
	    	responseHandler.setHeaders(HttpStatus.OK);
	    	return officeCaseDto;
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    	return null;
    }

    /**
     * Zwraca list� priorytet�w sprawy
     * 
     * @ds.Request(value = "/officecase/priority", method=RequestMethod.GET)
     * @return lista 
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/priority", method=RequestMethod.GET)
    public @ResponseBody ListDtos<CasePriority> getCasePriority() throws Exception {
    	List<CasePriority>  list = null;
    	try {
	        list = requestHandler.openContext(new Callable<List<CasePriority>>() {
	            @Override
	            public List<CasePriority> call() throws Exception {
	                return CasePriority.list();
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(list);
			return new ListDtos<CasePriority>(list);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return null;
    }

    /**
     * Zwraca list� folder�w sprawy.
     * 
     * @ds.Request(value = "/officecase/portfolio/{guid}", method=RequestMethod.GET)
     * @param guid identyfiaktor guid dzia�u
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/portfolio/{guid}", method=RequestMethod.GET)
    public @ResponseBody ListDtos<OfficeFolderDto> getPortfolio(@PathVariable final String guid) throws Exception {
    	List<OfficeFolderDto>  list = null;
    	try {
	        list = requestHandler.openContext(new Callable<List<OfficeFolderDto>>() {
	            @Override
	            public List<OfficeFolderDto> call() throws Exception {
	                //@TODO czy potrzebne jest dodanie filtrowania po roku tak jak jest na
	                //@see PortfolioBaseAction
	                return officeCaseService.getPortfolio(guid);
	            }
	        });
	        responseHandler.setUpStatusCodeForDtos(list);
			return new ListDtos<OfficeFolderDto>(list);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return null;
    }

    /**
     * Zwraca dokumenty jakie znajduj� si� w sprawie
     *
     * @ds.Request(value = "/officecase/{officeCaseId}/documents", method=RequestMethod.GET)
     * @param officeCaseId id sprawy
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/{officeCaseId}/documents/get", method=RequestMethod.GET)
    public @ResponseBody ListDtos<AssociativeDocumentStore.AssociativeDocumentBean> getDocuments(@PathVariable final String officeCaseId) throws Exception{
    	List<AssociativeDocumentStore.AssociativeDocumentBean>  list = null;
    	try {
	    	list = requestHandler.openContext(new Callable<List<AssociativeDocumentStore.AssociativeDocumentBean>>() {
	            @Override
	            public List<AssociativeDocumentStore.AssociativeDocumentBean> call() throws Exception {
	                return officeCaseService.getDocuments(officeCaseId);
	            }
	        });
	    	responseHandler.setUpStatusCodeForDtos(list);
			return new ListDtos<AssociativeDocumentStore.AssociativeDocumentBean>(list);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return null;
    }

    /**
     * Dodaje spraw� do sprawy
     *
     * @ds.Request(value = "/officecase/{officeCaseId}/officecase/{officeCaseAddId}/add", method=RequestMethod.PUT)
     *
     * @param officeCaseId
     * @param officeCaseAddId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/{officeCaseId}/officecase/{officeCaseAddId}/add",produces = MediaType.TEXT_HTML_VALUE, method=RequestMethod.PUT)
    public @ResponseBody String addOfficeCase(@PathVariable final String officeCaseId, @PathVariable final String officeCaseAddId) throws Exception {
    	try {
	        requestHandler.openContext(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                officeCaseService.addOfficeCaseToOfficeCase(officeCaseId, officeCaseAddId);
	                return null;
	            }
	        });
	        responseHandler.setHeaders(HttpStatus.CREATED);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
        return "/officecase/" + officeCaseId+ "/getcases";
    }

    /**
     * Usuwa spraw� ze sprawy
     *
     * @ds.Request(value = "/officecase/{officeCaseId}/officecase/{officeCaseAddId}/remove", method=RequestMethod.DELETE)
     *
     * @param officeCaseId
     * @param officeCaseAddId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/{officeCaseId}/officecase/{officeCaseRemoveId}/remove", produces = MediaType.TEXT_HTML_VALUE, method=RequestMethod.DELETE)
    public @ResponseBody String removeOfficeCase(@PathVariable final String officeCaseId, @PathVariable final String officeCaseRemoveId) throws Exception {
    	try {
	    	requestHandler.openContext(new Callable<Object>() {
	            @Override
	            public Object call() throws Exception {
	                officeCaseService.removeOfficeCaseToOfficeCase(officeCaseId, officeCaseRemoveId);
	                return null;
	            }
	        });
	    	responseHandler.setHeaders(HttpStatus.OK);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
        return "/officecase/" + officeCaseId+ "/getcases";
    }

    /**
     * Aktualizuje spraw�
     *
     * @ds.Request(value = "/officecase/update", method = RequestMethod.POST)
     * @param <code>officeCaseDto</code> obiekt sprawy kt�ry zostanie utworzony
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/update", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.POST)
    public @ResponseBody String update(@RequestBody final OfficeCaseDto officeCaseDto) throws Exception {
        Long id = null;
        try {
	        id = requestHandler.openTransaction(new Callable<Long>() {
	            @Override
	            public Long call() throws EdmException {
	               officeCaseService.updateOfficeCase(officeCaseDto);
	               return officeCaseDto.getId();
	            }
	        });
	        responseHandler.setHeaders(HttpStatus.CREATED);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
        return "/officecase/" + id + "/case";
    }

    /**
     * Usuwa spraw�
     *
     * @ds.Request(value = "/officecase/remove", method = RequestMethod.DELETE)
     * @param <code>officeCaseDto</code> obiekt sprawy kt�ry zostanie usuniety
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/remove", method = RequestMethod.POST)
    public @ResponseBody void delete(@RequestBody final KeyElement officeCaseDto) throws Exception {
        try {   
        	requestHandler.openTransaction(new Callable<Object>() {
                @Override
                public Object call() throws EdmException {
                        officeCaseService.deleteOfficeCase(officeCaseDto.getId(), officeCaseDto.getValue());
                    return null;
                }
            });
	        responseHandler.setHeaders(HttpStatus.ACCEPTED);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
    }

    /**
     * Zwraca histori� sprawy
     *
     * @ds.Request(value = "/officecase/{id}/audit", method = RequestMethod.GET)
     * @param <code>id</code> id sprawy pobrania historii sprawy
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/officecase/{id}/audit", method = RequestMethod.GET)
    public @ResponseBody ListDtos<WorkHistoryBean> getAudit(@PathVariable final String id) throws Exception {
    	List<WorkHistoryBean> results = null;
	    	try {
	        results = requestHandler.openContext(new Callable<List<WorkHistoryBean>>() {
	            @Override
	            public List<WorkHistoryBean> call() throws Exception {
	                return officeCaseService.getCaseAudit(id);
	            }
	        });
	    	responseHandler.setUpStatusCodeForDtos(results);
			return new ListDtos<WorkHistoryBean>(results);
    	} catch (Exception e) {
    		responseHandler.setHeaders(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return null;
    }

}
