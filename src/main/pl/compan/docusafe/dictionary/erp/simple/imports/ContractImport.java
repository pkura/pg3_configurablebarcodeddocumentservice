package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.Contract;

/**
 * Created by Damian on 17.04.14.
 */
public class ContractImport extends DictionaryImport<Contract> {

    @Override
    protected Contract getDictionary() {
        return new Contract();
    }

    @Override
    protected String getWSObjectName() {
        return "Contract";
    }
}
