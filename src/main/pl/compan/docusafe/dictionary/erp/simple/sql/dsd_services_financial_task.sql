CREATE TABLE [dbo].[dsd_services_financial_task](
	[id] [numeric](18, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idn] [varchar](50) NULL,
	[idm] [varchar](50) NULL,
	[sourceName] [varchar](128) NULL,
)
