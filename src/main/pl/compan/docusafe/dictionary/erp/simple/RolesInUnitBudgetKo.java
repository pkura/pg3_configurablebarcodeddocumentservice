package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.RolesInUnitBudgetKo")
@Table(name = RolesInUnitBudgetKo.TABLE_NAME)
public class RolesInUnitBudgetKo extends Dictionary<RolesInUnitBudgetKo> {

    private static Logger log = LoggerFactory.getLogger(RolesInUnitBudgetKo.class);

    public static final String TABLE_NAME = "dsd_services_roles_in_unit_budgetKo";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId", precision = 18, scale = 0)
    private double erpId;
    @Column(name = "bdbudzetkoidm", length = 200)
    private String bdbudzetkoidm;
    @Column(name = "bdBudzetKoId", precision = 18, scale = 0)
    private double bdBudzetKoId;
    @Column(name = "bdrolaidn", length = 200)
    private String bdrolaidn;
    @Column(name = "budzetid", precision = 18, scale = 0)
    private double budzetid;

    @Column(name = "nazwa", length = 200)
    private String nazwa;

    @Column(name = "osobaguid", length = 200)
    private String osobaguid;
    @Column(name = "osobaid", precision = 18, scale = 0)
    private double osobaid;
    @Column(name = "pracownikid", precision = 18, scale = 0)
    private double pracownikid;
    @Column(name = "pracowniknrewid")
    private int pracowniknrewid;

    public RolesInUnitBudgetKo() {
        super.setDictionaryName("Roles In Unit BudgetKo");
    }

    @Override
    public String getErpIdField() {
        return "erpId";
    }

    @Override
    public String getWsIdField() {
        return "bd_budzet_ko_id";
    }

    @Override
    public RolesInUnitBudgetKo create() {
        return new RolesInUnitBudgetKo();
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId", "bd_rola_id");
        fieldsMap.put("bdbudzetkoidm", "bd_budzet_ko_idm");
        fieldsMap.put("bdBudzetKoId", "bd_budzet_ko_id");
        fieldsMap.put("bdrolaidn", "bd_rola_idn");
        fieldsMap.put("budzetid", "budzet_id");
        fieldsMap.put("nazwa", "nazwa");
        fieldsMap.put("osobaguid", "osoba_guid");
        fieldsMap.put("osobaid", "osoba_id");
        fieldsMap.put("pracownikid", "pracownik_id");
        fieldsMap.put("pracowniknrewid", "pracownik_nrewid");

        return fieldsMap;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    public int getPracowniknrewid() {
        return pracowniknrewid;
    }

    public void setPracowniknrewid(int pracowniknrewid) {
        this.pracowniknrewid = pracowniknrewid;
    }

    public double getPracownikid() {
        return pracownikid;
    }

    public void setPracownikid(double pracownikid) {
        this.pracownikid = pracownikid;
    }

    public double getOsobaid() {
        return osobaid;
    }

    public void setOsobaid(double osobaid) {
        this.osobaid = osobaid;
    }

    public String getOsobaguid() {
        return osobaguid;
    }

    public void setOsobaguid(String osobaguid) {
        this.osobaguid = osobaguid;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public double getBudzetid() {
        return budzetid;
    }

    public void setBudzetid(double budzetid) {
        this.budzetid = budzetid;
    }

    public String getBdrolaidn() {
        return bdrolaidn;
    }

    public void setBdrolaidn(String bdrolaidn) {
        this.bdrolaidn = bdrolaidn;
    }

    public double getBdBudzetKoId() {
        return bdBudzetKoId;
    }

    public void setBdBudzetKoId(double bdBudzetKoId) {
        this.bdBudzetKoId = bdBudzetKoId;
    }

    public String getBdbudzetkoidm() {
        return bdbudzetkoidm;
    }

    public void setBdbudzetkoidm(String bdbudzetkoidm) {
        this.bdbudzetkoidm = bdbudzetkoidm;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}



