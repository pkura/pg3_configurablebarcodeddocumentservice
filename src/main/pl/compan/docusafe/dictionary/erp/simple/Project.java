package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.Project")
@Table(name = Project.TABLE_NAME)

public class Project extends Dictionary<Project>  {

    private static Logger log = LoggerFactory.getLogger(Project.class);

    public static final String TABLE_NAME = "dsd_services_project";
    public static final String GENERATOR = TABLE_NAME + "_seq";




    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId",precision = 18, scale = 0)
    private double erpId;

    @Column(name = "bprodzajwersjiid",precision = 18, scale = 0)
    private double bprodzajwersjiid;
    @Column(name = "identyfikatornum",precision = 18, scale = 0)
    private double identyfikatornum;
    @Column(name = "idm",length = 200)
    private String idm;
    @Column(name = "nazwa",length = 200)
    private String nazwa;
    @Column(name = "pdatako")
    private Date pdatako;
    @Column(name = "pdatapo")
    private Date pdatapo;
    @Column(name = "typkontraktid",precision = 18, scale = 0)
    private double typkontraktid;
    @Column(name = "typkontraktidn",length = 200)
    private String typkontraktidn;




    public Project() {
        super.setDictionaryName("Projekt");
    }

    @Override
    public String getErpIdField() {
        return "erpId";
    }

    @Override
    public String getWsIdField() {
        return "id";
    }

    @Override
    public Project create() {
        return new Project();
    }



    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("bprodzajwersjiid","bp_rodzaj_wersji_id");
        fieldsMap.put("identyfikatornum","identyfikator_num");
        fieldsMap.put("idm","idm");
        fieldsMap.put("nazwa","nazwa");
        fieldsMap.put("pdatako","p_datako");
        fieldsMap.put("pdatapo","p_datapo");
        fieldsMap.put("typkontraktid","typ_kontrakt_id");
        fieldsMap.put("typkontraktidn","typ_kontrakt_idn");



        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTypkontraktidn() {
        return typkontraktidn;
    }

    public void setTypkontraktidn(String typkontraktidn) {
        this.typkontraktidn = typkontraktidn;
    }

    public double getTypkontraktid() {
        return typkontraktid;
    }

    public void setTypkontraktid(double typkontraktid) {
        this.typkontraktid = typkontraktid;
    }

    public Date getPdatapo() {
        return pdatapo;
    }

    public void setPdatapo(Date pdatapo) {
        this.pdatapo = pdatapo;
    }

    public Date getPdatako() {
        return pdatako;
    }

    public void setPdatako(Date pdatako) {
        this.pdatako = pdatako;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public double getIdentyfikatornum() {
        return identyfikatornum;
    }

    public void setIdentyfikatornum(double identyfikatornum) {
        this.identyfikatornum = identyfikatornum;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public double getBprodzajwersjiid() {
        return bprodzajwersjiid;
    }

    public void setBprodzajwersjiid(double bprodzajwersjiid) {
        this.bprodzajwersjiid = bprodzajwersjiid;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }


}
