/**
 * 
 */
package pl.compan.docusafe.dictionary.erp.simple.imports;

import com.google.common.collect.Sets;
import org.apache.axis2.client.Stub;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.dictionary.erp.simple.Dictionary;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;


public abstract class DictionaryImport<E extends Dictionary> extends AbstractDictionaryImport {

	protected static Logger log = LoggerFactory.getLogger(DictionaryImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryService";

    Stub stub;
    private StringBuilder message;

    protected int importPageSize;
    protected int importPageNumber;
    protected Set<Long> itemIds;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        String stubClassName = Docusafe.getAdditionProperty("dictionary.erp.simple.stub");
        if(StringUtils.isNotEmpty(stubClassName)){
            Class<Stub> stubClass = (Class<Stub>) Class.forName(stubClassName).asSubclass(Stub.class);
            Constructor<Stub> constructor = stubClass.getDeclaredConstructor(conf.getAxisConfigurationContext().getClass());
            stub = constructor.newInstance(conf.getAxisConfigurationContext());
        }else{
            throw new EdmException("Nie podano w adds.properties nazwy klasy Stub [dictionary.erp.simple.stub]");
        }
        conf.setUpHttpParameters(stub, SERVICE_PATH);
    }

    @Override
    public void initImport() {
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }


    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            message.append("rozmiar strony: ").append(importPageSize);
            message.append(", nr strony: ").append(importPageNumber);
            Object[] items = getItems(importPageSize, importPageNumber++);
            if (items != null) {
                E dictionary = getDictionary();
                message.append(" Pobrano ").append(dictionary.getDictionaryName()).append(" do przetworzenia (ilo�� element�w: ").append(items.length).append(")");
                for (Object item : items) {
                    if (item != null) {
                        List<E> found = dictionary.findByErpId(item);
                        if (!found.isEmpty()) {
                            if (found.size() > 1) {
                                message.append(" Znaleziono wi�cej ni� 1 wpis o ").append(dictionary.getErpIdField()).append(": ").append(dictionary.getIdFromItem(item));
                            }
                            for (E resource : found) {
                                resource.setAllFieldsFromServiceObject(item);
                                resource.save();
                                itemIds.add(resource.getId());
                            }
                        } else {
                            E resource = getDictionary();
                            resource.setAllFieldsFromServiceObject(item);
                            resource.save();
                            itemIds.add(resource.getId());
                        }
                    }
                }
                return false; // import niezako?czony
            }

        }
        return true;
    }

    protected abstract E getDictionary();

    protected abstract String getWSObjectName();

    protected Object[] getItems(int importPageSize, int importPageNumber) throws RemoteException {
        try{
            Class<?> getterClass = Class.forName(stub.getClass().getName() + "$Get" + getWSObjectName());

            Object params = null;

            if(getterClass != null){
                Constructor<?> constructor = getterClass.getDeclaredConstructor();
                params = constructor.newInstance();
                Method method = getterClass.getMethod("setPage",int.class);
                method.invoke(params,importPageNumber);
                method = getterClass.getMethod("setPageSize", int.class);
                method.invoke(params,importPageSize);
            }
            if(params != null){
                Method method = stub.getClass().getMethod("get"+getWSObjectName(),params.getClass());
                Object response = method.invoke(stub,params);
                method = response.getClass().getMethod("get_return");
                return (Object[]) method.invoke(response);
            }else{
                throw new RemoteException("B��d przy ustawianiu parametr�w do pobrania danych z webservice");
            }
        }catch(RemoteException e){
            throw e;
        }catch (Exception e){
            throw new RemoteException("B��d przy pobieraniu element�w z webservice",e.getCause());
        }
    }

    @Override
    public void finalizeImport() throws EdmException {
        ServicesUtils.disableDeprecatedItem(itemIds, getDictionary().getTableName(), false);
    }

    @Override
    public String getMessage() {
        return message!=null? message.toString() : null;
    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }
}

