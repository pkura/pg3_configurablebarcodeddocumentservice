/**
 * 
 */
package pl.compan.docusafe.dictionary.erp.simple;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.CostInvoiceProduct")
@Table(name = CostInvoiceProduct.TABLE_NAME)
public class CostInvoiceProduct extends Dictionary<CostInvoiceProduct> {

    private final static Logger log = LoggerFactory.getLogger(CostInvoiceProduct.class);

    public static final String TABLE_NAME = "dsd_services_product";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId", precision = 18, scale = 0)
    private double erpId;
    @Column(name = "idm", length = 50)
	private String idm;
    @Column(name = "jmIdn", length = 50)
	private String jmIdn;
    @Column(name = "klaswytwIdn", length = 50)
	private String klaswytwIdn;
    @Column(name = "name", length = 128)
	private String name;
    @Column(name = "kind")
	private int kind;
    @Column(name = "vatRateId", length = 50)
	private String vatRateId;

    public CostInvoiceProduct() {
        super.setDictionaryName("Produkty");
    }

    @Override
    public CostInvoiceProduct create() {
        return new CostInvoiceProduct();
    }

    @Override
    public String getErpIdField() {
        return "idm";
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("idm","idm");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("jmIdn","jm_idn");
        fieldsMap.put("klaswytwIdn","klaswytw_idn");
        fieldsMap.put("kind","rodztowaru");
        fieldsMap.put("vatRateId","vatstaw_ids");

        return fieldsMap;
    }

    public static CostInvoiceProduct find(Long id) throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(CostInvoiceProduct.class);
        criteria.add(Restrictions.idEq(id));

        return (CostInvoiceProduct)criteria.uniqueResult();
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getErpId() {
		return erpId;
	}

	public void setErpId(double erpId) {
		this.erpId = erpId;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getJmIdn() {
		return jmIdn;
	}

	public void setJmIdn(String jmIdn) {
		this.jmIdn = jmIdn;
	}

	public String getKlaswytwIdn() {
		return klaswytwIdn;
	}

	public void setKlaswytwIdn(String klaswytwIdn) {
		this.klaswytwIdn = klaswytwIdn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getKind() {
		return kind;
	}

	public void setKind(int kind) {
		this.kind = kind;
	}

	public String getVatRateId() {
		return vatRateId;
	}

	public void setVatRateId(String vatRateId) {
		this.vatRateId = vatRateId;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CostInvoiceProduct that = (CostInvoiceProduct) o;

        if (available != that.available) return false;
        if (Double.compare(that.erpId, erpId) != 0) return false;
        if (id != that.id) return false;
        if (kind != that.kind) return false;
        if (idm != null ? !idm.equals(that.idm) : that.idm != null) return false;
        if (jmIdn != null ? !jmIdn.equals(that.jmIdn) : that.jmIdn != null) return false;
        if (klaswytwIdn != null ? !klaswytwIdn.equals(that.klaswytwIdn) : that.klaswytwIdn != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (vatRateId != null ? !vatRateId.equals(that.vatRateId) : that.vatRateId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        temp = Double.doubleToLongBits(erpId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (idm != null ? idm.hashCode() : 0);
        result = 31 * result + (jmIdn != null ? jmIdn.hashCode() : 0);
        result = 31 * result + (klaswytwIdn != null ? klaswytwIdn.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + kind;
        result = 31 * result + (vatRateId != null ? vatRateId.hashCode() : 0);
        return result;
    }
}
