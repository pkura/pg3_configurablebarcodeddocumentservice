CREATE TABLE dsd_services_project(
	[id] [int] PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[bprodzajwersjiid] [numeric](18, 0) NULL,
	[identyfikatornum] [numeric](18, 0) NULL,
    [idm] [varchar](250) NULL,
	[name] [varchar](250) NULL,
	[pdatako] [datetime] NULL,
	[pdatapo] [datetime] NULL,
	[typkontraktid] [numeric](18, 0) NULL,
	[typkontraktidn] [varchar](250) NULL,

)



