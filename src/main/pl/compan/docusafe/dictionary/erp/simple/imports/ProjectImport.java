package pl.compan.docusafe.dictionary.erp.simple.imports;
import pl.compan.docusafe.dictionary.erp.simple.Project;
/**
 * Created by Raf on 2014-05-09.
 */
public class ProjectImport extends DictionaryImport<Project> {
    @Override
    protected Project getDictionary() {
        return new Project();
    }

    @Override
    protected String getWSObjectName() {
        return "Project";
    }
}
