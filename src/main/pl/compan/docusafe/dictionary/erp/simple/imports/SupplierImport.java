package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.Supplier;
import pl.compan.docusafe.ws.simple.dictionaries.DictionaryServiceStub;

import java.rmi.RemoteException;

/**
 * Created by Damian on 17.04.14.
 */
public class SupplierImport extends DictionaryImport<Supplier> {

    @Override
    protected Supplier getDictionary() {
        return new Supplier();
    }

    @Override
    protected String getWSObjectName() {
        return "Supplier";
    }
}
