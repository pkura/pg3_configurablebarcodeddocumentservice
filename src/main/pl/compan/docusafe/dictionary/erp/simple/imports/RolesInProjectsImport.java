package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.RolesInProjects;

/**
 * Created by Damian on 17.04.14.
 */
public class RolesInProjectsImport extends DictionaryImport<RolesInProjects> {

    @Override
    protected RolesInProjects getDictionary() {
        return new RolesInProjects();
    }

    @Override
    protected String getWSObjectName() {
        return "RolesInProjects";
    }
}
