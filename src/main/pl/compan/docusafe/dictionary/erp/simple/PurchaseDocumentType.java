package pl.compan.docusafe.dictionary.erp.simple;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.*;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.PurchaseDocumentType")
@Table(name = PurchaseDocumentType.TABLE_NAME)
public class PurchaseDocumentType extends Dictionary<PurchaseDocumentType> {

    private final static Logger log = LoggerFactory.getLogger(PurchaseDocumentType.class);

    public static final String TABLE_NAME = "dsd_services_purchase_document_type";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId", precision = 18, scale = 0)
    private Double erpId;
    @Column(name = "czyWal", precision = 18, scale = 0)
    private Double czyWal;
    @Column(name = "idm", length=50)
    private String idm;
    @Column(name = "name", length=150)
    private String name;
    @Column(name = "currencySymbol", length=10)
    private String currencySymbol;
    @Column(name = "documentTypeIds", length=50)
    private String documentTypeIds;

    public PurchaseDocumentType() {
        super.setDictionaryName("Rodzaje dokumentów zakupowych");
    }

    @Override
    public PurchaseDocumentType create() {
        return new PurchaseDocumentType();
    }

    @Override
    public String getErpIdField() {
        return "idm";
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("idm","idm");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("available","czy_aktywny");
        fieldsMap.put("czyWal","czywal");
        fieldsMap.put("currencySymbol","symbolwaluty");
        fieldsMap.put("documentTypeIds","typdok_ids");
        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public static List<Long> getDocumentTypesId() throws EdmException {
        List<Double> erpIds = null;
        List<Long> result = new ArrayList<Long>();
        try {
            Criteria criteria = DSApi.context().session().createCriteria(PurchaseDocumentType.class);
            criteria.add(Restrictions.eq("available", true));
            criteria.setProjection(Projections.property("erpId"));
            erpIds =  criteria.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
        if(erpIds != null){
            for(Double erpId : erpIds){
                result.add(erpId.longValue());
            }
        }
        return result;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public Double getErpId() {
        return erpId;
    }
    public void setErpId(Double erpId) {
        this.erpId = erpId;
    }
    public Double getCzyWal() {
        return czyWal;
    }
    public void setCzyWal(Double czyWal) {
        this.czyWal = czyWal;
    }
    public String getIdm() {
        return idm;
    }
    public void setIdm(String idm) {
        this.idm = idm;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCurrencySymbol() {
        return currencySymbol;
    }
    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
    public String getDocumentTypeIds() {
        return documentTypeIds;
    }
    public void setDocumentTypeIds(String documentTypeIds) {
        this.documentTypeIds = documentTypeIds;
    }
    public void setAvailable(boolean available) {
        this.available = available;
    }
    public boolean isAvailable() {
        return available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PurchaseDocumentType that = (PurchaseDocumentType) o;

        if (available != that.available) return false;
        if (id != that.id) return false;
        if (currencySymbol != null ? !currencySymbol.equals(that.currencySymbol) : that.currencySymbol != null)
            return false;
        if (czyWal != null ? !czyWal.equals(that.czyWal) : that.czyWal != null) return false;
        if (documentTypeIds != null ? !documentTypeIds.equals(that.documentTypeIds) : that.documentTypeIds != null)
            return false;
        if (erpId != null ? !erpId.equals(that.erpId) : that.erpId != null) return false;
        if (idm != null ? !idm.equals(that.idm) : that.idm != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        result = 31 * result + (erpId != null ? erpId.hashCode() : 0);
        result = 31 * result + (czyWal != null ? czyWal.hashCode() : 0);
        result = 31 * result + (idm != null ? idm.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (currencySymbol != null ? currencySymbol.hashCode() : 0);
        result = 31 * result + (documentTypeIds != null ? documentTypeIds.hashCode() : 0);
        return result;
    }
}
