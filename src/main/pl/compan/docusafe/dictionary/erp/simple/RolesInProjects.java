package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.RolesInProjects")
@Table(name = RolesInProjects.TABLE_NAME)

public class RolesInProjects extends Dictionary<RolesInProjects>  {

    private static Logger log = LoggerFactory.getLogger(RolesInProjects.class);

    public static final String TABLE_NAME = "dsd_services_roles_In_projects";
    public static final String GENERATOR = TABLE_NAME + "_seq";


    @Id
        @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
        @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
        @Column(name = "id", nullable = false)
        private long id;
        @Column(name = "available", nullable = false)
        private boolean available = true;
        @Column(name = "erpId",precision = 18, scale = 0)
        private double erpId;

    @Column(name = "bprolaidn",length = 200)
    private String bprolaidn;
    @Column(name = "bproalkontraktid",precision = 18, scale = 0)
    private double bproalkontraktid;
    @Column(name = "czyblokadaprac",precision = 18, scale = 0)
    private double czyblokadaprac;
    @Column(name = "czyblokadaroli",precision = 18, scale = 0)
    private double czyblokadaroli;
    @Column(name = "kontraktid",precision = 18, scale = 0)
    private double kontraktid;
    @Column(name = "kontraktidm",length = 200)
    private String kontraktidm;
    @Column(name = "osobaguid",length = 200)
    private String osobaguid;
    @Column(name = "osobaid",precision = 18, scale = 0)
    private double osobaid;
    @Column(name = "pracownikid",precision = 18, scale = 0)
    private double pracownikid;
    @Column(name = "pracownikidn",length = 200)
    private String pracownikidn;
    @Column(name = "pracowniknrewid")
    private int pracowniknrewid;



    public RolesInProjects() {
        super.setDictionaryName("Role w projekcie");
    }

    @Override
    public String getErpIdField() {
        return "erpId";
    }

    @Override
    public String getWsIdField() {
        return "bp_rola_id";
    }

    @Override
    public RolesInProjects create() {
        return new RolesInProjects();
    }



    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","bp_rola_id");
        fieldsMap.put("bprolaidn","bp_rola_idn");
        fieldsMap.put("bprolakontraktid","bp_rola_kontrakt_id");
        fieldsMap.put("czyblokadaprac","czy_blokada_prac");
        fieldsMap.put("czyblokadaroli","czy_blokada_roli");
        fieldsMap.put("kontraktid","kontrakt_id");
        fieldsMap.put("kontraktidm","kontrakt_idm");
        fieldsMap.put("osobaguid","osoba_guid");
        fieldsMap.put("osobaid","osoba_id");
        fieldsMap.put("pracownikid","pracownik_id");
        fieldsMap.put("pracownikidn","pracownik_idn");
        fieldsMap.put("pracowniknrewid","pracownik_nrewid");


        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public String getBprolaidn() {
        return bprolaidn;
    }

    public void setBprolaidn(String bprolaidn) {
        this.bprolaidn = bprolaidn;
    }

    public double getBproalkontraktid() {
        return bproalkontraktid;
    }

    public void setBproalkontraktid(double bproalkontraktid) {
        this.bproalkontraktid = bproalkontraktid;
    }

    public double getCzyblokadaprac() {
        return czyblokadaprac;
    }

    public void setCzyblokadaprac(double czyblokadaprac) {
        this.czyblokadaprac = czyblokadaprac;
    }

    public double getCzyblokadaroli() {
        return czyblokadaroli;
    }

    public void setCzyblokadaroli(double czyblokadaroli) {
        this.czyblokadaroli = czyblokadaroli;
    }

    public double getKontraktid() {
        return kontraktid;
    }

    public void setKontraktid(double kontraktid) {
        this.kontraktid = kontraktid;
    }

    public String getKontraktidm() {
        return kontraktidm;
    }

    public void setKontraktidm(String kontraktidm) {
        this.kontraktidm = kontraktidm;
    }

    public String getOsobaguid() {
        return osobaguid;
    }

    public void setOsobaguid(String osobaguid) {
        this.osobaguid = osobaguid;
    }

    public double getOsobaid() {
        return osobaid;
    }

    public void setOsobaid(double osobaid) {
        this.osobaid = osobaid;
    }

    public double getPracownikid() {
        return pracownikid;
    }

    public void setPracownikid(double pracownikid) {
        this.pracownikid = pracownikid;
    }

    public String getPracownikidn() {
        return pracownikidn;
    }

    public void setPracownikidn(String pracownikidn) {
        this.pracownikidn = pracownikidn;
    }

    public int getPracowniknrewid() {
        return pracowniknrewid;
    }

    public void setPracowniknrewid(int pracowniknrewid) {
        this.pracowniknrewid = pracowniknrewid;
    }
}
