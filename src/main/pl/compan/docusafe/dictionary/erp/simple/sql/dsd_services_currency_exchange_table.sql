CREATE TABLE dsd_services_currency_exchange_table (
	[id] [numeric](18, 0) PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[czasOgloszenia] [datetime] NULL,
	[dataDo] [datetime] NULL,
	[dataOd] [datetime] NULL,
	[tolerancja] [numeric](18, 0) NULL,
	[uwagi] [varchar](254) NULL
)

