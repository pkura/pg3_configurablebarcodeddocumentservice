CREATE TABLE [dbo].[dsd_services_unit_of_measure](
	[id] [numeric](18, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[typIdn] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[precyzjaJm] [int] NULL,
)
