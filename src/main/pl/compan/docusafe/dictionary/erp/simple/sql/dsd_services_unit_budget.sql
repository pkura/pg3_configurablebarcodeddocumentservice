CREATE TABLE [dbo].[dsd_services_unit_budget](
	[id] [numeric](18, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[wersja] [int] NULL,
	[dataObowDo] [datetime] NULL,
	[dataObowOd] [datetime] NULL,
	[czyArchiwalny] [bit] NULL,
)