package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.UnitBudgetKo")
@Table(name = UnitBudgetKo.TABLE_NAME)
public class UnitBudgetKo extends Dictionary<UnitBudgetKo>  {

    private static Logger log = LoggerFactory.getLogger(UnitBudgetKo.class);

    public static final String TABLE_NAME = "dsd_services_unitbudgetko";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId",precision = 18, scale = 0)
    private Double erpId;
    @Column(name = "idm",length = 50)
    private String idm;
    @Column(name = "name",length = 128)
    private String name;
    @Column(name = "budzetId",precision = 18, scale = 0)
    private double budzetId;
    @Column(name = "budzetIdm",length = 128)
    private String budzetIdm;
    @Column(name = "czyAgregat")
    private int czyAgregat;
    @Column(name = "wersja")
    private int wersja;


    public UnitBudgetKo() {
        super.setDictionaryName("Bud�et");
    }

    @Override
    public String getErpIdField() {
        return "idm";
    }

    @Override
    public UnitBudgetKo create() {
        return new UnitBudgetKo();
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("idm","idm");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("budzetId","budzet_id");
        fieldsMap.put("budzetIdm","budzet_idm");
        fieldsMap.put("czyAgregat","czy_agregat");
        fieldsMap.put("wersja","wersja");
        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Double getErpId() {
        return erpId;
    }

    public void setErpId(Double erpId) {
        this.erpId = erpId;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBudzetId() {
        return budzetId;
    }

    public void setBudzetId(double budzetId) {
        this.budzetId = budzetId;
    }

    public String getBudzetIdm() {
        return budzetIdm;
    }

    public void setBudzetIdm(String budzetIdm) {
        this.budzetIdm = budzetIdm;
    }

    public int getCzyAgregat() {
        return czyAgregat;
    }

    public void setCzyAgregat(int czyAgregat) {
        this.czyAgregat = czyAgregat;
    }

    public int getWersja() {
        return wersja;
    }

    public void setWersja(int wersja) {
        this.wersja = wersja;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnitBudgetKo that = (UnitBudgetKo) o;

        if (available != that.available) return false;
        if (Double.compare(that.budzetId, budzetId) != 0) return false;
        if (czyAgregat != that.czyAgregat) return false;
        if (Double.compare(that.erpId, erpId) != 0) return false;
        if (id != that.id) return false;
        if (wersja != that.wersja) return false;
        if (budzetIdm != null ? !budzetIdm.equals(that.budzetIdm) : that.budzetIdm != null) return false;
        if (idm != null ? !idm.equals(that.idm) : that.idm != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        temp = Double.doubleToLongBits(erpId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (idm != null ? idm.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        temp = Double.doubleToLongBits(budzetId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (budzetIdm != null ? budzetIdm.hashCode() : 0);
        result = 31 * result + czyAgregat;
        result = 31 * result + wersja;
        return result;
    }
}
