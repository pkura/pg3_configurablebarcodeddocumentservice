package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.FinancialTask;

/**
 * Created by Damian on 17.04.14.
 */
public class FinancialTaskImport extends DictionaryImport<FinancialTask> {


    @Override
    protected FinancialTask getDictionary() {
        return new FinancialTask();
    }

    @Override
    protected String getWSObjectName() {
        return "FinancialTask";
    }

}
