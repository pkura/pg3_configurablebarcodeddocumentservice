CREATE TABLE dsd_services_currency (
	[id] [numeric](18, 0) PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL
)