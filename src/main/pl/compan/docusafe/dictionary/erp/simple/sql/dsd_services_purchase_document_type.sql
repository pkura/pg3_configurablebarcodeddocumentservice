CREATE TABLE dsd_services_purchase_document_type (
	[id] [numeric](18, 0) PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[czyWal] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](150) NULL,
	[currencySymbol] [varchar](10) NULL,
	[documentTypeIds] [varchar](50) NULL
)