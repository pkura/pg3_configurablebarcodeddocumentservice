package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.ContractBudget")
@Table(name = ContractBudget.TABLE_NAME)
public class ContractBudget extends Dictionary<ContractBudget> {

    private final static Logger log = LoggerFactory.getLogger(ContractBudget.class);

    public static final String TABLE_NAME = "dsd_services_contract_budget";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
	private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId", precision = 18, scale = 0)
	private double erpId;
    @Column(name = "idm",length=50)
	private String idm;
    @Column(name = "kontraktId", precision = 18, scale = 0)
	private double kontraktId;
    @Column(name = "name",length=128)
	private String name;
    @Column(name = "pDatako")
	private Date pDatako;
    @Column(name = "pDatapo")
	private Date pDatapo;
    @Column(name = "stan")
	private int stan;
    @Column(name = "status")
	private int status;
	
	public ContractBudget() {
		super.setDictionaryName("Bud�ety kontrakt�w");
	}

    @Override
    public ContractBudget create() {
        return new ContractBudget();
    }

    @Override
    public String getErpIdField() {
        return "idm";
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("idm","idm");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("kontraktId","kontrakt_id");
        fieldsMap.put("pDatako","p_datako");
        fieldsMap.put("pDatapo","p_datapo");
        fieldsMap.put("stan","stan");
        fieldsMap.put("status","status");

        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getErpId() {
		return erpId;
	}

	public void setErpId(double erpId) {
		this.erpId = erpId;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public double getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(double kontraktId) {
		this.kontraktId = kontraktId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getPDatako() {
		return pDatako;
	}

	public void setPDatako(Date pDatako) {
		this.pDatako = pDatako;
	}

	public Date getPDatapo() {
		return pDatapo;
	}

	public void setPDatapo(Date pDatapo) {
		this.pDatapo = pDatapo;
	}

	public int getStan() {
		return stan;
	}

	public void setStan(int stan) {
		this.stan = stan;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContractBudget that = (ContractBudget) o;

        if (available != that.available) return false;
        if (Double.compare(that.erpId, erpId) != 0) return false;
        if (id != that.id) return false;
        if (Double.compare(that.kontraktId, kontraktId) != 0) return false;
        if (stan != that.stan) return false;
        if (status != that.status) return false;
        if (idm != null ? !idm.equals(that.idm) : that.idm != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (pDatako != null ? !pDatako.equals(that.pDatako) : that.pDatako != null) return false;
        if (pDatapo != null ? !pDatapo.equals(that.pDatapo) : that.pDatapo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        temp = Double.doubleToLongBits(erpId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (idm != null ? idm.hashCode() : 0);
        temp = Double.doubleToLongBits(kontraktId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (pDatako != null ? pDatako.hashCode() : 0);
        result = 31 * result + (pDatapo != null ? pDatapo.hashCode() : 0);
        result = 31 * result + stan;
        result = 31 * result + status;
        return result;
    }
}
