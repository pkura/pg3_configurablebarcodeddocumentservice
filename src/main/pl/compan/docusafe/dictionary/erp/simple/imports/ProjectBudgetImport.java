package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.ProjectBudget;

/**
 * Created by Damian on 17.04.14.
 */
public class ProjectBudgetImport extends DictionaryImport<ProjectBudget> {

    @Override
    protected ProjectBudget getDictionary() {
        return new ProjectBudget();
    }

    @Override
    protected String getWSObjectName() {
        return "ProjectBugdet";
    }
}
