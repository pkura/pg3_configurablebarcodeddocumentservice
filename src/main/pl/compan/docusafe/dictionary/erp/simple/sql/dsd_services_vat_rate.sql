CREATE TABLE dsd_services_vat_rate(
	[id] [int] PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[czyZwolniona] [numeric](18, 0) NULL,
	[dataObowDo] [datetime] NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[niePodlega] [numeric](18, 0) NULL,
	[procent] [numeric](18, 2) NULL
)