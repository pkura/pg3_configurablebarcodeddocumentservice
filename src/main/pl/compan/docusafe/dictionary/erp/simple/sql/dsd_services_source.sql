CREATE TABLE dsd_services_source(
	[id] [int] PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idn] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL,
)