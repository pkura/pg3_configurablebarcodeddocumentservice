package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.UnitBudgetKo;
import pl.compan.docusafe.dictionary.erp.simple.UnitBudgetKoKind;

/**
 * Created by Damian on 17.04.14.
 */
public class UnitBudgetKoKindImport extends DictionaryImport<UnitBudgetKoKind> {


    @Override
    protected UnitBudgetKoKind getDictionary() {
        return new UnitBudgetKoKind();
    }

    @Override
    protected String getWSObjectName() {
        return "UnitBudgetKoKind";
    }

}
