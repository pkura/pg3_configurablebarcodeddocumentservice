package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.Supplier")
@Table(name = Supplier.TABLE_NAME)
public class Supplier extends Dictionary<Supplier> {

    private final static Logger log = LoggerFactory.getLogger(Supplier.class);

    public static final String TABLE_NAME = "dsd_services_supplier";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId", precision = 18, scale = 0)
    private Double erpId;
    @Column(name = "idm", length = 50)
    private String idm;
    @Column(name = "idn", precision = 18, scale = 0)
    private Double idn;
    @Column(name = "klasdostId", precision = 18, scale = 0)
    private Double klasdostId;
    @Column(name = "klasdostIdn", length = 50)
    private String klasdostIdn;
    @Column(name = "name", length = 128)
    private String name;
    @Column(name = "nip", length = 50)
    private String nip;
    @Column(name = "miasto", length = 50)
    private String miasto;
    @Column(name = "kodPocztowy", length = 50)
    private String kodPocztowy;
    @Column(name = "ulica", length = 50)
    private String ulica;
    @Column(name = "nrDomu", length = 50)
    private String nrDomu;
    @Column(name = "nrMieszkania", length = 50)
    private String nrMieszkania;

    public Supplier(){
        super.setDictionaryName("Kontrahenci");
    }

    @Override
    public String getErpIdField() {
        return "idm";
    }

    @Override
    public Supplier create(){
        return new Supplier();
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("idm","idm");
        fieldsMap.put("name","dostawca_nazwa");
        fieldsMap.put("idn","identyfikator_num");
        fieldsMap.put("klasdostId","klasdost_id");
        fieldsMap.put("klasdostIdn","klasdost_idn");
        fieldsMap.put("kodPocztowy","dostawca_kodpoczt");
        fieldsMap.put("miasto","dostawca_miasto");
        fieldsMap.put("nip","dostawca_nip");
        fieldsMap.put("nrDomu","dostawca_nrdomu");
        fieldsMap.put("nrMieszkania","dostawca_nrmieszk");
        fieldsMap.put("ulica","dostawca_ulica");
        return fieldsMap;
    }


    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Double getErpId() {
        return erpId;
    }

    public void setErpId(Double erpId) {
        this.erpId = erpId;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public Double getIdn() {
        return idn;
    }

    public void setIdn(Double idn) {
        this.idn = idn;
    }

    public Double getKlasdostId() {
        return klasdostId;
    }

    public void setKlasdostId(Double klasdostId) {
        this.klasdostId = klasdostId;
    }

    public String getKlasdostIdn() {
        return klasdostIdn;
    }

    public void setKlasdostIdn(String klasdostIdn) {
        this.klasdostIdn = klasdostIdn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getMiasto() {
        return miasto;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public String getKodPocztowy() {
        return kodPocztowy;
    }

    public void setKodPocztowy(String kodPocztowy) {
        this.kodPocztowy = kodPocztowy;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getNrDomu() {
        return nrDomu;
    }

    public void setNrDomu(String nrDomu) {
        this.nrDomu = nrDomu;
    }

    public String getNrMieszkania() {
        return nrMieszkania;
    }

    public void setNrMieszkania(String nrMieszkania) {
        this.nrMieszkania = nrMieszkania;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Supplier supplier = (Supplier) o;

        if (available != supplier.available) return false;
        if (id != supplier.id) return false;
        if (erpId != null ? !erpId.equals(supplier.erpId) : supplier.erpId != null) return false;
        if (idm != null ? !idm.equals(supplier.idm) : supplier.idm != null) return false;
        if (idn != null ? !idn.equals(supplier.idn) : supplier.idn != null) return false;
        if (klasdostId != null ? !klasdostId.equals(supplier.klasdostId) : supplier.klasdostId != null) return false;
        if (klasdostIdn != null ? !klasdostIdn.equals(supplier.klasdostIdn) : supplier.klasdostIdn != null)
            return false;
        if (kodPocztowy != null ? !kodPocztowy.equals(supplier.kodPocztowy) : supplier.kodPocztowy != null)
            return false;
        if (miasto != null ? !miasto.equals(supplier.miasto) : supplier.miasto != null) return false;
        if (name != null ? !name.equals(supplier.name) : supplier.name != null) return false;
        if (nip != null ? !nip.equals(supplier.nip) : supplier.nip != null) return false;
        if (nrDomu != null ? !nrDomu.equals(supplier.nrDomu) : supplier.nrDomu != null) return false;
        if (nrMieszkania != null ? !nrMieszkania.equals(supplier.nrMieszkania) : supplier.nrMieszkania != null)
            return false;
        if (ulica != null ? !ulica.equals(supplier.ulica) : supplier.ulica != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        result = 31 * result + (erpId != null ? erpId.hashCode() : 0);
        result = 31 * result + (idm != null ? idm.hashCode() : 0);
        result = 31 * result + (idn != null ? idn.hashCode() : 0);
        result = 31 * result + (klasdostId != null ? klasdostId.hashCode() : 0);
        result = 31 * result + (klasdostIdn != null ? klasdostIdn.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (nip != null ? nip.hashCode() : 0);
        result = 31 * result + (miasto != null ? miasto.hashCode() : 0);
        result = 31 * result + (kodPocztowy != null ? kodPocztowy.hashCode() : 0);
        result = 31 * result + (ulica != null ? ulica.hashCode() : 0);
        result = 31 * result + (nrDomu != null ? nrDomu.hashCode() : 0);
        result = 31 * result + (nrMieszkania != null ? nrMieszkania.hashCode() : 0);
        return result;
    }
}