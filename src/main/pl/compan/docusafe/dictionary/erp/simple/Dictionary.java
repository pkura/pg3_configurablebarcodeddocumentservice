package pl.compan.docusafe.dictionary.erp.simple;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.ClassUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.dictionary.erp.simple.utils.ReflectionUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.el.MethodNotFoundException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

public abstract class Dictionary<T extends Dictionary> implements java.io.Serializable{

    private final static Logger log = LoggerFactory.getLogger(Dictionary.class);

    private String dictionaryName = "S�ownik";

    public abstract T create();

    public List<T> list(Integer party, Boolean available) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(this.getClass());
            Preconditions.checkNotNull(party);
            criteria.add(Restrictions.eq("party", party));
            if (available != null) {
                criteria.add(Restrictions.eq("available", available));
            }
            return criteria.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public List<T> list(Boolean available) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(this.getClass());
            if (available != null) {
                criteria.add(Restrictions.eq("available", available));
            }
            return criteria.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public T get(Serializable id) throws EdmException {
        return (T) DSApi.context().session().get(this.getClass(), id);
    }

    /**
     * Finds Dictionary Entry by database Id
     * @param id
     * @return
     * @throws EdmException
     */
    public T findById(Long id) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(this.getClass());
            criteria.add(Restrictions.idEq(id));
            return (T) criteria.setMaxResults(1).uniqueResult();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public List<T> findByErpId(Object item) throws EdmException {
        Object erpId = getIdFromItem(item);
        return (List<T>) DictionaryUtils.findByGivenFieldValue(this.getClass(), this.getErpIdField(), erpId);
    }

    public Object getIdFromItem(Object item){
        boolean logErrors = AvailabilityManager.isAvailable("dictionary.erp.simple.logError");
        Method idGetter = ReflectionUtils.getGetter(item, this.getWsIdField(),logErrors);
        Object result=null;
        try {
            result = idGetter.invoke(item);
        } catch (Exception e){
            logErrorInInvocationIfNeeded(logErrors, idGetter, e);
        }
        return result;
    }

    public String getWsIdField() {
        Map<String, String> fieldMap = getFieldsToWsMap();
        if(fieldMap != null){
            String fieldName = fieldMap.get(getErpIdField());
            if(fieldName != null){
                return fieldName;
            }
        }
        return getErpIdField();
    }

    public void setAllFieldsFromServiceObject(Object item) {
        boolean logErrors = AvailabilityManager.isAvailable("dictionary.erp.simple.logError");

        for(Map.Entry<String,String> fieldToWs : getFieldsToWsMap().entrySet()){

            Class fieldType = ReflectionUtils.getFieldType(this, fieldToWs.getKey(),logErrors);
            if (fieldType == null) {
                continue;
            }
            Method fieldSetter = ReflectionUtils.getSetter(this, fieldToWs.getKey(), fieldType,logErrors);
            if (fieldSetter == null) {
                continue;
            }
            Method wsGetter = ReflectionUtils.getGetter(item, fieldToWs.getValue(),logErrors);
            if (wsGetter == null) {
                continue;
            }

            Object arg;
            try {
                arg = wsGetter.invoke(item);
            } catch (Exception e) {
                logErrorInInvocationIfNeeded(logErrors, wsGetter, e);
                continue;
            }

            if(ClassUtils.isAssignable(wsGetter.getReturnType(),fieldType,true)){
                try {
                    fieldSetter.invoke(this,arg);
                } catch (Exception e) {
                    logErrorInInvocationIfNeeded(logErrors, fieldSetter, e);
                }
            }else if(ClassUtils.isAssignable(fieldType,Boolean.class,true) && arg != null && arg.toString().matches("[-+]?\\d+(\\.\\d+)?")){
                arg = !Double.valueOf(arg.toString()).equals(new Double(0));
                try {
                    fieldSetter.invoke(this,arg);
                } catch (Exception e) {
                    logErrorInInvocationIfNeeded(logErrors, fieldSetter, e);
                }
            }else{
               if(logErrors){
                   String logMsg = "Result of method " + wsGetter.getName() + "cannot be an argument of method " + fieldSetter.getName();
                   log.error(logMsg,new MethodNotFoundException("Cannot apply result from item to setter method"));
               }
            }
        }
    }

    private void logErrorInInvocationIfNeeded(boolean logErrors, Method method, Exception e) {
        if(logErrors){
            String logMsg = "Exception occurs during invoke method " + method.getName() + "()";
            log.error(logMsg,e);
        }
    }

    public static <T> T find(Long id, Class<T> clazz) throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(clazz);
        criteria.add(Restrictions.idEq(id));

        return (T)criteria.uniqueResult();
    }

    public static <T> List<T> find(String fieldName, Object value, Class<T> clazz) throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(clazz);
        criteria.add(Restrictions.eq(fieldName,value));

        return (List<T>)criteria.list();
    }

    public abstract String getErpIdField();

    protected abstract Map<String,String> getFieldsToWsMap();

    public void persist(T dict) throws EdmException {
        DSApi.context().session().persist(dict);
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public abstract String getTableName();

    public abstract long getId();

    public String getDictionaryName() {
        return dictionaryName;
    }

    protected void setDictionaryName(String dictionaryName) {
        this.dictionaryName = dictionaryName;
    }
}
