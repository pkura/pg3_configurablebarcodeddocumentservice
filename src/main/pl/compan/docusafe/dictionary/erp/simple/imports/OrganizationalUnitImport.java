package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.OrganizationalUnit;
import pl.compan.docusafe.ws.simple.dictionaries.DictionaryServiceStub;

import java.rmi.RemoteException;

/**
 * Created by Damian on 17.04.14.
 */
public class OrganizationalUnitImport extends DictionaryImport<OrganizationalUnit> {

     @Override
    protected OrganizationalUnit getDictionary() {
        return new OrganizationalUnit();
    }

    @Override
    protected String getWSObjectName() {
        return "OrganizationalUnit";
    }
}
