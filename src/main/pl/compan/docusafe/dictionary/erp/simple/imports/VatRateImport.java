package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.VatRate;
import pl.compan.docusafe.ws.simple.dictionaries.DictionaryServiceStub;

import java.rmi.RemoteException;

/**
 * Created by Damian on 17.04.14.
 */
public class VatRateImport extends DictionaryImport<VatRate> {

    @Override
    protected VatRate getDictionary() {
        return new VatRate();
    }

    @Override
    protected String getWSObjectName() {
        return "VatRate";
    }
}
