package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.Source;

/**
 * Created by Damian on 17.04.14.
 */
public class SourceImport extends DictionaryImport<Source> {

    @Override
    protected Source getDictionary() {
        return new Source();
    }

    @Override
    protected String getWSObjectName() {
        return "FundingSource";
    }
}
