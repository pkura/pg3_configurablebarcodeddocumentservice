package pl.compan.docusafe.dictionary.erp.simple.cache;

/**
 * Created by kk on 14.05.14.
 */
public interface DictionaryCache {

    void invalidate();
    CacheParam getCacheParam();

}
