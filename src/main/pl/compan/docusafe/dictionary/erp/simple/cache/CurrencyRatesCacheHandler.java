package pl.compan.docusafe.dictionary.erp.simple.cache;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateMidnight;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.dictionary.erp.simple.CurrencyExchangeTable;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by kk on 29.04.14.
 */
@CacheDefinition(name = "currency-rates", defaultParams ="60m")
public class CurrencyRatesCacheHandler extends CacheHandler {

    private static final Logger log = LoggerFactory.getLogger(CurrencyRatesCacheHandler.class);

    private static final CurrencyRatesCacheHandler INSTANCE = new CurrencyRatesCacheHandler();

    private CurrencyRatesCacheHandler() {
    }

    public static final CurrencyRatesCacheHandler getInstance() {
        return INSTANCE;
    }

    private final LoadingCache<CurrencyTable, Optional<DictionaryServiceStub.CurrencyRate>> CURRENCY_TABLE_RATES = CacheBuilder.newBuilder().softValues().expireAfterWrite(getCacheParam().getExpiryAfterWriteTime(), getCacheParam().getExpiryAfterWriteUnit())
            .build(CacheLoader.from(new Function<CurrencyTable, Optional<DictionaryServiceStub.CurrencyRate>>() {
                @Override
                public Optional<DictionaryServiceStub.CurrencyRate> apply(final CurrencyTable currencyTable) {
                    try {
                        return Iterables.tryFind(CURRENCY_TABLES_RATES_LIST.get(0).tables, new Predicate<DictionaryServiceStub.CurrencyRate>() {
                            @Override
                            public boolean apply(DictionaryServiceStub.CurrencyRate input) {
                                return Objects.equal(input.getTabkurs_idn(), currencyTable.tableCode) && Objects.equal(input.getWaluta_idm(), currencyTable.currencyCode);
                            }
                        });
                    } catch (ExecutionException e) {
                        log.error(e.getMessage(), e);
                    }
                    return Optional.absent();
                }
            }));


    private final LoadingCache<Integer, CurrencyTablesRates> CURRENCY_TABLES_RATES_LIST = CacheBuilder.newBuilder().softValues().expireAfterWrite(getCacheParam().getExpiryAfterWriteTime(), getCacheParam().getExpiryAfterWriteUnit())
            .build(CacheLoader.from(new Function<Integer, CurrencyTablesRates>() {
                @Override
                public CurrencyTablesRates apply(final Integer currencyTable) {
                    return new CurrencyTablesRates();
                }
            }));

    @Override
    public void invalidate() {
        CURRENCY_TABLE_RATES.invalidateAll();
        CURRENCY_TABLES.invalidateAll();
        CURRENCY_TABLES_BY_DATE.invalidateAll();
        CURRENCY_TABLES_RATES_LIST.invalidateAll();
    }

    private static class CurrencyTablesRates {
        private List<DictionaryServiceStub.CurrencyRate> tables;

        private CurrencyTablesRates() {
            try {
                AxisClientConfigurator clientConf = new AxisClientConfigurator();
                DictionaryServiceStub stub = new DictionaryServiceStub(clientConf.getAxisConfigurationContext());
                clientConf.setUpHttpParameters(stub, "/services/DictionaryService");
                DictionaryServiceStub.GetCurrencyRate params = new DictionaryServiceStub.GetCurrencyRate();
                params.setPage(1);
                params.setPageSize(10000000);
                DictionaryServiceStub.GetCurrencyRateResponse result = stub.getCurrencyRate(params);
                tables = Lists.newArrayList(result.get_return());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public Optional<DictionaryServiceStub.CurrencyRate> getCurrencyRate(String currencyCode, String tableCode) {
        try {
            return CURRENCY_TABLE_RATES.get(CurrencyTable.newCurrencyTableRate(currencyCode, tableCode));
        } catch (ExecutionException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    private static class CurrencyTable {
        private String currencyCode;
        private String tableCode;

        private String getCurrencyCode() {
            return currencyCode;
        }

        private String getTableCode() {
            return tableCode;
        }

        private static CurrencyTable newCurrencyTableRate(String currencyCode, String tableCode) {
            CurrencyTable currencyTableRate = new CurrencyTable();
            currencyTableRate.currencyCode = currencyCode;
            currencyTableRate.tableCode = tableCode;
            return currencyTableRate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CurrencyTable that = (CurrencyTable) o;

            if (currencyCode != null ? !currencyCode.equals(that.currencyCode) : that.currencyCode != null) return false;
            if (tableCode != null ? !tableCode.equals(that.tableCode) : that.tableCode != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = currencyCode != null ? currencyCode.hashCode() : 0;
            result = 31 * result + (tableCode != null ? tableCode.hashCode() : 0);
            return result;
        }

    }


    private final LoadingCache<Integer, CurrencyTables> CURRENCY_TABLES = CacheBuilder.newBuilder().softValues().expireAfterWrite(getCacheParam().getExpiryAfterWriteTime(), getCacheParam().getExpiryAfterWriteUnit()).build(new CacheLoader<Integer, CurrencyTables>() {
        @Override
        public CurrencyTables load(Integer key) throws Exception {
            return new CurrencyTables();
        }
    });

    private static class CurrencyTables {
        private List<CurrencyExchangeTable> tables;

        private CurrencyTables() throws EdmException {
            try {
                Criteria criteria = DSApi.context().session().createCriteria(CurrencyExchangeTable.class);
                criteria.addOrder(Order.desc("dataOd"));
                criteria.addOrder(Order.desc("czasOgloszenia"));
                criteria.addOrder(Order.desc("dataDo"));
                criteria.add(Restrictions.eq("available", true));

                tables = criteria.list();
            } catch (HibernateException e) {
                throw new EdmHibernateException(e);
            }
        }
    }

    private final LoadingCache<Date, List<CurrencyExchangeTable>> CURRENCY_TABLES_BY_DATE = CacheBuilder.newBuilder().softValues().expireAfterWrite(getCacheParam().getExpiryAfterWriteTime(), getCacheParam().getExpiryAfterWriteUnit()).build(new CacheLoader<Date, List<CurrencyExchangeTable>>() {
        @Override
        public List<CurrencyExchangeTable> load(Date key) throws Exception {
            return Lists.newArrayList(findCurrencyTable(key));
        }
    });

    public List<CurrencyExchangeTable> getCurrencyTables(final Date tableDate) throws ExecutionException {
        return CURRENCY_TABLES_BY_DATE.get(new DateMidnight(tableDate.getTime()).toDate());
    }

    private Iterable<CurrencyExchangeTable> findCurrencyTable(final Date from) throws ExecutionException {
        List<CurrencyExchangeTable> tables = CURRENCY_TABLES.get(0).tables;

        return Iterables.filter(tables, new Predicate<CurrencyExchangeTable>() {
            @Override
            public boolean apply(CurrencyExchangeTable input) {
                return !new DateMidnight(input.getDataOd()).isAfter(from.getTime());
            }
        });
    }

    public Optional<DictionaryServiceStub.CurrencyRate> findCurrencyRate(String currencyCode, Date currencyDate) {
        try {
            for (CurrencyExchangeTable currencyExchangeTable : getCurrencyTables(currencyDate)) {
                Optional<DictionaryServiceStub.CurrencyRate> currencyRate = getCurrencyRate(currencyCode, currencyExchangeTable.getIdm());

                return currencyRate;
            }
        } catch (ExecutionException e) {
            log.error(e.getMessage(), e);
        }

        return Optional.absent();
    }
}
