CREATE TABLE [dbo].[dsd_services_project_budget](
	[id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](19, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[projektId] [numeric](19, 0) NULL,
	[stan] [int] NULL,
	[status] [int] NULL,
	[pDatako] [datetime] NULL,
	[pDatapo] [datetime] NULL,
)