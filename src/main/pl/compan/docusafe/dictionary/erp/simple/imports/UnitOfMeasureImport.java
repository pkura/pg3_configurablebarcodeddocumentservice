package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.UnitOfMeasure;
import pl.compan.docusafe.ws.simple.dictionaries.DictionaryServiceStub;

import java.rmi.RemoteException;

/**
 * Created by Damian on 17.04.14.
 */
public class UnitOfMeasureImport extends DictionaryImport<UnitOfMeasure> {

    @Override
    protected UnitOfMeasure getDictionary() {
        return new UnitOfMeasure();
    }

    @Override
    protected String getWSObjectName() {
        return "UnitOfMeasure";
    }
}
