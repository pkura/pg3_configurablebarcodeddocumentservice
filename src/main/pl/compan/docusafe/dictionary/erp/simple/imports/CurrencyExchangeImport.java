package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.CurrencyExchangeTable;

/**
 * Created by Damian on 17.04.14.
 */
public class CurrencyExchangeImport extends DictionaryImport<CurrencyExchangeTable> {

    @Override
    protected CurrencyExchangeTable getDictionary() {
        return new CurrencyExchangeTable();
    }

    @Override
    protected String getWSObjectName() {
        return "CurrencyExchangeTable";
    }
}
