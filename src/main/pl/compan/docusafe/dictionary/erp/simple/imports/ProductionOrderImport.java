package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.ProductionOrder;
import pl.compan.docusafe.ws.simple.dictionaries.DictionaryServiceStub;

import java.rmi.RemoteException;

/**
 * Created by Damian on 17.04.14.
 */
public class ProductionOrderImport extends DictionaryImport<ProductionOrder> {

    @Override
    protected ProductionOrder getDictionary() {
        return new ProductionOrder();
    }

    @Override
    protected String getWSObjectName() {
        return "ProductionOrder";
    }
}
