package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.PurchaseDocumentType;
import pl.compan.docusafe.ws.simple.dictionaries.DictionaryServiceStub;

import java.rmi.RemoteException;

/**
 * Created by Damian on 17.04.14.
 */
public class PurchaseDocumentTypeImport extends DictionaryImport<PurchaseDocumentType> {

    @Override
    protected PurchaseDocumentType getDictionary() {
        return new PurchaseDocumentType();
    }

    @Override
    protected String getWSObjectName() {
        return "PurchaseDocumentType";
    }
}
