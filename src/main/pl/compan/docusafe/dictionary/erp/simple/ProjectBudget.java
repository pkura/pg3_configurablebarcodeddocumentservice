package pl.compan.docusafe.dictionary.erp.simple;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.ProjectBudget")
@Table(name = ProjectBudget.TABLE_NAME)
public class ProjectBudget extends Dictionary<ProjectBudget>  {

    private static Logger log = LoggerFactory.getLogger(ProjectBudget.class);

    public static final String TABLE_NAME = "dsd_services_project_budget";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId",precision = 18, scale = 0)
    private double erpId;
    @Column(name = "idm", length=50)
    private String idm;
    @Column(name = "name",length = 128)
    private String name;
    @Column(name = "projektId",precision = 18, scale = 0)
    private double projektId;
    @Column(name = "stan")
    private int stan;
    @Column(name = "status")
    private int status;
    @Column(name = "pDatako")
    private Date pDatako;
    @Column(name = "pDatapo")
    private Date pDatapo;


    public ProjectBudget() {
        super.setDictionaryName("Bud�et Projekt�w");
    }

    @Override
    public String getErpIdField() {
        return "erpId";
    }

    @Override
    public String getWsIdField() {
        return "id";
    }

    @Override
    public ProjectBudget create() {
        return new ProjectBudget();
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("idm","idm");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("projektId","projekt_id");
        fieldsMap.put("stan","stan");
        fieldsMap.put("status","status");
        fieldsMap.put("pDatako","p_datako");
        fieldsMap.put("pDatapo","p_datapo");
        return fieldsMap;
    }

    public static ProjectBudget find(Long id) throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(ProjectBudget.class);
        criteria.add(Restrictions.idEq(id));

        return (ProjectBudget)criteria.uniqueResult();
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getProjektId() {
        return projektId;
    }

    public void setProjektId(double projektId) {
        this.projektId = projektId;
    }

    public int getStan() {
        return stan;
    }

    public void setStan(int stan) {
        this.stan = stan;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getpDatako() {
        return pDatako;
    }

    public void setpDatako(Date pDatako) {
        this.pDatako = pDatako;
    }

    public Date getpDatapo() {
        return pDatapo;
    }

    public void setpDatapo(Date pDatapo) {
        this.pDatapo = pDatapo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectBudget that = (ProjectBudget) o;

        if (available != that.available) return false;
        if (Double.compare(that.erpId, erpId) != 0) return false;
        if (id != that.id) return false;
        if (Double.compare(that.projektId, projektId) != 0) return false;
        if (stan != that.stan) return false;
        if (status != that.status) return false;
        if (idm != null ? !idm.equals(that.idm) : that.idm != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (pDatako != null ? !pDatako.equals(that.pDatako) : that.pDatako != null) return false;
        if (pDatapo != null ? !pDatapo.equals(that.pDatapo) : that.pDatapo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        temp = Double.doubleToLongBits(erpId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (idm != null ? idm.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        temp = Double.doubleToLongBits(projektId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + stan;
        result = 31 * result + status;
        result = 31 * result + (pDatako != null ? pDatako.hashCode() : 0);
        result = 31 * result + (pDatapo != null ? pDatapo.hashCode() : 0);
        return result;
    }
}
