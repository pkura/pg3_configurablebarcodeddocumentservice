package pl.compan.docusafe.dictionary.erp.simple.cache;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Created by kk on 14.05.14.
 */
@CacheDefinition(name="default", defaultParams = "1m")
public abstract class CacheHandler implements DictionaryCache {

    private static Logger log = LoggerFactory.getLogger(CacheHandler.class);

    private CacheParam cacheParam;

    CacheHandler() {

        try {
            String cacheName = this.getClass().getAnnotation(CacheDefinition.class).name();
            String defaultParms = this.getClass().getAnnotation(CacheDefinition.class).defaultParams();
            String cacheParamsRaw = Docusafe.getAdditionPropertyOrDefault("cache-params." + cacheName, defaultParms);

            if (cacheParamsRaw == null) {
                cacheParamsRaw = Docusafe.getAdditionPropertyOrDefault("cache-params", "1m");
            }

            cacheParam = CacheParam.makeCacheParam(cacheParamsRaw);
        } catch (Exception e) {
            cacheParam = CacheParam.makeCacheParam("1m");
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final CacheParam getCacheParam() {
        return cacheParam;
    }
}
