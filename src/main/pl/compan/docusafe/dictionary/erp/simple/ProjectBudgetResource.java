package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.ProjectBudgetResource")
@Table(name = ProjectBudgetResource.TABLE_NAME)
public class ProjectBudgetResource extends Dictionary<ProjectBudgetResource>  {

    private static Logger log = LoggerFactory.getLogger(ProjectBudgetResource.class);

    public static final String TABLE_NAME = "dsd_services_project_budget_resource";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId",precision = 18, scale = 0)
    private double erpId;
    @Column(name = "name",length = 128)
    private String name;
    @Column(name = "nrPoz")
    private int nrPoz;
    @Column(name = "etapId",precision = 18, scale = 0)
    private double etapId;
    @Column(name = "pCena",precision = 18, scale = 2)
    private double pCena;
    @Column(name = "pIlosc",precision = 18, scale = 0)
    private double pIlosc;
    @Column(name = "pWartosc",precision = 18, scale = 0)
    private double pWartosc;
    @Column(name = "rIlosc",precision = 18, scale = 0)
    private double rIlosc;
    @Column(name = "rWartosc",precision = 18, scale = 2)
    private double rWartosc;
    @Column(name = "zasobGlownyId",precision = 18, scale = 0)
    private double zasobGlownyId;
    @Column(name = "rodzajZasobu")
    private int rodzajZasobu;
    @Column(name = "czyGlowny")
    private boolean czyGlowny;


    public ProjectBudgetResource() {
        super.setDictionaryName("Zasoby bud�et�w projekt�w");
    }

    @Override
    public String getErpIdField() {
        return "erpId";
    }

    @Override
    public String getWsIdField() {
        return "id";
    }

    @Override
    public ProjectBudgetResource create() {
        return new ProjectBudgetResource();
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("nrPoz","nrpoz");
        fieldsMap.put("etapId","etap_id");
        fieldsMap.put("pCena","p_cena");
        fieldsMap.put("pIlosc","p_ilosc");
        fieldsMap.put("pWartosc","p_wartosc");
        fieldsMap.put("rIlosc","r_ilosc");
        fieldsMap.put("rWartosc","r_wartosc");
        fieldsMap.put("zasobGlownyId","zasob_glowny_id");
        fieldsMap.put("rodzajZasobu","rodzaj_zasobu");
        fieldsMap.put("czyGlowny","czy_glowny");

        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNrPoz() {
        return nrPoz;
    }

    public void setNrPoz(int nrPoz) {
        this.nrPoz = nrPoz;
    }

    public double getEtapId() {
        return etapId;
    }

    public void setEtapId(double etapId) {
        this.etapId = etapId;
    }

    public double getPCena() {
        return pCena;
    }

    public void setPCena(double pCena) {
        this.pCena = pCena;
    }

    public double getPIlosc() {
        return pIlosc;
    }

    public void setPIlosc(double pIlosc) {
        this.pIlosc = pIlosc;
    }

    public double getPWartosc() {
        return pWartosc;
    }

    public void setPWartosc(double pWartosc) {
        this.pWartosc = pWartosc;
    }

    public double getRIlosc() {
        return rIlosc;
    }

    public void setRIlosc(double rIlosc) {
        this.rIlosc = rIlosc;
    }

    public double getRWartosc() {
        return rWartosc;
    }

    public void setRWartosc(double rWartosc) {
        this.rWartosc = rWartosc;
    }

    public double getZasobGlownyId() {
        return zasobGlownyId;
    }

    public void setZasobGlownyId(double zasobGlownyId) {
        this.zasobGlownyId = zasobGlownyId;
    }

    public int getRodzajZasobu() {
        return rodzajZasobu;
    }

    public void setRodzajZasobu(int rodzajZasobu) {
        this.rodzajZasobu = rodzajZasobu;
    }

    public boolean isCzyGlowny() {
        return czyGlowny;
    }

    public void setCzyGlowny(boolean czyGlowny) {
        this.czyGlowny = czyGlowny;
    }
}
