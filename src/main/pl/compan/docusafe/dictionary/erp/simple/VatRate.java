package pl.compan.docusafe.dictionary.erp.simple;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.VatRate")
@Table(name = VatRate.TABLE_NAME)
public class VatRate extends Dictionary<VatRate>{

    private final static Logger log = LoggerFactory.getLogger(VatRate.class);

    public static final String TABLE_NAME = "dsd_services_vat_rate";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId", precision = 18, scale = 0)
    private double erpId;
    @Column(name = "czyZwolniona", precision = 18, scale = 0)
    private double czyZwolniona;
    @Column(name = "dataObowDo")
    private Date dataObowDo;
    @Column(name = "idm",length=50)
    private String idm;
    @Column(name = "name",length=128)
    private String name;
    @Column(name = "niePodlega", precision = 18, scale = 0)
    private double niePodlega;
    @Column(name = "procent", precision = 18, scale = 2)
    private double procent;

    public VatRate() {
        super.setDictionaryName("Stawki VAT");
    }

    @Override
    public VatRate create() {
        return new VatRate();
    }

    @Override
    public String getErpIdField() {
        return "idm";
    }

    public static VatRate find(Integer id) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(VatRate.class);
            criteria.add(Restrictions.idEq(id.longValue()));
            return (VatRate) criteria.setMaxResults(1).uniqueResult();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public long getId() {
        return id;
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("available","czy_aktywna");
        fieldsMap.put("czyZwolniona","czy_zwolniona");
        fieldsMap.put("dataObowDo","data_obow_do");
        fieldsMap.put("idm","idm");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("niePodlega","nie_podlega");
        fieldsMap.put("procent","procent");
        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCzyZwolniona() {
        return czyZwolniona;
    }

    public void setCzyZwolniona(double czyZwolniona) {
        this.czyZwolniona = czyZwolniona;
    }

    public Date getDataObowDo() {
        return dataObowDo;
    }

    public void setDataObowDo(Date dataObowDo) {
        this.dataObowDo = dataObowDo;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getNiePodlega() {
        return niePodlega;
    }

    public void setNiePodlega(double niePodlega) {
        this.niePodlega = niePodlega;
    }

    public double getProcent() {
        return procent;
    }

    public void setProcent(double procent) {
        this.procent = procent;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VatRate vatRate = (VatRate) o;

        if (available != vatRate.available) return false;
        if (Double.compare(vatRate.czyZwolniona, czyZwolniona) != 0) return false;
        if (Double.compare(vatRate.erpId, erpId) != 0) return false;
        if (id != vatRate.id) return false;
        if (Double.compare(vatRate.niePodlega, niePodlega) != 0) return false;
        if (Double.compare(vatRate.procent, procent) != 0) return false;
        if (dataObowDo != null ? !dataObowDo.equals(vatRate.dataObowDo) : vatRate.dataObowDo != null) return false;
        if (idm != null ? !idm.equals(vatRate.idm) : vatRate.idm != null) return false;
        if (name != null ? !name.equals(vatRate.name) : vatRate.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        temp = Double.doubleToLongBits(erpId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(czyZwolniona);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (dataObowDo != null ? dataObowDo.hashCode() : 0);
        result = 31 * result + (idm != null ? idm.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        temp = Double.doubleToLongBits(niePodlega);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(procent);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
