package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.UnitBudget")
@Table(name = UnitBudget.TABLE_NAME)
public class UnitBudget extends Dictionary<UnitBudget>  {

    private static Logger log = LoggerFactory.getLogger(UnitBudget.class);

    public static final String TABLE_NAME = "dsd_services_unit_budget";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId",precision = 18, scale = 0)
    private Double erpId;
    @Column(name = "idm",length = 50)
    private String idm;
    @Column(name = "name",length = 128)
    private String name;
    @Column(name = "wersja")
    private int wersja;
    @Column(name = "dataObowDo")
    private Date dataObowDo;
    @Column(name = "dataObowOd")
    private Date dataObowOd;
    @Column(name = "czyArchiwalny")
    private boolean czyArchiwalny;


    public UnitBudget() {
        super.setDictionaryName("Bud�et w okresie");
    }

    @Override
    public String getErpIdField() {
        return "idm";
    }

    @Override
    public UnitBudget create() {
        return new UnitBudget();
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("idm","idm");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("dataObowDo","data_obow_do");
        fieldsMap.put("dataObowOd","data_obow_od");
        fieldsMap.put("czyArchiwalny","czy_archiwalny");
        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Double getErpId() {
        return erpId;
    }

    public void setErpId(Double erpId) {
        this.erpId = erpId;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWersja() {
        return wersja;
    }

    public void setWersja(int wersja) {
        this.wersja = wersja;
    }

    public Date getDataObowDo() {
        return dataObowDo;
    }

    public void setDataObowDo(Date dataObowDo) {
        this.dataObowDo = dataObowDo;
    }

    public Date getDataObowOd() {
        return dataObowOd;
    }

    public void setDataObowOd(Date dataObowOd) {
        this.dataObowOd = dataObowOd;
    }

    public boolean isCzyArchiwalny() {
        return czyArchiwalny;
    }

    public void setCzyArchiwalny(boolean czyArchiwalny) {
        this.czyArchiwalny = czyArchiwalny;
    }
}
