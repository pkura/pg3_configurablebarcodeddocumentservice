CREATE TABLE dsd_services_contract_budget(
	[id] [int] PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[kontraktId] [numeric](18, 0) NULL,
	[name] [varchar](128) NULL,
	[pDatako] [datetime] NULL,
	[pDatapo] [datetime] NULL,
	[stan] [int] NULL,
	[status] [int] NULL
)