package pl.compan.docusafe.dictionary.erp.simple.utils;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.lang.reflect.Method;

/**
 * Created by Damian on 25.04.14.
 */
public class ReflectionUtils {
    private final static Logger log = LoggerFactory.getLogger(ReflectionUtils.class);

    /**
     *
     * @param clazz
     * @param fieldName
     * @param logErrors if true it log catched NoSuchFieldException otherwise it ignore this exception
     * @return
     */
    public static Class getFieldType(Class clazz, String fieldName, boolean logErrors) {
        Class fieldType = null;
        try {
            fieldType = clazz.getDeclaredField(fieldName).getType();
        } catch (NoSuchFieldException e) {
            if(logErrors){
                String logMsg = "Class " + clazz.getCanonicalName() + " hasn't member field with name " + fieldName;
                log.error(logMsg,e);
            }
        }
        return fieldType;
    }

    public static Class getFieldType(Object obj, String fieldName, boolean logErrors) {
        return getFieldType(obj.getClass(),fieldName,logErrors);
    }

    /**
     *
     * @param obj
     * @param fieldName
     * @param logErrors if true it log catched NoSuchMethodException otherwise it ignore this exception
     * @return
     */
    public static Method getGetter(Object obj, String fieldName,boolean logErrors){
        return getGetter(obj.getClass(),fieldName,logErrors);
    }

    public static Method getGetter(Class clazz, String fieldName,boolean logErrors) {
        return getMethod(clazz, "get", fieldName, null,logErrors);
    }


    /**
     *
     * @param obj
     * @param fieldName
     * @param argType
     * @param logErrors if true it log catched NoSuchMethodException otherwise it ignore this exception
     * @return
     */
    public static Method getSetter(Object obj, String fieldName, Class argType,boolean logErrors) {
        return getSetter(obj.getClass(),fieldName,argType, logErrors);
    }

    public static Method getSetter(Class clazz, String fieldName,Class argType ,boolean logErrors) {
        return getMethod(clazz, "set", fieldName, argType,logErrors);
    }

    public static Method getSetter(Class clazz, String fieldName,boolean logErrors) {
        Class argClass = getFieldType(clazz,fieldName,false);
        return getMethod(clazz, "set", fieldName, argClass,logErrors);
    }

    public static Method getMethod(Class clazz, String methodPrefix,String fieldName, Class argType, boolean logErrors ){
        String methodName =methodPrefix + fieldName.replaceFirst("\\w", fieldName.substring(0, 1).toUpperCase());

        Method fieldSetter = null;
        try {
            fieldSetter = getMethod(clazz,methodName,argType);
            if(fieldSetter == null){
                methodName = methodPrefix + fieldName;
                fieldSetter = getMethod(clazz,methodName,argType);
            }
            if (fieldSetter == null) {
                throw new NoSuchMethodException();
            }
        } catch (NoSuchMethodException e) {
            if(logErrors){
                String logMsg = "Class " + clazz.getCanonicalName() + " hasn't got " + methodPrefix + "ter method for field " + fieldName;
                log.error(logMsg,e);
            }
        }
        return fieldSetter;
    }

    private static Method getMethod(Class clazz, String methodName,Class argType) {
        try {
            if(argType != null){
                return clazz.getMethod(methodName,argType);
            }else{
                return clazz.getMethod(methodName);
            }
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

}
