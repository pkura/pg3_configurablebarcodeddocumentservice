CREATE TABLE [dbo].[dsd_services_unitbudgetko](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[bdStrBudzetId] [numeric](18, 0) NULL,
	[bdStrBudzetIdn] [varchar](128) NULL,
	[budzetId] [numeric](18, 0) NULL,
	[budzetIdm] [varchar](128) NULL,
	[czyAgregat] [int] NULL,
	[wersja] [int] NULL
)
