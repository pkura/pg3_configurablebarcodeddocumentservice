CREATE TABLE [dbo].[dsd_services_project_budget_item](
	[id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](19, 0) NULL,
	[name] [varchar](128) NULL,
	[nrPoz] [int] NULL,
	[budzetId] [numeric](19, 0) NULL,
	[pDatako] [datetime] NULL,
	[pDatapo] [datetime] NULL,
)