package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.UnitBudgetKo;

/**
 * Created by Damian on 17.04.14.
 */
public class UnitBudgetKoImport extends DictionaryImport<UnitBudgetKo> {


    @Override
    protected UnitBudgetKo getDictionary() {
        return new UnitBudgetKo();
    }

    @Override
    protected String getWSObjectName() {
        return "UnitBudgetKo";
    }

}
