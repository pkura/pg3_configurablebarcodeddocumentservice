package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.CostInvoiceProduct;
import pl.compan.docusafe.ws.simple.dictionaries.DictionaryServiceStub;

import java.rmi.RemoteException;

/**
 * Created by Damian on 17.04.14.
 */
public class CostInvoiceProductImport extends DictionaryImport<CostInvoiceProduct> {

    @Override
    protected CostInvoiceProduct getDictionary() {
        return new CostInvoiceProduct();
    }

    @Override
    protected String getWSObjectName() {
        return "CostInvoiceProduct";
    }
}
