package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.ContractBudget;
import pl.compan.docusafe.ws.simple.dictionaries.DictionaryServiceStub;

import java.rmi.RemoteException;

/**
 * Created by Damian on 17.04.14.
 */
public class ContractBudgetImport extends DictionaryImport<ContractBudget> {


    @Override
    protected ContractBudget getDictionary() {
        return new ContractBudget();
    }

    @Override
    protected String getWSObjectName() {
        return "ContractBogdet";
    }
}
