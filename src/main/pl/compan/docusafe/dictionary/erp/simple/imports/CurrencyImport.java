package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.Currency;
import pl.compan.docusafe.ws.simple.dictionaries.DictionaryServiceStub;

import java.rmi.RemoteException;

/**
 * Created by Damian on 17.04.14.
 */
public class CurrencyImport extends DictionaryImport<Currency> {

    @Override
    protected Currency getDictionary() {
        return new Currency();
    }

    @Override
    protected String getWSObjectName() {
        return "Currency";
    }
}
