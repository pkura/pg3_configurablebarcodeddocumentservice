package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.ProjectBudgeItem")
@Table(name = ProjectBudgeItem.TABLE_NAME)
public class ProjectBudgeItem extends Dictionary<ProjectBudgeItem>  {

    private static Logger log = LoggerFactory.getLogger(ProjectBudgeItem.class);

    public static final String TABLE_NAME = "dsd_services_project_budget_item";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId",precision = 18, scale = 0)
    private double erpId;
    @Column(name = "name",length = 128)
    private String name;
    @Column(name = "nrPoz")
    private int nrPoz;
    @Column(name = "budzetId",precision = 18, scale = 0)
    private double budzetId;
    @Column(name = "pDatako")
    private Date pDatako;
    @Column(name = "pDatapo")
    private Date pDatapo;


    public ProjectBudgeItem() {
        super.setDictionaryName("Etapy bud�et�w projekt�w");
    }

    @Override
    public String getErpIdField() {
        return "erpId";
    }

    @Override
    public String getWsIdField() {
        return "id";
    }

    @Override
    public ProjectBudgeItem create() {
        return new ProjectBudgeItem();
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("nrPoz","nrpoz");
        fieldsMap.put("budzetId","budzet_id");
        fieldsMap.put("pDatako","p_datako");
        fieldsMap.put("pDatapo","p_datapo");
        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNrPoz() {
        return nrPoz;
    }

    public void setNrPoz(int nrPoz) {
        this.nrPoz = nrPoz;
    }

    public double getBudzetId() {
        return budzetId;
    }

    public void setBudzetId(double budzetId) {
        this.budzetId = budzetId;
    }

    public Date getpDatako() {
        return pDatako;
    }

    public void setpDatako(Date pDatako) {
        this.pDatako = pDatako;
    }

    public Date getpDatapo() {
        return pDatapo;
    }

    public void setpDatapo(Date pDatapo) {
        this.pDatapo = pDatapo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectBudgeItem that = (ProjectBudgeItem) o;

        if (available != that.available) return false;
        if (Double.compare(that.budzetId, budzetId) != 0) return false;
        if (Double.compare(that.erpId, erpId) != 0) return false;
        if (id != that.id) return false;
        if (nrPoz != that.nrPoz) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (pDatako != null ? !pDatako.equals(that.pDatako) : that.pDatako != null) return false;
        if (pDatapo != null ? !pDatapo.equals(that.pDatapo) : that.pDatapo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        temp = Double.doubleToLongBits(erpId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + nrPoz;
        temp = Double.doubleToLongBits(budzetId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (pDatako != null ? pDatako.hashCode() : 0);
        result = 31 * result + (pDatapo != null ? pDatapo.hashCode() : 0);
        return result;
    }
}
