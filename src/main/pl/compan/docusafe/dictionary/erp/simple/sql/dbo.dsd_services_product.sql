CREATE TABLE [dbo].[dsd_services_product](
	[id] [numeric](18, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[jmIdn] [varchar](50) NULL,
	[klaswytwIdn] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[kind] [int] NULL,
	[vatRateId] [varchar](50) NULL,
)
