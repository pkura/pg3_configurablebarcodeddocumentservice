package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.Contract")
@Table(name = Contract.TABLE_NAME)
public class Contract extends Dictionary<Contract> {

    private final static Logger log = LoggerFactory.getLogger(Contract.class);

    public static final String TABLE_NAME = "dsd_services_contract";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId",precision = 18, scale = 0)
    private double erpId;
    @Column(name = "idm", length=50)
    private String idm;
    @Column(name = "idn", precision = 18, scale = 0)
    private double idn;
    @Column(name = "name", length=128)
    private String name;
    @Column(name = "pDataKo")
    private Date pDataKo;
    @Column(name = "pDataPo")
    private Date pDataPo;
    @Column(name = "typKontraktId", precision = 18, scale = 0)
    private double typKontraktId;
    @Column(name = "typKontraktIdn", length=128)
    private String typKontraktIdn;
    @Column(name = "kierownikId")
    private Integer kierownikId;
    @Column(name = "opiekunId")
    private Integer opiekunId;
    @Column(name = "dodatkowaAkceptacja")
    private Boolean dodatkowaAkceptacja;

    public Contract() {
        super.setDictionaryName("Kontrakty");
    }

    @Override
    public Contract create() {
        return new Contract();
    }

    @Override
    public String getErpIdField() {
        return "idm";
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("idm","idm");
        fieldsMap.put("idn","identyfikator_num");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("pDataKo","p_datako");
        fieldsMap.put("pDataPo","p_datapo");
        fieldsMap.put("typKontraktId","typ_kontrakt_id");
        fieldsMap.put("typKontraktIdn","typ_kontrakt_idn");

        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public double getIdn() {
        return idn;
    }

    public void setIdn(double idn) {
        this.idn = idn;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getPDataKo() {
        return pDataKo;
    }

    public void setPDataKo(Date pDataKo) {
        this.pDataKo = pDataKo;
    }

    public Date getPDataPo() {
        return pDataPo;
    }

    public void setPDataPo(Date pDataPo) {
        this.pDataPo = pDataPo;
    }

    public double getTypKontraktId() {
        return typKontraktId;
    }

    public void setTypKontraktId(double typKontraktId) {
        this.typKontraktId = typKontraktId;
    }

    public String getTypKontraktIdn() {
        return typKontraktIdn;
    }

    public void setTypKontraktIdn(String typKontraktIdn) {
        this.typKontraktIdn = typKontraktIdn;
    }

    public Integer getKierownikId() {
        return kierownikId;
    }

    public void setKierownikId(Integer kierownikId) {
        this.kierownikId = kierownikId;
    }

    public Integer getOpiekunId() {
        return opiekunId;
    }

    public void setOpiekunId(Integer opiekunId) {
        this.opiekunId = opiekunId;
    }

    public Boolean isDodatkowaAkceptacja() {
        return dodatkowaAkceptacja;
    }

    public void setDodatkowaAkceptacja(Boolean dodatkowaAkceptacja) {
        this.dodatkowaAkceptacja = dodatkowaAkceptacja;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Boolean getDodatkowaAkceptacja() {
        return dodatkowaAkceptacja;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contract contract = (Contract) o;

        if (available != contract.available) return false;
        if (Double.compare(contract.erpId, erpId) != 0) return false;
        if (id != contract.id) return false;
        if (Double.compare(contract.idn, idn) != 0) return false;
        if (Double.compare(contract.typKontraktId, typKontraktId) != 0) return false;
        if (dodatkowaAkceptacja != null ? !dodatkowaAkceptacja.equals(contract.dodatkowaAkceptacja) : contract.dodatkowaAkceptacja != null)
            return false;
        if (idm != null ? !idm.equals(contract.idm) : contract.idm != null) return false;
        if (kierownikId != null ? !kierownikId.equals(contract.kierownikId) : contract.kierownikId != null)
            return false;
        if (name != null ? !name.equals(contract.name) : contract.name != null) return false;
        if (opiekunId != null ? !opiekunId.equals(contract.opiekunId) : contract.opiekunId != null) return false;
        if (pDataKo != null ? !pDataKo.equals(contract.pDataKo) : contract.pDataKo != null) return false;
        if (pDataPo != null ? !pDataPo.equals(contract.pDataPo) : contract.pDataPo != null) return false;
        if (typKontraktIdn != null ? !typKontraktIdn.equals(contract.typKontraktIdn) : contract.typKontraktIdn != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        temp = Double.doubleToLongBits(erpId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (idm != null ? idm.hashCode() : 0);
        temp = Double.doubleToLongBits(idn);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (pDataKo != null ? pDataKo.hashCode() : 0);
        result = 31 * result + (pDataPo != null ? pDataPo.hashCode() : 0);
        temp = Double.doubleToLongBits(typKontraktId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (typKontraktIdn != null ? typKontraktIdn.hashCode() : 0);
        result = 31 * result + (kierownikId != null ? kierownikId.hashCode() : 0);
        result = 31 * result + (opiekunId != null ? opiekunId.hashCode() : 0);
        result = 31 * result + (dodatkowaAkceptacja != null ? dodatkowaAkceptacja.hashCode() : 0);
        return result;
    }
}
