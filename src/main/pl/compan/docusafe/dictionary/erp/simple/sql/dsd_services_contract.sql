CREATE TABLE dsd_services_contract(
	[id] [int] PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[idn] [numeric](18, 0) NULL,
	[name] [varchar](128) NULL,
	[pDataKo] [datetime] NULL,
	[pDataPo] [datetime] NULL,
	[typKontraktId] [numeric](18, 0) NULL,
	[typKontraktIdn] [varchar](128) NULL,
	[kierownikId] [int] NULL,
	[opiekunId] [int] NULL,
	[dodatkowaAkceptacja] [bit] NULL
)