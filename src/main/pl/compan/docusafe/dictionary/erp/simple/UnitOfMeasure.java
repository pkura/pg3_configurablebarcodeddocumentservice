package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.UnitOfMeasure")
@Table(name = UnitOfMeasure.TABLE_NAME)
public class UnitOfMeasure  extends Dictionary<UnitOfMeasure> {

    private final static Logger log = LoggerFactory.getLogger(UnitOfMeasure.class);

    public static final String TABLE_NAME = "dsd_services_unit_of_measure";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId",precision = 18, scale = 0)
    private double erpId;
    @Column(name = "idm", length=50)
    private String idm;
    @Column(name = "typIdn", length = 50)
    private String typIdn;
    @Column(name = "name", length=128)
    private String name;
    @Column(name = "precyzjaJm")
    private int precyzjaJm;

    public UnitOfMeasure() {
        super.setDictionaryName("Jednostki miar");
    }


    @Override
    public UnitOfMeasure create() {
        return new UnitOfMeasure();
    }

    @Override
    public String getErpIdField() {
        return "idm";
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("idm","idm");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("typIdn","jmtyp_idn");
        fieldsMap.put("precyzjaJm","precyzjajm");
        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public String getTypIdn() {
        return typIdn;
    }

    public void setTypIdn(String typIdn) {
        this.typIdn = typIdn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrecyzjaJm() {
        return precyzjaJm;
    }

    public void setPrecyzjaJm(int precyzjaJm) {
        this.precyzjaJm = precyzjaJm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnitOfMeasure that = (UnitOfMeasure) o;

        if (available != that.available) return false;
        if (Double.compare(that.erpId, erpId) != 0) return false;
        if (id != that.id) return false;
        if (precyzjaJm != that.precyzjaJm) return false;
        if (idm != null ? !idm.equals(that.idm) : that.idm != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (typIdn != null ? !typIdn.equals(that.typIdn) : that.typIdn != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        temp = Double.doubleToLongBits(erpId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (idm != null ? idm.hashCode() : 0);
        result = 31 * result + (typIdn != null ? typIdn.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + precyzjaJm;
        return result;
    }
}
