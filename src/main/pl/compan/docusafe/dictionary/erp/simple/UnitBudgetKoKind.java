package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.UnitBudgetKoKind")
@Table(name = UnitBudgetKoKind.TABLE_NAME)
public class UnitBudgetKoKind extends Dictionary<UnitBudgetKoKind>  {

    private static Logger log = LoggerFactory.getLogger(UnitBudgetKoKind.class);

    public static final String TABLE_NAME = "dsd_services_unit_budget_ko_kind";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId",precision = 18, scale = 0)
    private Double erpId;
    @Column(name = "name",length = 128)
    private String name;
    @Column(name = "bdBudzetKoIdm",length = 50)
    private String bdBudzetKoIdm;
    @Column(name = "bdBudzetKoId",precision = 18, scale = 0)
    private double bdBudzetKoId;
    @Column(name = "bdNadRodzajKoId",precision = 18, scale = 0)
    private double bdNadRodzajKoId;
    @Column(name = "nrPoz")
    private int nrPoz;
    @Column(name = "opis",length = 500)
    private String opis;
    @Column(name = "pKoszt",precision = 18, scale = 0)
    private double pKoszt;
    @Column(name = "pKosztLimit",precision = 18, scale = 0)
    private double pKosztLimit;
    @Column(name = "rKoszt",precision = 18, scale = 0)
    private double rKoszt;
    @Column(name = "wytworId",precision = 18, scale = 0)
    private double wytworId;
    @Column(name = "wytworIdm",length = 50)
    private String wytworIdm;


    public UnitBudgetKoKind() {
        super.setDictionaryName("Pozycje Bud�etowe");
    }

    @Override
    public String getErpIdField() {
        return "erpId";
    }

    @Override
    public String getWsIdField() {
        return "id";
    }

    @Override
    public UnitBudgetKoKind create() {
        return new UnitBudgetKoKind();
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("bdBudzetKoIdm","bd_budzet_ko_idm");
        fieldsMap.put("bdBudzetKoId","bd_budzet_ko_id");
        fieldsMap.put("bdNadRodzajKoId","bd_nad_rodzaj_ko_id");
        fieldsMap.put("nrPoz","nrpoz");
        fieldsMap.put("opis","opis");
        fieldsMap.put("pKoszt","p_koszt");
        fieldsMap.put("pKosztLimit","p_koszt_limit");
        fieldsMap.put("rKoszt","r_koszt");
        fieldsMap.put("wytworId","wytwor_id");
        fieldsMap.put("wytworIdm","wytwor_idm");

        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Double getErpId() {
        return erpId;
    }

    public void setErpId(Double erpId) {
        this.erpId = erpId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBdBudzetKoIdm() {
        return bdBudzetKoIdm;
    }

    public void setBdBudzetKoIdm(String bdBudzetKoIdm) {
        this.bdBudzetKoIdm = bdBudzetKoIdm;
    }

    public double getBdBudzetKoId() {
        return bdBudzetKoId;
    }

    public void setBdBudzetKoId(double bdBudzetKoId) {
        this.bdBudzetKoId = bdBudzetKoId;
    }

    public double getBdNadRodzajKoId() {
        return bdNadRodzajKoId;
    }

    public void setBdNadRodzajKoId(double bdNadRodzajKoId) {
        this.bdNadRodzajKoId = bdNadRodzajKoId;
    }

    public int getNrPoz() {
        return nrPoz;
    }

    public void setNrPoz(int nrPoz) {
        this.nrPoz = nrPoz;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public double getpKoszt() {
        return pKoszt;
    }

    public void setpKoszt(double pKoszt) {
        this.pKoszt = pKoszt;
    }

    public double getpKosztLimit() {
        return pKosztLimit;
    }

    public void setpKosztLimit(double pKosztLimit) {
        this.pKosztLimit = pKosztLimit;
    }

    public double getrKoszt() {
        return rKoszt;
    }

    public void setrKoszt(double rKoszt) {
        this.rKoszt = rKoszt;
    }

    public double getWytworId() {
        return wytworId;
    }

    public void setWytworId(double wytworId) {
        this.wytworId = wytworId;
    }

    public String getWytworIdm() {
        return wytworIdm;
    }

    public void setWytworIdm(String wytworIdm) {
        this.wytworIdm = wytworIdm;
    }
}
