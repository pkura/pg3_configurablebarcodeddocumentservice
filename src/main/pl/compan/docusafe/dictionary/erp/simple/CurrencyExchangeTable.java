package pl.compan.docusafe.dictionary.erp.simple;


import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.CurrencyExchangeTable")
@Table(name = CurrencyExchangeTable.TABLE_NAME)
public class CurrencyExchangeTable extends Dictionary<CurrencyExchangeTable>{

    private final static Logger log = LoggerFactory.getLogger(CurrencyExchangeTable.class);

    public static final String TABLE_NAME = "dsd_services_currency_exchange_table";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId",precision = 18, scale = 0)
    private double erpId;
    @Column(name = "idm",length = 50)
    private String idm;
    @Column(name = "czasOgloszenia")
    private Date czasOgloszenia;
    @Column(name = "dataDo")
    private Date dataDo;
    @Column(name = "dataOd")
    private Date dataOd;
    @Column(name = "tolerancja",precision = 18, scale = 0)
    private double tolerancja;
    @Column(name = "uwagi", length = 254)
    private String uwagi;


    public CurrencyExchangeTable() {
        super.setDictionaryName("Tabele kurs�w");
    }

    @Override
    public String getErpIdField() {
        return "idm";
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("idm","idm");
        fieldsMap.put("czasOgloszenia","dataczasoglosz");
        fieldsMap.put("dataDo","datdo");
        fieldsMap.put("dataOd","datod");
        fieldsMap.put("tolerancja","tolerancja");
        fieldsMap.put("uwagi","uwagi");
        return fieldsMap;
    }

    @Override
    public CurrencyExchangeTable create() {
        return new CurrencyExchangeTable();
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public Date getCzasOgloszenia() {
        return czasOgloszenia;
    }

    public void setCzasOgloszenia(Date czasOgloszenia) {
        this.czasOgloszenia = czasOgloszenia;
    }

    public Date getDataDo() {
        return dataDo;
    }

    public void setDataDo(Date dataDo) {
        this.dataDo = dataDo;
    }

    public Date getDataOd() {
        return dataOd;
    }

    public void setDataOd(Date dataOd) {
        this.dataOd = dataOd;
    }

    public double getTolerancja() {
        return tolerancja;
    }

    public void setTolerancja(double tolerancja) {
        this.tolerancja = tolerancja;
    }

    public String getUwagi() {
        return uwagi;
    }

    public void setUwagi(String uwagi) {
        this.uwagi = uwagi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CurrencyExchangeTable that = (CurrencyExchangeTable) o;

        if (available != that.available) return false;
        if (Double.compare(that.erpId, erpId) != 0) return false;
        if (id != that.id) return false;
        if (Double.compare(that.tolerancja, tolerancja) != 0) return false;
        if (czasOgloszenia != null ? !czasOgloszenia.equals(that.czasOgloszenia) : that.czasOgloszenia != null)
            return false;
        if (dataDo != null ? !dataDo.equals(that.dataDo) : that.dataDo != null) return false;
        if (dataOd != null ? !dataOd.equals(that.dataOd) : that.dataOd != null) return false;
        if (idm != null ? !idm.equals(that.idm) : that.idm != null) return false;
        if (uwagi != null ? !uwagi.equals(that.uwagi) : that.uwagi != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        temp = Double.doubleToLongBits(erpId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (idm != null ? idm.hashCode() : 0);
        result = 31 * result + (czasOgloszenia != null ? czasOgloszenia.hashCode() : 0);
        result = 31 * result + (dataDo != null ? dataDo.hashCode() : 0);
        result = 31 * result + (dataOd != null ? dataOd.hashCode() : 0);
        temp = Double.doubleToLongBits(tolerancja);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (uwagi != null ? uwagi.hashCode() : 0);
        return result;
    }
}
