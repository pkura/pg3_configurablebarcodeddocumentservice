/**
 * 
 */
package pl.compan.docusafe.dictionary.erp.simple;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity(name = "pl.compan.docusafe.dictionary.erp.simple.OrganizationalUnit")
@Table(name = OrganizationalUnit.TABLE_NAME)
public class OrganizationalUnit extends Dictionary<OrganizationalUnit> {

    private final static Logger log = LoggerFactory.getLogger(Contract.class);

    public static final String TABLE_NAME = "dsd_services_organizational_unit";
    public static final String GENERATOR = TABLE_NAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = GENERATOR)
    @SequenceGenerator(name = GENERATOR, sequenceName = GENERATOR)
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "available", nullable = false)
    private boolean available = true;
    @Column(name = "erpId",precision = 18, scale = 0)
    private double erpId;
    @Column(name = "czyKadrowa")
    private boolean czyKadrowa;
    @Column(name = "czyKosztowa")
    private boolean czyKosztowa;
    @Column(name = "czyOddzial")
    private boolean czyOddzial;
    @Column(name = "idm", length=50)
    private String idm;
    @Column(name = "idn", precision = 18, scale = 0)
    private double idn;
    @Column(name = "name", length=128)
    private String name;
    @Column(name = "komorkaNadIdn", length=50)
    private String komorkaNadIdn;
    @Column(name = "komorkaNadNazwa", length=128)
    private String komorkaNadNazwa;

    public OrganizationalUnit() {
        super.setDictionaryName("MPK");
    }

    @Override
    public OrganizationalUnit create() {
        return new OrganizationalUnit();
    }

    @Override
    public String getErpIdField() {
        return "idm";
    }

    @Override
    protected Map<String, String> getFieldsToWsMap() {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("erpId","id");
        fieldsMap.put("idm","idm");
        fieldsMap.put("name","nazwa");
        fieldsMap.put("idn","identyfikator_num");
        fieldsMap.put("available","czy_aktywna");
        fieldsMap.put("czyKadrowa","czy_kadrowa");
        fieldsMap.put("czyKosztowa","czy_kosztowa");
        fieldsMap.put("czyOddzial","czy_oddzial");
        fieldsMap.put("komorkaNadIdn","komorka_nad_idn");
        fieldsMap.put("komorkaNadNazwa","komorka_nad_nazwa");
        return fieldsMap;
    }

    @Override
    public String getTableName(){
        return TABLE_NAME;
    }

    @Override
    public void save() throws EdmException {
        log.info("Zapisuj� element z erpId: " + erpId);
        super.save();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public boolean isCzyKadrowa() {
        return czyKadrowa;
    }

    public void setCzyKadrowa(boolean czyKadrowa) {
        this.czyKadrowa = czyKadrowa;
    }

    public boolean isCzyKosztowa() {
        return czyKosztowa;
    }

    public void setCzyKosztowa(boolean czyKosztowa) {
        this.czyKosztowa = czyKosztowa;
    }

    public boolean isCzyOddzial() {
        return czyOddzial;
    }

    public void setCzyOddzial(boolean czyOddzial) {
        this.czyOddzial = czyOddzial;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public double getIdn() {
        return idn;
    }

    public void setIdn(double idn) {
        this.idn = idn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKomorkaNadIdn() {
        return komorkaNadIdn;
    }

    public void setKomorkaNadIdn(String komorkaNadIdn) {
        this.komorkaNadIdn = komorkaNadIdn;
    }

    public String getKomorkaNadNazwa() {
        return komorkaNadNazwa;
    }

    public void setKomorkaNadNazwa(String komorkaNadNazwa) {
        this.komorkaNadNazwa = komorkaNadNazwa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrganizationalUnit that = (OrganizationalUnit) o;

        if (available != that.available) return false;
        if (czyKadrowa != that.czyKadrowa) return false;
        if (czyKosztowa != that.czyKosztowa) return false;
        if (czyOddzial != that.czyOddzial) return false;
        if (Double.compare(that.erpId, erpId) != 0) return false;
        if (id != that.id) return false;
        if (Double.compare(that.idn, idn) != 0) return false;
        if (idm != null ? !idm.equals(that.idm) : that.idm != null) return false;
        if (komorkaNadIdn != null ? !komorkaNadIdn.equals(that.komorkaNadIdn) : that.komorkaNadIdn != null)
            return false;
        if (komorkaNadNazwa != null ? !komorkaNadNazwa.equals(that.komorkaNadNazwa) : that.komorkaNadNazwa != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available ? 1 : 0);
        temp = Double.doubleToLongBits(erpId);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (czyKadrowa ? 1 : 0);
        result = 31 * result + (czyKosztowa ? 1 : 0);
        result = 31 * result + (czyOddzial ? 1 : 0);
        result = 31 * result + (idm != null ? idm.hashCode() : 0);
        temp = Double.doubleToLongBits(idn);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (komorkaNadIdn != null ? komorkaNadIdn.hashCode() : 0);
        result = 31 * result + (komorkaNadNazwa != null ? komorkaNadNazwa.hashCode() : 0);
        return result;
    }
}


