package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.ProjectBudgeItem;

/**
 * Created by Damian on 17.04.14.
 */
public class ProjectBudgetItemImport extends DictionaryImport<ProjectBudgeItem> {

    @Override
    protected ProjectBudgeItem getDictionary() {
        return new ProjectBudgeItem();
    }

    @Override
    protected String getWSObjectName() {
        return "ProjectBudgetItem";
    }
}
