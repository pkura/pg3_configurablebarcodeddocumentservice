package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.UnitBudget;

/**
 * Created by Damian on 17.04.14.
 */
public class UnitBudgetImport extends DictionaryImport<UnitBudget> {


    @Override
    protected UnitBudget getDictionary() {
        return new UnitBudget();
    }

    @Override
    protected String getWSObjectName() {
        return "UnitBudget";
    }

}
