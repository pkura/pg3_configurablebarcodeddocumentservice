package pl.compan.docusafe.dictionary.erp.simple.cache;

import com.google.common.primitives.Ints;

import java.util.concurrent.TimeUnit;

/**
* Created by kk on 14.05.14.
*/
public class CacheParam {
    private int expiryAfterWriteTime;
    private TimeUnit expiryAfterWriteUnit;

    public static CacheParam makeCacheParam(String params) {
        CacheParam cacheParam = new CacheParam();
        String timeUnitRaw = params.replaceAll("\\d+", "");
        cacheParam.expiryAfterWriteTime = Ints.tryParse(params.replaceAll(timeUnitRaw, ""));

        if ("s".equals(timeUnitRaw)) {
            cacheParam.expiryAfterWriteUnit = TimeUnit.SECONDS;
        } else if ("m".equals(timeUnitRaw)) {
            cacheParam.expiryAfterWriteUnit = TimeUnit.MINUTES;
        } else {
            throw new UnsupportedOperationException();
        }
        return cacheParam;
    }

    public int getExpiryAfterWriteTime() {
        return expiryAfterWriteTime;
    }

    public TimeUnit getExpiryAfterWriteUnit() {
        return expiryAfterWriteUnit;
    }
}
