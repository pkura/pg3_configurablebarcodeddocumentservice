package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.RolesInUnitBudgetKo;

/**
 * Created by Raf on 2014-05-07.
 */
public class RolesInUnitBudgetKoImport extends DictionaryImport<RolesInUnitBudgetKo> {



    @Override
    protected RolesInUnitBudgetKo getDictionary() {
        return new RolesInUnitBudgetKo();
    }

    @Override
    protected String getWSObjectName() {
        return "RolesInUnitBudgetKo";
    }
}




