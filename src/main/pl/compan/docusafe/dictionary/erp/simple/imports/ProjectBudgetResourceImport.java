package pl.compan.docusafe.dictionary.erp.simple.imports;

import pl.compan.docusafe.dictionary.erp.simple.ProjectBudgetResource;

/**
 * Created by Damian on 17.04.14.
 */
public class ProjectBudgetResourceImport extends DictionaryImport<ProjectBudgetResource> {

    @Override
    protected ProjectBudgetResource getDictionary() {
        return new ProjectBudgetResource();
    }

    @Override
    protected String getWSObjectName() {
        return "ProjectBudgetItemResource";
    }
}
