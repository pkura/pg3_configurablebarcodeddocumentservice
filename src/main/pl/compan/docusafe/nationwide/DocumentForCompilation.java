package pl.compan.docusafe.nationwide;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;

/**
 * Zapami�tuje dane o jednym wybranym przez u�yytkownika dokumencie do dokonania kompilacji za��cznik�w.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class DocumentForCompilation
{
    private Long id;
    private Integer hversion;
    private Long documentId;    
    private Integer priority;
    /** nazwa u�ytkownika, kt�ry wybra� ten dokument */
    private String username;
    
    public static DocumentForCompilation find(Long id) throws EdmException
    {
        return Finder.find(DocumentForCompilation.class, id);
    }
    
    /**
     * Wyszukuje informacje o wszystkich wybranych dokumentach do kompilacji przez
     * danego u�ytkownika.
     */
    @SuppressWarnings("unchecked") 
    public static List<DocumentForCompilation> find(String username) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DocumentForCompilation.class);
            c.add(Restrictions.eq("username", username));
            c.addOrder(Order.asc("priority"));
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Wyszukuje identyfikatory wszystkich wybranych dokument�w do kompilacji przez
     * danego u�ytkownika posortowanych wed�ug priorytetu kompilacji.
     */
    @SuppressWarnings("unchecked") 
    public static List<Long> findIds(String username) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DocumentForCompilation.class);
            c.setProjection(Projections.property("documentId")).
                add(Restrictions.eq("username", username)).
                addOrder(Order.asc("priority"));
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Usuwa wszystkie wpisy z wybranymi dokumentami dla danego u�ytkownika.
     * 
     * @param documentId
     * @throws EdmException
     */
    public static void delete(String username, Long documentId) throws EdmException
    {
        PreparedStatement pst = null;
        try
        {
        	if (documentId !=null)
        	{
        		pst = DSApi.context().prepareStatement(
        				"delete from "+DSApi.getTableName(DocumentForCompilation.class)+ " where username=? and documentId=?");
        		pst.setString(1, username);
        		pst.setLong(2, documentId);
        	}
        	else
        	{
        		pst = DSApi.context().prepareStatement(
        				"delete from "+DSApi.getTableName(DocumentForCompilation.class)+ " where username=?");
        		pst.setString(1, username);	
        	}
            pst.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }
    
    
    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public Integer getPriority()
    {
        return priority;
    }

    public void setPriority(Integer priority)
    {
        this.priority = priority;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }           
}
