package pl.compan.docusafe.nationwide;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.PdfLayerParams;

import com.lowagie.text.BadElementException;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfLayer;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.codec.BmpImage;
import com.lowagie.text.pdf.codec.TiffImage;
/* User: Administrator, Date: 2006-01-17 12:44:23 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: LayeredPdf.java,v 1.14 2009/06/18 10:13:38 kubaw Exp $
 */
public class LayeredPdf
{
	
	/**
	 * Metoda robi z plikow jednego PDFa. NIE UZYWAC W WYPADKU ZALCZNIKOW!!!
	 * @param iterator
	 * @param layer
	 * @param appendMessage
	 * @param messageFirst
	 * @param os
	 * @param beginning
	 * @param keywords
	 * @param ending
	 * @throws IOException
	 * @throws DocumentException
	 */
	@Deprecated
    public static void filesAsPdf(Iterator iterator, String layer,
                                  String appendMessage, boolean messageFirst,
                                  OutputStream os, Element beginning, String keywords, Element ending)
        throws IOException, DocumentException
    {
        com.lowagie.text.Document pdfDoc = null;
        PdfWriter writer = null;
        PdfContentByte cb = null;

        File fontDir = new File(Docusafe.getHome(), "fonts");
        File arial = new File(fontDir, "arial.ttf");
        BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
            BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font font = new Font(baseFont, 12);
        font.setColor(200, 0, 0);

        //boolean layerAdded = false;

        // dodajemy poczatek
        if (beginning != null)
        {
            if (pdfDoc == null)
            {
                pdfDoc = new com.lowagie.text.Document(PageSize.A4);
                writer = PdfWriter.getInstance(pdfDoc, os);
                if (keywords != null)
                    pdfDoc.addKeywords(keywords);
                pdfDoc.open();
                cb = writer.getDirectContent();
            }
            pdfDoc.add(beginning);
        }
        
        // dodajemy podany tekst (o ile ma byc to dodane na poczatku)
        if (appendMessage != null && messageFirst)
        {
            if (pdfDoc == null)
            {
                pdfDoc = new com.lowagie.text.Document(PageSize.A4);
                writer = PdfWriter.getInstance(pdfDoc, os);
                if (keywords != null)
                    pdfDoc.addKeywords(keywords);
                pdfDoc.open();
                cb = writer.getDirectContent();
            }
            else
            {
                pdfDoc.newPage();
            }
            pdfDoc.add(new Phrase(appendMessage, font));
        }
        
        // iterujemy po poszczegolnych plikach i dodajemy
        while (iterator.hasNext())
        {
            File file = (File) iterator.next();
            int pages = getImagePages(file);

            for (int i=0; i < pages; i++)
            {
                if (isPdf(file))                
                {
                    // dodawanie pdf-a
                    
                    PdfReader reader = new PdfReader(file.getAbsolutePath());
                    
                    for (int j=0; j < reader.getNumberOfPages(); j++)
                    {
                        int pageNumber = j+1;
                        
                        Rectangle pageSize = reader.getPageSizeWithRotation(pageNumber);
                        if (pdfDoc == null)
                        {
                            pdfDoc = new com.lowagie.text.Document(pageSize);
                            writer = PdfWriter.getInstance(pdfDoc, os);
                            if (keywords != null)
                                pdfDoc.addKeywords(keywords);
                            pdfDoc.open();
                            cb = writer.getDirectContent();
                        }
                        else
                        {
                            pdfDoc.setPageSize(pageSize);
                            pdfDoc.newPage();
                        }
                        
                        PdfImportedPage page = writer.getImportedPage(reader, pageNumber);
                        int rotation = reader.getPageRotation(pageNumber);
                        if (rotation == 90 || rotation == 270) 
                        {
                            cb.addTemplate(page, 0, -1f, 1f, 0, 0, reader.getPageSizeWithRotation(pageNumber).height());
                        }
                        else 
                        {
                            cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }
                    }                    
                }
                else
                {
                    // dodawanie obrazka
                    
                    Image image = getImage(file, i);
                    Rectangle pageSize =
                        image.plainWidth() > image.plainHeight() ? PageSize.A3.rotate() : PageSize.A4;

                    if (pdfDoc == null)
                    {
                        pdfDoc = new com.lowagie.text.Document(pageSize);
                        writer = PdfWriter.getInstance(pdfDoc, os);
                        if (keywords != null)
                            pdfDoc.addKeywords(keywords);
                        pdfDoc.open();
                        cb = writer.getDirectContent();
                    }
                    else
                    {
                        pdfDoc.setPageSize(pageSize);
                        pdfDoc.newPage();
                    }

                    image.scaleToFit(pageSize.width(), pageSize.height());
                    image.setAbsolutePosition(0, 0);
                    cb.addImage(image);
                }
                
                if(!getRemarksOnSeperatePage() && i == 0)
                {
                    float pw = pdfDoc.getPageSize().width();
                    float ph = pdfDoc.getPageSize().height();

                    PdfLayer l1 = new PdfLayer("Uwagi", writer);
                    cb.beginLayer(l1);
                    Phrase p = new Phrase(layer, font);
                    ColumnText ct = new ColumnText(cb);
                    ct.setSimpleColumn(p, 20, 20, pw-20, ph-20, 15, Element.ALIGN_LEFT);
                    ct.go();
                    cb.endLayer();
                }
                
            }
        }
        
        // dodajemy warstwe z uwagami
        if (layer != null && getRemarksOnSeperatePage())
        {
            if (pdfDoc == null)
            {
                pdfDoc = new com.lowagie.text.Document(PageSize.A4);
                writer = PdfWriter.getInstance(pdfDoc, os);
                if (keywords != null)
                    pdfDoc.addKeywords(keywords);
                pdfDoc.open();
                cb = writer.getDirectContent();
            }
            else
            {
                pdfDoc.newPage();
            }
            
            float pw = pdfDoc.getPageSize().width();
            float ph = pdfDoc.getPageSize().height();

            PdfLayer l1 = new PdfLayer("Uwagi", writer);
            cb.beginLayer(l1);
            Phrase p = new Phrase(layer, font);
            ColumnText ct = new ColumnText(cb);
            ct.setSimpleColumn(p, 20, 20, pw-20, ph-20, 15, Element.ALIGN_LEFT);
            ct.go();
            cb.endLayer();

            //layerAdded = true;
        }
        
        // dodajemy podany tekst (o ile ma byc to dodane na koncu)
        if (appendMessage != null && !messageFirst)
        {
            if (pdfDoc == null)
            {
                pdfDoc = new com.lowagie.text.Document(PageSize.A4);
                writer = PdfWriter.getInstance(pdfDoc, os);
                if (keywords != null)
                    pdfDoc.addKeywords(keywords);
                pdfDoc.open();
            }
            else
            {
                pdfDoc.newPage();
            }
            pdfDoc.add(new Phrase(appendMessage, font));
        }
        
        // dodajemy koniec
        if (ending != null)
        {
            if (pdfDoc == null)
            {
                pdfDoc = new com.lowagie.text.Document(PageSize.A4);
                writer = PdfWriter.getInstance(pdfDoc, os);
                if (keywords != null)
                    pdfDoc.addKeywords(keywords);
                pdfDoc.open();
                cb = writer.getDirectContent();
            }
            pdfDoc.add(ending);
        }

        if (pdfDoc != null)
        {

            pdfDoc.close();
            writer.close();
        }
    }
	
	public static void streamsAsPdf(List<InputStream> streams, String layer,
			String appendMessage, boolean messageFirst, OutputStream os,
			Element beginning, String keywords, Element ending)
			throws IOException, DocumentException, EdmException  {
		streamsAsPdf(streams, layer, appendMessage, messageFirst, os, beginning, keywords, ending, null);
	
	}
	
	public static void streamsAsPdf(List<InputStream> streams, String layer, HashMap<String,Table> table,
			String appendMessage, boolean messageFirst, OutputStream os,
			Element beginning, String keywords, Element ending,String layerOnAllPages, Document document ) throws IOException, DocumentException, EdmException 
	{
		
		
		PdfLayerParams layerParams = null;
		if(document != null && AvailabilityManager.isAvailable("addLayerFromLogic"))
		{
			layerParams = document.getDocumentKind().logic().getPdfLayerParams(document);
		}
		
		com.lowagie.text.Document pdfDoc = null;
		PdfWriter writer = null;
		PdfContentByte cb = null;

		File fontDir = new File(Docusafe.getHome(), "fonts");
		File arial = new File(fontDir, "arial.ttf");
		BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		Font font = new Font(baseFont, 12);
		font.setColor(0, 0, 0);

		// dodajemy poczatek
		if (beginning != null) {
			if (pdfDoc == null) {
				pdfDoc = new com.lowagie.text.Document(PageSize.A4);
				writer = PdfWriter.getInstance(pdfDoc, os);
				if (keywords != null)
					pdfDoc.addKeywords(keywords);
				pdfDoc.open();
				cb = writer.getDirectContent();
			}
			pdfDoc.add(beginning);
		}

		// dodajemy podany tekst (o ile ma byc to dodane na poczatku)
		if (appendMessage != null && messageFirst) {
			if (pdfDoc == null) {
				pdfDoc = new com.lowagie.text.Document(PageSize.A4);
				writer = PdfWriter.getInstance(pdfDoc, os);
				if (keywords != null)
					pdfDoc.addKeywords(keywords);
				pdfDoc.open();
				cb = writer.getDirectContent();
			} else {
				pdfDoc.newPage();
			}
			pdfDoc.add(new Phrase(appendMessage, font));
		}

		// iterujemy po poszczegolnych plikach i dodajemy
		for(InputStream is: streams)
		{
			PdfReader reader = new PdfReader(is);

			for (int j = 0; j < reader.getNumberOfPages(); j++) 
			{
				int pageNumber = j + 1;

				Rectangle pageSize = reader
						.getPageSizeWithRotation(pageNumber);
				if (pdfDoc == null) {
					pdfDoc = new com.lowagie.text.Document(pageSize);
					writer = PdfWriter.getInstance(pdfDoc, os);
					if (keywords != null)
						pdfDoc.addKeywords(keywords);
					pdfDoc.open();
					cb = writer.getDirectContent();
				} else {
					pdfDoc.setPageSize(pageSize);
					pdfDoc.newPage();
				}

				PdfImportedPage page = writer.getImportedPage(reader,
						pageNumber);
				int rotation = reader.getPageRotation(pageNumber);
				if (rotation == 90 || rotation == 270) {
					cb.addTemplate(page, 0, -1f, 1f, 0, 0, reader
							.getPageSizeWithRotation(pageNumber)
							.height());
				} else {
					cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
				}
				if(layerOnAllPages != null)
				{

					float pw = pdfDoc.getPageSize().width();
					float ph = pdfDoc.getPageSize().height();
					PdfLayer l1 = new PdfLayer("Znak wodny", writer);
					l1.setView(true);
					l1.setOn(true);
					l1.setOnPanel(false);
					cb.beginLayer(l1);
					Phrase p = new Phrase(layerOnAllPages, font);
					ColumnText ct = new ColumnText(cb);
					ct.setSimpleColumn(p, 10,0,pw,30, 15,
							Element.ALIGN_LEFT);
					ct.go();
					cb.endLayer();
				}
				if(layerParams != null)
				{
					float pw = pdfDoc.getPageSize().width();
					float ph = pdfDoc.getPageSize().height();
					PdfLayer l1 = new PdfLayer(layerParams.getLlname(), writer);
					l1.setView(true);
					l1.setOn(true);
					l1.setOnPanel(false);
					cb.beginLayer(l1);
					Font llfont = new Font(baseFont, layerParams.getLlSize());
					llfont.setColor(200, 0, 0);
					llfont.setStyle(Font.BOLD);
					Phrase p = new Phrase(layerParams.getLltext(),llfont);
					ColumnText.showTextAligned(cb, PdfContentByte.ALIGN_LEFT, p,layerParams.getLlx(), ph-layerParams.getLly(), 
							layerParams.getLlrotation(), PdfWriter.RUN_DIRECTION_RTL, 0);
				}
			}
		

			if (!getRemarksOnSeperatePage()) 
			{
				float pw = pdfDoc.getPageSize().width();
				float ph = pdfDoc.getPageSize().height();
	
				PdfLayer l1 = new PdfLayer("Uwagi", writer);
				cb.beginLayer(l1);
				Phrase p = new Phrase(layer, font);
				ColumnText ct = new ColumnText(cb);
				ct.setSimpleColumn(p, 20, 20, pw - 20, ph - 20, 15,
						Element.ALIGN_LEFT);
				ct.go();
				cb.endLayer();
			}

		}

		// dodajemy warstwe z uwagami
		if (layer != null && getRemarksOnSeperatePage()) 
		{
			if (pdfDoc == null) 
			{
				pdfDoc = new com.lowagie.text.Document(PageSize.A4);
				writer = PdfWriter.getInstance(pdfDoc, os);
				if (keywords != null)
					pdfDoc.addKeywords(keywords);
				pdfDoc.open();
				cb = writer.getDirectContent();
			} 
			else 
			{
				pdfDoc.newPage();
			}

			float pw = pdfDoc.getPageSize().width();
			float ph = pdfDoc.getPageSize().height();

			PdfLayer l1 = new PdfLayer("Uwagi", writer);
			cb.beginLayer(l1);
			Phrase p = new Phrase(layer, font);
			ColumnText ct = new ColumnText(cb);
			ct.setSimpleColumn(p, 20, 20, pw - 20, ph - 20, 15,
					Element.ALIGN_LEFT);
			ct.go();
			cb.endLayer();

			// layerAdded = true;
		}

		// dodajemy tabele z uwagami, historia dekretacji oraz historia pisma
		if(!table.isEmpty()){
			if (pdfDoc == null) 
			{
				pdfDoc = new com.lowagie.text.Document(PageSize.A4);
				writer = PdfWriter.getInstance(pdfDoc, os);
				if (keywords != null)
					pdfDoc.addKeywords(keywords);
				pdfDoc.open();
				cb = writer.getDirectContent();
			} 
			else 
			{
				pdfDoc.newPage();
			}

			float pw = pdfDoc.getPageSize().width();
			float ph = pdfDoc.getPageSize().height();

			PdfLayer l1 = new PdfLayer("Uwagi", writer);
			cb.beginLayer(l1);
			Phrase p = new Phrase(layer, font);
			ColumnText ct = new ColumnText(cb);
			ct.setSimpleColumn(p, 20, 20, pw - 20, ph - 20, 15,
					Element.ALIGN_CENTER);
			ct.go();
			
			for(String key: table.keySet()){
				pdfDoc.add(table.get(key));
				pdfDoc.add(new Phrase("\n\n"));
			}
			cb.endLayer();
		}
		
		
		// dodajemy podany tekst (o ile ma byc to dodane na koncu)
		if (appendMessage != null && !messageFirst) 
		{
			if (pdfDoc == null) 
			{
				pdfDoc = new com.lowagie.text.Document(PageSize.A4);
				writer = PdfWriter.getInstance(pdfDoc, os);
				if (keywords != null)
					pdfDoc.addKeywords(keywords);
				pdfDoc.open();
			} 
			else 
			{
				pdfDoc.newPage();
			}
			pdfDoc.add(new Phrase(appendMessage, font));
		}

		// dodajemy koniec
		if (ending != null)
		{
			if (pdfDoc == null) 
			{
				pdfDoc = new com.lowagie.text.Document(PageSize.A4);
				writer = PdfWriter.getInstance(pdfDoc, os);
				if (keywords != null)
					pdfDoc.addKeywords(keywords);
				pdfDoc.open();
				cb = writer.getDirectContent();
			}
			pdfDoc.add(ending);
		}

		if (pdfDoc != null) {

			pdfDoc.close();
			writer.close();
		}
	}
	
	public static void streamsAsPdf(List<InputStream> streams, String layer,
			String appendMessage, boolean messageFirst, OutputStream os,
			Element beginning, String keywords, Element ending,String layerOnAllPages ) throws IOException, DocumentException, EdmException 
	{
		streamsAsPdf(streams, layerOnAllPages, appendMessage, messageFirst, os, beginning, keywords, ending, layerOnAllPages, null);
	}
	
	public static void streamsAsPdf(List<InputStream> streams, String layer,
			String appendMessage, boolean messageFirst, OutputStream os,
			Element beginning, String keywords, Element ending,String layerOnAllPages, Document document )
			throws IOException, DocumentException, EdmException {
		
		
		PdfLayerParams layerParams = null;
		if(document != null && AvailabilityManager.isAvailable("addLayerFromLogic"))
		{
			layerParams = document.getDocumentKind().logic().getPdfLayerParams(document);
		}
		
		com.lowagie.text.Document pdfDoc = null;
		PdfWriter writer = null;
		PdfContentByte cb = null;

		File fontDir = new File(Docusafe.getHome(), "fonts");
		File arial = new File(fontDir, "arial.ttf");
		BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		Font font = new Font(baseFont, 12);
		font.setColor(0, 0, 0);

		// dodajemy poczatek
		if (beginning != null) {
			if (pdfDoc == null) {
				pdfDoc = new com.lowagie.text.Document(PageSize.A4);
				writer = PdfWriter.getInstance(pdfDoc, os);
				if (keywords != null)
					pdfDoc.addKeywords(keywords);
				pdfDoc.open();
				cb = writer.getDirectContent();
			}
			pdfDoc.add(beginning);
		}

		// dodajemy podany tekst (o ile ma byc to dodane na poczatku)
		if (appendMessage != null && messageFirst) {
			if (pdfDoc == null) {
				pdfDoc = new com.lowagie.text.Document(PageSize.A4);
				writer = PdfWriter.getInstance(pdfDoc, os);
				if (keywords != null)
					pdfDoc.addKeywords(keywords);
				pdfDoc.open();
				cb = writer.getDirectContent();
			} else {
				pdfDoc.newPage();
			}
			pdfDoc.add(new Phrase(appendMessage, font));
		}

		// iterujemy po poszczegolnych plikach i dodajemy
		for(InputStream is: streams)
		{
			PdfReader reader = new PdfReader(is);

			for (int j = 0; j < reader.getNumberOfPages(); j++) 
			{
				int pageNumber = j + 1;

				Rectangle pageSize = reader
						.getPageSizeWithRotation(pageNumber);
				if (pdfDoc == null) {
					pdfDoc = new com.lowagie.text.Document(pageSize);
					writer = PdfWriter.getInstance(pdfDoc, os);
					if (keywords != null)
						pdfDoc.addKeywords(keywords);
					pdfDoc.open();
					cb = writer.getDirectContent();
				} else {
					pdfDoc.setPageSize(pageSize);
					pdfDoc.newPage();
				}

				PdfImportedPage page = writer.getImportedPage(reader,
						pageNumber);
				int rotation = reader.getPageRotation(pageNumber);
				if (rotation == 90 || rotation == 270) {
					cb.addTemplate(page, 0, -1f, 1f, 0, 0, reader
							.getPageSizeWithRotation(pageNumber)
							.height());
				} else {
					cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
				}
				if(layerOnAllPages != null)
				{

					float pw = pdfDoc.getPageSize().width();
					float ph = pdfDoc.getPageSize().height();
					PdfLayer l1 = new PdfLayer("Znak wodny", writer);
					l1.setView(true);
					l1.setOn(true);
					l1.setOnPanel(false);
					cb.beginLayer(l1);
					Phrase p = new Phrase(layerOnAllPages, font);
					ColumnText ct = new ColumnText(cb);
					ct.setSimpleColumn(p, 10,0,pw,30, 15,
							Element.ALIGN_LEFT);
					ct.go();
					cb.endLayer();
				}
				if(layerParams != null)
				{
					float pw = pdfDoc.getPageSize().width();
					float ph = pdfDoc.getPageSize().height();
					PdfLayer l1 = new PdfLayer(layerParams.getLlname(), writer);
					l1.setView(true);
					l1.setOn(true);
					l1.setOnPanel(false);
					cb.beginLayer(l1);
					Font llfont = new Font(baseFont, layerParams.getLlSize());
					llfont.setColor(200, 0, 0);
					llfont.setStyle(Font.BOLD);
					Phrase p = new Phrase(layerParams.getLltext(),llfont);
					ColumnText.showTextAligned(cb, PdfContentByte.ALIGN_LEFT, p,layerParams.getLlx(), ph-layerParams.getLly(), 
							layerParams.getLlrotation(), PdfWriter.RUN_DIRECTION_RTL, 0);
				}
			}
		

			if (!getRemarksOnSeperatePage()) 
			{
				float pw = pdfDoc.getPageSize().width();
				float ph = pdfDoc.getPageSize().height();
	
				PdfLayer l1 = new PdfLayer("Uwagi", writer);
				cb.beginLayer(l1);
				Phrase p = new Phrase(layer, font);
				ColumnText ct = new ColumnText(cb);
				ct.setSimpleColumn(p, 20, 20, pw - 20, ph - 20, 15,
						Element.ALIGN_LEFT);
				ct.go();
				cb.endLayer();
			}

		}

		// dodajemy warstwe z uwagami
		if (layer != null && getRemarksOnSeperatePage()) 
		{
			if (pdfDoc == null) 
			{
				pdfDoc = new com.lowagie.text.Document(PageSize.A4);
				writer = PdfWriter.getInstance(pdfDoc, os);
				if (keywords != null)
					pdfDoc.addKeywords(keywords);
				pdfDoc.open();
				cb = writer.getDirectContent();
			} 
			else 
			{
				pdfDoc.newPage();
			}

			float pw = pdfDoc.getPageSize().width();
			float ph = pdfDoc.getPageSize().height();

			PdfLayer l1 = new PdfLayer("Uwagi", writer);
			cb.beginLayer(l1);
			Phrase p = new Phrase(layer, font);
			ColumnText ct = new ColumnText(cb);
			ct.setSimpleColumn(p, 20, 20, pw - 20, ph - 20, 15,
					Element.ALIGN_LEFT);
			ct.go();
			cb.endLayer();

			// layerAdded = true;
		}

		// dodajemy podany tekst (o ile ma byc to dodane na koncu)
		if (appendMessage != null && !messageFirst) 
		{
			if (pdfDoc == null) 
			{
				pdfDoc = new com.lowagie.text.Document(PageSize.A4);
				writer = PdfWriter.getInstance(pdfDoc, os);
				if (keywords != null)
					pdfDoc.addKeywords(keywords);
				pdfDoc.open();
			} 
			else 
			{
				pdfDoc.newPage();
			}
			pdfDoc.add(new Phrase(appendMessage, font));
		}

		// dodajemy koniec
		if (ending != null)
		{
			if (pdfDoc == null) 
			{
				pdfDoc = new com.lowagie.text.Document(PageSize.A4);
				writer = PdfWriter.getInstance(pdfDoc, os);
				if (keywords != null)
					pdfDoc.addKeywords(keywords);
				pdfDoc.open();
				cb = writer.getDirectContent();
			}
			pdfDoc.add(ending);
		}

		if (pdfDoc != null) {

			pdfDoc.close();
			writer.close();
		}
	}
	
    
    private static boolean isPdf(File file)
    {
        return file.getName().toLowerCase().endsWith(".pdf");
    }   

    private static int getImagePages(File file)
        throws IOException
    {
        if (file.getName().toLowerCase().endsWith(".tif") ||
            file.getName().toLowerCase().endsWith(".tiff"))
        {
            RandomAccessFileOrArray raf = new RandomAccessFileOrArray(file.getAbsolutePath());
            return TiffImage.getNumberOfPages(raf);
        }

        return 1;
    }

    public static Image getImage(File file, int page)
        throws IOException, BadElementException
    {
        if (file.getName().toLowerCase().endsWith(".bmp"))
        {
            return BmpImage.getImage(file.getAbsolutePath());
        }
        else if (file.getName().toLowerCase().endsWith(".gif") ||
            file.getName().toLowerCase().endsWith(".jpg") ||
            file.getName().toLowerCase().endsWith(".png"))
        {
            return Image.getInstance(file.getAbsolutePath());
        }
        else if (file.getName().toLowerCase().endsWith(".tif") ||
            file.getName().toLowerCase().endsWith(".tiff"))
        {
            RandomAccessFileOrArray raf = new RandomAccessFileOrArray(file.getAbsolutePath());
            return TiffImage.getTiffImage(raf, page+1);
        }

        throw new IOException("Nieznany rodzaj pliku graficznego");
    }
    
    private static boolean getRemarksOnSeperatePage()
    {
       return DSApi.context().userPreferences().node("other").getBoolean("remarksOnSeperatePage", true);
    }
}
