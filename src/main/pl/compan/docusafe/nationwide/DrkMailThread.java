package pl.compan.docusafe.nationwide;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;

public class DrkMailThread implements Runnable 
{

	private static final Logger log = LoggerFactory.getLogger(DrkMailThread.class);
	
	private Long eventId;
	private Long flagId;
	private Long documentId;
	private String nrPolisy;
	
	public DrkMailThread(Long event_id, Long flag_id, Long document_id, String nr_polisy) 
	{
		this.eventId = event_id;
		this.flagId = flag_id;
		this.documentId = document_id;
		this.nrPolisy = nr_polisy;
	}
	
	public void run() 
	{
		//mapa wartosci dla tresci listu
		Map<String, Object> ctext = new HashMap<String, Object>();
		ctext.put("nr_polisy", this.nrPolisy);
		ctext.put("document_id", this.documentId);
		ctext.put("link", Docusafe.getBaseUrl());
		
		String guid = null;
		DSDivision division = null;
		
		try
		{
			DSApi.openAdmin();
			guid = DSApi.context().systemPreferences().node("drk-mail").get(this.flagId.toString(), null);
			division = DSDivision.find(guid);
			for(DSUser user : division.getUsers())
			{				
				if(user.getEmail() != null)
					((Mailer) ServiceManager.getService(Mailer.NAME)).send(user.getEmail(), user.getEmail(), null, Configuration.getMail(Mail.NEW_DRK_MAIL), ctext);					
			}
			
		}
		catch(Exception e)
		{
			log.error("",e);
		}
		finally
		{
			DSApi._close();
		}
	}

}
