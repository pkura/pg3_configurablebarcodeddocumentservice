package pl.compan.docusafe.nationwide;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.annotations.Unchecked;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.tiff.ImageKit;

@Unchecked
public class icrThread implements Runnable {

	private static final Log log = LogFactory.getLog(icrThread.class);
	private Document document;
	private Attachment attachment;
	
	private Long documentId;
	private Long attachmentId;
	
	public icrThread(Long documentId, Long attachmentId)
	{
		this.documentId = documentId;
		this.attachmentId = attachmentId;
	}
	
	public icrThread(Document document) throws EdmException
	{
		this.document = document;
		List<Attachment> ats = this.document.getAttachments();
        Collections.sort(ats, new Attachment.AttacgnebtComparatorByDate());
        this.attachment = ats.get(ats.size()-1);
		/*for(Attachment at : ats)
        {
        	if("Skan".equalsIgnoreCase(at.getTitle()))	
        	{		                        	
        		this.attachment = at;
        	}
        }*/
	}
	
	/**
	 * Metoda sprawdzaj�ca czy dla danego dokumentu powinnismy pociac zalacznik realizowana przez zapytanie sql dla zachowania wydajnosci
	 * @param document
	 * @return
	 * @throws Exception
	 */
	private static Boolean shuldCut(Document document) throws Exception
	{
		if(document.getDocumentKind().getDockindInfo().getIcrCutterCut() == null)
		{
			return true;
		}
		
		Boolean retValue = new Boolean(false);
		List<String> cutList = new ArrayList<String>(document.getDocumentKind().getDockindInfo().getIcrCutterCut());
		
		for(String value : cutList)
		{
			PreparedStatement ps = null;
			try
			{
				ps = DSApi.context().prepareStatement("SELECT COUNT(DOCUMENT_ID) AS count from "+document.getDocumentKind().getTablename()+" where "+document.getDocumentKind().getFieldByCn(value.split(":")[0]).getColumn()+" = ? and DOCUMENT_ID = ?");
				ps.setString(1, value.split(":")[1]);
				ps.setLong(2, document.getId());
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					if(rs.getInt("count") > 0)
					{
						retValue = true;
					}
				}
				rs.close();
				DSApi.context().closeStatement(ps);
				ps = null;
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			finally
			{
				DSApi.context().closeStatement(ps);
			}
		}
		return retValue;
	}
	
	public void run() {

		try 
		{
			DSApi.openAdmin();
			DSApi.context().begin();
			
			if(this.document == null)
			{
				this.document = Document.find(this.documentId);
				this.attachment = this.document.getAttachment(attachmentId);
			}
			
			if(!shuldCut(document))
			{
				return;
			}
			
			Attachment lastAttachment = this.attachment;
			
			if(!"image/tiff".equalsIgnoreCase(lastAttachment.getMostRecentRevision().getMime()))
				return;
			
			String originalFileName = lastAttachment.getMostRecentRevision().getOriginalFilename();
			
			
			
			File tempFile = File.createTempFile("icrThread", "tiff");
			tempFile.deleteOnExit();
			
			
			while(lastAttachment.getMostRecentRevision().getBinaryStream() == null)
			{
				Thread.sleep(5);
			}
			InputStream biss = lastAttachment.getMostRecentRevision().getBinaryStream();
			byte bufor[ ] = new byte[16536];
	        BufferedOutputStream outs =  new BufferedOutputStream(new FileOutputStream(tempFile),bufor.length);
	        int leng;
	        while ((leng = biss.read(bufor)) > 0)
	        {
	            outs.write(bufor, 0, leng);
	        }
	        outs.flush();
	        outs.close();
	        org.apache.commons.io.IOUtils.closeQuietly(outs);
	        org.apache.commons.io.IOUtils.closeQuietly(biss);
	        tempFile = ImageKit.tiffA3ToA4(tempFile);
			
	        AttachmentRevision ar = lastAttachment.createRevision(tempFile);
			ar.setOriginalFilename(originalFileName);
			ar.setMime("image/tiff");
			
			DSApi.context().commit();
			
		} 
		catch (Exception e) 
		{
			DSApi.context().setRollbackOnly();
			//log.error(e.getMessage(),new Exception(e.getMessage(),e.getCause()));
			log.error(new EdmException(e));
		}
		finally
		{
			DSApi._close();
		}
				
	}

}
