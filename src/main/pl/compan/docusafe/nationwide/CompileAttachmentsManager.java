package pl.compan.docusafe.nationwide;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.TextUtils;

/**
 * Mened�er udost�pniaj�cy funkcje zwi�zane z kompilacj� za��cznik�w (dla Nationwide w dokumentach biznesowych).
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class CompileAttachmentsManager
{
    private static final Log log = LogFactory.getLog(CompileAttachmentsManager.class);

    private static final String COMPILED_FILE_NAME = "kompilacja.pdf";

    /**
     * Sprawdza, czy w tym momencie zalogowany u�ytkownik mo�e wykona� kompilacj� za��cznik�w
     * dla podanego dokumentu.
     *
     * @document
     * @param fromSummary true oznacza, �e kompilacja ma zosta� dokonana z podsumowania; false oznacza ze z archiwum
     */
    public static CompilationStatus checkCompilationStatus(Document document/*, boolean fromSummary*/) throws EdmException
    {
        CompilationStatus status = new CompilationStatus();
        if (document.getDocumentKind() == null)
            return status;

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

        if (!hasPermissionToCompile(document/*, fromSummary*/))
            return status;

        return status;

    }

    /**
     * Sprawdza czy by�a ju� wcze�niej dokonywana kompilacja na tym dokumencie.
     */
    //private static boolean compilationDone(Document document) throws EdmException
    {
     //   return (attachment != null);
    }

    /**
     * Przekazuje list� identyfikator�w dokument�w o takim samym numerze sprawy co 'document', a kt�re si�
     * pojawi�y ju� po dokonaniu kompilacji dla dokumentu 'document'.
     * W zwi�zku z tym zak�ada, �e kompilacja by�a ju� wcze�niej dokonywana.
     */
    private static Map<Long,String> getNewDocumentIds(Document document, FieldsManager fm) throws EdmException
    {
        List<CompiledAttachmentEntry> entries = CompiledAttachmentEntry.find(document.getId());
        List<Long> oldDocs = new ArrayList<Long>();
        for (CompiledAttachmentEntry entry : entries)
            oldDocs.add(entry.getCompiledDocId());
        Map<Long,String> newDocs = new LinkedHashMap<Long,String>();
        DockindQuery dockindQuery = new DockindQuery(0, 0);
        dockindQuery.setDocumentKind(document.getDocumentKind());
        dockindQuery.orderAsc("ctime");
        SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);

        return newDocs;
    }

    /**
     * Przekazuje list� dokument�w o takim samym numerze sprawy co 'document', a kt�re si�
     * pojawi�y ju� po dokonaniu kompilacji dla dokumentu 'document'.
     * W zwi�zku z tym zak�ada, �e kompilacja by�a ju� wcze�niej dokonywana.
     */
    private static Map<Document,String> getNewDocuments(Document document, FieldsManager fm) throws EdmException
    {
        List<CompiledAttachmentEntry> entries = CompiledAttachmentEntry.find(document.getId());
        List<Long> oldDocs = new ArrayList<Long>();
        for (CompiledAttachmentEntry entry : entries)
            oldDocs.add(entry.getCompiledDocId());
        Map<Document,String> newDocs = new LinkedHashMap<Document,String>();
        DockindQuery dockindQuery = new DockindQuery(0, 0);
        dockindQuery.setDocumentKind(document.getDocumentKind());
        dockindQuery.orderAsc("ctime");
        SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);

        return newDocs;
    }

    /**
     * Sprawdza, czy aktualnie zalogowany u�ytkownik ma prawo kompilowa� ten dokument.
     *
     * @param document
     * @param fromSummary true oznacza, �e kompilacja ma zosta� dokonana z podsumowania; false oznacza ze z archiwum
     */
    private static boolean hasPermissionToCompile(Document document/*, boolean fromSummary*/) throws EdmException
    {
        // admin mo�e zawsze
        if (DSApi.context().isAdmin())
            return true;

        TaskSnapshot.Query query = new TaskSnapshot.Query(0, 1);
        query.setDocumentId(document.getId());
        query.setUser(DSApi.context().getDSUser());

        SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
        if (results.totalCount() > 0)
           // posiadam to pismo na swojej li�cie zada� - mog� kompilowa� zawsze
            return true;
     /*   if (!fromSummary)
        {*/
            // kompilacja z poziomu archiwum bez posiadania odpowiedniego zadania -
            // mo�liwa tylko, gdy nikt inny te� nie ma zadania w �adnym trybie dla tego dokumentu
            TaskSnapshot.Query query2 = new TaskSnapshot.Query(0, 1);
            query2.setDocumentId(document.getId());

            SearchResults<TaskSnapshot> results2 = TaskSnapshot.getInstance().search(query2);
            return (results2.totalCount() == 0);
    /*    }
        return false;*/
    }

    /**
     * Wykonuje kompilacje wszystkich za��cznik�w z dokument�w o tym samym atrybucie "numer sprawy".
     * W zale�no�ci od sytuacji mo�e to by� ca�kowita kompilacja od nowa lub dokompilowanie nowych
     * za��cznik�w do istniej�cego ju� "za��cznika pokompilacyjnego".
     *
     * @param document
     * @param fromSummary true oznacza, �e kompilacja ma zosta� dokonana z podsumowania; false oznacza ze z archiwum
     * @throws EdmException
     */
    public static void compile(Document document/*, boolean fromSummary*/) throws EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        if (!hasPermissionToCompile(document/*, fromSummary*/))
            throw new EdmException("Nie masz uprawnienia do dokonania kompilacji plik�w sprawy dla tego dokumentu");
        else
        {
            Map<Document,String> newDocs = getNewDocuments(document, fm);
            if (newDocs != null && newDocs.size() > 0)
                recompile(document, fm, newDocs);
            else
                throw new EdmException("Dla tego dokumentu by�a ju� wykonana kompilacja plik�w sprawy");
        }
    }

    /**
     * Wykonuje kompilacje wszystkich za��cznik�w z dokument�w o tym samym atrybucie "numer sprawy",
     * ��cznie z za��cznikiem w podanym dokumencie do jednego pliku do nowego za��cznika, kt�ry nadpisze
     * �r�d�owy plik w tym dokumencie. Musi to by� dokument {@link DocumentLogicLoader#NATIONWIDE_KIND}, kt�rego
     * typ dokumentu to "dyspozycja wyp�aty".
     *
     * @param document
     * @return
     * @throws EdmException
     */
    public static void compile(Document document, FieldsManager fm) throws EdmException
    {
        // wyszukiwanie dokument�w
        DocumentKind documentKind = document.getDocumentKind();

        DockindQuery dockindQuery = new DockindQuery(0, 0);
        dockindQuery.setDocumentKind(documentKind);
        dockindQuery.orderAsc("ctime");
        SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);

        // pobranie kolejnosci typow dokumentow do kompilacji
        String compileOrder = GlobalPreferences.getCompileAttachmentsOrder();
        Map<Integer,Integer> types = new LinkedHashMap<Integer,Integer>();
        int count = 0;
        if (compileOrder != null)
        {
            String[] chosenTypes = compileOrder.split(";");
            for (String type : chosenTypes)
            {
                try
                {
                    count++;
                    types.put(Integer.parseInt(type), count);
                }
                catch (NumberFormatException e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }

        // sortowanie wyszukanych dokument�w
        List<Document> results = Arrays.asList(searchResults.results());
        DocumentComparator documentComparator = new DocumentComparator(types, documentKind);
        try
        {
            // gdy nie wybrano �adnych ustawie� dla kolejno�ci to sortowanie jest zb�dne
            // bo ju� podczas wyszukania by�o posortowane po ctime
            if (types.size() > 0)
                Collections.sort(results, documentComparator);
        }
        catch (RuntimeException e)
        {
            // komparator w przypadku b��du EdmException rzuca RuntimeException bo nie ma innej mozliwosci
            // w zwiazku z tym przechwytuje tutaj ten wyjatek i rzuczam juz normalnie EdmException
            throw new EdmException(e.getMessage(), e);
        }

        // kompilacja za��cznik�w z wyszukanych dokument�w
        File tmp = null;
        try
        {
            tmp = File.createTempFile("docusafe_", ".pdf");

            OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));
            PdfUtils.attachmentsAsPdf(results.toArray(new Document[]{}), false, os, true);
            os.close();
        }
        catch (Exception e)
        {
            throw new EdmException("B��d podczas tworzenia pliku PDF z za�acznik�w", e);
        }

        if (tmp == null)
            throw new EdmException("B��d podczas tworzenia pliku PDF z za�acznik�w");

        count = 0;
        for (Document doc : results)
        {
            CompiledAttachmentEntry entry = new CompiledAttachmentEntry();
            entry.setDocumentId(document.getId());
            entry.setCompiledDocId(doc.getId());
            entry.setPos(count);
            Persister.create(entry);
            count++;
        }
        // pozosta�e rzeczy
        commonCompileWork(document, fm, tmp);
    }

    /**
     * Wykonuje powt�rn� kompilacje za��cznik�w z dokument�w o tym samym atrybucie "numer sprawy", kt�ra
     * polega na dokompilowaniu na koniec wcze�niej utworzonego pliku za��cznik�w z nowych dokument�w
     * (czyli takich kt�re pojawi�y si� po wcze�niejszej kompilacji).
     *
     * Powinno by� wykonywane tylko w przypadku gdy istniej� takie "nowe" dokumenty.
     *
     * @param document
     * @param fm
     * @param newDocuments informacja o dokumentach, kt�re trzeba dokompilowa� do poprzedniego wyniku kompilacji
     * @return
     * @throws EdmException
     */
    public static void recompile(Document document, FieldsManager fm, Map<Document,String> newDocuments) throws EdmException
    {
        // tworzenie listy dokument�w, kt�re wezm� udzia� w tworzeniu PDF-a
        List<Long> documents = new ArrayList<Long>();
        documents.add(document.getId());
        for (Document doc : newDocuments.keySet())
            documents.add(doc.getId());


        // kompilacja za��cznik�w z wyszukanych dokument�w
        File tmp = null;
        try
        {
            tmp = File.createTempFile("docusafe_", ".pdf");

            OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));
            PdfUtils.attachmentsAsPdf(documents.toArray(new Long[]{}), false, os, null, null, null, true);
            os.close();
        }
        catch (Exception e)
        {
            throw new EdmException("B��d podczas tworzenia pliku PDF z za�acznik�w", e);
        }

        if (tmp == null)
            throw new EdmException("B��d podczas tworzenia pliku PDF z za�acznik�w");

        int curPos = CompiledAttachmentEntry.getMaxPos(document.getId());
        for (Document newDoc : newDocuments.keySet())
        {
            curPos++;
            CompiledAttachmentEntry entry = new CompiledAttachmentEntry();
            entry.setDocumentId(document.getId());
            entry.setCompiledDocId(newDoc.getId());
            entry.setPos(curPos);
            Persister.create(entry);
        }

        // przenoszenie uwag
        if (document instanceof OfficeDocument)
        {
            OfficeDocument officeDocument = (OfficeDocument) document;
            for (Document doc : newDocuments.keySet())
            {
                copyRemarks(newDocuments.get(doc), doc, officeDocument);
            }
        }

        // pozosta�e rzeczy
        commonCompileWork(document, fm, tmp);
    }

    /**
     * Wykonuje kompilacje za��cznik�w z wybranych dokument�w przez danego u�ytkownika i tworzy nowe
     * pismo wewn�trzne o podanym typie (czyli warto�ci pola RODZAJ_FIELD_CN) zawieraj�ce "za��cznik pokompilacyjny".
     *
     * @param username   u�ytkownik, dla kt�rego ma zosta� wykonana kompilacja
     *
     * @return
     * @throws EdmException
     */
    public static void compileAndCreate(String username, Integer typDokumentuId) throws EdmException
    {
        // pobranie listy dokument�w, kt�re wezm� udzia� w tworzeniu PDF-a (posortowanej wed�ug priorytet�w)
        List<Long> documents = DocumentForCompilation.findIds(username);

        if (documents.size() == 0)
            throw new EdmException("Nie wybrano �adnych dokument�w");

        // kompilacja za��cznik�w z wybranych dokument�w
        File tmp = null;
        try
        {
            tmp = File.createTempFile("docusafe_", ".pdf");

            OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));
            PdfUtils.attachmentsAsPdf(documents.toArray(new Long[]{}), false, os, null, null, null, true);
            os.close();
        }
        catch (Exception e)
        {
            throw new EdmException("B��d podczas tworzenia pliku PDF z za�acznik�w", e);
        }

        if (tmp == null)
            throw new EdmException("B��d podczas tworzenia pliku PDF z za�acznik�w");

        Document document = Document.find(documents.get(0));
        DocumentKind documentKind = document.getDocumentKind();
        FieldsManager fm = documentKind.getFieldsManager(document.getId());

        // tworzenie pisma wewn�trznego
        OutOfficeDocument newDocument = new OutOfficeDocument();

        newDocument.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        newDocument.setDivisionGuid(DSDivision.ROOT_GUID);
        newDocument.setCurrentAssignmentAccepted(Boolean.FALSE);
        newDocument.setInternal(true);
        newDocument.setSummary(documentKind.getName());
        newDocument.setDocumentKind(documentKind);

        newDocument.setCreatingUser(DSApi.context().getPrincipalName());

        newDocument.setAssignedDivision(DSDivision.ROOT_GUID);
        newDocument.setClerk(DSApi.context().getPrincipalName());

        newDocument.create();

        Long newDocumentId = newDocument.getId();

        // ustalanie warto�ci p�l biznesowych
        Map<String,Object> values = new LinkedHashMap<String, Object>();
        documentKind.set(newDocument.getId(), values);

        documentKind.logic().archiveActions(newDocument, DocumentLogic.TYPE_IN_OFFICE);
        documentKind.logic().documentPermissions(newDocument);

        // tworzenie pierwszego wpisu (dla utworzonego dokumentu)
        CompiledAttachmentEntry entry = new CompiledAttachmentEntry();
        entry.setDocumentId(newDocument.getId());
        entry.setCompiledDocId(newDocument.getId());
        entry.setPos(0);
        Persister.create(entry);

        // tworzenie wpis�w dla pozosta�ych dokument�w
        int count = 1;
        for (Long docId : documents)
        {
            entry = new CompiledAttachmentEntry();
            entry.setDocumentId(newDocument.getId());
            entry.setCompiledDocId(docId);
            entry.setPos(count);
            Persister.create(entry);
            count++;
        }

        // przenoszenie uwag
        for (Long docId : documents)
        {
            Document doc = Document.find(docId);
        }

        // dodanie "za�acznika pokompilacyjnego"
        Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skompilowane pliki sprawy", 254));
        newDocument.createAttachment(attachment, true);

        attachment.createRevision(tmp).setOriginalFilename(COMPILED_FILE_NAME);

        Date entryDate = GlobalPreferences.getCurrentDay();
        Long journalId = Journal.getMainInternal().getId();

        DSApi.context().commit();
        DSApi.close();

        Integer sequenceId = Journal.TX_newEntry2(journalId, newDocumentId, entryDate);

        DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
        DSApi.context().begin();

       /* // przydzielanie pisma dziennika

        DSApi.context().commit();

        // pobieranie nowego numeru kolejnego z dziennika
        Integer sequenceId = Journal.TX_newEntry2(journalId, newDocument.getId(), entryDate, DSApi.context().session().connection());

        DSApi.context().begin();*/
        newDocument = OutOfficeDocument.findOutOfficeDocument(newDocumentId);

        newDocument.bindToJournal(journalId, sequenceId);

        // tworzenie nowego zadania
        WorkflowFactory.createNewProcess(newDocument, false);

        // po dokonaniu kompilacji usuwam zaznaczony wyb�r u�ytkownika
        DocumentForCompilation.delete(username, null);
    }

    /**
     * Dodaje wybrane dokumentu do kompilacji dla podanego u�ytkownika (w przypadku gdy pewien dokument
     * by� ju� wybrany przez tego u�ytkownika, drugi raz nie zostanie dodany).
     */
    public static void addDocumentsForCompilation(Long[] documentIds, String username) throws EdmException
    {
        List<Long> ids = Arrays.asList(documentIds);
        List<DocumentForCompilation> list = DocumentForCompilation.find(username);
        for (DocumentForCompilation docForCompilation : list)
        {
            if (ids.contains(docForCompilation.getId()))
                ids.remove(docForCompilation.getId());
        }

        for (Long id : ids)
        {
            DocumentForCompilation docForCompilation = new DocumentForCompilation();
            docForCompilation.setDocumentId(id);
            docForCompilation.setUsername(username);
            Persister.create(docForCompilation);
        }
    }

    /**
     * Usuwa podane dokumenty wybrane do kompilacji.
     */
    public static void removeDocumentsForCompilation(Long[] documentIds) throws EdmException
    {
        if (documentIds == null)
            return;

        for (Long id : documentIds)
        {
            DocumentForCompilation docForCompilation = DocumentForCompilation.find(id);
            if (docForCompilation != null)
                Persister.delete(docForCompilation);
        }

    }

    /**
     * Zapisuje nowe priorytety dokumentom.
     *
     * @param priorities mapa priorytet�w, gdzie kluczem jest ('p'+docForCompilationId), a warto�ci� priorytet
     * @param username
     * @throws EdmException
     */
    public static void updateDocumentsPriorities(Map<String,Integer> priorities, String username) throws EdmException
    {
        List<DocumentForCompilation> list = DocumentForCompilation.find(username);
        for (DocumentForCompilation docForCompilation : list)
        {
            Object obj = priorities.get("p"+docForCompilation.getId());
            Integer priority = obj != null ? Integer.parseInt(obj.toString()) : null;
            docForCompilation.setPriority(priority);
        }
    }

    /**
     * Wykonuje kompilacje za��cznik�w z podanych dokument�w i tworzy nowe pismo przychodz�ce
     * o podanym typie (czyli warto�ci pola RODZAJ_FIELD_CN) zawieraj�ce "za��cznik pokompilacyjny".
     *
     * @param docs   dokumenty, kt�rych za��czniki maj� by� skompilowane, w postaci mapy, gdzie kluczem
     * jest napis 'dX' (gdzie X to id dokumentu), a warto�ci� priorytet - im mniejszy tym wcze�niej
     * jego za��czniki pojawi� si� w wygenerowanym pliku.
     *
     * @return
     * @throws EdmException
     */
    public static void compileAndCreate(Map<String,Integer> docs, Integer typDokumentuId) throws EdmException
    {
        // tworzenie listy dokument�w, kt�re wezm� udzia� w tworzeniu PDF-a
        List<Long> documents = new ArrayList<Long>();
        for (String key : docs.keySet())
        {
            if (docs.get(key) != null)
                documents.add(Long.parseLong(key.substring(1)));
        }

        // sortowanie dokument�w wed�ug wyznaczonych przez u�ytkownika priorytet�w
        Collections.sort(documents, new PriorityComparator(docs));

        if (documents.size() == 0)
            throw new EdmException("Nie wybrano �adnych dokument�w");

        // kompilacja za��cznik�w z wybranych dokument�w
        File tmp = null;
        try
        {
            tmp = File.createTempFile("docusafe_", ".pdf");

            OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));
            PdfUtils.attachmentsAsPdf(documents.toArray(new Long[]{}), false, os, null, null, null, true);
            os.close();
        }
        catch (Exception e)
        {
            throw new EdmException("B��d podczas tworzenia pliku PDF z za�acznik�w", e);
        }

        if (tmp == null)
            throw new EdmException("B��d podczas tworzenia pliku PDF z za�acznik�w");

        Document document = Document.find(documents.get(0));
        DocumentKind documentKind = document.getDocumentKind();
        FieldsManager fm = documentKind.getFieldsManager(document.getId());

        // tworzenie pisma przychodzacego
        InOfficeDocument newDocument = new InOfficeDocument();
        newDocument.setSummary(documentKind.getName());
        newDocument.setDocumentKind(documentKind);

        // ustalenie rodzaju pisma przychodz�cego
        List<InOfficeDocumentKind> kinds = InOfficeDocumentKind.list();
        String kindName = documentKind.logic().getInOfficeDocumentKind();
        for (InOfficeDocumentKind inKind : kinds)
        {
            if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                newDocument.setKind(inKind);
        }

        newDocument.create();

        // ustalanie warto�ci p�l biznesowych
        Map<String,Object> values = new LinkedHashMap<String, Object>();
        documentKind.set(newDocument.getId(), values);

        documentKind.logic().archiveActions(newDocument, DocumentLogic.TYPE_IN_OFFICE);
        documentKind.logic().documentPermissions(newDocument);

        // tworzenie nowego zadania
        WorkflowFactory.createNewProcess(newDocument, false);


        // tworzenie pierwszego wpisu (dla utworzonego dokumentu)
        CompiledAttachmentEntry entry = new CompiledAttachmentEntry();
        entry.setDocumentId(newDocument.getId());
        entry.setCompiledDocId(newDocument.getId());
        entry.setPos(0);
        Persister.create(entry);

        // tworzenie wpis�w dla pozosta�ych dokument�w
        int count = 1;
        for (Long docId : documents)
        {
            entry = new CompiledAttachmentEntry();
            entry.setDocumentId(newDocument.getId());
            entry.setCompiledDocId(docId);
            entry.setPos(count);
            Persister.create(entry);
            count++;
        }

        // dodanie "za�acznika pokompilacyjnego"
        Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skompilowane pliki sprawy", 254));
        newDocument.createAttachment(attachment, true);

        attachment.createRevision(tmp).setOriginalFilename(COMPILED_FILE_NAME);
    }


    /**
     * Kopiuje uwagi z dokument 'sourceDoc' do dokumentu kancelaryjnego 'targetDocument' (o ile sourceDoc
     * to dokument kancelaryjny).
     *
     * @param documentKind
     * @param sourceDoc
     * @param targetDocument
     * @throws EdmException
     */
    private static void copyRemarks(String typDokumentu, Document sourceDoc, OfficeDocument targetDocument) throws EdmException
    {
        if (sourceDoc instanceof OfficeDocument && !sourceDoc.equals(targetDocument))
        {
            OfficeDocument officeDoc = (OfficeDocument) sourceDoc;
            for (Remark remark : officeDoc.getRemarks())
            {
                /*StringBuilder content = new StringBuilder("Uwaga z dokumentu \"").append(typDokumentu).
                    append("\" dodana ").append(DateUtils.formatJsDateTime(remark.getCtime())).
                    append(" przez u�ytkownika ").append(DSUser.safeToFirstnameLastname(remark.getAuthor()))
                    .append(": ").append(remark.getContent());*/
                StringBuilder content = new StringBuilder(remark.getContent()).append("; ").append(typDokumentu).
                    append("; ").append(DSUser.safeToFirstnameLastname(remark.getAuthor())).append("; ").
                    append(DateUtils.formatJsDateTime(remark.getCtime()));
                targetDocument.addRemark(new Remark(content.toString(), DSApi.context().getPrincipalName()));
            }
        }
    }

    /**
     * Wykonywanie wsp�lnych operacji dla kompilacji za��cznik�w po raz pierwszy i dokompilowywaniu
     * za��cznik�w
     */
    private static void commonCompileWork(Document document, FieldsManager fm, File tmp) throws EdmException
    {
        // usuwam wcze�niejsze za��czniki
        for (Attachment attachment : document.getAttachments())
        {
            attachment.delete(false);
        }

        // dodaje "za�acznik pokompilacyjny"
        Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skompilowane pliki sprawy", 254));
        document.createAttachment(attachment, true);

        attachment.createRevision(tmp).setOriginalFilename(COMPILED_FILE_NAME);


        Map<String,Object> fieldValues = new LinkedHashMap<String, Object>();
        document.getDocumentKind().setOnly(document.getId(), fieldValues);
    }

    /**
     * Komparator identyfikator�w (liczb Long) na podstawie priorytet�w z mapy w {@link #docs}.
     * Mapa ta jest postaci ("dXX",priorytet) gdzie XX to id dokumentu.
     *
     * U�ywane podczas kompilacji za��cznik�w do nowego dokumentu (z ekranu wyszukiwarki).
     *
     * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
     */
    public static class PriorityComparator implements Comparator<Long>
    {
        private Map<String,Integer> docs;

        public PriorityComparator(Map<String,Integer> docs)
        {
            this.docs = docs;
        }

        private Integer getPriority(Long docId)
        {
            return docs.get("d"+docId);
        }

        public int compare(Long docId1, Long docId2)
        {
            if (getPriority(docId1) < getPriority(docId2))
                return -1;
            else if (docId1.equals(docId2))
                return 0;
            else
                return 1;
        }
    }

    /**
     * Komparator dokument�w biznesowych Nationwide wed�ug typu dokumentu - priorytet typu jest
     * okre�lany poprzez map� {@link #types}, kt�rej kluczem jest id typu, a warto�ci� pewna liczba.
     * W wyniku pierw pojawi� si� dokumenty o mniejszej warto�ci tej liczby.
     *
     * Na potrzeby sortowania tworzy odpowiednie FieldsManagery, w zwi�zku z tym r�wnocze�nie
     * mo�e by� u�ywany jako generator/cache tych FieldsManager�w poprzez u�ywanie metody {@link #getFm()}.
     *
     * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
     *
     */
    public static class DocumentComparator implements Comparator<Document>
    {
        private Map<Integer,Integer> types;
        /** pomocnicza mapa, trzymaj�ca dla id danego dokumentu odpowiadaj�cy mu FieldsManager */
        private Map<Long, FieldsManager> fms;
        private DocumentKind documentKind;

        /**
         * liczba wi�ksza od priorytet�w wszystkich typ�w - przydzielane dokumentom o typach,
         * kt�rym nie przydzielono �adnej kolejno�ci.
         */
        private static final Integer MAX_PRIORITY = 10000;

        public DocumentComparator(Map<Integer,Integer> types, DocumentKind documentKind)
        {
            this.types = types;
            this.documentKind = documentKind;
            this.fms = new LinkedHashMap<Long, FieldsManager>();
        }

        /**
         * Przekazuje FieldsManagera dla danego dokumentu.
         */
        public FieldsManager getFm(Long documentId)
        {
            FieldsManager fm = fms.get(documentId);
            if (fm == null)
            {
                fm = documentKind.getFieldsManager(documentId);
                fms.put(documentId, fm);
            }
            return fm;
        }

        public int compare(Document doc1, Document doc2)
        {
                return doc1.getCtime().compareTo(doc2.getCtime());
        }
    }

    public static class CompilationStatus
    {
        /** okre�la czy aktualnie zalogowany u�ytkownik mo�e teraz dokona� kompilacji */
        private boolean canCompile;
        /** okre�la dokumenty o danym nr sprawy, kt�re pojawi�y si� od momentu ostatniej kompilacji */
        private Map<Long,String> newDocuments;

        public CompilationStatus()
        {
            this.canCompile = false;
        }

        public boolean isCanCompile()
        {
            return canCompile;
        }

        public void setCanCompile(boolean canCompile)
        {
            this.canCompile = canCompile;
        }

        public Map<Long,String> getNewDocuments()
        {
            return newDocuments;
        }

        public void setNewDocuments(Map<Long,String> newDocuments)
        {
            this.newDocuments = newDocuments;
        }
    }
}
