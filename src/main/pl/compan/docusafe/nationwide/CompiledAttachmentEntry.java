package pl.compan.docusafe.nationwide;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;

/**
 * Okre�la kolejno�� kompilowania dokument�w w danym skompilowanym za��czniku.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class CompiledAttachmentEntry
{
    private Long id;
    private Integer hversion;
    /** id dokumentu, dla kt�rego dokonano kompilacji plik�w sprawy */
    private Long documentId;
    /** id dokumentu, z kt�rego brano za��czniki do kompilacji */
    private Long compiledDocId;
    /** 
     * okre�la kolejno�� kompilacji np. gdy pos == 4 tzn, �e dokument o id compiledDocId by� kompilowany
     * jako 4
     */    
    private Integer pos;
    
    public CompiledAttachmentEntry()
    {
        
    }
    
    /**
     * Wyszukuje informacje o wszystkie dokumentach u�ytych do skompilowania plik�w sprawy dla
     * dokumentu o id 'documentId'.
     */
    @SuppressWarnings("unchecked") 
    public static List<CompiledAttachmentEntry> find(Long documentId) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(CompiledAttachmentEntry.class);
            c.add(Restrictions.eq("documentId", documentId));
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    /**
     * Przekazuje ostatni� "pozycj�" dla danego dokumentu (czyli defacto ilo�� wkompilowanych dokument�w).
     */
    @SuppressWarnings("unchecked") 
    public static Integer getMaxPos(Long documentId) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(CompiledAttachmentEntry.class);
            c.add(Restrictions.eq("documentId", documentId));
            c.setProjection(Projections.max("pos"));
            List<Integer> list = c.list();
            return list.get(0);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Usuwa wszystkie wpisy dotycz�ce skompilowanego za��cznika dla danego dokumentu.
     * 
     * @param documentId
     * @throws EdmException
     */
    public static void delete(Long documentId) throws EdmException
    {
        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("delete from "+DSApi.getTableName(CompiledAttachmentEntry.class)+" where document_id=?");
            pst.setLong(1, documentId);
            pst.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }
    
    public Long getCompiledDocId()
    {
        return compiledDocId;
    }
    
    public void setCompiledDocId(Long compiledDocId)
    {
        this.compiledDocId = compiledDocId;
    }
    
    public Long getDocumentId()
    {
        return documentId;
    }
    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }
    public Long getId()
    {
        return id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public Integer getPos()
    {
        return pos;
    }
    
    public void setPos(Integer pos)
    {
        this.pos = pos;
    }        
}
