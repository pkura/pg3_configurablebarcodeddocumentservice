package pl.compan.docusafe.reports.impl;

import java.io.File;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskListUtils;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;

public class TasklistDumpReport extends Report {

	public void doReport() throws Exception 
	{

		String username = null;
		String tab = null;
		
		for(ReportParameter rp: params)
		{
			if(rp.getFieldCn().equals("user"))
			{
				if(rp.getValue() instanceof String)
				{
					username = rp.getValueAsString();
				}
				else if (rp.getValue() instanceof String[])
				{
					username = ((String[])rp.getValue())[0];
				}
			}
			if(rp.getFieldCn().equals("tab"))
			{
				if(rp.getValue() instanceof String)
				{
					tab = rp.getValueAsString();
				}
				else if (rp.getValue() instanceof String[])
				{
					tab = ((String[])rp.getValue())[0];
				}
				
			}
			
		}
		if(tab==null)
			tab = InOfficeDocument.TYPE;
		//System.out.println(this.getUsername());
		// na razie generujemy ten raport tylko dla uzytkownika ktory go zamowil
		username = this.getUsername();
		
		if(username == null) throw new ReportException("Nie okreslono uzytkownika");
		
		
		
		DSUser user = DSUser.findByUsername(username);
		
		TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);	
		UniversalTableDumper dumper = new UniversalTableDumper(); 
	
		CsvDumper csvDumper = new CsvDumper();
		File csvFile = new File(this.getDestination(), "report.csv");			
		csvDumper.openFile(csvFile);
		dumper.addDumper(csvDumper);
		
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "report.xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
		
		//System.out.println("columns_"+tab);
		String[] properties;
		properties = DSApi.context().userPreferences(username).node("task-list").get("columns_"+tab,
				StringUtils.join(TaskListUtils.getDefaultColumnProperties(tab, null), ",")).split(",");

		     
		dumper.newLine();
		for(String s: properties)
		{
			//ystem.out.println(s);
			dumper.addText(TaskListUtils.getColumnDescription(s));
		}
		dumper.dumpLine();
		Class<Task> clazz = Task.class;
		for(Task task : taskList.getTasks(user))
		{
			dumper.newLine();
			for(String s:properties)
			{
				try
				{
					if("incomingDate".equals(s)) s = "documentDate";
					String val = String.valueOf(clazz.getMethod("get" + StringUtils.capitalize(s)).invoke(task));
					if(val.equals("null")) val = "-";
					dumper.addText(val);
				}
				catch(NoSuchMethodException e)
				{
					dumper.addText("-");
					log.error(e.getMessage(),e);
				}
			}
			dumper.dumpLine();
		}
		dumper.closeFileQuietly();
		
		
		
	}

}
