package pl.compan.docusafe.reports;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.*;

import org.apache.commons.dbutils.DbUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;

/**
 * Klasa zawierajaca polaczenie pomiedzy mechanizmem raportow, a srodowiskiem
 * @author wkutyla
 *
 */


public class ReportEnvironment
{
	static final Logger log = LoggerFactory.getLogger(ReportEnvironment.class);

	private static final String REPORTS_CONFIGURATION_FILE = "reports.xml";


	/**
	 * Metoda pobiera lokalizacje pliku dla raportu o ustalonym ID
	 * @param report
	 * @param fileName
	 * @param fileFormat
	 * @return
	 * @throws IOException
	 */
	@Deprecated
	public File getReportFile(Report report, String fileName, String fileFormat) throws IOException
	{
		return new File(Docusafe.getHome(), report.getId().toString()+"_"+fileName+"."+fileFormat);
	}

	/**
	 * Zwraca liste raportow do wyswietlenia
	 * FIXME - konfiguracja raportow powinna byc cachowana
	 * @return
	 * @throws EdmException
	 */
	public List<Map<String,String>> listReportsForDisplay() throws EdmException
	{
		log.trace("listReportsForDisplay");
		try
		{
			List<Map<String,String>> result = new ArrayList<Map<String,String>>();
			Map<String,String> bean;
			File f = new File(Docusafe.getHome(), REPORTS_CONFIGURATION_FILE);
			if (!f.exists())
				return result;
			DSUser u = DSApi.context().getDSUser();
			if (u == null)
				return result;
			log.trace("user {}",u.getName());
			Document doc = getConfiguration();
			String guid;
			String user;
			for(Element elem : (List<Element>)doc.getRootElement().elements("report"))
			{
				guid = elem.attributeValue("guid");
                                boolean isInDivision = isInDivisionUser(u,guid);
				user = elem.attributeValue("user");
				if((guid==null && user==null)|| (guid!=null && isInDivision) || (user!=null && u.getName().equals(user)))
				{
					bean = new HashMap<String, String>();
					bean.put("cn", elem.attributeValue("cn"));
					bean.put("name", elem.attributeValue("name"));
					bean.put("description", elem.getText());
					result.add(bean);
				}
			}
			return result;
		}
		catch(Exception e)
		{
			log.error("",e);
			throw new EdmException(e);
		}
	}

        /**
         * Dodaje mozliwosc defioniowania grup po przecinku
         */
        public static boolean isInDivisionUser(DSUser user, String guid) throws Exception
        {
            if(guid == null || guid.equals(""))
                return true;
            
            String[] guids = guid.split(",");
            if(guids.length == 1)
                return user.inDivisionByGuid(guid);
            else if(guids.length > 1)
            {
                for(String singleGuid : guids)
                {
                    if(user.inDivisionByGuid(singleGuid))
                        return true;
                }
            }
            
            
            return false;
        }
        
	/**
	 * Obiekt w ktorym przechowujemy XMLowa konfiguracje raportow
	 * parsowany tylko raz
	 */
	private static Document xmlConfiguration = null;
	/**
	 * Zwraca konfiguracje raportu. Nie ma sensu parsowac tego za kazdym razem
	 * @return
	 */
	public synchronized Document getConfiguration() throws Exception
	{
		if (xmlConfiguration == null)
		{
			log.trace("parsuje plik konfiguracyjny");
			File f = new File(Docusafe.getHome(), REPORTS_CONFIGURATION_FILE);
			xmlConfiguration = new SAXReader().read(f);
		}

		return xmlConfiguration;
	}
	/**
	 * Pobiera instancje raportu
	 * @param cn
	 * @return
	 * @throws ReportException
	 */
	public Report getReportInstanceByCn(String cn) throws ReportException
	{
		try
		{
			if(cn==null) throw new ReportException("Cn raportu rowne null");
			Report result;
			Document configuration = getConfiguration();
			for(Element elem : (List<Element>)configuration.getRootElement().elements("report"))
			{
				if(elem.attributeValue("cn").equals(cn))
				{
					String clas = elem.attributeValue("class");
					if(clas==null)
						throw new ReportException("Raport "+cn+" nie ma wskazanej klasy");
					Object o = Class.forName(clas).newInstance();
					if(!(o instanceof Report))
						throw new ReportException("Wskazana klasa nie dziedziczy po klasie Report");
					result = (Report)o;
					result.setTitle(elem.attributeValue("name"));
					if(elem.attributeValue("periodicalCapable")!=null)
						result.setPeriodicalCapable(Boolean.parseBoolean(elem.attributeValue("periodicalCapable")));
					else
						result.setPeriodicalCapable(false);
					result.setDescription(elem.getText());

					return result;
				}
				//System.out.println(elem.attributeValue("cn"));
			}
			throw new ReportException("Raport o podanym cn '"+cn+"' nie istnieje");

		}
		catch(Exception e)
		{
			log.debug("",e);
			throw new ReportException(e);
		}
	}


	protected static Map<String,Document> descriptors = new HashMap<String,Document>();

	/**
	 * Pobiera wskazany zasob do raportu
	 * @param name
	 * @param r
	 * @return
	 */
	protected InputStream getResource(String name, Report r) {
		log.trace("getResource {}",name);
		return r.getClass().getResourceAsStream(name);
	}
	/**
	 * Metoda zapewni jednorazowa inicjalizacje deskryptora
	 * @param desc
	 * @return
	 * @throws Exception
	 */
	protected synchronized Document getDescriptor(String desc,Report r) throws Exception {
		log.trace("Pobieranie deskryptora {}",desc);
		Document resp = descriptors.get(desc);
		if (resp==null)
		{
			log.debug("Inicjujemy deskryptor {}",desc);
			resp = new SAXReader().read(getResource(desc, r));
			descriptors.put(desc, resp);
		}
		return resp;
	}

	public Document getDefaultConfigForCn(String cn, Report r) throws ReportException
	{
		try
		{
			if(cn==null) throw new ReportException("Cn raportu rowne null");
			Document result;
			Document doc = getConfiguration();
			for(Element elem : (List<Element>)doc.getRootElement().elements("report"))
			{
				if(cn.equals(elem.attributeValue("cn")))
				{
					String desc = elem.attributeValue("descriptor");
					result = getDescriptor(desc, r);
					return result;
				}
			}
			throw new ReportException("Raport o podanym cn '"+cn+"' nie istnieje");

		} catch(Exception e) {
			log.debug(e.getMessage(), e);
			throw new ReportException(e);
		}
	}

	/**
	 *
	 * @param r
	 * @return
	 * @throws Exception
	 */
	public File getResultsDirForReport(Report r) throws Exception
	{
		return getResultsDirForReportId(r.getId());
	}

	/**
	 *
	 * @param r
	 * @return
	 * @throws Exception
	 */
	public File getResultsDirForReportId(Long id) throws Exception
	{
		File reports;
		if(Configuration.isAdditionOn(Configuration.ADDITION_ATTACHMENTS_AS_FILES)) {
			reports = new File(Configuration.getPathToAttachments(),"reports").getCanonicalFile();
		} else {
		    //wsparcie dla symlink�w
			reports = new File(Docusafe.getHome(),"reports").getCanonicalFile();
		}

		if(!reports.exists())
		{
			reports.mkdir();
		}

		if(!reports.isDirectory()) throw new Exception("Sciezka nie wskazuje na katalog");

		File ret = new File(reports, id.toString()).getCanonicalFile();
		if(!ret.exists()) ret.mkdir();

		if(!ret.isDirectory()) throw new Exception("Sciezka nie wskazuje na katalog");
		return ret;
	}

	/**
	 * Pobiera raporty dla uzytkownika
	 * @param user
	 * @return
	 * @throws ReportException
	 */
	public List<Report> getReportsForUser(DSUser user) throws ReportException
	{
		List<Report> ret = new ArrayList<Report>();
		Report r;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			StringBuilder sbQuery = new StringBuilder("select * from ds_new_report where (username=? ");
        	DSDivision[] groups = user.getDSGroups();
        	if(groups != null && groups.length > 0)
        	{
        		boolean notFirst = false;
        		sbQuery.append(" or guid in (");
        		for (int i = 0; i < groups.length; i++)
            	{
        			if (notFirst)
        			{
        				sbQuery.append(", ");
        			}
        			sbQuery.append("?");
        			notFirst = true;
            	}
        		sbQuery.append(")");
        	}
        	sbQuery.append(") ");
        	sbQuery.append(" order by id");
        	log.trace(sbQuery.toString());
        	ps = DSApi.context().prepareStatement(sbQuery.toString());

        	ps.setString(1, user.getName());

        	for (int i = 0; i < groups.length; i++)
        	{
        		ps.setString(i+2, groups[i].getGuid());
        	}
        	rs = ps.executeQuery();
        	while(rs.next())
        	{
        		try
        		{
	        		r = getReportInstanceByCn(rs.getString("report_cn"));
	        		r.setCtime(rs.getTimestamp("ctime"));
	        		r.setFinishProcessTime(rs.getTimestamp("finish_process_time"));
	        		r.setStatus(rs.getInt("status"));
	        		r.setId(rs.getLong("id"));
	        		r.setTitle(rs.getString("title"));
	        		r.setDescription(rs.getString("description"));
	        		ret.add(r);
        		}
        		catch(Exception e)
        		{
        			log.error(e.getMessage(),e);
        		}

        	}

		}
		catch(Exception e)
		{
			log.debug("",e);
			throw new ReportException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
		}

		return ret;

	}
	/**
	 * Usuwanie raportu
	 * @param id
	 * @throws EdmException
	 */
	public void deleteReportById(Long id) throws EdmException
	{
		PreparedStatement ps = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement("delete from ds_new_report where id=?");
    		ps.setLong(1, id);
    		ps.executeUpdate();

    	}
    	catch(Exception e)
    	{
    		log.debug("",e);
    		throw new EdmException(e);
    	}
    	finally
    	{
    		DSApi.context().closeStatement(ps);
    	}
	}

	public void registerReport(String reportCn, Map<String, Object> values, Map<String,Object> invalues) throws Exception{
		File file = null;
		Report r = getReportInstanceByCn(reportCn);
		r.init(getDefaultConfigForCn(reportCn, r), reportCn);

		List<ReportParameter> params = r.getParams();


		r.setPeriodical(false);
		r.setInterval(0);
		for (ReportParameter rp : params) {
			Object o = null;
			//sprawdza czy jest wielowartosciowy
			if ("file".equals(rp.getType())) {
				//String filename = "kryteria" + rp.getFieldCn() +".dsr";
				//FormFile ff = (FormFile)values.get(rp.getFieldCn());
				//files.put(filename, ff.getFile());
				if (file == null) {
					throw new IllegalArgumentException("brak wybranego pliku");
				}
				o = file.getName();
			} else if (invalues.get(rp.getFieldCn()) != null) {
				String value = (String) invalues.get(rp.getFieldCn());
				String[] tmp = value.split("\n");
				for (int i = 0; i < tmp.length; i++) {
					tmp[i] = tmp[i].trim();
				}
				o = tmp;
			} else {
				o = values.get(rp.getFieldCn());
			}

			if (o != null) {
				if ("date".equals(rp.getType())) {
					rp.setValue((String) o);
				} else if ("division".equals(rp.getType()) || "user".equals(rp.getType())) {
					rp.setValue(TextUtils.parseStringArray(o));
				} else if ("dockind".equals(rp.getType())) {
					rp.setValue(TextUtils.parseStringArray(o));
				} else {
					rp.setValue(TextUtils.parseStringArray(o));
				}
			}



		}
		r.setParams(params);

		saveReport(r);

		if (file != null) {
			FileUtils.copyFileToFile(file, new File(r.getDestination(), "kryteria.dsr"));
		}
	}

	public void saveReport(Report r) throws Exception {
		PreparedStatement ps = null;
		Long reportId = 0L;
		DSApi.context().begin();
		Date d = new Date();
		if (DSApi.isOracleServer()) {
			ps = DSApi.context().prepareStatement("insert into ds_new_report (id,report_cn, ctime, username, status, criteria, title, description, process_date, remote_key, guid) values(ds_new_report_id.nextval,?,?,?,?,?,?,?,?,?,?)");
		} else if(DSApi.isPostgresServer()) {
			ps = DSApi.context().prepareStatement("insert into ds_new_report (report_cn, ctime, username, status, criteria, title, description, process_date, remote_key, guid) values(?,?,?,?,?,?,?,?,?,?)");
		} else {
			ps = DSApi.context().prepareStatement("insert into ds_new_report (report_cn, ctime, username, status, criteria, title, description, process_date, remote_key, guid) values(?,?,?,?,?,?,?,?,?,?)");
		}
		ps.setString(1, r.getReportCn());
		ps.setTimestamp(2, new Timestamp(d.getTime()));
		ps.setString(3, DSApi.context().getPrincipalName());
		ps.setInt(4, Report.NEW_REPORT_STATUS);


		File tmpfile = File.createTempFile("docusafe_report_", ".tmp");
		OutputStream output = new BufferedOutputStream(new FileOutputStream(tmpfile));
		PrintStream pr = new PrintStream(output, true, "UTF-8");
		pr.print(r.getSearchCriteria().asXML());
		pr.close();
		output.close();
		InputStream is = new FileInputStream(tmpfile);
		ps.setBinaryStream(5, is, (int) tmpfile.length());

		ps.setString(6, r.getTitle());
		ps.setString(7, r.getDescription());
		ps.setTimestamp(8, new Timestamp(new Date().getTime()));

		String remKey = DSApi.context().getPrincipalName() + System.currentTimeMillis();
		Random rand = new Random(System.currentTimeMillis());
		while (remKey.length() < 30) {
			remKey += rand.nextInt(9) % 10;
		}
		ps.setString(9, remKey);
		ps.setString(10, r.getGuid());

		DSApi.context().closeStatement(ps);
		ps = DSApi.context().prepareStatement("select id from ds_new_report where ctime=?");
		ps.setTimestamp(1, new Timestamp(d.getTime()));
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			reportId = rs.getLong("id");
		}
		r.setId(reportId);

		r.setDestination(getResultsDirForReport(r));

		DSApi.context().commit();
	}


}
