package pl.compan.docusafe.reports;


import com.google.common.collect.Maps;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.CollectionUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.UserToJasperReports;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.archiwum.reports.JasperReportsKind;
import pl.compan.docusafe.parametrization.archiwum.reports.JasperReportsLogic;
import pl.compan.docusafe.reports.tools.AbstractTableDumper;
import pl.compan.docusafe.reports.tools.FileDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class JasperReport extends Report {


    private static final Logger log = LoggerFactory.getLogger(JasperReport.class);

    private Date startDate;
    private Date endDate;
    private String user;
    private String raportType;
    private String format;

    private UniversalTableDumper dumper = new UniversalTableDumper();

    @Override
    public void initParametersAvailableValues() throws ReportException {
        super.initParametersAvailableValues();

        try {

            for (ReportParameter rp : params) {
                if (rp.getFieldCn().equals("raportType")) {
                    rp.setAvailableValues(getReportTypeMaps());
                } else if (rp.getFieldCn().equals("format")) {
                    rp.setAvailableValues(getFormatMaps());
                } else if (rp.getFieldCn().equals("user")) {
                    rp.setAvailableValues(getUserMaps());
                }
            }
        } catch (Exception e) {
            log.error("", e);
            throw new ReportException(e);
        }
    }

    @Override
    public void doReport() throws Exception {
        for( ReportParameter rp : params )
        {
            if( rp.getFieldCn().equals("raportType") )
            {
                raportType = rp.getValueAsString();
            }
            if( rp.getFieldCn().equals("format") )
            {
                format = rp.getValueAsString();
            }
            if( rp.getFieldCn().equals("start_date")) {
                startDate = DateUtils.parseJsDate(rp.getValueAsString());
            }
            if(rp.getFieldCn().equals("end_date")) {
                endDate = DateUtils.parseJsDate(rp.getValueAsString());
            }
            if(rp.getFieldCn().equals("user")) {
                user = rp.getValueAsString();
            }
        }

        String fileName = JasperReportsKind.findByPath(raportType).getTitle() + "_" + DateUtils.formatDateWithHoursWithoutDash(Calendar.getInstance().getTime());

        File jasperFile = new JasperReportsLogic(this.destination, fileName)
                .getReport(format, raportType, startDate, endDate, StringUtils.isBlank(user) ? DSApi.context().getDSUser().getId() : Long.valueOf(user));

        AbstractTableDumper fileDumper = new FileDumper();
        fileDumper.openFile(jasperFile);
        dumper.addDumper(fileDumper);
    }

    public Map<String,String> getReportTypeMaps() throws EdmException {
        Map<String,String> listOfReport = Maps.newHashMap();
        List<JasperReportsKind > kindList = JasperReportsKind.list();
        for (JasperReportsKind rep : kindList) {

            if (GlobalPreferences.getAdminUsername().equals(DSApi.context().getDSUser().getName())) {
                listOfReport.put(rep.getPath(), rep.getTitle());
            } else {
                java.util.List<UserToJasperReports> list = getUsersToJasperReportsList(DSApi.context().getDSUser().getId());
                for (UserToJasperReports utjr : list) {
                    if (utjr.getReportId().equals(rep.getId())) {
                        listOfReport.put(rep.getPath(), rep.getTitle());
                    }
                }
            }
        }
        return listOfReport;
    }


    public static List<UserToJasperReports> getUsersToJasperReportsList(Long userId) throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(UserToJasperReports.class);
        criteria.add(Restrictions.eq("userId", userId));
        return criteria.list();
    }

    public Map<String,String> getFormatMaps() {
        Map<String,String> formatType = new HashMap<String, String>();
        formatType.put("PDF","PDF");
        formatType.put("HTML","HTML");
        formatType.put("XLS","XLS");
        formatType.put("XML","XML");
        formatType.put("CSV", "CSV");
        formatType.put("RTF", "RTF");
        return formatType;
    }

    public Map<String,String> getUserMaps() throws EdmException {
        Map<String,String> userMaps = new HashMap<String, String>();
        List<DSUser> supervisorUsers = DSUser.getSupervisorUsers(DSApi.context().getDSUser());

        if(!CollectionUtils.isEmpty(supervisorUsers)) {
            for (DSUser user : supervisorUsers) {
                userMaps.put(user.getId().toString(), user.asFirstnameLastnameName());
            }
        }

        userMaps.put(DSApi.context().getDSUser().getId().toString(), DSApi.context().getDSUser().asFirstnameLastnameName() );

        return userMaps;
    }
}
