package pl.compan.docusafe.reports;

import pl.compan.docusafe.core.EdmException;



/**
 * Wyjatek dla raportow
 * @author wkutyla
 *
 */
public class ReportException extends EdmException 
{
	static final long serialVersionUID = 1;
	
	public ReportException(String message)
	{
	   super(message);	        
	}

	public ReportException(String message, Throwable cause)
	{
	   super(message, cause);	
	}

	public ReportException(Throwable cause)
	{
	   super(cause);	
	}
}
