package pl.compan.docusafe.reports;

/**
 * Klasa abstrakcyjna zawierajaca stub raportu Docusafe
 *
 * @author wkutyla
 *
 */

import java.io.File;
import java.util.*;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;

public abstract class Report
{
	protected static final Logger log = LoggerFactory.getLogger(Report.class);

	public final static Integer NEW_REPORT_STATUS = 0;
	public final static Integer ERR_REPORT_STATUS = -1;
	public final static Integer DONE_REPORT_STATUS = 1;
	public final static Integer PROGRESS_REPORT_STATUS = 2;

	public final static String REPORT_CSV_FILENAME_PL = "raport.csv";
	public final static String REPORT_CSV_FILENAME_EN = "report.csv";
	public final static String REPORT_XLS_FILENAME_PL = "raport.xls";
	public final static String REPORT_XLS_FILENAME_EN = "report.xls";


	protected ReportEnvironment reportEnvironment = new ReportEnvironment();

	protected StringManager sm;
	/**
	 * Identyfikator raportu
	 */
	protected Long id;
	/**
	 * Identyfikator uzytkownika tworzacego raport
	 */
	protected String username;

	protected String guid;
	/**
	 * cn raportu
	 */
	protected String reportCn;
	/**
	 * Data utworzenia raportu
	 */
	protected Date ctime;
	/**
	 * Data przetwoerzenia raportu
	 */
	protected Date startProcessTime;

	/**
	 * Data zako�czenia przetwarzania
	 */
	protected Date finishProcessTime;
	/**
	 * Status raportu
	 */
	protected Integer status;

	/**
	 * Lista parametrow raportu wczytywana z XMLa.
	 */
	protected List<ReportParameter> params;

	/**
	 * Mapa parametrow o wartosciach != null;
	 * Uporzadkowanych wedlug cn
	 */
	protected Map<String, ReportParameter> parametersMap;
	/**
	 * Dockind dla raportu
	 */
	protected String dockindCn;

	/**
	 * Katalog w kt�rym raport ma zapisa� pliki
	 */
	protected File destination;

	/**
	 * Tytul wyswietlany uzytkownikowi
	 */
	protected String title;

	/**
	 * Wlasciciel raportu uzyskiwany na podstawie zmiennej
	 * username
	 */
	protected DSUser reportOwner = null;
	/**
	 * Opis uzytkownika
	 */
	protected String description;

	protected Boolean periodical;

	protected Boolean periodicalCapable;

	protected Integer interval;


	protected Boolean shareable;


	/**
	 * Metoda inicjuje raport w zaleznosci od pliku konfiguracyjnego zapisanego w formacie XML
	 * @param config
	 * @throws ReportException
	 */
	public final void init(Document config, String reportCn) throws ReportException
	{
		//System.out.println(config.asXML());
		this.reportCn = reportCn;
		sm = StringManager.getManager(this.getClass().getPackage().getName());
		//System.out.println(this.getClass().getPackage().getName());
		this.params = new ArrayList<ReportParameter>();
		this.parametersMap = new HashMap<String,ReportParameter>();

		Element elem  = config.getRootElement();

		this.dockindCn = elem.attributeValue("dockindCn");
		//if(elem.attributeValue("reportCn")!=null)
		//	this.reportCn = elem.attributeValue("reportCn");
		if(elem.attributeValue("periodical")!=null)
			this.periodical = Boolean.parseBoolean(elem.attributeValue("periodical"));
		else
			this.periodical = false;

		if(elem.attributeValue("interval")!=null)
			this.interval = Integer.parseInt(elem.attributeValue("interval"));
		else
			this.interval = 0;

		if(elem.attributeValue("shareable")!=null)
			this.shareable = Boolean.parseBoolean(elem.attributeValue("shareable"));
		else
			this.shareable = false;
		ReportParameter rp;

		for(Element e: (List<Element>)elem.elements("field"))
		{
			rp = new ReportParameter();
			rp.setFieldCn(e.attributeValue("cn"));
			rp.setLabel(e.attributeValue("lang-key"));
			rp.setType(e.attributeValue("type"));
			rp.setRequired(Boolean.parseBoolean(e.attributeValue("mandatory")));
			rp.setDockindCn(e.attributeValue("dockindCn"));
			rp.setRelation(e.attributeValue("relation"));
			rp.setAdditionalInfo(e.attributeValue("additional-info"));
        	rp.setMultiple(Boolean.parseBoolean(e.attributeValue("multiple")));
        	rp.setVisible(e.attribute("visible") != null ? Boolean.parseBoolean(e.attributeValue("visible")) : Boolean.valueOf(true));

			List<Element> values = e.elements("value");
			if(values.size()>1)
			{
				String[] vals = new String[values.size()];
				for(int i=0; i<values.size(); i++)
				{
					vals[i] = values.get(i).getText();
				}
				rp.setValue(vals);
			}
			else if(values.size()>0)
			{
				rp.setValue(values.get(0).getText());
			}
			params.add(rp);
			initParametersAvailableValues();
			if (rp.getValue()!=null)
			{
				parametersMap.put(rp.getFieldCn(), rp);
			}
		}

	}

	public void initParametersAvailableValues() throws ReportException
	{
		for(ReportParameter rp : params)
		{
			//System.out.println(rp.getFieldCn());
			rp.setAvailableValues(Collections.<String, String>emptyMap());
		}
	}
	/**
	 * Metoda zwraca XMLa zawierajacego definicje kryteriow
	 * @return
	 */
	public Document getSearchCriteria()
	{
		Document result = DocumentFactory.getInstance().createDocument();
		Element elem = result.addElement("report-context");
		elem.addAttribute("dockindCn", dockindCn);
		elem.addAttribute("reportCn", reportCn);
		elem.addAttribute("periodical", periodical.toString());
		elem.addAttribute("interval", interval.toString());
		elem.addAttribute("shareable", shareable.toString());
		result.setRootElement(elem);
		Element field;
		if(params==null) return result;
		for(ReportParameter rp:params)
		{
			field = elem.addElement("field");
			field.addAttribute("cn", rp.getFieldCn());
			field.addAttribute("type", rp.getType());
			field.addAttribute("lang-key", rp.getLabel());
			field.addAttribute("mandatory", rp.getRequired().toString());
			field.addAttribute("dockindCn", rp.getDockindCn());
			field.addAttribute("relation", rp.getRelation());
			field.addAttribute("additional-info", rp.getAdditionalInfo());
			field.addAttribute("multiple", rp.getMultiple() +"");

			if(rp.getVisible() != null)
				field.addAttribute("visible", rp.getVisible().toString());

			if(rp.getValue() instanceof String)
			{
				field.addElement("value").setText((String)rp.getValue());
			}
			else if(rp.getValue() instanceof String[])
			{
				for(String s: (String[])rp.getValue())
				{
					field.addElement("value").setText(s);
				}
			}
		}
		return result;

	}

	/**
	 * Metoda wywolywana przez serwis.
	 * @throws ReportException
	 */
	public final void generateReport() throws ReportException
	{
		try
		{
			destination = reportEnvironment.getResultsDirForReport(this);
			if(destination==null||!destination.isDirectory()) throw new ReportException("Nieprawid�owy katalog dla rezultatow raportu");
			reportOwner = DSUser.findByUsername(username);
			doReport();
		}
		catch(Exception e)
		{
			throw new ReportException(e);
		}
	}

    public static Report find(Long id) throws EdmException
    {
        return (Report) Finder.find(Report.class, id);
    }

	/**
	 * Metoda abstrakcyjna wykonujaca raport
	 * @throws Exception
	 */
	public abstract void doReport() throws Exception;




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getReportCn() {
		return reportCn;
	}

	public void setReportCn(String reportCn) {
		this.reportCn = reportCn;
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public Date getStartProcessTime() {
		return startProcessTime;
	}

	public void setStartProcessTime(Date startProcessTime) {
		this.startProcessTime = startProcessTime;
	}

	public Date getFinishProcessTime() {
		return finishProcessTime;
	}

	public void setFinishProcessTime(Date finishProcessTime) {
		this.finishProcessTime = finishProcessTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<ReportParameter> getParams() {
		return params;
	}

	public void setParams(List<ReportParameter> params) {
        Map<String, ReportParameter> parameterMap = toMap(params);
        validatePrams(parameterMap);
        this.parametersMap = parameterMap;
		this.params = params;
	}

    /**
     * Metoda s�u��ca do walidacji parametr�w domy�lnie pusta
     * @param parameterMap
     */
    protected void validatePrams(Map<String,ReportParameter> parameterMap) {
    }

    private Map<String, ReportParameter> toMap(List<ReportParameter> params) {
        return Maps.uniqueIndex(params, new Function<ReportParameter, String>() {
            public String apply(ReportParameter reportParameter) {
                return reportParameter.getFieldCn();
            }
        });  //To change body of created methods use File | Settings | File Templates.
    }

    public String getDockindCn() {
		return dockindCn;
	}

	public void setDockindCn(String dockindCn) {
		this.dockindCn = dockindCn;
	}

	public File getDestination() {
		return destination;
	}

	public void setDestination(File destination) {
		this.destination = destination;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getPeriodical() {
		return periodical;
	}

	public void setPeriodical(Boolean periodical) {
		this.periodical = periodical;
	}

	public Boolean getPeriodicalCapable() {
		return periodicalCapable;
	}

	public void setPeriodicalCapable(Boolean periodicalCapable) {
		this.periodicalCapable = periodicalCapable;
	}

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	public String getGuid()
	{
		return guid;
	}

	public void setGuid(String guid)
	{
		this.guid = guid;
	}

	public Boolean getShareable()
	{
		return shareable;
	}

	public void setShareable(Boolean shareable)
	{
		this.shareable = shareable;
	}

	public String getTextStatus()
	{
		sm = StringManager.getManager(this.getClass().getPackage().getName());
		switch (status) {
		case 0:
			return sm.getString("Oczekuje");
		case 1:
			return sm.getString("Ukonczony");
		case 2:
			return sm.getString("Procesowany");
		case -1:
			return sm.getString("Blad");
		default:
			return sm.getString("Nieznany");
		}
	}
	/**
	 * Pobieranie opisow
	 * @param key
	 * @return
	 * @TODO
	 */
	public String getLanguageValue(String key)
	{
		return sm.getString(key);
	}


}
