package pl.compan.docusafe.reports;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import edu.emory.mathcs.backport.java.util.Arrays;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ReportParameter implements Serializable
{
    private static final Logger LOG =  LoggerFactory.getLogger(ReportParameter.class);
	private static final long serialVersionUID = 1L;
	
	/**
	 * Porownanie =
	 */
	public static final String EQUAL = "equal";
	
	/**
	 * FROM - >=
	 */
	public static final String FROM = "from";
	
	/**
	 * TO <=
	 */
	public static final String TO = "to";
	/**
	 * !=
	 */
	public static final String NOT_EQUAL = "notEqual";
	/**
	 * Relacja >
	 */
	public static final String GREATER = "greater";
	
	/**
	 * Relacja < 
	 */
	
	public static final String LESS = "less";
	
	/**
	 * >=
	 */
	public static final String GREATER_EQUAL = "greaterEqual";
	
	/**
	 * <=
	 */
	
	public static final String LESS_EQUAL = "lessEqual";
	
	/**
	 * Nieobslugiwane 
	 */
	public static final String IS_NULL = "isnull";
	
	/**
	 * Jescze nie obslugiwane
	 */
	public static final String IS_NOT_NULL = "isnotnull";
	/**
	 * cn pola. Musi byc unikalny w ramach raportu
	 */
	
	/**
	 * Like %%;
	 */
	public static final String LIKE = "like";
	
	private String fieldCn;
	
	/**
	 * potrzebny do odpowiedniego wyswietlenia pola. Typ dockind oznacza ze jest to pole dockindowe
	 */
	private String type;
	
	/**
	 * okresla czy pole jest wymagane
	 */
	private Boolean required;
	
	/**
	 * nazwa wyswietlana na formatce
	 */
	private String label;
	
	/**
	 * CN pola dockindowego do ktorego odnosi sie kryterium.
	 */
	private String dockindCn;
	
	private String additionalInfo;
	/**
	 * relacja do porownywania wartosci
	 */
	private String relation = EQUAL;
	/**
	 * wartosc. Zawsze jest to String lub tablica Stringow.
	 */
	private Object value;
	/**
	 * mapa warto�ci do wy�wietlania w polu select. Musi zosta� zainicjalizowana za pomoc� raportu.
	 */
	private Map<String, String> availableValues;
	/**
	 * w przypadku parametr�w wybieranych z listy okre�la czy pole ma by� wielokrotnego wyboru. Domy�lnie true;
	 */
	private Boolean multiple;
	
	/**
	 * parametr mowi o tym czy raportparaeter ma byc widoczny na formatce
	 * wartosc domyslna = true
	 */
	private Boolean visible = true;
	
	public ReportParameter()
	{
		multiple = true;
	}
	
	public String getFieldCn() {
		return fieldCn;
	}
	public void setFieldCn(String fieldCn) {
		this.fieldCn = fieldCn;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Boolean getRequired() {
		return required;
	}
	public void setRequired(Boolean required) {
		this.required = required;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDockindCn() {
		return dockindCn;
	}
	public void setDockindCn(String dockindCn) {
		this.dockindCn = dockindCn;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public Object getValue() 
	{
		return value;
	}
	
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	/**
	 * Metoda zwraca wartosc jako string 
	 * Jesli value == null to zwracany jest pusty String
	 * @return
	 */
	public String getValueAsString() {
        if(value instanceof Object[] && ((Object[])value).length == 1){
            return ObjectUtils.toString(((Object[])value)[0]);
        }
		return value == null ? "" :value.toString();
	}

	@SuppressWarnings("unchecked")
	public List<String> valuesAsList()
	{
		if(this.getValue() == null)
			return Collections.EMPTY_LIST;
		else if(this.getValue() instanceof String[])
			return Arrays.asList((String[]) this.getValue());
		else if(this.getValue() instanceof String) 
			return Arrays.asList(new String[] { (String) this.getValue() });
		
		return Collections.EMPTY_LIST;
	}
	
	public void setValue(Object value) {
//        LOG.info("setValue = {}", value);
//        if(value instanceof Object[]){
//            assert ((Object[])value).length > 1;
//        }
		this.value = value;
	}
	public Map<String, String> getAvailableValues() {
		return availableValues;
	}
	public void setAvailableValues(Map<String, String> availableValues) {
		this.availableValues = availableValues;
	}
	public boolean getMultiple() {
		return multiple;
	}
	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}
	
	public boolean equals(ReportParameter rp)
	{
		if(this.getFieldCn().equals(rp.getFieldCn())) return true;
		else return false;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

    @Override
    public String toString() {
        return "ReportParameter{" +
                "label='" + label + '\'' +
                ", fieldCn='" + fieldCn + '\'' +
                ", value=" + value +
                '}';
    }
}
