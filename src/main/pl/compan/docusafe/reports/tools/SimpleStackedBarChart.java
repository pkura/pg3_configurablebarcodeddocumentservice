package pl.compan.docusafe.reports.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class SimpleStackedBarChart
{
	private String chartTitle;
	private String labelX;
	private String labelY;
	private DefaultCategoryDataset dataset;
	private boolean legend;
	private Double maxValue = 0.0;

	public SimpleStackedBarChart(String chartTitle, String labelX, String labelY, boolean legend)
	{
		super();
		this.chartTitle = chartTitle;
		this.labelX = labelX;
		this.labelY = labelY;
		this.legend = legend;
	}

	public void savePieChart(int width, int height,File file,PlotOrientation plotOrientation) throws IOException
	{
		JFreeChart chart = ChartFactory.createStackedBarChart
		(
	            chartTitle,  // chart title
	            labelX,                  // label X
	            labelY,                     //  label Y
	            dataset,                     // data
	            plotOrientation,    // the plot orientation
	            legend,                        // legend
	            true,                        // tooltips
	            false                        // urls
	    );
		chart.setBackgroundImageAlpha(0.3f);
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
        plot.getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.UP_45);
        Double tic = maxValue/10;
        tic = new Double(tic.intValue());
        if(tic < 1 )
        	tic = 1.0;
        ValueAxis axis = plot.getRangeAxis();
        ((NumberAxis) axis).setTickUnit(new NumberTickUnit(tic));
    	ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
    	OutputStream  os = new FileOutputStream(file);
    	ChartUtilities.writeChartAsJPEG(os,chart, width, height, info);
	}

	public void addValue(Double value,String series,String bar)
	{
		if(dataset == null)
			dataset = new DefaultCategoryDataset();
		dataset.addValue(value, series, bar);
		if(maxValue < value)
			maxValue = value;
	}

	public void addValue(Integer value,String series,String bar)
	{
		addValue(value.doubleValue(),series,bar);
	}
}
