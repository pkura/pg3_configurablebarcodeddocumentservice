package pl.compan.docusafe.reports.tools;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * Klasa  bazowa dla obiektow formatujacych zrzut danych do dokumentow
 * XLS, CSV i innych opartych o strukture pojedynczej tabeli
 * @author wkutyla
 *
 */
public abstract class AbstractTableDumper 
{
	protected static final Logger log = LoggerFactory.getLogger(AbstractTableDumper .class);
	
	/**
	 * Domyslny format daty
	 */
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	/**
	 * Domylsny format timestampu
	 */
    public static final DateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final String EMPTY_STRING = ""; 
    
    /**
     * Metoda otwieraja plik o wskazanym deskryptorze, inicjuje obiekt
     * @param file
     * @throws IOException
     */
    public abstract void openFile(File file) throws IOException;
    
    /**
     * Metoda otwiera nowa linie
     */
    public abstract void newLine();
    
    /**
     * Metoda dodaje do linii pole tekstowe
     * @param text
     */
    protected abstract void addToLine(String text);
 
    /**
     * Metoda zapisuje linie w pliku
     * @throws IOException
     */
    public abstract void dumpLine() throws IOException;
    
    /**
     * Zamkniecie pliku
     * @throws IOException
     */
    public abstract void closeFile() throws IOException;
    
    /**
	 * Zamkniecie pliku bez generowania wyjatku
	 */
	public void closeFileQuietly()
	{
		try
		{
			closeFile();
		}
		catch (Exception e)
		{
			log.error("",e);
		}
	}
	
	/**
	 * Dodawanie Integera
	 * @param liczba
	 */
	public abstract void addInteger(Integer liczba);
	
	
	/**
	 * Dodawanie pola Long
	 * @param liczba
	 */
	public void addLong(Long liczba)
	{
		//todo
		addToLine(liczba.toString());
	}

	/**
	 * Dodawanie pola money
	 * @param amount
	 * @param isComma TRUE - przecinek, FALSE - kropka
	 */
	public void addMoney(Number amount, boolean isComma)
	{
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		//addToLine(nf.format(amount));
		String _amount = nf.format(amount);
		if (isComma)
			_amount = _amount.replace('.', ',');
		else 
			_amount = _amount.replace(',', '.');
		addToLine(_amount);
	}
	
	/**
	 * Dodawanie pola money
	 * @param amount
	 * @param isComma TRUE - przecinek, FALSE - kropka
	 * @param scale - ilosc miejsc po przecinku
	 */
	public abstract void addMoney(Number amount, boolean isComma, int scale);
	
	/**
	 * Dodawanie pola money
	 * @param amount
	 */
	public void addMoney(Number amount)
	{
		//todo
		addMoney(amount, false);
	}
	
	/**
	 * Dodajemy do linii pole timestamp
	 * @param timestamp
	 */
	public void addTimestamp(Date timestamp)
	{
		addDateToLine(timestamp,TIMESTAMP_FORMAT);
	}
	/**
	 * Dodaje do linii date
	 * @param date
	 */
	public abstract void addDate(Date date);
	
	public abstract void addDate(Date date, final String format);
	
	protected void addDateToLine(Date date, DateFormat format)
	{
		if (date!=null)
		{
			addToLine(format.format(date));
		}
		else
		{
			addToLine(null);
		}
	}
	
	/**
	 * Dodaje pole tekstowe do linii
	 * @param text
	 */
	public void addText(String text)
	{
		addToLine(text);
	}
	
        public void addParagraph(String title)
        {
            try{
                newLine();
                addToLine(title);
                dumpLine();
            } catch (IOException e)
            {
                log.error("",e);
            }
        }
        /**
         * Dodaje pusty string
         */
        public void addEmptyText()
        {
            addToLine(EMPTY_STRING);
        }
	/**
	 * Dodaje pole tekstowe do linii
	 * @param text
	 */
	public void addBoolean(Boolean bool)
	{
		if(bool)
			addToLine("TAK");
		else
			addToLine("NIE");
	}
	
	/**
	 * Dodaje dowolny obiekt a kontrektie .toString()
	 * @param obj
	 */
	public void addObject(Object obj)
	{
		if (obj!=null)
			addToLine(obj.toString());
		else
			addToLine("");
	}
}
