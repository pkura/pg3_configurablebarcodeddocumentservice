package pl.compan.docusafe.reports.tools;

import java.util.ArrayList;
import java.util.List;

public class Row {
	private List<String> cells = new ArrayList<String>();
	private boolean headerRow;
	
	public Row() {
		this.headerRow = false;
	}
	
	public Row(boolean headerRow) {
		this.headerRow = headerRow;
	}
	
	public void addCell(String data) {
		cells.add(data);
	}
	
	public int getRowSize() {
		return cells.size();
	}
	
	public List<String> getCells() {
		return cells;
	}

	public boolean isHeaderRow() {
		return headerRow;
	}

	public void setHeaderRow(boolean headerRow) {
		this.headerRow = headerRow;
	}
}
