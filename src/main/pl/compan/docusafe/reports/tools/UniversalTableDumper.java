package pl.compan.docusafe.reports.tools;


import java.io.File;
import java.io.IOException;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Klasa ktora zrzuca jednoczesnie do kilku roznych plikow
 * Dumpery nalezy utworzyc, zrobic openFile, dodac do kolecji za pomoca metody
 * addDumper 
 * @author wkutyla
 *
 */
public class UniversalTableDumper extends AbstractTableDumper 
{
	protected static final Logger log = LoggerFactory.getLogger(UniversalTableDumper.class);
	
	private Collection<AbstractTableDumper> dumpers = new Vector<AbstractTableDumper>();

    @Override
    public void addParagraph(String title)
    {
        for (AbstractTableDumper dumper : dumpers)
        {			
                dumper.addParagraph(title);
        }
    }
	
        
	@Override
	protected void addToLine(String text) 
	{
		for (AbstractTableDumper dumper : dumpers)
		{			
			dumper.addToLine(text);
		}

	}

	@Override
	public void closeFile() throws IOException 
	{
		for (AbstractTableDumper dumper : dumpers)
		{			
				dumper.closeFile();			
		}

	}

	@Override
	public void dumpLine() throws IOException 
	{
		for (AbstractTableDumper dumper : dumpers)
		{
			dumper.dumpLine();
		}

	}

	@Override
	public void newLine() 
	{
		for (AbstractTableDumper dumper : dumpers)
		{
			dumper.newLine();
		}

	}

	/**
	 * Dodaje dumper do kolekcji obslugiwanych dumperow
	 * @param dumper
	 */
	public void addDumper(AbstractTableDumper dumper)
	{
		dumpers.add(dumper);
	}
	
	@Override
	public void openFile(File file) throws IOException 
	{
		log.warn("Nie uzywac tej metody w klasie dumpery powinny same sobie otworzyc plik");
	}
	

	/**
	 * Dodaje wiersz do tabeli.
	 * @param headers Tablica komorek wiersza
	 */
	public void addRow(Object[] row) throws IOException 
	{
		for (AbstractTableDumper dumper : dumpers)
		{	
			dumper.newLine();
			for (Object r: row) {
				dumper.addToLine(r.toString());
			}
			dumper.dumpLine();
		}
	}

    public void addRow(String... row) throws IOException {
		addRow((Object[])row);
	}
	
	/**
	 * Dodaje wiersz do tabeli bez rozpoczynania nowej linii.
	 * @param headers Tablica komorek wiersza
	 */
	public void addRowWithoutNewLine(Object[] row) throws IOException 
	{
		for (AbstractTableDumper dumper : dumpers)
		{
			for (Object r: row) {
				dumper.addToLine(r.toString());
			}
			dumper.dumpLine();
		}
	}

	@Override
	public void addMoney(Number amount, boolean isComma, int scale) 
	{
		for (AbstractTableDumper dumper : dumpers)
		{		
			dumper.addMoney(amount, isComma, scale);
		}
	}

	@Override
	public void addDate(Date date) 
	{
		if(date!=null)
		{
			for (AbstractTableDumper dumper : dumpers)
			{		
				dumper.addDate(date);
			}
		}
		else
		{
			for (AbstractTableDumper dumper : dumpers)
			{		
				dumper.addText("");
			}
		}
	}

	
	public void addInteger(Integer liczba) 
	{
		for (AbstractTableDumper dumper : dumpers)
		{			
			dumper.addInteger(liczba);
		}
		
	}

	@Override
	public void addDate(Date date, final String format) 
	{
		if(date!=null)
		{
			for (AbstractTableDumper dumper : dumpers)
			{		
				dumper.addDate(date, format);
			}
		}
		else
		{
			for (AbstractTableDumper dumper : dumpers)
			{		
				dumper.addText("");
			}
		}
		
	}

}
