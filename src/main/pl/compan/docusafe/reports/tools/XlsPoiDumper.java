package pl.compan.docusafe.reports.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.util.DateUtils;
/**
 * Obiekt ktory zrzuca do pliku XLS w prostej strukturze
 * (pierwszy wiersz kolumny) kolejne to 
 * @author wkutyla
 *
 */
public class XlsPoiDumper extends AbstractTableDumper
{
	
	//public static final int maxFileSize = 12000;
	public static final int maxFileSize = 60000;
	
	/**
	 * Czy pierwszy wiersz jest naglowkiem
	 */
	private boolean firstRowHeader = true;
	
	private HSSFCellStyle headerStyle;
	private HSSFCellStyle normalStyle;
	private HSSFWorkbook workbook;
	private HSSFSheet sheet;
	private HSSFRow row;
	
	private int rowCounter = 0;
	private int sheetCounter = 0;
	/**
	 * Zlicza maksymalna liczbe kolumn
	 */
	private int columnCount = 0;
	private int columnIndex = 0;
	private File xlsFile = null;
	
	static final Logger log = LoggerFactory.getLogger(XlsPoiDumper.class);
	
	@Override
	public void openFile(File file) throws IOException
	{
		
		xlsFile = file;
		workbook = new HSSFWorkbook();
        sheet = workbook.createSheet("Raport");
        sheetCounter++;
        
        headerStyle = workbook.createCellStyle();
        headerStyle.setBorderBottom(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setBottomBorderColor(HSSFColor.RED.index);
        
        headerStyle.setBorderLeft(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setLeftBorderColor(HSSFColor.RED.index);
        
        headerStyle.setBorderRight(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setRightBorderColor(HSSFColor.RED.index);
        
        headerStyle.setTopBorderColor(HSSFColor.RED.index);
        headerStyle.setBorderTop(HSSFCellStyle.BORDER_DOUBLE);
        
        normalStyle = workbook.createCellStyle();
	}
	
	@Override
	public void newLine()
	{		
		if (rowCounter <= maxFileSize)
		{
			rowCounter++;
			row =  sheet.createRow(rowCounter-1);
			columnIndex = 0;		
		}	
		else
		{
			sheetCounter++;
			sheet = workbook.createSheet("Raport cz. "+sheetCounter);
			rowCounter = 0;
			this.firstRowHeader = false;
			this.newLine();
			log.trace("przekroczona wielkosc pliku");
		}
	}
	
	@Override
	protected void addToLine(String text)
	{	
		if (rowCounter <= maxFileSize)
		{
			HSSFCell cell = row.createCell(columnIndex);
			columnIndex++;	
			if (columnCount<columnIndex)
			{
				columnCount = columnIndex;
			}
			if (rowCounter<=1 && firstRowHeader)
			{
				cell.setCellStyle(headerStyle);
			}
			else
			{
				cell.setCellStyle(normalStyle);
			}
			
			cell.setCellValue(text);
		}
	}
	
	@Override
	public void addInteger(Integer liczba)
	{
		if (rowCounter <= maxFileSize)
		{
			HSSFCell cell = row.createCell(columnIndex);
			columnIndex++;	
			if (columnCount<columnIndex)
			{
				columnCount = columnIndex;
			}
			if (rowCounter<=1 && firstRowHeader)
			{
				cell.setCellStyle(headerStyle);
			}
			else
			{
				cell.setCellStyle(normalStyle);
			}
			
			cell.setCellValue(liczba);
		}
	}
	
	@Override
	public void addMoney(Number amount, boolean isComma)
	{
		//System.out.println("dziala");
		if (rowCounter <= maxFileSize)
		{
			HSSFCell cell = row.createCell(columnIndex);
			columnIndex++;	
			if (columnCount<columnIndex)
			{
				columnCount = columnIndex;
			}
			if (rowCounter<=1 && firstRowHeader)
			{
				cell.setCellStyle(headerStyle);
			}
			else
			{
				cell.setCellStyle(normalStyle);
			}

			cell.setCellValue(amount.doubleValue());
		}
	}
	@Override
	public void addMoney(Number amount, boolean isComma, int scale)
	{
		if (rowCounter <= maxFileSize)
		{
			HSSFCell cell = row.createCell(columnIndex);
			columnIndex++;	
			if (columnCount<columnIndex)
			{
				columnCount = columnIndex;
			}
			//log.error("Jestem tu");
			
			//HSSFDataFormat.getBuiltinFormat();
			//HSSFDataFormat form  = workbook.createDataFormat();
			//form.getBuiltinFormat((short)2);
			//HSSFCellStyle styl = workbook.createCellStyle();
			//styl.setDataFormat(form.getFormat("####################0.00"));
			//cell.setCellStyle(styl);
			cell.setCellValue(amount.doubleValue());
			//cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
		}
		
	}
	
	public void addDate(Date data, final String format)
	{
		if (rowCounter <= maxFileSize)
		{
			HSSFCell cell = row.createCell(columnIndex);
			columnIndex++;	
			if (columnCount<columnIndex)
			{
				columnCount = columnIndex;
			}
			HSSFCellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setDataFormat(workbook.createDataFormat().getFormat(format));
			cell.setCellStyle(cellStyle);
			Calendar cal = Calendar.getInstance();
			cal.setTime(data);
			cell.setCellValue(cal);
			//cell.setCellStyle()			
			//cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
		}
	}
	
	public void addDate(Date data)
	{
		addDate(data, "yyyy-MM-dd");
	}
	
	@Override
	public void dumpLine() throws IOException
	{
		//nic nie robi
	}
	
	@Override
	public void closeFile() throws IOException
	{
		for(int i=0; i < columnCount; i++)
                {
                    try
                    {          	
                        sheet.autoSizeColumn((short)i);
                    }
                    catch (Exception exc) 
                    {
                        log.warn("Blad w metodzie sheet.autoSizeColumn", exc);
                    }
                }
                FileOutputStream fis = new FileOutputStream(xlsFile);
                workbook.write(fis);
                fis.close();
	}

	public boolean isFirstRowHeader() {
		return firstRowHeader;
	}

	public void setFirstRowHeader(boolean firstRowHeader) {
		this.firstRowHeader = firstRowHeader;
	}
}
