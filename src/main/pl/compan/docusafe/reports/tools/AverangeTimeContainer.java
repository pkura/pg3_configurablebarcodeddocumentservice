package pl.compan.docusafe.reports.tools;

import java.math.BigDecimal;
/**
 * Klasa ktorej celem jest zliczanie ilosci oraz sredniego czasu pewnych zdarzen
 * @author wkutyla
 *
 */
public class AverangeTimeContainer 
{	
	
	public static final int MILLIS = 0;
	public static final int HOUR = 2;
	public static final int DAY = 3;
	
	static final Long HOUR_IN_MILLIS = 60l * 60l * 1000l;
	static final Long DAY_IN_MILLIS = 24l * HOUR_IN_MILLIS;
	BigDecimal counter =  new BigDecimal(0);
	/**
	 * Liczba probek
	 */
	private long sampleCounter = 0;
	
	
	public AverangeTimeContainer()
	{
		super();
	}
	/**
	 * Zliczamy czas w ms[]
	 * @param duration
	 */
	public void count(long duration)
	{
		sampleCounter++;
		counter = counter.add(new BigDecimal(duration));
	}
	
	/**
	 * Zlicza czas wydarzenia, ktore wydarza sie pomiedzy A i B w [ms]
	 * @param timeA
	 * @param timeB
	 */
	public void count (long timeA, long timeB)
	{
		count(timeB-timeA);
	}
	
	/**
	 * Zwracany jest sredni czas
	 * @return
	 */
	public double averangeDuration()
	{
		return averangeDuration(MILLIS);
	}
	
	/**
	 * Zwracamy sredni czas w okreslonej jednostce
	 * @param unit - jednostka - domyslnie sekunda
	 * @return
	 */
	public double averangeDuration(int unit)
	{
		BigDecimal samples = new BigDecimal(sampleCounter);
		BigDecimal averange = counter.divide(samples,5,BigDecimal.ROUND_HALF_EVEN);
		BigDecimal periodInMillis;
		if (unit == DAY)
		{
			periodInMillis = new BigDecimal(DAY_IN_MILLIS);			
		}
		else if (unit == HOUR)
		{
			periodInMillis = new BigDecimal(HOUR_IN_MILLIS);			
		}
		else
		{
			periodInMillis = new BigDecimal(1000l);
		}
		averange = averange.divide(periodInMillis,5,BigDecimal.ROUND_HALF_EVEN);
		return averange.doubleValue();
	}
}
