package pl.compan.docusafe.reports.tools;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.DateUtils;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import java.util.Iterator;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PDFDumper extends AbstractTableDumper {
	private static final Logger log = LoggerFactory.getLogger(PDFDumper.class);
	// rozmiar czcionki
        private int fontsize = 10;
        // plik zrzutu
	private File pdfFile;
	// pdf document
	private Document pdfDoc;
	// czcionka
	private Font font;
	// tabela do zrzutu
	private Table table;
	// lista wierszy
	private List<Row> rows = new ArrayList<Row>();
	// aktualnie przetwarzany wiersz
	private Row actualRow;
	//
	private int actualNum;
        private ArrayList<String> headerParagraph = new ArrayList<String>();
	
	@Override
	public void openFile(File file) throws IOException {
		this.pdfFile = file;
		File fontDir = new File(Docusafe.getHome(), "fonts");
		File arial = new File(fontDir, "arial.ttf");
		BaseFont baseFont = null;
		try {
			baseFont = BaseFont.createFont(arial.getAbsolutePath(),BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			pdfDoc = new com.lowagie.text.Document(PageSize.A4.rotate(),30,60,30,30);
			PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));
		} catch (DocumentException e) {
			log.error("",e);
		}
		font = new Font(baseFont, (float)fontsize);
		actualNum = 1;
	}
        
    @Override
	public void addParagraph(String title)
        {
            headerParagraph.add(title);
        }
	/**
	 * Metoda pomocnicza przy ustalaniu d�ugo�ci wiersza w pliku PDF.
	 * @return
	 */
	private int getMaxRowSize() {
		int max  = 0;
		for(Row r : rows) {
			if (r.getRowSize()>max)
				max = r.getRowSize();
		}
		return max;
	}
	
	/**
	 * Metoda generuje tabel� PDF ze struktur danych.
	 * @throws Exception
	 */
	private void prepareTable() throws Exception {
	    float[] widths = new float[getMaxRowSize()];
	    Arrays.fill(widths, 1l);
		
	    table = new Table(widths.length);
	    table.setWidths(widths);
	    table.setWidth(100);
	    table.setCellsFitPage(true);
	    table.setPadding(1);
	    
	    for(Row row : rows) {
	    	for(String s : row.getCells()) {
	    		addCell(s, row.isHeaderRow());
	    	}
	    }
		
	}
	
	/**
	 * Dodanie kom�rki do tabeli podanej jako argument oraz o podanej czcionce.
	 * @param name - nazwa kom�rki nag��wka
	 * @param table - tabela
	 * @param font - czcionka
	 * @return
	 * @throws Exception 
	 */
	private void addCell(String name, boolean header) throws Exception {
	  Cell cell = new Cell(new Phrase(name, font));
	  if (header) {
		  cell.setBorderWidth(1.2f);
		  cell.setBackgroundColor(new Color(192, 192, 192));
	  }
	  cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
          table.addCell(cell);
	}
	
        public void addCell(Cell cell) throws Exception
        {
            table.addCell(cell);
        }
        
	@Override
	public void closeFile() throws IOException {
		pdfDoc.open();
		try {
                        //dodaj� nag��wki ktore maja nie by� w tabeli
                        addParagraphs();
			// tutaj przygotowanie tabeli ze struktur danych
			prepareTable();
			// zrzucenie tabeli
			pdfDoc.add(table);
		} catch (Exception e) {
			log.error("",e);
		}
		pdfDoc.close();
	}
	
        public void addParagraphs() throws DocumentException
        {
            
            if(!headerParagraph.isEmpty())
            {
                Iterator it = headerParagraph.iterator();
                while(it.hasNext())
                {
                    String text = (String)it.next();
                    pdfDoc.add(new Paragraph(text));
                }
            }
        }

	@Override
	public void addDate(Date date) {
		addDate(date, "yyyy-MM-dd");
	}

	@Override
	public void addDate(Date date, String format) {
		addToLine(DateUtils.formatSqlDate(date));
	}

	@Override
	public void addInteger(Integer liczba) {
		if (liczba==null)
			actualRow.addCell("");
		else
			actualRow.addCell(liczba.toString());
	}

	@Override
	public void addMoney(Number amount, boolean isComma, int scale) {
		NumberFormat nFormat = NumberFormat.getInstance();
		nFormat.setMinimumFractionDigits(scale);
		nFormat.setMaximumFractionDigits(scale);
		String _amount = nFormat.format(amount) .toString();
		if (isComma)
			_amount = _amount.replace('.', ',');
		addToLine(_amount);
	}

	@Override
	protected void addToLine(String text) {
		if (text==null) text="";
		actualRow.addCell(text);
	}

	@Override
	public void dumpLine() throws IOException {
		//dodaje wiersz do listy
		rows.add(actualRow);
	}

	@Override
	public void newLine() {
		// inicjalizacja nowego wiersza
		if (actualNum++<2)
			actualRow = new Row(true);
		else
			actualRow = new Row();
	}

}
