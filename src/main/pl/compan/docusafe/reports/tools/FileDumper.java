package pl.compan.docusafe.reports.tools;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class FileDumper extends AbstractTableDumper {

    private static final Logger log = LoggerFactory.getLogger(FileDumper.class);

    private File file;
    @Override
    public void openFile(File file) throws IOException {

    }

    @Override
    public void newLine() {
        log.error("Nie robi nowej linii to jest sam plik");
    }

    @Override
    protected void addToLine(String text) {
        log.error("Nie robi nowej linii to jest sam plik");
    }

    @Override
    public void dumpLine() throws IOException {
        log.error("Nie robi nowej linii to jest sam plik");
    }

    @Override
    public void closeFile() throws IOException {
    }

    @Override
    public void addInteger(Integer liczba) {
        log.error("Nie robi nowej linii to jest sam plik");
    }

    @Override
    public void addMoney(Number amount, boolean isComma, int scale) {
        log.error("Nie robi nowej linii to jest sam plik");
    }

    @Override
    public void addDate(Date date) {
        log.error("Nie robi nowej linii to jest sam plik");
    }

    @Override
    public void addDate(Date date, String format) {
        log.error("Nie robi nowej linii to jest sam plik");
    }
}
