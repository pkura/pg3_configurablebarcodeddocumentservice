package pl.compan.docusafe.reports.tools;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.DateField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
/**
 * Klasa ktora pomaga raportom dostawac sie do danych zaleznych od Dockindu
 * @author wkutyla
 *
 */
public class DockindHelper 
{
	static final Logger log = LoggerFactory.getLogger(DockindHelper.class);
	
	/**
	 * Czy w zapytaniu ma sie pojawic tylko Id, czy *
	 */
	private boolean idOnly = true;
	
	/**
	 * Metoda, ktora zwraca podzapytanie zawierajace kryteria dockinda - warunek umiesznany w pytaniu glownym
	 * document_id in (select document_id from dsg_d...) 
	 * @param dockind
	 * @param params - parametry pytania
	 * @return kontener z zapytaniem
	 */
	
	public PsParameterContainer getDockindSubquery(String dockindCn,List<ReportParameter> params) throws Exception
	{
		return getDockindSubquery(dockindCn, params,1);
	}
	/**
	 * Metoda, ktora zwraca podzapytanie zawierajace kryteria dockinda - warunek umiesznany w pytaniu glownym
	 * document_id in (select document_id from dsg_d...) 
	 * @param dockind
	 * @param params - parametry pytania
	 * @param firstBinding - numer pierwszego parametru w PS
	 * @return kontener z zapytaniem
	 */
	public PsParameterContainer getDockindSubquery(String dockindCn,List<ReportParameter> params,int firstBinding) throws Exception
	{		
		DocumentKind kind = DocumentKind.findByCn(dockindCn);
		if (kind == null)
			throw new Exception("Nie zainicjowano dockindu "+dockindCn);
		Map<String,Field> fields = kind.getDockindInfo().getDockindFieldsMap();
		if (fields == null)
			throw new Exception("Nie zainicjowano mapy pol w dockindzie "+dockindCn);
		PsParameterContainer resp = new PsParameterContainer();
		resp.setFirstBinding(firstBinding);
		resp.setLastBinding(firstBinding-1);
		StringBuilder whereSb = new StringBuilder();
		StringBuilder fromSb = new StringBuilder();
		StringBuilder selectSb = new StringBuilder(); 
		selectSb.append("select ");
		if (idOnly)
		{
			selectSb.append("a1.document_id");
		}
		else
		{
			selectSb.append("a1.*");
		}
		whereSb.append(" where ");
		fromSb.append(" from ");
		fromSb.append(kind.getTablename()+" a1 ");
		for (ReportParameter param : params)
		{
			log.trace("param={}",param.getDockindCn());
			if (param.getDockindCn()!=null)
			{
				Field field = fields.get(param.getDockindCn());
				if(field != null && field.isMultiple())
				{
					fromSb.append(", "+kind.getMultipleTableName() +" a2 ");
					whereSb.append("a1.document_id = a2.document_id and ");
					break;
				}
			}
		}
		
		for (ReportParameter param : params)
		{
			if (param.getValue()!=null && param.getDockindCn()!=null)
			{
				log.trace("znalazlem parametr {}={}",param.getDockindCn(),param.getValue());
				//Field field = kind.getFieldByCn(param.getDockindCn());
				//tak jest szybciej
				Field field = kind.getDockindInfo().getDockindFieldsMap().get(param.getDockindCn());
				if (resp.isOn())
				{
					whereSb.append(" and ");
				}
				else
				{
					resp.setOn(true);
				}
				if (param.getValue() instanceof String)
				{
					if(field.isMultiple())
					{
						whereSb.append("a2.FIELD_CN = '").append(field.getCn()).append("'");
						whereSb.append(" and a2.FIELD_VAL = ? ");
						addVal(field,(String) param.getValue(),resp);						
					}
					else
					{
						whereSb.append("a1."+field.getColumn());
						String rel = getRelation(param);
						log.trace("rel={}",rel);
						whereSb.append(rel);
						whereSb.append("?");
						addVal(field,(String) param.getValue(),resp);
					}					
				}
				if (param.getValue() instanceof String[])
				{
					if(field.isMultiple())
					{
						whereSb.append("a2.FIELD_CN = '").append(field.getCn()).append("'");
						whereSb.append(" and a2.FIELD_VAL");
					}
					else
					{
						whereSb.append("a1."+field.getColumn());
					}
					log.trace("in");
					whereSb.append(" in (");
					String[] pars = (String[]) param.getValue();
					boolean second = false;
					for (String val : pars)
					{
						addVal(field,val,resp);
						if (second)
						{
							whereSb.append(", ");
						}
						whereSb.append("?");
						second = true;
					}
					whereSb.append(")");
					
				}
			}
		}
		if (log.isDebugEnabled())
		{
			log.debug(resp.getSql().toString());
		}
		resp.setSql(new StringBuilder(selectSb.toString()+fromSb.toString()+whereSb.toString()));
		return resp;
	}
	
	
	/**
	 * Uzupelnia wlasciwa relacje dla obiektu ps
	 * @param param
	 * @return
	 */
	protected String getRelation(ReportParameter param)
	{
		if (param.getRelation()==null)
		{
			return " = ";
		}
		else if (param.getRelation().equalsIgnoreCase(ReportParameter.NOT_EQUAL))
		{
			return " <> ";
		}
		else if (param.getRelation().equalsIgnoreCase(ReportParameter.GREATER))
		{
			return " > ";
		}
		else if (param.getRelation().equalsIgnoreCase(ReportParameter.LESS))
		{
			return " < ";
		}
		else if (param.getRelation().equalsIgnoreCase(ReportParameter.GREATER_EQUAL) ||
				param.getRelation().equalsIgnoreCase(ReportParameter.FROM)
				)
		{
			return " >= ";
		}
		else if (param.getRelation().equalsIgnoreCase(ReportParameter.LESS_EQUAL) 
				|| param.getRelation().equalsIgnoreCase(ReportParameter.TO))
		{
			return " <= ";
		}
		log.warn("Nieznana relacja "+param.getRelation());
		return  " = ";
	}
	/**
	 * Dodajemy wartosc do kolejci
	 * @param field
	 * @param value
	 * @param container
	 */
	protected void addVal(Field field, String value, PsParameterContainer container)
	{		
		
		Object toAdd = value;
		if (field instanceof DateField)
		{
			java.util.Date dat = pl.compan.docusafe.util.DateUtils.parseJsDate(value);
			if (dat==null)
			{
				log.error("Zla data przyjalem dzisiejsza - cos z tym trzeba zrobic (moze wstepna walicacje kryteriow");
				dat = new java.util.Date();
			}	
			toAdd = dat;
		}				

			container.setLastBinding(container.getLastBinding()+1);
			container.getParamValues().add(toAdd);
			container.getParamTypes().add(field.getType());

	}
	/**
	 * Metoda wykonuje dowiazanie do obiektu ps przygotowanego w metodzie getSubkindQuery
	 * @param ps
	 * @param container
	 * @throws SQLException
	 */
	public void setUpPreparedStatement(PreparedStatement ps, PsParameterContainer container) throws SQLException
	{
		if (container.isOn())
		{
			Iterator<Object> itVal = container.getParamValues().iterator();
			Iterator<String> itType = container.getParamTypes().iterator();
			int licznik = container.getFirstBinding();
			while (licznik <= container.getLastBinding())				
			{
				String type = itType.next();
				Object val = itVal.next();
				if (log.isTraceEnabled())
				{
					log.trace("dowiazanie numer " + licznik + " typu " + type + "=" + val.toString());
				}
				if (Field.DATE.equals(type))
				{
					java.sql.Date dat = new java.sql.Date(((java.util.Date) val).getTime());
					ps.setDate(licznik, dat);
				}
				else
				{
					ps.setString(licznik, (String) val);
				}
				licznik ++;
			}
		}
		else
		{
			log.debug("brak kryteriow do polaczenia");
		}
	}
	public boolean isIdOnly() {
		return idOnly;
	}
	public void setIdOnly(boolean idOnly) {
		this.idOnly = idOnly;
	}
}
