package pl.compan.docusafe.reports.tools;

import java.io.File;
import java.io.IOException;

import org.jfree.data.general.PieDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractPieChart
{
	protected static final Logger log = LoggerFactory.getLogger(AbstractPieChart.class);
	public abstract void addPiecePie(String name,Double value);
	public abstract void setPieTitle(String title);
	public abstract String getPieTitle();
	public abstract PieDataset getDatset();
	public abstract void savePieChart(int width,int height,File file) throws IOException;
}
