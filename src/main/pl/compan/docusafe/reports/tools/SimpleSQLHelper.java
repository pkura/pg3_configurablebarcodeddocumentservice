package pl.compan.docusafe.reports.tools;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Klasa zawierajaca proste metody zwiazane z obsluga typow SQL
 * @author wkutyla
 *
 */
public class SimpleSQLHelper 
{
	
	static final Logger log = LoggerFactory.getLogger(SimpleSQLHelper.class);
	
	static final long DAY_IN_MILLIS = 1000l * 60l * 60l * 24l;
	/**
	 * Metoda parsuje date pochodzaca z krytetium tekstowego 
	 * @param date
	 * @return
	 */
	public java.sql.Date parseDate(String value)
	{
		return parseDate(value,0,null);
	}
	
	/**
	 * Metoda parsuje date ze wskazaniem
	 * @param value
	 * @param defaultValue
	 * @return
	 */
	public java.sql.Date parseDate(String value,java.util.Date defaultValue)
	{
		return parseDate(value,0,defaultValue);
	}
	
	
	/**
	 * Metoda parsuje date i godzine ze wskazaniem
	 * @param value
	 * @param defaultValue
	 * @return
	 */
	public java.sql.Date parseDateHour(String value, String hour,java.util.Date defaultValue)
	{
		return parseDateHour(value,hour,0,defaultValue);
	}
	/**
	 * Metoda parsuje date i przesuwa ja o wskazana liczbe dni
	 * Jest to przydatne przy budowaniu kryterium pomiedzy datami
	 * >= DATA1 AND < DATA2 
	 * @param value
	 * @param rollDay
	 * @return
	 */
	public java.sql.Date parseDate(String value,int rollDay)
	{
		return parseDate(value,rollDay,null);
		
	}
	
	
	/**
	 * Metoda parsuje date ze wskazaniem wartosci domyslnej w przypadku braku 
	 * @param value
	 * @param rollDay
	 * @param defaultValue
	 * @return
	 */
	public java.sql.Date parseDate(String value,int rollDay,java.util.Date defaultValue)
	{
		java.util.Date dat = pl.compan.docusafe.util.DateUtils.parseJsDate(value);
		if (dat==null && defaultValue!=null)
		{
			dat = defaultValue;
		}
		return addDayDate(dat,rollDay);
		
	}
	
	
	/**
	 * Metoda parsuje date ze wskazaniem wartosci domyslnej w przypadku braku 
	 * @param value
	 * @param rollDay
	 * @param defaultValue
	 * @return
	 */
	public java.sql.Date parseDateHour(String value,String hour, int rollDay,java.util.Date defaultValue)
	{
		java.util.Date dat = pl.compan.docusafe.util.DateUtils.parseJsDateTime(value + " " + hour);
		if (dat==null && defaultValue!=null)
		{
			dat = defaultValue;
		}
		return addDayDate(dat,rollDay);
		
	}
	
	/**
	 * Dodaje liczbe dni do daty
	 * @param date
	 * @param rollDay
	 * @return
	 */
	public java.sql.Date addDayDate(java.util.Date date,int rollDay)
	{
		if (date!=null)
		{
			long time = date.getTime();
			if (rollDay!=0)
			{
				long roll = DAY_IN_MILLIS * rollDay;
				time = time + roll;
			}
			return new java.sql.Date(time);
		}
		return null;
	}
	
	
}
