package pl.compan.docusafe.reports.tools;

import java.awt.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

public class SimplePieChart extends AbstractPieChart
{
	private DefaultPieDataset dataset;
	private String pieTitle;

	public void addPiecePie(String name,Double value) {
		if(dataset == null)
			dataset = new DefaultPieDataset();
		dataset.setValue(name, value);
	}

	@Override
	public void setPieTitle(String title)
	{
		pieTitle = title;
	}

	@Override
	public String getPieTitle()
	{
		return pieTitle;
	}

	@Override
	public PieDataset getDatset() {
		return dataset;
	}

	@Override
	public void savePieChart(int width, int height,File file) throws IOException
	{
		JFreeChart chart = ChartFactory.createPieChart3D(pieTitle, getDatset(), true, true, false);
		chart.setBackgroundImageAlpha(0.3f);
    	PiePlot3D plot = (PiePlot3D) chart.getPlot();
    	plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
    	plot.setCircular(false);
    	plot.setLabelGap(0.02);
    	plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{2}"));
    	ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
    	OutputStream  os = new FileOutputStream(file);
    	ChartUtilities.writeChartAsJPEG(os,chart, width, height, info);
	}
}
