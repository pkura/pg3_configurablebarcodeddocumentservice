package pl.compan.docusafe.reports.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.util.Date;

import org.apache.axis.utils.ByteArrayOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.util.DateUtils;
/**
 * Tool do raportow - klasa ktora zrzuca dane do pliku CSV
 * @author wkutyla
 *
 */
public class CsvDumper extends AbstractTableDumper
{
	static final Logger log = LoggerFactory.getLogger(CsvDumper.class);
	
	
	
    
	/**
	 * Separator pliku CSV
	 */
	private String fieldSeparator = ";";
	
	
	/**
	 * Kodowanie pliku CSV
	 */
	private String encoding = "windows-1250";
	
	/**
	 * Plik CSV
	 */
	private File csvFile = null;
	/**
	 * OuptutStream
	 */
	private OutputStream fo = null;
	
	
	private StringBuilder line = null;
	private boolean first = true;
	
	public CsvDumper()
	{
		
	}
	
	@Override
	public void openFile(File file) throws IOException
	{
		csvFile = file;
		fo = new FileOutputStream(file);
	}
	
	public void openStream()
	{
		fo = new ByteArrayOutputStream();
	}
	
	public OutputStream getStream()
	{
		return fo;
	}
	
	@Override
	public void newLine()
	{
		line = new StringBuilder();
		first = true;
	}
	
	@Override
	public void addInteger(Integer liczba)
	{
		addToLine(liczba.toString());
	}


	@Override
	protected void addToLine(String text)
	{
		if (!first) {
			line.append(fieldSeparator);
		} else {
			first = false;
		}

		if (text!=null) {
            if(StringUtils.containsAny(text,"\n;\" ")){
                line.append('"')
                    .append(text.replace("\"","\"\""))   // " -> "" zgodnie z csv
                    .append('"');
            } else {
			    line.append(text);
            }
		}
	}

	@Override
	public void dumpLine() throws IOException
	{
		if (line!=null)
		{
			IOUtils.write(line.toString(), fo, encoding);
			IOUtils.write("\r\n", fo);
		}
	}
	

	@Override
	public void closeFile() throws IOException
	{
		fo.flush();
		fo.close();		
	}
	
	public String getFieldSeparator() {
		return fieldSeparator;
	}

	public void setFieldSeparator(String fieldSeparator) {
		this.fieldSeparator = fieldSeparator;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	@Override
	public void addMoney(Number amount, boolean isComma, int scale) 
	{
		NumberFormat nFormat = NumberFormat.getInstance();
		nFormat.setMinimumFractionDigits(scale);
		nFormat.setMaximumFractionDigits(scale);
		String _amount = nFormat.format(amount) .toString();
		if (isComma)
			_amount = _amount.replace('.', ',');
		addToLine(_amount);
	}

	@Override
	public void addDate(Date date) 
	{
		addToLine(DateUtils.formatSqlDate(date));
		
	}

	@Override
	public void addDate(Date date, String format) 
	{
		addDate(date);		
	}


}
