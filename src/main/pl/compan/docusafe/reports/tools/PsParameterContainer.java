package pl.compan.docusafe.reports.tools;

import java.util.*;
/**
 * Klasa bedaca kontenerem zawierajacym dane pomocne przy tworzeniu kryteriow do obiektu PS
 * @author wkutyla
 *
 */
public class PsParameterContainer 
{
	/**
	 * Czy kryteria wystepuja
	 */
	private boolean on = false;
	
	/**
	 * Kryteria do zapytania doklejane do PS
	 */
	private StringBuilder sql = new StringBuilder();
	
	/**
	 * Typy parametrow dla PS oznaczenia pol sa tozsame z polami dockindFields z klasy Field
	 * @see 
	 */
	private Collection<String> paramTypes = new Vector<String>();
	
	/**
	 * Wartosci parametrow
	 */
	private Collection<Object> paramValues = new Vector<Object>();

	/**
	 * Numer pierwszego parametru w calym PSie
	 */
	private int firstBinding = 1;
	/**
	 * Numer ostatniego parametru w calym Psie
	 * Jesli nie bylo zadnych wiazan to lastBinding = firstBinding - 1
	 */
	private int lastBinding = 1;
	public boolean isOn() {
		return on;
	}

	public void setOn(boolean on) {
		this.on = on;
	}

	public StringBuilder getSql() {
		return sql;
	}

	public void setSql(StringBuilder sql) {
		this.sql = sql;
	}

	public Collection<Object> getParamValues() {
		return paramValues;
	}

	public void setParamValues(Collection<Object> paramValues) {
		this.paramValues = paramValues;
	}

	public int getFirstBinding() {
		return firstBinding;
	}

	public void setFirstBinding(int firstBinding) {
		this.firstBinding = firstBinding;
	}

	public int getLastBinding() {
		return lastBinding;
	}

	public void setLastBinding(int lastBinding) {
		this.lastBinding = lastBinding;
	}

	public Collection<String> getParamTypes() {
		return paramTypes;
	}

	public void setParamTypes(Collection<String> paramTypes) {
		this.paramTypes = paramTypes;
	}
}
