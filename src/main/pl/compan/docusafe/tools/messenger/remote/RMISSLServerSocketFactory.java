/*
 * RMISSLServerSocketFactory.java
 * 
 */
package pl.compan.docusafe.tools.messenger.remote;


import java.io.*;
import java.net.*;
import java.rmi.server.*;
import javax.net.ssl.*;
import javax.net.*;
import javax.net.ssl.*;


/**
 * SocketFactory ktore tworzy sockety do komunikacji poprzez ssl
 *
 * @author Piotr Komisarski
 */
@Deprecated
public class RMISSLServerSocketFactory
	implements RMIServerSocketFactory, Serializable {

    private static String ANON_CIPHER = "SSL_DH_anon_WITH_RC4_128_MD5";
    
    private static String[] CIPHERS = {ANON_CIPHER};
    
    public ServerSocket createServerSocket(int port)
	throws IOException
	{
                java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
                SSLServerSocketFactory socketFactory = (SSLServerSocketFactory)SSLServerSocketFactory.getDefault();
                SSLServerSocket ret = (SSLServerSocket)socketFactory.createServerSocket(port);
                ret.setEnabledCipherSuites(CIPHERS);
                ret.setEnableSessionCreation(true);
                return ret;
	}
}