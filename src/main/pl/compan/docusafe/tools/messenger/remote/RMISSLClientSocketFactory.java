/*
 * RMISSLClientSocketFactory.java
 * 
 */

package pl.compan.docusafe.tools.messenger.remote;


import java.io.*;
import java.net.*;
import java.rmi.server.*;
import javax.net.ssl.*;

/**
 * SocketFactory ktore tworzy sockety do komunikacji poprzez ssl
 * 
 * @author Piotr Komisarski
 */

@Deprecated
public class RMISSLClientSocketFactory
	implements RMIClientSocketFactory, Serializable {

    private static String ANON_CIPHER = "SSL_DH_anon_WITH_RC4_128_MD5";
    
    private static String[] CIPHERS = {ANON_CIPHER};
    
    public Socket createSocket(String host, int port)
	throws IOException
	{
	    SSLSocketFactory factory =
		(SSLSocketFactory)SSLSocketFactory.getDefault();
	    SSLSocket socket = (SSLSocket)factory.createSocket(host, port);
            socket.setEnabledCipherSuites(CIPHERS);
	    return socket;
	}
}
