/*
 * RemoteMessage.java
 *
 *
 */

package pl.compan.docusafe.tools.messenger.remote;

import java.io.Serializable;

/**
 *
 *  klasa, kt�r� serwer dostaje jako zapytanie.
 *
 *  AnswerListener jest po stronie klienta, weic nie ma potrzeby go serializowac (transient)
 *  title - nieuzywane, moze w przyszlosci
 *  username i password do autoryzacji uzytkownika
 * 
 * @author Piotr Komisarski
 */

@Deprecated
public class RemoteQuestionMessage implements Serializable {

    private transient AnswerListener answerListener;
    private String title;

    private String username;
    private String password;


    public RemoteQuestionMessage(String title, AnswerListener answerListener) {
        this.title = title;
        this.answerListener = answerListener;
    }






    public String getTitle() {
        return title;
    }

    public AnswerListener getAnswerListener() {
        return answerListener;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
