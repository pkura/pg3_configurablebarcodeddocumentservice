/*
 * AnswerListener.java
 * 
 * 
 */

package pl.compan.docusafe.tools.messenger.remote;

/**
 *
 * @author Piotr Komisarski
 * 
 * To jest interfejs, ktory zostanie odpalony przez DataManagera po stronie klienta (w serwerze nieuzywany), 
 * gdy ten dostanie odpowiedz.
 */

@Deprecated
public interface AnswerListener {

    public void actionPerformed(RemoteAnswerMessage ram);
    
}
