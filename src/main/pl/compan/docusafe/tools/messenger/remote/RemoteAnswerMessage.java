/*
 * RemoteAnswerMessage.java
 *
 */

package pl.compan.docusafe.tools.messenger.remote;

import java.io.Serializable;
import java.util.Date;

/**
 *  Klasa, kt�ra serwer musi odes�a� jako odpowied�.
 *
 *
 * @author Piotr Komisarski
 */

@Deprecated
public class RemoteAnswerMessage implements Serializable {

    public static final int ERROR = 0;
    public static final int WARNING = 1;
    public static final int INFO = 2;
    public static final int NONE = 3;


    private String title;
    private String content;
    private String messageAuthor;
    //imie uzytkownika, np. Romek  (Witaj Romek!)
    private String userName;
    private boolean authorization;
    //ERROR, WARNING, INFO albo NONE
    private int type;
    private Date date;


    public RemoteAnswerMessage() {
        date = new Date();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean getAuthorization() {
        return authorization;
    }

    public void setAuthorization(boolean arg) {
        this.authorization = arg;
    }

    public void setTitle(String arg) {
        this.title = arg;
    }

    public String getTitle() {
        return title;
    }

    public void setContent(String arg) {
        this.content = arg;
    }

    public String getContent() {
        return content;
    }

    public void setMessageAuthor(String arg) {
        this.messageAuthor = arg;
    }

    public String getMessageAuthor() {
        return messageAuthor;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
}
