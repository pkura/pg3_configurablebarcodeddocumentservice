/*
 * Execute.java
 * 
 * 
 */

package pl.compan.docusafe.tools.messenger.server;


import java.rmi.Remote;
import java.rmi.RemoteException;
import pl.compan.docusafe.tools.messenger.remote.RemoteAnswerMessage;
import pl.compan.docusafe.tools.messenger.remote.RemoteQuestionMessage;


/**
 * interfejs do komunikacji poprzez RMI
 *
 * @author Piter Boss
 */
public interface Execute extends Remote{

    
    
    //String getVersion();
    RemoteAnswerMessage askForMessage(RemoteQuestionMessage question) throws RemoteException;
}
