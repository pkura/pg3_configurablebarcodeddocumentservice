package pl.compan.docusafe.tools.messenger.server;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.tools.messenger.remote.RMISSLClientSocketFactory;
import pl.compan.docusafe.tools.messenger.remote.RMISSLServerSocketFactory;
import pl.compan.docusafe.tools.messenger.remote.RemoteAnswerMessage;
import pl.compan.docusafe.tools.messenger.remote.RemoteQuestionMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *  Klasa do otrzymywania zapytan od klientow i tworzenia odpowiedzi
 *
 * @author Piotr Komisarski
 */
public class ServerCore extends UnicastRemoteObject implements Execute
{

    private static final String VERSION_NUMBER = "0.0.0.1";
    private static List<Execute> stubReferences = new ArrayList<Execute>();
    private static Map<String, Integer> user2tasks = new HashMap<String, Integer>();
    private static Map<String, LinkedList<RemoteAnswerMessage>> listOfUser2Msgs = new HashMap<String, LinkedList<RemoteAnswerMessage>>();
    private static final Log log = LogFactory.getLog(ServerCore.class);
    private static Preferences prefs = Preferences.systemNodeForPackage(ServerCore.class);
    private static int port;
    private static Registry currentRegistry;
    static
    {
        port = prefs.getInt("port", 1099);
    }

    private ServerCore() throws RemoteException
    {
        super(0, new RMISSLClientSocketFactory(), new RMISSLServerSocketFactory());
    }

    //tu jest startowana komunikacja . Mozna to wywolywac wiele razy, nie trzeba stopowac. (np. po zmianie numeru portu)
    @Deprecated
    public static void runCommunication()
    {
        try
        {
            try{
                if (currentRegistry != null) currentRegistry.unbind("PiterBoss");
            }
            catch (NotBoundException nbe){}
            currentRegistry = LocateRegistry.createRegistry(port, new RMISSLClientSocketFactory(), new RMISSLServerSocketFactory());
            Execute server = new ServerCore();
            currentRegistry.rebind("PiterBoss", server);
            stubReferences.clear();
            stubReferences.add(server); //to jest po to zeby trzymal caly czas referencje do server
            //inaczej to na server-ze poleci GC i koniec z wiadomosciami
        }
        catch (Exception e)
        {
            log.error("ServerCore exception:", e);
            LogFactory.getLog("eprint").debug("", e);
        }
    }

    public String getVersion()
    {
        return VERSION_NUMBER;
    }

    public static void pushMessage(String author, String username, String title, String content, int type)
    {
        RemoteAnswerMessage ram = new RemoteAnswerMessage();
        ram.setMessageAuthor(author);
        ram.setTitle(title);
        ram.setContent(content);
        ram.setType(type);


        if (!listOfUser2Msgs.containsKey(username))
        {
            LinkedList<RemoteAnswerMessage> list = new LinkedList<RemoteAnswerMessage>();
            list.addLast(ram);
            listOfUser2Msgs.put(username, list);
        }
        else listOfUser2Msgs.get(username).addLast(ram);
    }

    /**
     *  t� funkcje na serwerze wywoluje kient. Remotequestion w argumentach pochodzi od klienta
     *  a zwracane remoteanswer wedruje do klienta
     */
    public RemoteAnswerMessage askForMessage(RemoteQuestionMessage question) throws RemoteException
    {
        RemoteAnswerMessage ram = new RemoteAnswerMessage();
        try
        {
            DSApi.openAdmin();
            if (question.getUsername() == null) throw new EdmException("");
            DSUser user = null;
            try
        	{
        		user = DSUser.findByUsername(question.getUsername());
        	}
        	catch (UserNotFoundException e) 
			{
        		 user = DSUser.findByExternalName(question.getUsername());
			}
            if (user == null) return ram;
            boolean authorization = user.verifyPassword(question.getPassword());
            ram.setAuthorization(authorization);
            if (!authorization) throw new EdmException("");
            ram.setUserName(user.getFirstname());

            TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
            int[] inty = taskList.getTaskCount(user);

            if (!user2tasks.containsKey(user.getName()))
            {
                user2tasks.put(user.getName(), inty[1]);
                ram.setTitle(createTitleNewMessages(inty[1]));
                ram.setContent(createContentAllMessages(inty[1]));
            }
            else if (user2tasks.get(user.getName()) < inty[1])
            {
                ram.setTitle(createTitleNewMessages(inty[1] - user2tasks.get(user.getName())));
                ram.setContent(createContentAllMessages(inty[1]));
                user2tasks.put(user.getName(), inty[1]);
            }
            else if (listOfUser2Msgs.containsKey(user.getName()) && listOfUser2Msgs.get(user.getName()).size() > 0)
            {
                RemoteAnswerMessage newRam = listOfUser2Msgs.get(user.getName()).removeFirst();
                newRam.setUserName(user.getName());
                newRam.setAuthorization(true);
                ram = newRam;
            }

            DSApi.close();
        }
        catch (EdmException ee)
        {
            DSApi._close();
            return ram;
        }
        return ram;
    }

    private String createTitleNewMessages(int newMessages)
    {
        return "Masz " + newMessages + " nowe zadania";
    }

    private String createContentAllMessages(int newMessages)
    {
        return "W sumie masz " + newMessages + " wszystkich zada�";
    }

    public static void setPort(int newPort)
    {
        port = newPort;
        prefs.putInt("port", newPort);
        try
        {
            prefs.flush();
        }
        catch (BackingStoreException bse)
        {
        }
        runCommunication();
    }

    public static int getPort()
    {
        return port;
    }
}