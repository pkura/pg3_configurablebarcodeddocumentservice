package pl.compan.docusafe.webwork;

/* User: Administrator, Date: 2006-02-07 14:12:15 */

/*
 * Copyright (c) 2002-2003 by OpenSymphony
 * All rights reserved.
 */

import com.opensymphony.xwork.ActionContext;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.interceptor.AroundInterceptor;
import com.opensymphony.xwork.interceptor.NoParameters;
import com.opensymphony.xwork.util.InstantiatingNullHandler;
import com.opensymphony.xwork.util.OgnlValueStack;
import com.opensymphony.xwork.util.XWorkConverter;
import com.opensymphony.xwork.util.XWorkMethodAccessor;

import java.util.Iterator;
import java.util.Map;

import pl.compan.docusafe.web.common.SkipNullify;

/**
 * 07/02/2006 (COM-PAN)
 * Parametry, kt�rych warto�ci� jest pusty napis s� konwertowane do warto�ci null.
 * Z konwersji niekt�rych parametr�w mo�na zrezygnowa� przy u�yciu adnotacji
 * SkipNullify u�ytej na klasie.
 * 10/02/2006 (COM-PAN)
 * Obs�uga adnotacji ParameterConversion.
 * 13/03/2006 (COM-PAN)
 * Obs�uga adnotacji SkipNullify zamiast ParameterConversion.
 * <p/>
 * An interceptor that gets the parameters Map from the action context and calls
 * {@link com.opensymphony.xwork.util.OgnlValueStack#setValue(String, Object) setValue} on
 * the value stack with the property name being the key in the map, and the value
 * being the associated value in the map.
 * <p/>
 * This interceptor sets up a few special conditions before setting the values on
 * the stack:
 * <p/>
 * <ul>
 * <li>It turns on null object handling, meaning if the property "foo" is null and
 * value is set on "foo.bar", then the foo object will be created as explained
 * in {@link com.opensymphony.xwork.util.InstantiatingNullHandler}.</li>
 * <li>It also turns off the ability to allow methods to be executed, which is done
 * as a security protection. This includes both static and non-static methods,
 * as explained in {@link com.opensymphony.xwork.util.XWorkMethodAccessor}.</li>
 * <li>Turns on reporting of type conversion errors, which are otherwise not normally
 * reported. It is important to report them here because this input is expected
 * to be directly from the user.</li>
 * </ul>
 *
 * @author Patrick Lightbody
 */
public class ParametersInterceptor extends AroundInterceptor
{
    //~ Methods ////////////////////////////////////////////////////////////////

    protected void after(ActionInvocation dispatcher, String result) throws Exception
    {
    }

    protected void before(ActionInvocation invocation) throws Exception
    {
        if (!(invocation.getAction() instanceof NoParameters))
        {
            final Map parameters = ActionContext.getContext().getParameters();
            if (log.isDebugEnabled())
            {
                log.debug("Setting params " + parameters);
            }
            try
            {
                invocation.getInvocationContext().put(InstantiatingNullHandler.CREATE_NULL_OBJECTS, Boolean.TRUE);
                invocation.getInvocationContext().put(XWorkMethodAccessor.DENY_METHOD_EXECUTION, Boolean.TRUE);
                invocation.getInvocationContext().put(XWorkConverter.REPORT_CONVERSION_ERRORS, Boolean.TRUE);
                if (parameters != null)
                {
                    final OgnlValueStack stack = ActionContext.getContext().getValueStack();
                    for (Iterator iterator = parameters.entrySet().iterator(); iterator.hasNext();)
                    {
                        Map.Entry entry = (Map.Entry) iterator.next();
                        Object value = null;

                        // lista parametr�w, kt�rych nie nale�y sprowadza� do pojedynczego
                        // napisu lub do null
                        SkipNullify skipNullify = invocation.getAction().getClass().getAnnotation(SkipNullify.class);
                        if (skipNullify == null || !present(skipNullify.attributes(), entry.getKey().toString()))
                        {
                            // warto�ci, kt�re sk�adaj� si� z samych spacji (lub jednoelementowe
                            // tablice, kt�rych jedna warto�� sk�ada si� z samych spacji) zamieniane
                            // s� na warto�� null.  Mo�e to �le wsp�dzia�a� z niekt�rymi akcjami,
                            // dla kt�rych przewidziano adnotacj� ParameterConversion.
                            if (entry.getValue() instanceof String[] &&
                                ((String[]) entry.getValue()).length == 1)
                            {
                                String string = ((String[]) entry.getValue())[0];
                                if (string.length() == 0 || string.trim().length() == 0)
                                    value = null;
                                else
                                    value = string;
                            }
                            else
                            {
                                value = entry.getValue();
                            }
                        }
                        else
                        {
                            value = entry.getValue();
                        }
                        stack.setValue(entry.getKey().toString(), value);

/*
                        ParameterConversion conversion =
                            invocation.getAction().getClass().getAnnotation(ParameterConversion.class);
                        if (conversion != null && conversion.skip().length > 0)
                        {
                            skipNullify = conversion.skip().clone();
                            Arrays.sort(skipNullify);
                        }
                        if (skipNullify == null || Arrays.binarySearch(skipNullify, entry.getKey().toString()) < 0)
                        {
                            // warto�ci, kt�re sk�adaj� si� z samych spacji (lub jednoelementowe
                            // tablice, kt�rych jedna warto�� sk�ada si� z samych spacji) zamieniane
                            // s� na warto�� null.  Mo�e to �le wsp�dzia�a� z niekt�rymi akcjami,
                            // dla kt�rych przewidziano adnotacj� ParameterConversion.
                            if (entry.getValue() instanceof String[] &&
                                ((String[]) entry.getValue()).length == 1)
                            {
                                String string = ((String[]) entry.getValue())[0];
                                if (string.length() == 0 || string.trim().length() == 0)
                                    value = null;
                                else
                                    value = string;
                            }
                            else
                            {
                                value = entry.getValue();
                            }
                        }
                        else
                        {
                            value = entry.getValue();
                        }
                        stack.setValue(entry.getKey().toString(), value);
*/
                    }
                }
            }
            finally
            {
                invocation.getInvocationContext().put(InstantiatingNullHandler.CREATE_NULL_OBJECTS, Boolean.FALSE);
                invocation.getInvocationContext().put(XWorkMethodAccessor.DENY_METHOD_EXECUTION, Boolean.FALSE);
                invocation.getInvocationContext().put(XWorkConverter.REPORT_CONVERSION_ERRORS, Boolean.FALSE);
            }
        }
    }

    private boolean present(String[] haystack, String needle)
    {
        if (haystack == null || haystack.length == 0)
            return false;
        // tablice rzadko b�d� mia�y wi�cej ni� kilka element�w, wi�c klonowanie,
        // sortowanie klonu i wyszukiwanie w nim warto�ci nie ma sensu
        for (int i=0; i < haystack.length; i++)
        {
            if (haystack[i].equals(needle))
                return true;
        }
        return false;
    }
}