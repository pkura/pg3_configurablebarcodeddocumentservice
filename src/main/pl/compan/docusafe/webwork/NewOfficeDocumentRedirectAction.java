package pl.compan.docusafe.webwork;

import com.google.common.escape.Escaper;
import com.google.common.net.UrlEscapers;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.webwork.event.*;

/* User: Administrator, Date: 2005-08-04 13:09:17 */

/**
 * Pusta akcja, kt�ra zwraca wynik nationwide lub success zale�nie
 * od tego, czy w licencji obecny jest parametr extra "nationwide" lub "business".
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: NationwideRedirectAction.java,v 1.4 2007/04/04 12:35:41 lk Exp $
 */
public class NewOfficeDocumentRedirectAction extends EventActionSupport
{
    /** @var dodatkowa w�a�ciwo�� przekazywana w requescie. W przypadku, gdy odno�nik
     * prowadzi z innej formatki i ma zostac przekazany dalej */
    private String prop;
	private String messageId;
    private String documentKindCn;
	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	//albo wa�czone dla wszystkich menu.left.kancelaria.simpleprzyjmipismo albo kazdy sam musi sobie okreslic
        	if(AvailabilityManager.isAvailable("menu.left.kancelaria.simpleprzyjmipismo") )//|| DSApi.context().userPreferences().node("other").getBoolean("simpledocumentmainin", false))
        		 event.setResult("simple");
        	else if (AvailabilityManager.isAvailable("menu.left.kancelaria.dwrprzyjmipismo"))
        		event.setResult("dwr");
        	else if (Configuration.hasExtra("business") )
			{
        			event.setResult("dockind");
			}
            else if (Configuration.hasExtra("nationwide"))
                event.setResult("nationwide");
        }
    }

	public String getMessageId() {
		return StringUtils.isNotEmpty(messageId) ? UrlEscapers.urlFormParameterEscaper().escape(messageId) : messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

    public String getProp() {
        return StringUtils.isNotEmpty(prop) ? UrlEscapers.urlFormParameterEscaper().escape(prop) : prop;
    }

    public void setProp(String prop) {
        this.prop = prop;
    }

    public String getDocumentKindCn() {
        return  StringUtils.isNotEmpty(documentKindCn) ? UrlEscapers.urlFormParameterEscaper().escape(documentKindCn) : documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn) {
        this.documentKindCn = documentKindCn;
    }
}
