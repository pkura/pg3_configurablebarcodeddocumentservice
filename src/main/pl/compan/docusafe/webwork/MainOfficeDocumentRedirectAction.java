package pl.compan.docusafe.webwork;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/* User: Administrator, Date: 2005-08-04 13:09:17 */

/**
 * Pusta akcja, kt�ra zwraca wynik nationwide lub success zale�nie
 * od tego, czy w licencji obecny jest parametr extra "nationwide" lub "business".
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: NationwideRedirectAction.java,v 1.4 2007/04/04 12:35:41 lk Exp $
 */
public class MainOfficeDocumentRedirectAction extends EventActionSupport
{
	private Long documentId;
	private String activity;
	private Boolean openViewer;
	
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(AvailabilityManager.isAvailable("menu.left.kancelaria.simpleprzyjmipismo") || 
        			AvailabilityManager.isAvailable("menu.left.kancelaria.przyjmijpismosimple"))
        		 event.setResult("simple");
        	else if(AvailabilityManager.isAvailable("kancelaria.ogolne.hidden"))
        		event.setResult("document-archive");
        	else
        		event.setResult("success");
        }
    }

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public Boolean getOpenViewer() {
		return openViewer;
	}

	public void setOpenViewer(Boolean openViewer) {
		this.openViewer = openViewer;
	}
}
