package pl.compan.docusafe.webwork;

import com.opensymphony.xwork.ActionSupport;

/**
 * Akcja zwracaj�ca zawsze warto�� {@link #SUCCESS}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SuccessAction.java,v 1.1 2004/08/13 14:47:05 lk Exp $
 */
public class SuccessAction extends ActionSupport
{
    public String execute() throws Exception
    {
        return SUCCESS;
    }
}
