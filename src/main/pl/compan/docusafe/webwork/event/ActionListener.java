package pl.compan.docusafe.webwork.event;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ActionListener.java,v 1.1 2004/08/12 15:30:38 lk Exp $
 */
public interface ActionListener
{
    void actionPerformed(ActionEvent event) throws Exception;
}
