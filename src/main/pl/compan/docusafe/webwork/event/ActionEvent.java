package pl.compan.docusafe.webwork.event;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.logging.Log;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.web.office.common.DwrDocumentMainTabAction;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * This class represents an event that is created on an incoming
 * request which triggers an ActionListener execution.
 * <p>
 * Additionally, the event has instance of the {@link Log} class
 * initialized with the name of the current (executing) class, ie.
 * the one inheriting from {@link EventActionSupport}.
 * <p>
 * The event also has an attribute map which enables user to pass
 * arbitrary objects between the listeners registered for this event.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ActionEvent.java,v 1.4 2009/02/17 09:50:17 wkuty Exp $
 */
public class ActionEvent
{
    private Log log;
    private ListenerChain listenerChain;
    private EventActionSupport action;
    private Map attributes;
    private boolean cancelled;
    private String result;

    private Set skipListeners;
    private Set executedListeners = new HashSet();

    public ActionEvent(ListenerChain listenerChain, EventActionSupport action)
    {
        this.listenerChain = listenerChain;
        if(action == null)
        	return;
        clearSessionDwrParams(action);
        this.action = action;
    }

    private void clearSessionDwrParams(EventActionSupport action) {
        try{
            boolean a = action.getClass().getName().contains("DocumentArchiveAction") ||
                    action.getClass().getName().contains("NewDockindDocumentAction")||
                    action.getClass().getName().contains("Dwr") ;
            if (!a)
            {
                HttpSession session = ServletActionContext.getRequest().getSession();
                session.removeAttribute(DwrFacade.DWR_SESSION_NAME);
                session.removeAttribute(DwrDocumentMainTabAction.FAST_ASSIGNMENT_SELECT_DIVISION);
                session.removeAttribute(DwrDocumentMainTabAction.FAST_ASSIGNMENT_SELECT_USER);
                session.removeAttribute(DwrDocumentMainTabAction.DELIVERY_TYPE);
            }
        }catch(Exception e){
        }
    }

    /**
     * 
     * @return
     * @deprecated - Akcje powinny uzywac swojego loggera. 
     */
    @Deprecated
    public Log getLog()
    {
        return log;
    }

    void setLog(Log log)
    {
        this.log = log;
    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String result)
    {
        this.result = result;
    }

    /**
     * Sets an attribute. Lifetime of the attribute is bounded
     * by the lifetime of the event, ie. until the request is
     * being processed by this action. The attributes do not survive
     * between requests.
     * @param name Name of the attribute.
     * @param attribute Value of the attribute, may be any object.
     */
    public void setAttribute(String name, Object attribute)
    {
        if (attributes == null)
            attributes = new HashMap();
        attributes.put(name, attribute);
    }

    /**
     * Returns named attribute value or null if the attribute
     * is missing.
     */
    public Object getAttribute(String name)
    {
        return attributes != null ? attributes.get(name) : null;
    }

    /**
     * Stops propagation of the event down the listener chain.
     */
    public void cancel()
    {
        cancelled = true;
    }

    public boolean isCancelled()
    {
        return cancelled;
    }

    public void skip(String name)
    {
        ActionListener listener = listenerChain.getNamedListener(name);
        if (listener == null)
            throw new IllegalArgumentException("No such listener: `"+name+"'.");
        if (executedListeners.contains(listener))
            throw new IllegalArgumentException("Listener `"+name+"' was already " +
                "executed.");
        if (skipListeners == null)
            skipListeners = new HashSet();
        skipListeners.add(listener);
    }
    
    public boolean hasListener(String name){
    	return null != listenerChain.getNamedListener(name);
    }

    public void skip(ActionListener listener)
    {
        if (skipListeners == null)
            skipListeners = new HashSet();
        skipListeners.add(listener);
    }

    void markAsExecuted(ActionListener listener)
    {
        executedListeners.add(listener);
    }

    boolean isSkip(ActionListener listener)
    {
        return skipListeners != null && skipListeners.contains(listener);
    }

    public void addActionError(String error)
    {
        action.addActionError(error);
    }

    public void addActionMessage(String message)
    {
        action.addActionMessage(message);
    }

    public void addActionMessage(Iterable<String> actionMessages) {
        if (actionMessages != null) {
            for (String message : actionMessages) {
                addActionMessage(message);
            }
        }
    }

    public Collection getActionMessages() {
        return action.getActionMessages();
    }

    public Collection getActionErrors() {
        return action.getActionErrors();
    }
}
