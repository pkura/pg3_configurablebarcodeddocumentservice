package pl.compan.docusafe.webwork.event;

import java.util.Enumeration;

import javax.servlet.http.HttpSession;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.web.office.common.DocumentArchiveTabAction;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ActionListenerWrapper.java,v 1.1 2004/08/12 15:30:38 lk Exp $
 */
class ActionListenerWrapper
{
    private ActionListener listener;
    private String name;

    public ActionListenerWrapper(String name, ActionListener listener)
    {
        if (listener == null)
            throw new NullPointerException("listener");   	
        
        
        this.name = name;
        this.listener = listener;
    }

    public String getName()
    {
        return name;
    }

    public ActionListener getListener()
    {
        return listener;
    }
}
