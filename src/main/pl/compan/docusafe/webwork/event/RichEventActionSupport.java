package pl.compan.docusafe.webwork.event;

import com.google.common.base.Optional;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

abstract public class RichEventActionSupport extends EventActionSupport {

    private static final Logger log = LoggerFactory.getLogger(RichEventActionSupport.class);

    /**
     * <p>Rejestruje metod� <code>registerListener</code> akcj� o nazwie <code>uncapitalize(listener.getClass().getSimpleName())</code>
     * (np. <code>class MyAwesomeListener</code> --> <code>"myAwesomeListener"</code>)</p>
     *
     * <p>Owija listener w kontekst Hibernate.</p>
     *
     * @param listener
     */
    public void hibernateListener(ActionListener listener) {

        String actionName = StringUtils.uncapitalize(listener.getClass().getSimpleName());

        log.info("[hibernateListener] registerListener({})", actionName);

        registerListener(actionName)
                .append(OpenHibernateSession.INSTANCE)
                .append(listener)
                .appendFinally(CloseHibernateSession.INSTANCE);
    }

    @Override
    abstract protected void setup();

    /**
     * <p>Ta sama funkcjonalno�� co <code>DSTransaction</code>
     * z dodatkiem wy�wietlania b��d�w na formatce metod�
     * <code>event.addActionError()</code>.</p>
     *
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     */
    public static class ActionTransaction extends DSTransaction {
        private final ActionEvent event;

        public ActionTransaction(ActionEvent event) {
            super();
            this.event = event;
        }

        @Override
        protected void handleError(String myMessage, Exception ex) {
            log.error(myMessage, ex);
            event.addActionError(getErrorMessage(ex));
        }
    }
}
