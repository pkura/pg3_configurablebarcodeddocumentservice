package pl.compan.docusafe.webwork.event;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Constants.java,v 1.1 2004/08/12 15:30:38 lk Exp $
 */
public final class Constants
{
    public static final String Package = Constants.class.getPackage().getName();
}
