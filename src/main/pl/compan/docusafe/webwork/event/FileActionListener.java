package pl.compan.docusafe.webwork.event;

import com.google.common.base.Optional;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

abstract public class FileActionListener extends RichActionListener {

    private static final Logger log = LoggerFactory.getLogger(FileActionListener.class);

    @Override
    public void action(ActionEvent event) throws Exception {}

    abstract public FileInfo fileAction(ActionEvent event) throws Exception;

    /**
     * Zwraca <code>OutputStream</code> z servlet response oraz ustawia headery:
     * <code>
     *     Content-Disposition: attachment; filename=fileInfo.filename
     *     Content-Type: fileInfo.contentType
     * </code>
     *
     * @param fileInfo
     * @return
     */
    private Optional<ServletOutputStream> getOutputStream(FileInfo fileInfo) {
        try {
            HttpServletResponse resp = ServletActionContext.getResponse();

            resp.setContentType(fileInfo.getContentType());
            resp.setCharacterEncoding(fileInfo.getCharset());
            resp.setHeader("Content-Disposition", "attachment; filename="+fileInfo.getFilename());

            log.debug("[getOutputStream] getting output stream");
            final ServletOutputStream outputStream = resp.getOutputStream();
            log.debug("[getOutputStream] outputStream = {}", outputStream);

            return Optional.fromNullable(outputStream);
        } catch(IOException e) {
            log.error("[getOutputStream] error", e);
            return Optional.absent();
        }
    }

    /**
     * Kontener na nazw� pliku, contentType oraz inputStream do tego pliku.
     */
    public static class FileInfo {
        private final String contentType;
        private String charset = "ISO-8859-2";
        private final String filename;
        private final InputStream inputStream;

        public FileInfo(String contentType, String filename, InputStream inputStream) {
            this.contentType = contentType;
            this.filename = filename;
            this.inputStream = inputStream;
        }

        public String getFilename() {
            return filename;
        }

        public InputStream getInputStream() {
            return inputStream;
        }

        public String getContentType() {
            return contentType;
        }

        public String getCharset() {
            return charset;
        }

        public FileInfo setCharset(String charset) {
            this.charset = charset;
            return this;
        }
    }


    /**
     * Wy�wietla error 404 z tekstem <code>text</code>
     * @param status
     * @param text
     */
    private void fallback(int status, String text, OutputStream out) {

        log.debug("[fallback] status = {}, text = {}, out = {}", status, text, out);

        try {
            ServletActionContext.getResponse().setStatus(status);
            ServletActionContext.getResponse().setContentType("text/html; charset=iso-8859-2");
            ServletActionContext.getResponse().setCharacterEncoding("iso-8859-2");

//            PrintWriter out = ServletActionContext.getResponse().getWriter();

            if(out == null) {
                out = ServletActionContext.getResponse().getOutputStream();
            }

            String html = "<html><head><title>B��d " + status + "</title></head><body><h1>B��d "+status+
                    " "+text+"</h1></body></html>";

            PrintWriter writer = new PrintWriter(out);

            writer.write(html);
            writer.flush();
            writer.close();
        } catch (Exception ex) {
            log.error("[fallback] error", ex);
            throw new IllegalStateException("Cannot show error: "+ex.getMessage());
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        ServletOutputStream out = null;
        try
        {
            final FileInfo info = fileAction(event);

            final Optional<ServletOutputStream> optionalOut = getOutputStream(info);

            log.debug("optionalOut = {}", optionalOut);
            if(optionalOut.isPresent())
            {
                out = optionalOut.get();

                final InputStream in = info.getInputStream();

                IOUtils.copy(in, out);
                out.flush();
                out.close();
            }
            else
            {
                fallback(404, "Nie znaleziono pliku", null);
                log.error("[actionPerformed] Error, cannot get output stream");
            }

        }
        catch(Exception ex)
        {
            fallback(500, "B��d: "+ex.getMessage(), out);
            log.error("[actionPerformed] Error, cannot get output stream", ex);
        }

    }
}
