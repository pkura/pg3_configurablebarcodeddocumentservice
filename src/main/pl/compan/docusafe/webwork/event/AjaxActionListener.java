package pl.compan.docusafe.webwork.event;

import com.google.gson.*;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.PrintWriter;
import java.util.Collection;

abstract public class AjaxActionListener extends RichActionListener {

    private static final Logger log = LoggerFactory.getLogger(AjaxActionListener.class);

    public abstract JsonElement ajaxAction(ActionEvent event) throws Exception;

    /**
     * Odpowiada fladze {@code failed} w json response.
     */
    protected boolean isFailed = false;

    @Override
    public void action(ActionEvent event) throws Exception {}

    protected JsonObject getJsonWrapper(JsonElement result, ActionEvent event)
    {
        JsonObject ret = new JsonObject();

        JsonArray errors = getJsonArrayFromCollection(event.getActionErrors());
        JsonArray messages = getJsonArrayFromCollection(event.getActionMessages());

        ret.add("errors", errors);
        ret.add("messages", messages);
        ret.add("result", result == null ? JsonNull.INSTANCE : result);

        return ret;
    }

    protected JsonArray getJsonArrayFromCollection(Collection collection) {
        JsonArray array = new JsonArray();

        for(Object c: collection)
        {
            if(c != null)
            {
                array.add(new JsonPrimitive(c.toString()));
            }
        }

        return array;
    }

    public JsonElement getParameterAsJson(String name) {
        String param = getParameter(name);

        if(param == null)
        {
            return JsonNull.INSTANCE;
        }
        else
        {
            final JsonParser parser = new JsonParser();

            return parser.parse(param);
        }
    }

    public void actionPerformed(ActionEvent event) throws Exception {
        try
        {
            JsonObject jsonWrapper = produceOutput(event);
            writeToResponse(jsonWrapper);
        }
        catch(Exception ex)
        {
            log.error("[actionPerformed] error", ex);
            addActionError(event, ex);
        }
    }

    protected JsonObject produceOutput(ActionEvent event) {
        JsonObject jsonWrapper;
        try
        {
            JsonElement output = ajaxAction(event);
            jsonWrapper = getJsonWrapper(output, event);
        }
        catch(Exception ex)
        {
            log.error("[actionPerformed] error", ex);
            addActionError(event, ex);
            jsonWrapper = getJsonWrapper(JsonNull.INSTANCE, event);

            isFailed = true;
        }
        jsonWrapper.addProperty("failed", isFailed);

        return jsonWrapper;
    }

    protected void writeToResponse(JsonObject jsonWrapper) throws Exception {
        writeToResponse(jsonWrapper.toString());
    }

    protected void writeToResponse(String responseString) throws Exception {
        PrintWriter out = null;
        try
        {
            ServletActionContext.getResponse().setContentType("application/json");
            ServletActionContext.getResponse().setCharacterEncoding("ISO-8859-2");

            out = ServletActionContext.getResponse().getWriter();
            out.write(responseString);
        }
        catch(Exception e)
        {
            throw new Exception(e);
        }
        finally
        {
            if(out != null)
            {
                out.flush();
                IOUtils.closeQuietly(out);
            }
        }
    }

    public void setFailed(boolean failed) {
        isFailed = failed;
    }

    public void markAsFailed() {
        isFailed = true;
    }
}
