package pl.compan.docusafe.webwork.event;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CloseHibernateSession.java,v 1.2 2004/10/05 15:27:38 lk Exp $
 */
public class CloseHibernateSession implements ActionListener
{
    public static final CloseHibernateSession INSTANCE = new CloseHibernateSession();

    public void actionPerformed(ActionEvent event)
    {
        try
        {
            DSApi.close();
        }
        catch (EdmException e)
        {
            event.getLog().error(e.getMessage(), e);
            event.addActionError("B��d podczas zamykania po��czenia z baz� danych");
        }
        catch (Exception e)
        {
            event.getLog().error(e.getMessage(), e);
        }
    }
}
