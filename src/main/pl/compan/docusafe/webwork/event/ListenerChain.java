package pl.compan.docusafe.webwork.event;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.DocumentArchiveTabAction;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Chain of ActionListener objects which are executed in order
 * they were appended.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ListenerChain.java,v 1.3 2006/02/20 15:42:52 lk Exp $
 */
public class ListenerChain
{
    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(
            ListenerChain.class.getPackage().getName(), null);
    private List listenerChain = new LinkedList();
    private ActionListenerWrapper finallyListener;
    private String name;

    /**
     * Private constructor, creating instances of this class
     * outside EventProcessingAction is prohibited.
     */
    ListenerChain(String name)
    {
        this.name = name;
    }

    List getChain()
    {
        return listenerChain;
    }

    ActionListener getNamedListener(String name)
    {
        for (Iterator iter=listenerChain.iterator(); iter.hasNext(); )
        {
            ActionListenerWrapper wrapper = (ActionListenerWrapper) iter.next();
            if (name.equals(wrapper.getName()))
            {
                return wrapper.getListener();
            }
        }
        return null;
    }

    ActionListenerWrapper getFinally()
    {
        return finallyListener;
    }

    private final ActionListener ADMIN_CHECK = new ActionListener() {
        public void actionPerformed(ActionEvent event) {
            if(!DSApi.context().isAdmin()){
                event.addActionError(sm.getString("CzynnoszDostepnaTylkoDlaAdministratora"));
                event.setResult("error");
                event.cancel();
            }
        }
    };

    /**
     * anuluje dalsze przetwarzenie je�li u�ytkownik nie jest adminem
     * @return
     */
    public ListenerChain appendAdminCheck(){
        return append(null, ADMIN_CHECK);
    }

    public ListenerChain append(ActionListener actionListener)
    {
        return append(null, actionListener);
    }

    /**
     * Appends an ActionListener to the listener chain. The <code>name</code>
     * may be used to refer to the ActionListener. If the <code>name</code>
     * is not null, it must be unique within the ListenerChain.
     * @param name
     * @param actionListener
     * @throws IllegalArgumentException If the <code>name</code> parameter
     *  is not unique within this ListenerChain.
     * @return The current instance of the ListenerChain (<code>this</code>).
     */
    public ListenerChain append(String name, ActionListener actionListener)
    {
        // checking uniqueness of the event name
        if (name != null)
        {
            for (Iterator iter=listenerChain.iterator(); iter.hasNext(); )
            {
                if (name.equals(((ActionListenerWrapper) iter.next()).getName()))
                    throw new IllegalArgumentException("There's already an event " +
                        "listener named '"+name+"'.");
            }
        }
        ActionListenerWrapper wrapper = new ActionListenerWrapper(name, actionListener);
        listenerChain.add(wrapper);
        return this;
    }

    public void appendFinally(ActionListener actionListener)
    {
        finallyListener = new ActionListenerWrapper(null, actionListener);
    }

    public String getName()
    {
        return name;
    }
}
