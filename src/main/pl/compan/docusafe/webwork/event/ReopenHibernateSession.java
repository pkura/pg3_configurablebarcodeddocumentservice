package pl.compan.docusafe.webwork.event;

import pl.compan.docusafe.core.DSApi;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ReopenHibernateSession.java,v 1.1 2005/09/16 11:16:13 lk Exp $
 */
public class ReopenHibernateSession extends OpenHibernateSession
{
    public static final ReopenHibernateSession INSTANCE = new ReopenHibernateSession();

    public void actionPerformed(ActionEvent event)
    {
        if (!DSApi.isContextOpen())
        {
            super.actionPerformed(event);
        }
    }
}
