package pl.compan.docusafe.webwork.event;

import com.google.gson.*;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

abstract public class AjaxSerializeActionListener extends AjaxActionListener {

    private static final Logger log = LoggerFactory.getLogger(AjaxSerializeActionListener.class);
    protected String rawInput;

    protected Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").create();

    @Override
    public final JsonElement ajaxAction(ActionEvent event) throws Exception {
        return null;
    }

    abstract public Object serializeAction(ActionEvent event) throws Exception;

    @Override
    public void actionPerformed(ActionEvent event) {
        try
        {
            processInput();
            Map<String, Object> mapWrapper = produceMapOutput(event);
            String response = gson.toJson(mapWrapper);
            writeToResponse(response);
        }
        catch(Exception ex)
        {
            log.error("[actionPerformed] error", ex);
            addActionError(event, ex);
        }
    }

    private void processInput() throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(ServletActionContext.getRequest().getInputStream(), writer, "UTF-8");
        rawInput = writer.toString();
//        processInput(json);
    }

    protected void processInput(String jsonString) {
    }

    protected Map<String, Object> produceMapOutput(ActionEvent event) {
        Map<String, Object> mapWrapper;
        try
        {
            Object output = serializeAction(event);
            mapWrapper = getMapWrapper(output, event);
        }
        catch(Exception ex)
        {
            log.error("[actionPerformed] error", ex);
            addActionError(event, ex);
            mapWrapper = getMapWrapper(null, event);

            isFailed = true;
        }
        mapWrapper.put("failed", isFailed);

        return mapWrapper;
    }

    private Map<String, Object> getMapWrapper(Object result, ActionEvent event) {
        Map<String, Object> ret = new HashMap<String, Object>();

        ret.put("errors", event.getActionErrors());
        ret.put("messages", event.getActionMessages());
        ret.put("result", result);

        return ret;
    }
}
