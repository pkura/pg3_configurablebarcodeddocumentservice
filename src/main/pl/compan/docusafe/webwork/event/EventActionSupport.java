package pl.compan.docusafe.webwork.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.security.auth.Subject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.boot.WatchDog;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.core.xes.XesLog;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionContext;
import com.opensymphony.xwork.ActionSupport;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EventActionSupport.java,v 1.37 2009/09/21 11:12:10 mariuszk Exp $
 */
public abstract class EventActionSupport extends ActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(EventActionSupport.class);  
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    private static final boolean PROFILE_ACTION_LISTENER_TIME = AvailabilityManager.isAvailable("profiling.listeners");
    public  String executedListener = "executedListener";
    private Map<String,ListenerChain> registeredListeners = new LinkedHashMap<String,ListenerChain>();
    /**
     * Default event name which should be used with {@link #registerListener}
     * to register action listener that will be invoked when no other listener
     * is triggered by the request parameters.
     * <p>
     * It has the value <code>pl.compan.docusafe.web.common.event.DEFAULT_ACTION</code>.
     */
    protected static final String DEFAULT_ACTION = EventActionSupport.class.getName()+".DEFAULT_ACTION";
    /**
     * wynik akcji, przekierowujacy do zasobu o adresie znajdujacym sie pod zmienna "redirectUrl"
     */
    protected static final String REDIRECT = "redirect-to-url";
    private String[] registeredSearchParameters;
     
    /**
     * Metoda wykonywana przed wywo�aniem setup().  Je�eli zwr�ci warto��
     * r�n� od null, warto�� ta jest zwracana przez {@link #execute()}
     * i dzia�anie akcji jest ko�czone.
     */
    protected String preExecute()
    {
        return null;
    }
    
    /**
     * Metoda wyciaga wszystkie parametry requestu i zapisuje je w logu
     * Zalecane jest wywolanie zawsze na poczatku fillForm
     * Unikac w akcji logowania (podanie hasla)
     * @param log
     */
    protected static void logRequest(org.slf4j.Logger log)
    {
    	log.trace("parametry:");
    	if (log.isTraceEnabled())
    	{
    		Map params = ServletActionContext.getRequest().getParameterMap();
    		Iterator itp = params.keySet().iterator();
    		while (itp.hasNext())
    		{
    			String key = (String) itp.next();
    			Object val = params.get(key);
    			log.trace("{}={}",key,val);
    		}
    		
    	}
    }

	 /**
     * Metoda wyciaga wszystkie parametry requestu i zapisuje je w logu
     * Zalecane jest wywolanie zawsze na poczatku fillForm
     * Unikac w akcji logowania (podanie hasla)
     * @param log
     */
    protected static void logRequest(pl.compan.docusafe.util.Logger log)
    {
    	log.trace("parametry:");
    	if (log.isTraceEnabled())
    	{
    		Map params = ServletActionContext.getRequest().getParameterMap();
    		Iterator itp = params.keySet().iterator();
    		while (itp.hasNext())
    		{
    			String key = (String) itp.next();
    			Object val = params.get(key);
    			log.trace("{}={}",key,val);
    		}

    	}
    }
    /**
     * For debugging only
     */
    /*
    @Override
    public void addActionError(String anErrorMessage) {
    	if(anErrorMessage == null || anErrorMessage.length() == 0){
    		Logger.getLogger("debug").debug("addActionError with empty message:");
    		Thread.dumpStack();
    	} else {
    		super.addActionError(anErrorMessage);
    	}
    }*/

    /**
     * <p>
     *     Modify result returned by {@code execute}.
     * </p>
     * @param result
     * @return
     */
    protected String modifyResult(String result) {
        return result;
    }

	@Override
    final public String execute() throws Exception
    {
    	pl.compan.docusafe.boot.WatchDog watchDog = new WatchDog(prepareWatchDogCode());
        String preResult = preExecute();
        if (preResult != null)
        {
            return modifyResult(preResult);
        }
      
        setup();

        ListenerChain listenerChain = null;
       
        for (Iterator<String> iter=registeredListeners.keySet().iterator(); iter.hasNext(); )
        {        	
            String action = iter.next();
            log.trace("listener check {}",action);
            // not expecting request parameter with this name
            if (DEFAULT_ACTION.equals(action))
                continue;

            String[] values = (String[]) ActionContext.getContext().getParameters().get(action);
            if (values == null || values.length == 0)
            {
                values = (String[]) ActionContext.getContext().getParameters().get(action+".x");
            }
            if (values == null || values.length == 0)
            {
                values = (String[]) ActionContext.getContext().getParameters().get("!event."+action);
            }
            if (values != null && values.length > 0 && values[0] != null &&
                values[0].toString().trim().length() > 0)
            {
            	log.trace("{} = true",action);
                listenerChain = registeredListeners.get(action);
              
                //eventName = action;
                break;
            }
        }
        String pc = ServletActionContext.getRequest().getContextPath();
        if(pc != null && pc.length() > 0)
        	Docusafe.setPageContext(pc.substring(1));
        else
        	Docusafe.setPageContext("");

        // if no appropriate listener was found, falling back for the
        // default listener
        if (listenerChain == null)
        {
            listenerChain = (ListenerChain) registeredListeners.get(DEFAULT_ACTION);
            if (listenerChain == null)
                return modifyResult(SUCCESS);
//                throw new IllegalArgumentException("No default event handler was " +
//                    "specified.");
        }

        // preparing the event object

        List chain = listenerChain.getChain();

        ActionEvent event = new ActionEvent(listenerChain, this);
        event.setLog(LogFactory.getLog(getClass()));
        
        boolean failed = false;

/*
        if (log.isDebugEnabled())
            log.debug("Wykonywanie akcji '"+listenerChain.getName()+"'");
*/
        StringBuilder profilingData = null;
        if(PROFILE_ACTION_LISTENER_TIME){
            profilingData = new StringBuilder();
        }

        // executing the listeners in chain
        // if a listener throws an exception, the message is added to
        // the errors collection but the exception is not rethrown
        for (Iterator iter=chain.iterator(); iter.hasNext(); )
        {
            ActionListenerWrapper wrapper = (ActionListenerWrapper) iter.next();

/*
            if (log.isDebugEnabled())
                log.debug("Wykonywanie "+wrapper.getListener());
*/

            if (event.isSkip(wrapper.getListener()))
            {
/*
                if (log.isDebugEnabled())
                    log.debug("Pomijanie "+wrapper.getListener()+" ("+wrapper.getName()+")");
*/
                continue;
            }

            try
            {
                if(PROFILE_ACTION_LISTENER_TIME){
                    long startTime = System.currentTimeMillis();
                    wrapper.getListener().actionPerformed(event);
                    profilingData.append(MessageFormat.format("profiling : {0} -> time {1} ms\n",
                            wrapper.getListener().getClass().getSimpleName(),
                            System.currentTimeMillis() - startTime));
                } else {
                	event.setAttribute(executedListener, listenerChain.getName());
                    wrapper.getListener().actionPerformed(event);
                    XesLog.doXesLogEvent((String)event.getAttribute(executedListener), this, event);
                }
                
 /*               if (log.isDebugEnabled())
                    log.debug("Wykonano akcj� "+wrapper.getListener()+": "+(System.currentTimeMillis()-startTime)+"ms");
*/
               // ServletActionContext.getRequest().getRequestURI(); 
              
                event.markAsExecuted(wrapper.getListener());
            }
            
            catch (Throwable t)
            {
                log.error(t.getMessage(), t);
                addActionError(sm.getString("eventProcessing.actionException",
                    t.toString()));

                ServletActionContext.getRequest().setAttribute("exception", t);
                failed = true;

                break; // for
            }
           
            if (event.isCancelled())
            {
                if (log.isDebugEnabled()){
                    StringBuilder listeners = new StringBuilder(100);
                    while (iter.hasNext())
                        listeners.append(((ActionListenerWrapper) iter.next()).getListener()+" ");
                        log.debug("Wydarzenie anulowanie, pomijanie: "+listeners);
                }
                break; // for
            }
        }

        // listener "finally" jest wykonywany zawsze, je�eli istnieje
        if (listenerChain.getFinally() != null)
        {
            try
            {
/*
                if (log.isDebugEnabled())
                    log.debug("Wykonywanie 'finally' "+listenerChain.getFinally().getListener());
*/
                if(PROFILE_ACTION_LISTENER_TIME){
                    long startTime = System.currentTimeMillis();
                    listenerChain.getFinally().getListener().actionPerformed(event);
                    profilingData.append(MessageFormat.format("profiling : {0} -> time {1} ms\n",
                            listenerChain.getFinally().getListener().getClass().getSimpleName(),
                            System.currentTimeMillis() - startTime));
                }   else {
                    listenerChain.getFinally().getListener().actionPerformed(event);
                }
                event.markAsExecuted(listenerChain.getFinally().getListener());
            }
            catch (Throwable t)
            {
                log.error(t.getMessage(), t);
                addActionError(sm.getString("eventProcessing.actionException",
                    t.toString()));
                failed = true;
            }
        }

        String webworkResult;

        if (event.getResult() == null)
        {
            // wiele akcji nie ma wyniku ERROR, wi�c przekierowanie do exception-report
            webworkResult = failed ? "exception-report" : SUCCESS;
        }
        else
        {
            webworkResult = event.getResult();
        }

        if(PROFILE_ACTION_LISTENER_TIME){
            log.info("=== profiling report ===\n{}", profilingData);
        }
/*
        if (log.isDebugEnabled())
            log.debug("Zwracanie "+webworkResult+" (failed="+failed+")");
*/

        if(DSApi.isContextOpen())
        {
        	DSApi.close();
        	log.error("ERR-001 context otwarty nie zamkni�ty "+ActionContext.getContext().getActionInvocation().getAction().getClass().getName());
        }
        watchDog.tick();
        return modifyResult(webworkResult);
    }

    /**
     * Metoda przeznaczona do wywo�ywania konretnych akcji z kodu
     * @param actionName
     * @return
     * @throws Exception
     */
    public final String execute(String actionName) throws Exception
    {
        String preResult = preExecute();
        if (preResult != null)
        {
            return preResult;
        }

        setup();

        ListenerChain listenerChain = registeredListeners.get(actionName);

        try{
            String pc = ServletActionContext.getRequest() .getContextPath();
            if(StringUtils.isNotBlank(pc))
                Docusafe.setPageContext(pc.substring(1));
            else
                Docusafe.setPageContext("");
        }catch(Exception e){
            Docusafe.setPageContext("");
        }

        // if no appropriate listener was found, falling back for the
        // default listener
        if (listenerChain == null)
        {
            listenerChain = (ListenerChain) registeredListeners.get(DEFAULT_ACTION);
            if (listenerChain == null)
                return SUCCESS;
        }

        // preparing the event object

        List chain = listenerChain.getChain();

        ActionEvent event = new ActionEvent(listenerChain, this);
        event.setLog(LogFactory.getLog(getClass()));

        boolean failed = false;

        StringBuilder profilingData = null;
        if(PROFILE_ACTION_LISTENER_TIME){
            profilingData = new StringBuilder();
        }

        // executing the listeners in chain
        // if a listener throws an exception, the message is added to
        // the errors collection but the exception is not rethrown
        for (Iterator iter=chain.iterator(); iter.hasNext(); )
        {
            ActionListenerWrapper wrapper = (ActionListenerWrapper) iter.next();

            if (event.isSkip(wrapper.getListener())){
                continue;
            }

            try
            {
                if(PROFILE_ACTION_LISTENER_TIME){
                    long startTime = System.currentTimeMillis();
                    wrapper.getListener().actionPerformed(event);
                    profilingData.append(MessageFormat.format("profiling : {0} -> time {1} ms\n",
                            wrapper.getListener().getClass().getSimpleName(),
                            System.currentTimeMillis() - startTime));
                } else {
                    wrapper.getListener().actionPerformed(event);
                }

                event.markAsExecuted(wrapper.getListener());
            }
            catch (Throwable t)
            {
                log.error(t.getMessage(), t);
                addActionError(sm.getString("eventProcessing.actionException",t.toString()));

                try{
                    ServletActionContext.getRequest().setAttribute("exception", t);
                }catch(Exception e){}
                failed = true;

                break; // for
            }

            if (event.isCancelled())
            {
                if (log.isDebugEnabled()){
                    StringBuilder listeners = new StringBuilder(100);
                    while (iter.hasNext())
                        listeners.append(((ActionListenerWrapper) iter.next()).getListener()+" ");
                    log.debug("Wydarzenie anulowanie, pomijanie: "+listeners);
                }
                break; // for
            }
        }

        // listener "finally" jest wykonywany zawsze, je�eli istnieje
        if (listenerChain.getFinally() != null)
        {
            try
            {
                if(PROFILE_ACTION_LISTENER_TIME){
                    long startTime = System.currentTimeMillis();
                    listenerChain.getFinally().getListener().actionPerformed(event);
                    profilingData.append(MessageFormat.format("profiling : {0} -> time {1} ms\n",
                            listenerChain.getFinally().getListener().getClass().getSimpleName(),
                            System.currentTimeMillis() - startTime));
                }   else {
                    listenerChain.getFinally().getListener().actionPerformed(event);
                }
                event.markAsExecuted(listenerChain.getFinally().getListener());
            }
            catch (Throwable t)
            {
                log.error(t.getMessage(), t);
                addActionError(sm.getString("eventProcessing.actionException",
                        t.toString()));
                failed = true;
            }
        }

        String webworkResult;

        if (event.getResult() == null)
        {
            // wiele akcji nie ma wyniku ERROR, wi�c przekierowanie do exception-report
            webworkResult = failed ? "exception-report" : SUCCESS;
        }
        else
        {
            webworkResult = event.getResult();
        }

        if(PROFILE_ACTION_LISTENER_TIME){
            log.info("=== profiling report ===\n{}", profilingData);
        }

        if(DSApi.isContextOpen())
        {
            DSApi.close();
            log.error("ERR-001 context otwarty nie zamkni�ty "+ActionContext.getContext().getActionInvocation().getAction().getClass().getName());
        }
        return webworkResult;
    }
    /**
     * Przygotowuje nazwe akcji i login uzytkownika dla obiektu watchdog
     */
    protected String prepareWatchDogCode()
    {       	
    	String login = "anonymous";
    	String name = ActionContext.getContext().getName();
    	//te informacje nie zawsze beda potrzebne
  	    		
    	Subject subject = (Subject) ActionContext.getContext().getSession().get(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);    	
    	if (subject!=null)
    	{
    		Set users = subject.getPrincipals(UserPrincipal.class);
    		if (users != null && users.size() > 0)
    		{
    			UserPrincipal principal = (UserPrincipal) users.iterator().next();
    			login = principal.getName();
    		}
    	}    		
    	return name+";"+login;
    }
    /**
     * Funkcja wywo�ywana w momencie tworzenia instancji klasy
     * dziedzicz�cej po EventProcessingAction. W tej funkcji
     * nale�y zarejestrowa� procedury obs�ugi wydarze�
     */
    protected abstract void setup();

    protected void registerSearchParameters(String[] names)
    {
        registeredSearchParameters = names;
    }

    protected String getSearchParameters()
    {
        if (registeredSearchParameters == null || registeredSearchParameters.length == 0)
            return "";

        StringBuilder result = new StringBuilder();

        try
        {
            for (int i=0; i < registeredSearchParameters.length; i++)
            {
                String name = registeredSearchParameters[i];
                Method getter = getClass().getMethod(
                    "get"+Character.toUpperCase(name.charAt(0))+
                    (name.length() > 1 ? name.substring(1) : ""), (Class[]) null);
                Object object = getter.invoke(this, (Object[]) null);
                if (object != null)
                {
                    if (result.length() > 0) result.append('&');
                    result.append(object.toString());
                }
            }

            return result.toString();
        }
        catch (NoSuchMethodException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (InvocationTargetException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Registers event listener for the specified event name and
     * returns an empty listener chain.
     * @param action Event name (request parameter name which will
     *  trigger execution of the listener chain).
     * @return Empty listener chain.
     */
    public ListenerChain registerListener(String action)
    {
        if (registeredListeners.get(action) != null)
            throw new IllegalArgumentException("W registeredListeners znajduje" +
                " si� ju� funkcja obs�ugi "+action+", registeredListeners="+
                registeredListeners);

        ListenerChain chain = new ListenerChain(action);
        registeredListeners.put(action, chain);

        return chain;
    }

    public String joinCollection(Collection coll, String str)
    {
        Collection<String> strColl = new ArrayList<String>(coll.size());
        for (Object obj : coll)
        {
            strColl.add(obj.toString());
        }
        return TextUtils.join(strColl, str);
    }

    public String joinCollection(Collection coll)
    {
        return joinCollection(coll, "\\n");
    }
    
    public String getText(String key)
    {
        if(ServletActionContext.getContext().getActionInvocation()!=null)
            if(ServletActionContext.getContext().getActionInvocation().getAction()!=null)
                return GlobalPreferences.loadPropertiesFile(ServletActionContext.getContext().getActionInvocation().getAction().getClass().getPackage().getName(),null).getString(key);
        return "";
    }

	public boolean isNewDocumentAction()
	{
		return false;
	}

	
	public String getExecutedListener()
	{
		return executedListener;
	}

	
	
}
