package pl.compan.docusafe.webwork.event;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.auth.AuthUtil;


/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OpenHibernateSession.java,v 1.2 2004/10/20 14:44:49 lk Exp $
 */
public class OpenHibernateSession implements ActionListener
{
    public static final OpenHibernateSession INSTANCE = new OpenHibernateSession();

    public void actionPerformed(ActionEvent event)
    {
        try
        {
            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
        }
        catch (EdmException e)
        {
            event.getLog().error(e.getMessage(), e);
            event.addActionError("Wyst�pi� b��d komunikacji z baz� danych, skontaktuj si� z administratorem systemu.");
        }
        catch (Exception e)
        {
        	e.printStackTrace();
            event.getLog().error(e.getMessage(), e);
        }
    }
}
