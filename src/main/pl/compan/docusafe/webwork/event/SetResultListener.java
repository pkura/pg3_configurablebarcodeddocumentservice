package pl.compan.docusafe.webwork.event;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SetResultListener.java,v 1.1 2004/08/12 15:30:38 lk Exp $
 */
public class SetResultListener implements ActionListener
{
    private String result;

    public SetResultListener(String result)
    {
        this.result = result;
    }

    public void actionPerformed(ActionEvent event)
    {
        event.setResult(result);
    }
}
