package pl.compan.docusafe.webwork.event;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;

public abstract class RichActionListener implements ActionListener {

    private static final Logger log = LoggerFactory.getLogger(RichActionListener.class);

    public void actionPerformed(ActionEvent event) throws Exception {
        try
        {
            action(event);

            try {
                onSuccess();
            } catch(Exception e) {
                log.error("[actionPerformed] onSuccess error", e);
            }
        }
        catch(Exception ex)
        {
            log.error("[actionPerformed] error", ex);
            event.addActionError(ex.getMessage());

            try {
                onError(ex);
            } catch (Exception e) {
                log.error("[actionPerformed] onError error", e);
            }
        }

        try {
            onComplete();
        } catch (Exception e) {
            log.error("[actionPerformed] onComplete error", e);
        }
    }

    public abstract void action(ActionEvent event) throws Exception;

    /**
     * Wywo�ywane po wykonaniu akcji (po onSuccess)
     * @throws Exception
     */
    public void onComplete() throws Exception {}

    /**
     * Wywo�ywane po bezb��dnym wykonaniu akcji (uruchamiane przed onComplete)
     * @throws Exception
     */
    public void onSuccess() throws Exception {}

    /**
     * Wywo�ywane, gdy akcja rzuci�a wyj�tkiem
     * @param ex
     * @throws Exception
     */
    public void onError(Exception ex) throws Exception {}

    protected void addActionError(ActionEvent event, Exception ex) {
        addActionError(event, ex.getMessage());
    }

    protected void addActionError(ActionEvent event, String msg) {
        event.addActionError(msg == null ? "B��d" : msg);
    }

    public String getParameter(String name) {
        return ServletActionContext.getRequest().getParameter(name);
    }

    public String getParameterOrDefault(String name, String def) {
        String ret = getParameter(name);
        return ret == null ? def : ret;
    }

    public String getParameterOrError(String name, String errorMessage) throws Exception
    {
        String param = ServletActionContext.getRequest().getParameter(name);

        if(param == null)
        {
            throw new EdmException(errorMessage+": "+name);
        }

        return param;
    }

    public String getParameterOrError(String name) throws Exception
    {
        return getParameterOrError(name, "Nie znaleziono parametru");
    }

    public Long getLongParameterOrError(String name) throws Exception
    {
        String param = getParameterOrError(name);

        Long ret;
        try
        {
            ret = Long.valueOf(param);
        }
        catch(NumberFormatException ex)
        {
            throw new EdmException("Nie mozna odczytac warto�ci liczbowej parametru '"+name+"': "+ex.getMessage());
        }

        return ret;
    }

    public Long getLongParameter(String name) throws Exception
    {
        String param = getParameter(name);

        Long ret;

        if(param != null) {
            try
            {
                ret = Long.valueOf(param);
            }
            catch(NumberFormatException ex)
            {
                throw new EdmException("Nie mozna odczytac warto�ci liczbowej parametru '"+name+"': "+ex.getMessage());
            }
        } else {
            ret = null;
        }

        return ret;
    }

    public Long getLongParameterOrDefault(String name, Long def) throws Exception {
        Long ret = getLongParameter(name);

        return ret == null ? def : ret;
    }

    public Integer getIntParameterOrDefault(String name, Integer def) throws Exception {
        return getLongParameterOrDefault(name, def.longValue()).intValue();
    }

    public String getRawInput() throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(ServletActionContext.getRequest().getInputStream(), writer, "UTF-8");
        return writer.toString();
    }

}
