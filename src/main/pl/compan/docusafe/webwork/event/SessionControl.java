package pl.compan.docusafe.webwork.event;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.ActionEventAdapter;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.workflow.DSjBPM;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;

/**
 * Klasa u�atwiaj�ca otwieranie i zamykanie sesji w ActionListenerach
 * @author Micha� Sankowski
 */
public class SessionControl {
	private final static Logger log = LoggerFactory.getLogger(SessionControl.class);

	public final static ActionListener OPEN_HIBERNATE_SESSION = OpenHibernateSession.INSTANCE;
	public final static ActionListener OPEN_JBPM_SESSION = new JbpmSessionOpener();
	public final static ActionListener OPEN_HIBERNATE_AND_JBPM_SESSION = new HibernateJbpmSessionOpener();

	public final static ActionListener CLOSE_JBPM_SESSION = new JbpmSessionClose();
	public final static ActionListener CLOSE_HIBERNATE_SESSION = CloseHibernateSession.INSTANCE;
	public final static ActionListener CLOSE_HIBERNATE_AND_JBPM_SESSION = new HibernateJbpmSessionCloser();

    public final static ActionListener SESSION_CLEAR = new HibernateSessionClear();



	private static class JbpmSessionOpener extends LoggedActionListener{
		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			DSjBPM.open();
		}
		@Override
		public Logger getLogger() {
			return log;
		}
		@Override
		public void handleException(Exception e, ActionEvent event) {
			getLogger().error(e.getLocalizedMessage(),e);
			event.addActionError(e.getLocalizedMessage());
		}
	}

	private static class HibernateJbpmSessionOpener extends LoggedActionListener{
		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
			DSjBPM.open();
            DSApi.context().setMessages(new ActionEventAdapter(event));
		}

		@Override
		public void handleException(Exception e, ActionEvent event) {
			getLogger().error(e.getLocalizedMessage(),e);
			event.addActionError(e.getLocalizedMessage());
		}

		@Override
		public Logger getLogger() {
			return log;
		}
	}

	private static class HibernateJbpmSessionCloser extends LoggedActionListener{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            DSApi.context().clearMessages();
			DSjBPM.closeContextQuietly();
			DSApi.close();
		}

		@Override
		public void handleException(Exception e, ActionEvent event) {
			getLogger().error(e.getLocalizedMessage(),e);
		}

		@Override
		public Logger getLogger() {
			return log;
		}
	}

	private static class JbpmSessionClose extends LoggedActionListener{
		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			DSjBPM.closeContextQuietly();
		}
		@Override
		public Logger getLogger() {
			return log;
		}
		@Override
		public void handleException(Exception e, ActionEvent event) {
			getLogger().error(e.getLocalizedMessage(),e);
		}
	}

    private static class HibernateSessionClear extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            DSApi.context().session().clear();
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }
}
