package pl.compan.docusafe.webwork;

import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.Result;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ServletResult implements Result {
    private final static Logger LOG = LoggerFactory.getLogger(ServletResult.class);

    public void execute(ActionInvocation actionInvocation) throws Exception {
//        LOG.info("ServletResult");
    }
}
