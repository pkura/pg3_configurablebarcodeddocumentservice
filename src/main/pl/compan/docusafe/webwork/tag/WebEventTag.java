package pl.compan.docusafe.webwork.tag;

import com.opensymphony.webwork.views.jsp.ui.AbstractUITag;
import com.opensymphony.xwork.util.OgnlValueStack;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WebEventTag.java,v 1.5 2008/12/29 08:29:46 pecet1 Exp $
 */
public class WebEventTag extends AbstractUITag
{
    public static final String TEMPLATE = "web-event.vm";

    private String event;
    private String confirm;
    private String disabled;
    private String onclick;

    protected String getDefaultTemplate()
    {
        return TEMPLATE;
    }

    public void setEvent(String event)
    {
        this.event = event;
    }

    public void setConfirm(String confirm)
    {
        this.confirm = confirm;
    }

    public void setDisabled(String disabled)
    {
        this.disabled = disabled;
    }

    public void setOnclick(String onclick)
    {
        this.onclick = onclick;
    }

    protected void evaluateParams(OgnlValueStack stack) {
        if (valueAttr == null) {
            valueAttr = "'Submit'";
        }
        
        super.evaluateParams(stack);

        addParameter("event", findValue(event, String.class));
        addParameter("confirm", findValue(confirm, String.class));
        addParameter("disabled", findValue(disabled, Boolean.class));
        addParameter("onclick", findValue(onclick, String.class));
    }
}
