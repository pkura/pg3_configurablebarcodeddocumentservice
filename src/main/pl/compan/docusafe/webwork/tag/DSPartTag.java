package pl.compan.docusafe.webwork.tag;

import com.opensymphony.webwork.views.jsp.WebWorkBodyTagSupport;
import javax.servlet.jsp.JspException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.web.part.DefaultPartView;
import pl.compan.docusafe.web.part.PartView;
import pl.compan.docusafe.core.cfg.part.RenderingContextBean;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DSPartTag extends WebWorkBodyTagSupport{
	private static final Logger log = LoggerFactory.getLogger(DSPartTag.class);

	private String name;
	private boolean eval = false;
	PartView view = DefaultPartView.INSTANCE;
	RenderingContextBean rcBean = new RenderingContextBean();

	@Override
	public int doStartTag() throws JspException {
		try{
			view = ((DSPartContainerTag) findAncestorWithClass(this, DSPartContainerTag.class)).getView(this);
//			log.info("part tag name = " + name + " view = " + view);
			if (view.isRendered()) {
//				view.beforePart(bodyContent, pageContext, null);
				return EVAL_BODY_BUFFERED;
			} else {
				return SKIP_BODY;
			}
//			bodyContent.writeOut(bodyContent);
//			bodyContent.writeOut(pageContext.getOut());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return EVAL_BODY_BUFFERED;
		}
	}

	@Override
	public void doInitBody() throws JspException {
		super.doInitBody();
		try {
			rcBean.setPartName(name);
			rcBean.setContents(bodyContent);
			view.beforePart(rcBean);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public int doAfterBody() throws JspException {
		try {
			if (view.isRendered()) {
				rcBean.setContents(bodyContent);
				view.afterPart(rcBean);
//				this.bodyContent.writeOut(pageContext.getOut());
//				((DSPartContainerTag) findAncestorWithClass(this, DSPartContainerTag.class)).addPart(this);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
//			log.info("" + getBodyContent());
//			pageContext.popBody();
//			log.info("" + getBodyContent());
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		try{
			if (view.isRendered()) {
				bodyContent.writeOut(pageContext.getOut());
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return EVAL_PAGE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEval() {
		return eval;
	}

	public void setEval(boolean eval) {
		this.eval = eval;
	}
}
