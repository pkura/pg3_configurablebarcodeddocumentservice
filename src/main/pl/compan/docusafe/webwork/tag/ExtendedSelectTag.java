package pl.compan.docusafe.webwork.tag;
import com.opensymphony.webwork.views.jsp.ui.SelectTag;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public class ExtendedSelectTag extends SelectTag{

	private final static Logger LOG = LoggerFactory.getLogger(ExtendedSelectTag.class);

	@Override
	protected String getDefaultTemplate() {
		return "extended-select.vm";
	}


	@Override
	public int doEndTag() throws JspException {
		int ret = super.doEndTag();

		return ret;
	}
}
