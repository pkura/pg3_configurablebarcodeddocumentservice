package pl.compan.docusafe.webwork.tag;

import com.opensymphony.webwork.views.jsp.ui.AbstractUITag;
import com.opensymphony.xwork.util.OgnlValueStack;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SubmitEventTag.java,v 1.14 2008/12/29 08:32:15 pecet1 Exp $
 */
public class SubmitEventTag extends AbstractUITag {

	public static final String TEMPLATE = "submit-event.vm";
	private String confirm;
	private String disabled;
	private String validate;

	protected String getDefaultTemplate() {
		return TEMPLATE;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}

	public void setValidate(String validate) {
		this.validate = validate;
	}

	protected void evaluateParams(OgnlValueStack stack) {
		if (valueAttr == null) {
			valueAttr = "'Submit'";
		}


		super.evaluateParams(stack);

		addParameter("confirm", findValue(confirm, String.class));
		addParameter("disabled", findValue(disabled, Boolean.class));
		if (validate != null) {
			addParameter("validate", validate);
		}

	}
}
