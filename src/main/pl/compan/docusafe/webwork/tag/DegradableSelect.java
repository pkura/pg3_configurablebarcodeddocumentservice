package pl.compan.docusafe.webwork.tag;
import com.opensymphony.webwork.views.jsp.ui.SelectTag;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public class DegradableSelect extends SelectTag{

	private final static Logger LOG = LoggerFactory.getLogger(ExtendedSelectTag.class);

	@Override
	protected String getDefaultTemplate() {
		return "degradable-select.vm";
	}

}
