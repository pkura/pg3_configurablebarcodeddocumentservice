package pl.compan.docusafe.webwork.tag;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opensymphony.webwork.views.jsp.WebWorkBodyTagSupport;
import java.util.Map.Entry;
import javax.servlet.jsp.JspException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.cfg.part.DSPartsManager;
import pl.compan.docusafe.web.part.PartView;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DSPartContainerTag extends WebWorkBodyTagSupport{
	private static Logger log = LoggerFactory.getLogger(DSPartContainerTag.class);
	private String containerName;

	@Override
	public void doInitBody() throws JspException {
		try{
			bodyContent.append("<div id=\""+containerName+"_container\" class=\"container\">");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		super.doInitBody();
	}

//		= new DSPartsManager(
//			new DefaultPartController(),
//			Collections.<String, PartView>emptyMap()
//		);;

//	private Map<String, BodyContent> partsContent
//		= new LinkedHashMap<String, BodyContent>();

//	@Override
//	public int doStartTag() throws JspException {
//		return super.doStartTag();
//	}



	public DSPartsManager getPartsManager(){
		return (DSPartsManager) findValue("partsManager");
	}

	@Override
	public int doEndTag() throws JspException {
		try {
//			bodyContent.clear();
//			for(int i=0; i < partsContent.size(); ++i){
//				pageContext.popBody();
//			}
//			for (String name : getPartsManager().getContainerController(containerName).provideOrder(partsContent.keySet())) {
//				log.info("<< name " + name + " - " + partsContent.get(name).getString());
//				partsContent.get(name).writeOut(pageContext.getOut());
//			}
			bodyContent.append("</div>");
			bodyContent.append("<script>var containerStructure = eval('(");
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			gson.toJson(getPartsManager().getStructure(), bodyContent);
			bodyContent.append(")')\n");
			if(!getPartsManager().getPrefixes().isEmpty()){
				for(Entry<String, String> entry: getPartsManager().getPrefixes().entrySet()){
					bodyContent.append("partAttributes.prefixes."+entry.getValue()+"='"+entry.getKey()+"';\n");
				}
			}
			bodyContent.append("</script>");
			bodyContent.writeOut(pageContext.getOut());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return EVAL_PAGE;
	}

	private String partName(DSPartTag tag){
		return tag.isEval()? findValue(tag.getName()).toString() : tag.getName();
	}

	PartView getView(DSPartTag tag){
		PartView ret = getPartsManager().getPartView(partName(tag));
		return ret;
	}

//	void addPart(DSPartTag tag){
////		if(tag.getBodyContent() == null){
////			throw new IllegalArgumentException("Null bodyContent for tag " + partName(tag));
////		}
////		log.info("this = " + this);
////		log.info(">> name " + partName(tag) + " - " + tag.getBodyContent().getString());
////		partsContent.put(partName(tag), tag.getBodyContent());
//	}

	public String getName() {
		return containerName;
	}

	public void setName(String name) {
		this.containerName = name;
	}
}
