package pl.compan.docusafe.webwork.tag;

import com.opensymphony.webwork.views.jsp.IncludeTag;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DwrFieldsTag extends IncludeTag {

    {
        setPage("/dwrdockind/dwr-test.jsp");
    }
}
