package pl.compan.docusafe.webwork.tag;
import com.opensymphony.webwork.views.jsp.ui.AbstractUITag;
import com.opensymphony.xwork.util.OgnlValueStack;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.RenderBean;

/**
 *
 * @author Micha� Sankowski
 */
public class RenderBeanTag extends AbstractUITag{
	private final static Logger LOG = LoggerFactory.getLogger(RenderBeanTag.class);

	@Override
	protected String getDefaultTemplate() {
		return null;
	}

	@Override
	protected void evaluateParams(OgnlValueStack stack) {
		super.evaluateParams(stack);
		RenderBean rb = (RenderBean)findValue("bean", RenderBean.class);
		if(rb == null) rb = (RenderBean) stack.peek();
		addParameter("bean", rb);
		addParameter("i18n", rb.getStringManager());
	}
}
