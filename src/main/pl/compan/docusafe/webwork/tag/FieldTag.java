package pl.compan.docusafe.webwork.tag;

import com.google.common.base.Preconditions;
import com.opensymphony.webwork.views.jsp.WebWorkBodyTagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.dockinds.field.FieldFormatting;

import javax.servlet.jsp.JspException;

/**
 * Tag wyświetlający w odpowiedni sposób pole
 */
public class FieldTag extends WebWorkBodyTagSupport {
    private static final Logger LOG = LoggerFactory.getLogger(FieldTag.class);

    enum Mode {
        DEFAULT_FORMATTING
    }

    private String object = "fm.getValue(#cn)";
    private String formatting = "formatting";

    Mode mode;

    @Override
	public int doStartTag() throws JspException {
        
        FieldFormatting fFormatting = (FieldFormatting)findValue(formatting);
        Preconditions.checkNotNull(fFormatting, "'"+formatting+"' returns null");
        try {
            switch (mode) {
                case DEFAULT_FORMATTING:
                    Object obj = findValue(object);
                    Preconditions.checkNotNull(obj, "'" + object + "' returns null");
                    fFormatting.getDefaultFormatter().format(obj, pageContext.getOut());
                    break;
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new JspException(e);
        }
        
        return SKIP_BODY;
    }

    public String getMode(){
        return mode.toString();
    }

    public void setMode(String mode){
        try{
            this.mode = Mode.valueOf(mode.toUpperCase().replace("-","_"));
        } catch(Exception e){
            LOG.error(e.getMessage(), e);
        }
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getFormatting() {
        return formatting;
    }

    public void setFormatting(String formatting) {
        this.formatting = formatting;
    }
}
