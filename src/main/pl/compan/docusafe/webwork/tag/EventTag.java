package pl.compan.docusafe.webwork.tag;

import com.opensymphony.webwork.views.jsp.ui.AbstractUITag;
import com.opensymphony.xwork.util.OgnlValueStack;

/**
 * The Class EventTag.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EventTag.java,v 1.14 2009/03/06 12:45:59 pecet1 Exp $
 */
public class EventTag extends AbstractUITag {

	/** The Constant TEMPLATE. */
	public static final String TEMPLATE = "event.vm";
	
	/** The confirm. */
	private String confirm;
	
	/** The disabled. */
	private String disabled;
	
	/** The onclick. */
	private String onclick;
	
	/** Control type */
	private String type;

	/* (non-Javadoc)
	 * @see com.opensymphony.webwork.views.jsp.ui.AbstractUITag#getDefaultTemplate()
	 */
	protected String getDefaultTemplate() {
		return TEMPLATE;
	}

	/**
	 * Sets the confirm.
	 *
	 * @param confirm the new confirm
	 */
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	/**
	 * Sets control type
	 * @param type Control type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/* (non-Javadoc)
	 * @see com.opensymphony.webwork.views.jsp.ui.AbstractUITag#setDisabled(java.lang.String)
	 */
	@Override
	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}

	/**
	 * Sets the onclick.
	 *
	 * @param onclick the new onclick
	 */
	public void setOnclick(String onclick) {
		this.onclick = onclick;
	}

	/* (non-Javadoc)
	 * @see com.opensymphony.webwork.views.jsp.ui.AbstractUITag#evaluateParams(com.opensymphony.xwork.util.OgnlValueStack)
	 */
	@Override
	protected void evaluateParams(OgnlValueStack stack) {
		if (valueAttr == null) {
			valueAttr = "'Submit'";
		}

		super.evaluateParams(stack);

		addParameter("confirm", findValue(confirm, String.class));
		addParameter("disabled", findValue(disabled, Boolean.class));
		addParameter("onclick", findValue(onclick, String.class));
		addParameter("type", findValue(type, String.class));
	}

}
