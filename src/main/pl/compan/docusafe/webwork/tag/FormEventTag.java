package pl.compan.docusafe.webwork.tag;

import com.opensymphony.webwork.views.jsp.ui.FormTag;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FormEventTag.java,v 1.1 2004/10/28 14:52:28 lk Exp $
 */
public class FormEventTag extends FormTag
{
    // nazwy p�l poprzedzone _, aby nie przykrywa�y p�l o tych
    // samych nazwach w klasie nadrz�dnej
    final public static String _OPEN_TEMPLATE = "form-event.vm";
    final public static String _TEMPLATE = "form-event-close.vm";

    public String getOpenTemplate()
    {
        return _OPEN_TEMPLATE;
    }

    protected String getDefaultTemplate()
    {
        return _TEMPLATE;
    }
}
