package pl.compan.docusafe.webwork;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/* User: Administrator, Date: 2005-08-04 13:09:17 */

/**
 * Pusta akcja, kt�ra zwraca wynik nationwide lub success zale�nie
 * od tego, czy w licencji obecny jest parametr extra "nationwide" lub "business".
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: NationwideRedirectAction.java,v 1.4 2007/04/04 12:35:41 lk Exp $
 */
public class NewDocumentRedirectAction extends EventActionSupport
{
	private String documentKindCn;
	private String author;
	private boolean userSearch;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if (Configuration.hasExtra("business") )
			{
        		if(!AvailabilityManager.isAvailable("menu.left.repository.search"))
        			event.setResult("explore-documents");
        		else if(userSearch)
        		{
        			author = DSApi.context().getPrincipalName();
        			try
        			{
        				documentKindCn = DocumentKind.getDefaultKind();
        			}
        			catch (Exception e) {
						LOG.error(e.getMessage(),e);
					}
        			System.out.println(author+"  "+ documentKindCn);
        			event.setResult("userSearch");
        		}
        		else 
        			event.setResult("dockind");
			}
            else if (Configuration.hasExtra("nationwide"))
                event.setResult("nationwide");
        }
    }

	public String getDocumentKindCn() {
		return documentKindCn;
	}

	public void setDocumentKindCn(String documentKindCn) {
		this.documentKindCn = documentKindCn;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public boolean isUserSearch() {
		return userSearch;
	}

	public void setUserSearch(boolean userSearch) {
		this.userSearch = userSearch;
	}
}
