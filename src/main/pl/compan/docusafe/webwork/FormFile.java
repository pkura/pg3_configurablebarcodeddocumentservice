package pl.compan.docusafe.webwork;

import java.io.*;
import java.net.URLConnection;

import org.apache.commons.logging.LogFactory;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FormFile.java,v 1.6 2009/02/26 08:09:22 pecet3 Exp $
 */
public class FormFile
{
    private File file;
    private String name;
    private String contentType;
    private static String tempFile;
    
    static
    {
        try
        {
            File tmp = File.createTempFile("tmp", ".tmp");
            tempFile = tmp.getParent();
            tmp.delete();
        }
        catch (IOException e)
        {
        	LogFactory.getLog("eprint").debug("", e);
        }
        
    }

    public FormFile(File file, String name, String contentType)
    {
        this.file = file;
        this.name = name;
        this.contentType = contentType;
    }

    public FormFile(byte[] imageArray, String imageName) throws IOException 
    {
    	File tmpFile = null;  
    	tmpFile = new File(System.getProperty("java.io.tmpdir"), imageName); 
        OutputStream os = new FileOutputStream(tmpFile);
        os.write(imageArray);
        os.flush();
        os.close();
        this.file = tmpFile;
        this.name = imageName;
        this.contentType = null;
	}

	public boolean sensible()
    {
        return file != null && file.isFile() && file.canRead() && file.length() > 0;
    }

    public File getFile()
    {
        return file;
    }

    public String getName()
    {
        return name;
    }
    
    public String getNameWithoutExtension()
    {
    	return name.substring(0, name.lastIndexOf(".") != -1 ? name.lastIndexOf(".") : name.length());
    }
    public String getFileExtension()
    {
    	return name.substring(name.lastIndexOf(".") != -1? name.lastIndexOf(".") : name.length());
    	
    } 
    public String getContentType()
    {
        return contentType;
    }

    public String getContentTypeOrGuess() {
        if(contentType == null) {
            return URLConnection.guessContentTypeFromName(file.getName());
        } else {
            return contentType;
        }
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof FormFile)) return false;

        final FormFile formFile = (FormFile) o;

        if (contentType != null ? !contentType.equals(formFile.contentType) : formFile.contentType != null) return false;
        if (file != null ? !file.equals(formFile.file) : formFile.file != null) return false;
        if (name != null ? !name.equals(formFile.name) : formFile.name != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (file != null ? file.hashCode() : 0);
        result = 29 * result + (name != null ? name.hashCode() : 0);
        result = 29 * result + (contentType != null ? contentType.hashCode() : 0);
        return result;
    }

    public String toString()
    {
        return getClass().getName()+"[file="+file+"]";
    }
}
