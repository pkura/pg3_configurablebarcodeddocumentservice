package pl.compan.docusafe.webwork;

/**
 * Interfejs dodawany do akcji, kt�re chc� od�wie�a� swoj� stron� co jaki� czas.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: IRefreshAction.java,v 1.1 2005/04/06 16:22:42 lk Exp $
 */
public interface IRefreshAction
{
    /**
     * Zwraca true, je�eli strona ma by� od�wie�ana.
     */
    public boolean isRefresh();

    public int getRefreshTime();
}
