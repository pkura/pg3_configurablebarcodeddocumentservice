package pl.compan.docusafe.webwork;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.dispatcher.WebWorkResultSupport;
import com.opensymphony.xwork.ActionInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.AvailabilityManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: TemplateResult.java,v 1.4 2006/02/20 15:42:52 lk Exp $
 */
public class TemplateResult extends WebWorkResultSupport
{
    private static final Log log = LogFactory.getLog(TemplateResult.class);

    private String template;

    protected void doExecute(String location, ActionInvocation actionInvocation)
        throws Exception
    {
        ServletActionContext.getRequest().setAttribute("include.location", location);

        PageContext pageContext = ServletActionContext.getPageContext();

        if (pageContext != null)
        {
            pageContext.include(template);
        }
        else
        {
            HttpServletRequest request = ServletActionContext.getRequest();
            HttpServletResponse response = ServletActionContext.getResponse();
            RequestDispatcher dispatcher = request.getRequestDispatcher(template);

            /*
             * Nag��wek s�u�y do wy��czenia trybu zgodno�ci w IE8,
             * je�li strona (DocuSafe) jest dodany do strefy "Intranet"
             */
            if(AvailabilityManager.isAvailable("layout2")) {
            	response.addHeader("X-UA-Compatible", "IE=edge");
            }
            // if the view doesn't exist, let's do a 404
            if (dispatcher == null)
            {
                response.sendError(404, "result '" + template + "' not found");

                return;
            }

            // If we're included, then include the view
            // Otherwise do forward
            // This allow the page to, for example, set content type
            try
            {
                if (!response.isCommitted() && (request.getAttribute("javax.servlet.include.servlet_path") == null))
                {
                    request.setAttribute("webwork.view_uri", template);
                    request.setAttribute("webwork.request_uri", request.getRequestURI());

                    dispatcher.forward(request, response);
                }
                else
                {
                    dispatcher.include(request, response);
                }
            }
            catch (Exception e)
            {
                log.error(e.getMessage(), e);
                throw e;
            }
        }
    }

    public void setTemplate(String template)
    {
        this.template = template;
    }
}
