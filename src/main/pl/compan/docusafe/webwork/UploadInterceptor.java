package pl.compan.docusafe.webwork;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.util.MimetypesFileTypeMap;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.interceptor.Interceptor;

/**
 * Interceptor WebWork.  Wyszukuje w klasie Action metody o sygnaturze
 * set*(FormFile) i nadaje im warto�ci, je�eli pod podan� nazw�
 * przekazany zosta� plik.  Formularz u�ywaj�cy tego interceptora
 * musi by� przesy�any metod� POST i by� kodowany jako multipart/form-data.
 * <p>
 * Interceptor jest zdefiniowany w xwork.xml jako element stosu
 * uploadStack.  Stos ten musi by� dodany do ka�dej akcji, kt�ra chce
 * z niego korzysta�.
 * 
 * <p>
 * Je�li chcemu ladowac 
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UploadInterceptor.java,v 1.18 2010/01/18 12:32:03 mariuszk Exp $
 */
public class UploadInterceptor implements Interceptor
{
    private static final Log log = LogFactory.getLog(UploadInterceptor.class);

    public void destroy()
    {
    }

    public void init()
    {
    }

    public String intercept(ActionInvocation invocation) throws Exception
    {
    	try
    	{
	        if (ServletActionContext.getRequest() instanceof MultiPartRequestWrapper)
	        {
	        	
	            MultiPartRequestWrapper multiWrapper = (MultiPartRequestWrapper)
	                ServletActionContext.getRequest();
	
	            if (multiWrapper.hasErrors())
	            {
	                log.error(multiWrapper.getErrors());
	                //throw new Exception("Nie mo�na odczyta� przes�anego pliku, by� mo�e wyst�pi� konflikt dost�pu do pliku na serwerze.");
	                return invocation.invoke();
	            }
	
	            Enumeration e = multiWrapper.getFileNames();
	            LinkedList<String> files = new LinkedList<String>();
	            while (e.hasMoreElements())
	            {
	            	Object o = e.nextElement();
	            	files.add((String) o);	            	
	            }
	            Collections.sort(files);            
	            for (String inputValue : files)
	            {
	                // get the value of this input tag
	                //String inputValue = (String) e.nextElement();
	                String inputValueName;
	
	                if (inputValue != null && inputValue.length() > 0)
	                {
	                    try
	                    {
	                    	if (inputValue.startsWith("multiFiles"))
	                    	{
	                    		inputValueName = "multiFiles";
							}
	                    	else
	                    	{
	                    		inputValueName = inputValue;
	                    	}
	                        String methodName = "set"+Character.toUpperCase(inputValueName.charAt(0))+
	                            inputValueName.substring(1);
	                        
	                        Method setFormFile = invocation.getAction().getClass().getMethod(methodName, new Class[] { FormFile.class } );
	                        if(AvailabilityManager.isAvailable("kancelaria.wlacz.walidacjaZalacznikaPrzyTworzeniuPisma")){
	                        	if(multiWrapper.getFile(inputValue) == null && multiWrapper.getFilesystemName(inputValue) != null){
	                        		ServletActionContext.getRequest().setAttribute("pustyPlik", true);
	                        	}
	                        }
	                        File file = multiWrapper.getFile(inputValue);
	                        if (file != null)
	                        {
	                            FormFile formFile = new FormFile(file,
	                                file.getName(),
	                                MimetypesFileTypeMap.getInstance().getContentType(file.getName())
	                                /*multiWrapper.getContentType(inputValue)*/);
	                            setFormFile.invoke(invocation.getAction(), new Object[] { formFile });
	                        }
	                    }
	                    catch (NoSuchMethodException e1)
	                    {
	                        continue;
	                    }
	                }
	            }
	        }
    	}
    	catch (Exception e) 
    	{
			log.error("",e);
			throw new Exception(e);
		}
        return invocation.invoke();
    }
}
