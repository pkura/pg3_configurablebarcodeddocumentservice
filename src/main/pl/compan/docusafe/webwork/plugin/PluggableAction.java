package pl.compan.docusafe.webwork.plugin;
/* User: Administrator, Date: 2006-01-09 16:45:32 */

/**
 * Interfejs, kt�ry powinny implementowa� akcje - pluginy.
 *
 * Wydarzenia obs�ugiwane s� przez metody o sygnaturach postaci
 * performEvent(WebEvent), gdzie "Event" to nazwa wydarzenia odpowiadaj�ca
 * nazwie podanej w tagu ds:web-event.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PluggableAction.java,v 1.1 2006/02/07 14:55:18 lk Exp $
 */
public interface PluggableAction
{
    /**
     * Metoda wywo�ywana po ewentualnej obs�udze wydarze�, powinna s�u�y�
     * do wype�nienia p�l formularza.  Wywo�ywana r�wnie� w sytuacji, gdy
     * nie by�o wydarze� do obs�u�enia, np. przy pierwszym wy�wietleniu
     * formularza.
     * @param event Wydarzenie, kt�re wywo�ano wcze�niej dla klasy (dla tego
     * samego requestu).
     */
    void prep(WebEvent event);
}
