package pl.compan.docusafe.webwork.plugin;

import org.apache.commons.logging.Log;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
/* User: Administrator, Date: 2006-01-11 11:06:40 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: WebEvent.java,v 1.2 2006/02/20 15:42:52 lk Exp $
 */
public class WebEvent
{
    private PluginSupport action;
    private PluggableAction target;
    private String event;
    private HttpServletRequest httpServletRequest;
    private HttpServletResponse httpServletResponse;
    private Log log;

    private Map<String, Object> attributes;
    private String result;

    public WebEvent(PluginSupport action, PluggableAction target, String event, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Log log)
    {
        this.action = action;
        this.target = target;
        this.event = event;
        this.httpServletRequest = httpServletRequest;
        this.httpServletResponse = httpServletResponse;
        this.log = log;
    }

    public String getEvent()
    {
        return event;
    }

    public HttpServletRequest getHttpServletRequest()
    {
        return httpServletRequest;
    }

    public HttpServletResponse getHttpServletResponse()
    {
        return httpServletResponse;
    }

    public boolean hasAttribute(String name)
    {
        return attributes != null && attributes.get(name) != null;
    }

    public Object getAttribute(String name)
    {
        return attributes != null ? attributes.get(name) : null;
    }

    public void setAttribute(String name, Object value)
    {
        if (attributes == null)
            attributes = new HashMap<String, Object>();
        attributes.put(name, value);
    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String result)
    {
        this.result = result;
    }

    void setLog(Log log)
    {
        this.log = log;
    }

    public Log getLog()
    {
        return log;
    }

    public PluggableAction getTarget()
    {
        return target;
    }

    public PluginSupport getAction()
    {
        return action;
    }

    public String toString()
    {
        return "WebEvent[target="+target+" event="+event+"]";
    }
}
