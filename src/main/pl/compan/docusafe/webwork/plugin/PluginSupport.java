package pl.compan.docusafe.webwork.plugin;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionSupport;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

/* User: Administrator, Date: 2006-01-09 16:42:45 */

/**
 * TODO: klasa nieuko�czona
 *
 * Klasa dzia�aj�ca analogicznie do EventSupportAction, jednak�e wspieraj�ca
 * tak� konstrukcj� klas wewn�trznych obs�uguj�cych wydarzenia, aby klasy
 * te nie odwo�ywa�y si� bezpo�rednio do zmiennych instancyjnych klasy
 * zawieraj�cej (dziedzicz�cej po EventSupportAction), ale by pos�ugiwa�y
 * si� w�asnymi zmiennymi instancyjnymi.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PluginSupport.java,v 1.4 2008/10/06 11:01:11 pecet4 Exp $
 */
public abstract class PluginSupport extends ActionSupport
{
    private static final String PREFIX = "!event.am.";
    private Log log = LogFactory.getLog(this.getClass());

    private Map<String, PluggableAction> plugins = new LinkedHashMap<String, PluggableAction>();

    public final String execute() throws Exception
    {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse response = ServletActionContext.getResponse();

        WebEvent event = null;

        // poszukiwanie parametru powoduj�cego wywo�anie zdarzenia

        // TODO: !event. bez .am. powoduje wywo�anie eventu na this
        Enumeration e = request.getParameterNames();
        while (e.hasMoreElements())
        {
            String parameter = (String) e.nextElement();
            String value = request.getParameter(parameter);

            if (value != null && parameter.startsWith(PREFIX))
            {
                String eventLocation = parameter.substring(PREFIX.length());
                String[] tmp = eventLocation.split("\\.", 2);
                if (tmp.length == 2)
                {
                    PluggableAction target = plugins.get(tmp[0]);
                    String eventName = tmp[1];
                    if (target != null && hasEvent(target, eventName))
                    {
                        event = new WebEvent(this, target, eventName, request, response, log);
                    }
                    else
                    {
                        log.warn("Znaleziono klas� docelow�, ale nie obs�uguje ona ��danego wydarzenia. "+
                            "target="+target+" eventName="+eventName);
                    }
                }
                else
                {
                    log.warn("Znaleziono niepoprawny parametr maj�cy wywo�ywa� wydarzenie: "+parameter+"="+value);
                }
            }
        }

        if (event != null)
        {
            if (log.isDebugEnabled())
                log.debug("Wywo�ywanie wydarzenia "+event);
            callEvent(event);
        }

        for (PluggableAction action : plugins.values())
        {
            if (log.isDebugEnabled())
                log.debug("Wywo�ywanie prep dla "+action);
            action.prep(event != null && event.getTarget() == action ? event : null);
        }

        if (event != null && event.getResult() != null)
        {
            return event.getResult();
        }

        return SUCCESS;
    }

    private static boolean hasEvent(PluggableAction target, String eventName)
    {
        return getHandler(target, eventName) != null;
    }

    private static void callEvent(WebEvent event)
    {
        Method handler = getHandler(event.getTarget(), event.getEvent());
        try
        {
            handler.invoke(event.getTarget(), event);
        }
        catch (IllegalAccessException e)
        {
        	LogFactory.getLog("eprint").debug("", e);
        }
        catch (InvocationTargetException e)
        {
        	LogFactory.getLog("eprint").debug("", e);
        }
    }

    private static Method getHandler(PluggableAction target, String eventName)
    {
        try
        {
            return target.getClass().getMethod("perform"+ StringUtils.capitalise(eventName), WebEvent.class);
        }
        catch (NoSuchMethodException e)
        {
            return null;
        }
    }

    protected void setup() { }

    protected void registerPlugin(String event, PluggableAction action, String name)
    {
        if (name == null)
            throw new NullPointerException("name");

        if (plugins.containsKey(name))
            throw new IllegalArgumentException("Konflikt nazw akcji: "+name);

        plugins.put(name, action);
    }

    public Map<String, PluggableAction> getAm() { return plugins; }
}
