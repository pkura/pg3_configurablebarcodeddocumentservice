package pl.compan.docusafe;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.*;

/**
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class AdditionManager {

    private final static Logger log = LoggerFactory.getLogger(AdditionManager.class);

    public static String getProperty(String name) {
        return Docusafe.getAdditionProperty(name);
    }

    public static String getPropertySafe(String name) {
        String ret = Docusafe.getAdditionProperty(name);
        return ret == null ? "" : ret;
    }

    public static String getProperty(String name, String dockindCn) {
        return Docusafe.getAdditionProperty(name, dockindCn);
    }

    public static String getPropertyOrDefault(String name, String def) {
        return Docusafe.getAdditionPropertyOrDefault(name, def);
    }

    public static String getPropertyOrDefault(String name, String dockindCn, String def) {
        return Docusafe.getAdditionPropertyOrDefault(name, dockindCn, def);
    }

    /**
     * <p>
     *     Zwraca property jako kolekcj�, dziel�c po przecinku.
     *     Np. je�li mamy addsa "superLista=abc,a,b,c"
     *     to <code>getPropertyAsList("superLista")</code>
     *     zwr�ci list� <code>["abc", "a", "b", "c"]</code>.
     * </p>
     *
     * <p>
     *     Je�li adds nie istnieje lub jest pusty,
     *     wtedy zwracana jest pusta kolekcja.
     * </p>
     *
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     * @param name
     * @return lista string�w z addsa <code>name</code> (dzielone po przecinku)
     */
    public static List<String> getPropertyAsList(String name) {
        List<String> list = new ArrayList<String>();

        String str = getProperty(name);

        if(str == null) {
            return list;
        } else {
            final String[] split = StringUtils.split(str, ",");
            return new ArrayList<String>(Collections2.transform(Arrays.asList(split), new Function<String, String>() {
                @Override
                public String apply(String s) {
                    return s.trim();
                }
            }));
        }
    }

    /**
     * <p>
     *     Zwraca <code>getPropertyAsList(name)</code> je�li nie jest to pusta kolekcja.
     *     W przeciwnym razie zwraca <code>def</code>.
     * </p>
     *
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     * @param name nazwa addsa
     * @param def kolekcja zwracana gdy <code>getPropertyAsList(name)</code> zwr�ci pust� kolekcja.
     * @return <code>getPropertyAsList(name)</code> je�li nie jest to pusta kolekcja, w przeciwnym razie zwraca <code>def</code>.
     */
    public static List<String> getPropertyAsListOrDefault(String name, List<String> def) {
        List<String> ret = getPropertyAsList(name);
        return ret.isEmpty() ? def : ret;
    }

    public static Map<String, String> getPropertyAsMap(String name) {
        Map<String, String> map = new HashMap<String, String>();

        Collection<String> list = getPropertyAsList(name);

        for(String s: list) {
            String[] split = s.split(":");
            map.put(split[0], split[1]);
        }

        return map;
    }

    public static Long getLongProperty(String name)
    {
        String param = getProperty(name);

        Long ret = null;

        if(param != null) {
            try
            {
                ret = Long.valueOf(param);
            }
            catch(NumberFormatException ex)
            {
                log.error("[getLongProperty] Cannot parse number of property '{}'", name, ex);
                ret = 0L;
            }
        }

        return ret;
    }

    public static Long getLongPropertyOrDefault(String name, Long def) {
        Long ret = getLongProperty(name);

        return ret == null ? def : ret;
    }

    public static Integer getIntPropertyOrDefault(String name, Integer def) {
        return getLongPropertyOrDefault(name, def.longValue()).intValue();
    }

    public static boolean getBooleanProperty(String s) {
        return Boolean.parseBoolean(getProperty(s));
    }

    public static boolean getBooleanPropertyOrDefault(String s, boolean def) {
        Boolean b =  Boolean.parseBoolean(getProperty(s));
        return b == null ? def : b;
    }

    public static Integer getIntProperty(String name) {
        Long value = getLongProperty(name);
        return value == null ? null : value.intValue();
    }

    public static boolean isDefined(String name) {
        return getProperty(name) != null;
    }
}
