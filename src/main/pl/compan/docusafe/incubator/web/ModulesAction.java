package pl.compan.docusafe.incubator.web;

import pl.compan.docusafe.webwork.event.*;

import java.util.HashMap;
import java.util.Map;

/* User: Administrator, Date: 2006-01-09 14:01:47 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ModulesAction.java,v 1.4 2008/10/06 10:40:34 pecet4 Exp $
 */
public class ModulesAction extends EventActionSupport
{
    private Map m1 = new HashMap();
    private M n1 = new M("n2");

    {
        m1.put("m2", new M("m3"));
    }

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doSend").
            append(OpenHibernateSession.INSTANCE).
            append(new Send()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    static class M {
        String m3;

        public M(String m3)
        {
            this.m3 = m3;
        }

        public String getM3()
        {
            return m3;
        }

        public void setM3(String m3)
        {
            this.m3 = m3;
        }

        public String toString()
        {
            return "m3='"+m3+"'";
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    private class Send implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    public Map getM1()
    {
        return m1;
    }

    public M getN1()
    {
        return n1;
    }
}
