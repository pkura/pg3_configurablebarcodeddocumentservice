package pl.compan.docusafe.incubator.web;

import pl.compan.docusafe.webwork.plugin.PluggableAction;
import pl.compan.docusafe.webwork.plugin.WebEvent;
/* User: Administrator, Date: 2006-01-11 12:40:00 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: TestPluggableAction.java,v 1.3 2008/10/06 11:27:01 pecet4 Exp $
 */
public class TestPluggableAction implements PluggableAction
{
    private String m1;

    public void prep(WebEvent event)
    {
    }

    public String name()
    {
        return "test";
    }

    public void performUpdate(WebEvent event)
    {
    }

    public String getM1()
    {
        return m1;
    }

    public void setM1(String m1)
    {
        this.m1 = m1;
    }
}
