package pl.compan.docusafe.incubator.web;

import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DynamicResultsAction.java,v 1.3 2006/02/20 15:42:27 lk Exp $
 */
public class DynamicResultsAction extends EventActionSupport
{
    private List columns = new ArrayList(10);
    private List results = new ArrayList(10);

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(new FillForm());
    }

    public static class Column
    {
        private String property;
        private String title;
        private String sortDesc;
        private String sortAsc;

        public Column(String property, String title, String sortDesc, String sortAsc)
        {
            this.property = property;
            this.title = title;
            this.sortDesc = sortDesc;
            this.sortAsc = sortAsc;
        }

        public String getProperty()
        {
            return property;
        }

        public String getTitle()
        {
            return title;
        }

        public String getSortDesc()
        {
            return sortDesc;
        }

        public String getSortAsc()
        {
            return sortAsc;
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            columns.add(new Column("officeNumber", "Nr KO", getSortLink("officeNumber", false), getSortLink("officeNumber", true)));
            columns.add(new Column("referenceId", "Znak pisma", getSortLink("officeNumber", false), getSortLink("officeNumber", true)));

            Map bean;

            bean = new HashMap();
                bean.put("officeNumber", "5");
                bean.put("referenceId", "UAM/15");
                results.add(bean);
            bean = new HashMap();
                bean.put("officeNumber", "1/AG");
                bean.put("referenceId", "AG/12/2004");
                results.add(bean);
        }
    }

    public String getSortLink(String property, boolean ascending)
    {
        return null;
    }

    public List getColumns()
    {
        return columns;
    }

    public List getResults()
    {
        return results;
    }
}
