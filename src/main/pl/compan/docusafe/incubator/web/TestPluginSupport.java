package pl.compan.docusafe.incubator.web;

import pl.compan.docusafe.webwork.plugin.PluginSupport;
/* User: Administrator, Date: 2006-01-11 11:23:12 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: TestPluginSupport.java,v 1.3 2006/02/08 14:54:19 lk Exp $
 */
public class TestPluginSupport extends PluginSupport
{
    private String a1 = "A1";

    public TestPluginSupport()
    {
        registerPlugin("update", new TestPluggableAction(), "test");
        registerPlugin("update", new TestPluggableAction(), "text");
    }


    public String getA1()
    {
        return a1;
    }

    public void setA1(String a1)
    {
        this.a1 = a1;
    }

    public A getA() { return new A(); }

    public static class A {
        public String getB() { return "B"; }
    }
}
