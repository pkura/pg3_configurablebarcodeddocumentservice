package pl.compan.docusafe.integration;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 *     Konwerter dokument�w z/do format�w docx, doc, odt itd.
 *     Jest to klient aplikacji DocumentConverter.
 * </p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
@Service
public class DocumentConverter {

    private final static Logger log = LoggerFactory.getLogger(DocumentConverter.class);

    protected final RestTemplate restTemplate;

    @Autowired
    public DocumentConverter(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public File convert(File source, String sourceName, String sourceExt, String targetExt) throws Exception {
        String applicationUrl = getApplicationUrl();

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
        parts.add("file", new FileSystemResource(source));

        String url = applicationUrl + "/convert?sourceName={sourceName}&sourceExt={sourceExt}&targetExt={targetExt}";
        log.debug("[convert] url = {}, sourceName = {}, sourceExt = {}, targetExt = {}", url, sourceName, sourceExt, targetExt);

        ResponseEntity<String> base64result = restTemplate.postForEntity(url, parts, String.class, sourceName, sourceExt, targetExt);

        if(base64result.getStatusCode().is2xxSuccessful()) {
            return decodedResultToFile(base64result.getBody());
        } else {
            throw new Exception("Cannot decode file "+sourceName+", response: "+base64result.getStatusCode()+" "+base64result.getBody());
        }

    }

    public File addBarcodeToPdf(File sourcePdf, String barcode, String type, Integer deltaX, Integer deltaY) throws Exception {
        String applicationUrl = getApplicationUrl();

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
        parts.add("pdfFile", new FileSystemResource(sourcePdf));

        String url = applicationUrl + "/add-barcode?type={type}&barcode={barcode}&deltaX={deltaX}&deltaY={deltaY}";
        log.debug("[convert] url = {}, type = {}, barcode = {}, deltaX = {}, deltaY = {}", url, type, barcode, deltaX, deltaY);

        ResponseEntity<String> base64result = restTemplate.postForEntity(url, parts, String.class, type, barcode, deltaX, deltaY);

        if(base64result.getStatusCode().is2xxSuccessful()) {
            return decodedResultToFile(base64result.getBody());
        } else {
            throw new Exception("Cannot add barcode to file "+sourcePdf+", response: "+base64result.getStatusCode()+" "+base64result.getBody());
        }
    }

    private String getApplicationUrl() throws Exception {
        String applicationUrl = AdditionManager.getProperty("app.documentconverter.url");
        if(applicationUrl == null) {
            throw new Exception("[convert] applicationUrl not set");
        }
        return applicationUrl;
    }

    public boolean isAvailable() {
        return AvailabilityManager.isAvailable("app.documentconverter");
    }

    protected File decodedResultToFile(String base64result) throws IOException {
        byte[] result = Base64.decodeBase64(base64result);
        File file = File.createTempFile("document_converter_result", null);
        file.deleteOnExit();
        FileUtils.writeByteArrayToFile(file, result);
        return file;
    }

    protected List<String> getAvailableExtensions() {
        return AdditionManager.getPropertyAsList("app.documentconverter.extensions");
    }

    public static String getMimeType(String extension) {
        return AdditionManager.getPropertyAsMap("app.documentconverter.extensionToMimeType").get(extension);
    }
}
