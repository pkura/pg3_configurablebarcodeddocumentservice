package pl.compan.docusafe.spring;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import org.springframework.stereotype.Service;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.core.utils.ChecksumAlgorithm;
import pl.compan.docusafe.rest.views.DocumentView;
import pl.compan.docusafe.rest.views.RichDocumentView;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class SmartSearchService {

    private final static Logger log = LoggerFactory.getLogger(SmartSearchService.class);

    private static final String HISTORY_403_PROPERTY = "smartSearchAccessDenied";
    private static final String HISTORY_403_MESSAGE = "Nieuprawniona pr�ba dost�pu do pisma";

    /**
     * <p>
     *     Find document by barcode
     * </p>
     * @param barcode
     * @return
     * @throws EdmException
     */
    public SmartSearchDocumentResponse searchSingleDocument(String barcode) throws EdmException {
        final boolean addToHistoryOn403 = AvailabilityManager.isAvailable("smartsearch.addToHistoryOn403");

        boolean isBarcode = ChecksumAlgorithm.PG.isValid(barcode);
        if(isBarcode) {
            return findSingleByBarcode(barcode, addToHistoryOn403);
        } else {
            return new SmartSearchDocumentResponse(null, SmartSearchDocumentResponse.DocumentStatus.BAD_REQUEST, null);
        }
    }

    public List<RichDocumentView> findByBarcodePrefix(String barcode) throws EdmException {
        final boolean addToHistoryOn403 = AvailabilityManager.isAvailable("smartsearch.addToHistoryOn403");
        return findByBarcodePrefix(barcode, addToHistoryOn403);
    }

    /**
     * <p>
     *     Find documents by barcode prefix
     * </p>
     * @param barcode
     * @param addToHistoryOn403
     * @return
     * @throws EdmException
     */
    private List<RichDocumentView> findByBarcodePrefix(String barcode, final boolean addToHistoryOn403) throws EdmException {
        List<InOfficeDocument> list = InOfficeDocument.findByBarcodePrefix(barcode);

        if (!list.isEmpty()) {
            ImmutableList<RichDocumentView> retList = FluentIterable.from(list).transform(new Function<InOfficeDocument, RichDocumentView>() {
                @Override
                public RichDocumentView apply(InOfficeDocument document) {
                    try {
                        Document.checkDocument(document, document.getId());
                        RichDocumentView docView = new RichDocumentView(document);
                        docView.barcode = document.getBarcode();
                        return docView;
                    } catch (EdmException e) {
                        throw new RuntimeException(e);
                    }
                }
            }).toList();

            return (List<RichDocumentView>)new ArrayList<RichDocumentView>(retList);
        } else {
            return Collections.<RichDocumentView>emptyList();
        }

    }

    /**
     * <p>
     *     Find single document by barcode (exact match)
     * </p>
     * @param barcode
     * @param addToHistoryOn403
     * @return
     * @throws EdmException
     */
    protected SmartSearchDocumentResponse findSingleByBarcode(String barcode, boolean addToHistoryOn403) throws EdmException {
        List<InOfficeDocument> list = InOfficeDocument.findByBarcodePrefix(barcode);

        if (!list.isEmpty()) {
            InOfficeDocument document = list.get(0);
            return documentToSingleResponse(document, addToHistoryOn403, barcode);
        } else {
            return new SmartSearchDocumentResponse(null, SmartSearchDocumentResponse.DocumentStatus.NOT_FOUND, getNotFoundDivision());
        }
    }

    /**
     * <p>
     *     Convert Document object into SmartSearchDocumentResponse
     * </p>
     * @param document
     * @param addToHistoryOn403
     * @param barcode
     * @return
     * @throws EdmException
     */
    protected SmartSearchDocumentResponse documentToSingleResponse(InOfficeDocument document, boolean addToHistoryOn403, String barcode) throws EdmException {
        Optional<DSDivision> divisionOpt = getDivisionFromDocument(document);
        DSDivision division;
        if(divisionOpt.isPresent()) {
            division = divisionOpt.get();
        } else {
            division = getNotFoundDivision();
        }

        try {
            Document.checkDocument(document, document.getId());
            RichDocumentView documentView = new RichDocumentView(document);
            documentView.barcode = barcode;
            return new SmartSearchDocumentResponse(documentView, SmartSearchDocumentResponse.DocumentStatus.OK, division);
        } catch(AccessDeniedException ex) {
            if(addToHistoryOn403) {
                document.addHistoryEntry(HISTORY_403_PROPERTY, HISTORY_403_MESSAGE);
            }
            return new SmartSearchDocumentResponse(null, SmartSearchDocumentResponse.DocumentStatus.ACCESS_DENIED, division);
        } catch(DocumentNotFoundException ex) {
            return new SmartSearchDocumentResponse(null, SmartSearchDocumentResponse.DocumentStatus.NOT_FOUND, division);
        } catch(DocumentLockedException ex) {
            return new SmartSearchDocumentResponse(null, SmartSearchDocumentResponse.DocumentStatus.LOCKED, division);
        }
    }

    private Optional<DSDivision> getDivisionFromDocument(Document doc) throws EdmException {
        if(doc instanceof OfficeDocument) {
            String guid = ((OfficeDocument) doc).getDivisionGuid();
            try {
                DSDivision division = DSDivision.find(guid);
                return Optional.of(division);
            } catch(DivisionNotFoundException ex) {
                log.error("[getDivisionFromDocument] division not found, doc.id = {}, guid = {}", doc.getId(), guid);
                return Optional.absent();
            }
        } else {
            return Optional.absent();
        }
    }

    private DSDivision getNotFoundDivision() {
        DSDivision division = new DivisionImpl();
        division.setName("[Nieznany dzia� dokumentu]");
        return division;
    }
}
