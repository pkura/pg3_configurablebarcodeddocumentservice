package pl.compan.docusafe.spring;

import com.beust.jcommander.internal.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DSLog;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;

import java.util.Date;
import java.util.List;

/**
 * Klasa s�u��ca opakowaniu metod u�ywanych w Controllerach Akcji.
 * S�u�y do przenisienie logiki biznesowej poza kontroller, aby metody mo�na by�o u�y� w innym miejscu.
 *
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
@Controller
@Scope("prototype")
public class ContainerManager {

    private static final Logger log = LoggerFactory.getLogger(ContainerManager.class);

    /**
     * Metoda s�u�y opakowaniu metody aktualizacji obiektu sprawy. Metoda wyci�gni�ta z controllera EditCaseAction.Update
    *
     * @param officeCaseData
     */
    public String updateOfficeCase(OfficeCaseInfo officeCaseData) throws EdmException{
    	OfficeCase c = OfficeCase.find(officeCaseData.getId());
    	
        boolean closed = c.isClosed();

        DSUser user = validateClerk(officeCaseData, c);

        if(officeCaseData.getTitle().length()==0)
            throw new EdmException("Nie podano tytu�u sprawy");

        if(officeCaseData.getTitle().length()>199)
            throw new EdmException("Za d�ugi tytu� sprawy");

        if(officeCaseData.getDescription().length()>199)
            throw new EdmException("Za d�ugi opis sprawy");

        if (DSApi.context().hasPermission(DSPermission.SPRAWA_ZMIANA_TERMINU))
        {
            if (officeCaseData.getFinishDate() != null)
                c.setFinishDate(officeCaseData.getFinishDate().toDate());
        }

        if (officeCaseData.getPrecedent() != null)
        {
            OfficeCase precedent = OfficeCase.find(officeCaseData.getPrecedent());
            log.debug("precedent=" + precedent);
            c.setPrecedent(precedent);
        }
        else
        {
            c.setPrecedent(null);
        }

        c.setClerk(user.getName());
        c.setTitle(officeCaseData.getTitle());
        c.setPriority(CasePriority.find(officeCaseData.getPriority()));
        c.setDescription(officeCaseData.getDescription());

        if(officeCaseData.getStatus() != null)
            c.setStatus(CaseStatus.find(Integer.valueOf(officeCaseData.getStatus())));

        sendToBip(c, closed);
        return finishInOfficeDocument(c);
    }

    protected DSUser validateClerk(OfficeCaseInfo officeCaseData, OfficeCase c) throws EdmException {
        DSUser clerkToAdd = DSUser.findByIdOrNameOrExternalName(officeCaseData.getClerk());
        if(c.getClerk().equals(officeCaseData.getClerk()))
            return clerkToAdd;
        DSUser[] users = DSDivision.find(c.getDivisionGuid()).getUsers(true);
        boolean contain = false;
        for(DSUser user : users){
            if(user.getName().equals(clerkToAdd.getName()))
                contain = true;
        }

        if(!contain)
            throw new EdmException("Ten u�ytkownik nie mo�e by� refenrentem!");
        return clerkToAdd;
    }

    /**
     * Usuwa spraw�. Opakowana metoda EditCaseAction.Delete
     * @param id
     * @param reason
     */
    public void deleteOfficeCase(Long id, String reason) throws EdmException
    {
          if(reason == null || reason.equals(""))
            {
                throw new EdmException("Nale�y poda� pow�d usuni�cia sprawy");
            }
            OfficeCase c = OfficeCase.find(id);
            if(!c.getDocuments().isEmpty())
            {
                throw new EdmException("Nie mo�na usun�� sprawy poniewa� znajduj� sie w niej dokumenty");
            }

            c.getParent().getAudit().add(Audit.create("deleteOfficeCase", DSApi.context().getPrincipalName(),
                    "Usuni�to spraw� " + c.getOfficeId() + " z powodu: " + reason));

            DSLog l = new DSLog();
            l.setCode(DSLog.DELETE_CASE);
            l.setCtime(new Date());
            l.setParam(c.getOfficeId());
            l.setLparam(c.getId());
            l.setReason(StringUtils.left(reason, 200));
            l.setUsername(DSApi.context().getPrincipalName());
            l.create();
            c.delete();
    }

    protected static String finishInOfficeDocument(OfficeCase c) throws EdmException {
        List<String> messages = Lists.newArrayList();
        if(c.getStatus().isClosingStatus())
        {
            TaskList tasks = (TaskList) ServiceManager.getService(TaskList.NAME);
            List<Task> userTasks = tasks.getTasks(DSUser.findByUsername(DSApi.context().getPrincipalName()));
            List<OfficeDocument> docList = c.getDocuments();
            for (OfficeDocument officeDoc : docList)
            {
                for (Task task : userTasks)
                {
					if(task.getDocumentId().equals(officeDoc.getId()))
					{
						String activityId = WorkflowFactory.activityIdOfWfNameAndKey(task.getWorkflowName(),task.getActivityKey());
						WorkflowFactory.getInstance().manualFinish(activityId, false);
						if(officeDoc.getOfficeNumber()!=null)
						messages.add(officeDoc.getOfficeNumber().toString());
						if(officeDoc.getBarcode()!=null)
							messages.add("barcode : " + officeDoc.getBarcode());	
					}
                }
            }
        }
        if(!messages.isEmpty())
            return "Zako�czono prac� z dokumentami: " + messages.toString();

        return null;
    }

    protected static void sendToBip(OfficeCase c, boolean closed) throws EdmException {
        // do us�ugi BIP przekazywane s� tylko pisma wchodz�ce
        List documents = c.getDocuments(InOfficeDocument.class);

        // je�eli sprawa zosta�a zamkni�ta lub ponownie otwarta,
        // wysy�am do BIP informacj� o pismach w tej sprawie
        if (((closed && !c.isClosed()) || (!closed && c.isClosed())) && documents.isEmpty())
        {
                	/*
                    try
                    {
                        Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                        for (Iterator iter=documents.iterator(); iter.hasNext(); )
                        {
                            InOfficeDocument doc = (InOfficeDocument) iter.next();
                            bip.submit(doc);
                        }
                    }
                    catch (ServiceException e)
                    {
                    }
                	*/
            DataMartEventProcessors.DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_CASE_INFO).documentId(c.getId());
        }
    }

    /**
     * Obiekt reprezentuj�cy informacje o sprawie. Obiekt s�u�y do przekazywania do metod istniej�cych w klasie
     * ContainerManager.
     */
    public static class OfficeCaseInfo{
        private Long id;
        private String documentId;
        private String title;
        private String description;
        private String officeId;
        private Long portfolioId;
        /** @var @see CasePriority.id*/
        private Integer priority;
        private DateTime openDate;
        private DateTime finishDate;
        private boolean closed;
        private String status;
        private String author;
        private String clerk;
        private DateTime ctime;
        private Integer sequenceId;

        private Long precedent;

        public OfficeCaseInfo() {
        }

        public OfficeCaseInfo(OfficeCaseDto officeCaseDto) {
            this.id = officeCaseDto.getId();
            this.documentId = officeCaseDto.getDocumentId();
            this.title = officeCaseDto.getTitle();
            this.description = officeCaseDto.getDescription();
            this.officeId = officeCaseDto.getOfficeId();
            this.portfolioId = officeCaseDto.getPortfolioId();
            this.priority = officeCaseDto.getPriority();
            this.openDate = officeCaseDto.getOpenDate();
            this.finishDate = officeCaseDto.getFinishDate();
            this.status = officeCaseDto.getStatus();
            this.author = officeCaseDto.getAuthor();
            this.clerk = officeCaseDto.getClerk();
            this.ctime= officeCaseDto.getCtime();
            this.sequenceId = officeCaseDto.getSequenceId();
            this.precedent = officeCaseDto.getPrecedent();
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getDocumentId() {
            return documentId;
        }

        public void setDocumentId(String documentId) {
            this.documentId = documentId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getOfficeId() {
            return officeId;
        }

        public void setOfficeId(String officeId) {
            this.officeId = officeId;
        }

        public Long getPortfolioId() {
            return portfolioId;
        }

        public void setPortfolioId(Long portfolioId) {
            this.portfolioId = portfolioId;
        }

        public Integer getPriority() {
            return priority;
        }

        public void setPriority(Integer priority) {
            this.priority = priority;
        }

        public DateTime getOpenDate() {
            return openDate;
        }

        public void setOpenDate(DateTime openDate) {
            this.openDate = openDate;
        }

        public DateTime getFinishDate() {
            return finishDate;
        }

        public void setFinishDate(DateTime finishDate) {
            this.finishDate = finishDate;
        }

        public boolean isClosed() {
            return closed;
        }

        public void setClosed(boolean closed) {
            this.closed = closed;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getClerk() {
            return clerk;
        }

        public void setClerk(String clerk) {
            this.clerk = clerk;
        }

        public DateTime getCtime() {
            return ctime;
        }

        public void setCtime(DateTime ctime) {
            this.ctime = ctime;
        }

        public Integer getSequenceId() {
            return sequenceId;
        }

        public void setSequenceId(Integer sequenceId) {
            this.sequenceId = sequenceId;
        }

        public Long getPrecedent() {
            return precedent;
        }

        public void setPrecedent(Long precedent) {
            this.precedent = precedent;
        }
    }
}