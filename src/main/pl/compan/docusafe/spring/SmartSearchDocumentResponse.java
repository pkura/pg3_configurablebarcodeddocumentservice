package pl.compan.docusafe.spring;

import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.rest.views.DivisionView;
import pl.compan.docusafe.rest.views.DocumentView;

public class SmartSearchDocumentResponse {
    enum DocumentStatus {
        OK,
        ACCESS_DENIED,
        NOT_FOUND,
        LOCKED,
        BAD_REQUEST
    }

    public final DocumentView document;
    public final DocumentStatus status;
    public final DivisionView division;
    final public String type = "DOCUMENT";

    public SmartSearchDocumentResponse(DocumentView document, DocumentStatus status, DSDivision division) {
        this.document = document;
        this.status = status;
        this.division = division == null ? null : new DivisionView(division);
    }
}
