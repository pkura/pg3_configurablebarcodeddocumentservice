package pl.compan.docusafe.spring.user;

import com.google.common.base.Optional;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.rest.helpers.MultiOptional;
import pl.compan.docusafe.rest.views.AttachmentView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class AttachmentServiceInContext extends AttachmentService {

    private final HttpServletRequest request;

    public AttachmentServiceInContext(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public MultiOptional<AttachmentRevision> getAttachmentRevision(final Long revisionId) throws EdmException {
        return MultiOptional.unwrapFromOptional(DSTransaction.instance().executeOpenContext(request, new DSTransaction.Function<MultiOptional<AttachmentRevision>>() {
            @Override
            public MultiOptional<AttachmentRevision> apply() throws Exception {
                return AttachmentServiceInContext.super.getAttachmentRevision(revisionId);
            }
        }));
    }

    @Override
    public MultiOptional<Attachment> getAttachment(final Long attachmentId) throws EdmException {
        return MultiOptional.unwrapFromOptional(DSTransaction.instance().executeOpenContext(request, new DSTransaction.Function<MultiOptional<Attachment>>() {
            @Override
            public MultiOptional<Attachment> apply() throws Exception {
                return AttachmentServiceInContext.super.getAttachment(attachmentId);
            }
        }));
    }

    @Override
    public MultiOptional<AttachmentView> getAttachmentView(final Long attachmentId) throws EdmException {
        return MultiOptional.unwrapFromOptional(DSTransaction.instance().executeOpenContext(request, new DSTransaction.Function<MultiOptional<AttachmentView>>() {
            @Override
            public MultiOptional<AttachmentView> apply() throws Exception {
                return AttachmentServiceInContext.super.getAttachmentView(attachmentId);
            }
        }));
    }

    public void putRevisionStreamInResponse(final AttachmentRevision revision, final String download, final HttpServletResponse response) throws EdmException, IOException {
        DSTransaction.instance().executeOpenContext(request, new DSTransaction.Function() {
            @Override
            public Object apply() throws Exception {
                AttachmentServiceInContext.super.putRevisionStreamInResponse(revision, download, response);
                return null;
            }
        });
    }

    public List<AttachmentView> getAttachmentViewsByDocumentId(final Long documentId) throws EdmException, SQLException {
        Optional<List<AttachmentView>> listOptional = DSTransaction.instance().executeOpenContext(request, new DSTransaction.Function<List<AttachmentView>>() {
            @Override
            public List<AttachmentView> apply() throws Exception {
                return AttachmentServiceInContext.super.getAttachmentViewsByDocumentId(documentId);
            }
        });

        if (listOptional.isPresent()) {
            return listOptional.get();
        } else {
            return Arrays.asList();
        }
    }
}