package pl.compan.docusafe.spring.user.office;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.bpmn.diagram.ProcessDiagramGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ManualAssignmentDto;
import pl.compan.docusafe.api.user.office.ProcessService;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cmis.model.CmisPrepend;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessUtils;
import pl.compan.docusafe.core.dockinds.process.manual.ManualMultiAssignmentFactory;
import pl.compan.docusafe.core.dockinds.process.manual.ManualMultiAssignmentManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.process.ProcessForDocument;
import pl.compan.docusafe.process.form.Button;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Serwis zwi�zany z metodami dotycz�cymi proces�w.
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
@Service
public class ProcessServiceImpl implements ProcessService {

    private static final Logger log = LoggerFactory.getLogger(ProcessServiceImpl.class);

    /**
     * Zwraca akcje na dokumencie zwi�zane z danym procesem
     *
     * @param documentId
     */
    @Override
    public List<KeyElement> getProcessActions(String documentId) throws EdmException {
        List<KeyElement> results = Lists.newArrayList();
        Document doc = Document.find(CmisPrepend.lastId(documentId));
        List<Button> processActions = ProcessUtils.getProcessActions(doc, ProcessActionContext.VIEW);

        for(Button b : processActions){
            results.add(new KeyElement(b.getLabel(), b.getName()));
        }

        return results;
    }

    /**
     * wykonuje akcje procesow� na dokumencie
     *
     * @param documentId
     * @param processAction
     */
    @Override
    public void executeProcessAction(String documentId, String processAction) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(CmisPrepend.lastId(documentId));
        ProcessUtils.executeProcessAction(doc, processAction);
    }

    /**
     * R�czna dekretacja
     *
     * @param assigns
     * @param documentId
     */
    @Override
    public void manualMutliAssign(List<ManualAssignmentDto> assigns, String documentId) throws Exception {
        OfficeDocument doc = OfficeDocument.find(CmisPrepend.lastId(documentId));
        ProcessUtils.validateOnList(doc);
        ManualMultiAssignmentManager manager = ManualMultiAssignmentFactory.create(assigns, doc);
        manager.manualAssign();
    }

    /**
     * Metoda zwraca list� id dokument�w kt�re posiadaja uruchomiony dany process
     *
     * @param processName
     * @return
     */
    @Override
    public List<Long> getDocumentsForProcess(String processName) throws EdmException {
        return ProcessForDocument.getDocumentsForProcess(processName);
    }

    /**
     * Metoda zwraca list� nazw proces�w powi�zanych z danym dokumentem
     *
     *
     * @param documentId
     * @return
     */
    @Override
    public List<KeyElement> getProcessForDocument(String documentId) throws EdmException {
        List<ProcessForDocument> processes = ProcessForDocument.getProcessesForDocument(CmisPrepend.lastId(documentId));
        Collection<KeyElement> resutls = Collections2.transform(processes, new Function<ProcessForDocument, KeyElement>() {
            @Nullable
            @Override
            public KeyElement apply(@Nullable ProcessForDocument input) {
                return new KeyElement(input.getId().getProcessId(), input.getProcessDefinitionId());
            }
        });
        return Lists.newArrayList(resutls);
    }

    @Override
    public String getProcessInstance(String processInstanceId) throws Exception {
        String s = null;

        try{
            Map<String, String> vars = new LinkedHashMap<String, String>();
            vars.put("processId", processInstanceId);

            s = getByRestInterface(vars);
        }catch ( Exception e){
            log.error("", e);
            throw new Exception(e.getMessage());
        }
        return s;
    }

    protected String getByRestInterface(Map<String, String> vars) {
        String s = null;
        File f = null;
        try{
            HttpEntity<String> requestEntity = new HttpEntity<String>(null,new HttpHeaders());
            RestTemplate template = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

            List<HttpMessageConverter<?>> messageConverters = template.getMessageConverters();
            messageConverters.add(new BufferedImageHttpMessageConverter());
            ResponseEntity<BufferedImage> responseEntity = template.exchange(getUrl("/process-instance/{processId}/diagram"),
                    HttpMethod.GET, requestEntity, BufferedImage.class, vars);
            f = FileUtils.createTempFile("base.png");
            ImageIO.write(responseEntity.getBody(), "png", f);
            s = FileUtils.encodeByBase64(f);
        }catch(Exception e){
            log.error("", e);
        } finally{
            org.apache.commons.io.FileUtils.deleteQuietly(f);
        }
        return s;
    }

    @Override
    public String getProcessDefinition(CharSequence processName) throws IOException {
        HttpEntity<String> requestEntity = new HttpEntity<String>(null,new HttpHeaders());
        RestTemplate template =  new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        List<HttpMessageConverter<?>> messageConverters = template.getMessageConverters();
        messageConverters.add(new BufferedImageHttpMessageConverter());
        ResponseEntity<String> responseEntity = template.exchange(getUrl("repository/process-definitions?key=" + processName),
                HttpMethod.GET, requestEntity, String.class);

        String textValue = getResourceURl(responseEntity);

        //podmieniam fragment urla, resourcedata pobiera definicj� procesu. Na sztywno na szybko zrobione
        String s1 = textValue.replaceFirst("resources", "resourcedata");

        ResponseEntity<String> exchange = template.exchange(s1, HttpMethod.GET, requestEntity, String.class);

        return exchange.getBody();
    }

    @Override
    public String loadProcessDefinition(String base64, String processName) throws IOException {
        File f = saveToFile(base64, processName);
        InputStream is = new FileInputStream(f);
        ProcessEngines.getDefaultProcessEngine().getRepositoryService().createDeployment()
                .addInputStream(processName, is).name(processName).deploy();
        return null;
    }

    private static File saveToFile(String base64, String fileName) throws IOException {
        byte[] encodedDocument = base64.getBytes();
        byte[] decodedDocument = org.apache.commons.codec.binary.Base64.decodeBase64(encodedDocument);

        File tempFile = FileUtils.createTempFile(fileName + ".bpmn");
        org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, decodedDocument);
        return tempFile;
    }

    protected String getResourceURl(ResponseEntity<String> responseEntity) throws IOException {
        String s = responseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(s);
        JsonNode data = node.get("data");
        JsonNode resource = data.get(0).get("resource");
        return resource.getTextValue();
    }

    protected static String getUrl(String uri){
        return  AdditionManager.getProperty("activiti.base.url")  + '/'+ uri;
    }
}
