package pl.compan.docusafe.spring.user.office;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.gson.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.api.user.office.TemplateDto;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.AttachmentHelper;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.repository.RepositoryProvider;
import pl.compan.docusafe.core.repository.SimpleFileSystemRepository;
import pl.compan.docusafe.core.templating.DocumentSnapshot;
import pl.compan.docusafe.core.templating.DocumentSnapshotSerializer;
import pl.compan.docusafe.core.templating.Templating;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.web.office.common.AttachmentsTabAction;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.FileActionListener;
import pl.compan.docusafe.webwork.event.RichEventActionSupport;

import javax.annotation.Nullable;
import java.io.*;
import java.net.ConnectException;
import java.text.Format;
import java.util.*;

/**
 * Opakowane metody Do szablon�w
 */
@Service
public class TemplateServiceImpl {

    private static final Logger log = LoggerFactory.getLogger(TemplateServiceImpl.class);

    public void deleteSnapshot(Long snapshotId) throws Exception{
        Long documentId = null;
        Optional<DocumentSnapshot> snapshotOptional = DocumentSnapshot.find(snapshotId);

        if(snapshotOptional.isPresent())
        {
            final DocumentSnapshot snapshot = snapshotOptional.get();

            if(snapshot.hasAccess()) {

                 documentId = snapshot.getDocument().getId();

                DSApi.context().session().delete(snapshot);
            } else {
                log.error("Brak uprawnie� do snapshotu "+snapshotId);
                throw new Exception("Brak uprawnie� do snapshotu "+snapshotId);
            }

        }
        else
        {
            log.error("Nie znaleziono snapshotu o id "+snapshotId);
            throw new Exception("Nie znaleziono snapshotu o id "+snapshotId);
        }

        try {
            final SimpleFileSystemRepository repo = RepositoryProvider.getDocumentRepository(documentId, snapshotId);
            repo.delete();
        } catch (EdmException e) {
            log.error("[ajaxAction] cannot gc repository at {}/{}", documentId, snapshotId);
        }
    }

    public DocumentSnapshot createSnapshot(Long documentId, String values) throws Exception{
        final Document document = Document.find(documentId);
        final UserImpl user = (UserImpl)DSApi.context().getDSUser();
        DocumentSnapshot snapshot = new DocumentSnapshot(document, user);

        if(values != null)
        {
            snapshot.setPlainJsonValues(values);
        }

        DSApi.context().session().save(snapshot);

        String title = "Snapshot "+snapshot.getId()+" dokumentu o identyfikatorze "+document.getId();
        snapshot.setTitle(title);

        DSApi.context().session().save(snapshot);

        return snapshot;
    }


    public JsonElement updateSnapshot(Long snapshotId, String values, String title) throws Exception{
        log.info("[updateSnapshot] values = {}", values);
        Optional<DocumentSnapshot> snapshotOptional = DocumentSnapshot.find(snapshotId);

        if(snapshotOptional.isPresent())
        {
            final DocumentSnapshot snapshot = snapshotOptional.get();

            if(snapshot.hasAccess())
            {
                snapshot.setPlainJsonValues(values);
                snapshot.updateMtime();
                snapshot.setTitle(title);
                DSApi.context().session().save(snapshot);
            }
            else
            {
                log.error("Brak uprawnie� do snapshotu " + snapshotId);
                throw new EdmException("Brak uprawnie� do snapshotu "+snapshotId);
            }

        }
        else
        {
            log.error("Nie znaleziono snapshotu o id" + snapshotId);
            throw new EdmException("Nie znaleziono snapshotu o id "+snapshotId);
        }

        return null;
    }

    public JsonElement getSnapshot(Long id) throws Exception{
        Optional<DocumentSnapshot> snapshotOptional = DocumentSnapshot.find(id);

        if(snapshotOptional.isPresent())
        {
            DocumentSnapshot snapshot = snapshotOptional.get();

            if(snapshot.hasAccess()) {

                JsonObject jsonView = snapshot.getJsonView();

                JsonArray availableTemplates = getAvailableTemplates(snapshot);
                jsonView.add("templates", availableTemplates);

                JsonArray fileDocuments = getFileDocuments(snapshot);
                jsonView.add("fileDocuments", fileDocuments);

                if(log.isDebugEnabled()) {
                    log.debug("[GetSnapshot.ajaxAction] jsonView = {}", jsonView);
                }

                return jsonView;
            } else {
                log.error("Brak uprawnie� do snapshotu "+id);
                throw new Exception("Brak uprawnie� do snapshotu "+id);
            }

        }
        else
        {
            log.error("Nie znaleziono snapshotu " + id);
            throw new Exception("Nie znaleziono snapshotu " + id);
        }
    }
    
    public JsonElement getSnapshots(Long documentId) throws Exception{
        Collection<DocumentSnapshot> snapshots = DocumentSnapshot.findByDocumentId(documentId);

        List<DocumentSnapshot> filteredSnapshots = new ArrayList<DocumentSnapshot>();

        DocumentSnapshot.filterByPermission(DSApi.context().getDSUser(), snapshots, filteredSnapshots);

        JsonArray array = new JsonArray();

        for(DocumentSnapshot snapshot: filteredSnapshots) {
            log.debug("ctime class = {}", snapshot.getCtime().getClass());
            final JsonObject jsonView = snapshot.getJsonView();

            array.add(jsonView);
        }

        return array;
    }

    public JsonArray getAvailableTemplates(DocumentSnapshot snapshot) throws EdmException {
        String dockindCn = snapshot.getDocument().getDocumentKind().getCn();
        return getAvailableTemplates(dockindCn);
    }

    public List<TemplateDto> getListOfAvailableTemplates(String dockindCn) throws EdmException {
        final String username = DSApi.context().getDSUser().getName();
        SimpleFileSystemRepository userRepo = RepositoryProvider.getUserTemplateRepository(username, dockindCn);
        ImmutableList<TemplateDto> listOfUserTemplatesDto = FluentIterable.from(userRepo.listAsString()).transform(new Function<String, TemplateDto>() {
            @Nullable
            @Override
            public TemplateDto apply(@Nullable String s) {
                TemplateDto templateDto = new TemplateDto();
                templateDto.setTemplateName(s);
                templateDto.setIsUserTemplate(true);
                return templateDto;
            }
        }).toList();
        SimpleFileSystemRepository globalRepo = RepositoryProvider.getGlobalTemplateRepository(dockindCn);
        ImmutableList<TemplateDto> listOfGlobalTemplatesDto=FluentIterable.from(globalRepo.listAsString()).transform(new Function<String, TemplateDto>() {
            @Nullable
            @Override
            public TemplateDto apply(@Nullable String s) {
                TemplateDto templateDto = new TemplateDto();
                templateDto.setTemplateName(s);
                templateDto.setIsUserTemplate(false);
                return templateDto;
            }
        }).toList();
        ArrayList arrayList = new ArrayList<TemplateDto>();
        arrayList.addAll(new ImmutableList.Builder<TemplateDto>()
                .addAll(listOfGlobalTemplatesDto)
                .addAll(listOfUserTemplatesDto)
                .build());
        return arrayList;
    }

    /**
     * Pobiera z adds�w list� dost�pnych templat�w danego snapshota
     */
    public JsonArray getAvailableTemplates(String dockindCn) throws EdmException {

        final String username = DSApi.context().getDSUser().getName();

        final JsonArray array = new JsonArray();

        // user repository
        SimpleFileSystemRepository userRepo = RepositoryProvider.getUserTemplateRepository(username, dockindCn);
        for(String filename: userRepo.listAsString()) {
            JsonObject obj = new JsonObject();

            obj.addProperty("name", filename);
            obj.addProperty("isUserTemplate", true);

            array.add(obj);
        }

        // global repository
        SimpleFileSystemRepository globalRepo = RepositoryProvider.getGlobalTemplateRepository(dockindCn);
        for(String filename: globalRepo.listAsString()) {

            JsonObject obj = new JsonObject();

            obj.addProperty("name", filename);
            obj.addProperty("isUserTemplate", false);

            array.add(obj);
        }

        return array;
    }

    private JsonArray getFileDocuments(DocumentSnapshot snapshot) throws EdmException {

        JsonArray array = new JsonArray();

        // document repository
        SimpleFileSystemRepository documentRepo = RepositoryProvider.getDocumentRepository(snapshot.getDocument().getId(), snapshot.getId());
        for(String filename: documentRepo.listAsString()) {
            JsonObject obj = new JsonObject();

            obj.addProperty("name", filename);
            obj.addProperty("isRtf", isRtf(filename));

            array.add(obj);
        }

        return array;
    }

    public JsonElement createFile(final DocumentSnapshot snapshot, final Long documentId, String templateName,
                                  String dockind, boolean isUserTemplate) throws Exception {
        // create temporarty file from templateName file
        // String templateName = "TEST_FakturaZapotrzebowanie_POIG.rtf";

        final String[] split = StringUtils.split(templateName, ".");
        if(split.length < 2) {
            throw new Exception("Cannot create temp file from filename: "+templateName);
        }

        final File tempFile = File.createTempFile(split[0], split[1]);
        final SimpleFileSystemRepository documentRepository = RepositoryProvider.getDocumentRepository(documentId, snapshot.getId());

//                final File tempFile = new File()


        // save filled rtf templated in temporary file
//                RtfTemplating rtfTemplating = new RtfTemplating();
//
//                String filePath = Docusafe.getHome() + "/templates/"+templateName;
//                FileInputStream in = new FileInputStream(filePath);
//
//                OutputStream out = new FileOutputStream(tempFile);
//                Map<String, String> values = new HashMap<String, String>();
//
//                rtfTemplating.process(in, out, values);
//                out.flush();
//                out.close();

        final JsonArray json = snapshot.getJson();
        final Map<String, Object> values = DocumentSnapshotSerializer.getMapFromJson(json);

        saveTemplate(values, documentId, templateName, dockind, tempFile, isUserTemplate);

        // get input stream from save temp file
        FileInputStream in = new FileInputStream(tempFile);

        String filename = getFilenameWithTimestamp(templateName);

        if(! documentRepository.add(filename, in)) {
            throw new EdmException("Nie mo�na utworzy� dokumentu");
        }

        JsonObject ret = new JsonObject();
        ret.add("name", new JsonPrimitive(filename));
        ret.add("isRtf", isRtfJson(filename));

        return ret;
    }

    public JsonElement createFile(Long snapshotId, final  Long documentId, String templateName,
                                  String dockind, boolean isUserTemplate) throws Exception{

        final Optional<DocumentSnapshot> documentSnapshotOptional = DocumentSnapshot.findCheckAccess(snapshotId);

        if(! documentSnapshotOptional.isPresent())
        {
            throw new Exception("Nie znaleziono snapshotu o id "+snapshotId);
        }

        final DocumentSnapshot snapshot = documentSnapshotOptional.get();

        return createFile(snapshot, documentId, templateName, dockind, isUserTemplate);
    }

    public JsonElement deleteFile(Long snapshotId, final  Long documentId, String documentName) throws EdmException {

        DocumentSnapshot.existsCheckAccessOrError(snapshotId);

        final SimpleFileSystemRepository documentRepository = RepositoryProvider.getDocumentRepository(documentId, snapshotId);

        if(! documentRepository.remove(documentName)) {
            throw new EdmException("Nie mo�na usun�� dokumentu "+ documentName);
        }

        return new JsonNull();
    }

    protected InputStream getSnapshotInputStream(String documentName, Long documentId, Long snapshotId) throws EdmException {
        DocumentSnapshot.existsCheckAccessOrError(snapshotId);

        final SimpleFileSystemRepository documentRepository = RepositoryProvider.getDocumentRepository(documentId, snapshotId);

        final InputStream inputStream = documentRepository.get(documentName);

        if(inputStream == null) {
            throw new EdmException("Nie mo�na pobra� pliku: "+documentName);
        }

        return inputStream;
    }

    public FileActionListener.FileInfo getFile(Long snapshotId, final  Long documentId, String documentName) throws Exception{
        InputStream inputStream = getSnapshotInputStream(documentName, documentId, snapshotId);

        String contentType = "application/pdf";
        if(isRtf(documentName)) {
            contentType = "application/rtf";
        }

        final FileActionListener.FileInfo fileInfo = new FileActionListener.FileInfo(contentType, documentName, inputStream);

        return fileInfo;
    }

    public JsonElement uploadFile(Long snapshotId, final  Long documentId, String documentName, File file) throws EdmException, FileNotFoundException {
        SimpleFileSystemRepository repo = RepositoryProvider.getDocumentRepository(documentId, snapshotId);

        repo.remove(documentName);
        repo.add(documentName, new FileInputStream(file));

        return new JsonNull();
    }

    /**
     * Tworzy pdfa i dodaje go do za��cznik�w
     * @param snapshotId
     * @param documentId
     * @param documentName
     * @return
     * @throws Exception
     */
    public JsonElement createPdf(Long snapshotId, final  Long documentId, String documentName) throws Exception {
        DocumentSnapshot.existsCheckAccessOrError(snapshotId);

        final SimpleFileSystemRepository documentRepository = RepositoryProvider.getDocumentRepository(documentId, snapshotId);

        final InputStream inputStream = documentRepository.get(documentName);

        if(inputStream == null) {
            throw new EdmException("Nie mo�na znale�� pliku "+documentName);
        }

        if(! isRtf(documentName)) {
            log.error("[CreatePdf] file is not rtf: {}", documentName);
            throw new EdmException("Dokument musi by� typu RTF");
        }

        String prefix = documentName.replace("\\.pdf$", "");
        String suffix = ".pdf";
        final File temp = File.createTempFile(prefix, suffix);

        OutputStream outputStream = new FileOutputStream(temp);

        try {
            PdfUtils.convertRtfToPdf(inputStream, outputStream, true);
        } catch(Exception ex) {
            final Throwable cause = ex.getCause();

            if(cause instanceof ConnectException) {
                log.error("[CreatePdf] connection error", ex);
                throw new EdmException("B��d podczas tworzenia dokumentu PDF");
            } else {
                throw new Exception(ex);
            }
        }

        inputStream.close();
        outputStream.close();

        final FileInputStream pdfInputStream = new FileInputStream(temp);

        String f = documentName.replaceFirst("\\.([A-z0-9]+)", ".pdf").replaceFirst("^[0-9]+_", "");
        final String filename = getFilenameWithTimestamp(f);

        if(! documentRepository.add(filename, pdfInputStream)) {
            log.error("Cannot create pdf for filename "+filename);
            throw new EdmException("Nie mo�na utworzy� PDF");
        }

        pdfInputStream.close();

        AttachmentHelper.createAttachment(documentId, temp, filename, f, null);

        return new JsonPrimitive(filename);
    }

    private String getFilenameWithTimestamp(String templateName) {
        long timestamp = new Date().getTime();

        return timestamp+"_"+templateName;
//        final String[] split = StringUtils.split(templateName, ".");
//
//        if(split.length < 2) {
//            return templateName+"_"+timestamp;
//        } else {
//            return templateName.replaceFirst("\\.([A-z0-9]+)$", "_"+timestamp+".$1");
//        }
    }

    private boolean isRtf(String filename) {
        return filename == null ? false : filename.matches("^.*\\.rtf$");
    }

    private JsonPrimitive isRtfJson(String filename) {
        return new JsonPrimitive(isRtf(filename));
    }

    /**
     *
     * @param values
     * @param documentId
     * @param templateName
     * @param dockind
     * @param output
     * @throws Exception
     */
    public static void saveTemplate(Map<String, Object> values, Long documentId, String templateName, String dockind, File output) throws Exception {
        saveTemplate(values, documentId, templateName, dockind, output, false);
    }
    /**
     *
     * @param values
     * @param documentId
     * @param templateName
     * @param dockind
     * @param output
     * @param isUserTemplate
     * @throws Exception
     */
    public static void saveTemplate(Map<String, Object> values, Long documentId, String templateName, String dockind, File output, boolean isUserTemplate) throws Exception {
        SimpleFileSystemRepository repo;
        if(isUserTemplate) {
            String username = DSApi.context().getDSUser().getName();
            repo = RepositoryProvider.getUserTemplateRepository(username, dockind);
        } else {
            repo = RepositoryProvider.getGlobalTemplateRepository(dockind);
        }
        Map<Class, Format> defaultFormatters = Maps.newLinkedHashMap();
        DocFacade df= Documents.document(documentId);
        Map<String, Object> params = Maps.newHashMap();
        for (String key: values.keySet())
        {
            params.put(key, values.get(key));
        }
        df.getDocument().getDocumentKind().logic().setAdditionalTemplateValues(df.getId(), params, defaultFormatters);
        FileOutputStream fos = new FileOutputStream(output);
        Templating.rtfTemplate(repo, templateName, params, fos, defaultFormatters, "true");
        IOUtils.closeQuietly(fos);
    }

    /**
     * <p>
     *     Zapisuje nowy template u�ytkownika w repozytorium.
     * </p>
     */
    public void addUserTemplate(File file, String dockind, String username) throws FileNotFoundException, EdmException {
        SimpleFileSystemRepository repo = RepositoryProvider.getUserTemplateRepository(username, dockind);
        addTemplate(file, repo);
    }

    /**
     * <p>
     *     Zapisuje nowy template globalny w repozytorium.
     * </p>
     */
    public void addGlobalTemplate(File file, String dockind) throws FileNotFoundException, EdmException {
        SimpleFileSystemRepository repo = RepositoryProvider.getGlobalTemplateRepository(dockind);
        addTemplate(file, repo);
    }

    protected void addTemplate(File file, SimpleFileSystemRepository repo) throws FileNotFoundException {
        String filename = file.getName();

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            repo.add(filename, fis);
        } finally {
            IOUtils.closeQuietly(fis);
        }
    }

    /**
     * <p>
     *     Usuwa template u�ytkownika z repozytorium.
     * </p>
     * @param dockind
     * @param name
     * @param username
     * @throws EdmException
     */
    public void deleteUserTemplate(String dockind, String name, String username) throws EdmException {
        SimpleFileSystemRepository repo = RepositoryProvider.getUserTemplateRepository(username, dockind);
        repo.remove(name);
    }

    /**
     * <p>
     *     Usuwa template u�ytkownika z repozytorium.
     * </p>
     * @param dockind
     * @param name
     * @throws EdmException
     */
    public void deleteGlobalTemplate(String dockind, String name) throws EdmException {
        SimpleFileSystemRepository repo = RepositoryProvider.getGlobalTemplateRepository(dockind);
        repo.remove(name);
    }
}
