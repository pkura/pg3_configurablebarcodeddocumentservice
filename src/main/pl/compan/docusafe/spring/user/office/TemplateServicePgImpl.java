package pl.compan.docusafe.spring.user.office;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.integration.DocumentConverter;
import pl.compan.docusafe.parametrization.pg.PgIntegration;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.FileActionListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

//@Service
public class TemplateServicePgImpl extends TemplateServiceImpl {

    private static final Logger log = LoggerFactory.getLogger(TemplateServicePgImpl.class);

    private final RestTemplate restTemplate;
    private final DocumentConverter documentConverter;

//    @Autowired
    public TemplateServicePgImpl(RestTemplate restTemplate, DocumentConverter documentConverter) {
        this.restTemplate = restTemplate;
        this.documentConverter = documentConverter;
    }

    @Override
    public FileActionListener.FileInfo getFile(Long snapshotId, final  Long documentId, String documentName) throws Exception{
        InputStream inputStream = null;
        FileOutputStream outputStream = null;
        try {
            inputStream = getSnapshotInputStream(documentName, documentId, snapshotId);

            File tempSnapshot = File.createTempFile("temp_snapshot", null);
            outputStream = new FileOutputStream(tempSnapshot);
            IOUtils.copy(inputStream, outputStream);
            outputStream.close();

            // convert rtf to pdf
            File pdf = documentConverter.convert(tempSnapshot, documentName, "rtf", "pdf");
            documentName = documentName.replaceFirst("\\.rtf$", ".pdf");

            // add barcode
            if(! documentConverter.isAvailable()) {
                log.error("[getFile] DocumentConverter not available, not adding barcode");
            } else {
                Document document = Document.find(documentId);
                String dockindCn = document.getDocumentKind().getCn();
                String barcode = (String)document.getFieldsManager().getFieldValues().get("DOC_BARCODE");
                String barcodeType = AdditionManager.getPropertyOrDefault("kancelaria.szablony.simple.pdfWithBarcodeFormat."+dockindCn, "qr");

                Integer deltaX = AdditionManager.getIntPropertyOrDefault("kancelaria.szablony.simple.barcodeMargin.x." + dockindCn, 0);
                Integer deltaY = AdditionManager.getIntPropertyOrDefault("kancelaria.szablony.simple.barcodeMargin.y." + dockindCn, 0);

                if(barcode != null && StringUtils.isNotBlank(barcode)) {
                    log.debug("[getFile] adding barcode to snapshotId = {}, documentId = {}, documentName = {}, barcode = {}, barcodeType = {}", snapshotId, documentId, documentName, barcode, barcodeType);
                    pdf = documentConverter.addBarcodeToPdf(pdf, barcode, barcodeType, deltaX, deltaY);
                } else {
                    log.debug("[getFile] barcode is null, not adding to pdf, documentId = {}, snapshotId = {}, documentName", snapshotId, documentId, documentName);
                }
            }


            String contentType = "application/pdf";
            final FileActionListener.FileInfo fileInfo = new FileActionListener.FileInfo(contentType, documentName, new FileInputStream(pdf));

            return fileInfo;

        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }
    }

}
