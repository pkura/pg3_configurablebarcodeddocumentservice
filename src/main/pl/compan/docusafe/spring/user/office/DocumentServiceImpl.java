package pl.compan.docusafe.spring.user.office;

import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Maps;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.compan.docusafe.api.user.office.*;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.XmlGenerator;
import pl.compan.docusafe.core.base.document.DocumentSmartFinder;
import pl.compan.docusafe.core.cmis.DSObjectDataConverter;
import pl.compan.docusafe.core.cmis.model.CmisPrepend;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.*;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.utils.ChecksumAlgorithm;
import pl.compan.docusafe.rest.views.RichDocumentView;
import pl.compan.docusafe.spring.SmartSearchService;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.web.admin.FullTextManager;
import pl.compan.docusafe.web.archive.repository.search.SearchDockindDocumentHelper;
import pl.compan.docusafe.web.archive.repository.search.SearchDockindDocumentsAction;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

/**
 */
@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    SmartSearchService smartSearch;
	
    private static final Logger log = LoggerFactory.getLogger(DocumentServiceImpl.class);

    @Override
    public boolean validateBarcode(String algorithmName, String barcode) throws EdmException {
        try
        {
            return ChecksumAlgorithm.valueOf(algorithmName).isValid(barcode);
        }catch(Exception e){
            log.error("", e);
            throw new EdmException(e.getMessage());
        }
    }

    @Override
    public List<Long> findByBarcode(String barcode) throws EdmException {
        try{
            return OfficeDocument.findDocumentsIdByBarcode(barcode);
        }catch(Exception e){
            log.error("", e);
            throw new EdmException(e.getMessage());
        }
    }

    /**
     * Zwraca podany rekord s�ownika dokumentu
     * @param documentId
     * @param dictionary
     * @param id warto�� id s�ownika
     * @throws EdmException
     */
    @Override
    public List<FieldView> getDocumentDictionaryObject(String documentId, String dictionary, Long id) throws EdmException {
        List<FieldView> results = Lists.newArrayList();
        String dicCn = getDicCn(dictionary);
        try{
            Document document = Document.find(CmisPrepend.lastId(documentId));
            FieldsManager fm  = document.getFieldsManager();
            dicCn = getCorrectDicCn(fm, dicCn);
            Field dictionaryField = (Field) fm.getField(dicCn);
            
            Map<String,Object> dictionaryValues = FieldsManagerUtils.getDictionaryExtractedValues(fm, dicCn, id, new FieldsManagerUtils.DateEnumValuesGetter(true));

            for(Field f : fm.getDictionaryFields(dicCn)){
                //nazwa pola jesli pole jest wielowarto�ciowe nale�y uci�� liczb� z ko�ca
                String dictionaryFieldCn = getDictionaryFieldCn(dictionaryField, f);
                // je�li zawiera pole to dodajemy do listy
                if(dictionaryValues.containsKey(dictionaryFieldCn)){
                    String v = getValues(dictionaryValues, dictionaryFieldCn);
                    results.add(new FieldView(dictionaryFieldCn, f.getName(), v));
                }
            }

            log.error("dictionary Object{}", results);

        }catch(Exception e){
            log.error("", e);
            throw new EdmException(e.getMessage());
        }

        return results;
    }

	/**
     * Wyszukiwanie
     *
     * @param dictionaryDtos
     * @return
     * @throws EdmException
     */
    @Override
    public List<DictionaryDto> searchDictionary(DictionaryDto dictionaryDtos) throws EdmException {
        List<DictionaryDto> results = Lists.newArrayList();
        DocumentKind dk = DocumentKind.findByCn(dictionaryDtos.getDockindCn());
        Map<String, FieldData> maps = dictionaryDtos.getFieldDataMap(dk);
        //pole, o kt�re jest pytanie
        Field fieldByCn = dk.getFieldByCn(dictionaryDtos.getDictionaryCn());

        if(fieldByCn instanceof EnumerationField){
            // parsuje mo�liwe odpowiedzi pola
           results.add(new DictionaryDto(fieldByCn.getEnumItems()));
        } else {
            //parsuje pole typu s�ownikowego
            parseDictionaryField(dictionaryDtos, results, dk, maps);
        }
        return results;
    }

    /**
     * Parsuje pola typu Dictionary podczas wyszukiwania
     * @param dictionaryDtos
     * @param results
     * @param dk
     * @param maps
     * @throws EdmException
     */
    private void parseDictionaryField(DictionaryDto dictionaryDtos, List<DictionaryDto> results, DocumentKind dk, Map<String, FieldData> maps) throws EdmException {
        int limit = dictionaryDtos.getLimit(),
            offset = dictionaryDtos.getOffset();
        DwrDictionaryBase dictionary = (DwrDictionaryBase) FieldsManagerUtils.getDictionaryFromDockind(dk, dictionaryDtos.getDictionaryCn());

        List<Map<String, Object>> es = dictionary.search(maps, limit, offset);
        if(es == null)
            es = dictionary.search(maps, limit, offset, false, maps);

        for(Map<String,Object> ma : es){
            log.error("search res {}", ma);
            results.add(new DictionaryDto(ma));
        }
    }

    @Override
    public List<DictionaryDto> add(DictionaryDto dictionaryDtos) throws EdmException {
        List<DictionaryDto> results = Lists.newArrayList();
        DocumentKind dk = DocumentKind.findByCn(dictionaryDtos.getDockindCn());
        DwrDictionaryBase dictionary = (DwrDictionaryBase) FieldsManagerUtils.getDictionaryFromDockind(dk, dictionaryDtos.getDictionaryCn());
        Map<String, FieldData> maps = dictionaryDtos.getFieldDataMap(dk, true);

        dictionary.validateField(maps);
        long es = dictionary.add(maps);
        if (es == -1)
        	throw new EdmException("Ju� istnieje taki wpis!");

        Map<String,Object> ma = Maps.newHashMap();
        ma.put("ID", es);
        results.add(new DictionaryDto(ma));

        return results;
    }


    @Override
    public Boolean update(DictionaryDto dictionaryDtos) throws EdmException {
        DocumentKind dk = DocumentKind.findByCn(dictionaryDtos.getDockindCn());
        Map<String, FieldData> maps = dictionaryDtos.getFieldDataMap(dk, true);
        DwrDictionaryBase dictionary = (DwrDictionaryBase) FieldsManagerUtils.getDictionaryFromDockind(dk, dictionaryDtos.getDictionaryCn());

        long es = dictionary.update(maps);

        return es > 0 ? true : false;
    }

    @Override
    public Boolean remove(DictionaryDto dictionaryDtos) throws EdmException {
        DocumentKind dk = DocumentKind.findByCn(dictionaryDtos.getDockindCn());
        Map<String, FieldData> maps = dictionaryDtos.getFieldDataMap(dk, true);
        DwrDictionaryBase dictionary = (DwrDictionaryBase) FieldsManagerUtils.getDictionaryFromDockind(dk, dictionaryDtos.getDictionaryCn());
        String id = null;
        if(maps.containsKey(DwrDictionaryBase.ID) && maps.get(DwrDictionaryBase.ID) != null)
            id = maps.get(DwrDictionaryBase.ID).getStringData();

        long es = dictionary.remove(id);

        return es > 0 ? true : false;
    }

    @Override
    public List<OfficeDocumentDto> searchDocuments(OfficeDocumentDto document) throws EdmException {
        try {
            DocumentKind dk = DocumentKind.findByCn(document.getDockindCn());
            Map<String, Object> maps = document.getDocumentFieldObjectMap(dk);
            Map<String, FieldView> fieldViewMap = document.convertToMap();
            SearchDockindDocumentsAction act = new SearchDockindDocumentsAction();
            act.setDocumentKindCn(document.getDockindCn());
            act.setDescription(document.getDescription());
            act.setAscending(document.getAscending());
            act.setSortField(document.getSortingField());
            act.setLimit(document.getLimit());
            act.setOffset(document.getOffset());

            if (fieldViewMap.containsKey("ID")) {
                FieldView documentId = fieldViewMap.get("ID");
                act.setDocumentId(Long.valueOf(documentId.getValue()));
            }

            if (fieldViewMap.containsKey("KO")) {
                FieldView ko = fieldViewMap.get("KO");
                act.setOfficeNumber(ko.getValue());
            }
            act.setValues(maps);


            SearchDockindDocumentHelper.instance().search(act, null);

            log.error("{}", act.getResults().size());

            return transformDocumentSearchList((List<Map<String, String>>) act.getResults());
        }catch(Exception e){
            log.error("", e);
            throw new EdmException(e.getMessage());
        }
    }

    @Override
    public String getDocumentAsXml(String documentId) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(CmisPrepend.lastId(documentId));
        XmlGenerator xmlGen = new XmlGenerator(doc);
        try {
			xmlGen.generateXml();
		} catch (TransformerException e) {
			log.error("Error connected with transform {}", e.getMessageAndLocation());
		} catch (IOException e) {
			log.error("Error IO {}", e.getMessage());
		} catch (ParserConfigurationException e) {
			log.error("Parser error {}", e.getLocalizedMessage());
		}
        return Base64.encodeBase64String(xmlGen.getXml());
    }

    private List<OfficeDocumentDto> transformDocumentSearchList(List<Map<String, String>> results) {
        List<OfficeDocumentDto> documents = Lists.newArrayList();

        for(Map<String, String> r : results){
            OfficeDocumentDto officeDocumentDto = new OfficeDocumentDto();
            officeDocumentDto.fromMap(r);
            documents.add(officeDocumentDto);
        }
        return documents;
    }

    @Override
    public List<RichDocumentView> fullTextSearchDocument(FullTextSearchDto text) throws Exception {
    	String query = text.getText();//decodeBase64(text.getText());
    	log.debug("Solr query ", query);
    	List<RichDocumentView> listRichDocs = DocumentSmartFinder.instance().find(query, text.getLimit(), text.getOffset());
    	List<RichDocumentView> listRichDocsBarcode = smartSearch.findByBarcodePrefix(query);
    	listRichDocs = ListUtils.sum(listRichDocsBarcode, ListUtils.subtract(listRichDocs, listRichDocsBarcode));
//        List<FullTextSearchResponse> resp = FullTextManager.getDocumentsFullTextResult2(listRichDocs);
        return listRichDocs;
    }

    private String decodeBase64(String base64) throws UnsupportedEncodingException {
    	return new String(pl.compan.docusafe.util.Base64.decode(base64.getBytes()), "UTF-8");
	}

	/**
     * Konwertuje cny po� s�ownik�w. je�li jest wielowarto�ciowy uscina ostatni� cyfr� pola maja zazwyczaj format
     * S�OWNIK_POLE_X gdzie X to cyfra np. SENDER_COUNTRY_1.
     *
     * jesli nie jest wielowartosciowy zwraca normalny cn.
     *
     * @see pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils.getDictionaryExtractedValues
     * @param dictionaryField
     * @param f
     * @return
     */
    private String getDictionaryFieldCn(Field dictionaryField, Field f) {
        return dictionaryField.getCn() + '_' + f.getCnWithoutLastUnderscore(dictionaryField.isMultiple());
    }

    /**
     * Konwertuje field na warto�� w zale�no�ci od typu pola
     * @param dictionaryValues
     * @param dictionaryFieldCn
     * @return
     */
    private String getValues(Map<String, Object> dictionaryValues, String dictionaryFieldCn) {
        Object v = dictionaryValues.get(dictionaryFieldCn);
        if(v instanceof EnumValues){
            return ((EnumValues)v).getSelectedValue();
        }if(v instanceof java.sql.Date) {
            return DateUtils.formatJsDate((java.sql.Date)v);
        }
        return String.valueOf(v);
    }

    private String getDicCn(String dictionary) {
        return DSObjectDataConverter.cutDSCmisCn(dictionary);
    }
    
    /**
     * Method returns cn of dictionary document
     * @param fm
     * @param dicCn
     * @return String witch is cn from document in form of 'SENDER_HERE' or 'SENDER' OR 'RECIPIENT_HERE' or 'RECIPIENT' 
     * @throws EdmException
     */
    private String getCorrectDicCn(FieldsManager fm, String dicCn) throws EdmException {
 		String dictionaryCn = null;
 		if(fm.getFieldValues().keySet().contains(dicCn)){
 			dictionaryCn = dicCn;
 		} else if (fm.getFieldValues().keySet().contains(dicCn+"_HERE")) {
 			dictionaryCn = dicCn+"_HERE";
 		}
 		return dictionaryCn;
 	}

    /**
     * Pobiera histori� dokumentu
     * @param documentId
     * @return
     * @throws Exception
     */
    @Override
    public List<WorkHistoryBean> getAudit(String documentId) throws Exception {
        List<WorkHistoryBean> results = Lists.newArrayList();
        try{
            Document doc = Document.find(CmisPrepend.lastId(documentId));
            List<Audit>  listAudit = new ArrayList<Audit>();
            if(doc instanceof InOfficeDocument && ((InOfficeDocument) doc).getMasterDocumentId()!=null){
                listAudit.addAll(((InOfficeDocument )doc ).getMasterDocument().getWorkHistory());
            }
            else if (doc instanceof OutOfficeDocument && ((OutOfficeDocument) doc).getMasterDocument()!=null){
                listAudit.addAll(((OutOfficeDocument )doc ).getMasterDocument().getWorkHistory());
            }
            listAudit.addAll(doc.getWorkHistory());

            for(Audit a : listAudit) {
                if (a.getPriority() != null && a.getPriority() > Audit.HIGHEST_PRIORITY)
                    throw new NoSuchElementException();
                results.add(new WorkHistoryBean(a));
            }

        }catch(Exception e){
            log.error("", e);
            throw e;
        }
        return results;
    }

    /**
     * Dodaje pismo do obserwowanych
     *
     *
     * @param documentId
     * @return
     */
    @Override
    public void watch(String documentId) throws EdmException {
        Document doc = Document.find(CmisPrepend.lastId(documentId));
        DSApi.context().watch(URN.create(doc));
    }

    /**
     * Usuwa pismo z obserwowanych
     *
     *
     * @param documentId
     * @return
     */
    @Override
    public void unwatch(String documentId) throws EdmException {
        Document doc = Document.find(CmisPrepend.lastId(documentId));
        DSApi.context().unwatch(URN.create(doc));
    }
}
