package pl.compan.docusafe.spring.user.office;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.FluentIterable;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.OfficeCaseService;
import pl.compan.docusafe.api.user.office.OfficeFolderDto;
import pl.compan.docusafe.api.user.office.WorkHistoryBean;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.cmis.model.CmisPrepend;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.spring.ContainerManager;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.rest.RestRequestHandler;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.web.office.CaseToCase;
import pl.compan.docusafe.web.office.CaseToCaseAction;
import pl.compan.docusafe.web.office.in.CaseAction;

import javax.annotation.Nullable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * Implementuje Service zarz�dzaj�cy sprawami. Wszystkei metody wymagaja
 * contextu
 */
@Service
public class OfficeCaseServiceImpl implements OfficeCaseService{

    @Autowired
    ContainerManager containerManager;

    private static final Logger log = LoggerFactory.getLogger(OfficeCaseServiceImpl.class);

    private final static Function<OfficeCase, OfficeCaseDto> FROM_OFFICE_CASE = new Function<OfficeCase, OfficeCaseDto>() {
        @Nullable
        @Override
        public OfficeCaseDto apply(@Nullable OfficeCase input) {
            return input == null ? null : new OfficeCaseDto(input);
        }
    };

    private final static Function<OfficeFolder, OfficeFolderDto> FROM_OFFICE_FOLDER = new Function<OfficeFolder, OfficeFolderDto>(){

        @Nullable
        @Override
        public OfficeFolderDto apply(@Nullable OfficeFolder officeFolder) {
            return officeFolder == null ? null : new OfficeFolderDto(officeFolder);
        }
    };

    @Override
    public List<OfficeCaseDto> getUserOfficeCase(String username) {
        List<OfficeCaseDto> cases = null;
        try {
            DSUser user = getUser(username);
            cases = transform(OfficeCase.findUserCases(user, null));
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }

        return cases;
    }

    @Override
    public List<OfficeCaseDto> getDivisionOfficeCase(String guid) {
        List<OfficeCaseDto> cases = null;
        try {
            DSDivision division = getDivision(guid);
            cases = transform(OfficeCase.findByDivision(division));
        }catch (DivisionNotFoundException e){
            throw new IllegalArgumentException(e);
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }

        return cases;
    }

    private List<OfficeCaseDto> transform(List<OfficeCase> oc){
        return FluentIterable.from(oc)
                .transform(FROM_OFFICE_CASE)
                .toList();
    }

    @Override
    public List<AssociativeDocumentStore.AssociativeDocumentBean> getDocumentOfficeCase(String documentId) {
        try {
            OfficeDocument doc = getDocument(documentId);
            List<AssociativeDocumentStore.AssociativeDocumentBean> allAssociatedDocuments = AssociativeDocumentStore.getAllAssociatedDocuments(doc.getId());
            return allAssociatedDocuments;
        } catch (EdmException e) {
            log.error("", e);
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<OfficeCaseDto> getOfficeCaseToOfficeCase(Long officeCaseId) {
        List<OfficeCaseDto> result = null;
        try{
            OfficeCase c = findOfficeCase(officeCaseId.toString());
            result = transform(CaseToCase.findLinkedOfficeCases(c));
        }catch(Exception e){
            throw new IllegalArgumentException(e);
        }
        return result;
    }

    /**
     * Tworzy spraw�
     * @param officeCaseDto
     * @return
     */
    @Override
    public boolean createOfficeCase(OfficeCaseDto officeCaseDto) {
        try{
            if(StringUtils.isBlank(officeCaseDto.getDocumentId()))
                throw new IllegalArgumentException("Aby za�o�y� spraw� wymagany jest dokument!");

            CaseAction caseAction = new CaseAction();
            caseAction.setCaseTitle(officeCaseDto.getTitle());
            caseAction.setDescription2(officeCaseDto.getDescription());
            caseAction.setCaseFinishDate(DateUtils.formatJsDate(officeCaseDto.getFinishDate().toDate()));
            caseAction.setPortfolioId(officeCaseDto.getPortfolioId());
            caseAction.setDocumentId(Long.valueOf(officeCaseDto.getDocumentId()));
            caseAction.setPriority(officeCaseDto.getPriority());
            caseAction.setClerk(officeCaseDto.getClerk());

            RestRequestHandler.executeAction("doFullCreate", caseAction);
            officeCaseDto.setId(caseAction.getCaseId());

        }catch(Exception e){
            log.error("", e);
            throw new IllegalStateException(e.getMessage());
        }
        return true;
    }

    /**
     * Metoda dodaje dokumenty do sprawy
     * @param documentId
     * @param caseId
     * @return
     * @throws EdmException
     */
    @Override
    public String addDocumentToOfficeCase(String documentId, Long caseId, boolean caseReopen) throws EdmException
    {
        String msg = null;
        try {
            OfficeDocument document = getDocument(documentId);
            OfficeCase officeCase = OfficeCase.find(caseId);

            if (document.getContainingCase() != null && officeCase.getOfficeId().equals(document.getContainingCase().getOfficeId()))
                throw new EdmException("Dokument " + documentId+" znajduje si� ju� w sprawie " + officeCase.getOfficeId());

            boolean inCase = document.getContainingCase() != null;

            if (inCase) {
                document.getContainingCase().getAudit().add(Audit.create("moveDocument",
                        DSApi.context().getPrincipalName(),
                        "Przeniesiono pismo" + document.getDocumentTypeAsString() + "o numerze " + document.getOfficeNumber() + " do sprawy " + officeCase.getOfficeId()));
                officeCase.getAudit().add(Audit.create("moveDocument",
                        DSApi.context().getPrincipalName(),
                        "Przeniesiono pismo" + document.getDocumentTypeAsString() + "o numerze " + document.getOfficeNumber() + " ze sprawy " + document.getContainingCase().getOfficeId()));
            }

            document.setContainingCase(officeCase, true);

            // dodawanie do sprawy odpowiedzi udzielonych wcze�niej na to pismo
            if (document.getType() == DocumentType.INCOMING) {
                Set<OutOfficeDocument> out = ((InOfficeDocument) document).getOutDocuments();
                for (OutOfficeDocument outdoc : out ) {
                    if (outdoc.getContainingCase() == null)
                        outdoc.setContainingCase(officeCase, true);
                }
            }

            //BipUtils.writeXml(c);

            if (document.getType().equals(InOfficeDocument.TYPE)) {
                Date date = new Date();
                date = DateUtils.plusDays(date, ((InOfficeDocument) document).getKind().getDays());
                officeCase.setFinishDate(date);
            }
            if (officeCase.getStatus().isClosingStatus() && caseReopen) {
                officeCase.setStatus(CaseStatus.find(CaseStatus.ID_REOPEN));
            }
            TaskSnapshot.updateByDocumentId(document.getId(), document.getStringType());

            if (inCase) {
                msg = "Pismo przeniesiono do sprawy " + officeCase.getOfficeId();
            } else {
                msg = "Dodano pismo " + document.getDocumentTypeAsString() + " o numerze " + document.getOfficeNumber();
                officeCase.getAudit().add(Audit.create("addDocument",
                        DSApi.context().getPrincipalName(),
                        msg));
            }

            if (AvailabilityManager.isAvailable("setCaseIdAsDocumentNr", document.getDocumentKind().getCn())) {
                PreparedStatement ps = null;
                try {
                    ps = DSApi.context().prepareStatement("update dsg_normal_dockind set NR_DOKUMENTU = ? where document_id = ?");
                    ps.setString(1, document.getContainingCase().getOfficeId());
                    ps.setLong(2, document.getId());
                    ps.executeUpdate();
                    ps.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    throw new EdmException(e.getMessage());
                } finally {
                    DSApi.context().closeStatement(ps);
                }
            }
        }catch(DocumentNotFoundException e) {
            throw new EdmException("Nie istnieje taki dokumentu!");
        }
        catch (DivisionNotFoundException e) {
            throw new EdmException(e.getMessage() + " w kt�rym znajdowa�a si� ta sprawa");
        }
        catch (Exception e) {
        	log.error("",e );
        } finally {
            DSApi.context()._rollback();
        }

        log.info("msg {}", msg);
        return msg;
    }

    @Override
    public String removeDocumentFromOfficeCase(String documentId, Long officeCaseId) throws EdmException {
        String msg = null;
        try
        {
            OfficeDocument document = getDocument(documentId);
            if(document.getContainingCase() == null){
                return msg;
            }
            String caseIdInfo = document.getContainingCase() != null ? document.getContainingCase().getOfficeId() : document.getContainingCaseId().toString();
            document.getContainingCase().getAudit().add(Audit.create("removeDocumentFromCase",
                    DSApi.context().getPrincipalName(),
                    "Usuni�to pismo" + document.getDocumentTypeAsString() + "o numerze "+document.getOfficeNumber()+" ze sprawy "+document.getContainingCase().getOfficeId()));

            document.addWorkHistoryEntry(Audit.create("removeDocumentFromCase",
                    DSApi.context().getPrincipalName(),
                    "Usuni�to pismo ze sprawy "+document.getContainingCase().getOfficeId()));

            document.setContainingCaseId(null);
            document.setCaseDocumentId(null);//kasuje znak sprawy, w kt�rej znajduje si� dokument

            TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());


            msg = "Usuni�to pismo ze sprawy "+ caseIdInfo;
        }catch(Exception e){
            log.error("", e);
            throw new EdmException(e);
        }

        return msg;
    }

    @Override
    public OfficeCaseDto getOfficeCase(String officeCaseId) throws Exception {
        OfficeCase c =findOfficeCase(officeCaseId);
        c.hasPermission();
        return new OfficeCaseDto(c);
    }

    @Override
    public List<OfficeFolderDto> getPortfolio(String guid) throws EdmException {
        List<OfficeFolder> portfolios = OfficeFolder.getByDivisionGuidToShowOnView(guid);
        return FluentIterable.from(portfolios)
                .transform(FROM_OFFICE_FOLDER)
                .toList();
    }

    private OfficeCase findOfficeCase(String officeCaseId) throws Exception {
        OfficeCase c = null;
        try {
            c = StringUtils.isNumeric(officeCaseId) ? OfficeCase.find(Long.valueOf(officeCaseId))
                    : OfficeCase.findByOfficeId(officeCaseId);
            Preconditions.checkArgument(c.canViewOfficeCase(), "U�ytkownik nie ma dost�pu do sprawy!");
        } catch (CaseNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (EdmException e){
            log.error("", e);
            throw new Exception("Nieoczekiwany b��d");
        } 
        return c;
    }

    /**
     * Zwraca usera albo po id albo po username albo po external name.
     * @param username
     * @return
     * @throws EdmException
     */
    private DSUser getUser(String username) throws EdmException {
        Preconditions.checkNotNull(username, "guid cannot be null");
        DSUser u = DSUser.findByIdOrNameOrExternalName(username);
        return u;
    }

    private DSDivision getDivision(String guid) throws EdmException {
        Preconditions.checkNotNull(guid, "guid cannot be null");
        DSDivision division = null;
        try{
            division = DSDivision.find(guid);
        }catch(DivisionNotFoundException e){
            division = DSDivision.findByCode(guid);
        }

        return division;
    }

    /**
     * W razie gdyby interfejs wymaga� po��czenia po innym rodzaju identyfikatora ni� id.
     * @param documentId
     * @return
     */
    private OfficeDocument getDocument(String documentId) throws EdmException {
        OfficeDocument document = null;

        if(documentId == null)
            return document;

        if(StringUtils.isNumeric(documentId)){
            document = OfficeDocument.find(Long.valueOf(documentId));
        } else {
            Long docId = CmisPrepend.lastId(documentId);
            document = OfficeDocument.find(docId);
        }

        return document;
    }

    /**
     * Zwraca dokumenty w sprawie
     *
     * @param officeCaseId
     * @return
     */
    @Override
    public List<AssociativeDocumentStore.AssociativeDocumentBean> getDocuments(String officeCaseId) throws Exception {
        List<AssociativeDocumentStore.AssociativeDocumentBean> results = Lists.newArrayList();
        OfficeCase officeCase = findOfficeCase(officeCaseId);
        List<OfficeDocument> documents = officeCase.getDocuments();

        for(OfficeDocument doc : documents){
            results.add(AssociativeDocumentStore.fromOfficeDocument(doc));
        }

        return results;
    }

    /**
     * Powi�zuje spraw� ze spraw�
     *
     * @param officeCaseId
     * @param officeCaseAddId
     */
    @Override
    public void addOfficeCaseToOfficeCase(String officeCaseId, String officeCaseAddId) throws EdmException {
        CaseToCaseAction ctc = new CaseToCaseAction();
        ctc.createCaseToCase(Long.valueOf(officeCaseAddId), Long.valueOf(officeCaseId));
    }

    /**
     * Usuwa spraw� ze sprawy
     *
     * @param officeCaseId
     * @param officeCaseRemoveId
     */
    @Override
    public void removeOfficeCaseToOfficeCase(String officeCaseId, String officeCaseRemoveId) throws SQLException, EdmException {
        CaseToCaseAction ctc = new CaseToCaseAction();
        ctc.removeCaseToCase(Long.valueOf(officeCaseRemoveId), Long.valueOf(officeCaseId));
    }

    /**
     * Aktualizuje spraw�
     *
     * @param officeCaseDto
     * @return
     */
    @Override
    public String updateOfficeCase(OfficeCaseDto officeCaseDto) throws EdmException {
        try{
            ContainerManager.OfficeCaseInfo officeCaseInfo = new ContainerManager.OfficeCaseInfo(officeCaseDto);
            return containerManager.updateOfficeCase(officeCaseInfo);
        }catch(Exception e){
            log.error("", e);
            throw new EdmException(e.getMessage());
        }
    }

    /**
     * Usuwa spraw�
     *
     * @param officeCaseDto
     * @return
     */
    @Override
    public String deleteOfficeCase(String id, String reason) throws EdmException {
        try
        {
            containerManager.deleteOfficeCase(Long.valueOf(id), reason);
        }catch(Exception e){
            log.error("", e);
            throw new EdmException(e.getMessage());
        }
        return null;
    }

    /**
     * Zwrava histori� sprawy
     *
     * @param id
     * @return
     */
    @Override
    public List<WorkHistoryBean> getCaseAudit(String id) throws EdmException {
        List<WorkHistoryBean> results = Lists.newArrayList();
        try{
            OfficeCase oc = OfficeCase.find(Long.valueOf(id));
            if(oc == null)
                throw new EdmException("Taka sprawa nie istnieje");

            List<Audit> audits = oc.getAudit();
            for(Audit a : audits){
                results.add(new WorkHistoryBean(a));
            }
        }catch(Exception e){
            log.error("", e);
            throw new EdmException(e.getMessage());
        }

        return results;
    }
}
