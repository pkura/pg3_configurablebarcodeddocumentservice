package pl.compan.docusafe.spring.user.office;

import com.beust.jcommander.internal.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.compan.docusafe.api.user.UserService;
import pl.compan.docusafe.api.user.office.JasperReportDto;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.UserToJasperReports;
import pl.compan.docusafe.parametrization.archiwum.reports.JasperReportsKind;
import pl.compan.docusafe.parametrization.archiwum.reports.JasperReportsLogic;
import pl.compan.docusafe.reports.ReportEnvironment;
import pl.compan.docusafe.spring.user.UserServiceImpl;
import pl.compan.docusafe.web.reports.JasperReportsAction;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Klasa reprezetuj�ca metody do obs�ugi Jasper-report�w
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 22.07.14
 */
@Service
public class JasperReportServiceImpl {

    @Autowired
    private UserService userService;

    /**
     * zwraca liste raport�w jaspera
     * @return
     */
    public List<KeyElement> getReportsList() throws EdmException {
        String userName = DSApi.context().getDSUser().getName();
        Long userId = DSApi.context().getDSUser().getId();

        List<KeyElement> results = Lists.newArrayList();
        List<JasperReportsKind> kindList = JasperReportsKind.list();

        for(JasperReportsKind k : kindList){
            if (GlobalPreferences.getAdminUsername().equals(userName)) {
                results.add(new KeyElement(k.getPath(), k.getTitle()));
            } else {
                List<UserToJasperReports> list = JasperReportsAction.getUsersToJasperReportsList(userId);
                for (UserToJasperReports utjr : list) {
                    if (utjr.getReportId().equals(k.getId())) {
                        results.add(new KeyElement(k.getPath(), k.getTitle()));

                    }
                }
            }
        }

        return results;
    }

    public List<KeyElement> getReportsType(){
        List<KeyElement> listOfReportType = Lists.newArrayList();
        listOfReportType.add(new KeyElement("PDF", "PDF"));
        listOfReportType.add(new KeyElement("HTML", "HTML"));
        listOfReportType.add(new KeyElement("XLS", "XLS"));
        listOfReportType.add(new KeyElement("XML", "XML"));
        listOfReportType.add(new KeyElement("CSV", "CSV"));
        listOfReportType.add(new KeyElement("RTF", "RTF"));


        return listOfReportType;
    }

    /**
     * Wywoluje raporty jaspera instantowo
     * @param report
     * @return
     * @throws Exception
     */
    public String addReport(JasperReportDto report) throws Exception {
        JasperReportsLogic jasper = new JasperReportsLogic();
        String userName = report.getUserId();
        report.setUserId(getUserId(userName));
        File archivePackage = jasper.getReport(report);
        return Base64.encodeBase64String(FileUtils.readFileToByteArray(archivePackage));
    }

    /**
     * Rejestruje do kolejki
     * @param report
     * @return
     * @throws Exception
     */
    public String addReportToService(JasperReportDto report) throws Exception {
        ReportEnvironment reportEnvironment = new ReportEnvironment();
        Map<String, Object> values = Maps.newHashMap();
        //@TODO doda� parametry
        Map<String, Object> invalues = Maps.newHashMap();
        reportEnvironment.registerReport("jasperReport",values, invalues);
            return null;
    }

    private String getUserId(String userName) throws EdmException {
        try{
        return userService.getByIdOrNameOrExternalName(userName).get().getId().toString();
        }catch (Exception e){
            throw new EdmException("Brak u�ytkownika o tym numerze");
        }
    }
}
