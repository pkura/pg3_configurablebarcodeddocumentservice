package pl.compan.docusafe.spring.user;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.rest.helpers.MultiOptional;
import pl.compan.docusafe.rest.views.AttachmentView;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

@Service
public class AttachmentService {

//    private boolean inDSContext = false;
//
//    public AttachmentService(boolean inDSContext) {
//        this.inDSContext = inDSContext;
//    }

    public AttachmentService inDSContext(final HttpServletRequest request) {
        return new AttachmentServiceInContext(request);
    }

    public MultiOptional<AttachmentRevision> getAttachmentRevision(Long revisionId) throws EdmException {
        try {
            AttachmentRevision attachmentRevision = AttachmentRevision.find(revisionId);
            return MultiOptional.of(attachmentRevision);
        } catch(AttachmentRevisionNotFoundException ex) {
            return MultiOptional.absent();
        } catch(AccessDeniedException ex) {
            return MultiOptional.denied();
        }
    }

    public MultiOptional<Attachment> getAttachment(Long attachmentId) throws EdmException {
        try {
            Attachment attachment = Attachment.find(attachmentId);
            return MultiOptional.of(attachment);
        } catch(AttachmentNotFoundException ex) {
            return MultiOptional.absent();
        } catch(DocumentNotFoundException ex) {
            return MultiOptional.absent();
        } catch(AccessDeniedException ex) {
            return MultiOptional.denied();
        }
    }

    public MultiOptional<AttachmentView> getAttachmentView(Long attachmentId) throws EdmException {
        MultiOptional<Attachment> attOpt = getAttachment(attachmentId);
        if(attOpt.isPresent()) {
            return MultiOptional.of(new AttachmentView(attOpt.get()));
        } else {
            return MultiOptional.transform(attOpt);
        }
    }

    public List<AttachmentView> getAttachmentViewsByDocumentId(Long documentId) throws EdmException, SQLException {
        List<Attachment> attachments = Attachment.findAllAttachmentByDocumentId(documentId);
        return FluentIterable.from(attachments).transform(new Function<Attachment, AttachmentView>() {
            @Override
            public AttachmentView apply(Attachment attachment) {
                return new AttachmentView(attachment);
            }
        }).toList();
    }

    public void putRevisionStreamInResponse(AttachmentRevision revision, String download, HttpServletResponse response) throws EdmException, IOException {
        String filename = revision.getOriginalFilename();
        InputStream is = null;
        try {
            if(download != null) {
                response.setHeader("Content-Disposition", "attachment; filename=\""+revision.getOriginalFilename()+"\"");
                response.setHeader("Content-Transfer-Encoding", "binary");
            }
            response.flushBuffer();
            is = revision.getBinaryStream();
            IOUtils.copy(is, response.getOutputStream());
        } finally {
            if(is != null) {
                is.close();
            }
        }
    }

}
