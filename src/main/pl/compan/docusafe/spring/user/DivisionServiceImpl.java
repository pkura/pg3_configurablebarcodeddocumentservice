package pl.compan.docusafe.spring.user;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionService;
import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.rest.ResourceNotFoundException;
import pl.compan.docusafe.util.DSApiUtils;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of DivisionService, all methods require open ds context
 */
@Service
public class DivisionServiceImpl implements DivisionService {
    private final static Function<DSDivision, DivisionDto> FROM_DS_DIVISION = new Function<DSDivision, DivisionDto>() {
        @Nullable
        @Override
        public DivisionDto apply(@Nullable DSDivision input) {
            return input == null ? null : fromDivision(input);
        }
    };

    private final static Predicate<DSDivision> NOT_HIDDEN_PREDICATE = new Predicate<DSDivision>() {
        @Override
        public boolean apply(DSDivision input) {
            return !input.isHidden();
        }
    };

    private static DivisionDto fromDivision(DSDivision input) {
        DivisionDto ret = new DivisionDto();
        ret.setId(input.getId());
        ret.setCode(input.getCode());
        ret.setGuid(input.getGuid());
        ret.setName(input.getName());
        ret.setType(DivisionType.FROM_STRING.apply(input.getDivisionType()));
        try {
            if(input.getParent() != null) {
                ret.setParentId(input.getParent().getId());
            }
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
        return ret;
    }

    private static void updateWith(DSDivision division, DivisionDto dto){
        division.setName(dto.getName());
        division.setCode(dto.getCode());
        try {
            if(!Objects.equal(division.getParent().getId(), dto.getParentId())){
                division.setParent(DSDivision.findById(dto.getParentId()));
            }
        } catch (DivisionNotFoundException ex) {
            throw new IllegalArgumentException(ex);
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static void checkDivisionWithId(DivisionDto div){
        checkNotNull(div, "division cannot be null");
        checkArgument(div.getId() != null, "division must have id");
    }


    @Override
    public List<DivisionDto> getAllDivisions() {
        try {
            return FluentIterable.from(Arrays.asList(DSDivision.getAllDivisions()))
                    .filter(NOT_HIDDEN_PREDICATE)
                    .transform(FROM_DS_DIVISION)
                    .toList();
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public List<UserDto> getUsers(DivisionDto division) {
        checkDivisionWithId(division);
        try {
            return Lists.transform(Arrays.asList(DSDivision.findById(division.getId().intValue()).getUsers()), UserServiceImpl.FROM_DS_USER);
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<DivisionDto> getSubdivisions(DivisionDto parentDivision) {
        checkDivisionWithId(parentDivision);
        try {
            Iterable<DSDivision> subdivisions = Arrays.asList(
                    DSDivision.findById(parentDivision.getId()).getChildren());
            return FluentIterable.from(subdivisions)
                    .filter(NOT_HIDDEN_PREDICATE)
                    .transform(FROM_DS_DIVISION)
                    .toList();
        } catch (DivisionNotFoundException ex) {
            throw new IllegalArgumentException(ex);
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void addUser(DivisionDto division, UserDto user) {
        DSApiUtils.checkInTransaction();
        checkNotNull(division, "division cannot be null");
        checkNotNull(user, "user cannot be null");
        try {
            DSDivision.findById(division.getId()).addUser(DSUser.findById(user.getId()));
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void addUser(String divisionGuid, String externalName) {
        DSApiUtils.checkInTransaction();
        checkNotNull(divisionGuid, "division cannot be null");
        checkNotNull(externalName, "user cannot be null");
        try {
            DSDivision.find(divisionGuid).addUser(DSUser.findByExternalName(externalName));
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
     public boolean removeUser(DivisionDto division, UserDto user) {
        DSApiUtils.checkInTransaction();
        checkDivisionWithId(division);
        checkNotNull(user, "user cannot be null");
        try {
            return DSDivision.findById(division.getId()).removeUser(DSUser.findById(user.getId()));
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public boolean removeUser(String divisionGuid, String externalName) {
        DSApiUtils.checkInTransaction();
        checkNotNull(divisionGuid, "division cannot be null");
        checkNotNull(externalName, "user cannot be null");
        try {
            return DSDivision.find(divisionGuid).removeUser(DSUser.findByExternalName(externalName));
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public List<DivisionDto> getUserDivisions(UserDto user) {
        try {
            return FluentIterable.from(DSUser.findById(user.getId()).getDivisionsAsList())
                            .filter(NOT_HIDDEN_PREDICATE)
                            .transform(FROM_DS_DIVISION)
                            .toList();
        } catch (UserNotFoundException ex) {
            throw new IllegalArgumentException("Brak użytkownika o id = " + user.getId());
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<DivisionDto> getByGuid(String guid) {
        try {
            DSDivision dsDivision = DSDivision.find(checkNotNull(guid, "guid cannot be null"));
            return dsDivision.isHidden()
                    ? Optional.<DivisionDto>absent()
                    : Optional.of(fromDivision(dsDivision));
        } catch (DivisionNotFoundException ex) {
            return Optional.absent();
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Optional<DivisionDto> getById(long divisionId) {
        try {
            DSDivision dsDivision = DSDivision.findById(divisionId);
            return dsDivision.isHidden()
                    ? Optional.<DivisionDto>absent()
                    : Optional.of(fromDivision(dsDivision));
        } catch (DivisionNotFoundException ex) {
            return Optional.absent();
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public long create(DivisionDto newDivision) {
        DSApiUtils.checkInTransaction();
        checkNotNull(newDivision, "division cannot be null");
        checkArgument(newDivision.getParentId() != null, "cannot create division without parent id");
        try {
            DSDivision ret = DSDivision.findById(newDivision.getParentId())
                    .createDivision(newDivision.getName(), newDivision.getCode(), newDivision.getGuid());
            ret.setDivisionType(newDivision.getType().getId());
            updateWith(ret, newDivision);
            return ret.getId();
        } catch (DuplicateNameException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void update(DivisionDto division) {
        DSApiUtils.checkInTransaction();
        checkDivisionWithId(division);
        try {
            DSDivision dsDivision = DSDivision.findById(division.getId());
            updateWith(dsDivision, division);
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void delete(DivisionDto division) throws ResourceNotFoundException {
        DSApiUtils.checkInTransaction();
        checkDivisionWithId(division);
        try {
            DSDivision dsDivision = DSDivision.findById(division.getId());
            if(dsDivision.hasUsers()){
                throw new EdmException("Dział zawiera użytkowników!");
            }

            dsDivision.setHidden(true);
        } catch (EdmException ex) {
            throw new ResourceNotFoundException(ex.getMessage());
        }
    }
}
