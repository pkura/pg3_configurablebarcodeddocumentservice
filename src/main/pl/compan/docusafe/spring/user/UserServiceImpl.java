package pl.compan.docusafe.spring.user;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.UserService;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.rest.ResourceNotFoundException;
import pl.compan.docusafe.util.DSApiUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.form.UpdateUser;
import pl.compan.docusafe.web.office.RolesAction;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

/**
 *  Implementation of UserService, all methods require open ds context
 */
@Service
public class UserServiceImpl implements UserService {

    private final static Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    public final static Function<DSUser, UserDto> FROM_DS_USER = new Function<DSUser, UserDto>() {
        @Override
        public @Nullable UserDto apply(@Nullable DSUser dsUser) {
            return dsUser == null ? null : new UserDto(dsUser);
        }
    };

    @Override
    public List<UserDto> getAllUsers() {
        try {
            return Lists.transform(DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME), FROM_DS_USER);
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Optional<UserDto> getByNameOrExternalName(String name) {
        DSUser ret = null;
        try {
            ret = DSUser.findByUsername(name);
        } catch (UserNotFoundException e) {
            try {
                ret = DSUser.findByExternalName(name);
            } catch (EdmException e1) {
                return Optional.absent();
            }
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }

        return getResult(ret);
    }

    private Optional<UserDto> getResult(DSUser ret) {
        return ret.isDeleted()
                ? Optional.<UserDto>absent()
                : Optional.of(fromUser(ret));
    }

    @Override
    public Optional<UserDto> getById(long id) {
        try {
            DSUser ret = DSUser.findById(id);
            return getResult(ret);
        } catch (UserNotFoundException e) {
            return Optional.absent();
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public long create(UserDto user) {
        DSApiUtils.checkInTransaction();
        user.validate();
        try {
            DSUser dsUser = UserFactory.getInstance().createUser(user.getUsername(),
                    user.getFirstname(), user.getLastname());
            if(user.getPassword() != null) {
                dsUser.setPassword(user.getPassword(), true);
            }
            updateWith(dsUser, user);
            user.setId(dsUser.getId());
            return dsUser.getId();
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }
    }

    public void update(UserDto user){
        DSApiUtils.checkInTransaction();
        if(user.getId() == null){
            throw new IllegalArgumentException("invalid id");
        }
        try {
            DSUser dsUser = UserFactory.getInstance().findById(user.getId());
            updateWith(dsUser, user);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static UserDto fromUser(DSUser user){
        UserDto ret = new UserDto();
        ret.setFirstname(user.getFirstname());
        ret.setLastname(user.getLastname());
        ret.setId(user.getId());
        ret.setUsername(user.getName());
        ret.setPersonNumber(user.getExternalName());
        ret.setEmail(user.getEmail());
        return ret;
    }

    /**
     * Update selected DSUser with attributes from selected UserDto
     * @param dsUser target of update
     * @param user source of update
     */
    private void updateWith(DSUser dsUser, UserDto user) throws EdmException {
        dsUser.setFirstname(user.getFirstname());
        dsUser.setLastname(user.getLastname());
        dsUser.setEmail(user.getEmail());
        dsUser.setExternalName(user.getPersonNumber());
        dsUser.update();
    }

    @Override
    public void delete(UserDto user) throws EdmException {
        DSApiUtils.checkInTransaction();
        if(user.getId() == null){
            throw new IllegalArgumentException("invalid id");
        }
        try {
            DSUser dsUser = UserFactory.getInstance().findById(user.getId());
            dsUser.setDeleted(true);
            dsUser.update();
        }catch(UserNotFoundException e){
            DSUser dsUser = UserFactory.getInstance().findByExternalName(user.getPersonNumber());
            dsUser.setDeleted(true);
            dsUser.update();
        } catch (EdmException e) {
            throw new ResourceNotFoundException(e);
        }
    }

    /**
     * Zwraca dost�pne role w systemie
     *
     * @return
     */
    @Override
    public List<KeyElement> getAvailableOfficeRoles() throws EdmException {
        Role[] roles = Role.list();
        Collection<KeyElement> elements = convertRoleToKeyElement(Lists.newArrayList(roles));
        return Lists.newArrayList(elements.iterator());
    }

    protected Collection<KeyElement> convertRoleToKeyElement(List<Role> roles) {
        return Collections2.transform(roles, new Function<Role, KeyElement>() {
            @Nullable
            @Override
            public KeyElement apply(@Nullable Role input) {
                return new KeyElement(input.getId().toString(), input.getName());
            }
        });
    }

    /**
     * Lista r�l jakie b�d� zaktualizowane u�ytkownikowi
     *
     * @param roles
     */
    @Override
    public void updateUserOfficeRoles(List<Long> roles, String user) throws EdmException {
        try{
            UpdateUser.updateOfficeRoles(roles.toArray(new Long[roles.size()]), DSUser.findByIdOrNameOrExternalName(user));
        }catch(Exception e){
            LOG.error("", e);
            throw new ResourceNotFoundException("");
        }
    }

    /**
     * Zwraca list� r�l usera
     *
     * @param id
     * @return
     */
    @Override
    public List<KeyElement> getUserOfficeRoles(String id) throws EdmException {
        List<KeyElement> els = Lists.newArrayList();
        try
        {
            DSUser user = DSUser.findByIdOrNameOrExternalName(id);
            List<Long> userRolesId = Role.listUserRoles(user.getName());
            Role[] roles = Role.list();

            List<Role> userRoles = Lists.newArrayList();
            for(Role r : roles){
                if(userRolesId.contains(r.getId())){
                    userRoles.add(r);
                }
            }

            Collection<KeyElement> keyElements = convertRoleToKeyElement(userRoles);
            els =  Lists.newArrayList(keyElements.iterator());
        }catch(Exception e){
            LOG.error("", e);
            throw new ResourceNotFoundException("");
        }

        return els;
    }

	@Override
	public Optional<UserDto> getByIdOrNameOrExternalName(String userId) {
		Optional<UserDto> optionalUserDto;
		try{
			DSUser user = DSUser.findByIdOrNameOrExternalName(userId);
			optionalUserDto = getResult(user);
		}catch(Exception e){
            LOG.error("", e);
            throw new ResourceNotFoundException("");
        }
		return optionalUserDto;
	}
}
