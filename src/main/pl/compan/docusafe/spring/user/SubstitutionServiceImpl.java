package pl.compan.docusafe.spring.user;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.FluentIterable;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.compan.docusafe.api.user.*;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.util.DSApiUtils;

import javax.annotation.Nullable;
import java.util.List;

/**
 *
 */
@Service
public class SubstitutionServiceImpl implements SubstitutionService {

    private final Function<SubstituteHistory, SubstitutionDto> fromSubstituteHistory
            = new Function<SubstituteHistory, SubstitutionDto>() {
        @Nullable
        @Override
        public SubstitutionDto apply(@Nullable SubstituteHistory input) {
            return input == null ? null : fromSubstituteHistory(input);
        }
    };

    @Autowired
    private UserService userService;

    private UserDto unknownUser(String login){
        UserDto ret = new UserDto();
        ret.setUsername(login);
        ret.setFirstname("Unknown user '" + login + "'");
        ret.setLastname("");
        return ret;
    }

    private SubstitutionDto fromSubstituteHistory(SubstituteHistory input) {
        SubstitutionDto ret = new SubstitutionDto();
        Optional<UserDto> user = userService.getByNameOrExternalName(input.getLogin());
        if(user.isPresent()) { //nie ma gwarancji, �e u�ytkownicy istniej�
            ret.setUserSubsituted(user.get());
        } else {
            ret.setUserSubsituted(unknownUser(input.getLogin()));
        }
        user = userService.getByNameOrExternalName(input.getSubstLogin());
        if(user.isPresent()) {
            ret.setUserSubstituting(user.get());
        } else {
            ret.setUserSubstituting(unknownUser(input.getSubstLogin()));
        }
        ret.setStart(new DateTime(input.getSubstitutedFrom()));
        ret.setEnd(new DateTime(input.getSubstitutedThrough()));
        ret.setId(input.getId());
        return ret;
    }

    @Override
    public Optional<SubstitutionDto> getById(long id) {
        try {
            SubstituteHistory substitution = SubstituteHistory.find(id);
            if(substitution != null && substitution.getStatus() == SubstituteHistory.DELETED){
                return Optional.absent();
            }
            return Optional.fromNullable(substitution)
                           .transform(fromSubstituteHistory);
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<SubstitutionDto> getAllCurrentSubstitutions() {
        return getSubstitutionsAtTime(DateTime.now());
    }

    @Override
    public List<SubstitutionDto> getSubstitutionsAtTime(DateTime time) {
        try {
            return FluentIterable.from(SubstituteHistory.getSubstitutionsAtTime(time.toDate()))
                                 .transform(fromSubstituteHistory)
                                 .toList();
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<SubstitutionDto> getUserSubstitution(UserDto user) {
        try {
            return Optional.fromNullable(SubstituteHistory.findActualSubstitute(user.getUsername()))
                    .transform(fromSubstituteHistory);
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<SubstitutionDto> getSubstitutedByUser(UserDto user) {
        try {
            return FluentIterable.from(SubstituteHistory.getSubstitutedByUser(user.getUsername()))
                                 .transform(fromSubstituteHistory)
                                 .toList();
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public long create(SubstitutionDto substitution) throws EdmException {
        DSApiUtils.checkInTransaction();
        SubstituteHistory hist = new SubstituteHistory(substitution);
        try {
            hist.create();
            return hist.getId();
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void cancel(SubstitutionDto substitution) {
        try {
            SubstituteHistory substituteHistory = SubstituteHistory.find(substitution.getId());
            if(substituteHistory != null
                    && substituteHistory.getStatus() != SubstituteHistory.DELETED) {
                substituteHistory.cancel();
            } else if (substituteHistory == null) {
                throw new IllegalArgumentException("Substitution " + substitution.getId() + " not found");
            } else if (substituteHistory.getStatus() == SubstituteHistory.DELETED){
                throw new IllegalArgumentException("Substitition " + substitution.getId() + " already cancelled");
            }
        } catch (EdmException ex) {
            throw new RuntimeException(ex);
        }
    }
}
