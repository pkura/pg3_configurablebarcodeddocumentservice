package pl.compan.docusafe.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.util.ObjectRing;

import java.util.HashMap;
import java.util.Map;

/* User: Administrator, Date: 2005-06-28 10:08:25 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Console.java,v 1.3 2006/02/20 15:42:27 lk Exp $
 */
public class Console
{
    public static final int FATAL = 1;
    public static final int ERROR = 2;
    public static final int WARN = 3;
    public static final int INFO = 4;
    public static final int DEBUG = 5;

    private static final Map instances = new HashMap();

    private String serviceId;
    // trzymanych jest 128 najnowszych wiadomości
    private ObjectRing messages = new ObjectRing(128);
    private Log log;

    public static class Message
    {
        private long timestamp;
        private String driverId;
        private int logLevel;
        private String message;

        public Message(long timestamp, String driverId, int logLevel, String message)
        {
            this.timestamp = timestamp;
            this.driverId = driverId;
            this.logLevel = logLevel;
            this.message = message;
        }

        public String getLogLevelName()
        {
            switch (logLevel)
            {
                case FATAL: return "FATAL";
                case ERROR: return "ERROR";
                case WARN: return "WARN";
                case INFO: return "INFO";
                case DEBUG: return "DEBUG";
                default: return "";
            }
        }

        public long getTimestamp()
        {
            return timestamp;
        }

        public String getDriverId()
        {
            return driverId;
        }

        public int getLogLevel()
        {
            return logLevel;
        }

        public String getMessage()
        {
            return message;
        }
    }

    public Console(String serviceId)
    {
        this.serviceId = serviceId;
        this.log = LogFactory.getLog("service."+serviceId);
    }

    synchronized static Console getInstance(String serviceId)
    {
        Console console = (Console) instances.get(serviceId);
        if (console == null)
        {
            console = new Console(serviceId);
            instances.put(serviceId, console);
        }
        return console;
    }

    void log(String driverId, int logLevel, String message, Throwable t)
    {
        if (logLevel > DEBUG) logLevel = DEBUG;
        if (logLevel < FATAL) logLevel = FATAL;

        switch (logLevel)
        {
            case FATAL: log.fatal(message, t); break;
            case ERROR: log.error(message, t); break;
            case WARN: log.warn(message, t); break;
            case INFO: log.info(message, t); break;
            case DEBUG: log.debug(message, t); break;
        }

        Message m = new Message(System.currentTimeMillis(), driverId, logLevel, message);

        messages.add(m);
    }

    Message[] getMessages()
    {
        return (Message[]) messages.list(new Message[0]);
    }
}
