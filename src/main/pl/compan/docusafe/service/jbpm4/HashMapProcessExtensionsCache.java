package pl.compan.docusafe.service.jbpm4;

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.process.form.ActivityController;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.ProcessExtensions;
import pl.compan.docusafe.process.form.Button;
import pl.compan.docusafe.process.form.LinkButton;
import pl.compan.docusafe.process.form.MessageButton;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.io.InputStream;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Implementacja ProcessExtensionsCache, inicjalizuje obiekty tak p�no jak si� da
 */
public class HashMapProcessExtensionsCache extends ServiceDriver implements ProcessExtensionsCache{
    private final static Logger LOG = LoggerFactory.getLogger(HashMapProcessExtensionsCache.class);

    private final static AtomicInteger parseCount = new AtomicInteger(0);

    volatile Map<String, ProcessExtensions> extensions
        = new ConcurrentHashMap<String, ProcessExtensions>();

    /**
     * Udost�pnia rozszerzenia procesu
     * @param processDefinitionId - identyfikator definicji procesu
     * @return ProcessExtensions, nigdy null
     */
    public ProcessExtensions provide(String processDefinitionId) {
        Preconditions.checkNotNull(processDefinitionId, "processDefinitionId cannot be null");
        LOG.info("provide");
        //mo�liwa wielokrotna inicjalizacja ProcessExtensions
        if(!extensions.containsKey(processDefinitionId)){
            extensions.put(processDefinitionId, generate(processDefinitionId));
        }
        return extensions.get(processDefinitionId);
    }

    private static ProcessExtensions generate(String processId){
        ProcessExtensions ret = new ProcessExtensions();

        LOG.info("generating process extensions");
        
        String deploymentId = Jbpm4Provider.getInstance()
                .getRepositoryService()
                .createProcessDefinitionQuery()
                .processDefinitionId(processId)
                .uniqueResult()
                .getDeploymentId();

        Set<String> resources = Jbpm4Provider.getInstance()
                .getRepositoryService()
                .getResourceNames(deploymentId);

        for(String resource : resources){
            if(resource.endsWith(".activities.xml")){
                InputStream is = null;
                try{
                    is = Jbpm4Provider.getInstance()
                        .getRepositoryService()
                        .getResourceAsStream(deploymentId, resource);
                    Document doc = new SAXReader().read(is);
                    ret.setActivityController(parse(doc));
                    LOG.info("document parsed ok");
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                } finally {
                    IOUtils.closeQuietly(is);
                }
                break;
            }
        }

        return ret;
    }

    private static ActivityController parse (Document doc){
        LOG.info("parsing activities.xml - total count = {}", parseCount.getAndIncrement());
        StringManager sm = GlobalPreferences.loadPropertiesFile(HashMapProcessExtensionsCache.class.getPackage().getName(),null);

        Multimap<String, Button> buttons = ArrayListMultimap.create();
        Multimap<String, LinkButton> linkButtons = ArrayListMultimap.create();
        Multimap<String, MessageButton> messageButtons = ArrayListMultimap.create();

        for(Element el : (Collection<Element>)doc.getRootElement().elements("state")){
            String stateName = el.attributeValue("name");
            for(Element sb : (Collection<Element>)el.elements("button")){
                buttons.put(stateName,
                    ActivityController.createButton(
                            sb.attributeValue("label"),
                            sb.attributeValue("name"),
                            sb.attributeValue("fieldCn"),
                            "true".equals(sb.attributeValue("inNewLine")),
                            "true".equals(sb.attributeValue("confirm")),
                            StringUtils.isEmpty(sb.attributeValue("confirmMsg"))?sm.getString("confirmActionMsg"):sb.attributeValue("confirmMsg"),
                            "true".equals(sb.attributeValue("confirmConditional")),
                            sb.attributeValue("confirmMsgConditionalField"),
                            sb.attributeValue("confirmMsgConditionalValue"),
                            sb.attributeValue("onClick"),
                            StringUtils.isEmpty(sb.attributeValue("cssClass"))?"btn":sb.attributeValue("cssClass"),
                            "true".equals(sb.attributeValue("correction")),
                            "true".equals(sb.attributeValue("force-remark")),
                            "true".equals(sb.attributeValue("store-activity")),
                            "true".equals(sb.attributeValue("noValidate")),
                            "true".equals(sb.attributeValue("sign-activity")),
                            "true".equals(sb.attributeValue("selectAcceptance")),
                            "true".equals(sb.attributeValue("selectAcceptanceExtended"))));
            }

            for(Element slb : (Collection<Element>)el.elements("link-button")){
                linkButtons.put(stateName,
                        ActivityController.createLinkButton(
                                slb.attributeValue("label"),
                                slb.attributeValue("link"),
                                slb.attributeValue("queryString")));
            }
            
            for(Element slb : (Collection<Element>)el.elements("submit-button")){
                linkButtons.put(stateName,
                        ActivityController.createSubmitButton(
                                slb.attributeValue("label"),
                                slb.attributeValue("queryString")));
            }

            for(Element slb : (Collection<Element>)el.elements("template-button")){
                linkButtons.put(stateName,
                        ActivityController.createDownloadTemplateButton(
                                slb.attributeValue("label"),
                                slb.attributeValue("template-name"),
                                slb.attributeValue("special"),
                                slb.attributeValue("asPdf")));
            }

            for(Element slb : (Collection<Element>)el.elements("message-button")){
                messageButtons.put(stateName,
                        ActivityController.createMessageButton(
                                slb.attributeValue("label"),
                                slb.attributeValue("type")));
            }
        }
        LOG.info("tworzenie ActivityController");
        return new ActivityController(buttons, linkButtons, messageButtons);
    }

    @Override
    protected void start() throws ServiceException {

    }

    @Override
    protected void stop() throws ServiceException {

    }

    @Override
    protected boolean canStop() {
        return false;
    }
}
