package pl.compan.docusafe.service.jbpm4;

import pl.compan.docusafe.core.jbpm4.ProcessExtensions;
import pl.compan.docusafe.service.Service;

/**
 * Cache dla klas ProcessExtensions
 */
public interface ProcessExtensionsCache extends Service {

    String SERVICE_ID = "jbpm4-process-extensions-cache";

    /**
     * Udost�pnia obiekt ProcessExtensions dla danego identyfikatora, wymaga pe�nej inicjalizacji systemu docusafe
     * @param processId - identyfikator procesu (nie nazwa!)
     * @return
     */
    ProcessExtensions provide(String processId); 

}
