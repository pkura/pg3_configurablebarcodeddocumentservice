package pl.compan.docusafe.service.tasklist;

import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import com.google.common.collect.Sets;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;

public class RefreshTaskList extends ServiceDriver implements Service
{
	private Timer timer;

	private static Set<Long> documentsToRefresh = new HashSet<Long>();

	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		// 1s - 1000ms
		timer.schedule(new HandleTasks(), 1000, 1 * DateUtils.MINUTE);
	}

	protected boolean canStop()
	{
		return false;
	}

	protected void stop() throws ServiceException
	{
		if (timer != null)
			timer.cancel();
	}

	class HandleTasks extends TimerTask
	{
		Set<Long> toRemoveIds = new HashSet<Long>();

		public void run()
		{
			try
			{
			
				log.info("Uruchamiam refresh listy zada�");
				console(Console.INFO, "Uruchomienie serwisu RefreshTaskList.");
				DSApi.openAdmin();
				Set<Long> documents = RefreshTaskList.documentsToRefresh;

				DSApi.context().begin();
				console(Console.INFO, "List id dokument�w do od�wie�enie na li�cie zada� : " + documents);
				for (Long docId : documents)
				{
					console(Console.INFO, "Od�wie�enie docID: " + docId);
					JBPMTaskSnapshot.updateTaskList(docId, "anyType", TaskSnapshot.UPDATE_TYPE_ONE_DOC, null);
					toRemoveIds.add(docId);
				}

				
				DSApi.context().commit();
				/**
				 * EventsService
				 */

                // jest juz w LabelsService -> zakomentowuje
//				DSApi.context().begin();
//				EventFactory.startEventByHandler(null);
//				DSApi.context().commit();

			}
			catch (Throwable t)
			{
				console(Console.ERROR, t.getMessage());
				t.printStackTrace();
				log.error(t.getMessage(), t);
			}
			finally
			{
				RefreshTaskList.documentsToRefresh.removeAll(toRemoveIds);
				toRemoveIds.clear();
				try
				{
					DSApi._close();
				}
				catch (Exception e3)
				{
					console(Console.ERROR, e3.getMessage());
					log.error(e3.getMessage(), e3);
				}
			}
		}

	}

	public static Set<Long> getDocumentsToRefresh()
	{
		return documentsToRefresh;
	}

	public static void setDocumentsToRefresh(Set<Long> documentsToRefresh)
	{
		RefreshTaskList.documentsToRefresh = documentsToRefresh;
	}

	public static void addDocumentToRefresh(long id)
	{
		RefreshTaskList.documentsToRefresh.add(id);
	}

}
