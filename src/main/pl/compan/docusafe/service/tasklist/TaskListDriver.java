package pl.compan.docusafe.service.tasklist;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.bookmarks.FilterCondition;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;

import com.google.common.collect.Sets;

/* User: Administrator, Date: 2005-08-22 13:23:40 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: TaskListDriver.java,v 1.58 2010/08/03 16:59:56 pecet5 Exp $
 */
public class TaskListDriver extends ServiceDriver implements TaskList {

	private final static Set<String> NO_VALUE_FILTER_BY = Sets.newHashSet(Arrays.asList("task_count_1", "task_count_2_or_more"));

	/**
	 * Zwraca dwuelementow? tablic� zawieraj?c? liczby zada� przyj�tych i
	 * oczekuj?cych na u�ytkownika.
	 */
	public int[] getTaskCount(DSUser user) throws EdmException {
		// zadania w wewn�trznym workflow
		int[] taskCount = WorkflowFactory.getTaskCount(user.getName());
		return taskCount;
	}

	public List<Task> getTasks(TaskListParameters parameters) throws EdmException {
		return null;
	}

	public Map getDocumentTasks(DSDivision divisionA, DSUser user, String documentType, int offset, int LIMIT) throws EdmException {
		return getDocumentTasks(divisionA, user, documentType, null, "documentOfficeNumber", true, offset, LIMIT);
	}

	public Map getDocumentTasks(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField,
			boolean ascending, int offset, int LIMIT) throws EdmException {
		return getDocumentTasks(divisionA, user, documentType, searchOfficeNumber, sortField, ascending, offset, LIMIT, null, null);
	}

	public Map getDocumentTasks(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField,
			boolean ascending, int offset, int LIMIT, String filterBy, Object filterValue) throws EdmException {
		return getDocumentTasks(divisionA, user, documentType, searchOfficeNumber, sortField, ascending, offset, LIMIT, filterBy,
				filterValue, Boolean.TRUE);
	}

	public Map getDocumentTasks(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField,
			boolean ascending, int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup) throws EdmException {
		return getDocumentTasks(divisionA, user, documentType, searchOfficeNumber, sortField, ascending, offset, LIMIT, filterBy,
				filterValue, withBackup, null, true, null);
	}

	public Map getDocumentTasks(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField,
			boolean ascending, int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup, Collection labelsFiltering,
			boolean labelsAll, List<FilterCondition> filterConditions) throws EdmException {
		if (divisionA == null) {
			if (user == null)
				throw new NullPointerException("user");

		}
		if (InOfficeDocument.TYPE.equals(documentType)) {
			return getInDocumentTasks(divisionA, user, searchOfficeNumber, sortField, ascending, offset, LIMIT, filterBy, filterValue,
					withBackup, labelsFiltering, labelsAll, filterConditions);
		} else if (OutOfficeDocument.INTERNAL_TYPE.equals(documentType)) {
			return getInternalDocumentTasks(divisionA, user, searchOfficeNumber, sortField, ascending, offset, LIMIT, filterBy,
					filterValue, withBackup, labelsFiltering, labelsAll, filterConditions);
		} else if (OutOfficeDocument.TYPE.equals(documentType)) {
			return getOutDocumentTasks(divisionA, user, searchOfficeNumber, sortField, ascending, offset, LIMIT, filterBy, filterValue,
					withBackup, labelsFiltering, labelsAll, filterConditions);
		} else if (OfficeOrder.TYPE.equals(documentType)) {
			return getOfficeOrderTasks(divisionA, user, searchOfficeNumber, sortField, ascending, offset, LIMIT);
		}

		throw new EdmException("Nieznany typ dokumentu: " + documentType);
	}    
	public Map getInDocumentTasksPackage(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField,
			boolean ascending, int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup, Collection labelsFiltering,
			boolean labelsAll, List<FilterCondition> filterConditions) throws EdmException {
		if (divisionA == null) {
			if (user == null)
				throw new NullPointerException("user");

			return getInDocumentTasksPackage(divisionA, user, searchOfficeNumber, sortField, ascending, offset, LIMIT, filterBy,
					filterValue, withBackup, labelsFiltering, labelsAll, filterConditions);
		
		}else
			throw new EdmException("getOutgoingDocumentTasksPackage  - typ przekazany: " + documentType);
		}
	public Map getInDocumentTasksWitoutPackage(DSDivision divisionNS, DSUser user, Integer offset, int tempLimit, Integer searchOfficeNumber, String sortField,
			Boolean ascending, String parsedFilterBy, Object filterValue, Boolean withBackup, Collection<Label> labels, boolean allLabels,
			List<FilterCondition> filterConditions) throws EdmException
	{
		if(divisionNS!=null){
			user = null;
			return getInDocumentTasksWotoutPackage(divisionNS, user, searchOfficeNumber, sortField, ascending, offset, tempLimit, parsedFilterBy, filterValue, withBackup, labels, allLabels, filterConditions);
		}
		else if (divisionNS == null) {
					if (user == null)
						throw new NullPointerException("user");
				return getInDocumentTasksWotoutPackage(divisionNS, user, searchOfficeNumber, sortField, ascending, offset, tempLimit, parsedFilterBy, filterValue, withBackup, labels, allLabels, filterConditions);
        }else
            throw new EdmException("getIncomingDocumentTasksPackage  - typ przekazany: " + "");
	}
	
	/*public Map getInDocumentTasksWitoutPackage(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField,
			boolean ascending, int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup, Collection labelsFiltering,
			boolean labelsAll, List<FilterCondition> filterConditions) throws EdmException {
		if (divisionA == null) {
			if (user == null)
				throw new NullPointerException("user");

			return getIncomingDocumentTaskWitoutPackage(divisionA, user, searchOfficeNumber, sortField, ascending, offset, LIMIT, filterBy,
					filterValue, withBackup, labelsFiltering, labelsAll, filterConditions);
		
		}else
			throw new EdmException("getOutgoingDocumentTasksPackage  - typ przekazany: " + documentType);
		}
		*/
	
	public Map getInDocumentTasks(DSDivision divisionA, DSUser user, int offset, int LIMIT, Integer officeNumber, String sortField,
			boolean ascending) throws EdmException {
		return getInDocumentTasks(divisionA, user, offset, LIMIT, officeNumber, sortField, ascending, null, null);
	}

	public Map getInDocumentTasks(DSDivision divisionA, DSUser user, int offset, int LIMIT, Integer officeNumber, String sortField,
			boolean ascending, String filterBy, Object filterValue) throws EdmException {
		return getInDocumentTasks(divisionA, user, offset, LIMIT, officeNumber, sortField, ascending, filterBy, filterValue, null, null,
				true, null);// skasowalem poniewaz tak wybieral domyslnie tylko
							// backupowe
		// return getInDocumentTasks(user, offset, LIMIT, officeNumber,
		// sortField, ascending, filterBy, filterValue,Boolean.TRUE,null,true);
	}

	public Map getInDocumentTasks(DSDivision divisionA, DSUser user, int offset, int LIMIT, Integer officeNumber, String sortField,
			boolean ascending, String filterBy, Object filterValue, Boolean withBackup, Collection labels, boolean labelsAll,
			List<FilterCondition> filterConditions) throws EdmException {
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		if (LIMIT == 0)
			query = new TaskSnapshot.Query();
		else
			query = new TaskSnapshot.Query(offset, LIMIT);
		query.setDocumentType(InOfficeDocument.TYPE);
		query.setUser(user);
		query.setDivision(divisionA);
		query.setAscending(ascending);
		query.setSortField(sortField);
		query.setOfficeNumber(officeNumber);
		query.setSubstitutions(true);
		query.setWithBackup(withBackup);

		if (NO_VALUE_FILTER_BY.contains(filterBy)) {
			query.setFilterBy(filterBy);
			query.setFilterValue(null);
		} else if (filterBy != null && filterValue != null) {
			query.setFilterBy(filterBy);
			query.setFilterValue(filterValue);
		}
		query.setFilterConditions(filterConditions);

		if (user != null) {
			if (DSApi.context().hasPermission(user, DSPermission.BOK_LISTA_PISM)) {
				query.setBOK(true);
			}
		}

		query.setLabelsFiltering(labels);
		query.setLabelsAll(labelsAll);
		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		for (TaskSnapshot task : results.results()) {
			res.add(task.toFullTask(flagsOn));
		}
		Map map = new LinkedHashMap<String, Object>();
		map.put("tasks", res);
		map.put("size", results.totalCount());
		return map;
	}

	/**
	 * Uproszczone pobieranie pism przychodzacych
	 * 
	 * @param user
	 * @return
	 * @throws EdmException
	 */
	public Map getSimpleInDocumentTasks(DSDivision divisionA, DSUser user, Integer searchOfficeNumber, String sortField, boolean ascending,
			int offset, int LIMIT) throws EdmException {
		return getInDocumentTasks(divisionA, user, searchOfficeNumber, sortField, ascending, offset, LIMIT, null, null);
	}

	/**
	 * Standardowe pobieranie pism przychodzacych
	 * 
	 * @param user
	 * @return
	 * @throws EdmException
	 */

	public List<Task> getTasks(DSUser user) throws EdmException {
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		query = new TaskSnapshot.Query();
		query.setUser(user);
		query.setAscending(true);
		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		for (TaskSnapshot task : results.results()) {
			res.add(task.toFullTask(flagsOn));
		}
		return res;
	}

	public List<Task> getTasks(DSUser user, int offset, int LIMIT) throws EdmException {
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		query = new TaskSnapshot.Query(offset, LIMIT);
		query.setUser(user);
		query.setAscending(true);
		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();

		for (TaskSnapshot task : results.results()) {
			res.add(task.toFullTask(flagsOn));
		}
		return res;
	}

	public Map getInDocumentTasks(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending, int offset,
			int LIMIT, String filterBy, Object filterValue) throws EdmException {
		return getInDocumentTasks(divisionA, user, officeNumber, sortField, ascending, offset, LIMIT, filterBy, filterValue, Boolean.FALSE,
				null, true, null);
	}

	public Map getInDocumentTasks(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending, int offset,
			int LIMIT, String filterBy, Object filterValue, Boolean withBackup, Collection labels, boolean labelsAll,
			List<FilterCondition> filterConditions) throws EdmException {
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		if (LIMIT == 0)
			query = new TaskSnapshot.Query();
		else
			query = new TaskSnapshot.Query(offset, LIMIT);
		query = new TaskSnapshot.Query();
		query.setDocumentType(InOfficeDocument.TYPE);
		query.setUser(user);
		query.setDivision(divisionA);
		query.setAscending(ascending);
		query.setSortField(sortField);
		query.setOfficeNumber(officeNumber);
		query.setSubstitutions(true);
		query.setWithBackup(withBackup);
		query.setLabelsFiltering(labels);
		query.setLabelsAll(labelsAll);

		if (filterBy != null && filterValue != null) {
			query.setFilterBy(filterBy);
			query.setFilterValue(filterValue);
		}
		query.setFilterConditions(filterConditions);

		setQueryFilterByPermission(user, query);
		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		for (TaskSnapshot task : results.results()) {
			res.add(task.toFullTask(flagsOn));
		}
		Map map = new LinkedHashMap<String, Object>();
		map.put("tasks", res);
		map.put("size", results.totalCount());
		return map;

	}

	/**
	 * Konfiguracja wyszukiwania w zale�no�ci od uprawnie�
	 * @param user
	 * @param query
	 * @throws EdmException
	 */
	private void setQueryFilterByPermission(DSUser user, TaskSnapshot.Query query) throws EdmException 
	{
		if(user == null)
			return;
		
		if (DSApi.context().hasPermission(user, DSPermission.BOK_LISTA_PISM))
			query.setBOK(true);
	}

	private Map getInternalDocumentTasks(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending,
			int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup) throws EdmException {
		return getInternalDocumentTasks(divisionA, user, officeNumber, sortField, ascending, offset, LIMIT, filterBy, filterValue,
				withBackup, null, true, null);

	}

	private Map getInternalDocumentTasks(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending,
			int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup, Collection labels, boolean labelsAll,
			List<FilterCondition> filterConditions) throws EdmException

	{
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		query = new TaskSnapshot.Query();
		if (LIMIT == 0)
			query = new TaskSnapshot.Query();
		else
			query = new TaskSnapshot.Query(offset, LIMIT);
		query.setDocumentType(OutOfficeDocument.INTERNAL_TYPE);
		query.setUser(user);
		query.setDivision(divisionA);
		query.setAscending(ascending);
		query.setSortField(sortField);
		query.setOfficeNumber(officeNumber);
		query.setSubstitutions(true);
		query.setWithBackup(withBackup);
		query.setLabelsFiltering(labels);
		query.setLabelsAll(labelsAll);
		if (filterBy != null && filterValue != null) {
			query.setFilterBy(filterBy);
			query.setFilterValue(filterValue);
		}
		query.setFilterConditions(filterConditions);

		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		for (TaskSnapshot task : results.results()) {
			res.add(task.toFullTask(flagsOn));
		}
		Map map = new LinkedHashMap<String, Object>();
		map.put("tasks", res);
		map.put("size", results.totalCount());
		return map;
	}

	
	private Map getInternalDocumentTasks(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending,
			int offset, int LIMIT, String filterBy, Object filterValue) throws EdmException {
		return getInternalDocumentTasks(divisionA, user, officeNumber, sortField, ascending, offset, LIMIT, filterBy, filterValue,
				Boolean.TRUE);
	}

	private Map getOutDocumentTasks(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending,
			int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup) throws EdmException {
		return getOutDocumentTasks(divisionA, user, officeNumber, sortField, ascending, offset, LIMIT, filterBy, filterValue, withBackup,
				null, true, null);
	}

	private Map getOutDocumentTasks(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending,
			int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup, Collection labels, boolean labelsAll,
			List<FilterCondition> filterConditions) throws EdmException {
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		query = new TaskSnapshot.Query();
		if (LIMIT == 0)
			query = new TaskSnapshot.Query();
		else
			query = new TaskSnapshot.Query(offset, LIMIT);
		query.setDocumentType(OutOfficeDocument.TYPE);
		query.setUser(user);
		query.setDivision(divisionA);
		query.setAscending(ascending);
		query.setSortField(sortField);
		query.setOfficeNumber(officeNumber);
		query.setSubstitutions(true);
		query.setWithBackup(withBackup);
		query.setLabelsFiltering(labels);
		query.setLabelsAll(labelsAll);
		if (filterBy != null && filterValue != null) {
			query.setFilterBy(filterBy);
			query.setFilterValue(filterValue);
		}
		query.setFilterConditions(filterConditions);

		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		for (TaskSnapshot task : results.results()) {
			res.add(task.toFullTask(flagsOn));
		}
		Map map = new LinkedHashMap<String, Object>();
		map.put("tasks", res);
		map.put("size", results.totalCount());
		return map;
	}

	private Map getOfficeOrderTasks(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending,
			int offset, int LIMIT) throws EdmException {

		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		query = new TaskSnapshot.Query();
		if (LIMIT == 0)
			query = new TaskSnapshot.Query();
		else
			query = new TaskSnapshot.Query(offset, LIMIT);
		query.setDocumentType(OfficeOrder.TYPE);
		query.setUser(user);
		query.setDivision(divisionA);
		query.setAscending(ascending);
		query.setSortField(sortField);
		query.setOfficeNumber(officeNumber);
		query.setSubstitutions(true);
		// query.setLabelsFiltering(labels);

		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		for (TaskSnapshot task : results.results()) {
			res.add(task.toFullTask(flagsOn));
		}
		Map map = new LinkedHashMap<String, Object>();
		map.put("tasks", res);
		map.put("size", results.totalCount());
		return map;
	}

	private Map getOutDocumentTasks(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending,
			int offset, int LIMIT, String filterBy, Object filterValue) throws EdmException {
		return getOutDocumentTasks(divisionA, user, officeNumber, sortField, ascending, offset, LIMIT, filterBy, filterValue, Boolean.TRUE);
	}

	public List<Task> getUserOrderTasks(DSDivision division, DSUser user, Integer officeNumber, String sortField, boolean ascending)
			throws EdmException {
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		query = new TaskSnapshot.Query();
		query.setDocumentType(OfficeOrder.TYPE);
		query.setDocumentAuthor(user.getName());
		query.setAscending(ascending);
		query.setDivision(division);
		query.setSortField(sortField);
		query.setOfficeNumber(officeNumber);
		query.setProcessDefinitionId(WorkflowFactory.wfManualName());
		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		for (TaskSnapshot task : results.results()) {
			res.add(task.toFullTask(flagsOn));
		}
		return res;
	}

	public List<Task> getWatchOrderTasks(DSUser user, Integer officeNumber, String sortField, boolean ascending) throws EdmException {
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		query = new TaskSnapshot.Query();
		query.setDocumentType(OfficeOrder.TYPE);
		query.setExTasks(true);
		query.setExTasksUser(user.getName());
		query.setAscending(ascending);
		query.setSortField(sortField);
		query.setOfficeNumber(officeNumber);
		query.setProcessDefinitionId(WorkflowFactory.wfManualName());
		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		for (TaskSnapshot task : results.results()) {
			res.add(task.toFullTask(flagsOn));
		}
		return res;
	}

	private boolean getBoolean(Object object) {
		return object != null && object instanceof Boolean && ((Boolean) object).booleanValue();
	}

	private Date getDate(Object object) {
		return object != null && object instanceof Date ? (Date) object : null;
	}

	protected void start() throws ServiceException {
	}

	protected void stop() throws ServiceException {
	}

	protected boolean canStop() {
		return false;
	}

	public Map getDocumentTasks(DSUser user, String documentType) throws EdmException {
		return null;
	}

	public Map getSimpleInDocumentTasks(DSUser user, Integer earchOfficeNumber, String sortField, boolean ascending) throws EdmException {
		return null;
	}

	public List<Task> getTasks(DSUser user, Long documentId) throws EdmException {
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		query = new TaskSnapshot.Query();
		query.setUser(user);
		query.setAscending(true);
		query.setDocumentId(documentId);
		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		for (TaskSnapshot task : results.results()) {
			res.add(task.toFullTask(flagsOn));
		}
		return res;
	}

	public Map getWatchesDocumentTasks(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber,
			String sortField, boolean ascending, int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup,
			Collection labels, boolean labelsAll, List<FilterCondition> filterConditions, Long[] documentIds) throws EdmException,
			SQLException {

		log.info("Metoda getWatchesDocumentTasks()");
		log.info("getWatchesDocumentTasks.filterBy: " + filterBy.toString());

		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		query = new TaskSnapshot.Query();
		if (LIMIT == 0)
			query = new TaskSnapshot.Query();
		else
			query = new TaskSnapshot.Query(offset, LIMIT);

		if (documentIds == null || documentIds.length <= 0) {
			documentIds = new Long[1];
			documentIds[0] = 0L;
		}
		query.setDocumentIds(documentIds);
		query.setWatches(true);

		// query.setDocumentType(OutOfficeDocument.INTERNAL_TYPE);
		// query.setUser(user);
		// query.setDivision(divisionA);
		query.setAscending(ascending);
		query.setSortField(sortField);
		// query.setOfficeNumber(officeNumber);
		query.setSubstitutions(true);
		query.setWithBackup(withBackup);
		// query.setLabelsFiltering(labels);
		// query.setLabelsAll(labelsAll);
		if (filterBy != null && filterValue != null) {
			query.setFilterBy(filterBy);
			query.setFilterValue(filterValue);
		}
		query.setFilterConditions(filterConditions);

		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().searchForWatches(query);

		List<Task> res = new ArrayList<Task>();
		for (TaskSnapshot task : results.results()) {
			Integer ko = task.getDocumentOfficeNumber();
			if (ko == null)
				task.setUrn("document:" + task.getDocumentId() + "," + task.getDocumentType());
			else
				task.setUrn("document:" + task.getDocumentId() + "," + task.getDocumentType() + "," + ko);
			res.add(task.toFullTask(flagsOn));
		}
		Map map = new LinkedHashMap<String, Object>();
		map.put("tasks", res);
		map.put("size", results.totalCount());

		return map;
	}
	
	private Map getInDocumentTasksPackage(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending,
			int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup, Collection labels, boolean labelsAll,
			List<FilterCondition> filterConditions) throws EdmException

	{
		String documentKindCn = "";
		DocumentKind kind = null;
		boolean  isDocumentKind = false;
		if (AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow")&& Docusafe.getAdditionProperty("kind-package")!=null &&!Docusafe.getAdditionProperty("kind-package").isEmpty()){
			 documentKindCn = Docusafe.getAdditionProperty("kind-package");
			 kind = DocumentKind.findByCn(documentKindCn);
			isDocumentKind = true;
		}else {
			throw new EdmException("Paczki nie w�aczone lub brakuje okreslenia nazwy cn dokinda dla Paczki Docusafe.getAdditionProperty( kind-package)");
		}
		
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		query = new TaskSnapshot.Query();
		if (LIMIT == 0)
			query = new TaskSnapshot.Query();
		else
			query = new TaskSnapshot.Query(offset, LIMIT);
		query.setDocumentType(InOfficeDocument.TYPE);
		query.setUser(user);
		query.setDivision(divisionA);
		query.setAscending(ascending);
		query.setSortField(sortField);
		query.setOfficeNumber(officeNumber);
		query.setSubstitutions(true);
		query.setWithBackup(withBackup);
		query.setLabelsFiltering(labels);
		query.setLabelsAll(labelsAll);
		query.setInPackageTask(true);
		query.setDockindId(kind.getId());
		
		if (filterBy != null && filterValue != null) {
			query.setFilterBy(filterBy);
			query.setFilterValue(filterValue);
		}
		query.setFilterConditions(filterConditions);

		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		for (TaskSnapshot task : results.results()) {
					res.add(task.toFullTask(flagsOn));
					//if (documentKindCn.contains(Document.find(task.getDocumentId()).getDocumentKind().getCn()))
		}
		Map map = new LinkedHashMap<String, Object>();
		map.put("tasks", res);
		map.put("size", res.size());
		//map.put("taskInPackageCount",results.totalCount()-res.size() );
		return map;
	}
	
	
	private Map getOutgoingDocumentTaskWitoutPackage(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending,
			int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup, Collection labels, boolean labelsAll,
			List<FilterCondition> filterConditions) throws EdmException

	{
		String documentKindCn = "";
		if (AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow")&& Docusafe.getAdditionProperty("kind-package")!=null &&!Docusafe.getAdditionProperty("kind-package").isEmpty()){
			 documentKindCn = Docusafe.getAdditionProperty("kind-package");
		
		}else {
			throw new EdmException("Paczki nie w�aczone lub brakuje okreslenia nazwy cn dokinda dla Paczki Docusafe.getAdditionProperty( kind-package)");
		}
		
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		query = new TaskSnapshot.Query();
		if (LIMIT == 0)
			query = new TaskSnapshot.Query();
		else
			query = new TaskSnapshot.Query(offset, LIMIT);
		query.setDocumentType(OutOfficeDocument.TYPE);
		query.setUser(user);
		query.setDivision(divisionA);
		query.setAscending(ascending);
		query.setSortField(sortField);
		query.setOfficeNumber(officeNumber);
		query.setSubstitutions(true);
		query.setWithBackup(withBackup);
		query.setLabelsFiltering(labels);
		query.setLabelsAll(labelsAll);
		
		
		if (filterBy != null && filterValue != null) {
			query.setFilterBy(filterBy);
			query.setFilterValue(filterValue);
		}
		query.setFilterConditions(filterConditions);

		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		int paczki = 0 ;
		for (TaskSnapshot task : results.results()) {
			
			Document doc = Document.find(task.getDocumentId());
			if(documentKindCn.contains(doc.getDocumentKind().getCn()))
				paczki++;
			if(doc.isDeleted()){}//nie dodawaj
				
			 else if (!documentKindCn.contains(doc.getDocumentKind().getCn()) &&  doc.getInPackage()!=null &&!doc.getInPackage())
					res.add(task.toFullTask(flagsOn));
		}
		Map map = new LinkedHashMap<String, Object>();
		map.put("tasks", res);
		map.put("size", res.size());
		map.put("taskInPackageCount", res.size()-paczki );
		return map;
	}

	private Map getInDocumentTasksWotoutPackage(DSDivision divisionA, DSUser user, Integer officeNumber, String sortField, boolean ascending, int offset,
			int LIMIT, String filterBy, Object filterValue, Boolean withBackup, Collection labels, boolean labelsAll, List<FilterCondition> filterConditions)
			throws EdmException
	{
		DocumentKind kind = null;
		String documentKindCn = "";
	
		if (AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow")&& Docusafe.getAdditionProperty("kind-package")!=null &&!Docusafe.getAdditionProperty("kind-package").isEmpty()){
			 documentKindCn = Docusafe.getAdditionProperty("kind-package");
			 kind =  DocumentKind.findByCn(documentKindCn);
		}else {
			throw new EdmException("Paczki dokument�w nie w�aczone lub brakuje okreslenia nazwy cn dokinda dla Paczki Docusafe.getAdditionProperty( kind-package)");
		}
		boolean flagsOn = "true".equals(Configuration.getProperty("flags"));
		TaskSnapshot.Query query;
		if (LIMIT == 0)
			query = new TaskSnapshot.Query();
		else
		query = new TaskSnapshot.Query(offset, LIMIT);
		query.setDocumentType(InOfficeDocument.TYPE);
		query.setUser(user);
		query.setDivision(divisionA);
		query.setAscending(ascending);
		query.setSortField(sortField);
		query.setOfficeNumber(officeNumber);
		query.setSubstitutions(true);
		query.setWithBackup(withBackup);
		query.setLabelsFiltering(labels);
		query.setLabelsAll(labelsAll);
		query.setInPackageTask(Boolean.FALSE);
		query.setDockindId(kind.getId());
		if (filterBy != null && filterValue != null)
		{
			query.setFilterBy(filterBy);
			query.setFilterValue(filterValue);
		}
		query.setFilterConditions(filterConditions);

		setQueryFilterByPermission(user, query);
		SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
		List<Task> res = new ArrayList<Task>();
		/*for (TaskSnapshot task : results.results())
		{
			Document doc = Document.find(task.getDocumentId(), false);
			
			if(doc.isDeleted())
			{}
			else	if (doc.getInPackage()!=null && !doc.getInPackage() )
				res.add(task.toFullTask(flagsOn));
		}*/
		int paczki = 0;
		for (TaskSnapshot task : results.results()) {
				res.add(task.toFullTask(flagsOn));
		/*	Document doc = Document.find(task.getDocumentId());
			if(documentKindCn.contains(doc.getDocumentKind().getCn()))
				paczki++;
			if(doc.isDeleted()){}//nie dodawaj
				
			 else if (!documentKindCn.contains(doc.getDocumentKind().getCn()) &&  doc.getInPackage()!=null &&!doc.getInPackage())
					res.add(task.toFullTask(flagsOn));*/
		}
		Map map = new LinkedHashMap<String, Object>();
		map.put("tasks", res);
		map.put("size", results.totalCount());
		//map.put("taskInPackageCount",results.totalCount()-paczki );
		//map.put("taskInPackageCount",results.totalCount()-res.size() );

		return map;

	}

	
	
}
	
