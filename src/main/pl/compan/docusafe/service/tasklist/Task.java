package pl.compan.docusafe.service.tasklist;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gson.JsonElement;
import org.jfree.util.Log;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.workflow.ActivityId;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.regtype.EnumItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Task.java,v 1.50 2010/08/13 09:12:33 mariuszk Exp $
 */
public class Task implements Cloneable
{
    private final static Logger LOG = LoggerFactory.getLogger(Task.class);

    private final static StringManager sm = GlobalPreferences.loadPropertiesFile("",null);

    protected boolean documentTask;
    protected boolean messageTask;
    protected ActivityId activityId;
    /** Nie zapisywane! */
    protected transient String workflowName;
    protected String activityKey;//
    protected boolean accepted;//
    /**
     * Data otrzymania, lastStateTime.
     */
    protected Date receiveDate;//
    protected Date shortReceiveDate;
    /**
     * Opis zadania, activity_name.
     */
    protected String description;//
    /**
     * Dekretuj�cy, activity_lastActivityUser.
     */
    protected String sender;
    protected String objective;//
    protected String process;//w kolumnie process_name

    /* W�asno�ci dla documentTask = true */

    protected Long documentId;//
    protected Integer documentOfficeNumber;//
    protected Integer documentOfficeNumberYear;//
    protected String documentSummary;//
    protected String documentReferenceId;//numer w sprawie
    protected String documentSender;
    protected String officeCase;//numer sprawy
    protected Long caseId;//
    protected Date caseDeadlineDate;
    /**
     * G��wny odbiorca dokumentu.
     */
    protected String documentRecipient;
    protected String documentType;//
    protected boolean documentBok;
    protected boolean documentFax;
    protected boolean documentCrm;

    protected String packageId;//
    protected String processDefinitionId;//
    protected boolean manual;
    protected List<FlagBean> flags;
    protected List<FlagBean> userFlags;

    protected String urn;
    protected String link;

    protected Date deadlineTime;//
    protected String deadlineRemark;//
    protected String deadlineAuthor;//

    protected boolean pastDeadline;

    protected String documentDelivery;   // sposob odbioru (narazie tylko dla pism wych.)
    protected String documentPrepState;  // stan pisma wych. (Brudnopis/Czystopis)

    protected String dockindName;//nazwa dockindu
    private boolean replyUnnecessary; //pismo nie wymaga odpowiedzi
    
    //atrybuty z dockindow ustawiane w logic w metodzie getTaskListParams()
    private String dockindStatus;
    private String dockindNumerDokumentu;
    private BigDecimal dockindKwota;
    private String dockindKategoria;
    private String dockindBusinessAtr1;
    private String dockindBusinessAtr2;
    private String dockindBusinessAtr3;
    private String dockindBusinessAtr4;
    private String dockindBusinessAtr5;
    private String dockindBusinessAtr6;
    private Date documentDate;
    private Boolean labelhidden;
    private Boolean isAnyImageInAttachments;
    private Boolean isAnyPdfInAttachments;
    private String markerClass;

    public Task clone(){
        try {
            Task ret = (Task) super.clone();
            ret.documentTask = this.documentTask;
            ret.messageTask = this.messageTask;
            ret.activityId = this.activityId;
            ret.workflowName = this.workflowName;
            ret.activityKey = this.activityKey;
            ret.accepted = this.accepted;
            ret.receiveDate = this.receiveDate;
            ret.shortReceiveDate = this.shortReceiveDate;
            ret.description = this.description;
            ret.sender = this.sender;
            ret.objective = this.objective;
            ret.process = this.process;

            ret.documentId = this.documentId;
            ret.documentOfficeNumber = this.documentOfficeNumber;
            ret.documentOfficeNumberYear = this.documentOfficeNumberYear;
            ret.documentSummary = this.documentSummary;
            ret.documentReferenceId = this.documentReferenceId;
            ret.documentSender = this.documentSender;
            ret.officeCase = this.officeCase;
            ret.caseId = this.caseId;
            ret.caseDeadlineDate = this.caseDeadlineDate;
            ret.documentRecipient = this.documentRecipient;
            ret.documentType = documentType;
            ret.documentBok = this.documentBok;
            ret.documentFax = this.documentFax;
            ret.documentCrm = this.documentCrm;

            ret.packageId = this.packageId;
            ret.processDefinitionId = this.processDefinitionId;
            ret.manual = this.manual;
            ret.flags = this.flags;
            ret.userFlags = this.userFlags;

            ret.urn = this.urn;
            ret.link = this.link;

            ret.deadlineTime = this.deadlineTime;
            ret.deadlineRemark = this.deadlineRemark;
            ret.deadlineAuthor = this.deadlineAuthor;

            ret.pastDeadline = this.pastDeadline;

            ret.documentDelivery = this.documentDelivery;
            ret.documentPrepState = this.documentPrepState;

            ret.dockindName = this.dockindName;
            ret.replyUnnecessary = this.replyUnnecessary;

            ret.dockindStatus = this.dockindStatus;
            ret.dockindNumerDokumentu = this.dockindNumerDokumentu;
            ret.dockindKwota = this.dockindKwota;
            ret.dockindKategoria = this.dockindKategoria;
            ret.dockindBusinessAtr1 = this.dockindBusinessAtr1;
            ret.dockindBusinessAtr2 = this.dockindBusinessAtr2;
            ret.dockindBusinessAtr3 = this.dockindBusinessAtr3;
            ret.dockindBusinessAtr4 = this.dockindBusinessAtr4;
            ret.dockindBusinessAtr5 = this.dockindBusinessAtr5;
            ret.dockindBusinessAtr6 = this.dockindBusinessAtr6;
            ret.documentDate = this.documentDate;
            ret.labelhidden = this.labelhidden;
            ret.isAnyImageInAttachments = this.isAnyImageInAttachments;
            ret.isAnyPdfInAttachments = this.isAnyPdfInAttachments;
            ret.markerClass = this.markerClass;
            return ret;
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static class TaskComparator implements Comparator<Task>
    {
        public int compare(Task e1, Task e2)
        {
        	if(e1 != null && e1.getDocumentOfficeNumber() != null && e2 != null)
        		return e1.getDocumentOfficeNumber().compareTo(e1.getDocumentOfficeNumber());
        	else
        		return 1;
        }
    }
    public String getMarkerClass() {
		return markerClass;
	}

	public void setMarkerClass(String markerClass) {
		this.markerClass = markerClass;
	}

	public Boolean isAnyImage()
    {
    	return isAnyImageInAttachments != null && isAnyImageInAttachments;
    }
	
	public Boolean isAnyPdf()
    {
    	return isAnyPdfInAttachments != null && isAnyPdfInAttachments;
    }

	public Date getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}

	public boolean isReplyUnnecessary()
    {
        return replyUnnecessary;
    }

    public void setReplyUnnecessary(boolean replyUnnecessary)
    {
        this.replyUnnecessary = replyUnnecessary;
    }

    public String getUrn()
    {
        return urn;
    }

    public void setUrn(String urn)
    {
        this.urn = urn;
    }

    public String getLink()
    {
        return link;
    }

    public void setLink(String link)
    {
        this.link = link;
    }

    public boolean isDocumentTask()
    {
        return documentTask;
    }

    public void setDocumentTask(boolean documentTask)
    {
        this.documentTask = documentTask;
    }

    public boolean isMessageTask()
    {
        return messageTask;
    }

    public void setMessageTask(boolean messageTask)
    {
        this.messageTask = messageTask;
    }

    public String getWorkflowName()
    {
        return workflowName;
    }

    public void setWorkflowName(String workflowName)
    {
        this.workflowName = workflowName;
    }

    /**
     * Dla jbpm4 == taskId
     * @return
     */
    //TODO zmieni� nazw� na zgodn� z jbpm4 po tym jak jbpm3 zostanie wyeliminowany
    public String getActivityKey()
    {
        return activityKey;
    }

    public void setActivityKey(String activityKey)
    {
        this.activityKey = activityKey;
    }

    public boolean isAccepted()
    {
        return accepted;
    }
    
    public boolean isAcceptedIlpolHack()
    {
        if (AvailabilityManager.isAvailable("disabledIlpolHack"))
            return accepted;
    	Log.error("ILPOL HACK" + caseId + " "+accepted);
    	if(caseId != null && caseId > 0)
    		return true;
        return accepted;
    }

    public void setAccepted(boolean accepted)
    {
        this.accepted = accepted;
    }

    public Date getReceiveDate()
    {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate)
    {
        this.receiveDate = receiveDate;
    }

    public Date getShortReceiveDate()
    {
        return shortReceiveDate;
    }

    public void setShortReceiveDate(Date shortReceiveDate)
    {
        this.shortReceiveDate = shortReceiveDate;
    }
    
    public String getDescription()
    {
        if(description!=null && (description.equals("Zad. w dekr. recznej") || description.equals("Zad. w dekr. r�cznej")))
            return sm.getString("task.list.Zad.wDekr.recznej");
        else if (description==null)
        return "";
        else
        return sm.getString(description);
    }
    public String getRealDescription() {
    	return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getSender()
    {
        return sender;
    }

    public void setSender(String sender)
    {
        this.sender = sender;
    }

    public String getObjective()
    {
        if(objective!=null &&( objective.equals("Pismo przyjete") || objective.equals("Pismo przyj�te") || objective.equals("Document accepted")))
            return sm.getString("task.list.PismoPrzyjete");
        return sm.getString(objective);
    }

    public void setObjective(String objective)
    {
        this.objective = objective;
    }

    public String getProcess()
    {
        if(process.equals("Obieg reczny"))
            return sm.getString("task.list.ObiegReczny");
        else if(process.equals(sm.getString("doWiadomosci")))
        	return sm.getString("task.list.DoWiadomosci");
        return sm.getString(process);
    }

    public void setProcess(String process)
    {
        this.process = process;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Integer getDocumentOfficeNumber()
    {
        return documentOfficeNumber;
    }

    public void setDocumentOfficeNumber(Integer documentOfficeNumber)
    {
        this.documentOfficeNumber = documentOfficeNumber;
    }

    public Integer getDocumentOfficeNumberYear()
    {
        return documentOfficeNumberYear;
    }

    public void setDocumentOfficeNumberYear(Integer documentOfficeNumberYear)
    {
        this.documentOfficeNumberYear = documentOfficeNumberYear;
    }

    public String getDocumentSummary()
    {
        return documentSummary;
    }

    public void setDocumentSummary(String documentSummary)
    {
        this.documentSummary = documentSummary;
    }

    public String getOfficeCase()
    {
        return officeCase;
    }

    public void setCaseId(Long caseId)
    {
        this.caseId = caseId;
    }
    
    public Long getCaseId()
    {
        return caseId;
    }

    public void setOfficeCase(String officeCase)
    {
        this.officeCase = officeCase;
    }
    
    public String getDocumentReferenceId()
    {
        return documentReferenceId;
    }

    public void setDocumentReferenceId(String documentReferenceId)
    {
        this.documentReferenceId = documentReferenceId;
    }

    public String getDocumentSender()
    {
        return documentSender;
    }

    public void setDocumentSender(String documentSender)
    {
        this.documentSender = documentSender;
    }

    public String getDocumentRecipient()
    {
        return documentRecipient;
    }

    public void setDocumentRecipient(String documentRecipient)
    {
        this.documentRecipient = documentRecipient;
    }

    public String getDocumentType()
    {
        return documentType;
    }

    public void setDocumentType(String documentType)
    {
        this.documentType = documentType;
    }

    public boolean isDocumentBok()
    {
        return documentBok;
    }

    public void setDocumentBok(boolean documentBok)
    {
        this.documentBok = documentBok;
    }
    
    public boolean isDocumentCrm()
    {
        return documentCrm;
    }

    public void setDocumentCrm(boolean documentCrm)
    {
        this.documentCrm = documentCrm;
    }

    public ActivityId getActivityId()
    {
        return activityId;
    }

    public void setActivityId(ActivityId activityId)
    {
        this.activityId = activityId;
    }

    public boolean isDocumentFax()
    {
        return documentFax;
    }

    public void setDocumentFax(boolean documentFax)
    {
        this.documentFax = documentFax;
    }

    public String getPackageId()
    {
        return packageId;
    }

    public void setPackageId(String packageId)
    {
        this.packageId = packageId;
    }

    public String getProcessDefinitionId()
    {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId)
    {
        this.processDefinitionId = processDefinitionId;
    }

    public boolean isManual()
    {
        return manual;
    }

    public void setManual(boolean manual)
    {
        this.manual = manual;
    }

    public Date getDeadlineTime()
    {
        return deadlineTime;
    }

    public void setDeadlineTime(Date deadlineTime)
    {
        this.deadlineTime = deadlineTime;
    }

    public String getDeadlineRemark()
    {
        return deadlineRemark;
    }

    public void setDeadlineRemark(String deadlineRemark)
    {
        this.deadlineRemark = deadlineRemark;
    }

    public String getDeadlineAuthor()
    {
        return deadlineAuthor;
    }

    public void setDeadlineAuthor(String deadlineAuthor)
    {
        this.deadlineAuthor = deadlineAuthor;
    }

    public boolean isPastDeadline()
    {
        return pastDeadline;
    }

    public void setPastDeadline(boolean pastDeadline)
    {
        this.pastDeadline = pastDeadline;
    }

    public String getDocumentDelivery()
    {
        return documentDelivery;
    }

    public void setDocumentDelivery(String documentDelivery)
    {
        this.documentDelivery = documentDelivery;
    }

    public String getDocumentPrepState()
    {
        return documentPrepState;
    }

    public void setDocumentPrepState(String documentPrepState)
    {
        this.documentPrepState = documentPrepState;
    }

    public String getDockindName()
    {
        return dockindName;
    }
    
    public void setDockindName(String dockindName)
    {
        this.dockindName = dockindName;
    }
    
    public void addFlag(String c, String description)
    {
        if (flags == null)
            flags = new ArrayList<FlagBean>();
        flags.add(new FlagBean(c, description));
    }

    public List getFlags()
    {
        return flags;
    }

    public void addUserFlag(String c, String description)
    {
        if (userFlags == null)
            userFlags = new ArrayList<FlagBean>();
        userFlags.add(new FlagBean(c, description));
    }

    public List getUserFlags()
    {
        return userFlags;
    }


    
    public static class FlagBean
    {
        private String c;
        private String description;

        public FlagBean(String c, String description)
        {
            this.c = c;
            this.description = description;
        }

        public String getC()
        {
            return c;
        }

        public String getDescription()
        {
            return description;
        }
    }

    public String toString()
    {
        return getClass().getName()+"[documentTask="+documentTask+" messageTask="+messageTask+
            " accepted="+accepted+" documentId="+documentId+
            "]";
    }

	public String getDockindStatus() {
		return dockindStatus;
	}

	public void setDockindStatus(String dockindStatus) {
		this.dockindStatus = dockindStatus;
	}

	public String getDockindNumerDokumentu() {
		return dockindNumerDokumentu;
	}

	public void setDockindNumerDokumentu(String dockindNumerDokumentu) {
		this.dockindNumerDokumentu = dockindNumerDokumentu;
	}

	public BigDecimal getDockindKwota() {
		return dockindKwota;
	}

	public void setDockindKwota(Float dockindKwota) {
		this.dockindKwota = new BigDecimal(dockindKwota).setScale(2);
	}

	public void setDockindKwota(BigDecimal dockindKwota) {
		this.dockindKwota = dockindKwota;
	}

	public String getDockindKategoria() {
		return dockindKategoria;
	}

	public void setDockindKategoria(String dockindKategoria) {
		this.dockindKategoria = dockindKategoria;
	}

	public String getDockindBusinessAtr1() {
		return dockindBusinessAtr1;
	}

	public void setDockindBusinessAtr1(String dockindBusinessUnit) {
		this.dockindBusinessAtr1 = dockindBusinessUnit;
	}

	public String getDockindBusinessAtr2() {
		return dockindBusinessAtr2;
	}

	public void setDockindBusinessAtr2(String dockindBusinessAtr2) {
		this.dockindBusinessAtr2 = dockindBusinessAtr2;
	}

	public String getDockindBusinessAtr3() {
		return dockindBusinessAtr3;
	}

	public void setDockindBusinessAtr3(String dockindBusinessAtr3) {
		this.dockindBusinessAtr3 = dockindBusinessAtr3;
	}

	public String getDockindBusinessAtr4() {
		return dockindBusinessAtr4;
	}

	public void setDockindBusinessAtr4(String dockindBusinessAtr4) {
		this.dockindBusinessAtr4 = dockindBusinessAtr4;
	}

	public void setLabelhidden(Boolean labelhidden) {
		this.labelhidden = labelhidden;
	}

	public Boolean isLabelhidden() {
		return labelhidden;
	}

    public Date getCaseDeadlineDate()
    {
        return caseDeadlineDate;
    }

    public void setCaseDeadlineDate(Date caseDeadlineDate)
    {
        this.caseDeadlineDate = caseDeadlineDate;
    }

	public void setIsAnyImageInAttachments(Boolean isAnyImageInAttachments) {
		this.isAnyImageInAttachments = isAnyImageInAttachments;
	}

	public Boolean getIsAnyImageInAttachments() {
		return isAnyImageInAttachments;
	}
	
	public void setIsAnyPdfInAttachments(Boolean isAnyPdfInAttachments) {
		this.isAnyPdfInAttachments = isAnyPdfInAttachments;
	}

	public Boolean getIsAnyPdfInAttachments() {
		return isAnyPdfInAttachments;
	}

	public String getDockindBusinessAtr5() {
		return dockindBusinessAtr5;
	}

	public void setDockindBusinessAtr5(String dockindBusinessAtr5) {
		this.dockindBusinessAtr5 = dockindBusinessAtr5;
	}

	public String getDockindBusinessAtr6() {
		return dockindBusinessAtr6;
	}

	public void setDockindBusinessAtr6(String dockindBusinessAtr6) {
		this.dockindBusinessAtr6 = dockindBusinessAtr6;
	}
	
	public void setDockindBusinessAtrs(String[] taskListParams) {
		if (taskListParams != null && taskListParams.length == 6) {
			if (taskListParams[0] != null)
				this.setDockindBusinessAtr1(taskListParams[0]);
			if (taskListParams[1] != null)
				this.setDockindBusinessAtr2(taskListParams[1]);
			if (taskListParams[2] != null)
				this.setDockindBusinessAtr3(taskListParams[2]);
			if (taskListParams[3] != null)
				this.setDockindBusinessAtr4(taskListParams[3]);
			if (taskListParams[4] != null)
				this.setDockindBusinessAtr5(taskListParams[4]);
			if (taskListParams[5] != null)
				this.setDockindBusinessAtr6(taskListParams[5]);
		}
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activityKey == null) ? 0 : activityKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (activityKey == null)
		{
			if (other.activityKey != null)
				return false;
		}
		else if (!activityKey.equals(other.activityKey))
			return false;
		return true;
	}

}
