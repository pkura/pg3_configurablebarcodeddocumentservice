package pl.compan.docusafe.service.tasklist;

import java.util.Collection;
import java.util.List;

import pl.compan.docusafe.core.bookmarks.FilterCondition;
import pl.compan.docusafe.core.users.DSUser;

/**
 * Klasa pomocnicza s�u��ca do przekazywania parametr�w podczas wyci�gania zada� z TaskListy.
 * Docelowo ma zast�pi� wieloargumentowe wywo�ania metod API, kt�re s�u�� do pobierania zada� 
 * i przez kt�re zmiany w dzia�aniu TaskListy wymagaj� przedudowania ca�ego stosu API.
 * 
 * @author PN
 */
public class TaskListParameters {
	private DSUser user;
	private String documentType;
	private Integer searchOfficeNumber; 
	private String sortField; 
	private boolean ascending;
	private int offset;
	private int LIMIT;
	private String filterBy;
	private Object filterValue;
	private Boolean withBackup;
	private Collection labelsFiltering;
	private boolean labelsAll;
	private List<FilterCondition> filterConditions;
	public DSUser getUser() {
		return user;
	}
	public void setUser(DSUser user) {
		this.user = user;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public Integer getSearchOfficeNumber() {
		return searchOfficeNumber;
	}
	public void setSearchOfficeNumber(Integer searchOfficeNumber) {
		this.searchOfficeNumber = searchOfficeNumber;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public boolean isAscending() {
		return ascending;
	}
	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLIMIT() {
		return LIMIT;
	}
	public void setLIMIT(int lIMIT) {
		LIMIT = lIMIT;
	}
	public String getFilterBy() {
		return filterBy;
	}
	public void setFilterBy(String filterBy) {
		this.filterBy = filterBy;
	}
	public Object getFilterValue() {
		return filterValue;
	}
	public void setFilterValue(Object filterValue) {
		this.filterValue = filterValue;
	}
	public Boolean getWithBackup() {
		return withBackup;
	}
	public void setWithBackup(Boolean withBackup) {
		this.withBackup = withBackup;
	}
	public Collection getLabelsFiltering() {
		return labelsFiltering;
	}
	public void setLabelsFiltering(Collection labelsFiltering) {
		this.labelsFiltering = labelsFiltering;
	}
	public boolean isLabelsAll() {
		return labelsAll;
	}
	public void setLabelsAll(boolean labelsAll) {
		this.labelsAll = labelsAll;
	}
	public List<FilterCondition> getFilterConditions() {
		return filterConditions;
	}
	public void setFilterConditions(List<FilterCondition> filterConditions) {
		this.filterConditions = filterConditions;
	}
	
}