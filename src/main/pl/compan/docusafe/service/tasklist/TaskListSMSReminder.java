package pl.compan.docusafe.service.tasklist;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListCounterManager;
import pl.compan.docusafe.core.office.workflow.snapshot.UserCounter;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.sms.SMSIntelisoDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:tomasz.lipka@com-pan.pl">Tomasz Lipka</a>
 *
 */
public class TaskListSMSReminder extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(TaskListSMSReminder.class);

	public static Integer TERM_KIND_CO_ILE_MINUT = 1;
	public static Integer TERM_KIND_CO_ILE_GODZIN = 2;
	public static Integer TERM_KIND_RAZ_DZIENNIE = 3;
	public static Integer TERM_KIND_RAZ_W_TYGODNIU = 4;
	public static Integer TERM_KIND_RAZ_W_MIESIACU = 5;


	private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(TaskListSMSReminder.class.getPackage().getName(),null);

	public TaskListSMSReminder()
	{
	}

	protected boolean canStop()
	{
		return true;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi SMS");
		String run = Docusafe.getAdditionProperty("taskListSMS");
		if("false".equalsIgnoreCase(run))
			return;


		timer = new Timer(true);
		timer.schedule(new SMSSender(), (long) 2*1000 ,(long) 1*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("SystemPowiadamianiaWystartowal"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi");
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}

	class SMSSender extends TimerTask
	{

		/**
		 * Lista uzytkownikow do wysylki
		 * @return
		 */
		private List<DSUser> prepUsers()
		{
			log.trace("propUsers START");
			List<DSUser> users = new ArrayList<DSUser>();
			String[] usersFromProperties;

			try
			{
				usersFromProperties = DSApi.context().systemPreferences().node("taskListSMS").keys();
				for(int i=0;i<usersFromProperties.length;i++)
				{
					log.trace("ADD user "+ usersFromProperties[i]);
					if(usersFromProperties[i] != null && usersFromProperties[i].length() > 0)
					{
						DSUser user = null;
						try
						{
							user = DSUser.findByUsername(usersFromProperties[i]);
						}catch(UserNotFoundException e){
							user = DSUser.findByExternalName(usersFromProperties[i]);
							log.trace("wyszukuje po external user name" + usersFromProperties[i]);
						}
						
						if(user.getMobileNum() != null)
							users.add(user);
						else
							log.trace("User getMobileNum = NULL");
					}
				}
			}
			catch(UserNotFoundException e)
			{
				TaskListSMSReminder.log.error("",e.getMessage());
				console(Console.ERROR, "Nie znaleziono u�ytkownika [ "+e+" ]");
			}
			catch(EdmException e)
			{
				TaskListSMSReminder.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			catch(Throwable e)
			{
				TaskListSMSReminder.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}

			log.trace("propUsers STOP");
			return users;
		}

		private void sendSMS(DSUser user)
		{
				log.trace("wysylka SMS do uzytkownika {}",user.getName());
				StringBuilder msg = new StringBuilder();
				UserCounter uc = null;
				try
				{
					TaskListCounterManager mgr = new TaskListCounterManager(false);
					uc = mgr.getUserCounter(user.getName(),DSApi.context().systemPreferences().node("taskListSMS").getLong(user.getName(), 0));
				}
				catch(EdmException e)
				{
					TaskListSMSReminder.log.error("B��d",e);
					console(Console.ERROR, "B��d [ "+e+" ]");
				}
	
				msg.append("O godzinie "+DateUtils.formatCommonTime(new Date())+" na twojej liscie zadan znajdowalo sie "+uc.getAllTask()+" zadan.\n");
				msg.append("Nowe zadania z ktorymi si� jeszcze nie zapoznales - "+uc.getNewNotAcceptedTask()+". \n");
				msg.append("Zadania z ktorymi nie zakonczyles jeszcze pracy - "+(int)(uc.getNewAcceptedTask())+".\n");

				Map<String, Object> ctext = new HashMap<String, Object>();
				ctext.put("imie", user.getFirstname());
				ctext.put("nazwisko", user.getLastname());
				ctext.put("czas", DateUtils.formatCommonTime(new Date()));
				ctext.put("zadaniaAll", uc.getAllTask());
				ctext.put("zadaniaNewNotAccept", uc.getNewNotAcceptedTask());
				ctext.put("zadaniaNew", uc.getNewTask());
				ctext.put("zadaniaOld", uc.getNewAcceptedTask());
				ctext.put("link", Docusafe.getBaseUrl());
				ctext.put("msg",msg.toString());
	
				if(uc.getNewTask() > 0)
				{
					try
					{
					((SMSIntelisoDriver) ServiceManager.getService(SMSIntelisoDriver.NAME)).send(user.getMobileNum(), Configuration.getTemplate("sms.txt"), ctext);
						console(Console.INFO, "Wysylam SMS do "+user.getName()+" na numer "+user.getMobileNum()+" czas "+new Date());
						TaskListSMSReminder.log.info("Wysylam SMS do "+user.getName()+" na numer "+user.getMobileNum()+" czas "+new Date());
					}
					catch(Exception e)
					{
						TaskListSMSReminder.log.error("B��d wys�ania SMS o onowym zadaniu do " + user.getName(),e);
						console(Console.ERROR, "B��d [ "+e+" ]");
					}
				}
				else
				{
					String kom = "Nie wysylam SMS do "+user.getName()+" na numer "+user.getMobileNum()+" czas "+new Date()+" - brak nowych zadan";
					console(Console.INFO, kom);
					log.trace(kom);
				}
				uc.setLastSent(new Date());
				DSApi.context().systemPreferences().node("taskListSMS").putLong(user.getName(), new Date().getTime());
		}

		/**
		 * Czy nalezy wyslac SMS-a
		 * @param user
		 * @return
		 */
		private boolean shouldSend(DSUser user)
		{
			if("admin".equals(user.getName()))
				log.debug("");
			log.trace("shouldSend START");
			/**Ostatnia akywnosc*/
			Long lastActivityTime = DSApi.context().systemPreferences().node("taskListSMS").getLong(user.getName(), 0);
			/**Rodzaj okresu*/
			Integer termKind = DSApi.context().userPreferences(user.getName()).node("taskListSMS").getInt("termKindSMS", TERM_KIND_CO_ILE_MINUT);
			Integer coIle = DSApi.context().userPreferences(user.getName()).node("taskListSMS").getInt("timeSMS", 10);
			/**Domyslnie poniedzialek*/
			Integer dayOfWeek =  DSApi.context().userPreferences(user.getName()).node("taskListSMS").getInt("dayOfWeekSMS", 2);
			/**O ktorej godzinie*/
			String time =  DSApi.context().userPreferences(user.getName()).node("taskListSMS").get("timeSMSg", "9:00");
			if(log.isTraceEnabled())
				log.trace("shouldSend lastActivityTime = {}, termKind = {}, coIle = {}, dayOfWeek = {} ,time = {}",
						lastActivityTime,termKind,coIle,dayOfWeek,time);
			String[] timeTab = time.split(":");
			Integer hour = Integer.parseInt(timeTab[0]);
			Integer minut = null;
			if (timeTab.length>1)
				minut = Integer.parseInt("00");
			Date lastActivityDate = null;
			if(!lastActivityTime.equals(0L))
			{
				lastActivityDate = new Date(lastActivityTime);
			}
			else
				return true;
			
			log.trace("shouldSend lastActivity = {}",lastActivityDate);

			Calendar newActivity = Calendar.getInstance();
			newActivity.setTime(lastActivityDate);
			if(TERM_KIND_CO_ILE_MINUT.equals(termKind))
			{
				newActivity.add(Calendar.MINUTE,coIle);
			}
			else if(TERM_KIND_CO_ILE_GODZIN.equals(termKind))
			{
				newActivity.add(Calendar.HOUR_OF_DAY,coIle);
			}
			else if(TERM_KIND_RAZ_DZIENNIE.equals(termKind))
			{
				newActivity.add(Calendar.DAY_OF_YEAR,1);
				newActivity.set(Calendar.HOUR_OF_DAY, hour);
				newActivity.set(Calendar.MINUTE, minut);
			}
			else if(TERM_KIND_RAZ_W_TYGODNIU.equals(termKind))
			{
				newActivity.add(Calendar.WEEK_OF_YEAR,1);
				newActivity.set(Calendar.HOUR_OF_DAY, hour);
				newActivity.set(Calendar.MINUTE, minut);
			}
			else if(TERM_KIND_RAZ_W_MIESIACU.equals(termKind))
			{
				newActivity.add(Calendar.MONTH,1);
				newActivity.set(Calendar.DAY_OF_WEEK, dayOfWeek);
				newActivity.set(Calendar.HOUR_OF_DAY, hour);
				newActivity.set(Calendar.MINUTE, minut);
			}
			log.trace("shouldSend newActivity = {}",DateUtils.formatJsDateTime(newActivity.getTime()));
			if(newActivity.getTime().before(new Date()))
			{
				log.trace("shouldSend STOP shouldSend = true");
				return true;
			}
			else
			{
				log.trace("shouldSend STOP shouldSend = false");
				return false;
			}
		}

		public void run()
		{
			try
			{
				log.trace("Petla powiadomienia START");
				DSApi.openAdmin();
				for(DSUser user : prepUsers())
				{
					try
					{
						DSApi.context().begin();
						if(shouldSend(user))
							sendSMS(user);
						DSApi.context().commit();
					}
					catch (Throwable e) 
					{
						TaskListSMSReminder.log.error("",e);
						console(Console.ERROR, "B��d [ "+e+" ]");
						DSApi.context()._rollback();
					}
				}
			}
			catch(EdmException e)
			{
				TaskListSMSReminder.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			catch(Throwable e)
			{
				TaskListSMSReminder.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			finally
			{
				log.trace("Petla powiadomienia STOP");
				DSApi._close();
			}
		}
	}
	
	
}
