package pl.compan.docusafe.service.tasklist;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.bookmarks.FilterCondition;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.Service;
/* User: Administrator, Date: 2005-08-22 13:07:11 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: TaskList.java,v 1.19 2010/08/03 16:59:56 pecet5 Exp $
 */
public interface TaskList extends Service
{
    public static final String NAME = "tasklist";

    /**
     * Zwraca dwuelementow� tablic� zawieraj�c� liczby zada� przyj�tych
     * i oczekuj�cych na u�ytkownika.
     */
    int[] getTaskCount(DSUser user) throws EdmException;

    /**
     * Zwraca list� obiekt�w {@link Task} odpowiadaj�c� zadaniom zwi�zanym
     * z danym typem dokumentu.
     * @return Lista obiekt�w {@link Task}, nigdy null.
     * @throws EdmException Je�eli typ dokumentu jest nieznany lub
     * wyst�pi inny b��d.
     */
    
    Map getDocumentTasks(DSUser user, String documentType) throws EdmException;

    Map getDocumentTasks(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField, boolean ascending, int offset, int LIMIT) throws EdmException;
   
    Map getDocumentTasks(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField, boolean ascending, int offset, int LIMIT, String filterBy,Object filterValue) throws EdmException;

    
    Map getSimpleInDocumentTasks(DSUser user, Integer earchOfficeNumber, String sortField, boolean ascending) throws EdmException;
    
    Map getInDocumentTasks(DSDivision divisionA, DSUser user, int offset, int LIMIT, Integer officeNumber, String sortField, boolean ascending) throws EdmException;
    
    Map getInDocumentTasks(DSDivision divisionA, DSUser user, int offset, int LIMIT, Integer officeNumber, String sortField, boolean ascending, String filterBy,Object filterValue) throws EdmException;
//    Map getInDocumentTasks(DSUser user, int offset, int LIMIT, Integer officeNumber, String sortField, boolean ascending, String filterBy,Object filterValue,Boolean withBackup) throws EdmException;
    /**
     * 
     * @param user
     * @param offset
     * @param LIMIT
     * @param officeNumber
     * @param sortField
     * @param ascending
     * @param filterBy 
     * @param filterValue
     * @param withBackup
     * @param labelsFiltering - Jesli kolekcja jest niepusta to zwrocona zostanie lista dokumentow zawierajacych dana etykietke
     * @param labelsAll - czy wszystkie zadania czy domyslne
     * @return
     * @throws EdmException
     */
    Map getInDocumentTasks(DSDivision divisionA, DSUser user, int offset, int LIMIT, Integer officeNumber, String sortField, boolean ascending, String filterBy,Object filterValue,Boolean withBackup,Collection labelsFiltering,boolean labelsAll,List<FilterCondition> filterConditions) throws EdmException;
    Map getDocumentTasks(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField, boolean ascending, int offset, int LIMIT, String filterBy,Object filterValue,Boolean withBackup) throws EdmException;
    /**
     * Pobranie listy zadan
     * @param user
     * @param documentType
     * @param searchOfficeNumber
     * @param sortField
     * @param ascending
     * @param offset
     * @param LIMIT
     * @param filterBy
     * @param filterValue
     * @param withBackup
     * @param labelsFiltering
     * @param labelsAll - czy wszystkie zadania czy domyslne
     * @return
     * @throws EdmException
     */
    Map getDocumentTasks(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField, boolean ascending, int offset, int LIMIT, String filterBy,Object filterValue,Boolean withBackup,Collection labelsFiltering,boolean labelsAll,List<FilterCondition> filterConditions) throws EdmException;
    
    List<Task> getUserOrderTasks(DSDivision divisionA, DSUser user, Integer searchOfficeNumber, String sortField, boolean ascending) throws EdmException;

    List<Task> getWatchOrderTasks(DSUser user, Integer searchOfficeNumber, String sortField, boolean ascending) throws EdmException;
    
    List<Task> getTasks(DSUser user) throws EdmException;
    /**
     * Pobieranie listy zada�
     * @param user
     * @param offset
     * @param LIMIT
     * @return
     * @throws EdmException
     */
    List<Task> getTasks(DSUser user,int offset,int LIMIT) throws EdmException;
    List<Task> getTasks(DSUser user,Long documentId) throws EdmException;
    
    List<Task> getTasks(TaskListParameters parameters) throws EdmException;
    
    Map getWatchesDocumentTasks(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField, boolean ascending, int offset, int LIMIT, String filterBy, Object filterValue, Boolean withBackup, Collection labelsFiltering, boolean labelsAll, List<FilterCondition> filterConditions, Long[] documentIds) throws EdmException, SQLException;
/**
 * 
 * @param divisionA
 * @param user
 * @param documentType
 * @param searchOfficeNumber
 * @param sortField
 * @param ascending
 * @param offset
 * @param LIMIT
 * @param filterBy
 * @param filterValue
 * @param withBackup
 * @param labelsFiltering
 * @param labelsAll
 * @param filterConditions
 * @return
 * @throws EdmException
 */

    Map getInDocumentTasksPackage(DSDivision divisionA, DSUser user, String documentType, Integer searchOfficeNumber, String sortField, boolean ascending, int offset, int LIMIT, String filterBy,Object filterValue,Boolean withBackup,Collection labelsFiltering,boolean labelsAll,List<FilterCondition> filterConditions) throws EdmException;
 
   
	Map getInDocumentTasksWitoutPackage(DSDivision divisionNS, DSUser user, Integer offset, int tempLimit, Integer searchOfficeNumber, String sortField,
			Boolean ascending, String parsedFilterBy, Object filterValue, Boolean withBackup, Collection<Label> labels, boolean allLabels,
			List<FilterCondition> filterConditions) throws EdmException;
    

}
