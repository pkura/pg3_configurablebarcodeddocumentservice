package pl.compan.docusafe.service;

/* User: Administrator, Date: 2005-04-21 16:40:07 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ServiceNotFoundException.java,v 1.7 2009/03/04 09:27:37 pecet3 Exp $
 */
public class ServiceNotFoundException extends ServiceException
{
    public ServiceNotFoundException(String serviceId)
    {
        super("Nie istnieje us�uga "+serviceId);
    }

    public ServiceNotFoundException(String message, Throwable cause)
    {
        super(message, cause);
    }       
}
