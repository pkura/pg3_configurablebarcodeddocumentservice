package pl.compan.docusafe.service.epuap;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.axis2.AxisFault;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.ProcessingInstruction;
import org.dom4j.io.SAXReader;
import org.jfree.util.Log;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.certificates.ElectronicSignatureStore;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.nfos.NfosFactory;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytka;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.gov.epuap.ws.pull.PkPullServiceStub;
import pl.gov.epuap.ws.pull.PkPullServiceStub.IdentyfikatorPodmiotuTyp;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullOczekujace;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullOczekujaceTyp;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullPobierz;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullPobierzTyp;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullPotwierdz;
import pl.gov.epuap.ws.pull.PkPullServiceStub.ZapytaniePullOczekujace;
import pl.gov.epuap.ws.pull.PkPullServiceStub.ZapytaniePullOczekujaceTyp;
import pl.gov.epuap.ws.pull.PkPullServiceStub.ZapytaniePullPobierz;
import pl.gov.epuap.ws.pull.PkPullServiceStub.ZapytaniePullPobierzTyp;
import pl.gov.epuap.ws.pull.PkPullServiceStub.ZapytaniePullPotwierdz;
import pl.gov.epuap.ws.pull.PkPullServiceStub.ZapytaniePullPotwierdzTyp;
import pl.gov.epuap.ws.pull.PkPullServiceStub.ZapytaniePullTyp;
import pl.gov.epuap.ws.pull.PullFaultMsg;


/**
 * Klasa EpuapManager.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class EpuapImportManager 
{

	private static final Logger log = LoggerFactory.getLogger(EpuapImportManager.class);
	private String adresSkrytki;
	private String nazwaSkrytki;
	private IdentyfikatorPodmiotuTyp podmiot;
	private PkPullServiceStub service;
	
	/** wskazuje, czy obsluzony zostal dokument, czy UPO. Domyslnie dokument. */
	private boolean dokument1_upo0 = true;
	
	/** wskazuje, czy obsluzony zostal dokument (TRUE), czy UPO (FALSE). Domyslnie dokument */
	public boolean isDokument1_upo0() { return dokument1_upo0; }
	public void setDokument1_upo0(boolean dokument1_upo0) { this.dokument1_upo0 = dokument1_upo0; }
	
	/** do obslugi dat w XML, Joda Time, Thread-safe */
	private static final DateTimeFormatter XML_DATE_TIME_FORMAT = ISODateTimeFormat.dateTime();

	
	public EpuapImportManager(EpuapSkrytka skrytka) throws Exception 
	{
//		this(skrytka.getAdresSkrytki(),	skrytka.getNazwaSkrytki(),skrytka.getIdentyfikatorPodmiotu());
		this(skrytka.getAdresSkrytki(),	skrytka.getNazwaSkrytki(),skrytka.getIdentyfikatorPodmiotu(),skrytka.getCertificateFileName(), skrytka.getCertificatePassword());
	}
	
	public EpuapImportManager(String adresSkrytki, String nazwaSkrytki, String identyfikatorPodmiotu) throws Exception 
	{
		this(adresSkrytki, nazwaSkrytki,identyfikatorPodmiotu ,null, null);
	}
	
	/**
	 * @param adresSkrytki
	 * @param nazwaSkrytki
	 * @param identyfikatorPodmiotu
	 * @param individualCertificateFile jesli certyfikat jest zale�ny od innych element�w mozemy przekaza� w�asny znajduj�cy si� w HOME
	 * @param certificatePassword has�o do certyfiaktu
	 * @throws Exception
	 */
	public EpuapImportManager(String adresSkrytki, String nazwaSkrytki, String identyfikatorPodmiotu, String individualCertificateFile, String certificatePassword) throws Exception 
	{
		super();
		this.adresSkrytki = adresSkrytki;
		this.nazwaSkrytki = nazwaSkrytki;
		this.podmiot = new IdentyfikatorPodmiotuTyp();
		this.podmiot.setIdentyfikatorPodmiotuTyp(identyfikatorPodmiotu);
		
		AxisClientConfigurator conf = new AxisClientConfigurator();
		if(individualCertificateFile != null && !individualCertificateFile.isEmpty())
			conf = new AxisClientConfigurator(individualCertificateFile, certificatePassword);
		
		this.service = new PkPullServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(service, "/pk_external_ws/services/pull");
	}
	
	public Integer getOczekujace() throws AxisFault, Exception
	{
		ZapytaniePullOczekujaceTyp zapytanieOczekujaceTyp = new ZapytaniePullOczekujaceTyp();
		zapytanieOczekujaceTyp.setAdresSkrytki(adresSkrytki);
		zapytanieOczekujaceTyp.setNazwaSkrytki(nazwaSkrytki);		
		zapytanieOczekujaceTyp.setPodmiot(podmiot);
		
		ZapytaniePullOczekujace zapytanieOczekujace = new ZapytaniePullOczekujace();
		zapytanieOczekujace.setZapytaniePullOczekujace(zapytanieOczekujaceTyp);
		
		OdpowiedzPullOczekujace odpowiedzOczekujacy = service.oczekujaceDokumenty(zapytanieOczekujace);
		OdpowiedzPullOczekujaceTyp odpt = odpowiedzOczekujacy.getOdpowiedzPullOczekujace();
		return odpt.getOczekujace();
	}
	
	public OdpowiedzPullPobierzTyp pobierzNastepny() throws RemoteException, PullFaultMsg
	{
		ZapytaniePullPobierzTyp zapytaniePobierzTyp = new ZapytaniePullPobierzTyp();
		zapytaniePobierzTyp.setAdresSkrytki(adresSkrytki);
		zapytaniePobierzTyp.setNazwaSkrytki(nazwaSkrytki);
		zapytaniePobierzTyp.setPodmiot(podmiot);
		
		ZapytaniePullPobierz zapytaniePobierz = new ZapytaniePullPobierz();
		zapytaniePobierz.setZapytaniePullPobierz(zapytaniePobierzTyp);
		OdpowiedzPullPobierz odpowiedz = service.pobierzNastepny(zapytaniePobierz);
		return odpowiedz.getOdpowiedzPullPobierz();
	}
	
	/** Zapisuje dokument i jego za��czniki, zak�ada istnienie okre�lonej struktury element�w w zapisywanym dokumencie.
	 * Nadaje si� wi�c tylko do zwyk�ych dokument�w ale nie do UPO */
	public InOfficeDocument zapiszDokument(OdpowiedzPullPobierzTyp odpowiedz) throws IOException, EdmException, DocumentException, PullFaultMsg, NoSuchAlgorithmException
	{
		InOfficeDocument doc = createDocument(odpowiedz);
		  Map<String,Object> values = new HashMap<String, Object>();
		
		doc.getDocumentKind().logic().setAdditionalValuesFromEpuapDocument(odpowiedz ,doc ,values);
		doc.getDocumentKind().saveDictionaryValues(doc, values);
		doc.getDocumentKind().saveMultiDictionaryValues(doc,values);
		doc.getDocumentKind().setOnly(doc.getId(),values);
		DSApi.context().session().update(doc);
		/**Zapisuje formularz*/
		Attachment attachment = new Attachment("Formularz");
		doc.createAttachment(attachment);
		Integer lenght = FileUtils.lenghtFormInputStream(odpowiedz.getDokument().getZawartosc().getInputStream());
		attachment.createRevision(odpowiedz.getDokument().getZawartosc().getInputStream(),lenght,odpowiedz.getDokument().getNazwaPliku());
		/***/
		attachment = new Attachment("Dane dodatkowe");
		doc.createAttachment(attachment);
		
		//lenght = FileUtils.lenghtFormInputStream(odpowiedz.getDaneDodatkowe().getInputStream());
		//attachment.createRevision(odpowiedz.getDaneDodatkowe().getInputStream(),lenght,"dane.xml");
		String danedodatkowe = dodajXSLdoDanychDodatkowych(odpowiedz);
		lenght = FileUtils.lenghtFormInputStream(new ByteArrayInputStream(danedodatkowe.getBytes()));
		attachment.createRevision(new ByteArrayInputStream(danedodatkowe.getBytes()),lenght,"dane.xml");
		
		SAXReader reader = new SAXReader();
		org.dom4j.Document xmlDocument = reader.read(odpowiedz.getDokument().getZawartosc().getInputStream());
		List<org.dom4j.Element> zalaczniki  = null;
		try
		{
			zalaczniki = xmlDocument.getRootElement().element("TrescDokumentu").element("Wartosc").element("Zalaczniki").elements("Zalacznik");
		}
		catch (Exception e) {
			/* nie wymaga jakiejkolwiek obslugi ani logowania.
			 * To po prostu brak zalacznikow */
		}
		if(zalaczniki != null)
		{
			for (Element zalacznik : zalaczniki) 
			{
				String nazwaPliku = zalacznik.attributeValue("nazwaPliku");
				org.dom4j.Element daneZalacznika = zalacznik.element("DaneZalacznika");
				if(daneZalacznika.getText().trim().length() > 1)
				{
					byte[] bytes = Base64.decodeBase64(daneZalacznika.getText().trim());
					InputStream is = new ByteArrayInputStream(bytes);
					attachment = new Attachment("Za��cznik");
					doc.createAttachment(attachment);
					lenght = FileUtils.lenghtFormInputStream(odpowiedz.getDokument().getZawartosc().getInputStream());
					attachment.createRevision(is,bytes.length,nazwaPliku);
				}
			}
		}
		return doc;
	}
	
	/** Zapisuje nowy dokument w Docusafe, przy czym podstaw� jest
	 * albo tre�� dokumentu z ePUAP (plus za��czniki) albo UPO.
	 * 
	 * NFOS wymaga by UPO nie bylo przypinane jako zalacznik lecz stanowilo osobny dokument */
	public InOfficeDocument zapiszDokumentlubUPO(OdpowiedzPullPobierzTyp odpowiedz, boolean upoJakoDokument) throws IOException, EdmException, DocumentException, PullFaultMsg, NoSuchAlgorithmException
	{
		InOfficeDocument doc = null;
		
		Attachment attachment = null;
		Integer lenght = null;
		
		SAXReader reader = new SAXReader();
		org.dom4j.Document xmlDocument = null;
		org.dom4j.Element root = null;
		try {
			xmlDocument = reader.read(odpowiedz.getDokument().getZawartosc().getInputStream());
			root = xmlDocument.getRootElement();
//			tescik(odpowiedz.getDokument().getZawartosc().getInputStream());
		}
		catch (Exception e) {
			log.error("blad parsowania ", e);
		}
		
		// ePUAP ID
		Long epuapId = null;
		epuapId = getEpuapId(odpowiedz);
		
		// czy to dokument czy UPO?
		org.dom4j.Element element  = null;
		element = root.element("TrescDokumentu");

		boolean documentForce = false;
		if(!root.getName().equals("Dokument"))
			documentForce = true;
		
		if (element==null && !documentForce) {
			
			// teraz wiadomo, ze to jest UPO ---------------------------------
			// TODO maila ma zosta� wys�any do osoby kt�ra wys�a�a pismo
			// TODO tu moze byc m.in. PND (nadchodzi po tym, jak przez iles dni nie uda sie dostarczyc pisma Doreczycielem )
			
			dokument1_upo0 = false;
			if (upoJakoDokument) {
//				String[] upoType_ID = getUpoIdEtType(root);
				doc = createUPODocument(odpowiedz, epuapId, root);
				Map<String,Object> values = new HashMap<String, Object>();
				doc.getDocumentKind().logic().setAdditionalValuesFromEpuapDocument(odpowiedz ,doc ,values);
				doc.getDocumentKind().saveDictionaryValues(doc, values);
				doc.getDocumentKind().saveMultiDictionaryValues(doc,values);
				doc.getDocumentKind().setOnly(doc.getId(),values);
				doc.getDocumentKind().logic().saveEpuapDetails(doc, epuapId, null);
				DSApi.context().session().update(doc);
			}
			
			obslugaPowiazanUPO(doc, epuapId, odpowiedz, root, upoJakoDokument);
			
		} else {
			
			// teraz wiadomo, ze to jest dokument ---------------------------------
			// mail ma zosta� wys�any na adres przypisany od skrytki
			
			dokument1_upo0 = true;
			doc = createDocument(odpowiedz);
			
			doc.getDocumentKind().logic().saveEpuapDetails(doc, epuapId, null);
			DSApi.context().session().update(doc);
			
			  Map<String,Object> values = new HashMap<String, Object>();
				
				doc.getDocumentKind().logic().setAdditionalValuesFromEpuapDocument(odpowiedz ,doc ,values);
				doc.getDocumentKind().saveDictionaryValues(doc, values);
				doc.getDocumentKind().saveMultiDictionaryValues(doc,values);
				doc.getDocumentKind().setOnly(doc.getId(),values);
				DSApi.context().session().update(doc);
			
			List<org.dom4j.Element> zalaczniki  = listaZalacznikow( element );
			if(zalaczniki != null)
			{
				for (Element zalacznik : zalaczniki)
				{
					String nazwaPliku = zalacznik.attributeValue("nazwaPliku");
					org.dom4j.Element daneZalacznika = zalacznik.element("DaneZalacznika");
					if(daneZalacznika.getText().trim().length() > 1)
					{
						byte[] bytes = Base64.decodeBase64(daneZalacznika.getText().trim());
						InputStream is = new ByteArrayInputStream(bytes);
						attachment = new Attachment("Za��cznik");
						doc.createAttachment(attachment);
						lenght = FileUtils.lenghtFormInputStream(odpowiedz.getDokument().getZawartosc().getInputStream());
						attachment.createRevision(is,bytes.length,nazwaPliku);
					}
				}
			}
		}
		
		//wyciaganie podpisu
		List<org.dom4j.Element> signatureElements  = null;
		signatureElements = root.elements("Signature");
		long addedSignatureId = 0;
		if(signatureElements!=null && !signatureElements.isEmpty()){
			try {
				String signatureFile = IOUtils.toString(odpowiedz.getDokument().getZawartosc().getInputStream(), "UTF-8");
				String date = signatureElements.get(0).element("Object").element("QualifyingProperties").element("SignedProperties")
					.element("SignedSignatureProperties").element("SigningTime").getStringValue();
				Date signingTime = DateUtils.epuapDateFormat.parse(date);
				addedSignatureId = ElectronicSignatureStore.getInstance()
					.storeDocumentSignatureFromFile(doc.getId(), signatureFile, signatureFile, signingTime);
			} catch (ParseException e) {
				log.error("blad zapisu podpisu do bazy", e);
			} catch (SQLException e) {
				log.error("blad zapisu podpisu do bazy", e);
			} catch (Exception e) {
				log.error("Nieznany blad odczytu sygnatury. Dokument bez mozliwosci weryfikacji podpisu zostanie zapisany w systemie.", e);
			}
			if(signatureElements.size()>1){
				for(org.dom4j.Element sigElement : signatureElements){
					try {
						//wyci�ganie daty z podpisu
						String date = sigElement.element("Object").element("QualifyingProperties").element("SignedProperties")
											.element("SignedSignatureProperties").element("SigningTime").getStringValue();
						Date signingTime = DateUtils.epuapDateFormat.parse(date);
						//tworzenie nowego podpisu i przypisanie go do dokumentu
						ElectronicSignatureStore.getInstance()
							.storeDocumentSignatureFromFile(doc.getId(), "null", 
								sigElement.asXML(), signingTime, addedSignatureId);
					} catch (SQLException e) {
						log.error("blad zapisu podpisu do bazy", e);
					} catch (Throwable t) {
						log.error("blad zapisu podpisu do bazy", t);
					}
				}		
			}	
		}
		
		//czynnosci identyczne dla obu przypadkow,
		// ale UWAGA, bo zmienna element ma tu zupelnie rozne zawartosci 
		
		/* Zapisuje formularz */
		attachment = new Attachment("Formularz");
		doc.createAttachment(attachment);
		lenght = FileUtils.lenghtFormInputStream(odpowiedz.getDokument().getZawartosc().getInputStream());
		attachment.createRevision(odpowiedz.getDokument().getZawartosc().getInputStream(),lenght,odpowiedz.getDokument().getNazwaPliku());
		
		
		/* Zapisuje dane dodatkowe (metadane, m.in. zawieraj�ce adres skrytki i ID epuapowe dokumentu) */
		attachment = new Attachment("Dane dodatkowe");
		doc.createAttachment(attachment);
		String danedodatkowe = dodajXSLdoDanychDodatkowych(odpowiedz);
		lenght = FileUtils.lenghtFormInputStream(new ByteArrayInputStream(danedodatkowe.getBytes()));
		attachment.createRevision(new ByteArrayInputStream(danedodatkowe.getBytes()),lenght,"dane.xml");
		return doc;
	}
	
	private String dodajXSLdoDanychDodatkowych(OdpowiedzPullPobierzTyp odpowiedz) {
	try {
	SAXReader reader = new SAXReader();
	org.dom4j.Document xmlDocument = reader.read(odpowiedz.getDaneDodatkowe().getInputStream()); 
	String stylePath = "file:///" + Docusafe.getHome().getPath() + "\\nfosXSL\\" + "dane_style.xsl";
	Map<String,String> args = new HashMap<String, String>();
	args.put( "type", "text/xsl" );
	args.put( "href", stylePath );

	DocumentFactory factory = new DocumentFactory();

	ProcessingInstruction pi = factory.createProcessingInstruction( "xml-stylesheet", args );
	List l = xmlDocument.content();
	l.add(0, pi);
	xmlDocument.setContent(l);
	return xmlDocument.asXML();
	}
	catch (Exception e) {
	log.error("blad parsowania ", e);
	}
	return null;
	}
	/** Wpisuje do dokumentu albo link do UPO albo UPO jako zalacznik.
	 * Dokument moze nie istniec i nie jest to blad (w przypadku UPO typu PND).
	 * Takie dokumenty mog� te� istnie� dwa (przychodz�cy i wychodz�cy)
	 * o ile wysy�amy Z i DO skrytki obs�ugiwanej przez DS.
	 * 
	 * @param upoDoc - dokument DS zrobiony na podstawie UPO,
	 * 				   moze byc NULL! jesli UPO ma byc zalacznikiem.
	 * @param epuapId - id nadane dokumentowi przez ePUAP
	 * @param upo - obiekt wyslany do nas przez ePUAP
	 * @param root - korzen drzewa XML
	 * @param upoJakoDokument - gdy FALSE, UPO bedzie zalacznikiem do glownego dokumentu.
	 * 							Gdy TRUE, UPO bedzie osobnym dokumentem. */
	private void obslugaPowiazanUPO(Document upoDoc, Long epuapId, OdpowiedzPullPobierzTyp upo, org.dom4j.Element root, boolean upoJakoDokument) {
		
		// znalezc dokument, z ktorym UPO jest powi�zane i mu dopisac link albo zalacznik.

		// TODO : koniecznie umozliwic parametryzacje tego wywolania.
		// Inne systemy moga chciec korzystac z innych fabryk.
		// Nie mozna tego jednak przeniesc do logiki, bo do ktorej? - np. ta fabryka zwraca rozne dokumenty roznych typow.
		List<Document> docs = NfosFactory.getDocumentsByEpuapId (epuapId);
		if (docs==null) return;
		
		
		// okreslenie typu UPO i przy okazji nazwy zalacznika w DS ( != nazwie pliku )
		
		String nazwaUpo = "UPD";
		org.dom4j.Element element = root.element("UPP"); // czy to potwierdzenie PRZEDLOZENIA ?
		if (element!=null)
			nazwaUpo = "UPP";
		else {
			element = root.element("UPD"); // czy to potwierdzenie DOSTARCZENIA ?
			if (element!=null) {
				//pnd to upd z informacj� �e up�yn�� termin odebrania dokumentu
				List<Element> listElements = element.elements("InformacjaUzupelniajaca");
				if(listElements != null || listElements.size() > 0) {
					for (Element element2 : listElements) {
						if(element2.attributeValue("TypInformacjiUzupelniajacej").equals("PrzyczynaNiepowodzenia")){
							nazwaUpo = "PND";
						}
					}
				}
			}
		}
		
		// Przetwarzanie kolejnych dokumentow powiazanych z tym UPO.
		// Dla dokument�w przesy�anych pomiedzy skrytkami obs�ugiwanymi przez DS
		// po otrzymaniu dokumentu przychodzacego UPO doda sie jeszcze raz
		// do dokumentu wychodzacego, bo one maja to samo epuapID. Oto dlaczego:
		// w ePUAP jest jeden dokument i ma okreslone epuapID. U nas sa dwa dokumenty:
		// ten wyslany do epuap z jednej skrytki i otrzymany z epuap z drugiej skrytki.
		// nie da sie okreslic, do ktorego z nich nalezy podpiac UPO.
		
		// TODO : zrobic mechanizm, kt. sprawdza, czy taki (identyczny) zalacznik/link juz istnieje.
		// - to chyba jedyne mozliwe zabezpieczenie przed dodawaniem podwojnym
		
		for (Document doc : docs){
			
			// dodanie zalacznika lub linku ------------------------------
			if (upoJakoDokument)
				dodajUPOJakoLinkDoDokumentu(upoDoc, doc, nazwaUpo, upo);
			else
				dodajUPOJakoZalacznik(upoDoc, doc, nazwaUpo, upo);
			
			
			// zaznaczenie powodzenia dostarczenia w dokumencie -----------
			if (doc instanceof OutOfficeDocument)
				if ("UPD".equals(nazwaUpo)) { // dodajemy tylko dla UPD, bo UPO nie oznacza faktycznego odbioru
					int kod = upo.getStatus().getKod(); // kod statusu, nie wiem, czy jest wiarygodny
					Date dtDoreczenia = odczytajDateDoreczenia(element);
					if (dtDoreczenia!=null) {
						// wpisac w tym dokumencie date doreczenia (co oznacza powodzenie doreczenia)
						doc.getDocumentKind().logic().oznaczDokumentJakoDostarczony(upoDoc, doc, upo, dtDoreczenia, kod);
					}
				}
		}
	}
	
	/** Odczytuje z dokumentu date doreczenia
	 * @param element - element UPD z UPO, dokladniej {@code<pos:UPD>} 
	 * @throws ParseException */
	private Date odczytajDateDoreczenia(Element element) {
		if (AvailabilityManager.isAvailable("nfos.DataOdbioru")) {
			org.dom4j.Element data = element.element("DataOdbioru");
			Date dataOdbioru;
			try {
				dataOdbioru = new SimpleDateFormat("yyyy-MM-dd").parse(data
						.getText());
			} catch (ParseException e) {
				log.error("Blad parsowania daty");
				dataOdbioru = null;
			}
			return dataOdbioru;
		} else {
			 org.dom4j.Element data = element.element("DataDoreczenia");
			 return XML_DATE_TIME_FORMAT.parseDateTime(data.getText()).toDate();
		}
	}
	
	/** 
	 * @param upoDoc	- dokument DS zrobiony na podstawie UPO, tutaj zawsze NULL, bo mamy zapisac jako zalacznik 
	 * @param doc		- dokument, do kt�rego trzeba UPO dopisac (jako zalacznik)
	 * @param nazwaUpo	- nazwa dla zalacznika (nie nazwa pliku)
	 * @param upo		- obiekt wyslany do nas przez ePUAP
	 */
	private void dodajUPOJakoZalacznik(Document upoDoc, Document doc, String nazwaUpo, OdpowiedzPullPobierzTyp upo){
		try {
			Attachment attachment = new Attachment(nazwaUpo);
			doc.createAttachment(attachment);
			int lenght = FileUtils.lenghtFormInputStream(upo.getDokument().getZawartosc().getInputStream());
			attachment.createRevision(upo.getDokument().getZawartosc().getInputStream(), lenght, upo.getDokument().getNazwaPliku());
		} catch (Exception e) {
			//AccessDeniedException, IOException, EdmException
			Log.error("blad dodawania zalacznika do dokumentu", e);
		}
	}
	
	/** 
	 * @param upoDoc	- dokument DS zrobiony na podstawie UPO, tutaj nigdy NULL,
	 * 					bo mamy zapisac link do istniejacego (tego) dokumentu 
	 * @param doc		- dokument, w kt�rym ma powstac link do UPO
	 * @param nazwaUpo	- nazwa dla zalacznika (nie nazwa pliku), tu moze byc bez znaczenia
	 * @param upo		- obiekt wyslany do nas przez ePUAP
	 */
	private void dodajUPOJakoLinkDoDokumentu(Document upoDoc, Document doc, String nazwaUpo, OdpowiedzPullPobierzTyp upo){
		try {
			
			doc.getDocumentKind().logic().dodajLinkDoUPO(upoDoc, doc, nazwaUpo, upo);
			
		} catch (Exception e) {
			Log.error("blad dodawania do dokumentu linku do UPO", e);
		}
	}
	
	
	/** zwraca liste zalacznikow do dokumentu z elementu "TrescDokumentu" (opakowanie try/catch) */
	@SuppressWarnings("unchecked")
	private List<org.dom4j.Element> listaZalacznikow( org.dom4j.Element elementTrescDokumentu ){
		List<org.dom4j.Element> zalaczniki  = null;
		try {
			if(zalaczniki == null)
				zalaczniki = elementTrescDokumentu.element("Zalaczniki").elements("Zalacznik");
		} catch (Exception e) { }
		try {
			if(zalaczniki==null){
				zalaczniki = elementTrescDokumentu.element("Wartosc").element("Zalaczniki").elements("Zalacznik");
			}
		} catch (Exception e) { /* nie wymaga jakiejkolwiek obslugi ani logowania. To po prostu brak zalacznikow*/ }
		
		return zalaczniki;
	}
	
	/** odczytuje z XML metadanych (wysylanych przez ePUAP do uzytkownikow koncowych)
	 * id jakie dany dokument ma nadane w ePUAP.
	 * Jest ono potrzebne do powi�zania UPO z dokumentem */
	private Long getEpuapId(OdpowiedzPullPobierzTyp odpowiedzEpuap){
		
		SAXReader reader = new SAXReader();
		org.dom4j.Document xmlDocument = null;
		org.dom4j.Element elemEpuapId = null;
		try {
			xmlDocument = reader.read(odpowiedzEpuap.getDaneDodatkowe().getInputStream());
		}
		catch (Exception e) {
			Log.error("blad parsowania metadanych", e);
			return null;
		}
		
		try {
			elemEpuapId = xmlDocument.getRootElement()
					.element("HistoriaKomunikacji")
					.element("Skrytka")
					.element("IdentyfikatorDokumentu");
		}
		catch (NullPointerException npe){
			Log.error("XML metadanych nie zawiera elementu /HistoriaKomunikacji/Skrytka/IdentyfikatorDokumentu choc powinien");
			return null;
		}
		if (elemEpuapId==null) return null;
		
		String epuapIdStr = (String) elemEpuapId.getData();
		if (StringUtils.isEmpty(epuapIdStr))
			return null;
		return Long.parseLong(epuapIdStr);
	}
	
	/** odczytuje z XML metadanych (wysylanych przez ePUAP do uzytkownikow koncowych)
	 * id jakie dany dokument ma nadane w ePUAP.
	 * Jest ono potrzebne do powi�zania UPO z dokumentem */
	private Long getEpuapIdNfos(OdpowiedzPullPobierzTyp odpowiedzEpuap){
		
		SAXReader reader = new SAXReader();
		org.dom4j.Document xmlDocument = null;
		org.dom4j.Element elemEpuapId = null;
		try {
			xmlDocument = reader.read(odpowiedzEpuap.getDaneDodatkowe().getInputStream());
		}
		catch (Exception e) {
			Log.error("blad parsowania metadanych", e);
			return null;
		}
		
		try {
			elemEpuapId = xmlDocument.getRootElement()
					.element("HistoriaKomunikacji")
					.element("Doreczenie")
//					.element("IdentyfikatorPoswiadczenia");
					.element("IdentyfikatorDokumentu");
		}
		catch (NullPointerException npe){
			Log.error("XML metadanych nie zawiera elementu /HistoriaKomunikacji/Doreczenie/IdentyfikatorDokumentu choc powinien");
			return null;
		}
		if (elemEpuapId==null) return null;
		
		String epuapIdStr = (String) elemEpuapId.getData();
		if (StringUtils.isEmpty(epuapIdStr))
			return null;
		return Long.parseLong(epuapIdStr);
	}
	
	/** odczytuje z XMLa UPO typ tego UPO (UPP/UPD) oraz jego ID.
	 * Jest to potrzebne do stworzenia tytu�u.
	 * Zwraca tablice stringow 2-elementowa:
	 * [0] - typ
	 * [1] - id (zawierajace tez typ */
	private String[] getUpoIdEtType(org.dom4j.Element root){
		
		String[] ret = new String[]{"PND","bez numeru"};
		
		org.dom4j.Element element = root.element("UPP"); // czy to potwierdzenie PRZEDLOZENIA ?
		if (element!=null)
			ret[0] = "UPP";
		else {
			element = root.element("UPD"); // czy to potwierdzenie DOSTARCZENIA ?
			if (element!=null){
				ret[0] = "UPD";
				//pnd to upd z informacj� �e up�yn�� termin odebrania dokumentu
				List<Element> listElements = element.elements("InformacjaUzupelniajaca");
				if(listElements != null || listElements.size() > 0) {
					for (Element element2 : listElements) {
						if(element2.attributeValue("TypInformacjiUzupelniajacej").equals("PrzyczynaNiepowodzenia")){
							ret[0] = "PND";
							ret[1] = (String) element2.getData();
						} if(element2.attributeValue("TypInformacjiUzupelniajacej").equals("Identyfikator ePUAP dokumentu")) {
							ret[1] = (String) element2.getData();
						}
					}
				}
			}
		}
		if (element != null && !ret[0].equals("PND"))
			ret[1] = element.element("IdentyfikatorPoswiadczenia").getText();
		
		return ret;
	}	
	
	public OdpowiedzPullPotwierdz potwierdzOdebranie(OdpowiedzPullPobierzTyp odpowiedz) throws IOException, NoSuchAlgorithmException, PullFaultMsg
	{
		ZapytaniePullPotwierdz potwierdzenie = new ZapytaniePullPotwierdz();
		ZapytaniePullPotwierdzTyp potwierdzTyp = new ZapytaniePullPotwierdzTyp();
		setParam(potwierdzTyp);
		String skrot = Base64.encodeBase64String(DigestUtils.sha(odpowiedz.getDokument().getZawartosc().getInputStream()));
		potwierdzTyp.setSkrot(skrot.trim());
		potwierdzenie.setZapytaniePullPotwierdz(potwierdzTyp);
		return service.potwierdzOdebranie(potwierdzenie);
	}
	
	private void setParam(ZapytaniePullTyp zapytanieTyp)
	{
		zapytanieTyp.setAdresSkrytki(adresSkrytki);
		zapytanieTyp.setNazwaSkrytki(nazwaSkrytki);
		zapytanieTyp.setPodmiot(podmiot);
	}
	
	/** tworzy dokument z ePUAP tradycyjnie - por. createUPODocument */ 
	private InOfficeDocument createDocument(OdpowiedzPullPobierzTyp odpowiedz) throws EdmException
	{
		InOfficeDocument doc = new InOfficeDocument();
		DocumentKind documentKind = DocumentKind.findByCn(Docusafe.getAdditionProperty("dockind.default"));
		doc.setDocumentKind(documentKind);
		doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
		doc.setDivisionGuid(DSDivision.ROOT_GUID);
		doc.setCurrentAssignmentAccepted(Boolean.FALSE);
		doc.setCreatingUser(DSApi.context().getPrincipalName());
		doc.setSummary("Pismo ePUAP od "+odpowiedz.getDanePodmiotu().getImieSkrot()+
				" "+odpowiedz.getDanePodmiotu().getNazwiskoNazwa()+" "+odpowiedz.getDokument().getNazwaPliku());
		doc.setForceArchivePermissions(false);
		doc.setAssignedDivision(DSDivision.ROOT_GUID);
		doc.setClerk(DSApi.context().getPrincipalName());
		doc.setSender(createSender(odpowiedz));
		doc.setSource("epuap");
		if (Docusafe.getAdditionProperty("DomyslnyOdbiorcaInNadawcaOut")!=null &&!Docusafe.getAdditionProperty("DomyslnyOdbiorcaInNadawcaOut").isEmpty() )
		{
		Recipient rec = new Recipient();
		Person pers = new Person().find(Long.parseLong(Docusafe.getAdditionProperty("DomyslnyOdbiorcaInNadawcaOut")));
		rec.fromMap(pers.toMap());
		rec.create();
		doc.addRecipient(rec);
		}
		doc.setInPackage(false);
		doc.setDelivery(InOfficeDocumentDelivery.findByName(InOfficeDocumentDelivery.DELIVERY_EPUAP)); // w bazie musi istnie� taki wpis
		doc.setKind(InOfficeDocumentKind.getInOfficeDocumentKind(documentKind));
		Calendar currentDay = Calendar.getInstance();
		currentDay.setTime(GlobalPreferences.getCurrentDay());
		doc.setIncomingDate(currentDay.getTime());
		doc.setOriginal(true);
		doc.create();
		Journal journal = Journal.getMainIncoming();
		Long journalId;
		journalId = journal.getId();
		Integer sequenceId = null;
		sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date(currentDay.getTime().getTime()));
		doc.bindToJournal(journalId, sequenceId);
		doc.setDocumentKind(documentKind);
		documentKind.logic().setDocValuesAfterImportFromEpuap(OfficeDocument.find(doc.getId()), null, false);
		documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
		documentKind.logic().documentPermissions(doc);
		return doc;
	}
	
	
	/** tworzy caly nowy dokument w Docusafe na podstawie UPO z ePUAP
	 * - por. createDocument.
	 * Razem z UPO i dokumentem przychodza z ePUAP metadane
	 * I SA ONE IDENTYCZNE DLA DOKUMENTU I DLA JEGO UPO, NIE ROZNIA SIE NAWET MILISEKUNDAMI
	 * Ponizej jest PRZYKLADOWY XML metadanych.
	 * <pre> {@code
<?xml version="1.0" encoding="UTF-8"?>
<DaneDodatkowe>
    <HistoriaKomunikacji>
        <Skrytka>
            <Nadawca>urzadMK</Nadawca>
            <Odbiorca>urzadMK</Odbiorca>
            <AdresNadania>/urzadMK/ds</AdresNadania>
            <AdresOdpowiedzi>/urzadMK/SKRYTKA</AdresOdpowiedzi>
            <IdentyfikatorDokumentu>280634</IdentyfikatorDokumentu>
            <IdentyfikatorPoswiadczenia>0</IdentyfikatorPoswiadczenia>
            <RodzajDokumentu>Dokument</RodzajDokumentu>
            <DataPrzekazania>2013-02-05T15:30:14.57</DataPrzekazania>
            <IdentyfikatorZleceniaDoreczenia/>
        </Skrytka>
    </HistoriaKomunikacji>
</DaneDodatkowe> } </pre>
	 * @param epuapId - id jakie w ePUAP nadano temu UPO i jednoczesnie dokumentowi, ktorego UPO dotyczy
	 * (bo dokument i jego upo maja takie same epuapId!!!*/ 
	private InOfficeDocument createUPODocument(OdpowiedzPullPobierzTyp odpowiedz, Long epuapId, org.dom4j.Element root) throws EdmException
	{
		String[] idEtType = getUpoIdEtType(root);
		
		
		InOfficeDocument doc = new InOfficeDocument();
		DocumentKind documentKind = DocumentKind.findByCn(DocumentKind.NORMAL_KIND);
		doc.setDocumentKind(documentKind);
		doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
		doc.setDivisionGuid(DSDivision.ROOT_GUID);
		doc.setCurrentAssignmentAccepted(Boolean.FALSE);
		doc.setCreatingUser(DSApi.context().getPrincipalName());
		doc.setSummary("UPO - Po�wiadczenie " + (idEtType[0].equals("PND")?"niedor�czenia Dokumentu":idEtType[1]) + " do dokumentu o ID " + epuapId +" w ePUAP"); //+ " "+odpowiedz.getDokument().getNazwaPliku());
		doc.setForceArchivePermissions(false);
		doc.setAssignedDivision(DSDivision.ROOT_GUID);
		doc.setClerk(DSApi.context().getPrincipalName());
//		Person sender = Person.findByGivenFieldValue("adresSkrytkiEpuap", odpowiedz.getAdresOdpowiedzi());

		doc.setSender(createSender(odpowiedz));					// TODO : brak nadawcy. Co z tym zrobic?
		doc.setSource("epuap");
		doc.setDelivery(InOfficeDocumentDelivery.findByName(InOfficeDocumentDelivery.DELIVERY_EPUAP)); // w bazie musi istnie� taki wpis
		doc.setKind(InOfficeDocumentKind.getInOfficeDocumentKind(documentKind));
		Calendar currentDay = Calendar.getInstance();
		currentDay.setTime(GlobalPreferences.getCurrentDay());
		doc.setIncomingDate(currentDay.getTime());
		doc.setDocumentDate(odpowiedz.getDataNadania().getTime());
		doc.setOriginal(true);
		doc.setInPackage(false);
		doc.create();
		
/* UPO nie jest osobno dodawane do dziennika */
		
//		Journal journal = Journal.getMainIncoming();
//		Long journalId;
//		journalId = journal.getId();
//		Integer sequenceId = null;
//		sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date(currentDay.getTime().getTime()));
//		doc.bindToJournal(journalId, sequenceId);
		
		doc.setDocumentKind(documentKind);
		documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
		documentKind.logic().documentPermissions(doc);
		return doc;
	}
	
	
	private Sender createSender(OdpowiedzPullPobierzTyp odpowiedz) throws EdmException
	{
		//traktuj� adres skrytki jako unikaln� warto�� dla persona,
		//czyli brak os�b o takim samym adresie skrytki,
		//je�li taki przypadek wyst�pi bior� pierwszy� z g�ry.
//		String adresSkrytkiEpuap = odpowiedz.getAdresSkrytki();
		String adresSkrytkiEpuap = odpowiedz.getAdresOdpowiedzi();
		List<Person> personList = null;
		Person[] persons = null;
		
		try {
			QueryForm form = new QueryForm(0, 0);
			form.addProperty("adresSkrytkiEpuap", adresSkrytkiEpuap);
			form.addProperty("DISCRIMINATOR", "PERSON");
			SearchResults<? extends Person> personSearchResults = Person.search(form);
			persons = personSearchResults.results();
			
		} catch (EdmException e) {
			log.error("B�ad podczas pobierania z dso_person", e);
		}
		if (persons.length > 0) {
			Sender sender = new Sender();
			sender.fromMap(persons[0].toMap());
			sender.setDictionaryGuid("rootdivision");
			sender.setDictionaryType(Person.DICTIONARY_RECIPIENT);
			sender.setLparam("epuap");
			sender.setBasePersonId(persons[0].getId());
			sender.create();
			return sender;
		} else {
			Person person = new Person();
			person.setDictionaryGuid(DSDivision.ROOT_GUID);
			person.setFirstname(odpowiedz.getDanePodmiotu().getImieSkrot());
			person.setLastname(odpowiedz.getDanePodmiotu().getNazwiskoNazwa());
			person.setNip(odpowiedz.getDanePodmiotu().getNip());
			person.setRegon(odpowiedz.getDanePodmiotu().getRegon());
			person.setPesel(odpowiedz.getDanePodmiotu().getPesel());
			person.setAdresSkrytkiEpuap(adresSkrytkiEpuap);
			person.setIdentifikatorEpuap(odpowiedz.getDanePodmiotu().getIdentyfikator());
			person.setZgodaNaWysylke(odpowiedz.getDanePodmiotu().getZgoda());
			person.setCountry("PL");
			person.setLparam("epuap");
			if(person.createIfNew())
				log.info("Utworzono nowy obiekt person id =" + person.getId());
			else log.info("Problem z utworzeniem u�ytkownika");
			
			Sender sender = new Sender();
			sender.fromMap(person.toMap());
			sender.setBasePersonId(person.getId());
			try {
				sender.setDictionaryType(Person.DICTIONARY_SENDER);
			} catch (EdmException e) {
				log.error(e.getMessage());
			}
			sender.create();
			return sender;
		}
	}
	
	private String typUPO(org.dom4j.Element root) {
		org.dom4j.Element element = null;
		element = root.element("TrescDokumentu");
		if (element != null) {
			return null;
		} else {
			String nazwaUpo = "PND";
			/* org.dom4j.Element */element = root.element("UPP"); // czy to potwierdzenie PRZEDLOZENIA ?
			if (element != null) return nazwaUpo = "UPP";
			else {
				element = root.element("UPD"); // czy to potwierdzenie DOSTARCZENIA ?
				if (element != null) return nazwaUpo = "UPD";
			}
			return nazwaUpo;
		}
	}
	/**
	 * Metoda testowa - wyci�ga do pliku c:/holder-new.xml zawarto�� dokumentu z ePUAP
	 * @param inputStream
	 */
	private void tescik(InputStream inputStream){
		OutputStream outputStream = null;
	 
		try {
			// write the inputStream to a FileOutputStream
			outputStream = 
	                    new FileOutputStream(new File("c:/holder-new.xml"));
	 
			int read = 0;
			byte[] bytes = new byte[1024];
	 
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
	 
			System.out.println("Done!");
	 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
	 
			}
		}
	}
	
}
