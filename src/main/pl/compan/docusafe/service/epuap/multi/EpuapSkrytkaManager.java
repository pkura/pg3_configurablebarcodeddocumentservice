package pl.compan.docusafe.service.epuap.multi;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class EpuapSkrytkaManager
{
	private static final Logger log = LoggerFactory.getLogger(EpuapSkrytkaManager.class);
	public static EpuapSkrytka find(Long id) throws EdmException {
		return Finder.find(EpuapSkrytka.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public static synchronized List<EpuapSkrytka> list() throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(EpuapSkrytka.class);
			return (List<EpuapSkrytka>) c.list();
		}catch(Exception e){
			log.error("", e);
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static synchronized List<EpuapSkrytka> list(boolean available) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(EpuapSkrytka.class);
			c.add(Restrictions.eq("available", available));
			return (List<EpuapSkrytka>) c.list();
		}catch(Exception e){
			log.error("", e);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public static synchronized EpuapSkrytka findByAdresSkrytki(String adresSkrytki) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(EpuapSkrytka.class);
			c.add(Restrictions.eq("adresSkrytki", adresSkrytki));
			return (EpuapSkrytka) c.uniqueResult();
		}catch(Exception e){
			log.error("", e);
			return null;
		}
	}
	
	/**
	 * 
	 * @param id
	 * @param param1
	 * @return
	 */
	public static EpuapSkrytka findByIdAndParam1(Long id, String param1)
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(EpuapSkrytka.class);
			c.add(Restrictions.not(Restrictions.eq("id", id)));
			c.add(Restrictions.eq("param1", param1));
			
			return (EpuapSkrytka) c.uniqueResult();
		}catch(Exception e){
			log.error("", e);
			return null;
		}
	}
}
