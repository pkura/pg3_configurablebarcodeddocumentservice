package pl.compan.docusafe.service.epuap.multi;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Entity reprezentuje informacje o skrytece ePUAPowej. 
 * 
 * Wykorzystywane przez serwis EpuapMultiImportService oraz EpuapExportService
 * 
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
@SuppressWarnings("serial")
public class EpuapSkrytka implements Serializable
{
	private static final Logger log = LoggerFactory.getLogger(EpuapSkrytka.class);
	
	private Long id;
	
	/** np. /urzadMK/ds */
	private String adresSkrytki;
	/** np. SKRYTKADS */		
	private String nazwaSkrytki;
	/** np. urzadMK */			
	private String identyfikatorPodmiotu;
	private boolean available = false;
	
	private String certificateFileName;
	private String certificatePassword;
	private String email = null;
	/** Parametr definiowany w zale�no�ci od potrzeb w systemie */
	private String param1;
	/** Parametr definiowany w zale�no�ci od potrzeb w systemie */
	private String param2;
	
	private boolean notification;
	
	public String toString(){
		return nazwaSkrytki +" ("+ adresSkrytki +")";
	}
	
	
	/** wyszukiwanie na podstawie parametru 1 */
	@SuppressWarnings("unchecked")
	public static List<EpuapSkrytka> findByParam1Equality(String parametr) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(EpuapSkrytka.class);
			c.add(Restrictions.eq("param1", parametr));
			return (List<EpuapSkrytka>) c.list();
		} catch(Exception e){
			log.error("", e);
			return null;
		}
	}
	
	//---------------------------

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getAdresSkrytki()
	{
		return adresSkrytki;
	}

	public void setAdresSkrytki(String adresSkrytki)
	{
		this.adresSkrytki = adresSkrytki;
	}

	public String getNazwaSkrytki()
	{
		return nazwaSkrytki;
	}

	public void setNazwaSkrytki(String nazwaSkrytki)
	{
		this.nazwaSkrytki = nazwaSkrytki;
	}

	public String getIdentyfikatorPodmiotu()
	{
		return identyfikatorPodmiotu;
	}

	public void setIdentyfikatorPodmiotu(String identyfikatorPodmiotu)
	{
		this.identyfikatorPodmiotu = identyfikatorPodmiotu;
	}

	public boolean isAvailable()
	{
		return available;
	}
	
	public void setAvailable(boolean available)
	{
		this.available = available;
	}

	/**
	 * Param1 to id systemu dziedzinowego.
	 * @return
	 */
	public String getParam1()
	{
		return param1;
	}

	public void setParam1(String param1)
	{
		this.param1 = param1;
	}

	public String getParam2()
	{
		return param2;
	}

	public void setParam2(String param2)
	{
		this.param2 = param2;
	}


	public String getCertificateFileName()
	{
		return certificateFileName;
	}


	public void setCertificateFileName(String certificateFileName)
	{
		this.certificateFileName = certificateFileName;
	}


	public String getCertificatePassword() {
		return certificatePassword;
	}


	public void setCertificatePassword(String certificatePassword) {
		this.certificatePassword = certificatePassword;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public void setNotification(boolean notification) {
		this.notification = notification;
	}


	public boolean isNotification() {
		return notification;
	}
}
