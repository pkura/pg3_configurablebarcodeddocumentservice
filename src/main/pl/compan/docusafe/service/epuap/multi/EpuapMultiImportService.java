package pl.compan.docusafe.service.epuap.multi;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.axis2.AxisFault;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.events.handlers.DocumentMailHandler;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.epuap.EpuapImportManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullPobierzTyp;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullPotwierdz;
import pl.gov.epuap.ws.pull.PullFaultMsg;

import com.google.common.collect.Maps;

/**
 * Rozbudowana klasa serwisu importu dokument�w z ePUAP,
 * wzgl�dem orygina�u r�ni si� tym, �e
 * zak�ada mo�liwo�c istnienia wielu skrytek r�wnocze�nie
 * i wszystkie one s� przegl�dane. Konfiguracja nie le�y sztywno w adds,
 * lecz trzymana jest w bazie (zarz�dzanie z zak�adki Administracja->Skrytki ePUAP).
 * 
 * Istotne jest, �e po utworzeniu dokumentu uruchamiany jest zwi�zany z nim proces
 * a nie metoda WorkflowFactory.createNewProcess !
 * 
 * TODO : Metoda WorkflowFactory.createNewProcess dodawa�a do dokumentu parametr objective
 * = "do realizacji ePUAP" i p�niej obecno�� tego stringu by�a sprawdzana w innych miejscach.
 * Sprawdzi�, czy to jest potrzebne */
public class EpuapMultiImportService extends ServiceDriver implements Service
{
	private static final Logger log = LoggerFactory.getLogger("EpuapMultiImportService");
	private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(EpuapMultiImportService.class.getPackage().getName(),null);
	private Long schedulePeriod = 2L;
	private Property[] properties;
	
	public EpuapMultiImportService()
	{
		properties = new Property[] { new SchedulePeriodProperty() };
	}

	protected boolean canStop()
	{
		return true;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi EpuapMultiImportService");
		timer = new Timer(true);
		timer.schedule(new Import(), 0, schedulePeriod*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("SystemWystartowal"));
		console(Console.INFO, sm.getString("epuapMultiImportExportServicePeriod", schedulePeriod));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi EpuapMultiImportService");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}	

	/** TODO : do przemyslenia i napisania od nowa struktura lapania wyjatkow tutaj.
	 * Obecnie sa 3 poziomy try/catch!!!
	 * Wazne zeby pojedynczy dokument nie wywalal calego pobierania,
	 * bo przy kolejnym podejsciu dokumenty przychodza w identycznej kolejnosci
	 * wiec pierwszy dokument, ktory wywala pobieranie, uniemozliwia pobranie calej reszty.
	 * Uwzglednic tez to, ze pobieramy z roznych skrytek - kazda jest osobno konfigurowana i tu tez moga byc problemy. */
	class Import extends TimerTask
	{
		@Override
		public void run() 
		{
			console(Console.INFO, "Start");
			try
			{
				if(!DSApi.isContextOpen()) DSApi.openAdmin();
				
				List<EpuapSkrytka> skrytki = EpuapSkrytkaManager.list(true);

				if(skrytki == null || skrytki.isEmpty())
				{
					console(Console.INFO, "Brak zdefiniowanych skrytek ePUAP");
					return;
				}
				DSApi.close();

				boolean clean = true;
				boolean shouldCloseCtx = false;

				for(EpuapSkrytka skrytka : skrytki)
				{
					// obsluga poszczegolnych skrytek, odseparowana przez TRY/CATCH
					try
					{
						EpuapImportManager manager = new EpuapImportManager(skrytka);

						Integer ile = manager.getOczekujace();
						console(Console.INFO, "Odbiera " +ile+ " dokument�w ze skrytki: " + skrytka.getNazwaSkrytki());
						while (ile > 0)
						{
							//pobranie dokumentu
							try
							{
								OdpowiedzPullPobierzTyp odpowiedz = manager.pobierzNastepny();
								try {
									console(Console.INFO, "Pobra� status: "+ odpowiedz.getStatus().getKomunikat());
								}
								catch (Exception e) {  log.error(e.getMessage());  }
								
								//badanie transakcji
								if (DSApi.isContextOpen()) {
									// cos innego trwa (moze poprzednie wywolanie sie nie skonczylo), nie isc dalej.
									clean = true; 				// po to, zeby nie robilo rollback
									shouldCloseCtx = false; 	// i nie zamykalo kontekstu
									console(Console.INFO, "Trwa transakcja, odczekac na zakonczenie");
									break;
								}
								clean = false;
								shouldCloseCtx = true;
								DSApi.openAdmin();
								DSApi.context().begin();
								
								// zapis dokumentu

								// TODO : tutaj okreslic, czy UPO ma byc zapisane jako dokument, czy jako zalacznik.
								// Dla NFOS zapisujemy na razie zawsze jako dokument
								boolean upoJakoDokument = true;
								

								InOfficeDocument document = manager.zapiszDokumentlubUPO(odpowiedz, upoJakoDokument);

								//je�li dokument jest typu upo to
								
								
								if ( manager.isDokument1_upo0() || ((!manager.isDokument1_upo0()) && upoJakoDokument) )
								{
									saveParams(document, skrytka, manager.isDokument1_upo0());
									console(Console.INFO, "Zapisa� pod ID  "+ document.getId());
								}


								//uruchomienie procesu

								Map<String, Object> map = Maps.newHashMap();
								map.put(ASSIGN_USER_PARAM, "admin");
								map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");
								try
								{
									//uruchomienie procesu okre�lonego w dockindzie
									document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
									TaskSnapshot.updateByDocument(document);
								}
								catch (Exception e)
								{
									log.error("B��d przy uruchamianiu procesu: "+e.getMessage(), e);
								}
								
								DSApi.context().commit();
								
								//----------------------------------------------------------------------------------------------
								// obsluga odeslania potwierdzenia odbioru

								DSApi.context().begin(); 
								OdpowiedzPullPotwierdz odpowiedzPotwierdz = manager.potwierdzOdebranie(odpowiedz);
								console(Console.INFO, "Wys�a� potwierdzenie  "+ odpowiedzPotwierdz.getOdpowiedzPullPotwierdz().getStatus().getKomunikat());
								
								TaskSnapshot.updateByDocument(document);
								DSApi.context().commit();
								console(Console.INFO, "Pobra� dokument ze skrytki " + odpowiedz.getAdresSkrytki());
								console(Console.INFO, "");

								//po odebraniu i wys�aniu potwierdzenia wsy�amy powiadomienie mailowe.
								EpuapSkrytka es = document.getDocumentKind().logic().wybierzSkrytkeNadawcyEpuap(document);
								if(es.isNotification()){
									DocumentMailHandler.createEvent(document.getId(), "");
								}
								
								clean = true;			// nie ma robic rollback
								shouldCloseCtx = true;	// ale ma zamykac kontekst
							}
							catch (PullFaultMsg e)
							{
								log.error(e.getFaultMessage().getWyjatek().getKomunikat() +", "+ e.getMessage(), e);
								e.printStackTrace();
								console(Console.ERROR, e.getFaultMessage().getWyjatek().getKomunikat());
							}
							catch (Throwable t)
							{ 
								log.error(t.getMessage(),t);
								console(Console.ERROR, t.getMessage());

								if (t instanceof AxisFault) {

									// TODO : to moze byc blad, z ktorego usluga sie juz nie podniesie i juz nigdy nic nie odbierze,
									// chyba ze AxisClientConfigurator moze powtornie cos zainicjowac
								}
							}
							finally
							{
								ile --;

								if (clean==false)
									DSApi.context().rollback();
								if (shouldCloseCtx == true)
									DSApi.close();
							}
						} // end of while
					}
					catch (Exception e)
					{
						// tu tylko wyjatki z zewnetrznej obudowy,
						// bo w petli odbioru pojedynczych dokumentow jest osobna obsluga
						throw new Exception(e);
					}
				}
			}
			catch (PullFaultMsg e)
			{
				log.error(e.getMessage(),e);
				e.printStackTrace();
				console(Console.ERROR, e.getFaultMessage().getWyjatek().getKomunikat());
			}
			catch (Throwable t)
			{
				log.error(t.getMessage(),t);
				console(Console.ERROR, t.getCause().getMessage());
			}
		}

		/** wywo�uje metod� w logice dokumentu, kt�ra mo�e (specyficznie dla tego dokumentu)
		 *  wype�ni� warto�ci p�l na dokumencie. */
		private void saveParams(InOfficeDocument document, EpuapSkrytka skrytka, boolean dokument1_upo0) throws EdmException {
			document.setDocumentDate(new Date());
			document.getDocumentKind().logic().setDocValuesAfterImportFromEpuap (document, skrytka, dokument1_upo0);
		}
	}
	
	
	class SchedulePeriodProperty extends Property {
		public SchedulePeriodProperty() {
            super(SIMPLE, PERSISTENT, EpuapMultiImportService.this, "schedulePeriod", "Cz�stotliwo�� pobierania (w minutach)", String.class);
        }

        protected Object getValueSpi() {
            return schedulePeriod;
        } 

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (EpuapMultiImportService.this) {
            	if(object != null) {
            		schedulePeriod = Long.parseLong((String) object);
            	} else {
            		schedulePeriod = 2L;
            	}
            }
        }	
	}
	
	public Property[] getProperties() {
		return properties;
	}

	public void setProperties(Property[] properties) {
		this.properties = properties;
	}
}
