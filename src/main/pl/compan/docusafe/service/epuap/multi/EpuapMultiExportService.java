package pl.compan.docusafe.service.epuap.multi;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.handlers.DocumentMailHandler;
import pl.compan.docusafe.parametrization.nfos.NfosFactory;
import pl.compan.docusafe.parametrization.nfos.NfosLogicable;
import pl.compan.docusafe.parametrization.nfos.UpoLink;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.epuap.EpuapExportDocument;
import pl.compan.docusafe.service.epuap.EpuapExportManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.OdpowiedzDoreczyciela;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.OdpowiedzDoreczycielaTyp;
import pl.gov.epuap.ws.skrytka.NadajFaultMsg;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.IdentyfikatorDokumentuTyp;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.OdpowiedzSkrytki;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.StatusTyp;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class EpuapMultiExportService extends ServiceDriver implements Service
{
	private static final Logger log = LoggerFactory.getLogger("EpuapMultiExportService");
	private Timer timer;
	private Long schedulePeriod = 2L;
	private Property[] properties;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(EpuapMultiExportService.class.getPackage().getName(),null);

	public EpuapMultiExportService()
	{
		properties = new Property[] { new SchedulePeriodProperty() };
	}

	protected boolean canStop()
	{
		return true;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi EpuapMultiExportService");
		timer = new Timer(true);
		timer.schedule(new Export(), 0 ,schedulePeriod*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("SystemWystartowal"));
		console(Console.INFO, sm.getString("epuapMultiImportExportServicePeriod", schedulePeriod));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi EpuapMultiExportService");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}
	
	class Export extends TimerTask
	{
		@Override
		public void run() 
		{
			console(Console.INFO, "Start");
            try
            {
            	DSApi.openAdmin();
            	
            	EpuapExportManager manager = null;
				
				List<EpuapExportDocument> exportDocuments = EpuapExportDocument.documentToSend();
				console(Console.INFO, "Do wys�ania "+ (exportDocuments != null ? exportDocuments.size() : "0") );
				for (EpuapExportDocument document : exportDocuments) 
				{
					OutOfficeDocument officeDocument = null;
					try
					{
						manager = new EpuapExportManager(document.getEpuapSkrytka()); // ma byc tak jak jest, ale pamietaj: document.getEpuapSkrytka() oznacza skrytke PRZEZ KTORA sie wysyla a nie adresata!!!!!
	    				DSApi.context().begin();
						officeDocument = OutOfficeDocument.findOutOfficeDocument(document.getDocumentId());
						Recipient recipient = document.getPerson();
						console(Console.INFO, "Wysy�a do  "+recipient.getAdresSkrytkiEpuap()); // a tu jest adresat
						if(EpuapExportDocument.USLUGA_SKRYTKA.equals(document.getRodzajUslugi()))
						{
							wyslijSkrytka(manager, officeDocument, recipient, document);
						}
						else
						{
							wyslijDoreczyciel(manager, officeDocument, recipient, document);
						}
						DSApi.context().commit();
		            }
		            catch (NadajFaultMsg e)
		            {
		            	document.setStatus(EpuapExportDocument.STATUS_BLAD_DANYCH);
						document.setOdpowiedz(e.getFaultMessage().getWyjatek().getKomunikat());
						log.error(e.getMessage(),e);
						console(Console.ERROR, e.getFaultMessage().getWyjatek().getKomunikat());
						sendStatus(officeDocument, e.getFaultMessage().getWyjatek().getKomunikat(), document);
						DSApi.context().commit();
					}
		            catch (RuntimeException e)
		            {
		            	document.setStatus(EpuapExportDocument.STATUS_BLAD_DANYCH);
						document.setOdpowiedz(e.getMessage());
						log.error(e.getMessage(),e);
						console(Console.ERROR, e.getMessage());
						sendStatus(officeDocument, e.getMessage(), document);
						DSApi.context().commit();
					}
		            catch (Throwable t)
		            {
		            	document.setStatus(EpuapExportDocument.STATUS_BLAD_KRYTYCZNY);
						document.setOdpowiedz(t.getMessage());
						log.error(t.getMessage(),t);
						console(Console.ERROR, t.getMessage());
						sendStatus(officeDocument, t.getMessage(), document);
						DSApi.context().commit();
		            }finally{
		            	manager = null;
		            }
				}
            }
            catch (Throwable t)
            {
            	log.error(t.getMessage(),t);
            	console(Console.ERROR, t.getMessage());
            }
            finally
            {
            	DSApi._close();
            }
		}

		/**
		 * Wysy�a status
		 * @param officeDocument
		 * @param komunikat
		 */
		private void sendStatus(OutOfficeDocument officeDocument, String komunikat, EpuapExportDocument document)
		{
			if(officeDocument == null)
			{
				log.info("brak dokumentu");
			}
			
			officeDocument.getDocumentKind().logic().sendEpuapExportStatus(officeDocument, komunikat, document);
				
		}
	}
	
	/** wyslanie w trybie doreczenia */
	private void wyslijDoreczyciel(
			EpuapExportManager manager,
			OutOfficeDocument officeDocument,
			Recipient recipient,
			EpuapExportDocument document)
			throws NadajFaultMsg, DocumentException, IOException, EdmException, Exception
	{
		OdpowiedzDoreczyciela odp = manager.wyslijDoreczyciel(recipient.getAdresSkrytkiEpuap(),officeDocument);
		OdpowiedzDoreczycielaTyp odpTyp = odp.getOdpowiedzDoreczyciela();
		System.out.println("/- UPD epuapID: " +odpTyp.getIdentyfikatorDokumentu().getIdentyfikatorDokumentuTyp());
		System.out.println("\\_ doc epuapID: " +odpTyp.getIdentyfikatorDokumentu());
		
		PkDoreczycielServiceStub.StatusTyp status = odpTyp.getStatus();
		System.out.println(status.getKomunikat());
		
		if(status != null)
		{
			console(Console.INFO, "STATUS : "+status.getKomunikat()+" ["+status.getKod()+"]");
			document.setOdpowiedz(status.getKomunikat());
		}
		Date dataWyslania = new Date();
		document.setStatus(EpuapExportDocument.STATUS_POPRAWNIE_WYSLANY);
		document.setDataNadania(dataWyslania);
		
		// wpisac do samego dokumentu poprawne wyslanie i date
		saveEpuapDetails(
				officeDocument,
				odpTyp.getIdentyfikatorDokumentu().getIdentyfikatorDokumentuTyp(),
				dataWyslania);
	}
	
	/** wyslanie w trybie przedlozenia */
	private void wyslijSkrytka(
			EpuapExportManager manager,
			OutOfficeDocument officeDocument,
			Recipient recipient,
			EpuapExportDocument document)
			throws NadajFaultMsg, DocumentException, IOException, EdmException, Exception
	{
		OdpowiedzSkrytki odp = manager.wyslij(
				officeDocument.getDescription(),
				(officeDocument.getDocumentDate() != null  ?  DateUtils.formatJsDate(officeDocument.getDocumentDate())  :  ""),
				recipient.getAdresSkrytkiEpuap(),
				officeDocument.getAttachments(),
				officeDocument);
		
		
		// TODO: tutaj tez odczytywac bledy wysylania
		
		/* StatusTyp � jest to typ wykorzystywany jako odpowiedz� w przypadku prawid�owego zako�czenia operacji (w przypadku b��du zawsze rzucany jest WyjatekTyp jako fault). Obiekt zawiera nast�pujace sk�adniki:
			-	Kod � numeryczny kod statusu zako�czenia operacji, jego warto�s� ma znaczenie tylko w przypadku konkretnego uzgodnienia pomi�dzy komunikujacymi si� systemami
			-	Komunikat � tekstowy komunikat przeznaczony dla u�ytkownika, zawierajacy dodatkowe informacje (opis przebiegu, spos�b przyj�cia, informacj� o duplikacie, itp.)
	
		WyjatekTyp � w przypadku niepowodzenia operacji us�uga zwraca status fault tego typu, zawierajacy nast�pujace sk�adniki
			-	Kod � numeryczny kod b��du, warto�s� bez znaczenia dla podsystemu komunikacyjnego, s�u�y jedynie u�atwieniu szukania ewentualnych b��d�w
			-	Komunikat � tekstowy komunikat przeznaczony dla u�ytkownika, zawierajacy opis i przyczyn� wyst�pienia b��du */
		
		
		IdentyfikatorDokumentuTyp upp = odp.getOdpowiedzSkrytki().getIdentyfikatorUpp();
		if(upp != null)
		{
			Map<String, Object> values = new HashMap<String, Object>();
			Date dataDoreczenia = pobierzDateDoreczenia(odp);
			values.put(NfosLogicable.DATA_ODBIORU_CN, dataDoreczenia);
			
			officeDocument.getDocumentKind().setOnly(officeDocument.getId(), values);
			System.out.println("/- UPP epuapID: " +upp.getIdentyfikatorDokumentuTyp());
		}
		IdentyfikatorDokumentuTyp doc = odp.getOdpowiedzSkrytki().getIdentyfikatorDokumentu();
		if(doc != null)
		{
			System.out.println("\\_ doc epuapID: " +doc.getIdentyfikatorDokumentuTyp());
		}
		StatusTyp status = odp.getOdpowiedzSkrytki().getStatus();
		if(status != null)
		{
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(NfosLogicable.DATA_ODBIORU_CN, new Date());
			officeDocument.getDocumentKind().setOnly(officeDocument.getId(), values);
			
			console(Console.INFO, "STATUS : "+status.getKomunikat()+" ["+status.getKod()+"]");
			document.setOdpowiedz(status.getKomunikat());
		}
		Date dataWyslania = new Date();
		document.setStatus(EpuapExportDocument.STATUS_POPRAWNIE_WYSLANY);
		document.setDataNadania(dataWyslania);

		
		// wpisac do dokumentu poprawne wyslanie i date oraz epuapID
		saveEpuapDetails(officeDocument, doc.getIdentyfikatorDokumentuTyp(), dataWyslania);
		
		
		StatusTyp statusOdbiorcy = odp.getOdpowiedzSkrytki().getStatusOdbiorcy();
		if(statusOdbiorcy != null)
		{
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(NfosLogicable.DATA_ODBIORU_CN, new Date());
			officeDocument.getDocumentKind().setOnly(officeDocument.getId(), values);
			console(Console.INFO, "STATUS ODBIORCY : "+statusOdbiorcy.getKomunikat()+" ["+statusOdbiorcy.getKod()+"]");
			
		}
		
		/* TODO : UPO (o ile w ogole nadeszlo, bo zamiast niego moze przyjsc sam status - zaleznie od konfiguracji skrytki)
		 * powinno byc zapisywane do dokumentu lub do zalacznika - zaleznie od ustawien. Przerobic, bo teraz zawsze robi zalacznik */
		
		/* Zapisuje zalacznik (UPO typu UPP, o ile przyszlo - patrz todo powyzej) */
		if(odp.getOdpowiedzSkrytki().getZalacznik() != null && odp.getOdpowiedzSkrytki().getZalacznik().getZawartosc() != null)
		{
			console(Console.INFO, "Odbieram dokument "+odp.getOdpowiedzSkrytki().getZalacznik().getNazwaPliku() );
			Attachment attachment = new Attachment(odp.getOdpowiedzSkrytki().getZalacznik().getNazwaPliku());

			InOfficeDocument dokumentUPP = createUPPDocument(odp, recipient);
			EpuapSkrytka epuapSkrytka = EpuapSkrytkaManager.findByAdresSkrytki(recipient.getAdresSkrytkiEpuap());
			afterCreate(odp, dokumentUPP.getId(), epuapSkrytka);
			
			
			dokumentUPP.createAttachment(attachment);
			Integer lenght = FileUtils.lenghtFormInputStream(odp.getOdpowiedzSkrytki().getZalacznik().getZawartosc().getInputStream());
			attachment.createRevision(odp.getOdpowiedzSkrytki().getZalacznik().getZawartosc().getInputStream(), lenght,odp.getOdpowiedzSkrytki().getZalacznik().getNazwaPliku());
			DSApi.context().session().saveOrUpdate(dokumentUPP);
			//FIXME sprawdzenie czy uzytkownik chce otrzymywac powiadomienia
			// teraz robie tak, �e je�li uzytkowni nie ma podanego maila to nie che powiadomien
			Long docid = UpoLink.getUpoDocumentId(dokumentUPP.getId());
			try {
				DSUser user = DSUser.findByUsername(Document.find(docid).getAuthor());
				if(user.getEmail() != null || !(user.getEmail().isEmpty())){
					DocumentMailHandler.createEvent(dokumentUPP.getId(), "");
				}
			} catch (Exception e) {
				log.error("Brak uzytkownika", e);
			}
			
//			officeDocument.createAttachment(attachment);
		}
	}
	
	private void afterCreate(OdpowiedzSkrytki odpowiedz, Long id, EpuapSkrytka epuapSkrytka) {
		try {
			Document nfosDocIn = Document.find(id);
			Long epuapUpoID = Long.parseLong(odpowiedz.getOdpowiedzSkrytki().getIdentyfikatorUpp().getIdentyfikatorDokumentuTyp());
			Long epuapID = Long.parseLong(odpowiedz.getOdpowiedzSkrytki().getIdentyfikatorDokumentu().getIdentyfikatorDokumentuTyp());
			FieldsManager fm = nfosDocIn.getDocumentKind().getFieldsManager(id);
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(NfosLogicable.DOC_CZY_UPO_CN, NfosLogicable.UPO);
			values.put(NfosLogicable.EPUAP_ID_CN, epuapUpoID);
			values.put(NfosLogicable.STATUS_CN, NfosLogicable.BIEZACY);
			values.put("DZIAL", DSDivision.find(DSDivision.ROOT_GUID).getId());
			values.put(NfosLogicable.SYSTEM_CN, epuapSkrytka.getId());
			values.put("DOC_DATE", new Date());
			nfosDocIn.getDocumentKind().setOnly(id, values);
			Document doc = NfosFactory.getDocumentByEpuapId(epuapID);
			nfosDocIn.getDocumentKind().logic().dodajLinkDoUPO(nfosDocIn, doc, "UPP", null);
		} catch (DocumentNotFoundException e) {
			log.error("Dokument nie znaleziony", e.getMessage());
		} catch (DocumentLockedException e) {
			log.error("Dokument zablokowany", e.getMessage());
		} catch (AccessDeniedException e) {
			log.info("Odmowa dost�pu", e.getMessage());
		} catch (EdmException e) {
			log.error(e.getMessage());
		} catch (Exception e){
			log.debug("B��d podczas wiazania dokument�w", e.getMessage());
		}
//		nfosDocIn.getDocumentKind().logic().dodajLinkDoUPO(upoDoc, doc, nazwaUpo, upo);
	}

	/**
	 * Pobiera date dor�czenia z UPP.xml znajduj�cy si� w @param odp, przy za�o�eniu,
	 * �e format daty to "yyyy-MM-dd'T'HH:mm:ss.SSS" i struktura xml to:
	 * <document><UPP><DataDoreczenia>
	 * 
	 * @param odp
	 * @return 
	 * @throws Exception
	 */
	private Date pobierzDateDoreczenia(OdpowiedzSkrytki odp)
			throws Exception {
		SAXReader reader = new SAXReader();
		InputStream is = odp.getOdpowiedzSkrytki().getZalacznik().getZawartosc().getInputStream();
		File file = FileUtils.fileFromBlobInputStream(is);
		org.dom4j.Document document = reader.read(file);
		Element el = document.getRootElement();
		
		String dataDoreczenia = el.element("UPP").element("DataDoreczenia").getStringValue();
		SimpleDateFormat parserSDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		return parserSDF.parse(dataDoreczenia);
	}

	/** zapisuje przydzielone epuapID w dokumencie wyslanym
	 * poprzez wywolanie odpowiedniej metody w logice,
	 * (domyslnie nic nie robi) */
	private void saveEpuapDetails(OutOfficeDocument doc, String idstr, Date dataWyslania)
	{
		Long id = Long.parseLong(idstr);
		if (id!=null)
			doc.getDocumentKind().logic().saveEpuapDetails(doc, id, dataWyslania);
	}
	
	/** odczytuje z XMLa UPO typ tego UPO (UPP/UPD) oraz jego ID.
	 * Jest to potrzebne do stworzenia tytu�u.
	 * Zwraca tablice stringow 2-elementowa:
	 * [0] - typ
	 * [1] - id (zawierajace tez typ */
	private String[] getUpoIdEtType(org.dom4j.Element root){
		
		String[] ret = new String[]{"PND","bez numeru"};
		
		org.dom4j.Element element = root.element("UPP"); // czy to potwierdzenie PRZEDLOZENIA ?
		if (element!=null)
			ret[0] = "UPP";
		else {
			element = root.element("UPD"); // czy to potwierdzenie DOSTARCZENIA ?
			if (element!=null)
				ret[0] = "UPD";
		}
		if (element != null)
			ret[1] = element.element("IdentyfikatorPoswiadczenia").getText();
		
		return ret;
	}
	
	/**
	 * @param odpowiedz, jak� dostajemy podczas wysy�ania pisama za przed�u�eniem
	 * @param recipient, ktr�ry zosta� wykorzystany podczas wysy�ania pisma, w ciele funkji
	 * bedzie zamieniany na nadawce UPP.
	 * @return
	 * @throws EdmException
	 * @throws IOException
	 * @throws DocumentException
	 */
	private InOfficeDocument createUPPDocument(
			OdpowiedzSkrytki odpowiedz,
			Recipient recipient
	) throws EdmException, IOException, DocumentException {
		
		Long epuapId =  Long.parseLong(odpowiedz.getOdpowiedzSkrytki().getIdentyfikatorDokumentu().getIdentyfikatorDokumentuTyp());
		
		SAXReader reader = new SAXReader();
		InputStream is = odpowiedz.getOdpowiedzSkrytki().getZalacznik().getZawartosc().getInputStream();
		File file = FileUtils.fileFromBlobInputStream(is);
		org.dom4j.Document document = reader.read(file);
		Element root = document.getRootElement();
		
		String[] idEtType = getUpoIdEtType(root);
		
		InOfficeDocument doc = new InOfficeDocument();
		DocumentKind documentKind = DocumentKind.findByCn(DocumentKind.NORMAL_KIND);
		doc.setDocumentKind(documentKind);
		doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
		doc.setDivisionGuid(DSDivision.ROOT_GUID);
		doc.setCurrentAssignmentAccepted(Boolean.FALSE);
		doc.setCreatingUser(DSApi.context().getPrincipalName());
		doc.setSummary("UPO - Po�wiadczenie " +idEtType[1]+ " do dokumentu o ID " +epuapId +" w ePUAP"); //+ " "+odpowiedz.getDokument().getNazwaPliku());
		doc.setForceArchivePermissions(false);
		doc.setAssignedDivision(DSDivision.ROOT_GUID);
		doc.setClerk(DSApi.context().getPrincipalName());
		doc.setSource("epuap");
		doc.setDelivery(InOfficeDocumentDelivery.findByName(InOfficeDocumentDelivery.DELIVERY_EPUAP)); // w bazie musi istnie� taki wpis
		doc.setKind(InOfficeDocumentKind.getInOfficeDocumentKind(documentKind));
		Calendar currentDay = Calendar.getInstance();
		currentDay.setTime(GlobalPreferences.getCurrentDay());
		doc.setIncomingDate(currentDay.getTime());
		doc.setOriginal(true);
		doc.create();
		Sender sender = senderFromRecipient(recipient); // Z funkcji wyslijSkrytka odbiorca jest nadawc� dla odpowiedzi.
		doc.setSender(sender);
		doc.setDocumentKind(documentKind);
		documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
		documentKind.logic().documentPermissions(doc);
		return doc;
	}
	
	/**
	 * Z odbiorcy tworzy nadawc�. Ten sam basePersonID.
	 * 
	 * @param recipient 
	 * @return Nadawca przypisany do person o basePersonId z reciepienta
	 * @throws EdmException
	 */
	private Sender senderFromRecipient(Recipient recipient) throws EdmException{
		Person person = Person.findIfExist(recipient.getBasePersonId());
		Sender sender = new Sender();
		sender.fromMap(recipient.toMap());
		sender.setBasePersonId(person.getId());
		sender.create();
		sender.setDictionaryType(Person.DICTIONARY_SENDER);
		sender.update();
		return sender;
	}
	
//	/**
//	 * Szuka dokumentu wychodz�cego o id epuapu pobranego z odpowiedzi skrytki
//	 * nast�pnie wyciaga nadawc� dokumentu.
//	 */
//	private Sender getSenderFromDocument(OdpowiedzSkrytki odpowiedz){
//		Long epuapId = Long.parseLong(odpowiedz.getOdpowiedzSkrytki().getIdentyfikatorDokumentu().getIdentyfikatorDokumentuTyp());
//		List<Document> listaDokumentow = NfosFactory.getDocumentsByEpuapId(epuapId);
//		if(listaDokumentow.size() > 0 && listaDokumentow != null){
//			if(listaDokumentow.get(0) instanceof OutOfficeDocument){
//				(OutOfficeDocument)listaDokumentow.get(0).
//				
//			}
//		}
//		
//		return null;
//	}
	
	class SchedulePeriodProperty extends Property {
		public SchedulePeriodProperty() {
            super(SIMPLE, PERSISTENT, EpuapMultiExportService.this, "schedulePeriod", "Cz�stotliwo�� wysy�ania (w minutach)", String.class);
        }

        protected Object getValueSpi() {
            return schedulePeriod;
        } 

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (EpuapMultiExportService.this) {
            	if(object != null) {
            		schedulePeriod = Long.parseLong((String) object);
            	} else {
            		schedulePeriod = 2L;
            	}
            }
        }	
	}
	
	public Property[] getProperties() {
		return properties;
	}

	public void setProperties(Property[] properties) {
		this.properties = properties;
	}
}
