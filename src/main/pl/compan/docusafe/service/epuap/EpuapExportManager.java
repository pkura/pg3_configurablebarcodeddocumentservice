package pl.compan.docusafe.service.epuap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.IOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.axis2.AxisFault;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.certificates.ElectronicSignature;
import pl.compan.docusafe.core.certificates.ElectronicSignatureBean;
import pl.compan.docusafe.core.certificates.ElectronicSignatureStore;
import pl.compan.docusafe.core.certificates.SzyfrowanieSignatureEpuap;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytka;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorDokumentu;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorDokumentuTyp;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.OdpowiedzDoreczyciela;
import pl.gov.epuap.ws.skrytka.NadajFaultMsg;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.AdresOdpowiedzi;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.AdresSkrytki;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.CzyProbne;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.DaneDodatkowe;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.Dokument;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.DokumentTyp;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.IdentyfikatorPodmiotu;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.IdentyfikatorPodmiotuTyp;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.OdpowiedzSkrytki;

/**
 * Klasa EpuapManager.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class EpuapExportManager 
{
	private static final Logger log  = LoggerFactory.getLogger(EpuapExportManager.class);
	private AdresOdpowiedzi adresOdpowiedzi;
	private pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.AdresOdpowiedzi adresOdpowiedziDoreczyciel;
	private IdentyfikatorPodmiotu podmiot;
	private pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorPodmiotu podmiotDoreczyciel;
	private PkSkrytkaServiceStub serviceSkrytka;
	private PkDoreczycielServiceStub serviceDoreczyciel;
	
	/**
	 * Metoda zapisuje dokument do kolejki dokument�w do wysy�ki.
	 * Jes�li dokument ma wielu odbiorc�w tworzone sa wpisy dla kazdego.
	 * <br/>
	 * <b>Doreczyciel</b> - wymaga posiadania (przez petenta) podpisu kwalifikowanego
	 * w celu podpisania odpowiedzi. Je�li odpowiedz od petenta dojdzie, jest to r�wnoznaczne z dokumentem papierowym. 
	 * <br/>
	 * <b>Skrytka</b> - nie wymaga podpisu od petenta ale nie ma mocy prawnej,
	 * za pomoca skrytki moga by� wysy�ane dokumenty pomiedzy urz�dami.
	 * 
	 * @param rodzajUslugi = sposob nadania dokumentu: "Skrytka" (przedlozenie) lub "Doreczyciel" (doreczenie).
	 * */
    public static void sendToEpuap(Long documentId, boolean checkKO,Integer rodzajUslugi)throws EdmException
    {
		OutOfficeDocument document = null;
		try
		{
			document = OutOfficeDocument.findOutOfficeDocument(documentId);

			if(document.getDelivery() != null && OutOfficeDocumentDelivery.DELIVERY_EPUAP.equalsIgnoreCase(document.getDelivery().getName()))
			{
				if(document.getOfficeNumber() == null && checkKO)
					throw new EdmException("Aby wys�a� dokument do ePUAP nale�y najpierw nada� numer KO");
				if(EpuapExportDocument.isSent(documentId))
					throw new EdmException("Dokument zosta� ju� wys�any");
				
				
				for (Recipient recipient : document.getRecipients())
				{
					EpuapExportDocument export = EpuapExportDocument.findByRecipient(documentId,recipient);
					if(export == null)
						export = new EpuapExportDocument();
					export.setDocumentId(documentId);
					export.setPerson(recipient);
					export.setRodzajUslugi(rodzajUslugi);
					export.setStatus(EpuapExportDocument.STATUS_DO_WYSYLKI);
					export.setDataNadania( new java.util.Date() ); // traktujemy jako data wstawienia do kolejki

					
					// uzupelniamy skrytke nadawcy (a nie adresata - 
					// traktowac to jako skrytka PRZEZ KT�RA sie wysyla).
					// Adresat jest wziety z DSO_Person (recipient)
					EpuapSkrytka skrytkaNadawcy = document.getDocumentKind().logic().wybierzSkrytkeNadawcyEpuap(document);

					
					if (skrytkaNadawcy==null)
					{ 
					/*Property[] pop = ServiceManager.getService("EpuapExportService").getProperties();
					
					
					for (Property property : pop)
					{
						if (property.getName().equals("adresSkrytki"))
							
							skrytkaNadawcy.setAdresSkrytki((String)property.getValue());
					
						
					}
					
					
						// TODO : jesli NULL, wstawic domyslna! Okreslac ja moze na podstawie adds tak, jak zawsze sie robilo wczesniej
						*/
					}
						if (skrytkaNadawcy!=null)
							export.setEpuapSkrytka(skrytkaNadawcy);
					
					export.create();
				
			}
			}
		}
		
		catch (DocumentNotFoundException e) 
		{
			log.error(e.getMessage(),e);
		}
    }
    
    /** Tworzy uniwersalny manager wysylania - pozniej przy wywolaniu
     * moze uzywac doreczyciela albo nadania na skrytke.
	 * Parametr wywolania to skrytka NADAWCY a nie ODBIORCY
	 * czyli PRZEZ JAKA skrytke sie wysyla a nie DO jakiej. */
	public EpuapExportManager(EpuapSkrytka skrytka)  throws AxisFault, Exception 
	{
		this(skrytka.getAdresSkrytki(),	skrytka.getNazwaSkrytki(),skrytka.getIdentyfikatorPodmiotu());
	}
	
	public EpuapExportManager(String adresSkrytki, String nazwaSkrytki, String identyfikatorPodmiotu) throws AxisFault, Exception 
	{
		this(adresSkrytki, nazwaSkrytki, identyfikatorPodmiotu, null,null);
	}
	/** Tworzy uniwersalny manager wysylania - pozniej przy wywolaniu
     * moze uzywac doreczyciela albo nadania na skrytke.
	 * Parametry wywolania dotycza NADAWCY a nie ODBIORCY
	 * czyli przez jaka skrytke sie wysyla a nie do jakiej. */
	public EpuapExportManager(String adresSkrytki, String nazwaSkrytki, String identyfikatorPodmiotu,String individualCertificateFile, String certificatePassword) throws AxisFault, Exception 
	{
		super();
		this.adresOdpowiedziDoreczyciel = new pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.AdresOdpowiedzi();
		adresOdpowiedziDoreczyciel.setAdresOdpowiedzi(adresSkrytki);
		pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorPodmiotuTyp typD = new pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorPodmiotuTyp();
		typD.setIdentyfikatorPodmiotuTyp(identyfikatorPodmiotu);
		this.podmiotDoreczyciel = new pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorPodmiotu();
		podmiotDoreczyciel.setIdentyfikatorPodmiotu(typD);
		
		this.adresOdpowiedzi = new AdresOdpowiedzi();
		adresOdpowiedzi.setAdresOdpowiedzi(adresSkrytki);
		IdentyfikatorPodmiotuTyp typ = new IdentyfikatorPodmiotuTyp();
		typ.setIdentyfikatorPodmiotuTyp(identyfikatorPodmiotu);
		this.podmiot = new IdentyfikatorPodmiotu();
		podmiot.setIdentyfikatorPodmiotu(typ);
		
		AxisClientConfigurator conf = new AxisClientConfigurator(Docusafe.getAdditionProperty("epuap-axis-properties-prefix"));

		if(individualCertificateFile != null && !individualCertificateFile.isEmpty())
			conf = new AxisClientConfigurator(Docusafe.getAdditionProperty("epuap-axis-configurator-prefix"), individualCertificateFile, certificatePassword);
		
		this.serviceSkrytka = new PkSkrytkaServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(serviceSkrytka, "/pk_external_ws/services/skrytka");
		
		this.serviceDoreczyciel = new PkDoreczycielServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(serviceDoreczyciel, "/pk_external_ws/services/doreczyciel");
	}

	public OdpowiedzSkrytki wyslij(String tresc,String data,String adresSkrytki, List<Attachment> attachments,OutOfficeDocument officeDocument) throws NadajFaultMsg, DocumentException, IOException, Exception 
	{
		Dokument doc = new Dokument();
		DokumentTyp dokumentTyp = new DokumentTyp();
		dokumentTyp.setNazwaPliku("odpowiedz.xml");
		dokumentTyp.setTypPliku("application/octet-stream");
		DataHandler dh = new DataHandler(new ByteArrayDataSource(createOdpowiedz(tresc,data,attachments).getBytes("UTF-8")));
		dokumentTyp.setZawartosc(dh);
		doc.setDokument(dokumentTyp);
		AdresSkrytki adresSkrytkiDo = new AdresSkrytki();
		adresSkrytkiDo.setAdresSkrytki(adresSkrytki);
		CzyProbne czyProbne = new CzyProbne();
		czyProbne.setCzyProbne(false);
		DaneDodatkowe daneDodatkowe = new DaneDodatkowe();
		daneDodatkowe.setDaneDodatkowe(new DataHandler(new FileDataSource(Docusafe.getHome().getAbsolutePath()+"/dane.xml")));
		return serviceSkrytka.nadaj(doc, podmiot, adresSkrytkiDo, adresOdpowiedzi, czyProbne, daneDodatkowe);
	}
	
	public OdpowiedzDoreczyciela wyslijDoreczyciel(String adresSkrytki,OutOfficeDocument officeDocument) throws NadajFaultMsg, DocumentException, IOException, Exception 
	{
		pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.Dokument doc = new pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.Dokument();
		pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.DokumentTyp dokumentTyp = new pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.DokumentTyp();
		pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.AdresSkrytki adresSkrytkiDo = new pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.AdresSkrytki();
		pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.CzyProbne czyProbne = new pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.CzyProbne();
		pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.TerminDoreczenia terminDoreczenia = new pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.TerminDoreczenia();
		pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorSprawy identyfikatorSprawy = new pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorSprawy();
		pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.DaneDodatkowe daneDodatkowe = new pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.DaneDodatkowe();
		dokumentTyp.setNazwaPliku("odpowiedz.xml");
		dokumentTyp.setTypPliku("application/octet-stream");	
		DataHandler dh = createOdpowiedzDoreczyciel(officeDocument);
		dokumentTyp.setZawartosc(dh);
		doc.setDokument(dokumentTyp);
		IdentyfikatorDokumentuTyp idDoctyp = new IdentyfikatorDokumentuTyp();
		idDoctyp.setIdentyfikatorDokumentuTyp(String.valueOf(officeDocument.getId()));
		IdentyfikatorDokumentu idDoc = new IdentyfikatorDokumentu();
		idDoc.setIdentyfikatorDokumentu(idDoctyp);
		adresSkrytkiDo.setAdresSkrytki(adresSkrytki);
		czyProbne.setCzyProbne(false);
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, 14);
		terminDoreczenia.setTerminDoreczenia(c);
		identyfikatorSprawy.setIdentyfikatorSprawy(officeDocument.getContainingCase() != null ? officeDocument.getContainingCase().getOfficeId() : "Brak");
		daneDodatkowe.setDaneDodatkowe(new DataHandler(new FileDataSource(Docusafe.getHome().getAbsolutePath()+"/dane.xml")));
		return serviceDoreczyciel.dorecz(doc, podmiotDoreczyciel, adresSkrytkiDo, adresOdpowiedziDoreczyciel, terminDoreczenia, czyProbne, idDoc,identyfikatorSprawy, daneDodatkowe, null);
	}
	
	
	private DataHandler createOdpowiedzDoreczyciel(OutOfficeDocument officeDocument) throws Exception
	{
		// pobierz podpisany dokument wraz z podpisem z bazy
		ElectronicSignatureStore signs = ElectronicSignatureStore.getInstance();
		List<ElectronicSignatureBean> signatures = signs.findSignaturesForDocument(officeDocument.getId());
		if( signatures.size()==0 )
			throw new Exception("Dokument musi by� podpisany");
		
		// pobranie podpisanego dokumentu z podpisem z bazy oracle 
		String signStr = "";
		ElectronicSignature docSign = new ElectronicSignature();
		// pobranie podpisanego zaszyfrowanego dokumentu z podpisem na bazie firebird
		if (DSApi.isFirebirdServer()){
			long  idSignatures = signatures.get(signatures.size()-1).getId();
			long  idDocument = officeDocument.getId();
			  signStr = getBinaryStream(officeDocument, idSignatures, idDocument);
		}else {
		 docSign = signs.get(signatures.get(signatures.size()-1).getId());
		 signStr = docSign.getSignatureFile();
		}
		String exportStr = "";
		
		//PrintWriter zapiss = new PrintWriter("E:/aEPUAP/signStrPoPidpisie.xml");
	   //   zapiss.println(signStr);
	    //  zapiss.close();
		//int posPoczatek = signStr.lastIndexOf("<str:DaneZalacznika>");
	//	int posKoncowa =  signStr.indexOf("</str:DaneZalacznika>"); //-1
//		
//	      int posPoczatek = signStr.lastIndexOf("<wnio:Zalaczniki>");
//			int posKoncowa =  signStr.indexOf("</wnio:Zalaczniki>"); 
//			//wydobycie  tre�ci za�acznika 
//			String zalacznik ="";
//			if (posPoczatek < 0  || posKoncowa < 0){
//				posPoczatek = 0;
//				posKoncowa = 2;
//			 zalacznik = signStr.substring(posPoczatek, posKoncowa);
//			
//			}else{
//				 zalacznik = signStr.substring(posPoczatek, posKoncowa);
//			}
//		 zalacznik = signStr.substring(posPoczatek, posKoncowa);
//		//String text = "";
//		
//		PrintWriter zapis = new PrintWriter("E:/aEPUAP/zalacznikPoPidpisie.xml");
//	    zapis.println(zalacznik);
//	   zapis.close();
//		
		
		//usuwanie pustych lini 
		Scanner sc = new Scanner(signStr);
		int i = 0;
		String line = "";
		while (sc.hasNext()) {
			line = sc.nextLine();
			if (!line.isEmpty())
				exportStr += line + "\n";
		}
	
	
		//sprawdzenie prawid�owej struktury dokumentu z podpisem xml 
		if (exportStr.indexOf("</ds:Object></ds:Signature></wnio:Dokument>") < 0 ) {
			// czy zawiera pe�ny tag 
			if (exportStr.indexOf("</xades:QualifyingProperties>")< 0) {
				if (exportStr.indexOf("</xades:QualifyingProperties")> 0 )
				{
					exportStr += "></ds:Object></ds:Signature></wnio:Dokument>";
				}
			 else
				exportStr += "</ds:Object></ds:Signature></wnio:Dokument>";
		} else if (exportStr.indexOf("</ds:Object></ds:Signature>")< 0 && exportStr.indexOf("</wnio:Dokument>")>0) {
			int pos = exportStr.indexOf("</wnio:Dokument>");
			exportStr = exportStr.substring(0, pos )+ "</ds:Object></ds:Signature>" + exportStr.substring(pos);
		}
	}
		// zwraca wartos� podpisanego pliku z podpisem jako byte

		return new DataHandler(new ByteArrayDataSource(exportStr.getBytes("UTF-8")));

		//int posDigest= signStr.indexOf("<ds:DigestMethod");
		//String transformStr= "<Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\"><XPath xmlns:dsig=\"&dsig;\">not(ancestor-or-self::dsig:Signature)</XPath></Transform></Transforms>";
		//signStr= signStr.substring(0, posDigest) + transformStr + signStr.substring(posDigest);
		
		
	/*	pos1= signStr.indexOf("<ds:Reference");
		pos2= signStr.indexOf("<ds:DigestMethod");
		String transformStr= "<ds:Reference URI=\"\"><ds:Transforms><ds:Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></ds:Transforms>";
		signStr= signStr.substring(0,pos1)+transformStr+signStr.substring(pos2);
	*/	
		
		
	//	pos1 = signStr.indexOf(" URI=\"#SignedProperties-1\"");
	//	signStr = signStr.substring(0, pos1)+ signStr.substring( pos1+(new String(" URI=\"#SignedProperties-1\"")).length());

		
		// sygnature podpinamy pod element wnio:Dokument, wewnatrz niego, na koncu - tzn tuz przed zamknieciem taga
		// int pos = xmlStr.lastIndexOf("</wnio:Dokument");
		
		//int pos= xmlStr.lastIndexOf("</");
	//	xmlStr= xmlStr.substring(0, pos) + signStr.substring(signStr.indexOf("<ds:Signature")) + xmlStr.substring(pos);
		
	/*	String xmlStr2 = "";
	      Scanner odczyt = new Scanner(new File("D:/sygnaturka2.xml"), "UTF-8");
		    while (odczyt.hasNext())
		    {
		    	//xmlStr2+= odczyt.nextLine()+"\n";
		    	xmlStr2+= odczyt.nextLine()+"\n";
		    }*/
		    
	}

	/** mylna nazwa metody. To nie jest zadna odpowiedz. Tutaj wysylamy nasze pismo do adresata w ePUAP. Pismo to moze byc odpowiedzia na jakies inne pismo, ale wcale nie musi. plik 'odpowiedz.xml' jest naszym szablonem wysylania pism */
	public static String createOdpowiedz(String tresc,String data, List<Attachment> attachments) throws DocumentException, IOException, Exception
	{
		SAXReader reader = new SAXReader();
		org.dom4j.Document xmlDocument = reader.read(new FileInputStream(Docusafe.getHome().getAbsolutePath()+"/odpowiedz.xml"));
		Element root = xmlDocument.getRootElement();// /**/.element("Dokument");/**/
		if(!StringUtils.isEmpty(data))
		{
			Element dateElement = root.element("OpisDokumentu").element("Data").element("Czas");
			dateElement.addText(data);
		}
		Element wartosc = root.element("TrescDokumentu").element("Wartosc");
		wartosc.element("TrescWniosku").element("ZawartoscTresci").addText(tresc.trim());
		Element zalaczniki = wartosc.element("Zalaczniki");
		for (Attachment attachment : attachments)
		{
			Element zalacznik = zalaczniki.addElement("str:Zalacznik");
			zalacznik.addAttribute("format", attachment.getMostRecentRevision().getMime());
			zalacznik.addAttribute("nazwaPliku", attachment.getMostRecentRevision().getOriginalFilename());
			zalacznik.addAttribute("kodowanie", "base64");
			Element dane = zalacznik.addElement("str:DaneZalacznika"); 
			dane.addText(getBase64(attachment.getMostRecentRevision())); 
		}
		return xmlDocument.asXML().trim();
	}
	
	public static String getBase64(AttachmentRevision revision) throws IOException, EdmException
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();; 
		IOUtils.copy(revision.getAttachmentStream(), out);
		return Base64.encodeBase64String(out.toByteArray());
	}
	
	/*public String createOdpowiedzXmlDoreczyciel(OutOfficeDocument officeDocument) throws DocumentException, EdmException, IOException
	{
		String tresc= officeDocument.getDescription();
		String data= officeDocument.getDocumentDate() != null ? DateUtils.formatJsDate(officeDocument.getDocumentDate()) : "";
		List<Attachment> attachments= officeDocument.getAttachments();

		SAXReader reader= new SAXReader();
		org.dom4j.Document xmlDocument = reader.read(new FileInputStream(Docusafe.getHome().getAbsolutePath()+"/odpowiedz.xml"));
		if(!StringUtils.isEmpty(data))
		{
			Element dateElement = xmlDocument.getRootElement().element("OpisDokumentu").element("Data").element("Czas");
			dateElement.addText(data);
		}
		Element wartosc = xmlDocument.getRootElement().element("TrescDokumentu").element("Wartosc");
		wartosc.element("TrescWniosku").element("ZawartoscTresci").addText(tresc.trim());
		Element zalaczniki = wartosc.element("Zalaczniki");
		for (Attachment attachment : attachments)
		{
			Element zalacznik = zalaczniki.addElement("str:Zalacznik");
			zalacznik.addAttribute("format", attachment.getMostRecentRevision().getMime());
			zalacznik.addAttribute("nazwaPliku", attachment.getMostRecentRevision().getOriginalFilename());
			zalacznik.addAttribute("kodowanie", "base64"); 
			Element dane = zalacznik.addElement("str:DaneZalacznika"); 
			dane.addText(getBase64(attachment.getMostRecentRevision())); 
		}
		//return new DataHandler(new ByteArrayDataSource(
		return xmlDocument.asXML().trim();
		//.getBytes("UTF-8")));
	}*/
	private String getBinaryStream(OutOfficeDocument officeDocument,
			long idSignatures, long idDocument) throws EdmException {

		boolean closeStm = true;
		String content = null;
		PreparedStatement pst = null;
		try {

			pst = DSApi.context().prepareStatement("select uri from ds_electronic_signature where id = ? and document_id =?");
			pst.setLong(1, idSignatures);
			pst.setLong(2, idDocument);
			ResultSet rs = pst.executeQuery();

			if (rs.next()) {
				String lokalizacja = rs.getString(1);

				FileInputStream fis = null;
				// create file object
				File file = new File(lokalizacja);
				try {
					// create FileInputStream object
					fis = new FileInputStream(file);
					/*
					 * Create byte array large enough to hold the content of the
					 * file. Use File.length to determine size of the file in
					 * bytes.
					 */
					byte fileContent[] = new byte[(int) file.length()];
					/*
					 * To read content of the file in byte array, use int
					 * read(byte[] byteArray) method of java FileInputStream
					 * class.
					 */
					fis.read(fileContent);

					// tworzenie stringa sygnatury z byte array
					String signature = new String(fileContent);
					SzyfrowanieSignatureEpuap sse = new SzyfrowanieSignatureEpuap();
					content = sse.Odszyfruj(signature);

					fis.close();
					return content;

				} catch (FileNotFoundException e) {
					log.error("Nie znaleziono pliku sygnatury" + e);
				} catch (IOException ioe) {
					log.error("System nie mo�e odczyta� dokumentu z wskazanego pliku "+ ioe);
				}
			}
		}

		catch (HibernateException e) {
			throw new EdmHibernateException(e,
					"B�ad poczas odczytu za�acznika z Bazy danych firebird");
		} catch (SQLException e) {
			throw new EdmSQLException(e);
		} finally {
			if (closeStm) {
				DSApi.context().closeStatement(pst);
			}

		}
		return content;

	}
}

