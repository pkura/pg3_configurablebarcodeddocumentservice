package pl.compan.docusafe.service.epuap;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.dom4j.DocumentException;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.OdpowiedzDoreczyciela;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.OdpowiedzDoreczycielaTyp;
import pl.gov.epuap.ws.skrytka.NadajFaultMsg;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.IdentyfikatorDokumentuTyp;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.OdpowiedzSkrytki;
import pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.StatusTyp;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 *
 */
public class EpuapExportService extends ServiceDriver implements Service
{
	private static final Logger log = LoggerFactory.getLogger("EpuapImportService");
	private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(EpuapExportService.class.getPackage().getName(),null);
	private Property[] properties;
	private String adresSkrytki;
	private String nazwaSkrytki;
	private String identyfikatorPodmiotu;
	
    class AdresSkrytkiProperty extends Property {
        public AdresSkrytkiProperty(){
            super(SIMPLE, PERSISTENT, EpuapExportService.this, "adresSkrytki", "adresSkrytki", String.class);
        }
        protected Object getValueSpi(){
            return adresSkrytki;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (EpuapExportService.this){
            	if(object != null)
            		adresSkrytki = (String) object;
            	else
            		adresSkrytki = "";
            }
        }
    }
    
    class NazwaSkrytkiProperty extends Property {
        public NazwaSkrytkiProperty(){
            super(SIMPLE, PERSISTENT, EpuapExportService.this, "nazwaSkrytki", "nazwaSkrytki", String.class);
        }
        protected Object getValueSpi(){
            return nazwaSkrytki;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (EpuapExportService.this){
            	if(object != null)
            		nazwaSkrytki = (String) object;
            	else
            		nazwaSkrytki = "";
            }
        }
    }
    
    class IdentyfikatorPodmiotuProperty extends Property {
        public IdentyfikatorPodmiotuProperty(){
            super(SIMPLE, PERSISTENT, EpuapExportService.this, "identyfikatorPodmiotu", "identyfikatorPodmiotu", String.class);
        }
        protected Object getValueSpi(){
            return identyfikatorPodmiotu;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (EpuapExportService.this){
            	if(object != null)
            		identyfikatorPodmiotu = (String) object;
            	else
            		identyfikatorPodmiotu = "";
            }
        }
    }

	public EpuapExportService()
	{
		properties = new Property[] { new IdentyfikatorPodmiotuProperty(), new NazwaSkrytkiProperty(), new AdresSkrytkiProperty()};
	}

	protected boolean canStop()
	{
		return true;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi EpuapImportService");
		timer = new Timer(true);
		timer.schedule(new Export(), 0 ,1*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("SystemWystartowal"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi EpuapImportService");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}	
	
	class Export extends TimerTask
	{
		@Override
		public void run() 
		{
			console(Console.INFO, "Start");
            try
            {
            	DSApi.openAdmin();
				EpuapExportManager manager = new EpuapExportManager(adresSkrytki,nazwaSkrytki,identyfikatorPodmiotu);
				
				List<EpuapExportDocument> exportDocuments = EpuapExportDocument.documentToSend();
				console(Console.INFO, "Do wys�ania "+ (exportDocuments != null ? exportDocuments.size() : "0") );
				if (exportDocuments!=null && exportDocuments.size() > 0)
				{
					for (EpuapExportDocument document : exportDocuments)
					{
						try
						{
							DSApi.context().begin();
							OutOfficeDocument officeDocument = OutOfficeDocument.findOutOfficeDocument(document.getDocumentId());
							Recipient recipient = document.getPerson();
							console(Console.INFO, "Wysy�a do  " + recipient.getAdresSkrytkiEpuap());
							if (EpuapExportDocument.USLUGA_SKRYTKA.equals(document.getRodzajUslugi()))
							{
								wyslijSkrytka(manager, officeDocument, recipient, document);
								addWpisHistDoDokuemntu("Przedlozeniem", officeDocument,recipient);
							} else
							{
								wyslijDoreczyciel(manager, officeDocument, recipient, document);
								addWpisHistDoDokuemntu("Doreczeniem", officeDocument,recipient);
							}
							DSApi.context().commit();
						} catch (NadajFaultMsg e)
						{
							document.setStatus(EpuapExportDocument.STATUS_BLAD_DANYCH);
							document.setOdpowiedz(e.getFaultMessage().getWyjatek().getKomunikat());
							log.error(e.getMessage(), e);
							console(Console.ERROR, e.getFaultMessage().getWyjatek().getKomunikat());
							DSApi.context().commit();
						} catch (RuntimeException e)
						{
							document.setStatus(EpuapExportDocument.STATUS_BLAD_DANYCH);
							document.setOdpowiedz(e.getMessage());
							log.error(e.getMessage(), e);
							console(Console.ERROR, e.getMessage());
							DSApi.context().commit();
						} catch (Throwable t)
						{
							document.setStatus(EpuapExportDocument.STATUS_BLAD_KRYTYCZNY);
							document.setOdpowiedz(t.getMessage());
							log.error(t.getMessage(), t);
							console(Console.ERROR, t.getMessage());
							DSApi.context().commit();
						}
					}
				}
			}
            catch (Throwable t)
            {
            	log.error(t.getMessage(),t);
            	console(Console.ERROR, t.getMessage());
            }
            finally
            {
            	DSApi._close();
            }
		}
	}
	 private void addWpisHistDoDokuemntu(String description, OfficeDocument doc, Recipient recipient) throws UserNotFoundException, EdmException
		{
		 Date data= new Date();
		 StringBuilder sb = new StringBuilder();
		 sb.append("Dokument zostal wyslany do ePUAP. Odbiorca Pisma :") ;
		 sb.append(recipient.getFirstname() + " "+recipient.getLastname()+" ");	
		 sb.append( ", metoda :"+description+" ");
		 sb.append( ", na adres :"+recipient.getAdresSkrytkiEpuap()+" ");
		 sb.append(" w dniu "+ new Date());
			Audit wpis = new Audit();
			wpis.setDescription(sb.toString());
			wpis.setCtime(new Date());
			wpis.setUsername(DSApi.context().getDSUser().getName());
			wpis.setProperty("giveAccept");
			wpis.setLparam("");
			wpis.setPriority(1);
			DataMartManager.addHistoryEntry(doc.getId(), wpis);
			
		}
	private void wyslijDoreczyciel(EpuapExportManager manager,OutOfficeDocument officeDocument,Recipient recipient,EpuapExportDocument document) throws NadajFaultMsg, DocumentException, IOException, EdmException, Exception
	{
			OdpowiedzDoreczyciela odp = manager.wyslijDoreczyciel(recipient.getAdresSkrytkiEpuap(),officeDocument);
			OdpowiedzDoreczycielaTyp odpTyp = odp.getOdpowiedzDoreczyciela();
			System.out.println(odpTyp.getIdentyfikatorDokumentu().getIdentyfikatorDokumentuTyp());
			System.out.println(odpTyp.getIdentyfikatorZlecenia());
			PkDoreczycielServiceStub.StatusTyp status = odpTyp.getStatus();
			System.out.println(status.getKomunikat());
			
			if(status != null)
			{
				console(Console.INFO, "STATUS : "+status.getKomunikat()+" ["+status.getKod()+"]");
				document.setOdpowiedz(status.getKomunikat());
			}
			document.setStatus(EpuapExportDocument.STATUS_POPRAWNIE_WYSLANY);
			document.setDataNadania(new Date());
	}
	
	
	private void wyslijSkrytka(EpuapExportManager manager,OutOfficeDocument officeDocument,Recipient recipient,EpuapExportDocument document) throws NadajFaultMsg, DocumentException, IOException, EdmException, Exception
	{
			OdpowiedzSkrytki odp = manager.wyslij(officeDocument.getDescription(),(officeDocument.getDocumentDate() != null ?
					DateUtils.formatJsDate(officeDocument.getDocumentDate()): ""),recipient.getAdresSkrytkiEpuap(), officeDocument.getAttachments(),officeDocument);
			IdentyfikatorDokumentuTyp upp = odp.getOdpowiedzSkrytki().getIdentyfikatorUpp();
			if(upp != null)
			{
				System.out.println(upp.getIdentyfikatorDokumentuTyp());
			}
			IdentyfikatorDokumentuTyp doc = odp.getOdpowiedzSkrytki().getIdentyfikatorDokumentu();
			if(doc != null)
			{
				System.out.println(doc.getIdentyfikatorDokumentuTyp());
			}
			StatusTyp status = odp.getOdpowiedzSkrytki().getStatus();
			if(status != null)
			{
				console(Console.INFO, "STATUS : "+status.getKomunikat()+" ["+status.getKod()+"]");
				document.setOdpowiedz(status.getKomunikat());
			}
			document.setStatus(EpuapExportDocument.STATUS_POPRAWNIE_WYSLANY);
			document.setDataNadania(new Date());
			StatusTyp statusOdbiorcy = odp.getOdpowiedzSkrytki().getStatusOdbiorcy();
			if(statusOdbiorcy != null)
			{
				console(Console.INFO, "STATUS ODBIORCY : "+statusOdbiorcy.getKomunikat()+" ["+statusOdbiorcy.getKod()+"]");
			}
			/**Zapisuje zalacznik*/
			if(odp.getOdpowiedzSkrytki().getZalacznik() != null && odp.getOdpowiedzSkrytki().getZalacznik().getZawartosc() != null)
			{
				console(Console.INFO, "Odbieram dokument "+odp.getOdpowiedzSkrytki().getZalacznik().getNazwaPliku() );
				Attachment attachment = new Attachment("Odpowiedz");
				officeDocument.createAttachment(attachment);
				Integer lenght = FileUtils.lenghtFormInputStream(odp.getOdpowiedzSkrytki().getZalacznik().getZawartosc().getInputStream());
				attachment.createRevision(odp.getOdpowiedzSkrytki().getZalacznik().getZawartosc().getInputStream(),lenght,odp.getOdpowiedzSkrytki().getZalacznik().getNazwaPliku());
			}
	}
	
	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties) 
	{
		this.properties = properties;
	}

	public String getAdresSkrytki() {
		return adresSkrytki;
	}

	public void setAdresSkrytki(String adresSkrytki) {
		this.adresSkrytki = adresSkrytki;
	}

	public String getIdentyfikatorPodmiotu() {
		return identyfikatorPodmiotu;
	}

	public void setIdentyfikatorPodmiotu(String identyfikatorPodmiotu) {
		this.identyfikatorPodmiotu = identyfikatorPodmiotu;
	}

	public String getNazwaSkrytki() {
		return nazwaSkrytki;
	}

	public void setNazwaSkrytki(String nazwaSkrytki) {
		this.nazwaSkrytki = nazwaSkrytki;
	}
}
