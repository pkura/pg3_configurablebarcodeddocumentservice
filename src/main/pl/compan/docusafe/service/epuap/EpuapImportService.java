package pl.compan.docusafe.service.epuap;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.google.common.collect.Maps;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.gov.epuap.ws.pull.PullFaultMsg;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullPobierzTyp;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullPotwierdz;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 *
 */
public class EpuapImportService extends ServiceDriver implements Service
{
	private static final Logger log = LoggerFactory.getLogger("EpuapImportService");
	private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(EpuapImportService.class.getPackage().getName(),null);
	private Property[] properties;
	private String adresSkrytki;
	private String nazwaSkrytki;
	private String identyfikatorPodmiotu;
	private static EpuapImportManager manager;

	class AdresSkrytkiProperty extends Property {
		public AdresSkrytkiProperty(){
			super(SIMPLE, PERSISTENT, EpuapImportService.this, "adresSkrytki", "adresSkrytki", String.class);
		}
		protected Object getValueSpi(){
			return adresSkrytki;
		} 
		protected void setValueSpi(Object object) throws ServiceException{
			synchronized (EpuapImportService.this){
				if(object != null)
					adresSkrytki = (String) object;
				else
					adresSkrytki = "";
				manager = null;
			}
		}
	}

	class NazwaSkrytkiProperty extends Property {
		public NazwaSkrytkiProperty(){
			super(SIMPLE, PERSISTENT, EpuapImportService.this, "nazwaSkrytki", "nazwaSkrytki", String.class);
		}
		protected Object getValueSpi(){
			return nazwaSkrytki;
		} 
		protected void setValueSpi(Object object) throws ServiceException{
			synchronized (EpuapImportService.this){
				if(object != null)
					nazwaSkrytki = (String) object;
				else
					nazwaSkrytki = "";
				manager = null;
			}
		}
	}

	class IdentyfikatorPodmiotuProperty extends Property {
		public IdentyfikatorPodmiotuProperty(){
			super(SIMPLE, PERSISTENT, EpuapImportService.this, "identyfikatorPodmiotu", "identyfikatorPodmiotu", String.class);
		}
		protected Object getValueSpi(){
			return identyfikatorPodmiotu;
		} 
		protected void setValueSpi(Object object) throws ServiceException{
			synchronized (EpuapImportService.this){
				if(object != null)
					identyfikatorPodmiotu = (String) object;
				else
					identyfikatorPodmiotu = "";
				manager = null;
			}
		}
	}

	public EpuapImportService()
	{
		properties = new Property[] { new IdentyfikatorPodmiotuProperty(), new NazwaSkrytkiProperty(), new AdresSkrytkiProperty()};
	}

	protected boolean canStop()
	{
		return true;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi EpuapImportService");
		timer = new Timer(true);
		timer.schedule(new Import(), 0 ,1*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("SystemWystartowal"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi EpuapImportService");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}	

	class Import extends TimerTask
	{
		@Override
		public void run() 
		{
			console(Console.INFO, "Start");
			try
			{
				if(manager == null)
					manager = new EpuapImportManager(adresSkrytki,nazwaSkrytki,identyfikatorPodmiotu);
				Integer ile = manager.getOczekujace();
				console(Console.INFO, "Odbiera "+ile + " dokument�w");
				while (ile > 0)
				{
					OdpowiedzPullPobierzTyp odpowiedz = manager.pobierzNastepny();
					try
					{
						console(Console.INFO, "Pobra� status: "+ odpowiedz.getStatus().getKomunikat());
					}
					catch (Exception e) {
						log.error(e.getMessage());
					}
					DSApi.openAdmin();
					DSApi.context().begin(); 
				
					//InOfficeDocument document = manager.zapiszDokument(odpowiedz);
					InOfficeDocument document = manager.zapiszDokumentlubUPO(odpowiedz, true);
					console(Console.INFO, "Zapisa� pod ID  "+ document.getId());
					if (AvailabilityManager.isAvailable("wyslijDoEpuap")){
					 createNewProcesEpuap(document);
					} 
					else{
					WorkflowFactory.createNewProcess(document,true,"do realizacji ePUAP");
					}
					DSApi.context().commit();
					DSApi.context().begin(); 
					OdpowiedzPullPotwierdz opdowiedzPotwierdz = manager.potwierdzOdebranie(odpowiedz);
					console(Console.INFO, "Wys�a� potwierdzenie  "+ opdowiedzPotwierdz.getOdpowiedzPullPotwierdz().getStatus().getKomunikat());

					ile = manager.getOczekujace();
					if(ile != null && ile > 0 )
					{
						OdpowiedzPullPobierzTyp odpowiedzUPP = manager.pobierzNastepny();
						Attachment attachment = new Attachment("UPP");
						document.createAttachment(attachment);
						int lenght = FileUtils.lenghtFormInputStream(odpowiedzUPP.getDokument().getZawartosc().getInputStream());
						attachment.createRevision(odpowiedzUPP.getDokument().getZawartosc().getInputStream(),lenght,odpowiedzUPP.getDokument().getNazwaPliku());
						opdowiedzPotwierdz = manager.potwierdzOdebranie(odpowiedzUPP);
						console(Console.INFO, "Wys�a� potwierdzenie odebrania UPP  "+ opdowiedzPotwierdz.getOdpowiedzPullPotwierdz().getStatus().getKomunikat());
					}
					TaskSnapshot.updateByDocument(document);
					DSApi.context().commit();
					DSApi.close();
					console(Console.INFO, "Pobra� dokument u�ytkownika  "+ odpowiedz.getDaneNadawcy().getUzytkownik()+", z Dnia: "+odpowiedz.getDataNadania().getTime().toLocaleString());
					ile --; ile --;    				

				}
			}
			catch (PullFaultMsg e) {
				log.error(e.getMessage(),e);
				e.printStackTrace();
				console(Console.ERROR, e.getFaultMessage().getWyjatek().getKomunikat());

			}
			catch (Throwable t)
			{
				log.error(t.getMessage(),t);
				console(Console.ERROR, t.getMessage());
			}
		}
	}


	public Property[] getProperties()
	{
		return properties;
	}

	public void createNewProcesEpuap(InOfficeDocument document) throws EdmException
	{
		Map<String, Object> params = Maps.newHashMap();
		params.put(Jbpm4Constants.KEY_PROCESS_NAME, Jbpm4Constants.MANUAL_PROCESS_NAME);
//		params.put(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM,  document.getAuthor());
        String odbiorca = Docusafe.getAdditionPropertyOrDefault("epuap.odbiorca", "");
        if(! odbiorca.equals("")) {
            params.put(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, odbiorca);
        } else {
            params.put(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, document.getAuthor());
        }
        params.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
		params.put(Jbpm4WorkflowFactory.OBJECTIVE, "Do realizacji z ePUAP");

		document.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.MANUAL_PROCESS_NAME).getLogic()
				.startProcess(document, params);
		
	}

	public void setProperties(Property[] properties) 
	{
		this.properties = properties;
	}

	public String getAdresSkrytki() {
		return adresSkrytki;
	}

	public void setAdresSkrytki(String adresSkrytki) {
		this.adresSkrytki = adresSkrytki;
	}

	public String getIdentyfikatorPodmiotu() {
		return identyfikatorPodmiotu;
	}

	public void setIdentyfikatorPodmiotu(String identyfikatorPodmiotu) {
		this.identyfikatorPodmiotu = identyfikatorPodmiotu;
	}

	public String getNazwaSkrytki() {
		return nazwaSkrytki;
	}

	public void setNazwaSkrytki(String nazwaSkrytki) {
		this.nazwaSkrytki = nazwaSkrytki;
	}
}
