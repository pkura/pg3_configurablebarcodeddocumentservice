package pl.compan.docusafe.service.epuap;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytka;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa EpuapExportDocument.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class EpuapExportDocument 
{
	private final static Logger log = LoggerFactory.getLogger(EpuapExportDocument.class);
	
	public static final Integer STATUS_BLAD_KRYTYCZNY = 1;
	public static final Integer STATUS_NIE_WYSYLAJ = 2;
	public static final Integer STATUS_BLAD_DANYCH = 3;
	public static final Integer STATUS_POPRAWNIE_WYSLANY = 9;
	
	public static final Integer USLUGA_DORECZYCIEL = 50;
	public static final Integer USLUGA_SKRYTKA = 51;
	
	/**Wysyla wszystkie ze statusem > 10 */
	public static final Integer STATUS_DO_WYSYLKI = 10;
	public static final Integer STATUS_BRAK_ODPOWIEDZI = 15;
	
	//--------------------------------- POLA ---------------------------------------
	
	private Long id;
	private Long documentId;
	private String odpowiedz;
	private Integer status;
	
	/** Data dodania do kolejki do wyslania */	private Date dataNadania;
	/** adresat */								private Recipient person;
	/** doreczenie czy przedlozenie */			private Integer rodzajUslugi;
	
	/**
	 * Powiazanie ze skrzynk� PRZEZ JAKA ma zosta� wys�any dokument
	 * 
	 * A wiec NIE JEST to ADRESAT tylko NADAWCA.
	 * 
	 * Czytelniku! W razie Twoich w�tpliwo�ci:
	 * Na pewno nie jest to Adresat tylko Nadawca.
	 * Adresat b�dzie brany z DSO_Person (recipient)
	 */
	private EpuapSkrytka epuapSkrytka;
	
	//--------------------------------- POLA // ---------------------------------------
	
	public static String statusAsString(Integer id)
	{
		if(STATUS_BLAD_KRYTYCZNY.equals(id))
			return "B��d krytyczny";
		else if(STATUS_BLAD_DANYCH.equals(id))
			return "B��d danych";
		else if(STATUS_BRAK_ODPOWIEDZI.equals(id))
			return "Brak odpowiedzi serwera";
		else if(STATUS_DO_WYSYLKI.equals(id))
			return "Przekazany do wys�ania";
		else if(STATUS_NIE_WYSYLAJ.equals(id))
			return "Nie wysy�aj";
		else if(STATUS_POPRAWNIE_WYSLANY.equals(id))
			return "Wys�any";
		else
			return "Brak";
	}
	
	
    public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().saveOrUpdate(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	
    
	public static synchronized List<EpuapExportDocument> sentDocuments(Integer first,Integer offset) 
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(EpuapExportDocument.class);
			c.add(Restrictions.eq("status", STATUS_POPRAWNIE_WYSLANY));
			c.setFirstResult(first);
			c.setMaxResults(offset);
			return c.list();
		}
		catch (Exception e) 
		{
			return null;
		}		
	}
	
	public static synchronized List<EpuapExportDocument> erroDocuments(Integer first,Integer offset) 
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(EpuapExportDocument.class);
			c.add(Restrictions.lt("status", STATUS_POPRAWNIE_WYSLANY));
			c.setFirstResult(first);
			c.setMaxResults(offset);
			return c.list();
		}
		catch (Exception e) 
		{
			log.error("", e);
			return null;
		}		
	}
	
	public static boolean isSent(Long documentId) 
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(EpuapExportDocument.class);
			c.add(Restrictions.eq("status", STATUS_POPRAWNIE_WYSLANY));
			c.add(Restrictions.eq("documentId", documentId));
			return c.list() != null && c.list().size() > 0;
		}
		catch (Exception e) 
		{
			return false;
		}		
	}
	
	public static EpuapExportDocument findByRecipient(Long documentId, Person recipient) 
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(EpuapExportDocument.class);
			c.add(Restrictions.eq("person", recipient));
			c.add(Restrictions.eq("documentId", documentId));
			if( c.list() != null && c.list().size() > 0)
				return (EpuapExportDocument) c.list().get(0);
		}
		catch (Exception e) 
		{
			return null;
		}
		return null;
	}
	
	public static synchronized List<EpuapExportDocument> documentToSend() 
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(EpuapExportDocument.class);
			c.add(Restrictions.ge("status", STATUS_DO_WYSYLKI));
			return c.list();
		}
		catch (Exception e) 
		{
			return null;
		}		
	}
    
	public Date getDataNadania() {
		return dataNadania;
	}
	public void setDataNadania(Date dataNadania) {
		this.dataNadania = dataNadania;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOdpowiedz() {
		return odpowiedz;
	}
	public void setOdpowiedz(String odpowiedz) {
		this.odpowiedz = odpowiedz;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}


	public Recipient getPerson() {
		return person;
	}


	public void setPerson(Recipient person) {
		this.person = person;
	}


	public static EpuapExportDocument find(Long id) throws EdmException {
		return Finder.find(EpuapExportDocument.class, id);
	}


	public Integer getRodzajUslugi() {
		return rodzajUslugi;
	}


	public void setRodzajUslugi(Integer rodzajUslugi) {
		this.rodzajUslugi = rodzajUslugi;
	}

	/** skrytka NADAWCY (nie odbiorcy). Na 100% !!! */
	public EpuapSkrytka getEpuapSkrytka()
	{
		return epuapSkrytka;
	}

	/** skrytka NADAWCY (nie odbiorcy). Na 100% !!! */
	public void setEpuapSkrytka(EpuapSkrytka epuapSkrytka)
	{
		this.epuapSkrytka = epuapSkrytka;
	}
}
