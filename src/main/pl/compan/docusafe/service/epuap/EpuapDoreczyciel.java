package pl.compan.docusafe.service.epuap;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.axis2.AxisFault;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.AdresOdpowiedzi;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.AdresSkrytki;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.CzyProbne;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.DaneDodatkowe;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.Dokument;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.DokumentTyp;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorDokumentu;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorDokumentuTyp;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorPodmiotu;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorPodmiotuTyp;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.IdentyfikatorSprawy;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.OdpowiedzDoreczyciela;
import pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.TerminDoreczenia;
import pl.gov.epuap.ws.skrytka.NadajFaultMsg;


public class EpuapDoreczyciel 
{
	private static final Logger log  = LoggerFactory.getLogger(EpuapExportManager.class);
	private AdresOdpowiedzi adresOdpowiedziDoreczyciel;
	private IdentyfikatorPodmiotu podmiotDoreczyciel;
	private PkDoreczycielServiceStub serviceDoreczyciel;  
    
	
	public EpuapDoreczyciel(String adresSkrytki, String nazwaSkrytki, String identyfikatorPodmiotu) throws AxisFault, Exception 
	{
		super();
		this.adresOdpowiedziDoreczyciel = new AdresOdpowiedzi();
		adresOdpowiedziDoreczyciel.setAdresOdpowiedzi(adresSkrytki);
		IdentyfikatorPodmiotuTyp typD = new IdentyfikatorPodmiotuTyp();
		typD.setIdentyfikatorPodmiotuTyp(identyfikatorPodmiotu);
		this.podmiotDoreczyciel = new IdentyfikatorPodmiotu();
		podmiotDoreczyciel.setIdentyfikatorPodmiotu(typD);	
		AxisClientConfigurator conf = new AxisClientConfigurator();
		this.serviceDoreczyciel = new PkDoreczycielServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(serviceDoreczyciel, "/pk_external_ws/services/doreczyciel");
	}
	
	public OdpowiedzDoreczyciela wyslijDoreczyciel(String tresc,String data,String adresSkrytki, List<Attachment> attachments,OutOfficeDocument officeDocument) throws NadajFaultMsg, DocumentException, IOException, Exception 
	{
		Dokument doc = new Dokument();
		DokumentTyp dokumentTyp = new DokumentTyp();
		dokumentTyp.setNazwaPliku("odpowiedz.xml");
		dokumentTyp.setTypPliku("application/octet-stream");	
		DataHandler dh = createOdpowiedz(tresc,data,attachments);
		dokumentTyp.setZawartosc(dh);
		doc.setDokument(dokumentTyp);
		IdentyfikatorDokumentuTyp idDoctyp = new IdentyfikatorDokumentuTyp();
		idDoctyp.setIdentyfikatorDokumentuTyp(String.valueOf(officeDocument.getId()));
		IdentyfikatorDokumentu idDoc = new IdentyfikatorDokumentu();
		idDoc.setIdentyfikatorDokumentu(idDoctyp);
		AdresSkrytki adresSkrytkiDo = new AdresSkrytki();
		adresSkrytkiDo.setAdresSkrytki(adresSkrytki);
		CzyProbne czyProbne = new CzyProbne();
		czyProbne.setCzyProbne(false);
		TerminDoreczenia terminDoreczenia = new TerminDoreczenia();
		terminDoreczenia.setTerminDoreczenia(null);
		IdentyfikatorSprawy identyfikatorSprawy = new IdentyfikatorSprawy();
		identyfikatorSprawy.setIdentyfikatorSprawy(officeDocument.getContainingCase() != null ? officeDocument.getContainingCase().getOfficeId() : "Brak");
		DaneDodatkowe daneDodatkowe = new DaneDodatkowe();
		daneDodatkowe.setDaneDodatkowe(new DataHandler(new FileDataSource(Docusafe.getHome().getAbsolutePath()+"/dane.xml")));
		return serviceDoreczyciel.dorecz(doc, podmiotDoreczyciel, adresSkrytkiDo, adresOdpowiedziDoreczyciel, terminDoreczenia, czyProbne, idDoc,identyfikatorSprawy, daneDodatkowe, null);
	}
	
	private DataHandler createOdpowiedz(String tresc,String data, List<Attachment> attachments) throws DocumentException, IOException, Exception
	{
		SAXReader reader = new SAXReader();
		org.dom4j.Document xmlDocument = reader.read(new FileInputStream(Docusafe.getHome().getAbsolutePath()+"/odpowiedz.xml"));
		if(!StringUtils.isEmpty(data))
		{
			Element dateElement = xmlDocument.getRootElement().element("OpisDokumentu").element("Data").element("Czas");
			dateElement.addText(data);
		}
		Element wartosc = xmlDocument.getRootElement().element("TrescDokumentu").element("Wartosc");
		wartosc.element("TrescWniosku").element("ZawartoscTresci").addText(tresc.trim());
		Element zalaczniki = wartosc.element("Zalaczniki");
		for (Attachment attachment : attachments)
		{
			Element zalacznik = zalaczniki.addElement("str:Zalacznik");
			zalacznik.addAttribute("format", attachment.getMostRecentRevision().getMime());
			zalacznik.addAttribute("nazwaPliku", attachment.getMostRecentRevision().getOriginalFilename());
			zalacznik.addAttribute("kodowanie", "base64"); 
			Element dane = zalacznik.addElement("str:DaneZalacznika"); 
			dane.addText(getBase64(attachment.getMostRecentRevision())); 
		}		
		return new DataHandler(new ByteArrayDataSource(xmlDocument.asXML().trim().getBytes("UTF-8")));
	}
	
	private String getBase64(AttachmentRevision revision) throws IOException, EdmException
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();; 
		IOUtils.copy(revision.getAttachmentStream(), out);
		return Base64.encodeBase64String(out.toByteArray());
	}
}
