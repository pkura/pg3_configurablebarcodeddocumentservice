package pl.compan.docusafe.service.stagingarea;

import org.hibernate.*;
import org.hibernate.classic.Lifecycle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;

import java.io.File;
import java.io.Serializable;
import java.util.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RetainedObject.java,v 1.3 2006/12/07 13:53:43 mmanski Exp $
 */
public class RetainedObject implements Lifecycle
{
    private static final Log log = LogFactory.getLog(RetainedObject.class);

    private Long id;
    private String source;
    private String type;
    private String title;
    private Date ctime;
    private Map<String, String> properties;
    private List<RetainedObjectAttachment> attachments;

    RetainedObject()
    {
    }

    public static void create(String title, String source, Map<String, String> properties, File... files) throws EdmException
    {
        log.debug("create title="+title+" properties="+properties+" files="+files+
            (files != null ? " ("+files.length+")" : ""));

        RetainedObject object = new RetainedObject();
        object.setTitle(title);
        object.setSource(source);
        if (properties != null)
        {
            object.setProperties(new HashMap<String, String>(properties));
        }

        Persister.create(object, true);

        if (files != null)
        {
            for (File file : files)
            {
                if (!file.isFile())
                    throw new EdmException("Plik "+file+" nie istnieje");
                RetainedObjectAttachment.create(object, file);
            }
        }
    }

    public void delete() throws EdmException
    {
        Persister.delete(this);
    }

    public static List<RetainedObject> findBySource(String source) throws EdmException
    {
        try
        {
            return (List<RetainedObject>) DSApi.context().classicSession().find(
                "from o in class "+RetainedObject.class.getName()+
                " where o.source = ? order by o.ctime desc", source, Hibernate.STRING);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static RetainedObject find(Long id) throws EdmException
    {
        return Finder.find(RetainedObject.class, id);
    }

    public void addAttachment(RetainedObjectAttachment attachment)
    {
        if (attachments == null)
            attachments = new ArrayList<RetainedObjectAttachment>();
        attachments.add(attachment);
    }

    public String get(String property)
    {
        return properties != null ? properties.get(property) : null;
    }

    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }

    public String getSource()
    {
        return source;
    }

    private void setSource(String source)
    {
        this.source = source;
    }

    public Date getCtime()
    {
        return ctime;
    }

    private void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public List<RetainedObjectAttachment> getAttachments()
    {
        if (attachments == null)
            attachments = new ArrayList<RetainedObjectAttachment>();
        return attachments;
    }

    private void setAttachments(List<RetainedObjectAttachment> attachments)
    {
        this.attachments = attachments;
    }

    public Map<String, String> getProperties()
    {
        return properties;
    }

    private void setProperties(Map<String, String> properties)
    {
        this.properties = properties;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    /* Lifecycle */

    public boolean onSave(Session session) throws CallbackException
    {
        if (ctime == null)
        {
            ctime = new Date();
        }
        return false;
    }

    public boolean onUpdate(Session session) throws CallbackException
    {
        return false;
    }

    public boolean onDelete(Session session) throws CallbackException
    {
        return false;
    }

    public void onLoad(Session session, Serializable serializable)
    {
    }
}
