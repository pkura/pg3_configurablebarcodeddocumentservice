package pl.compan.docusafe.service.stagingarea;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.BlobInputStream;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.util.MimetypesFileTypeMap;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RetainedObjectAttachment.java,v 1.8 2009/07/14 14:21:15 mariuszk Exp $
 */
public class RetainedObjectAttachment
{
    private Long id;
    private String cn;
    private String filename;
    private int size;
    private String mime;
    private RetainedObject object;

    RetainedObjectAttachment()
    {
    }

    public static RetainedObjectAttachment create(RetainedObject object, File file) throws EdmException
    {
        RetainedObjectAttachment attachment = new RetainedObjectAttachment();
        attachment.setObject(object);
        attachment.setFilename(file.getName());
        attachment.setSize((int) file.length());
        attachment.setMime(MimetypesFileTypeMap.getInstance().getContentType(file.getName()));
        object.addAttachment(attachment);
        Persister.create(attachment, true);

        PreparedStatement ps = null;
        FileInputStream stream = null;

        try
        {
            stream = new FileInputStream(file);

            int rows;
            if (DSApi.isOracleServer())
            {
                ps = DSApi.context().prepareStatement(
                    "select contentdata from "+DSApi.getTableName(RetainedObjectAttachment.class)+
                    " where id = ? for update");
                ps.setLong(1, attachment.getId());

                ResultSet rs = ps.executeQuery();
                if (!rs.next())
                    throw new EdmException("Nie mo�na odczyta� tre�ci za��cznika "+attachment.getId());

                rows = 1;

                Blob blob = rs.getBlob(1);
                if (!(blob instanceof oracle.sql.BLOB))
                    throw new EdmException("Spodziewano si� obiektu oracle.sql.BLOB");

                OutputStream out = ((oracle.sql.BLOB) blob).getBinaryOutputStream();
                byte[] buf = new byte[2048];
                int count;
                while ((count = stream.read(buf)) > 0)
                {
                    out.write(buf, 0, count);
                }
                out.close();
            }
            else
            {
                ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(RetainedObjectAttachment.class)+
                    " set contentdata = ? where id = ?");
                ps.setBinaryStream(1, stream, (int) file.length());
                ps.setLong(2, attachment.getId());
                rows = ps.executeUpdate();
            }

            if (rows != 1)
                throw new EdmException("Nie uda�o si� doda� tre�ci binarnej");

            return attachment;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (IOException e)
        {
            throw new EdmException(e.getMessage(), e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
            if (stream != null) try { stream.close(); } catch (Exception e) { }
        }
    }

    public static RetainedObjectAttachment find(Long id) throws EdmException
    {
        return Finder.find(RetainedObjectAttachment.class, id);
    }

    public InputStream getInputStream() throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement("select contentdata from ds_retained_attachment where id = ?");
            ps.setLong(1, getId());
            ResultSet rs = ps.executeQuery();
            if (!rs.next())
                throw new EdmException("Nie mo�na odczyta� tre�ci za��cznika "+getId());

            InputStream is = rs.getBinaryStream(1);
            // sterownik dla SQLServera nie wspiera ResultSet.getBlob
            //Blob blob = rs.getBlob(1);
            if (is != null)
            {
                return new BlobInputStream(is, ps);
            }
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        // Nie mo�na zamkn�� obiektu Statement od razu po zwr�ceniu strumienia,
        // poniewa� w�wczas nie b�dzie mo�liwe odczytanie danych ze strumienia.
        // Statement jest zamykane w metodzie BlobInputStream.closeStream()
    }
    public long saveToFile(File file) throws EdmException, IOException
    {
        if (file.exists() && !file.canWrite())
            throw new EdmException("Brak uprawnie� do zapisu pliku "+file);

        OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
        InputStream is = getInputStream();
        byte[] buf = new byte[2048];
        int count;
        long total = 0;
        while ((count = is.read(buf)) > 0)
        {
            total += count;
            os.write(buf, 0, count);
        }
        org.apache.commons.io.IOUtils.closeQuietly(is);
        org.apache.commons.io.IOUtils.closeQuietly(os);
        return total;
    }
    
    public File saveToTempFile() throws EdmException, IOException
    {
        String suffix;
        if (mime == null || mime.indexOf('/') < 0)
        {
            suffix = ".tmp";
        }
        else if (mime.indexOf("image/tiff") >= 0)
        {
            suffix = ".tif";
        }
        else if (mime.indexOf("image/jpeg") >= 0)
        {
            suffix = ".jpg";
        }
        else
        {
            String ms = mime.substring(mime.indexOf('/')+1);
            if (ms.indexOf(';') > 0)
                ms = ms.substring(0, ms.indexOf(';'));
            if (ms.length() == 0)
                ms = "tmp";
            suffix = "." + ms;
        }

        File file = File.createTempFile("docusafe_pdf", suffix);
        saveToFile(file);
        return file;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public Integer getSize()
    {
        return (Integer)size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public String getMime()
    {
        return mime;
    }

    public void setMime(String mime)
    {
        this.mime = mime;
    }

    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public RetainedObject getObject()
    {
        return object;
    }

    public void setObject(RetainedObject object)
    {
        this.object = object;
    }
}
