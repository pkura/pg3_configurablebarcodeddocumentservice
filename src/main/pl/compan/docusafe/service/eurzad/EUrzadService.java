package pl.compan.docusafe.service.eurzad;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.prefs.Preferences;

import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.bip.MaxusBipDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HexUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.ws.eurzad.EUrzad_wymiana_danychStub;
import pl.compan.docusafe.ws.eurzad.SocketFactory;


//klasa nie skonczona, w trakcie prac. Do sprawdzenia.
public class EUrzadService extends ServiceDriver implements Service
{
	private Timer timer;
	private String idUrzedu;
	private String login;
	private String password;
	private String serviceAddress = "https://com-pan.eurzad.org:443/soap-compan/dataexchange.php";

	public static String EURZAD_PREFS_NODE = "eUrzad_prefs";
	public static String URZADID_KEY = "urzadId";
	public static String LOGIN_KEY = "login";
	public static String PASSWORD_KEY = "password";
	public static String ADDRESS_KEY = "address";
	public static String KOORDYNATOR_KEY = "koordynator";


	@Override
	protected boolean canStop() {
		return true;
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
        timer.schedule(new EUrzadProcessor(), 10000, 5*DateUtils.MINUTE);
	}

	@Override
	protected void stop() throws ServiceException
	{
		if (timer != null) timer.cancel();

	}


	public class EUrzadProcessor extends TimerTask
	{

		@Override
		public void run()
		{
			DSContext ctx = null;
			try
			{
				ctx = DSApi.openAdmin();
				ctx.begin();
				Preferences node = DSApi.context().systemPreferences().node(EUrzadService.EURZAD_PREFS_NODE);
	        	idUrzedu = node.get(EUrzadService.URZADID_KEY, "");
	        	login = node.get(EUrzadService.LOGIN_KEY, "");
	        	password = node.get(EUrzadService.PASSWORD_KEY, "");
	        	serviceAddress = node.get(EUrzadService.ADDRESS_KEY, "");
	        	ctx.commit();
	        	if(serviceAddress.equals("")||login.equals("")||password.equals("")||idUrzedu.equals("")) return;
				EUrzad_wymiana_danychStub.serviceAddress = serviceAddress;
				Protocol myProtocolHandler = new Protocol("https", new SocketFactory(), 443);
				EUrzad_wymiana_danychStub eurzad = new EUrzad_wymiana_danychStub();
				eurzad._getServiceClient().getOptions().setProperty(HTTPConstants.CUSTOM_PROTOCOL_HANDLER, myProtocolHandler);

				processExport(eurzad);
				processImport(eurzad);

			}
			catch(Exception e)
			{
				if(ctx!=null) ctx.setRollbackOnly();
				log.error(e.getMessage(), e);
			}
			finally
			{
				DSApi._close();
			}

		}

		private ArrayList<String> processNadawcaElement(Element nadawca)
		{
			StringManager sm = StringManager.getManager(this.getClass().getPackage().getName());
			//StringManager sm = StringManager.getManager();
			ArrayList<String> res = new ArrayList<String>();
			DSContext ctx = null;
			try
			{
				ctx = DSApi.openAdmin();
				ctx.begin();


				String koordynator = ctx.systemPreferences().node(EURZAD_PREFS_NODE).get(EUrzadService.KOORDYNATOR_KEY,"");
				if(koordynator==null || koordynator.equals("")) throw new EdmException("Nie zdefniniowano koordynatora dla eUrzedu");
				Sender s = new Sender();
				s.setFirstname(nadawca.attributeValue("imie")!=null?nadawca.attributeValue("imie"):"");
				s.setLastname(nadawca.attributeValue("nazwisko")!=null?nadawca.attributeValue("nazwisko"):"");
				s.setStreet((nadawca.attributeValue("ulica")!=null?nadawca.attributeValue("ulica"):"") +
						(nadawca.attributeValue("numer_domu")!=null?nadawca.attributeValue("numer_domu"):"") + "/"+
						(nadawca.attributeValue("numer_lokalu")!=null?nadawca.attributeValue("numer_lokalu"):""));
				s.setLocation(nadawca.attributeValue("miasto")!=null?nadawca.attributeValue("miasto"):"");
				s.setZip(nadawca.attributeValue("kod_pocztowy")!=null?nadawca.attributeValue("kod_pocztowy"):"");
				s.setNip(nadawca.attributeValue("nip")!=null?nadawca.attributeValue("nip"):"");
				s.setPesel(nadawca.attributeValue("pesel")!=null?nadawca.attributeValue("pesel"):"");
				s.setEmail(nadawca.attributeValue("email")!=null?nadawca.attributeValue("email"):"");
				s.setLparam("eurzad");
				s.setWparam(Long.valueOf(nadawca.attributeValue("id")));
				//System.out.println(nadawca.attributeValue("id"));


				InOfficeDocument doc;
				List<Element> ebs = nadawca.elements("ebstream");
				File ebFile;
				for(Element eb:ebs)
				{
					//System.out.println("iteracja");
					try
					{
						doc = new InOfficeDocument();
						doc.setExternalId(eb.attributeValue("idform"));
						//doc.setCtime(new Date());
						String data = eb.attributeValue("data_otrzymania");
						if(data.contains(".")) data = data.substring(0, data.indexOf("."));
						//System.out.println(data);
						doc.setDocumentDate(DateUtils.sqlDateTimeFormatNoSeconds.parse(data));
						doc.setTitle("eUrzad");
						doc.setIncomingDate(DateUtils.sqlDateTimeFormatNoSeconds.parse(data));
						doc.setSummary(sm.getString("PismoPrzyjetePrzezEUrzad"));
						doc.setSender(s);
						///LabelsManager
						doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
				        doc.setDivisionGuid(DSDivision.ROOT_GUID);
				        doc.setAssignedDivision(DSDivision.ROOT_GUID);
				        doc.setCurrentAssignmentAccepted(Boolean.FALSE);
				        doc.setCreatingUser(ctx.getPrincipalName());
				        //doc.setDocumentKind(DocumentKind.findByCn(DocumentKind.NORMAL_KIND));
				        doc.setKind(InOfficeDocumentKind.findByPartialName("Wniosek").get(0));

				        doc.create();
				        ctx.session().flush();
				        ctx.session().refresh(doc);


						ebFile = File.createTempFile("ebstream"+doc.getExternalId(), ".ebf");
						OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(ebFile),"UTF-8");
						fw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
						fw.write(eb.element("ebForm").asXML());
						fw.flush();
						fw.close();

						Attachment att = new Attachment("ebstream");
						doc.createAttachment(att);
						att.createRevision(ebFile);





						res.add(doc.getExternalId());
						WorkflowFactory.createNewProcess(doc, false, "Pismo przyj�te", koordynator, DSDivision.ROOT_GUID, koordynator);

						PreparedStatement ps = DSApi.context().prepareStatement("insert into dso_eurzad(documentId,modifyDate, type) values(?,?,?)");
						ps.setLong(1, doc.getId());
						ps.setTimestamp(2, new java.sql.Timestamp(new Date().getTime()));
						ps.setString(3, "in");
						ps.execute();
						DSApi.context().closeStatement(ps);


					}
					catch(Exception e)
					{
						log.error(e.getMessage(), e);
						//e.printStackTrace();
					}
				}
				ctx.commit();

			}
			catch(EdmException e)
			{
				if(ctx!=null) ctx.setRollbackOnly();
				log.error(e.getMessage(), e);
			}

			//s.set

			return res;
		}

		private String parseExportConfirmation(List<String> ids)
		{
			String dataOdbioru = DateUtils.sqlDateTimeFormatNoSeconds.format(new Date());
			Element elem;
			Document doc = DocumentFactory.getInstance().createDocument();
			elem = doc.addElement("confirm_export");
			doc.setRootElement(elem);
			elem = elem.addElement("formularze");
			Element temp;
			for(String id:ids)
			{
				temp = elem.addElement("formularz");
				temp.addAttribute("idform", id);
				temp.addAttribute("data_odbioru", dataOdbioru);
			}
			return doc.asXML();
		}

		private void processExport(EUrzad_wymiana_danychStub eurzad) throws IOException, DocumentException, NoSuchAlgorithmException
		{
			EUrzad_wymiana_danychStub.ExportData ed = new EUrzad_wymiana_danychStub.ExportData();
			ed.setIdurzedu(idUrzedu);
			ed.setLogin(login);
			ed.setPass(password);

			/*ed.setIdurzedu("34");
			ed.setLogin("jwierzgala");
			ed.setPass("ComPan1#");*/

			EUrzad_wymiana_danychStub.ExportDataResponse edr = eurzad.ExportData(ed);
			File temp = File.createTempFile("eurzad", ".xml");
			OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(temp),"UTF-8");

			String xml = edr.get_return();
			//System.out.println(xml);
			fw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			fw.write(xml);
			fw.flush();
			fw.close();

			//temp = new File(Docusafe.getHome(),"export_data_compan.xml");
			//System.out.println("robie plik " + temp.getAbsolutePath());
			SAXReader saxReader = new SAXReader();
			Document document = saxReader.read(temp);
			Element elem = document.getRootElement();

			MessageDigest digest = MessageDigest.getInstance("MD5");
		    digest.update(elem.element("Eurzad").asXML().getBytes());
		    byte[] hash = digest.digest();
		    String md5 = HexUtils.convert(hash);
		    if(!elem.valueOf("@md5").equals(md5))
		    {

		    }
		    elem = elem.element("Eurzad");
		    List<String> processedIds = new ArrayList<String>();
		    List<Element> nadawcy = elem.elements("Nadawca");
		    for(Element n : nadawcy)
		    {
		    	//System.out.println("uteracja nadawcy");
		    	processedIds.addAll(processNadawcaElement(n));
		    }

		    String response = parseExportConfirmation(processedIds);

		    EUrzad_wymiana_danychStub.ConfirmDelivery cd = new EUrzad_wymiana_danychStub.ConfirmDelivery();
		    cd.setLogin(login);
			cd.setPass(password);
		    cd.setXMLdocument(response);

		    //wysya�anie potwierdzenia
		    eurzad.ConfirmDelivery(cd);
		}



		private void processImport(EUrzad_wymiana_danychStub eurzad) throws IOException, DocumentException, NoSuchAlgorithmException
		{
			EUrzad_wymiana_danychStub.ImportData id = new EUrzad_wymiana_danychStub.ImportData();
			id.setLogin(login);
			id.setPass(password);
			org.dom4j.io.XMLWriter writer = null;
			DocumentFactory factory = org.dom4j.DocumentFactory.getInstance();
			Document document = factory.createDocument();
			document.addDocType("eurzad", MaxusBipDriver.PUBLICID, MaxusBipDriver.SYSTEMID);
			Element eurzad_elem = document.addElement("eksport_eurzad");
			document.setRootElement(eurzad_elem);
			eurzad_elem.addAttribute("id_urzedu", idUrzedu);
			Element temp;
			Element komorki;
			Element paczka;

			paczka = eurzad_elem.addElement("paczka");
			paczka.addAttribute("data_publikacji", DateUtils.formatSqlDate(new Date()));
			komorki = paczka.addElement("komorki_org");
			paczka.addElement("nowe_konta");

			PreparedStatement ps = null;
			try
			{
				DSContext ctx = DSApi.openAdmin();
				ps = ctx.prepareStatement("select documentId from dso_eurzad where type=? and state=?");
				ps.setString(1,"out");
				ps.setString(2, "to_send");
				ResultSet rs = ps.executeQuery();
				Long docid;
				OutOfficeDocument doc;
				while(rs.next())
				{

					docid = rs.getLong("documentId");
					//System.out.println(docid);
					doc = (OutOfficeDocument)OutOfficeDocument.find(docid);
					processOutDocument(doc, paczka, komorki);
					markAsSend(docid);
				}
			}
			catch(Exception e)
			{
				log.error(e.getMessage(), e);
				//e.printStackTrace();
			}
			finally
			{
				DSApi.context().closeStatement(ps);
			}
			//System.out.println(document.asXML());
			log.debug(document.asXML());
			id.setXmldata(document.asXML());

			EUrzad_wymiana_danychStub.ImportDataResponse idr = eurzad.ImportData(id);

		}

		private void processOutDocument(OutOfficeDocument out, Element paczka, Element kom) throws EdmException
		{
			Element rel;
			Element temp;
			OfficeCase cas;
			temp = paczka.addElement("petent");
			temp.addAttribute("id", out.getInDocument().getSender().getWparam()+"");
			temp.addAttribute("pesel_regon", out.getInDocument().getSender().getPesel());
			if(out.getContainingCase()==null)
			{
				rel = temp.addElement("pisma_niepowiazane");
			}
			else
			{
				temp = temp.addElement("sprawy");
				cas = out.getContainingCase();
				temp = temp.addElement("sprawa");
				temp.addAttribute("id_kanc", cas.getId()+"");
				temp.addElement("nr_sprawy").setText(cas.getOfficeId()!=null?cas.getOfficeId():"");
				temp.addElement("data_zalozenia").setText(DateUtils.formatSqlDate(cas.getCtime()));
				temp.addElement("opis").setText(cas.getDescription()!=null?cas.getDescription():"");
				temp.addElement("sposob_zalatwienia");
				temp.addElement("stan_sprawy").setText(cas.getStatus().getName()!=null?cas.getStatus().getName():"");
				temp.addElement("planowany_termin_zalatwienia").setText(DateUtils.formatSqlDate(cas.getFinishDate()));
				temp.addElement("faktyczny_termin_zalatwienia");
				temp.addElement("id_komorki_org").setText(cas.getDivisionGuid()!=null?cas.getDivisionGuid():"");
				try
				{
					DSDivision div = DSDivision.find(cas.getDivisionGuid());
					Element komorka = kom.addElement("komorka_org");
					komorka.addElement("id").setText(div.getGuid());
					komorka.addElement("nazwa").setText(div.getName());
				}
				catch(Exception e)
				{
					log.error(e.getMessage(), e);
					//e.printStackTrace();
				}
				temp.addElement("nazwisko_pracownika").setText(cas.getClerk());
				rel = temp;
			}
			temp = rel.addElement("pismo");
			temp.addAttribute("id", out.getId()+"");
			temp.addElement("nr_pisma").setText(out.getOfficeNumber()+"");
			temp.addElement("data_pisma").setText(DateUtils.formatSqlDate(out.getCtime()));
			temp.addElement("planowany_termin_zalatwienia").setText(out.getFinishDate()!=null?DateUtils.formatSqlDate(out.getFinishDate()):"");
			temp.addElement("faktyczny_termin_zalatwienia");
			temp.addElement("stan_pisma").setText(out.getOfficeStatus()!=null?out.getOfficeStatus():"");
			temp.addElement("id_komorki_org").setText(out.getDivisionGuid()!=null?out.getDivisionGuid():"");
			temp.addElement("nazwisko_pracownika").setText(out.getClerk()!=null?out.getClerk():"");
			temp.addElement("opis").setText(out.getDescription()!=null?out.getDescription():"");
			temp.addElement("sposob_zalatwienia");
			temp.addElement("kierunek");
			temp = temp.addElement("zalaczniki");
			Element zal;
			Element plik;
			for(Attachment a:out.getAttachments())
			{
				zal = temp.addElement("zalacznik");
				zal.addAttribute("id", a.getId()+"");
				zal.addElement("numer").setText(a.getId()+"");
				zal.addElement("opis").setText(a.getDescription()!=null?a.getDescription():"");
				zal.addElement("data").setText(DateUtils.formatSqlDate(a.getCtime()));
				plik = zal.addElement("pliki").addElement("plik");
				//TODO: dodac obsluge przesylania tresci zalcznikow
			}
		}

	}

	public static void addOutgoingToEurzad(OutOfficeDocument out, Long inId)
	{
		PreparedStatement ps = null;
		try
		{
			ps = DSApi.context().prepareStatement("insert into dso_eurzad(documentId,modifyDate, type,state,inId) values(?,?,?,?,?)");
			ps.setLong(1, out.getId());
			ps.setTimestamp(2, new java.sql.Timestamp(new Date().getTime()));
			ps.setString(3, "out");
			ps.setString(4, "to_send");
			ps.setLong(5, inId);
			ps.executeUpdate();
		}
		catch(Exception e)
		{
			//dlaczego nie jest logowane????
			//e.printStackTrace();
			LogFactory.getLog(EUrzadService.class).error(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
	}

	private void markAsSend(Long id)
	{

		PreparedStatement ps = null;
		try
		{
			ps = DSApi.context().prepareStatement("update dso_eurzad set state=? where documentId=?");
			ps.setString(1, "sent");
			ps.setLong(2, id);
			ps.executeUpdate();
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
	}


}
