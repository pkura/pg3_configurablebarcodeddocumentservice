package pl.compan.docusafe.service.upload;

import pl.compan.docusafe.service.ServiceException;
/* User: Administrator, Date: 2005-10-07 10:20:11 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: InvalidTokenException.java,v 1.1 2005/10/10 14:56:10 lk Exp $
 */
public class InvalidTokenException extends ServiceException
{
    public InvalidTokenException(String message)
    {
        super(message);
    }

    public InvalidTokenException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
