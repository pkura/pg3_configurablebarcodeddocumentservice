package pl.compan.docusafe.service.upload;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.service.RuntimeProperty;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.ShUtils;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: UploadDriver.java,v 1.10 2009/06/26 08:53:10 mariuszk Exp $
 */
public class UploadDriver extends ServiceDriver implements Upload
{
    public static File tempDir;

    private Map tokens = new HashMap();
    private int tokenSerial;
    
    public File getTempDir() {
		return tempDir;
	}

	protected void start() throws ServiceException
    {
        int serial = 1;
        File temp;
        boolean mkdirs;
        do
        {
            temp = new File(Docusafe.getTemp(), Docusafe.getUploadPath() + "_upload_"+(serial++));
        }
        while (!(mkdirs=temp.mkdirs()) && serial < 1000);

        if (!mkdirs)
            throw new ServiceException("Nie mo�na utworzy� katalogu, usu� niepotrzebne " +
                "ju� katalogi "+
                new File(Docusafe.getTemp(), Docusafe.getUploadPath() + "_upload_").getAbsolutePath()+"*");

        tempDir = temp;

        Docusafe.printk("upload: u�ywanie katalogu "+tempDir.getAbsolutePath());
    }

    /**
     * Usuwa przechowywane pliki oraz utworzony na nie katalog.
     * @throws ServiceException
     */
    protected void stop() throws ServiceException
    {
        if (tempDir != null)
        {
            Docusafe.printk("upload: usuwanie katalogu "+tempDir.getAbsolutePath());

            ShUtils.delete(tempDir);

/*
            tempDir.listFiles(new FileFilter()
            {
                public boolean accept(File pathname)
                {
                    pathname.deleteOnExit();
                    return false;
                }
            });
            tempDir.delete();
*/
        }
    }

    // TODO: rekurencyjne usuwanie

    protected boolean canStop()
    {
        return false;
    }

    public RuntimeProperty[] getRuntimeProperties()
    {
        return new RuntimeProperty[] {
            new RuntimeProperty("Liczba zapami�tanych plik�w", new Integer(tokens.size())),
            new RuntimeProperty("Katalog", tempDir.getAbsolutePath())
        };
    }

    /* Upload */

    public String retain(File file, String name) throws ServiceException
    {
        if (name == null)
            throw new NullPointerException("name");
        if (!file.exists() || !file.isFile())
            throw new ServiceException(file.getAbsolutePath()+" nie istnieje " +
                "lub nie jest plikiem");

        File temp;

        try
        {
            temp = File.createTempFile("upload_", null, tempDir);

            FileChannel ifc = new FileInputStream(file).getChannel();
            FileChannel ofc = new FileOutputStream(temp).getChannel();
            ofc.transferFrom(ifc, 0, ifc.size());
            ifc.close();
            ofc.close();
        }
        catch (IOException e)
        {
            throw new ServiceException("Nie mo�na skopiowa� pliku "+file.getAbsolutePath(), e);
        }

        return createToken(file, name);
    }

    public String retain(File file) throws ServiceException
    {
        return retain(file, file.getName());
    }

    public File retrieveFile(String token) throws ServiceException, InvalidTokenException
    {
        return getNamedFile(token).file;
    }

    public String retrieveName(String token) throws ServiceException, InvalidTokenException
    {
        return getNamedFile(token).name;
    }

    public void free(String token) throws ServiceException, InvalidTokenException
    {
        freeToken(token);
    }

    private synchronized String createToken(File file, String name)
    {
        NamedFile nf = new NamedFile();
        nf.file = file;
        nf.name = name;
        String token = "token"+Integer.toHexString(tokenSerial++);
        tokens.put(token, nf);
        return token;
    }

    private void freeToken(String token) throws InvalidTokenException
    {
        NamedFile nf = (NamedFile) tokens.get(token);

        if (nf == null)
            throw new InvalidTokenException(token);

        nf.file.delete();
        tokens.remove(token);
    }

    private NamedFile getNamedFile(String token) throws InvalidTokenException
    {
        NamedFile nf = (NamedFile) tokens.get(token);

        if (nf == null)
            throw new InvalidTokenException(token);

        return nf;
    }

    private static class NamedFile
    {
        private File file;
        private String name;
    }


    private int multiSerial = 0;

    public synchronized String createMultiToken() throws ServiceException
    {
        if (!tempDir.canWrite())
            throw new ServiceException("Brak prawa do zapisu w katalogu "+tempDir);

        String token;
        while (new File(tempDir, (token = "dir" + (++multiSerial))).exists())
            ;

        log.debug("Utworzono multiToken: "+token);

        return token;
    }

    public void freeMultiToken(String token) throws ServiceException
    {
        File dir = new File(tempDir, token);
        if (dir.isDirectory())
            ShUtils.delete(dir);
    }

    private Map multiNames = new HashMap();

    public void retain(String multiToken, File file, String name)
        throws ServiceException, InvalidTokenException
    {
        File dir = new File(tempDir, multiToken);
        if (!dir.isDirectory() && !dir.mkdir())
            throw new InvalidTokenException("Nieprawid�owy token: "+multiToken);

        int serial = 0;
        Properties props = new Properties();
        try
        {
            File count = new File(dir, "count.properties");
            if (count.isFile())
            {
                InputStream is = new FileInputStream(count);
                props.load(is);
                org.apache.commons.io.IOUtils.closeQuietly(is);
                serial = Integer.parseInt(props.getProperty("serial", "0"));
            }
            serial++;
            props.setProperty("serial", String.valueOf(serial));
            OutputStream os = new FileOutputStream(count);
            props.store(os, "");
            os.close();
        }
        catch (IOException e)
        {
            throw new ServiceException(e.getMessage(), e);
        }

        NamedFile nf = new NamedFile();
        nf.file = file;
        nf.name = name;

        log.debug("Zapisywanie pliku "+file+" pod multiTokenem "+multiToken+"/"+serial);

        try
        {
            FileChannel ifc = new FileInputStream(file).getChannel();
            FileChannel ofc = new FileOutputStream(new File(dir, String.valueOf(serial))).getChannel();
            ofc.transferFrom(ifc, 0, ifc.size());
            ifc.close();
            ofc.close();
        }
        catch (IOException e)
        {
            throw new ServiceException(e.getMessage(), e);
        }

        multiNames.put(multiToken+"/"+serial, file.getName());
    }

    public boolean isMultiTokenValid(String multiToken) throws ServiceException
    {
        File dir = new File(tempDir, multiToken);
        return dir.isDirectory();
    }

    /**
     * Iterator zwracaj�cy klasy Map.Entry.  Kluczami s� nazwy plik�w,
     * warto�ciami obiekty File.
     */
    public Iterator retrieveMultiFiles(String multiToken) throws ServiceException, InvalidTokenException
    {
        File dir = new File(tempDir, multiToken);
        if (!dir.isDirectory())
            throw new InvalidTokenException("Nieznany token: "+multiToken);

        return new MultiIterator(multiToken,
            dir.listFiles(new FileFilter()
            {
                public boolean accept(File file)
                {
                    return file.getName().indexOf('.') < 0;
                }
            }));
    }

    private class MultiIterator implements Iterator
    {
        private String multiToken;
        private File[] files;
        private int index;

        public MultiIterator(String multiToken, File[] files)
        {
            this.multiToken = multiToken;
            this.files = files;
        }

        public boolean hasNext()
        {
            return index < files.length;
        }

        public Object next()
        {
            final File file = files[index++];
            final String name = (String) multiNames.get(multiToken+"/"+file.getName());

            return new Map.Entry() {
                public Object getKey()
                {
                    return name;
                }

                public Object getValue()
                {
                    return file;
                }

                public Object setValue(Object value)
                {
                    throw new UnsupportedOperationException();
                }
            };
        }

        public void remove()
        {
            throw new UnsupportedOperationException();
            
        }
    }
}
