package pl.compan.docusafe.service.upload;

import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceException;

import java.io.File;
import java.util.Iterator;
/* User: Administrator, Date: 2005-10-07 10:09:42 */

/**
 * Us�uga u�ywana do czasowego przechowywania plik�w dodanych przez
 * u�ytkownika.
 * <p>
 * Plik dodawany jest przy pomocy metody {@link #retain(java.io.File)}
 * zwracaj�cej token, kt�ry nast�pnie mo�e by� u�yty do odczytania pliku
 * lub jego oryginalnej nazwy.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Upload.java,v 1.3 2006/02/20 15:42:29 lk Exp $
 */
public interface Upload extends Service
{
    public static final String NAME = "upload";

    /**
     * Zapisuje przekazany plik i zwraca jego symboliczn� nazw� (token).
     * @param file
     * @return Token, kt�rym nale�y pos�ugiwa� si� podczas odzyskiwania
     * pliku.
     */
    String retain(File file) throws ServiceException;

    /**
     * Zapisuje przekazany plik i zwraca jego symboliczn� nazw� (token).
     * @param file
     * @param name Nazwa pliku, je�eli ma by� inna ni� file.getName()
     * @return Token, kt�rym nale�y pos�ugiwa� si� podczas odzyskiwania
     * pliku.
     */
    String retain(File file, String name) throws ServiceException;

    /**
     * Zwraca plik zwi�zany z tokenem.  Nazwa pliku jest inna
     * ni� w momencie dodawania go metod� retain().
     * @param token
     * @throws ServiceException
     * @throws InvalidTokenException
     */
    File retrieveFile(String token) throws ServiceException, InvalidTokenException;

    /**
     * Zwraca oryginaln� nazw� przekazanego pliku zwi�zanego z tokenem.
     * @param token
     * @return
     * @throws ServiceException
     * @throws InvalidTokenException
     */
    String retrieveName(String token) throws ServiceException, InvalidTokenException;

    /**
     * Usuwa plik; od tej pory token jest niewa�ny i pr�ba jego u�ycia
     * spowoduje rzucenie wyj�tku InvalidTokenException.
     * @param token
     * @throws ServiceException
     */
    void free(String token) throws ServiceException, InvalidTokenException;

    String createMultiToken() throws ServiceException;

    void freeMultiToken(String token) throws ServiceException;

    void retain(String multiToken, File file, String name) throws ServiceException,
        InvalidTokenException;

    Iterator retrieveMultiFiles(String multiToken) throws ServiceException, InvalidTokenException;

    public boolean isMultiTokenValid(String multiToken) throws ServiceException;
}
