package pl.compan.docusafe.service;

/* User: Administrator, Date: 2005-04-20 13:08:02 */

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Service.java,v 1.8 2005/10/10 14:56:11 lk Exp $
 */
public interface Service
{
    String getDriverId();
    Property[] getProperties();
    RuntimeProperty[] getRuntimeProperties();
    Command[] getCommands();
    Property getProperty(String id) throws PropertyNotFoundException;
    boolean hasConsole();
}
