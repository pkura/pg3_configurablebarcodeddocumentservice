package pl.compan.docusafe.service;
/* User: Administrator, Date: 2005-10-07 12:46:08 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: RuntimeProperty.java,v 1.1 2005/10/10 14:56:11 lk Exp $
 */
public class RuntimeProperty
{
    private String name;
    private Object value;
    private Class type;

    public RuntimeProperty(String name, Object value)
    {
        if (name == null)
            throw new NullPointerException("name");
        if (value == null)
            throw new NullPointerException("value");
        this.name = name;
        this.value = value;
        this.type = value.getClass();
    }

    public String getName()
    {
        return name;
    }

    public Object getValue()
    {
        return value;
    }

    public Class getType()
    {
        return type;
    }
}
