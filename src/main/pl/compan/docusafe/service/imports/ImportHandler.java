package pl.compan.docusafe.service.imports;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.imports.ImportedDocumentInfo;

/**
 * Klasa bazowa dla obs�ugi importu dokument�w okre�lonego typu z pliku csv(by� mo�e p�niej tak�e xml).
 * 
 *  @author docusafe
 *
 */

public abstract class ImportHandler
{
    private static Map<String, ImportHandler> instances = new HashMap<String, ImportHandler>();
    
    /**
     * Przygotowuje informacje do importu pojedynczego dokumentu(.
     * Uwaga: Zwracany obiekt ImportedDocumentInfo nie jest umieszczany w bazie, poniewa� nie ma ustawionego
     * pola importedFileInfo. Nale�y ustawi� warto�� tego pola na zewn�trz oraz wywo�a� dla obiektu metod� create(). 
     * 
     * @param file plik z danymi importu(csv)
     * @param line linia pliku csv
     * @throws EdmException
     */
    public abstract ImportedDocumentInfo createDocumentInfo(String line, int lineCount) throws EdmException;

    /**
     * Wykonuje czynno�ci zwi�zane z importem pojedynczego dokumentu.
     *  
     * @param importedDocumentInfo
     * @param line linia pliku csv
     * @return true - je�li import si� powi�d�
     */
    public abstract void createDocument(ImportedDocumentInfo importedDocumentInfo, String line) throws EdmException;

    public static final ImportHandler getInstance(String cn)
    {
        ImportHandler handler = instances.get(cn);
        if (handler == null)
        {
            //TODO: cn powinien by� mapowany na nazw� klasy(tymczasowo wpisane "na twardo"
            String className = null;
            if (cn.equals(DocumentLogicLoader.ALD_KIND))
                className = "pl.compan.docusafe.service.imports.ALDImportHandler";
            else if (cn.equals(DocumentLogicLoader.DF_KIND))
                className = "pl.compan.docusafe.service.imports.DfImportHandler";
            else if (cn.equals(DocumentLogicLoader.DP_KIND))
                className = "pl.compan.docusafe.service.imports.DpImportHandler";
            else if (cn.equals(DocumentLogicLoader.DAA_KIND))
                className = "pl.compan.docusafe.service.imports.DaaImportHandler";
            else if (cn.equals(DocumentLogicLoader.DC_KIND))
                className = "pl.compan.docusafe.service.imports.DcImportHandler";
            else if (cn.equals(DocumentLogicLoader.ROCKWELL_KIND))
                className = "pl.compan.docusafe.service.imports.RockwellImportHandler";
            
            try
            {
                Class handlerClass = Class.forName(className);
                Constructor constructor = handlerClass.getConstructor();
                handler = (ImportHandler) constructor.newInstance();
                instances.put(cn, handler);
            }
            catch (ClassNotFoundException e)
            {
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
            
        return handler;
    }
    
}
