package pl.compan.docusafe.service.imports;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.primitives.Ints;
import org.apache.commons.io.FileUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.parametrization.pg.importer.PgImageImportService;
import pl.compan.docusafe.parametrization.pg.importer.PgImageImporter;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.imports.dsi.DSIAttribute;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DefaultDSIHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.RemarksTabAction;
import pl.compan.docusafe.webwork.FormFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Comparator;

/**
 * <p>Handler importu z serwisu <code>ImageImportService</code>. Logika jak w <code>DefaultDSIHandler</code> opr�cz
 * akcji po zaimportowaniu - przenosimy w tym wypadku plik za��cznika do katalogu backupowego.</p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class ImageImportDSIHandler extends DefaultDSIHandler {

    private static final Logger log = LoggerFactory.getLogger(ImageImportDSIHandler.class);

    private static final String afterImportPath = Docusafe.getHome() + "/import/after_import";
    private static final String LPARAM_VALUE = "ImageImport";

    @Override
    public String getDockindCn() {
        return DocumentLogicLoader.NORMAL_KIND;
    }

    /**
     * Renames file via <code>ImageImportService.removeTimestamp</code> method.
     *
     * @param file
     * @throws IOException
     */
    protected File removeTimestamp(File file) throws IOException {
        String newFilename = ImageImportService.removeTimestamp(file.getName());
        File newFile = new File(file.getParentFile(), newFilename);
        FileUtils.moveFile(file, newFile);
        return newFile;
    }

    @Override
    protected void prepareImport() throws Exception {

        // sprawdzamy czy dokument o podanym barkodzie ju� istnieje
        // je�li tak to go updatujemy, w przeciwnym razie tworzymy nowy dokument

        String barcodeCn = ServiceDriver.getServicePropertyOrDefault(PgImageImportService.class.toString(), "barcodeFieldCn", PgImageImporter.DOC_BARCODE_CN);
        Optional<String> barcode = dsiBean.getAttributeValueByCn(barcodeCn);

        if(barcode.isPresent()) {
            importDocument = getDocumentByBarcode(barcodeCn, barcode.get());
        }

        // usuwamy timestamp z nazw plik�w
        if(attachmentsFile != null && attachmentsFile.size() > 0) {
            for(int i = 0; i < attachmentsFile.size(); i++) {
                FormFile formFile = attachmentsFile.get(i);
                File oldFile = formFile.getFile();
                File newFile = removeTimestamp(oldFile);
                FormFile newFormFile = new FormFile(newFile, newFile.getName(), formFile.getContentType());

                attachmentsFile.set(i, newFormFile);
            }

        }
    }

    private Document getDocumentByBarcode(String barcodeCn, String barcode) throws EdmException {

        Field field = documentKind.getFieldByCn(barcodeCn);

        DockindQuery dockindQuery = new DockindQuery(0, 1);
        dockindQuery.setDocumentKind(documentKind);
        dockindQuery.setCheckPermissions(false);
//        dockindQuery.addForceSearchEq(barcodeCn);
//        dockindQuery.stringField(field, barcode);

        dockindQuery.barcode(barcode, documentKind.getDockindInfo().isForType(DocumentType.INCOMING));

        SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);

        Document[] documents = searchResults.results();

        if(documents.length == 0) {
            return null;
        } else {
            return documents[0];
        }
    }

    public void actionAfterCreate(Long documentId, DSIBean dsiB)throws Exception
    {
        // ustaw pole czyCzystopis
        Document document = Document.find(documentId);
        document.setCzyCzystopis(false);

        // przenie� za��czniki do katalogu "after_import"

        if(attachmentsFile != null) {
            for(FormFile formFile: attachmentsFile) {
                moveToAfterImport(formFile);
            }
        }

        // pole uwagi
        String remarksFieldCn = ServiceDriver.getServicePropertyOrDefault("ImageImportService", "remarksFieldCn", ImageImportService.DOC_REMARKS_CN);

        Optional<String> remarks = getDSIValue(dsiBean, remarksFieldCn);

        if(remarks.isPresent()) {
            RemarksTabAction.addRemark(document, remarks.get());
        }

//        File file = new File(dsiB.getFilePath());
//        String newFilename = ImageImportService.putTimestamp(file.getName());
//
//        File destFile = new File(afterImportPath, newFilename);
//        log.debug("[actionAfterCreate] moving file from = {}, to = {}", file.getAbsolutePath(), destFile.getAbsolutePath());
//
//        try {
//            FileUtils.moveFile(file, destFile);
//        } catch(Exception ex) {
//            log.error("[actionAfterCreate] file move error from = {}, to = {}", file.getAbsolutePath(), destFile.getAbsolutePath(), ex);
//        }
    }

    private void moveToAfterImport(FormFile formFile) {
        File file = formFile.getFile();
        String newFilename = ImageImportService.putTimestamp(file.getName());

        File destFile = new File(afterImportPath, newFilename);
        log.debug("[actionAfterCreate] moving file from = {}, to = {}", file.getAbsolutePath(), destFile.getAbsolutePath());

        try {
            FileUtils.moveFile(file, destFile);
        } catch(Exception ex) {
            log.error("[actionAfterCreate] file move error from = {}, to = {}", file.getAbsolutePath(), destFile.getAbsolutePath(), ex);
        }
    }

    private Optional<String> getDSIValue(DSIBean bean, String fieldCn) {
        for(DSIAttribute dsiAttribute: bean.getAttributes()) {
            String attributeCn = dsiAttribute.getAttributeCn();

            if(Objects.equal(attributeCn, fieldCn)) {
                return Optional.fromNullable(dsiAttribute.getAttributeValue());
            }
        }

        return Optional.absent();
    }

    @Override
    protected void addAttachment(Document document, FormFile formFile) throws EdmException, FileNotFoundException {
        // try to find attachment with lparam = "ImageImport"
        // with last revision same as formFile.contentType
        Optional<Attachment> attOpt = getAttachmentForUpdate(document, formFile);

        if(! attOpt.isPresent()) {
            createAttachment(document, formFile);
        } else {

            File file = formFile.getFile();
            FileInputStream is = new FileInputStream(file);

            try {
                attOpt.get().createRevision(is, Ints.checkedCast(file.length()), file.getName());
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    log.error("[addAttachment] Cannot close stream, doc.id = {}, formFile = {}", document.getId(), formFile);
                }
            }

        }
    }

    protected void createAttachment(Document document, FormFile formFile) throws EdmException {
        Attachment attachment = new Attachment(formFile.getName());
        attachment.setLparam(LPARAM_VALUE);
        document.createAttachment(attachment);
        attachment.createRevision(formFile.getFile()).setOriginalFilename(formFile.getName());
    }

    /**
     * <p>
     *     Znajduje attachment taki, �e:
     *     <ul>
     *         <li>attachment.lparam = ImageImport</li>
     *         <li>attachment.getLatestRevision().contentType = formFile.contentType</li>
     *     </ul>
     * </p>
     *
     *
     * @param document
     * @param formFile
     * @return
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     */
    public Optional<Attachment> getAttachmentForUpdate(Document document, FormFile formFile) throws EdmException {

        final String contentType = formFile.getContentTypeOrGuess();

        ImmutableList<Attachment> attachments = FluentIterable.from(document.getAttachments()).filter(new Predicate<Attachment>() {
            @Override
            public boolean apply(Attachment att) {
                return Objects.equal(att.getLparam(), LPARAM_VALUE);
            }
        }).filter(new Predicate<Attachment>() {
            @Override
            public boolean apply(Attachment attachment) {
                ImmutableList<AttachmentRevision> revs = FluentIterable.from(attachment.getRevisions()).toSortedList(new Comparator<AttachmentRevision>() {
                    @Override
                    public int compare(AttachmentRevision o1, AttachmentRevision o2) {
                        return o1.getId().compareTo(o2.getId());
                    }
                });

                return revs.size() > 0 && Objects.equal(revs.get(0).getMime(), contentType);
            }
        }).toList();

        if(attachments.size() > 0) {
            return Optional.of(attachments.get(0));
        } else {
            return Optional.absent();
        }
    }


}
