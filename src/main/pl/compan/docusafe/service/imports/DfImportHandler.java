package pl.compan.docusafe.service.imports;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.dictionary.DfInst;
import pl.compan.docusafe.core.dockinds.logic.DfLogic;
import pl.compan.docusafe.core.imports.ImportManager;
import pl.compan.docusafe.core.imports.ImportedDocumentInfo;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;

public class DfImportHandler extends ImportHandler
{
    private static final int FIELDS_COUNT = 17;
    
    public DfImportHandler()
    {        
    }

    @Override
    public void createDocument(ImportedDocumentInfo importedDocumentInfo, String line) throws EdmException
    {
        DocumentKind dockind = DocumentKind.findByCn("df");
        
        // sprawdzam poprawnosc pliku tiff
        //File tiffFile = new File(importedDocumentInfo.getImportedFileInfo().getTiffDirectory() + File.pathSeparator + importedDocumentInfo.getTiffName());
        File tiffFile = new File(importedDocumentInfo.getImportedFileInfo().getTiffDirectory() + "/" + importedDocumentInfo.getTiffName());
        if (tiffFile == null || !tiffFile.exists())
        {
            throw new EdmException("Podano niepoprawny plik tiff");
        }

        String[] fields = line.split("\\s*~\\s*", FIELDS_COUNT);
        
        int i;
        for (i = 0; i < fields.length; ++i)
        {
            int f = fields[i].indexOf('"');
            int l = fields[i].lastIndexOf('"');
            if (f >= 0 && f != l)
                fields[i] = fields[i].substring(f + 1, l);
        }

        Document newDocument = new Document("", "");
        
        try
        {
            Map<String, Object> values = new HashMap<String, Object>();
            EnumItem eit = null; 
            //eit = dockind.getFieldByCn("KLASA").getEnumItemByTitle(fields[0]);
            Integer klasaId = Integer.parseInt(fields[0]);
            values.put("KLASA", klasaId);
            Integer typId = Integer.parseInt(fields[1]);
            //eit = ((EnumRefField) dockind.getFieldByCn("TYP")).getEnumItemByTitle(fields[1], eit.getId());
            values.put("TYP", typId);
            if(TextUtils.trimmedStringOrNull(fields[2])!=null)
                values.put("DATA", DateUtils.sqlDateFormat.parse(fields[2]));
            
            if(TextUtils.trimmedStringOrNull(fields[3])!=null)
                values.put("INVOICE_NUMBER", fields[3]);
            
            
            Object[] multipleValues = new Object[5];
            for(int j=0, counter=0; j<5; j++)
            {
                if (fields[4+j] != null)
                    multipleValues[counter++] = fields[4+j];
            }
            values.put(DfLogic.NUMERY_TRANSAKCJI_FIELD_CN, multipleValues);
            
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("name", fields[9]);
            params.put("nip", fields[12]);
            List<DfInst> instList = DfInst.find(params);
            DfInst dfInst = null;
            
            if (instList.size() == 0)
            {
                dfInst = new DfInst();
                dfInst.setName(fields[9]);
                dfInst.setNip(fields[12]);
                dfInst.create();
            }
            else
            {
                dfInst = instList.get(0);
            }
            values.put("INST", dfInst.getId());
            if (TextUtils.trimmedStringOrNull(fields[10]) != null)
            {
                eit = dockind.getFieldByCn("OKRESROK").getEnumItemByTitle(fields[10]);
                values.put("OKRESROK", eit.getId());
            }

            
            if (TextUtils.trimmedStringOrNull(fields[11]) != null)
            {
                 eit = dockind.getFieldByCn("OKRESMIESIAC").getEnumItem(Integer.parseInt(fields[11]));
                 values.put("OKRESMIESIAC", eit.getId());
            }
            
            values.put("NIP", fields[12]);
            values.put("NUMER_AGENTA", fields[13]);
            
            if (TextUtils.trimmedStringOrNull(fields[14]) != null)
            {
                eit = dockind.getFieldByCn("NUMER_RACHUNKU").getEnumItemByTitle(fields[14]);
                values.put("NUMER_RACHUNKU", eit.getId());
            }
            
            EnumItem stat = dockind.getFieldByCn("STATUS").getEnumItemByCn("archiwalny");
            values.put("STATUS", stat.getId());

            dockind.logic().validate(values, dockind, null);

            newDocument.setFolder(Folder.getRootFolder());

            newDocument.create();
            newDocument.setDocumentKind(dockind);
            
            Long newDocumentId = newDocument.getId();

            dockind.set(newDocumentId, values);

            dockind.logic().archiveActions(newDocument, DocumentKindsManager.getType(newDocument));
            dockind.logic().documentPermissions(newDocument);
            
            Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
            newDocument.createAttachment(attachment);
            attachment.createRevision(tiffFile).setOriginalFilename(fields[15]);
            
        }
        catch (ValidationException e)
        {
            throw new EdmException("B��d walidacji danych: " + e.getMessage());
        }
        catch (EdmException e)
        {
            throw e;
        }
        catch (Throwable e)
        {
            throw new EdmException("B��d podczas importu dokumentu: " + e.getMessage());
        }

        Box box;
        try
        {
            box = Box.findByName(dockind.getProperties().get(DocumentKind.BOX_LINE_KEY),importedDocumentInfo.getBoxNumber());
        }
        catch (EntityNotFoundException e)
        {
            // takie pudlo nie istnieje - tworze je
            box = new Box(importedDocumentInfo.getBoxNumber().toUpperCase());
            box.setOpen(true);
            box.setOpenTime(new Date());
            box.setLine(dockind.getProperties().get(DocumentKind.BOX_LINE_KEY));
            box.create();
            box.setCloseTime(new Date());
        }
        newDocument.setBox(box);
        
        // zapamietujemy id dokumentu, ktory utworzylismy
        importedDocumentInfo.setDocumentId(newDocument.getId());
        
    }

    @Override
    public ImportedDocumentInfo createDocumentInfo(String line, int lineCount) throws EdmException
    {
        String[] fields = line.split("\\s*~\\s*");
        int i;
        for (i = 0; i < fields.length; ++i)
        {
            int f = fields[i].indexOf('"');
            int l = fields[i].lastIndexOf('"');
            if (f >= 0 && f != l)
                fields[i] = fields[i].substring(f + 1, l);
        }
        ImportedDocumentInfo importedDocumentInfo = new ImportedDocumentInfo();
        importedDocumentInfo.setStatus(ImportManager.STATUS_WAITING);
        importedDocumentInfo.setLineNumber(lineCount);
        
        if (fields.length == FIELDS_COUNT)
        {
            importedDocumentInfo.setBoxNumber(fields[16]);
            //importedDocumentInfo.setDate(new java.util.Date());
            
            Date date = ImportManager.parseDateValidated(fields[2]);
            if (date != null)
                importedDocumentInfo.setDate(date);
            /*else
            {
                importedDocumentInfo.setStatus(ImportManager.STATUS_ERROR);
                importedDocumentInfo.setErrorInfo("Podana data jest niepoprawna");
            }
             **/
            importedDocumentInfo.setTiffName(fields[15]);
        }
        else
        {
            importedDocumentInfo.setProcessedTime(new Date());
            importedDocumentInfo.setStatus(ImportManager.STATUS_DONE);
            importedDocumentInfo.setResult(ImportManager.RESULT_ERROR);
            importedDocumentInfo.setErrorInfo("Niepoprawnie opisany dokument - oczekiwano dok�adnie " + FIELDS_COUNT + " warto�ci (by�o " + fields.length + ")");
        }

        return importedDocumentInfo;
    }

}
