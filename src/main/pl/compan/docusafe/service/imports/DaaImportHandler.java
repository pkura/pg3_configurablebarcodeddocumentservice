package pl.compan.docusafe.service.imports;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.EnumRefField;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgencja;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgent;
import pl.compan.docusafe.core.imports.ImportManager;
import pl.compan.docusafe.core.imports.ImportedDocumentInfo;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.TextUtils;

public class DaaImportHandler extends ImportHandler
{
    private static final int FIELDS_COUNT = 10;
    
    public DaaImportHandler()
    {        
    }

    @Override
    public void createDocument(ImportedDocumentInfo importedDocumentInfo, String line) throws EdmException
    {
        DocumentKind dockind = DocumentKind.findByCn(DocumentLogicLoader.DAA_KIND);
        
        // sprawdzam poprawnosc pliku tiff
        File tiffFile = new File(importedDocumentInfo.getImportedFileInfo().getTiffDirectory() + "/" + importedDocumentInfo.getTiffName());
        if (tiffFile == null || !tiffFile.exists())
        {
            throw new EdmException("Podano niepoprawny plik tiff");
        }

        String[] fields = line.split("\\s*;\\s*", FIELDS_COUNT);
        int i;
        for (i = 0; i < fields.length; ++i)
        {
            int f = fields[i].indexOf('"');
            int l = fields[i].lastIndexOf('"');
            if (f >= 0 && f != l)
                fields[i] = fields[i].substring(f + 1, l);
            if (fields[i].equals("BRAK"))
                fields[i] = "";
        }

        Document newDocument = new Document("", "");
        
        try
        {
            Map<String, Object> values = new HashMap<String, Object>();

            Integer typId = Integer.parseInt(fields[3]);
            values.put("TYP_DOKUMENTU", typId);

            EnumRefField erf = (EnumRefField) dockind.getFieldByCn("TYP_DOKUMENTU");
            Integer kategoriaId = (Integer) erf.getEnumItemDiscriminator(typId);
            values.put("KATEGORIA", kategoriaId);

            if(TextUtils.trimmedStringOrNull(fields[2]) != null)
                values.put("DATA_WPLYWU", DateUtils.sqlDateFormat.parse(fields[2]));
            
            //Znajd� agenta
            DaaAgent daaAgent = null;
            DaaAgencja daaAgencja = null;
            Long agencjaId = null;
            try
            {
                agencjaId = Long.parseLong(fields[6]);
                daaAgencja = new DaaAgencja().find(agencjaId);
            }
            catch (Exception e)
            {     
            	//throw new EdmException("Nie odnaleziono agencji" + e.getMessage());
            }
            
            boolean hasImie = fields[9] != null && fields[9].length() > 0; 
            boolean hasNazwisko = fields[8] != null && fields[8].length() > 0; 
            boolean hasNumer = fields[7] != null && fields[7].length() > 0; 
            boolean agentGiven = hasImie || hasNazwisko || hasNumer;

            //dla kategorii innej ni� Dokumenty specjalisty/OWCA agent jest ignorowany
            agentGiven = agentGiven && kategoriaId == 10;
            
            if (agentGiven)
            {
                QueryForm form = new QueryForm(0, 10);
                if (hasImie)
                    form.addProperty("imie", fields[9]);
                if (hasNazwisko)
                    form.addProperty("nazwisko", fields[8]);
                if (hasNumer)
                    form.addProperty("numer", fields[7]);
                if (daaAgencja != null)
                    form.addProperty("agencja", daaAgencja);
                SearchResults<? extends DaaAgent> results = DaaAgent.search(form);
                if (results.totalCount() > 0)
                    daaAgent = results.next();
            }

            if (daaAgent == null && daaAgencja == null)
            {
                throw new EdmException("Nie podano istniej�cej agencji" + 
                    (agencjaId != null ? "(podane id agencji: " + agencjaId + ")" : ""));
            }
            
            if (daaAgent != null && daaAgencja == null)
            {
                if (daaAgent.getAgencja() != null)
                    daaAgencja = daaAgent.getAgencja();
            }
            else if (daaAgent == null && daaAgencja != null)
            {
                if (agentGiven)
                {
                    daaAgent = new DaaAgent();
                    if (hasImie)
                        daaAgent.setImie(fields[9]);
                    if (hasNazwisko)
                        daaAgent.setNazwisko(fields[8]);
                    if (hasNumer)
                        daaAgent.setNumer(fields[7]);
                    daaAgent.setAgencja(daaAgencja);
                    daaAgent.create();
                }
            }   
            
            if (daaAgent != null)
                values.put("AGENT", daaAgent.getId());
            if (daaAgencja != null)
                values.put("AGENCJA", daaAgencja.getId());
            
            Integer klasaId = null;
            if (fields[4].length() > 0)
            {
                klasaId = Integer.parseInt(fields[4]);
                values.put("KLASA_RAPORTU", klasaId);
            }
            
            Integer rodzajId = null;
            if (fields[5].length() > 0)
            {
                rodzajId = Integer.parseInt(fields[5]);
                values.put("RODZAJ_SIECI", rodzajId);
            }
            
            values.put("STATUS", 10); // status: 10 = Przyj�ty

            dockind.logic().validate(values, dockind, null);

            newDocument.setFolder(Folder.getRootFolder());

            newDocument.create();
            newDocument.setDocumentKind(dockind);
            
            Long newDocumentId = newDocument.getId();

            dockind.set(newDocumentId, values);

            dockind.logic().archiveActions(newDocument, DocumentKindsManager.getType(newDocument));
            dockind.logic().documentPermissions(newDocument);
            
            Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
            newDocument.createAttachment(attachment);
            //attachment.createRevision(tiffFile).setOriginalFilename(fields[7]);
            attachment.createRevision(tiffFile).setOriginalFilename(importedDocumentInfo.getTiffName());
            
            
        }
        catch (ValidationException e)
        {
            throw new EdmException("B��d walidacji danych: " + e.getMessage());
        }
        catch (EdmException e)
        {
            throw e;
        }
        catch (Throwable e)
        {
            throw new EdmException("B��d podczas importu dokumentu: " + e.getMessage());
        }

        Box box;
        try
        {
            box = Box.findByName(dockind.getProperties().get(DocumentKind.BOX_LINE_KEY),importedDocumentInfo.getBoxNumber());
        }
        catch (EntityNotFoundException e)
        {
            // takie pudlo nie istnieje - tworze je
            box = new Box(importedDocumentInfo.getBoxNumber().toUpperCase());
            box.setOpen(true);
            box.setOpenTime(new Date());
            box.setLine(dockind.getProperties().get(DocumentKind.BOX_LINE_KEY));
            box.create();
            box.setCloseTime(new Date());
        }
        newDocument.setBox(box);
        
        // zapamietujemy id dokumentu, ktory utworzylismy
        importedDocumentInfo.setDocumentId(newDocument.getId());
        
    }

    @Override
    public ImportedDocumentInfo createDocumentInfo(String line, int lineNumber) throws EdmException
    {
        String[] fields = line.split("\\s*;\\s*", FIELDS_COUNT);
        int i;
        for (i = 0; i < fields.length; ++i)
        {
            int f = fields[i].indexOf('"');
            int l = fields[i].lastIndexOf('"');
            if (f >= 0 && f != l)
                fields[i] = fields[i].substring(f + 1, l);
            if (fields[i].equals("BRAK"))
                fields[i] = "";
        }
        
        //Je�eli pierwsza linia ma 10 p�l i pole Typ dokumentu nie jest liczb�
        //to linia jest traktowana jako linia z nag��wkami i jest pomijana
        if (lineNumber == 1 && fields.length == FIELDS_COUNT && !fields[3].matches("\\d+"))
        {
            return null;
        }
        
        ImportedDocumentInfo importedDocumentInfo = new ImportedDocumentInfo();
        importedDocumentInfo.setStatus(ImportManager.STATUS_WAITING);
        importedDocumentInfo.setLineNumber(lineNumber);
        
        if (fields.length == FIELDS_COUNT)
        {
            importedDocumentInfo.setBoxNumber(fields[1]);
            //importedDocumentInfo.setDate(new java.util.Date());
            
            try
            {
            Date date = ImportManager.parseDateValidated(fields[2]);
            if (date != null)
                importedDocumentInfo.setDate(date);
            importedDocumentInfo.setTiffName(fields[0]);
            }
            catch (Exception e)
            {
                
            }
        }
        else
        {
            importedDocumentInfo.setProcessedTime(new Date());
            importedDocumentInfo.setStatus(ImportManager.STATUS_DONE);
            importedDocumentInfo.setResult(ImportManager.RESULT_ERROR);
            importedDocumentInfo.setErrorInfo("Niepoprawnie opisany dokument - oczekiwano dok�adnie " + FIELDS_COUNT + " warto�ci (by�o " + fields.length + ")");
        }

        return importedDocumentInfo;
    }

}
