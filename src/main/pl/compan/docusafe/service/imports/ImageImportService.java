package pl.compan.docusafe.service.imports;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.service.*;
import pl.compan.docusafe.service.barcodes.recognizer.ZxingUtils;
import pl.compan.docusafe.service.imports.dsi.DSIAttribute;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIManager;
import pl.compan.docusafe.service.imports.dsi.ImportHelper;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import static pl.compan.docusafe.service.barcodes.recognizer.ZxingUtils.decodeBarcode;

/**
 * <p>Serwis importuj�cy dokumenty (pisma) z plik�w pdf z zasobu.</p>
 *
 * <b>Zasada dzia�ania</b>
 *
 * <p>
 *     Co okre�lony czas serwis skanuje katalog <code>HOME/import/before_import</code>.
 *     Ka�dy plik z tego katalogu serwis zaci�ga na kolejk�, przerzuca do katalogu <code>HOME/import/do_import</code>
 *     i wrzuca do docusafowego DSI Importera z t� �cie�k�. Nast�pnie DSI Importer zaci�ga plik z <code>do_import</code>
 *     jako pismo, po czym po udanej operacji plik jest przenoszony do katalogu <code>HOME/import/after_import</code>.
 * </p>
 *
 * <p>
 *     Serwis ma mo�liwo�� odczytywania barkod�w z obrazk�w (za��cznik�w). Nale�y skonfigurowa� addition property
 *     o nazwie <code>service.property.ImageImportService.barcodeFormats</code> w formie listy oddzielonej przecinkami
 *     zawieraj�cej nazwy z enuma {@link BarcodeFormat}. <br>
 *     Na przyk�ad: <code>service.property.ImageImportService.barcodeFormats = CODE_128,QR_CODE</code>
 * </p>
 *
 * <p>
 *     Ze wzgl�du na konstrukcj� DSI Importera nie da�o si� zrobi�, �eby po b��dzie DSI Importera plik by� przenoszony
 *     do katalogu <code>failed_import</code>. Kwestia refactoringu, na kt�ry nie starczy�o czasu. W tej chwili plik zostaje w
 *     <code>do_import</code> (ale nie jest zaci�gany ponownie na kolejk� DSI Importera).
 * </p>
 *
 * <b>Konfiguracja</b>
 * <p>
 *     W <code>adds.properties</code>:
 *     <ul>
 *         <li>
 *             <code>service.period.ImageImportService</code> - okres w sekundach, czyli co ile sekund b�dzie skanowany katalog before_import
 *         </li>
 *         <li>
 *             <code>service.delay.ImageImportService</code> - op�nienie w sekundach, czyli po uruchomieniu Docusafe ile sekund czekamy do pierwszego uruchomienia serwisu
 *         </li>
 *     </ul>
 * </p>
 *
 * </p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class ImageImportService extends IterateTimerServiceDriver {
    private static final Logger log = LoggerFactory.getLogger(ImageImportService.class);

    private static final String beforeImportPath = Docusafe.getHome() + "/import/before_import";
    private static final String importPath = Docusafe.getHome() + "/import/do_import";

    public static final String DOC_REMARKS_CN = "DOC_REMARKS";

    /**
     * Regexp for file extension
     */
    public static final String FILE_EXT_REGEXP = "(\\.[A-Za-z0-9]+)$";
    public static final String FILE_TIMESTAMP_REGEXP = "_[0-9]+"+FILE_EXT_REGEXP;

    public ImageImportService() {
        setTask(new Task(beforeImportPath));
    }

    /**
     * <p>Adds timestamp to filename</p>
     *
     * <p>
     *     Example:<br>
     *     <code>putTimestamp("myFile.txt")</code> returns <code>"myFile_123123123.txt"</code>
     * </p>
     *
     * @param filename
     * @return
     */
    public static String putTimestamp(String filename) {
        Long timestamp = new Date().getTime();

        String replace = "_" + timestamp.toString() + "$1";

        return filename.replaceFirst(FILE_EXT_REGEXP, replace);
    }

    /**
     * <p>Removes timestamp from filename added by <code>putTimestamp()</code></p>
     *
     * <p>
     *     Example:<br>
     *     <code>removeTimestamp("myFile_123123312.txt")</code> returns <code>"myFile.txt"</code>
     * </p>
     *
     * @param filename
     * @return
     */
    public static String removeTimestamp(String filename) {
        return filename.replaceFirst(FILE_TIMESTAMP_REGEXP, "$1");
    }

    public class Task extends FileGroupTimerTask {

        private Map<DecodeHintType, Object> hints = new HashMap<DecodeHintType, Object>();

        public Task(String folderPath) {
            super(folderPath);
            setIterateInContext(true);
        }


        @Override
        public void beforeRun() throws Exception {
            super.beforeRun();

            // initialize barcode formats
            initBarcodeFormats();
        }

        private void initBarcodeFormats() {
            Collection<String> stringBarcodeFormats = getServicePropertyAsList("barcodeFormats");

            Collection<BarcodeFormat> barcodeFormats = Collections2.transform(stringBarcodeFormats, new Function<String, BarcodeFormat>() {
                @Override
                public BarcodeFormat apply(String name) {
                    return BarcodeFormat.valueOf(name);
                }
            });

            hints = new HashMap<DecodeHintType, Object>();
//            hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
            hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
            hints.put(DecodeHintType.POSSIBLE_FORMATS, barcodeFormats);
        }


        @Override
        protected void forEach(List<File> files, int index) throws EdmException, IOException, SQLException {
            log.debug("[forEach] i = {}", index);

            if(files.size() == 0) {
                log.debug("[forEach] size == 0, continue");
            } else {

                final List<File> destFiles = getDestFiles(files);
                final Collection<String> filePaths = Collections2.transform(destFiles, new Function<File, String>() {
                    @Override
                    public String apply(File file) {
                        return file.getAbsolutePath();
                    }
                });

                log.debug("[forEach] filePaths = {}", filePaths);

                File firstDestFile = destFiles.get(0);

                String firstFilePath = firstDestFile.getAbsolutePath();
                String description = files.get(0).getName().replaceFirst("\\.[A-Za-z0-9]+$", "");
                String userImportId = ((Long)new Date().getTime()).toString();
                String importDockind = "normal.image_import";

                log.info("[forEach] creating bean: {}", firstFilePath);
                DSIBean bean = ImportHelper.getSimpleDSIBean(userImportId, description, importDockind, filePaths);

                // odczytujemy barcode z obrazk�w i dodajemy do pola "DOC_BARCODE"
                Optional<String> barcode = ZxingUtils.decodeBarcode(files, hints);
                if(barcode.isPresent()) {
                    log.debug("[forEach] znaleziono barkod: "+barcode.get());
                    bean.addAttribute(new DSIAttribute("DOC_BARCODE", barcode.get()));
                } else {
                    log.debug("[forEach] nie znaleziono barkodu");
                }

                DSIManager manager = new DSIManager();
                manager.saveDSIBean(bean, DSApi.context().session().connection());

                moveFiles(files, destFiles);
            }

        }

        private void moveFiles(List<File> files, List<File> destFiles) throws IOException {
            for(int i = 0; i < files.size(); i++) {
                File file = files.get(i);
                File destFile = destFiles.get(i);

                log.info("[forEach] moving file {} --> {}", file.getAbsolutePath(), destFile.getAbsolutePath());
                FileUtils.moveFile(file, destFile);
            }
        }

        private List<File> getDestFiles(List<File> files) {
            List<File> destFiles = new ArrayList<File>();

            for(File file: files) {
                String destFilename = putTimestamp(file.getName());
                File destFile = new File(importPath, destFilename);
                destFiles.add(destFile);
            }

            return destFiles;
        }

        private String getFilePath(File file) {
            String destFilename = putTimestamp(file.getName());
            File destFile = new File(importPath, destFilename);

            String filePath = destFile.getAbsolutePath();

            return filePath;
        }
    }
}
