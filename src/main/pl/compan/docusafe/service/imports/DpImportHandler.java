package pl.compan.docusafe.service.imports;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.dictionary.DpInst;
import pl.compan.docusafe.core.dockinds.dictionary.DpStrona;
import pl.compan.docusafe.core.imports.ImportManager;
import pl.compan.docusafe.core.imports.ImportedDocumentInfo;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;

public class DpImportHandler extends ImportHandler
{
    private static final int FIELDS_COUNT = 17;
    
    public DpImportHandler()
    {        
    }

    @Override
    public void createDocument(ImportedDocumentInfo importedDocumentInfo, String line) throws EdmException
    {

        
        DocumentKind dockind = DocumentKind.findByCn("dp");
        
        // sprawdzam poprawnosc pliku tiff
        //File tiffFile = new File(importedDocumentInfo.getImportedFileInfo().getTiffDirectory() + File.pathSeparator + importedDocumentInfo.getTiffName());
        File tiffFile = new File(importedDocumentInfo.getImportedFileInfo().getTiffDirectory() + "/" + importedDocumentInfo.getTiffName());
        if (tiffFile == null || !tiffFile.exists())
        {
            throw new EdmException("Podano niepoprawny plik tiff");
        }

        String[] fields = line.split("\\s*~\\s*", FIELDS_COUNT);
        
        int i;
        for (i = 0; i < fields.length; ++i)
        {
            int f = fields[i].indexOf('"');
            int l = fields[i].lastIndexOf('"');
            if (f >= 0 && f != l)
                fields[i] = fields[i].substring(f + 1, l);

        }

        Document newDocument = new Document("", "");
        
        try
        {
            Map<String, Object> values = new HashMap<String, Object>();
            Integer klasaId = Integer.parseInt(fields[0]);
            values.put("KLASA", klasaId);
            Integer typId = Integer.parseInt(fields[1]);
            values.put("TYP", typId);
            if (typId == 10 || typId == 20)
                values.put("DATA_UMOWY", DateUtils.sqlDateFormat.parse(fields[2]));
            if (typId == 20)
                values.put("DATA_ANEKSU", DateUtils.sqlDateFormat.parse(fields[3]));
            if (typId == 30 || typId == 40)
            {
                values.put("DATA_POCZATKOWA", DateUtils.sqlDateFormat.parse(fields[4]));
                values.put("DATA_KONCOWA", DateUtils.sqlDateFormat.parse(fields[5]));
            }
            if (klasaId != 10)
                values.put("DATA", DateUtils.sqlDateFormat.parse(fields[6]));
            Map<String, Object> params = new HashMap<String, Object>();
            
            if (klasaId == 80) //80 = Sprawy s�dowe
            {
                String[] strona = fields[7].split("\\s*;\\s*", 3);
                if (strona.length > 0 && strona[0].length() > 0)
                    params.put("imie", strona[0]);
                if (strona.length > 1 && strona[1].length() > 0)
                    params.put("nazwisko", strona[1]);
                if (strona.length > 2 && strona[2].length() > 0)
                    params.put("sygnatura", strona[2]);
                List<DpStrona> strony = DpStrona.find(params);
                DpStrona dpStrona = null;
                if (strony.size() == 0)
                {
                    dpStrona = new DpStrona();
                    if (strona.length > 0 && strona[0].length() > 0)
                        dpStrona.setImie(strona[0]);
                    if (strona.length > 1 && strona[1].length() > 0)
                        dpStrona.setNazwisko(strona[1]);
                    if (strona.length > 2 && strona[2].length() > 0)
                        dpStrona.setSygnatura(strona[2]);
                    dpStrona.create();
                }
                else
                {
                    dpStrona = strony.get(0);
                }
                values.put("STRONA", dpStrona.getId());
            }

            values.put("SYGNATURA", fields[8]);
            EnumItem stat = dockind.getFieldByCn("STATUS").getEnumItemByCn("archiwalny");
            values.put("STATUS", stat.getId());
            values.put("RODZAJ_SPRAWY", fields[9]);
            
            if (fields[10].length() > 0)
            {
                params.clear();
                params.put("name", fields[10]);
                List<DpInst> instList = DpInst.find(params);
                DpInst dpInst = null;
                if (instList.size() == 0)
                {
                    dpInst = new DpInst();
                    dpInst.setName(fields[10]);
                    dpInst.create();
                }
                else
                {
                    dpInst = instList.get(0);
                }
                values.put("INST", dpInst.getId());
            }
            
            if (fields[11].length() > 0)
                values.put("IMIE", fields[11]);
            if (fields[12].length() > 0)
            values.put("NAZWISKO", fields[12]);
            if (klasaId == 20 || klasaId == 30 || klasaId == 70 || typId == 30 || typId == 40)
            {
                EnumItem eit = dockind.getFieldByCn("MIEJSCOWOSC").getEnumItemByTitle(fields[13]);
                values.put("MIEJSCOWOSC", eit.getId());
            }

            if (fields[16].length() > 0)
            {
                String[] numPorz = fields[16].split("\\s*;\\s*");
                values.put("NUMERY_PORZADKOWE", numPorz);
            }            
            
            dockind.logic().validate(values, dockind, null);

            newDocument.setFolder(Folder.getRootFolder());

            newDocument.create();
            newDocument.setDocumentKind(dockind);
            
            Long newDocumentId = newDocument.getId();

            dockind.set(newDocumentId, values);

            dockind.logic().archiveActions(newDocument, DocumentKindsManager.getType(newDocument));
            dockind.logic().documentPermissions(newDocument);
            
            Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
            newDocument.createAttachment(attachment);
            attachment.createRevision(tiffFile).setOriginalFilename(fields[14]);
            
        }
        catch (ValidationException e)
        {
            throw new EdmException("B��d walidacji danych: " + e.getMessage());
        }
        catch (EdmException e)
        {
            throw e;
        }
        catch (Throwable e)
        {
            throw new EdmException("B��d podczas importu dokumentu: " + e.getMessage());
        }

        Box box;
        try
        {
            box = Box.findByName(dockind.getProperties().get(DocumentKind.BOX_LINE_KEY),importedDocumentInfo.getBoxNumber());
        }
        catch (EntityNotFoundException e)
        {
            // takie pudlo nie istnieje - tworze je
            box = new Box(importedDocumentInfo.getBoxNumber().toUpperCase());
            box.setOpen(true);
            box.setOpenTime(new Date());
            box.setLine(dockind.getProperties().get(DocumentKind.BOX_LINE_KEY));
            box.create();
            box.setCloseTime(new Date());
        }
        newDocument.setBox(box);
        
        // zapamietujemy id dokumentu, ktory utworzylismy
        importedDocumentInfo.setDocumentId(newDocument.getId());
        
    }

    @Override
    public ImportedDocumentInfo createDocumentInfo(String line, int lineCount) throws EdmException
    {
        String[] fields = line.split("\\s*~\\s*", FIELDS_COUNT);
        int i;
        for (i = 0; i < fields.length; ++i)
        {
            int f = fields[i].indexOf('"');
            int l = fields[i].lastIndexOf('"');
            if (f >= 0 && f != l)
                fields[i] = fields[i].substring(f + 1, l);
        }
        ImportedDocumentInfo importedDocumentInfo = new ImportedDocumentInfo();
        importedDocumentInfo.setStatus(ImportManager.STATUS_WAITING);
        importedDocumentInfo.setLineNumber(lineCount);
        
        if (fields.length == FIELDS_COUNT)
        {
            importedDocumentInfo.setBoxNumber(fields[15]);
            Date date = ImportManager.parseDateValidated(fields[6]);
            if (date != null)
                importedDocumentInfo.setDate(date);
            /*
            else
            {
                importedDocumentInfo.setStatus(ImportManager.STATUS_ERROR);
                importedDocumentInfo.setErrorInfo("Podana data jest niepoprawna");
            }
            */
            importedDocumentInfo.setTiffName(fields[14]);
        }
        else
        {
            importedDocumentInfo.setProcessedTime(new Date());
            importedDocumentInfo.setStatus(ImportManager.STATUS_DONE);
            importedDocumentInfo.setResult(ImportManager.RESULT_ERROR);
            importedDocumentInfo.setErrorInfo("Niepoprawnie opisany dokument - oczekiwano dok�adnie " + FIELDS_COUNT + " warto�ci (by�o " + fields.length + ")");
        }

        return importedDocumentInfo;
    }

}
