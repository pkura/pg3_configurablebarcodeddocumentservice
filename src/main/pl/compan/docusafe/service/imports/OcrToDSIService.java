package pl.compan.docusafe.service.imports;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.imports.dsi.DSIAttribute;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;

public class OcrToDSIService extends ServiceDriver implements Service {

	private static final Logger LOG = pl.compan.docusafe.util.LoggerFactory.getLogger(OcrToDSIService.class);
	
	private Timer timer = null;
	
	private static File OUTPUT = null; //new File("Z:\\output\\");
	private static File IMPORT = null; //new File("Z:\\import\\");
	
	protected boolean canStop() 
	{
		return false;
	}
	
	public boolean hasConsole() 
	{
		return true;
	}

	protected void start() throws ServiceException 
	{
		try
		{
			OUTPUT = new File(Docusafe.getAdditionProperty("ocr.output"));
			IMPORT = new File(Docusafe.getAdditionProperty("ocr.import"));
			
			if(OUTPUT.exists() && IMPORT.exists())
			{
				timer = new Timer(true);
				timer.schedule(new Import(), 1 * DateUtils.MINUTE, 1 * DateUtils.MINUTE);
				
				console(Console.INFO, "Usluga uruchomiona");
			}
			else
			{
				throw new EdmException("Blad przy uruchamianiu uslugi");
			}
		}
		catch (Exception e) 
		{
			console(Console.ERROR, "Usluga nie zostala uruchomiona - nie znaleziono katalogow ocr");
			LOG.error("Nie znaleziono katalogow do ocr, usluga nie zostanie uruchomiona");
		}
	}

	protected void stop() throws ServiceException 
	{
		if (timer != null) timer.cancel();
	}
	
	private class Import extends TimerTask
	{
		public synchronized void run() 
		{
			try
			{
				DSApi.openAdmin();

                // iterujemy po wszystkich plikach z IMPORT,
                // kt�re maj� czas modyfikacji starszy ni� 15 sekund
				for(File baseFile : IMPORT.listFiles(new FileFilter() {
					
					public boolean accept(File pathname) {
						if(System.currentTimeMillis() - pathname.lastModified() > 15 * DateUtils.SECOND)
							return true;
						else
							return false;
					}
				}))
				{
					DSApi.context().begin();
					try
					{
						LOG.debug("base file : {}",baseFile.getName());
						Map<String,String> values = getValuesFromFilename(baseFile.getName());
						LOG.debug("VALUES : {}",values);
						DSIBean bean = new DSIBean();
						List<DSIAttribute> attrs = new LinkedList<DSIAttribute>();
						
						for(String field : values.keySet())
							attrs.add(new DSIAttribute(field, values.get(field)));
						
						for(File extraFile : getAllFiles(values.get("NAME")))
							attrs.add(new DSIAttribute("FILE", extraFile.getAbsolutePath().replace("\\", "/")));									
						
						bean.setBoxCode(values.containsKey("BOX") ? values.get("BOX") : "noneed");
						bean.setDocumentKind(values.get("DOCKIND"));
						bean.setFilePath("filesinattributes");
						bean.setImportKind(values.containsKey("IMPORT_KIND") ? Integer.valueOf(values.get("IMPORT_KIND")) : 2);
						bean.setAttributes(attrs);
						bean.setImportStatus(1); //zmienic na 1 zeby uruchomic import
						bean.setSubmitDate(new Date());
						bean.setUserImportId(values.get("NAME")); //powinna byc cala nazwa pliku ale jest za dluga
						bean.setErrCode("");
						LOG.debug("ATTRS : {}",attrs);
						
						DSIManager manager = new DSIManager();
						manager.saveDSIBean(bean, DSApi.context().session().connection());					
						
						log.debug("USUWA ZAIMPORTOWANY PLIK "+ baseFile.getAbsolutePath());
						baseFile.delete(); //wywalamy plik	a moze zmienic mu nazwe?		
					}
					catch (Exception e) 
					{
						LOG.error(e.getMessage(),e);
						console(Console.ERROR, e.getMessage());
					}
					DSApi.context().commit();
				}
			}
			catch (Exception e) 
			{
				LOG.error(e.getMessage(),e);
				console(Console.ERROR, e.getMessage());
			}
			finally
			{
				DSApi._close();
			}
		}
	}
	
	private Map<String,String> getValuesFromFilename(String fileName)
	{
		Map<String,String> values = new HashMap<String, String>();
		String line = fileName.split("\\.")[0];
		line = line.replaceAll("\\[", "");
		
		for(String str : line.split("\\]"))
		{
			System.out.println(str+ " "+str.split("-").length);
			values.put(str.split("-")[0], (str.split("-").length > 1 ? str.split("-")[1]: null));
		}
			
		return values;
	}

    /**
     * Zwraca list� wszystkich plik�w z <code>OUTPUT</code>,
     * kt�rych nazwa zaczyna si� od <code>fileName</code>.
     *
     * @param fileName
     * @return
     */
	private synchronized List<File> getAllFiles(final String fileName)
	{
		List<File> files = new ArrayList<File>();
		
		for(File extraFile : OUTPUT.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				if(name.startsWith(fileName))
				{
					return true;
				}
				else
					return false;
			}
		}))
		{
			files.add(extraFile);
		}
		
		return files;
	}
}
