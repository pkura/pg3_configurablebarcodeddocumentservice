package pl.compan.docusafe.service.imports;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import oracle.jdbc.driver.OracleDriver;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.Patron;
import pl.compan.docusafe.core.crm.Region;
import pl.compan.docusafe.core.crm.Role;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.parametrization.ilpol.LeasingObjectType;
import pl.compan.docusafe.util.LoggerFactory;

public class ImpulsImportUtils 
{
	private static final Log log = LogFactory.getLog("agent_log");
	private static Integer time;
    private static String url;
	private static String user;
    private static String pass;
    private static boolean onStratServer;
    private static Map<Integer,Role> rolesMap ;
    private static Map<Integer,Region> regionMap ;
    private static Map<Long,LeasingObjectType> typeMap ;
	private final static String PREFIX = "ilpol.";
	public static final Integer FROM_VKONTRAHENCI = 1;
	public static final Integer FROM_VDFKONTRAHENCI = 2;
	public static String vumowy;
	public static String vdfewnioski;
	public static String vkontrahenci;
	public static String vdfekontrahenci;
	public static String vkontrahencirole;
	public static String vwezwania_crm;
	public static String VWYGASAJACEUL_CRM;
	
	/** 
	 * Propertiesy przekazane do bazy danych
	 */
	private static Properties configuration = null;
	/**
	 * Czy agent jest wlaczony?
	 */
	private static boolean serviceOn = false;
	private static long lastApp;
	private static long lastContract;

	static
	{
		rolesMap =  new LinkedHashMap<Integer, Role>();
		regionMap =  new LinkedHashMap<Integer, Region>();
		typeMap = new LinkedHashMap<Long, LeasingObjectType>();
		try
		{
			for(Role r:Role.roleList())
			{
				rolesMap.put(r.getLeoId(), r);
			}
			for(Region r : Region.regionList())
			{
				regionMap.put(r.getLeoId(), r);
			}
			for (LeasingObjectType t : LeasingObjectType.list())
			{
				typeMap.put(t.getId(),t);
			}
		}
		catch (Exception e) 
		{
			log.debug("",e);
		}
		finally
		{
		//	DSApi._close();
		}
	}
	
	public static void initImport()
	{
		try
		{
			
        	File config = new File(Docusafe.getHome(), "agent.config");
        	if(config.exists())
        	{
        		log.debug("Wczytuje parametry importu ilpol");
        		configuration = new Properties();
        		configuration.load(Docusafe.class.getClassLoader().getResourceAsStream("agent.properties"));
        		configuration.load(new FileInputStream(config));
        		url = configuration.getProperty("url");
	        	user = configuration.getProperty("username");
	        	pass = configuration.getProperty("password");
	        	vumowy= configuration.getProperty("vumowy");
	        	vdfewnioski= configuration.getProperty("vdfewnioski");
	        	vkontrahenci= configuration.getProperty("vkontrahenci");
	        	vkontrahencirole= configuration.getProperty("vkontrahencirole");
	        	vwezwania_crm= configuration.getProperty("vwezwania_crm");
	        	VWYGASAJACEUL_CRM= configuration.getProperty("VWYGASAJACEUL_CRM");
	        	vdfekontrahenci = configuration.getProperty("vdfekontrahenci");
	        	
	        	onStratServer = "true".equals(configuration.getProperty("onStratServer"));
	        	lastContract = Long.parseLong(configuration.getProperty("lastContract", "0"));
	        	lastApp = Long.parseLong(configuration.getProperty("lastApp", "0"));
        	}   
        	else
        	{
        		serviceOn = false;
        		return;
        	}
			
			if (configuration.getProperty(PREFIX+"enabled") != null && configuration.getProperty(PREFIX+"enabled").equalsIgnoreCase("true"))
			{
				serviceOn = true;
	            time = Integer.parseInt(configuration.getProperty(PREFIX+"agentTime"));
	            try
	            {
	            	OracleDriver Driver = new OracleDriver();
	            	DriverManager.registerDriver(Driver);	            	
	            }
	            catch (SQLException e)
	            {
	            	log.error("Blad w czasie incjowania drivera",e);
	            }
			}
			else
			{
				serviceOn = false;
			}
		}
		catch (Exception e)
		{
			log.error("Blad w czasie wczytywania pliku ilpol", e);
			serviceOn = false;
		}
	}
	
    public static Connection connectToLEO() throws SQLException
    {   	
    	if(url == null)
    		initImport();
    	return DriverManager.getConnection(url, user, pass);
    }
    
    public static void disconnectOracle(Connection con)
    {
    	if (con!=null)
    	{
    		try
    		{
    			con.close();
    		} catch (Exception e) { log.error(e.getMessage(),e);}
    	}
    }
    public static ArrayList<DlContractDictionary> findContractByAppNumber(String appNumber,Long dsAppId) throws EdmException
    {
    	ArrayList<DlContractDictionary>  dlc = new ArrayList<DlContractDictionary>();
    	Set<Long> dlcIds = new HashSet<Long>();
    	Connection conn = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	Long leoIdumowy = null;
    	try
    	{
    		conn = connectToLEO();
    		ps =conn.prepareStatement(" select idumowy from "+vumowy+" where nrwniosku = ? ");
    		ps.setString(1, appNumber);
    		rs = ps.executeQuery();
    		
    		while(rs.next())
    		{
    			leoIdumowy = rs.getLong(1);
    			if(leoIdumowy != null && leoIdumowy > 0)
    			{
    				DlContractDictionary contract = getContractByIdUmowy(conn, leoIdumowy,dsAppId);
    				dlc.add(contract);
    				dlcIds.add(contract.getId());
    			}
    		}
    		rs.close();
    		ps.close();
	   		 ArrayList<Long> dsContracts = DlApplicationDictionary.getInstance().getContracts(dsAppId);
	   		 if(dlc.size() < dsContracts.size())
	   		 {
	   			 for (Long dsConId : dsContracts) 
	   			 {
	   				 if(!dlcIds.contains(dsConId))
	   					 DlContractDictionary.getInstance().find(dsConId).setApplicationId(null);
	   			 }
	   		 }
    	}
		catch (Exception e)
		{
			log.error(e.getMessage(),e);
			throw new EdmException("B��d dodania wniosku :"+ e.getMessage());
		}
		finally
		{
			if(conn != null)
			{
				disconnectOracle(conn);
			}
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
    	return dlc;
    }
    
    
    public static DlContractDictionary findContractByNumber(String numerumowy) throws EdmException
    {
    	DlContractDictionary  dlc = null;
    	Connection conn = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		conn = connectToLEO();
    		ps =conn.prepareStatement("select * from "+vumowy+" where nrumowy = ? ");
    		ps.setString(1, numerumowy);
    		rs = ps.executeQuery();
    		if(rs.next())
    		{
    			Contractor contractor = null;
    			Contractor dostawca = null;
    			String klientId = rs.getString("idklienta");
    			String dostawcaId = rs.getString("iddostawcy");
    			
    			contractor = getContractor(klientId, 1, conn, FROM_VKONTRAHENCI);
    			dostawca = getContractor(dostawcaId, 1, conn, FROM_VKONTRAHENCI);    			

    			
    			dlc = new DlContractDictionary();
    			dlc.setDataKoncaUmowy(rs.getDate("DATAZAKONCZENIA"));
    			dlc.setDataUmowy(rs.getDate("DATAPODPISANIA"));
    			if(contractor == null)
    			{
    				throw new EdmException("Brak kontrahenta w LEO");
    			}
    			else
    			{
    				dlc.setIdKlienta(contractor.getId());
    			}
    			if(dostawca != null)
    				dlc.setIdDostawcy(dostawca.getId());
    			dlc.setNumerUmowy(numerumowy);
    			dlc.setOpisUmowy(rs.getString("NAZWAPRZEDMIOTU"));
    			dlc.setStatus(rs.getString("STATUS"));
    			dlc.setLeasingObjectType(ImpulsImportUtils.getType(rs.getString("TYPPRZEDMIOTU"), rs.getLong("IDTYPPRZEDMIOTU")));
    			dlc.create();
    			
    		}
    	}
		catch (Exception e)
		{
			log.error("",e);
			throw new EdmException("B��d dodania wniosku :"+ e.getMessage());
		}
		finally
		{
			if(conn != null)
			{
				disconnectOracle(conn);
			}
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
    	return dlc;
    }
    
    /**Zwraca IDUMOWY w DS dla umowy o podanym ID w LEO
     * @throws EdmException */
    public static DlContractDictionary getContractByIdUmowy(Connection conn,Long leoIdUmowy,Long dsAppId) throws EdmException
    {
    	log.debug("SZUKA UMOWY O IDLEO="+leoIdUmowy);
    	DlContractDictionary  dlc = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps =conn.prepareStatement("select * from "+vumowy+" where idumowy = ?");
    		ps.setLong(1, leoIdUmowy);
    		rs = ps.executeQuery();
    		if(rs.next())
    		{
    			log.debug("Znalazl wpisy dla idumowyLeo");
    			String numweUmowy = rs.getString("nrumowy");
    			if(numweUmowy != null)
    			{
    				Contractor contractor = null;
    				Contractor dostawca = null;
        			String klientId = rs.getString("idklienta");
        			String dostawcaId = rs.getString("iddostawcy");
        			
        			contractor = getContractor(klientId, 1, conn, FROM_VKONTRAHENCI);
        			dostawca = getContractor(dostawcaId, 1, conn, FROM_VKONTRAHENCI);
    				List<DlContractDictionary>  conList = DlContractDictionary.findByNumerUmowy(numweUmowy);
    				if(conList != null && conList.size() > 0)
    				{
    					log.trace("ZNALAZL UMOWE W DS DLA IDUMOWY "+leoIdUmowy);
    					dlc=conList.get(0);
    					setContractValue(dlc, rs, contractor, dostawca, dsAppId);
    					return dlc;    					 
    				}
        			log.error("DODAJE UMOWE "+numweUmowy+" DO DS DLA WNIOISKU "+ leoIdUmowy);
        			dlc = new DlContractDictionary();
        			setContractValue(dlc, rs, contractor, dostawca, dsAppId);
        			dlc.create();    
        			return dlc;
    			}
    			else
    			{
    				log.debug("NIE ZNALEZIONO UMOWY DO WNIOSKU O IDLOEUMOWY = "+ leoIdUmowy);
    				return null;
    			}
    		}
    		else
    		{
    			log.debug("NIE ZNALEZIONO UMOWY DO WNIOSKU O IDLOEUMOWY = "+ leoIdUmowy);
    			return null;
    		}
    	}
		catch (Exception e)
		{
			log.error("",e);
			throw new EdmException("B��d dodania wniosku :"+ e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
    }
    
    private static void setContractValue(DlContractDictionary dlc,ResultSet rs, Contractor contractor, Contractor dostawca,Long dsAppId) throws SQLException, EdmException
    {
    	dlc.setDataKoncaUmowy(rs.getDate("DATAZAKONCZENIA"));
		dlc.setDataUmowy(rs.getDate("DATAPODPISANIA"));
		if(contractor == null)
		{
			throw new EdmException("Brak kontrahenta w LEO");
		}
		else
		{
			dlc.setIdKlienta(contractor.getId());
		}
		if(dostawca != null)
			dlc.setIdDostawcy(dostawca.getId());
		dlc.setNumerUmowy(rs.getString("nrumowy"));
		dlc.setApplicationId(dsAppId);
		dlc.setOpisUmowy(rs.getString("NAZWAPRZEDMIOTU"));
		dlc.setStatus(rs.getString("STATUS"));
		log.debug("SET STAN "+ rs.getString("NOWYUZYWANY")+ " "+DlContractDictionary.getStanIdByLeoCN(rs.getString("NOWYUZYWANY")));
		dlc.setStan(DlContractDictionary.getStanIdByLeoCN(rs.getString("NOWYUZYWANY")));
		log.debug("SET WARTOSC "+rs.getFloat("WARTOSCPRZEDMIOTU"));
		dlc.setWartoscUmowy(rs.getFloat("WARTOSCPRZEDMIOTU"));
		dlc.setLeasingObjectType(ImpulsImportUtils.getType(rs.getString("TYPPRZEDMIOTU"), rs.getLong("IDTYPPRZEDMIOTU")));
    }
    
    /**Zwraca IDWNIOSKU w DS dla umowy o podanym ID w LEO
     * @throws EdmException */
    public static DlApplicationDictionary getApplicationByIdUmowy(Connection conn,Long idUmowy) throws EdmException
    {
    	DlApplicationDictionary app = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps =conn.prepareStatement("select * from "+vdfewnioski+" where idumowy = ?");
    		ps.setLong(1, idUmowy);
    		rs = ps.executeQuery();
    		if(rs.next())
    		{
    			String numerWniosku = rs.getString("nrwniosku");
    			if(numerWniosku != null)
    			{
    				List<DlApplicationDictionary>  appList = DlApplicationDictionary.findByNumerWniosku(numerWniosku);
    				if(appList != null && appList.size() > 0)
    				{
    					log.trace("ZNALAZL WNIOSEK W DS DLA IDUMOWY "+idUmowy);
    					return appList.get(0);    					 
    				}
    				
    				Contractor contractor = null;
    				Contractor dostawca = null;
        			String klientId = rs.getString("klient");
        			String dostawcaId = rs.getString("dostawca");
        			
        			contractor = getContractor(klientId, 1, conn, FROM_VDFKONTRAHENCI);
        			dostawca = getContractor(dostawcaId, 1, conn, FROM_VDFKONTRAHENCI);    
        			 	
        			log.error("DODAJE WNIOSEK "+numerWniosku+" DO DS DLA UMOWY "+ idUmowy);

        			app = new DlApplicationDictionary();
        			app.setNumerWniosku(numerWniosku);
        			if(contractor == null)
        			{
        				throw new EdmException("Brak kontrahenta w LEO");
        			}
        			else
        			{
        				app.setIdKlienta(contractor.getId());
        			}
        			if(dostawca != null)
        				app.setIdDostawcy(dostawca.getId());
        			app.setOpis("Przedmiot: "+rs.getString("PRZEDMIOT")+"\n Status: "+rs.getString("STATUS"));
        			app.setStatus(rs.getString("STATUS"));
        			app.setLeasingObjectType(ImpulsImportUtils.getType(rs.getString("TYPPRZEDMIOTU"), rs.getLong("IDTYPPRZEDMIOTU")));
        			app.create();  
        			return app;
    			}
    			else
    			{
    				log.error("BRAK NUMERY WNIOSKU W LEO DLA UMOWY O IDLEO =" + idUmowy);
    				return null;
    			}
    		}
    		else
    		{
    			log.debug("NIE ZNALEZIONO WNIOSKU DO UMOWY O IDLOE = " + idUmowy);
    			return null;
    		}
    	}
		catch (Exception e)
		{
			log.error("",e);
			throw new EdmException("B��d dodania wniosku :"+ e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
    }
    
    public static DlApplicationDictionary findApplicationByNumber(String numerwniosku) throws EdmException
    {
    	DlApplicationDictionary app = null;
    	Connection conn = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		conn = connectToLEO();
    		ps =conn.prepareStatement("select * from "+vdfewnioski+" where nrwniosku = ? ");
    		ps.setString(1, numerwniosku);
    		rs = ps.executeQuery();
    		if(rs.next())
    		{
    			Contractor contractor = null;
    			Contractor dostawca = null;
    			String klientId = rs.getString("klient");
    			String dostawcaId = rs.getString("dostawca");
    			
    			contractor = getContractor(klientId, 1, conn, FROM_VDFKONTRAHENCI);
    			dostawca = getContractor(dostawcaId, 1, conn, FROM_VDFKONTRAHENCI);    
    			
//    			DSApi.context().begin();
    			app = new DlApplicationDictionary();
    			app.setNumerWniosku(numerwniosku);
    			if(contractor == null)
    			{
    				throw new EdmException("Brak kontrahenta w LEO");
    			}
    			else
    			{
    				app.setIdKlienta(contractor.getId());
    			}
    			if(dostawca != null)
    				app.setIdDostawcy(dostawca.getId());
    			app.setOpis("Przedmiot: "+rs.getString("PRZEDMIOT")+"\n Status: "+rs.getString("STATUS"));
    			app.setStatus(rs.getString("STATUS"));
    			app.setLeasingObjectType(ImpulsImportUtils.getType(rs.getString("TYPPRZEDMIOTU"), rs.getLong("IDTYPPRZEDMIOTU")));
    			app.create();
    			
    		}
    	}
		catch (Exception e)
		{
//			try{DSApi.context().rollback();}catch (Exception e2){}
			log.error("",e);
			throw new EdmException("B��d dodania wniosku :"+ e.getMessage());
		}
		finally
		{
			if(conn != null)
			{
				disconnectOracle(conn);
			}
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
    	return app;
    }
    
    /**Zwraca kontrahenta z tabeli vkontrahenci o numerze firmano lub null
     * @throws EdmException */
    public static Contractor getContractor(String idKlienta,Integer countKontrahenci,Connection conn,Integer kind) throws EdmException
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	String nip = null;
		Contractor contractor = null;
    	try
    	{
    		if(kind.equals(FROM_VKONTRAHENCI))
    			ps =conn.prepareStatement("select * from "+vkontrahenci+" where firmano = " + idKlienta);
    		else
    			ps =conn.prepareStatement("select * from "+vdfekontrahenci+" where firmano = " + idKlienta);
	    	rs = ps.executeQuery();
	    	log.trace("SZUKA W LEO kontrahenta LEOID = "+ idKlienta);
	    	if(rs.next())
	    	{
	    		log.trace("Znalazl kontrahenta w LEO");
	    		nip = rs.getString("NIP");
	    		if(nip == null)
	    		{
	    			log.error("Brak numeru NIP dla kontrahenta w LEO "+ idKlienta);
	    			return null;
	    		}
	    	}
	    	else
	    	{
	    		log.error("Nie znaleziono kontrahenta w LEO"+ idKlienta);
	    		return null;	        		
	    	}
	    	
			log.trace("Szuka w DS kontarhenta o NIP = "+ nip+"koniec");
			if(nip.length() > 10)
    		{
    			nip = nip.substring(nip.length()-10, nip.length());
    		}
        	List<Contractor> contractors = Contractor.findByNIP(nip);
        	boolean openContext = false;
			try
			{
				if(contractors == null || contractors.size() < 1 )
				{
					countKontrahenci ++;
					log.error("Dodaje kontarenta do DS o nip = "+nip);

					contractor = new Contractor();
					if(kind.equals(FROM_VKONTRAHENCI))
	        			setContractorToContractValues(contractor, rs, idKlienta, conn, nip);
					else
						setContractorToApplicationValues(rs, contractor, nip, idKlienta, conn,true);

				}
				else
				{
						log.trace("UPDATE kontahent w DS o numerze nip " + nip);
						contractor = contractors.get(0);		    				
						if(kind.equals(FROM_VKONTRAHENCI))
		        			setContractorToContractValues(contractor, rs, idKlienta, conn, nip);
						else
							setContractorToApplicationValues(rs, contractor, nip, idKlienta, conn,false);
		//				contractor.save();

				}
			}
			catch (Exception e) 
			{
				log.error(e.getMessage()+" Blad pobtrania kontrahenta "+idKlienta,e);
				throw new EdmException(e);
			}
    	}
    	catch (Exception e) 
    	{
			log.error(e.getMessage()+" Blad pobtrania kontrahenta "+idKlienta,e);
			throw new EdmException(e);
		}
    	finally
    	{
    		if(rs != null){try {rs.close();}catch (SQLException e) {}}
    		if(ps != null){try {ps.close();}catch (SQLException e) {}}	
    	}
    	return contractor;
    }
    
    /**Ustawia warto�ci w obiekcie contractor na podstawie rsK z tablei vkontrahenci*/
    public static void setContractorToContractValues(Contractor contractor, ResultSet rsK,String idKlienta,Connection conn,String nip) throws SQLException, Exception
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		log.trace("UPDATE START DS kontarhenta");
	    	contractor.setName(StringUtils.left(rsK.getString("SYMBOL"), 50));
			contractor.setFullName(StringUtils.left(rsK.getString("ADR_NAZWA"), 200));
			contractor.setCode(StringUtils.left(rsK.getString("ADR_KOD"), 50));
			contractor.setCity(StringUtils.left(rsK.getString("ADR_POCZTA"), 50));
			contractor.setStreet(StringUtils.left(rsK.getString("ADR_ULICA"), 50));
			String temp = StringUtils.left(rsK.getString("KONTAKT_TELK"), 50);
			String tel2 = StringUtils.left(rsK.getString("KONTAKT_TEL1"), 50);
			contractor.setPhoneNumber(( temp != null ? temp :"" )+" "+( tel2 != null ? tel2 :"" ));
			contractor.setPhoneNumber2(StringUtils.left(rsK.getString("KONTAKT_TEL2"), 50));
			contractor.setFax(StringUtils.left(rsK.getString("KONTAKT_FAX"), 50));
			contractor.setEmail(StringUtils.left(rsK.getString("KONTAKT_EMAIL"), 50));
			contractor.setNip(nip);
			contractor.setRegon(rsK.getString("VAT_REGON"));      	
			log.trace("UPDATE REGION DS kontarhenta");
			contractor.setRegion(getRegion(rsK.getString("REGION"),rsK.getInt("ID_REGIONU"))); 	
			log.trace("UPDATE ROLE DS kontarhenta");
	    	contractor.setRole(getRole(idKlienta, conn));
	    	log.trace("UPDATE STOP DS kontarhenta");
	    	contractor.create(false);
    	}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DbUtils.closeQuietly(ps);
    	}
    }
    
    /**Zwraca list� opiekun�w klienta po jego numerze firmano*/
    public static List<String> getOpiekuni(String idKlienta,Connection conn) throws Exception
    {
    	PreparedStatement ps = null;
    	ResultSet rs= null;
    	List<String> opiekuni = new ArrayList<String>();
    	try
    	{

    		ps = conn.prepareStatement("select opiekun from "+vdfekontrahenci+" where firmano = ?");//"select * from "+vkontrahenci+" where firmano = " + idKlienta);
    		ps.setString(1, idKlienta);
    		rs = ps.executeQuery();
    		while(rs.next())
    		{
    			String username = StringUtils.trimToNull(rs.getString(1));
    			if(username != null && username.length() > 2)
    				opiekuni.add(username);
    		}
    	}
    	finally
    	{
    		DbUtils.closeQuietly(ps);
    	}
    	return opiekuni;
    }
    
    /**Ustawia warto�ci w obiekcie contractor na podstawie rsK z tablei vdfkontrahenci*/
    public static void setContractorToApplicationValues(ResultSet rsK, Contractor contractor, String nip,String idKlienta, Connection conn,boolean create) throws Exception
    {
    		log.trace("UPDATE START DS kontarhenta");
			contractor.setName(StringUtils.left(rsK.getString("SYMBOL"), 50));
			contractor.setFullName(StringUtils.left(rsK.getString("ADR_NAZWA"), 200));
			contractor.setCode(StringUtils.left(rsK.getString("ADR_KOD"), 50));
			contractor.setCity(StringUtils.left(rsK.getString("ADR_POCZTA"), 50)); 
			contractor.setStreet(StringUtils.left(rsK.getString("ADR_ULICA"), 50));
			String temp = StringUtils.left(rsK.getString("TELK"), 50);
			contractor.setPhoneNumber(( temp != null ? temp :"" )+" - "+rsK.getString("KONTAKT_TEL1"));
			contractor.setPhoneNumber2(StringUtils.left(rsK.getString("KONTAKT_TEL2"), 50));
			contractor.setFax(StringUtils.left(rsK.getString("KONTAKT_FAX"), 50));
			contractor.setNip(nip);

//			for(String username: getOpiekuni(idKlienta, conn))
//			{
//				Patron.findOrCreate(username, contractor,"LEO");
////				contractor.addPatron(username.toLowerCase(), "LEO");
//			}
			if(create)
			{
				log.trace("CREATE patrona ");
				contractor.create(false);
			}
	        List <String> list = getOpiekuni(idKlienta, conn);
	        Set<Patron> patrons =null;
	        if(contractor.getPatrons() != null )
	        	patrons= new HashSet<Patron>(contractor.getPatrons());
	        if(patrons == null)
	        	patrons = new HashSet<Patron>();
	        if(log.isTraceEnabled())
		        for (Patron patron : patrons) 
		        {
		        	if(patron == null)
		        	{
		        		log.trace("PATEON NULL !!");
		        		continue;
		        	}
					log.trace("Partons "+patron.getId()+" "+patron.getOwn()+" "+patron.getUsername()+" ");
				}
	        
	        if(patrons != null)
	        {
	        	for (Patron  p : new HashSet<Patron>(patrons))
		        {
	        		
		        	if("LEO".equalsIgnoreCase(p.getOwn()) && !list.contains(p.getUsername()))
		        	{
		        		log.trace("Usuwa patrona "+p.getUsername());
//		        		p.delete();
		        		log.trace("Usuwa z seta");
		        		patrons.remove(p);
		        	}
				}
	        }
	        log.error(patrons.size());
	        for (String username : list) 
	        {
	        	log.trace("dodjea patrona "+username);
				patrons.add(Patron.findOrCreate(username, contractor,"LEO"));
				log.trace(patrons.size());
			}
	        if(log.isTraceEnabled())
		        for (Patron patron : patrons) 
		        {
		        	if(patron == null)
		        	{
		        		log.trace("PATEON NULL !!");
		        		continue;
		        	}
					log.trace("Partons "+patron.getId()+" "+patron.getOwn()+" "+patron.getUsername()+" ");
				}
	        contractor.setPatrons(patrons);

			contractor.setRegon(rsK.getString("REGON"));
//			log.trace("UPDATE REGION DS kontarhenta");
//			contractor.setRegion(getRegion(rsK.getString("REGION"),rsK.getInt("ID_REGIONU"))); 	
//			log.trace("UPDATE ROLE DS kontarhenta");
	    	//contractor.setRole(getRole(idKlienta, conn));
	    	log.trace("UPDATE STOP DS kontarhenta");
    }
    
    private static Region getRegion(String nazwa, Integer leoID) throws EdmException
	{
    	if(nazwa == null || "null".equals(nazwa) )
    		return null;
    	Region r = regionMap.get(leoID);
    	if(r == null)
    	{
    		if(r == null)
    		{
    			r= new Region();
    			r.setCn(leoID.toString());
    			r.setName(nazwa);
    			r.setLeoId(leoID);
    			r.create();
    			regionMap.put(leoID, r);
    		}
    		else
    		{
    			r.setName(nazwa);
    			DSApi.context().session().update(r);
    		}
    		LoggerFactory.getLogger("mariusz").debug("POPRAWIL "+ r.getId());
    		return r;
    	}
    	return r;
	}
    
    public static Long getType(String nazwa, Long leoID) throws EdmException
	{
    	log.trace("SZUKA TYPLEASINGU "+ nazwa+" nrLEO  "+leoID);
    	if(nazwa == null || "null".equals(nazwa) )
    		return null;
    	LeasingObjectType t = typeMap.get(leoID);
    	if(t == null)
    	{
			t= new LeasingObjectType();
			t.setName(nazwa);
			t.setLeoId(leoID);
			t.create();
			typeMap.put(leoID, t);
    		return t.getId();
    	}
		else
		{
			/*t.setName(nazwa);
			DSApi.context().session().update(t);
			LoggerFactory.getLogger("mariusz").debug("POPRAWIL typ"+ t.getId());*/
		}
    	log.trace("ZWRACA TYPLEASINGU "+ nazwa+" nrDS  "+t.getId());
    	return t.getId();
	}

	private static Set<Role> getRole(String idKlienta,Connection conn) throws EdmException, SQLException
    {
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	Set<Role> roles = new HashSet<Role>();
    	try
    	{
			ps =conn.prepareStatement("select * from "+vkontrahenci+"role where firmano = " + idKlienta);
	    	rs = ps.executeQuery();
	    	
	    	while (rs.next()) 
	    	{
	    		String nazwa = rs.getString("nazwa");
	    		Integer rolaId = rs.getInt("rolano");
	    		Role newRole = getOneRole(nazwa, rolaId);
	    		if(newRole != null)
	    			roles.add(newRole);
	    	}
    	}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DbUtils.closeQuietly(ps);
    	}
    	return roles;
    }
	
	private static Role getOneRole(String nazwa,Integer leoId) throws EdmException
	{
    	Role r = rolesMap.get(leoId);
/*		if(r == null)
		{
			r = new Role();
			r.setName(nazwa);
			r.setCn("LEO_"+leoId);
			r.setLeoId(leoId);
			r.create();
			log.error("Dodaje role "+ nazwa);
			DSApi.context().session().flush();
			rolesMap.put(r.getLeoId(), r);
		}
		else if(r.getLeoId() == null)
		{
			r.setLeoId(leoId);
			log.error("Dodaje leoId "+ leoId);
		}
		*/
    	return r;
	}

	public static Integer getTime()
	{
		return time;
	}

	public static boolean isServiceOn()
	{
		return serviceOn;
	}

	public static void setTime(Integer time)
	{
		ImpulsImportUtils.time = time;
	}

	public static void setServiceOn(boolean serviceOn)
	{
		ImpulsImportUtils.serviceOn = serviceOn;
	}

	public static boolean isOnStratServer() {
		return onStratServer;
	}

	public static void setOnStratServer(boolean onStratServer) {
		ImpulsImportUtils.onStratServer = onStratServer;
	}

	public static long getLastApp() {
		return lastApp;
	}

	public static void setLastApp(long lastApp) {
		ImpulsImportUtils.lastApp = lastApp;
	}

	public static long getLastContract() {
		return lastContract;
	}

	public static void setLastContract(long lastContract) {
		ImpulsImportUtils.lastContract = lastContract;
	}
}
