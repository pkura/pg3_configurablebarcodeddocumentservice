package pl.compan.docusafe.service.imports.dsi;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.FeedbackException;
import java.util.Date;
/**  
 * Klasa odpowiedzialna za obsluge informacji feedbacku
 * @author wkutyla
 *
 */
public abstract class FeedbackHandler 
{
	protected DSIBean dsiBean;
	protected String feedbackCode;
	protected Date feedbackDate;
	
	protected boolean feedbackOn = false;
	
	
	public static FeedbackHandler getHandler(DSIBean dsi)
	{
		FeedbackHandler handler = null;
		handler = new IronMountainFeedbackHandler();
		handler.setDsiBean(dsi);
		if (Docusafe.getAdditionProperty("importDocument.feedback").equalsIgnoreCase("true"))
		{
			handler.setFeedbackOn(true);
		}
		return handler;
	}
	
	public void sendFeedback () throws FeedbackException
	{
		if (feedbackOn)
		{
			sendFeedbackBody(); 
		}
		else
		{
			feedbackCode = "DISABLED";
		}
		feedbackDate = new java.util.Date();	
	}
	/**
	 * Metoda wysyla feedback do systemu klienta
	 * odpowiedz zapisana w feedbackCode i feedbackDate
	 * @throws FeedbackException - wystepuje gdy nie udalo sie nawiazac kontaktu z usluga
	 */
	protected abstract void sendFeedbackBody () throws FeedbackException;

	public DSIBean getDsiBean() {
		return dsiBean;
	}

	public void setDsiBean(DSIBean dsiBean) {
		this.dsiBean = dsiBean;
	}

	public String getFeedbackCode() {
		return feedbackCode;
	}

	public void setFeedbackCode(String feedbackCode) {
		this.feedbackCode = feedbackCode;
	}

	public Date getFeedbackDate() {
		return feedbackDate;
	}

	public void setFeedbackDate(Date feedbackDate) {
		this.feedbackDate = feedbackDate;
	}

	public boolean isFeedbackOn() {
		return feedbackOn;
	}

	public void setFeedbackOn(boolean feedbackOn) {
		this.feedbackOn = feedbackOn;
	}
	
	
}
