package pl.compan.docusafe.service.imports.dsi;

import java.util.Iterator;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.ImportedFileInfo;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public abstract class DSIParser
{
	private static final Logger log = LoggerFactory.getLogger(DSIParser.class);
	public abstract DSIBeanIterator getDSIBeanIterator(ImportedFileInfo importedFileInfo);
	
	public void parseFile(ImportedFileInfo importedFileInfo) throws EdmException
	{
		DSIManager manager = new DSIManager();
		for (Iterator<DSIBean> it = getDSIBeanIterator(importedFileInfo); it.hasNext();)
		{
			DSIBean bean = it.next();
			try
			{
				manager.saveDSIBean(bean, DSApi.context().session().connection());
			}
			catch (Exception e) 
			{
				log.error("",e);
				throw new EdmException("",e);
			}
		}
	}
}
