package pl.compan.docusafe.service.imports.dsi;

import java.io.File;
import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.imports.dsi.DSIImportService.ImportDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;

/**
 * Service do pobierania plik�w z katalog�w (3 katalogi: dla skan�w, faks�w i emaili)
 * @author Jerzy Pir�g
 */
public class DSIMultiDirImport extends ServiceDriver implements Service 
{
	private Timer timer;
	private static final String scanFolderPath = Docusafe.getAdditionProperty("importDocument.scanFolderPath");
	private static final String faxFolderPath = Docusafe.getAdditionProperty("importDocument.faxFolderPath");
	private static final String emailFolderPath = Docusafe.getAdditionProperty("importDocument.emailFolderPath");
	private static final String importDockind = Docusafe.getAdditionProperty("importDocument.dockindCn");
	private static final Log log = LogFactory.getLog(DSIMultiDirImport.class);
	private static StringManager sm = GlobalPreferences.loadPropertiesFile(
			DSIMultiDirImport.class.getPackage().getName(), null);
	
	private DSIManager manager = new DSIManager();
	
	@Override
	protected void start() throws ServiceException {
		System.out.println("DSI SUPPLY START");
		if (Docusafe.getAdditionProperty("importDocument").equals("false"))
			return;
		
		if(StringUtils.isEmpty(importDockind))
			return;
		
		Integer time = Integer.parseInt(Docusafe.getAdditionProperty("importDocument.time"));

		timer = new Timer(true);
		timer.schedule(new ImportDocument(), 1000, time * DateUtils.SECOND/* 30*DateUtils.SECOND */);
		System.out.println("DSI SUPPLY START wystartowa�");
	}

	@Override
	protected void stop() throws ServiceException {
		if (timer != null)
			timer.cancel();
	}

	@Override
	protected boolean canStop() {
		return false;
	}
	
	public class ImportDocument extends TimerTask {
		public ImportDocument() {
		}

		public void run() {
			try
			{
				importFromDir(scanFolderPath);
				importFromDir(faxFolderPath);
				importFromDir(emailFolderPath);
			}catch(Exception e){
				log.error("Blad w czasie dzialania servicu", e);
			}
		}
		
		/**
		 * Zasila tabele dsi_import_table z przekazanego katalogu
		 * @throws EdmException 
		 * @throws HibernateException 
		 * @throws SQLException 
		 */
		private void importFromDir(String folderPath) throws Exception {
//			System.out.println("DO IMPORT SUPPLY FROM FOLDER:" + folderPath);
			List<DSIBean> beans = new ArrayList<DSIBean>();
			
			DSApi.openAdmin();
			java.sql.Connection con = DSApi.context().session().connection();
			File importDir = new File(folderPath);
			for(File file : importDir.listFiles())
			{
				DSIBean existsFile = manager.findByFilePath(con, file.getAbsolutePath());
				
				if(existsFile != null)
				{
					//System.out.println("Plik o absolutnej sciezce "+ file.getName()+" ju� zostal w przeszlosci juz zaimportowany. Brak mo�liwo�ci importu");
					log.error("File exists:" + file.getName());
					continue;
				}
				System.out.println("Importowanie pliku "+file.getName() +" ze sciezki "+ folderPath);
				log.error("Zapisuje plik");
				log.debug("tak"); 
				beans.add(supplyImport(file));	
			}
			
			try
			{
				DSApi.context().begin();
				
				for(DSIBean dsi : beans){
					manager.saveDSIBean(dsi, con);
				}
				
				DSApi.context().commit();
			}catch(Exception e ){
				log.error("Blad: ", e);
				DSApi.context()._rollback();
				throw new EdmException(e.getMessage());
			}
			
			DSApi._close();
		}
		
		/**
		 * Zasila tabele dsi_import_table dokumentem
		 * @param file
		 * @throws Exception 
		 */
		private DSIBean supplyImport(File file) throws Exception 
		{
			DSIImportHandler handler = DSIImportHandler.getHandler(importDockind);
			DSIBean dsiBean = new DSIBean();
			
			handler.supplyImport(file, dsiBean);
			
			return dsiBean;
		}
	}
}
