package pl.compan.docusafe.service.imports.dsi;

/**
 * Klasa reprezentujaca pojedynczy atrybut DSI_IMPORT_ATTRIBUTES
 * @author wkutyla
 *
 */
public class DSIAttribute implements java.io.Serializable 
{
	static final long serialVersionUID = 23234;
	
	private long attributeId;
	private long importId;
	private String attributeCn;
	private String attributeValue;
	private String setCode;
	private long lineNo;
	
	public DSIAttribute()
	{}
	
	public DSIAttribute(String attributeCn,String attributeValue)
	{
		this.attributeCn = attributeCn;
		this.attributeValue = attributeValue;
	}
	
	public long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(long attributeId) {
		this.attributeId = attributeId;
	}
	public long getImportId() {
		return importId;
	}
	public void setImportId(long importId) {
		this.importId = importId;
	}
	public String getAttributeCn() {
		return attributeCn;
	}
	public void setAttributeCn(String attributeCn) {
		this.attributeCn = attributeCn;
	}
	public String getAttributeValue() {
		return attributeValue;
	}
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
	public String getSetCode() {
		return setCode;
	}
	public void setSetCode(String setCode) {
		this.setCode = setCode;
	}
	public long getLineNo() {
		return lineNo;
	}
	public void setLineNo(long lineNo) {
		this.lineNo = lineNo;
	}
}
