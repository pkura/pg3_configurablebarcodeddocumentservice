package pl.compan.docusafe.service.imports.dsi;

/** Klasa zarzadzajaca mechanizmami importow z tabeli DSI
 * 
 * @author wkutyla
 *
 */

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.*;

import net.sourceforge.rtf.util.StringUtils;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.PostgreSQLDialect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.ws.im.UploadDocumentResponse;

public class DSIManager 
{
	public static final Integer IMPORT_STATUS_ERROR = -1;
	public static final Integer IMPORT_STATUS_DO_IMPORT = 1;
	public static final Integer IMPORT_STATUS_OK = 2;	
	public static final Integer IMPORT_STATUS_PARS= 3;
	
	private static final Logger log = LoggerFactory.getLogger(DSIManager.class);
	
	/**  
	 * Pobieramy liste danych do zaimportowania
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public Collection<DSIBean> getToImportList(Connection con) throws SQLException
	{
		Collection<DSIBean> resp = new Vector<DSIBean>();
		Statement stm = null;
		try
		{
			stm = con.createStatement();
			ResultSet rs = stm.executeQuery("select * from dsi_import_table where import_status = 1 order by import_id" );
			while (rs.next())
			{
				DSIBean dsi = getDSIBeanFromRow(rs);
				dsi.setAttributes(getAttributes(con,dsi.getImportId()));
				resp.add(dsi);
			}
			rs.close();
		}
		finally
		{
			DbUtils.closeQuietly(stm);
		}

        log.info("DSIManager.size() {}", resp.size());
		return resp;
	}
	
	
	
	
	/**
	 * Sprawdza czy dokument o takiej nazwie istieniej w tabeli do importu.
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public DSIBean findByFilePath(Connection con, String fileName) throws SQLException
	{
		DSIBean resp = null;
		PreparedStatement stm = null;
		try
		{
			stm = con.prepareStatement("select * from dsi_import_table where file_path = ? order by import_id");
			stm.setString(1, fileName);
			ResultSet rs = stm.executeQuery();
			while (rs.next())
			{
				resp = getDSIBeanFromRow(rs);
				resp.setAttributes(getAttributes(con,resp.getImportId()));
				break;
			}
			rs.close();
		}
		finally
		{
			DbUtils.closeQuietly(stm);
		}
		return resp;
		
	}
	
	
	
	
	/**  
	 * Pobieramy liste danych po imporcie dla danego numeru user_import_id
	 * Zwraca kolekcje DSIBean-ow, Jeśli loadAttributes = true wczytuje atrybuty
	 * @param con
	 * @param userImportId
	 * @param loadAttributes - czy zaladowac atrybuty
	 * @return
	 * @throws SQLException
	 */
	public Collection<DSIBean> findDSIBeanByUserImportId(Connection con,String userImportId,boolean loadAttributes) throws SQLException
	{
		Collection<DSIBean> resp = new Vector<DSIBean>();
		PreparedStatement ps = null;
		try
		{
			ps = con.prepareStatement("select * from dsi_import_table where user_import_id = ? order by import_id" );
			ps.setString(1, userImportId);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				DSIBean dsi = getDSIBeanFromRow(rs);
				if(loadAttributes)
					dsi.setAttributes(getAttributes(con,dsi.getImportId()));
				resp.add(dsi);
			}
			rs.close();
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
		return resp;
	}
	
	
	public Collection<DSIBean> getToFeedbackList(Connection con) throws SQLException
	{
		Collection<DSIBean> resp = new Vector<DSIBean>();
		Statement stm = null;
		try
		{
			stm = con.createStatement();
			ResultSet rs = stm.executeQuery("select * from dsi_import_table where import_status in (-1,2) and feedback_status = 0 order by import_id" );
			while (rs.next())
			{
				DSIBean dsi = getDSIBeanFromRow(rs);
				//dsi.setAttributes(getAttributes(con,dsi.getImportId()));
				resp.add(dsi);
			}
			rs.close();
		}
		finally
		{
			DbUtils.closeQuietly(stm);
		}
		return resp;
	}
	
	/**
	 * Zmienia status import_status podanego user_import_id na IMPORT_STATUS_DO_IMPORT
	 * "Ropoczynajac import dokumentow"(uzywane po parsowaniu)
	 * @throws SQLException
	 * @throws EdmException 
	 */
	public void startUserImport(String userImportId) throws SQLException, EdmException
	{
		PreparedStatement ps = null;
		try
		{
			ps = DSApi.context().prepareStatement("update dsi_import_table set import_status = ? where user_import_id = ?");
			ps.setInt(1,IMPORT_STATUS_DO_IMPORT);
			ps.setString(2, userImportId);
			ps.execute();
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * Oznaczamy wynik dzialania eksportu
	 * @param dsi
	 * @param con
	 * @param isFromWebService
	 * @throws SQLException
	 */
	public void markImportResult (DSIBean dsi, Connection con, boolean isFromWebService) throws SQLException
	{
		PreparedStatement ps = null;
		try
		{
			if(isFromWebService)
			{
				ps = con.prepareStatement("insert into dsi_import_table(import_status,process_date,document_id,err_code,user_import_id,feedback_status, submit_date)values(?,?,?,?,?,1,?)");
				log.info("[markImportResult] ImportStatus: " + dsi.getImportStatus());
				ps.setInt(1,dsi.getImportStatus());
				ps.setTimestamp(2,new Timestamp(new java.util.Date().getTime()));
				log.info("[markImportResult] DocumentId: " + dsi.getDocumentId());
				ps.setLong(3, dsi.getDocumentId());
				log.info("[markImportResult] ErrCode: " + dsi.getErrCode());
				ps.setString(4,dsi.getErrCode());
				log.info("[markImportResult] ImportId: " + dsi.getImportId());
				ps.setLong(5,dsi.getImportId());
				ps.setTimestamp(6,new Timestamp(new java.util.Date().getTime()));
				ps.execute();
			}
			else
			{
				ps = con.prepareStatement("update dsi_import_table set import_status = ?, process_date = ?, document_id = ?, err_code = ? where import_id = ?");
				ps.setInt(1,dsi.getImportStatus());
				ps.setTimestamp(2,new Timestamp(new java.util.Date().getTime()));
				ps.setLong(3, dsi.getDocumentId());
				ps.setString(4,dsi.getErrCode());
				ps.setLong(5,dsi.getImportId());
				ps.execute();
			}
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
	}
	public void saveDSIBean(DSIBean dsi,Connection con) throws SQLException, IOException, EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long importId =	null;
		try
		{
			

				if(DSApi.isOracleServer())
				{
					ps =  con.prepareStatement("select dsi_import_table_id.nextval from dual");
					rs = ps.executeQuery();
					if(rs.next())
						importId = rs.getLong(1);
					rs.close();
					ps = con.prepareStatement("insert into dsi_import_table(import_status,document_kind,import_kind, submit_date,file_path,box_code,user_import_id,err_code,IMPORT_ID)values(?,?,?,?,?,?,?,?,?)");
				}
                else if(DSApi.isPostgresServer()) {
                    ps =  con.prepareStatement("select nextval('dsi_import_table_import_id_seq')");
                    rs = ps.executeQuery();
                    if(rs.next())
                        importId = rs.getLong(1);
                    rs.close();
                    ps = con.prepareStatement("insert into dsi_import_table(import_status,document_kind,import_kind, submit_date,file_path,box_code,user_import_id,err_code,IMPORT_ID)values(?,?,?,?,?,?,?,?,?)");
                }
				else
				{
					ps = con.prepareStatement("insert into dsi_import_table(import_status,document_kind,import_kind, submit_date,file_path,box_code,user_import_id,err_code)values(?,?,?,?,?,?,?,?)");
				}
				if(dsi.getImageArray() != null && dsi.getImageArray().length > 1)
				{
					File file = FileUtils.getFileFromBytes(dsi.getImageArray());
					FileUtils.copyFileToFile(file, new File(DSIImportService.filePath,dsi.getImageName()));
					dsi.setFilePath(dsi.getImageName());
				}
				log.info("[saveBEAN] ImportStatus: " + dsi.getImportStatus());
				ps.setInt(1,dsi.getImportStatus());				
				log.info("[saveBEAN] document_kind: " + dsi.getDocumentKind());
				ps.setString(2,dsi.getDocumentKind());				
				log.info("[saveBEAN] import_kind: " + dsi.getImportKind());
				ps.setInt(3,dsi.getImportKind());				
				log.info("[saveBEAN] submit_date: " + dsi.getSubmitDate());
				ps.setTime(4,new java.sql.Time(dsi.getSubmitDate().getTime()));				
				log.info("[saveBEAN] file_path: " + dsi.getFilePath());
				ps.setString(5,dsi.getFilePath());				
				log.info("[saveBEAN] box_code: " + dsi.getBoxCode());
				ps.setString(6,dsi.getBoxCode());
				log.info("[saveBEAN] user_import_id: " + dsi.getUserImportId());
				ps.setString(7,dsi.getUserImportId());
				log.info("[saveBEAN] err_code: " + dsi.getErrCode());
				ps.setString(8, dsi.getErrCode());
				if(DSApi.isOracleServer() || DSApi.isPostgresServer())
				{
					ps.setLong(9, importId);
				}
				ps.execute();
				ps.close();
				if(importId == null)
				{
					ps = con.prepareStatement("select @@IDENTITY");
					rs = ps.executeQuery();
					if(rs.next())
					{
						importId = rs.getLong(1);
					}
				}
				if(dsi.getAttributes() != null)
				{
					for (DSIAttribute attr : dsi.getAttributes())
					{
						attr.setImportId(importId);
						saveAttribute(attr, con);
					}
				}
				dsi.setImportId(importId);
		}
		finally
		{
			DbUtils.close(rs);
			DbUtils.closeQuietly(ps);
		}
	}

    public String getNextValSql(Dialect dialect) {
        if(dialect instanceof Oracle10gDialect) {
            return "select dsi_import_table_id.nextval from dual";
        } else if(dialect instanceof PostgreSQLDialect) {
            return "select nextval('dsi_import_table_import_id_seq')";
        } else {
            throw new NotImplementedException("Dialect "+dialect+" not supported");
        }
    }

	public void saveAttribute(DSIAttribute attr, Connection con) throws SQLException
	{
		PreparedStatement ps = null;
		ResultSet rs= null;
		try
		{
			Long attrId= null;
			if(DSApi.isOracleServer() || DSApi.isPostgresServer())
			{
                ps = con.prepareStatement(getNextValSql(DSApi.getDialect()));
                rs = ps.executeQuery();
                if(rs.next())
                    attrId = rs.getLong(1);
                ps = con.prepareStatement("insert into DSI_IMPORT_ATTRIBUTES(import_id,attribute_cn,attribute_value,ATTRIBUTE_ID)values(?,?,?,?)");
                if(attrId != null)
                    ps.setLong(4, attrId);
            }
			else
			{
				ps = con.prepareStatement("insert into DSI_IMPORT_ATTRIBUTES(import_id,attribute_cn,attribute_value)values(?,?,?)");
			}
			ps.setLong(1, attr.getImportId());
			ps.setString(2, attr.getAttributeCn());
			ps.setString(3, attr.getAttributeValue());		
			ps.execute();
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * Oznaczamy feedback
	 * @param dsi
	 * @param feedback
	 * @param con
	 * @throws SQLException
	 */
	public void markImportFeedback (DSIBean dsi, FeedbackHandler feedback, Connection con) throws SQLException
	{
		PreparedStatement ps = null; 
		try
		{
			ps = con.prepareStatement("update dsi_import_table set feedback_status = ?, feedback_date = ?, feedback_response = ? where import_id = ?");
			ps.setInt(1, 1);
			ps.setTimestamp(2, new Timestamp(feedback.getFeedbackDate().getTime()));
			ps.setString(3, feedback.getFeedbackCode());
			ps.setLong(4,dsi.getImportId());
			ps.execute();
			//ps.close();
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
	}
	/**
	 * Pobieramy DSIBean
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private DSIBean getDSIBeanFromRow (ResultSet rs) throws SQLException
	{		
		DSIBean resp = new DSIBean();
		resp.setImportId(rs.getLong("import_id"));
		resp.setUserImportId(rs.getString("user_import_id"));
		resp.setDocumentKind(rs.getString("document_kind"));
		resp.setImportKind(rs.getInt("import_kind"));
		if (rs.getTimestamp("submit_date")!=null)
		{
			resp.setSubmitDate(rs.getTimestamp("submit_date"));
		}
		resp.setImportStatus(rs.getInt("import_status"));
		if (rs.getTimestamp("process_date")!=null)
		{
			resp.setProcessDate(rs.getTimestamp("process_date"));
		}
		resp.setDocumentId(rs.getLong("document_id"));
		resp.setErrCode(rs.getString("err_code"));
		resp.setFilePath(rs.getString("file_path"));
		resp.setBoxCode(rs.getString("box_code"));
		return resp;
	}
	
	private Collection<DSIAttribute> getAttributes(Connection con, long importId) throws SQLException
	{
		PreparedStatement ps = null;
		try
		{
			Collection<DSIAttribute> resp = new Vector<DSIAttribute>();
			ps = con.prepareStatement("select * from dsi_import_attributes where import_id = ?");
			ps.setLong(1,importId);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				DSIAttribute atr = new DSIAttribute();
				atr.setAttributeId(rs.getLong("attribute_id"));
				atr.setImportId(rs.getLong("import_id"));
				atr.setAttributeCn(rs.getString("attribute_cn"));
				atr.setAttributeValue(rs.getString("attribute_value"));
				atr.setSetCode(rs.getString("set_code"));
				atr.setLineNo(rs.getLong("line_no"));
				resp.add(atr);
			}
			rs.close();			
			return resp;
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
	}
	 
	public static Long persistDocument(String documentKind,Map<String, Object> values,int importKind, DSIBean bean) throws EdmException
	{
		 return persistDocument(documentKind, values, importKind, bean, new DefaultDSIHandler());
	}

    /**
     * Zapisanie dokumentu przychodzacego.
     *
     * @param values the values
     * @param packageBarcode the package barcode
     * @throws EdmException the edm exception
     */
    public static Long persistDocument(String documentKind,Map<String, Object> values,int importKind, DSIBean bean, DSIImportHandler handler) throws EdmException
    {
        Long documentId = null;
        try
        {
            DSIManager dsiManager = new DSIManager();
            bean = prepareIncomingDefaultDSIBean(bean, documentKind,values, importKind);

            dsiManager.saveDSIBean(bean, DSApi.context().session().connection());

            DSIImportService.ImportDocument importDocument = new DSIImportService().new ImportDocument(handler);
            importDocument._importDSI(bean, null);

            dsiManager.markImportResult(bean, DSApi.context().session().connection(), false);
            documentId = bean.getDocumentId();
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
        }

        return documentId;
    }

    /**
     * Przygotowanie obiektu do importu.
     * @param importKind DSIImportHandler.INCOMING_DOCUMENT_IMPORT
     *
     * @param values the values
     */
    private static DSIBean prepareIncomingDefaultDSIBean(DSIBean bean, String documentKind,Map<String, Object> values, int importKind) throws EdmException {
        DSIBean dsiBean = bean == null ? new DSIBean() : bean;
        DocumentKind docKind = DocumentKind.findByCn(documentKind);
        Map<String, Field> dockindFields = docKind.getDockindInfo().getDockindFieldsMap();
        List<DSIAttribute> dsiAttributes = new ArrayList<DSIAttribute>();

        dsiBean.setAttributes(dsiAttributes);
        dsiBean.setBoxCode(DSIDefinitions.CODE_NO_NEED_BOX);
        dsiBean.setDocumentKind(documentKind);

        for(String attribute : values.keySet())
        {
            if(dockindFields.containsKey(attribute) && dockindFields.get(attribute).isMultiple()){
                setMulipleAttribiute(dsiAttributes,attribute,values.get(attribute));
            } else {
                dsiAttributes.add(new DSIAttribute(String.valueOf(attribute), String.valueOf(values.get(attribute))));
            }
        }
        dsiBean.setImportKind(importKind);
        dsiBean.setSubmitDate(Calendar.getInstance().getTime());
        //dsiBean.setUserImportId(String.valueOf(values.get(Prosikalogic.BARCODE_FIELD_CN)));
        dsiBean.setImportStatus(DSIManager.IMPORT_STATUS_OK);

        return dsiBean;
    }

    /**
     * gdy  jest wielowartościowe tworzenie dokumentów przez API @see DsCmisManager.persist
     * przekazuje klucze w postaci: klucz,klucz,klucz
     *
     * @param dsiAttributes
     * @param attribute
     * @param o
     */
    private static void setMulipleAttribiute(List<DSIAttribute> dsiAttributes, String cn, Object value) {
        String[] keys = StringUtils.split(String.valueOf(value),",");
        if(keys != null && keys.length > 0){
            for(String key : keys){
                dsiAttributes.add(new DSIAttribute(String.valueOf(cn), key));
            }
        }


    }

}
