package pl.compan.docusafe.service.imports.dsi;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;

public class DefaultDSIHandler extends DSIImportHandler {

	@Override
	protected void prepareImport() throws Exception {
    }

	@Override
	public String getDockindCn() {
		
		return getDsiBean().getDocumentKind();
	}
	
	@Override
	protected boolean isStartProces()
	{
		return true;
	}
	
	  @Override
	    protected void setUpDocumentBeforeCreate(Document document) throws Exception {
	        super.setUpDocumentBeforeCreate(document);
	        if(AvailabilityManager.isAvailable("addSenderAndReceiver.defaultDSIHandler", getDsiBean().getDocumentKind()))
	        	addSenderAndReceiver((OfficeDocument) document, dsiBean);
	    }

	    @Override
	    protected void setUpDocumentAfterCreate(Document document) throws Exception {
	        super.setUpDocumentAfterCreate(document);
	        if(AvailabilityManager.isAvailable("addToJournal.mainIncoming.defaultDSIHandler", getDsiBean().getDocumentKind()))
	        	Journal.bindToJournal((OfficeDocument) document, Journal.getMainIncoming());
	        if(AvailabilityManager.isAvailable("addToJournal.mainOutgoing.defaultDSIHandler", getDsiBean().getDocumentKind()))
	        	Journal.bindToJournal((OfficeDocument) document, Journal.getMainOutgoing());
	    }

}
