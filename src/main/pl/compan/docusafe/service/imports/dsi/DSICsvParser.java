package pl.compan.docusafe.service.imports.dsi;

import java.util.LinkedList;

import pl.compan.docusafe.core.imports.ImportedFileInfo;
/***
 * Parser og�lny plik�w csv. Lista attributes zawiera kolejno�c i nazwy kolumn jakie powinny znajdowac sie w pliku csv
 * @author Mariusz
 *
 */
public class DSICsvParser extends DSIParser
{
	private DSICsvBeanIterator iterator;
	
	public DSICsvParser(LinkedList<String> attributes,Integer importKind,String boxColumn,String fileColumn)
	{
		iterator = new DSICsvBeanIterator(attributes,importKind,boxColumn,fileColumn);
	}
	
	public DSICsvParser(LinkedList<String> attributes,Integer importKind,String boxColumn,String fileColumn,String encoding)
	{
		iterator = new DSICsvBeanIterator(attributes,importKind,boxColumn,fileColumn,encoding);
	}
	
	public DSICsvParser(LinkedList<String> attributes,Integer importKind,String boxColumn,String fileColumn,char fieldSeparator,char encapsulator)
	{
		iterator = new DSICsvBeanIterator(attributes,importKind,boxColumn,fileColumn,fieldSeparator,encapsulator);
	}
	
	public DSICsvParser(LinkedList<String> attributes,Integer importKind,String boxColumn,String fileColumn,char fieldSeparator,char encapsulator,String encoding)
	{
		iterator = new DSICsvBeanIterator(attributes,importKind,boxColumn,fileColumn,fieldSeparator,encapsulator,encoding);
	}

	public DSIBeanIterator getDSIBeanIterator(ImportedFileInfo importedFileInfo)
	{
		iterator.setImportedFileInfo(importedFileInfo);
		return iterator;
	}
}
