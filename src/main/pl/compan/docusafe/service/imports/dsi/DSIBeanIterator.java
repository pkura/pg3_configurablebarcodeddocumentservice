package pl.compan.docusafe.service.imports.dsi;

import java.util.Iterator;

public abstract class DSIBeanIterator implements Iterator<DSIBean>
{
	public abstract boolean hasNext();
	public abstract DSIBean next();
	public void remove(){}
}
