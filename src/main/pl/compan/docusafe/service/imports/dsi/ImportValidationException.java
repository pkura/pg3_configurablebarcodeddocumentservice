package pl.compan.docusafe.service.imports.dsi;

/**
 * 
 * @author wkutyla
 * Wyjatek za pomoca ktorego przekazywane sa bledy walidacji danych przez Handler
 */
public class ImportValidationException extends Exception 
{	
	static final long serialVersionUID = 132442;
	
	public ImportValidationException()
	{
		super();
	}
	
	public ImportValidationException(Throwable cause)
	{
		super(cause);
	}
	
    public ImportValidationException(String message)
	{
		super(message);
	}
	    
}
