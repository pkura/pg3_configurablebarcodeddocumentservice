package pl.compan.docusafe.service.imports.dsi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.common.base.Optional;
import com.google.common.collect.Maps;

import com.google.common.primitives.Ints;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.crypto.IndividualRepositoryKey;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.ImportLogic;
import pl.compan.docusafe.core.imports.ImportManager;
import pl.compan.docusafe.core.imports.ImportedFileInfo;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.aegon.AbsenceRequestDSIHandler;
import pl.compan.docusafe.parametrization.aegon.DpPteDSIHandler;
import pl.compan.docusafe.parametrization.aegon.EmployeeDSIHandler;
import pl.compan.docusafe.parametrization.ald.ALDUMOWYDSIHandler;
import pl.compan.docusafe.parametrization.eservice.EserviceDSIHandler;
import pl.compan.docusafe.parametrization.ic.ContractDsiHandler;
import pl.compan.docusafe.parametrization.ic.ICDSIHandler;
import pl.compan.docusafe.parametrization.ic.SADImportHandler;
import pl.compan.docusafe.parametrization.ilpol.LeasingDSIHandler;
import pl.compan.docusafe.parametrization.nfos.NfosEmailDSIHandler;
import pl.compan.docusafe.parametrization.p4.P4DSIHandler;
import pl.compan.docusafe.parametrization.pika.PikaDSIHandler;
import pl.compan.docusafe.parametrization.prosika.ProsikaDSIHandler;
import pl.compan.docusafe.parametrization.ra.RockwellDSIHandler;
import pl.compan.docusafe.parametrization.ul.ULDSIHandler;
import pl.compan.docusafe.parametrization.umwawa.UMWawaDSIHandler;
import pl.compan.docusafe.parametrization.wzl.WZLDSIHandler;
import pl.compan.docusafe.service.imports.ImageImportDSIHandler;
import pl.compan.docusafe.service.stagingarea.RetainedObject;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;

/** 
 * Klasa ktorej zadaniem jest wykonanie importu pojedynczego dokumentu
 * @author wkutyla
 *
 */
public abstract class DSIImportHandler 
{
	protected static final Logger log = LoggerFactory.getLogger(DSIImportHandler.class);
	protected static final Map<String, byte[]> keys = new HashMap<String, byte[]>();
	public final static String ASSIGN_USER_PARAM = "assignee";
	public final static String ASSIGN_DIVISION_GUID_PARAM = "guid";
	
	/**
	 * Metoda koryguje dokumenty 
	 * @throws Exception
	 */
	protected abstract void prepareImport() throws Exception;
	/**
	 * Metoda zwraca czy startowa� proces 
	 */
	protected abstract boolean isStartProces();

	public abstract String getDockindCn();
	public DSIParser getParser(){return null;}
	
	protected int importMethod = 1;
	
	protected DSIBean dsiBean = null;
	protected Map<String, Object> values = null;
	/**Jesli dokument nadpisujemy pod ta zmienno logika hendlera wstawia dokument do nadpisania*/
	protected Document importDocument;
	protected Box archiveBox = null;
	protected FormFile attachmentFile = null;	
	
	protected List<FormFile> attachmentsFile = null;
	
	protected StringBuilder message = new StringBuilder();
	protected DocumentKind documentKind = null;
	/**Wymagany numer pud�a archiwalnego*/
	public boolean isRequiredBox(){return true;}
	/**Wymagany za��cznik*/
	public boolean isRequiredAttachment() {return true;}
	
	public final static int INCOMING_DOCUMENT_IMPORT = 1;
	public final static int ARCHIVE_ONLY_IMPORT = 2;
	public final static int OUTGOING_DOCUMENT_IMPORT = 3;
	public final static int INTERNAL_DOCUMENT_IMPORT = 4;
	public final static int TO_STAGGING_AREA_IMPORT = 5;
	
	public final static String ROCKWELL_DOCUMENT_KIND = "rockwell";

	public void actionAfterCreate(Long documentId, DSIBean dsiB)throws Exception
	{
		if( dsiBean.getDocumentId() == null ){
			dsiBean.setDocumentId(documentId);
		}
	}
	
	public static DSIImportHandler getHandler(ImportedFileInfo iFi)
	{
		//return getHandler(ImportManager.getDockindCn(iFi.getImportType()));
		return getHandler(ImportManager.getDockindCn(iFi.getImportType()), iFi.getImportType());
	}
	
	
	
	public static DSIImportHandler getHandler(String documentKindCn)
	{
		return getHandler(documentKindCn, null);
	}

    public static DSIImportHandler getHandler(String documentKindCn, Integer importType) {
        DSIImportHandler handler = null;
        if (documentKindCn.equalsIgnoreCase(ROCKWELL_DOCUMENT_KIND)) {
            handler = new RockwellDSIHandler();
        } else if (documentKindCn.equalsIgnoreCase(DocumentLogicLoader.INVOICE_IC_KIND)) {
            handler = new ICDSIHandler();
        } else if (documentKindCn.equalsIgnoreCase(DocumentLogicLoader.SAD_KIND)) {
            handler = new SADImportHandler();
        } else if (documentKindCn.equalsIgnoreCase(DocumentLogicLoader.CONTRACT_IC_KIND)) {
            handler = new ContractDsiHandler();
        } else if (documentKindCn.equalsIgnoreCase(DocumentLogicLoader.ALD_UMOWY_KIND)) {
            handler = new ALDUMOWYDSIHandler();
        } else if (documentKindCn.equalsIgnoreCase(DocumentLogicLoader.PROSIKA)) {
            handler = new ProsikaDSIHandler();
        } else if (documentKindCn.equalsIgnoreCase(DocumentLogicLoader.EMPLOYEE_KIND)) {
            handler = new EmployeeDSIHandler();
        } else if (documentKindCn.equalsIgnoreCase(DocumentLogicLoader.DP_PTE_KIND)) {
            handler = new DpPteDSIHandler();
        } else if (documentKindCn.equalsIgnoreCase(DocumentLogicLoader.P4_KIND)) {
            handler = new P4DSIHandler();
        } else if (documentKindCn.equalsIgnoreCase(DocumentLogicLoader.AKT_NOTARIALNY)) {
            handler = new UMWawaDSIHandler();
        } else if (documentKindCn.equalsIgnoreCase(DocumentLogicLoader.ABSENCE_REQUEST)) {
            handler = new AbsenceRequestDSIHandler();
        } else if (documentKindCn.equalsIgnoreCase(DocumentLogicLoader.DL_KIND)) {
            handler = new LeasingDSIHandler();
        } else if (documentKindCn.equalsIgnoreCase("eservice")) {
            handler = new EserviceDSIHandler();
        } else if(documentKindCn.equals(DocumentLogicLoader.PIKA_DOCKIND)){
        	handler = new PikaDSIHandler();
        } else if(documentKindCn.equals(DocumentLogicLoader.NORMAL_KIND)){
        	handler = new WZLDSIHandler();
        } else if(documentKindCn.equals(DocumentLogicLoader.NFOS_EMAIL)){
        	handler = new NfosEmailDSIHandler();
        } else if(documentKindCn.equals("ul_normal_out")){
        	handler = new ULDSIHandler();
        } else if(documentKindCn.equals("normal.image_import")) {
            handler = new ImageImportDSIHandler();
        } else {
        	handler = new DefaultDSIHandler();
        }
        return handler;
    }
	
	public static DSIImportHandler getHandler(DSIBean dsi,Box box, FormFile file) throws Exception
	{
		DSIImportHandler handler = getHandler(dsi.getDocumentKind());
		if(handler == null)
			throw new Exception("Nieznany typ importu "+dsi.getDocumentKind());
		handler.setDsiBean(dsi); 
		handler.setArchiveBox(box);
		handler.setAttachmentFile(file);
		return handler;
	}
	
	public static DSIImportHandler getHandler(DSIBean dsi,Box box, List<FormFile> files) throws Exception
	{
		DSIImportHandler handler = getHandler(dsi.getDocumentKind());
		if(handler == null)
			throw new Exception("Nieznany typ importu "+dsi.getDocumentKind());
		handler.setDsiBean(dsi); 
		handler.setArchiveBox(box);
		handler.setAttachmentsFile(files);
		return handler;
	}

    public static DSIImportHandler getHandler(DSIBean dsi, DSIImportHandler handler, Box box, List<FormFile> files) throws Exception
    {
        if(handler == null)
            throw new Exception("Nieznany typ importu "+dsi.getDocumentKind());
        handler.setDsiBean(dsi);
        handler.setArchiveBox(box);
        handler.setAttachmentsFile(files);
        return handler;
    }
	
	public void doImport() throws Exception
	{
		IndividualRepositoryKey dks = new IndividualRepositoryKey();
		dks.setKeyCode(getDockindCn());
		dks.setKey(keys.get(getDockindCn()));
		DSApi.context().credentials().addDocusafeKeyStore(dks);
		
		log.debug("[DSIImportHandler/doImport] Started ...");
		documentKind = DocumentKind.findByCn(getDockindCn());
		log.info("[DSIImportHandler] getDockindCn(): " + getDockindCn() + ", documentKind: " + documentKind);
		values = new TreeMap<String, Object>();
		Iterator<DSIAttribute> it = dsiBean.getAttributes().iterator();
		while (it.hasNext())
		{
			DSIAttribute atr = it.next();
			Field f = documentKind.getFieldByCn(atr.getAttributeCn());
			if(f != null && f.isMultiple())
			{
				if(values.containsKey(atr.getAttributeCn()))
				{
					List<Object> multiAtr = (List<Object>) values.get(atr.getAttributeCn());
					multiAtr.add(atr.getAttributeValue());
					values.put(atr.getAttributeCn(), multiAtr);
				}			
				else
				{
					List<Object> multiAtr = new ArrayList<Object>();
					multiAtr.add(atr.getAttributeValue());
					values.put(atr.getAttributeCn(), multiAtr);					
				}
			}
			else
			{
				values.put(atr.getAttributeCn(), atr.getAttributeValue());
			}
		}		
		prepareImport();
		//documentKind.logic().correctValues(values, documentKind);
		Long documentId = null;
		if(dsiBean.getImportKind() != null)
			importMethod = dsiBean.getImportKind();
		switch (importMethod)
		{
			case INCOMING_DOCUMENT_IMPORT: 
				documentId = createOfficeDocument();
			break;
			case ARCHIVE_ONLY_IMPORT:
				documentId = createArchiveDocument();
			break;
			case OUTGOING_DOCUMENT_IMPORT:
				documentId = createOutOfficeDocument();
			break;			
			case INTERNAL_DOCUMENT_IMPORT:
				documentId = createInternalOfficeDocument();
			break;
			case TO_STAGGING_AREA_IMPORT:
				documentId = createStaggingAreaEntry();
			break;
		}		
		actionAfterCreate(documentId,dsiBean);
		
		
		dsiBean.setImportStatus(2);
		dsiBean.setErrCode("OK");
	}
	
	/**
	 * Tworzy obiekt do zapisu do dsi_import_table.
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public void supplyImport(File file, DSIBean dsiBean) throws Exception
	{
	}
	
	/**
	 * Akcja wywo�ywana w momencie gdy ju� taki plik w importcie istnieje podczas DSISupplyHandler
	 * Je�li zwraca false dokument nie jest zaciagany podczas zasialania.
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public boolean supplyFileExist(File file) throws Exception
	{
		return false;
	}

	/**
	 * Metoda dodaje dokument wewnetrzny
	 * @return
	 * @throws Exception
	 */
	protected Long createInternalOfficeDocument() throws Exception
	{
		OutOfficeDocument document = null;
		if(importDocument != null)
		{
			document = (OutOfficeDocument) importDocument;
			//if(document.getOfficeNumber() != null)
			//{
				setUpDocumentBeforeCreate(document);
				overrideDocument(document);
				return document.getId();
			//}
		}
		else
			document = new OutOfficeDocument();
		
		
		document.setPermissionsOn(false);
		setUpDocumentBeforeCreate(document);
		
		/* Atrybuty office */
		document.setSummary(documentKind.getName());
        document.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        document.setDivisionGuid(DSDivision.ROOT_GUID);
        document.setCurrentAssignmentAccepted(Boolean.FALSE);
        document.setCreatingUser(DSApi.context().getPrincipalName());
        document.setInternal(true);
        
        
        document.setForceArchivePermissions(false);
        document.setAssignedDivision(DSDivision.ROOT_GUID);
        document.setClerk(DSApi.context().getPrincipalName());
        
        document.create();
        
        setUpDocumentAfterCreate(document);
        
        document.addAssignmentHistoryEntry(new AssignmentHistoryEntry(AssignmentHistoryEntry.IMPORT,DSApi.context().getPrincipalName(), "x","x","x","x", false, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION), true, null);
        
        //WorkflowFactory.createNewProcess(document, true, "Document accepted");
        ActionEvent event = getActionEvent();
		document.getDocumentKind().logic().onStartProcess(document, event);
        
		return document.getId();
	}

    private ActionEvent getActionEvent() throws EdmException {
        pl.compan.docusafe.webwork.event.ActionEvent event = new ActionEvent(null, null);
        DSUser user = DSApi.context().getDSUser();
        event.setAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, user.getName());

        DSDivision[] divisions = user.getDivisionsWithoutGroupPosition();
        if (divisions != null && divisions.length > 0) {
            event.setAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, divisions[0].getGuid());
        } else {
            event.setAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
        }

        return event;
    }

	/**
	 * Metoda dodaje dokument wychodzacy
	 * @return
	 * @throws Exception
	 */
	protected Long createOutOfficeDocument() throws Exception
	{
		OutOfficeDocument document = new OutOfficeDocument();
		
		document.setPermissionsOn(false);
		setUpDocumentBeforeCreate(document);
		
		/* Atrybuty office */
		document.setSummary(documentKind.getName());
        document.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        document.setDivisionGuid(DSDivision.ROOT_GUID);
        document.setCurrentAssignmentAccepted(Boolean.FALSE);
        document.setCreatingUser(DSApi.context().getPrincipalName());

        //Recipient rec = new Recipient();
        //rec.create();
        //document.addRecipient(rec);
        
        
        document.setForceArchivePermissions(false);
        document.setAssignedDivision(DSDivision.ROOT_GUID);
        document.setClerk(DSApi.context().getPrincipalName());
        
        document.create();
        
        setUpDocumentAfterCreate(document);
        
        document.addAssignmentHistoryEntry(new AssignmentHistoryEntry(AssignmentHistoryEntry.IMPORT,DSApi.context().getPrincipalName(), "x","x","x","x", false, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION), true, null);
        
        if(isStartProces())
	        if(WorkflowFactory.jbpm)
	        {
	        	pl.compan.docusafe.webwork.event.ActionEvent event = new ActionEvent(null, null);
				event.setAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, "admin");
				event.setAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
	             try
	             {
	     	        //uruchomienie procesu okre�lonego w dockindzie
	            	 document.getDocumentKind().logic().onStartProcess(document, event);
	     	        TaskSnapshot.updateByDocument(document);
	             }
	             catch (Exception e)
	             {
	             	log.error("B��d przy uruchamianiu procesu: "+e.getMessage(), e);
	             }
	        }
	        else
	        {
	        	WorkflowFactory.createNewProcess(document, true, "Document accepted");
	        }

	//	document.getDocumentKind().logic().onStartProcess(document);
        
		return document.getId();
	}
	
	protected Long createOfficeDocument() throws Exception
	{
		InOfficeDocument document = null;
		if(importDocument != null)
		{
			document = (InOfficeDocument) importDocument;
			if(document.getOfficeNumber() != null)
			{
				setUpDocumentBeforeCreate(document);
				overrideDocument(document);
				return document.getId();
			}
		}
		else
			document = new InOfficeDocument();
		
		//Importujemy - kontrola moze byz wylaczona
		document.setPermissionsOn(false);
		setUpDocumentBeforeCreate(document);
		
		//Sprawdzenie duplikat�w 
		SearchResults<Document> duplicateDoc = documentKind.checkDuplicate(values);
		if (duplicateDoc != null && duplicateDoc.count() > 0)
        {
			Long lastDoc = new Long(0);
			while(duplicateDoc.hasNext())
            {
                Document tmpdoc = duplicateDoc.next();
                if(tmpdoc.getId() > lastDoc)
                    lastDoc = tmpdoc.getId();                
            }
            document.setDuplicateDoc(lastDoc);
        }
		
        /* Atrybuty office */
		document.setSummary(documentKind.getName());
        document.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        document.setDivisionGuid(DSDivision.ROOT_GUID);
        document.setCurrentAssignmentAccepted(Boolean.FALSE);
        document.setCreatingUser(DSApi.context().getPrincipalName());

        document.setForceArchivePermissions(false);
        document.setAssignedDivision(DSDivision.ROOT_GUID);
        document.setClerk(DSApi.context().getPrincipalName());
        if(document.getSender() == null && !AvailabilityManager.isAvailable("dontCreateSenderOnImpportetInofficeDocument"))
        {
        	Sender sender = new Sender();
        	sender.create();
        	document.setSender(sender);
        }
        //document.setCtime(new Date());
        document.setOriginal(true);
        Calendar currentDay = Calendar.getInstance();
        currentDay.setTime(GlobalPreferences.getCurrentDay());
        document.setIncomingDate(currentDay.getTime());
        
        List<InOfficeDocumentKind> kinds = InOfficeDocumentKind.list();
        String kindName = documentKind.logic().getInOfficeDocumentKind();
        boolean canChooseKind = (kindName == null);
        if (!canChooseKind)
        {                       
            for (InOfficeDocumentKind inKind : kinds)
            {
                if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                	document.setKind(InOfficeDocumentKind.find(inKind.getId()));
            }
        }
        else
        {
        	document.setKind(InOfficeDocumentKind.find(1));            	
        }
       // DSApi.context().session().flush();
        document.create();
        
        setUpDocumentAfterCreate(document);
//        Journal journal = Journal.getMainIncoming();
//        Long journalId;
//        journalId = journal.getId();
//        Integer sequenceId = null;
//        sequenceId = Journal.TX_newEntry2(journalId, document.getId(), new Date(currentDay.getTime().getTime()));
//        
//        document.bindToJournal(journalId, sequenceId);
        
        document.addAssignmentHistoryEntry(new AssignmentHistoryEntry(AssignmentHistoryEntry.IMPORT,
                DSApi.context().getPrincipalName(), "x",
               "x","x","x", false, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION), true, null);
		//WorkflowFactory.createNewProcess(document, true, "Document accepted");

        DocumentLogic logic = document.getDocumentKind().logic();
        onStartProcessAction(logic, document);
       //
		if (AvailabilityManager.isAvailable("DSIImportHandler.multiple.OfficePoints"))
		{
			logic.onStartProcess(document, null, (String)values.get("FILEPREFIX"));
		} else
		{
			logic.onStartProcess(document);
		}
        return document.getId();
	}

    /**
     * <p>
     *     Execute import action on ImportLogic.
     * </p>
     * @param logic
     * @param document
     */
    protected void onStartProcessAction(DocumentLogic logic, Document document) {
        if(logic instanceof ImportLogic) {
            ((ImportLogic)logic).onImportAction(document);
        }
    }

	protected Long createArchiveDocument() throws Exception
	{
		
		
		String summary = documentKind.getName();
		Document document = null;
		if(importDocument != null)
			document = importDocument;
		else
			document = new Document(summary,summary);

		setUpDocumentBeforeCreate(document);
		//document.create();
		
        document.setDocumentKind(documentKind);
        document.setForceArchivePermissions(true);
        document.setCtime(new Date());;
        document.setFolder(Folder.getRootFolder());

        document.create();                           
        
        setUpDocumentAfterCreate(document);
        
        return document.getId();

	}
	
	protected Long createStaggingAreaEntry(){
        String title = "PLIK TEST ";        
        Map<String, String> properties = new HashMap<String, String>();
        properties.put("banner", "banner");
        properties.put("faxID", "faxID");
        properties.put("subject", "temat");
        properties.put("fromline", "fromLine");
        properties.put("rfaxid", "rfaID");
        properties.put("port", "port");
        File[] zalaczniki = new File[1];
        zalaczniki[0] = attachmentsFile.get(0).getFile();
        try {
			RetainedObject.create(title, "fax", properties, zalaczniki);
		} catch (EdmException e) {
			log.error("B��d podczas dodawania dokumentu do poczekalni: " + e.getStackTrace());
		}
		return null;
	}
	
	protected void setUpDocumentBeforeCreate(Document document) throws Exception
	{
		documentKind.logic().correctImportValues(document,values,message);
		document.setDocumentKind(documentKind);
        documentKind.logic().globalValidate(values, document);
	}
	
	protected void setUpDocumentAfterCreate(Document document) throws Exception
	{
		
		dsiBean.setDocumentId(document.getId());
		
		if(this.attachmentsFile != null)
		{
			
			for(FormFile attachmentFile: this.attachmentsFile)
			{
                addAttachment(document, attachmentFile);
			}
		}
		
		if (archiveBox != null) 
		{
			document.setBox(archiveBox);
		}
		documentKind.saveDictionaryValues(document, values);
		documentKind.saveMultiDictionaryValues(document, values);
		documentKind.logic().removeValuesBeforeSet(values);
		documentKind.setOnly(document.getId(), values);
        documentKind.logic().archiveActions(document,DocumentLogic.TYPE_IN_OFFICE);
        documentKind.logic().documentPermissions(document);
        
	}

    protected void addAttachment(Document document, FormFile formFile) throws EdmException, FileNotFoundException {
        Attachment attachment = new Attachment(formFile.getName());
        document.createAttachment(attachment);
        attachment.createRevision(formFile.getFile()).setOriginalFilename(attachmentFile == null ? formFile.getName() : formFile.getName());
    }

	/*protected void setUpDocumentAfterCreate(Document document) throws Exception
	{
		dsiBean.setDocumentId(document.getId());
		if (attachmentFile != null && attachmentFile.getFile() != null) 
		{
			Attachment attachment = new Attachment(attachmentFile.getFile().getName());
			document.createAttachment(attachment);
			attachment.createRevision(attachmentFile.getFile()).setOriginalFilename(
					attachmentFile.getName());
		}
		if (archiveBox != null) 
		{
			document.setBox(archiveBox);
		}
		documentKind.set(document.getId(), values);
        documentKind.logic().archiveActions(document,DocumentLogic.TYPE_IN_OFFICE);
        documentKind.logic().documentPermissions(document);
        
	}*/
	
	/**
	 * Metoda nadpisuje istniejsacy wczesniej dokument
	 * @param document
	 * @throws Exception
	 * @throws EdmException
	 */
	private void overrideDocument(OfficeDocument document) throws Exception, EdmException
	{
		dsiBean.setDocumentId(document.getId());
		
		if (attachmentsFile != null && attachmentsFile.size() == 1) 
		{
			attachmentFile = attachmentsFile.get(0);
			
			List<Attachment> attachments = document.getAttachments();
			if(attachments != null && attachments.size() > 0)
			{
				Attachment attachment = attachments.get(0);
				attachment.createRevision(attachmentFile.getFile()).setOriginalFilename(attachmentFile.getName());
			}
			else
			{
				Attachment attachment = new Attachment(attachmentFile.getFile().getName());
				document.createAttachment(attachment);
				attachment.createRevision(attachmentFile.getFile()).setOriginalFilename(attachmentFile.getName());
			}
		}
		else if(attachmentsFile != null)
		{
			for(FormFile attachmentFile : this.attachmentsFile)
			{
				Attachment attachment = new Attachment(attachmentFile.getFile().getName());
				document.createAttachment(attachment);
				attachment.createRevision(attachmentFile.getFile()).setOriginalFilename(attachmentFile.getName());
			}
		}
		
		if (archiveBox != null) 
		{
			document.setBox(archiveBox);
		}
		documentKind.setWithHistory(document.getId(), values, false);
        documentKind.logic().archiveActions(document,DocumentLogic.TYPE_IN_OFFICE);
        documentKind.logic().documentPermissions(document);		
	}
	
	/*private void overrideDocument(InOfficeDocument document) throws Exception, EdmException
	{
		dsiBean.setDocumentId(document.getId());
		if (attachmentFile != null && attachmentFile.getFile() != null) 
		{
			List<Attachment> attachments = document.getAttachments();
			if(attachments != null && attachments.size() > 0)
			{
				Attachment attachment = attachments.get(0);
				attachment.createRevision(attachmentFile.getFile()).setOriginalFilename(
						attachmentFile.getName());
			}
			else
			{
				Attachment attachment = new Attachment(attachmentFile.getFile().getName());
				document.createAttachment(attachment);
				attachment.createRevision(attachmentFile.getFile()).setOriginalFilename(
						attachmentFile.getName());
			}
		}
		if (archiveBox != null) 
		{
			document.setBox(archiveBox);
		}
		documentKind.setWithHistory(document.getId(), values, false);
        documentKind.logic().archiveActions(document,DocumentLogic.TYPE_IN_OFFICE);
        documentKind.logic().documentPermissions(document);		
	}*/
	
	public void parseImportedFile(ImportedFileInfo importedFileInfo) throws Exception
	{
		getParser().parseFile(importedFileInfo);
	}

	public DSIBean getDsiBean() {
		return dsiBean;
	}
	public void setDsiBean(DSIBean dsiBean) {
		this.dsiBean = dsiBean;
	}

	public Box getArchiveBox() {
		return archiveBox;
	}

	public void setArchiveBox(Box archiveBox) {
		this.archiveBox = archiveBox;
	}

	public FormFile getAttachmentFile() {
		return attachmentFile;
	}

	public void setAttachmentFile(FormFile attachmentFile) {
		this.attachmentFile = attachmentFile;
	}
	
	public void setAttachmentsFile(List<FormFile> attachmentsFile)
	{
		if (isRequiredAttachment())
			this.attachmentsFile = new ArrayList<FormFile>(attachmentsFile);
	}
	
	public static void addToKeysMap(String cn, byte[] key)
	{
		keys.put(cn, key);
	}

    /**
     * Tworzy sendera i receipienta w mapie atrybutu�w na podstawie przekazanego id
     * @param document
     * @param bean
     */
    protected void addSenderAndReceiver(OfficeDocument document, DSIBean bean) throws EdmException {
        String dkCn = document.getDocumentKind().getCn();
        if (bean.getAttributeByCn(Person.SENDER_CN).isPresent())
        {
            if(DwrDictionaryFacade.getDwrDictionary(dkCn, Person.SENDER_CN) == null )
            {
                log.error("Dockind {} nie ma pola SENDER", dkCn);
                return;
            }
            Person person = Person.find(Long.parseLong(bean.getAttributeValueByCn(Person.SENDER_CN).get()));

            log.error("Person.firstname: {}", person.getFirstname());
            Sender sender = new Sender();
            sender.fromMap(person.toMap());
            sender.setDictionaryGuid("rootdivision");
            sender.setDictionaryType(Person.DICTIONARY_SENDER);
            sender.setBasePersonId(person.getId());
            sender.create();
            document.setSender(sender);
        }

        if (bean.getAttributeByCn(Person.RECIPIENT_CN).isPresent())
        {
            if(DwrDictionaryFacade.getDwrDictionary(dkCn, Person.RECIPIENT_CN) == null )
            {
                log.error("Dockind {} nie ma pola REcipient", dkCn);
                return;
            }
            Person person = Person.find(Long.parseLong(bean.getAttributeValueByCn(Person.RECIPIENT_CN).get()));
            log.error("Person.firstname: {}", person.getFirstname());
            Recipient recipient = new Recipient();
            recipient.fromMap(person.toMap());
            recipient.setDictionaryGuid("rootdivision");
            recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
            recipient.setBasePersonId(person.getId());
            recipient.setDocumentId(document.getId());
            recipient.setDocument(document);
            recipient.create();
            document.addRecipient(recipient);
        }
    }
}
