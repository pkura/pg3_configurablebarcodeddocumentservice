package pl.compan.docusafe.service.imports.dsi;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;

import java.util.*;
/** 
 * Klasa reprezentujaca dane zebrane w tabelach DSI_IMPORT_TABELE
 * @author wkutyla
 *
 */
public class DSIBean implements java.io.Serializable 
{
	static final long serialVersionUID = 323;
	private Long importId;
	private String userImportId;
	private String documentKind;
	private Integer importKind;
	private Date submitDate;
	private Integer importStatus;
	private Date processDate;
	private Long documentId;
	private String errCode;
	private String filePath;
	private String boxCode;
	private byte[] imageArray;
	private String imageName;
	private String documentURL;
	
	private Collection<DSIAttribute> attributes = new ArrayList<DSIAttribute>();
	
	private boolean correct = true; 

    public DSIBean addAttribute(DSIAttribute attr) {
        attributes.add(attr);
        return this;
    }

    public Optional<DSIAttribute> getAttributeByCn(final String cn) {

        return Iterables.tryFind(attributes, new Predicate<DSIAttribute>() {
            @Override
            public boolean apply(DSIAttribute dsiAttribute) {
                return cn.equals(dsiAttribute.getAttributeCn());
            }
        });

    }

    public Optional<String> getAttributeValueByCn(final String cn) {
        Optional<DSIAttribute> opt = Iterables.tryFind(attributes, new Predicate<DSIAttribute>() {
            @Override
            public boolean apply(DSIAttribute dsiAttribute) {
                return cn.equals(dsiAttribute.getAttributeCn());
            }
        });

        if(opt.isPresent()) {
            return Optional.of(opt.get().getAttributeValue());
        } else {
            return Optional.absent();
        }
    }
	
	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public Long getImportId() {
		return importId;
	}

	public void setImportId(Long importId) {
		this.importId = importId;
	}

	public String getUserImportId() {
		return userImportId;
	}

	public void setUserImportId(String userImportId) {
		this.userImportId = userImportId;
	}

	public String getDocumentKind() {
		return documentKind;
	}

//    public String getDocumentKindCn() {
//        return StringUtils.isBlank(documentKind) ? documentKind : StringUtils.split(documentKind, ".")[0];
//    }

	public void setDocumentKind(String documentKind) {
		this.documentKind = documentKind;
	}

	public Integer getImportKind() {
		return importKind;
	}

	public void setImportKind(Integer importKind) {
		this.importKind = importKind;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public Integer getImportStatus() {
		return importStatus;
	}

	public void setImportStatus(Integer importStatus) {
		this.importStatus = importStatus;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getBoxCode() {
		return boxCode;
	}

	public void setBoxCode(String boxCode) {
		this.boxCode = boxCode;
	}

	public Collection<DSIAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(Collection<DSIAttribute> attributes) {
		this.attributes = attributes;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public byte[] getImageArray() {
		return imageArray;
	}

	public void setImageArray(byte[] imageArray) {
		this.imageArray = imageArray;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public void setDocumentURL(String documentURL) {
		this.documentURL = documentURL;
	}

	public String getDocumentURL() {
		return documentURL;
	}
}
