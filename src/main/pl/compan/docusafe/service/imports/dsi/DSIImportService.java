/**
 *
 */
package pl.compan.docusafe.service.imports.dsi;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jbpm.JbpmContext;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.FeedbackException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.workflow.DSjBPM;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.ws.im.UploadDocumentRequest;
import pl.compan.docusafe.ws.im.UploadDocumentResponse;
import pl.compan.docusafe.ws.im.UploadDocumentStatus;

/**
 * @author Mariusz Kilja�czyk Obsluga importow dokumentow dla dokumentow typu
 */
public class DSIImportService extends ServiceDriver implements Service {
	private Timer timer;
	public static final String filePath = Docusafe.getAdditionProperty("importDocument.filePath");
	private static final Log log = LogFactory.getLog(DSIImportService.class);
	private static StringManager sm = GlobalPreferences.loadPropertiesFile(
			DSIImportService.class.getPackage().getName(), null);

	private DSIManager manager = new DSIManager();

	protected void start() throws ServiceException {

		System.out.println("DSISTART");
		if (Docusafe.getAdditionProperty("importDocument").equals("false"))
			return;

		Integer time = Integer.parseInt(Docusafe
				.getAdditionProperty("importDocument.time"));
		//filePath = Docusafe.getAdditionProperty("importDocument.filePath");

		timer = new Timer(true);
		timer.schedule(new ImportDocument(), 1000, time * DateUtils.SECOND/* 30*DateUtils.SECOND */);
		System.out.println("DSISTART wystartowa�");
	}

	protected void stop() throws ServiceException {
		if (timer != null)
			timer.cancel();
	}

	protected boolean canStop() {
		return false;
	}

	public class ImportDocument extends TimerTask {

        /** @var pole definiuje handler uzywany w przypadku gdy musimy u�y� go poza wakiem*/
        private DSIImportHandler importHandler;
		public ImportDocument() {
		}

        /**
         * Wywo�anie np. new DSIImportService().new ImportDocument(new DefaultHandler());
         * @param importHandler
         */
        public ImportDocument(DSIImportHandler importHandler) {
            this.importHandler = importHandler;
        }

		public void run() {
			try {
				importFromQueue();
				reportFeedbacks();
			} catch (Throwable e) {
				log.error("Blad w czasie dzialania servicu", e);
			}
		}

		public UploadDocumentResponse importFromWebServices(
				UploadDocumentRequest request) {
			try {
				UploadDocumentResponse response = importWebServices(request);
				return response;
			}
			catch (Exception e) {
				log.error("[WS/Upload document/importFromWebServices] Import error", e);
				UploadDocumentResponse response = UploadDocumentResponse.CreateErrorResponse(e);
				return response;
			}
		}

		boolean doFeedBack = true;

		public void reportFeedbacks() throws Exception {
			DSApi.openAdmin();
			try {
				Collection<DSIBean> lista = manager.getToFeedbackList(DSApi
						.context().session().connection());
				Iterator<DSIBean> it = lista.iterator();
				while (it.hasNext()) {
					DSIBean dsi = it.next();
					reportFeedback(dsi);
				}
			} catch (Exception e) {
				log.error("Blad przy wysylaniu feedbackow", e);
			} finally {
				DSApi._close();
			}
		}

		public void reportFeedback(DSIBean dsi) throws Exception {
			try {
				FeedbackHandler handler = FeedbackHandler.getHandler(dsi);
				handler.sendFeedback();
				DSApi.context().begin();
				manager.markImportFeedback(dsi, handler, DSApi.context()
						.session().connection());
				DSApi.context().commit();

			} catch (FeedbackException f) {
				log.error(f.getMessage());
				log.debug("Blad w odpowiedzi Feedbacku", f);
			}
		}

		/**
		 *
		 * @return Ilosc zaimportowanych dokumentow
		 */
		private int importFromQueue() throws Exception {

			return doImport();
		}

		private UploadDocumentResponse importWebServices(UploadDocumentRequest request)
		throws Exception {
			UploadDocumentResponse response = new UploadDocumentResponse();
			//doImport(true, request,response);
			DSIBean bean = new DSIBean();
			List<DSIAttribute> attributes = new ArrayList<DSIAttribute>();
			for (int i = 0; i < request.getDescriptors().length; i++) {
				DSIAttribute dsiAttr = new DSIAttribute();
				dsiAttr.setAttributeCn(request.getDescriptors()[i]
						.getDescriptorName());
				dsiAttr.setAttributeValue(request.getDescriptors()[i]
						.getDescriptorValue());
				attributes.add(dsiAttr);
			}
			bean.setAttributes(attributes);
			bean.setBoxCode(request.getBoxNumber());
			bean.setDocumentKind(request.getDocumentKind());
			bean.setFilePath(request.getImageFullName());
			//bean.setImageArray(request.getImageArray());

			byte[] barray = null;
			if (request.getImageArray() != null)
			{
	            try
	            {
	            	barray = Base64.decodeBase64(request.getImageArray());
	            }
	            catch (Exception e)
	            {
	            	log.error("Blad w czasie konwersji tablicy bajt�w obrazu z base64", e);
	            	return UploadDocumentResponse.CreateErrorResponse(e);
		        }
			}
			bean.setImageArray(barray);

			bean.setImageName(request.getImageName());
			bean.setImportId(request.getImportId());
			try {
				DSApi.openAdmin();
				try {
					importOneDSI(bean, response);
				} catch (Exception e) {
					bean.setErrCode(e.getMessage());
					bean.setImportStatus(DSIManager.IMPORT_STATUS_ERROR);
					log.error(e.getMessage(), e);
					response = UploadDocumentResponse.CreateErrorResponse(e);
				} finally {
					DSApi._close();
				}
				DSApi.openAdmin();
				try {
					DSApi.context().begin();
					java.sql.Connection con = DSApi.context().session()
							.connection();
					manager.markImportResult(bean, con, true);
					DSApi.context().commit();
				} catch (Exception e) {
					log.error("Blad w czasie zapisu statusu importu ", e);
					response = UploadDocumentResponse.CreateErrorResponse(e);
				} finally {
					DSApi._close();
				}
			} catch (Exception e1) {
				log.error("Blad w czasie importu id=" + bean.getImportId(), e1);
				response = UploadDocumentResponse.CreateErrorResponse(e1);
			}
			return response;
		}

		/**
		 *
		 * @return Ilosc zaimportowanych dokumentow
		 */
		private int doImport() throws Exception {

			int resp = 0;
			java.sql.Connection con = null;
			try {
				Iterator<DSIBean> it = null;

				DSApi.openAdmin();
				con = DSApi.context().session().connection();
				Collection<DSIBean> beans = manager.getToImportList(con);
				it = beans.iterator();
				DSApi._close();

				while (it.hasNext()) {

					DSIBean dsi = it.next();
					try {
						try {
                            DSApi.openAdmin();
							importOneDSI(dsi);
							if (dsi.getImportStatus().intValue() == 2) {
								resp++;
							}
						} catch (Exception e) {
							dsi.setErrCode(e.getMessage());
							dsi.setImportStatus(-1);
							log.error(e.getMessage(), e);
						} finally {
							DSApi._close();
						}
						try {
                            DSApi.openAdmin();
							DSApi.context().begin();
							con = DSApi.context().session().connection();
							manager.markImportResult(dsi, con, false);
							DSApi.context().commit();
						} catch (Exception e) {
							log.debug("Blas w czasie opisu importu ", e);
						} finally {
							DSApi._close();
						}
					} catch (Exception e1) {
						log.error("Blad w czasie importu id=" + dsi.getImportId(), e1);
					}
				}
			} finally {
				DSApi._close();
			}
			return resp;
		}

		/** Metoda pomocnicza
		 *
		 * @param failTest - jesli true to byl blad
		 * @param dsi
		 * @param errMessage
		 */
		private void checkTest(boolean failTest, DSIBean dsi, String errMessage) {
			if (failTest) {
				dsi.setCorrect(false);
				dsi.setErrCode(errMessage);
			}
		}

		public boolean importOneDSI(DSIBean dsi) throws Exception 
		{
			return importOneDSI(dsi, null);			
		}

		/**
		 *
		 * @param dsi
		 * @return
		 * @throws Exception
		 */
		private boolean importOneDSI(DSIBean dsi,
				UploadDocumentResponse response) throws Exception {
			try{
				DSjBPM.open();
				return _importDSI(dsi,response);
			} finally {
				JbpmContext ctx = null;
				try
				{
					ctx = DSjBPM.context();
				}
				catch (Exception e)
				{
					log.debug(e.getMessage(), e);
				}
				DSjBPM.closeContextQuietly(ctx);
			}
		}


		protected boolean _importDSI(DSIBean dsi,	UploadDocumentResponse response) throws Exception {
			//boolean imported = false;
			boolean resp = false;
			FormFile attachment = null;
			List<FormFile> attachments = null;
			boolean failTest = true;
			String errorString = "";
			
			// sprawdzenie czy wymagany za��cznik (je�li pole file_path == noneed, zalacznik niewymagany)
			if (dsi.getFilePath() != null && !dsi.getFilePath().equalsIgnoreCase(DSIDefinitions.CODE_NO_NEED_ATTACHMENT)) {				
			
			if (dsi.getImageArray() != null && dsi.getImageName() != null)
			{
				failTest = false;
				attachment = new FormFile(dsi.getImageArray(), dsi.getImageName());
			}
			/** MULTI ATTACHMENTS TEST **/
			else if(dsi.getFilePath() != null && dsi.getFilePath().equalsIgnoreCase(DSIDefinitions.CODE_FILES_IN_ATTRIBUTES))
			{
				attachments = new ArrayList<FormFile>();
				failTest = false;
				
				for(DSIAttribute attr : dsi.getAttributes())
				{
					if(attr.getAttributeCn().equalsIgnoreCase("FILE"))
					{
						String realPath = "";
                        String path = attr.getAttributeValue();
						if(isAbsolutePath(path))
						{
							realPath = path;
						}
						else
						{
							realPath = filePath + "/" + path;
						}
						
						File currentAttachment = new File(realPath);
						
						if(!currentAttachment.exists())
						{
							failTest = true;
						}
						else
						{
							attachments.add(new FormFile(currentAttachment, currentAttachment.getName(), null));
						}
					}
				}
				
				errorString = DSIDefinitions.ERR_023_MISSING_MULTI_ATTACHMENT;
			}
			/** MULTI ATTACHMENTS TEST : END**/
			else if (dsi.getFilePath() != null)
			{
				String realPath = null;
                String path = dsi.getFilePath();
				/**Jesli sciezka bezwzgledna to bez addsa*/
				if(path.contains("noneed"))
				{
					failTest = false;
					attachment = null;
				}
				else if(isAbsolutePath(path))
				{
					realPath = path;
				}
				else if(Docusafe.getAdditionProperty("importDocument.multiFolderImport").equals("true")){
					realPath = path;
				}
				else
				{
					realPath = filePath + "/" + path;
				}

				File f = new File(realPath);
				if (f.exists()) {
					failTest = false;
					attachment = new FormFile(f, f.getName(), null);
				}
				else
				{
					failTest = true;
					errorString = DSIDefinitions.ERR_001_MISSING_ATTACHMENT + ":" + filePath + "/" + dsi.getFilePath();
				}
			}
			
			if(attachment != null && attachments == null)
			{
				attachments = new ArrayList<FormFile>();
				attachments.add(attachment);
			}
			} else {
				failTest = false;
			}
			
			if(AvailabilityManager.isAvailable("dsi.splitA3toA4"))
			{
				Integer margin = Integer.parseInt(Docusafe.getAdditionProperty("dsi.splitA3toA4.margin"));
				 ArrayList<FormFile> splitAttachments = new ArrayList<FormFile>();
				 for (FormFile formFile : attachments) 
				 {
					File source = formFile.getFile();
					File splitFile = PdfUtils.splitA3toA4(source,margin);
					splitAttachments.add(new FormFile(splitFile, source.getName(), null));
				 }
				 attachments = new ArrayList<FormFile>(splitAttachments);
			}
			
			
			checkTest(failTest, dsi, errorString);
			//checkTest(failTest, dsi, DSIDefinitions.ERR_001_MISSING_ATTACHMENT + ":" + filePath + "/" + dsi.getFilePath());
			
			Box box = null;
			if (dsi.isCorrect())
			{
				failTest = true;
				if(dsi.getBoxCode() != null && dsi.getBoxCode().equalsIgnoreCase(DSIDefinitions.CODE_NO_NEED_BOX))
				{
					failTest = false;
					log.info("pomijam pudlo");
				}
				else if (dsi.getBoxCode() != null)
				{
					String boxLine = DocumentKind.getDockindInfo(dsi.getDocumentKind()).getProperties().get("box-line");
					try
					{
						box = Box.findByName(boxLine, dsi.getBoxCode());
					}
					catch (Exception e)
					{
						DSApi.context().begin();
						Box newBox = new Box(dsi.getBoxCode());
						newBox.setLine(boxLine);
						newBox.create();
						box = newBox;
						DSApi.context().commit();
					}
					if (box != null)
					{
						failTest = false;
					}
				}
				
				checkTest(failTest, dsi, DSIDefinitions.ERR_002_MISSING_BOX);
			}
			if (dsi.isCorrect()) {
				failTest = true;
				if (dsi.getAttributes().size() == 0)
					checkTest(failTest, dsi,
							DSIDefinitions.ERR_003_MISSING_ATTRIBUTES);
			}
			/* Po wstepnej weryfikacji przechodzimy do sprawdzenia */
			if (dsi.isCorrect())
			{
				boolean commit = true;
				DSApi.context().begin();
				//DSIImportHandler handler = DSIImportHandler.getHandler(dsi, box, attachment);

				DSIImportHandler handler = null;
                if(importHandler == null ){
                    //wyszukuje handler z listy
                    handler = DSIImportHandler.getHandler(dsi, box, attachments);
                } else {
                    //ustawia handler podany w konstruktorze
                    handler = DSIImportHandler.getHandler(dsi,importHandler, box, attachments);
                }
				try {
					handler.doImport();
				}
				catch (ImportValidationException ve)
				{
					log.error("Blad w czasie parsowania argument�w import_id=" + dsi.getImportId(), ve);
					DSApi.context().rollback();
					dsi.setImportStatus(-1);
					dsi.setErrCode(ve.getMessage());
					dsi.setDocumentId(new Long(-1));
					commit = false;
					//
					if (response != null) {
						response.setStatusCode(UploadDocumentStatus.AttributeNotParsed);
						response.appendStatusDesc(ve.getMessage());
					}
				}
				catch (Exception e) {
					log.error("Blad w czasie importu dokumentu import_id=" + dsi.getImportId(), e);
					DSApi.context().rollback();
					dsi.setImportStatus(-1);
					String errCode =e.getMessage();
					if(errCode == null || errCode.equals("NULL"))
					{
						if(e.getStackTrace() != null && e.getStackTrace()[0] != null)
							errCode = e.getStackTrace()[0].toString();
					}
					dsi.setErrCode(errCode);
					dsi.setDocumentId(-1l);
					commit = false;
					//
					if (response != null) {
						response.assignError(e);
					}
				}
				if (commit) {
					DSApi.context().commit();
				}
			}
			else
			{
				log.error("DSI jest zawiera b��dy " + dsi.getErrCode());
				dsi.setImportStatus(-1);
				dsi.setDocumentId(-1l);
			}

			// set response
			if (response != null) {
				// check if request was completed
				if (dsi.getImportStatus().equals(2))
				{
					response.setDsDocumentID(dsi.getDocumentId());
					response.setStatusCode(UploadDocumentStatus.Success);
					response.setDocumentURL(dsi.getDocumentURL());
					Calendar cal = Calendar.getInstance();
					cal.setTime(dsi.getProcessDate());
					response.setArchiveDate(cal);
					log.error("response.getDocumentURL()"+response.getDocumentURL());
					log.error("response.getArchiveDate()"+response.getArchiveDate());
				}
				// process if there wasn't any internal error before
				else if (response.getStatusCode() == UploadDocumentStatus.Success) {
					response.assignDSIResponse(dsi.getErrCode());
				}
			}

			return resp;
		}

	}

    protected boolean isAbsolutePath(String path) {
        final File file = new File(path);
        return file.isAbsolute();
    }

}
