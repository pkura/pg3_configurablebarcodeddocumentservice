package pl.compan.docusafe.service.imports.dsi;

import javax.xml.rpc.holders.LongHolder;

import org.tempuri.ServiceLocator;
import org.tempuri.ServiceSoapStub;
import pl.compan.docusafe.core.FeedbackException;

/**
 * Klasa odpowiedzialna za feedback do 
 * @author wkutyla
 *
 */
public class IronMountainFeedbackHandler extends FeedbackHandler 
{
	protected void sendFeedbackBody() throws FeedbackException
	{				
			try
			{
				String hashParam = dsiBean.getUserImportId() + dsiBean.getDocumentId().toString() + "#Iron^";
				String hash = org.apache.commons.codec.digest.DigestUtils.md5Hex(hashParam);
				ServiceSoapStub binding;
				binding = (ServiceSoapStub) new ServiceLocator().getServiceSoap();
				int res = binding.reportDocStatus(new Long(dsiBean.getUserImportId()), new LongHolder(dsiBean.getDocumentId()), dsiBean.getImportStatus(), dsiBean.getErrCode(),hash);
				feedbackCode = "SOAP response="+res;							
			}
			catch (Exception e) 
			{
				throw new FeedbackException ("[IM] - nieudane wyslanie do Webservicu import_id=" + dsiBean.getImportId() ,e);
			}
	}
}
