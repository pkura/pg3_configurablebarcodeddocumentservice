package pl.compan.docusafe.service.imports.dsi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVStrategy;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.imports.ImportedFileInfo;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DSICsvBeanIterator extends DSIBeanIterator
{
	private static final Logger log = LoggerFactory.getLogger(DSICsvBeanIterator.class);
	private ImportedFileInfo importedFileInfo;
	private CSVParser csvParser;
	private BufferedReader reader;
	private String[] fields;
	private String encoding;
	private char fieldSeparator = ';'; 
	private char encapsulator = '"';
	/**
	 * Lista atrybut�w jakie powinny znajdowa� si� w pliku CSV
	 */
	private LinkedList<String> attributes;
	private Integer importKind;
	private String boxColumn;
	private String fileColumn;
	
	public DSICsvBeanIterator(LinkedList<String> attributes,Integer importKind,String boxColumn,String fileColumn)
	{
		initIterator(attributes, importKind, boxColumn, fileColumn);
	}
	
	public DSICsvBeanIterator(LinkedList<String> attributes,Integer importKind,String boxColumn,String fileColumn,String encoding)
	{
		this.encoding = encoding;
		initIterator(attributes, importKind, boxColumn, fileColumn);
	}
	
	public DSICsvBeanIterator(LinkedList<String> attributes,Integer importKind,String boxColumn,String fileColumn,char fieldSeparator,char encapsulator)
	{
		this.encapsulator = encapsulator;
		this.fieldSeparator = fieldSeparator;
		initIterator(attributes, importKind, boxColumn, fileColumn);
	}
	
	public DSICsvBeanIterator(LinkedList<String> attributes,Integer importKind,String boxColumn,String fileColumn,char fieldSeparator,char encapsulator,String encoding)
	{
		this.encapsulator = encapsulator;
		this.fieldSeparator = fieldSeparator;
		this.encoding = encoding;
		initIterator(attributes, importKind, boxColumn, fileColumn);
	}
	
	private void initIterator(LinkedList<String> attributes,Integer importKind,String boxColumn,String fileColumn)
	{
		this.attributes = attributes;
		this.boxColumn = boxColumn;
		this.importKind= importKind;
		this.fileColumn = fileColumn;
	}

	public void setImportedFileInfo(ImportedFileInfo importedFileInfo)
	{
		try
		{
			File f = new File(importedFileInfo.getTiffDirectory()+"/"+importedFileInfo.getFileName());
			this.importedFileInfo = importedFileInfo;
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(f),(encoding != null ? encoding : Docusafe.getAdditionProperty(Configuration.ADDITION_IMPORT_ENCODING))));
			CSVStrategy strategy = new CSVStrategy(fieldSeparator,encapsulator,CSVStrategy.COMMENTS_DISABLED,false,false,true);			
			csvParser = new CSVParser(reader,strategy);
		}
		catch (Exception e) 
		{
			log.error("",e);
		}			
	}

	public DSIBean next()
	{
		return createBean();
	}

	private DSIBean createBean()
	{
		DSIBean resp = new DSIBean();
		List<DSIAttribute> attrs = new LinkedList<DSIAttribute>();
		resp.setDocumentKind(importedFileInfo.getDocumentKind());
		resp.setImportKind(importKind);
		resp.setSubmitDate(new Date());
		resp.setImportStatus(DSIManager.IMPORT_STATUS_PARS);	
		String boxCode = null;
		String fileName = null;
		if(fields == null || fields.length < attributes.size())
		{
			resp.setErrCode(DSIDefinitions.ERR_022_COLUMN_MISSING);
			resp.setBoxCode("");
			resp.setFilePath("");
			resp.setImportStatus(DSIManager.IMPORT_STATUS_ERROR);
		}
		else
		{
			for (int i = 0; i < attributes.size(); i++)
			{
				if(boxCode == null && boxColumn.equals(attributes.get(i)))
				{
					boxCode = fields[i];
					continue;
				}
				if(fileName == null && fileColumn.equals(attributes.get(i)))
				{
					fileName = fields[i];
					continue;
				}
				attrs.add(new DSIAttribute(attributes.get(i),fields[i]));
			}
			resp.setBoxCode(boxCode);
			String filePath = importedFileInfo.getTiffDirectory()+"/"+fileName;
			filePath = filePath.replace("\\", "/");
			resp.setFilePath(filePath);
		}
		resp.setAttributes(attrs);	
		resp.setUserImportId(importedFileInfo.getId().toString());
		return resp;
	}

	public boolean hasNext()
	{
		try
		{
			boolean hasNext = (fields = csvParser.getLine()) != null;
			if(!hasNext)
			{
				reader.close();
			}
			return hasNext;
		}
		catch (Exception e) 
		{
			log.error("",e);
			return false;
		}
	}
}
