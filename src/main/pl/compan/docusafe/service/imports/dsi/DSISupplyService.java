package pl.compan.docusafe.service.imports.dsi;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.imports.dsi.DSIImportService.ImportDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;

/**
 * Service do pobierania plików z katalogu i zasilania tabeli dsi_import_table
 * @author lukaszw
 */
public class DSISupplyService extends ServiceDriver implements Service 
{
	private Timer timer;
	private static final String filePath = Docusafe.getAdditionProperty("importDocument.filePath");
	private static final String importDockind = Docusafe.getAdditionProperty("importDocument.dockindCn");
	private static final Log log = LogFactory.getLog(DSISupplyService.class);
	private static StringManager sm = GlobalPreferences.loadPropertiesFile(
			DSISupplyService.class.getPackage().getName(), null);
	
	private DSIManager manager = new DSIManager();
	
	@Override
	protected void start() throws ServiceException {
		console(Console.INFO, "DSI SUPPLY START");
		if (Docusafe.getAdditionProperty("importDocument").equals("false"))
			return;
		
		if(StringUtils.isEmpty(importDockind))
			return;
		
		Integer time = Integer.parseInt(Docusafe.getAdditionProperty("importDocument.time"));

		timer = new Timer(true);
		timer.schedule(new ImportDocument(), 1000, time * DateUtils.SECOND/* 30*DateUtils.SECOND */);
		console(Console.INFO, "DSI SUPPLY START wystartował");
	}

	@Override
	protected void stop() throws ServiceException {
		if (timer != null)
			timer.cancel();
	}

	@Override
	protected boolean canStop() {
		return false;
	}
	
	public class ImportDocument extends TimerTask {
		public ImportDocument() {
		}

		public void run() {
			try
			{
				importFromDir();
			}catch(ImportValidationException e){
				console(Console.ERROR, e.getMessage());
				log.error("",e);
			}
			catch(Exception e){
				console(Console.ERROR, e.getMessage());
				log.error("", e);
			}
		}
		
		/**
		 * Zasila tabele dsi_import_table z przekazanego katalogu
		 * @throws EdmException 
		 * @throws HibernateException 
		 * @throws SQLException 
		 */
		private void importFromDir() throws Exception {
			log.info("DO IMPORT SUPPLY");
			List<DSIBean> beans = new ArrayList<DSIBean>();
			
			DSApi.openAdmin();
			java.sql.Connection con = DSApi.context().session().connection();
			File importDir = new File(filePath);
			for(File file : importDir.listFiles())
			{
				DSIBean dsiBean = manager.findByFilePath(con, file.getName());
				
				if(dsiBean != null)
				{
					if(dsiBean.getImportStatus().equals(DSIManager.IMPORT_STATUS_DO_IMPORT))
					{
						console(Console.INFO, "Nie zaciagam plik jest jeszcze w imporcie " + file.getName());
						continue;
					}
					boolean ok = getHandler().supplyFileExist(file);
					log.info("Usuwam plik po zaciągnięciu: " + file.delete());
					continue;
				}
				
				DSIBean newDsiBean = supplyImport(file);
				
				if(newDsiBean != null)
				{
					beans.add(newDsiBean);
					console(Console.INFO, "Zapisuje plik do importu " + file.getName());
					log.info("Zapisuje plik do importu " + file.getName());
				}
				else
				{
					console(Console.INFO,"Nie ma dokumentu o barkodzie " + file.getName());
				}
			}
			
			if(!beans.isEmpty())
				saveDsiBeans(beans, con);
			
			if(con != null)
				con.close();
			
			DSApi._close();
			
			importDir= null;
		}
		
		/**
		 * Zapisuje wpisy do tabeli dsi_import_table
		 * @param beans
		 * @param con
		 */
		private void saveDsiBeans(List<DSIBean> beans, Connection con) throws EdmException
		{
			try
			{
				DSApi.context().begin();
				
				for(DSIBean dsi : beans){
					manager.saveDSIBean(dsi, con);
				}
				
				DSApi.context().commit();
			}catch(Exception e ){
				log.error("Blad: ", e);
				DSApi.context()._rollback();
				throw new EdmException(e.getMessage());
			}
		}

		/**
		 * Zwraca handler związany z dokumentem
		 * @return
		 * @throws Exception
		 */
		private DSIImportHandler getHandler() throws Exception
		{
			return DSIImportHandler.getHandler(importDockind);
		}
		
		
		/**
		 * Zasila tabele dsi_import_table dokumentem
		 * @param file
		 * @throws Exception 
		 */
		private DSIBean supplyImport(File file) throws Exception 
		{
			DSIBean dsiBean = new DSIBean();
			try
			{
				DSIImportHandler handler = getHandler();
				handler.supplyImport(file, dsiBean);
				
			}catch(ImportValidationException e){
				log.error(e.getMessage());
				return null;
			}
			
			return dsiBean;
		}
	}
}
