package pl.compan.docusafe.service.imports.dsi;


import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumerationField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.util.DateUtils;

/**
 * Klasa zawiera pomocnicze metody
 * @author wkutyla
 *
 */
public class ImportHelper 
{
	public final static String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	public final static String DOCKIND_DATE_FORMAT = "dd-MM-yyyy";
	
	/**
	 * Zaminia nazwe pola w mapie
	 * @param values
	 * @param oldName
	 * @param newName
	 */
	public static void renameField(Map<String,Object> values,String oldName,String newName)
	{
		Object obj;
		obj = values.get(oldName);
		if(obj != null)
		{
			values.remove(oldName);
			values.put(newName, obj);
		}
	}

	/**
	 * Usuwa wpis je�li ma odpowiedni� warto�� (value) - przydatne by oznaczy� jak�� warto�� jako null
	 * @param values
	 * @param key
	 * @param value
	 */
	public static void removeEntryIfEquals(Map<String, Object> values, String key, Object value) {
		if (values.containsKey(key) && values.get(key).equals(value)) {
			values.remove(key);
		}
	}
	
	/**
	 * Zamienia wartosci pol typu enum z wartosci cn na wartosci jakie zapisuje sie w bazie danych
	 * @param values
	 * @param documentKindCn
	 * @throws EdmException
	 */
	public static void changeCnToID(Map<String,Object> values,String documentKindCn) throws EdmException
	{
		DocumentKind documentKind = DocumentKind.findByCn(documentKindCn);
    	Map<String, Object> valuesTmp = new HashMap<String, Object>(values);
		Set<String> valuesSet = valuesTmp.keySet();
		
		for (String cn : valuesSet)
		{
			Field f = documentKind.getFieldByCn(cn);
			if(f != null)
			{
				if(f instanceof EnumerationField || f.getType().equals(Field.ENUM_REF) || f.getType().equals(Field.DATA_BASE))
				{
					values.put(cn, f.getEnumItemByCn(values.get(cn).toString()).getId());
				}
			}
		}
	}
	/**Usuwa wszystkie wpisy nie pasuj�ce do �adnego z pol dokumentu*/
	public static void removeInvalidFields(Map<String,Object> values,String documentKindCn)throws EdmException
	{
    	DocumentKind documentKind = DocumentKind.findByCn(documentKindCn);
    	Map<String, Object> valuesTmp = new HashMap<String, Object>(values);
		Set<String> valuesSet = valuesTmp.keySet();
		
		for (String cn : valuesSet)
		{
			Field f = documentKind.getFieldByCn(cn);
			if(f == null)
			{
				values.remove(cn);
				LogFactory.getLog(ImportHelper.class).error("ERR:0015 - Brak pola dla cn = "+cn);
			}
		}
	}
	
	/**
	 * Metoda sprawdza i koryguje format pola z data
	 * Jesli pole nie bedzie znalezione to brak reakcji
	 * @param values
	 * @param fieldName
	 * @throws ImportValidationException
	 */
	public static void correctDateFormat(Map<String,Object> values, String fieldName) throws ImportValidationException
	{
		String param = (String) values.get(fieldName);
		if (param!=null)
		{
			String date = correctDateFormat(param,fieldName);
			values.put(fieldName, date);
		}
	}
	
	/**
	 * Metoda sprawdza i koryguje warto��i bigDecimal
	 * @param values
	 * @param fieldName
	 * @throws ImportValidationException
	 */
	public static void correctBigDecimal(Map<String,Object> values, String fieldName) throws ImportValidationException
	{
		String param = (String) values.get(fieldName);
		if (param!=null)
		{
			BigDecimal val = new BigDecimal(param);
			values.put(fieldName, val);
		}
	}

	
	/**
	 * Metoda sprawdza w jakim formacie jest wprowadzona data i zapisuje go w formacie wlasciwym dla dockinda
	 * @param param
	 * @param fieldName
	 * @return
	 * @throws ImportValidationException
	 */
	public static String correctDateFormat(String param,String fieldName) throws ImportValidationException
	{
		SimpleDateFormat dockindFormat = new SimpleDateFormat(DOCKIND_DATE_FORMAT);
		SimpleDateFormat defaultFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		
		Date date = null;		
		date = parseDate(param,defaultFormat);
		if (date == null)
		{
			date = parseDate(param,dockindFormat);
		}
		if (date==null)
		{
			throw new ImportValidationException(DSIDefinitions.ERR_020_DATE_FORMAT_IS_BAD + fieldName);
		}
		return dockindFormat.format(date);
		
	}
	
	/**
	 * Wyrzuca z mapy element o ile jest on pustym Stringiem
	 * @param values
	 * @param fieldCn
	 */
	public static void removeEmptyValue(Map<String, Object> values, String fieldCn)
	{
		if (values.get(fieldCn)!=null)
		{
			String val = values.get(fieldCn).toString();
			if (val.trim().length()==0)
			{
				values.remove(fieldCn);
			}
		}
	}
	/**
	 * Metoda ustawa domyslna wartosc dla pola ktorego nie ma, jesli istnieje nic sie nie dzieje
	 * @param values
	 * @param fieldName
	 * @param defaultValue
	 */
	public static void setDefaultValue(Map<String,Object> values, String fieldName, Object defaultValue)
	{
		String param = (String) values.get(fieldName);
		if (param==null)
		{
			values.put(fieldName, defaultValue);
		}
	}
	
	/**
	 * Zwraca wartosc liczbowa pola jesli nie znajdzie to generowany jest Exception
	 * @param values
	 * @param fieldName
	 * @return
	 * @throws ImportValidationException
	 */
	public static double getDoubleValue(Map<String,Object> values,String fieldName) throws ImportValidationException
	{
		String param = (String) values.get(fieldName);
		return getDoubleValue(param,fieldName);
	}
	
	/**
	 * Zwraca wartosc liczbowa pola, jesli jest brak to wyrzucamy Exception
	 * @param param
	 * @param fieldName
	 * @return
	 * @throws ImportValidationException
	 */
	public static double getDoubleValue(String param,String fieldName) throws ImportValidationException 
	{
		if (param==null)
		{
			throw new ImportValidationException(DSIDefinitions.ERR_010_FIELD_MISSING + fieldName);
		}
		DecimalFormat decimalFormat = new DecimalFormat("###########0.00"); 
		DecimalFormatSymbols decimalSymbols = new DecimalFormatSymbols();
		decimalSymbols.setDecimalSeparator(".".toCharArray()[0]);
		decimalFormat.setDecimalFormatSymbols(decimalSymbols);
		Number number = null;
		try
		{	
			number = decimalFormat.parse(param);
		}
		catch (Exception e)
		{
			throw new ImportValidationException(DSIDefinitions.ERR_021_MONEY_FORMAT_IS_BAD+fieldName);
		}
		return number.doubleValue();
		//decimalFormat.setC
	}
	/**
	 * Metoda parsuje date, jest format jest zly to zwraca null
	 * @param param
	 * @param format
	 * @return
	 */
	private static Date parseDate(String param, SimpleDateFormat format)
	{
		format.setLenient(false);
		try
		{
			return format.parse(param);
		}
		catch (Exception e)
		{
			return null;
		}
	}

    /**
     * Usuwa spacje z wyznaczonego pola (kt�re musi by� stringiem lub kolekcj� string�w)
     * @param values
     * @param fieldCn
     */
    public static void removeSpaces(Map<String, Object> values, String fieldCn) {
        if(values.get(fieldCn) instanceof String){
            values.put(fieldCn,((String)values.get(fieldCn)).replace(" ",""));
        } else if(values.get(fieldCn) instanceof Collection
                && ((Collection) values.get(fieldCn)).iterator().next() instanceof String){
            ArrayList<String> list = new ArrayList<String>();
            for(String string: (Collection<String>) values.get(fieldCn)){
                list.add(string.replace(" ",""));
            }
            values.put(fieldCn, list);
        } else {
            throw new RuntimeException("Pole " + fieldCn + " nie zawiera ci�gu znak�w lub kolekcji ci�gu znak�w");
        }
    }

    /**
     * <p>Tworzy standardowy <code>DSIBean</code> z nast�puj�cymi w�a�ciwo�ciami:
     *
     * <pre>
     * bean.documentKind = importDockind
     * bean.userImportId = userImportId
     * bean.filePath     = filePath
     * </pre>
     *
     * Oraz atrybuty DSI (DSIAttribute):
     * <pre>
     * DOC_DATE        = data tera�niejsza
     * DOC_DESCRIPTION = description
     * </pre>
     *
     * </p>
     *
     * <p>
     *     <b>Uwaga</b>: bean nie jest dodawany do importu - zwracany jest tylko nowo utworzony obiekt <code>DSIBean</code>.
     * </p>
     * @param userImportId bean.userImportId
     * @param description DSIAttribute("DOC_DESCRIPTION", description)
     * @param importDockind bean.documentKind
     * @return DSIBean bean
     * @throws EdmException
     * @throws IOException
     * @throws SQLException
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     */
    public static DSIBean getSimpleDSIBean(String userImportId, String description, String importDockind) {
        DSIBean bean = new DSIBean();

        List<DSIAttribute> attrs = new LinkedList<DSIAttribute>();

        String dateString = DateUtils.sqlDateFormat.format(new Date());
        attrs.add(new DSIAttribute("DOC_DATE", dateString));
        attrs.add(new DSIAttribute("DOC_DESCRIPTION", description));

//        bean.setBoxCode(values.containsKey("BOX") ? values.get("BOX") : "noneed");
        bean.setBoxCode("noneed");
        bean.setDocumentKind(importDockind);
//        bean.setImportKind(values.containsKey("IMPORT_KIND") ? Integer.valueOf(values.get("IMPORT_KIND")) : DSIImportHandler.INCOMING_DOCUMENT_IMPORT);
        bean.setImportKind(DSIImportHandler.INCOMING_DOCUMENT_IMPORT);
        bean.setAttributes(attrs);
        bean.setImportStatus(1);
        bean.setSubmitDate(new Date());
        bean.setUserImportId(userImportId);
        bean.setErrCode("");

        return bean;
    }

    public static DSIBean getSimpleDSIBean(String userImportId, String description, String importDockind, String filePath) {
        final DSIBean bean = getSimpleDSIBean(userImportId, description, importDockind);
        bean.setFilePath(filePath);
        return bean;
    }

    public static DSIBean getSimpleDSIBean(String userImportId, String description, String importDockind, Collection<String> filePaths) {
        final DSIBean bean = getSimpleDSIBean(userImportId, description, importDockind);
        bean.setFilePath(DSIDefinitions.CODE_FILES_IN_ATTRIBUTES);

        Collection<DSIAttribute> attrs = bean.getAttributes();
        for(String filePath: filePaths) {
            attrs.add(new DSIAttribute("FILE", filePath));
        }

        bean.setAttributes(attrs);

        return bean;
    }


}
