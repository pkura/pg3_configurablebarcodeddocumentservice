package pl.compan.docusafe.service.imports.dsi;

public class DSIDefinitions 
{
	public final static String ERR_000_FATAL_ERROR = "ERR-000: Blad fatalny";
	public final static String ERR_001_MISSING_ATTACHMENT = "ERR-001: Brak zalacznika";
	public final static String ERR_002_MISSING_BOX = "ERR-002: Niewlasciwe pudlo";
	public final static String ERR_003_MISSING_ATTRIBUTES = "ERR-003: Brak atrybutow";
	public final static String ERR_010_FIELD_MISSING = "ERR-010: Brak pola ";
	public final static String ERR_020_DATE_FORMAT_IS_BAD = "ERR-020: Zly format daty w polu ";
	public final static String ERR_021_MONEY_FORMAT_IS_BAD = "ERR-021: Zly format kwoty w polu ";
	public final static String ERR_022_COLUMN_MISSING = "ERR-022: Brak kolumn ";
	public final static String ERR_023_MISSING_MULTI_ATTACHMENT = "ERR-023: Brak zalacznikow";

	public final static String CODE_NO_NEED_BOX = "noneed";
	public final static String CODE_NO_NEED_ATTACHMENT = "noneed";
	public final static String CODE_FILES_IN_ATTRIBUTES = "filesinattributes";
	
}
