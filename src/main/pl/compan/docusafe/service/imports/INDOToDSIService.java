package pl.compan.docusafe.service.imports;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVStrategy;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.imports.dsi.DSIAttribute;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIManager;
import pl.compan.docusafe.util.DateUtils;
import de.schlichtherle.io.ArchiveDetector;
import de.schlichtherle.io.ArchiveException;

public class INDOToDSIService extends ServiceDriver implements Service {

	private static final Log LOG = LogFactory.getLog(INDOToDSIService.class);
	
	private Timer timer = null;
	
	private static File TEMP = null;
	/**Folder z plikami zip do importu*/
	private static File IMPORT = null;
	/**Przeprcesowane pliki*/
	private static File IMPORTED_FILES = null;
	/**Za�czniki dla DSI*/
	private static File IMPORT_DOCUMENT_FILE_PATH = null;
	
	
	protected boolean canStop() 
	{
		return false;
	}
	
	public boolean hasConsole() 
	{
		return true;
	}

	protected void start() throws ServiceException 
	{
		try
		{
			TEMP = new File(Docusafe.getAdditionProperty("indo.temp"));
			IMPORT = new File(Docusafe.getAdditionProperty("indo.import"));
			IMPORTED_FILES = new File(Docusafe.getAdditionProperty("indo.imported.files"));
			IMPORT_DOCUMENT_FILE_PATH  = new File(Docusafe.getAdditionProperty("importDocument.filePath"));
			
			if(TEMP.exists() && IMPORT.exists())
			{
				timer = new Timer(true);
				timer.schedule(new Import(), 1 * DateUtils.MINUTE, 1 * DateUtils.MINUTE);
				
				console(Console.INFO, "Usluga uruchomiona");
			}
			else
			{
				throw new EdmException("Blad przy uruchamianiu uslugi");
			}
		}
		catch (Exception e) 
		{
			console(Console.ERROR, "Usluga nie zostala uruchomiona");
			LOG.error("Usluga nie zostanie uruchomiona",e);
		}
	}

	protected void stop() throws ServiceException 
	{
		if (timer != null) timer.cancel();
	}
	
	private class ZipFileFilter implements FilenameFilter
	{

		public boolean accept(File dir, String name) 
		{
			return name.endsWith(".zip");
		}

	}
	
	
	private class Import extends TimerTask
	{
		public synchronized void run() 
		{
			try
			{
				DSApi.openAdmin();
				List<String> importZip = new ArrayList<String>(Arrays.asList(IMPORT.list(new ZipFileFilter())));
				for(String baseFile : importZip)
				{
					console(Console.INFO, "Import z pliku "+baseFile);
					String attachment = "filesinattributes";
					try
					{
						DSApi.context().begin();
						unzip(IMPORT.getAbsolutePath()+"//"+baseFile,TEMP);
						for(File oneFolder : TEMP.listFiles())
						{
							console(Console.INFO, "Folder "+oneFolder.getAbsolutePath());
							for (File inFile : oneFolder.listFiles(new InFileFilter())) 
							{
								console(Console.INFO, "Plik "+inFile.getAbsolutePath());
								Map<String,String> values = new HashMap<String, String>();
								values.put("KLASA", oneFolder.getName());
								getValuesFromCsv(inFile, values);
								attachment = inFile.getName().substring(0,inFile.getName().length()-4 )+".tif";
								File attachmentFile = new File(inFile.getAbsolutePath().substring(0,inFile.getAbsolutePath().length()-4 )+".tif");
								FileUtils.moveFileToDirectory(attachmentFile, IMPORT_DOCUMENT_FILE_PATH,true);
								values.put("DOCKIND","eservice");
								DSIBean bean = new DSIBean();
								List<DSIAttribute> attrs = new LinkedList<DSIAttribute>();
								for(String field : values.keySet())
									attrs.add(new DSIAttribute(field, values.get(field)));
								bean.setBoxCode(values.containsKey("Lokalizacja") ? values.get("Lokalizacja") : "noneed");
								bean.setDocumentKind(values.get("DOCKIND"));
								bean.setFilePath(attachment);
								bean.setImportKind(values.containsKey("IMPORT_KIND") ? Integer.valueOf(values.get("IMPORT_KIND")) : 2);
								bean.setAttributes(attrs);
								bean.setImportStatus(1); //zmienic na 1 zeby uruchomic import
								bean.setSubmitDate(new Date());
								bean.setUserImportId(values.get("Id. dokumentu")); //powinna byc cala nazwa pliku ale jest za dluga
								bean.setErrCode("");
								DSIManager manager = new DSIManager();
								manager.saveDSIBean(bean, DSApi.context().session().connection());	
								
								console(Console.INFO, "Zaimportowano import id "+bean.getUserImportId());
							}
							FileUtils.deleteDirectory(oneFolder);
						}
						FileUtils.moveFileToDirectory(new File(IMPORT,baseFile), IMPORTED_FILES,true);
						for (File f : TEMP.listFiles()) 
						{
								FileUtils.deleteDirectory(f);
						}
						DSApi.context().commit();
					}
					catch (Exception e)
					{
						log.error(e.getMessage(),e);
						console(Console.ERROR, e.getMessage());
					}
				}
			}
			catch (Exception e) 
			{
				log.error(e.getMessage(),e);
				console(Console.ERROR, e.getMessage());
			}
			finally
			{
				DSApi._close();
			}
		}
	}
	
	public static void getValuesFromCsv(File inFile,Map<String,String> values) throws IOException
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inFile),Docusafe.getAdditionProperty(Configuration.ADDITION_IMPORT_ENCODING)));
		CSVStrategy strategy = new CSVStrategy('\t','"',CSVStrategy.COMMENTS_DISABLED,false,false,true);
		CSVParser csvParser = new CSVParser(reader,strategy);
		String[] keys = csvParser.getLine();
		String[] vals = null;
		while((vals = csvParser.getLine()) != null)
		{
			for (int i = 0; i < vals.length; i++)
			{
				values.put(keys[i], vals[i]);
			}
		}
		reader.close();
	}
	
	public static class InFileFilter implements FileFilter
	{

		public boolean accept(File pathname) 
		{
			return pathname.getName().endsWith(".txt");
		}
	}
	
	
	
	private static void unzip(String zipPath, File unzipFolder) throws ArchiveException 
	{
	    de.schlichtherle.io.File zipFile = new de.schlichtherle.io.File(zipPath);
	    boolean result = zipFile.archiveCopyAllTo(unzipFolder,ArchiveDetector.NULL);
	    zipFile.umount();
	}
}
