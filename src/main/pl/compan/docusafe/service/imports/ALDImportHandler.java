package pl.compan.docusafe.service.imports;

import java.awt.image.RenderedImage;
import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.AccessLog;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.imports.ImportManager;
import pl.compan.docusafe.core.imports.ImportedDocumentInfo;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.ImageUtils;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;

import com.asprise.util.tiff.TIFFReader;

public class ALDImportHandler extends ImportHandler
{
    //private static final int FIELDS_COUNT = 11;
	private static final int FIELDS_COUNT = 12;

    private static final Log log = LogFactory.getLog(ALDImportHandler.class);

    public ALDImportHandler()
    {
    }

    @Override
    public void createDocument(ImportedDocumentInfo importedDocumentInfo, String line) throws EdmException
    {

        DocumentKind dockind = DocumentKind.findByCn("ald");

        // sprawdzam poprawnosc pliku tiff
        LoggerFactory.getLogger("tomekl").debug("path {}", importedDocumentInfo.getImportedFileInfo().getTiffDirectory() + File.separator + importedDocumentInfo.getTiffName());
        
        File tiffFile = new File(importedDocumentInfo.getImportedFileInfo().getTiffDirectory() + File.separator
           + importedDocumentInfo.getTiffName());
        if (tiffFile == null || !tiffFile.exists())
        {
            throw new EdmException("Podano niepoprawny plik tiff");
        }

        String[] fields = line.split("\\s*;\\s*", FIELDS_COUNT);

        int i;
        for (i = 0; i < fields.length; ++i)
        {
            int f = fields[i].indexOf('"');
            int l = fields[i].lastIndexOf('"');
            if (f >= 0 && f != l)
                fields[i] = fields[i].substring(f + 1, l);
        }
        Document newDocument = null;
     /*   if("Opony - potwierdzenia".equalsIgnoreCase(fields[8]))
        {
        	newDocument = createDocument(fields,dockind,tiffFile);
        }
        else*/
        {
        	boolean startProcess = !"Opony - potwierdzenia".equalsIgnoreCase(fields[8]);
        	newDocument = createOfficeDocument(fields,dockind,tiffFile,startProcess);
        }


        Box box;
        try
        {
            box = Box.findByName(dockind.getProperties().get(DocumentKind.BOX_LINE_KEY),importedDocumentInfo.getBoxNumber());
        }
        catch (EntityNotFoundException e)
        {
            // takie pudlo nie istnieje - tworze je
            box = new Box(importedDocumentInfo.getBoxNumber().toUpperCase());
            box.setOpen(true);
            box.setLine(dockind.getProperties().get(DocumentKind.BOX_LINE_KEY));
            box.setOpenTime(new Date());
            box.create();
            box.setCloseTime(new Date());
        }
        newDocument.setBox(box);

        // zapamietujemy id dokumentu, ktory utworzylismy
        importedDocumentInfo.setDocumentId(newDocument.getId());

    }

    private Document createOfficeDocument(String[] fields,DocumentKind dockind,File tiffFile, boolean startProcess) throws EdmException
    {
    	InOfficeDocument newDocument = new InOfficeDocument();
        Sender sender = new Sender();
        sender.setOrganization(fields[9]);
        //sender.setAnonymous(true);
        newDocument.setSender(sender);

        Map address = GlobalPreferences.getAddress();
        Recipient recipient = new Recipient();
        recipient.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
        recipient.setStreet((String) address.get(GlobalPreferences.STREET));
        recipient.setZip((String) address.get(GlobalPreferences.ZIP));
        recipient.setLocation((String) address.get(GlobalPreferences.LOCATION));
        recipient.setCountry("PL");
        newDocument.addRecipient(recipient);

        newDocument.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        newDocument.setDivisionGuid(DSDivision.ROOT_GUID);
        newDocument.setCurrentAssignmentAccepted(Boolean.FALSE);

        // data, z jak� zostanie utworzony wpis w dzienniku
        Date entryDate;
        Long journalId;

        try
        {
            if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
                throw new EdmException("Brak uprawnienia do przyjmowania pism w KO");

            Date docDate = DateUtils.sqlDateFormat.parse(fields[6]);

            newDocument.setDocumentDate(docDate);
            Map<String, Object> values = new HashMap<String, Object>();
            values.put("NR_FAKTURY", fields[0]);
            values.put("NIP_DOSTAWCY", fields[1]);
            values.put("NR_REJESTRACYJNY", fields[2]);
            values.put("NR_VIN", fields[3]);
            values.put("NAZWA_KLIENTA", fields[4]);
            values.put("NR_KONTRAKTU", fields[5]);
            values.put("NAZWA_DOSTAWCY", fields[9]);
            values.put("DATA", docDate);
            EnumItem cat = dockind.getFieldByCn("KATEGORIA").getEnumItemByTitle(fields[8]);
            values.put("KATEGORIA", cat.getId());
            EnumItem stat = dockind.getFieldByCn("STATUS").getEnumItemByCn("NOWY");
            values.put("STATUS", stat.getId());
            
            LoggerFactory.getLogger("tomekl").debug("");
            if(fields[10] != null && fields[10].length() > 0)
            {
            	Date fields10 = DateUtils.parseDateAnyFormat(fields[10]);
            	values.put("TERMIN_PLATNOSCI", DateUtils.formatJsDate(fields10));
            }
            
            dockind.logic().validate(values, dockind, null);
            newDocument.setSummary(dockind.getName());

            //nieznany wi�c nie jest ustawiany
            //InOfficeDocumentKind kind = InOfficeDocumentKind.find(kindId);
            //newDocument.setKind(kind);

            Calendar currentDay = Calendar.getInstance();
            currentDay.setTime(GlobalPreferences.getCurrentDay());

            entryDate = new Date(currentDay.getTime().getTime());

            Date breakOfDay = DateUtils.endOfDay(currentDay.getTime());

            // je�eli faktyczna data jest p�niejsza ni� otwarty
            // dzie�, pozostawiam dat� i ustawiam godzin� 23:59:59.999
            if (new Date().after(breakOfDay))
            {
                currentDay.setTime(breakOfDay);
            }
            else
            {
                Calendar now = Calendar.getInstance();
                currentDay.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
                currentDay.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
                currentDay.set(Calendar.SECOND, now.get(Calendar.SECOND));
            }

            Journal journal = Journal.getMainIncoming();

            journalId = journal.getId();

            newDocument.setCreatingUser(DSApi.context().getPrincipalName());
            newDocument.setIncomingDate(currentDay.getTime());

            newDocument.setAssignedDivision(DSDivision.ROOT_GUID);
            // referent nie jest na razie przydzielany
            //doc.setClerk(DSApi.context().getDSUser().getName());

            //Calendar cal = Calendar.getInstance();
            //cal.add(Calendar.DATE, DateUtils.getBusinessDays(kind.getDays()));

            newDocument.setStatus(InOfficeDocumentStatus.findByCn("PRZYJETY"));
            newDocument.setFolder(Folder.getRootFolder());

            newDocument.create();

            newDocument.setDocumentKind(dockind);

            Long newDocumentId = newDocument.getId();

            //dockind.set(newDocumentId, values);
            dockind.setOnly(newDocumentId, values);

            dockind.logic().archiveActions(newDocument, DocumentKindsManager.getType(newDocument));
            dockind.logic().documentPermissions(newDocument);

            Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
            newDocument.createAttachment(attachment);
            AttachmentRevision ar = attachment.createRevision(tiffFile);
            ar.setOriginalFilename(fields[11]);
            Long revisionId = ar.getId();
            File imageFile;

            /*if(Attachment.IMAGES_DECOMPRESSED_IN_LOCAL_FILE_SYSTEM)
            {
                TIFFReader reader = new TIFFReader(tiffFile);
                int pages = reader.countPages();

                for(int iter=0;iter<pages;iter++)
                {
                    RenderedImage im = reader.getPage(iter);
                    String path = AttachmentRevision.getPathToDecompressedImage(revisionId, AttachmentRevision.PNG, false, iter);


                    imageFile = new File(path);
                    ImageUtils.save(ImageUtils.getBufferedImage(im), AttachmentRevision.PNG, imageFile);
                }

            }*/

            //w DocumentMainAction nie ma akcji dla dokumentu ALD, wi�c nie wykonuj�
            //specificAdditions(newDocument);
        }
        catch (ValidationException e)
        {
            log.error(e.getMessage(), e);
            throw new EdmException("B��d walidacji danych: " + e.getMessage());
        }
        catch (EdmException e)
        {
            log.error(e.getMessage(), e);
            throw e;
        }
        catch (Throwable e)
        {
            log.error(e.getMessage(), e);
            throw new EdmException("B��d podczas importu dokumentu: " + e.getMessage());
        }

        // tworzenie wpisu w dzienniku

        Integer sequenceId;
        try
        {
            sequenceId = Journal.TX_newEntry2(journalId, newDocument.getId(), entryDate);
        }
        catch (EdmException e)
        {
            log.error(e.getMessage(), e);
            throw e;
        }

        try
        {
            // powi�zanie dokumentu z bie��c� sesj�
            newDocument = InOfficeDocument.findInOfficeDocument(newDocument.getId());
            newDocument.bindToJournal(journalId, sequenceId);

            if(startProcess)
            	WorkflowFactory.createNewProcess(newDocument, true);


            AccessLog.logPersonalDataInput(DSApi.context().getPrincipalName(),
                newDocument.getId(),
                Arrays.asList(new Sender[] { newDocument.getSender() }));

        }
        catch (Exception e)
        {
        	e.printStackTrace();
        	LogFactory.getLog("eprint").error("", e);
            log.error("B��d podczas tworzenia zadania dla importowanego dokumentu: "+e.getMessage()+ " "+e.getCause()+" "+e.getClass()+" "+e.getMessage());
            throw new EdmException("B��d podczas tworzenia zadania dla importowanego dokumentu: "+e.getMessage()+ " "+e.getCause()+" "+e.getClass()+" "+e.getMessage());
        }

        return newDocument;
    }
  /*
    private Document createDocument_(String[] fields,DocumentKind dockind,File tiffFile) throws EdmException
    {
    	Document newDocument = new Document(dockind.getName(),dockind.getName());
        Sender sender = new Sender();
        sender.setOrganization(fields[9]);
        //sender.setAnonymous(true);
       // newDocument.setSender(sender);

        Map address = GlobalPreferences.getAddress();
        Recipient recipient = new Recipient();
        recipient.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
        recipient.setStreet((String) address.get(GlobalPreferences.STREET));
        recipient.setZip((String) address.get(GlobalPreferences.ZIP));
        recipient.setLocation((String) address.get(GlobalPreferences.LOCATION));
        recipient.setCountry("PL");
      //  newDocument.getRecipients().add(recipient);

    //    newDocument.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
    //    newDocument.setDivisionGuid(DSDivision.ROOT_GUID);
    //    newDocument.setCurrentAssignmentAccepted(Boolean.FALSE);

        // data, z jak� zostanie utworzony wpis w dzienniku

        try
        {
            if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
                throw new EdmException("Brak uprawnienia do przyjmowania pism w KO");

            Date docDate = DateUtils.sqlDateFormat.parse(fields[6]);

     //       newDocument.setDocumentDate(docDate);
            Map<String, Object> values = new HashMap<String, Object>();
            values.put("NR_FAKTURY", fields[0]);
            values.put("NIP_DOSTAWCY", fields[1]);
            values.put("NR_REJESTRACYJNY", fields[2]);
            values.put("NR_VIN", fields[3]);
            values.put("NAZWA_KLIENTA", fields[4]);
            values.put("NR_KONTRAKTU", fields[5]);
            values.put("NAZWA_DOSTAWCY", fields[9]);
            values.put("DATA", docDate);
            EnumItem cat = dockind.getFieldByCn("KATEGORIA").getEnumItemByTitle(fields[8]);
            values.put("KATEGORIA", cat.getId());
            EnumItem stat = dockind.getFieldByCn("STATUS").getEnumItemByCn("NOWY");
            values.put("STATUS", stat.getId());

            dockind.logic().validate(values, dockind);
    //        newDocument.setSummary(dockind.getName());

            //nieznany wi�c nie jest ustawiany
            //InOfficeDocumentKind kind = InOfficeDocumentKind.find(kindId);
            //newDocument.setKind(kind);

            Calendar currentDay = Calendar.getInstance();
            currentDay.setTime(GlobalPreferences.getCurrentDay());

         //   entryDate = new Date(currentDay.getTime().getTime());

            Date breakOfDay = DateUtils.endOfDay(currentDay.getTime());

            // je�eli faktyczna data jest p�niejsza ni� otwarty
            // dzie�, pozostawiam dat� i ustawiam godzin� 23:59:59.999
            if (new Date().after(breakOfDay))
            {
                currentDay.setTime(breakOfDay);
            }
            else
            {
                Calendar now = Calendar.getInstance();
                currentDay.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
                currentDay.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
                currentDay.set(Calendar.SECOND, now.get(Calendar.SECOND));
            }

    //        newDocument.setCreatingUser(DSApi.context().getPrincipalName());
    //        newDocument.setIncomingDate(currentDay.getTime());

    //        newDocument.setAssignedDivision(DSDivision.ROOT_GUID);
            // referent nie jest na razie przydzielany
            //doc.setClerk(DSApi.context().getDSUser().getName());

            //Calendar cal = Calendar.getInstance();
            //cal.add(Calendar.DATE, DateUtils.getBusinessDays(kind.getDays()));

      //      newDocument.setStatus(InOfficeDocumentStatus.findByCn("PRZYJETY"));
            newDocument.setFolder(Folder.getRootFolder());

            newDocument.create();

            newDocument.setDocumentKind(dockind);

            Long newDocumentId = newDocument.getId();

            dockind.set(newDocumentId, values);

            dockind.logic().archiveActions(newDocument, DocumentKindsManager.getType(newDocument));
            dockind.logic().documentPermissions(newDocument);

            Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
            newDocument.createAttachment(attachment);
            AttachmentRevision ar = attachment.createRevision(tiffFile);
            ar.setOriginalFilename(fields[10]);
            Long revisionId = ar.getId();
            File imageFile;

            if(Attachment.IMAGES_DECOMPRESSED_IN_LOCAL_FILE_SYSTEM)
            {
                TIFFReader reader = new TIFFReader(tiffFile);
                int pages = reader.countPages();

                for(int iter=0;iter<pages;iter++)
                {
                    RenderedImage im = reader.getPage(iter);
                    String path = ContentAction.getPathToDecompressedImage(revisionId, ContentAction.PNG, false, iter);


                    imageFile = new File(path);
                    ImageUtils.save(ImageUtils.getBufferedImage(im), ContentAction.PNG, imageFile);
                }

            }

            //w DocumentMainAction nie ma akcji dla dokumentu ALD, wi�c nie wykonuj�
            //specificAdditions(newDocument);
        }
        catch (ValidationException e)
        {
            log.error(e.getMessage(), e);
            throw new EdmException("B��d walidacji danych: " + e.getMessage());
        }
        catch (EdmException e)
        {
            log.error(e.getMessage(), e);
            throw e;
        }
        catch (Throwable e)
        {
            log.error(e.getMessage(), e);
            throw new EdmException("B��d podczas importu dokumentu: " + e.getMessage());
        }
        return newDocument;
    }*/

    @Override
    public ImportedDocumentInfo createDocumentInfo(String line, int lineCount) throws EdmException
    {
        String[] fields = line.split("\\s*;\\s*", FIELDS_COUNT);
        int i;
        for (i = 0; i < fields.length; ++i)
        {
            int f = fields[i].indexOf('"');
            int l = fields[i].lastIndexOf('"');
            if (f >= 0 && f != l)
                fields[i] = fields[i].substring(f + 1, l);
        }
        ImportedDocumentInfo importedDocumentInfo = new ImportedDocumentInfo();
        importedDocumentInfo.setStatus(ImportManager.STATUS_WAITING);
        importedDocumentInfo.setLineNumber(lineCount);

        if (fields.length == FIELDS_COUNT)
        {
            importedDocumentInfo.setBoxNumber(fields[7]);
            Date date = ImportManager.parseDateValidated(fields[6]);
            if (date != null)
                importedDocumentInfo.setDate(date);
            /*
            else
            {
                importedDocumentInfo.setStatus(ImportManager.STATUS_ERROR);
                importedDocumentInfo.setResult(ImportManager.RESULT_ERROR);
                importedDocumentInfo.setErrorInfo("Podana data jest niepoprawna: " + fields[6]);
            }
            */
            importedDocumentInfo.setTiffName(fields[11]);
        }
        else
        {
            importedDocumentInfo.setProcessedTime(new Date());
            importedDocumentInfo.setStatus(ImportManager.STATUS_DONE);
            importedDocumentInfo.setResult(ImportManager.RESULT_ERROR);
            importedDocumentInfo.setErrorInfo("Niepoprawnie opisany dokument - oczekiwano dok�adnie " + FIELDS_COUNT + " warto�ci (by�o " + fields.length + ")");
        }

        return importedDocumentInfo;
    }

}
