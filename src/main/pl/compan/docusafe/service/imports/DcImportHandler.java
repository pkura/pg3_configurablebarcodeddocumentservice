/*
 * DcImportHandler.java
 * 
 */

package pl.compan.docusafe.service.imports;


import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.imports.ImportManager;
import pl.compan.docusafe.core.imports.ImportedDocumentInfo;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.DcLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.util.DateUtils;

/**
 * @author Piotr Komisarski
 */
public class DcImportHandler extends ImportHandler{

    public ImportedDocumentInfo createDocumentInfo(String line, int lineCount) throws EdmException {

        ImportedDocumentInfo info = new ImportedDocumentInfo();

        info.setLineNumber(lineCount);

        String[] fields = line.split("\\|");

        String tiffName = fields[fields.length-1].replace("\"", "");
        info.setTiffName(tiffName);
        
        
        String boxNumber = fields[0].replace("\"", "");
        if(boxNumber==null)// || boxNumber.length()!= 8)
            throw new EdmException("Nieprawidłowy box number !");
        
        info.setBoxNumber(boxNumber);
        info.setStatus(ImportManager.STATUS_WAITING);
        return info;
    }

    public void createDocument(ImportedDocumentInfo info, String line) throws EdmException {
        

        DocumentKind dockind = DocumentKind.findByCn("dc");
        
        File tiffFile = new File(info.getImportedFileInfo().getTiffDirectory() + "\\" + info.getTiffName());
        if (tiffFile == null || !tiffFile.exists())
        {

            throw new EdmException("Podano niepoprawny plik tiff");
        }
        
        
        String[] fields = line.split("\\|");        
        for(int i=0; i<fields.length; i++)
            fields[i] = fields[i].replace("\"", "");

        Document document = new  Document("title", "summary");//i tak beda nadpisane w archiveaction 

        Folder folder = Folder.getRootFolder();
        document.setFolder(folder);
        document.create();
        document.setDocumentKind(dockind);

        DocumentKind dk = DocumentKind.findByCn("dc");
        Map<String, Object> values = new HashMap<String, Object>();


        values.put("STATUS", dockind.getFieldByCn(DcLogic.STATUS_FIELD_CN).getEnumItemByCn(DcLogic.STATUS_ARCHIWALNY_CN).getId());

        // korekta pewnego bledu w specyfikacji importu
        boolean zmianaDanych = false;
        EnumItem cat = null;
        if(fields[3].equals("CRS"))
            cat = dockind.getFieldByCn(DcLogic.KATEGORIA_FIELD_CN).getEnumItemByCn(DcLogic.KATEGORIA_REKLAMACJE_CN);
        else if(fields[3].equals("W"))
            cat = dockind.getFieldByCn(DcLogic.KATEGORIA_FIELD_CN).getEnumItemByCn(DcLogic.KATEGORIA_WYPLATY_CN);
        else if(fields[3].equals("O"))
            cat = dockind.getFieldByCn(DcLogic.KATEGORIA_FIELD_CN).getEnumItemByCn(DcLogic.KATEGORIA_OSWIADCZENIA_CN);
        else if(fields[3].equals("R"))
            cat = dockind.getFieldByCn(DcLogic.KATEGORIA_FIELD_CN).getEnumItemByCn(DcLogic.KATEGORIA_ROZWODY_CN);
        else if(fields[3].equals("P"))
        {
            cat = dockind.getFieldByCn(DcLogic.KATEGORIA_FIELD_CN).getEnumItemByCn("ZMIANA_DANYCH");            
            zmianaDanych = true;
        }
        else
            throw new EdmException("nieznany rodzaj dokumentu (inny niz CSR, O, W, R, P).");

        values.put(DcLogic.KATEGORIA_FIELD_CN, cat.getId());

        
        if(!zmianaDanych && fields[4].length()!= 9 &&  fields[4].length()!= 0)
                throw new EdmException("Nr sprawy nie jest 9-cyfrowy");
		
        if(fields[5].length() == 0)
                throw new EdmException("Nr rachunku jest obowiazkowy");

        if(fields[5].length()!= 10)
                throw new EdmException("Nr rachunku nie jest 10-cyfrowy");
		

        if (!zmianaDanych)
        {
        	values.put(DcLogic.NUMER_SPRAWY_FIELD_CN, fields[4].length() > 0 ? fields[4] : null);
        }
        values.put(DcLogic.NR_RACHUNKU_FIELD_CN, fields[5]);

        values.put(DcLogic.IMIE_FIELD_CN, fields[6]);
        values.put(DcLogic.NAZWISKO_FIELD_CN, fields[7]);

        values.put(DcLogic.PESEL_FIELD_CN, fields[8]);

        Date data=null, data_uposazonego=null;
        try
        {
            if(fields[9].length()>0)
                data = DateUtils.parseDateAnyFormat(fields[9]);
        }
        catch(ParseException pe)
        {
            throw new EdmException("nieprawidlowy format daty na polu nr. 9");
        }
        if (zmianaDanych)
        {
        	DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        	try
        	{
        		data = dateFormat.parse(fields[4].substring(0,8));
        	}
        	catch (ParseException pe)
        	{
        		throw new EdmException("nieprawidlowy format daty na polu nr. 4");
        	}
        }
        Map<String, Object> hiddenValues = new HashMap<String, Object>();
        hiddenValues.put(DcLogic.NUMER_PLIKU_FIELD_CN, fields[12]);
                
        try
        {
            if(fields[26].length()>0)    
                data_uposazonego = DateUtils.parseDateAnyFormat(fields[26]);
        }
        catch(ParseException pe)
        {
            throw new EdmException("nieprawidlowy format daty na polu nr.26");
        }
       
        if(data!=null)
            values.put(DcLogic.DATA_FIELD_CN, data);
        values.put(DcLogic.SYGNATURA_FIELD_CN, fields[10]);
        
        
        
        cat = dockind.getFieldByCn("TYP").getEnumItemByTitle(fields[11]);
        values.put(DcLogic.TYP_FIELD_CN, cat.getId());
        
        values.put(DcLogic.IMIE_UPOSAZONEGO_FIELD_CN, fields[13]);
        values.put(DcLogic.NAZWISKO_UPOSAZONEGO_FIELD_CN, fields[19]);
        if(data_uposazonego!=null)
            values.put(DcLogic.DATA_UPOSAZONEGO_FIELD_CN, data_uposazonego);
        
        dk.set(document.getId(), values);
        dk.setOnly(document.getId(), hiddenValues);
        dockind.logic().validate(values, dockind, null);
        dk.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
        dk.logic().documentPermissions(document);
        
        Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
        document.createAttachment(attachment);
        AttachmentRevision ar = attachment.createRevision(tiffFile);
        ar.setOriginalFilename(info.getTiffName());
        
        
        
        Box box;
        try
        {
            box = Box.findByName(dockind.getProperties().get(DocumentKind.BOX_LINE_KEY),info.getBoxNumber());
        }
        catch (EntityNotFoundException e)
        {
            // takie pudlo nie istnieje - tworze je
            box = new Box(info.getBoxNumber().toUpperCase());
            box.setOpen(true);
            box.setLine(dockind.getProperties().get(DocumentKind.BOX_LINE_KEY));
            box.setOpenTime(new Date());
            box.create();
            box.setCloseTime(new Date());
        }
        document.setBox(box);
        
        // zapamietujemy id dokumentu, ktory utworzylismy
        info.setDocumentId(document.getId());
        
    }
}
