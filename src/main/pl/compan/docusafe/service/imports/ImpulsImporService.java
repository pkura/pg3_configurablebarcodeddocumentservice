package pl.compan.docusafe.service.imports;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;

public class ImpulsImporService extends ServiceDriver implements Service
{
	private static final Log log = LogFactory.getLog("agent_log");
	private Timer timer = null; 
	private Long lastContract;
	private Long lastApplication;

	public void initService()
	{
		try
		{
			ImpulsImportUtils.initImport();
		}
		catch (Exception e)
		{
			log.error("Blad w czasie wczytywania pliku ilpol", e);
		}
	}
	
    protected void start() throws ServiceException
    {
    	initService();
    	if(ImpulsImportUtils.isServiceOn())
        {
    		timer = new Timer(true);
    		log.debug("Uruchamiam import ilpol co "+ImpulsImportUtils.getTime()+" minut");
            timer.schedule(new Import(), 10000, ImpulsImportUtils.getTime() * DateUtils.MINUTE);
        }
        
    }

    protected void stop() throws ServiceException
    {
        if (timer != null) timer.cancel();
    }

    protected boolean canStop()
    {
        return false;
    }

    class Import extends TimerTask
	{
		public void run()
		{
			Connection conn = null;
			try
			{
				DSApi.openAdmin();
				Calendar cal = Calendar.getInstance();
				lastContract = ImpulsImportUtils.getLastContract();
				lastApplication = ImpulsImportUtils.getLastApp();
				log.debug("Jest godzina "+cal.get(Calendar.HOUR_OF_DAY));
				if (cal.get(Calendar.HOUR_OF_DAY) == 19 || ImpulsImportUtils.isOnStratServer())
				{
					ImpulsImportUtils.setOnStratServer(false);
					conn = ImpulsImportUtils.connectToLEO();
					//importContracts(conn);
					importApplication(conn);
				}
			}
			catch (Exception e)
			{
				log.error("B��d podczas pracy agenta ilpol ", e);
			}
			finally
			{
				if (conn != null)
				{
					ImpulsImportUtils.disconnectOracle(conn);
				}
				try
				{
					DSApi.close();
				}
				catch (EdmException e)
				{
					log.error("B��d podczas roz��czania po��czenia do bazy ILPL ", e);
				}
			}
		}
	}
    
    private synchronized void importContracts(Connection conn ) throws SQLException, EdmException
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	Integer countUmowy = 0;
    	Integer countKontrahenci = 0;
    	
    	try
    	{
    		log.error("SZUKA UMOW OD IDUMOWY "+ lastContract);
    		ps = conn.prepareStatement("select count(NRUMOWY) from "+ImpulsImportUtils.vumowy+" where IDUMOWY >= ? ");
    		ps.setLong(1, lastContract);
    		rs = ps.executeQuery();
    		if(rs.next())
    		{
    			log.error("Znaleziono "+ rs.getInt(1)+" um�w");
    		}
    		if(rs != null)
    			rs.close();
    		if(ps != null)
    			ps.close();
    		
	    	ps =conn.prepareStatement("select * from "+ImpulsImportUtils.vumowy+" where IDUMOWY >= ? order by IDUMOWY ");
	        ps.setLong(1, lastContract);
	    	rs = ps.executeQuery();	       
	    	log.error("Wykonal select po umowach");	
	    	int i = 0;
	    	while(rs.next())
	    	{
	    		String nrUmowy = null;
	    		String idKlienta = null;
	    		Long idUmowy = null;
	    		Contractor contractor = null;
	    		Contractor dostawca = null;
	    		String idDostawcy = null;
	    		try
		    	{
	    			nrUmowy = null;
		    		nrUmowy = rs.getString("NRUMOWY");
		    		idKlienta = rs.getString("IDKLIENTA");		    		
		    		idDostawcy = rs.getString("IDDOSTAWCY");
		    		idUmowy = rs.getLong("IDUMOWY");
		    		lastContract = rs.getLong("IDUMOWY");
		    		log.trace("IDUMOWY = "+lastContract);
		    		
		    		//Najpierw zajumje sie kontrahentem bo jezeli jest umowa to i tak aktualizuje kontrahenta
		    		contractor = ImpulsImportUtils.getContractor(idKlienta, countKontrahenci, conn,ImpulsImportUtils.FROM_VKONTRAHENCI);
		    		//dodanie dostawcy(nic dalej z nim nie robimy bo nie zaplacili)
		    		dostawca = ImpulsImportUtils.getContractor(idDostawcy, countKontrahenci, conn,ImpulsImportUtils.FROM_VKONTRAHENCI);
		    		if(contractor == null)
	        		{
	        			log.debug("Brak kontrahenta idLEO "+idKlienta);
	        			continue;
	        		}	        		
		    		DlApplicationDictionary application = ImpulsImportUtils.getApplicationByIdUmowy(conn,idUmowy);		    		
		    		log.trace("SZUKA umowy w ds nr "+ nrUmowy+" Klient nrLEO  "+idKlienta );		    		
		    		List<DlContractDictionary> umowy = DlContractDictionary.findByNumerUmowy(nrUmowy);
		    		DlContractDictionary dlc = null;
		    		if(umowy == null || umowy.size() < 1)
		    		{
		    			countUmowy ++;
		    			log.error("DODAJE umowe do DS "+ nrUmowy + " kontrahent idDS = "+contractor.getId());
		    			if(!DSApi.context().isTransactionOpen())
		    				DSApi.context().begin();
		    			dlc = new DlContractDictionary();
		    			dlc.setDataKoncaUmowy(rs.getDate("DATAZAKONCZENIA"));
		    			dlc.setDataUmowy(rs.getDate("DATAPODPISANIA"));
		    			if(contractor != null)
		    				dlc.setIdKlienta(contractor.getId());
		    			if(dostawca != null)
		    				dlc.setIdDostawcy(dostawca.getId());
		    			dlc.setNumerUmowy(nrUmowy);
		    			dlc.setOpisUmowy(rs.getString("NAZWAPRZEDMIOTU"));
		    			dlc.setStatus(rs.getString("STATUS"));
		    			if(application != null)
		    				dlc.setApplicationId(application.getId());
		    			dlc.setLeasingObjectType(ImpulsImportUtils.getType(rs.getString("TYPPRZEDMIOTU"), rs.getLong("IDTYPPRZEDMIOTU")));
		    			dlc.create();
		    			DSApi.context().commit();
		    		}
		    		else
		    		{
		    			log.trace("UPDATE umowe do DS "+ nrUmowy + " kontrahent idDS = "+contractor.getId());
		    			if(!DSApi.context().isTransactionOpen())
		    				DSApi.context().begin();
		    			dlc = umowy.get(0);
		    			dlc.setDataKoncaUmowy(rs.getDate("DATAZAKONCZENIA"));
		    			dlc.setDataUmowy(rs.getDate("DATAPODPISANIA"));
		    			if(contractor != null)
		    				dlc.setIdKlienta(contractor.getId());
		    			if(dostawca != null)
		    				dlc.setIdDostawcy(dostawca.getId());
		    			dlc.setNumerUmowy(nrUmowy);
		    			dlc.setOpisUmowy(rs.getString("NAZWAPRZEDMIOTU"));
		    			dlc.setStatus(rs.getString("STATUS"));
		    			if(application != null)
		    				dlc.setApplicationId(application.getId());
		    			dlc.setLeasingObjectType(ImpulsImportUtils.getType(rs.getString("TYPPRZEDMIOTU"), rs.getLong("IDTYPPRZEDMIOTU")));
		    			DSApi.context().commit();
		    		}
		    		if(dlc != null && application != null)
		    		{
		    			if(!DSApi.context().isTransactionOpen())
		    				DSApi.context().begin();		    			
		    			application.setContractId(dlc.getId());
		    			DSApi.context().commit();
		    		}
		    	}
	    		catch (Exception e) 
	    		{
	        		log.error("B��d dodania umowy "+nrUmowy,e);
				}
	    		i++;
	    		log.trace("_______NEXT__________");
				if(i%200 == 0)
				{
					log.error("Czyszczenie sesi po "+i+" dokumentach");
					DSApi.context().session().flush();
					DSApi.context().session().clear();
				}
	    	}
    	}
    	catch (Exception e) 
		{
    		log.error("B��d podczas pracy agenta ilpol ",e);
		}		
		finally
		{
			if(ps != null){try {ps.close();}catch (SQLException e) {}}
			if(rs != null){try {rs.close();}catch (SQLException e) {}}
			log.debug("Sko�czono update um�w dodano "+countUmowy+"nowych um�w oraz "+countKontrahenci+"kontrahent�w");
		}
    }
    
    private synchronized void importApplication(Connection conn ) throws SQLException, EdmException
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	Integer countA = 0; 
    	
    	try
    	{
    		ps =conn.prepareStatement("select * from "+ImpulsImportUtils.vdfewnioski+" where IDWNIOSKU >= ? and STATUS not like '100%' order by IDWNIOSKU");
    		ps.setLong(1, lastApplication);
        	rs = ps.executeQuery();	       
        	log.debug("Wykonal select po wnioskach od idwniosku "+ lastApplication);
        	int i=0;
        	while (rs.next())
        	{
        		String numerWniosku = null; 
        		Long idUmowy = null;
        		try
        		{
        			numerWniosku = null; 
	        		String idKlienta = null;
	        		String idDostawcy = null;
	        		String nip = null;
	        		Contractor contractor = null;
	        		Contractor dostawca = null;
	        		
	        		numerWniosku = rs.getString("NRWNIOSKU");
	        		lastApplication = rs.getLong("IDWNIOSKU");
	        		log.trace("IDWNIOSKU "+ lastApplication);
	        		idKlienta = rs.getString("KLIENT");
	        		idDostawcy = rs.getString("DOSTAWCA");
	        		idUmowy = rs.getLong("IDUMOWY");
	        		
	        		contractor = ImpulsImportUtils.getContractor(idKlienta, countA, conn, ImpulsImportUtils.FROM_VDFKONTRAHENCI);
	        		//dodanie dostawcy(nic dalej z nim nie robimy bo nie zaplacili)
	        		dostawca = ImpulsImportUtils.getContractor(idDostawcy, countA, conn, ImpulsImportUtils.FROM_VDFKONTRAHENCI);
	        		if(contractor == null)
	        		{
	        			log.debug("Brak kontrahenta LEOID = "+idKlienta );
	        			continue;
	        		}
	        		DlContractDictionary contract = null;
	        		if(idUmowy > 0)
	        			contract = ImpulsImportUtils.getContractByIdUmowy(conn,idUmowy,null);		  
	        		log.trace("SZUKA wniosek nr "+ numerWniosku+" Klient nrLEO  "+idKlienta );
	        		List<DlApplicationDictionary> umowy = DlApplicationDictionary.findByNumerWniosku(numerWniosku);
	        		DlApplicationDictionary app = null;
	        		if(umowy == null || umowy.size() < 1)
	        		{
	        			log.error("Dodaje wniosek do DS "+ numerWniosku + " kontrahent idDS = "+contractor.getId());
	        			if(!DSApi.context().isTransactionOpen())
	        				DSApi.context().begin();
	        			app = new DlApplicationDictionary();
	        			app.setNumerWniosku(numerWniosku);
	        			if(contractor != null)
	        				app.setIdKlienta(contractor.getId());
	        			if(dostawca != null)
	        				app.setIdDostawcy(dostawca.getId());
	        			app.setOpis("Przedmiot: "+rs.getString("PRZEDMIOT"));
	        			if(contract != null)
	        				app.setContractId(contract.getId());
	        			app.setStatus(rs.getString("STATUS"));
	        			app.setLeasingObjectType(ImpulsImportUtils.getType(rs.getString("TYPPRZEDMIOTU"), rs.getLong("IDTYPPRZEDMIOTU")));
	        			app.create();
	        			DSApi.context().commit();
	        		}
	        		else
	        		{
	        			log.trace("UPDATE wniosek w DS "+ numerWniosku + " kontrahent idDS = "+contractor.getId());
	        			if(!DSApi.context().isTransactionOpen())
	        				DSApi.context().begin();
	        			app = umowy.get(0);
	        			app.setNumerWniosku(numerWniosku);
	        			if(contractor != null)
	        				app.setIdKlienta(contractor.getId());
	        			if(dostawca != null)
	        				app.setIdDostawcy(dostawca.getId());
	        			app.setOpis("Przedmiot: "+rs.getString("PRZEDMIOT")+"\n Status: "+rs.getString("STATUS"));
	        			if(contract != null)
	        				app.setContractId(contract.getId());
	        			app.setStatus(rs.getString("STATUS"));
	        			app.setLeasingObjectType(ImpulsImportUtils.getType(rs.getString("TYPPRZEDMIOTU"), rs.getLong("IDTYPPRZEDMIOTU")));
	        			DSApi.context().commit();
	        		}
	        		if(contract != null && app != null)
	        		{
	        			if(!DSApi.context().isTransactionOpen())
	        				DSApi.context().begin();
	        			contract.setApplicationId(app.getId());
	        			DSApi.context().commit();
	        		}
	    		}
	    		catch (Exception e) 
	    		{
	    			if(DSApi.context().isTransactionOpen())
	    				DSApi.context()._rollback();
	        		log.error("B��d dodania wniosku "+numerWniosku,e);
				}
	    		log.trace("_______NEXT__________");
	    		i++;
	    		if(i%200 == 0)
				{
					log.error("Czyszczenie sesi po "+i+" dokumentach");
					DSApi.context().session().flush();
					DSApi.context().session().clear();
				}
        	}
    	}
    	catch (Exception e) 
		{
    		 log.error("B��d podczas pracy agenta ilpol ",e);
		}		
		finally
		{
			if(rs != null){try {rs.close();}catch (SQLException e) {}}
			if(ps != null){try {ps.close();}catch (SQLException e) {}}			
		}
    }
}
