package pl.compan.docusafe.service.imports;

import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.imports.ImportedDocumentCommon;
import pl.compan.docusafe.core.imports.ImportedFileBlob;
import pl.compan.docusafe.core.imports.ImportedFileCsv;
import pl.compan.docusafe.core.imports.ImportedFileInfo;
import pl.compan.docusafe.core.imports.ImportManager;
import pl.compan.docusafe.core.imports.ImportedDocumentInfo;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.util.DateUtils;

import java.util.*;
import java.io.File;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;


/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class ImportDocumentsService extends ServiceDriver implements Service
{
    private static final Log log = LogFactory.getLog(ImportDocumentsService.class);
    
    /**
     * Po zaimportowaniu pewnej liczby dokument�w warto zamkn�� sesj�
     * i otworzy� j� ponownie, poniewa� po wykonaniu du�ej ilo�ci operacji
     * podczas pojedynczej sesji znacznie wzrasta u�ycie procesora i import
     * przebiega bardzo powoli.
     */
    public static final int MAX_SESSION_DOCUMENTS = 100;
    
    private Timer timer;
    
    protected void start() throws ServiceException
    {
        if (!Configuration.hasExtra("business"))
            return;        
        timer = new Timer(true);
        timer.schedule(new ImportDocuments(),1000, 30 * DateUtils.SECOND);
    }
    
    protected void stop() throws ServiceException
    {
        if (timer != null)
            timer.cancel();
    }
    
    protected boolean canStop()
    {
        return false;
    }
    
    private static void indexDSIImport()
    {
    	try
    	{
    		DSApi.openAdmin();
    		List<ImportedFileInfo> importedFiles = ImportedFileInfo.findByStatus(ImportManager.STATUS_PARSE);
    		for (ImportedFileInfo importedFileInfo : importedFiles)
			{
				try
				{
					DSIImportHandler handler = DSIImportHandler.getHandler(importedFileInfo);
					handler.parseImportedFile(importedFileInfo);
					DSApi.context().begin();
					importedFileInfo.setStatus(ImportManager.STATUS_WAITING);
					DSApi.context().commit();
				}
				catch (Exception e) 
				{
					log.error("",e);
					DSApi.context().begin();
					importedFileInfo.setStatus(ImportManager.STATUS_ERROR);
					DSApi.context().commit();
				}	
			}
    	}
    	catch (Exception e) 
    	{
			log.error("",e);
		}
    	finally
    	{
    		DSApi._close();
    	}
    }
    
    class ImportDocuments extends TimerTask
    {
        public void run()
        {
            try
            {
            	/**Odpalenie indeksowania importu do DSI*/
            	indexDSIImport();
            	
                DSApi.openAdmin();                
                Integer docCounter = 0;
                List<ImportedFileInfo> importedFiles = ImportedFileInfo.findByStatus(ImportManager.STATUS_WORKING);
                
                if (importedFiles.isEmpty())
                {
                    return;
                }
                
                //b�dziemy przegl�da� list� id �eby m�c za�adowa� obiekt ImportedFileInfo ponownie
                //w razie konieczno�ci zrestartowania sesji
                List<Long> fileInfoIds = new ArrayList<Long>();
                for (ImportedFileInfo importedFileInfo : importedFiles)
                {
                    fileInfoIds.add(importedFileInfo.getId());
                }
                
                for (Long fileInfoId : fileInfoIds)
                {
                    Integer importType = null;
                    ImportedFileInfo importedFileInfo = null;
                    importedFileInfo = ImportedFileInfo.find(fileInfoId);
                    importType = importedFileInfo.getImportType();
                    DSApi.context().session().evict(importedFileInfo);
                    
                    if (ImportManager.TYPE_AEGON_BIZ_SIMPLE.equals(importType))
                    {
                        DSApi.context().begin();
                        
                        List<ImportedDocumentInfo> importedDocuments = ImportedDocumentInfo.find(fileInfoId,
                            ImportManager.STATUS_ERROR);
                        for (ImportedDocumentInfo importedDocumentInfo : importedDocuments)
                        {
                            importedDocumentInfo.setProcessedTime(new Date());
                            importedDocumentInfo.setStatus(ImportManager.STATUS_DONE);
                            importedDocumentInfo.setResult(ImportManager.RESULT_ERROR);
                        }
                        DSApi.context().commit();
                        
                        importedDocuments = ImportedDocumentInfo.find(fileInfoId, ImportManager.STATUS_WORKING);
                        
                        for (ImportedDocumentInfo importedDocumentInfo : importedDocuments)
                        {
                            DSApi.context().begin();
                            
                            if (!createDocument(importedDocumentInfo))
                                importedDocumentInfo.setResult(ImportManager.RESULT_ERROR);
                            else
                                importedDocumentInfo.setResult(ImportManager.RESULT_OK);
                            importedDocumentInfo.setStatus(ImportManager.STATUS_DONE);
                            importedDocumentInfo.setProcessedTime(new Date());
                            
                            DSApi.context().commit();
                        }
                        
                        DSApi.context().begin();
                        
                        importedFileInfo = ImportedFileInfo.find(fileInfoId);
                        importedFileInfo.setStatus(ImportManager.STATUS_DONE);
                        importedFileInfo.setEndTime(new Date());
                        
                        DSApi.context().commit();
                    }
                    else if (ImportManager.TYPE_OGOLNY_XML.equals(importType))
                    {                        
                        List<ImportedDocumentCommon> importedDocuments = ImportedDocumentCommon.find(fileInfoId,
                            ImportManager.STATUS_WORKING);
                        
                        int counter = 0;
                        ImportedFileBlob blob = ImportedFileBlob.findByFileInfo(importedFileInfo);
                        Map<Long, org.dom4j.Element> docs = new HashMap<Long, org.dom4j.Element>();
                        SAXReader reader = new SAXReader();
                        try
                        {
                            org.dom4j.Document doc = reader.read(blob.getXmlFile());
                            org.dom4j.Element root = doc.getRootElement();
                            for (ImportedDocumentCommon importedDocumentCommon : importedDocuments)
                            {
                                int nif = importedDocumentCommon.getNumberInFile();
                                Iterator itr = root.elements("document").iterator();
                                int i;
                                for (i = 0; i < nif; ++i)
                                {
                                    if (itr.hasNext())
                                        itr.next();
                                }
                                docs.put(importedDocumentCommon.getId(), (org.dom4j.Element) itr.next());
                            }
                            counter = 0;
                            for (ImportedDocumentCommon importedDocumentCommon : importedDocuments)
                            {
                                DSApi.context().begin();
                                
                                if (!createDocumentFromXml(importedDocumentCommon, docs.get(importedDocumentCommon.getId())))
                                {
                                    importedDocumentCommon.setResult(ImportManager.RESULT_ERROR);
                                }
                                else
                                {
                                    importedDocumentCommon.setResult(ImportManager.RESULT_OK);
                                }
                                importedDocumentCommon.setStatus(ImportManager.STATUS_DONE);
                                importedDocumentCommon.setProcessedTime(new Date());
                                
                                DSApi.context().commit();
                            }
                            
                            DSApi.context().begin();
                            
                            importedFileInfo = ImportedFileInfo.find(fileInfoId);
                            importedFileInfo.setStatus(ImportManager.STATUS_DONE);
                            importedFileInfo.setEndTime(new Date());
                            
                            DSApi.context().commit();
                        }
                        catch (Exception e)
                        {
                        	LogFactory.getLog("eprint").debug("", e);
                            DSApi.context().begin();
                            
                            importedFileInfo = ImportedFileInfo.find(fileInfoId);
                            importedFileInfo.setStatus(ImportManager.STATUS_ERROR);
                            importedFileInfo.setEndTime(new Date());
                            
                            DSApi.context().commit();
                        }
                        
                    }
                    else if (ImportManager.isCsvImport(importType, false))
                    {
                        String typeCn = null;
                        
                        typeCn = ImportManager.getDockindCn(importType);
                        
                        ImportedFileCsv blob = ImportedFileCsv.findByFileInfo(fileInfoId);
                        
                        ArrayList<String> lines = blob.getLines();
                        
                        /*
                         DSApi.context().begin();
                         
                         List<ImportedDocumentInfo> importedDocuments = ImportedDocumentInfo.find(fileInfoId,
                         ImportManager.STATUS_ERROR);
                         for (ImportedDocumentInfo importedDocumentInfo : importedDocuments)
                         {
                         importedDocumentInfo.setProcessedTime(new Date());
                         importedDocumentInfo.setStatus(ImportManager.STATUS_DONE);
                         importedDocumentInfo.setResult(ImportManager.RESULT_ERROR);
                         }
                         DSApi.context().commit();
                         */

                        List<Long> infoIds = new ArrayList<Long>();
                        synchronized (ImportedDocumentInfo.class)
                        {
                            List<ImportedDocumentInfo> importedDocuments = null;
                            importedDocuments = ImportedDocumentInfo.find(fileInfoId, ImportManager.STATUS_WORKING);

                            log.info("Znaleziono dokument�w do importu: " + importedDocuments.size() + " dokument�w typu " + importType);

                            for (ImportedDocumentInfo importedDocumentInfo : importedDocuments)
                            {
                                infoIds.add(importedDocumentInfo.getId());
                            }
                        }
                        log.info("Rozpocz�to import " + infoIds.size() + " dokument�w typu " + importType);
                        
                        for (Long infoId : infoIds)
                        {
                            synchronized (ImportedDocumentInfo.class)
                            {
                                
                                DSApi.context().begin();
                                
                                String errorInfo = null;
                                ImportedDocumentInfo importedDocumentInfo = ImportedDocumentInfo.find(infoId);
                                try
                                {
                                    log.debug("import dokumentu: " + importedDocumentInfo.getTiffName());
                                    createDocumentCsv(importedDocumentInfo, lines, typeCn);
                                    ++docCounter;
                                    log.error("NBUMER DOC "+ docCounter);
                                    if (docCounter == MAX_SESSION_DOCUMENTS)
                                    {
                                        DSApi.context().commit();
                                        DSApi._close();
                                        DSApi.openAdmin();
                                        DSApi.context().begin();
                                        importedDocumentInfo = ImportedDocumentInfo.find(infoId);
                                        docCounter = 0;
                                        log.info("Restart sesji");
                                    }
                                    log.info("import dokumentu udany");
                                    //DSApi.context().commit();
                                }
                                catch (Exception e)
                                {
                                    log.error("import dokumentu nieudany", e);
                                    errorInfo = e.getMessage();
                                    errorInfo = errorInfo.substring(0, Math.min(200, errorInfo.length())).replaceAll("\n", " ");
                                    //try {DSApi.context().rollback();} catch (EdmException f) {log.error(f.getMessage(), f);};
                                    DSApi.context().setRollbackOnly();
                                    DSApi._close();
                                    DSApi.openAdmin();
                                    DSApi.context().begin();
                                    importedDocumentInfo = ImportedDocumentInfo.find(infoId);
                                    docCounter = 0;
                                    log.info("Restart sesji");
                                }
                                if (!DSApi.context().isTransactionOpen())
                                {
                                	log.error("Transakcja nie byla otwarta");
                                	DSApi.context().begin();
                                }
                                //DSApi.context().begin();
                                if (errorInfo != null)
                                {
                                    importedDocumentInfo.setResult(ImportManager.RESULT_ERROR);
                                    importedDocumentInfo.setErrorInfo(errorInfo);
                                }
                                else
                                {
                                    importedDocumentInfo.setResult(ImportManager.RESULT_OK);
                                }

                                importedDocumentInfo.setProcessedTime(new Date());
                                importedDocumentInfo.setStatus(ImportManager.STATUS_DONE);
                                
                                DSApi.context().session().update(importedDocumentInfo);
                                DSApi.context().commit();
                            }
                        }
                        
                        synchronized (ImportedDocumentInfo.class)
                        {
                            DSApi.context().begin();
                            importedFileInfo = ImportedFileInfo.find(fileInfoId);
                            importedFileInfo.setStatus(ImportManager.STATUS_DONE);
                            importedFileInfo.setEndTime(new Date());
                            DSApi.context().commit();
                            log.info("zako�czenie importu dokument�w");
                        }
                        
                    }
                    else
                    {
                    }
                }
                
            }
            catch (Exception e)
            {
            	DSApi.context().setRollbackOnly();
                log.error("b��d krytyczny podczas importu", e);
            }
            finally
            {
                DSApi._close();
            }
        }
        
        /**
         * Tworzymy nowy dokument na podstawie informacji z importedDocumentInfo. Zwraca true <=> uda�o sie
         * poprawnie utworzyc dokument.
         *
         * @param importedDocumentInfo informacja o dokumencie do zaimportowania
         * @return
         * @throws EdmException
         */
        private boolean createDocument(ImportedDocumentInfo importedDocumentInfo) throws EdmException
        {
            Doctype doctype = Doctype.findByCn("nationwide");
            
            Doctype.Field typField = doctype.getFieldByCn("RODZAJ");
            Doctype.EnumItem typEnum;
            try
            {
                typEnum = typField.getEnumItemById(importedDocumentInfo.getTypDokumentuId());
            }
            catch (EntityNotFoundException e)
            {
                importedDocumentInfo.setErrorInfo("Podano niepoprawny identyfikator typu dokumentu");
                return false;
            }
            
            // sprawdzam poprawnosc pliku tiff
            File tiffFile = new File(importedDocumentInfo.getImportedFileInfo().getTiffDirectory() + "/"
                + importedDocumentInfo.getTiffName());
            if (tiffFile == null || !tiffFile.exists())
            {
                importedDocumentInfo.setErrorInfo("Podano niepoprawny plik tiff");
                return false;
            }
            
            String summary = typEnum.getTitle() + " (" + importedDocumentInfo.getNrPolisy() + ")";
            Document newDocument = new Document(summary, summary);
            
            newDocument.setFolder(Folder.getRootFolder());
            newDocument.setDoctype(doctype);
            
            newDocument.create();
            DSApi.context().session().flush();
            
            doctype.set(newDocument.getId(), typField.getId(), typEnum.getId());
            Doctype.Field field = doctype.getFieldByCn("NR_POLISY");
            doctype.set(newDocument.getId(), field.getId(), importedDocumentInfo.getNrPolisy());
            field = doctype.getFieldByCn("DATA_WPLYWU");
            doctype.set(newDocument.getId(), field.getId(), importedDocumentInfo.getDate());
            field = doctype.getFieldByCn("STATUS");
            doctype.set(newDocument.getId(), field.getId(), field.getEnumItemByCn("ARCHIWALNY").getId());

            DSApi.context().session().flush();
            
            if (tiffFile != null)
            {
                Attachment attachment = new Attachment("Skan");
                newDocument.createAttachment(attachment);
                
                attachment.createRevision(tiffFile).setOriginalFilename(importedDocumentInfo.getTiffName());
            }
            
            Box box;
            try
            {
                box = Box.findByName(null,importedDocumentInfo.getBoxNumber());
            }
            catch (EntityNotFoundException e)
            {
                // takie pudlo nie istnieje - tworze je
                box = new Box(importedDocumentInfo.getBoxNumber().toUpperCase());
                box.setOpen(true);
                box.setOpenTime(new Date());
                box.create();
                box.setCloseTime(new Date());
            }
            newDocument.setBox(box);
            
            // zapamietujemy id dokumentu, ktory utworzylismy
            importedDocumentInfo.setDocumentId(newDocument.getId());
            
            return true;
        }
        
        /**
         * Tworzy nowy dokument na podstawie informacji z importedDocumentCommon oraz pobranych
         * z elementu XMLa.
         * Zwraca true <=> uda�o sie poprawnie utworzyc dokument.
         * 
         * @param importedDocumentCommon informacja o dokumencie do zaimportowania
         * @param root element dom4j dokumentu
         * @return
         * @throws EdmException
         */
        private boolean createDocumentFromXml(ImportedDocumentCommon importedDocumentCommon, org.dom4j.Element root) throws EdmException
        {
            try
            {
                if (!root.getName().equals("document"))
                    throw new EdmException("Blob nie zawiera prawid�owych danych importu");
                
                Iterator itr = root.elements("document").iterator();
                
                String dockindName = null;
                
                Element docElement = root;
                Element el = null;
                dockindName = docElement.attributeValue("doctype-id").toLowerCase();
                DocumentKind dockind = null;
                if (dockindName != null)
                {
                    dockind = DocumentKind.findByCn(dockindName);
                    if (dockind == null)
                    {
                        importedDocumentCommon.setErrorInfo("Niew�a�ciwy typ dokumentu: " + dockindName);
                        return false;
                    }
                }
                else
                {
                    importedDocumentCommon.setErrorInfo("Nie podano typu dokumentu");
                    return false;
                }
                
                el = docElement.element("attachments");
                if (el != null)
                {
                    itr = el.elementIterator("attachment");
                    File tiffFile = null;
                    while (itr.hasNext())
                    {
                        el = (Element) itr.next();
                        String fileName = el.attributeValue("file-name");
                        if (fileName == null)
                            fileName = "";
                        String attachmentName = el.attributeValue("attachment-name");
                        if (attachmentName == null)
                            attachmentName = "";
                        tiffFile = new File(importedDocumentCommon.getImportedFileInfo().getTiffDirectory() + "/" + fileName);
                        if (tiffFile == null || !tiffFile.exists())
                        {
                            importedDocumentCommon.setErrorInfo("Podano niepoprawny plik tiff: " + fileName);
                            return false;
                        }
                    }
                }
                
                String title = "";
                String description = "";
                Element ael = docElement.element("archive");
                if (ael != null)
                {
                    el = ael.element("title");
                    if (el != null)
                        title = el.getStringValue();
                    el = ael.element("description");
                    if (el != null)
                        description = el.getStringValue();
                }
                Document newDocument = new Document(title, description);
                Folder folder = null;
                
                Field field = null;
                Map<String, Object> values = new HashMap<String, Object>();
                el = docElement.element("doctype");
                if (el != null)
                {
                    itr = el.elementIterator();
                    while (itr.hasNext())
                    {
                        el = (Element) itr.next();
                        field = dockind.getFieldByCn(el.getName().toUpperCase());
                        if (field == null)
                            dockind.getFieldByCn(el.getName().toLowerCase());
                        if (field != null)
                        {
                            values.put(field.getCn(), el.getData());
                        }
                        else
                        {
                        }
                    }
                }
                
                try
                {
                    dockind.logic().validate(values, dockind, null);
                }
                catch (ValidationException e)
                {
                    importedDocumentCommon.setErrorInfo("B��d walidacji danych: " + e.getMessage());
                    return false;
                }
                
                newDocument.setDocumentKind(dockind);
                newDocument.setFolder(Folder.getRootFolder());
                newDocument.create();
                DSApi.context().session().flush();
                
                dockind.set(newDocument.getId(), values);
                dockind.logic().archiveActions(newDocument, DocumentKindsManager.getType(newDocument));
                dockind.logic().documentPermissions(newDocument);
                if (newDocument.getFolder() == null)
                {
                    if (ael != null)
                    {
                        String folderId = ael.attributeValue("folder-id");
                        if (folderId != null)
                        {
                            Long fid = Long.parseLong(folderId);
                            try
                            {
                                folder = (Folder) DSApi.context().session().get(Folder.class, fid);
                                if (folder != null)
                                {
                                }
                            }
                            catch (Exception e)
                            {
                                folder = null;
                            }
                        }
                        String folderName = ael.attributeValue("folder-name");
                        if (folder == null && folderName != null)
                        {
                            String[] splitName = folderName.split("/");
                            folderName = splitName[splitName.length - 1];
                            try
                            {
                                folder = Folder.findSystemFolder(folderName);
                                if (folder != null)
                                {
                                }
                            }
                            catch (Exception e)
                            {
                                folder = null;
                            }
                        }
                        /*
                         el = ael.element("permissions");
                         if (el != null){
                         itr = el.elementIterator("permission");
                         while (itr.hasNext()){
                         el = (Element) itr.next();
                         attr = el.attribute("resource");
                         }
                         }
                         */
                    }
                    if (folder == null)
                    {
                        folder = Folder.getRootFolder();
                    }
                    newDocument.setFolder(folder);
                }
                
                el = docElement.element("attachments");
                if (el != null)
                {
                    itr = el.elementIterator("attachment");
                    File tiffFile = null;
                    while (itr.hasNext())
                    {
                        el = (Element) itr.next();
                        String fileName = el.attributeValue("file-name");
                        String attachmentName = el.attributeValue("attachment-name");
                        tiffFile = new File(importedDocumentCommon.getImportedFileInfo().getTiffDirectory() + "/" + fileName);
                        Attachment attachment = new Attachment(attachmentName);
                        newDocument.createAttachment(attachment);
                        attachment.createRevision(tiffFile).setOriginalFilename(fileName);
                    }
                }
                
                importedDocumentCommon.setDocumentId(newDocument.getId());
            }
            catch (Exception e)
            {
                throw new EdmException("B��d podczas odczytu/parsowania strumienia XML");
            }
            
            return true;
        }
    }


    /**
     * Tworzy nowy dokument z pliku csv o podanym kodzie(mapowanie kod�w na klas� dziedzicz�c�
     * z ImportHandler przyda�oby si� umie�ci� w pliku .properties(?)).
     * Zwraca true <=> uda�o sie poprawnie utworzyc dokument.
     *
     * @param importedDocumentInfo informacja o dokumencie do zaimportowania
     * @param lines linie pliku csv
     * @param typeCn kod typu importu
     * @return
     * @throws EdmException
     */
    private void createDocumentCsv(ImportedDocumentInfo importedDocumentInfo, ArrayList<String> lines, String typeCn)
        throws EdmException
    {
        ImportHandler handler = ImportHandler.getInstance(typeCn);
        if (handler == null)
        {
            throw new EdmException("Nie znaleziono klasy handlera importu: " + typeCn);
        }

        try
        {        	
            handler.createDocument(importedDocumentInfo, lines.get(importedDocumentInfo.getLineNumber() - 1)); 
        }
        catch (EdmException e)
        {
            throw e;
        }
    }

}
