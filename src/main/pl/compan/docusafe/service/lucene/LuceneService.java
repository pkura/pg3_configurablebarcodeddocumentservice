package pl.compan.docusafe.service.lucene;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.util.Version;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.lucene.IndexHelper;
import pl.compan.docusafe.lucene.IndexStatus;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;

@Deprecated
public class LuceneService extends ServiceDriver implements Service {
	private static final Logger log = LoggerFactory.getLogger(LuceneService.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(LuceneService.class.getPackage().getName(),null);
    public static final int MAX_ATTACHMENTS_INDEXED = 1000;
    private Timer timer;
	
	private static IndexHelper indexHelper = null;
	private static StandardAnalyzer analyzer = null;
	private IndexWriter indexWriter = null;
	private Boolean firstTime = true;
	private Integer period = 10;
	
	public LuceneService() {}
	
	protected void start() {
		if (!AvailabilityManager.isAvailable("menu.left.repository.fulltextSearch"))
			return;
		
		log.info("start indeksowania Lucene");
		// ustalenie interwa�u czasowego
		timer = new Timer(true);
		period = Integer.parseInt(Docusafe.getAdditionProperty("fulltextSearch.period"));
		indexHelper = new IndexHelper();
        timer.schedule(new IndexFiles(),1000, period * DateUtils.MINUTE);
        analyzer = new StandardAnalyzer(Version.LUCENE_46);
        console(Console.INFO, sm.getString("SystemWystartowal"));
	}

	protected void stop() throws ServiceException {
		log.info("stop indeksowania Lucene");
		console(Console.INFO, sm.getString("StopUslugi"));
		if (timer != null) timer.cancel();
	}
	
	private class IndexFiles extends TimerTask {
		/**
		 * Tutaj odbywa sie indeksowanie plik�w.
		 */
		public void run() {
			try {
				DSApi.openAdmin();
                // otwarcie index writera (nie tworzymy nowego indeksu - dopisujemy do istniej�cego)
                IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_46, analyzer);
                iwc.setOpenMode(Docusafe.getIndexesFiles().list().length == 0 ? IndexWriterConfig.OpenMode.CREATE : IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
                indexWriter = new IndexWriter(Docusafe.getIndexes(), iwc);

				String dockindCns = Docusafe.getAdditionProperty("fulltextSearch.dk");
                for(String dockindCn : dockindCns.split(",")){
                    indexDockindDocuments(dockindCn);
                }
                if(AvailabilityManager.isAvailable("fulltextSearch.indexAllAttachmentRevisions")){
                    indexAttachmentRevisions();
                }
//                DSApi.context().begin();
//                DSApi.context().systemPreferences().node("luceneService").putBoolean("firstTime", false);
//                DSApi.context().commit();
			} catch (Exception e) {
				log.error("", e);
				console(Console.ERROR, "ERROR: "+e.getMessage());
			} finally {
				try {
					// je�eli zainicjowano indexWritera
					if (indexWriter != null) {
						indexWriter.close();
						indexWriter = null;
					}
					DSApi.close();
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
		}
	}

    private void indexDockindDocuments(String dockindCn) throws Exception {
        DocumentKind dk = DocumentKind.findByCn(dockindCn);
        List<Long> documents = indexHelper.getDocumentsToIndex(dk.getId());

        // je�eli nie ma co indeksowa� to czekamy do nast�pnego sprawdzenia
        if (documents == null || documents.size()==0) {
            return;
        }

        firstTime = DSApi.context().systemPreferences().node("luceneService").getBoolean("firstTime", true);

        Long start = System.currentTimeMillis();

        // zaindeksowanie podanych dokument�w
        for (Long docId : documents) {
            DSApi.context().begin();
            indexHelper.IndexDocument(docId, indexWriter);
            DSApi.context().commit();
        }

        Long end = System.currentTimeMillis();
        console(Console.INFO, "Zaindeksowano plik�w: "+documents.size()+" Czas indeksowania [ms]: "+(end-start));

    }

    private void indexAttachmentRevisions() throws EdmException, IOException {
        log.info("indexing attachment revisions");

        List<AttachmentRevision> atts = DSApi.context().session()
                .createCriteria(AttachmentRevision.class)
                .add(Restrictions.eq("indexStatusId", IndexStatus.NOT_INDEXED.getId()))
                .setMaxResults(MAX_ATTACHMENTS_INDEXED)
                .list();

        log.info("{} unindexed attachment found (max {})", atts.size(), MAX_ATTACHMENTS_INDEXED);
        for(AttachmentRevision rev: atts){
            DSApi.context().begin();
            log.info("indexing attachment revision : " + rev.getId());
            indexHelper.indexAttachmentRevision(rev, indexWriter);
            DSApi.context().commit();
        }
    }


    protected boolean canStop() {
		return false;
	}
	
	public boolean hasConsole(){
		return true;
	}
}
