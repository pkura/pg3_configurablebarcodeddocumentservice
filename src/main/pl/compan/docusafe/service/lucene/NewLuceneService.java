package pl.compan.docusafe.service.lucene;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.hibernate.Query;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.general.mapers.utils.strings.PolishStringUtils;
import pl.compan.docusafe.lucene.IndexHelper;
import pl.compan.docusafe.service.IterateTimerServiceDriver;
import pl.compan.docusafe.service.IterateTimerTask;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.annotation.Nullable;
import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewLuceneService extends IterateTimerServiceDriver {
    private static final Logger log = LoggerFactory.getLogger(NewLuceneService.class);

    public NewLuceneService() {
        super(new Task().setIterateInContext(true));
    }

    public static Map<String, String> latinToUtf8 = new HashMap<String, String>();

    static {
        // lowercase
        latinToUtf8.put("b1", "c485");
        latinToUtf8.put("b9", "c485"); // windows-1250

        latinToUtf8.put("ea", "c499");
        latinToUtf8.put("f3", "c3b3");
        latinToUtf8.put("b3", "c582");
        latinToUtf8.put("bf", "c5bc");

        latinToUtf8.put("bc", "c5ba");
        latinToUtf8.put("9f", "c5ba"); // windows-1250

        latinToUtf8.put("e6", "c487");
        latinToUtf8.put("f1", "c584");

        latinToUtf8.put("b6", "c59b");
        latinToUtf8.put("9c", "c59b"); // windows-1250

        // upper case
        latinToUtf8.put("a1", "c485");
        latinToUtf8.put("a9", "c485"); // windows-1250

        latinToUtf8.put("ca", "c499");
        latinToUtf8.put("d3", "c3b3");
        latinToUtf8.put("a3", "c582");
        latinToUtf8.put("af", "c5bc");

        latinToUtf8.put("ac", "c5ba");
        latinToUtf8.put("8f", "c5ba"); // windows-1250

        latinToUtf8.put("c6", "c487");
        latinToUtf8.put("d1", "c584");

        latinToUtf8.put("a6", "c59b");
        latinToUtf8.put("8c", "c59b"); // windows-1250
    }

    public static class Task extends IterateTimerTask<Document> {

        private FSDirectory index;
        private IndexWriter indexWriter;
        private StandardAnalyzer analyzer;
        private IndexHelper indexHelper;

        @Override
        protected void beforeRun() throws Exception {
            String indexFolder = Docusafe.getAdditionPropertyOrDefault("fulltext.documentsIndexFolder", "indexes_documents");

            // setup index data
            final File fileIndexes = new File(Docusafe.getHome(), indexFolder);
            fileIndexes.mkdir();

            index = FSDirectory.open(fileIndexes);

            indexHelper = new IndexHelper();
            analyzer = new StandardAnalyzer(Version.LUCENE_46);

            IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_46, analyzer);
            iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
            indexWriter = new IndexWriter(index, iwc);

            // get iterator for documents
            String queryString = "SELECT d FROM Document d";
            Query q = DSApi.context().session().createQuery(queryString);
//            q.setMaxResults(100);
            iterator = q.iterate();
        }

        @Override
        protected void afterRun() throws Exception {
            if(indexWriter != null) {
                indexWriter.close();
            }

            if(index != null) {
                index.close();
            }
        }

        @Override
        protected void forEach(Document document, int index) throws Exception {
            log.info("[forEach] indexing doc.id = {}", document.getId());

            org.apache.lucene.document.Document luceneDoc = buildIndexedDocuments(document, null);

            //                String contentField = "ID: "+ doc.getDocumentId() + ", " +
            //                        "Autor: "+ doc.getAuthor() + ", " +
            //                        "Tytu�: "+ doc.getTitle() + ", " +
            //                        "Czas utworzenia: "+ doc.getCtime() + ", " +
            //                        "Abstrakt: "+ doc.getAbstrakt() + ", " +
            //                        "Lokalizacja: "+ doc.getLocation() + ", " +
            //                        "Typ: "+ doc.getType() + ", " +
            //                        "Organizational Unit: "+ doc.getOrganizationalUnit();
            //                luceneDoc.add( new Field( "content", contentField, Field.Store.YES, Field.Index.ANALYZED ) );

            if(indexWriter.getConfig().getOpenMode() == IndexWriterConfig.OpenMode.CREATE) {
                indexWriter.addDocument(luceneDoc);
            } else {
                indexWriter.updateDocument(new Term("id", document.getId().toString()), luceneDoc);
            }
        }

        private org.apache.lucene.document.Document buildIndexedDocuments(Document document, Long maxAttachmentRevisionId) throws EdmException, IOException {
            org.apache.lucene.document.Document luceneDoc = new org.apache.lucene.document.Document();
            Date ctime = document.getCtime();
            String text = getTextFromAttachments(document, maxAttachmentRevisionId);
            InputStream stream = IOUtils.toInputStream(new PolishStringUtils().zamienPolskieZnaki(text));
            final FieldsManager fm = document.getFieldsManager();
            Object docDescription = fm.getFieldValues().get("DOC_DESCRIPTION");

            luceneDoc.add( new StringField( "id", document.getId().toString(), Field.Store.YES) );
            luceneDoc.add( new StringField( "author", Strings.nullToEmpty(document.getAuthor()), Field.Store.YES) );
            luceneDoc.add( new LongField( "ctime", ctime == null ? 0L : ctime.getTime(), Field.Store.NO) );
            luceneDoc.add( new TextField( "title", new PolishStringUtils().zamienPolskieZnaki(Strings.nullToEmpty(document.getTitle())), Field.Store.YES) );
            luceneDoc.add(new TextField("content", new BufferedReader(new InputStreamReader(stream, "ISO-8859-2"))));

            luceneDoc.add(new StringField("DOC_DESCRIPTION", docDescription != null ? new PolishStringUtils().zamienPolskieZnaki(docDescription.toString()) : "", Field.Store.YES));
            return luceneDoc;
        }

    }

    public static String getTextFromAttachments(Document document, Long maxAttachmentRevisionId) throws EdmException, IOException {
        return getTextFromAttachments(document, true, maxAttachmentRevisionId);
    }

    private static boolean newAttachmentsRevisions(Document document, Long maxAttachmentRevisionId) throws EdmException {
        for(Attachment att : document.getAttachments()){
            if(att.getNewestRevision().getId() > maxAttachmentRevisionId){
                return true;
            }
        }
        return false;
    }

    /**
     * <p>
     *     Gets concatenated text from all {@code text/plain} and {@code application/pdf} attachments, newest revisions.
     * </p>
     *
     *
     * @param document
     * @param convertEncoding if true it will convert UTF-8 to ISO-8859-2
     * @return concatenated text from attachments
     * @throws EdmException
     * @throws IOException
     */
    public static String getTextFromAttachments(Document document, boolean convertEncoding, Long maxAttachmentRevisionId) throws EdmException, IOException {

        log.trace("[getTextFromAttachments] document.id = {}", document.getId());

        final List<Attachment> attachments = document.getAttachments();

        String text = "";

        if (attachments.size() == 0) {
            log.trace("[getTextFromAttachments] no attachments");
            return text;
        }

        if (maxAttachmentRevisionId == null || newAttachmentsRevisions(document, maxAttachmentRevisionId)) {
            for (Attachment attachment : attachments) {
                AttachmentRevision rev = attachment.getNewestRevision();
                if (log.isTraceEnabled()) {
                    log.trace("[getFirstAttachmentStream]     doc.id = {}, rev.id = {}, rev.mime = {}", document.getId(), rev.getId(), rev.getMime());
                }

                try {
                    // pdf
                    if (Objects.equal(rev.getMime(), "application/pdf")) {
                        log.info("[getFirstAttachmentStream]     indexing pdf, rev.id = {}", rev.getId());

                        InputStream stream = rev.getBinaryStream();
//                        text += getTextFromPdf(stream) + " ";
                        String s = getTextFromPdf(stream) + " ";

//                        byte[] latin2 = new String(utf8, "UTF-8").getBytes("ISO-8859-2");

                        if (convertEncoding) {
                            byte[] utf8 = s.getBytes("UTF-8");
                            text += magic(utf8);
                        } else {
                            text += s;
                        }

                    }
                    // text
                    else if (Objects.equal(rev.getMime(), "text/plain")) {
                        log.info("[getFirstAttachmentStream]     indexing text, rev.id = {}", rev.getId());
                        InputStream stream = rev.getBinaryStream();


    //                        byte[] utf8 = IOUtils.toByteArray(stream);
    //                        byte[] latin2 = new String(utf8, "UTF-8").getBytes("ISO-8859-2");

                        byte[] utf8 = IOUtils.toByteArray(stream);
                        if (convertEncoding) {
                            text += magic(utf8);
                        } else {
                            String s = new String(utf8, "UTF-8");
                            text += s;
                        }
    //                        text += IOUtils.toString(stream) + " ";

                    }
                } catch (Exception ex) {
                    log.error("[getFirstAttachmentStream]     error", ex);
                }
                text += " ";
            }
        }else{
            text = null;
        }
        return text;

    }

    private static String revereMagicSingle(String word) {
        String wordAfter = latinToUtf8.get(word);
        return wordAfter == null ? word : wordAfter;
    }

    public static String reverseMagic(byte[] latin2) {
        String str = Hex.encodeHexString(latin2);

        String line = Hex.encodeHexString(latin2);
        Pattern r = Pattern.compile("([A-Za-z0-9]{2})");
        Matcher m = r.matcher(line);
        String previous = null;
        List<String> wordList = new ArrayList<String>();
        while (m.find()) {
            wordList.add(m.group(1));
        }

        ImmutableList<Object> list = FluentIterable.from(wordList).transform(new Function<String, Object>() {
            @Nullable
            @Override
            public Object apply(@Nullable String input) {
                return revereMagicSingle(input);
            }
        }).toList();

        str = StringUtils.join(list, "");

//        // lowercase
//        str = str.replace("b1", "c485");
//        str = str.replace("b9", "c485"); // windows-1250
//
//        str = str.replace("ea", "c499");
//        str = str.replace("f3", "c3b3");
//        str = str.replace("b3", "c582");
//        str = str.replace("bf", "c5bc");
//
//        str = str.replace("bc", "c5ba");
//        str = str.replace("9f", "c5ba"); // windows-1250
//
//        str = str.replace("e6", "c487");
//        str = str.replace("f1", "c584");
//
//        str = str.replace("b6", "c59b");
//        str = str.replace("9c", "c59b"); // windows-1250
//
//        // upper case
//        str = str.replace("a1", "c485");
//        str = str.replace("a9", "c485"); // windows-1250
//
//        str = str.replace("ca", "c499");
//        str = str.replace("d3", "c3b3");
//        str = str.replace("a3", "c582");
//        str = str.replace("af", "c5bc");
//
//        str = str.replace("ac", "c5ba");
//        str = str.replace("8f", "c5ba"); // windows-1250
//
//        str = str.replace("c6", "c487");
//        str = str.replace("d1", "c584");
//
//        str = str.replace("a6", "c59b");
//        str = str.replace("8c", "c59b"); // windows-1250

        Hex h = new Hex();

        try {
            byte[] decode = h.decode(str.getBytes());
            return new String(decode, "UTF-8");
//            byte[] utf8 = new String(latin2, "ISO-8859-2").getBytes("UTF-8");
//            return new String(utf8, "UTF-8");
        } catch (DecoderException e) {
            log.error("[reverseMagic] error", e);
            return "";
        } catch (UnsupportedEncodingException e) {
            log.error("[reverseMagic] error", e);
            return "";
        }
    }

    public static String magic(byte[] utf8) {
        String str = Hex.encodeHexString(utf8);
        str = str.replace("c485", "b1");
        str = str.replace("c499", "ea");
        str = str.replace("c3b3", "f3");
        str = str.replace("c582", "b3");
        str = str.replace("c5bc", "bf");
        str = str.replace("c5ba", "bc");
        str = str.replace("c487", "e6");
        str = str.replace("c584", "f1");
        str = str.replace("c59b", "b6");

        Hex h = new Hex();

        try {
            byte[] decode = h.decode(str.getBytes());
            return new String(decode, "ISO-8859-2");
        } catch (DecoderException e) {
            log.error("[magic] error", e);
            return "";
        } catch (UnsupportedEncodingException e) {
            log.error("[magic] error", e);
            return "";
        }
    }

    /**
     * <a href="http://stackoverflow.com/a/18101544/1637178">http://stackoverflow.com/a/18101544/1637178</a>
     * @param stream
     * @return
     */
    protected static String getTextFromPdf(InputStream stream) {
        PDFParser parser = null;
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;
        PDFTextStripper pdfStripper;

        String parsedText;

        log.debug("[getTextFromAttachments] processing stream");
        try {
            parser = new PDFParser(stream);
            parser.parse();
            cosDoc = parser.getDocument();
            pdfStripper = new PDFTextStripper();
            pdDoc = new PDDocument(cosDoc);
            parsedText = pdfStripper.getText(pdDoc);

//            log.debug("[getTextFromAttachments] parsedText = {}", parsedText);

            return parsedText;
        } catch (Exception e) {
            log.error("[getTextFromAttachments] error", e);
            return "";
        } finally {
            try {
                if (cosDoc != null) {
                    cosDoc.close();
                }
                if (pdDoc != null) {
                    pdDoc.close();
                }
            } catch (Exception ex) {
                log.error("[getTextFromAttachments] error, cannot close documents", ex);
            }
        }
    }

}