package pl.compan.docusafe.service.zebra;

/**
 * Interfejs, kt�ry ma zwraca� definicj� barcodu w odpowiednim jezyku programowania
 * drukarki Zebra
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public interface ZebraDriver 
{
	public String getBarcodeDefString();
}
