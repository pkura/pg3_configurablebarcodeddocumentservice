package pl.compan.docusafe.service.zebra;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.prefs.Preferences;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.PrintServiceAttribute;
import javax.print.attribute.standard.PrinterName;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * UWAGA. Operowanie na obiektach ZebraPrinter musi zawsze odbywac sie w otawrtym kontekscie.
 */
public class ZebraPrinter
{

	private static Integer fieldsPositionStart = 205;
	private static Integer fieldsPositionStep = 25;
	private static Integer barcodeHeight = 90;
	private static final Logger log = LoggerFactory.getLogger(ZebraPrinter.class);
	private transient StringManager sm = StringManager.getManager(ZebraPrinter.class.getPackage().getName());

	private Integer id;

	/** Nazwa drukarki do wy�wietlenia u�ytkownikowi */
	private String userPrinterName;
	private Boolean usePrinter;
	private String printerName;
	private String firstLine;
	private String secondLine;
	private String barcodeType;
	private String barcodeValue;
	private String language;

	private transient List<String> definedFields;
	private transient Map<String, String> fields = new HashMap<String, String>();
	private static final String ZEBRA_FIELDS_POSITION_START = "zebra.fields.positionStart";
	private static final String ZEBRA_FIELDS_POSITION_STEP = "zebra.fields.positionStep";
	private static final String ZEBRA_BARCODE_HEIGHT ="zebra.barcodeHeight";
	
	transient PrintService psZebra;

	public ZebraPrinter()
	{
	}

	/**
	 * UWAGA. Operowanie na obiektach ZebraPrinter musi zawsze odbywac sie w otawrtym kontekscie.
	 * @throws EdmException
	 */
	public ZebraPrinter(Preferences node)
	{
		try
		{
			retrieveSettings(node);
		} catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public void init() throws EdmException
	{
		String sPrinterName = null;
		PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
		for (int i = 0; i < services.length; i++)
		{
			PrintServiceAttribute attr = services[i].getAttribute(PrinterName.class);
			sPrinterName = ((PrinterName) attr).getValue();
			if (sPrinterName.indexOf(printerName) >= 0)
			{
				psZebra = services[i];
				break;
			}
		}
		if (psZebra == null)
		{
			throw new EdmException(sm.getString("BladInicjalizacjiDrukarkiKodowKreskowychZebra"));
		}
	}

	/**
	 * Drukowanie
	 *
	 * @throws EdmException
	 */
	public void print() throws EdmException
	{
		try
		{
			DocPrintJob job = psZebra.createPrintJob();
			ZebraDriver zebraDriver = null;
			if (language != null && language.equalsIgnoreCase("EPL"))
				zebraDriver = new ZebraDriverEPL(buildProperties());
			else if (language != null && language.equalsIgnoreCase("ZPL"))
				zebraDriver = new ZebraDriverZPL(buildProperties());
			if (zebraDriver != null)
			{
				byte[] by = zebraDriver.getBarcodeDefString().getBytes();
				DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
				Doc doc = new SimpleDoc(by, flavor, null);
				job.print(doc, null);
			}
		} 
		catch (Exception e)
		{
			throw new EdmException(e);
		}
	}

	/**
	 * Zapisuje ustawienia drukarki, kt�ra b�dzie u�ywana do drukowania
	 *
	 * @throws EdmException
	 */
	public void saveSettings() throws EdmException
	{
		try
		{
			Preferences node = getPrintersNode();
			node.putBoolean("usePrinter", usePrinter == null ? false : true);
			node.put("printerName", printerName);
			node.put("userPrinterName", userPrinterName);
			node.put("printerLanguage", StringUtils.isNotBlank(language) ? language : "");
			node.put("firstLine", StringUtils.isNotBlank(firstLine) ? firstLine : "");
			node.put("secondLine", StringUtils.isNotBlank(secondLine) ? secondLine : "");
			node.put("barcodeType", StringUtils.isNotBlank(barcodeType) ? barcodeType : "");
			node.put("barcodeValue", StringUtils.isNotBlank(barcodeValue) ? barcodeValue : "");
			node.put("printerId", String.valueOf(this.id));
			if (definedFields == null)
				definedFields = Lists.newArrayList();
			JSONArray arr = new JSONArray(definedFields);
			node.put("definedFields", arr.toString());
			node.flush();
		}
		catch (Exception e) 
		{
			throw new EdmException(e);
		}
	}

	/**
	 * Pobiera ustawienia drukarki, kt�ra aktualnie jest u�ywana do drukowania
	 *
	 * @param node
	 * @throws EdmException
	 */
	public void retrieveSettings(Preferences node) throws EdmException
	{
		try
		{
			this.usePrinter = node.getBoolean("usePrinter", new Boolean(false));
			this.userPrinterName = node.get("userPrinterName", "");
			this.printerName = node.get("printerName", "");
			this.language = node.get("printerLanguage", "");
			this.firstLine = node.get("firstLine", "");
			this.secondLine = node.get("secondLine", "");
			this.barcodeType = node.get("barcodeType","");
			JSONArray arr = new JSONArray(node.get("definedFields", "[]"));
			definedFields = new ArrayList<String>();
			for(int i=0;i<arr.length();i++)
			{
				definedFields.add(arr.getString(i));
			}
		}
		catch (Exception e)
		{
			throw new EdmException(e);
		}
	}

	/**
	 * Buduje ustawienia drukarki potrzebne do wydruku
	 * U�ywane przez DriverManagera
	 *
	 * @return
	 */
	private Properties buildProperties()
	{
		Properties properties = new Properties();
		properties.put("fieldsPositionStart", Integer.parseInt(Docusafe.getAdditionProperty(ZEBRA_FIELDS_POSITION_START)));
		properties.put("fieldsPositionStep", Integer.parseInt(Docusafe.getAdditionProperty(ZEBRA_FIELDS_POSITION_STEP)));
		properties.put("barcodeHeight", Integer.parseInt(Docusafe.getAdditionProperty(ZEBRA_BARCODE_HEIGHT)));
		properties.put("barcodeType", barcodeType);
		properties.put("barcodeValue", barcodeValue);
		if(StringUtils.isNotBlank(firstLine))
			properties.put("firstLine", firstLine);
		if(StringUtils.isNotBlank(secondLine))
			properties.put("secondLine", secondLine);
		properties.put("fields", fields);

		return properties;
	}

	/**
	 * Sprawdza czy by�a zapisana drukarka przed wprowadzaniem zmian zwi�zanych z dodaniem wielu drukarek
	 *
	 * @param node
	 * @return
	 */
	public static boolean isOldPrinterDefined(Preferences node)
	{
		return (node.get("userPrinterName", "").equals("") && !node.get("printerName", "").equals(""));
	}

	/**
	 * Zapisuje konfiguracj� drukarki do tabeli ds_printers
	 */
	public void savePrinterToDatabase()
	{
		try
		{
			DSApi.context().begin();
//			List<String> adddasd = Lists.newArrayList();
//			adddasd.add("dasdasd");
//			this.setDefinedFields(adddasd);
			DSApi.context().session().saveOrUpdate(this);
			DSApi.context().commit();
		} catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}

	}

	/**
	 * Usuwa konfiguracje drukark z tabeli ds_printers
	 */
	public void deletePrinterFromDatabase()
	{
		try
		{
			DSApi.context().begin();
			DSApi.context().session().delete(this);
			DSApi.context().commit();
		} catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * Pobiera drukark� po nazwie drukarki zdefiniowanej przez u�ytkownika
	 *
	 * @param userPrinterName
	 * @return
	 */
	public static ZebraPrinter findByUserPrinterName(String userPrinterName)
	{
		if (userPrinterName == null)
			return null;

		Criteria criteria = null;
		try
		{
			criteria = DSApi.context().session().createCriteria(ZebraPrinter.class);
			criteria.add(Restrictions.eq("userPrinterName", userPrinterName));
		} catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		return (ZebraPrinter) criteria.uniqueResult();
	}

	/**
	 * Pobiera drukark� po nazwie drukarki w systemie
	 *
	 * @param printerName
	 * @return
	 */
	public static ZebraPrinter findByPrinterName(String printerName)
	{
		if (printerName == null)
			return null;

		Criteria criteria = null;
		try
		{
			criteria = DSApi.context().session().createCriteria(ZebraPrinter.class);
			criteria.add(Restrictions.eq("printerName", printerName));
		} catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		return (ZebraPrinter) criteria.uniqueResult();
	}

	/**
	 * Pobiera wszystkie konfiguracje drukarek kt�re zosta�y zapisane
	 * @return
	 */
	public static List<ZebraPrinter> getAllSavedPrinters()
	{
		Criteria criteria = null;
		try
		{
			criteria = DSApi.context().session().createCriteria(ZebraPrinter.class);
		} catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		return criteria.list();
	}

	/**
	 * Wyszukuje systemowe nazwy drukarek kt�re ju� s�� zapisane
	 * @return
	 */
	public static List<String> definedSystemPrinters()
	{
		List<String> systemPrinters = Lists.newArrayList();
		PreparedStatement ps = null;
		try
		{
			ps = DSApi.context().prepareStatement("select printerName from ds_printers");
			ResultSet rs = ps.executeQuery();

			if(rs.getFetchSize() < 1)
				return null;

			if(rs.next())
				systemPrinters.add(rs.getString(1));
			rs.close();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return systemPrinters;
	}

	/**
	 * Wyszukuje nazwy drukarek kt�re ju� s� zapisane
	 * @return
	 */
	public static List<String> definedUserPrinters()
	{
		List<String> systemPrinters = Lists.newArrayList();
		PreparedStatement ps = null;
		try
		{
			ps = DSApi.context().prepareStatement("select userPrinterName from ds_printers");
			ResultSet rs = ps.executeQuery();

			if(rs.getFetchSize() < 1)
				return null;

            while(rs.next())
				systemPrinters.add(rs.getString(1));
			rs.close();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return systemPrinters;
	}

	/**
	 * Pobiera nazw� drukarki kt�ra jest aktualnie u�ywana do drukowania
	 *
	 * @return
	 */
	public static String getDefaultUserPrinterName()
	{
		return getPrintersNode().get("userPrinterName", null);
	}

	/**
	 * Pobiera node z drukark� kt�ra jest aktualnie u�ywana do drukowania
	 * @return
	 */
	public static Preferences getPrintersNode()
	{
		return DSApi.context().userPreferences().node(GlobalPreferences.NODE_NEW_BARCODE_PRINTER);
	}

	public static boolean isPrinterDefinedForUser()
	{
		return StringUtils.isNotEmpty(getDefaultUserPrinterName());
	}

	/**
	 * Usuwa value z prefs�w.
	 * U�ywane do czyszczenia ustawie� u�ytkownik�w po usuni�ciu drukarki.
	 */
	public void deletePrefsFromDatabase()
	{
		if (id == null)
			return;
		try
		{
			PreparedStatement pst;
			pst = DSApi.context().prepareStatement("DELETE FROM DS_PREFS_VALUE WHERE node_id IN(SELECT node_id FROM DS_PREFS_VALUE where PREF_KEY like ? AND PREF_VALUE like ?)");
			pst.setString(1, "printerId");
			pst.setString(2, String.valueOf(id));
			pst.executeUpdate();
		} catch (SQLException e)
		{
			log.error(e.getMessage(), e);
		} catch(EdmException exp)
		{
			log.error(exp.getMessage(), exp);
		}
	}

	/**
	 * Zmienia ustawienia dla wszystkich u�ytkownik�w po updacie drukarek
	 * @param prefsToUpdate
	 */
	public void updatePrefsForDatabase(Map<String, Object> prefsToUpdate)
	{
		if (prefsToUpdate.size() == 0)
			return;
		for (Map.Entry<String, Object> pref : prefsToUpdate.entrySet())
		{
			if (id == null)
				return;
			try
			{
				PreparedStatement pst;
				pst = DSApi.context().prepareStatement("UPDATE DS_PREFS_VALUE " +
						"SET PREF_VALUE = ? " +
						"WHERE PREF_KEY like ? AND " +
						"node_id IN(SELECT node_id FROM DS_PREFS_VALUE where PREF_KEY like ? AND PREF_VALUE like ?)");
				if (pref.getValue() == null)
					pst.setString(1, "");
				else
					pst.setString(1, pref.getValue().toString());

				pst.setString(2, pref.getKey());
				pst.setString(3, "printerId");
				pst.setString(4, String.valueOf(id));
				pst.executeUpdate();
			} catch (SQLException e)
			{
				log.error(e.getMessage(), e);
			} catch(EdmException exp)
			{
				log.error(exp.getMessage(), exp);
			}
		}
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ZebraPrinter that = (ZebraPrinter) o;

		if (printerName != null ? !printerName.equals(that.printerName) : that.printerName != null) return false;
		if (userPrinterName != null ? !userPrinterName.equals(that.userPrinterName) : that.userPrinterName != null)
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = userPrinterName != null ? userPrinterName.hashCode() : 0;
		result = 31 * result + (printerName != null ? printerName.hashCode() : 0);
		return result;
	}

	public Boolean getUsePrinter()
	{
		return usePrinter;
	}

	public void setUsePrinter(Boolean usePrinter)
	{
		this.usePrinter = usePrinter;
	}

	public String getPrinterName()
	{
		return printerName;
	}

	public void setPrinterName(String printerName)
	{
		this.printerName = printerName;
	}

	public String getFirstLine()
	{
		return firstLine;
	}

	public void setFirstLine(String firstLine)
	{
		this.firstLine = firstLine;
	}

	public String getSecondLine()
	{
		return secondLine;
	}

	public void setSecondLine(String secondLine)
	{
		this.secondLine = secondLine;
	}

	public String getBarcodeType()
	{
		return barcodeType;
	}

	public void setBarcodeType(String barcodeType)
	{
		this.barcodeType = barcodeType;
	}

	public String getBarcodeValue()
	{
		return barcodeValue;
	}

	public void setBarcodeValue(String barcodeValue)
	{
		this.barcodeValue = barcodeValue;
	}

	public Map<String, String> getFields()
	{
		return fields;
	}

	public void setFields(Map<String, String> fields)
	{
		this.fields = fields;
	}

	public List<String> getDefinedFields()
	{
		return definedFields;
	}

	public void setDefinedFields(List<String> definedFields)
	{
		this.definedFields = definedFields;
	}

	public String getLanguage() 
	{
		return language;
	}

	public void setLanguage(String language) 
	{
		this.language = language;
	}

	public String getUserPrinterName()
	{
		return userPrinterName;
	}

	public void setUserPrinterName(String userPrinterName)
	{
		this.userPrinterName = userPrinterName;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}
}
