package pl.compan.docusafe.service.zebra;

import java.util.Properties;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa generuj�ca komendy j�zyka EPL dla drukarki Zebra
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class ZebraDriverEPL extends AbstractZebraDriver 
{	
	private static final String ZEBRA_EPL_FONT = "zebra.epl.font";
	private static final String ZEBRA_EPL_HORIZONTAL_START = "zebra.epl.horizontalStart";
	private static final String ZEBRA_EPL_VERTICAL_BARCODE_START = "zebra.epl.verticalBarcodeStart";
	private static final String ZEBRA_EPL_VERTICAL_FLINE_START = "zebra.epl.verticalFlineStart";
	private static final String ZEBRA_EPL_VERTICAL_SLINE_START = "zebra.epl.verticalSlineStart";
	private static final String ZEBRA_EPL_LABEL_HEIGHT = "zebra.epl.labelHeight";
	private static final String ZEBRA_EPL_LABEL_WIDTH = "zebra.epl.labelWidth";
	private static final String ZEBRA_EPL_LABEL_GAPS = "zebra.epl.labelGaps";
    private static Logger log = LoggerFactory.getLogger(ZebraDriverEPL.class);
	
	public ZebraDriverEPL(Properties properties) 
	{
		super(properties);
	}

	public String getBarcodeDefString() 
	{
		String font =  Docusafe.getAdditionProperty(ZEBRA_EPL_FONT);
		String horizontalStart = Docusafe.getAdditionProperty(ZEBRA_EPL_HORIZONTAL_START);
		String verticalBarcodeStart = Docusafe.getAdditionProperty(ZEBRA_EPL_VERTICAL_BARCODE_START);
		String verticalFlineStart = Docusafe.getAdditionProperty(ZEBRA_EPL_VERTICAL_FLINE_START);
		String verticalSlineStart = Docusafe.getAdditionProperty(ZEBRA_EPL_VERTICAL_SLINE_START);
		String labelHeight  = Docusafe.getAdditionProperty(ZEBRA_EPL_LABEL_HEIGHT);
		String labelWidth = Docusafe.getAdditionProperty(ZEBRA_EPL_LABEL_WIDTH);
		String labelGaps = Docusafe.getAdditionProperty(ZEBRA_EPL_LABEL_GAPS);
		StringBuilder sb = new StringBuilder();
		//czyszczenie bufora
		sb.append("\nN\n");
		//szerokosc etykiety
		sb.append("q"+labelWidth+"\n");
		//wysokosc etykiety plus odstep pomiedzy etykietami
		sb.append("Q"+labelHeight+",30\n");
		sb.append(getBarcode(getBarcodeType(),horizontalStart,verticalBarcodeStart));
		// pobranie typu czcionki

		if(StringUtils.isNotBlank(getFirstLine()))
			sb.append("A").append(horizontalStart).append(","+verticalFlineStart+",0,").append(font).append(",1,1,N,\"").append(getFirstLine()).append("\"\n");
		if(StringUtils.isNotBlank(getSecondLine()))	
			sb.append("A").append(horizontalStart).append(","+verticalSlineStart+",0,").append(font).append(",1,1,N,\"").append(getSecondLine()).append("\"\n");

		// fields under barcode
		int step = 0;
		for(Entry<String, String> e : getFields().entrySet())
		{
			sb.append("A").append(horizontalStart).append(",").append(getFieldsPositionStart() + getFieldsPositionStep() * step).append(",0,").append(font).append(",1,1,N,\"")
			.append(e.getKey()).append(": ").append(e.getValue()).append("\"\n");
			step++;
		}
		// command to print 1 copy
		sb.append("P1,1\n");
		//podanie etykiety do oderwania, wysuwa etykiete do przodu a przed nastepnym drukiem cofa tasme
		sb.append("JF");
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	private String getBarcode(String cn,String horizontalStart, String verticalStart)
	{
		String barcode = null;
		
		if(cn == null || cn.equals("B1") || cn.equals("B3")) // CODE 39
		{
			barcode = "B"+horizontalStart+","+verticalStart+",0,3,2,5," + getBarcodeHeight() + ",B,\"" + getBarcodeValue() + "\"\n";
		}
		else if(cn.equals("BA")) // CODE 93
		{
			barcode = "B"+horizontalStart+","+verticalStart+",0,9,2,5," + getBarcodeHeight() + ",B,\"" + getBarcodeValue() + "\"\n";
		}
		else if(cn.equals("BC")) // CODE 128
		{
			barcode = "B"+horizontalStart+","+verticalStart+",0,1,2,6," + getBarcodeHeight() + ",B,\"" + getBarcodeValue() + "\"\n";
		}
		else if (cn.equals("B2")) // Interleaved 2 of 5
		{
			barcode = "B"+horizontalStart+","+verticalStart+",0,2,2,6," + getBarcodeHeight() + ",B,\"" + getBarcodeValue() + "\"\n";
		}
		else if (cn.equals("B8")) // EAN-8
		{
			barcode = "B"+horizontalStart+","+verticalStart+",0,E80,2,4," + getBarcodeHeight() + ",B,\"" + getBarcodeValue() + "\"\n";
		}
        else if (cn.equals("B8")) // EAN-8
        {
            barcode = "B"+horizontalStart+","+verticalStart+",0,E80,2,4," + getBarcodeHeight() + ",B,\"" + getBarcodeValue() + "\"\n";
        }
        log.error("*********TEST EPL PRINT:" + barcode);
		return barcode;
	}
}
