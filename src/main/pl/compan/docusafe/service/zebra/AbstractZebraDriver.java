package pl.compan.docusafe.service.zebra;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Klasa abstrakcyjna ustawiająca parametry dla drukowanego barcodu
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public abstract class AbstractZebraDriver implements ZebraDriver
{	
	private Integer fieldsPositionStart;
	private Integer fieldsPositionStep;
	private Integer barcodeHeight;
	private String firstLine;
	private String secondLine;
	private String barcodeType;
	private String barcodeValue;
	private Map<String, String> fields = new HashMap<String, String>();
	
	
	
	public AbstractZebraDriver(Properties props) 
	{
		fieldsPositionStart = (Integer) props.get("fieldsPositionStart");
		fieldsPositionStep = (Integer) props.get("fieldsPositionStep");
		barcodeHeight = (Integer) props.get("barcodeHeight");
		firstLine = props.getProperty("firstLine");
		secondLine = props.getProperty("secondLine", "");
		barcodeType = props.getProperty("barcodeType", null);
		barcodeValue = props.getProperty("barcodeValue");
		fields = (Map<String, String>) props.get("fields");
	}

	public Integer getFieldsPositionStart() 
	{
		return fieldsPositionStart;
	}

	public void setFieldsPositionStart(Integer fieldsPositionStart) 
	{
		this.fieldsPositionStart = fieldsPositionStart;
	}

	public Integer getFieldsPositionStep() 
	{
		return fieldsPositionStep;
	}

	public void setFieldsPositionStep(Integer fieldsPositionStep) 
	{
		this.fieldsPositionStep = fieldsPositionStep;
	}

	public Integer getBarcodeHeight() 
	{
		return barcodeHeight;
	}

	public void setBarcodeHeight(Integer barcodeHeight) 
	{
		this.barcodeHeight = barcodeHeight;
	}

	public String getFirstLine() 
	{
		return firstLine;
	}

	public void setFirstLine(String firstLine)
	{
		this.firstLine = firstLine;
	}

	public String getSecondLine() 
	{
		return secondLine;
	}

	public void setSecondLine(String secondLine) 
	{
		this.secondLine = secondLine;
	}

	public String getBarcodeType() 
	{
		return barcodeType;
	}

	public void setBarcodeType(String barcodeType) 
	{
		this.barcodeType = barcodeType;
	}

	public Map<String, String> getFields() 
	{
		return fields;
	}

	public void setFields(Map<String, String> fields) 
	{
		this.fields = fields;
	}

	public String getBarcodeValue() 
	{
		return barcodeValue;
	}

	public void setBarcodeValue(String barcodeValue) 
	{
		this.barcodeValue = barcodeValue;
	}
}
