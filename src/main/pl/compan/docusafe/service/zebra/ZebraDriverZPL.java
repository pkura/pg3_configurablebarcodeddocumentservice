package pl.compan.docusafe.service.zebra;

import java.util.Properties;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;

/**
 * Klasa generuj�ca komendy w j�zyku ZPL dla drukarki Zebra
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class ZebraDriverZPL extends AbstractZebraDriver
{
	private static final String ZEBRA_ZPL_FONT = "zebra.zpl.font";
	private static final String ZEBRA_ZPL_HORIZONTAL_START = "zebra.zpl.horizontalStart";
	private static final String ZEBRA_ZPL_VERTICAL_BARCODE_START = "zebra.zpl.verticalBarcodeStart";
	private static final String ZEBRA_ZPL_VERTICAL_FLINE_START = "zebra.zpl.verticalFlineStart";
	private static final String ZEBRA_ZPL_VERTICAL_SLINE_START = "zebra.zpl.verticalSlineStart";
	private static final String ZEBRA_ZPL_LABEL_HEIGHT = "zebra.zpl.labelHeight";
	private static final String ZEBRA_ZPL_LABEL_WIDTH = "zebra.zpl.labelWidth";
	private static final String ZEBRA_ZPL_LABEL_GAPS = "zebra.zpl.labelGaps";
	private static final String ZEBRA_ZPL_FIELDS_HORIZONTAL_START = "zebra.zpl.fields.horizontalStart";
	private static final String ZEBRA_ZPL_FIELDS_FONT_SIZE = "zebra.fields.fontSize";
    private static final Logger log = LoggerFactory.getLogger(ZebraDriverZPL.class);

    public ZebraDriverZPL(Properties properties)
	{
		super(properties);
	}

	public String getBarcodeDefString() 
	{
		String font =  Docusafe.getAdditionProperty(ZEBRA_ZPL_FONT);
		String horizontalStart = Docusafe.getAdditionProperty(ZEBRA_ZPL_HORIZONTAL_START);
		String horizontalFieldsStart = Docusafe.getAdditionProperty(ZEBRA_ZPL_FIELDS_HORIZONTAL_START);
		String verticalBarcodeStart = Docusafe.getAdditionProperty(ZEBRA_ZPL_VERTICAL_BARCODE_START);
		String verticalFlineStart = Docusafe.getAdditionProperty(ZEBRA_ZPL_VERTICAL_FLINE_START);
		String verticalSlineStart = Docusafe.getAdditionProperty(ZEBRA_ZPL_VERTICAL_SLINE_START);
		String labelHeight  = Docusafe.getAdditionProperty(ZEBRA_ZPL_LABEL_HEIGHT);
		String labelWidth = Docusafe.getAdditionProperty(ZEBRA_ZPL_LABEL_WIDTH);
		String labelGaps = Docusafe.getAdditionProperty(ZEBRA_ZPL_LABEL_GAPS);
		String fontSize = Docusafe.getAdditionProperty(ZEBRA_ZPL_FIELDS_FONT_SIZE);
		StringBuilder sb = new StringBuilder();

        if(getBarcodeType().equals("QR")) return getQRDefString();


		sb.append("^XA"); 
		// pobranie typu czcionki
		
		// ustawienie typu czcionki
		sb.append("^FO").append(horizontalStart).append(","+verticalFlineStart+"^").append(font).append("N,25,8^FD").append(getFirstLine()).append("^FS").append("\n");
		sb.append("^FO").append(horizontalStart).append(","+verticalSlineStart+"^").append(font).append("N,25,8^FD").append(getSecondLine()).append("^FS").append("\n"); 
		
		sb.append("^FO").append(horizontalStart).append(","+verticalBarcodeStart+"").append(getBarcode(getBarcodeType())).append("^FD").append(getBarcodeValue()).append("^FS");
		int step = 0;
		for(Entry<String, String> e : getFields().entrySet())
		{
			sb.append("^FO").append(horizontalFieldsStart).append(",").append(getFieldsPositionStart() + getFieldsPositionStep() * step).append("^").append(font).append("N,"+fontSize+"^FD")
			.append(e.getKey()).append(": ").append(e.getValue()).append("^FS").append("\n");
			step++;
		}
		sb.append("^XZ");
		System.out.println(sb.toString());
        log.error("*************ZEBRA TEST QR: "+sb.toString());
		return sb.toString();
	}
	
	private String getBarcode(String cn)
	{
		String answer = "^B3N,N," + getBarcodeHeight() + ",Y,N";
		if(cn==null)
			return answer;
		
		if(cn.equals("B1") || cn.equals("B3"))
		{
			answer = "^"+cn+"N,N," + getBarcodeHeight() + ",Y,N";
		}
		else if(cn.equals("B2") || cn.equals("BA") || cn.equals("BC"))
		{
			answer = "^"+cn+"N," + getBarcodeHeight() + ",Y,N,N";
		}
		else if(cn.equals("B8"))
		{
			answer = "^"+cn+"N," + getBarcodeHeight() + ",Y,N";
		}
        else if(cn.equals("QR"))
        {

            String fontSize = Docusafe.getAdditionProperty("zebra.fields.qr.fontSize");
            answer = "^BQN,2," + fontSize;
        }
		return answer;
	}

    private String getQRDefString()
    {
        String font =  Docusafe.getAdditionProperty(ZEBRA_ZPL_FONT);
        String qrHorizontalStart = Docusafe.getAdditionProperty("zebra.fields.qr.horizontal.start");
        String qrVerticalStart = Docusafe.getAdditionProperty("zebra.fields.qr.vertical.start");
        String labelHorizontalStart = Docusafe.getAdditionProperty("zebra.fields.qr.labelHorizontal.start");
        String labelVerticalStart = Docusafe.getAdditionProperty("zebra.fields.qr.labelVertical.start");

        StringBuilder sb = new StringBuilder();
        sb.append("^XA");
        sb.append("^CF0,20,30^FO"+labelHorizontalStart+","+labelVerticalStart);
        sb.append("^FD   ").append(getBarcodeValue()).append("^FS");
        sb.append(getBarcode("QR")).append("^FO").append(qrHorizontalStart).append(",").append(qrVerticalStart);
        sb.append("^FD   ").append(getBarcodeValue()).append("^FS");
        sb.append("^XZ");
        System.out.println(sb.toString());
        String str = sb.toString();
        log.error("*************ZEBRA TEST QR: "+sb.toString());
        return sb.toString();
    }
}
