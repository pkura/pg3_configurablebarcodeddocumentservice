package pl.compan.docusafe.service.zebra;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.prefs.Preferences;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.BarcodeUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;

public class ZebraPrinterManager 
{
	 private final static StringManager sm = 
	        GlobalPreferences.loadPropertiesFile(ZebraPrinterManager.class.getPackage().getName(),null);
	 
	public void printLabelFromDocument(InOfficeDocument doc) throws EdmException
	{
		ZebraPrinter zb;
		if (!ZebraPrinter.getDefaultUserPrinterName().equals(""))
		{
			Preferences node = ZebraPrinter.getPrintersNode();
			zb = new ZebraPrinter(node);
		}
		else
			throw new EdmException("Brak zdefiniowanej drukarki");
		zb.init();
		if(zb.getUsePrinter())
		{
			Map<String,String> fields = new LinkedHashMap<String,String>();
			fields.put(toLatina(sm.getString("NumerPisma")),toLatina(TextUtils.nullSafeObject(doc.getOfficeNumber())));
			fields.put(toLatina(sm.getString("DataPrzyjecia")),toLatina(DateUtils.formatCommonDate(doc.getDocumentDate())));
			fields.put(toLatina(sm.getString("Przyjmujacy")),toLatina(DSUser.findByUsername(doc.getAuthor()).asFirstnameLastname()));
			fields.put(toLatina(sm.getString("Login")),doc.getTrackingNumber());
			fields.put(toLatina(sm.getString("Haslo")),doc.getTrackingPassword());
			zb.setBarcodeValue(doc.getBarcode() != null ? doc.getBarcode() : BarcodeUtils.getIdBarcode(doc));
			zb.setFields(fields);
	    	zb.print();
		}
	} 
	/**
	 * Drukuje etykete, pobiera numer barcode oraz w mape linni drukowanych pod kodem, mapa zawiera key= nazwa value = wartos�
	 * Nazwa i warto�� s� drukowane (NAZWA : Warto��) 
	 * @param barcode
	 * @param fields
	 * @throws EdmException
	 */
	public void printLabel(String barcode,Map<String,String> fields ) throws EdmException
	{
		Preferences node = ZebraPrinter.getPrintersNode();
		ZebraPrinter zb = new ZebraPrinter(node);
		zb.init();
		if(zb.getUsePrinter())
		{
			zb.setBarcodeValue(barcode);
			zb.setFields(fields);
	    	zb.print();
		}
	} 
	
	
	private String toLatina(String field)
	{
		if(!AvailabilityManager.isAvailable("print.barcode.latinize"))
			return field;
		return TextUtils.latinize(field);
	}
}
