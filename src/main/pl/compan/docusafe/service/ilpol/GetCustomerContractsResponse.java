package pl.compan.docusafe.service.ilpol;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class GetCustomerContractsResponse extends AbstractIlpolRespose{
	//dane zwrotne
	private ContractData[] contrData;
	//loger
	private final static Logger log = LoggerFactory.getLogger(GetCustomerContractsResponse.class);
	public GetCustomerContractsResponse(PagingData page, String customerId,
			String tableName) {
		super(page, customerId, tableName);
	}

	
	public ContractData[] getContrData() {
		return contrData;
	}
	public void setContrData(ContractData[] contrData) {
		this.contrData = contrData;
	}
	public PageInfo getpInfo() {
		return pInfo;
	}
	public void setpInfo(PageInfo pInfo) {
		this.pInfo = pInfo;
	}
	/**
	 * Pobranie rekordow i utworzenie z nich obiektow
	 */
	public void setDataFromDB(){
		contrData = new ContractData[this.numberOfObject()];
		DataBaseConnection dbc = new DataBaseConnection();
		try {
			ps = dbc.getConnection().prepareStatement(this.sqlCreate());
			rs = ps.executeQuery();
			int i = 0;
			while(rs.next()){
				contrData[i] = new ContractData(rs.getString("contractnumber"), rs.getDouble("contractvalue"), rs.getString("contractsubject"),
						rs.getDate("contractinitialdate"), rs.getDate("contractfialdate"),
						rs.getString("status"), rs.getString("contracttype"));
				i++;
			}
			
			
		} catch (SQLException e) {
			log.error(e.getMessage());
		}
		finally{
			dbc.closeConn();
		}
	}
	

}
