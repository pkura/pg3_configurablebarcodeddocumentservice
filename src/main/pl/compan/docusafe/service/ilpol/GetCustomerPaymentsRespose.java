package pl.compan.docusafe.service.ilpol;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class GetCustomerPaymentsRespose  extends AbstractIlpolRespose {
	private PaymentData[] paymentData;
	
	//loger
	private final static Logger log = LoggerFactory.getLogger(GetCustomerPaymentsRespose.class);
	public GetCustomerPaymentsRespose(PagingData page, String customerId,
			String tableName) {
		super(page, customerId, tableName);
	}
	public PaymentData[] getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(PaymentData[] paymentData) {
		this.paymentData = paymentData;
	}

	public PageInfo getpInfo() {
		return pInfo;
	}

	public void setpInfo(PageInfo pInfo) {
		this.pInfo = pInfo;
	}
	/**
	 * Pobranie rekordow i utworzenie z nich obiektow
	 */
	public void setDataFromDB(){
		//okreslenie wielkosci tablicy
		paymentData = new PaymentData[this.numberOfObject()];
		DataBaseConnection dbc = new DataBaseConnection();
		try {
			ps = dbc.getConnection().prepareStatement(this.sqlCreate());
			rs = ps.executeQuery();
			int i = 0;
			while(rs.next()){
				paymentData[i] = new PaymentData(rs.getString("ulnNumber"), rs.getDouble("plnValue"), rs.getDouble("eurValue"),
						rs.getDouble("plnGrowingValue"), rs.getDouble("eurGrowingValue"), rs.getDate("documentDate"),
						rs.getDate("paymentDate"), rs.getString("documentNumber"), rs.getString("description"));
				
				i++;
			}
			
			
		} catch (SQLException e) {
			log.error(e.getMessage());
		}
		finally{
			dbc.closeConn();
		}
	}
	
}
