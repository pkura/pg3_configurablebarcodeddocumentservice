package pl.compan.docusafe.service.ilpol;

import java.io.Serializable;
/**
 * Klasa zwracajaca aktualna strone oraz ilosc rekordow dla zapytania wykonanego bez paginacji
 * @author user
 *
 */
public class PageInfo{
	private int pageNumber;
	private int totalRows;
	
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getTotalRows() {
		return totalRows;
	}
	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}
	
	
	
}
