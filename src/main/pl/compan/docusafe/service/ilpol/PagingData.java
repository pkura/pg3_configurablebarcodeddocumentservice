package pl.compan.docusafe.service.ilpol;
/**
 * Klasa przejazujaca parametry paginacji dla odbowiedzi WS
 * @author user
 *
 */
public class PagingData {
	private int PageNumber;
	private int ItemsOnPage;
	private String SortBy;
	private boolean SortAsc;
	
	public int getPageNumber() {
		return PageNumber;
	}
	public void setPageNumber(int pageNumber) {
		PageNumber = pageNumber;
	}
	public int getItemsOnPage() {
		return ItemsOnPage;
	}
	public void setItemsOnPage(int itemsOnPage) {
		ItemsOnPage = itemsOnPage;
	}
	public String getSortBy() {
		return SortBy;
	}
	public void setSortBy(String sortBy) {
		SortBy = sortBy;
	}
	public boolean isSortAsc() {
		return SortAsc;
	}
	public void setSortAsc(boolean sortAsc) {
		SortAsc = sortAsc;
	}
	
	public String sort(){
		String sort;
		if(SortAsc == false){
			sort = "order by " + SortBy + " desc";
		} else{
			sort = "order by " + SortBy;
		}
		return sort;
	}
	@Override
	public String toString() {
		return "PagingData [PageNumber=" + PageNumber + ", ItemsOnPage="
				+ ItemsOnPage + ", SortBy=" + SortBy + ", SortAsc=" + SortAsc
				+ "]";
	}
	
}
