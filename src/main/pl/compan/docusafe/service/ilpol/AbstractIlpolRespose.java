package pl.compan.docusafe.service.ilpol;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.wssk.WsskService;
/**
 * Klasa abstrakcyjna trzymajca wsp�lne nethody wykorzystywane przez klasy potomne
 * @author lukasz chmielewski
 *
 */
public abstract class AbstractIlpolRespose {
	//dane zwracane
	protected PageInfo pInfo;
	// dane do zapytan
	protected String customerId;
	protected PagingData page;
	protected String tableName;
	// do zapyta�
	protected PreparedStatement ps = null;
	protected ResultSet rs = null;
	protected String query;
	//loger
	private final static Logger log = LoggerFactory.getLogger(AbstractIlpolRespose.class);
	public AbstractIlpolRespose(String customerId) {
		this.customerId = customerId;
	}

	public AbstractIlpolRespose(String customerId, String tableName) {
		this.customerId = customerId;
		this.tableName = tableName;
	}

	public AbstractIlpolRespose(PagingData page, String customerId,
			String tableName) {
		pInfo = new PageInfo();
		this.page = page;
		this.customerId = customerId;
		this.tableName = tableName;
	}
	public abstract void setDataFromDB();
	/**
	 * Pobranie ilosci rekord�w
	 */
	public void setPageInfoFromQuery(){
		pInfo.setPageNumber(page.getPageNumber());
		
		DataBaseConnection dbc = new DataBaseConnection();
		String sql = "select count(*) as rowcount  from " + tableName + 
		" where nip = '" + customerId + "'";
		try {
			ps = dbc.getConnection().prepareStatement(sql);
			rs = ps.executeQuery();
			rs.next();
			pInfo.setTotalRows(rs.getInt("rowcount"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		finally{
			dbc.closeConn();
		}
	}
	/**
	 * Tworzenie zapytania na tak z paginowaniem jak i bez
	 * @return
	 */
	public String sqlCreate(){
		if(page instanceof PagingData){
			int startRow = page.getPageNumber()*page.getItemsOnPage() - page.getItemsOnPage();
			int endRow = page.getPageNumber()*page.getItemsOnPage();
			query = "select * from " +
					"(select a.*,rownum as rnum from " +
					"(select * from " + tableName + " where nip = '" + customerId + "' " + page.sort() +")" +
					" a where rownum <= "+ endRow +")" +
					" where rnum > " + startRow;
		}
		else{
			query = "select * from " + tableName + 
					" where nip = '" + customerId + "'";
		}
		return query;
	}
	/**
	 * Zwracanie ilosci obiekt�w kt�re zostana zwrocone przez zapytanie
	 *
	 * @return
	 */
	public int numberOfObject(){
		if((page.getItemsOnPage()*page.getPageNumber()-page.getItemsOnPage()) >= pInfo.getTotalRows()){
			return 1;
		}
		else if((page.getItemsOnPage()*page.getPageNumber()) > pInfo.getTotalRows()){
			return pInfo.getTotalRows() - (page.getItemsOnPage() * (page.getPageNumber()-1));
		}
		else if(pInfo.getTotalRows() < page.getItemsOnPage()){
			return pInfo.getTotalRows();
		} 
		return page.getItemsOnPage();
	}
	/**
	 * Pobranie listy kolumn
	 * @param tableNameSchema
	 * @return
	 */
	public List<String> columnListCreate(String tableNameSchema) {
		List<String> columnList = new ArrayList<String>();
		String tableNameNoSchema = tableNameSchema.substring(tableNameSchema
				.indexOf(".") + 1);
		String queryCol = "select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME= '"
				+ tableNameNoSchema + "'";
		DataBaseConnection dbc = new DataBaseConnection();
		try {
			ps = dbc.getConnection().prepareStatement(queryCol);
			rs = ps.executeQuery();
			while (rs.next()) {
				columnList.add(rs.getString("COLUMN_NAME"));
			}
		} catch (SQLException e) {
			log.error(e.getMessage());
		} finally {
			dbc.closeConn();
		}
		return columnList;
	}
/**
 * Utworzenie obiekt�w typu DataItem z kluczem jako nazwa kolumny
 * @param tableName
 * @return
 */
	public DataItem[] keyValue(String tableName) {
		List<String> columnList = columnListCreate(tableName);
		DataItem[] di = new DataItem[columnList.size()];
		String query = "select * from " + tableName + " where nip = '"
				+ customerId + "'";
		DataBaseConnection dbc = new DataBaseConnection();
		try {
			ps = dbc.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				int i = 0;
				for (String column : columnList) {
					di[i] = new DataItem(column, rs.getString(column));
					i++;
				}
			} else{
				return null;
			}
		} catch (SQLException e) {
			log.error(e.getMessage());
		} finally {
			dbc.closeConn();
		}
		return di;
	}
	public Map<String,Boolean> EdiTableListGet() {
		List<String> columnList = columnListCreate("SYSDBA.ILP_V_GETCUSTOMERDETAILS_EDIT");
		Map<String,Boolean> etl = new HashMap<String,Boolean>();
		String query = "select * from SYSDBA.ILP_V_GETCUSTOMERDETAILS_EDIT";
		DataBaseConnection dbc = new DataBaseConnection();
		try {
			ps = dbc.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				for (String column : columnList) {
					if(rs.getString(column).equalsIgnoreCase("F"))
							etl.put(column, false);
					else
						etl.put(column, true);
				}
			} else{
				return null;
			}
		} catch (SQLException e) {
			log.error(e.getMessage());
		} finally {
			dbc.closeConn();
		}
		//return di;
		return etl;
	}
	
}
