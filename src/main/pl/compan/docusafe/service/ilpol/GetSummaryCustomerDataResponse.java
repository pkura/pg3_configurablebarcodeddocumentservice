package pl.compan.docusafe.service.ilpol;

public class GetSummaryCustomerDataResponse extends AbstractIlpolRespose{

	
	private DataItem[] CustomerData;
	private DataItem[] PaymentsData;
	private DataItem[] CokData;

	public GetSummaryCustomerDataResponse(String customerId) {
		super(customerId);
	}
	public DataItem[] getCustomerData() {
		return CustomerData;
	}

	public void setCustomerData(DataItem[] customerData) {
		CustomerData = customerData;
	}

	public DataItem[] getPaymentsData() {
		return PaymentsData;
	}

	public void setPaymentsData(DataItem[] paymentsData) {
		PaymentsData = paymentsData;
	}

	public DataItem[] getCokData() {
		return CokData;
	}

	public void setCokData(DataItem[] cokData) {
		CokData = cokData;
	}
	@Override
	public void setDataFromDB() {
		// przy pojedy�czym rekordzie nie u�ywana
		
	}

	
}
