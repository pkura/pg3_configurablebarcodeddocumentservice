package pl.compan.docusafe.service.ilpol;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
/**
 * Klasa inicjujca polaczenie z baza
 * @author user
 *
 */
public class DataBaseConnection {

	private Context initCtx;
	private DataSource dataSource;
	private Connection connection;
	private final String tomcatResource  = "jdbc/portal";
	
	public DataBaseConnection() {
		try {
			initCtx = new InitialContext();
			dataSource = (DataSource) initCtx.lookup("java:comp/env/" + tomcatResource);
	    	connection = dataSource.getConnection();
	    	
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Context getInitCtx() {
		return initCtx;
	}

	public void setInitCtx(Context initCtx) {
		this.initCtx = initCtx;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	public void closeConn(){
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
