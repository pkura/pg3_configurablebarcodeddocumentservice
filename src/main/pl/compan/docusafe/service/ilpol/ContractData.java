package pl.compan.docusafe.service.ilpol;

import java.io.Serializable;
import java.util.Date;
/**
 * Klasa do zapytania WSDL GetCustomerContract
 * @author user
 *
 */
public class ContractData{
	private String contractNumber;
	private double contractValue;
	private String contractSubject;
	private Date contractInitialDate;
	private Date contractFinalDate;
	private String status;
	private String contractType;
	
	
	public ContractData(){
		
	}


	public ContractData(String contractNumber, double contractValue,
			String contractSubject, Date contractInitialDate,
			Date contractFinalDate, String status, String contractType) {
		this.contractNumber = contractNumber;
		this.contractValue = contractValue;
		this.contractSubject = contractSubject;
		this.contractInitialDate = contractInitialDate;
		this.contractFinalDate = contractFinalDate;
		this.status = status;
		this.contractType = contractType;
	}


	public String getContractNumber() {
		return contractNumber;
	}


	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}


	public double getContractValue() {
		return contractValue;
	}


	public void setContractValue(double contractValue) {
		this.contractValue = contractValue;
	}


	public String getContractSubject() {
		return contractSubject;
	}


	public void setContractSubject(String contractSubject) {
		this.contractSubject = contractSubject;
	}


	public Date getContractInitialDate() {
		return contractInitialDate;
	}


	public void setContractInitialDate(Date contractInitialDate) {
		this.contractInitialDate = contractInitialDate;
	}


	public Date getContractFinalDate() {
		return contractFinalDate;
	}


	public void setContractFinalDate(Date contractFinalDate) {
		this.contractFinalDate = contractFinalDate;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getContractType() {
		return contractType;
	}


	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	
	
}
