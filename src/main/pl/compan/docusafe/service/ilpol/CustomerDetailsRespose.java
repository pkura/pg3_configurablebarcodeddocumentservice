package pl.compan.docusafe.service.ilpol;

import java.io.Serializable;
import java.util.Map;

public class CustomerDetailsRespose  extends AbstractIlpolRespose{
	
	public CustomerDetailsRespose(String customerId, String tableName) {
		super(customerId, tableName);
	}

	private EditableDataItems[] edi;

	public EditableDataItems[] getEdi() {
		return edi;
	}

	public void setEdi(EditableDataItems[] edi) {
		this.edi = edi;
	}

	@Override
	public void setDataFromDB() {
		Map<String, Boolean> etl = this.EdiTableListGet();
		DataItem[] di = this.keyValue(tableName);
		if (di == null) {
			this.edi = null;
		} else {
			EditableDataItems[] edi = new EditableDataItems[etl.size()];
			int i = 0;
			for (DataItem dataItem : di) {
				if (etl.containsKey(dataItem.getKey())) {
					edi[i] = new EditableDataItems(etl.get(dataItem.getKey()),
							dataItem);
					i++;
				}
			}
			this.edi = edi;
		}
	}
}
