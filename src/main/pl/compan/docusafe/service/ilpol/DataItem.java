package pl.compan.docusafe.service.ilpol;

import java.io.Serializable;
/**
 * Klasa sluzaca do przekazywania danych przez WS jako klucz->wartosc 
 * @author user
 *
 */
public class DataItem{
	private String key;
	private String value;
	public DataItem(String key, String value) {
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}