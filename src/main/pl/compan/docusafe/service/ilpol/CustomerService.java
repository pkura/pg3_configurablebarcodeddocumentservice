package pl.compan.docusafe.service.ilpol;

import pl.compan.docusafe.core.DSApi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.hibernate.Query;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.admin.dictionaries.ExternalDictionariesAction;
/**
 * G�wna klasa WebServicu
 * @author user
 *
 */
public class CustomerService {
	private final static Logger log = LoggerFactory.getLogger(CustomerService.class);
	public GetSummaryCustomerDataResponse GetSummaryCustomerData(String CustomerId){
		GetSummaryCustomerDataResponse gscdr = new GetSummaryCustomerDataResponse(CustomerId);
		gscdr.setCustomerData(gscdr.keyValue("SYSDBA.ILP_V_GSCD_CUSTOMERDATA"));
		gscdr.setPaymentsData(gscdr.keyValue("SYSDBA.ILP_V_GSCD_PAYMENTSDATA"));
		gscdr.setCokData(gscdr.keyValue("DOCUSAFE.ILP_V_GSCD_COKDATA"));
		if(gscdr.getCokData() == null && gscdr.getCustomerData() == null && gscdr.getPaymentsData() == null){
			log.info("Nie znaleziono wynik�w GetSummaryCustomerDataResponse dla NIP: " + CustomerId);
			return null;
		}
		return gscdr;
	}
	public GetCustomerContractsResponse GetCustomerContracts(String CustomerId,PagingData Page){
		GetCustomerContractsResponse customerContract = new GetCustomerContractsResponse(Page,CustomerId,"SYSDBA.ILP_V_GETCUSTOMERCONTRACTS");
		customerContract.setPageInfoFromQuery();
		if(customerContract.getpInfo().getTotalRows() == 0){
			log.info("Nie znaleziono wynik�w GetCustomerContractsResponse dla NIP: " + CustomerId + ", dla stron " + Page.toString());
			return null;
			}
		customerContract.setDataFromDB();
		return customerContract;
	}
	public GetCustomerPaymentsRespose GetCustomerPayments(String CustomerId, PagingData Page){
		GetCustomerPaymentsRespose customerPayment = new GetCustomerPaymentsRespose(Page,CustomerId,"SYSDBA.ILP_V_GETCUSTOMERPAYMENTS");
		customerPayment.setPageInfoFromQuery();
		if(customerPayment.getpInfo().getTotalRows() == 0){
			log.info("Nie znaleziono wynik�w GetCustomerPaymentsRespose dla NIP: " + CustomerId + ", dla stron " + Page.toString());
			return null;
			}
		customerPayment.setDataFromDB();
		return customerPayment;
	}
	public CustomerDetailsRespose GetCustomerDetails(String CustomerId){
		CustomerDetailsRespose customerDetails = new CustomerDetailsRespose(CustomerId, "SYSDBA.VKONTRAHENCI");
		customerDetails.setDataFromDB();
		if(customerDetails.getEdi() == null){
			log.info("Nie znaleziono wynik�w CustomerDetailsRespose dla NIP: " + CustomerId);
			return null;
		}
		return customerDetails;
	}
}
 
