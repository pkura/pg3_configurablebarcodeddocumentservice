package pl.compan.docusafe.service.ilpol;

public class EditableDataItems {
	private boolean Editable;
	private DataItem di;
	
	public EditableDataItems(boolean editable, DataItem di) {
		Editable = editable;
		this.di = di;
	}
	public boolean isEditable() {
		return Editable;
	}
	public void setEditable(boolean editable) {
		Editable = editable;
	}
	public DataItem getDi() {
		return di;
	}
	public void setDi(DataItem di) {
		this.di = di;
	}
	
}
