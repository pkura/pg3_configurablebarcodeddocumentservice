package pl.compan.docusafe.service.ilpol;

import java.io.Serializable;
import java.util.Date;
/**
 * Klasa do zapytania WSDL GetCustomerPayment
 * @author user
 *
 */
public class PaymentData implements Serializable{
	private String ulnNumber;
	private double plnValue;
	private double eurValue;
	private double plnGrowingValue;
	private double eurGrowingValue;
	private Date documentDate;
	private Date paymentDate;
	private String documentNumber;
	private String description;
	
	
	public PaymentData(String ulnNumber, double plnValue, double eurValue,
			double plnGrowingValue, double eurGrowingValue, Date documentDate,
			Date paymentDate, String documentNumber, String description) {
		super();
		this.ulnNumber = ulnNumber;
		this.plnValue = plnValue;
		this.eurValue = eurValue;
		this.plnGrowingValue = plnGrowingValue;
		this.eurGrowingValue = eurGrowingValue;
		this.documentDate = documentDate;
		this.paymentDate = paymentDate;
		this.documentNumber = documentNumber;
		this.description = description;
	}
	public String getUlnNumber() {
		return ulnNumber;
	}
	public void setUlnNumber(String ulnNumber) {
		this.ulnNumber = ulnNumber;
	}
	public double getPlnValue() {
		return plnValue;
	}
	public void setPlnValue(double plnValue) {
		this.plnValue = plnValue;
	}
	public double getEurValue() {
		return eurValue;
	}
	public void setEurValue(double eurValue) {
		this.eurValue = eurValue;
	}
	public double getPlnGrowingValue() {
		return plnGrowingValue;
	}
	public void setPlnGrowingValue(double plnGrowingValue) {
		this.plnGrowingValue = plnGrowingValue;
	}
	public double getEurGrowingValue() {
		return eurGrowingValue;
	}
	public void setEurGrowingValue(double eurGrowingValue) {
		this.eurGrowingValue = eurGrowingValue;
	}
	public Date getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
