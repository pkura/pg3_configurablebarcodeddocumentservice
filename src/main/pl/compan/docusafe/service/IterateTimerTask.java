package pl.compan.docusafe.service;

import java.util.Collection;
import java.util.Iterator;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;

/**
 * Interruptable TimerTask.
 *
 * @author Jan �wi�cki <a href="mailto:jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public abstract class IterateTimerTask<K> extends TimerTask
{
    private static final Logger log = LoggerFactory.getLogger(IterateTimerTask.class);

    /**
     * Je�li ustawione na {@code true} to iterator przy nast�pnym wywo�aniu lub wcze�niej zostanie przerwany.
     */
    protected boolean stop = false;

    /**
     * Ilo�� wierszy do przetworzenia w danej p�tli.
     */
    protected Integer count = -1;

    protected Collection<K> collection;
    protected Iterator<K> iterator;

    public static final boolean SQL_COUNT = true;
    public static final boolean SQL_ALL = false;

    /**
     * Je�li <code>true</code> b�dzie otwarty kontekst przed rozpocz�ciem iteracji
     * i zamkni�ty po zako�czeniu iteracji.
     */
    private boolean iterateInContext = false;

    /**
     * <p>Metoda wywo�ywana przed pierwsz� iteracj�.
     * Metoda powinna zainicjalizowa� {@code collection}.</p>
     */
    protected abstract void beforeRun() throws Exception;

    /**
     * <p>Metoda wywo�ywana po ostatniej iteracji.</p>
     */
    protected abstract void afterRun() throws Exception;

    /**
     * <p>
     *     Metoda wywo�ywana, gdy zostanie rzucony og�lny
     *     wyj�tek w metodzie <code>forEach</code>.
     * </p>
     * @param ex
     */
    protected void onError(Exception ex) {};

    /**
     * <p>
     *     Metoda wywo�ywana, gdy pojawi� si� wyj�tek
     *     wewn�trz p�tli w metodzie <code>forEach</code>.
     *     Kod b�dzie kontyunowa� dalsze iteracje pomimo b��du.
     * </p>
     * @param ex wyj�tek rzucony wewn�trz iteracji
     * @param i indeks iteracji
     */
    protected void onIterationError(Exception ex, int i) {};

    /**
     * <p>Metoda forEach wywo�ywana przy ka�dej iteracji na elemencie zbioru (pierwszy argument)
     * oraz indeksie elementu (numer elementu w zbiorze).</p>
     */
    protected abstract void forEach(K element, int index) throws Exception;

    public IterateTimerTask()
    {
        log.debug("constructor");
    }

    @Override
    public void run()
    {
        try
        {
            if(iterateInContext) {
                log.debug("[run] open context");
                DSApi.openAdmin();
            }

            log.debug("[run] before beforeRun");
            beforeRun();
            log.debug("[run] after beforeRun");

            if(stop == true)
            {
                log.info("[run] stop == true, stopping");

                if(iterateInContext) {
                    DSApi.close();
                }

                return;
            }

            if(iterator == null)
            {
                if(collection != null)
                {
                    log.debug("[run] collection is not null, initializing iterator");
                    iterator = collection.iterator();
                }
                else
                {
                    log.error("[run] iterator is null and collection is null, exiting");
                    return;
                }
            }
            else
            {
                log.debug("[run] iterator is not null");
            }

            int i = 0;

            log.debug("[run] starting iterations");
            while(iterator.hasNext())
            {
                log.trace("[run]     iteration {}", i);

                try {
                    K element = iterator.next();
                    forEach(element, i);
                } catch(Exception ex) {
                    log.error("[run]     error", ex);
                    onIterationError(ex, i);
                }

                if(stop == true)
                {
                    log.info("[run] stopping iterations");
                    break;
                }

                i++;
            }
            log.debug("[run] finished iterations");

            log.debug("[run] before afterRun");
            afterRun();
            log.debug("[run] after afterRun");

            if(iterateInContext) {
                log.debug("[run] close context");
                DSApi.close();
            }

            log.debug("[run] end");
        }
        catch(Exception ex)
        {
            log.error("[run] error", ex);
            onError(ex);

            if(iterateInContext) {
                DSApi._close();
            }
        }
    }

    /**
     * Zatrzymuje iteracje.
     */
    public void stop()
    {
        this.stop = true;
    }

    /**
     *
     */
    public IterateTimerTask<K> setIterateInContext(boolean iterateInContext) {
        this.iterateInContext = iterateInContext;
        return this;
    }

//    /**
//     * Pobiera SELECTa. Je�li {@code bCount} r�wna si� {@code SQL_COUNT} SQL powinien zawiera� {@code SELECT count(1) as c FROM ...} (czyli jeden element).
//     * W przypadku {@code SQL_ALL} SQL powinien by� odpowiednikiem {@code SELECT * FROM ...} (czyli wiele element�w).
//     * @param selectType
//     * @return
//     */
//    //	protected abstract String getQueryString(boolean selectType);

    public Collection<K> getCollection()
    {
        return collection;
    }

    public IterateTimerTask<K> setCollection(Collection<K> collection)
    {
        this.collection = collection;
        return this;
    }

    public Iterator<K> getIterator()
    {
        return iterator;
    }

    public IterateTimerTask<K> setIterator(Iterator<K> iterator)
    {
        this.iterator = iterator;
        return this;
    }

}
