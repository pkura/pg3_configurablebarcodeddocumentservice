package pl.compan.docusafe.service;

/* User: Administrator, Date: 2005-04-20 13:18:05 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ServiceDriverNotSelectedException.java,v 1.1 2005/04/25 15:34:03 lk Exp $
 */
public class ServiceDriverNotSelectedException extends ServiceException
{
    public ServiceDriverNotSelectedException(String serviceId)
    {
        super("Us�uga "+serviceId+" nie zosta�a uruchomiona");
    }

    public ServiceDriverNotSelectedException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
