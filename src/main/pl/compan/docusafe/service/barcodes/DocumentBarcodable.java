package pl.compan.docusafe.service.barcodes;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 27.06.13
 * Time: 15:42
 * To change this template use File | Settings | File Templates.
 */
public interface DocumentBarcodable {
    List<BufferedImage> getPages() throws IOException;
    void setBarcodes(Set<String> barcodes);
    Set<String> getBarcodes();
    boolean addBarcode(String barcode);
    String getBarcode();
    File getFile();
    void setFile(File file);
}
