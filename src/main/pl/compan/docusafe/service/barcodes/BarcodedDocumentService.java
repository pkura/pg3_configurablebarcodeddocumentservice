package pl.compan.docusafe.service.barcodes;

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.common.io.Files;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.logic.AttachmentsManager;
import pl.compan.docusafe.service.barcodes.recognizer.Recognizer;
import pl.compan.docusafe.service.barcodes.recognizer.xing.BarcodeFinder;
import pl.compan.docusafe.service.barcodes.slicer.ImageSlicer;
import pl.compan.docusafe.service.*;
import pl.compan.docusafe.util.DateUtils;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 27.06.13
 * Time: 16:42
 * To change this template use File | Settings | File Templates.
 */
public abstract class BarcodedDocumentService extends ServiceDriver implements Service {

    private Timer timer;
    long period = 1l;
    private Boolean syncing = false;
    Property[] properties;

    class PeriodSyncTimeProperty extends Property {
        public PeriodSyncTimeProperty() {
            super(SIMPLE, PERSISTENT, BarcodedDocumentService.this, "period", "Czestotliwo�c synchronizacji w minutach", String.class);
        }

        protected Object getValueSpi() {
            return period;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (BarcodedDocumentService.this) {
                if (object != null) {
                    period = Long.parseLong((String) object);
                    stop();
                } else {
                    period = 1440L;
                }
            }
        }
    }

    @Override
    public Property[] getProperties() {
        return properties;
    }

    public BarcodedDocumentService() {
        properties = new Property[] {	new PeriodSyncTimeProperty()        };
    }

    @Override
    protected void start() throws ServiceException {
        timer = new Timer();
        timer.schedule(new BDTask(), 10 * DateUtils.SECOND, period*DateUtils.MINUTE);
    }

    @Override
    protected void stop() throws ServiceException {
        if(timer != null){
            console(Console.INFO, "System BarcodedDocumentService zako�czy� dzia�anie");
            timer.cancel();
            timer = new Timer();
            timer.schedule(new BDTask(), 10 * DateUtils.SECOND, period * DateUtils.MINUTE);
            console(Console.INFO, "System BarcodedDocumentService wystartowal. Cz�stotliwo��: "+period+" m.");
        }
    }

    @Override
    protected boolean canStop() {
        return !syncing;  //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     *
     * @return �cie�ka dost�powa do katalogu z skanami do przetworzenia
     */
    protected abstract String getScansFolder();

    /**
     *
     * @return cn pola, pod kt�rego zostanie podpi�ty skan.
     * Je�li null to nie dodaje za��cznika pod �adne pole.
     */
    protected abstract String getAttachmentColumnCn();

    /**
     *
     * @return szablon dodawanego za�acznik
     */
    protected abstract String getAttachmentNameTemplate();

    /**
     *
     * @return cn dokcinda, dla kt�rego szukamy barcodu.
     * Je�li null, nie filtrujemy wg dockindu.
     */
    protected abstract String getDockindCn();

    /**
     *
     * @return �cie�ka dost�powa do katalogu z poprawnie przetworzonymi skanami.
     * Je�li null to plik skanu zostanie usuni�ty.
     */
    protected abstract String getFinishedScansFolder();

    /**
     *
     * @return klasa implementuj�c� interfejs rozponaznia barkodu
     */
    protected abstract Recognizer getRecognizer();

    /**
     *
     * @return klasa implementuj�c� interfejs rozponaznia barkodu.
     * Uruchomiana jako druga dla ka�dego pliku, je�eli pierwsza nie rozpona�a.
     */
    protected abstract Recognizer getAlternativeRecognizer();

    /**
     *
     * @return klasa implementuj�ca interfejs dzielenia obrazka na potrzeby rozpozania barkodu
     */
    protected abstract ImageSlicer getImageSlicer();

    protected String getDebugModeFolder() {
        return null;
    };
    
    protected String getBarcode(File[] files) {
    	Multimap<String, DocumentBarcodable> documentBarcodables = (new BDTask()).searchBarcode(files);
    	String e = null;
    	if (documentBarcodables.keySet().iterator().hasNext()){
    		e = documentBarcodables.keySet().iterator().next();
    	}
        return e;
    };

    public class BDTask extends TimerTask {

        @Override
        public void run() {
            synchronized (syncing) {
                try {
                    syncing = true;
                    long start = System.currentTimeMillis();
                    try {
                        boolean dockindFiltering = getDockindCn() != null;

                        File mainFolder = new File(getScansFolder());
                        File[] files = mainFolder.listFiles();

                        Multimap<String, DocumentBarcodable> barcodeToDocument = searchBarcode(files);
                        Set<DocumentBarcodable> barcodedScanDocuments = Sets.newHashSet();
                        for (Iterator<DocumentBarcodable> iterator = barcodeToDocument.values().iterator(); iterator
								.hasNext();) {
							DocumentBarcodable e = iterator.next();
							barcodedScanDocuments.add(e);							
						}

                        Set<DocumentBarcodable> unSuccessfulProcessed = Sets.newHashSet(barcodedScanDocuments);
                        Set<DocumentBarcodable> successfulProcessed = Sets.newHashSet();

                        for (String barcode : barcodeToDocument.keySet()) {
                            boolean wasOpened = false;
                            try {
                                if (!DSApi.isContextOpen()) {
                                    DSApi.openAdmin();
                                    wasOpened = true;
                                }

                                List<OfficeDocument> dsDocuments = OfficeDocument.findAllByBarcode(barcode);
                                if (!dsDocuments.isEmpty()) {
                                    if (dsDocuments.size() > 1) {
                                        log.debug("Znaleziono wi�cej ni� jeden dokument z barkodem: "+barcode);
                                    }

                                    for (OfficeDocument dsDocument : dsDocuments) {
                                        if (!dockindFiltering || getDockindCn().equals(dsDocument.getDocumentKind().getCn())) {
                                            DSApi.context().begin();

                                            try {
                                                int i = 1;
                                                for (DocumentBarcodable docBarcoded : barcodeToDocument.get(barcode)) {
                                                    if (i > 1) {
                                                        log.debug("Znaleziono wi�cej ni� jeden skan dokumentu dla barkodu: " + barcode);
                                                    }

                                                    String attachementName = prepareAttachementName(getAttachmentNameTemplate(), i++);
                                                    Attachment attachment = createAttachement(dsDocument, docBarcoded, attachementName);

                                                    if (getAttachmentColumnCn() != null) {
                                                        updateDocumentColumnField(getAttachmentColumnCn(), dsDocument, attachment);
                                                    }

                                                    unSuccessfulProcessed.remove(docBarcoded);
                                                    successfulProcessed.add(docBarcoded);
                                                }
                                                DSApi.context().commit();
                                            } catch (EdmException e) {
                                                log.error(e.getMessage(), e);
                                                console(Console.ERROR, e.getMessage());
                                                DSApi.context().rollback();
                                            }
                                        }
                                    }
                                } else {
                                    log.error("Nie znaleziono w systemie dokumentu dla barkodu: " + barcode);
                                    console(Console.ERROR, "Nie znaleziono w systemie dokumentu dla barkodu: " + barcode);
                                }

                            } finally {
                                if (DSApi.isContextOpen() && wasOpened) {
                                    DSApi.close();
                                }
                            }
                        }

                        moveFinishedDocuments(successfulProcessed);

                        log.debug("FINISHED");
                        for (DocumentBarcodable doc : barcodedScanDocuments) {
                            log.error(doc.getFile().getName() + " | " + doc.getBarcodes());
                        }

                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                        console(Console.ERROR, e.getMessage());
                    } finally {
                        log.debug("TOTAL: " + (System.currentTimeMillis() - start));
                    }
                } finally{
                    syncing = false;

                }
            }
        }

        public Multimap<String, DocumentBarcodable> searchBarcode(File[] files) {
        	
        	boolean debugMode = getDebugModeFolder() != null;
            boolean dockindFiltering = getDockindCn() != null;
            Recognizer primaryRecognizer = getRecognizer();
            Recognizer alternativeRecognizer = getAlternativeRecognizer();
            ImageSlicer slicerService = getImageSlicer();

            Preconditions.checkArgument(getScansFolder() != null, "Nieustawiono �cie�ki do skan�w");
            Preconditions.checkArgument(getAttachmentNameTemplate() != null, "Nieustawiono szablonu nazwy dodawangeo za��cznika");
            Preconditions.checkNotNull(primaryRecognizer, "Nieustawiono klasy rozpoznaj�cej barkod");
            Preconditions.checkNotNull(slicerService, "Nieustawiono klasy dziel�cej obraz");

            List<Recognizer> recognizers = Lists.newArrayList(primaryRecognizer,alternativeRecognizer);


            Preconditions.checkNotNull(files, "Niepoprawna �cie�ka do folderu skan�w");

            Set<DocumentBarcodable> barcodedScanDocuments = Sets.newHashSet();
            Multimap<String, DocumentBarcodable> barcodeToDocument = ArrayListMultimap.create();

            for (File file : files) {
                String fileName = file.getName();

                DocumentBarcodable document;

                if (fileName.toLowerCase().endsWith(".pdf")) {
                    document = new PdfBarcodedDocument();
                } else if (fileName.toLowerCase().endsWith(".tif") || fileName.toLowerCase().endsWith(".tiff")) {
                    document = new TiffBarcodedDocument();
                } else {
                    log.error("Nie obs�ugiwany plik: " + fileName);
                    console(Console.ERROR, "Nie obs�ugiwany plik: "+fileName);
                    continue;
                }

                document.setFile(file);
                barcodedScanDocuments.add(document);
            }

            Preconditions.checkState(!barcodedScanDocuments.isEmpty(), "Nie znaleziono �adnych skan�w plik�w");

            String barcode = null;
            for (DocumentBarcodable document : barcodedScanDocuments) {
                String fileName = document.getFile().getName();
                log.debug("Przetwarzam: " + fileName);

                barcode = findCorrectBarcode(slicerService, recognizers, document, dockindFiltering, debugMode);
                if (barcode != null) {
                    barcodeToDocument.put(barcode, document);
                }
                if (document.getBarcode() == null) {
                    log.debug("Nie znaleziono barkodu dla: " + fileName);
                }
            }
			return barcodeToDocument;
        };
        
        /**
         *
         * @param slicerService
         * @param recognizers
         * @param document
         * @param dockindFiltering
         * @param debugMode
         * @return barkod dokumentu, je�li zosta� on potwierdzony poprzez pomy�lne wyszukania dokumentu w systemie
         */
        private String findCorrectBarcode(ImageSlicer slicerService, List<Recognizer> recognizers, DocumentBarcodable document, boolean dockindFiltering, boolean debugMode) {
            String fileName = document.getFile().getName();
            for (Recognizer currentRecognizer : recognizers) {
                try {
                    List<BufferedImage> pages = document.getPages();
                    int pageNo = 0;
                    for (BufferedImage page : pages) {
                        pageNo++;
                        if (debugMode) {
                            savePageImage(getDebugModeFolder(), fileName, pageNo, page);
                        }

                        slicerService.setPage(page);
                        int sliceNo = 0;
                        for (BufferedImage slice : slicerService.getSlicedImages()) {
                            sliceNo++;
                            if (debugMode) {
                                saveSlicedImage(getDebugModeFolder(), fileName, pageNo, sliceNo, slice);
                            }

                            try {
                                String barcode = currentRecognizer.recognize(slice);
                                if (barcode != null) {
                                    log.debug("ZNALEZIONO " + document.getFile().getName() + " barkod:" + barcode);
                                    if (isAnyOfficeDocumentWithBarcode(barcode, dockindFiltering)) {
                                        document.addBarcode(barcode);
                                        return barcode;
                                    }

                                    return null;


                                }else {//mechanizm Wojtka Kuty�y
                                BarcodeFinder finder = new BarcodeFinder();
                                if(document instanceof PdfBarcodedDocument){
                                barcode = 	finder.findBarcode(document.getFile());
                                }
                                		
                                }
                            } catch (EdmException e) {
                                log.error(e.getMessage(), e);
                            }

                        }
                    }


                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            return null;
        }

        private boolean isAnyOfficeDocumentWithBarcode(String barcode, boolean dockindFiltering) throws EdmException {
            boolean wasOpened = false;
            try {
                if (!DSApi.isContextOpen()) {
                    DSApi.openAdmin();
                    wasOpened = true;
                }

                List<OfficeDocument> dsDocuments = OfficeDocument.findAllByBarcode(barcode);
                if (!dsDocuments.isEmpty()) {
                    if (dsDocuments.size() > 1) {
                        log.debug("Znaleziono wi�cej ni� jeden dokument z barkodem: "+barcode);
                    }

                    for (OfficeDocument dsDocument : dsDocuments) {
                        if (!dockindFiltering || getDockindCn().equals(dsDocument.getDocumentKind().getCn())) {
                            return true;
                        }
                    }
                }
            } finally {
                if (DSApi.isContextOpen() && wasOpened) {
                    DSApi.close();
                }
            }
            return false;
        }

        private void moveFinishedDocuments(Set<DocumentBarcodable> successfulProcessed) {
            for (DocumentBarcodable doc : successfulProcessed) {
                try {
                    if (getFinishedScansFolder() !=  null) {
                        Files.move(doc.getFile(), new File(getFinishedScansFolder() + File.separator + doc.getFile().getName()));
                    } else {
                        doc.getFile().delete();
                    }
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }

        private void savePageImage(String debugFolder, String fileName, int pageNo, BufferedImage page) throws IOException {
            File pageToSave = new File(debugFolder + "//" + fileName + "_p" + pageNo + ".jpg");
            ImageIO.write(page, "jpg", pageToSave);
        }

        private void saveSlicedImage(String debugFolder, String fileName, int pageNo, int sliceNo, BufferedImage slice) throws IOException {
            File sliceToSave = new File(debugFolder + "//" + fileName + "_p" + pageNo + "_s" + sliceNo + ".jpg");
            ImageIO.write(slice, "jpg", sliceToSave);
        }

        private Attachment createAttachement(OfficeDocument dsDocument, DocumentBarcodable docBarcoded, String attachementName) throws EdmException {
            Attachment attachment = new Attachment(attachementName);
            dsDocument.createAttachment(attachment);
            attachment.createRevision(docBarcoded.getFile());

            List<JBPMTaskSnapshot> task = JBPMTaskSnapshot.findByDocumentId(dsDocument.getId());
            for(JBPMTaskSnapshot bean : task) {
                bean.setIsAnyImageInAttachments(AttachmentsManager.isAnyImageInAttachments(dsDocument.getId()));
                bean.setIsAnyPdfInAttachments(AttachmentsManager.isAnyPdfInAttachments(dsDocument.getId()));
            }

            return attachment;
        }

        private String prepareAttachementName(String attachmentNameTemplate, int fileNumber) {
            return attachmentNameTemplate.replace("%file_no%",""+fileNumber);
        }

        private void updateDocumentColumnField(String attachemntColumnCn, OfficeDocument dsDocument, Attachment attachment) throws EdmException {
            Field attachemntField = dsDocument.getDocumentKind().getFieldByCn(attachemntColumnCn);
            if (attachemntField == null || !Field.ATTACHMENT.equals(attachemntField.getType())) {
                throw new EdmException("Brak lub niepoprawny typ pola na dokumencie, cn: " + attachemntColumnCn);
            }

            try {
                StringBuilder sb = new StringBuilder("UPDATE ");
                sb.append(dsDocument.getDocumentKind().getTablename()).append(" SET ")
                        .append(attachemntField.getColumn()).append(" = ? WHERE document_id = ?");

                PreparedStatement ps = DSApi.context().prepareStatement(sb);
                ps.setLong(1, attachment.getId());
                ps.setLong(2, dsDocument.getId());
                ps.executeUpdate();
            } catch (SQLException e) {
                throw new EdmException(e);
            }
        }
    }
}
