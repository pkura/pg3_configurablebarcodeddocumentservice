package pl.compan.docusafe.service.barcodes;

import com.google.common.collect.Lists;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import pl.compan.docusafe.util.ImageUtils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 27.06.13
 * Time: 15:51
 * To change this template use File | Settings | File Templates.
 */
public class PdfBarcodedDocument extends AbstractBarcodedDocument {

    @Override
    public List<BufferedImage> getPages() throws IOException {
        List<BufferedImage> pages = Lists.newLinkedList();

        PDDocument document = PDDocument.load(getFile());
        List<PDPage> pDPages = document.getDocumentCatalog().getAllPages();
        for (PDPage pDPage : pDPages) {
            int rotation = pDPage.findRotation();
            Map<String, PDXObjectImage> images = pDPage.getResources().getImages();
            if (images.size() > 0) {
                for (String imageKey : images.keySet()) {
                    BufferedImage image = images.get(imageKey).getRGBImage();
                    if (image != null && rotation != 0) {
                        image = ImageUtils.rotateImage(image, rotation);
                    }
                    if(image != null){
                    	pages.add(image);
                    }
                }
            } else {
                BufferedImage image = pDPage.convertToImage(BufferedImage.TYPE_BYTE_GRAY, 300);
                if (image != null && rotation != 0) {
                    image = ImageUtils.rotateImage(image, rotation);
                }
                if(image != null){
                	pages.add(image);
                }
            }
        }

        return pages;
    }
}
