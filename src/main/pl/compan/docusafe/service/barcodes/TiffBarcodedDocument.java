package pl.compan.docusafe.service.barcodes;

import com.google.common.collect.Lists;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.codec.TiffImage;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

import pl.compan.docusafe.util.ImageUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 27.06.13
 * Time: 15:51
 * To change this template use File | Settings | File Templates.
 */
public class TiffBarcodedDocument extends AbstractBarcodedDocument {

    @Override
    public List<BufferedImage> getPages() throws IOException {
        List<BufferedImage> pages = Lists.newLinkedList();
        
        ImageInputStream is = ImageIO.createImageInputStream(getFile());
        if (is == null || is.length() == 0){
          // handle error
        }
        Iterator<ImageReader> iterator = ImageIO.getImageReaders(is);
        if (iterator == null || !iterator.hasNext()) {
        	/*try {
			//	convertTiffToPdf();
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
          throw new IOException("Image file format not supported by ImageIO: ");
        }
        // We are just looking for the first reader compatible:
        ImageReader reader = (ImageReader) iterator.next();
        iterator = null;
        reader.setInput(is);
        
        int nbPages = reader.getNumImages(true);
        for (int i = 0; i < nbPages; i++) {
        	BufferedImage page = reader.read(i);
        	int rotation = 0;
                    if (page != null && rotation != 0) {
                    	page = ImageUtils.rotateImage(page, rotation);
                    }
                    if(page != null){
                    	pages.add(page);
                    }
		}
        return pages;
    }
    public void convertTiffToPdf() throws IOException, DocumentException
    {String inFileDir="";
    String inTifFileName="";
    String workFileDir="";
         //Read the Tiff File
    String filePath =  getFile().getPath();
       RandomAccessFileOrArray myTiffFile=new RandomAccessFileOrArray(inFileDir+"/"+inTifFileName);
       //Find number of images in Tiff file
       int numberOfPages=TiffImage.getNumberOfPages(myTiffFile);
       Document TifftoPDF=new Document();
     String   workPdfFileName = inTifFileName+".pdf";
       PdfWriter.getInstance(TifftoPDF, new FileOutputStream(workFileDir+"/"+workPdfFileName));
       TifftoPDF.open();
       //Run a for loop to extract images from Tiff file
       //into a Image object and add to PDF recursively
       for(int i=1;i<=numberOfPages;i++)
       {
           Image tempImage=TiffImage.getTiffImage(myTiffFile, i);
           //Reader barcodeReader = new MultiFormatReader();
           //barcodeReader.bi
           TifftoPDF.add(tempImage);
       }
       TifftoPDF.close();
    }
}
