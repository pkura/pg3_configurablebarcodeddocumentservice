package pl.compan.docusafe.service.barcodes;

import com.google.common.collect.Sets;

import java.io.File;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 27.06.13
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractBarcodedDocument implements DocumentBarcodable {
    Set<String> barcodes = Sets.newHashSet();
    File documentFile;

    @Override
    public String getBarcode() {
        return barcodes.isEmpty() ? null : barcodes.iterator().next();
    }

    @Override
    public boolean addBarcode(String barcode) {
        return barcodes.add(barcode);
    }

    @Override
    public Set<String> getBarcodes() {
        return barcodes;
    }

    @Override
    public File getFile() {
        return documentFile;
    }

    @Override
    public void setBarcodes(Set<String> barcodes) {
        this.barcodes = barcodes;
    }

    @Override
    public void setFile(File file) {
        this.documentFile = file;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractBarcodedDocument that = (AbstractBarcodedDocument) o;

        if (!documentFile.equals(that.documentFile)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return documentFile.hashCode();
    }
}
