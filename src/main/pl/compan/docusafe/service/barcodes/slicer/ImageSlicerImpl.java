package pl.compan.docusafe.service.barcodes.slicer;

import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 27.06.13
 * Time: 16:14
 * To change this template use File | Settings | File Templates.
 */
public class ImageSlicerImpl implements ImageSlicer {

    BufferedImage page;
    int areaH,areaW;
    int areaSizeH,areaSizeW;
    int stepsH,stepsW;
    int boundaryConditionYPos,boundaryConditionXPos;
    int stepSizeH, stepSizeW;


    /**
     *
     * @param areaH Wielko�� badanego obszaru w procentach wysokosci strony
     * @param areaW Wielko�� badanego obszaru w procentach wysokosci strony
     * @param realStepsH Ilo�� przeskok�w w wysoko�ci
     * @param realStepsW Ilo�� przeskok�w w szerokosci
     */
    public ImageSlicerImpl(int areaH, int areaW, int realStepsH, int realStepsW) {
        this.areaH = areaH;
        this.areaW = areaW;
        this.stepsH = realStepsH-1;
        this.stepsW = realStepsW-1;
    }

    @Override
    public void setPage(BufferedImage page) {
        this.page = page;
        int pageH = getPage().getHeight();
        int pageW = getPage().getWidth();
        /**Wielkosc obszaru badanego w punktach*/
        areaSizeH = (pageH * areaH) / 100;
        areaSizeW = (pageW * areaW) / 100;
        /**Wielkosc obszaru po ktorym ma byc robiony przeskok*/
        int pageJumpH = pageH - areaSizeH;
        int pageJumpW = pageW - areaSizeW;
        /**Wielkosc skoku pomiedzy kolejnymi badaniami*/
        stepSizeH = Math.round(pageJumpH / stepsH);
        stepSizeW = Math.round(pageJumpW / stepsW);

        boundaryConditionYPos = pageH - areaSizeH;
        boundaryConditionXPos = pageW - areaSizeW;

/*        System.out.println("I h: "+getPage().getHeight()+", I w: "+getPage().getWidth()+
                ", areaSizeW: "+areaSizeW+", areaSizeH: "+areaSizeH+
                ", stepSizeW: "+stepSizeW+", stepSizeH: "+stepSizeH+
                ", pageJumpH: "+pageJumpH+", pageJumpW: "+pageJumpW+
                ", boundaryConditionXPos: "+ boundaryConditionXPos +", boundaryConditionYPos: "+ boundaryConditionYPos);*/

    }

    @Override
    public BufferedImage getPage() {
        return page;
    }

    @Override
    public Iterable<BufferedImage> getSlicedImages() {
        return new Iterable<BufferedImage>() {
            @Override
            public Iterator<BufferedImage> iterator() {
                return new Iterator<BufferedImage>() {

                    BufferedImage buffer;
                    int xPointer;
                    int yPointer;
                    int xPos;
                    int yPos;

                    @Override
                    public boolean hasNext() {
                        while (xPointer <= stepsW) {
                            while (yPointer <= stepsH) {
                                try {
                                    if (xPointer == stepsW) {
                                        xPos =  boundaryConditionXPos;
                                    } else {
                                        xPos =  stepSizeW * xPointer;
                                    }
                                    if (yPointer == stepsH) {
                                        yPointer++;
                                        yPos =  boundaryConditionYPos;
                                    } else {
                                        yPos =  stepSizeH * yPointer++;
                                    }

                                    buffer = getPage().getSubimage(xPos, yPos, areaSizeW, areaSizeH);
                                    return true;
                                } catch (RasterFormatException e3) {
                                }
                            }
                            xPointer++;
                            yPointer = 0;
                        }

                        return false;
                    }

                    @Override
                    public BufferedImage next() {
                        return buffer;
                    }

                    @Override
                    public void remove() {
                    }
                };
            }
        };
    }
}
