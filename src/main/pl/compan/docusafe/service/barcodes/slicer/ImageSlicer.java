package pl.compan.docusafe.service.barcodes.slicer;

import java.awt.image.BufferedImage;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 27.06.13
 * Time: 16:08
 * To change this template use File | Settings | File Templates.
 */
public interface ImageSlicer {
    void setPage(BufferedImage page);
    BufferedImage getPage();
    Iterable<BufferedImage> getSlicedImages();
}