/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.compan.docusafe.service.barcodes.slicer;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Obiekt ktory tnie obrazek na slice
 * @author wojciech.kutyla
 */
public class Slicer 
{
    private final static Logger log = LoggerFactory.getLogger(Slicer.class);
    
    //nie ustawiac ponizej 800
    private Integer xSize = 1200;
    //nie ustawiac ponizej 400
    private Integer ySize = 600;
    
    private Integer xStep = 600;
    
    private Integer yStep = 600;
    
    private Integer x1;
    
    private Integer x2;
    
    private Integer y1;
    
    private Integer y2;
    
    private Integer xBorder;
    
    private Integer yBorder;
    /**
     * Obraze do ladowania
     */
    private BufferedImage image;
    /**
     * Znacznik czy ostatni slice
     */
    private boolean lastSlice;
    
    private Integer currentX = 0;
    private Integer currentY = 0;
    public void setImage(BufferedImage img)
    {
        image = img;
        lastSlice = false;
        currentX = 0;
        currentY = 0;
        xBorder = img.getWidth();
        yBorder = img.getHeight();
    }
    
    /**
     * Wyznacza wspolrzedne nowego slice
     * @return 
     */
    public boolean nextSlice()
    {      
        if (lastSlice)
        {
            return false;
        }
        x1 = currentX;
        y1 = currentY;
        x2 = currentX+xSize;
        y2 = currentY+ySize;
        
        if (x2>=xBorder)
        {
            x2 = xBorder;
            currentX = 0;
            currentY = currentY+yStep;
        }
        else
        {
            currentX = currentX + xStep;
        }
        if (y2>=yBorder)
        {
            y2=yBorder;
            if (x2>=xBorder)
            {
                lastSlice = true;
            }
        }
        log.trace("x1="+x1+" y1="+y1+" x2="+x2+" y2="+y2);
        return !lastSlice;
        
    }
    
    /**
     * Zwraca slice jako rectangle
     * @return 
     */
    public Rectangle getSliceAsRectangle()
    {
        return new Rectangle(x1,y1,x2,y2);
    }
    
    /**
     * Zwraca slice jako obraz
     * @return 
     */
    public BufferedImage getSliceAsImage()
    {
        return image.getSubimage(x1, y1, x2-x1, y2-y1);
    }
     /**
     * Konwersja do szarosci
     * @param image
     * @return 
     */
    public BufferedImage toGrayImage(BufferedImage image)
    {
        log.trace("convertToBlackAndWhite");
        ColorConvertOp op = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);       
        BufferedImage resp =  op.filter(image, null);                        
        return resp;
    }
    
    /**
     * Konwersja to B&W
     * @param image
     * @return 
     */
    public BufferedImage toBinaryImage(final BufferedImage image) 
    {
        final BufferedImage blackAndWhiteImage = new BufferedImage(
            image.getWidth(null), 
            image.getHeight(null), 
            BufferedImage.TYPE_BYTE_BINARY);
        final Graphics2D g = (Graphics2D) blackAndWhiteImage.getGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return blackAndWhiteImage;
    }
}
