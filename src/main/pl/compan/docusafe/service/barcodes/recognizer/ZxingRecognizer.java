package pl.compan.docusafe.service.barcodes.recognizer;

import com.google.common.base.Optional;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.GlobalHistogramBinarizer;
import com.google.zxing.common.HybridBinarizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 27.06.13
 * Time: 16:34
 * To change this template use File | Settings | File Templates.
 */
public class ZxingRecognizer implements Recognizer {

    private static final Logger log = LoggerFactory.getLogger(ZxingRecognizer.class);
    
    private Map<DecodeHintType, Object> hints;
    private Reader barcodeReader;

    public ZxingRecognizer(Reader reader) {
        this.barcodeReader = reader;
        this.hints = new HashMap<DecodeHintType, Object>();
    }

    public ZxingRecognizer(Reader reader, Map<DecodeHintType,Object> hints) {
        this.barcodeReader = reader;
        this.hints = hints;
    }

    public ZxingRecognizer(Map<DecodeHintType, Object> hints) {
        this.barcodeReader = new MultiFormatReader();
        this.hints = hints;
    }

    public Optional<String> recognizeOptional(InputStream stream) throws IOException, EdmException {
        return Optional.fromNullable(recognize(stream));
    }

    public Optional<String> recognizeOptional(BufferedImage slice) {
        return Optional.of(recognize(slice));
    }

    public String recognize(InputStream stream) throws IOException, EdmException {
        BufferedImage image = ImageIO.read(stream);
        return recognize(image);
    }

    @Override
    public String recognize(BufferedImage slice) {
        LuminanceSource source = new BufferedImageLuminanceSource(slice);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
//        BinaryBitmap bitmap = new BinaryBitmap(new GlobalHistogramBinarizer(source));
        String finalResult = null;
        try {
            if (hints != null) {
                finalResult = barcodeReader.decode(bitmap, hints).getText();
            } else {
                finalResult = barcodeReader.decode(bitmap).getText();
            }
        } catch (NotFoundException e) {
            log.debug("[recognize] debug", e);
        } catch (Exception ex) {
            log.error("[recognize] error", ex);
        }
        return finalResult;
    }
}
