package pl.compan.docusafe.service.barcodes.recognizer;

import java.io.File;

public interface WireBarcodeFinder {
	String find(File[] files);
}
