package pl.compan.docusafe.service.barcodes.recognizer.xing;

/**
 * Opisuje rozmiaj Slice
 * @author Dell
 */
public class SliceArea
{
    private int minX;
    private int minY;
    private int maxX;
    private int maxY;

    public SliceArea()
    {

    }

    /**
     *
     * @param miX
     * @param miY
     * @param maX
     * @param maY
     */
    public SliceArea(int miX, int miY, int maX, int maY)
    {
        minX = miX;
        minY = miY;
        maxX = maX;
        maxY = maY;
    }
    /**
     * @return the minX
     */
    public int getMinX() {
        return minX;
    }

    /**
     * @param minX the minX to set
     */
    public void setMinX(int minX) {
        this.minX = minX;
    }

    /**
     * @return the minY
     */
    public int getMinY() {
        return minY;
    }

    /**
     * @param minY the minY to set
     */
    public void setMinY(int minY) {
        this.minY = minY;
    }

    /**
     * @return the maxX
     */
    public int getMaxX() {
        return maxX;
    }

    /**
     * @param maxX the maxX to set
     */
    public void setMaxX(int maxX) {
        this.maxX = maxX;
    }

    /**
     * @return the maxY
     */
    public int getMaxY() {
        return maxY;
    }

    /**
     * @param maxY the maxY to set
     */
    public void setMaxY(int maxY) {
        this.maxY = maxY;
    }
}
