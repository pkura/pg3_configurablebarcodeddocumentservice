/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.compan.docusafe.service.barcodes.recognizer.xing;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Dell
 */
public class SeparateFileInfo 
{
    private String fileName;
    private List<PageRange> ranges = new LinkedList<PageRange>();

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the ranges
     */
    public List<PageRange> getRanges() {
        return ranges;
    }

    /**
     * @param ranges the ranges to set
     */
    public void setRanges(List<PageRange> ranges) {
        this.ranges = ranges;
    }
}
