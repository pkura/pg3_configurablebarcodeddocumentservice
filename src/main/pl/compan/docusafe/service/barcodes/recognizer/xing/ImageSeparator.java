/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.compan.docusafe.service.barcodes.recognizer.xing;

import com.asprise.util.tiff.TIFFReader;
import com.asprise.util.tiff.TIFFWriter;
import com.google.zxing.NotFoundException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.codec.TiffImage;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Obiekt, ktory rozcina obiekt na kilka mniejszych 
 * @author Dell
 */
public class ImageSeparator 
{
     private final static Logger log = LoggerFactory.getLogger(ImageSeparator.class);
     
     private String inFileDir;
     private String outFileDir;
     private String workFileDir;
     private String workPdfFileName;
     private String inTifFileName;
     private SeparateFileInfo info;
     
     public void convertTiffToPdf() throws IOException, DocumentException
     {
          //Read the Tiff File
        RandomAccessFileOrArray myTiffFile=new RandomAccessFileOrArray(inFileDir+"/"+inTifFileName);
        //Find number of images in Tiff file
        int numberOfPages=TiffImage.getNumberOfPages(myTiffFile);
        Document TifftoPDF=new Document();
        workPdfFileName = inTifFileName+".pdf";
        PdfWriter.getInstance(TifftoPDF, new FileOutputStream(workFileDir+"/"+workPdfFileName));
        TifftoPDF.open();
        //Run a for loop to extract images from Tiff file
        //into a Image object and add to PDF recursively
        for(int i=1;i<=numberOfPages;i++)
        {
            Image tempImage=TiffImage.getTiffImage(myTiffFile, i);
            //Reader barcodeReader = new MultiFormatReader();
            //barcodeReader.bi
            TifftoPDF.add(tempImage);
        }
        TifftoPDF.close();
     }

     public String getBarcode(File pdfFile) throws IOException, NotFoundException {
         log.debug("Szukamy separatora");
         info = new SeparateFileInfo();
         info.setFileName(inTifFileName);

         String barcode = null;
         NotFoundException notFoundException = null;

                 PDDocument document = PDDocument.load(pdfFile);
         List<PDPage> pDPages = document.getDocumentCatalog().getAllPages();
        try
        {
            XingBarcodeFinder finder = new XingBarcodeFinder();
            int licznik = 0;
            PageRange range = new PageRange();
            range.setUpFromTo(1);
            info.getRanges().add(range);
            for (PDPage pDPage : pDPages)
            {
                licznik++;
                log.debug("licznik="+licznik);
                boolean found = false;
                Map<String, PDXObjectImage> images = pDPage.getResources().getImages();
                if (images.isEmpty())
                {
                    log.debug("Nie ma obrazkow");
//                    continue;
                    return null;
                }
                for (String imageKey : images.keySet()) 
                {
                    BufferedImage image = images.get(imageKey).getRGBImage();

                    barcode = finder.findBarcode(image);

					if(barcode != null) {
					    return barcode;
					}

                }

                break;
            }
        }
        finally
        {
            document.close();
        }

        if(notFoundException != null) {
            throw notFoundException;
        }

        return barcode;
     }
     
     int tiffConversion = TIFFWriter.TIFF_CONVERSION_TO_GRAY;
     int tiffCompression = TIFFWriter.TIFF_COMPRESSION_PACKBITS;
     
     public void sliceImage() throws IOException
     {
         
        File file = new File(inFileDir+"/"+inTifFileName);
        int rangeNum = 0;
        for (PageRange range : info.getRanges())
        {
            rangeNum++;
            log.debug("rangeNum="+rangeNum);
            List<BufferedImage> list = new LinkedList<BufferedImage>();
            TIFFReader reader = new TIFFReader(file);
            int pages = reader.countPages();
            for(int iter=0;iter<pages;iter++)
            {
                int pageNum = iter+1;
                log.trace("Strona "+pageNum);
                if (pageNum >= range.getFrom() && pageNum <= range.getTo())
                {
                    log.debug("dodajemy");
                    RenderedImage im = reader.getPage(iter);                
                    list.add((BufferedImage)im);
                }
            }            
            BufferedImage[] b = new BufferedImage[list.size()];
            int licznik  = 0;
            for (BufferedImage bi : list)
            {
                b[licznik] = bi;
                licznik ++;
            }
            File output = new File(outFileDir+"/"+rangeNum+"_"+inTifFileName);
                    
            TIFFWriter.createTIFFFromImages(b, tiffConversion, tiffCompression, output);
        }
     }
    /**
     * @return the inFileDir
     */
    public String getInFileDir() {
        return inFileDir;
    }

    /**
     * @param inFileDir the inFileDir to set
     */
    public void setInFileDir(String inFileDir) {
        this.inFileDir = inFileDir;
    }

    /**
     * @return the outFileDir
     */
    public String getOutFileDir() {
        return outFileDir;
    }

    /**
     * @param outFileDir the outFileDir to set
     */
    public void setOutFileDir(String outFileDir) {
        this.outFileDir = outFileDir;
    }

    /**
     * @return the inTifFileName
     */
    public String getInTifFileName() {
        return inTifFileName;
    }

    /**
     * @param inTifFileName the inTifFileName to set
     */
    public void setInTifFileName(String inTifFileName) {
        this.inTifFileName = inTifFileName;
    }

    /**
     * @return the workFileDir
     */
    public String getWorkFileDir() {
        return workFileDir;
    }

    /**
     * @param workFileDir the workFileDir to set
     */
    public void setWorkFileDir(String workFileDir) {
        this.workFileDir = workFileDir;
    }

    /**
     * @return the workPdfFileName
     */
    public String getWorkPdfFileName() {
        return workPdfFileName;
    }

    /**
     * @param workPdfFileName the workPdfFileName to set
     */
    public void setWorkPdfFileName(String workPdfFileName) {
        this.workPdfFileName = workPdfFileName;
    }
}
