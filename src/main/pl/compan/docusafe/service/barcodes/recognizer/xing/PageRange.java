/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.compan.docusafe.service.barcodes.recognizer.xing;

/**
 * Zakres stron
 * @author Dell
 */
public class PageRange 
{
    private int from;
    private int to;

    public void setUpFromTo(int start)
    {
        from = start;
        to = start;
    }
            
    public void increaseTo()
    {
        to = to + 1;
    }
    /**
     * @return the from
     */
    public int getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(int from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public int getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(int to) {
        this.to = to;
    }
}
