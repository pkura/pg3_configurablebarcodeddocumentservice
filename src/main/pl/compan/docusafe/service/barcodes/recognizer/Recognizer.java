package pl.compan.docusafe.service.barcodes.recognizer;

import pl.compan.docusafe.core.EdmException;

import java.awt.image.BufferedImage;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 27.06.13
 * Time: 16:33
 * To change this template use File | Settings | File Templates.
 */
public interface Recognizer {

    String recognize(BufferedImage slice) throws EdmException;

}

