/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.compan.docusafe.service.barcodes.recognizer.xing;

import java.awt.image.BufferedImage;
import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Reader;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.GlobalHistogramBinarizer;

/**
 *
 * @author user
 */
public class XingBarcodeFinder
{
    private final static Logger log = LoggerFactory.getLogger(XingBarcodeFinder.class);
    
    /**
     * Wyszuluje barcode w danym slice
     * @param slice
     * @return 
     */
    public String findBarcode(BufferedImage slice)
    {        
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
        hints.put(DecodeHintType.TRY_HARDER,Boolean.TRUE);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, Lists.newArrayList(BarcodeFormat.CODE_128));
        Reader barcodeReader = new MultiFormatReader();
        try
        {
            //BufferedImage slice = image.getSubimage(550, 650, 550, 450);
            LuminanceSource source = new BufferedImageLuminanceSource(slice);
            //BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            BinaryBitmap bitmap = new BinaryBitmap(new GlobalHistogramBinarizer(source));
            String finalResult = null;
        
            if (hints != null) {
                finalResult = barcodeReader.decode(bitmap,hints).getText();
            } else 
            {
                finalResult = barcodeReader.decode(bitmap).getText();
            }
            return finalResult;
        } 
        catch (Exception e)
        {
            log.debug("e",e);
            return null;
        }
    }
}
