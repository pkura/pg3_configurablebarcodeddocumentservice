package pl.compan.docusafe.service.barcodes.recognizer.xing;

public class BarcodeHelper {

    public static final String PASSWORD_ENVELOPE = "001";
    public static final String PROTOCOL = "002";
    public static final String EXAM_CARD = "003";
    public static final String SUBJECT_ON_SEMESTER = "004";
    public static final String CERTIFICATE = "005";
    public static final String DIPLOMA = "006";
    public static final String DIPLOMA_PACKAGE = "007";
    public static final String EXAM_CARD_WITH_GRADE = "008";
    public static final String STATEMENT_B_R = "009";
    public static final String GRADUATE_CARD = "010";
    public static final String BARCODE_SEPERATOR = ":";
    public static final String charTab = "-/ABCDEFGHIJKLMNOPRSTUWXYZabcdefghijklmnoprstuwxyz0123456789";

    public static String getSummaryCode(String prefix, String code) throws Exception {
	for (int i = 0; i < prefix.length(); i++) {
	    int indexOf = charTab.indexOf(prefix.charAt(i));
	    if (indexOf < 0) {
		throw new Exception("Prefix zawiera niedozwolone znaki");
	    }
	}
	for (int i = 0; i < code.length(); i++) {
	    int indexOf = charTab.indexOf(code.charAt(i));
	    if (indexOf < 0) {
		throw new Exception("Code zawiera niedozwolone znaki");
	    }
	}
	String summaryCode = prefix + BARCODE_SEPERATOR + addZeros(code);
	return addCheckCharacter(summaryCode);
    }

    public static String addZeros(String code)
    {
         String ret = "";
        int len = 6-code.length();
        for(int i=0; i<len; i++) ret=ret+"0";
        return ret+code;
    }

    public static String getCode(String summaryCode) {
	try {
	    return summaryCode.split(BARCODE_SEPERATOR)[1];
	} catch (Exception e) {
	    return null;
	}
    }

    public static String getPrefix(String summaryCode) {
	try {
	    return summaryCode.split(BARCODE_SEPERATOR)[0];
	} catch (Exception e) {
	    return null;
	}
    }

    private static String addCheckCharacter(String summaryCode) throws Exception {
	String codeWithoutSeparators = summaryCode.replaceAll(BARCODE_SEPERATOR, "");
	for (int i = 0; i < codeWithoutSeparators.length(); i++) {
	    int indexOf = charTab.indexOf(codeWithoutSeparators.charAt(i));
	    if (indexOf < 0) {
		throw new Exception("Nie mo�na doda� sumy kontrolnej gdy� kod zawiera niedozwolone znaki");
	    }
	}
	char c = generateCheckCharacter(codeWithoutSeparators);
	return summaryCode + BARCODE_SEPERATOR + c;
    }

    private static char generateCheckCharacter(String code) {
	int factor = 2;
	int sum = 0;
	int n = charTab.length();
	for (int i = code.length() - 1; i >= 0; i--) {
	    int codePoint = charTab.indexOf(code.charAt(i));
	    int addend = factor * codePoint;
	    factor = (factor == 2) ? 1 : 2;
	    addend = (addend / n) + (addend % n);
	    sum += addend;
	}
	int remainder = sum % n;
	int checkCodePoint = n - remainder;
	checkCodePoint %= n;
	return charTab.charAt(checkCodePoint);
    }

    public static boolean validateCheckCharacter(String code) {
	String input = code.replaceAll(BARCODE_SEPERATOR, "");
	for (int i = 0; i < input.length(); i++) {
	    int indexOf = charTab.indexOf(input.charAt(i));
	    if (indexOf < 0) {
		return false;
	    }
	}
	int factor = 1;
	int sum = 0;
	int n = charTab.length();
	for (int i = input.length() - 1; i >= 0; i--) {
	    int codePoint = charTab.indexOf(input.charAt(i));
	    int addend = factor * codePoint;
	    factor = (factor == 2) ? 1 : 2;
	    addend = (addend / n) + (addend % n);
	    sum += addend;
	}
	int remainder = sum % n;
	return (remainder == 0);
    }
}

