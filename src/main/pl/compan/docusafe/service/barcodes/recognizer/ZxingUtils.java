package pl.compan.docusafe.service.barcodes.recognizer;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Collections2;
import com.google.zxing.DecodeHintType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class ZxingUtils {

    private static final Logger log = LoggerFactory.getLogger(ZxingUtils.class);

    /**
     * <p>
     *     Metoda zwraca barkod odczytany z pierwszego obrazka z listy plik�w <code>files</code>.
     *     Je�li obrazek nie mia� barkodu szukamy w kolejnym obrazku.
     * </p>
     *
     * @param files
     * @param hints
     * @return
     * @throws IOException
     */
    public static Optional<String> decodeBarcode(Collection<File> files, Map<DecodeHintType, Object> hints) throws IOException {
        for(File file: files) {
            if(! file.exists()) {
                log.error("[decodeBarcode] file doesn't exist, filePath = {}", file.getAbsolutePath());
                continue;
            }

            final Optional<String> barcode = decodeBarcode(file, hints);

            if(barcode.isPresent()) {
                return barcode;
            }
        }

        return Optional.absent();
    }

    /**
     * <p>
     *     Metoda zwraca barkod odczytany z pierwszego obrazka z listy plik�w <code>filePath</code>.
     *     Je�li obrazek nie mia� barkodu szukamy w kolejnym obrazku.
     * </p>
     * @param filePaths lista �cie�ek do plik�w
     * @return barkod z jednego z obrazk�w
     */
    public static Optional<String> decodeBarcodeFromPaths(Collection<String> filePaths, Map<DecodeHintType, Object> hints) throws IOException {
        Collection<File> files = Collections2.transform(filePaths, new Function<String, File>() {
            @Override
            public File apply(String filename) {
                return new File(filename);
            }
        });

        return decodeBarcode(files, hints);
    }

    /**
     * <p>
     *     Metoda zwraca barkod z pliku. Je�li plik nie jest obrazkiem zwraca <code>Optional.absent()</code>.
     * </p>
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     * @param file
     * @return barkod z obrazka
     */
    public static Optional<String> decodeBarcode(File file, Map<DecodeHintType, Object> hints) throws IOException {

        BufferedImage image = ImageIO.read(file);

        if(image == null) {
            log.warn("[decodeBarcode] file '{}' is not image", file);
            return Optional.absent();
        }

        ZxingRecognizer recognizer = new ZxingRecognizer(hints);

        return Optional.fromNullable(recognizer.recognize(image));
    }
}
