/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.compan.docusafe.service.barcodes.recognizer.xing;


import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.codec.TiffImage;
import java.io.FileOutputStream;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Document;

/**
 * Konwersja TIFa do PDFa
 * @author Dell
 */
public class TiffToPdf 
{
public static void main(String args[]){
    try{
        //Read the Tiff File
        RandomAccessFileOrArray myTiffFile=new RandomAccessFileOrArray("c:/projekty/cvswork/ocr/etc/1.tif");
        //Find number of images in Tiff file
        int numberOfPages=TiffImage.getNumberOfPages(myTiffFile);
        System.out.println("Number of Images in Tiff File" + numberOfPages);
        Document TifftoPDF=new Document();
        PdfWriter.getInstance(TifftoPDF, new FileOutputStream("c:/projekty/cvswork/ocr/etc/1.pdf"));
        TifftoPDF.open();
        //Run a for loop to extract images from Tiff file
        //into a Image object and add to PDF recursively
        for(int i=1;i<=numberOfPages;i++)
        {
            Image tempImage=TiffImage.getTiffImage(myTiffFile, i);
            TifftoPDF.add(tempImage);
        }
        TifftoPDF.close();
        System.out.println("Tiff to PDF Conversion in Java Completed" );
    }
    catch (Exception i1){
        i1.printStackTrace();
    }      
    }    
}
