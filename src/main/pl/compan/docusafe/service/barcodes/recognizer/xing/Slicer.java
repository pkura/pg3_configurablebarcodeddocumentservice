package pl.compan.docusafe.service.barcodes.recognizer.xing;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

/**
 * Obiekt ktory tnie obraz na Slice
 * @author Dell
 */
public class Slicer
{
    private int ySize = 300;
    private int yStep = 300;
    /**
     * Metoda tnie obraz na mniejsze slice - tnie na paski o calej szerokosci i wysokosci 300
     * @param image
     * @return
     */
    public List<SliceArea> getSlices(BufferedImage image)
    {
        List<SliceArea> resp = new LinkedList<SliceArea>();
        int y = 0;
        while (y+ySize<image.getHeight()-1)
        {

            //System.out.println("slice "+y);
            SliceArea slice = new SliceArea(0, y, image.getWidth()-1, y+ySize);
            resp.add(slice);
            y = y + yStep;
        }
        SliceArea slice = new SliceArea(0, image.getHeight()-yStep, image.getWidth()-1, image.getHeight()-1);
        resp.add(slice);
        return resp;
    }
}
