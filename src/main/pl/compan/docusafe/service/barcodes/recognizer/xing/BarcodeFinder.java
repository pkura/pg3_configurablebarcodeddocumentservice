/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.compan.docusafe.service.barcodes.recognizer.xing;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.service.barcodes.slicer.Slicer;

/**
 *
 * @author wojciech.kutyla
 */
public class BarcodeFinder 
{
    private final static Logger log = LoggerFactory.getLogger(BarcodeFinder.class);
    
    private boolean findByRvision = true;
    
    private boolean findByXing = true;
    
    private boolean findOnlyInFirstPage = true;
    
    /**
     * Poszukuje barkodu w pliku PDF
     * @param pdfFile
     * @return
     * @throws IOException 
     */
    public String findBarcode(File pdfFile) throws IOException
    {
        PDDocument document = PDDocument.load(pdfFile);
        List<PDPage> pDPages = document.getDocumentCatalog().getAllPages();
        try
        {        
            XingBarcodeFinder xing = new XingBarcodeFinder();
           RVisionFinder rvision = new RVisionFinder();
            int i = 0;
            int page = 0;
            for (PDPage pDPage : pDPages)
            {
                page++;
                if (findOnlyInFirstPage && page > 1)
                {
                    log.trace("pomijamy strone numer {}",page);
                    continue;
                }
                log.debug("page={}",page);
                Map<String, PDXObjectImage> images = pDPage.getResources().getImages();
                if (images.isEmpty())
                {
                    continue;
                }
                for (String imageKey : images.keySet()) 
                {
                    i++;
                    log.trace("image={}",i);
                    Slicer slicer = new Slicer();
                   // Slicer slicer = new Slicer();
                    BufferedImage image = images.get(imageKey).getRGBImage();          
                    BufferedImage bwImage = slicer.toBinaryImage(image);
                    slicer.setImage(image);
                    while (slicer.nextSlice())
                    {
                        String barcode;
                        if (findByRvision)
                        {
                            log.trace("szukamy przez rvision");
                            Rectangle rec = slicer.getSliceAsRectangle();
                            barcode = rvision.findBarcode(bwImage, rec);
                            if (barcode != null)
                            {
                                return barcode;
                            }
                        }
                        if (findByXing)
                        {
                            log.trace("szukamy przez xing");
                            barcode = xing.findBarcode(slicer.getSliceAsImage());
                            if (barcode != null)
                            {
                                return barcode;
                            }
                        }
                    }
                }
            }
        }
        finally
        {
            document.close();
        }        
        return null;
    }

    /**
     * @return the findByRvision
     */
    public boolean isFindByRvision() {
        return findByRvision;
    }

    /**
     * @param findByRvision the findByRvision to set
     */
    public void setFindByRvision(boolean findByRvision) {
        this.findByRvision = findByRvision;
    }

    /**
     * @return the findByXing
     */
    public boolean isFindByXing() {
        return findByXing;
    }

    /**
     * @param findByXing the findByXing to set
     */
    public void setFindByXing(boolean findByXing) {
        this.findByXing = findByXing;
    }

    /**
     * @return the findOnlyInFirstPage
     */
    public boolean isFindOnlyInFirstPage() {
        return findOnlyInFirstPage;
    }

    /**
     * @param findOnlyInFirstPage the findOnlyInFirstPage to set
     */
    public void setFindOnlyInFirstPage(boolean findOnlyInFirstPage) {
        this.findOnlyInFirstPage = findOnlyInFirstPage;
    }
}
