/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.compan.docusafe.service.barcodes.recognizer.xing;

import com.java4less.vision.Barcode1DReader;
import com.java4less.vision.BarcodeData;
import com.java4less.vision.RImage;
import com.java4less.vision.VisionException;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Klasa odpowiedzialna za 
 * @author user
 */
public class RVisionFinder
{
    private final static Logger log = LoggerFactory.getLogger(RVisionFinder.class);
    
    /**
     * Wyszukuje barcode w okrelsonym slice
     * @param bwImage
     * @param slice
     * @return 
     */
    public String findBarcode(BufferedImage bwImage,Rectangle slice)
    {
        Barcode1DReader reader=new Barcode1DReader();
        reader.setSymbologies(Barcode1DReader.CODE128);
        reader.setProgressListener(null);
        //BufferedImage bwImage = toBinaryImage(image);
       
        RImage rim=new RImage(bwImage,slice);	
        try
        {
            BarcodeData[] barcodes = reader.scan(rim);			                
            if(barcodes.length>0)
            {
                  for (BarcodeData b : barcodes)
                  {
                      log.debug("barcode={}",b.getValue());
                      return b.getValue();
                  }
            }
        }
        catch (VisionException e)
        {
            log.error("",e);
            return null;
        }
        catch (RuntimeException e)
        {
            log.debug("",e);
            return null;
        }
       return null;
    }
    
    
}
