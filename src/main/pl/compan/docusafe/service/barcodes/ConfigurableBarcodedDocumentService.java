package pl.compan.docusafe.service.barcodes;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.barcodes.recognizer.Recognizer;
import pl.compan.docusafe.service.barcodes.recognizer.WireBarcodeFinder;
import pl.compan.docusafe.service.barcodes.recognizer.ZxingRecognizer;
import pl.compan.docusafe.service.barcodes.slicer.ImageSlicer;
import pl.compan.docusafe.service.barcodes.slicer.ImageSlicerImpl;

import java.io.File;
import java.util.EnumSet;
import java.util.Hashtable;

/**
 * Created by kk on 2015-04-17.
 */
public class ConfigurableBarcodedDocumentService extends BarcodedDocumentService implements WireBarcodeFinder{

    private String scanFolder;
    private String attachmentNameTemplate;
    private String slicerConfig;

    public ConfigurableBarcodedDocumentService() {
        properties = new Property[] {	new PeriodSyncTimeProperty(), new ScanFolderProperty(), new AttachmentNameTemplateProperty(), new SlicerConfigProperty()};
    }

    class PeriodSyncTimeProperty extends Property {
        public PeriodSyncTimeProperty() {
            super(SIMPLE, PERSISTENT, ConfigurableBarcodedDocumentService.this, "period", "Czestotliwość synchronizacji w minutach", String.class);
        }

        protected Object getValueSpi() {
            return period;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (ConfigurableBarcodedDocumentService.this) {
                if (object != null) {
                    period = Long.parseLong((String) object);
                    stop();
                } else {
                    period = 1440L;
                }
            }
        }
    }

    class ScanFolderProperty extends Property {
        public ScanFolderProperty() {
            super(SIMPLE, PERSISTENT, ConfigurableBarcodedDocumentService.this, "scanFolder", "Folder wejściowy", String.class);
        }

        protected Object getValueSpi() {
            return scanFolder;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (ConfigurableBarcodedDocumentService.this) {
                if (object != null) {
                    scanFolder = (String) object;
                    stop();
                } else {
                    scanFolder = getServicePropertyKey("scanFolder");
                }
            }
        }
    }

    class AttachmentNameTemplateProperty extends Property {
        public AttachmentNameTemplateProperty() {
            super(SIMPLE, PERSISTENT, ConfigurableBarcodedDocumentService.this, "attachmentNameTemplateProperty", "Szablon nazw załączników", String.class);
        }

        protected Object getValueSpi() {
            return attachmentNameTemplate;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (ConfigurableBarcodedDocumentService.this) {
                if (object != null) {
                    attachmentNameTemplate = (String) object;
                    stop();
                } else {
                    attachmentNameTemplate = "Skan dokumentu nr %file_no%";
                }
            }
        }
    }

    class SlicerConfigProperty extends Property {
        public SlicerConfigProperty() {
            super(SIMPLE, PERSISTENT, ConfigurableBarcodedDocumentService.this, "slicerConfig", "Konfiguracja slicera", String.class);
        }

        protected Object getValueSpi() {
            return slicerConfig;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (ConfigurableBarcodedDocumentService.this) {
                if (object != null) {
                    String toSet = (String) object;
                    parseSlicerParams(toSet);

                    slicerConfig = toSet;
                    stop();
                } else {
//                    slicerConfig = "8; 20; 20; 14";
                	slicerConfig = getServiceProperty("slicerConfig");
                }
            }
        }
    }

    private Integer[] parseSlicerParams(String toSet) {
        Preconditions.checkNotNull(toSet, "Nieprawidłowe parametry");
        Preconditions.checkState(toSet.split(";").length == 4, "Nieprawidłowe parametry");
        Iterable<String> split = Splitter.on(";").trimResults().split(toSet);
        return FluentIterable.from(split).transform(new Function<String, Integer>() {
            @Override
            public Integer apply(String input) {
                Preconditions.checkNotNull(input, "Nieprawidłowe parametry");
                return Integer.valueOf(input);
            }
        }).toArray(Integer.class);
    }

    @Override
    protected String getScansFolder() {
    	if (scanFolder == null) {
    		scanFolder = getServiceProperty("scanFolder");
    	}
        return scanFolder;
    }

    @Override
    protected String getAttachmentColumnCn() {
        return Docusafe.getAdditionProperty("ocr.attachmentColumnCn");
    }

    @Override
    protected String getAttachmentNameTemplate() {
    	if (attachmentNameTemplate == null) {
    		attachmentNameTemplate = getServiceProperty("attachmentNameTemplate");
    	}
        return attachmentNameTemplate;
    }

    @Override
    protected String getDockindCn() {
        return Docusafe.getAdditionProperty("ocr.dockindCn");
    }

    @Override
    protected String getFinishedScansFolder() {
        return Docusafe.getAdditionProperty("ocr.finished");
    }

    @Override
    protected Recognizer getRecognizer() {
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, Lists.newArrayList(BarcodeFormat.CODE_128));
        return new ZxingRecognizer(new MultiFormatReader(), hints);
    }

    @Override
    protected Recognizer getAlternativeRecognizer() {
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.FALSE);
//        hints.put(DecodeHintType.CHARACTER_SET, "ISO-8859-1");
//        hints.put(DecodeHintType.ASSUME_GS1, true);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, Lists.newArrayList(BarcodeFormat.CODE_128));
        return new ZxingRecognizer(new MultiFormatReader(), hints);
    }

    @Override
    protected ImageSlicer getImageSlicer() {
    	if (slicerConfig == null) {
    		slicerConfig = getServiceProperty("slicerConfig");
    	}
        try {
            Integer[] params = parseSlicerParams(slicerConfig);
            return new ImageSlicerImpl(params[0], params[1], params[2], params[3]);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ImageSlicerImpl(8, 20, 20, 14);
        }
    }
    @Override
    protected String getDebugModeFolder() {
        return Docusafe.getAdditionProperty("ocr.debug-mode-folder");
    }

	@Override
	public String find(File[] files) {
		String barcode = getBarcode(files);
		return barcode;
	}
}
