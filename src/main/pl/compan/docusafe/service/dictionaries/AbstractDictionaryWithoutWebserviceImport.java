package pl.compan.docusafe.service.dictionaries;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;

import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 12.08.13
 * Time: 15:38
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractDictionaryWithoutWebserviceImport implements DictionaryImport {

    @Override
    public void initImport() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        return;
    }

    @Override
    public String getMessage() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void finalizeImport() throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void materializeView() throws EdmException, SQLException {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
