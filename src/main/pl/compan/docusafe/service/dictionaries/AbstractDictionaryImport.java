package pl.compan.docusafe.service.dictionaries;

import pl.compan.docusafe.core.EdmException;

import java.sql.SQLException;

public abstract class AbstractDictionaryImport implements DictionaryImport {
	
    public boolean isSleepAfterEachDoImport() {
        return true;
    }
	
	public String getMessage() {
		return null;
	}
	
	public void materializeView() throws EdmException, SQLException {
		return;
	}
	
	public void initImport() {
		return;
	}
	
	public void finalizeImport() throws EdmException {
		return;
	}
}
