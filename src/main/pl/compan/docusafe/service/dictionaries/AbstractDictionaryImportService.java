package pl.compan.docusafe.service.dictionaries;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;

import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 02.08.13
 * Time: 10:56
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractDictionaryImportService extends ServiceDriver implements Service {
    public static final String IMPORT_SLEEP_TIME_AFTER_EACH_REQUEST_ADDITION_PROPERTY = "import.sleep_time_after_each_request";
    private Timer timer;
    private Long period = getTimerDefaultPeriod();
    private boolean syncing = false;
    private final ImmutableList<DictionaryImport> importDictionaries = getAvailableImports();

    private Map<String,Boolean> runImports = Maps.newHashMap();
    private Property[] properties;

    /**
     *
     * @return lista mo�liwych import�w
     */
    protected abstract ImmutableList<DictionaryImport> getAvailableImports();

    /**
     *
     * @return domy�lna czestotliwo�� uruchumiania synchronizacji/timera
     */
    protected abstract long getTimerDefaultPeriod();

    /**
     *
     * @return warto�� op�nienia startu synchronizacji/timera po uruchomieniu Docusafe'a
     */
    protected abstract long getTimerStartDelay();

    class PeriodSyncTimeProperty extends Property {
        public PeriodSyncTimeProperty() {
            super(SIMPLE, PERSISTENT, AbstractDictionaryImportService.this, "period", "Czestotliwoo\u009C� synchronizacji w sekundach", String.class);
        }

        protected Object getValueSpi() {
            return period;
        }

        protected void setValueSpi(final Object object) throws ServiceException {
            synchronized (AbstractDictionaryImportService.this) {
                if (object != null) {
                    period = Long.parseLong((String) object);
                } else {
                    period = getTimerDefaultPeriod();
                }
            }
        }
    }

    class DictionaryOnOffProperty extends Property {

        protected DictionaryOnOffProperty(String id) {
            super(SIMPLE, PERSISTENT, AbstractDictionaryImportService.this, id, "Synchronizacja: "+id, Boolean.class);
        }

        protected Object getValueSpi() {
            return runImports.get(this.getId());
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (AbstractDictionaryImportService.this) {
                if (object != null) {
                    runImports.put(this.getId(), ((Boolean) object));
                } else {
                    runImports.put(this.getId(), false);
                }
            }
        }
    }

    @Override
    public Property[] getProperties() {
        return properties;
    }

    public AbstractDictionaryImportService() {
        List<Property> propertiesList = Lists.newLinkedList();
        propertiesList.add(new PeriodSyncTimeProperty());

        for (DictionaryImport imp : importDictionaries) {
            propertiesList.add(new DictionaryOnOffProperty(imp.getClass().getSimpleName()));
        }

        properties = propertiesList.toArray(new Property[]{});
    }

    @Override
    protected void start() throws pl.compan.docusafe.service.ServiceException {
        AxisClientConfigurator conf = new AxisClientConfigurator();
        for (DictionaryImport imp : importDictionaries) {
            try {
                imp.initConfiguration(conf);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }

        timer = new Timer(true);
        timer.schedule(new Import(), getTimerStartDelay(), period * DateUtils.SECOND);
    }


    @Override
    protected void stop() throws pl.compan.docusafe.service.ServiceException {
        if (timer != null)
            timer.cancel();
    }

    @Override
    protected boolean canStop() {
        return !syncing;
    }

    class Import extends TimerTask {

        @Override
        public void run() {
            boolean done;
            try {
                long sleepTimeAfterEachRequest = getSleepTimeAfterEachImport();
                DSApi.openAdmin();

                for (DictionaryImport imp : getAvailableImports()) {
                    synchronized (imp) {
                        syncing = true;
                        if (runImports.get(imp.getClass().getSimpleName()) != null && runImports.get(imp.getClass().getSimpleName())) {
                            try {

                                imp.initImport();
                                console(Console.INFO, imp.getClass().getName() + ": Rozpocz�to synchronizacj�");
                                DSApi.close();

                                do {
                                    DSApi.openAdmin();
                                    DSApi.context().begin();
                                    done = imp.doImport();
                                    if (imp.getMessage() != null) {
                                        console(Console.INFO, imp.getClass().getName() + ": "+imp.getMessage());
                                    }

                                    DSApi.context().commit();
                                    DSApi.close();
                                    if (imp.isSleepAfterEachDoImport() && sleepTimeAfterEachRequest > 0) {
                                        Thread.sleep(sleepTimeAfterEachRequest);
                                    }
                                } while (!done);

                                DSApi.openAdmin();
                                DSApi.context().begin();
                                imp.finalizeImport();
                                DSApi.context().commit();
                                DSApi.context().begin();
                                imp.materializeView();
                                DSApi.context().commit();

                                console(Console.INFO, imp.getClass().getName() + ": Zako�czono synchronizacj�");
                            } catch (Exception e) {
                                DSApi.context().rollback();
                                console(Console.ERROR, imp.getClass().getName() + ": BLAD " + e.getMessage());
                                log.error(e.getMessage(), e);
                            }
                        }
                    }
                }

            } catch (Exception e) {
                console(Console.ERROR, ": BLAD " + e.getMessage());
                log.error(e.getMessage(), e);
            } finally {
                syncing = false;
                try {
                    DSApi.close();
                } catch (EdmException e) {
                    console(Console.ERROR, ": BLAD " + e.getMessage());
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * read value from addition property, which define sleep time after each service requests
     * @return time in ms.
     */
    protected long getSleepTimeAfterEachImport() {
        if (Docusafe.getAdditionProperty(IMPORT_SLEEP_TIME_AFTER_EACH_REQUEST_ADDITION_PROPERTY) != null) {
            try {
                return Long.valueOf(Docusafe.getAdditionProperty(IMPORT_SLEEP_TIME_AFTER_EACH_REQUEST_ADDITION_PROPERTY));
            } catch (NumberFormatException e) {
            }
        }
        return 0l;
    }
}
