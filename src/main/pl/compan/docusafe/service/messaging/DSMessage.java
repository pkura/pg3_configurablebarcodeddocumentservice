package pl.compan.docusafe.service.messaging;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
/* User: Administrator, Date: 2005-10-18 13:35:27 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DSMessage.java,v 1.3 2006/02/20 15:42:28 lk Exp $
 */
public abstract class DSMessage
{
    private String messageID;
    private Date timestamp;

    private Map properties;

    protected DSMessage(String messageID, Date timestamp)
    {
        this.messageID = messageID;
        this.timestamp = timestamp;
    }

    public void acknowledge()
    {

    }

    public String getMessageID()
    {
        return messageID;
    }

    public Date getTimestamp()
    {
        return timestamp;
    }

    public void setStringProperty(String name, String value)
    {
        setProperty(name, value);
    }

    public void setIntProperty(String name, Integer value)
    {
        setProperty(name, value);
    }

    public void setLongProperty(String name, Long value)
    {
        setProperty(name, value);
    }

    private void setProperty(String name, Object value)
    {
        if (name == null)
            throw new NullPointerException("name");
        if (value == null)
            throw new NullPointerException("value");
        if (properties == null)
            properties = new HashMap();
        properties.put(name, value);
    }
}
