package pl.compan.docusafe.service.messaging;

import java.util.Date;
/* User: Administrator, Date: 2005-10-18 14:32:41 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: TextDSMessage.java,v 1.1 2005/10/18 14:39:40 lk Exp $
 */
public class TextDSMessage extends DSMessage
{
    private String text;

    TextDSMessage(String messageID, Date timestamp)
    {
        super(messageID, timestamp);
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }
}
