package pl.compan.docusafe.service.labels;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.labels.DocumentToLabelBean;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;

public class LabelsService extends ServiceDriver implements Service
{
	private Timer timer;
	//private ExecutorService exec;
	//private Lock lock = new ReentrantLock();
	
    protected void start() throws ServiceException
    {
        timer = new Timer(true);
        timer.schedule(new HandleLabels(), 1000, 1 * DateUtils.MINUTE/*30*DateUtils.SECOND*/);
    }
	protected boolean canStop()
	{
       return false;
	}
	 
	protected void stop() throws ServiceException
	{
       if (timer != null) timer.cancel();
	}
	
	class HandleLabels extends TimerTask
    {
		public void run()
        {
			try
			{
				try
				{
					log.info("Uruchamiam update etykiet");
					DSApi.openAdmin();				
					List<DocumentToLabelBean> beans = LabelsManager.getBeansForService();
					Date now = new Date();
					Label l;
					for(DocumentToLabelBean dtlb:beans)
					{
						try
						{
							DSApi.context().begin();
							if(dtlb.getActionTime()!=null&&dtlb.getActionTime().before(now))
							{
								if(log.isTraceEnabled())
									log.trace(dtlb.toString());
								l = LabelsManager.findLabelById(dtlb.getLabelId());
								if(l.getAncestorId()!=null && l.getAncestorId()>0)
								{
									LabelsManager.addLabel(dtlb.getDocumentId(), l.getAncestorId(), Label.SYSTEM_LABEL_OWNER);
								}
								dtlb.setHidding(false);
								dtlb.setActionTime(null);
								if(l.getDeletesAfter())
								{
									DSApi.context().session().delete(dtlb);
								}
								else DSApi.context().session().save(dtlb);
							}
							DSApi.context().commit();
						}
						catch(Exception e)
						{
							DSApi.context().rollback();
							log.error("LABELSERVICE 00-0034 :",e);
						}
					}
					/**
					 * EventsService
					 */
					DSApi.context().begin();
					EventFactory.startEventByHandler(null);				
					DSApi.context().commit();
					
				}
				catch(EdmException e)
				{
					log.error("",e);
					try
					{
						DSApi.context().setRollbackOnly();		
					}catch (Exception e2) {
						log.error("LABELSERVICE 00-0035 :",e2);
					}
				}
			}
			catch (Throwable  t) 
			{
				t.printStackTrace();
				log.error(t);
				log.error("LABELSERVICE 00-0037 :",t);
			}
			finally
			{
				try
				{
					DSApi._close();
				}
				catch (Exception e3) {
					log.error("LABELSERVICE 00-0036 :",e3);
				}
				//exec.shutdown();
			}
        }
    
    }

}
