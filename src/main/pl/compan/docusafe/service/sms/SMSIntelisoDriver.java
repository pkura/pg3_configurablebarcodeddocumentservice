package pl.compan.docusafe.service.sms;

import java.io.Reader;
import java.io.StringWriter;
import java.net.URL;
import java.util.Map;

import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.VelocityUtils;
import pl.inteliso.RetObject;
import pl.inteliso.SMSSenderBindingStub;
import pl.inteliso.SMSSenderLocator;


/**
 * @author Mariusz K
 */
public class SMSIntelisoDriver extends ServiceDriver
{
	public static final String NAME = "SMSDriver";
	
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(SMSIntelisoDriver.class);
    private static final Logger companLOG = pl.compan.docusafe.util.LoggerFactory.getLogger(SMSIntelisoDriver.class);

    private String username;
    private String password;
    private String from;

    private StringManager sm =
            GlobalPreferences.loadPropertiesFile(SMSIntelisoDriver.class.getPackage().getName(),null);

    private Property[] properties;

    private boolean sending;

    public SMSIntelisoDriver()
    {
        properties = new Property[]
                {
                        new UsernameProperty(),
                        new PasswordProperty(),
                        new FromProperty(),
                };
    }

    public boolean hasConsole()
    {
        return true;
    }


    public void send(String subject, Reader msg, Map<String, ?> context ) throws Exception {
        String content = applyTemplate(msg, context);
        //TODO: Dorobi� zmienna w serwisie tak jak user i haslo
    	String SMSSenderPort_address = "	http://ws.sms-api.pl/smswebservice_v2.php";
    	URL endpoint = new URL(SMSSenderPort_address);
        pl.inteliso.SMSSenderLocator service = new SMSSenderLocator();
        pl.inteliso.SMSSenderBindingStub stub = new SMSSenderBindingStub(endpoint,service);
        stub.setUsername("");
        stub.setPassword("");
        RetObject ret = stub.SMSSend(username, password, from, subject, content);
        //TODO:ddac komunikaty na konsole i do loga
        console(Console.INFO, "Wys�a� SMS na numer "+subject + " odpowied� serwera "+ret.getDescription() + " [ "+ret.getStatus()+" ]") 	;
        System.out.println(ret.getDescription());        
    }

    private String applyTemplate(Reader msg, Map<String, ?> context) throws EdmException {
        String content;StringWriter sw = new StringWriter();
        try
        {
            VelocityEngine ve = VelocityUtils.getEngine();
            ve.init();
            ve.evaluate(new VelocityContext(context), sw, "SMS", msg);
        }
        catch (Exception e)
        {
            throw new EdmException(sm.getString("BlednySzablonSMS")+e.getMessage(), e);
        }

        content = sw.toString();
        return content;
    }

    /* ServiceDriver */

    protected void start() throws ServiceException
    {
        console(Console.INFO, sm.getString("Serwis SMSDriver wystartowal"));
    }

    protected void stop() throws ServiceException
    {
        console(Console.INFO, sm.getString("Serwis SMSDriver zako�czy� dzia�anie"));
    }

    protected boolean canStop()
    {
        return !sending;
    }

    public Property[] getProperties()
    {
        return properties;
    }

    class UsernameProperty extends Property
    {
        public UsernameProperty()
        {
            super(SIMPLE, PERSISTENT, SMSIntelisoDriver.this, "username", sm.getString("NazwaUzytkownika"), String.class);
        }

        protected Object getValueSpi()
        {
            return username;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (SMSIntelisoDriver.this)
            {
                username = (String) object;
            }
        }
    }

    class PasswordProperty extends Property
    {
        public PasswordProperty()
        {
            super(SIMPLE, PERSISTENT, SMSIntelisoDriver.this, "password", sm.getString("Haslo"), String.class);
        }

        protected Object getValueSpi()
        {
            return password;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (SMSIntelisoDriver.this)
            {
                password = (String) object;
            }
        }
    }

    class FromProperty extends Property
    {
        public FromProperty()
        {
            super(SIMPLE, PERSISTENT, SMSIntelisoDriver.this, "from", sm.getString("Od"), String.class);
        }

        protected Object getValueSpi()
        {
            return from;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            from = (String) object;
        }
    }


	
}
