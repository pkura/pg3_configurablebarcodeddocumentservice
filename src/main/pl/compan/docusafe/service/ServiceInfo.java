package pl.compan.docusafe.service;

import java.util.LinkedHashMap;
import java.util.Map;

/* User: Administrator, Date: 2005-04-20 14:19:12 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ServiceInfo.java,v 1.7 2005/05/12 10:58:43 lk Exp $
 */
public class ServiceInfo
{
    /**
     * Specjalna warto�� parametru loadOnStartup, oznaczaj�ca domy�lny sterownik.
     */
    public static final String LOAD_DEFAULT = "__default__";

    private String serviceId;
    private String description;
    private String defaultDriver;
    private boolean fixed;

    private Map driverInfos = new LinkedHashMap();

    ServiceInfo(String serviceId, String description, String defaultDriver, boolean fixed)
    {
        this.serviceId = serviceId;
        this.description = description;
        this.defaultDriver = defaultDriver;
        this.fixed = fixed;
    }

    String getDefaultDriverId()
    {
        return defaultDriver;
    }

    void addDriverInfo(ServiceDriverInfo driverInfo)
    {
        driverInfos.put(driverInfo.getDriverId(), driverInfo);
    }

    ServiceDriverInfo getDriverInfo(String driverId)
    {
        return (ServiceDriverInfo) driverInfos.get(driverId);
    }

    public String getServiceId()
    {
        return serviceId;
    }

    public String getDescription()
    {
        return description;
    }

    public boolean isFixed()
    {
        return fixed;
    }

    public ServiceDriverInfo[] getDriverInfos()
    {
        return (ServiceDriverInfo[]) driverInfos.values().toArray(new ServiceDriverInfo[driverInfos.size()]);
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ServiceInfo)) return false;

        final ServiceInfo serviceInfo = (ServiceInfo) o;

        if (serviceId != null ? !serviceId.equals(serviceInfo.serviceId) : serviceInfo.serviceId != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (serviceId != null ? serviceId.hashCode() : 0);
    }

    public String toString()
    {
        return "ServiceInfo[serviceId="+serviceId+" description="+description+
            " defaultDriver="+defaultDriver+"]";
    }
}
