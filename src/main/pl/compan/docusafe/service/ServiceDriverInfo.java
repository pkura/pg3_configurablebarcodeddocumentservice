package pl.compan.docusafe.service;

/* User: Administrator, Date: 2005-04-20 13:09:39 */

/**
 * Klasa opisuj�ca sterownik us�ugi na podstawie wpisu w pliku xml.
 * Instancje s� tworzone przez ServiceManager.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ServiceDriverInfo.java,v 1.1 2005/04/25 15:34:03 lk Exp $
 */
public class ServiceDriverInfo
{
    private String serviceId;
    private String driverId;
    private String description;
    private String clazz;

    public ServiceDriverInfo(String serviceId, String driverId, String description, String clazz)
    {
        this.serviceId = serviceId;
        this.driverId = driverId;
        this.description = description;
        this.clazz = clazz;
    }

    public String getServiceId()
    {
        return serviceId;
    }

    public String getDriverId()
    {
        return driverId;
    }

    public String getDescription()
    {
        return description;
    }

    public String getClazz()
    {
        return clazz;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ServiceDriverInfo)) return false;

        final ServiceDriverInfo serviceDriverInfo = (ServiceDriverInfo) o;

        if (driverId != null ? !driverId.equals(serviceDriverInfo.driverId) : serviceDriverInfo.driverId != null) return false;
        if (serviceId != null ? !serviceId.equals(serviceDriverInfo.serviceId) : serviceDriverInfo.serviceId != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (serviceId != null ? serviceId.hashCode() : 0);
        result = 29 * result + (driverId != null ? driverId.hashCode() : 0);
        return result;
    }

    public String toString()
    {
        return "ServiceDriverInfo[driverId="+driverId+" serviceId="+serviceId+
            " description="+description+" clazz="+clazz+"]";
    }
}
