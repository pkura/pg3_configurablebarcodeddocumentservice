package pl.compan.docusafe.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.DSApi;

import java.util.Collection;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * Klasa bazowa dla sterownik�w us�ug.
 * <p>
 * Uwaga - w konstruktorze nie mo�na u�ywa� konsoli (ServiceManager.console()).
 */
public abstract class ServiceDriver implements Service
{
    protected final Log log = LogFactory.getLog(getClass());

    private static final Property[] EMPTY_PROPERTIES = new Property[0];
    private static final Command[] EMPTY_COMMANDS = new Command[0];
    private static final RuntimeProperty[] EMPTY_RUNTIME_PROPERTIES = new RuntimeProperty[0];

    private ServiceDriverInfo driverInfo;

    protected abstract void start() throws ServiceException;
    protected abstract void stop() throws ServiceException;

    /**
     * Okre�la, czy us�uga mo�e by� zatrzymana przez u�ytkownika.
     * Nawet je�eli metoda canStop() zwraca warto�� false, metoda
     * stop() nadal mo�e by� wywo�ana podczas ko�czenia pracy
     * aplikacji.
     */
    protected abstract boolean canStop();

    /**
     * Us�ugi, kt�re wysy�aj� komunikaty na konsol� powinny zaimplementowa�
     * t� us�ug� i zwraca� true, aby konsola zosta�a wy�wietlona.
     */ 
    public boolean hasConsole()
    {
        return true;
    }

    public String getDriverId()
    {
        return driverInfo.getDriverId();
    }

    public String getServiceId()
    {
        return driverInfo.getServiceId();
    }

    public Property[] getProperties()
    {
        return EMPTY_PROPERTIES;
    }

    public RuntimeProperty[] getRuntimeProperties()
    {
        return EMPTY_RUNTIME_PROPERTIES;
    }

    public Command[] getCommands()
    {
        return EMPTY_COMMANDS;
    }

    public Property getProperty(String id) throws PropertyNotFoundException
    {
        return findProperty(getProperties(), id);
    }

    protected Property findProperty(Property[] properties, String id) throws PropertyNotFoundException
    {
        for (int i=0; i < properties.length; i++)
        {
            if (properties[i].getId().equals(id))
                return properties[i];
        }
        throw new PropertyNotFoundException(id);
    }

    protected Preferences getPreferences()
    {
        return DSApi.context().systemPreferences().
            node("services/"+driverInfo.getServiceId()+"/"+driverInfo.getDriverId());
    }

    /**
     * Metoda wywo�ywana w ServiceManager po utworzeniu instancji
     * sterownika, a przed jej uruchomieniem metod� start().
     */
    final void setDriverInfo(ServiceDriverInfo driverInfo)
    {
        this.driverInfo = driverInfo;
    }

    protected final void init()
    {
        Property[] properties = getProperties();
        for (int i=0; i < properties.length; i++)
        {
            properties[i].init();
        }
    }

    protected final void console(int logLevel, String message)
    {
        ServiceManager.console(this, logLevel, message);
    }

    protected final void console(int logLevel, String message, Throwable t)
    {
        ServiceManager.console(this, logLevel, message, t);
    }

    /**
     * <p>
     *     Zwraca klucz do adds�w dla danego servicu.
     *     Np. je�li <code>serviceId = "MyService"</code>
     *     oraz <code>name = "myProperty"</code> to metoda
     *     zwr�ci <code>service.property.MyService.myProperty</code>
     * </p>
     *
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     * @param name service property name
     * @return service property key
     */
    protected final String getServicePropertyKey(String name) {
        return getServicePropertyKey(this.getServiceId(), name);
    }

    /**
     * <p>
     *     Zwraca addsa przypisanego do danego serwisu.
     *     Patrz metoda {@link #getServicePropertyKey}.
     * </p>
     *
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     * @param name nazwa parametru serwisu
     * @return warto�� parametru serwisu
     */
    protected final String getServiceProperty(String name) {
    	try {
    		String e = this.getServiceId();
    		return getServiceProperty(e, name);
		} catch (Exception e) {
			return getServiceProperty(this.getClass().getSimpleName(), name);
		}
    }

    protected final String getServicePropertyOrDefault(String name, String def) {
        return getServicePropertyOrDefault(this.getServiceId(), name, def);
    }

    /**
     * <p>
     *     Zwraca {@link #getServiceProperty(String name)} jako kolekcj�.
     * </p>
     *
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     * @param name nazwa parametru serwisu
     * @return {@link #getServiceProperty(String name)} jako kolekcj�
     */
    protected final Collection<String> getServicePropertyAsList(String name) {
        return getServicePropertyAsList(this.getServiceId(), name);
    }

    public static String getServicePropertyKey(String serviceId, String name) {
        return "service.property."+serviceId+"."+name;
    }

    public static String getServiceProperty(String serviceId, String name) {
        String additionKey = getServicePropertyKey(serviceId, name);
        return AdditionManager.getProperty(additionKey);
    }

    public static String getServicePropertyOrDefault(String serviceId, String name, String def) {
        String additionKey = getServicePropertyKey(serviceId, name);
        return AdditionManager.getPropertyOrDefault(additionKey, def);
    }

    public static Collection<String> getServicePropertyAsList(String serviceId, String name) {
        String additionKey = getServicePropertyKey(serviceId, name);
        return AdditionManager.getPropertyAsList(additionKey);
    }

}
