package pl.compan.docusafe.service.users;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.*;
import pl.compan.docusafe.core.users.ad.ActiveDirectoryManager;
import pl.compan.docusafe.parametrization.p4.P4Helper;
import pl.compan.docusafe.service.*;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.CurrentDayAction;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 *
 */
public class LdapUserImport extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(LdapUserImport.class);

	private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);
	private Property[] properties;
	private Long period = 240L;
	private String filter = "";
	private String divisionAttrName = "";
	private String emailAttrName = "mail";
	
    class LdapTimesProperty extends Property
    {
        public LdapTimesProperty()
        {
            super(SIMPLE, PERSISTENT, LdapUserImport.this, "period", "Czestotliwo�c synchronizacji w minutach", String.class);
        }

        protected Object getValueSpi()
        {
            return period;
        } 

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (LdapUserImport.this)
            {
            	if(object != null)
            	{
            		period = Long.parseLong((String) object);
            	}
            	else
            	{
            		period = 240L;
            	}
            
            }
        }
    }
    
    class MailAttrNameProperty extends Property{
        public MailAttrNameProperty(){
            super(SIMPLE, PERSISTENT, LdapUserImport.this, "emailAttrName", "emailAttrName", String.class);
        }
        protected Object getValueSpi(){
            return emailAttrName;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (LdapUserImport.this){
            	if(object == null)
            		emailAttrName = "mail";
            	else
            		emailAttrName = (String) object;
            }
        }
    }
    
    class DivisionAttrNameProperty extends Property{
        public DivisionAttrNameProperty(){
            super(SIMPLE, PERSISTENT, LdapUserImport.this, "divisionAttrName", "divisionAttrName", String.class);
        }
        protected Object getValueSpi(){
            return divisionAttrName;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (LdapUserImport.this){
            	if(object == null)
            		divisionAttrName = "divisionName";
            	else
            		divisionAttrName = (String) object;
            }
        }
    }
    
    class FilterProperty extends Property
    {
        public FilterProperty()
        {
            super(SIMPLE, PERSISTENT, LdapUserImport.this, "filter", "Filtr", String.class);
        }

        protected Object getValueSpi()
        {
            return filter;
        } 

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (LdapUserImport.this)
            {
            	filter = (String) object;
            }
        }
    }


	public LdapUserImport()
	{
		properties = new Property[] { new LdapTimesProperty() ,new FilterProperty(),new DivisionAttrNameProperty(),new MailAttrNameProperty()};
	}

	protected boolean canStop()
	{
		return false;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi LdapUserImport");
		timer = new Timer(true);
		timer.schedule(new Import(), (long) 2*1000 ,(long) period*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("StartUslugiLdapUserImport"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}

	class Import extends TimerTask
	{
		public void run()
		{
			ActiveDirectoryManager ad = null;
			DirContext context = null;
			try
			{
				console(Console.INFO, "Start");
				DSApi.openAdmin();
				console(Console.INFO, "Laczy z AD");
				ad = new ActiveDirectoryManager();
				context = ad.LdapConnectAsAdmin();
				console(Console.INFO, "Polaczyl sie z AD");
				SearchControls controls = new SearchControls(); 
				controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
				String tmpfilter = "(&(objectClass=user)(objectClass=person)(objectClass=organizationalPerson))";
				if(!StringUtils.isEmpty(filter))
				{
					tmpfilter = filter;
				}
				console(Console.INFO, "Szuka uzytkownikow");
				NamingEnumeration<SearchResult> renum = context.search("",tmpfilter, controls);
				while (renum.hasMoreElements())
				{
					try
					{
						DSApi.context().begin();
			            String divisionName = null;
						SearchResult element = (SearchResult) renum.nextElement();					
						String name = getStringAttr(element,"sAMAccountName");
			            String firstname = getStringAttr(element,"givenName");
			            String lastname = getStringAttr(element,"sn");
			            String email = null;
                        email = getStringAttr(element,emailAttrName);
			            /**Jesli nie ma to uzywamy userPrincipalName*/
			            if(StringUtils.isEmpty(email))
			            {
			            	email = getStringAttr(element,"userPrincipalName");
			            }
			            String distinguishedName = getStringAttr(element,"distinguishedName");
			            if(StringUtils.isNotEmpty(distinguishedName))
			            {
				    		String[] tab = distinguishedName.split(",");
				    		for (String el : tab)
				    		{
				    			if(el.startsWith("OU"))
				    			{
				    				divisionName = el.substring(3);
				    				break;
				    			}
				    		}
			            }			
			            if(StringUtils.isEmpty(divisionName))
			            {
			            	divisionName = "Dzia� g��wny";
			            }
						if(name != null && firstname != null && lastname != null && StringUtils.isNotEmpty(divisionName))
						{
							DSDivision division = null;
							try 
							{
								if (Docusafe.getAdditionProperty("active.directory.root.division")!=null)
								{
									division = DSDivision.findByName(Docusafe.getAdditionProperty("active.directory.root.division"));
								}
                                else if (CurrentDayAction.P_4_CURRENT_DAY)
                                {
                                    division = DSDivision.find(P4Helper.TASMOWA_GUID);
                                }
								else
								{
									division = DSDivision.findByName(divisionName);
								}
							}
							catch (DivisionNotFoundException e) {
                                log.trace("DivisionNotFoundException: divisionName = {} ", divisionName);
                                DSDivision parent = DSDivision.find(DSDivision.ROOT_GUID);
								division = parent.createDivision(divisionName,"NEW");
								DSApi.context().commit();
								DSApi.close();
								DSApi.openAdmin();
								DSApi.context().begin();
								DSApi.context().session().refresh(division);
							}
							DSUser user = null;
							console(Console.INFO, "Szuka uzytkownika");
                            if (AvailabilityManager.isAvailable("p4.ldap.emailNotFromLdap"))
                            {
                                email = new StringBuilder(firstname.toLowerCase()).append(".").append(lastname.toLowerCase()).append("@play.pl").toString();
                            }
							try
							{
								user = DSUser.findByUsername(name);
								console(Console.INFO, "Znalazl "+ user.asFirstnameLastname());
								user.setFirstname(firstname);
								user.setLastname(lastname);
								user.setEmail(email);
								user.update();
							}
							catch (UserNotFoundException e)
							{
								console(Console.INFO, "Nie znalazl dodaje");
								user = UserFactory.getInstance().createUser(name, firstname, lastname);
								user.setEmail(email);
								user.setCtime(new Timestamp(new Date().getTime()));
								user.setAdUser(true);
								user.setLoginDisabled(true);								
								console(Console.INFO, "Dodal "+user.asFirstnameLastname());
							}
							DSApi.context().session().save(user);
							division.addUser(user);
						}
						else
						{
							throw new EdmException("Brak podstawowych parametr�w "+name+","+firstname+","+lastname+","+email);
						}
						DSApi.context().commit();
					}
					catch (Exception e) 
					{
						LdapUserImport.log.error("",e);
						console(Console.ERROR, "B��d [ "+e+" ]");
						DSApi.context().rollback();
					}
				}
			}
			catch(Exception e)
			{
				LdapUserImport.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			finally
			{
				
				log.trace("Import  STOP");
				console(Console.INFO, "Koniec synchronizacji");
				if(context != null)
					try {
						context.close();
					} catch (NamingException e) {
						log.error(e.getMessage(),e);
					}
				DSApi._close();
			}
		}
	}
	
	private String getStringAttr(SearchResult element,String name) throws NamingException
	{
		if(element.getAttributes().get(name) != null)
		{
			return (String) element.getAttributes().get(name) .get();
		}
		return null;
	}

	public Property[] getProperties() {
		return properties;
	}

	public void setProperties(Property[] properties) {
		this.properties = properties;
	}
}
