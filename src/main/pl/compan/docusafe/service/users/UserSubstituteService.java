package pl.compan.docusafe.service.users;

import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.SqlUserFactory;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class UserSubstituteService extends ServiceDriver implements Service
{
	private Timer timer;
	
    protected void start() throws ServiceException
    {
        timer = new Timer(true);
        timer.schedule(new UpdateSubstitute(), 1000, 8 * DateUtils.HOUR/*30*DateUtils.SECOND*/);
    }
  
	protected boolean canStop()
	{
       return false;
	}
	 
	protected void stop() throws ServiceException
	{
       if (timer != null) timer.cancel();
	}
	
	class UpdateSubstitute extends TimerTask
    {
		public void run()
        {
			Calendar cal = Calendar.getInstance();
			log.debug("Jest godzina "+cal.get(Calendar.HOUR_OF_DAY));
			if (cal.get(Calendar.HOUR_OF_DAY) == 1)
			{
				console(Console.INFO, "Jest godzina "+cal.get(Calendar.HOUR_OF_DAY) + "uruchamiam update");
				log.debug("Jest godzina "+cal.get(Calendar.HOUR_OF_DAY) + "uruchamiam update");
				try
				{
					DSApi.openAdmin();
					DSApi.context().begin();
					log.error("ODPALAM RELOAD SUBSTITUTE");
					SqlUserFactory.reloadSubstitute();
					DSApi.context().commit();
				}
				catch (Throwable  t) 
				{
					t.printStackTrace();
					log.error(t);
					log.error("UserSubstituteService 00-00123 :",t);
					/**Skoro padl uruchamiam go ponowanie*/
					timer = new Timer(true);
				    timer.schedule(new UpdateSubstitute(), 1000, 8 * DateUtils.HOUR/*30*DateUtils.SECOND*/);
				}
				finally
				{
					try
					{
						DSApi._close();
					}
					catch (Exception e3) {
						log.error("UserSubstituteService 00-00124 :",e3);
					}
				}
			}
			else
			{
				log.debug("Jest godzina "+cal.get(Calendar.HOUR_OF_DAY) + "nie uruchamiam update");
			}
        }
    
    }

}
