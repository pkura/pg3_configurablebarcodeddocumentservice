package pl.compan.docusafe.service;

/* User: Administrator, Date: 2005-04-25 11:24:34 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Command.java,v 1.1 2005/04/25 15:34:02 lk Exp $
 */
public abstract class Command
{
    private ServiceDriver driver;
    private String id;
    private String name;

    protected Command(ServiceDriver driver, String id, String name)
    {
        this.driver = driver;
        this.id = id;
        this.name = name;
    }

    public ServiceDriver getDriver()
    {
        return driver;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public abstract boolean execute() throws ServiceException;
}
