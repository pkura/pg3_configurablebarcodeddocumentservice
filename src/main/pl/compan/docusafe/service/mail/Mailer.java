package pl.compan.docusafe.service.mail;

import org.apache.commons.mail.Email;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.mail.MailMessageType;
import pl.compan.docusafe.service.Service;

import javax.mail.MessagingException;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Map;
import java.io.File;
import java.io.Reader;
import java.util.Set;

/* User: Administrator, Date: 2005-05-11 10:46:09 */

/**
 * Wysy�anie maila jako napisu, do jednego odbiorcy.
 * Wysy�anie maila z za��cznikiem.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Mailer.java,v 1.8 2010/01/27 10:32:18 mariuszk Exp $
 */
public interface Mailer extends Service
{
    public static final String NAME = "mailer";

    /**
     * Tworzy emaila o tre�ci typu text/html b�d� text/plain (parametr emailContentType -> patrz sta�e EmailConstants) po czym go wysy�a.
     * @param subject Temat listu.  Je�eli null, tre�� maila musi rozpoczyna� si� od
     * "Subject: xxx\n", gdzie xxx jest tematem listu.
     */
    void send(String to, String email, String subject, String msg, String emailContentType) throws EdmException;

    /**
     * Wysy�a email.
     * @param subject Temat listu.  Je�eli null, tre�� maila musi rozpoczyna� si� od
     * "Subject: xxx\n", gdzie xxx jest tematem listu.
     */
    void send(String to, String email, String subject, String msg) throws EdmException;

    /**
     * Wysy�a mail, traktuj�c jego tre�� jako szablon Velocity.  Przed wys�aniem
     * szablon jest wykonywany przy wykorzystaniu kontekstu zawartego w parametrze
     * context.
     */
    void send(String to, String email, String subject, String msg, Map<String, ?> context) throws EdmException;

    /**
     * Wysy�a mail, traktuj�c jego tre�� jako szablon Velocity.  Przed wys�aniem
     * szablon jest wykonywany przy wykorzystaniu kontekstu zawartego w parametrze
     * context.
     */
    void send(String to, String email, String subject, Reader msg, Map<String, ?> context) throws EdmException;

    /**
     * Wysy�a mail, traktuj�c jego tre�� jako szablon Velocity.  Przed wys�aniem
     * szablon jest wykonywany przy wykorzystaniu kontekstu zawartego w parametrze
     * context.
     */
    void send(String to, String email, String subject, Reader msg, Map<String, ?> context,boolean checkIsBlocked) throws EdmException;

    /**
     * Tworzy emaila o tre�ci typu text/html b�d� text/plain (parametr emailContentType -> patrz sta�e EmailConstants) po czym go wysy�a na adres odbiorcy z kana�u email
     * @param subject Temat listu.  Je�eli null, tre�� maila musi rozpoczyna� si� od
     * @param emailChannel kana� wp�ywu e-maila, na podstawie kt�rego pozyskiwane s� informacje o odbiorcy wiadomo�ci
     * "Subject: xxx\n", gdzie xxx jest tematem listu.
     */
    void send(String toEmail, String toPersonal, String subject, String msg, boolean checkIsBlocked, String emailContentType, DSEmailChannel emailChannel, Collection<File> attachedFiles) throws EdmException;

    Email send(String toEmail, Collection<String> toCc, Collection<String> toBcc, String toPersonal, String subject, String msg, boolean checkIsBlocked, String emailContentType, DSEmailChannel emailChannel, Collection<File> attachedFiles) throws EdmException;

    MailMessage send(String toEmail, Collection<String> toCc, Collection<String> toBcc, String toPersonal, String subject, String msg, boolean checkIsBlocked, String emailContentType, DSEmailChannel emailChannel, Collection<File> attachedFiles, MailMessageType messageType, Long contractorId) throws EdmException, IOException, MessagingException;

    void send(String toEmail, String toPersonal, String subject, String msg, Iterable<Attachment> attachments) throws EdmException;

    void send(String toEmail, String toPersonal, String subject, Reader msg, Map<String, ?> context, boolean checkIsBlocked, Iterable<Attachment> attachments) throws EdmException ;

    void send(String toEmail, String userEmail, String fromName, String subject, String msg, String aPath) throws EdmException;

    static class Attachment {
        final String name;
        final URL url;

        public Attachment(String name, URL url) {
            this.name = name;
            this.url = url;
        }
    }
   
    void send(String[] toEmail, String[] emailCC, String[] emailBCC, String subject, String msg, Iterable<Attachment> attachments) throws EdmException;

    
    /**
     * Wysy�a maila do odbiorcy  na email okreslony na formatce zawierajac w mailu zalaczniki o ile zostaly dodane do dokumentu 
     * 
     * @param mailto
     * @param subject
     * @param message
     * @param attachments
     * @throws EdmException 
     */
	void send(String mailto, String subject, String message, Collection<pl.compan.docusafe.service.mail.Mailer.Attachment> attachments) throws EdmException;

	void send(String[] toEmails,String subject, String[] emailCC, String[] emailBCC, Reader msg, Map<String, ?> params
			) throws EdmException;
}