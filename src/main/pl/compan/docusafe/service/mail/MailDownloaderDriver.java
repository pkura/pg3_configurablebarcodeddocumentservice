package pl.compan.docusafe.service.mail;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.MessageManager;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
/* User: Administrator, Date: 2006-12-06 13:17:28 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class MailDownloaderDriver extends ServiceDriver implements Service
{
	private static final Logger log = LoggerFactory.getLogger("kamilj");
	
    private String pop3Host;
    private String pop3Username;
    private String pop3Password;

    private Property[] properties;

    private Timer timer;
    
    private final static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);

    public MailDownloaderDriver()
    {
    	if(!AvailabilityManager.isAvailable("UTP.wlacz.ukryjKanal")){
    		properties = new Property[]
    				{
    				new Pop3HostProperty(),
    				new Pop3UsernameProperty(),
    				new Pop3PasswordProperty(),
    				};
    	} else {
    		properties = new Property[]
    				{ };
    	}
    }

    protected void start() throws ServiceException
    {
        // us�uga dzia�a wy��cznie gdy w��czone ADDITION_MAIL
        if (!Configuration.additionAvailable(Configuration.ADDITION_MAIL)) {
        	log.debug("MailDownloaderDriver: ADDITION_MAIL not enabled");
        	return;
        }
            
        timer = new Timer(true);
        timer.schedule(new DownloadMail(), 2*1000, 1*DateUtils.MINUTE);
    }

    protected void stop() throws ServiceException
    {
        if (timer != null) timer.cancel();
    }

    protected boolean canStop()
    {
        return false;
    }

    public Property[] getProperties()
    {
        return properties;
    }

    public boolean hasConsole()
    {
        return true;
    }

    class DownloadMail extends TimerTask
    {
        public void run()
        {	
            try
            {
            	DSContext ctx = DSApi.openAdmin();
            	// pobranie wszystkich kana��w mailowych
	            List<DSEmailChannel> channels = MessageManager.getEnabledChannels();
	            if(channels == null)
	            	return;
	            	
            	for(DSEmailChannel emailChannel : channels)
            	{
            		pop3Host = emailChannel.getHost();
            		pop3Username = emailChannel.getUser();
            		pop3Password = emailChannel.getPassword();
            		
                    try
                    {
                       // DSApi.openAdmin();
                        
                        console(Console.INFO, sm.getString(
                			"LaczenieZHostem") + ": " + emailChannel.getHost() + "; " +
                			emailChannel.getUser());
                        console(Console.INFO, sm.getString("ZaczetoPobieracPoczte"));
                        // pobranie wiadomo�ci
                        int msgCount = MessageManager.downloadMessagesFromEmailChannel(emailChannel);
                        console(Console.INFO, sm.getString("PobranoWiadomosc", msgCount));
                        console(Console.INFO, sm.getString("SkonczonoPobieracPoczte"));
                    }
                    catch (EdmException e)
                    {
                        console(Console.ERROR, e.getMessage(), e);
                    }
                                      
            	}	            
            }
            catch(EdmException e)
            {
            	console(Console.ERROR, e.getMessage(), e);
            }
            finally
            {
                DSApi._close();
            } 
        }
    }


    class Pop3HostProperty extends Property
    {
        public Pop3HostProperty()
        {
            super(SIMPLE, PERSISTENT, MailDownloaderDriver.this, "pop3Host", sm.getString("SerwerPOP3"), String.class);
        }

        protected Object getValueSpi()
        {
            return pop3Host;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (MailDownloaderDriver.this)
            {
                pop3Host = (String) object;
            }
        }
    }

    class Pop3UsernameProperty extends Property
    {
        public Pop3UsernameProperty()
        {
            super(SIMPLE, PERSISTENT, MailDownloaderDriver.this, "pop3Username", sm.getString("NazwaUzytkownikaPOP3"), String.class);
        }

        protected Object getValueSpi()
        {
            return pop3Username;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (MailDownloaderDriver.this)
            {
                pop3Username = (String) object;
            }
        }
    }

    class Pop3PasswordProperty extends Property
    {
        public Pop3PasswordProperty()
        {
            super(SIMPLE, PERSISTENT, MailDownloaderDriver.this, "pop3Password", sm.getString("HasloUzytkownikaPOP3"), String.class);
        }

        protected Object getValueSpi()
        {
            return pop3Password;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (MailDownloaderDriver.this)
            {
                pop3Password = (String) object;
            }
        }
    }
}
