package pl.compan.docusafe.service.mail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.mail.MessageAttachment;
import pl.compan.docusafe.core.mail.MessageManager;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.prosika.Prosikalogic;
import pl.compan.docusafe.parametrization.prosika.barcodes.BarcodeHandler;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;

public class ChannelMailProcessor_ extends ServiceDriver implements Service
{

	private Property[] properties;
	private List<DSEmailChannel> channels;
	private List<MailMessage> messages;
	private static Logger log = LoggerFactory.getLogger(ChannelMailProcessor_.class);
	
	private Timer timer;
	    
	private final static StringManager sm = 
	        GlobalPreferences.loadPropertiesFile(ChannelMailProcessor_.class.getPackage().getName(),null);

	protected boolean canStop() 
	{
		return false;
	}

	protected void start() throws ServiceException 
	{
		timer = new Timer(true);
        timer.schedule(new ProcessMail(), 2*1000, 1*DateUtils.MINUTE);
	}

	protected void stop() throws ServiceException 
	{

	}
	
	class ProcessMail extends TimerTask
	{
	    public void run()
	    {
	    	try
	    	{
	    		DSContext ctx = DSApi.openAdmin();
	    		channels = MessageManager.getChannels();
	    		messages = MailMessage.getUnprocessed();
	    		for(MailMessage mm : messages)
	    		{
	    			for(DSEmailChannel ec : channels)
	    			{
	    				if(ec!=null&&mm!=null&&ec.fitsConditions(mm))
	    				{
	    					try
	    					{
	    						
		    					if(createProsikaDocFromMail(mm, ec))
		    					{
			    					if(!DSApi.isContextOpen())
			    						ctx = DSApi.openAdmin();
			    					//System.out.println(mm.getProcessed());
			    					MailMessage.markAsProcessed(mm.getId());
			    					break;
		    					}
		    					
	    					}
	    					catch(EdmException e)
	    					{
	    						DSApi.context().setRollbackOnly();
	    					}
	    					catch(IllegalStateException e)
	    					{
	    						log.error("",e);
	    					}
	    				}
	    				
	    			}
	    		}
	    		
	    		
	    	}
	    	catch(Exception e)
	    	{
	    		log.debug("Blad uslugi ChannelMailDownloader",e);
	    	}
	    	finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
	    }
	}
	
	public static Boolean createProsikaDocFromMail(MailMessage mm, DSEmailChannel channel)
	{
		InOfficeDocument doc = new InOfficeDocument();
      	
      	doc.setOriginal(false);
	      
	    Sender sender = new Sender();
		     
		  sender.setTitle(TextUtils.trimmedStringOrNull(mm.getFrom(), 30));
		  

		  doc.setSender(sender);
	      doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
	      doc.setDivisionGuid(DSDivision.ROOT_GUID);
	      doc.setCurrentAssignmentAccepted(Boolean.FALSE);
      
      

	      Long journalId;
	      
	      		
      try
      {
    	  DSApi.openAdmin();
    	  String bar = channel.getNextBarcode();
    	  log.trace("przydzielono barcode {}",bar);    	  
    	  
          DSApi.context().begin();

         
          String content = "";
          Properties props = new Properties();
          Session session = Session.getDefaultInstance(props, null);

          InputStream input = mm.getBinaryStream();
          MimeMessage message = new MimeMessage(session, input);
          input.close();


          if (message.getContent() instanceof Multipart)
          {
              Multipart mp = (Multipart) message.getContent();

	            for (int j=0, m = mp.getCount(); j < m; j++) {
	                Part part = mp.getBodyPart(j);

                  String disposition = part.getDisposition();
                  if ((disposition != null) &&
	                    ((disposition.equals(Part.ATTACHMENT) ||
	                    (disposition.equals(Part.INLINE)))))
	              {
                	  
                  }
                  if (part.getContent() instanceof String)
                      content = (String) part.getContent();
                  
              }
          }
          else
          {
              if (message.getContent() instanceof String)
                  content = (String) message.getContent();
          }
          
          
          
          doc.setDocumentDate(new Date());

          DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.PROSIKA);                

          doc.setSummary(channel.getKolejka());

          InOfficeDocumentKind kind = InOfficeDocumentKind.list().get(0);
          doc.setKind(kind);

          Calendar currentDay = Calendar.getInstance();
          currentDay.setTime(GlobalPreferences.getCurrentDay());


          Journal journal = Journal.getMainIncoming();

          journalId = journal.getId();

          doc.setCreatingUser(DSApi.context().getPrincipalName());
          doc.setIncomingDate(currentDay.getTime());

          doc.setAssignedDivision(DSDivision.ROOT_GUID);
          // referent nie jest na razie przydzielany
          //doc.setClerk(DSApi.context().getDSUser().getName());

          Calendar cal = Calendar.getInstance();
          cal.add(Calendar.DATE, DateUtils.getBusinessDays(kind.getDays()));

          doc.create();        
          doc.setStatus(InOfficeDocumentStatus.findByCn("PRZYJETY"));     
                                  
          doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(DSApi.context().getPrincipalName(), AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION));
          
          doc.setDocumentKind(documentKind);

          Long newDocumentId = doc.getId();

          Map<String,Object> values = new HashMap<String,Object>();
          
          if(bar==null) return false;
          values.put(Prosikalogic.BARCODE_FIELD_CN, bar);
          if("email".equalsIgnoreCase(channel.getContentType()))
              values.put(Prosikalogic.SPOSOB_PRZYJECIA_CN, 20);
          else
        	  values.put(Prosikalogic.SPOSOB_PRZYJECIA_CN, 10);
          //values.put(Prosikalogic.ZRODLO_PISMA_NIET_FIELD_CN, channel.getKolejka());
          documentKind.setOnly(newDocumentId, values);
          

          //documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
          //documentKind.logic().documentPermissions(doc);  
          DSApi.context().session().save(doc);
          DSApi.context().session().flush();
          
         // System.out.println(channel.getContentType());
          if("email".equalsIgnoreCase(channel.getContentType()))
          {
        	  
        	  //System.out.println("email");
        	  File file = File.createTempFile("Content", ".txt");
        	  FileWriter fw = new FileWriter(file);
        	  fw.write(content);
        	  fw.close();
        	  
        	  Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull(mm.getSubject(), 254)!=null?(TextUtils.trimmedStringOrNull(mm.getSubject(), 254)):"Attachment");
              doc.createAttachment(attachment);
              AttachmentRevision ar = attachment.createRevision(file);
              ar.setOriginalFilename(mm.getSubject());
              
              if(Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM)
            	  ar.setOnDisc(AttachmentRevision.FILE_ON_DISC);
              else
            	  ar.setOnDisc(AttachmentRevision.FILE_IN_DATABASE);
          }
          int i=0;
          log.trace("tworzymy zalaczniki");
          for(MessageAttachment ma : mm.getAttachments())
          {        	  
        	  i++;        	        	  
        	  log.trace("zalacznik numer {}",i);
        	  File enriched;
        	  
        	  if(ma.getContentType()!=null && ma.getContentType().startsWith("image/tif"))
        	  {
        		  File temp;
        		  temp = FileUtils.fileFromBlobInputStream(ma.getBinaryStream(), ma.getFilename());
        		  enriched = BarcodeHandler.enrichTiffWithBarcodeReturnsFile(temp, bar, DateUtils.formatCommonDateTime(new Date()));
        	  }
        	  else
        	  {
        		  enriched = FileUtils.fileFromBlobInputStream(ma.getBinaryStream(), bar+"_"+i);
        	  }        	  
        	  String attachmentTitle = TextUtils.trimmedStringOrNull(bar+"_"+i, 254);
        	  if (attachmentTitle==null || attachmentTitle.trim().length()==0)
        	  {
        		  log.warn("nie udalo sie utworzyc nazwy zalacznika w dokumentcie id={}",doc.getId());
        		  attachmentTitle = "Attachment";
        	  }  
        	  log.debug("attachmentTitle = {}",attachmentTitle);
        	  Attachment attachment = new Attachment(attachmentTitle);
              doc.createAttachment(attachment);
              AttachmentRevision ar = attachment.createRevision(enriched);
              
              //ar.setOriginalFilename(ma.getFilename());
              
              if(Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM)
            	  ar.setOnDisc(AttachmentRevision.FILE_ON_DISC);
              else
            	  ar.setOnDisc(AttachmentRevision.FILE_IN_DATABASE);
          }
          DSApi.context().commit();
      }
      catch (Throwable e)
      {
    	  log.error(e.getMessage(), e);
          DSApi.context().setRollbackOnly();          
          
          return false;
      }
      finally
      {
          try { DSApi.close(); } catch (Exception e) { }
      }
      
      Integer sequenceId;
      
      try
      {
          sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date());
      }
      catch (EdmException e)
      {
          log.debug("",e);
          return false;
      }
      
      
      
      
      
      try
      {
	    	  DSApi.openAdmin();
	
	          DSApi.context().begin();
	          //System.out.println(doc.getId());
	          DSApi.context().session().refresh(doc);
	          doc.bindToJournal(journalId, sequenceId);
	
	          //WorkflowFactory.createNewProcess(doc, true);
	          WorkflowFactory.createNewProcess(doc, true, "Pismo przyj�te", "admin", DSDivision.ROOT_GUID, "admin");
	          doc.getDocumentKind().logic().onStartProcess(doc);
	
	          DSApi.context().commit();
      }
      catch (EdmException e)
      {
	    	  DSApi.context().setRollbackOnly();
	    	  log.error("",e);
	          return false;
      }
      finally
      {
          	try { DSApi.close(); } catch (Exception e) { }
      }
      return true;
      
	}

}
