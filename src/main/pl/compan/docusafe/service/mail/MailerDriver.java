package pl.compan.docusafe.service.mail;

import java.io.*;
import java.util.*;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.*;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.directwebremoting.WebContextFactory;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.mail.*;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.VelocityUtils;

import com.google.common.base.Preconditions;

import pl.compan.docusafe.web.mail.MailMessageAction;

/* User: Administrator, Date: 2005-05-11 15:25:20 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: MailerDriver.java,v 1.22 2010/02/23 07:32:16 mariuszk Exp $
 */
public class MailerDriver extends ServiceDriver implements Mailer
{
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MailerDriver.class);
    private static final Logger companLOG = pl.compan.docusafe.util.LoggerFactory.getLogger(MailerDriver.class);
    private static final String EMAIL_CHARSET = "iso-8859-2";

    private String smtpServer;
    private int smtpPort = 25;
    private boolean smtpAuth;
    private boolean smtpSsl;
    private boolean smtpTls;
    private String smtpUsername;
    private String smtpPassword;
    private String fromEmail;

    private StringManager sm =
            GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);

    private Property[] properties;

    //private Session session;
    /**
     * Transport SMTP.  Zmiennej przypisywana jest warto�� null po
     * zmianie ka�dego parametru maj�cego wp�yw na dane wysy�ki
     * (np. nazwa serwera).
     */
    //private Transport transport;

    private boolean sending;

    public MailerDriver()
    {
        properties = new Property[]
                {
                        new SmtpServerProperty(),
                        new SmtpPortProperty(),
                        new SmtpSSLProperty(),
                        new SmtpTLSProperty(),
                        new SmtpAuthProperty(),
                        new SmtpUsernameProperty(),
                        new SmtpPasswordProperty(),
                        new FromEmailProperty(),
                };
    }

    public boolean hasConsole()
    {
        return true;
    }

    /**
     * Sprawdza czy uzytkownik ma zablokowane konto. Je�eli konto nie istnieje to zwrca false.
     * (Nie powinny byc wysylane powiadomienia do takich osob)
     * @param userMail adres e-mail uzytkownika
     * @return czy zablokowany
     */
    private boolean checkIsBlocked(String userMail)  throws EdmException {
        DSUser user = null;
        try {
            user =  DSUser.findByEmail(userMail);
        } catch (UserNotFoundException ex) {
            // nie trzeba obslugiwac
        }
        return user == null ? false : user.isLoginDisabled();
    }

    public void send(String toEmail, String toPersonal, String subject, String msg, Iterable<Attachment> attachments) throws EdmException
    {
        MultiPartEmail email = new MultiPartEmail();
        try
        {
            email.addTo(toEmail, toPersonal);
            email.setFrom(fromEmail);
            email.setMsg(msg);
            email.setSubject(subject);
            if (attachments!=null)
            {
                for(Attachment att: attachments){
                    EmailAttachment  attachment = new EmailAttachment();
                    attachment.setURL(att.url);
                    attachment.setDisposition(EmailAttachment.ATTACHMENT);
                    attachment.setDescription(att.name);
                    attachment.setName(att.name);
                    email.attach(attachment);
                }
            }
            send(email);
        }
        catch(EmailException e)
        {
            throw new EdmException(sm.getString("NiePowiodloSieWyslaniePoczty"), e);
        }
    }

    public void send(String toEmail, String toPersonal, String subject, Reader msg,Map<String, ?> context,boolean checkIsBlocked, Iterable<Attachment> attachments) throws EdmException {
        Preconditions.checkArgument(!StringUtils.isBlank(toEmail), "email cannot be blank");
        if (checkIsBlocked && checkIsBlocked(toEmail)) {
            console(Console.ERROR, "Uzytkownik"+toEmail+" ma zablokowane logowanie nie wysyam maila");
            return;
        }
        String content = applyTemplate(msg, context);

        final int nl;
        if (subject == null && (nl = content.indexOf('\n')) > 0 && content.startsWith("Subject:")){
            subject = content.substring("Subject:".length(), nl).trim();
            content = content.substring(nl).trim();
        }

        if (subject == null){
            throw new EdmException(sm.getString("NiePodanoTematuWysylanegoListu"));
        }

        MultiPartEmail email = new MultiPartEmail();
        try {
            email.addTo(toEmail, toPersonal);
            email.setFrom(fromEmail);
            email.setMsg(content);
            email.setSubject(subject);
            if (attachments!=null)
            {
                for(Attachment att: attachments){
                    EmailAttachment  attachment = new EmailAttachment();
                    attachment.setURL(att.url);
                    attachment.setDisposition(EmailAttachment.ATTACHMENT);
                    attachment.setDescription(att.name);
                    attachment.setName(att.name);
                    email.attach(attachment);
                }
            }
            send(email);
        } catch (EmailException e) {
            throw new EdmException(sm.getString("NiePowiodloSieWyslaniePoczty"), e);
        }
    }

    public void send(String toEmail, String toPersonal, String subject, Reader msg,Map<String, ?> context,boolean checkIsBlocked) throws EdmException
    {
        if (checkIsBlocked && checkIsBlocked(toEmail))
        {
            console(Console.ERROR, "Uzytkownik"+toEmail+" ma zablokowane logowanie nie wysyam maila");
            return;
        }
        String content = applyTemplate(msg, context);

        send(toEmail, toPersonal, subject, content, checkIsBlocked, EmailConstants.TEXT_PLAIN, null, null);
    }

    private String applyTemplate(Reader msg, Map<String, ?> context) throws EdmException {
        String content;StringWriter sw = new StringWriter();
        try
        {
            VelocityEngine ve = VelocityUtils.getEngine();
            ve.init();
            ve.evaluate(new VelocityContext(context), sw, "Mailer", msg);
        }
        catch (Exception e)
        {
            throw new EdmException(sm.getString("BlednySzablonPoczty")+e.getMessage(), e);
        }

        content = sw.toString();
        return content;
    }

    public void send(String toEmail, String toPersonal, String subject, Reader msg,
                     Map<String, ?> context) throws EdmException
    {
        send(toEmail,toPersonal,subject,msg,context,true);
    }

    public void send(String toEmail, String toPersonal, String subject, String msg, Map<String, ?> context) throws EdmException
    {
        send(toEmail, toPersonal, subject, msg, context,true);
    }

    public void send(String toEmail, String toPersonal, String subject, String msg, Map<String, ?> context,boolean checkIsBlocked) throws EdmException
    {
        if (checkIsBlocked && checkIsBlocked(toEmail))
        {
            console(Console.ERROR, "Uzytkownik"+toEmail+" ma zablokowane logowanie nie wysyam maila");
            return;
        }
        StringWriter sw = new StringWriter(msg.length() * 2);
        try
        {
            VelocityEngine ve = new VelocityEngine();
            ve.init();
            ve.evaluate(new VelocityContext(context), sw, "Mailer", msg);
        }
        catch (Exception e)
        {
            throw new EdmException(sm.getString("BlednySzablonPoczty")+e.getMessage(), e);
        }

        send(toEmail, toPersonal, subject, sw.toString());
    }

    public void send(String toEmail, String toPersonal, String subject, String msg) throws EdmException
    {
        send(toEmail, toPersonal, subject, msg, true, EmailConstants.TEXT_PLAIN, null, null);
    }

    @Override
    public void send(String toEmail, String toPersonal, String subject, String msg, String emailContentType) throws EdmException{
        send(toEmail, toPersonal, subject, msg, true, emailContentType, null, null);
    }

    @Override
    public MailMessage send(String toEmail, Collection<String> toCc, Collection<String> toBcc, String toPersonal, String subject, String msg, boolean checkIsBlocked, String emailContentType, DSEmailChannel emailChannel, Collection<File> attachedFiles, MailMessageType messageType, Long contractorId) throws EdmException, IOException, MessagingException {
        Email emailMessage = send(toEmail, toCc, toBcc, toPersonal, subject, msg, checkIsBlocked, emailContentType, emailChannel, attachedFiles);
        return MessageManager.saveMessage(emailChannel, emailMessage.getMimeMessage(), messageType, toEmail, toCc, toBcc, contractorId);
    }
    
    @Override
    public Email send(String toEmail, Collection<String> toCc, Collection<String> toBcc, String toPersonal, String subject, String msg, boolean checkIsBlocked, String emailContentType, DSEmailChannel emailChannel, Collection<File> attachedFiles) throws EdmException {
        Email emailMessage = null;

        if (checkIsBlocked && checkIsBlocked(toEmail))
        {
            console(Console.ERROR, "Uzytkownik "+toEmail+" ma zablokowane logowanie nie wysylam maila");
            return emailMessage;
        }
        log.trace("send to: {},{}",toEmail,toPersonal);
        companLOG.trace("send to: {},{}",toEmail,toPersonal);
        if (StringUtils.isBlank(toEmail))
        {
            log.warn("Bledny adres email {}",toEmail);
            companLOG.warn("Bledny adres email {}",toEmail);
            throw new EdmException(sm.getString("NiePowiodloSieWyslaniePoczty"));
        }

        if(StringUtils.isBlank(fromEmail)){
            log.error("Bledny adres email wysy�aj�cy {}",toEmail);
            companLOG.warn("Bledny adres email wysy�aj�cy {}",toEmail);
            return emailMessage;
        }

        final int nl;
        if (subject == null && (nl = msg.indexOf('\n')) > 0 && msg.startsWith("Subject:"))
        {
            subject = msg.substring("Subject:".length(), nl).trim();
            msg = msg.substring(nl).trim();
        }

        if(subject == null){
            throw new EdmException(sm.getString("NiePodanoTematuWysylanegoListu"));
        }

        try
        {
            if(isHTMLContent(emailContentType)){
                emailMessage = new HtmlEmail();
                // pod spodem typ zawartosci jest stalany na EmailConstans.TEXT_HTML
                ((HtmlEmail)emailMessage).setHtmlMsg(msg);
                if(attachedFiles != null){
                    attachFiles((HtmlEmail)emailMessage, attachedFiles);
                }
            }else{
                emailMessage = new SimpleEmail();
                // ustawia tresc wiadomosci na msg i typ zawartosci na EmailConstants.TEXT_PLAIN;
                emailMessage.setMsg(msg);
            }

            /*
                Ustawienie odbiorc�w wiadomo�ci
            */
            emailMessage.addTo(toEmail, toPersonal);

            if(toCc != null){
                for(String ccEmailAddress : toCc){
                    emailMessage.addBcc(ccEmailAddress);
                }
            }

            if(toBcc != null){
                for(String bccEmailAddress : toBcc){
                    emailMessage.addBcc(bccEmailAddress);
                }
            }

            if(emailChannel != null){
                emailMessage.setFrom(emailChannel.getSmtpUsername());
            } else {
                emailMessage.setFrom(fromEmail);
            }
            emailMessage.setSubject(subject);
            emailMessage.setCharset(EMAIL_CHARSET);

            if(emailChannel != null){
                send(emailMessage, emailChannel);
            }else{
                send(emailMessage);
            }

            return emailMessage;
        }
        catch (EmailException e)
        {
            throw new EdmException(sm.getString("NiePowiodloSieWyslaniePoczty"), e);
        }
    }

    @Override
    public void send(String toEmail, String toPersonal, String subject, String msg, boolean checkIsBlocked, String emailContentType, DSEmailChannel emailChannel, Collection<File> attachedFiles) throws EdmException {
        send(toEmail, null, null, toPersonal, subject, msg, checkIsBlocked, emailContentType, emailChannel, attachedFiles);
    }

    public void send(Email email, DSEmailChannel emailChannel) throws EdmException{
        log.trace("send(Email {0})",email);
        if (email == null)
            throw new NullPointerException("email");

        if (emailChannel.getSmtpHost() == null || emailChannel.getSmtpHost().trim().length() == 0)
            throw new EdmException(sm.getString("NieOkreslonoSerweraSMTP"));
        if (emailChannel.getSmtpPort() > 0)
            email.setSmtpPort(emailChannel.getSmtpPort());
        if (!StringUtils.isEmpty(emailChannel.getSmtpPassword()))
            email.setAuthentication(emailChannel.getSmtpUsername(), emailChannel.getSmtpPassword());

        email.setHostName(emailChannel.getSmtpHost());

        if(emailChannel.isSmtpSSL())
        {
            System.out.println("SET SSL ");
            if(log.isTraceEnabled())
                log.trace("ssl=true");
            email.setSSL(true);
            email.setSslSmtpPort(String.valueOf(emailChannel.getSmtpPort()));
        }
        EmailAttachment attachment = new EmailAttachment();

        if(emailChannel.isSmtpTLS())
        {
            System.out.println("SET TLS ");
            email.setTLS(true);
        }
        // copied note:
        // b��d w commons-email - email.setHostName() nie odnosi skutku
        // nale�y przekompilowa� commons-email zmieniaj�c warto�� pola
        // Email.MAIL_HOST na "mail.smtp.host"
        System.setProperty("mail.smtp.host", emailChannel.getSmtpHost());
        System.setProperty("mail.smtp.localhost", emailChannel.getSmtpHost());

        if(AvailabilityManager.isAvailable("mail.smtp.dsn.notify"))
        {
            System.getProperties().put("mail.smtp.dsn.notify",
                    "SUCCESS,FAILURE,DELAY ORCPT=rfc822;"+emailChannel.getSmtpUsername());
            System.getProperties().put("mail.smtp.dsn.ret", "HDRS");
        }

        final String recipients = getRecipientsAsString(email);
        console(Console.INFO, sm.getString("WysylamMailNaAdres")+" "+recipients);
        try
        {
            LoggerFactory.getLogger("mariusz").error("Wysal maila do "+ recipients);

            // je�li mimeMessage nie istnieje, to mo�emy wywo�a� send (czyli buildMimeMessage + send)
            if(email.getMimeMessage() == null) {
                email.send();
            } else {
                // je�li ju� istnieje, to nie mo�na wywo�a� send bo dostaniemy illegal state
                email.sendMimeMessage();
            }
            log.trace(sm.getString("WyslanoListDo",email.getSubject(),recipients));
            companLOG.trace(sm.getString("WyslanoListDo",email.getSubject(),recipients));
            console(Console.INFO, sm.getString("WyslanoListDo",email.getSubject(),recipients));
        }
        catch (Throwable e)
        {
            log.trace(sm.getString("NiePowiodloSieWysylanieListuDo",email.getSubject(),recipients));
            companLOG.trace(sm.getString("NiePowiodloSieWysylanieListuDo",email.getSubject(),recipients));
            console(Console.ERROR, sm.getString("NiePowiodloSieWysylanieListuDo",email.getSubject(),recipients));
            throw new EdmException(sm.getString("NiePowiodloSieWyslaniePoczty"), e);
        }
    }

    public void send(String toEmail ,String userEmail,String fromName, String subject, String msg,String aPath) throws EdmException
    {
        send(toEmail ,userEmail,fromName,subject,msg,aPath,true);
    }

    public void send(String toEmail ,String userEmail,String fromName, String subject, String msg,String aPath,boolean checkIsBlocked) throws EdmException
    {
        if (checkIsBlocked && checkIsBlocked(toEmail))
        {
            console(Console.ERROR, "Uzytkownik"+toEmail+" ma zablokowane logowanie nie wysylam maila");
            return;
        }
        MultiPartEmail  email = new MultiPartEmail();
        try
        {
            EmailAttachment attachment = new EmailAttachment();

            attachment.setPath(aPath);
            attachment.setDisposition("Zalacznik.pdf");
            attachment.setDescription("Zalacznik.pdf");
            attachment.setName("Zalacznik.pdf");

            email.addTo(toEmail);
            email.setFrom(userEmail,fromName);
            email.setSubject(subject);
            email.attach(attachment);
            email.setMsg(msg);
            send(email);
        }
        catch (EmailException e)
        {
            throw new EdmException(sm.getString("NiePowiodloSieWyslaniePoczty"), e);
        }
    }

    private void send(Email email) throws EdmException
    {
        log.trace("send(Email {0})",email);
        if (email == null)
            throw new NullPointerException("email");

        // email.setDebug(true);
        if (smtpServer == null || smtpServer.trim().length() == 0)
            throw new EdmException(sm.getString("NieOkreslonoSerweraSMTP"));
        if (smtpPort != 25 && smtpPort > 0)
            email.setSmtpPort(smtpPort);
        if (smtpAuth)
            email.setAuthentication(smtpUsername, smtpPassword);
        email.setHostName(smtpServer);

        setHeaders(email);

        if(smtpSsl)
        {
            System.out.println("SET SSL ");
            if(log.isTraceEnabled())
                log.trace("ssl=true");
            email.setSSL(true);
            email.setSslSmtpPort(String.valueOf(smtpPort));
        }

        if(smtpTls)
        {
            System.out.println("SET TLS ");
            email.setTLS(true);
        }
        // b��d w commons-email - email.setHostName() nie odnosi skutku
        // nale�y przekompilowa� commons-email zmieniaj�c warto�� pola
        // Email.MAIL_HOST na "mail.smtp.host"
        System.setProperty("mail.smtp.host", smtpServer);
        System.setProperty("mail.smtp.localhost", smtpServer);

        if(AvailabilityManager.isAvailable("mail.smtp.dsn.notify"))
        {
            System.getProperties().put("mail.smtp.dsn.notify",
                    "SUCCESS,FAILURE,DELAY ORCPT=rfc822;"+fromEmail);
            System.getProperties().put("mail.smtp.dsn.ret", "HDRS");
        }

        final String recipients = getRecipientsAsString(email);
        console(Console.INFO, sm.getString("WysylamMailNaAdres")+" "+recipients);
        try
        {
            LoggerFactory.getLogger("mariusz").error("Wysal maila do "+ recipients);

            // je�li mimeMessage nie istnieje, to mo�emy wywo�a� send (czyli buildMimeMessage + send)
            if(email.getMimeMessage() == null) {
                email.send();
            } else {
                // je�li ju� istnieje, to nie mo�na wywo�a� send bo dostaniemy illegal state
                email.sendMimeMessage();
            }
            log.trace(sm.getString("WyslanoListDo",email.getSubject(),recipients));
            companLOG.trace(sm.getString("WyslanoListDo",email.getSubject(),recipients));
            console(Console.INFO, sm.getString("WyslanoListDo",email.getSubject(),recipients));
        }
        catch (Throwable e)
        {
            log.trace(sm.getString("NiePowiodloSieWysylanieListuDo",email.getSubject(),recipients));
            companLOG.trace(sm.getString("NiePowiodloSieWysylanieListuDo",email.getSubject(),recipients));
            console(Console.ERROR, sm.getString("NiePowiodloSieWysylanieListuDo",email.getSubject(),recipients));
            throw new EdmException(sm.getString("NiePowiodloSieWyslaniePoczty"), e);
        }
    }

    private void attachFiles(HtmlEmail email, Collection<File> attachments) throws EmailException{
        EmailAttachment attachment;
        for(File file : attachments){
            attachment = new EmailAttachment();
            attachment.setPath(file.getPath());
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            if(file.getName().lastIndexOf("ORGNAME_") != -1){
                attachment.setName(file.getName().substring(file.getName().lastIndexOf("ORGNAME_")+"ORGNAME_".length()));
            }else{
                attachment.setName(file.getName());
            }
            attachment.setDescription(file.getName());
            email.attach(attachment);
        }
    }

    /**
     * Metoda weryfikuje czy zawartosc wiadomosc e-mail jest w formacie HTML czy PLAIN TEXT
     * @param contentType typ zawartosci e-maila
     * @return wskazanie (boolean) czy zawartosc e-maila jest typu HTML czy nie
     */
    private static boolean isHTMLContent(String contentType){
        boolean isHTML = false;
        if(EmailConstants.TEXT_PLAIN == contentType){
            return isHTML = false;
        }else if(EmailConstants.TEXT_HTML == contentType){
            return isHTML = true;
        }

        return isHTML;
    }

    private void setHeaders(Email email)
    {
        Map<String, String> props = new HashMap<String, String>();
        if(AvailabilityManager.isAvailable("mail.disposition.notification"))
            props.put("Disposition-Notification-To", fromEmail);

        if(!props.isEmpty())
            email.setHeaders(props);

    }

    private String getRecipientsAsString(Email email)
    {
        StringBuffer recipients = new StringBuffer();
        try
        {
            email.buildMimeMessage();
            for (Address address : email.getMimeMessage().getAllRecipients())
            {
                recipients.append(address.toString());
                recipients.append(", ");
            }
        }
        catch (MessagingException e)
        {
            log.warn(e.getMessage(), e);
        }
        catch (EmailException e)
        {
            log.warn(e.getMessage(), e);
        }
        if (recipients.length() > 0)
            recipients.setLength(recipients.length()-2);

        return recipients.toString();
    }



    /* ServiceDriver */

    protected void start() throws ServiceException
    {
        console(Console.INFO, sm.getString("Serwis MailerDriver wystartowal"));
    }

    protected void stop() throws ServiceException
    {
        console(Console.INFO, sm.getString("Serwis MailerDriver zako�czy� dzia�anie"));
    }

    protected boolean canStop()
    {
        return !sending;
    }

    public Property[] getProperties()
    {
        return properties;
    }

    class SmtpServerProperty extends Property
    {
        public SmtpServerProperty()
        {
            super(SIMPLE, PERSISTENT, MailerDriver.this, "smtpServer", sm.getString("SerwerSMTP"), String.class);
        }

        protected Object getValueSpi()
        {
            return smtpServer;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (MailerDriver.this)
            {
                smtpServer = (String) object;
                //transport = null;
            }
        }
    }

    class SmtpPortProperty extends Property
    {
        public SmtpPortProperty()
        {
            super(SIMPLE, PERSISTENT, MailerDriver.this, "smtpPort", sm.getString("PortSMTP"), Integer.class);
        }

        protected Object getValueSpi()
        {
            return new Integer(smtpPort);
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null) return;
            synchronized (MailerDriver.this)
            {
                smtpPort = ((Integer) object).intValue();
                //transport = null;
            }
        }
    }

    class SmtpAuthProperty extends Property
    {
        public SmtpAuthProperty()
        {
            super(SIMPLE, PERSISTENT, MailerDriver.this, "smtpAuth", sm.getString("AutentykacjaSMTP"), Boolean.class);
        }

        protected Object getValueSpi()
        {
            return Boolean.valueOf(smtpAuth);
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null) return;
            synchronized (MailerDriver.this)
            {
                smtpAuth = ((Boolean) object).booleanValue();
                //transport = null;
            }
        }
    }
    class SmtpSSLProperty extends Property
    {
        public SmtpSSLProperty()
        {
            super(SIMPLE, PERSISTENT, MailerDriver.this, "smtpSsl", sm.getString("SSL"), Boolean.class);
        }

        protected Object getValueSpi()
        {
            return Boolean.valueOf(smtpSsl);
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null) return;
            synchronized (MailerDriver.this)
            {
                smtpSsl = ((Boolean) object).booleanValue();
                //transport = null;
            }
        }
    }

    class SmtpTLSProperty extends Property
    {
        public SmtpTLSProperty()
        {
            super(SIMPLE, PERSISTENT, MailerDriver.this, "smtpTls", sm.getString("TLS"), Boolean.class);
        }

        protected Object getValueSpi()
        {
            return Boolean.valueOf(smtpTls);
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null) return;
            synchronized (MailerDriver.this)
            {
                smtpTls = ((Boolean) object).booleanValue();
                //transport = null;
            }
        }
    }


    class SmtpUsernameProperty extends Property
    {
        public SmtpUsernameProperty()
        {
            super(SIMPLE, PERSISTENT, MailerDriver.this, "smtpUsername", sm.getString("NazwaUzytkownikaSMTP"), String.class);
        }

        protected Object getValueSpi()
        {
            return smtpUsername;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (MailerDriver.this)
            {
                smtpUsername = (String) object;
                //transport = null;
            }
        }
    }

    class SmtpPasswordProperty extends Property
    {
        public SmtpPasswordProperty()
        {
            super(SIMPLE, PERSISTENT, MailerDriver.this, "smtpPassword", sm.getString("HasloUzytkownikaSMTP"), String.class);
        }

        protected Object getValueSpi()
        {
            return smtpPassword;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (MailerDriver.this)
            {
                smtpPassword = (String) object;
                //transport = null;
            }
        }
    }

    class FromEmailProperty extends Property
    {
        public FromEmailProperty()
        {
            super(SIMPLE, PERSISTENT, MailerDriver.this, "fromEmail", sm.getString("ZwrotnyAdresEmail"), String.class);
        }

        protected Object getValueSpi()
        {
            return fromEmail;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
//            if (object != null)
//            {
//                if (!((String) object).matches("^[a-zA-Z,.-_]+@("))
//            }
            fromEmail = (String) object;
        }
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }
    public void send(String[] toEmails,String subject, String[] emailCC, String[] emailBCC, Reader msg, Map<String, ?> params) throws EdmException{
    	//MultiPartEmail email = new MultiPartEmail();
    	 HtmlEmail email = new HtmlEmail();
    	try
    	{
    		email.addTo(toEmails);
    		if(emailCC != null){
    			email.addCc(emailCC);
    		}
    		if(emailBCC != null){
    			email.addBcc(emailBCC);
    		}
    	    String content = applyTemplate(msg, params);

            final int nl;
            if (subject == null && (nl = content.indexOf('\n')) > 0 && content.startsWith("Subject:")){
                subject = content.substring("Subject:".length(), nl).trim();
                content = content.substring(nl).trim();
            }

            if (subject == null){
                throw new EdmException(sm.getString("NiePodanoTematuWysylanegoListu"));
            }

    		email.setFrom(fromEmail);
    		 
    		email.setSubject(subject);
    		((HtmlEmail)email).setHtmlMsg(content);
    	//	email.setMsg(content);
    		email.setCharset(EMAIL_CHARSET);
    		send(email);
    	}
    	catch(EmailException e)
    	{
    		throw new EdmException(sm.getString("NiePowiodloSieWyslaniePoczty"), e);
    	}

    }
    public void send(String[] toEmail, String[] emailCC, String[] emailBCC, String subject, String msg, Iterable<Attachment> attachments) throws EdmException {
    	MultiPartEmail email = new MultiPartEmail();
    	try
    	{
    		email.addTo(toEmail);
    		if(emailCC != null){
    			email.addCc(emailCC);
    		}
    		if(emailBCC != null){
    			email.addBcc(emailBCC);
    		}
    		email.setFrom(fromEmail);
    		email.setMsg(msg);
    		email.setSubject(subject);
    		if (attachments!=null)
    		{
    			for(Attachment att: attachments){
    				EmailAttachment  attachment = new EmailAttachment();
    				attachment.setURL(att.url);
    				attachment.setDisposition(EmailAttachment.ATTACHMENT);
    				attachment.setDescription(att.name);
    				attachment.setName(att.name);
    				email.attach(attachment);
    			}
    		}
    		send(email);
    	}
    	catch(EmailException e)
    	{
    		throw new EdmException(sm.getString("NiePowiodloSieWyslaniePoczty"), e);
    	}

    }
    
    public void send(String mailto, String subject, String message, Collection<pl.compan.docusafe.service.mail.Mailer.Attachment> attachments) throws EdmException{
    	MultiPartEmail email = new MultiPartEmail();
    	try
    	
    	{
    		if (mailto==null || mailto.isEmpty() || subject==null || subject.isEmpty() || message==null || message.isEmpty())
    		throw new EdmException(" Brakuje parametrow do wyslania maila");
    	
    	 
    		email.addTo(mailto);
    		email.setFrom(fromEmail);
    		email.setMsg(message);
    		email.setSubject(subject);
    		
    		if (attachments!=null)
    		{
    			for(Attachment att: attachments){
    				EmailAttachment  attachment = new EmailAttachment();
    				attachment.setURL(att.url);
    				attachment.setDisposition(EmailAttachment.ATTACHMENT);
    				attachment.setDescription(att.name);
    				attachment.setName(att.name);
    				email.attach(attachment);
    			}
    		}
    		send(email);
    	}
    	catch(EmailException e)
    	{
    		throw new EdmException(sm.getString("NiePowiodloSieWyslaniePoczty"), e);
    	}

    }


	
}
