package pl.compan.docusafe.service.tktelekom;

import com.google.common.collect.Maps;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.mosty.NormalLogic;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.ocr.OcrFileService;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.util.*;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;

/**
 * Created with IntelliJ IDEA.
 * User: jtarasiuk
 * Date: 20.06.13
 * Time: 00:01
 * To change this template use File | Settings | File Templates.
 */
public class FileAsAttachmentToDocusafeService extends ServiceDriver implements Service
{
    private final static Logger LOG = LoggerFactory.getLogger(OcrFileService.class);
    private final static String NAME = "file-attachment-service";
    public static final String THREAD_NAME = "file-attachment-service_thread";
    public static final String PREFIX_FOLDER_IN = "IN_";
    public static final String PREFIX_FOLDER_INT = "INT_";
    public static final String PREFIX_FOLDER_OUT = "OUT_";

    private static Set<File> attachmentDirs = new HashSet<File>();
    static
    {
        String attachmentFolders = Docusafe.getAdditionProperty("attachmentFolders");
        if (attachmentFolders != null && attachmentFolders.isEmpty() == false)
            for (String a : attachmentFolders.split(";"))
            {
                addWatchedDirectory(new File(a));    
            }
    }

    private Timer timer;
    /**
     * Zbi�r obserwowanych katalog�w
     */
    private Set<File> ocrDirs = new HashSet<File>();

    public static FileAsAttachmentToDocusafeService getService() throws ServiceException
    {
        return (FileAsAttachmentToDocusafeService) ServiceManager.getService(NAME);
    }

    /**
     * Dodaje katalog z ocrowanymi plikami do obserwowanych przez ten serwis
     * @param directory
     */
    private static void addWatchedDirectory(File directory){
        checkNotNull(directory, "directory cannot be null");  
        if(directory.exists())
            attachmentDirs.add(directory);
        else
        {         	 
        	  boolean success = ( new File("C:\\" + directory.getName()).mkdir() );
        	  if (success) 
        	  {
                  	attachmentDirs.add(directory);
        	        LOG.info("directory submitted : {}", directory);
        	  }
        	  else
        		  checkArgument(directory.isDirectory(), "directory must be existing directory");
        }

    }

    //====================
    //  Service Driver
    //====================

    /**
     * Zwraca kopie zbioru obserwowanych katalog�w
     * @return
     */
    private synchronized List<File> cloneWatchedDirectories(){
        return new ArrayList<File>(attachmentDirs);
    }

    private final TimerTask SERVICE_TASK = new TimerTask() {
        @Override
        public void run() {
            List<File> dirs = cloneWatchedDirectories();
            LOG.debug("checking dirs : {}", dirs);
            for(File dir: dirs){
                for(File file : dir.listFiles())
                {
                    if (dir.getName().startsWith(PREFIX_FOLDER_IN))
                        createNormalDocument(dir, file, PREFIX_FOLDER_IN, NormalLogic.NORMAL_DOCKIND_CN);
                    else if (dir.getName().startsWith(PREFIX_FOLDER_INT))
                        createNormalDocument(dir, file, PREFIX_FOLDER_INT, NormalLogic.NORMAL_INT_DOCKIND_CN);
                    else if (dir.getName().startsWith(PREFIX_FOLDER_OUT))
                        createNormalDocument(dir, file, PREFIX_FOLDER_OUT, NormalLogic.NORMAL_OUT_DOCKIND_CN);
                }
            }
        }
    };

    /**
     * Tworzy nowy dokument w zaleznosci w jakim folderze znajdowal sie zalacznik:
     * IN_xxxx -> pismo przychodzace
     * INT_xxx -> pismo wew.
     * OUT_xxx -> pismo wychodzace
     * xxx -> kod dzialu do ktorego ma zostac zadekretowane pismo
     * @param dir
     * @param file
     * @param prefix
     * @param dockindCn
     */
    private void createNormalDocument(File dir, File file, String prefix, String dockindCn)
    {
        OfficeDocument newDocument = null;

        if (dockindCn.equals(NormalLogic.NORMAL_DOCKIND_CN))
        {
            newDocument = new InOfficeDocument();
            try
            {
                ((InOfficeDocument)newDocument).setIncomingDate(GlobalPreferences.getCurrentDay());
            }
            catch (EdmException e)
            {
                log.error(e.getMessage(), e);
            }
        }
        else if (dockindCn.equals(NormalLogic.NORMAL_INT_DOCKIND_CN))
        {
            newDocument = new OutOfficeDocument();
            ((OutOfficeDocument)newDocument).setInternal(true);
        }
        else if (dockindCn.equals(NormalLogic.NORMAL_OUT_DOCKIND_CN))
        {
            newDocument = new OutOfficeDocument();
        }

        if (newDocument != null)
        {
            try
            {
                DSApi.openAdmin();
                DSApi.context().begin();

                DocumentKind normalDocumentKind = DocumentKind.findByCn(dockindCn);

                newDocument.setCreatingUser(DSApi.context().getPrincipalName());
                newDocument.setSummary(normalDocumentKind.getName());
                newDocument.setDocumentKind(normalDocumentKind);
                String code = dir.getName().replace(prefix, "");
                String guid = DSDivision.findByCode(code).getGuid();
                guid = guid != null ? guid : DSDivision.ROOT_GUID;
                newDocument.setAssignedDivision(guid);
                newDocument.setCurrentAssignmentGuid(guid);
                newDocument.setDivisionGuid(guid);
                newDocument.setCurrentAssignmentAccepted(Boolean.FALSE);

                newDocument.create();

                DSApi.context().session().save(newDocument);

                Attachment attachment = new Attachment(file.getName());
                newDocument.createAttachment(attachment);
                attachment.createRevision(file).setOriginalFilename(file.getName());
                boolean fileDeleted = file.delete();
                if(!fileDeleted){
                    log.error("Nie uda�o si� skacowa� pliku: " + file.getAbsoluteFile());
                }

                Map<String, Object> map = Maps.newHashMap();
                map.put(ASSIGN_DIVISION_GUID_PARAM, guid);
                newDocument.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(newDocument, map);

                TaskSnapshot.updateByDocument(newDocument);
                DSApi.context().commit();
            } catch (Exception ex) {
                DSApi.context().setRollbackOnly();
                LOG.error(ex.getMessage(), ex);
            } finally {
                DSApi._close();
            }
        }
    }

    @Override
    protected void start() throws ServiceException {
        timer = new Timer(THREAD_NAME, true);
        timer.scheduleAtFixedRate(SERVICE_TASK, 0, 1 * DateUtils.MINUTE);
    }

    @Override
    protected void stop() throws ServiceException {
        timer.cancel();
    }

    @Override
    protected boolean canStop() {
        return false;
    }
}
