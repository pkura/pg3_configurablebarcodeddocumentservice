package pl.compan.docusafe.service.bip;

import java.text.ParseException;
import java.util.Date;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Serwis BIPowy, ktory odczytuje z datamartu ostatnie zmiany w dokumentach i przekazauje je do
 * wlaczonego drivera serwisu BIPu. Serwis ten jest uruchamiany cyklicznie w ustalonym odstepie czasowym.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class BipService extends ServiceDriver implements Service
{
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BipService.class);
	
	/**
	 * Tablica za parametrami serwisu
	 */
	private Property[] properties;
	
	/**
	 * Maksymalna liczba dokumentow, ktora jest przekazywana do BIPu
	 */
	private Integer maxDocuments;
	
	/**
	 * Czas, od ktorego ma sie rozpoczac odczytywanie zmian w datamart'cie
	 */
	private Date fromDate;
	
	/**
	 * Obiekt Timer'a, ktory uruchamia serwis w ustalonym odstepie czasowym
	 */
	private Timer timer;
	
	/**
	 * Obiekt odczytujacy dane z datamart'u
	 */
	private DataMartBipDocumentsReader reader;
	
	/**
	 * Konstruktor
	 */
	public BipService() 
	{
		properties = new Property[] {
				new MaxDocumentsProperty(),
				new FromDateProperty()
		};
	}
	
	/**
	 * Zwraca parametry serwisu dla widoku
	 */
	@Override
	public Property[] getProperties() 
	{
		return properties;
	}
	
	@Override
	protected boolean canStop() 
	{
		return false;
	}
	
	/**
	 * Uruchamia serwis
	 */
	@Override
	protected void start() throws ServiceException 
	{
		if (!AvailabilityManager.isAvailable(Configuration.ADDITION_BIP_SERVICE))
		{
			LOG.info("Usluga BipService jest wylaczona.");
			return;
		}
		
		console(Console.INFO, "Uruchomienie serwisu BIPu.");
		timer = new Timer();
		timer.schedule(new ReadDataMart(), 2 * 1000, 1 * DateUtils.MINUTE);
	}
	
	/**
	 * Zatrzymuje serwis
	 */
	@Override
	protected void stop() throws ServiceException 
	{
		if (timer != null)
			timer.cancel();
	}
	
	/**
	 * Okresowo odczytuje ostatnie zmiany w dokumentach z datamartu i przekazuje zbi�r zmienionych
	 * dokument�w do odpowiedniego drivera BIPu.
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class ReadDataMart extends TimerTask
	{
		public void run() 
		{
			try
			{
				DSApi.openAdmin();
				
				console(Console.INFO, "Rozpoczecie odczytywania danych z DataMartu ...");
				// utworzenie obiektu odczytujacego dane z datamart
				if (reader == null)
					reader = new DataMartBipDocumentsReader(maxDocuments, fromDate);
				else
				{
					if (!fromDate.equals(reader.getStartTime()))
						reader.setStartTime(fromDate);
					
					reader.setMaxDocuments(maxDocuments);
				}
				
				// odczyt zmian w dokumentach
				Set<InOfficeDocument> documents = reader.read();
				
				console(Console.INFO, "Zakonczenie odczytywania danych z DataMartu.");
				console(Console.INFO, "Liczba dokumentow do przekazania do BIPu: " + documents.size());
				
				if (documents.size() > 0)
				{
					// przekazanie zmian do BIPa
					console(Console.INFO, "Tworzenie uslugi wysylajacej dane do BIPu ...");
					
					Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
					
					console(Console.INFO, "Utworzono usluge BIP: " + bip.getDriverId());
					console(Console.INFO, "Wysylanie dokumentow do BIPu ...");
					
					int correctlySend = 0;
					for (InOfficeDocument document : documents)
					{
						try
						{
							bip.submit(document);
							correctlySend++;
						}
						catch (ServiceException ex)
						{
							console(Console.INFO, "Blad podczas wysylania dokumentu o ID: " + document.getId());
							console(Console.INFO, "Powod: " + ex.getMessage());
						}
					}

					console(Console.INFO, "Poprawnie wyslano: " + correctlySend + " dokumentow.");
				}
				
				console(Console.INFO, "----------------------------------------------------");
			}
			catch (EdmException ex)
			{
				console(Console.ERROR, ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
			finally
			{
				DSApi._close();
			}
		}
	}
	
	/**
	 * Parametr okreslajacy maksymalna liczbe dokumento przesylana do BIPa przy pojedynczym 
	 * uruchomieniu serwisu
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	class MaxDocumentsProperty extends Property
	{
		public MaxDocumentsProperty() 
		{
			super(SIMPLE, PERSISTENT, BipService.this, "maxDocumentsForBip", "Maks. liczba dokumentow", String.class);
		}
		
		@Override
		protected Object getValueSpi() 
		{
			return maxDocuments;
		}
		
		@Override
		protected void setValueSpi(Object object) throws ServiceException 
		{
			synchronized (BipService.this) 
			{
				maxDocuments = Integer.parseInt((String) object);
			}
		}
	}
	
	/**
	 * Parametr okreslajacy czas od ktorego ma rozpoczac sie pobieranie danych z datamartu
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	class FromDateProperty extends Property
	{
		public FromDateProperty() 
		{
			super(SIMPLE, PERSISTENT, BipService.this, "fromDateEventForBip", "Dokumenty od daty", String.class, DATE);
		}
		
		@Override
		protected Object getValueSpi() 
		{
			if (fromDate != null)
				return DateUtils.formatJsDateTimeWithSeconds(fromDate);
			else
				return null;
		}
		
		@Override
		protected void setValueSpi(Object object) throws ServiceException 
		{
			synchronized (BipService.this) 
			{
				try
				{
					if (object != null)
						fromDate = DateUtils.parseDateTimeAnyFormat((String) object);
				}
				catch (ParseException ex)
				{
					LOG.error(ex.getMessage(), ex);
					console(Console.WARN, "Blad podczas parsowania daty od ktorej maja byc pobierane dokumenty z DataMartu!");
				}
			}
		}
	}
}
