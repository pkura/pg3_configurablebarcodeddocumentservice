package pl.compan.docusafe.service.bip;

import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriverNotSelectedException;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceNotFoundException;
import pl.compan.docusafe.util.DateUtils;

public class MaxusBipDriver extends AbstractFileBip implements Bip
{
    private Property[] properties;
    private Timer timer;

    Logger log = LoggerFactory.getLogger(MaxusBipDriver.class);
    public MaxusBipDriver() throws ServiceNotFoundException, ServiceDriverNotSelectedException
    {
        properties = new Property[] {
            new DirectoryProperty(MaxusBipDriver.this)
        };
    }

    protected void stop() throws ServiceException
    {
    	if (timer != null) timer.cancel();
    }

    protected void start() throws ServiceException
    {
    	 timer = new Timer(true);
         timer.schedule(new CreateXMLFile(), DateUtils.DAY, 4 * DateUtils.DAY/*30*DateUtils.SECOND*/);
    }

    protected synchronized boolean canStop()
    {
        return true;
    }

    public Property[] getProperties()
    {
        return properties;
    }
    
    protected void appendXml(OfficeCase sprawa, Element elem, String akcja) throws ServiceException
    {
  
        String val;
        
        Element temp;
        
        Element spr = elem.addElement("Sprawa");
        
        temp = spr.addElement("Id");
        val = sprawa.getId().toString();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("Akcja");
        temp.setText(akcja);
      //TODO:opisac
        
        
        temp = spr.addElement("Numer");
        val = sprawa.getOfficeId();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("KodSprawy");
        val = sprawa.getOfficeId();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("NumerWywolujacy");
        val = sprawa.getTrackingNumber();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("KodWywolujacy");
        val = sprawa.getTrackingPassword();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("Rodzaj");
        val = sprawa.getRwa().getDescription();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("Status");
        val = sprawa.getStatus().getName();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("Stan");
        val = sprawa.getStatus().getName();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("KategoriaArchiwalna");
        val = sprawa.getRwa().getAcHome();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("Data");
        val = DateUtils.formatSqlDate(sprawa.getCtime());
        if(val!=null) temp.setText(val);
        
        
        temp = spr.addElement("Termin");
        val = DateUtils.formatSqlDate(sprawa.getFinishDate());
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("Zalatwiono");
      //TODO:opisac
        
        temp = spr.addElement("Temat");
        val = sprawa.getDescription();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("Przebieg");
      //TODO:opisac
        
        temp = spr.addElement("Sposob");
      //TODO:opisac
        
        temp = spr.addElement("NumerWTeczce");
        //val = sprawa.getOfficeId();
        //if(val!=null) temp.setText(val);
        
        temp = spr.addElement("NumerWKomorce");
        //val = sprawa.getOfficeId();
        //if(val!=null) temp.setText(val);
        
        temp = spr.addElement("NumerUPracownika");
        //val = sprawa.getOfficeId();
        //if(val!=null) temp.setText(val);
        
        temp = spr.addElement("TeczkaSymbol");
        val = sprawa.getParent().getOfficeId();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("TeczkaNazwa");
        val = sprawa.getParent().getName();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("PodteczkaNumer");
      //TODO:opisac
        
        temp = spr.addElement("PodteczkaNazwa");
      //TODO:opisac
        
        temp = spr.addElement("Prowadzacy");
        try
        {
        	val = DSUser.findByUsername(sprawa.getClerk()).asFirstnameLastname();
        }
        catch(Exception unfe)
        {
        	val = sprawa.getClerk();
        }
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("Komorka");
        try
        {
        	val = DSDivision.find(sprawa.getDivisionGuid()).getName();
        	if(val!=null) temp.setText(val);
        }
        catch(EdmException e)
        {
        	//nic nie robimy
        }
        temp = spr.addElement("KomorkaSkrot");
        val = sprawa.getDivisionGuid();
        if(val!=null) temp.setText(val);
        
        temp = spr.addElement("KodStrony");
      //TODO:opisac
        try{
	        Sender s = ((InOfficeDocument)sprawa.getDocuments(InOfficeDocument.class).get(0)).getSender();
	        if(s!=null){
		        temp = spr.addElement("Nazwisko");
		        val = s.getLastname();
		        if(val!=null) temp.setText(val);
	
		        
		        temp = spr.addElement("Imie");
		        val = s.getFirstname();
		        if(val!=null) temp.setText(val);
	
		        
		        temp = spr.addElement("Instytucja");
		        val = s.getOrganization();
		        if(val!=null) temp.setText(val);
	
		        
		        temp = spr.addElement("NIP");
		        val = s.getNip();
		        if(val!=null) temp.setText(val);
	
		        
		        temp = spr.addElement("PESEL");
		        val = s.getPesel();
		        if(val!=null) temp.setText(val);
	
		        
		        temp = spr.addElement("REGON");
		        val = s.getRegon();
		        if(val!=null) temp.setText(val);
	        }
        }
        catch(Exception e )
        {
        	//nie robimy nic. Nie udalo sie po rpostu dodac informacji o nadawcy pisma
        }
        
    }
    
    class CreateXMLFile extends TimerTask
    {
        public void run()
        {
           
        }
    }

	
}
