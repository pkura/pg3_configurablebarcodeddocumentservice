package pl.compan.docusafe.service.bip;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpRecoverableException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceException;

import java.io.*;

/* User: Administrator, Date: 2005-04-22 14:09:36 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PlocmanBipDriver.java,v 1.5 2006/02/20 15:42:28 lk Exp $
 */
public class PlocmanBipDriver extends QueueingBipDriver
{
    private static final Log log = LogFactory.getLog(PlocmanBipDriver.class);

    private String url;
    private String username;
    private String password;
    private File directory;

    private Property[] properties;

    public PlocmanBipDriver()
    {
        properties = new Property[] {
            new UrlProperty(),
            new UsernameProperty(),
            new PasswordProperty(),
            new DirectoryProperty()
        };
    }

    public Property[] getProperties()
    {
        return properties;
    }

    protected void submit(File xml) throws ServiceException
    {
        if (url == null || url.length() == 0)
            throw new ServiceException("Nie okre�lono adresu serwera BIP");
        if (username == null || username.length() == 0)
            throw new ServiceException("Nie okre�lono loginu serwera BIP");
        if (password == null || password.length() == 0)
            throw new ServiceException("Nie okre�lono has�a serwera BIP");

        submit(url, username, password, xml);
    }

    protected File getTempDirectory()
    {
        return directory;
    }

    private void submit(String url, String username, String password, File xmlFile)
        throws ServiceException
    {
        log.info("Wysy�anie pliku "+xmlFile.getAbsolutePath());

        StringBuilder xml = new StringBuilder(1024);
        Reader reader = null;
        try
        {
            reader = new InputStreamReader(new FileInputStream(xmlFile), "utf-8");
            char[] buf = new char[1024];
            int count;
            while ((count = reader.read(buf)) > 0)
            {
                xml.append(buf, 0, count);
            }
            reader.close();
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
            throw new ServiceException("Nie mo�na pobra� pliku", e);
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            throw new ServiceException("Nie mo�na pobra� pliku", e);
        }
        finally
        {
            if (reader != null) try { reader.close(); } catch (Exception e) { }
        }

        HttpClient httpClient = new HttpClient();
        httpClient.setConnectionTimeout(20*1000);
        httpClient.setTimeout(20*1000);

        PostMethod post = new PostMethod(url);
        post.addRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");

        post.addParameter("login", username);
        post.addParameter("haslo", password);
        post.addParameter("bip", xml.toString());

        try
        {
            log.info("Wysy�anie pisma do BIP ("+url+")");
            int status = httpClient.executeMethod(post);
            String response = post.getResponseBodyAsString();
            log.info("odpowied� z BIP: "+post.getStatusCode()+" "+post.getStatusText()+"\n"+response);

            if (status == 200 && !xmlFile.delete())
            {
                // TODO: tworzy� list� plik�w, kt�re nie b�d� ponownie wys�ane
                File newName = new File(xmlFile.getAbsolutePath()+".delete");
                log.warn("Nie mo�na usun�� pliku "+xmlFile.getAbsolutePath()+
                    ", pr�ba zmiany nazwy na "+newName.getAbsolutePath());
                if (!xmlFile.renameTo(newName))
                    log.warn("Zmiana nazwy pliku "+xmlFile.getAbsolutePath()+
                        " na "+newName.getAbsolutePath()+" nie powiod�a si�");
            }
        }
        catch (java.net.ConnectException e)
        {
            log.error(e.getMessage());
        }
        catch (java.net.SocketTimeoutException e)
        {
            log.error(e.getMessage());
        }
        catch (HttpRecoverableException e)
        {
            log.error(e.getMessage());
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }


    class UrlProperty extends Property
    {
        public UrlProperty()
        {
            super(SIMPLE, PERSISTENT, PlocmanBipDriver.this, "url", "Adres serwera", String.class);
        }

        protected Object getValueSpi()
        {
            return url;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            url = (String) object;
        }
    }

    class UsernameProperty extends Property
    {
        public UsernameProperty()
        {
            super(SIMPLE, PERSISTENT, PlocmanBipDriver.this, "username", "Login", String.class);
        }

        protected Object getValueSpi()
        {
            return username;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            username = (String) object;
        }
    }

    class PasswordProperty extends Property
    {
        public PasswordProperty()
        {
            super(SIMPLE, PERSISTENT, PlocmanBipDriver.this, "password", "Has�o", String.class);
        }

        protected Object getValueSpi()
        {
            return password;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            password = (String) object;
        }
    }

    class DirectoryProperty extends Property
    {
        public DirectoryProperty()
        {
            super(SIMPLE, PERSISTENT, PlocmanBipDriver.this, "directory", "Katalog tymczasowy", String.class);
        }

        protected Object getValueSpi()
        {
            return directory != null ? directory.getAbsolutePath() : null;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null)
                return;
            File tmp = new File((String) object);
            if (!tmp.isDirectory())
                throw new ServiceException(tmp+" nie istnieje lub nie jest katalogiem");
            if (!tmp.canWrite())
                throw new ServiceException("Brak praw zapisu w katalogu "+tmp);
            directory = tmp;
        }
    }
}
