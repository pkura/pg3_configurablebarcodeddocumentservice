package pl.compan.docusafe.service.bip;

import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;

/**
 * Klasa tworzaca XMLa sprawy dla uslugi BIP.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class CaseBipXml implements BipXml
{
	public static final String PUBLICID = "-//COM-PAN//Sprawa w BIP//EN";
    public static final String SYSTEMID = "http://docusafe.pl/dtd/bip.dtd";
    
    /**
     * Zwraca obiekt Document zawierajacy calego XMLa
     */
	public Document getXML(InOfficeDocument doc) throws EdmException
	{
		DocumentFactory factory = DocumentFactory.getInstance();
        Document document = factory.createDocument();

        document.addDocType("sprawa", PUBLICID, SYSTEMID);
        
        Element eksport = document.addElement("EksportBIP_G");
        Element sprawa = createElementSprawa(eksport, doc);

        Element pisma = sprawa.addElement("pisma");

        createElementPismoPrzyjete(pisma, doc);

        for (Iterator iter=doc.getOutDocuments().iterator(); iter.hasNext(); )
        {
            OutOfficeDocument outdoc = (OutOfficeDocument) iter.next();
            createElementPismoWyslane(pisma, doc, outdoc);
        }

        return document;
	}
	
	/**
	 * Tworzy element sprawy na podstawie przekazanego dokumentu docusafe
	 * 
	 * @param eksport
	 * @param doc
	 * @return
	 * @throws EdmException
	 */
	private Element createElementSprawa(Element eksport, InOfficeDocument doc) throws EdmException
	{
		Element sprawa = eksport.addElement("sprawa");
        sprawa.addAttribute("tracking", doc.getTrackingNumber());
        sprawa.addAttribute("password", doc.getTrackingPassword());
        sprawa.addAttribute("timestamp", String.valueOf(System.currentTimeMillis()));
        
        sprawa.addElement("Akcja").setText("Popraw");
        
        if (doc.getContainingCase() != null && doc.getContainingCase().isClosed())
        {
            sprawa.addElement("zakonczona");
        }

        if (doc.getContainingCase() != null)
        {
            OfficeCase c = doc.getContainingCase();
            sprawa.addElement("numer").addCDATA(c.getOfficeId());
            if (c.getOpenDate() != null)
                sprawa.addElement("data-zalozenia").setText(DateUtils.formatBipDate(c.getOpenDate()));
            if (c.getFinishDate() != null)
                sprawa.addElement("data-zakonczenia").setText(DateUtils.formatBipDate(c.getFinishDate()));
            try
            {
                sprawa.addElement("referent").addCDATA(DSUser.safeToFirstnameLastname(c.getClerk()));
                sprawa.addElement("dzial").addCDATA(DSDivision.safeGetName(c.getDivisionGuid()));
            }
            catch (EdmException e)
            {
            }
            // dzial
            if (c.getStatus() != null)
                sprawa.addElement("status").addCDATA(c.getStatus().getName());
        }
        else
        {
            if (doc.getCaseDocumentId() != null)
                sprawa.addElement("numer").addCDATA(doc.getCaseDocumentId());
            sprawa.addElement("data-zalozenia").setText(DateUtils.formatBipDate(doc.getIncomingDate()));
            try
            {
                sprawa.addElement("referent").addCDATA(DSUser.safeToFirstnameLastname(doc.getClerk()));
            }
            catch (EdmException e)
            {
            }
        }
        
        return sprawa;
	}
	
	/**
	 * Tworzy element <pismo-przyjete> w elemencie <pisma>
	 * 
	 * @param pisma
	 * @param doc
	 * @throws EdmException
	 */
	private void createElementPismoPrzyjete(Element pisma, InOfficeDocument doc) throws EdmException
	{
		Element pismoPrzyjete = pisma.addElement("pismo-przyjete");
        pismoPrzyjete.addElement("numer-kancelaryjny").setText(String.valueOf(doc.getOfficeNumber()));
        if (doc.getCaseDocumentId() != null)
            pismoPrzyjete.addElement("sygnatura").addCDATA(doc.getCaseDocumentId());
        pismoPrzyjete.addElement("data-przyjecia").setText(DateUtils.formatBipDate(doc.getIncomingDate()));
        if (doc.getDocumentDate() != null)
            pismoPrzyjete.addElement("data-pisma").setText(DateUtils.formatBipDate(doc.getDocumentDate()));
        pismoPrzyjete.addElement("tresc").addCDATA(doc.getSummary());
        if (doc.getReferenceId() != null)
            pismoPrzyjete.addElement("znak").addCDATA(doc.getReferenceId());
        try
        {
            pismoPrzyjete.addElement("referent").addCDATA(DSUser.safeToFirstnameLastname(doc.getClerk()));
        }
        catch (EdmException e)
        {
        }
        if (doc.getSender() != null)
        {
            Element nadawcy = pismoPrzyjete.addElement("nadawcy");
            Element kontakt = nadawcy.addElement("kontakt");
            updateElementKontakt(kontakt, doc.getSender());
        }

        if (doc.getRecipients().size() > 0)
        {
            Element odbiorcy = pismoPrzyjete.addElement("odbiorcy");
            for (Iterator iter=doc.getRecipients().iterator(); iter.hasNext(); )
            {
                Element kontakt = odbiorcy.addElement("kontakt");
                updateElementKontakt(kontakt, (Recipient) iter.next());
            }
        }
	}
	
	/**
	 * Tworzy element <pismo-wyslane> w elemencie <pisma>
	 * 
	 * @param pisma
	 * @param doc
	 * @param outdoc
	 * @throws EdmException
	 */
	private void createElementPismoWyslane(Element pisma, InOfficeDocument doc, OutOfficeDocument outdoc) throws EdmException
	{
		Element pismo = pisma.addElement("pismo-wyslane");
        if (outdoc.getCaseDocumentId() != null)
            pismo.addElement("sygnatura").setText(outdoc.getCaseDocumentId());
        if (outdoc.getDocumentDate() != null)
            pismo.addElement("data-pisma").setText(DateUtils.formatBipDate(outdoc.getDocumentDate()));
        pismo.addElement("tresc").addCDATA(outdoc.getSummary());
        try
        {
            pismo.addElement("referent").addCDATA(DSUser.safeToFirstnameLastname(doc.getClerk()));
        }
        catch (EdmException e)
        {
        }
        if (outdoc.getSender() != null)
        {
            Element nadawcy = pismo.addElement("nadawcy");
            Element kontakt = nadawcy.addElement("kontakt");
            updateElementKontakt(kontakt, outdoc.getSender());
        }
        if (outdoc.getRecipients().size() > 0)
        {
            Element odbiorcy = pismo.addElement("odbiorcy");
            for (Iterator recipientsIter=outdoc.getRecipients().iterator(); recipientsIter.hasNext(); )
            {
                Person person = (Person) recipientsIter.next();
                Element kontakt = odbiorcy.addElement("kontakt");
                updateElementKontakt(kontakt, person);
            }
        }
	}
	
	/**
	 * Tworzy element <kontakt> na podstawie przekazanego obiektu typu Person
	 * 
	 * @param kontakt
	 * @param person
	 */
	private void updateElementKontakt(Element kontakt, Person person)
    {
        if (person.getFirstname() != null)
            kontakt.addElement("imie").addCDATA(person.getFirstname());
        if (person.getLastname() != null)
            kontakt.addElement("nazwisko").addCDATA(person.getLastname());
        if (person.getOrganization() != null || person.getOrganizationDivision() != null)
        {
            StringBuilder org = new StringBuilder();
            if (person.getOrganization() != null)
                org.append(person.getOrganization());
            if (person.getOrganizationDivision() != null)
            {
                if (org.length() > 0) org.append(" / ");
                org.append(person.getOrganizationDivision());
            }
            kontakt.addElement("organizacja").addCDATA(org.toString());
        }
        if (person.getStreet() != null)
            kontakt.addElement("adres").addCDATA(person.getStreet());
        if (person.getZip() != null)
            kontakt.addElement("kod").addCDATA(person.getZip());
        if (person.getLocation() != null)
            kontakt.addElement("miejscowosc").addCDATA(person.getLocation());
    }
}
