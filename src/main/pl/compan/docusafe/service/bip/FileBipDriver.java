package pl.compan.docusafe.service.bip;

import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriverNotSelectedException;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceNotFoundException;

/* User: Administrator, Date: 2005-04-20 15:36:13 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: FileBipDriver.java,v 1.4 2009/06/29 07:07:28 pecet3 Exp $
 */
public class FileBipDriver extends AbstractFileBip implements Bip
{
    private Property[] properties;

    public FileBipDriver() throws ServiceNotFoundException, ServiceDriverNotSelectedException
    {
        properties = new Property[] {
            new DirectoryProperty(FileBipDriver.this)
        };
    }

    protected void stop() throws ServiceException
    {
    }

    protected void start() throws ServiceException
    {
    }

    protected synchronized boolean canStop()
    {
        return true;
    }

    public Property[] getProperties()
    {
        return properties;
    }
}
