package pl.compan.docusafe.service.bip;

import org.dom4j.Document;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/* User: Administrator, Date: 2005-04-20 14:09:06 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: AbstractBipDriver.java,v 1.8 2010/07/27 11:26:24 kamilj Exp $
 */
public abstract class AbstractBipDriver extends ServiceDriver
{
    public static final String PUBLICID = "-//COM-PAN//Sprawa w BIP//EN";
    public static final String SYSTEMID = "http://docusafe.pl/dtd/bip.dtd";

    protected void save(InOfficeDocument document, File file) throws ServiceException
    {
        org.dom4j.io.XMLWriter writer = null;
        try
        {
        	
            OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(file), "utf-8");

            writer = new org.dom4j.io.XMLWriter(os);
                //new OutputFormat("    ", true));
            writer.write(getXml(document));
            getXml(document);
        }
        catch (Exception e)
        {
            throw new ServiceException("B��d zapisu pliku dla BIP", e);
        }
        finally
        {
            if (writer != null) try { writer.close(); } catch (Exception e) { }
        }
    }

    protected Document getXml(InOfficeDocument doc) throws EdmException
    {
        BipXml bipXml = new CaseBipXml();
        return bipXml.getXML(doc);
    }
}
