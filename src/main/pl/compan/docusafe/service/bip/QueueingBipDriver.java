package pl.compan.docusafe.service.bip;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.service.ServiceException;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* User: Administrator, Date: 2005-04-25 12:06:52 */

/**
 * Sterownik dla BIP, kt�ry kolejkuje wywo�ania metody wysy�aj�cej
 * dokument do serwera BIP ({@link #submit(java.io.File)}).
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: QueueingBipDriver.java,v 1.6 2006/02/20 15:42:28 lk Exp $
 */
public abstract class QueueingBipDriver extends AbstractBipDriver implements Bip
{
    private static final Log log = LogFactory.getLog(QueueingBipDriver.class);

    private List<File> queue = new LinkedList<File>();
    private Submitter submitter = new Submitter();
    private boolean busy;

    /**
     * Metoda, kt�ry zajmuje si� wys�aniem pliku do BIP.
     * @param xml
     * @throws ServiceException Je�eli wysy�ka pliku by�a nieudana,
     *  w tej sytuacji plik nie jest oznaczany jako wys�any i jego
     *  wysy�ka mo�e by� ponowiona w przysz�o�ci.
     */
    protected abstract void submit(File xml) throws ServiceException;

    public void submit(InOfficeDocument document) throws ServiceException
    {
        if (!document.isSubmitToBip())
            return;

        if (getTempDirectory() == null)
            throw new ServiceException("Nie okre�lono katalogu na pliki XML");

        if (!(getTempDirectory().isDirectory() || getTempDirectory().mkdirs()))
            throw new ServiceException("Nie mo�na utworzy� katalogu na pliki BIP: "+
                getTempDirectory());

        File file = new File(getTempDirectory(), "bip."+System.currentTimeMillis()+".xml");

        save(document, file);

        synchronized (queue)
        {
            queue.add(file);
            queue.notifyAll();
        }
    }

    protected abstract File getTempDirectory();

    protected void start() throws ServiceException
    {
        submitter.start();

        if (getTempDirectory() != null)
        {
            File[] files = getTempDirectory().listFiles(new FileFilter()
            {
                public boolean accept(File pathname)
                {
                    return pathname.isFile() && pathname.getName().startsWith("bip") &&
                        pathname.getName().endsWith(".xml");
                }
            });

            if (files != null && files.length > 0)
            {
                synchronized (queue)
                {
                    for (int i=0; i < files.length; i++)
                    {
                        queue.add(files[i]);
                    }
                    queue.notifyAll();
                }
            }
        }

    }

    /**
     * Pr�buje zatrzyma� w�tek wysy�aj�cy informacje do BIP. Je�eli nie
     * uda si� to w ci�gu pi�ciu sekund, rzucany jest wyj�tek ServiceException.
     */
    protected void stop() throws ServiceException
    {
        if (!submitter.isAlive())
            return;

        submitter.terminate();
        try
        {
            submitter.join(5000);
        }
        catch (InterruptedException e)
        {
        }
        if (submitter.isAlive())
            throw new ServiceException("Nie mo�na w tej chwili zatrzyma� sterownika BIP");
    }

    protected final boolean canStop()
    {
        return !busy;
    }

    class Submitter extends Thread
    {
        /**
         * Flaga okre�laj�ca, czy w�tek ma dzia�a�. Po zmianie jej warto�ci
         * na false, metoda run ko�czy dzia�anie tak szybko, jak to mo�liwe.
         */
        private boolean running = true;

        public void run()
        {
            List files = null;

            while (running)
            {
                try
                {
                    // je�eli pojawi� si� nowe pliki do wys�ania, s�
                    // one przepisywane do nowej tablicy
                    synchronized (queue)
                    {
                        queue.wait();

                        if (!running)
                            break;

                        if (queue.size() > 0)
                        {
                            files = new ArrayList<File>(queue);
                        }
                    }

                    // wysy�anie plik�w z tablicy files
                    // pliki, kt�rych nie uda�o si� wys�a�,
                    // s� usuwane z tablicy, pozosta�e pozostaj�,
                    // aby usun�� je z tablicy queue

                    if (files != null)
                    {
                        try
                        {
                            busy = true;

                            for (Iterator iter=files.iterator(); iter.hasNext(); )
                            {
                                if (!running)
                                    break;

                                File file = (File) iter.next();
                                try
                                {
                                    log.info("Wysy�anie do BIP pliku "+file);
                                    submit(file);

                                    //log.info("Usuwanie pliku "+file);
                                    //file.delete();
                                    file.renameTo(new File(file.getAbsolutePath()+".sent"));
                                }
                                catch (Exception e)
                                {
                                    iter.remove();
                                }
                            }

                            synchronized (queue)
                            {
                                queue.removeAll(files);
                            }
                        }
                        finally
                        {
                            busy = false;
                        }
                    }
                }
                catch (InterruptedException e)
                {
                    continue;
                }
            }
        }

        public synchronized void terminate() {
            this.running = false;
            synchronized (queue)
            {
                queue.notifyAll();
            }
        }
    }
}
