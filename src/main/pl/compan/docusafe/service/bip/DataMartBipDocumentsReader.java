package pl.compan.docusafe.service.bip;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.BackingStoreException;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartQuery;
import pl.compan.docusafe.core.datamart.DataMartReadable;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa odczytujaca ostatnie zmiany dotyczace dokumentow wysylanych do uslugi BIP.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class DataMartBipDocumentsReader implements DataMartReadable<InOfficeDocument> 
{
	/**
	 * LOGGER
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DataMartBipDocumentsReader.class);
	
	/**
	 * Domyslna maksymalna liczba dokumentow
	 */
	private static final Integer DEFAULT_MAX_DOCUMENTS = 50;
	
	/**
	 * Startowy identyfikator eventu
	 */
	private static final Long START_EVENT_ID = 0l;
	
	/**
	 * Maksymalna liczba dokumentow
	 */
	private Integer maxDocuments;
	
	/**
	 * Identyfikator ostatniego odczytanego eventu
	 */
	private Long lastEventID;
	
	/**
	 * Czas od ktorego usluga rozpoczela pobieranie eventow
	 */
	private Date startTime;
	
	/**
	 * Domyslny konstruktor
	 */
	public DataMartBipDocumentsReader() 
	{
		maxDocuments = DEFAULT_MAX_DOCUMENTS;
		loadLastEventId();
		loadStartTime();
	}
	
	/**
	 * Konstruktor przyjmujacy dwa parametry:
	 * - maksymalna liczbe dokumentow, ktora bedzie zwracala ta usluga
	 * - czas od ktorego beda pobierane eventy (event_date > startTime)
	 * 
	 * @param maxDocuments
	 * @param startTime
	 */
	public DataMartBipDocumentsReader(Integer maxDocuments, Date startTime)
	{
		// maksymalna liczba dokumentow do zwrocenia przez usluge
		if (maxDocuments == null)
			maxDocuments = DEFAULT_MAX_DOCUMENTS;
		else
			this.maxDocuments = maxDocuments;
		
		// identyfikator ostatniego eventu
		loadLastEventId();
		
		// ustawienie czasu startowego uslugi
		if (startTime == null)
			loadStartTime();
		else
			this.startTime = startTime;
	}
	
	/**
	 * Odczytuje z DataMartu informacje o zmianach w dokumentach przesylanych do BIPu i zwraca 
	 * odpowiedni zbior dokumentow
	 */
	public Set<InOfficeDocument> read() throws EdmException 
	{
		// zbudowanie zapytania wyszukujacego odpowiednie wpisy
		DataMartQuery query = DataMartQuery.forEventCodes(DataMartDefs.BIP_DOCUMENT_INFO, DataMartDefs.BIP_CASE_INFO);
		query.setSelectStatement(DataMartQuery.ALL_COLS_STATEMENT);
		query.eventDateAfter(startTime)
			 .eventIdGratherThen(lastEventID.toString())
			 .orderBy(DataMartQuery.EVENT_ID, true);
		
		Set<InOfficeDocument> documents = new LinkedHashSet<InOfficeDocument>();
		
		try
		{
			
			PreparedStatement st = query.prepareStatementBip();
			ResultSet resultSet = st.executeQuery();
			
			while (resultSet.next() && documents.size() < maxDocuments)
			{
				if (resultSet.getString(DataMartQuery.EVENT_CODE).equals(DataMartDefs.BIP_DOCUMENT_INFO) 
						&& !containsDocument(documents, resultSet.getLong(DataMartQuery.DOCUMENT_ID)))
				{
					// dodanie do zbioru pojedynczego dokumentu jesli w nim nie istnieje
					addBipDocumentInfo(documents, resultSet);
				}
				else if (resultSet.getString(DataMartQuery.EVENT_CODE).equals(DataMartDefs.BIP_CASE_INFO))
				{
					// dodanie do zbioru dokumentow ze sprawy jesli w nim nie istnieja
					addBipCaseInfo(documents, resultSet);
				}
				
				// ustawienie najwiekszego identyfikatora eventu
				if (lastEventID < resultSet.getLong(DataMartQuery.EVENT_ID))
					lastEventID = resultSet.getLong(DataMartQuery.EVENT_ID);
			}
		}
		catch (SQLException ex)
		{
			LOG.error(ex.getMessage(), ex);
			throw new EdmException(ex.getMessage(), ex);
		}
		
		// jesli odczyt przebiegl poprawnie - zapisanie identyfikatora ostatniego eventu
		saveLastEventId();
		return documents;
	}
	
	public Integer getMaxDocuments() 
	{
		return maxDocuments;
	}

	public void setMaxDocuments(Integer maxDocuments) 
	{
		if (maxDocuments == null) return;
		this.maxDocuments = maxDocuments;
	}

	/**
	 * Ustawia czas od ktorego maja byc pobierane eventy z DataMartu,
	 * zapisuje ta zmienna w systemPreferences
	 * 
	 * @param startTime
	 */
	public void setStartTime(Date startTime) throws EdmException
	{
		try
		{
			if (startTime == null) return;
			
			GlobalPreferences.setBipDataMartReaderStartTime(startTime);
			this.startTime = startTime;
		}
		catch (BackingStoreException ex)
		{
			LOG.error(ex.getMessage(), ex);
			throw new EdmException(ex.getMessage(), ex);
		}
		
	}
	
	public Date getStartTime() 
	{
		return startTime;
	}
	
	/**
	 * Zapisuje w zmiennych systemowych ostatni identyfikator poprawnie odczytanego eventu.
	 */
	private void saveLastEventId() throws EdmException
	{
		try
		{
			GlobalPreferences.setBipDataMartReaderLastEventId(lastEventID.toString());
		}
		catch (BackingStoreException ex)
		{
			LOG.error(ex.getMessage(), ex);
			throw new EdmException(ex.getMessage(), ex);
		}
	}
	
	/**
	 * Odczytuje ze zmiennych systemowych informacje o identyfikatorze ostatniego
	 * poprawnie odczytanego eventu.
	 */
	private void loadLastEventId()
	{
		String eventId = GlobalPreferences.getBipDataMartReaderLastEventId(START_EVENT_ID.toString());
		lastEventID = Long.parseLong(eventId);
	}

	/**
	 * Laduje czas startowy uslugi ze zmiennych systemowych.
	 */
	private void loadStartTime()
	{
		String st = GlobalPreferences.getBipDataMartReaderStartTime();
		
		if (st != null)
		{
			try
			{
				startTime = DateUtils.parseDateTimeAnyFormat(st);
			}
			catch (ParseException ex)
			{
				LOG.warn(ex.getMessage(), ex);
			}
		}
		else
			startTime = new Date();
	}
	
	/**
	 * Sprawdza czy podany zbior zawiera juz w sobie dokument o podanym identyfikatorze.
	 * 
	 * @param documents
	 * @param documentId
	 * @return
	 */
	private boolean containsDocument(Set<InOfficeDocument> documents, Long documentId)
	{
		boolean ret = false;
		for (InOfficeDocument document : documents)
		{
			if (document.getId().equals(documentId))
			{
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	/**
	 * Wyszukuje i dodaje do podanego zbioru pojedynczy dokument na ktorym zarejestrowano zdarzenie:
	 * BIP_DOCUMENT_INFO
	 * 
	 * @param documents
	 * @param resultSet
	 * @throws EdmException
	 * @throws SQLException
	 */
	private void addBipDocumentInfo(Set<InOfficeDocument> documents, ResultSet resultSet) 
		throws EdmException, SQLException
	{
		InOfficeDocument document = InOfficeDocument
			.findInOfficeDocument(resultSet.getLong(DataMartQuery.DOCUMENT_ID));
		if (document != null)
			documents.add(document);
		else
			LOG.warn("Serwis Bip nie odnalazl dokumentu o id: " + resultSet.getLong(DataMartQuery.DOCUMENT_ID) 
					+ ", podczas odczytywania danych z DataMart!");	
	}
	
	/**
	 * Wyszukuje odpowiednio sprawe i pobiera z niej dokumenty, ktore zostana dodane do podanego zbioru.
	 * Akcja wywolywana przy zdarzeniu typu: BIP_CASE_INFO
	 * 
	 * @param documents
	 * @param resultSet
	 * @throws EdmException
	 * @throws SQLException
	 */
	private void addBipCaseInfo(Set<InOfficeDocument> documents, ResultSet resultSet)
		throws EdmException, SQLException
	{
		OfficeCase officeCase = OfficeCase.find(resultSet.getLong(DataMartQuery.DOCUMENT_ID));
		if (officeCase != null)
		{
			List<InOfficeDocument> list = officeCase.getDocuments(InOfficeDocument.class);
			for (InOfficeDocument document : list)
			{
				if (!containsDocument(documents, document.getId()))
				{
					documents.add(document);
				}
			}
		}
	}
}
