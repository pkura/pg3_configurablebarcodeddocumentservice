package pl.compan.docusafe.service.bip;

import java.util.Set;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceException;

/* User: Administrator, Date: 2005-04-20 14:09:24 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Bip.java,v 1.7 2010/07/27 11:26:23 kamilj Exp $
 */
public interface Bip extends Service
{
    public static final String NAME = "bip";

    void submit(InOfficeDocument document) throws ServiceException;
    
    //public void submit(Set<InOfficeDocument> documents) throws ServiceException;
}
