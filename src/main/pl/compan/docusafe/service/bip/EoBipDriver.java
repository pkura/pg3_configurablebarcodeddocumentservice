package pl.compan.docusafe.service.bip;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.*;
import java.util.Properties;

/* User: Administrator, Date: 2005-04-25 15:55:17 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: EoBipDriver.java,v 1.6 2006/02/06 16:05:57 lk Exp $
 */
public class EoBipDriver extends QueueingBipDriver
{
    private static final Log log = LogFactory.getLog(EoBipDriver.class);

    private String smtpServer;
    private int smtpPort = 25;
    private boolean smtpAuth;
    private String smtpUsername;
    private String smtpPassword;
    private String email;
    private String fromEmail;
    private File directory;

    private Property[] properties;

    private Session session;
    /**
     * Transport SMTP.  Zmiennej przypisywana jest warto�� null po
     * zmianie ka�dego parametru maj�cego wp�yw na dane wysy�ki
     * (np. nazwa serwera).
     */
    private Transport transport;

    public EoBipDriver()
    {
        properties = new Property[]
        {
            new SmtpServerProperty(),
            new SmtpPortProperty(),
            new SmtpAuthProperty(),
            new SmtpUsernameProperty(),
            new SmtpPasswordProperty(),
            new EmailProperty(),
            new FromEmailProperty(),
            new DirectoryProperty()
        };
    }

    public Property[] getProperties()
    {
        return properties;
    }

    protected void submit(File file) throws ServiceException
    {
        if (smtpServer == null)
            throw new ServiceException("Nie okre�lono nazwy serwera SMTP");
        if (smtpAuth && (smtpUsername == null || smtpPassword == null))
            throw new ServiceException("Nie okre�lono nazwy u�ytkownika ani has�a serwera SMTP");
        if (email == null)
            throw new ServiceException("Nie okre�lono docelowego adresu email");
        if (fromEmail == null)
            throw new ServiceException("Nie okre�lono zwrotnego adresu email");

        StringBuilder xml = new StringBuilder(1024);
        Reader reader = null;
        try
        {
            reader = new InputStreamReader(new FileInputStream(file), "utf-8");
            char[] buf = new char[1024];
            int count;
            while ((count = reader.read(buf)) > 0)
            {
                xml.append(buf, 0, count);
            }
            reader.close();
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
            throw new ServiceException("Nie mo�na pobra� pliku", e);
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            throw new ServiceException("Nie mo�na pobra� pliku", e);
        }
        finally
        {
            if (reader != null) try { reader.close(); } catch (Exception e) { }
        }

        sendMail(xml.toString());
    }

    private void sendMail(String text) throws ServiceException
    {
        if (transport == null)
        {
            Properties mailProperties = new Properties();
            mailProperties.setProperty("mail.smtp.host", smtpServer);
            mailProperties.setProperty("mail.smtp.auth", Boolean.valueOf(smtpAuth).toString());
            mailProperties.setProperty("mail.smtp.port", String.valueOf(smtpPort));
            mailProperties.setProperty("mail.smtp.connectiontimeout", String.valueOf(10*1000));
            mailProperties.setProperty("mail.smtp.timeout", String.valueOf(10*1000));

            Authenticator authenticator = null;
            if (smtpAuth)
            {
                authenticator = new Authenticator()
                {
                    protected PasswordAuthentication getPasswordAuthentication()
                    {
                        return new PasswordAuthentication(smtpUsername, smtpPassword);
                    }
                };
            }

            session = Session.getInstance(mailProperties, authenticator);

            try
            {
                transport = session.getTransport("smtp");
            }
            catch (NoSuchProviderException e)
            {
                throw new ServiceException("Nie ma prowidera typu smtp", e);
            }
        }


        try
        {
            transport.connect();

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromEmail));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            message.setSubject("[Docusafe] Wysy�ka do BIP");

            Multipart multipart = new MimeMultipart();

            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText("Dane znajduj� si� w za��czniku");
            multipart.addBodyPart(messageBodyPart);

            messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(text, "text/xml; charset=utf-8");
            messageBodyPart.setHeader("Content-ID", "<docusafe.bip."+System.currentTimeMillis()+".xml>");
            messageBodyPart.setHeader("Content-Disposition", "attachment; filename=\"bip.xml\"");
            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);

            message.saveChanges();

            log.info("Wysy�anie listu na adres "+email+" (from: "+fromEmail+")");

            transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));

            log.info("Wys�ano list na adres "+email+" (from: "+fromEmail+")");

            transport.close();
        }
        catch (MessagingException e)
        {
            throw new ServiceException("Nie mo�na wys�a� poczty (smtpServer="+smtpServer+
                ", smtpPort="+smtpPort+", smtpAuth="+smtpAuth+
                ", smtpUsername="+smtpUsername+", smtpPassword="+smtpPassword+")", e);
        }
        finally
        {
            try
            {
                transport.close();
            }
            catch (MessagingException e)
            {
                log.error(e.getMessage(), e);
            }
        }
    }

    protected File getTempDirectory()
    {
        return directory;
    }


    class SmtpServerProperty extends Property
    {
        public SmtpServerProperty()
        {
            super(SIMPLE, PERSISTENT, EoBipDriver.this, "smtpServer", "Serwer SMTP", String.class);
        }

        protected Object getValueSpi()
        {
            return smtpServer;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            smtpServer = (String) object;
            transport = null;
        }
    }

    class SmtpPortProperty extends Property
    {
        public SmtpPortProperty()
        {
            super(SIMPLE, PERSISTENT, EoBipDriver.this, "smtpPort", "Port SMTP", Integer.class);
        }

        protected Object getValueSpi()
        {
            return new Integer(smtpPort);
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null) return;
            smtpPort = ((Integer) object).intValue();
            transport = null;
        }
    }

    class SmtpAuthProperty extends Property
    {
        public SmtpAuthProperty()
        {
            super(SIMPLE, PERSISTENT, EoBipDriver.this, "smtpAuth", "Autentykacja SMTP", Boolean.class);
        }

        protected Object getValueSpi()
        {
            return Boolean.valueOf(smtpAuth);
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null) return;
            smtpAuth = ((Boolean) object).booleanValue();
            transport = null;
        }
    }

    class SmtpUsernameProperty extends Property
    {
        public SmtpUsernameProperty()
        {
            super(SIMPLE, PERSISTENT, EoBipDriver.this, "smtpUsername", "Nazwa u�ytkownika SMTP", String.class);
        }

        protected Object getValueSpi()
        {
            return smtpUsername;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            smtpUsername = (String) object;
            transport = null;
        }
    }

    class SmtpPasswordProperty extends Property
    {
        public SmtpPasswordProperty()
        {
            super(SIMPLE, PERSISTENT, EoBipDriver.this, "smtpPassword", "Has�o u�ytkownika SMTP", String.class);
        }

        protected Object getValueSpi()
        {
            return smtpPassword;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            smtpPassword = (String) object;
            transport = null;
        }
    }

    class EmailProperty extends Property
    {
        public EmailProperty()
        {
            super(SIMPLE, PERSISTENT, EoBipDriver.this, "email", "Adres email", String.class);
        }

        protected Object getValueSpi()
        {
            return email;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            email = (String) object;
        }
    }

    class FromEmailProperty extends Property
    {
        public FromEmailProperty()
        {
            super(SIMPLE, PERSISTENT, EoBipDriver.this, "fromEmail", "Zwrotny adres email", String.class);
        }

        protected Object getValueSpi()
        {
            return fromEmail;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            fromEmail = (String) object;
        }
    }

    class DirectoryProperty extends Property
    {
        public DirectoryProperty()
        {
            super(SIMPLE, PERSISTENT, EoBipDriver.this, "directory", "Katalog tymczasowy", String.class);
        }

        protected Object getValueSpi()
        {
            return directory != null ? directory.getAbsolutePath() : null;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null)
                return;
            File tmp = new File((String) object);
            if (!tmp.isDirectory())
                throw new ServiceException(tmp+" nie istnieje lub nie jest katalogiem");
            if (!tmp.canWrite())
                throw new ServiceException("Brak praw zapisu w katalogu "+tmp);
            directory = tmp;
        }
    }
}
