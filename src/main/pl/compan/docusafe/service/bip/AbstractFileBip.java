package pl.compan.docusafe.service.bip;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceDriverNotSelectedException;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.ServiceNotFoundException;
import pl.compan.docusafe.util.DateUtils;

public abstract class AbstractFileBip extends AbstractBipDriver {
	 private File directory;

	 public void submit(InOfficeDocument document) throws ServiceException
	 {
		 if (!document.isSubmitToBip()) return;

		 Long idValue = null;
		 String lastExportDateString;
		 java.util.Date lastExportDate = null;
		 String selectQuery = null;
		 String updateQuery = null;
		 String insertQuery = null;
		 ResultSet rs = null;
	     PreparedStatement ps = null;
		 try
		 {
			 if (ServiceManager.getDriver(Bip.NAME).equals("file")) {
				if (AvailabilityManager.isAvailable("file.bip.many.files")) {
			        if (directory == null)
			            throw new ServiceException("Nie okre�lono katalogu na pliki BIP");
			        if (!(directory.isDirectory() || directory.mkdirs()))
			            throw new ServiceException("Nie mo�na utworzy� katalogu na pliki BIP: "+directory);
			        File file = new File(directory, "bip."+System.currentTimeMillis()+".xml");
			        save(document, file);
				}
		        selectQuery = "SELECT * FROM dso_file_bip_state WHERE document_id=?";
		        updateQuery = "UPDATE dso_file_bip_state SET modify_date=?, stan=? WHERE document_id=?";
		        insertQuery = "INSERT INTO dso_file_bip_state (DOCUMENT_ID,STAN,MODIFY_DATE) VALUES(?,?,?)";
		        lastExportDateString = DSApi.context().systemPreferences().node("file_bip").get("file_export_date", "");
		        idValue = document.getId();
			 } else {
				 selectQuery = "SELECT * FROM dso_maxus_bip_state WHERE case_id=?";
				 updateQuery = "UPDATE dso_maxus_bip_state set modify_date=?, stan=? where case_id=?";
				 insertQuery = "INSERT INTO dso_maxus_bip_state (case_id,stan,modify_date) VALUES(?,?,?)";
				 lastExportDateString = DSApi.context().systemPreferences().node("maxus_bip").get("maxus_export_date", "");
				 idValue = document.getContainingCase() == null ? null : document.getContainingCase().getId();
			}

			if(lastExportDateString.length() > 0)
	    	{
	    		lastExportDate = DateUtils.parseJsDateTime(lastExportDateString);
	    	}


	        if(idValue == null) return;

        	ps = DSApi.context().prepareStatement(selectQuery);
        	ps.setLong(1, idValue);
        	rs = ps.executeQuery();
        	if(rs.next())
        	{
        		PreparedStatement ps1 = DSApi.context().prepareStatement(updateQuery);
        		ps1.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
        		if(lastExportDate == null || rs.getTimestamp("modify_date").after(lastExportDate))
        			ps1.setString(2, "Dodaj");
        		else
        			ps1.setString(2, "Popraw");
        		ps1.setLong(3, idValue);
        		ps1.execute();
        		DSApi.context().closeStatement(ps1);
        	}
        	else
        	{
        		PreparedStatement ps1 = DSApi.context().prepareStatement(insertQuery);
        		ps1.setLong(1, idValue);
        		ps1.setString(2, "Dodaj");
        		ps1.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
        		ps1.execute();
        		DSApi.context().closeStatement(ps1);
        	}
        }
        catch(SQLException e)
        {
        	throw new ServiceException(e.getMessage());
        }
        catch(EdmException e)
        {
        	throw new ServiceException(e.getMessage());
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }

   class DirectoryProperty extends Property
   {
        public DirectoryProperty(ServiceDriver _this) throws ServiceNotFoundException, ServiceDriverNotSelectedException {
            super(SIMPLE, PERSISTENT, _this, "directory", "Folder na pliki xml", String.class);
        }
        protected Object getValueSpi()
        {
            return directory != null ? directory.getAbsolutePath() : null;
        }

        protected void setValueSpi(Object object) throws ServiceException
	    {
            if (object == null)
                throw new ServiceException("Nie podano nazwy katalogu");
	            File dir = new File((String) object);
	            if (!dir.isDirectory())
	                throw new ServiceException(dir+" nie istnieje lub nie jest katalogiem");
	            directory = dir;
	        }
	    }
}
