package pl.compan.docusafe.service.bip;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import java.util.zip.Checksum;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.Base64;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Driver serwisu dla BIPa dla extranetu.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class ExtranetBipDriver extends AbstractBipDriver implements Bip
{
	private static final Logger LOG = LoggerFactory.getLogger(ExtranetBipDriver.class);
	
	private String url;
	private String password;
	private File directory;
	private Property[] properties;
	
	
	public ExtranetBipDriver() 
	{
		properties = new Property[] 
        {
			new UrlProperty(),
			new PasswordProperty(),
			new DirectoryProperty()
		};
	}
	
	@Override
	public Property[] getProperties() 
	{
		return properties;
	}

	public File getTempDirectory() 
	{
		return directory;
	}
	
	@Override
	protected boolean canStop() 
	{
		// nic nie robi
		return false;
	}
	
	@Override
	protected void start() throws ServiceException 
	{
		// nic nie robi
	}
	
	@Override
	protected void stop() throws ServiceException 
	{
		// nic nie robi
	}
	
	/**
	 * Utworzenie na podstawie przekazanego dokumentu pliku XML opisujacego sprawe i wyslanie do BIP.
	 */
	public void submit(InOfficeDocument document) throws ServiceException 
	{
		if (!document.isSubmitToBip())
            return;

        if (getTempDirectory() == null)
            throw new ServiceException("Nie okre�lono katalogu na pliki XML");

        if (!(getTempDirectory().isDirectory() || getTempDirectory().mkdirs()))
            throw new ServiceException("Nie mo�na utworzy� katalogu na pliki BIP: "+
                getTempDirectory());

        File file = new File(getTempDirectory(), "bip."+System.currentTimeMillis()+".xml");

        save(document, file);
        
        submit(file);
	}
	
	/**
	 * Sprawdzenie poprawnosci ustawionych parametrow i wysylka pliku XML do BIPa
	 * 
	 * @param xml
	 * @throws ServiceException
	 */
	private void submit(File xml) throws ServiceException 
	{
		if (url == null || url.length() == 0)
            throw new ServiceException("Nie okre�lono adresu serwera BIP");
        if (password == null || password.length() == 0)
            throw new ServiceException("Nie okre�lono has�a serwera BIP");

        sendToBip(xml);
	}
	
	/**
	 * Nawiazuje polaczenie ze wskazanym serwerem i przesyla mu BIPa w postaci zadania POST
	 * z parametrem "xml".
	 * 
	 * @param xmlFile
	 * @throws ServiceException
	 */
	private void sendToBip(File xmlFile) throws ServiceException
	{
		try
		{
			HttpClient httpClient = new HttpClient();
	        httpClient.setConnectionTimeout(20 * 1000);
	        httpClient.setTimeout(20 * 1000);
	        
	        PostMethod post = new PostMethod(url);
	        
	        // ustawienie parametru XML
	        String sendData = getSendData(xmlFile);
	        post.addParameter("xml", sendData);
	        
	        // wyslanie bipa jako POST z parametrem "xml"
	        int responseStatus = httpClient.executeMethod(post);
	        String response = post.getResponseBodyAsString();
	        
	        // jesli odpowiedz jest inna niz 200 oznacza blad, rzucenie wyjatku, ktory przekaze go na konsole
	        if (!StringUtils.trim(response).equals("200") || responseStatus != 200)
            {
	        	throw new ServiceException("Nie udalo sie przekazac pisma do BIP, status odpowiedzi serwera: " + post.getResponseBodyAsString());
            }
	        
	        // jesli poprawnie wyslano do bip'a, usuniecie niepotrzebnego pliku
	        xmlFile.delete();
		}
		catch (Exception ex)
		{
			LOG.error(ex.getMessage(), ex);
			throw new ServiceException(ex.getMessage(), ex);
		}
        
	}
	
	/**
	 * Zwraca ciag XML sprawy ze wskazanego pliku
	 * 
	 * @param xmlFile
	 * @return
	 * @throws ServiceException
	 * @throws UnsupportedEncodingException
	 */
	private String getXmlText(File xmlFile) throws ServiceException, UnsupportedEncodingException
	{
		StringBuilder xml = new StringBuilder(1024);
        Reader reader = null;
        try
        {
        	// odczyt pliku w kodowaniu UTF-8
            reader = new InputStreamReader(new FileInputStream(xmlFile), "utf-8");
            char[] buf = new char[1024];
            int count;
            while ((count = reader.read(buf)) > 0)
            {
                xml.append(buf, 0, count);
            }
            reader.close();
        }
        catch (IOException e)
        {
            LOG.error(e.getMessage(), e);
            throw new ServiceException("Nie mo�na pobra� pliku", e);
        }
        catch (Exception e)
        {
            LOG.error(e.getMessage(), e);
            throw new ServiceException("Nie mo�na pobra� pliku", e);
        }
        finally
        {
            if (reader != null) try { reader.close(); } catch (Exception e) { }
        }
        
        return xml.toString();
	}
	
	/**
	 * Zwraca dane, ktore maja zostac przeslane do BIPa. Dane sa zaszyfrowane za pomoca AES i zakodowane w Base64.
	 * 
	 * @param xmlFile
	 * @return
	 * @throws ServiceException
	 */
	private String getSendData(File xmlFile) throws ServiceException
	{
		StringBuilder sendData = new StringBuilder();
        
        try
        {
        	String xmlText = getXmlText(xmlFile);
        	
        	// wektor, ktorym beda szyfrowane dane
           	byte[] randomBytes = new byte[16];
           	SecureRandom random = new SecureRandom();
           	random.nextBytes(randomBytes);
           	IvParameterSpec parameterSpec = new IvParameterSpec(randomBytes); 
           	
           	// suma kontrolna
           	Long checksum = getChecksum(xmlText).getValue();
            
            sendData.append("<xml id=\"ImportSpraw1.0\" key=\"")
            	.append(new String(Base64.encode(parameterSpec.getIV())))
            	.append("\" crc=\"")
            	.append(new String(Base64.encode(checksum.toString().getBytes())))
            	.append("\">")
            	.append("<![CDATA[")
            	.append(encode(xmlText, parameterSpec))
            	.append("]]></xml>");  
        }
        catch (Exception ex)
        {
        	LOG.error(ex.getMessage(), ex);
        	throw new ServiceException(ex.getMessage(), ex);
        }
        
        return sendData.toString();
	}
	
	/**
	 * Szyfruje dany tekst w AES/CBC/NoPadding na podstawie hasla i przekazanego wektora szyfrujacego.
	 * Klucz - 16 bajtow
	 * wektor szyfrujacy - 16 bajtow
	 * text - wielokrotnosc 16 bajtow
	 * 
	 * Dane sa zwracane jako zakodowane w Base64
	 * 
	 * @param text
	 * @param parameterSpec
	 * @return
	 * @throws Exception
	 */
	private String encode(String text, IvParameterSpec parameterSpec) throws Exception
	{
		// ustawienie klucza AES, uzupelnienie go zerowymi bajtami
		SecretKey secretKey = new SecretKeySpec(prepareTextBytes(password.getBytes()), "AES");
		
		// Inicjalizacja obiektu szyfrujacego
		Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);
		
		// zaszyfrowanie danych i zwrocenie zakodowanych w Base64. Dane sa uzupelniane do wielokrotnosci 16 bajtow
		// bajty sa przekazywane w kodowaniu UTF-8
		byte[] encrypted = cipher.doFinal(prepareTextBytes(text.getBytes("utf-8")));
		return new String(Base64.encode(encrypted));
	}
	
	/**
	 * Buduje sume kontrolna dla przekazanego tekstu.
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	private static Checksum getChecksum(String text) throws Exception
	{
		// suma budowana jest na podstawie bajtow w kodowaniu UTF-8
		byte[] buffer = text.getBytes("utf-8");
		ByteArrayInputStream bis = new ByteArrayInputStream(buffer);
		CheckedInputStream inputStream = new CheckedInputStream(bis, new CRC32());
		byte[] readBuffer = new byte[32];

		while (inputStream.read(readBuffer) > 0);

		return inputStream.getChecksum();
	}
	
	/**
	 * Uzupelnia brakujace bajty dla wielekrotnosci 16. 
	 * 
	 * @param text
	 * @return
	 */
	private byte[] prepareTextBytes(byte[] text)
	{
		int modulo = text.length % 16; 
		if (modulo != 0)
		{
			byte[] tmp;
			if (text.length < 16)
			{
				tmp = new byte[16];
			}
			else
			{
				tmp = new byte[text.length + 16 - modulo];
			}
			
			for (int i = 0; i < tmp.length; i++)
			{
				if (i > text.length - 1)
				{
					tmp[i] = '\0';
				}
				else
				{
					tmp[i] = text[i];
				}
			}
			return tmp;
		}
		return text;
	}
	
	/**
	 * Parametr URL
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	class UrlProperty extends Property
    {
        public UrlProperty()
        {
            super(SIMPLE, PERSISTENT, ExtranetBipDriver.this, "url", "Adres serwera", String.class);
        }

        protected Object getValueSpi()
        {
            return url;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            url = (String) object;
        }
    }
	
	/**
	 * Parametr haslo
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	class PasswordProperty extends Property
    {
        public PasswordProperty()
        {
            super(SIMPLE, PERSISTENT, ExtranetBipDriver.this, "password", "Has�o", String.class);
        }

        protected Object getValueSpi()
        {
            return password;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            password = (String) object;
        }
    }
	
	/**
	 * Parametr katalog tymczasowy
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	class DirectoryProperty extends Property
    {
        public DirectoryProperty()
        {
            super(SIMPLE, PERSISTENT, ExtranetBipDriver.this, "directory", "Katalog tymczasowy", String.class);
        }

        protected Object getValueSpi()
        {
            return directory != null ? directory.getAbsolutePath() : null;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null)
                return;
            File tmp = new File((String) object);
            if (!tmp.isDirectory())
                throw new ServiceException(tmp+" nie istnieje lub nie jest katalogiem");
            if (!tmp.canWrite())
                throw new ServiceException("Brak praw zapisu w katalogu "+tmp);
            directory = tmp;
        }
    }
}
