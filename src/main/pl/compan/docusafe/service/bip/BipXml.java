package pl.compan.docusafe.service.bip;

import org.dom4j.Document;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocument;

public interface BipXml 
{
	public Document getXML(InOfficeDocument doc) throws EdmException;
}
