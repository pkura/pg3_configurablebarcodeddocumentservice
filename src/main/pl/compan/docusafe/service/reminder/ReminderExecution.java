package pl.compan.docusafe.service.reminder;

import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.Reminder;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Maps;

public class ReminderExecution {

	protected static Logger log = LoggerFactory.getLogger(ReminderExecution.class);
	
	private Reminder reminder;
	private List<Long> foundDocs;
	
	public ReminderExecution(Reminder reminder) {
		this.reminder = reminder;
	}
	
	public void init(List<Long> foundDocs) {
		this.foundDocs = foundDocs;
	}
	
	public int remind(MailerDriver driver) {
		ReminderRecipients recipients = new ReminderRecipients(reminder);
		int counter = 0;
		for (Long docId : foundDocs) {
            Map<String, Object> params = Maps.newHashMap();
            DocFacade df= Documents.document(docId);
            
            try {
				params.put("fields", df.getFields());
				params.put("docId", docId);
				recipients.find(docId);
				
				//TODO
				log.error("docId: "+docId);
				System.out.println("docId: "+docId); //--------------DEBUG
				
				List<String> emails = recipients.getEmails();
				Reader template = Configuration.getMail(reminder.getTemplateFileName());
				
				counter+=sendEmail(template, driver, emails, params);
			} catch (Exception e) {
				System.out.println(e.toString());
				log.error(e.getMessage(), e);
			}
		}
		return counter;
	}
	
	private int sendEmail(Reader template, MailerDriver driver, List<String> recipients, Map<String,Object> params) throws EdmException {
		int docCounter = 0;
		for (String email : recipients) {
			//driver.send(email, email, null, template, params, true);
			//TODO
			log.error(reminder.getName()+": Wysy�am maila do: "+email);
			System.out.println(reminder.getName()+": Wysy�am maila do: "+email); //--------------DEBUG
			docCounter++;
		}
		return docCounter;
	}
	
	private Map<String, Object> prepareTemplateValues(FieldsManager fm) throws EdmException {
		Map<String, Object> templateValues = new HashMap<String, Object>();
		Map<String, Object> fmValues = fm.getFieldValues();

		for (String key : fmValues.keySet()) {
			if (fmValues.get(key) instanceof java.util.Date) {
				String data = DateUtils.formatCommonDate((java.util.Date) fmValues.get(key));
				templateValues.put(key, data);
			} else
				templateValues.put(key, fmValues.get(key));
		}

		return templateValues;
	}
}
