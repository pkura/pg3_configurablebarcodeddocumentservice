package pl.compan.docusafe.service.reminder;

import java.util.List;

import pl.compan.docusafe.core.EdmException;

public interface ReminderDocumentFinder {
	
	public List<Long> findDocs() throws EdmException;
}
