package pl.compan.docusafe.service.reminder;

import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Reminder;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * Przyk�adowe u�ycie:
		<reminders>
			<reminder name="zblizajacy_sie_termin_rozliczenia" period="1d">
				<template file-name="zblizajacy_sie_termin_rozliczenia.txt"/>
				<sql>
					<![CDATA[
						select document_id from dsg_pcz_application_wde where status=160 and DATEDIFF(day, TERMIN_POWROTU, GETDATE()) between 10 and 14
					]]>
				</sql>
				<recipient type="username_field_from_dictionary" split-sequence="/">
					DELEGOWANY/USERNAME
				</recipient>
			</reminder>
			<reminder name="uplynal_termin_rozliczenia" period="2d">
				<template file-name="uplynal_termin_rozliczenia.txt"/>
				<sql>
					<![CDATA[
						select document_id from dsg_pcz_application_wde where status=160 and DATEDIFF(day, TERMIN_POWROTU, GETDATE()) > 14
					]]>
				</sql>
				<recipient type="task_list_users"/>
			</reminder>
		</reminders>
 *		
 * TODO 
 * 1. Wersja prototypowa
 * 2. Nie wysy�a maili, tylko wyrzuca info
 * 3. W tym za�o�eniu, nie dozwolone jest restartowanie maszyny.
 * Nale�y utworzy� tabel� przechowuj�c� id dokument�w, nazw� przypomnienie oraz dat� ostatniego uruchomienia
 * i zgodnie z tym, w razie konieczno�ci uruchamia� przypomnienie.
 * 
 * 
 * @author kk
 */
public class DockindConfiguredReminderService extends ServiceDriver implements Service {
	
	protected static Logger log = LoggerFactory.getLogger(DockindConfiguredReminderService.class);
	private static final String AVAILABS_REMINDER_SERVICE_NAME = "service.disable.DockindReminder";
	private Timer timer;
	private Property[] properties;
	Set<Integer> periodSet = Sets.newHashSet();

	public DockindConfiguredReminderService() {
		properties = new Property[] {
			new PeriodOneDayProperty(),	
			new PeriodTwoDaysProperty()	
		};
	}
	
	@Override
	public Property[] getProperties() {
		return properties;
	}

	class PeriodOneDayProperty extends Property {

		private static final int PERIOD = 1;
		
		protected PeriodOneDayProperty() {
			super(SIMPLE, PERSISTENT, DockindConfiguredReminderService.this, "isOneDayReminder", "W��czony/wy��czony przypominacz obs�uguj�cy przypomnienie co 1 dob�", Boolean.class);
		}

		@Override
		protected Object getValueSpi() {
			if (periodSet.contains(PERIOD)) {
				return Boolean.valueOf(true);
			} else {
				return Boolean.valueOf(false);
			}
		}
		
		@Override
		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (DockindConfiguredReminderService.this) {
				if (object != null) {
					if (Boolean.valueOf(true).equals(object)) {
						periodSet.add(PERIOD);
					}
					if (Boolean.valueOf(false).equals(object)) {
						periodSet.remove(PERIOD);
					}
				}
			}
		}

	}
	
	class PeriodTwoDaysProperty extends Property {
		
		private static final int PERIOD = 2;

		protected PeriodTwoDaysProperty() {
			super(SIMPLE, PERSISTENT, DockindConfiguredReminderService.this, "isTwoDaysReminder", "W��czony/wy��czony przypominacz obs�uguj�cy przypomnienie co 2 doby", Boolean.class);
		}
		
		@Override
		protected Object getValueSpi() {
			if (periodSet.contains(PERIOD)) {
				return Boolean.valueOf(true);
			} else {
				return Boolean.valueOf(false);
			}
		}
		
		@Override
		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (DockindConfiguredReminderService.this) {
				if (object != null) {
					if (Boolean.valueOf(true).equals(object)) {
						periodSet.add(PERIOD);
					}
					if (Boolean.valueOf(false).equals(object)) {
						periodSet.remove(PERIOD);
					}
				}
			}
		}
	}
	
	
	@Override
	protected void start() throws ServiceException {
		if (AvailabilityManager.isAvailable(AVAILABS_REMINDER_SERVICE_NAME)) {
			return;
		}
		
		timer = new Timer();
		
		timer.schedule(new DockindReminder(PeriodOneDayProperty.PERIOD), 0, PeriodOneDayProperty.PERIOD*DateUtils.DAY);
		timer.schedule(new DockindReminder(PeriodTwoDaysProperty.PERIOD), 0, PeriodTwoDaysProperty.PERIOD*DateUtils.DAY);
		
	}

	@Override
    protected void stop() throws ServiceException
    {
        if (timer != null)
            timer.cancel();
    }

	@Override
	protected boolean canStop() {
		// TODO Auto-generated method stub
		return false;
	}
	
	private class DockindReminder extends TimerTask {
		public int timerPeriod = 0;

		
		public DockindReminder(int timerPeriod) {
			this.timerPeriod = timerPeriod;
		}
		
		@Override
		public void run() {
			if (periodSet.contains(timerPeriod)) {
				try {
					DSApi.openAdmin();

					MailerDriver driver = (MailerDriver) ServiceManager.getService(Mailer.NAME);

					List<DocumentKind> docKinds = DocumentKind.list();
					List<Reminder> reminders = Lists.newArrayList();
					for (DocumentKind docKind : docKinds) {
						reminders = docKind.getDockindInfo().getReminders(timerPeriod + "d");
						for (Reminder reminder : reminders) {
							console(Console.INFO, reminder.getName() + ": Uruchomiono reminder dla dockindu: " + docKind.getCn());
							ReminderDocumentFinder finder = new SqlReminderDocumentFinder(docKind, reminder);
							List<Long> foundDocs;

							try {
								foundDocs = finder.findDocs();
							} catch (EdmException e) {
								console(Console.ERROR, reminder.getName() + ": BLAD " + e.toString());
								log.error(e.getMessage(), e);
								continue;
							}
							console(Console.INFO, reminder.getName() + ": Znaleziono " + (foundDocs != null ? foundDocs.size() : "zero")
									+ " dokument�w po terminie");

							ReminderExecution exec = new ReminderExecution(reminder);
							exec.init(foundDocs);
							int sentReminds = exec.remind(driver);
							console(Console.INFO, reminder.getName() + ": Wys�ano " + sentReminds + " przypomnie�");
						}
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					console(Console.ERROR, e.toString());
				} finally {
					try {
						DSApi.close();
					} catch (EdmException e) {
						log.error(e.getMessage(), e);
					}
				}
			}
		}
	}
}

