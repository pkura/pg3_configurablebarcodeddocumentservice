package pl.compan.docusafe.service.reminder;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.Reminder;
import pl.compan.docusafe.core.dockinds.Reminder.RecipientType;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;

public class ReminderRecipients {
	private static final String GET_USER_FROM_TASKLIST_QUERY = "select assigned_resource from DSW_JBPM_TASKLIST where document_id = ?";
	private Reminder reminder;
	private List<String> emails = Lists.newArrayList();
	
	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}
	
	public ReminderRecipients(Reminder reminder) {
		this.reminder = reminder;
	}

	public List<String> find(Long docId) throws DivisionNotFoundException, EdmException {
		if (reminder.getRecipientType().equals(RecipientType.GUID) && emails.isEmpty()) {
			findByGuid();
		}
		if (reminder.getRecipientType().equals(RecipientType.USERNAME) && emails.isEmpty()) {
			findByUserName();
		}
		if (reminder.getRecipientType().equals(RecipientType.TASK_LIST)) {
			findFromTaskList(docId);
		}
		if (reminder.getRecipientType().equals(RecipientType.USERNAME_FIELD_FROM_DICTIONARY)) {
			findByUsernameFieldFromDictionary(docId);
		}
		return emails;
	}
	
	private void findFromTaskList(Long docId) throws EdmException {
		Set<String> userNames = Sets.newHashSet();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DSApi.context().prepareStatement(GET_USER_FROM_TASKLIST_QUERY);
			ps.setLong(1, docId);
			rs = ps.executeQuery();
			while (rs.next()) {
				userNames.add(rs.getString(1));
			}
		} catch (SQLException e) {
			throw new EdmSQLException(e);
		} finally {
			DSApi.context().closeStatement(ps);
			DbUtils.closeQuietly(rs);
		}
		
		emails = Lists.newArrayList();
		
		for (String username : userNames) {
			if (DSUser.findByUsername(username) != null) {
				emails.add(DSUser.findByUsername(username).getEmail());
			}
		}
		
		if (emails.isEmpty()) {
			throw new EdmException("Nie mo�na znale�� u�ytkownik�w przypisanych do dokumentu");
		}
		
	}
	
	private void findByGuid() throws DivisionNotFoundException, EdmException {
		if (reminder.getRecipient() != null) {
			for (DSUser user : DSDivision.find(reminder.getRecipient()).getUsers()) {
				if (StringUtils.isNotBlank(user.getEmail())) {
					emails.add(user.getEmail());
				}
			}
		}
	}
	
	private void findByUserName() throws UserNotFoundException, EdmException {
		if (reminder.getRecipient() != null) {
			if (DSUser.findByUsername(reminder.getRecipient()) != null) {
				emails.add(DSUser.findByUsername(reminder.getRecipient()).getEmail());
			}
		}
	}
	
	private void findByUsernameFieldFromDictionary(Long docId) throws DivisionNotFoundException, EdmException {
		emails = Lists.newArrayList();
		String username = null;
		FieldsManager fm = Document.find(docId).getFieldsManager();
		
		String[] recipientParams = reminder.getRecipient().split(reminder.getRecipientSplitSeq());
		if (recipientParams.length!=2) {
			throw new EdmException("Nieprawid�owa ilo�� atrybut�w w parametrach odbiorcy");
		}
		if (fm.getValue(recipientParams[0])!=null && fm.getValue(recipientParams[0]) instanceof Map<?, ?>) {
			Map<String,Object> dic = (Map<String,Object>)fm.getValue(recipientParams[0]);
			username = dic.get(recipientParams[1]).toString();
		}
		
		if (StringUtils.isNotEmpty(username)) {
			if (DSUser.findByUsername(username) != null) {
				emails.add(DSUser.findByUsername(reminder.getRecipient()).getEmail());
			}
		} else {
			throw new EdmException("Nie mo�na odczyta� username'u z s�ownika, "+reminder.getRecipient());
		}
	}
}
