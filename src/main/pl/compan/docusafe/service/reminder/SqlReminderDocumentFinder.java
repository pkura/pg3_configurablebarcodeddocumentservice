package pl.compan.docusafe.service.reminder;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Reminder;

import com.google.common.collect.Lists;

public class SqlReminderDocumentFinder implements ReminderDocumentFinder {
	private DocumentKind docKind;
	private Reminder reminderParams;
	private String searchQuery;
	private List<Long> foundDocs = Lists.newArrayList();
	
	public SqlReminderDocumentFinder(DocumentKind docKind, Reminder reminderParams) {
		this.docKind = docKind;
		this.reminderParams = reminderParams;
	}
	
	public void prepareSearchQuery() throws EdmException {
		if (!StringUtils.isEmpty(reminderParams.getSearchQuery())) {
			this.searchQuery = StringUtils.replace(reminderParams.getSearchQuery(), "$dockind_tablename", docKind.getTablename());
			this.searchQuery = StringUtils.replace(searchQuery, "$dockind_id", docKind.getId().toString());
		} else {
			throw new EdmException("Brak zapytania dot. szukania dokumentów dla reminder'a");
		}
	}
	
	public List<Long> findDocs() throws EdmException {
		prepareSearchQuery();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			DSApi.context().begin();
			ps = DSApi.context().prepareStatement(searchQuery);

			rs = ps.executeQuery();
			while (rs.next()) {
				foundDocs.add(rs.getLong(1));
			}
		} catch (SQLException e) {
			throw new EdmSQLException(e);
		} finally {
			DSApi.context().rollback();
			DSApi.context().closeStatement(ps);
			DbUtils.closeQuietly(rs);
		}
		return foundDocs;
	}
	
	
	
}
