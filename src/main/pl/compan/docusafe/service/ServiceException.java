package pl.compan.docusafe.service;

import pl.compan.docusafe.core.EdmException;

/* User: Administrator, Date: 2005-04-20 13:17:31 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ServiceException.java,v 1.5 2008/11/08 13:59:30 wkuty Exp $
 */
public class ServiceException extends EdmException
{
    public ServiceException(String message)
    {
        super(message);
    }

    public ServiceException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
