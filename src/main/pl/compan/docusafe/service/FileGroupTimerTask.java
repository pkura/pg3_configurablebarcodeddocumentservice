package pl.compan.docusafe.service;

import org.apache.commons.collections.iterators.ArrayIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;

import java.io.File;
import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

abstract public class FileGroupTimerTask extends IterateTimerTask<List<File>> {

    private static final Logger log = LoggerFactory.getLogger(FileGroupTimerTask.class);

    private String folderPath;
    private final boolean sort;
    private boolean locked;

    public FileGroupTimerTask() {
        this.sort = true;
    }

    public FileGroupTimerTask(String folderPath) {
        this(folderPath, true);
    }

    public FileGroupTimerTask(String folderPath, boolean sort) {
        checkNotNull(folderPath);
        this.folderPath = folderPath;
        this.sort = sort;
    }

    @Override
    public void beforeRun() throws Exception {
        if(folderPath == null) {
            throw new Exception("folderPath is null");
        }

        File folder = new File(folderPath);

        if (!folder.isDirectory()) {
            throw new Exception("folder " + folderPath + " is not directory");
        }

        if (isLocked(folder)) {
            log.info("[beforeRun] skipping interation, folder is locked, folderPath = {}", folderPath);
            stop = true;
            return;
        } else {
            log.info("[beforeRun] doing iteration, folderPath = {}", folderPath);
            stop = false;
        }

        File[] list = folder.listFiles();

        if (list == null) {
            throw new Exception("file list is null, not directory or IO error");
        }

        if (sort) {
            Arrays.sort(list);
        }

        Collection<List<File>> fileGroups = groupByFilename(list);

        iterator = fileGroups.iterator();
    }

    private Collection<List<File>> groupByFilename(File[] list) {
        Map<String, List<File>> map = new HashMap<String, List<File>>();

        for(File file: list) {
            String name = file.getName().replaceFirst("\\.[A-Za-z0-9]+$", "");

            if(map.get(name) == null) {
                map.put(name, new ArrayList<File>());
            }

            map.get(name).add(file);
        }

        return map.values();
    }

    @Override
    protected void afterRun() {

    }

    @Override
    protected abstract void forEach(List<File> element, int index) throws Exception;

    public boolean isLocked(File folder) {
        File file = new File(folder, FileTimerTask.LOCK_FILENAME);
        return file.exists();
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public static class FileGroup {
        private List<File> files;

        public List<File> getFiles() {
            return files;
        }

        public void setFiles(List<File> files) {
            this.files = files;
        }
    }
}
