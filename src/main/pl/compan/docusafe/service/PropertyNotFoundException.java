package pl.compan.docusafe.service;

/* User: Administrator, Date: 2005-04-20 15:46:29 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PropertyNotFoundException.java,v 1.1 2005/04/25 15:34:03 lk Exp $
 */
public class PropertyNotFoundException extends ServiceException
{
    public PropertyNotFoundException(String propertyId)
    {
        super("Nie znaleziono w�asno�ci "+propertyId);
    }

    public PropertyNotFoundException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
