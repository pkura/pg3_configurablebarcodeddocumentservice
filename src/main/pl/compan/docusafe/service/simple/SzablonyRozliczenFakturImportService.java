package pl.compan.docusafe.service.simple;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.SzablonyRozliczenFakturFactory;
import pl.compan.docusafe.general.canonical.model.SzablonyRozliczenFaktur;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.RodzajePlanowZakupuImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SzablonyRozliczenFakturImportService extends ServiceDriver{

	public static final Logger log = LoggerFactory.getLogger(KontrahentKontoImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private SzablonyRozliczenFakturFactory szablonRozliczenFakturFactory;

	private SzablonyRozliczenFakturFactory getBudzetKoKomorekFactory()
	{
		if(szablonRozliczenFakturFactory == null)
		{
			szablonRozliczenFakturFactory = new SzablonyRozliczenFakturFactory();
		}
		return szablonRozliczenFakturFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, SzablonyRozliczenFakturImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (SzablonyRozliczenFakturImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public SzablonyRozliczenFakturImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System SzablonyRozliczenFakturImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System SzablonyRozliczenFakturImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importSzablonowRozliczenFaktur();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importSzablonowRozliczenFaktur(){		
		// Pobranie szablonow z [WS]
		List<SzablonyRozliczenFaktur> listaNowych = getBudzetKoKomorekFactory().pobierzSzablonyRozliczenFakturZWS();

		// Pobranie szablonow z [BD]	
		List<SzablonyRozliczenFaktur> listaStarych = getBudzetKoKomorekFactory().pobierzSzablonyRozliczenFakturZBD();

		// Połączenie starej i nowej listy szablonow
		SzablonyRozliczenFakturFactory budzetKoKomorekFactory = new SzablonyRozliczenFakturFactory();
		List<SzablonyRozliczenFaktur> listaAktualna = budzetKoKomorekFactory.aktualizujSzablonyRozliczenFaktur(listaNowych, listaStarych);

		// Zapisanie aktualnej listy szablonow do bazy danych
		
		budzetKoKomorekFactory.ZapiszSzablonyRozliczenFakturDoDB(listaAktualna);
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;	
	}
}
