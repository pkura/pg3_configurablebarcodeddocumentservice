package pl.compan.docusafe.service.simple;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.NumeryZamowienFactory;
import pl.compan.docusafe.general.canonical.factory.ZamowienieSimplePozycjeFactory;
import pl.compan.docusafe.general.canonical.model.NumeryZamowien;
import pl.compan.docusafe.general.canonical.model.ZamowienieSimplePozycje;
import pl.compan.docusafe.general.hibernate.dao.NumeryZamowienDBDAO;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.ZrodloFinansowaniaProjektowImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.GenericDocumentServiceStub;
import pl.compan.docusafe.ws.client.simple.general.GenericDocumentServiceStubFactory;
import pl.compan.docusafe.ws.client.simple.general.GenericDocumentServiceStub.GetDocument;

public class NumeryZamowienImportService extends ServiceDriver{

	public static final Logger log = LoggerFactory.getLogger(NumeryZamowienImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private NumeryZamowienFactory numeryZamowienFactory;
	private ZamowienieSimplePozycjeFactory zamowienieSimplePozycjeFactory;
	private GenericDocumentServiceStub gdStub;
	
	private NumeryZamowienFactory getNumeryZamowienFactory()
	{
		if(numeryZamowienFactory == null)
		{
			numeryZamowienFactory = new NumeryZamowienFactory();
		}
		return numeryZamowienFactory;
	}
	
	private ZamowienieSimplePozycjeFactory getZamowienieSimplePozycjeFactory()
	{
		if(zamowienieSimplePozycjeFactory == null)
		{
			zamowienieSimplePozycjeFactory = new ZamowienieSimplePozycjeFactory();
		}
		return zamowienieSimplePozycjeFactory;
	}
	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, NumeryZamowienImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (NumeryZamowienImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public NumeryZamowienImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System NumeryZamowienImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System NumeryZamowienImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importNumeryZamowien();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importNumeryZamowien(){		
		List<NumeryZamowien> listaAktualna=new ArrayList<NumeryZamowien>();
		try{
			// Pobranie z [WS]
			console(Console.INFO, "-----poczatek importu-----");
			List<NumeryZamowien> listaNowych = getNumeryZamowienFactory().pobierzNumeryZamowienZWS();
			console(Console.INFO, "-----sciagnieto z WS-----");
			// Pobranie z [BD]	
			List<NumeryZamowien> listaStarych = getNumeryZamowienFactory().pobierzNumeryZamowienZBD();
			console(Console.INFO, "-----sciagnieto z BD-----");
			// Połączenie starej i nowej listy 
			NumeryZamowienFactory numeryZamowienFactory = new NumeryZamowienFactory();
			listaAktualna = numeryZamowienFactory.aktualizujNumeryZamowien(listaNowych, listaStarych);
			console(Console.INFO, "-----zaaktualizowano-----");
			// Zapisanie aktualnej listy do bazy danych
			numeryZamowienFactory.zapiszNumeryZamowienDoDB(listaAktualna);
			console(Console.INFO, "-----zapisano-----");
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
		console(Console.INFO, "-----import pozycji zamowien-----");
		
		
		//sciagniecie pozycji
		for(NumeryZamowien nz:listaAktualna){
			try {
				if(nz.getAvailable()!=null&&nz.getAvailable()){
					importPosition(nz.getIdm(),nz.getErpId());
				}
			}catch (RemoteException e) {
				console(Console.INFO, "-----Blad zwiazany z pobraniem danych dla pozycji z WS, aktualizacja nieudana-----");
				log.error("", e);
			}
		}
		console(Console.INFO, "-----sciagnieto pozycje zamowien-----");

	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;	
	}
	
	
	
	private void importPosition(String idm,Long erpId) throws RemoteException
	{
		try{
			GetDocument params = new GetDocument();
			params.setDocumentType(InvoiceGeneralConstans.GD_DOCUMENT_TYPE);
			params.setIdm(idm);

			gdStub = GenericDocumentServiceStubFactory.getInstance();

			String response = new String(gdStub.getDocument(params).get_return().getBytes(), "UTF-8");
			SAXReader reader = new SAXReader();
			org.dom4j.Document doc = reader.read(new ByteArrayInputStream(response.getBytes()));
			List pozycjeF = doc.getRootElement().element("zamdostpozycje").elements("zamdostpoz");
			List<ZamowienieSimplePozycje> pozycjeWS=new ArrayList<ZamowienieSimplePozycje>();
			for (Iterator<Element> iterator = pozycjeF.iterator(); iterator.hasNext();) 
			{
				Element el = iterator.next();
				pozycjeWS.add(new ZamowienieSimplePozycje(el,idm,erpId));

			}

			List<ZamowienieSimplePozycje> pozycjeDB=getZamowienieSimplePozycjeFactory().pobierzZamowienieSimplePozycjeZBD();
			List<ZamowienieSimplePozycje> listaAktualna = getZamowienieSimplePozycjeFactory().aktualizujZamowienieSimplePozycje(pozycjeWS, pozycjeDB);
			getZamowienieSimplePozycjeFactory().zapiszZamowienieSimplePozycjeDoDB(listaAktualna);

		}catch(UnsupportedEncodingException e){
			log.debug("",e);
		}catch(DocumentException e){
			log.debug("",e);
		}



	}
}
