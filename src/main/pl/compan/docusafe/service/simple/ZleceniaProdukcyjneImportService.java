package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.ZleceniaProdukcyjneFactory;
import pl.compan.docusafe.general.canonical.model.ZleceniaProdukcyjne;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.WalutaImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZleceniaProdukcyjneImportService extends ServiceDriver{
	public static final Logger log = LoggerFactory.getLogger(ZleceniaProdukcyjneImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private ZleceniaProdukcyjneFactory zleceniaProdukcyjneFactory;
	
	private ZleceniaProdukcyjneFactory getZleceniaProdukcyjneFactory()
	{
		if(zleceniaProdukcyjneFactory == null)
		{
			zleceniaProdukcyjneFactory = new ZleceniaProdukcyjneFactory();
		}
		return zleceniaProdukcyjneFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ZleceniaProdukcyjneImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ZleceniaProdukcyjneImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public ZleceniaProdukcyjneImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System ZleceniaProdukcyjneImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System ZleceniaProdukcyjneImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				
				DSApi.context().begin();
				importZleceniaProdukcyjne();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importZleceniaProdukcyjne(){		
		try{
			// Pobranie zlecenia produkcyjne z [WS]
			List<ZleceniaProdukcyjne> listaNowych = getZleceniaProdukcyjneFactory().PobierzZleceniaProdukcyjneWS();

			// Pobranie zlecenia produkcyjne z [BD]	
			List<ZleceniaProdukcyjne> listaStarych = getZleceniaProdukcyjneFactory().pobierzZleceniaProdukcyjneZBD();

			// Połączenie starej i nowej listy zlecen produktow
			ZleceniaProdukcyjneFactory zleceniaProdukcyjneFactory = new ZleceniaProdukcyjneFactory();
			List<ZleceniaProdukcyjne> listaAktualna = zleceniaProdukcyjneFactory.aktualizujZleceniaProdukcyjne(listaNowych, listaStarych);

			// Zapisanie aktualnej listy zlecenia produktow do bazy danych
			zleceniaProdukcyjneFactory.zapiszZleceniaProdukcyjneDoBD(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}

}
