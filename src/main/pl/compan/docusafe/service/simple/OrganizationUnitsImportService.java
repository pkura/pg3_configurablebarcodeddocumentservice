package pl.compan.docusafe.service.simple;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.general.canonical.factory.JednostkaOrganizacyjnaFactory;
import pl.compan.docusafe.general.canonical.factory.PracownikFactory;
import pl.compan.docusafe.general.canonical.factory.StrukturaOrganizacyjnaFactory;
import pl.compan.docusafe.general.canonical.model.JednostkaOrganizacyjna;
import pl.compan.docusafe.general.canonical.model.Pracownik;
import pl.compan.docusafe.general.hibernate.dao.StrukturaOrganizacyjnaDBFactory;
import pl.compan.docusafe.general.ldap.factory.LDAPFactory;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class OrganizationUnitsImportService extends ServiceDriver {
	public static final Logger log = LoggerFactory
			.getLogger(OrganizationUnitsImportService.class);
	// private final static StringManager sm =
	// GlobalPreferences.loadPropertiesFile(WsskOrganizationalUnitsImportService.class.getPackage().getName(),
	// null);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;

	private JednostkaOrganizacyjnaFactory joFactory;

	private JednostkaOrganizacyjnaFactory getJednostkaOrganizacyjnaFactory() {
		if (joFactory == null) {
			joFactory = new JednostkaOrganizacyjnaFactory();

		}
		return joFactory;
	}

	private PracownikFactory pracownikFactory;

	private PracownikFactory getPracownikFactory() {
		if (pracownikFactory == null) {
			pracownikFactory = new PracownikFactory();
		}
		return pracownikFactory;
	}

	class ImportTimesProperty extends Property {
		public ImportTimesProperty() {
			super(SIMPLE, PERSISTENT, OrganizationUnitsImportService.this,
					"period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi() {
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (OrganizationUnitsImportService.this) {
				if (object != null) {
					period = (Integer) object;
					stop();
				} else
					period = 24;
			}
		}
	}

	public OrganizationUnitsImportService() {
		properties = new Property[] { new ImportTimesProperty() };
	}

	@Override
	protected void start() throws ServiceException {
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		console(Console.INFO, "System  OrganizationUnitsImport Częstotliwość: "+period+" h.");
		log.info("System OrganizationUnitsImport Częstotliwość: " + period
				+ " h.");
	}

	@Override
	protected void stop() throws ServiceException {
		if (timer != null) {
			timer.cancel();
			timer = new Timer(true);
			timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
			console(Console.INFO, "System  OrganizationUnitsImport Częstotliwość: "+period+" h.");
			log.info("System OrganizationUnitsImport Częstotliwość: " + period
					+ " h.");
		}
	}

	@Override
	protected boolean canStop() {
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			// Zaimportowanie struktury organizacyjnej
			importStrukturyOrg();

			// Zaimportowanie użytkowników
			importUzytkownikow();
		}
	}

	/**
	 * Metoda pobiera strukturę organizacyjną z webserwisu - wystawionego przez
	 * Simple. Następnie pobiera aktualnie obowiązującą strukturę w DocuSafe
	 * Tworzy nową (zaktualizowaną) strukturę w DocuSafe Zapisuje zaktualizowaną
	 * strukturę DocuSafe jako obowiązującą.
	 */
	public void importStrukturyOrg() {

		// Pobranie jednostek organizacyjnych z [WS]
		List<JednostkaOrganizacyjna> listaJONowych = getJednostkaOrganizacyjnaFactory()
				.PobierzJednostkiOrganizacyjneWS();

		// Pobranie jednostek organizacyjnych z [BD]
		List<JednostkaOrganizacyjna> listaJOStarych = getJednostkaOrganizacyjnaFactory()
				.pobierzJednostkiOrganizacyjneZBD();

		// Połączenie starej i nowej struktury organizacyjnej
		StrukturaOrganizacyjnaFactory soFactory = new StrukturaOrganizacyjnaFactory();

		List<JednostkaOrganizacyjna> aktualnaStruktura = soFactory
				.PolaczStrukturyOrganizacyjne(listaJONowych, listaJOStarych);

		// Zapisanie aktualnej struktury organizacyjnej do bazy danych
		StrukturaOrganizacyjnaDBFactory strukturaOrganizacyjnaDBFactory = new StrukturaOrganizacyjnaDBFactory();

		strukturaOrganizacyjnaDBFactory.ZapiszStruktureDoBD(aktualnaStruktura);

	}

	/**
	 * Metoda pobiera użytkowników z webserwisu - wystawionego przez Simple
	 * Następnie użytkowników wprowadza do systemu DocuSafe
	 */
	public void importUzytkownikow() {
		// Pobranie użytkowników z [WS]
		List<Pracownik> listaPracownikowNowa = getPracownikFactory().PobierzPracownikowWS();

		// Aktualizowanie pobranym użytkownikom loginu z LDAP
        if (getImportPracownikowAktualizujLoginZLdap()) {
			LDAPFactory ldapFactory = new LDAPFactory();
			listaPracownikowNowa = ldapFactory.aktualizujLoginZLDAPDlaPracownikow(listaPracownikowNowa);
		}

		// Pobranie użytkowników z [BD]
		List<Pracownik> listaPracownikowStara = getPracownikFactory().PobierzPracownikowDB();

		// Połączenie starej i nowej listy pracowników
		PracownikFactory pracownikFactory = new PracownikFactory();
		List<Pracownik> listaPracownikowAktualna = pracownikFactory.aktualizujListePracownikow(listaPracownikowNowa, listaPracownikowStara);

		// Aktualizowanie pobranym użytkownikom loginu z LDAP
        if (getImportPracownikowErpWykonajMapowanie()) {
			// Zaktualizowanie przypisania użytkowników do struktury organizacyjnej (opcjonalny)
			listaPracownikowAktualna = pracownikFactory.aktualizujPrzpisanieDoKomorekOrganizacyjnych(listaPracownikowAktualna);
		}
		
		// Zapisanie aktualnej listy pracownikow do bazy danych
		pracownikFactory.zapiszListePracownikowDoBD(listaPracownikowAktualna);
	}

    private boolean getImportPracownikowErpWykonajMapowanie() {
        return Boolean.valueOf(Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_ERP_WYKONAJ_MAPOWANIE_PERSONAGREEMENT"));
    }

    private boolean getImportPracownikowAktualizujLoginZLdap() {
        return Boolean.valueOf(Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_AKTUALIZUJ_LOGIN_Z_LDAP"));
    }

    public Property[] getProperties() {
		return properties;

	}

	public void setProperties(Property[] properties) {
		this.properties = properties;
	}
}
