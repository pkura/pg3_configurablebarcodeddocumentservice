package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.ProjektBudzetZasobFactory;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetZasob;
import pl.compan.docusafe.general.hibernate.dao.ProjectBudgetItemResourceDBDAO;
import pl.compan.docusafe.general.hibernate.model.ProjektBudzetZasobDB;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.BudzetKoKomorekImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ProjektBudzetuZasobImportService extends ServiceDriver{
	public static final Logger log = LoggerFactory.getLogger(ProjektBudzetuZasobImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private ProjektBudzetZasobFactory projektBudzetZasobFactory;
	
	private ProjektBudzetZasobFactory getProjektBudzetZasobFactory()
	{
		if(projektBudzetZasobFactory == null)
		{
			projektBudzetZasobFactory = new ProjektBudzetZasobFactory();
		}
		return projektBudzetZasobFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ProjektBudzetuZasobImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ProjektBudzetuZasobImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public ProjektBudzetuZasobImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System ProjektBudzetuZasobImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System ProjektBudzetuZasobImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importProjektBudzetZasob();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Blad synchronizacji-----");
				log.info(e.getMessage(), e);
			} 
		}
	}

	public void importProjektBudzetZasob(){		
		try{
			// Pobranie zasoby z projektow budzetow z [WS]
			List<ProjektBudzetZasob> listaNowych = getProjektBudzetZasobFactory().PobierzProjektBudzetZasobWS();

			// Pobranie zasoby z projektow budzetow z [BD]	
			List<ProjektBudzetZasob> listaStarych = getProjektBudzetZasobFactory().pobierzProjektBudzetZasobZBD();

			// Połączenie starej i nowej listy zasobow z projektow budzetow
			ProjektBudzetZasobFactory projektBudzetZasobFactory = new ProjektBudzetZasobFactory();
			List<ProjektBudzetZasob> listaAktualna = projektBudzetZasobFactory.aktualizujProjektBudzetZasob(listaNowych, listaStarych);

			// Zapisanie aktualnej listy zasobow z projektow budzetow do bazy danych
			projektBudzetZasobFactory.zapiszProjektBudzetZasobDoBD(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
