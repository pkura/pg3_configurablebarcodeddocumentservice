package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.ProjektBudzetFactory;
import pl.compan.docusafe.general.canonical.model.ProjektBudzet;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.WalutaImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ProjektBudzetImportService extends ServiceDriver{
	public static final Logger log = LoggerFactory.getLogger(ProjektBudzetImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private ProjektBudzetFactory projektBudzetFactory;
	
	private ProjektBudzetFactory getProjektBudzetFactory()
	{
		if(projektBudzetFactory == null)
		{
			projektBudzetFactory = new ProjektBudzetFactory();
		}
		return projektBudzetFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ProjektBudzetImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ProjektBudzetImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public ProjektBudzetImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System ProjektBudzetImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System ProjektBudzetImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importProjektBudzet();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Wystapil blad podczas synchronizacji-----");
				log.info(e.getMessage(), e);
			} 
		}
	}

	public void importProjektBudzet(){		
		try{
			// Pobranie projekty budzetow z [WS]
			List<ProjektBudzet> listaNowych = getProjektBudzetFactory().PobierzProjektBudzetWS();

			// Pobranie projekty budzetow z [BD]	
			List<ProjektBudzet> listaStarych = getProjektBudzetFactory().pobierzProjektBudzetZBD();

			// Połączenie starej i nowej listy projektow budzetow
			ProjektBudzetFactory projektBudzetFactory = new ProjektBudzetFactory();
			List<ProjektBudzet> listaAktualna = projektBudzetFactory.aktualizujProjektBudzet(listaNowych, listaStarych);

			// Zapisanie aktualnej listy projektow budzetow do bazy danych
			projektBudzetFactory.zapiszProjektBudzetDoBD(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}

}
