package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.StawkaVatDBFactory;
import pl.compan.docusafe.general.canonical.factory.StawkaVatFactory;
import pl.compan.docusafe.general.canonical.factory.StawkaVatWSFactory;
import pl.compan.docusafe.general.canonical.model.StawkaVat;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class StawkaVatImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(StawkaVatImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private StawkaVatDBFactory svDBFactory;

	private StawkaVatDBFactory getStawkaVatDBFactory()
	{
		if (svDBFactory==null)
		{
			svDBFactory = new StawkaVatDBFactory();
		}
		return svDBFactory;
	}


	private StawkaVatWSFactory stawkaVatFactory;

	public StawkaVatWSFactory getStawkaVatWSFactory() {
		if (stawkaVatFactory==null)
		{
			stawkaVatFactory = new StawkaVatWSFactory();
		}
		return stawkaVatFactory;
	}



	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, StawkaVatImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (StawkaVatImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public StawkaVatImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System StawkaVatImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System StawkaVatImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importStawkaVat();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (Exception e) {
				try {
					DSApi.context().rollback();
				} catch (EdmException e1) {
					log.info(e.getMessage(), e);
				}
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importStawkaVat(){				 
		try{
			List<StawkaVat> listaNowych = getStawkaVatWSFactory().PobierzStawkaVat();
			List<StawkaVat> listaStarych = getStawkaVatDBFactory().pobierzStawkaVatZBD();

			StawkaVatFactory w = new StawkaVatFactory();

			List<StawkaVat> aktualnaStruktura = w.polaczStawkaVat(listaNowych, listaStarych);

			for (StawkaVat stawkaVat : aktualnaStruktura)
			{
				getStawkaVatDBFactory().Zapisz(stawkaVat);				
			}	
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
