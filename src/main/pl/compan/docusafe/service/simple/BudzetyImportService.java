package pl.compan.docusafe.service.simple;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.BudzetyFactory;
import pl.compan.docusafe.general.canonical.model.Budzet;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.BudzetPozycjeSimpleImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BudzetyImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(BudzetyImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private BudzetyFactory budzetyFactory;
	
	private BudzetyFactory getBudzetyFactory()
	{
		if(budzetyFactory == null)
		{
			budzetyFactory = new BudzetyFactory();
		}
		return budzetyFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, BudzetyImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (BudzetyImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public BudzetyImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System BudzetyImport Cz�stotliwo��: "+period+" h.");
		console(Console.INFO, "System BudzetyImport Cz�stotliwo��: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpocz�to synchronizacj�-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importBudzety();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zako�czono synchronizacj�-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
				console(Console.INFO, "-----Wyst�pi� b��d podczas synchronizacj�-----");
			} catch (Exception ex) {
				log.info(ex.getMessage(), ex);
				console(Console.INFO, "-----Wyst�pi� b��d podczas synchronizacj�-----");
			}
		}
	}

	public void importBudzety(){		
		// Pobranie budzety z [WS]
		List<Budzet> listaNowych = getBudzetyFactory().PobierzBudzetWS();

		// Pobranie budzety z [BD]	
		List<Budzet> listaStarych = getBudzetyFactory().pobierzBudzetZBD();

		// Po��czenie starej i nowej listy budzetow
		BudzetyFactory budzetyFactory = new BudzetyFactory();
		List<Budzet> listaAktualna = budzetyFactory.aktualizujBudzet(listaNowych, listaStarych);

		// Zapisanie aktualnej listy budzetow do bazy danych
		budzetyFactory.zapiszBudzetDoBD(listaAktualna);
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}


}
