package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.BudzetKOKomorekFactory;
import pl.compan.docusafe.general.canonical.model.BudzetKOKomorek;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BudzetKoKomorekImportService extends ServiceDriver{

	public static final Logger log = LoggerFactory.getLogger(BudzetKoKomorekImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private BudzetKOKomorekFactory kontrahentKontoFactory;

	private BudzetKOKomorekFactory getBudzetKoKomorekFactory()
	{
		if(kontrahentKontoFactory == null)
		{
			kontrahentKontoFactory = new BudzetKOKomorekFactory();
		}
		return kontrahentKontoFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, BudzetKoKomorekImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (BudzetKoKomorekImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public BudzetKoKomorekImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System BudzetKoKomorek Częstotliwość: "+period+" h.");
		console(Console.INFO, "System BudzetKoKomorek Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				
				DSApi.context().begin();
				importBudzetKoKomorek();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importBudzetKoKomorek(){		
		try{
			// Pobranie budzetow z [WS]
			List<BudzetKOKomorek> listaNowych = getBudzetKoKomorekFactory().pobierzBudzetyKoKomorekZWS();

			// Pobranie budzetow z [BD]	
			List<BudzetKOKomorek> listaStarych = getBudzetKoKomorekFactory().pobierzBudzetyKoKomorekZBD();

			// Połączenie starej i nowej listy budzetow
			BudzetKOKomorekFactory budzetKoKomorekFactory = new BudzetKOKomorekFactory();
			List<BudzetKOKomorek> listaAktualna = budzetKoKomorekFactory.aktualizujBudzetyKoKomorek(listaNowych, listaStarych);

			// Zapisanie aktualnej listy budzetow do bazy danych

			budzetKoKomorekFactory.ZapiszBudzetyKoKomorekDoDB(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;	
	}
}