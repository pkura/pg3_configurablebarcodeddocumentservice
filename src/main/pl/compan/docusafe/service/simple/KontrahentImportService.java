package pl.compan.docusafe.service.simple;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.KontrahentDBFactory;
import pl.compan.docusafe.general.canonical.factory.KontrahentFactory;
import pl.compan.docusafe.general.canonical.factory.KontrahentWSFactory;
import pl.compan.docusafe.general.canonical.model.Kontrahent;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.BudzetyImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class KontrahentImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(KontrahentImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private KontrahentDBFactory kontrahentDBFactory;

	private KontrahentDBFactory getKontrahentDBFactory()
	{
		if (kontrahentDBFactory==null)
		{
			kontrahentDBFactory = new KontrahentDBFactory();
		}
		return kontrahentDBFactory;
	}


	private KontrahentWSFactory kontrahentFactory;

	public KontrahentWSFactory getKontrahentWSFactory() {
		if (kontrahentFactory==null)
		{
			kontrahentFactory = new KontrahentWSFactory();
		}
		return kontrahentFactory;
	}



	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, KontrahentImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (KontrahentImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public KontrahentImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System KontrahentImport Częstotliwo¶ć: "+period+" h.");
		console(Console.INFO, "System KontrahentImport Częstotliwo¶ć: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				importKontrahent();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	/**
	 * Metoda pobiera kontrahentów z webserwisu - wystawionego przez Simple.
	 * Dodaje nowych kontrahentów albo aktualizuje już istniejących.
	 */
	public void importKontrahent(){				 

		// Pobranie kontrahentów z [WS]
		List<Kontrahent> listaNowych = getKontrahentWSFactory().PobierzKontrahentow();

		// Pobranie kontrahentów z [BD]
		List<Kontrahent> listaStarych = getKontrahentDBFactory().pobierzKontrahentaZBD();

		KontrahentFactory kontrahent = new KontrahentFactory();

		List<Kontrahent> aktualnaStruktura = kontrahent.sprawdzKontrahent(listaNowych, listaStarych);

		for (Kontrahent kont : aktualnaStruktura)
		{
			getKontrahentDBFactory().Zapisz(kont);				
		}	
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
