package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.PlanZakupowFactory;
import pl.compan.docusafe.general.canonical.model.PlanZakupow;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.WalutaImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PlanZakupowImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(PlanZakupowImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private PlanZakupowFactory planZakupowFactory;

	private PlanZakupowFactory getPlanZakupowFactory()
	{
		if(planZakupowFactory == null)
		{
			planZakupowFactory = new PlanZakupowFactory();
		}
		return planZakupowFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, PlanZakupowImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (PlanZakupowImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public PlanZakupowImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System PlanZakupowImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System PlanZakupowImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();		
				importPlanZakupow();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importPlanZakupow(){		
		try{
			// Pobranie planu zakupow z [WS]
			List<PlanZakupow> listaNowych = getPlanZakupowFactory().pobierzPlanZakupowZWS();

			// Pobranie planu zakupow z [BD]	
			List<PlanZakupow> listaStarych = getPlanZakupowFactory().pobierzPlanZakupowZBD();

			// Połączenie starej i nowej listy planu zakupow
			PlanZakupowFactory planZakupowFactory = new PlanZakupowFactory();
			List<PlanZakupow> listaAktualna = planZakupowFactory.aktualizujPlanZakupow(listaNowych, listaStarych);

			// Zapisanie aktualnej listy planu zakupow do bazy danych
			planZakupowFactory.ZapiszPlanZakupowDoDB(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}

}
