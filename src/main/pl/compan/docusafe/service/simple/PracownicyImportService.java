package pl.compan.docusafe.service.simple;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.PracownikERPFactory;
import pl.compan.docusafe.general.canonical.model.Pracownik;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.PlanZakupowImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PracownicyImportService extends ServiceDriver{
	public static final Logger log = LoggerFactory.getLogger(PracownicyImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private PracownikERPFactory pracownikERPFactory;
	
	private PracownikERPFactory getPracownikERPFactory()
	{
		if(pracownikERPFactory == null)
		{
			pracownikERPFactory = new PracownikERPFactory();
		}
		return pracownikERPFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, PracownicyImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (PracownicyImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public PracownicyImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System PracownicyImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System PracownicyImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importPracownikow();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importPracownikow(){		
		// Pobranie pracownikow z [WS]
		List<Pracownik> listaNowych = getPracownikERPFactory().PobierzPracownikWS();

		// Pobranie pracownikow z [BD]	
		List<Pracownik> listaStarych = getPracownikERPFactory().pobierzPracownikZBD();

		// Połączenie starej i nowej listy pracownikow
		PracownikERPFactory pracownikERPFactory = new PracownikERPFactory();
		List<Pracownik> listaAktualna = pracownikERPFactory.aktualizujPracownikow(listaNowych, listaStarych);

		// Zapisanie aktualnej listy pracownikow do bazy danych
		pracownikERPFactory.zapiszPracownikowDoBD(listaAktualna);
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}

}
