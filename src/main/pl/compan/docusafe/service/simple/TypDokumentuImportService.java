package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.TypDokumentuDBFactory;
import pl.compan.docusafe.general.canonical.factory.TypDokumentuFactory;
import pl.compan.docusafe.general.canonical.factory.TypDokumentuWSFactory;
import pl.compan.docusafe.general.canonical.model.TypDokumentu;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class TypDokumentuImportService extends ServiceDriver{
	public static final Logger log = LoggerFactory.getLogger(TypDokumentuImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private TypDokumentuDBFactory tdDBFactory;

	private TypDokumentuDBFactory getTypDokumentuDBFactory()
	{
		if (tdDBFactory==null)
		{
			tdDBFactory = new TypDokumentuDBFactory();
		}
		return tdDBFactory;
	}


	private TypDokumentuWSFactory tdFactory;

	public TypDokumentuWSFactory getTypDokumentuWSFactory() {
		if (tdFactory==null)
		{
			tdFactory = new TypDokumentuWSFactory();
		}
		return tdFactory;
	}



	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, TypDokumentuImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (TypDokumentuImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public TypDokumentuImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System TypDokumentuImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System TypDokumentuImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importTypDokumentu();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Blad synchronizacji-----");
				log.info(e.getMessage(), e);
			} 
		}
	}

	public void importTypDokumentu(){				 
		try{
			List<TypDokumentu> listaNowych = getTypDokumentuWSFactory().PobierzTypDokumentu();
			List<TypDokumentu> listaStarych = getTypDokumentuDBFactory().pobierzTypDokumentuZBD();

			TypDokumentuFactory typDokumentu = new TypDokumentuFactory();

			List<TypDokumentu> aktualnaStruktura = typDokumentu.polaczTypDokumentu(listaNowych, listaStarych);

			for (TypDokumentu typDoc : aktualnaStruktura)
			{
				getTypDokumentuDBFactory().Zapisz(typDoc);				
			}	
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
