package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;



import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.ZrodloFinansowaniaProjektowFactory;
import pl.compan.docusafe.general.canonical.model.ZrodloFinansowaniaProjektow;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.WalutaImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZrodloFinansowaniaProjektowImportService extends ServiceDriver{
	public static final Logger log = LoggerFactory.getLogger(ZrodloFinansowaniaProjektowImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private ZrodloFinansowaniaProjektowFactory zrodloFinansowaniaProjektowFactory;
	
	private ZrodloFinansowaniaProjektowFactory getZrodloFinansowaniaProjektowFactory()
	{
		if(zrodloFinansowaniaProjektowFactory == null)
		{
			zrodloFinansowaniaProjektowFactory = new ZrodloFinansowaniaProjektowFactory();
		}
		return zrodloFinansowaniaProjektowFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ZrodloFinansowaniaProjektowImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ZrodloFinansowaniaProjektowImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public ZrodloFinansowaniaProjektowImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System ZrodloFinansowaniaProjektowImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System ZrodloFinansowaniaProjektowImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importZrodloFinansowaniaProjektow();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Blad synchronizacji-----");
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importZrodloFinansowaniaProjektow(){		
		try{
			// Pobranie zrodla finansowania projektow z [WS]
			List<ZrodloFinansowaniaProjektow> listaNowych = getZrodloFinansowaniaProjektowFactory().PobierzZrodloFinansowaniaProjektowWS();

			// Pobranie zrodla finansowania projektow z [BD]	
			List<ZrodloFinansowaniaProjektow> listaStarych = getZrodloFinansowaniaProjektowFactory().pobierzZrodloFinansowaniaProjektowZBD();

			// Połączenie starej i nowej listy zrodla finansowania projektow
			ZrodloFinansowaniaProjektowFactory zrodloFinansowaniaProjektowFactory = new ZrodloFinansowaniaProjektowFactory();
			List<ZrodloFinansowaniaProjektow> listaAktualna = zrodloFinansowaniaProjektowFactory.aktualizujZrodloFinansowaniaProjektow(listaNowych, listaStarych);

			// Zapisanie aktualnej listy zrodla finansowania projektow do bazy danych
			zrodloFinansowaniaProjektowFactory.zapiszZrodloFinansowaniaProjektowDoBD(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}

}
