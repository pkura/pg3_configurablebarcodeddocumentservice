package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.RoleWProjekcieFactory;
import pl.compan.docusafe.general.canonical.model.RoleWProjekcie;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.MiejsceDostawyImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RoleWProjekcieImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(RoleWProjekcieImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private RoleWProjekcieFactory roleWProjekcieFactory;

	private RoleWProjekcieFactory getRoleWProjekcieFactory()
	{
		if(roleWProjekcieFactory == null)
		{
			roleWProjekcieFactory = new RoleWProjekcieFactory();
		}
		return roleWProjekcieFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, RoleWProjekcieImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (RoleWProjekcieImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public RoleWProjekcieImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System RoleWProjekcieImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System RoleWProjekcieImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importRoleWProjekcie();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importRoleWProjekcie(){		
		try{
		// Pobranie rol w projekcie z [WS]
		List<RoleWProjekcie> listaNowych = getRoleWProjekcieFactory().pobierzRoleWProjekcieZWS();

		// Pobranie rol w projekcie z [BD]	
		List<RoleWProjekcie> listaStarych = getRoleWProjekcieFactory().pobierzRoleWProjekcieZBD();

		// Połączenie starej i nowej listy rol w projekcie
		RoleWProjekcieFactory roleWProjekcieFactory = new RoleWProjekcieFactory();
		List<RoleWProjekcie> listaAktualna = roleWProjekcieFactory.aktualizujRoleWProjekcie(listaNowych, listaStarych);

		// Zapisanie aktualnej listy rol w projekcie do bazy danych
		roleWProjekcieFactory.ZapiszRoleWProjekcieDoDB(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
		
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
