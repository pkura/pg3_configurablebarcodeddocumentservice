package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.JednostkaMiaryDBFactory;
import pl.compan.docusafe.general.canonical.factory.JednostkaMiaryFactory;
import pl.compan.docusafe.general.canonical.factory.JednostkaMiaryWSFactory;
import pl.compan.docusafe.general.canonical.model.JednostkaMiary;
import pl.compan.docusafe.general.hibernate.model.UnitOfMeasureDB;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.MiejsceDostawyImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class JednostkaMiaryImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(JednostkaMiaryImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private JednostkaMiaryDBFactory jednostkaMiaryDBFactory;

	private JednostkaMiaryDBFactory getJednostkaMiaryDBFactory()
	{
		if (jednostkaMiaryDBFactory==null)
		{
			jednostkaMiaryDBFactory = new JednostkaMiaryDBFactory();
		}
		return jednostkaMiaryDBFactory;
	}


	private JednostkaMiaryWSFactory jednostkaMiaryFactory;

	public JednostkaMiaryWSFactory getJednostkaMiaryWSFactory() {
		if (jednostkaMiaryFactory==null)
		{
			jednostkaMiaryFactory = new JednostkaMiaryWSFactory();
		}
		return jednostkaMiaryFactory;
	}



	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, JednostkaMiaryImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (JednostkaMiaryImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public JednostkaMiaryImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System JednostkaMiaryImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System JednostkaMiaryImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
			timer = new Timer();
			timer.schedule(new Import(), 0, period * DateUtils.HOUR);
			log.info("System JednostkaMiaryImportService wystartowal. Częstotliwość: "+period+" h.");
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importJednostkaMiary();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Blad synchronizacji-----");
				log.info(e.getMessage(), e);
			} 
		}
	}

	public void importJednostkaMiary(){				 

		List<JednostkaMiary> listaNowych;
		try {
			listaNowych = getJednostkaMiaryWSFactory().PobierzJednostkaMiary();

			List<JednostkaMiary> listaStarych = getJednostkaMiaryDBFactory().pobierzJednostkaMiaryZBD();

			JednostkaMiaryFactory jednostkaMiary = new JednostkaMiaryFactory();

			List<JednostkaMiary> aktualnaStruktura = jednostkaMiary.sprawdzJednostkaMiary(listaNowych, listaStarych);

			for (JednostkaMiary jm : aktualnaStruktura)
			{
				getJednostkaMiaryDBFactory().Zapisz(jm);				
			}	
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}

}
