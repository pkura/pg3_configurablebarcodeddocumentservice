package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.ProjektFactory;
import pl.compan.docusafe.general.canonical.model.Projekt;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.WalutaImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ProjektImportService extends ServiceDriver{
	public static final Logger log = LoggerFactory.getLogger(ProjektImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private ProjektFactory projektFactory;
	
	private ProjektFactory getProjektFactory()
	{
		if(projektFactory == null)
		{
			projektFactory = new ProjektFactory();
		}
		return projektFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ProjektImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ProjektImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public ProjektImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System ProjektImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System ProjektImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importProjekt();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Blad synchronizacji-----");
				log.info(e.getMessage(), e);
			} 
		}
	}

	public void importProjekt(){		
		try{
			// Pobranie projekty z [WS]
			List<Projekt> listaNowych = getProjektFactory().PobierzProjektWS();

			// Pobranie projekty z [BD]	
			List<Projekt> listaStarych = getProjektFactory().pobierzProjektZBD();

			// Połączenie starej i nowej listy projektow
			ProjektFactory projektFactory = new ProjektFactory();
			List<Projekt> listaAktualna = projektFactory.aktualizujProjekt(listaNowych, listaStarych);

			// Zapisanie aktualnej listy projektow do bazy danych
			projektFactory.zapiszProjektDoBD(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
