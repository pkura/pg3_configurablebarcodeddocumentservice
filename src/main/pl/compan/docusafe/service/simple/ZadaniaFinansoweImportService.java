package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.ZadaniaFinansoweFactory;
import pl.compan.docusafe.general.canonical.model.ZadaniaFinansowe;
import pl.compan.docusafe.general.hibernate.dao.FinancialTaskDBDAO;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.WalutaImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZadaniaFinansoweImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(ZadaniaFinansoweImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private ZadaniaFinansoweFactory zadaniaFinansoweFactory;
	
	private ZadaniaFinansoweFactory getZadaniaFinansoweFactory()
	{
		if(zadaniaFinansoweFactory == null)
		{
			zadaniaFinansoweFactory = new ZadaniaFinansoweFactory();
		}
		return zadaniaFinansoweFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ZadaniaFinansoweImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ZadaniaFinansoweImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public ZadaniaFinansoweImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System ZadaniaFinansoweImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System ZadaniaFinansoweImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();

				DSApi.context().begin();
				importZadaniaFinansowe();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Blad synchronizacji-----");
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importZadaniaFinansowe(){	
		try{
		// Pobranie zadan finansoweych z [WS]
		List<ZadaniaFinansowe> listaNowych = getZadaniaFinansoweFactory().PobierzZadaniaFinansoweWS();

		// Pobranie zamowien dostawcow z [BD]	
		List<ZadaniaFinansowe> listaStarych = getZadaniaFinansoweFactory().pobierzZadaniaFinansoweZBD();

		// Połączenie starej i nowej listy zadan dostawcy
		ZadaniaFinansoweFactory zadaniaFinansoweFactory = new ZadaniaFinansoweFactory();
		List<ZadaniaFinansowe> listaAktualna = zadaniaFinansoweFactory.aktualizujZadaniaFinansowe(listaNowych, listaStarych);

		// Zapisanie aktualnej listy zadan finansowych do bazy danych
		zadaniaFinansoweFactory.zapiszZadaniaFinansoweDoBD(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}


}
