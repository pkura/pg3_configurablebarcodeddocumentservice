package pl.compan.docusafe.service.simple;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.TypyUrlopowFactory;
import pl.compan.docusafe.general.canonical.model.TypyUrlopow;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.TypDokumentuImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class TypyUrlopowImportService extends ServiceDriver{
	public static final Logger log = LoggerFactory.getLogger(TypyUrlopowImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private TypyUrlopowFactory typyUrlopowFactory;
	
	private TypyUrlopowFactory getTypyUrlopowFactory()
	{
		if(typyUrlopowFactory == null)
		{
			typyUrlopowFactory = new TypyUrlopowFactory();
		}
		return typyUrlopowFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, TypyUrlopowImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (TypyUrlopowImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public TypyUrlopowImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System TypyUrlopowImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System TypyUrlopowImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importTypyUrlopow();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importTypyUrlopow(){		
		// Pobranie typow urlopow z [WS]
		List<TypyUrlopow> listaNowych = getTypyUrlopowFactory().pobierzTypyUrlopoweZWS();

		// Pobranie typow urlopow z [BD]	
		List<TypyUrlopow> listaStarych = getTypyUrlopowFactory().pobierzTypyUrlopowZBD();

		// Połączenie starej i nowej listy typow urlopu
		TypyUrlopowFactory typyUrlopowFactory = new TypyUrlopowFactory();
		List<TypyUrlopow> listaAktualna = typyUrlopowFactory.aktualizujTypyUrlopow(listaNowych, listaStarych);

		// Zapisanie aktualnej listy typow urlopu do bazy danych
		typyUrlopowFactory.ZapiszTypyUrlopowoDB(listaAktualna);
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
