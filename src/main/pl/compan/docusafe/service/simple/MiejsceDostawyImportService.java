package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.MiejsceDostawyBDFactory;
import pl.compan.docusafe.general.canonical.factory.MiejsceDostawyFactory;
import pl.compan.docusafe.general.canonical.factory.MiejsceDostawyWSFactory;
import pl.compan.docusafe.general.canonical.model.MiejsceDostawy;
import pl.compan.docusafe.general.hibernate.model.DeliveryLocationDB;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class MiejsceDostawyImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(MiejsceDostawyImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private MiejsceDostawyBDFactory mdDBFactory;

	private MiejsceDostawyBDFactory getMiejsceDostawyBDFactory()
	{
		if (mdDBFactory==null)
		{
			mdDBFactory = new MiejsceDostawyBDFactory();
		}
		return mdDBFactory;
	}


	private MiejsceDostawyWSFactory mdFactory;

	public MiejsceDostawyWSFactory getMiejsceDostawyWSFactory() {
		if (mdFactory==null)
		{
			mdFactory = new MiejsceDostawyWSFactory();
		}
		return mdFactory;
	}



	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, MiejsceDostawyImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (MiejsceDostawyImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public MiejsceDostawyImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System MiejsceDostawyImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System MiejsceDostawyImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importMiejsceDostawy();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Blad synchronizacji-----");
				log.info(e.getMessage(), e);
			} 
		}
	}

	public void importMiejsceDostawy(){				 

		List<MiejsceDostawy> listaNowych;
		try {
			listaNowych = getMiejsceDostawyWSFactory().PobierzMiejsceDostawy();

			List<MiejsceDostawy> listaStarych = getMiejsceDostawyBDFactory().pobierzMiejsceDostawyZBD();

			MiejsceDostawyFactory miejsceDostawy = new MiejsceDostawyFactory();

			List<MiejsceDostawy> aktualnaStruktura = miejsceDostawy.polaczMiejsceDostawy(listaNowych, listaStarych);

			for (MiejsceDostawy md : aktualnaStruktura)
			{
				getMiejsceDostawyBDFactory().Zapisz(md);				
			}	
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
