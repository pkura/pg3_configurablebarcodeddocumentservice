package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.TypPlatnosciDBFactory;
import pl.compan.docusafe.general.canonical.factory.TypPlatnosciFactory;
import pl.compan.docusafe.general.canonical.factory.TypPlatnosciWSFactory;
import pl.compan.docusafe.general.canonical.model.TypPlatnosci;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class TypPlatnosciImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(TypPlatnosciImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private TypPlatnosciDBFactory tpDBFactory;

	private TypPlatnosciDBFactory getTypPlatnosciDBFactory()
	{
		if (tpDBFactory==null)
		{
			tpDBFactory = new TypPlatnosciDBFactory();
		}
		return tpDBFactory;
	}


	private TypPlatnosciWSFactory typPlatnosciFactory;

	public TypPlatnosciWSFactory getTypPlatnosciWSFactory() {
		if (typPlatnosciFactory==null)
		{
			typPlatnosciFactory = new TypPlatnosciWSFactory();
		}
		return typPlatnosciFactory;
	}



	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, TypPlatnosciImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (TypPlatnosciImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public TypPlatnosciImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, 12*DateUtils.HOUR);
		log.info("System TypPlatnosciImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System TypPlatnosciImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importTypPlatnosci();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Blad synchronizacji-----");
				log.info(e.getMessage(), e);
			} 
		}
	}

	public void importTypPlatnosci(){				 
		try{
			List<TypPlatnosci> listaNowych = getTypPlatnosciWSFactory().PobierzTypPlatnosci();

			List<TypPlatnosci> listaStarych = getTypPlatnosciDBFactory().pobierzTypPlatnosciZBD();

			TypPlatnosciFactory tpFactory = new TypPlatnosciFactory();

			List<TypPlatnosci> aktualnaStruktura = tpFactory.polaczTypyPlatnosci(listaNowych, listaStarych);

			for (TypPlatnosci tp : aktualnaStruktura)
			{
				getTypPlatnosciDBFactory().Zapisz(tp);				
			}
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
