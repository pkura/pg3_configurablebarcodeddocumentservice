package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.ProduktyFakturyDBFactory;
import pl.compan.docusafe.general.canonical.factory.ProduktyFakturyFactory;
import pl.compan.docusafe.general.canonical.factory.ProduktyFakturyWSFactory;
import pl.compan.docusafe.general.canonical.model.ProduktyFaktury;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.WalutaImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ProduktyFakturyImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(ProduktyFakturyImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private ProduktyFakturyDBFactory pfDBFactory;

	private ProduktyFakturyDBFactory getProduktyFakturyDBFactory()
	{
		if (pfDBFactory==null)
		{
			pfDBFactory = new ProduktyFakturyDBFactory();
		}
		return pfDBFactory;
	}


	private ProduktyFakturyWSFactory produktyFakturyFactory;

	public ProduktyFakturyWSFactory getProduktyFakturyWSFactory() {
		if (produktyFakturyFactory==null)
		{
			produktyFakturyFactory = new ProduktyFakturyWSFactory();
		}
		return produktyFakturyFactory;
	}



	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ProduktyFakturyImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ProduktyFakturyImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public ProduktyFakturyImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System ProduktyFakturyImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System ProduktyFakturyImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importProduktyFaktury();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Blad synchronizacji-----");
				log.info(e.getMessage(), e);
			} 
		}
	}

	public void importProduktyFaktury(){				 
		try{
			List<ProduktyFaktury> listaNowych = getProduktyFakturyWSFactory().PobierzProduktyFaktury();
			List<ProduktyFaktury> listaStarych = getProduktyFakturyDBFactory().pobierzProduktyFakturyZBD();

			ProduktyFakturyFactory pff = new ProduktyFakturyFactory();

			List<ProduktyFaktury> aktualnaStruktura = pff.polaczProduktyFaktury(listaNowych, listaStarych);

			for (ProduktyFaktury pf : aktualnaStruktura)
			{
				getProduktyFakturyDBFactory().Zapisz(pf);				
			}
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
