package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jbpm.api.Execution;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.ProcessInstance;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.general.hibernate.dao.FakturaExportStatusDBDAO;
import pl.compan.docusafe.general.hibernate.model.FakturaExportStatusDB;
import pl.compan.docusafe.general.jbpm.WyslijDoERP;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.ws.client.simple.general.RequestServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.RequestServiceFactory.SimpleErpStatusFaktury;

public class RequestInvoiceScheduled extends ServiceDriver{

	private final static Logger log = LoggerFactory.getLogger(RequestInvoiceScheduled.class);
	private final static Log loger = LogFactory.getLog("messaging_log");
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(RequestInvoiceScheduled.class.getPackage().getName(),null) ;
	private final static int sprawdzStatusFakturyCoIle = Integer.parseInt(Docusafe.getAdditionProperty("RequestInvoiceScheduled.sprawdzStatusFaktury.coIle"));
	
	private Timer sprawdzStatusFakturyTimer;
	
	class SprawdzStatusFakturyTask extends TimerTask{

		@Override
		public void run() {
			List<FakturaExportStatusDB> listaWyeksportowanychFaktur = FakturaExportStatusDBDAO.list();
			
			for(final FakturaExportStatusDB fakturaExportStatusDB: listaWyeksportowanychFaktur){

				new Thread(new Runnable(){

					@Override
					public void run() {
						 try {
							 String guid = fakturaExportStatusDB.getGuid();
							 Long documentId = fakturaExportStatusDB.getDocumentId();
							 loger.info("Id " + documentId + " Guid faktury kosztowej: " + guid);
							 SimpleErpStatusFaktury statusFaktury = new RequestServiceFactory().getStatusForGuid(guid);
							 
							 switch(statusFaktury){
							 	case FINISHED:
							 	case INVALID:
							 	case FAULT:
									try {
										FakturaExportStatusDBDAO.delete(fakturaExportStatusDB);
								 		wyslijSygnal(documentId, statusFaktury);
										dodajWpisHistorii(documentId, statusFaktury, guid);
									} catch (Exception e){
										log.error(e.getMessage(), e);
									}
							 		break;
							 }
						 } 
						 catch (RemoteException e) {
							 log.error(e.getMessage(), e);
						 }
					}
					
				}).start();
				
			}
		}
		
		private void wyslijSygnal(Long documentId, SimpleErpStatusFaktury status){
		
			boolean contextOpened = false;
			
			try {
				contextOpened = DSApi.openContextIfNeeded();

				ExecutionService executionService = Jbpm4Provider.getInstance().getExecutionService();
				
				try {
					for(String processId: Jbpm4ProcessLocator.processIds(documentId)){
						ProcessInstance processInstance = executionService.findProcessInstanceById(processId);
					
					if(processInstance == null)
						continue;
					
					Execution executionWaiting = processInstance.findActiveExecutionIn(WyslijDoERP.PROCESS_EXECUTION_NAME);
					
					if (executionWaiting == null)
						continue;
					
					executionService.signalExecutionById(executionWaiting.getId(), status.toString());
										
				}				
			 		DSApi.context().begin();
			 		TaskSnapshot.updateByDocument(OfficeDocument.find(documentId));
			 		DSApi.context().commit();
				} catch (EdmException e) {
					log.error(e.getMessage(), e);
					DSApi.context().rollback();
				}
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
			}
			finally{
				DSApi.closeContextIfNeeded(contextOpened);
			}
		}
		
		private void dodajWpisHistorii(Long documentId, SimpleErpStatusFaktury statusFaktury, String guid){
			boolean contextOpened = false;
			
			try {
				contextOpened = DSApi.openContextIfNeeded();
				DataMartManager.addHistoryEntry(documentId, new Audit().create(InvoiceGeneralConstans.STATUS_ERP, "admin", sm.getString("statusWyslaniaDoERP",statusFaktury.toString(),guid),statusFaktury.toString()));
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
			}
			finally{
				DSApi.closeContextIfNeeded(contextOpened);
			}
		}
	}
	
	@Override
	protected void start() throws ServiceException {
		sprawdzStatusFakturyTimer = new Timer("RequestInvoiceScheduled-Timer",true);
		
		log("start");
		sprawdzStatusFakturyTimer.schedule(new SprawdzStatusFakturyTask(), 0L, RequestInvoiceScheduled.sprawdzStatusFakturyCoIle*DateUtils.MINUTE);
	}

	@Override
	protected void stop() throws ServiceException {
		if (sprawdzStatusFakturyTimer != null)
			sprawdzStatusFakturyTimer.cancel();
		
		log("stop");
	}

	@Override
	protected boolean canStop() {
		return false;
	}
	
	private void log(String message){
		log.info(message);
		console(Console.INFO, message);
	}

}