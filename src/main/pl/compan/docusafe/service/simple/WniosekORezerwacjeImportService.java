package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.WniosekORezerwacjeFactory;
import pl.compan.docusafe.general.canonical.model.WniosekORezerwacje;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.WalutaImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WniosekORezerwacjeImportService extends ServiceDriver{
	
	public static final Logger log = LoggerFactory.getLogger(WniosekORezerwacjeImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 2;
	private WniosekORezerwacjeFactory wniosekORezerwacjeFactory;

	private WniosekORezerwacjeFactory getWniosekORezerwacjeFactory()
	{
		if(wniosekORezerwacjeFactory == null)
		{
			wniosekORezerwacjeFactory = new WniosekORezerwacjeFactory();
		}
		return wniosekORezerwacjeFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, WniosekORezerwacjeImportService.this, "period", "Co ile minut", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (WniosekORezerwacjeImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public WniosekORezerwacjeImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.MINUTE);
		log.info("System WniosekORezerwacjeImport Częstotliwość: "+period+" m.");
		console(Console.INFO, "System WniosekORezerwacjeImport Częstotliwość: "+period+" m.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				
				DSApi.context().begin();
				importWniosekORezerwacje();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importWniosekORezerwacje(){		
		try{
			// Pobranie z [WS]
			console(Console.INFO, "-----poczatek importu-----");
			List<WniosekORezerwacje> listaNowych = getWniosekORezerwacjeFactory().pobierzWnioskiORezerwacjeZWS();
			console(Console.INFO, "-----sciagnieto z WS-----");
			// Pobranie z [BD]	
			List<WniosekORezerwacje> listaStarych = getWniosekORezerwacjeFactory().pobierzWniosekORezerwacjeZBD();
			console(Console.INFO, "-----sciagnieto z BD-----");
			// Połączenie starej i nowej listy 
			WniosekORezerwacjeFactory budzetKoKomorekFactory = new WniosekORezerwacjeFactory();
			List<WniosekORezerwacje> listaAktualna = budzetKoKomorekFactory.aktualizujWnioskiORezerwacje(listaNowych, listaStarych);
			console(Console.INFO, "-----zaaktualizowano-----");
			// Zapisanie aktualnej listy do bazy danych

			budzetKoKomorekFactory.zapiszWniosekORezerwacjeDoDB(listaAktualna);
			console(Console.INFO, "-----zapisano-----");
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;	
	}
}
