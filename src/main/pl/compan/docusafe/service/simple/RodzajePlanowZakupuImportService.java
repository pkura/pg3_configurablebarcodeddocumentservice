package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.RodzajePlanowZakupuFactory;
import pl.compan.docusafe.general.canonical.model.RodzajePlanowZakupu;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.PracownicyImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RodzajePlanowZakupuImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(RodzajePlanowZakupuImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private RodzajePlanowZakupuFactory rodzajePlanowZakupuFactory;

	private RodzajePlanowZakupuFactory getRodzajePlanowZakupuFactory()
	{
		if(rodzajePlanowZakupuFactory == null)
		{
			rodzajePlanowZakupuFactory = new RodzajePlanowZakupuFactory();
		}
		return rodzajePlanowZakupuFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, RodzajePlanowZakupuImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (RodzajePlanowZakupuImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public RodzajePlanowZakupuImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System RodzajePlanowZakupuImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System RodzajePlanowZakupuImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importRodzajePlanowZakupu();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importRodzajePlanowZakupu(){		
		try{
			// Pobranie rodzajow planow zakupu z [WS]
			List<RodzajePlanowZakupu> listaNowych = getRodzajePlanowZakupuFactory().pobierzRodzajePlanowZakupuZWS();

			// Pobranie rodzajow planow zakupu z [BD]	
			List<RodzajePlanowZakupu> listaStarych = getRodzajePlanowZakupuFactory().pobierzRodzajePlanowZakupuZBD();

			// Połączenie starej i nowej listy rodzajow planow zakupu
			RodzajePlanowZakupuFactory rodzajePlanowZakupuFactory = new RodzajePlanowZakupuFactory();
			List<RodzajePlanowZakupu> listaAktualna = rodzajePlanowZakupuFactory.aktualizujRodzajePlanowZakupu(listaNowych, listaStarych);

			// Zapisanie aktualnej listy rodzajow planow zakupu do bazy danych
			rodzajePlanowZakupuFactory.ZapiszRodzajePlanowZakupuDoDB(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
