package pl.compan.docusafe.service.simple;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.BudzetDoKomorkiFactory;
import pl.compan.docusafe.general.canonical.model.BudzetDoKomorki;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.BudzetKoKomorekImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BudzetDoKomorkiImportService extends ServiceDriver{

	public static final Logger log = LoggerFactory.getLogger(BudzetDoKomorkiImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private BudzetDoKomorkiFactory kontrahentKontoFactory;

	private BudzetDoKomorkiFactory getBudzetKoKomorekFactory()
	{
		if(kontrahentKontoFactory == null)
		{
			kontrahentKontoFactory = new BudzetDoKomorkiFactory();
		}
		return kontrahentKontoFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, BudzetDoKomorkiImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (BudzetDoKomorkiImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public BudzetDoKomorkiImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System BudzetDoKomorkiImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System BudzetDoKomorkiImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importBudzetDoKomorki();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importBudzetDoKomorki(){		
		// Pobranie z [WS]
		List<BudzetDoKomorki> listaNowych = getBudzetKoKomorekFactory().pobierzBudzetyDoKomorekZWS();

		// Pobranie z [BD]	
		List<BudzetDoKomorki> listaStarych = getBudzetKoKomorekFactory().pobierzBudzetyDoKomorekZBD();

		// Połączenie starej i nowej listy 
		BudzetDoKomorkiFactory budzetKoKomorekFactory = new BudzetDoKomorkiFactory();
		List<BudzetDoKomorki> listaAktualna = budzetKoKomorekFactory.aktualizujBudzetyDoKomorki(listaNowych, listaStarych);

		// Zapisanie aktualnej listy do bazy danych
		
		budzetKoKomorekFactory.zapiszBudzetDoKomorkiDoDB(listaAktualna);
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;	
	}
}
