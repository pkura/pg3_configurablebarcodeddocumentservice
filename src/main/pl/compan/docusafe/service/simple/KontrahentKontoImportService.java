package pl.compan.docusafe.service.simple;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.KontrahentKontoFactory;
import pl.compan.docusafe.general.canonical.model.KontrahentKonto;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.KontrahentImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class KontrahentKontoImportService  extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(KontrahentKontoImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private KontrahentKontoFactory kontrahentKontoFactory;

	private KontrahentKontoFactory getKontrahentKontoFactory()
	{
		if(kontrahentKontoFactory == null)
		{
			kontrahentKontoFactory = new KontrahentKontoFactory();
		}
		return kontrahentKontoFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, KontrahentKontoImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (KontrahentKontoImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public KontrahentKontoImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System  KontrahentKontoImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System  KontrahentKontoImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importKontrahentKonto();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importKontrahentKonto(){		
		// Pobranie kont bankowych z [WS]
		List<KontrahentKonto> listaNowych = getKontrahentKontoFactory().pobierzKontaBankoweZWS();

		// Pobranie kont bankowych z [BD]	
		List<KontrahentKonto> listaStarych = getKontrahentKontoFactory().pobierzKontaBankoweZBD();

		// Połączenie starej i nowej listy kont bankowych
		KontrahentKontoFactory kontrahentKontoFactory = new KontrahentKontoFactory();
		List<KontrahentKonto> listaAktualna = kontrahentKontoFactory.aktualizujKontaBankowe(listaNowych, listaStarych);

		// Zapisanie aktualnej listy kont bankowych do bazy danych
		kontrahentKontoFactory.ZapiszKontaBankoweDoDB(listaAktualna);
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}

