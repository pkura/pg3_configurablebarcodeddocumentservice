package pl.compan.docusafe.service.simple;

import java.util.List;

import com.google.common.base.Strings;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.hibernate.model.PurchaseDocumentTypeDB;
import pl.compan.docusafe.service.simple.SimpleRepositoryImportService;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralLogic;


import com.google.common.collect.Lists;

public class GeneralSimpleRepositoryImportService extends SimpleRepositoryImportService {

    @Override
    public String getDockindCn() throws EdmException {
        return InvoiceGeneralLogic.DOC_KIND_CN;
    }

    @Override
    public String getDynamicDictionaryCn() throws EdmException {
        return InvoiceGeneralLogic.POZYCJE_FAKTURY_CN;
    }

    @Override
    public Long getContextId() throws EdmException {
        return InvoiceGeneralLogic.REPOSITORY_POSITION_CONTEXT_ID;
    }

    @Override
    public Long getDictIdToOmitDictionariesUpdate() throws EdmException {
        return null;
    }

    @Override
    public List<Long> getObjectIds() throws EdmException {
        return PurchaseDocumentTypeDB.getDocumentTypesId();
    }
}
