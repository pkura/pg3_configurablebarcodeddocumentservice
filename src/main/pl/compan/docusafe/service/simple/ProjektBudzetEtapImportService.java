package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.ProjektBudzetEtapFactory;
import pl.compan.docusafe.general.canonical.model.ProjektBudzetEtap;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.WalutaImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ProjektBudzetEtapImportService extends ServiceDriver{
	public static final Logger log = LoggerFactory.getLogger(ProjektBudzetEtapImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private ProjektBudzetEtapFactory projektBudzetEtapFactory;
	
	private ProjektBudzetEtapFactory getProjektBudzetEtapFactory()
	{
		if(projektBudzetEtapFactory == null)
		{
			projektBudzetEtapFactory = new ProjektBudzetEtapFactory();
		}
		return projektBudzetEtapFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ProjektBudzetEtapImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ProjektBudzetEtapImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public ProjektBudzetEtapImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System ProjektBudzetEtapImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System ProjektBudzetEtapImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importProjektBudzetEtap();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Blad podczas synchronizacji-----");
				log.info(e.getMessage(), e);
			} 
		}
	}

	public void importProjektBudzetEtap(){		
		try{
			// Pobranie projekty budzetow etap z [WS]
			List<ProjektBudzetEtap> listaNowych = getProjektBudzetEtapFactory().PobierzProjektBudzetEtapWS();

			// Pobranie projekty budzetow etap z [BD]	
			List<ProjektBudzetEtap> listaStarych = getProjektBudzetEtapFactory().pobierzProjektBudzetEtapZBD();

			// Połączenie starej i nowej listy projektow budzetow etap
			ProjektBudzetEtapFactory projektBudzetEtapFactory = new ProjektBudzetEtapFactory();
			List<ProjektBudzetEtap> listaAktualna = projektBudzetEtapFactory.aktualizujProjektBudzetEtap(listaNowych, listaStarych);

			// Zapisanie aktualnej listy projektow budzetow etap do bazy danych
			projektBudzetEtapFactory.zapiszProjektBudzetEtapDoBD(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}

}
