package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.ZrodloFactory;
import pl.compan.docusafe.general.canonical.model.Zrodlo;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.TypyWnioskowORezerwacjeImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZrodloImportService extends ServiceDriver{
	public static final Logger log = LoggerFactory.getLogger(ZrodloImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private ZrodloFactory zrodloFactory;
	
	private ZrodloFactory getZrodloFactory()
	{
		if(zrodloFactory == null)
		{
			zrodloFactory = new ZrodloFactory();
		}
		return zrodloFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ZrodloImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ZrodloImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public ZrodloImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System ZrodloImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System ZrodloImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importZrodlo();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				console(Console.INFO, "-----Blad synchronizacji-----");
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importZrodlo(){		
		try {
			// Pobranie zrodla z [WS]
			List<Zrodlo> listaNowych;

			listaNowych = getZrodloFactory().PobierzZrodloWS();


			// Pobranie zrodla z [BD]	
			List<Zrodlo> listaStarych = getZrodloFactory().pobierzZrodloZBD();

			// Połączenie starej i nowej listy zrodla
			ZrodloFactory zrodloFactory = new ZrodloFactory();
			List<Zrodlo> listaAktualna = zrodloFactory.aktualizujZrodlo(listaNowych, listaStarych);

			// Zapisanie aktualnej listy zrodla do bazy danych
			zrodloFactory.zapiszZrodloDoBD(listaAktualna);
			
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
