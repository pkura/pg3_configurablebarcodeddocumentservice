package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.BudzetPozycjeSimpleFactory;
import pl.compan.docusafe.general.canonical.model.BudzetPozycjaSimple;
import pl.compan.docusafe.general.hibernate.dao.BudzetPozycjaSimpleDBDAO;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.MiejsceDostawyImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BudzetPozycjeSimpleImportService   extends ServiceDriver{

	public static final Logger log = LoggerFactory.getLogger(BudzetPozycjeSimpleImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private BudzetPozycjeSimpleFactory kontrahentKontoFactory;

	private BudzetPozycjeSimpleFactory getBudzetKoKomorekFactory()
	{
		if(kontrahentKontoFactory == null)
		{
			kontrahentKontoFactory = new BudzetPozycjeSimpleFactory();
		}
		return kontrahentKontoFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, BudzetPozycjeSimpleImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (BudzetPozycjeSimpleImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 12;
			}
		}
	}

	public BudzetPozycjeSimpleImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System BudzetPozycjaImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System BudzetPozycjaImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				
				DSApi.context().begin();
				importBudzetPozycjeSimple();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
				console(Console.INFO, "-----Wystąpił błąd podczas synchronizacji-----");
			} 
		}
	}

	public void importBudzetPozycjeSimple(){		
		// Pobranie kont bankowych z [WS]
		List<BudzetPozycjaSimple> listaNowych;
		try {
			listaNowych = getBudzetKoKomorekFactory().pobierzBudzetPozycjeSimpleZWS();


		// Pobranie kont bankowych z [BD]	
		List<BudzetPozycjaSimple> listaStarych = getBudzetKoKomorekFactory().pobierzBudzetPozycjeSimpleZBD();

		// Połączenie starej i nowej listy kont bankowych
		BudzetPozycjeSimpleFactory budzetKoKomorekFactory = new BudzetPozycjeSimpleFactory();
		List<BudzetPozycjaSimple> listaAktualna = budzetKoKomorekFactory.aktualizujBudzetyKoKomorek(listaNowych, listaStarych);

		// Zapisanie aktualnej listy kont bankowych do bazy danych
		
		budzetKoKomorekFactory.ZapiszBudzetyKoKomorekDoDB(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;	
	}
}
