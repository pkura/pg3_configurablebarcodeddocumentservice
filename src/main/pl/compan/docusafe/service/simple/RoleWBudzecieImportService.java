package pl.compan.docusafe.service.simple;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.RoleWBudzecieFactory;
import pl.compan.docusafe.general.canonical.model.RoleWBudzecie;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.WalutaImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RoleWBudzecieImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(RoleWBudzecieImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private RoleWBudzecieFactory roleWBudzecieFactory;

	private RoleWBudzecieFactory getRoleWBudzecieFactory()
	{
		if(roleWBudzecieFactory == null)
		{
			roleWBudzecieFactory = new RoleWBudzecieFactory();
		}
		return roleWBudzecieFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, RoleWBudzecieImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (RoleWBudzecieImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public RoleWBudzecieImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System RoleWBudzecieImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System RoleWBudzecieImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importRoleWBudzecie();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importRoleWBudzecie(){		
		try{
			// Pobranie rol w budzecie z [WS]
			List<RoleWBudzecie> listaNowych = getRoleWBudzecieFactory().pobierzRoleWBudzecieZWS();

			// Pobranie rol w budzecie z [BD]	
			List<RoleWBudzecie> listaStarych = getRoleWBudzecieFactory().pobierzRoleWBudzecieZBD();

			// Połączenie starej i nowej listy rol w budzecie
			RoleWBudzecieFactory roleWBudzecieFactory = new RoleWBudzecieFactory();
			List<RoleWBudzecie> listaAktualna = roleWBudzecieFactory.aktualizujRoleWBudzecie(listaNowych, listaStarych);

			// Zapisanie aktualnej listy rol w budzecie do bazy danych
			roleWBudzecieFactory.ZapiszRoleWBudzecieDoDB(listaAktualna);
		} catch (RemoteException e) {
			console(Console.INFO, "-----Blad zwiazany z pobraniem danych z WS, aktualizacja nieudana-----");
			log.error("", e);
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
