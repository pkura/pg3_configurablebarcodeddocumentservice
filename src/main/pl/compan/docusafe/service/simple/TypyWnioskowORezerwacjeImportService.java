package pl.compan.docusafe.service.simple;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.general.canonical.factory.TypyWnioskowORezerwacjeFactory;
import pl.compan.docusafe.general.canonical.model.TypyWnioskowORezerwacje;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.simple.TypyUrlopowImportService.Import;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class TypyWnioskowORezerwacjeImportService extends ServiceDriver
{
	public static final Logger log = LoggerFactory.getLogger(TypyWnioskowORezerwacjeImportService.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 12;
	private TypyWnioskowORezerwacjeFactory typyWnioskowORezerwacjeFactory;

	private TypyWnioskowORezerwacjeFactory getTypyWnioskowORezerwacjeFactory()
	{
		if(typyWnioskowORezerwacjeFactory == null)
		{
			typyWnioskowORezerwacjeFactory = new TypyWnioskowORezerwacjeFactory();
		}
		return typyWnioskowORezerwacjeFactory;
	}

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, TypyWnioskowORezerwacjeImportService.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (TypyWnioskowORezerwacjeImportService.this)
			{
				if (object != null){
					period = (Integer) object;
					stop();
				}
				else
					period = 2;
			}
		}
	}

	public TypyWnioskowORezerwacjeImportService()
	{
		properties = new Property[] { new ImportTimesProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		timer.schedule(new Import(),  DateUtils.MINUTE, period*DateUtils.HOUR);
		log.info("System TypyWnioskowORezerwacjeImport Częstotliwość: "+period+" h.");
		console(Console.INFO, "System TypyWnioskowORezerwacjeImport Częstotliwość: "+period+" h.");
	}

	@Override
	protected void stop() throws ServiceException
	{
		if(timer != null){
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {
			console(Console.INFO, "-----Rozpoczęto synchronizację-----");
			try {
				boolean ctx=DSApi.openContextIfNeeded();
				DSApi.context().begin();
				importTypyWnioskowORezerwacje();
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx);
				console(Console.INFO, "-----Zakończono synchronizację-----");
			} catch (EdmException e) {
				log.info(e.getMessage(), e);
			}
		}
	}

	public void importTypyWnioskowORezerwacje(){		
		// Pobranie typow wnioskow o rezerwacje z [WS]
		List<TypyWnioskowORezerwacje> listaNowych = getTypyWnioskowORezerwacjeFactory().pobierzTypyWnioskowORezerwacjeZWS();

		// Pobranie typow wnioskow o rezerwacje z [BD]	
		List<TypyWnioskowORezerwacje> listaStarych = getTypyWnioskowORezerwacjeFactory().pobierzTypyWnioskowORezerwacjeZBD();

		// Połączenie starej i nowej listy typow wnioskow o rezerwacje
		TypyWnioskowORezerwacjeFactory typyWnioskowORezerwacjeFactory = new TypyWnioskowORezerwacjeFactory();
		List<TypyWnioskowORezerwacje> listaAktualna = typyWnioskowORezerwacjeFactory.aktualizujTypyWnioskowORezerwacje(listaNowych, listaStarych);

		// Zapisanie aktualnej listy typow wnioskow o rezerwacje do bazy danych
		typyWnioskowORezerwacjeFactory.ZapiszTypyWnioskowORezerwacjeDoDB(listaAktualna);
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}

}
