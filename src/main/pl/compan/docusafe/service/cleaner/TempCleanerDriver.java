package pl.compan.docusafe.service.cleaner;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.upload.UploadDriver;
import pl.compan.docusafe.util.DateUtils;
/* User: Administrator, Date: 2007-01-22 15:08:57 */
/**
 *Us�uga czysci katalog temp ze starych plikow
 * @version $Id$
 */
public class TempCleanerDriver extends ServiceDriver implements Service
{
    private Timer timer;
    protected void start() throws ServiceException
    {
        timer = new Timer(true);
        timer.schedule(new CleanFiles(), 10000, 6 * DateUtils.HOUR/*30*DateUtils.SECOND*/);
      //  timer.schedule(new CleanFiles(), (long) 2*1000 ,(long) 1*DateUtils.MINUTE);
    }

    protected void stop() throws ServiceException
    {
        if (timer != null) timer.cancel();
    }

    protected boolean canStop()
    {
        return false;
    }

    class CleanFiles extends TimerTask
    {
        public void run()
        {
        	console(Console.INFO, "Wystartowal serwis czyszcz�cy");
        	// nie ma sensu tworzyc nowego watku dla tej uslugi
        	Calendar cal = Calendar.getInstance();        	
        	long cur = new Date().getTime();
            if(cal.get(Calendar.HOUR_OF_DAY) < 20 && cal.get(Calendar.HOUR_OF_DAY) > 7) return; 
            try
            {
            	File tempDir = new File(System.getProperty("java.io.tmpdir"));
            	File[] childs = tempDir.listFiles();
            	
            	
            	long fileTime;
            	Integer id = 0;
            	for(File temp : childs)
            	{
            		console(Console.INFO, "Usuwa "+temp.getAbsolutePath());
            		if(!temp.equals(UploadDriver.tempDir) && 
            				(temp.getName().startsWith(Docusafe.getUploadPath())||!temp.isDirectory()))
            		{
            			
	            		fileTime = temp.lastModified();
	            		if(cur - fileTime > DateUtils.DAY)
	            		{
	            			if(temp.isDirectory())
	            			{
	            				for(File t:temp.listFiles())
	            				{
	            					t.delete();
	            				}
	            			}
	            			temp.delete();
	            		}
            		}
            	}
            }
            catch (Exception e)
            {
               log.error(e.getMessage(),e);
               console(Console.ERROR,e.getMessage());
            }
            try
            {
            	console(Console.INFO, "Czyszcze tempPng ");
            	for(File file:new File(Docusafe.getPngTempFile()).listFiles())
            	{
            		if(!file.isDirectory())
            		{
            			if((cur - file.lastModified()) > 3 * DateUtils.DAY)
            			{
            				console(Console.INFO, "Usuwa "+file.getAbsolutePath());
            				file.delete();
            			}
            		}
            		else
            		{
            			for(File ogr :file.listFiles())
            			{
            				if(!ogr.isDirectory())
                    		{
                    			if((cur - ogr.lastModified()) > 3 * DateUtils.DAY)
                    			{
                    				console(Console.INFO, "Usuwa "+ogr.getAbsolutePath());
                    				ogr.delete();
                    			}
                    		}
            			}
            		}
            	}
            	console(Console.INFO, " Zako�czyl czyszczenie tempPng ");
            }
            catch (Exception e)
            {
               log.error(e.getMessage(),e); 
               console(Console.ERROR,e.getMessage());
            }
            //czyszczenie folder�w zdefiniowanych w adds:
            //TODO: Po��czy� to w jedno �eby nie by�o trzy razy kopiowania kodu ale jaki� for po li�cie
            try
            {
            	console(Console.INFO, "Czyszcze folder�w z adds ");
            	if(Docusafe.getAdditionProperty("tempCleanerDriver.files") == null || Docusafe.getAdditionProperty("tempCleanerDriver.files").length() < 4 )
            		return;
            	console(Console.INFO, "Czyszcze folder�w "+Docusafe.getAdditionProperty("tempCleanerDriver.files"));
            	String filesPath[] = Docusafe.getAdditionProperty("tempCleanerDriver.files").split(";");
            	Integer days = 30;
            	if(Docusafe.getAdditionProperty("tempCleanerDriver.days") != null && Docusafe.getAdditionProperty("tempCleanerDriver.days").length() > 0 )
            		days = Integer.parseInt(Docusafe.getAdditionProperty("tempCleanerDriver.days"));
            	for (String filePath : filesPath) 
            	{
            		console(Console.INFO, "Czyszcze folderu z adds "+filePath + " Starszych ni� dni ="+days);
            		for(File file:new File(filePath).listFiles())
                	{
                		if(!file.isDirectory())
                		{
                			if((cur - file.lastModified()) > days * DateUtils.DAY)
                			{
                				console(Console.INFO, "Usuwa "+file.getAbsolutePath());
                				file.delete();
                			}
                		}
                		else
                		{
                			for(File ogr :file.listFiles())
                			{
                				if(!ogr.isDirectory())
                        		{
                        			if((cur - ogr.lastModified()) > days * DateUtils.DAY)
                        			{
                        				console(Console.INFO, "Usuwa "+ogr.getAbsolutePath());
                        				ogr.delete();
                        			}
                        		}
                			}
                		}
                	}
                	console(Console.INFO, " Zako�czyl czyszczenie "+filePath);
				}
            }
            catch (Exception e)
            {
               log.error(e.getMessage(),e); 
               console(Console.ERROR,e.getMessage());
            }
        }
    }
}
