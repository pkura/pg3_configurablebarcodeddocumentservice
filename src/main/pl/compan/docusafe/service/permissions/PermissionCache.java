package pl.compan.docusafe.service.permissions;

import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceException;

/* User: Administrator, Date: 2005-04-27 17:04:46 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PermissionCache.java,v 1.4 2006/02/20 15:42:28 lk Exp $
 */
public interface PermissionCache extends Service
{
    final String NAME = "permissions";

    /**
     * Zwraca true, je�eli u�ytkownik ma dane uprawnienie.
     * Metoda nie odwo�uje si� do kontekstu, korzysta wy��cznie
     * z cache.
     */
    boolean hasPermission(String username, String permission);
    
    /**
     * Zwraca true, je�eli u�ytkownik ma dane uprawnienie.
     * Metoda odwo�uje si� do kontekstu. Key to klucz profil<->user
     */
    boolean hasPermissionWithContext(String username, String permission, String key);
    
    /**
     * Aktualizuje list� uprawnie� u�ytkownika, je�eli istnieje taka potrzeba,
     * tzn. nast�pi�a inwalidacja u�ytkownika.
     * Metoda musi by� wywo�ywana w otwartym kontek�cie.
     */
    void update(String username) throws ServiceException;

    /**
     * Sprawdza, czy wywo�anie update(String) jest konieczne.
     * Nie wymaga otwartej sesji Hibernate.
     */
    boolean updateNeeded(String username) throws ServiceException;

    /**
     * Oznacza zapami�tane role u�ytkownika jako niewa�ne.
     */
    void invalidate(DSUser user);
    /**
     * Oznacza zapami�tane role dzia�u jako niewa�ne.
     */
    void invalidate(DSDivision division);
    /**
     * Odczytuje ponownie uprawnienia roli.
     */
    void reload(Role role);
    /**
     * Usuwa zapami�tane informacje o tej roli.
     */ 
    void remove(Role role);
}
