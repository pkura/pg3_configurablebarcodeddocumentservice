package pl.compan.docusafe.service.permissions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import std.fun;
import std.lambda;

import java.util.*;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import org.apache.commons.logging.LogFactory;
//import org.bouncycastle.asn1.x509.DSAParameter;

import com.opensymphony.webwork.ServletActionContext;

/* User: Administrator, Date: 2005-04-28 09:47:15 */

/**
 * Przetrzymuje w cache informacje o uprawnieniach u�ytkownik�w
 * i grup.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PermissionsDriver.java,v 1.19 2009/10/29 12:05:50 mariuszk Exp $
 */
public class PermissionsDriver extends ServiceDriver implements PermissionCache
{
	static final Logger log = LoggerFactory.getLogger(PermissionsDriver.class);
    /**
     * Long (role id) -> Set(DSPermission)
     */
    private Map<Long, Set<String>> role2permissions = new HashMap<Long, Set<String>>();

    /**
     * String (username) -> Set(Long (role id))
     */
    private Map<String, Set<Long>> user2roles = new HashMap<String, Set<Long>>();
    /**
     * String (guid) -> Set(Long (role id))
     */
    private Map<String, Set<Long>> division2roles = new HashMap<String, Set<Long>>();
    /**
     * String (username) -> Set(String (guid))
     */
    private Map<String, Set<String>> user2division = new HashMap<String, Set<String>>();

    
    
    private Map<String, Set<String>> user2substitutions = new HashMap<String, Set<String>>();

    public boolean hasPermission(String username, String permission)
    {
    	boolean a1 = hasPermissionInternal(username, permission, false, Profile.emptyKey) ;
    	boolean a2 = hasPermissionInternal(username, DSPermission.WSZYSTKIE_UPRAWNIENIA.getName(), false, Profile.emptyKey) ;
    	boolean a3 = DSPermission.getAvailablePermissionNameSet().contains(permission);
        return a1||( a2 && a3 );
    }

    /**
     * String (username) -> Map (String (permission) -> [HAS, HAS_NOT])
     */
//    private Map user2permission = new HashMap();

    /**
     * czy u�ytkownik z danym kluczem ma dost�p do danego uprawnienia
     * klucze "", "#" i null s� traktowane jako brak klucza
     */
    public boolean hasPermissionWithContext(String username, String permission, String key){  	
    	
    	boolean a1 = hasPermissionInternal(username, permission, true, key) ;
    	boolean a2 = hasPermissionInternal(username, DSPermission.WSZYSTKIE_UPRAWNIENIA.getName(), true, key) ;
    	boolean a3 = DSPermission.getAvailablePermissionNameSet().contains(permission);
        return a1||( a2 && a3 );
    }

    
    private Set<Long> getUserRoles(String username, String userKey){
    	Set<Long> rolesIds = new HashSet<Long>();
    	if(Docusafe.getAdditionProperty("userProfileKeys") != null && Docusafe.getAdditionProperty("userProfileKeys").equals("true")){
    		DSUser user = null;
    		String[] userRoles = null;
    		boolean hasBeenOpened = false;
			try {
				if (!DSApi.isContextOpen()) {
					DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
				} else {
					hasBeenOpened = true;
				}
		    	user = DSUser.findByUsername(username);
		    	userRoles = user.getOfficeRoles();
		    	
    		}catch(EdmException e){
				log.error(e.getMessage());
			}finally {
				try {
					if(!hasBeenOpened){//je�li nie by� otwarty to zamknij
						DSApi.close();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
	    	
	    	Map<String,String> profile2Keys = user.getProfilesWithKeys();
	    	
	    	if(AvailabilityManager.isAvailable("PermissionsDriver.withoutUserSources")){
		    	if(!profile2Keys.containsValue(userKey)){
		    		return rolesIds;
		    	} 
		    	
	    		try{
	    			for(String profileName: user.getProfileNames()){
						if(!profile2Keys.get(profileName).equals(userKey))
							continue;
						
	        			Profile profile = Profile.findByProfilename(profileName);
	        			for(Role role: profile.getOfficeRoles()){
	        				rolesIds.add(role.getId());
	        			}
	        		}
				} catch (EdmException e) {
					log.error(e.getMessage(), e);
				}
	    		
	    		return rolesIds;
	    	}
    		
	    	for(String r : userRoles){
	    		Role role = null;
	    		try{
	    			role = Role.findByName(r);
	    		}catch(EdmException e){
	    			log.error(e.getMessage());
	    		}
	    		
	    		if(Arrays.asList(role.getUserSources(username)).contains("own")){
	    			rolesIds.add(role.getId());
	    		}else{
	    			for(String profile : role.getUserSources(username)){	    				
	    				if(profile2Keys.containsKey(profile) && (profile2Keys.get(profile).equals("") || profile2Keys.get(profile).equals(Profile.emptyKey) || profile2Keys.get(profile).equals(userKey))){
	    					rolesIds.add(role.getId());
	    					break;
	    				}
	    			}
	    		}
	    	}
	    	
    	}
    	return rolesIds;
    }
    
    
    private boolean hasPermissionInternal(String username, String permission, boolean withKey, String userKey, boolean ... shouldProcess)
    {
        if (username == null)
            throw new NullPointerException("username");
        if (permission == null)
            throw new NullPointerException("permission");

		if (userKey == null || userKey.equals("")) {
			userKey = Profile.emptyKey;
        }
        // znale�� role u�ytkownika -> Set(Long)
        // znale�� role grup, w kt�rych jest u�ytkownik -> Set(Long)
        // a. znale�� uprawnienie w role2permissions Long -> Set(String)
        // b. znale�� rol� w permission2roles   String -> Set(Long)

        Boolean has = null;

        Set<Long> roleIds = null;
        synchronized (this)
        {
        	if(withKey){
        		roleIds = getUserRoles(username, userKey);
        	}else{
        		roleIds = user2roles.get(username);
        	}
        }
        //Modul Dokumenty, w adds.properties trzeba dodac wpis archiveOn
        //Dla biznesu nie ma znaczenia czy uzytkownik ma uprawnienie 'dostep do
        //modulu dokumenty' czy nie to i tak bedzie mogl tam wejsc
        //permission.equals("ARCHIWUM_DOSTEP") || 
        
        if (roleHasPermission(roleIds, permission))
        {

            has = Boolean.TRUE;
        }

        if (has == null)
        {
            Set guids = null;
            synchronized (this)
            {
                guids = (Set) user2division.get(username);
            }
            if (guids != null && guids.size() > 0)
            {
                for (Iterator iter=guids.iterator(); iter.hasNext(); )
                {
                    String guid = (String) iter.next();
                    synchronized (this)
                    {
                        roleIds = division2roles.get(guid);
                    }
                    if (roleHasPermission(roleIds, permission))
                    {
                        has = Boolean.TRUE;
                        break;
                    }
                }
            }
        }
        
        if(has==null && shouldProcess.length==0)
        {
			DSUser user;
			boolean hasBeenOpened = false;
			try {
				if (!DSApi.isContextOpen()) {
					DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
				} else {
					hasBeenOpened = true;
				}
//				if (!DSApi.context().isTransactionOpen()) {
//					DSApi.context().begin();
//				}
				user = DSUser.findByUsername(username);
				DSUser[] usersSubs = user.getSubstituted();

				for (int i = 0; i < usersSubs.length; i++) {
					if (hasPermissionInternal(usersSubs[i].getName(), permission, withKey, userKey, false)) {
						has = Boolean.TRUE;
						break;
					}
				}
			} catch (UserNotFoundException e) {
				log.debug(e.getMessage(), e);
			} catch (EdmException e) {
				DSApi.context().setRollbackOnly();
				log.warn(e.getMessage(), e);
			} //Tu dodalem zamykanie DSApi
			finally {
				try {
					if(!hasBeenOpened){//je�li nie by� otwarty to zamknij
						DSApi.close();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
        }

		if (log.isTraceEnabled()) {
			log.trace("hasPermission({},{}) = {}", username, permission, has == Boolean.TRUE);
		}

        return has == Boolean.TRUE;
    }

    private boolean roleHasPermission(Set<Long> roleIds, String permission)
    {
        if (roleIds == null || roleIds.size() == 0 ||
            role2permissions == null || role2permissions.size() == 0) return false;

        for (Long roleId : roleIds)
        {
            Set<String> perms = role2permissions.get(roleId);
            if (perms != null && perms.contains(permission))
                return true;
        }
        return false;
    }

    /**
     * Ta metoda �ci�le wsp�pracuje z update(String).  Powinna
     * zwraca� true tylko wtedy, gdy update powinno by� wywo�ane.
     * <p>
     * Metoda nie wymaga otwartej sesji Hibernate.
     * @param username
     */
    public synchronized boolean updateNeeded(String username)
        throws ServiceException
    {
        return
            (user2roles.get(username) == null) ||
            (user2division.get(username) == null);
    }

    public synchronized void update(String username) throws ServiceException
    {
        /*
        Wszystkie uprawnienia

select rp.role_id, rp.permission_name
from dso_role_permissions rp left outer join dso_role_usernames ru on rp.role_id = ru.role_id
left outer join dso_role_divisions rd on rp.role_id = rd.role_id
where ru.username in ('lk')
or rd.division_guid in ('BFA7FFB80071D29A1035AF23C591B28391D');
        */
        try
        {
            // odczytywanie r�l, w kt�rych znajduje si� u�ytkownik
            if (user2roles.get(username) == null)
            {
                Role[] roles = Role.list();
                
                Set<Long> roleIds = new HashSet<Long>();
                for (int i=0; i < roles.length; i++)
                {
                	
                    if (roles[i].getUsernames() != null &&
                        roles[i].getUsernames().contains(username))
                    {
                        roleIds.add(roles[i].getId());
                    }
                }
                user2roles.put(username, roleIds);
            }

            // odczytywanie dzia��w, w kt�rych jest u�ytkownik
            if (user2division.get(username) == null)
            {
                DSUser user = DSUser.findByUsername(username);
                
                DSUser[] substitutedUsers = user.getSubstituted();
                Set<String> set = new HashSet<String>();
                if(substitutedUsers != null)
	                for(DSUser us : substitutedUsers)
	                    set.add(us.getName());

                user2substitutions.put(user.getName(), set);
                DSDivision[] divisions = user.getDivisions();
                Collection<String> guids = fun.map(divisions, new lambda<DSDivision, String>()
                {
                    public String act(DSDivision o)
                    {
                        return o.getGuid();
                    }
                });
                user2division.put(username, new HashSet<String>(guids));
            }
        }
        catch (EdmException e)
        {
            throw new ServiceException(e.getMessage(), e);
        }
    }
    
    private void update(String username , boolean shouldProcess)
    {
        
    }

    public synchronized void invalidate(DSUser user)
    {
        if (user == null)
            throw new NullPointerException("user");
        user2roles.remove(user.getName());
        user2division.remove(user.getName());
    }

    public synchronized void invalidate(DSDivision division)
    {
        if (division == null)
            throw new NullPointerException("division");
        division2roles.remove(division.getGuid());
    }

    public synchronized void reload(Role role)
    {
        if (role == null)
            throw new NullPointerException("role");
        role2permissions.put(role.getId(), new HashSet<String>(role.getPermissions()));
    }

    public synchronized void remove(Role role)
    {
        if (role == null)
            throw new NullPointerException("role");
        role2permissions.remove(role.getId());
    }

    /* ServiceDriver */

    protected void start() throws ServiceException
    {
        try
        {
            Role[] roles = Role.list();
            for (int i=0; i < roles.length; i++)
            {
                reload(roles[i]);
            }
        }
        catch (EdmException e)
        {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    protected void stop() throws ServiceException
    {
    }

    protected boolean canStop()
    {
        return false;
    }
}
