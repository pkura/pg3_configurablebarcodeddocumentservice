package pl.compan.docusafe.service.reports;

import org.apache.commons.lang.StringUtils;
import org.hibernate.CallbackException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeProjection;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import javax.validation.UnexpectedTypeException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Raport proces�w
 *
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
@SuppressWarnings("unused")
public class ProcessReportBean implements Serializable, Lifecycle {

    private static Logger log = LoggerFactory.getLogger(ProcessReport.class);
    StringManager sm = GlobalPreferences.loadPropertiesFile(ProcessReportBean.class.getPackage().getName(), null);
    //
    private ProcessReportBeanId compositeId;
    private Long documentId;
    private String processName;
    private String statusCn;
    private Date start;
    private Date end;
    private Long duration;
    private Long divisionId;
    private String division;
    private Long userId;
    private String user;

    public ProcessReportBean() {
    }

    public static SearchResults<ProcessReportBean> search(Query query) {

        try {
            // proste zapytanie, wyci�gni�cie wbranych kolumn z warunkiem where
            NativeCriteria nc = DSApi.context().createNativeCriteria("PROCESS_REPORT_VIEW", "procs");

            //kolumny
            NativeProjection columnProjection = NativeExps.projection();
            for (HibernateColumn col : HibernateColumn.values())
                if (StringUtils.isNotEmpty(col.columnDbName))
                    columnProjection.addProjection(col.columnDbName);
            nc.setProjection(columnProjection);


            if (query.getDocumentId() != null)
                //where
                nc.add(NativeExps.eq(HibernateColumn.DOCUMENT_ID.columnDbName, query.getDocumentId()));
            else {
                DocumentKind documentKind = query.getDocumentKindId() != null ? DocumentKind.find(query.getDocumentKindId()) : null;

                //join
                if (documentKind != null) {
                    String documentKindTable = documentKind.getTablename();
                    nc.addJoin(NativeExps.innerJoin(documentKindTable, "dockind", "dockind.DOCUMENT_ID", "procs." + HibernateColumn.DOCUMENT_ID.columnDbName));
                }

                //where
                if (documentKind != null) {
                    List<String> processesNames = documentKind.getDockindInfo().getProcessesDeclarations().getNames();
                    processesNames.addAll(getElementsWithDashToUnderscoreChar(processesNames));//todo to mi si� nie podoba
                    nc.add(NativeExps.in(HibernateColumn.PROCESS_NAME.columnDbName, processesNames));
                }

                if (query.getIntervalFrom() != null)
                    nc.add(NativeExps.gte(HibernateColumn.START.columnDbName, query.getIntervalFrom()));
                if (query.getIntervalTo() != null) {
                    if (query.getOnlyNotCompleted() == null || !query.getOnlyNotCompleted()) {
//                        int comparedDate = compareDate(query.getIntervalTo(), new Date());
//                        if (comparedDate < 0)//data przed - dokladnosc do dni
                        {
                            Date nextDaydateTo = query.getIntervalTo();
                            nextDaydateTo.setTime(nextDaydateTo.getTime() + DateUtils.DAY);

                            nc.add(NativeExps.lt(HibernateColumn.END.columnDbName, nextDaydateTo));
                            nc.add(NativeExps.isNotNull(HibernateColumn.END.columnDbName));
                        }
                    } else {
                        nc.add(NativeExps.lte(HibernateColumn.START.columnDbName, query.getIntervalTo()));
                    }
                } else if (query.getOnlyNotCompleted() != null && query.getOnlyNotCompleted()) {
                    nc.add(NativeExps.isNull(HibernateColumn.END.columnDbName));
                }

                if (query.getDurationMillis() != null)
                    nc.add(NativeExps.lte(HibernateColumn.DURATION_MILLIS.columnDbName, query.getDurationMillis()));

                if (query.getStatusCn() != null && query.getStatusCn().length > 0)
                    nc.add(NativeExps.in(HibernateColumn.STATUS_CN.columnDbName, query.getStatusCn()));
                if (query.getDivisionIds() != null)
                    nc.add(NativeExps.in(HibernateColumn.DIVISION_ID.columnDbName, query.getDivisionIds()));
                if (query.getUserId() != null)
                    nc.add(NativeExps.eq(HibernateColumn.USER_ID.columnDbName, query.getUserId()));

                //todo opoznienie
                //if (query.getDelayMillis()!=null)
                //    nc.add(NativeExps.eq(Field.DELAY_MILLIS.columnHibName,query.getDelayMillis()));
            }


            // pobranie
            List<ProcessReportBean> results = new ArrayList<ProcessReportBean>();
            CriteriaResult cr = nc.criteriaResult();
            while (cr.next()) {
                ProcessReportBean bean = new ProcessReportBean();

                for (HibernateColumn col : HibernateColumn.values())
                    bean.setValue(col, cr);

                results.add(bean);
            }


//            Criteria criteria = DSApi.context().session().createCriteria(ProcessReportBean.class,"procs");
//
//            if (query.getDocumentId() != null)
//                criteria.add(Restrictions.eq(HibernateColumn.DOCUMENT_ID.columnHibName, query.getDocumentId()));
//            else {
//
//                if (query.getDocumentKindId()!=null){
//                    DocumentKind documentKind = DocumentKind.find(query.getDocumentKindId());
//
//                    List<String> processesNames = getNames(documentKind);
//
//                    criteria.add(Restrictions.in(HibernateColumn.PROCESS_NAME.columnHibName, processesNames));
//
//                    String documentKindTable = documentKind.getTablename();
//                    //join like: SELECT * FROM PROCESS_REPORT_VIEW V JOIN DSG_AMS_KARTA_OBIEGOWA K ON V.DOCUMENT_ID_ = k.DOCUMENT_ID
//                }
//
//                if (query.getIntervalFrom() != null)
//                    criteria.add(Restrictions.ge(HibernateColumn.START.columnHibName, query.getIntervalFrom()));
//                if (query.getOnlyNotCompleted() == null || !query.getOnlyNotCompleted()) {
//                    if (query.getIntervalTo() != null) {
////                        int comparedDate = compareDate(query.getIntervalTo(), new Date());
////                        if (comparedDate < 0)//data przed - dokladnosc do dni
//                        {
//                            Date nextDaydateTo = query.getIntervalTo();
//                            nextDaydateTo.setTime(nextDaydateTo.getTime() + DateUtils.DAY);
//
//                            criteria.add(Restrictions.lt(HibernateColumn.END.columnHibName, nextDaydateTo));
//                            criteria.add(Restrictions.isNotNull(HibernateColumn.END.columnHibName));
//                        }
//                    }
//                } else if (query.getOnlyNotCompleted() != null && query.getOnlyNotCompleted()) {
//                    criteria.add(Restrictions.isNull(HibernateColumn.END.columnHibName));
//                }
//
//                if (query.getDurationMillis() != null)
//                    criteria.add(Restrictions.le(HibernateColumn.DURATION_MILLIS.columnHibName, query.getDurationMillis()));
//
//                if (query.getStatusCn() != null && query.getStatusCn().length > 0)
//                    criteria.add(Restrictions.in(HibernateColumn.STATUS_CN.columnHibName, query.getStatusCn()));
//                if (query.getDivisionId() != null)
//                    criteria.add(Restrictions.eq(HibernateColumn.DIVISION_ID.columnHibName, query.getDivisionId()));
//                if (query.getUserId() != null)
//                    criteria.add(Restrictions.eq(HibernateColumn.USER_ID.columnHibName, query.getUserId()));
//
//                //todo opoznienie
//                //if (query.getDelayMillis()!=null)
//                //    criteria.add(Restrictions.eq(Field.DELAY_MILLIS.columnHibName,query.getDelayMillis()));
//            }
//
//            //criteria.addOrder(query.isAscending() ? Order.asc(query.getSortField()) : Order.desc(query.getSortField()));
//
//            List<ProcessReportBean> results = criteria.list();
//
            int totalCount = results.size();

            //            if (query.limit > 0)
            //            {
            //                criteria.setFirstResult(query.offset);
            //                criteria.setMaxResults(query.limit);
            //            }
            //
            //            return new SearchResultsAdapter<ProcessReportBean>(results, query.offset, totalCount, ProjectEntry.class);

            return new SearchResultsAdapter<ProcessReportBean>(results, 0, totalCount, ProcessReportBean.class);
        } catch (EdmException e) {
            log.debug(e.getMessage(), e);
        }
        return null;
    }

    public static int compareDate(Date dateFirst, Date dateSecond) {
        Calendar calendarFirst = Calendar.getInstance();
        calendarFirst.setTime(dateFirst);
        Calendar calendarSecond = Calendar.getInstance();
        calendarSecond.setTime(dateSecond);

        int d = 0;
        if ((d = compare(calendarFirst, calendarSecond, Calendar.YEAR)) != 0)
            return d;
        if ((d = compare(calendarFirst, calendarSecond, Calendar.MONTH)) != 0)
            return d;
        if ((d = compare(calendarFirst, calendarSecond, Calendar.DAY_OF_MONTH)) != 0)
            return d;
        return 0;
    }

    private static int compare(Calendar cFirst, Calendar cSecond, int field) {
        return compare(cFirst.get(field), cSecond.get(field));
    }

    private static int compare(int iFirst, int iSecond) {
        int d = iSecond - iFirst;
        if (d == 0) return 0;
        return d / Math.abs(d);
    }

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger log) {
        ProcessReportBean.log = log;
    }

    public static List<String> getElementsWithDashToUnderscoreChar(List<String> list){
        List<String> sub = new ArrayList<String>();
        for(String el : list)
            if (el.contains("-"))
                sub.add(el.replaceAll("-","_"));
        return sub;
    }

    public String getValueAsStringSafety(HibernateColumn hibernateColumn) {
        String value = getValueAsString(hibernateColumn);
        return (value != null) ? value : "";
    }

    public String getValueAsString(HibernateColumn hibernateColumn) {
        Object obj = getValue(hibernateColumn);
        return getValueAsString(obj);
    }

    public Object getValue(HibernateColumn hibernateColumn) {
        switch (hibernateColumn) {
            case DOCUMENT_ID:
                return documentId;
            case PROCESS_NAME:
                return processName;
            case STATUS_CN:
                return statusCn;
            case START:
                return start;
            case END:
                return end;
            case DURATION_MILLIS:
                return duration;
            case DELAY_MILLIS:
                return 0L;//todo
            case DIVISION_ID:
                return divisionId;
            case DIVISION:
                return division;
            case USER_ID:
                return userId;
            case USER:
                return user;
            default:
                throw new UnexpectedTypeException();
        }
    }

    public void setValue(HibernateColumn hibernateColumn, CriteriaResult value) {
        if (value == null)
            return;

        String columnDbName = hibernateColumn.columnDbName;
        if (columnDbName == null)
            return;

        switch (hibernateColumn) {
            case DOCUMENT_ID:
                documentId = value.getLong(columnDbName, null);
                break;
            case PROCESS_NAME:
                processName = value.getString(columnDbName, null);
                break;
            case STATUS_CN:
                statusCn = value.getString(columnDbName, null);
                break;
            case START:
                start = value.getDate(columnDbName, null);
                break;
            case END:
                end = value.getDate(columnDbName, null);
                break;
            case DURATION_MILLIS:
                duration = value.getLong(columnDbName, null);
                break;
            case DELAY_MILLIS:
                // = value.getLong(columnHibName,null);//todo
                break;
            case DIVISION_ID:
                divisionId = value.getLong(columnDbName, null);
                break;
            case DIVISION:
                division = value.getString(columnDbName, null);
                break;
            case USER_ID:
                userId = value.getLong(columnDbName, null);
                break;
            case USER:
                user = value.getString(columnDbName, null);
                break;
            default:
                throw new UnexpectedTypeException();
        }
    }

    public static String getValueAsString(Object obj) {
        if (obj == null)
            return null;
        if (obj instanceof String)
            return (String) obj;
        if (obj instanceof Long)
            return Long.toString((Long) obj);
        if (obj instanceof Date)
            return obj.toString();
        throw new UnexpectedTypeException();
    }

    public boolean isNotNull(HibernateColumn hibernateColumn) {
        return !isNull(hibernateColumn);
    }

    public boolean isNull(HibernateColumn hibernateColumn) {
        return getValue(hibernateColumn) == null;
    }

    public ProcessReportBeanId getCompositeId() {
        return compositeId;
    }

    public void setCompositeId(ProcessReportBeanId compositeId) {
        this.compositeId = compositeId;
    }

    public StringManager getSm() {
        return sm;
    }

    public void setSm(StringManager sm) {
        this.sm = sm;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getStatusCn() {
        return statusCn;
    }

    public void setStatusCn(String statusCn) {
        this.statusCn = statusCn;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(Long divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public boolean onSave(Session session) throws CallbackException {
        return false; // only view
    }

    @Override
    public boolean onUpdate(Session session) throws CallbackException {
        return false; // only view
    }

    @Override
    public boolean onDelete(Session session) throws CallbackException {
        return false; // only view
    }

    @Override
    public void onLoad(Session session, Serializable serializable) {
        //To change body of implemented methods use File | Settings | File Templates.
        // only view
    }

    @Override
    public int hashCode() {
        int result = sm != null ? sm.hashCode() : 0;
        result = 31 * result + (compositeId != null ? compositeId.hashCode() : 0);
        result = 31 * result + (documentId != null ? documentId.hashCode() : 0);
        result = 31 * result + (processName != null ? processName.hashCode() : 0);
        result = 31 * result + (statusCn != null ? statusCn.hashCode() : 0);
        result = 31 * result + (start != null ? start.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (divisionId != null ? divisionId.hashCode() : 0);
        result = 31 * result + (division != null ? division.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProcessReportBean that = (ProcessReportBean) o;

        if (compositeId != null ? !compositeId.equals(that.compositeId) : that.compositeId != null) return false;
        if (division != null ? !division.equals(that.division) : that.division != null) return false;
        if (divisionId != null ? !divisionId.equals(that.divisionId) : that.divisionId != null) return false;
        if (documentId != null ? !documentId.equals(that.documentId) : that.documentId != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (end != null ? !end.equals(that.end) : that.end != null) return false;
        if (processName != null ? !processName.equals(that.processName) : that.processName != null) return false;
        if (sm != null ? !sm.equals(that.sm) : that.sm != null) return false;
        if (start != null ? !start.equals(that.start) : that.start != null) return false;
        if (statusCn != null ? !statusCn.equals(that.statusCn) : that.statusCn != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;

        return true;
    }

    @Override
    public String toString() {
        return StringUtils.join(new Object[]{compositeId, documentId, processName, statusCn, start, end, duration, divisionId, division, userId, user}, ' ');
    }

    public enum HibernateColumn {
        DOCUMENT_ID("documentId", "DOCUMENT_ID_"),
        PROCESS_NAME("processName", "PROCESS_NAME_"),
        STATUS_CN("statusCn", "STATUS_CN_"),
        START("start", "START_"),
        END("end", "END_"),
        DURATION_MILLIS("duration", "DURATION_"),
        DELAY_MILLIS("delay", null),//todo - brak w widoku
        DIVISION_ID("divisionId", "DIVISION_ID_"),
        DIVISION("division", "DIVISION_"),
        USER_ID("userId", "USER_ID_"),
        USER("user", "USER_");
        //
        public final String columnHibName;
        public final String columnDbName;

        private HibernateColumn(String columnHibName, String columnDbName) {
            this.columnHibName = columnHibName;
            this.columnDbName = columnDbName;
        }
    }

    public static class Query {

        private Long documentId = null;
        private Long durationMillis = null;
        private Long delayMillis = null;
        private Long documentKindId = null;
        private Long[] divisionIds = null;
        private Long userId = null;
        private Date intervalFrom = null;
        private Date intervalTo = null;
        private String[] statusCn = null;
        private Boolean onlyNotCompleted = null;

        public Query() {
        }

        public Boolean getOnlyNotCompleted() {
            return onlyNotCompleted;
        }

        public void setOnlyNotCompleted(Boolean onlyNotCompleted) {

            this.onlyNotCompleted = onlyNotCompleted;
        }

        public Long getDocumentId() {
            return documentId;
        }

        public void setDocumentId(Long documentId) {
            this.documentId = documentId;
        }

        public Long getDurationMillis() {
            return durationMillis;
        }

        public void setDurationMillis(Long duration) {
            this.durationMillis = duration;
        }

        public Long getDelayMillis() {
            return delayMillis;
        }

        public void setDelayMillis(Long delay) {
            this.delayMillis = delay;
        }

        public Long getDocumentKindId() {
            return documentKindId;
        }

        public void setDocumentKindId(Long documentKindId) {
            this.documentKindId = documentKindId;
        }

        public Long[] getDivisionIds() {
            return divisionIds;
        }

        public void setDivisionIds(Long[] divisionIds) {
            this.divisionIds = divisionIds;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Date getIntervalFrom() {
            return intervalFrom;
        }

        public void setIntervalFrom(Date intervalFrom) {
            this.intervalFrom = intervalFrom;
        }

        public Date getIntervalTo() {
            return intervalTo;
        }

        public void setIntervalTo(Date intervalTo) {
            this.intervalTo = intervalTo;
        }

        public String[] getStatusCn() {
            return statusCn;
        }

        public void setStatusCn(String[] statusCn) {
            this.statusCn = statusCn;
        }
    }
}
