package pl.compan.docusafe.service.reports;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.dbutils.DbUtils;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportEnvironment;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;

public class NewReportService extends ServiceDriver implements Service
{
	private Timer timer;

	Logger log = LoggerFactory.getLogger(NewReportService.class.getName());
	protected boolean canStop() {
		return true;
	}

	protected void start() throws ServiceException 
	{
		timer = new Timer(true);
        timer.schedule(new ReportProcessor(), 1000, 1*DateUtils.MINUTE);
	}

	protected void stop() throws ServiceException 
	{
		if (timer != null) timer.cancel();
		 
	}
	
	public class ReportProcessor extends TimerTask
	{
		protected ReportEnvironment reportEnvironment = new ReportEnvironment();
		public void run() 
		{
			log.trace("Rusza nowy serwis raportowy");
			console(Console.INFO, "Rusza nowy serwis raportowy");
			//System.out.println("Dziala nowy serwis raportowy");
			PreparedStatement ps = null;
			ResultSet rs = null;
			try
			{
				DSApi.openAdmin();
				//statystyki online dla prosiki tymczasowo
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				String reportCn;
				String xmlCriteria;
				Report r;
			
				log.trace("Wyciagam raporty z bazy");
				console(Console.INFO, "Wyciagam raporty z bazy");
	        	ps = DSApi.context().prepareStatement("select * from ds_new_report where status=? and process_date <?");
	        	ps.setInt(1, Report.NEW_REPORT_STATUS);
	        	ps.setTimestamp(2, new Timestamp(new Date().getTime()));
	        	rs = ps.executeQuery();
	        	//DbUtils.closeQuietly(ps);
	        	while(rs.next())
	        	{
	        		console(Console.INFO, "Generuje Raport");
	        		//System.out.println("cos tam iteruje");
	        		PreparedStatement ps1 = null;
	        		Long id = 0L;
	        		try
	        		{		        		
		        		File file = null;
						if(DSApi.isPostgresServer()){
							log.trace("Czytam kryteria do pliku");
							InputStream is = rs.getBinaryStream("criteria");
							file = File.createTempFile("docusafe_report_", ".xml");
							OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
							byte[] buffer = new byte[1024];
							int count;
							while ((count = is.read(buffer)) > 0)
							{
								os.write(buffer, 0, count);
							}
							org.apache.commons.io.IOUtils.closeQuietly(is);
							org.apache.commons.io.IOUtils.closeQuietly(os);
						} else {
							Blob blob = rs.getBlob("criteria");
							if (blob != null)
							{
								log.trace("Czytam kryteria do pliku");
								InputStream is = blob.getBinaryStream();
								file = File.createTempFile("docusafe_report_", ".xml");
								OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
								byte[] buffer = new byte[1024];
								int count;
								while ((count = is.read(buffer)) > 0)
								{
									os.write(buffer, 0, count);
								}
								org.apache.commons.io.IOUtils.closeQuietly(is);
								org.apache.commons.io.IOUtils.closeQuietly(os);
							}
						}

		                id = rs.getLong("id");
		        		reportCn = rs.getString("report_cn");
		        		r = reportEnvironment.getReportInstanceByCn(reportCn);
		        		log.trace("Parsuje kryteria");
		        		SAXReader sax = new SAXReader();		        		
		        		r.init(sax.read(file), reportCn);
		        		r.setId(id);
		        		r.setDestination(reportEnvironment.getResultsDirForReport(r));
		        		r.setUsername(rs.getString("username"));
		        		
		        		DSApi.context().begin();
		        		ps1 = DSApi.context().prepareStatement("update ds_new_report set start_process_time=?, status=? where id=?");
		        		ps1.setTimestamp(1, new Timestamp(new Date().getTime()));
		        		ps1.setInt(2, Report.PROGRESS_REPORT_STATUS);
		        		ps1.setLong(3, r.getId());
		        		ps1.executeUpdate();
		        		DbUtils.closeQuietly(ps1);
		        		DSApi.context().commit();

		        		log.trace("robie raport");
		        		r.generateReport();


		        		DSApi.context().begin();
		        		ps1 = DSApi.context().prepareStatement("update ds_new_report set finish_process_time=?, status=? where id=?");
		        		ps1.setTimestamp(1, new Timestamp(new Date().getTime()));
		        		ps1.setInt(2, Report.DONE_REPORT_STATUS);
		        		ps1.setLong(3, r.getId());
		        		ps1.executeUpdate();
		        		DbUtils.closeQuietly(ps1);
		        		
		        		//obsluga raportow powtarzalnych
		        		if(r.getPeriodical() && r.getInterval()!=0)
		        		{
		        			Calendar c = Calendar.getInstance();
		        			c.setTime(new Date());
		        			c.add(Calendar.HOUR_OF_DAY, r.getInterval());
		        			if(DSApi.isOracleServer())
		                		ps1 = DSApi.context().prepareStatement("insert into ds_new_report (id,report_cn, ctime, username, status, criteria, title, description, process_date, remote_key) values(ds_new_report_id.nextval,?,?,?,?,?,?,?,?,?)");
		                	else
		                		ps1 = DSApi.context().prepareStatement("insert into ds_new_report (report_cn, ctime, username, status, criteria, title, description, process_date, remote_key) values(?,?,?,?,?,?,?,?,?)");
		        			ps1.setString(1, r.getReportCn());
		                	ps1.setTimestamp(2, new Timestamp(new Date().getTime()));
		                	ps1.setString(3, r.getUsername());
		                	ps1.setInt(4, Report.NEW_REPORT_STATUS);
		                	
		                	File temp = File.createTempFile("docusafe_report_", ".tmp");
		    				OutputStream output = new BufferedOutputStream(new FileOutputStream(temp));
		    				PrintStream pr = new PrintStream(output, true, "UTF-8");
		    				pr.print(r.getSearchCriteria().asXML());
		    				pr.close();
		    				output.close();
		    				InputStream is = new FileInputStream(temp);
		    				
		    				ps1.setBinaryStream(5, is, (int) temp.length());
		                	
		                	ps1.setString(6, r.getTitle());
		                	ps1.setString(7, r.getDescription());
		                	ps1.setTimestamp(8, new Timestamp(c.getTime().getTime()));
		                	ps1.setString(9, rs.getString("remote_key"));
		        			ps1.executeUpdate();
		        			DbUtils.closeQuietly(ps1);
		        		}
		        		DSApi.context().commit();
		        		console(Console.INFO, "Wyciagam zakonczyl raport");
		        		log.trace("Raport ukonczony");
	        		}
	        		catch(Exception e)
	        		{
	        			console(Console.ERROR, "B��D :"+e.getMessage());
	        			ps1 = DSApi.context().prepareStatement("update ds_new_report set status = ? where id = ?");
		        		ps1.setInt(1, Report.ERR_REPORT_STATUS);
		        		ps1.setLong(2, id);
		        		ps1.executeUpdate();
		        		DSApi.context().closeStatement(ps1);
	        			DSApi.context().setRollbackOnly();
	        			log.error(e.getMessage(),e);
	        		}
	        		finally
	        		{
	        			DSApi.context().closeStatement(ps1);
	        		}
	        	}
			}
			catch(Exception e)
			{
				log.error(e.getMessage(),e);
			}
			finally
			{
				DSApi.context().closeStatement(ps);
				DSApi._close();
			}
		}
	}
}
