package pl.compan.docusafe.service.reports;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class StatusReportProvider {

    private static final Logger log = LoggerFactory.getLogger(StatusReportProvider.class);

    private Map<Long, Long> cacheDocumentToDockind = new HashMap<Long,Long>();
    private Map<Long, Map<String,String>> cacheDockindToStatuses = new HashMap<Long,Map<String,String>>();

    public StatusReportProvider (){}

    public String getTitle (Long docId, String statusCn){
        try{
            Map<String,String> statuses = getDocumentStatuses(docId);
            return statuses.get(statusCn);
        } catch (Exception e){
            log.error(e.getMessage(),e);
            return null;
        }
    }

    private Map<String,String> getDocumentStatuses(Long docId) throws EdmException {
        Long dockindId = cacheDocumentToDockind.get(docId);
        if (dockindId==null){
            DocumentKind dockind = Document.find(docId).getDocumentKind();
            dockindId = dockind.getId();
            cacheDocumentToDockind.put(docId,dockindId);
            return addStatusesToCache(dockind);
        } else
            return cacheDockindToStatuses.get(dockindId);
    }


    private Map<String,String> addStatusesToCache (DocumentKind dockind) throws EdmException {
        Map<String,String> statuses = new HashMap<String, String>();
        Field status = dockind.getFieldByCn("STATUS");
        if (status != null) {
            for(EnumItem item : status.getEnumItems())
                statuses.put(item.getCn(),item.getTitle());
        }
        cacheDockindToStatuses.put(dockind.getId(), statuses);
        return statuses;
    }
}
