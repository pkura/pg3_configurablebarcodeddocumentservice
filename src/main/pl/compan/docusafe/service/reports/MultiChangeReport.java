package pl.compan.docusafe.service.reports;

import pl.compan.docusafe.core.EdmException;

public class MultiChangeReport extends ReportGenerator {

	public static final String REPORT_ID = "multichange";
    public static final String REPORT_VERSION = "1.0";
    
    public static String getHtmlXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/multichange-html";
    }
	
    public static String getFoXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/multichange-fo";
    }
    
	protected MultiChangeReport(ReportCriteria criteria) 
	{
		super(criteria);
	}

	public void createReport() throws EdmException 
	{
	
	}

	public String getGeneratorId() 
	{
		return REPORT_ID;
	}

	public String getReportTitle() 
	{
		return "title";
	}

	public String getReportVersion() 
	{
		return REPORT_VERSION;
	}

}
