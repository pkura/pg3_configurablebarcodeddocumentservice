package pl.compan.docusafe.service.reports;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.admin.BokAction;

/* User: Administrator, Date: 2005-08-04 14:46:35 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: OutDocumentsReport.java,v 1.19 2007/10/25 15:12:43 mmanski Exp $
 */
public class OutDocumentsReport extends IntOutDocumentsReport
{
    public static final String REPORT_ID = "outdocument";
    public static final String REPORT_VERSION = "1.0";
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(BokAction.class.getPackage().getName(),null);
    public static final String TITLE = sm.getString("RejestrKorespondencjiWychodzacej");

    public OutDocumentsReport(ReportCriteria criteria)
    {
        super(criteria);
    }

    public String getGeneratorId()
    {
        return REPORT_ID;
    }

    public String getReportTitle()
    {
        return TITLE;
    }

    public String getReportVersion()
    {
        return REPORT_VERSION;
    }

    protected boolean isInternal()
    {
        return false;
    }
    
    /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci HTML */
    public static String getHtmlXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/out-documents-html";
    }
    
    /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci PDF */
    public static String getFoXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/out-documents-fo";
    }
}
