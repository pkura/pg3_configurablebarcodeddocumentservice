package pl.compan.docusafe.service.reports;


import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.HibernateException;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TimeUtils;
import pl.compan.docusafe.web.admin.BokAction;
import pl.compan.docusafe.web.reports.office.ProcessReportAction;

import java.io.IOException;
import java.util.*;

/**
 * Raport proces�w
 *
 * @author Wiktor Ocet
 */
public class ProcessReport extends ReportGenerator {

    //
    private static final Logger log = LoggerFactory.getLogger(ProcessReport.class);
    private static StringManager sm = GlobalPreferences.loadPropertiesFile(BokAction.class.getPackage().getName(), null);
    //
    public static final String REPORT_ID = "processreport";
    public static final String REPORT_VERSION = "1.0";
    public static final String TITLE = sm.getString("ProcessReport");
    //
    private ReportCriteria criteria;
    //Long
    private Long documentId = null;
    private Long durationMillis = null;
    private Long delayMillis = null;
    private Long documentKindId = null;
    private Long[] divisionIds = null;
    private Long userId = null;
    //Date
    private Date intervalFrom = null;
    private Date intervalTo = null;
    //Map<String,String>
    private Map<String, String> statusCnTitleMap = null;
    //Boolean
    private Boolean onlyNotCompleted = null;
    private Boolean onlyDelayed = null;
    private Map<String, String> statusCns;


    public ProcessReport(ReportCriteria criteria) {
        super(criteria);
        this.criteria = criteria;
    }

    /**
     * Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci HTML
     */
    public static String getHtmlXslResource() {
        return "pl/compan/docusafe/web/reports/office/process-report-html";
    }

    /**
     * Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci PDF
     */
    public static String getFoXslResource() {
        return "pl/compan/docusafe/web/reports/office/process-report-fo";
    }

    @Override
    public String getGeneratorId() {
        return REPORT_ID;
    }

    @Override
    public String getReportTitle() {
        return TITLE;
    }

    @Override
    public String getReportVersion() {
        return REPORT_VERSION;
    }

    @Override
    public void createReport() throws EdmException {

        try {
            DSApi.openAdmin();

            // wersja raportu
            write(DocumentHelper.createElement("version").addText(REPORT_VERSION));

            //generowanie sekcji opisuj�cej kryteria tworzenia raportu
            Element elCriteria = generateCriteriaSection();
            write(elCriteria);

            //wyniki
            write("<results>");

            loadParametersFromCriteria();

            ProcessReportBean.Query query = getQuery();
            SearchResults<ProcessReportBean> results = ProcessReportBean.search(query);

            //uzupe�nienie statusCnTitleMap - wszystkie statusy je�eli znany dockind
            //dopiero po wyszukaniu, by nie ograniczy� wynik�w
            if ((statusCnTitleMap == null || statusCnTitleMap.size() == 0) && documentKindId != null)
                statusCnTitleMap = getStatusCns();
            StatusReportProvider statusProvider = new StatusReportProvider ();

            DocumentKind documentKind = query.getDocumentKindId() != null ? DocumentKind.find(query.getDocumentKindId()) : null;
            Map<String, String> namesToTitle = (documentKind != null)
                    ? documentKind.getDockindInfo().getProcessesDeclarations().getMappedNamesToTitles()
                    : new HashMap<String, String>();


            long currentTimeMillis = System.currentTimeMillis();
            long totalCount = 0L;
            long delayCount = 0L;
            long delaySum = 0L;
            long durationCount = 0L;
            long durationSum = 0L;
            for (ProcessReportBean bin : results.results()) {
                if (bin == null) continue;

                Element element = DocumentHelper.createElement("document");

                boolean delayed = false;
                if (delayMillis != null) {
                    Long binDuration = bin.getDuration();
                    long binDelay = binDuration == null || binDuration == 0
                            ? bin.getStart() != null
                            ? currentTimeMillis - bin.getStart().getTime() // op�nienie = od daty rozpocz�cia procesu do teraz
                            : -1 // chyba niemo�liwe - musi by� data rozpocz�cia procesu
                            : binDuration - delayMillis; // std obliczenie : czas wykonywania - czas na proces
                    if (binDelay > 0) {
                        String stringBinDelay = TimeUtils.getTimeAsString(binDelay);
                        element.addAttribute(ProcessReportField.DELAY.formKey, stringBinDelay);
                        delayed = true;

                        ++delayCount;
                        delaySum += binDelay;
                    }
                }

                if (onlyDelayed == null || (onlyDelayed && delayed)) {

                    totalCount++;
                    element.addAttribute("lp", String.valueOf(totalCount));

                    for (ProcessReportField field : ProcessReportField.values()) {
                        if (field.hibernateColumn != null && bin.isNotNull(field.hibernateColumn)) {
                            Object value = bin.getValue(field.hibernateColumn);
                            String stringValue = null;

                            switch (field) {
                                case PROCESS_NAME:
                                    String title = namesToTitle.get((String) value);
                                    stringValue = (StringUtils.isNotEmpty(title)) ? title : (String) value;
                                    break;
                                case STATUS_CN:
                                    String statusCn = (String) value;
                                    String statusTitle = statusProvider.getTitle(bin.getDocumentId(),statusCn);
                                    if (StringUtils.isEmpty(statusTitle))
                                        statusTitle = sm.getString("indefinite");
                                    stringValue = statusTitle + " (" + statusCn + ")";
                                    break;
                                case DURATION:
                                    long durationMillis = (Long) value;
                                    if (durationMillis > 0) {
                                        stringValue = TimeUtils.getTimeAsString(durationMillis);

                                        ++durationCount;
                                        durationSum += durationMillis;
                                    }
                                case DELAY:
                                    break;
                                default:
                                    stringValue = bin.getValueAsString(value);
                                    break;
                            }

                            if (stringValue != null)
                                element.addAttribute(field.formKey, stringValue);
                        }
                    }

                    write(element);
                }
            }

            Element elementTotalCount = DocumentHelper.createElement("totalcount");
            elementTotalCount.addAttribute("count", String.valueOf(totalCount));
            write(elementTotalCount);

            Element elementDurationMean = DocumentHelper.createElement("durationMean");
            elementDurationMean.addAttribute("value", durationCount > 0
                    ? TimeUtils.getTimeAsString(durationCount > 0 ? durationSum / durationCount : 0L)
                    : sm.getString("noDuration"));
            write(elementDurationMean);

            Element elementDelayMean = DocumentHelper.createElement("delayMean");
            elementDelayMean.addAttribute("value", delayCount > 0
                    ? TimeUtils.getTimeAsString(delaySum / delayCount)
                    : sm.getString("noDelay"));
            write(elementDelayMean);

            write("</results>");

        } catch (IOException e) {
            throw new EdmException(sm.getString("BladZapisuPlikuTymczasowego"), e);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
//        } catch (SQLException e) {
//            throw new EdmSQLException(e);
        } finally {
            DSApi._close();
        }
    }

    private ProcessReportBean.Query getQuery() {
        String[] statusCns = null;
        if (statusCnTitleMap != null) {
            List<String> statusCnList = getDistinct(statusCnTitleMap.keySet());
            statusCns = statusCnList.toArray(new String[statusCnList.size()]);
        }

        ProcessReportBean.Query query = new ProcessReportBean.Query();
        query.setDocumentId(documentId);
        query.setDivisionIds(divisionIds);
        query.setUserId(userId);
        query.setDocumentKindId(documentKindId);
        query.setIntervalFrom(intervalFrom);
        query.setIntervalTo(intervalTo);
        query.setDurationMillis(durationMillis);
        query.setDelayMillis(delayMillis);
        query.setStatusCn(statusCns);
        query.setOnlyNotCompleted(onlyNotCompleted);
        return query;
    }

    private void loadParametersFromCriteria() {
        for (Iterator iter = criteria.getCriteria().iterator(); iter.hasNext(); ) {
            ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();

            if (criterium instanceof ReportCriteria.Eq) {
                ReportCriteria.Eq criteriumEq = (ReportCriteria.Eq) criterium;
                String property = criterium.getProperty();

                if (property.equals(ProcessReportAction.Criteria.documentId.name()))
                    documentId = (Long) (criteriumEq.getValue());
                else if (property.equals(ProcessReportAction.Criteria.durationMillis.name()))
                    durationMillis = (Long) (criteriumEq.getValue());
                else if (property.equals(ProcessReportAction.Criteria.delayMillis.name()))
                    delayMillis = (Long) (criteriumEq.getValue());
                else if (property.equals(ProcessReportAction.Criteria.documentKind.name()))
                    documentKindId = (Long) (criteriumEq.getValue());
                else if (property.equals(ProcessReportAction.Criteria.division.name()))
                    divisionIds = (Long[]) (criteriumEq.getValue());
                else if (property.equals(ProcessReportAction.Criteria.user.name()))
                    userId = (Long) (criteriumEq.getValue());
                else if (property.equals(ProcessReportAction.Criteria.status.name()))
                    statusCnTitleMap = (Map<String, String>) (criteriumEq.getValue());
                else if (property.equals(ProcessReportAction.Criteria.intervalFrom.name()))
                    intervalFrom = (Date) (criteriumEq.getValue());
                else if (property.equals(ProcessReportAction.Criteria.intervalTo.name()))
                    intervalTo = (Date) (criteriumEq.getValue());

                else if (property.equals(ProcessReportAction.Criteria.onlyNotCompleted.name()))
                    onlyNotCompleted = (Boolean) (criteriumEq.getValue());
                else if (property.equals(ProcessReportAction.Criteria.onlyDelayed.name()))
                    onlyDelayed = (Boolean) (criteriumEq.getValue());
            }
            //else if (criterium instanceof ReportCriteria.EqText){}
            //else if (criterium instanceof ReportCriteria.Range){}
        }
    }

    /**
     * generowanie sekcji opisuj�cej kryteria tworzenia raportu
     */
    private Element generateCriteriaSection() {
        Element elCriteria = DocumentHelper.createElement("criteria");
        for (Iterator iter = criteria.getCriteria().iterator(); iter.hasNext(); ) {
            ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();
            if (criterium instanceof ReportCriteria.Eq) {
                ReportCriteria.Eq criteriaEq = (ReportCriteria.Eq) criterium;
                String criteriaProperty = criterium.getProperty();
                Object criteriaValue = criteriaEq.getValue();

                Element el = DocumentHelper.createElement("eq");

                el.addAttribute("property", criteriaProperty);
                el.addAttribute("value", propertyValue(criteriaEq.getValue()));

                if (criteriaValue != null) {

                    if (criteriaProperty.equals(ProcessReportAction.Criteria.user.name())) {
                        Long userId = (Long) criteriaValue;
                        try {
                            DSUser user = DSUser.findById(userId);
                            if (user != null)
                                el.addAttribute("name", user.getLastnameFirstnameName());
                        } catch (UserNotFoundException e) {
                            Log.error(e.getMessage(), e);
                        } catch (EdmException e) {
                            Log.error(e.getMessage(), e);
                        }
                    } else if (criteriaProperty.equals(ProcessReportAction.Criteria.documentKind.name())) {
                        Long documentKindId = (Long) criteriaValue;
                        try {
                            DocumentKind documentKind = DocumentKind.find(documentKindId);
                            if (documentKind != null)
                                el.addAttribute("name", documentKind.getName());
                        } catch (EdmException e) {
                            Log.error(e.getMessage(), e);
                        }
                    } else if (criteriaProperty.equals(ProcessReportAction.Criteria.division.name())) {
                        Long[] divisionIds = (Long[]) criteriaValue;
                        List<String> divisionTitlesList = new ArrayList<String>();
                        
                        try {
                        	for (Long dId : divisionIds) {
                        		DSDivision division = DSDivision.findById(dId.intValue());
                        		divisionTitlesList.add(division.getName());
							}
                            el.addAttribute("name", StringUtils.join(divisionTitlesList, ", "));
                        } catch (DivisionNotFoundException e) {
                            log.error(e.getMessage(), e);
                        } catch (EdmException e) {
                            Log.error(e.getMessage(), e);
                        }
                    } else if (criteriaProperty.equals(ProcessReportAction.Criteria.durationMillis.name())
                            || criteriaProperty.equals(ProcessReportAction.Criteria.delayMillis.name())) {
                        Long millis = (Long) criteriaValue;
                        el.addAttribute("name", TimeUtils.getTimeAsString(millis));
                    } else if (criteriaProperty.equals(ProcessReportAction.Criteria.intervalFrom.name())
                            || criteriaProperty.equals(ProcessReportAction.Criteria.intervalTo.name())) {
                        String sDate = DateUtils.jsDateFormat.format((Date) criteriaValue);
                        el.addAttribute("name", sDate);
                    } else if (criteriaProperty.equals(ProcessReportAction.Criteria.onlyNotCompleted.name())
                            || criteriaProperty.equals(ProcessReportAction.Criteria.onlyDelayed.name())) {
                        boolean b = getCheckboxToBoolean((Boolean) criteriaValue);
                        el.addAttribute("name", b ? sm.getString("true") : sm.getString("false"));
                    } else if (criteriaProperty.equals(ProcessReportAction.Criteria.status.name())) {
                        Map<String, String> statusCnTitleMap = (Map<String, String>) criteriaValue;
                        List<String> statusTitlesList = getDistinct(statusCnTitleMap.values());
                        String[] statusTitles = statusTitlesList.toArray(new String[statusTitlesList.size()]);
                        el.addAttribute("name", StringUtils.join(statusTitles, ", "));
                    } else if (criteriaProperty.equals(ProcessReportAction.Criteria.documentId.name())) {
                        Long docId = (Long) criteriaValue;
                        el.addAttribute("name", Long.toString(docId));
                    }
                }
                elCriteria.add(el);
            }
            //  else if (criterium instanceof ReportCriteria.EqText) {
            //} else if (criterium instanceof ReportCriteria.Range) {
            //} else {
            //    throw new IllegalArgumentException("Nieznany typ kryterium: " + criterium);
            //}

        }
        return elCriteria;
    }

    private <T> List<T> getDistinct(Collection<T> set) {
        List<T> list = new ArrayList<T>();
        for (T t : set)
            if (!list.contains(t))
                list.add(t);
        return list;
    }

    private boolean getCheckboxToBoolean(Boolean b) {
        return b == null ? false : b;
    }

    public Map<String, String> getStatusCns() {
        statusCns = new HashMap<String, String>();
        try {
            Field status = DocumentKind.find(documentKindId).getFieldByCn("STATUS");
            if (status != null) {
                List<EnumItem> statuses = status.getEnumItems();
                for (EnumItem ei : statuses)
                    statusCns.put(ei.getCn(), ei.getTitle());
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return statusCns;
    }

    public enum ProcessReportField {
        DOCUMENT_ID(ProcessReportBean.HibernateColumn.DOCUMENT_ID, "documentId", "Id dokumentu"),
        PROCESS_NAME(ProcessReportBean.HibernateColumn.PROCESS_NAME, "processName", "Nazwa procesu"),
        STATUS_CN(ProcessReportBean.HibernateColumn.STATUS_CN, "status", "Status"),
        START(ProcessReportBean.HibernateColumn.START, "start", "Data rozpocz�cia"),
        END(ProcessReportBean.HibernateColumn.END, "end", "Data zako�czenia"),
        DURATION(ProcessReportBean.HibernateColumn.DURATION_MILLIS, "duration", "Czas ��czny"),
        DELAY(ProcessReportBean.HibernateColumn.DELAY_MILLIS, "delay", "Op�nienie"),
        DIVISION(ProcessReportBean.HibernateColumn.DIVISION, "division", "Dzia�"),
        USER(ProcessReportBean.HibernateColumn.USER, "user", "U�ytkownik");
        //
        public final ProcessReportBean.HibernateColumn hibernateColumn;
        public final String formKey;
        public final String title;

        private ProcessReportField(ProcessReportBean.HibernateColumn hibernateColumn, String formKey, String title) {
            this.hibernateColumn = hibernateColumn;
            this.formKey = formKey;
            this.title = title;
        }
    }
}
