package pl.compan.docusafe.service.reports.dockind;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.HibernateException;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.util.DateUtils;

public class FakturyKosztoweNW extends AbstractDocumentsCountReport
{
    public static final String REPORT_ID = "faktury-kosztowe";
    public static final String REPORT_VERSION = "1.0";

    public FakturyKosztoweNW(ReportCriteria criteria)
    {
        super(criteria);
    }

    public String getGeneratorId()
    {
        return REPORT_ID;
    }

    public String getReportVersion()
    {
        return REPORT_VERSION;
    }
    
    /** 
     * Zwraca klas� dostarczaj�c� specyficzne informacje do wyszukiwania dokument�w na potrzeby
     * szczeg��w raportu.
     */
    public static SearchDocumentsHandler getSDHandler()
    {
        return new AssignedReportSDHandler();
    }
    
    protected String getSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected String getAliasSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected static String getSqlColumnStatic(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
    {
        throw new IllegalStateException("Napotkano niepoprawn� definicj� kryterium raportu");
    }

    @Deprecated
    protected StringBuilder very_old_getSqlStatement(Criterion colCriterion) throws EdmException
    {
        StringBuilder sql = new StringBuilder("select ");
        sql.append("distinct inv.document_id as id, ");
        sql.append("tsk.assigned_user as username, ");
        sql.append("tsk.dsw_assignment_accepted as accepted, ");
        sql.append("inv.nr_faktury as inv_number, ");
        sql.append("vendor.name + ' nr. ' + vendor.numerKontrahenta as vendor_info, ");
        sql.append("tsk.documentCdate as cdate, ");
        sql.append("datediff(dd, tsk.documentCdate, GETDATE()) as days_total, ");
        sql.append("tsk.rcvDate as rcvDate, ");
        sql.append("datediff(dd, tsk.rcvDate, GETDATE()) as days_user ");
        sql.append("from dsg_invoice inv ");
        sql.append("join dsw_tasklist tsk with (nolock) on inv.document_id = dso_document_id ");
        sql.append("join df_dicinvoice vendor on inv.dostawca = vendor.id ");
        sql.append("where inv.akceptacja_finalna is null or inv.akceptacja_finalna = 0 ");
        sql.append("order by datediff(dd, tsk.documentCdate, GETDATE()) desc");
        return sql;
    }
    
    @Deprecated
    protected StringBuilder old_getSqlStatement(Criterion colCriterion) throws EdmException
    {
        StringBuilder sql = new StringBuilder("select ");
        sql.append("distinct inv.document_id as id, ");
        sql.append("tsk.assigned_user as username, ");
        sql.append("tsk.dsw_assignment_accepted as accepted, ");
        sql.append("inv.nr_faktury as inv_number, ");
        sql.append("vendor.name + ' nr. ' + vendor.numerKontrahenta as vendor_info, ");
        sql.append("tsk.documentCdate as cdate, ");
        sql.append("datediff(dd, tsk.documentCdate, GETDATE()) as days_total, ");
        sql.append("tsk.rcvDate as rcvDate, ");
        sql.append("datediff(dd, tsk.rcvDate, GETDATE()) as days_user, ");
        //sql.append("inv.data_wystawienia as data_wystawienia, ");
        sql.append("CONVERT(varchar(12), inv.data_wystawienia, 105) as data_wystawienia, ");
        sql.append("inv.kwota_brutto as kwota_brutto, ");
        //sql.append("CONVERT(varchar(20), inv.kwota_brutto, 0) as kwota_brutto, ");
        sql.append("inv.opis_towaru as opis_towaru ");
        sql.append("from dsg_invoice inv ");
        sql.append("join dsw_tasklist tsk with (nolock) on inv.document_id = dso_document_id ");
        sql.append("join df_dicinvoice vendor on inv.dostawca = vendor.id ");
        sql.append("where inv.akceptacja_finalna is null or inv.akceptacja_finalna = 0 ");
        sql.append("order by datediff(dd, tsk.documentCdate, GETDATE()) desc");
        return sql;
    }

    protected StringBuilder getSqlStatement(Criterion colCriterion) throws EdmException
    {
        StringBuilder sql = new StringBuilder("select ");
        sql.append("distinct inv.document_id as id, ");
        sql.append("(CASE WHEN tsk.resource_count > 1 and tsk.dsw_assignment_accepted = 0 THEN 'grupa' ELSE tsk.dsw_resource_res_key END) as username, ");
        sql.append("tsk.dsw_assignment_accepted as accepted, ");
        sql.append("inv.nr_faktury as inv_number, ");
        sql.append("(vendor.name + ' nr. ' + ISNULL(vendor.numerKontrahenta, 'brak numeru')) as vendor_info, ");
        sql.append("tsk.documentCdate as cdate, ");
        sql.append("datediff(dd, tsk.documentCdate, GETDATE()) as days_total, ");
        sql.append("tsk.rcvDate as rcvDate, ");
        sql.append("datediff(dd, tsk.rcvDate, GETDATE()) as days_user, ");
        sql.append("CONVERT(varchar(12), inv.data_wystawienia, 105) as data_wystawienia, ");
        sql.append("inv.kwota_brutto as kwota_brutto, ");
        sql.append("inv.opis_towaru as opis_towaru ");
        sql.append("from dsg_invoice inv ");
        sql.append("join dsw_tasklist tsk with (nolock) on inv.document_id = dso_document_id ");
        
        if (DocumentKind.isAvailable(DocumentLogicLoader.INVOICE_PTE))
        	sql.append("join dp_inst vendor on inv.dostawca = vendor.id ");
        else
        	sql.append("join df_dicinvoice vendor on inv.dostawca = vendor.id ");
        
        sql.append("where inv.akceptacja_finalna is null or inv.akceptacja_finalna = 0 ");
        sql.append("order by datediff(dd, tsk.documentCdate, GETDATE()) desc");
        return sql;
    }
    
    protected String resultColumnName()
    {
        return sm.getString("LiczbaDokumentow");
    }
    
    protected String resultValueSuffix()
    {
        return null;
    }
    
    public static class AssignedReportSDHandler implements SearchDocumentsHandler 
    { 
        public String getSqlColumn(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
        {
            return getSqlColumnStatic(fieldType, fieldCn, documentKind);
        }            
        
        public String getSqlDocumentId()
        {
            return "dt.document_id";
        }
        
        public String getSqlFromClause(DocumentKind documentKind, Report report)
        {
            StringBuilder sql = new StringBuilder(); 
           /* sql.append(" from dsg_invoice_ic ic");
            sql.append(" join ds_document_acceptance acc on acc.document_id = ic.document_id");
            sql.append(" join ds_user us on us.name = acc.username");*/
            return sql.toString();
        }
        
        public String getResultColumnNameOnDetails(Report report)
        {
            // dla tego raportu nie ma kolumny z wynikiem na szczeg�ach
            return null;
        }
        
        public String getResultValueSuffix()
        {
            return "";
        }
        
        public boolean selectDistinct()
        {
            return false;
        }
    }
    
    public void createReport() throws EdmException
    {
        report = dockindCriteria.getReport();         
        PreparedStatement ps = null;
        try
        {
            DSApi.openAdmin();
            
            // wersja raportu
            write(DocumentHelper.createElement("version").addText(getReportVersion()));

            // generowanie elementu z informacjami g��wnymi
            Element elInfo = DocumentHelper.createElement("info");
            elInfo.addAttribute("name", getReportTitle());
            String orientation = report.getProperty(Report.PDF_ORIENTATION);
            //elInfo.addAttribute("orientation", orientation == null ? Report.VERTICAL_ORIENTATION : orientation);
            elInfo.addAttribute("orientation", Report.HORIZONTAL_ORIENTATION);
            write(elInfo);
            
                        
            // GENEROWANIE NAG��WKA TABELKI
            // kryteria wierszowe
            
            Element elHeader = DocumentHelper.createElement("header");
            String[] rows = new String[]{"Id dokumentu","Username","Przyj�cie pisma","Numer faktury", "Dostawca", 
            		"Data utworzenia dokumentu","Dni w systemie", "Data otrzymania przez u�ytkownika", "Dni u u�ytkownika", "Data wystawienia", "Kwota brutto", "Opis towaru"};
            for (String row : rows)
            {
                Element el = DocumentHelper.createElement("column");
                el.addAttribute("name", row);
                elHeader.add(el);
            }
            // kryterium kolumnowe - zak�adam, �e mo�e by� tylko jedno
            write(elHeader);
            
            write("<results>");
            // GENEROWANIE TRE�CI G��WNEJ TABELKI
            // tworzenie odpowiedniego zapytania SQL
            StringBuilder sql = getSqlStatement(null);
                        
            String firstColumnStyle = report.getProperty(Report.FIRST_COLUMN_STYLE);
            String secondColumnStyle = report.getProperty(Report.SECOND_COLUMN_STYLE);            
            String firstColumnValue = null;
            String secondColumnValue = null;
            
            ps = DSApi.context().prepareStatement(sql.toString());
            // ustalanie parametr�w zapytania
            
            ResultSet rs = ps.executeQuery();

            String[] cols = new String[rs.getMetaData().getColumnCount()];
            int[] partSum = new int[cols.length];  // sumy cz�ciowe
            int[] sum = new int[cols.length]; 
            for(int i = 0; i<cols.length;i++)
            {
            	cols[i] = rs.getMetaData().getColumnName(i+1);
            }
            
            // umieszczanie wynik�w w tabelce
            // http://localhost:8080/docusafe/repository/edit-dockind-document.action?id=2866
            int count=0;
            while (rs.next())
            {
            	count++;
                Element element = DocumentHelper.createElement("row");                
                for (String column : cols)
                {                    
                    Element e = DocumentHelper.createElement("cell");
                    String value;
                    if(column.equals("cdate")||column.equals("rcvDate"))
                    	value = DateUtils.formatJsDate(rs.getDate(column));
                    else if(column.equals("accepted"))
                    {
                    	if(rs.getInt(column)==1) value = "TAK";
                    	else value = "NIE";
                    }
                    else if(column.equals("kwota_brutto"))
                    {
                    	try
                    	{
                    		value = rs.getBigDecimal("kwota_brutto").setScale(2).toString();
                    		LoggerFactory.getLogger("tomekl").debug("test {}",value);
                    	}
                    	catch (Exception ex) 
                    	{
							value = null;
						}
                    }
                    else value = rs.getString(column);
                    if(column.equals("id")){
                    	e.addAttribute("link", dockindCriteria.getContextPath()+"/repository/edit-document.action?id="+value);
                    }
                    e.addAttribute("value", value);  
                    element.add(e);
                }                
                write(element);
            }
            
            
    		Element nElem = DocumentHelper.createElement("row");
    		for (int i=0; i < cols.length; i++)
            {
                Element ne = DocumentHelper.createElement("cell");
                if (i == 0)
                {
                    ne.addAttribute("value", "Razem dokument�w");
                    ne.addAttribute("bold", "true");
                }
                if (i == 2)
                {
                    ne.addAttribute("value", String.valueOf(count));
                    ne.addAttribute("align", "left");
                }
                nElem.add(ne);
             
            }
    		write(nElem);
        	
            rs.close();
            
            write("</results>");     
        }
        catch (IOException e)
        {
            throw new EdmException("B��d zapisu pliku tymczasowego", e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
            DSApi._close();
        }
    }
    
    protected void addWhereClause(StringBuilder sql) throws EdmException
    {
        for (Criterion criterion : report.getCriteria().values())
        {
            ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
            if (criterium == null)
                continue;
            
            else if (criterium.getProperty().equals("user"))
            {
                Object[] keys = getEnumerationKeys(criterion);                    
                if (keys != null && keys.length > 0)
                {
                    String str = null;
                    for (int i=0; i < keys.length; i++)
                    {                            
                        str = (str == null ? "?" : str+",?");                            
                    }
                    sql.append(" and us.name in (").append(str).append(")");
                }
            }
            else if (criterium.getProperty().equals("division"))
            {
                Object[] keys = getEnumerationKeys(criterion);                    
                if (keys != null && keys.length > 0)
                {
                    String str = null;
                    for (int i=0; i < keys.length; i++)
                    {                            
                        str = (str == null ? "?" : str+",?");                            
                    }
                    sql.append(" and div.guid in (").append(str).append(")");
                }
            }
        }
        
    }
    
    
}