package pl.compan.docusafe.service.reports.dockind;

import java.util.List;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.service.reports.ReportCriteria;

/**
 * Klasa bazowa dla wszystkich raport�w/zestawie� liczbowych (czyli takich kt�re zliczaj�, sumuj�, 
 * licz� �redni� itp. z r�nych danych dla r�nego typu dokument�w) dla mechanizmu dockind.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public abstract class AbstractDocumentsCountReport extends AbstractDockindReport
{
    public AbstractDocumentsCountReport(ReportCriteria criteria)
    {
        super(criteria);
    }
    
    /** Przekazuje nazw� dla u�ytkownika kolumny pokazuj�cej wyniki raportu */
    protected abstract String resultColumnName();
    
    /** 
     * Przekazuje napis (suffix), kt�ry ma by� dodany do wszystkich warto�ci w kolumnie pokazuj�cej
     * wyniki raportu.
     */
    protected abstract String resultValueSuffix();
    
    /**
     * Dodaje do zbioru kolumn raportu kolumny specyficzne dla tego raportu.
     */
    protected void addSpecificColumns(List<ColumnInfo> columns, Element elHeader, Criterion colCriterion) throws EdmException
    {
        // kolumny dla kryterium kolumnowego
        if (colCriterion != null)
        {
            List<Object> values = getEnumerationValues(colCriterion);
            if (values != null)
                for (Object value : values)
                {
                    Element el = DocumentHelper.createElement("column");
                    el.addAttribute("name", value.toString());
                    el.addAttribute("align", "center");
                    elHeader.add(el);
                    columns.add(new ColumnInfo(Field.INTEGER, null, "center", true, null));
                }
        }
        // kolumna zliczajaca
        Element el = DocumentHelper.createElement("column");
        el.addAttribute("name", colCriterion != null ? sm.getString("SUMA") : resultColumnName());
        el.addAttribute("align", "center");
        elHeader.add(el);
        columns.add(new ColumnInfo(Field.INTEGER, null, "center", true, resultValueSuffix()));  
    }
}
