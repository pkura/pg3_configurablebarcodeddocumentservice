package pl.compan.docusafe.service.reports.dockind;

import java.util.List;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.util.StringManager;

/**
 * Klasa bazowa dla wszystkich raport�w/zestawie� dokument�w (czyli takich, kt�re pokazuj� list� 
 * dokument�w spe�niaj�cych podane kryteria) dla mechanizmu dockind.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public abstract class AbstractDocumentsListReport extends AbstractDockindReport
{
    StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    public AbstractDocumentsListReport(ReportCriteria criteria)
    {
        super(criteria);
    }
    
    /**
     * Dodaje do zbioru kolumn raportu kolumny specyficzne dla tego raportu.
     */
    protected void addSpecificColumns(List<ColumnInfo> columns, Element elHeader, Criterion colCriterion) throws EdmException
    {
        for (String col : report.getPropertyArray(Report.SPECIFIC_COLUMNS))
        {
            int dotPos = col.indexOf(".");
            String type = col.substring(0, dotPos);
            String fieldCn = col.substring(dotPos+1);
            
            Element el = DocumentHelper.createElement("column");
            el.addAttribute("name", getSpecificColumnName(type, fieldCn));
            el.addAttribute("align", "center");
            elHeader.add(el);
            columns.add(new ColumnInfo(getColumnType(type, fieldCn, dockindCriteria.getDocumentKind()), fieldCn, "center", true, null));    
        }
    }
    
    /**
     * Przekazuje nazw� specyficznej kolumny - gdy by�o ju� zdefiniowane kryterium dla takiej kolumny,
     * to pobieram nazw� stamt�d w przeciwnym razie zwracam domy�ln� nazw�.
     */
    protected String getSpecificColumnName(String type, String fieldCn) throws EdmException
    {
        Criterion criterion = report.getCriterion(type, fieldCn);
        if (criterion != null)
            return criterion.getName();
        
        if ("standard".equals(type))
        {
            if ("id".equals(fieldCn))
                return sm.getString("column.id");
            else if ("description".equals(fieldCn))
                return sm.getString("column.description");
            else if ("officeNumber".equals(fieldCn))
                return sm.getString("column.officeNumber");   
            else if ("user".equals(fieldCn))
                return sm.getString("column.user");
            else if ("division".equals(fieldCn))
                return sm.getString("column.division");
            else if ("date".equals(fieldCn))
                return sm.getString("column.date");
        }
        else
        {
            return dockindCriteria.getDocumentKind().getFieldByCn(fieldCn).getName();
        }
        return null;
    }
}