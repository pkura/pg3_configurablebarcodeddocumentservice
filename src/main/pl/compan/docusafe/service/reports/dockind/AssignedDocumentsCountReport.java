package pl.compan.docusafe.service.reports.dockind;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.service.reports.ReportCriteria;

/**
 * Liczbowy raport zadekretowanych dokument�w. (Stary OPS)
 * 
 * Standardowe kryteria okre�laj�:
 * <ul>
 * <li> date - data dekretacji dokumentu,
 * <li> division - dzia�, do kt�rego jest dekretowany dokument,
 * <li> user - u�ytkownik, kt�ry wykonuje dekretacji.
 * </ul>
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class AssignedDocumentsCountReport extends AbstractDocumentsCountReport
{
    public static final String REPORT_ID = "assigned-docs-count";
    public static final String REPORT_VERSION = "1.0";

    public AssignedDocumentsCountReport(ReportCriteria criteria)
    {
        super(criteria);
    }

    public String getGeneratorId()
    {
        return REPORT_ID;
    }

    public String getReportVersion()
    {
        return REPORT_VERSION;
    }
    
    /** 
     * Zwraca klas� dostarczaj�c� specyficzne informacje do wyszukiwania dokument�w na potrzeby
     * szczeg��w raportu.
     */
    public static SearchDocumentsHandler getSDHandler()
    {
        return new AssignedReportSDHandler();
    }
    
    protected String getSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected String getAliasSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected static String getSqlColumnStatic(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
    {
        if ("standard".equals(fieldType))
        {
            if ("date".equals(fieldCn))
                return "ht.cdate";
            else if ("division".equals(fieldCn))
                return "ht.targetguid";
            else if ("user".equals(fieldCn))
                return "ht.sourceuser";
            else if ("result".equals(fieldCn)) 
                // dla tego raportu nie u�ywamy kolumny z wynikiem na szczeg�ach - zwracam 1 �eby co� by�o
                return "1";
        }
        else if ("dockind".equals(fieldType))
        {
            return "dt."+(documentKind.getFieldByCn(fieldCn).getColumn());
        }
        throw new IllegalStateException("Napotkano niepoprawn� definicj� kryterium raportu");
    }

    protected StringBuilder getSqlStatement(Criterion colCriterion) throws EdmException
    {
        String partitionString = getPartitionString(report);
        StringBuilder sql = new StringBuilder("select ");
        if (partitionString != null)
            sql.append(partitionString).append(", ");
        String colCriterionString = colCriterion != null ? getColumnCriterionString(report) : null;
        if (colCriterionString != null)
            sql.append(colCriterionString);
        sql.append("count(dt.document_id) from ");
        sql.append(dockindCriteria.getDocumentKind().getTablename()).append(" dt,dso_document_asgn_history ht").append(" where (dt.document_id=ht.document_id)");
        // z historii dekretacja tylko bierzemy pod uwag� te wpisy, kt�re dotycz� dekretacji do kogo�
        sql.append(" and (ht.type is null) and (ht.finished=0) and (ht.accepted=0) and (ht.processname<>'rejestracja pisma')");
        // kryteria raportu w WHERE
        addWhereClause(sql);

        if (partitionString != null)
            sql.append(" group by ").append(partitionString);
        
        return sql;
    }

    protected String resultColumnName()
    {
        return sm.getString("LiczbaDokumentow");
    }
    
    protected String resultValueSuffix()
    {
        return null;
    }
    
    public static class AssignedReportSDHandler implements SearchDocumentsHandler 
    { 
        public String getSqlColumn(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
        {
            return getSqlColumnStatic(fieldType, fieldCn, documentKind);
        }            
        
        public String getSqlDocumentId()
        {
            return "dt.document_id";
        }
        
        public String getSqlFromClause(DocumentKind documentKind, Report report)
        {
            StringBuilder fromSql = new StringBuilder(documentKind.getTablename());
            fromSql.append(" dt,dso_document_asgn_history ht").append(" where (dt.document_id=ht.document_id)");
            fromSql.append(" and (ht.type is null) and (ht.finished=0) and (ht.accepted=0) and (ht.processname<>'rejestracja pisma')");
            return fromSql.toString();
        }
        
        public String getResultColumnNameOnDetails(Report report)
        {
            // dla tego raportu nie ma kolumny z wynikiem na szczeg�ach
            return null;
        }
        
        public String getResultValueSuffix()
        {
            return "";
        }
        
        public boolean selectDistinct()
        {
            return false;
        }
    }
}
