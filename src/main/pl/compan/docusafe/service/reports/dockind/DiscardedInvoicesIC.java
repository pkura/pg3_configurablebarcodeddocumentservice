package pl.compan.docusafe.service.reports.dockind;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.service.reports.dockind.AbstractDockindReport.SearchDocumentsHandler;
import pl.compan.docusafe.service.reports.dockind.UserAcceptedInvoicesIC.AssignedReportSDHandler;
import pl.compan.docusafe.util.DateUtils;

public class DiscardedInvoicesIC extends AbstractDocumentsCountReport
{
    public static final String REPORT_ID = "discarded-inv-ic";
    public static final String REPORT_VERSION = "1.0";
	
	public DiscardedInvoicesIC(ReportCriteria criteria)
	{
		super(criteria);
	}
	
	public String getGeneratorId()
    {
        return REPORT_ID;
    }

    public String getReportVersion()
    {
        return REPORT_VERSION;
    }
    public static SearchDocumentsHandler getSDHandler()
    {
        return new AssignedReportSDHandler();
    }
    
    protected String getSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected String getAliasSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected static String getSqlColumnStatic(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
    {
        if ("standard".equals(fieldType))
        {
            if ("date".equals(fieldCn))
                return "aud.ctime";
            return "1";
        }
        throw new IllegalStateException("Napotkano niepoprawn� definicj� kryterium raportu");
    }

    protected StringBuilder getSqlStatement(Criterion colCriterion) throws EdmException
    {
    	String partitionString = getPartitionString(report);
        StringBuilder sql = new StringBuilder("select ");
        sql
	    	.append("aud.username as username, ")
	    	.append("dateadd(dd,0, datediff(dd,0,aud.ctime)) as discardTime, ") 
	    	.append("count (distinct inv.document_id) as numDocs ")
	
	    	.append("from dsg_invoice_ic inv ")
	    	.append("join dso_document_audit aud on aud.document_id = inv.document_id ")
	    	.append("where inv.status = 6 ")
	    	.append("and aud.ctime > ? and aud.ctime < ?")
	    	.append("and  CHARINDEX('Odrzucony', aud.description) > 0 ");
        addWhereClause(sql);
        sql.append(" group by aud.username, dateadd(dd,0, datediff(dd,0,aud.ctime))");
        return sql;
    }

    protected String resultColumnName()
    {
        return sm.getString("LiczbaDokumentow");
    }
    
    protected String resultValueSuffix()
    {
        return null;
    }
    
    public static class AssignedReportSDHandler implements SearchDocumentsHandler 
    { 
        public String getSqlColumn(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
        {
            return getSqlColumnStatic(fieldType, fieldCn, documentKind);
        }            
        
        public String getSqlDocumentId()
        {
            return "dt.document_id";
        }
        
        public String getSqlFromClause(DocumentKind documentKind, Report report)
        {
            StringBuilder sql = new StringBuilder(); 
            sql.append(" from dsg_invoice_ic ic");
            sql.append(" join ds_document_acceptance acc on acc.document_id = ic.document_id");
            sql.append(" join ds_user us on us.name = acc.username");
            return sql.toString();
        }
        
        public String getResultColumnNameOnDetails(Report report)
        {
            // dla tego raportu nie ma kolumny z wynikiem na szczeg�ach
            return null;
        }
        
        public String getResultValueSuffix()
        {
            return "";
        }
        
        public boolean selectDistinct()
        {
            return false;
        }
    }
    
    public void createReport() throws EdmException
    {
        report = dockindCriteria.getReport();         
        PreparedStatement ps = null;
        try
        {
            DSApi.openAdmin();
            
            // wersja raportu
            write(DocumentHelper.createElement("version").addText(getReportVersion()));

            // generowanie elementu z informacjami g��wnymi
            Element elInfo = DocumentHelper.createElement("info");
            elInfo.addAttribute("name", getReportTitle());
            String orientation = report.getProperty(Report.PDF_ORIENTATION);
            elInfo.addAttribute("orientation", orientation == null ? Report.VERTICAL_ORIENTATION : orientation);
            write(elInfo);
            
            // generowanie sekcji opisuj�cej kryteria tworzenia raportu
            Element elCriteria = DocumentHelper.createElement("criteria");           
            for (Criterion criterion : report.getCriteria().values())
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
                if (criterium == null)
                    continue;
                if (criterium.getProperty()=="days")
               	 continue;
                
                Element el = null;                
                if (criterium instanceof ReportCriteria.Range)
                {
                    el = DocumentHelper.createElement("range");
                    el.addAttribute("property", criterion.getName());
                   
                    if (criterium.getProperty().equals("date"))
                    {
                        String start = (String) ((ReportCriteria.Range) criterium).getStart();
                        String end = (String) ((ReportCriteria.Range) criterium).getEnd();
                        
                        if (start != null)
                        {
                            dateFrom = DateUtils.parseJsDate(start);
                            el.addAttribute("start", start);
                        }
                        if (end != null)
                        {
                            dateTo = DateUtils.parseJsDate(end);
                            el.addAttribute("end", end);
                        }   
                    }
                }
                else if (criterium instanceof ReportCriteria.ValueEnumeration)
                {
                    el = DocumentHelper.createElement("enumeration");
                    el.addAttribute("property", criterion.getName());
                    List<Object> values = getEnumerationValues(criterion);
                    for (Object value : values)
                    {
                        Element val = DocumentHelper.createElement("value");
                        val.addAttribute("title", value.toString());
                        el.add(val);
                    }
                }
                else
                {
                    throw new IllegalArgumentException("Nieznany typ kryterium: "+criterium);
                }                
                if (el != null)                                     
                    elCriteria.add(el);                 
            }            
            write(elCriteria);
                        
            // GENEROWANIE NAG��WKA TABELKI
            // kryteria wierszowe
            
            Element elHeader = DocumentHelper.createElement("header");
            String[] rows = new String[]{"U�ytkownik","Data odrzucenia", "Liczba odrzuce�"};
            for (String row : rows)
            {
                Element el = DocumentHelper.createElement("column");
                el.addAttribute("name", row);
                elHeader.add(el);
            }
            // kryterium kolumnowe - zak�adam, �e mo�e by� tylko jedno
            write(elHeader);
            
            write("<results>");
            // GENEROWANIE TRE�CI G��WNEJ TABELKI
            // tworzenie odpowiedniego zapytania SQL
            StringBuilder sql = getSqlStatement(null);
                        
            String firstColumnStyle = report.getProperty(Report.FIRST_COLUMN_STYLE);
            String secondColumnStyle = report.getProperty(Report.SECOND_COLUMN_STYLE);            
            String firstColumnValue = null;
            String secondColumnValue = null;
            
            ps = DSApi.context().prepareStatement(sql.toString());
            // ustalanie parametr�w zapytania
            int count = 0;       
            for (Criterion criterion : report.getCriteria().values())
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
                if (criterium == null)
                    continue;
                
                
                if (criterium.getProperty().equals("date"))
                {
                    if (dateFrom != null )            
                        ps.setDate(++count, new java.sql.Date(dateFrom.getTime()));
                    if (dateTo != null)
                        ps.setDate(++count, new java.sql.Date(dateTo.getTime()));
                }
                else if (criterium.getProperty().equals("user"))
                {
                    Object[] keys = getEnumerationKeys(criterion);                    
                    if (keys != null && keys.length > 0)
                    {
                        for (Object key : keys)
                        {     
                            ps.setString(++count, key.toString());
           
                        }
                    }
                }
                else if (criterium.getProperty().equals("division"))
                {
                    Object[] keys = getEnumerationKeys(criterion);                    
                    if (keys != null && keys.length > 0)
                    {
                        for (Object key : keys)
                        {     
                            ps.setString(++count, key.toString());
           
                        }
                    }
                }
            }
            // wykonanie zapytania
            ResultSet rs = ps.executeQuery();

            String[] cols = new String[rs.getMetaData().getColumnCount()];
            int[] partSum = new int[cols.length];  // sumy cz�ciowe
            int[] sum = new int[cols.length]; 
            sum[2]=0;
            for(int i = 0; i<cols.length;i++)
            {
            	cols[i] = rs.getMetaData().getColumnName(i+1);
            }
            
            // umieszczanie wynik�w w tabelce
            while (rs.next())
            {
                Element element = DocumentHelper.createElement("row");                
                count = 0;
                for (String column : cols)
                {                    
                    count++;
                    Element e = DocumentHelper.createElement("cell");
                    String value;
                    if(column.equals("discardTime"))
                    	value = DateUtils.formatJsDate(rs.getDate(column));
                    else value = rs.getString(column);
                    if(rs.getString(column).equals(firstColumnValue)) value = "";
                    if (column.equalsIgnoreCase("numDocs"))
                    {
                         sum[count-1] += Integer.valueOf(value);
                         partSum[count-1] += Integer.valueOf(value);
                    }
                    
                    
                    e.addAttribute("value", value);  
                    element.add(e);
                }                
                write(element);
            }
            
            
        		Element nElem = DocumentHelper.createElement("row");
        		for (int i=0; i < cols.length; i++)
                {
                    Element ne = DocumentHelper.createElement("cell");
                    if (i == 0)
                    {
                        ne.addAttribute("value", "Razem");
                        ne.addAttribute("bold", "true");
                    }
                    if (i==2)
                    {
                        ne.addAttribute("value", String.valueOf(sum[2]));
                        ne.addAttribute("align", "left");
                    }
                    nElem.add(ne);
                }
        		write(nElem);
        	
            rs.close();
            
            if (report.propertyOn(Report.ROW_SUMMARY))
            {
            }
            
            write("</results>");     
        }
        catch (IOException e)
        {
            throw new EdmException("B��d zapisu pliku tymczasowego", e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
            DSApi._close();
        }
    }
    
    protected void addWhereClause(StringBuilder sql) throws EdmException
    {
        for (Criterion criterion : report.getCriteria().values())
        {
            ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
            if (criterium == null)
                continue;
            
            else if (criterium.getProperty().equals("user"))
            {
                Object[] keys = getEnumerationKeys(criterion);                    
                if (keys != null && keys.length > 0)
                {
                    String str = null;
                    for (int i=0; i < keys.length; i++)
                    {                            
                        str = (str == null ? "?" : str+",?");                            
                    }
                    sql.append(" and aud.username in (").append(str).append(")");
                }
            }
        }
        
    }
    
    
}

