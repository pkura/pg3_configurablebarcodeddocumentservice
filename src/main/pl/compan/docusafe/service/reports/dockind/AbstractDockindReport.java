package pl.compan.docusafe.service.reports.dockind;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.HibernateException;
import org.hibernate.dialect.FirebirdDialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.SQLServerDialect;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.core.dockinds.field.EnumerationField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.service.reports.ReportGenerator;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Klasa bazowa dla wszystkich klas generuj�cych "raporty dockind".
 *  
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public abstract class AbstractDockindReport extends ReportGenerator
{
     private static final Logger log = LoggerFactory.getLogger(AbstractDockindReport.class);
     protected StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
     protected DockindReportCriteria dockindCriteria;
     protected Report report;
     protected Date dateFrom;
     protected Date dateTo;
     
     public AbstractDockindReport(ReportCriteria criteria)
     {
         super(criteria);
         this.dockindCriteria = (DockindReportCriteria) criteria;
         this.report = dockindCriteria.getReport();
     }

     public abstract String getGeneratorId();

     public String getReportTitle()
     {
         return dockindCriteria.getReport().getName();
     }

     public abstract String getReportVersion();
     
     /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci HTML */
     public static String getHtmlXslResource()
     {
         return "pl/compan/docusafe/web/reports/dockind/dockind-report-html";
     }
     
     /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci PDF */
     public static String getFoXslResource()
     {
         return "pl/compan/docusafe/web/reports/dockind/dockind-report-fo";
     }
     
     /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci CSV */
     public static String getCsvXslResource()
     {
         return "pl/compan/docusafe/web/reports/dockind/dockind-report-csv";
     }
     
     /** Przekazuje zapytanie SQL generuj�ce z�dany raport */
     protected abstract StringBuilder getSqlStatement(Criterion colCriterion) throws EdmException;
     
     /** Dodaje do zbioru kolumn raportu kolumny specyficzne dla tego raportu. */
     protected abstract void addSpecificColumns(List<ColumnInfo> columns, Element elHeader, Criterion colCriterion) throws EdmException;
     
   
     
     
     public void createReport() throws EdmException
     {
         setReport(dockindCriteria.getReport());         
         PreparedStatement ps = null;
         try
         {
             DSApi.openAdmin();
             
             // wersja raportu
             write(DocumentHelper.createElement("version").addText(getReportVersion()));

             // generowanie elementu z informacjami g��wnymi
             Element elInfo = DocumentHelper.createElement("info");
             elInfo.addAttribute("name", getReportTitle());
             String orientation = getReport().getProperty(Report.PDF_ORIENTATION);
             elInfo.addAttribute("orientation", orientation == null ? Report.VERTICAL_ORIENTATION : orientation);
             write(elInfo);
             
             // generowanie sekcji opisuj�cej kryteria tworzenia raportu
             Element elCriteria = DocumentHelper.createElement("criteria");           
             for (Criterion criterion : getReport().getCriteria().values())
             {
                 ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
                 if (criterium == null)
                     continue;
                 if (criterium.getProperty()=="days")
                	 continue;
                 
                 Element el = null;                
                 if (criterium instanceof ReportCriteria.Range)
                 {
                     el = DocumentHelper.createElement("range");
                     el.addAttribute("property", criterion.getName());
//                     System.out.println(criterium.getProperty());
//                     System.out.println(((ReportCriteria.Range) criterium).getStart());
//                     System.out.println(((ReportCriteria.Range) criterium).getEnd());
                     if (criterium.getProperty().equals("date"))
                     {
                         String start = (String) ((ReportCriteria.Range) criterium).getStart();
                         String end = (String) ((ReportCriteria.Range) criterium).getEnd();
                         
                         if (start != null)
                         {
                             dateFrom = DateUtils.parseJsDate(start);
                             el.addAttribute("start", start);
                         }
                         if (end != null)
                         {
                             dateTo = DateUtils.parseJsDate(end);
                             el.addAttribute("end", end);
                         }   
                     }
                    
                 }
                 else if (criterium instanceof ReportCriteria.ValueEnumeration)
                 {
                     el = DocumentHelper.createElement("enumeration");
                     el.addAttribute("property", criterion.getName());
                     List<Object> values = getEnumerationValues(criterion);
                     for (Object value : values)
                     {
                         Element val = DocumentHelper.createElement("value");
                         val.addAttribute("title", value.toString());
                         el.add(val);
                     }
                 }
                 else
                 {
                     throw new IllegalArgumentException("Nieznany typ kryterium: "+criterium);
                 }                
                 if (el != null)                                     
                     elCriteria.add(el);                 
             }            
             write(elCriteria);
             
                         
             // GENEROWANIE NAG��WKA TABELKI
             // kryteria wierszowe
             List<ColumnInfo> columns = new ArrayList<ColumnInfo>();
             Element elHeader = DocumentHelper.createElement("header");
             for (String row : getReport().getPropertyArray(Report.ROW_PARTITION))
             {
                 Element el = DocumentHelper.createElement("column");
                 el.addAttribute("name", getReport().getCriterion(row).getName());
                 elHeader.add(el);
                 columns.add(new ColumnInfo(getColumnType(getReport().getCriterion(row), dockindCriteria.getDocumentKind()), getReport().getCriterion(row).getFieldCn(), null, false, null));
             }
             // kryterium kolumnowe - zak�adam, �e mo�e by� tylko jedno
             Criterion colCriterion = getReport().getProperty(Report.COLUMN_PARTITION) != null ? getReport().getCriterion(getReport().getProperty(Report.COLUMN_PARTITION)) : null;
             addSpecificColumns(columns, elHeader, colCriterion); 
             write(elHeader);
             
             write("<results>");
             // GENEROWANIE TRE�CI G��WNEJ TABELKI
             // tworzenie odpowiedniego zapytania SQL
             StringBuilder sql = getSqlStatement(colCriterion);
             
             ps = DSApi.context().prepareStatement(sql.toString());
             // ustalanie parametr�w zapytania
             int count = 0;       
             for (Criterion criterion : getReport().getCriteria().values())
             {
                 ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
                 if (criterium == null)
                     continue;
                 
                 if (criterium instanceof ReportCriteria.Range)
                 {
                     if (criterium.getProperty().equals("date"))
                     {
                         if (dateFrom != null ) {
                             ps.setDate(++count, new java.sql.Date(dateFrom.getTime()));
                         }
                         if (dateTo != null) {
                             ps.setDate(++count, new java.sql.Date(dateTo.getTime()));
                             //log.info("set[" + count + "] = " + dateTo);
                         }
                     }
                 }
                 else if (criterium instanceof ReportCriteria.ValueEnumeration)
                 {
                     Object[] keys = getEnumerationKeys(criterion);                    
                     if (keys != null && keys.length > 0)
                     {
                         String columnType = getColumnType(criterion, dockindCriteria.getDocumentKind());
                         for (Object key : keys)
                         {     
                             
                             if (Field.STRING.equals(columnType) || "division".equals(columnType) || "user".equals(columnType)) {
                                 ps.setString(++count, key.toString());
                                 //log.info("set[" + count + "] = " + key.toString());
                             } else {
                                 ps.setInt(++count, (Integer) key);
                                 //log.info("set[" + count + "] = " + key);
                             }
                         }
                     }
                 }
             }
             // wykonanie zapytania
             ResultSet rs = ps.executeQuery();
             String firstColumnStyle = getReport().getProperty(Report.FIRST_COLUMN_STYLE);
             String secondColumnStyle = getReport().getProperty(Report.SECOND_COLUMN_STYLE);            
             String firstColumnValue = null;
             String secondColumnValue = null;
             int[] partSum = new int[columns.size()];  // sumy cz�ciowe
             int[] sum = new int[columns.size()];      // sumy z ca�ych kolumn
             
             // pomocnicze rzeczy dla link�w do szczeg�owego widoku
             String[] rows = getReport().getPropertyArray(Report.ROW_PARTITION);
             List<String> detailsCols = null;
             if (getReport().getPropertyArray(Report.DETAILS_COLS) != null)            
                 detailsCols = Arrays.asList(getReport().getPropertyArray(Report.DETAILS_COLS));             
             
             // umieszczanie wynik�w w tabelce
             while (rs.next())
             {
                 Element element = DocumentHelper.createElement("row");                
                 count = 0;
                 String link = dockindCriteria.getContextPath()+"/reports/dockind/documents-list.action?documentKindCn="+dockindCriteria.getDocumentKind().getCn()+"&reportCn="+getReport().getCn();
                 for (ColumnInfo column : columns)
                 {                    
                     count++;
                     Element e = DocumentHelper.createElement("cell");
                     String value = getSqlValue(column, rs, count);
                     if (column.getValueSuffix() != null)
                         value += column.getValueSuffix();
                     String currentLink = null;
                     
                     // gdy w��czony szczeg�owy podgl�d i odpowiednia kolumna akurat, to zamiast zwyk�ej warto�ci tworz� link
                     if (getReport().getPropertyArray(Report.DETAILS_VIEW).length > 0 && count <= getReport().getPropertyArray(Report.ROW_PARTITION).length)
                     {
                         Criterion criterion = getReport().getCriterion(rows[count-1]);
                         link = link + "&criteria."+criterion.getType()+"_"+criterion.getFieldCn()+"="+getSqlKey(column,rs,count);
                         
                         if (detailsCols != null && detailsCols.contains(rows[count-1]))
                             currentLink = link;
                     }  
                     else if (getReport().getProperty(Report.DOCUMENT_LINK_COL) != null && Integer.valueOf(getReport().getProperty(Report.DOCUMENT_LINK_COL)).intValue()+getReport().getPropertyArray(Report.ROW_PARTITION).length == count)
                     {                         
                         currentLink = dockindCriteria.getContextPath()+"/repository/edit-document.action?id="+value;
                     }
                     
                     if (getReport().propertyOn(Report.ROW_SUMMARY))
                     {
                         // akcje zwi�zane z obecno�ci� w raporcie "wierszy sumuj�cych"
                         if (count == 1 && firstColumnValue != null && !value.equals(firstColumnValue))
                         {
                             addSummaryRow(getReport(), columns, partSum, false);
                         }
                         if (columns.get(count-1).isCountColumn())
                         {
                             sum[count-1] += Integer.valueOf(value);
                             partSum[count-1] += Integer.valueOf(value);
                         }
                     }
                     
                     if (count == 1 && firstColumnStyle != null)
                     {
                         // specjalne traktowanie pierwszej kolumny
                         if (!value.equals(firstColumnValue))
                         {                            
                             firstColumnValue = value;
                             secondColumnValue = null;
                             if (Report.STYLE_SPECIAL_ROW.equals(firstColumnStyle))
                             {
                                 Element ne = DocumentHelper.createElement("row");  
                                 ne.addAttribute("single-value", value);
                                 ne.addAttribute("colspan", String.valueOf(columns.size()-1));
                                 if (currentLink != null)
                                     ne.addAttribute("link", currentLink);
                                 write(ne);
                                 e.addAttribute("value", "");                                  
                             }
                             else
                             {
                                 e.addAttribute("value", value);
                                 if (currentLink != null)
                                     e.addAttribute("link", currentLink);
                             }
                         }
                         else
                             e.addAttribute("value", ""); 
                     }
                     else if (count == 2 && secondColumnStyle != null)
                     {
                         // specjalne traktowanie drugiej kolumny
                         if (!value.equals(secondColumnValue))
                         {
                             secondColumnValue = value;
                             e.addAttribute("value", value); 
                             if (currentLink != null)
                                 e.addAttribute("link", currentLink);
                         }
                         else
                             e.addAttribute("value", ""); 
                     }
                     else
                     {
                         // warto�ci bez specjalnego traktowania
                         e.addAttribute("value", value); 
                         if (currentLink != null)
                             e.addAttribute("link", currentLink);
                         if (column.getAlign() != null)
                             e.addAttribute("align", column.getAlign());
                     }   
                     element.add(e);
                 }                
                 write(element);
             }
             
             rs.close();
             
             if (getReport().propertyOn(Report.ROW_SUMMARY))
             {
                 if (firstColumnValue != null)
                     addSummaryRow(getReport(), columns, partSum, false);
                 addSummaryRow(getReport(), columns, sum, true);
             }
             
             write("</results>");     
         }
         catch (IOException e)
         {
             throw new EdmException("B��d zapisu pliku tymczasowego", e);
         }
         catch (SQLException e)
         {
             throw new EdmSQLException(e);
         }
         catch (HibernateException e)
         {
             throw new EdmHibernateException(e);
         }
         finally
         {
             if (ps != null) {
            	 DSApi.context().closeStatement(ps);
             }
             DSApi._close();
         }
     }

	/**
      * Dodaje do generowanego pliku XML, wiersz zliczaj�cy warto�ci wcze�niejszych kolumny.
      * 
      * @param report  definicja raportu z dockinda
      * @param columns  lista kolumn w raporcie
      * @param sum  tablica z sumami warto�ci wcze�niejszych kolumn
      * @param fullSum  true => wiersz zliczaj�cy dla ca�ego raportu; 
      *                 false => wiersz zliczaj�cy dla ostatniej warto�ci g��wnego kryterium podzia�u
      * @throws IOException
      */
     private void addSummaryRow(Report report, List<ColumnInfo> columns, int[] sum, boolean fullSum) throws IOException
     {
         // dodajemy wiersz sumuj�cy
         Element nElem = DocumentHelper.createElement("row");  
         for (int i=1; i <= columns.size(); i++)
         {
             Element ne = DocumentHelper.createElement("cell");
             if (fullSum && i == 1)
             {
                 ne.addAttribute("value", sm.getString("PODSUMOWANIE"));
                 ne.addAttribute("bold", "true");
             }
             else if (!fullSum && i == report.getPropertyArray(Report.ROW_PARTITION).length)
             {
                 ne.addAttribute("value", sm.getString("OGOLEM"));
                 ne.addAttribute("bold", "true");
             }
             else
             {
                 ne.addAttribute("value", columns.get(i-1).isCountColumn() ? String.valueOf(sum[i-1]) : "");
                 ne.addAttribute("align", "center");
                 if (fullSum)
                     ne.addAttribute("bold", "true");
             }
             nElem.add(ne);
             sum[i-1] = 0;
         }
         write(nElem);
     }
     
     protected void addWhereClause(StringBuilder sql) throws EdmException
     {
         for (Criterion criterion : getReport().getCriteria().values())
         {
             ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
             if (criterium == null)
                 continue;
             
             if (criterium instanceof ReportCriteria.Range)
             {
                 if (criterium.getProperty().equals("date"))
                 {
                     if (dateFrom != null)
                         sql.append(" and (").append(getSqlColumn("standard","date")).append(">=?)");
                     if (dateTo != null)
                         sql.append(" and (").append(getSqlColumn("standard","date")).append("<=?)");
                 }
             }
             else if (criterium instanceof ReportCriteria.ValueEnumeration)
             {
                 Object[] keys = getEnumerationKeys(criterion);                    
                 if (keys != null && keys.length > 0)
                 {
                     String str = null;
                     for (int i=0; i < keys.length; i++)
                     {                            
                         str = (str == null ? "?" : str+",?");                            
                     }
                     sql.append(" and (").append(getSqlColumn(criterion.getType(), criterion.getFieldCn())).append(" in (").append(str).append("))");
                 }
             }
         }
         
     }
     
     private String getSqlColumn(String id, Report report) throws EdmException
     {
         Criterion criterion = report.getCriterion(id);
         if (criterion != null){   
             return getSqlColumn(criterion.getType(), criterion.getFieldCn());    
         }
         throw new IllegalStateException("Napotkano niepoprawn� definicj� kryterium raportu");
     }
     
     private String getAliasSqlColumn(String id, Report report) throws EdmException
     {
         Criterion criterion = report.getCriterion(id);
         if (criterion != null){   
             return getAliasSqlColumn(criterion.getType(), criterion.getFieldCn());    
         }
         throw new IllegalStateException("Napotkano niepoprawn� definicj� kryterium raportu");
     }
     
     protected abstract String getAliasSqlColumn(String fieldType, String fieldCn) throws EdmException;
     protected abstract String getSqlColumn(String fieldType, String fieldCn) throws EdmException;
     
     protected String getPartitionString(Report report) throws EdmException
     {
         String[] rowPartition = report.getPropertyArray(Report.ROW_PARTITION);
         String result = null;         
         for (String row : rowPartition)
             result = (result == null ? getSqlColumn(row, report) : result + "," + getSqlColumn(row, report)); 
         return result;
     }
     
     protected String getAliasPartitionString(Report report) throws EdmException{
    	 String[] rowPartition = report.getPropertyArray(Report.ROW_PARTITION);
         String result = null;
         Criterion criterion;
         
         for (String row : rowPartition){
        	 	criterion = report.getCriterion(row);
         		if(!criterion.getType().equals("abstract")){
         			result = (result == null ? getAliasSqlColumn(row, report) : result + "," + getAliasSqlColumn(row, report)); 
         		}
         }
         return result;
     }
     
     protected String getColumnCriterionString(Report report) throws EdmException
     {
         String col = report.getProperty(Report.COLUMN_PARTITION);
         String result = null;
         if (col != null)
         {
             String columnType = getColumnType(report.getCriterion(col), dockindCriteria.getDocumentKind());
             Object[] values = getEnumerationKeys(report.getCriterion(col));        
             if (values != null)
                 for (Object value : values)
                 {
                     String sql;
                     if (DSApi.getDialect() instanceof FirebirdDialect)
                         throw new EdmException("Ta funkcjonalno�� nie jest jeszcze zaimplementowana dla bazy Firebird");
                     else if (DSApi.getDialect() instanceof SQLServerDialect)
                          sql = "sum(case when "+getSqlColumn(col, report)+"="+
                         (Field.STRING.equals(columnType) || "division".equals(columnType) || "user".equals(columnType) ? "'"+value+"'" : value)+
                         " then 1 else 0 end)";
                     else if (DSApi.getDialect() instanceof Oracle10gDialect)
                         sql = "sum(If("+getSqlColumn(col, report)+"="+
                         (Field.STRING.equals(columnType) || "division".equals(columnType) || "user".equals(columnType) ? "'"+value+"'" : value)+
                         ",1,0))";
                     else
                         throw new EdmException("Ta funkcjonalno�� nie jest zaimplementowana dla u�ywanej bazy danych");
                     if (result == null)
                         result = sql + ", ";
                     else
                         result += sql + ", ";
                 }
         }
         return result;            
     }
     
     /**
      * Przekazuje warto�ci danego kryterium wyboru (zak�adam, �e jest to kryterium typu Enumeration).
      * @param criterion
      * @return
      */
     protected Object[] getEnumerationKeys(Criterion criterion)
     {
         ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
         if (criterium == null)
             return null;

         return ((ReportCriteria.ValueEnumeration) criterium).getValues();
     }
     
     /**
      * Przekazuje warto�ci danego kryterium wyboru (zak�adam, �e jest to kryterium typu Enumeration)
      * przekszta�cone do czytelnej dla u�ytkownika postaci.
      * @param criterion
      * @return
      */
     protected List<Object> getEnumerationValues(Criterion criterion) throws EdmException
     {
         ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
         if (criterium == null)
             return null;

         Object[] keys = ((ReportCriteria.ValueEnumeration) criterium).getValues();
         if (keys == null)
             return null;
         List<Object> values = new ArrayList<Object>();
         for (Object key : keys)
         {
             if ("standard".equals(criterion.getType()))
             {
                 if ("division".equals(criterion.getFieldCn()))
                     values.add(DSDivision.find(key.toString()).getName());
                 else if ("user".equals(criterion.getFieldCn()))
                     values.add(DSUser.findByUsername(key.toString()).asFirstnameLastname());
                 else
                     values.add(key);
             }
             else
                 values.add(dockindCriteria.getDocumentKind().getFieldByCn(criterion.getFieldCn()).getEnumItem((Integer) key).getTitle());                
         }
         return values;
     }
     
     /**
      * Przekazuje typ pola reprezentowanego przez dane kryterium raportu.
      */
     protected static String getColumnType(Criterion criterion, DocumentKind documentKind) throws EdmException
     {
         return getColumnType(criterion.getType(), criterion.getFieldCn(), documentKind);
     }
     
     /**
      * Przekazuje typ pola reprezentowanego przez dane kryterium raportu.
      */
     protected static String getColumnType(String type, String fieldCn, DocumentKind documentKind) throws EdmException
     {
         if ("standard".equals(type)){
        	 
             if ("date".equals(fieldCn))
                 return Field.DATE;
             else if ("division".equals(fieldCn))
                 return "division";
             else if ("user".equals(fieldCn))
                 return "user";
             else if ("description".equals(fieldCn))
                 return Field.STRING;
             else if ("id".equals(fieldCn))
                 return Field.LONG;
             else if ("officeNumber".equals(fieldCn))
                 return Field.INTEGER;
             else if ("days".equals(fieldCn))
                 return Field.INTEGER;
             else
                 return Field.STRING;
         }
         else if ("abstract".equals(type))
         {
        	 if ("diff".equals(fieldCn))
        		 return Field.INTEGER;
        	 else if ("diffUser".equals(fieldCn))
        		 return Field.INTEGER;
        	 else if ("diffUserHour".equals(fieldCn))
        		 return Field.INTEGER;
        	 else if ("avgTime".equals(fieldCn))
        		 return Field.LONG;
        	 else if ("avgTimeHour".equals(fieldCn))
        		 return Field.LONG;
        	 else
                 return Field.STRING;
	 
         }
        	 
         else 
             return documentKind.getFieldByCn(fieldCn).getType();
     }
     
     protected String getSqlKey(ColumnInfo column, ResultSet rs, int pos) throws SQLException, EdmException
     {
         if (Field.DATE.equals(column.getType()))
         {
             Date date = rs.getDate(pos);
             if (rs.wasNull())
                 return null;
             else
                 return DateUtils.formatJsDate(date);
         }
         else if (Field.INTEGER.equals(column.getType()) || Field.ENUM.equals(column.getType()) || Field.ENUM_REF.equals(column.getType()))
         {
             int i = rs.getInt(pos);
             if (rs.wasNull())
                 return null;
             else                          
                 return String.valueOf(i);
         }
         else if (Field.LONG.equals(column.getType()))
         {
             long l = rs.getLong(pos);
             if (rs.wasNull())
                 return null;
             else    
                 return String.valueOf(l);
         }
         else // STRING - w tym "division" i "user"
         {
             String s = rs.getString(pos);
             if (rs.wasNull())
                 return null;
             else
                 return s;
         }
     }
     
     /**
      * Pobiera warto�� kolumny z ResultSeta i odpowiednio go konwertuje w zale�no�ci od typu.
      */
     protected String getSqlValue(ColumnInfo column, ResultSet rs, int pos) throws SQLException, EdmException
     {
         if (Field.DATE.equals(column.getType()))
         {
             Date date = rs.getDate(pos);
             if (rs.wasNull())
                 return "BRAK";
             else
                 return DateUtils.formatJsDate(date);
         }
         else if (Field.INTEGER.equals(column.getType()))
             return String.valueOf(rs.getInt(pos));
         else if (Field.LONG.equals(column.getType()))
             return String.valueOf(rs.getLong(pos));
         else if (Field.ENUM.equals(column.getType()) || Field.ENUM_REF.equals(column.getType()))
         {
             int i = rs.getInt(pos);
             if (rs.wasNull())
                 return "BRAK";
             else
                 return dockindCriteria.getDocumentKind().getFieldByCn(column.getFieldCn()).getEnumItem(i).getTitle();
         }
         else if ("division".equals(column.getType()))
         {
             String guid = rs.getString(pos);
             if (rs.wasNull())
                 return "BRAK";
             else
                 return DSDivision.find(guid).getName();
         }
         else if ("user".equals(column.getType()))
         {
             String username = rs.getString(pos);
             if (rs.wasNull())
                 return "BRAK";
             else
             return DSUser.findByUsername(username).asFirstnameLastname();
         }
         else //if (Field.STRING.equals(columnType))
             return String.valueOf(rs.getString(pos));
         // TODO: w przyszlosci jak gdzies na raporcie bedzie kolumna typu CLASS, nalezy tu dodac odpowiedniego if-a
     }
     
     public class ColumnInfo
     {
         private String type;
         private String fieldCn;
         private String align;
         private boolean countColumn;
         private String valueSuffix;
         
         public ColumnInfo(String type, String fieldCn, String align, boolean countColumn, String valueSuffix)
         {
             this.type = type;
             this.fieldCn = fieldCn;
             this.align = align;
             this.countColumn = countColumn;
             this.valueSuffix = valueSuffix;
         }
         
         public String getAlign()
         {
             return align;
         }
         public String getFieldCn()
         {
             return fieldCn;
         }
         
         public String getType()
         {
             return type;
         }

         public boolean isCountColumn()
         {
             return countColumn;
         }

         public String getValueSuffix()
         {
             return valueSuffix;
         }                
     }
     
     protected static String parse(String fieldKey, Object value) throws EdmException
     {
         if (value instanceof Object[])
         {
             if (((Object[]) value).length > 0)
                 return ((Object[]) value)[0].toString();
             else
                 return null;
         }
         else
             return value.toString();
     }
     
     /**
      * Przekazuje warto�� danego pola czytaln� dla u�ytkownika (na podstawie warto�ci-klucza tego pola).
      */
     protected static String getValue(String type, String fieldCn, String value, DocumentKind documentKind) throws EdmException
     {
         if ("null".equals(value))
             return "BRAK";
         if ("standard".equals(type))
         {
             if ("user".equals(fieldCn))
                 return DSUser.findByUsername(value).asFirstnameLastname();
             else if ("division".equals(fieldCn))
                 return DSDivision.safeGetName(value);
             else if ("date".equals(fieldCn))
                 return value;
         }
         else
         {
             Field field = documentKind.getFieldByCn(fieldCn);
             if (field instanceof EnumerationField || Field.ENUM_REF.equals(field.getType()))
                 return field.getEnumItem(Integer.valueOf(value)).getTitle();
             else
                 return value;
             // TODO: w przyszlosci jak gdzies na szczegolach wynikow bedzie kolumna typu CLASS, nalezy tu dodac odpowiedniego if-a
         }
         return null;
     }
     
     /**
      * Wyszukiwanie dokument�w w raporcie wg. podanych kryteri�w. 
      * 
      * UWAGA: w wynikach dokumenty mog� sie powtarza� np. w raporcie �redniego przebywania na li�cie,
      * gdy jeden u�ytkownik wielokrotnie pracowa� nad jednym dokumentem.
      * 
      * @param reportCn  'cn' raportu, dla kt�rego robimy wyszukiwanie
      * @param handler  obiekt dostarczaj�cy specyficzne dla raportu, dla kt�rego robimy to wyszukiwanie  
      * @param criteria  kryteria wyszukiwania w postaci (pole,pojedyncza wartosc pola)
      * @param documentKind  rodzaj dokumentu, kt�rego dotyczy ten raport
      * @param criteriaInfo  mapa, do kt�rej zostan� zapisane kryteria wyszukiwania w czytelnej dla u�ytkownika postaci
      * @return
      * @throws EdmException
      */
     public static List<Map<String,String>> searchDocuments(String reportCn, SearchDocumentsHandler handler, Map<String,Object> criteria, DocumentKind documentKind, Map<String,String> criteriaInfo) throws EdmException
     {           
         Report report = documentKind.getReport(reportCn);        
         
         StringBuilder sql = new StringBuilder("select ").append(handler.selectDistinct() ? "distinct " : "").append(handler.getSqlDocumentId()).append(",")
         .append(handler.getSqlColumn("standard", "date", documentKind)).append(", ")
         // gdy w raporcie nie ma podzia�u wzgl�dem u�ytkownika, to tutaj zamiast tej warto�ci daje ''
         // ze wzgl�du na raport dzia�owy z listy zada�, kt�ry wypisa�by wi�cej ni� jedn� linie, gdy
         // nieprzyj�ta dekretacja na dzia� w kt�rym wi�cej ni� jedna osoba
         .append(report.getCriterion("standard", "user") != null ? handler.getSqlColumn("standard", "user", documentKind) : "''").append(", ")
         .append(handler.getSqlColumn("standard", "division", documentKind)).append(", ")
         .append(report.getCriterion("standard", "result") != null ? handler.getSqlColumn("standard", "result", documentKind) : "''")
         .append(" from ");
         sql.append(handler.getSqlFromClause(documentKind,report));
         
         for (String fieldKey : criteria.keySet())
         {
             int dotPos = fieldKey.indexOf("_");
             String type = fieldKey.substring(0, dotPos);
             String fieldCn = fieldKey.substring(dotPos+1);
             String value = parse(fieldKey, criteria.get(fieldKey));
             Criterion criterion = report.getCriterion(type, fieldCn);
             criteriaInfo.put(criterion.getName(), getValue(type, fieldCn, value, documentKind));
             if (value == null)
                 continue;
                         
             sql.append(" and ("+handler.getSqlColumn(type, fieldCn, documentKind)+(value.equals("null") ? " is null" : "=?")+")");
         }
          
         PreparedStatement ps = null;
         try
         {
             ps = DSApi.context().prepareStatement(sql.toString());
             
             int count = 0;
             for (String fieldKey : criteria.keySet())
             {
                 int dotPos = fieldKey.indexOf("_");
                 String type = fieldKey.substring(0, dotPos);
                 String fieldCn = fieldKey.substring(dotPos+1);
                 String value = parse(fieldKey, criteria.get(fieldKey));
                 if (value == null)
                     continue;                                
                 
                 if (Field.INTEGER.equals(getColumnType(type, fieldCn, documentKind)))
                 {
                     if (!value.equals("null"))
                     ps.setInt(++count, Integer.valueOf(value));
                 }
                 else if (Field.DATE.equals(getColumnType(type, fieldCn, documentKind)))
                 {
                     if (!value.equals("null"))
                         ps.setDate(++count, new java.sql.Date(DateUtils.parseJsDate(value).getTime()));
                 }
                 else //if (Field.STRING.equals(getColumnType(type, fieldCn, documentKind)))
                 {
                     if (!value.equals("null"))
                         ps.setString(++count, value);
                 }
             }
             
             ResultSet rs = ps.executeQuery();
             List<Map<String,String>> docs = new ArrayList<Map<String,String>>();
             while (rs.next())
             {
                 Long id = rs.getLong(1);
                 Map<String,String> bean = new LinkedHashMap<String, String>();
                 bean.put("id", id.toString());
                 bean.put("date", DateUtils.formatJsDate(rs.getDate(2)));
                 bean.put("user", rs.getString(3));
                 bean.put("division", rs.getString(4));
                 if(!rs.getString(5).equals(""))
                	 bean.put("result", String.valueOf(rs.getInt(5))+handler.getResultValueSuffix());
                 docs.add(bean);
             }
             return docs;
         }
         catch (SQLException e)
         {
             throw new EdmSQLException(e);
         }
         finally
         {
             DSApi.context().closeStatement(ps);
         }        
     }


	public Report getReport() {
		return report;
	}
	
	public void setReport(Report report) {
		this.report =  report;
	}

	/**
      * Interfejs dla obiekt�w dostarczaj�ych potrzebne informacje dla wyszukiwania dokument�w
      * na potrzeby szczeg��w danego raportu.
      */
     public interface SearchDocumentsHandler
     {
         /**
          * Przekazuje nazw� kolumny-kryterium w postaci SQL.
          */
         String getSqlColumn(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException;
         
         /**
          * Przekazuje kawa�ek SQL oznaczaj�cy identyfikator dokumentu. 
          */
         String getSqlDocumentId();
         
         /**
          * Przekazuje kawa�ek SQL okre�laj�cy, z kt�rych tabel zapytanie korzysta razem z niezb�dnymi
          * warunkami WHERE do wygenerowanie tego raportu.
          */
         String getSqlFromClause(DocumentKind documentKind, Report report);
         
         /**
          * Przekazuje nazw� kolumny z wynikiem na szczeg�ach raportu.
          */
         String getResultColumnNameOnDetails(Report report);
         
         /**
          * Przekazuje napis (suffix), kt�ry ma by� dodany do wszystkich warto�ci w kolumnie pokazuj�cej
          * wynik na szczeg�ach raportu.
          */
         String getResultValueSuffix();
         
         /**
          * Okre�la czy u�y� znacznika "distinct" w zapytaniu SQL.
          */
         boolean selectDistinct();
     }
}
