package pl.compan.docusafe.service.reports.dockind;

import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.service.reports.ReportCriteria;

public class DockindReportCriteria extends ReportCriteria
{
    private DocumentKind documentKind;
    private Report report;
    /** kontekst aplikacji - u�ywany do utworzenia poprawnych link�w na raporcie HTML */ 
    private String contextPath;
    
    public DocumentKind getDocumentKind()
    {
        return documentKind;
    }
    
    public void setDocumentKind(DocumentKind documentKind)
    {
        this.documentKind = documentKind;
    }
    
    public Report getReport()
    {
        return report;
    }
    
    public void setReport(Report report)
    {
        this.report = report;
    }

    public String getContextPath()
    {
        return contextPath;
    }

    public void setContextPath(String contextPath)
    {
        this.contextPath = contextPath;
    }  
}
