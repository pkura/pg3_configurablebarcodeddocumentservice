package pl.compan.docusafe.service.reports.dockind;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.service.reports.ReportCriteria;

/**
 * Zestawienie dokument�w na li�cie zada�.
 * 
 * Standardowe kryteria okre�laj�:
 * <ul>
 * <li> date - data otrzymania dokumentu na list� zada� (dla podtypu ALL i UNREALIZED) lub dat� utworzenia
 * dokumentu w systemie Docusafe (dla podtypu SINCE_CREATED),
 * <li> division - dzia�, w kt�rym obecnie znajduje si� dokument (nale�y pami�ta� o sytuacji, gdy jeden u�ytkownik
 * znajduje si� w kilku dzia�ach, w�wczas ka�de zadanie z jego listy nale�y tylko do jednego dzia�u - jedno
 * pismo nie mo�e znajdywa� si� w 2 dzia�ach jednocze�nie; ponadto jak pismo dekretowane jest na dzia�, a nie
 * bezpo�rednio do uzytkownika, to taki dokument na pocz�tku znajduje si� np. u 5 u�ytkownik�w, ale do zestawienia
 * danego dzia�u oczywi�cie ten dokument liczymy nie jako 5, ale jako 1!),
 * <li> user - u�ytkownik, na kt�rego li�cie obecnie znajduje si� dokument.
 * </ul> 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class DocumentsReport extends AbstractDocumentsListReport
{
    public static final String REPORT_ID = "documents";
    public static final String REPORT_VERSION = "1.0";
    
    // podtypy raport�w
    /** wszystkie dokumenty z listy zada� ('date' oznacza dat� otrzymania na list�) */
    public static final String TYPE_ALL = "all"; // domy�lny
  
    
    public DocumentsReport(ReportCriteria criteria)
    {
        super(criteria);
    }
    
    public String getGeneratorId()
    {
        return REPORT_ID;
    } 
     
    public String getReportVersion()
    {
        return REPORT_VERSION;
    }
    
    public StringBuilder getSqlStatement(Criterion colCriterion) throws EdmException
    {
        String partitionString = getPartitionString(report);
        String aliasPartitionString = getAliasPartitionString(report);
        StringBuilder sql = new StringBuilder("select distinct ");
        boolean needsComma = partitionString != null;
        
        if (partitionString != null)
            sql.append(partitionString);
        for (String col : report.getPropertyArray(Report.SPECIFIC_COLUMNS))
        {
            int dotPos = col.indexOf(".");
            String type = col.substring(0, dotPos);
            String fieldCn = col.substring(dotPos+1);
            
            if (needsComma)
                sql.append(", ");
            else
                needsComma = true;
            
            sql.append(getSqlColumn(type, fieldCn));
        }
        sql.append(" from ");
        sql.append(dockindCriteria.getDocumentKind().getTablename()).append(" dt, dsw_task_history_entry t, ds_document d").append(" where (dt.document_id=t.documentId) and (t.documentId=d.id)");
       
        addWhereClause(sql);

        if (partitionString != null)
            sql.append(" order by ").append(aliasPartitionString);
        
        return sql;        
    }
    
    protected String getSqlColumn(String fieldType, String fieldCn)throws EdmException
    {
    	if ("standard".equals(fieldType))
        {
            if ("date".equals(fieldCn))
                return "t.receiveDate";
            else if ("division".equals(fieldCn))
                return "t.divisionGuid";
            else if ("user".equals(fieldCn))
                return "t.username";
            else if ("id".equals(fieldCn))
                return "t.documentId";
            else if ("description".equals(fieldCn))
                return "d.description";
        }
        else if ("dockind".equals(fieldType))
        {
            return "dt."+(dockindCriteria.getDocumentKind().getFieldByCn(fieldCn).getColumn());
        }
        else if ("abstract".equals(fieldType))// ten typ sluzy dio zwracania jakichs kolumn wirtualnych tworzonych za pomoca zapytania
        {									  // alias musi byc taki sam jak nazwa kolumny z xmla
        	if("diff".equals(fieldCn))
        		return "DateDiff(day,t.documentCdate,getDate()) as diff";
        	else if("diffUser".equals(fieldCn))
        		return "DateDiff(day,t.rcvDate,getDate()) as diffUser";
        	else if("diffUserHour".equals(fieldCn))
        		return "DateDiff(hour,t.rcvDate,getDate()) as diffUserHour";
        }
        throw new IllegalStateException("Napotkano niepoprawn� definicj� kryterium raportu");
    }
    protected String getAliasSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        if ("standard".equals(fieldType))
        {
        	 if ("date".equals(fieldCn))
                 return "t.receiveDate";
             else if ("division".equals(fieldCn))
                 return "t.divisionGuid";
             else if ("user".equals(fieldCn))
                 return "t.username";
             else if ("id".equals(fieldCn))
                 return "t.documentId";
             else if ("description".equals(fieldCn))
                 return "d.description";
                
        }
        else if ("dockind".equals(fieldType))
        {
            return "dt."+(dockindCriteria.getDocumentKind().getFieldByCn(fieldCn).getColumn());
        }
        else if ("abstract".equals(fieldType))// ten typ sluzy dio zwracania jakichs kolumn wirtualnych tworzonych za pomoca zapytania
        {									  // alias musi byc taki sam jak nazwa kolumny z xmla
        	return fieldCn;
        }
        throw new IllegalStateException("Napotkano niepoprawn� definicj� kryterium raportu");
    }
}
