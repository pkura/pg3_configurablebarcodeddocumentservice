package pl.compan.docusafe.service.reports.dockind;

import org.hibernate.dialect.FirebirdDialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.SQLServerDialect;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.util.StringManager;

/**
 * Raport �redniego czasu przebywania dokumentu na li�cie zada� (do raportu brane s� zar�wno zadania ju� 
 * zako�czone jak i te aktualnie znajduj�ce si� na li�cie).
 * 
 * Standardowe kryteria okre�laj�:
 * <ul>
 * <li> date - data otrzymania dokumentu na list� zada�,
 * <li> division - dzia�, w kt�rym znajdowa�y si� dokumenty na li�cie,
 * <li> user - u�ytkownik, na kt�rego li�cie znajdowa�y si� dokumenty.
 * </ul>
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class AverageTimeOnTasklistReport extends AbstractDocumentsCountReport
{
    public static final String REPORT_ID = "average-task-time";
    public static final String REPORT_VERSION = "1.0";
     
    
    public AverageTimeOnTasklistReport(ReportCriteria criteria)
    {
        super(criteria);
    }
    
    public String getGeneratorId()
    {
        return REPORT_ID;
    } 
     
    public String getReportVersion()
    {
        return REPORT_VERSION;
    }
    
    
    /** 
     * Zwraca klas� dostarczaj�c� specyficzne informacje do wyszukiwania dokument�w na potrzeby
     * szczeg��w raportu.
     */
    public static SearchDocumentsHandler getSDHandler()
    {
        return new AverageTimeReportSDHandler();
    }
    
    @SuppressWarnings("deprecation")
	public StringBuilder getSqlStatement(Criterion colCriterion) throws EdmException
    {
        String partitionString = getPartitionString(report);
        StringBuilder sql = new StringBuilder("select ");
        if (partitionString != null)
            sql.append(partitionString).append(", ");
        String colCriterionString = colCriterion != null ? getColumnCriterionString(report) : null;
        if (colCriterionString != null)
            sql.append(colCriterionString);
        if (DSApi.getDialect() instanceof SQLServerDialect){
            //sql.append("avg(datediff(hour,t.receiveTime,case when t.finishTime is null then getdate() else t.finishTime end)), ");
            sql.append(" avg(datediff(day,t.receiveTime,case when t.finishTime is null then getdate() else t.finishTime end)) from ");
        }
        else if (DSApi.getDialect() instanceof FirebirdDialect)
            throw new EdmException("Ten raport nie jest jeszcze zaimplementowany dla bazy Firebird");
        else if (DSApi.isOracleServer())
            throw new EdmException("Ten raport nie jest jeszcze zaimplementowany dla bazy Oracle");
        else
            throw new EdmException("Ten raport nie jest zaimplementowany dla u�ywanej bazy danych");
        
        
        //sql.append(" from ");
        sql.append(dockindCriteria.getDocumentKind().getTablename()).append(" dt,dsw_task_history_entry t").append(" where (dt.document_id=t.documentId)");
        sql.append(" and (t.receiveDate is not null)");
        // kryteria raportu w WHERE
        addWhereClause(sql);

        if (partitionString != null){
            sql.append(" group by ").append(getAliasPartitionString(report));
            sql.append(" order by ").append(getAliasPartitionString(report).split(",")[0]);
        }
        return sql;
    }
    
    protected String getSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected String getAliasSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
    	if("abstract".equals(fieldType))
    	{
    		return fieldCn;
    	}
    	else
    		return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected static String getSqlColumnStatic(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
    {
        if ("standard".equals(fieldType))
        {
            if ("date".equals(fieldCn))
                return "t.receiveDate";
            else if ("division".equals(fieldCn))
                return "t.divisionGuid";
            else if ("user".equals(fieldCn))
                return "t.username";
            else if ("result".equals(fieldCn))
            {
                if (DSApi.getDialect() instanceof SQLServerDialect)
                    return "datediff(hour,t.receiveTime,case when t.finishTime is null then getdate() else t.finishTime end)";
                else
                    throw new EdmException("Ten raport nie jest zaimplementowany dla u�ywanej bazy danych");
            }
        }
        else if ("dockind".equals(fieldType))
        {
            return "dt."+(documentKind.getFieldByCn(fieldCn).getColumn());
        }
        else if ("abstract".equals(fieldType))
        {
        	if ("avgTime".equals(fieldCn))
            {
                if (DSApi.getDialect() instanceof SQLServerDialect)
                    return "avg(datediff(day,t.receiveTime,case when t.finishTime is null then getdate() else t.finishTime end)) as avgTime";
                else
                    throw new EdmException("Ten raport nie jest zaimplementowany dla u�ywanej bazy danych");
            }
        	else if ("avgTimeHour".equals(fieldCn))
            {
                if (DSApi.getDialect() instanceof SQLServerDialect)
                    return "avg(datediff(hour,t.receiveTime,case when t.finishTime is null then getdate() else t.finishTime end)) as avgTimeHour";
                else
                    throw new EdmException("Ten raport nie jest zaimplementowany dla u�ywanej bazy danych");
            }
        }
        throw new IllegalStateException("Napotkano niepoprawn� definicj� kryterium raportu");
    }
    
    protected String resultColumnName()
    {
        return sm.getString("SredniCzasPrzebywaniaDokumentuNaLisciaZadan");
    }
    
    protected String resultValueSuffix()
    {
        return " ";
    }
    
    public static class AverageTimeReportSDHandler implements SearchDocumentsHandler 
    { 
        public String getSqlColumn(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
        {
            return getSqlColumnStatic(fieldType, fieldCn, documentKind);
        }  
        
        public String getSqlDocumentId()
        {
            return "dt.document_id";
        }
        
        public String getSqlFromClause(DocumentKind documentKind, Report report)
        {
            StringBuilder fromSql = new StringBuilder(documentKind.getTablename());
            fromSql.append(" dt,dsw_task_history_entry t").append(" where (dt.document_id=t.documentId)");
            fromSql.append(" and (t.receiveDate is not null)");
            return fromSql.toString();
        }
        
        public String resultColumnName()
        {
        	StringManager sm = GlobalPreferences.loadPropertiesFile(AbstractDockindReport.class.getPackage().getName(),null);
            return sm.getString("SredniCzasPrzebywaniaDokumentuNaLisciaZadan");
        }
        
        public String getResultColumnNameOnDetails(Report report)
        {            
            StringManager sm = GlobalPreferences.loadPropertiesFile(AbstractDockindReport.class.getPackage().getName(),null);
            return sm.getString("NaLiscieZadanPrzez");
        }
        
        public String getResultValueSuffix()
        {
            return " ";
        }
        
        public boolean selectDistinct()
        {
            return false;
        }
    }
}
