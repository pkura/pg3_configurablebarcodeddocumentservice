package pl.compan.docusafe.service.reports.dockind;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FileUtils;

public class RockwellReport extends AbstractDocumentsListReport
{
	


	
	public static final String REPORT_ID = "rockwell";
    public static final String REPORT_VERSION = "1.0";

    protected Date invoiceDateFrom;
    protected Date invoiceDateTo;
    String document_id = "ID";
	String document_date = "Document date";
	String document_type = "Document type";
	Long[] documentIds;
	
	
    public RockwellReport(ReportCriteria criteria)
    {
        super(criteria);
    }

    public String getGeneratorId()
    {
        return REPORT_ID;
    }

    public String getReportVersion()
    {
        return REPORT_VERSION;
    }
    
    /** 
     * Zwraca klas� dostarczaj�c� specyficzne informacje do wyszukiwania dokument�w na potrzeby
     * szczeg��w raportu.
     */
    public static SearchDocumentsHandler getSDHandler()
    {
        return new AssignedReportSDHandler();
    }
    
    protected String getSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected String getAliasSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected static String getSqlColumnStatic(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
    {
    	if("date".equals(fieldCn))
    		return "document_date";
    	if("invoiceDate".equals(fieldCn))
    		return "invoice_date";
    	if("BU".equals(fieldCn))
    		return "business_unit";
    	if("DOCUMENT_STATUS".equals(fieldCn)) 
    		return "status";
        throw new IllegalStateException("Napotkano niepoprawn� definicj� kryterium raportu");
    }

    protected StringBuilder getSqlStatement(Criterion colCriterion) throws EdmException
    {
        StringBuilder sql = new StringBuilder("select * ");
        sql.append("from ap_report ");
        addWhereClause(sql);
        //System.out.println(sql.toString());
        return sql;
    }

    protected String resultColumnName()
    {
        return sm.getString("LiczbaDokumentow");
    }
    
    protected String resultValueSuffix()
    {
        return null;
    }
    
    public static class AssignedReportSDHandler implements SearchDocumentsHandler 
    { 
        public String getSqlColumn(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
        {
            return getSqlColumnStatic(fieldType, fieldCn, documentKind);
        }            
        
        public String getSqlDocumentId()
        {
            return "dt.document_id";
        }
        
        public String getSqlFromClause(DocumentKind documentKind, Report report)
        {
            StringBuilder sql = new StringBuilder(); 
           /* sql.append(" from dsg_invoice_ic ic");
            sql.append(" join ds_document_acceptance acc on acc.document_id = ic.document_id");
            sql.append(" join ds_user us on us.name = acc.username");*/
            return sql.toString();
        }
        
        public String getResultColumnNameOnDetails(Report report)
        {
            // dla tego raportu nie ma kolumny z wynikiem na szczeg�ach
            return null;
        }
        
        public String getResultValueSuffix()
        {
            return "";
        }
        
        public boolean selectDistinct()
        {
            return false;
        }
    }
    
    private void createMetaInfo() throws IOException, EdmException {
    	 // wersja raportu
        write(DocumentHelper.createElement("version").addText(getReportVersion()));

        // generowanie elementu z informacjami g��wnymi
        Element elInfo = DocumentHelper.createElement("info");
        elInfo.addAttribute("name", getReportTitle());
        String orientation = getReport().getProperty(Report.PDF_ORIENTATION);
        elInfo.addAttribute("orientation", orientation == null ? Report.VERTICAL_ORIENTATION : orientation);
        write(elInfo);
        
        Element elCriteria = DocumentHelper.createElement("criteria");           
        for (Criterion criterion : getReport().getCriteria().values())
        {
            ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
            if (criterium == null)
                continue;
            if (criterium.getProperty()=="days")
           	 continue;
            
            Element el = null;                
            if (criterium instanceof ReportCriteria.Range)
            {
                el = DocumentHelper.createElement("range");
                el.addAttribute("property", criterion.getName());
               
                if (criterium.getProperty().equals("date"))
                {
                    String start = (String) ((ReportCriteria.Range) criterium).getStart();
                    String end = (String) ((ReportCriteria.Range) criterium).getEnd();
                    
                    if (start != null)
                    {
                        dateFrom = DateUtils.parseJsDate(start);
                        el.addAttribute("start", start);
                    }
                    if (end != null)
                    {
                        dateTo = DateUtils.parseJsDate(end);
                        el.addAttribute("end", end);
                    }   
                }
                if (criterium.getProperty().equals("invoiceDate"))
                {
                    String start = (String) ((ReportCriteria.Range) criterium).getStart();
                    String end = (String) ((ReportCriteria.Range) criterium).getEnd();
                    
                    if (start != null)
                    {
                        invoiceDateFrom = DateUtils.parseJsDate(start);
                        el.addAttribute("start", start);
                    }
                    if (end != null)
                    {
                        invoiceDateTo = DateUtils.parseJsDate(end);
                        el.addAttribute("end", end);
                    }   
                }
            }
            else if (criterium instanceof ReportCriteria.ValueEnumeration)
            {
                el = DocumentHelper.createElement("enumeration");
                el.addAttribute("property", criterion.getName());
                List<Object> values = getEnumerationValues(criterion);
                for (Object value : values)
                {
                    Element val = DocumentHelper.createElement("value");
                    val.addAttribute("title", value.toString());
                    el.add(val);
                }
            }
            else
            {
                throw new IllegalArgumentException("Nieznany typ kryterium: "+criterium);
            }                
            if (el != null)                                     
                elCriteria.add(el);                 
        }            
        write(elCriteria);
    }
    
    @Override
    public void createCSVReport() throws EdmException {    
        PreparedStatement ps = null;
        try
        {
            DSApi.openAdmin();
            
            //info i kryteria
            createMetaInfo();
            
            // GENEROWANIE TRE�CI G��WNEJ TABELKI
            // tworzenie odpowiedniego zapytania SQL
            StringBuilder sql = getSqlStatement(null);
                        
            
            
            ps = DSApi.context().prepareStatement(sql.toString());
            int count=0;
            for (Criterion criterion : getReport().getCriteria().values())
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
                if (criterium == null)
                    continue;
                
                
                if (criterium.getProperty().equals("date"))
                {
                    if (dateFrom != null )            
                        ps.setDate(++count, new java.sql.Date(dateFrom.getTime()));
                    if (dateTo != null)
                        ps.setDate(++count, new java.sql.Date(dateTo.getTime()));
                }
                if (criterium.getProperty().equals("invoiceDate"))
                {
                    if (invoiceDateFrom!= null )            
                        ps.setDate(++count, new java.sql.Date(invoiceDateFrom.getTime()));
                    if (invoiceDateTo != null)
                        ps.setDate(++count, new java.sql.Date(invoiceDateTo.getTime()));
                }
                else if (criterium.getProperty().equals("BU"))
                {
                    Object[] keys = getEnumerationKeys(criterion);                    
                    if (keys != null && keys.length > 0)
                    {
                        for (Object key : keys)
                        {     
                            ps.setString(++count, DocumentKind.findByCn(DocumentLogicLoader.ROCKWELL_KIND).getFieldByCn("BU").getEnumItem((Integer)key).getCn());
                            //System.out.println(DocumentKind.findByCn(DocumentKind.ROCKWELL_KIND).getFieldByCn("BU").getEnumItem((Integer)key).getCn());
           
                        }
                    }
                }
                else if (criterium.getProperty().equals("DOCUMENT_STATUS"))
                {
                    Object[] keys = getEnumerationKeys(criterion);                    
                    if (keys != null && keys.length > 0)
                    {
                        for (Object key : keys)
                        {     
                            ps.setString(++count, key.toString());
                            //System.out.println(key.toString());
           
                        }
                    }
                }
            }
            // ustalanie parametr�w zapytania
            count = 0;
            ResultSet rs = ps.executeQuery();
            
            File dir = new File(Docusafe.getHome()+"/reports");
            if (!dir.exists()) {
            	dir.mkdir();
            }
            File file = new File(Docusafe.getHome()+"/reports/report_"+getReportHandle().getId()+".csv");
            FileWriter fw = new FileWriter(file);
            
            String[] cols = new String[rs.getMetaData().getColumnCount()];
            for(int i = 0; i<cols.length;i++)
            {
            	cols[i] = rs.getMetaData().getColumnName(i+1);
            	fw.write(cols[i]);
            	fw.append(';');
            }
            fw.append('\n');
            
            
            
            
            while (rs.next())
            {            
            	for (String column : cols)
                {                    
                    String value;
                    if(column.equals("cdate")||column.equals("rcvDate"))
                    	value = DateUtils.formatJsDate(rs.getDate(column));
                    else if(column.equals("accepted"))
                    {
                    	if(rs.getInt(column)==1) value = "Nie";
                    	else value = "Tak";
                    }
                    else value = rs.getString(column);

                    if (value == null) {
                    	value = "null";
                    }
                    fw.write(value);
                    fw.append(';');
                }
            	fw.append('\n');
            }
        	
            rs.close();
            fw.flush();
            fw.close();
        }
        catch (IOException e)
        {
            throw new EdmException("B��d zapisu pliku tymczasowego", e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
            DSApi._close();
        }
    }
    
    
    public void createReport() throws EdmException
    {
        setReport(dockindCriteria.getReport());         
        PreparedStatement ps = null;
        try
        {
            DSApi.openAdmin();
            
            // wersja raportu
            write(DocumentHelper.createElement("version").addText(getReportVersion()));

            // generowanie elementu z informacjami g��wnymi
            Element elInfo = DocumentHelper.createElement("info");
            elInfo.addAttribute("name", getReportTitle());
            String orientation = getReport().getProperty(Report.PDF_ORIENTATION);
            elInfo.addAttribute("orientation", orientation == null ? Report.VERTICAL_ORIENTATION : orientation);
            write(elInfo);
            
            Element elCriteria = DocumentHelper.createElement("criteria");           
            for (Criterion criterion : getReport().getCriteria().values())
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
                if (criterium == null)
                    continue;
                if (criterium.getProperty()=="days")
               	 continue;
                
                Element el = null;                
                if (criterium instanceof ReportCriteria.Range)
                {
                    el = DocumentHelper.createElement("range");
                    el.addAttribute("property", criterion.getName());
                   
                    if (criterium.getProperty().equals("date"))
                    {
                        String start = (String) ((ReportCriteria.Range) criterium).getStart();
                        String end = (String) ((ReportCriteria.Range) criterium).getEnd();
                        
                        if (start != null)
                        {
                            dateFrom = DateUtils.parseJsDate(start);
                            el.addAttribute("start", start);
                        }
                        if (end != null)
                        {
                            dateTo = DateUtils.parseJsDate(end);
                            el.addAttribute("end", end);
                        }   
                    }
                    if (criterium.getProperty().equals("invoiceDate"))
                    {
                        String start = (String) ((ReportCriteria.Range) criterium).getStart();
                        String end = (String) ((ReportCriteria.Range) criterium).getEnd();
                        
                        if (start != null)
                        {
                            invoiceDateFrom = DateUtils.parseJsDate(start);
                            el.addAttribute("start", start);
                        }
                        if (end != null)
                        {
                            invoiceDateTo = DateUtils.parseJsDate(end);
                            el.addAttribute("end", end);
                        }   
                    }
                }
                else if (criterium instanceof ReportCriteria.ValueEnumeration)
                {
                    el = DocumentHelper.createElement("enumeration");
                    el.addAttribute("property", criterion.getName());
                    List<Object> values = getEnumerationValues(criterion);
                    for (Object value : values)
                    {
                        Element val = DocumentHelper.createElement("value");
                        val.addAttribute("title", value.toString());
                        el.add(val);
                    }
                }
                else
                {
                    throw new IllegalArgumentException("Nieznany typ kryterium: "+criterium);
                }                
                if (el != null)                                     
                    elCriteria.add(el);                 
            }            
            write(elCriteria);
                        
            // GENEROWANIE NAG��WKA TABELKI
            // kryteria wierszowe
            
            
            // kryterium kolumnowe - zak�adam, �e mo�e by� tylko jedno
            
            
            
            // GENEROWANIE TRE�CI G��WNEJ TABELKI
            // tworzenie odpowiedniego zapytania SQL
            StringBuilder sql = getSqlStatement(null);
                        
            
            
            ps = DSApi.context().prepareStatement(sql.toString());
            int count=0;
            for (Criterion criterion : getReport().getCriteria().values())
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
                if (criterium == null)
                    continue;
                
                
                if (criterium.getProperty().equals("date"))
                {
                    if (dateFrom != null )            
                        ps.setDate(++count, new java.sql.Date(dateFrom.getTime()));
                    if (dateTo != null)
                        ps.setDate(++count, new java.sql.Date(dateTo.getTime()));
                }
                if (criterium.getProperty().equals("invoiceDate"))
                {
                    if (invoiceDateFrom!= null )            
                        ps.setDate(++count, new java.sql.Date(invoiceDateFrom.getTime()));
                    if (invoiceDateTo != null)
                        ps.setDate(++count, new java.sql.Date(invoiceDateTo.getTime()));
                }
                else if (criterium.getProperty().equals("BU"))
                {
                    Object[] keys = getEnumerationKeys(criterion);                    
                    if (keys != null && keys.length > 0)
                    {
                        for (Object key : keys)
                        {     
                            ps.setString(++count, DocumentKind.findByCn(DocumentLogicLoader.ROCKWELL_KIND).getFieldByCn("BU").getEnumItem((Integer)key).getCn());
                            //System.out.println(DocumentKind.findByCn(DocumentKind.ROCKWELL_KIND).getFieldByCn("BU").getEnumItem((Integer)key).getCn());
           
                        }
                    }
                }
                else if (criterium.getProperty().equals("DOCUMENT_STATUS"))
                {
                    Object[] keys = getEnumerationKeys(criterion);                    
                    if (keys != null && keys.length > 0)
                    {
                        for (Object key : keys)
                        {     
                            ps.setString(++count, key.toString());
                            //System.out.println(key.toString());
           
                        }
                    }
                }
            }
            // ustalanie parametr�w zapytania
            count = 0;
            ResultSet rs = ps.executeQuery();
            
            Element elHeader = DocumentHelper.createElement("header");
            String[] cols = new String[rs.getMetaData().getColumnCount()];
            for(int i = 0; i<cols.length;i++)
            {
            	cols[i] = rs.getMetaData().getColumnName(i+1);
            	Element el = DocumentHelper.createElement("column");
                el.addAttribute("name", cols[i]);
                elHeader.add(el);
            }
            write(elHeader);
            write("<results>");
            // umieszczanie wynik�w w tabelce
            // http://localhost:8080/docusafe/repository/edit-dockind-document.action?id=2866
            while (rs.next())
            {
            	count++;
                Element element = DocumentHelper.createElement("row");                
                for (String column : cols)
                {                    
                    Element e = DocumentHelper.createElement("cell");
                    String value;
                    if(column.equals("cdate")||column.equals("rcvDate"))
                    	value = DateUtils.formatJsDate(rs.getDate(column));
                    else if(column.equals("accepted"))
                    {
                    	if(rs.getInt(column)==1) value = "Nie";
                    	else value = "Tak";
                    }
                    else value = rs.getString(column);
                    if(column.equals("id"))
                    {
                    	e.addAttribute("link", dockindCriteria.getContextPath()+"/repository/edit-document.action?id="+value);
                    }
                    e.addAttribute("value", value);  
                    element.add(e);
                }                
                write(element);
            }
            
            
    		Element nElem = DocumentHelper.createElement("row");
    		for (int i=0; i < cols.length; i++)
            {
                Element ne = DocumentHelper.createElement("cell");
                if (i == 0)
                {
                    ne.addAttribute("value", "Razem dokument�w");
                    ne.addAttribute("bold", "true");
                }
                if (i == 2)
                {
                    ne.addAttribute("value", String.valueOf(count));
                    ne.addAttribute("align", "left");
                }
                nElem.add(ne);
             
            }
    		write(nElem);
        	
            rs.close();
            
            write("</results>");     
        }
        catch (IOException e)
        {
            throw new EdmException("B��d zapisu pliku tymczasowego", e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
            DSApi._close();
        }
    }
    
    protected void addWhereClause(StringBuilder sql) throws EdmException
    {
    	boolean isFirst = true;
        for (Criterion criterion : getReport().getCriteria().values())
        {
        	
            ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) dockindCriteria.getCriterium(criterion.getFieldCn());
            if (criterium == null)
                continue;
            
            if (criterium instanceof ReportCriteria.Range)
            {
                if (criterium.getProperty().equals("date"))
                {
                    if (dateFrom != null)
                    {
                        sql.append((isFirst?" where ":" and ") + "(").append(getSqlColumn("standard","date")).append(">=?)");
                        isFirst = false;
                    }
                    if (dateTo != null)
                    {
                        sql.append((isFirst?" where ":" and ") + "(").append(getSqlColumn("standard","date")).append("<=?)");
                        isFirst = false;
                	}
                }
                if (criterium.getProperty().equals("invoiceDate"))
                {
                    if (invoiceDateFrom != null)
                    {
                    	//System.out.println(invoiceDateFrom.toString());
                        sql.append((isFirst?" where ":" and ") + "(").append(getSqlColumn("standard","invoiceDate")).append(">=?)");
                        isFirst = false;
                    }
                    if (invoiceDateTo != null)
                    {
                    	//System.out.println(invoiceDateTo.toString());
                        sql.append((isFirst?" where ":" and ") + "(").append(getSqlColumn("standard","invoiceDate")).append("<=?)");
                        isFirst = false;
                    }
                }
            }
            else if (criterium instanceof ReportCriteria.ValueEnumeration)
            {
                Object[] keys = getEnumerationKeys(criterion);                    
                if (keys != null && keys.length > 0)
                {
                    String str = null;
                    for (int i=0; i < keys.length; i++)
                    {                            
                        str = (str == null ? "?" : str+",?");                            
                    }
                    sql.append((isFirst?" where ":" and ") + "(").append(getSqlColumn(criterion.getType(), criterion.getFieldCn())).append(" in (").append(str).append("))");
                }
            }
            isFirst = false;
        }
        
    }

	
}
