package pl.compan.docusafe.service.reports.dockind;

import org.hibernate.dialect.SQLServerDialect;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.util.StringManager;


/**
 * Liczbowy raport dokument�w na listach zada� u�ytkownik�w/dzia��w.
 * 
 * Standardowe kryteria okre�laj�:
 * <ul>
 * <li> date - data otrzymania dokumentu na list� zada�,
 * <li> division - dzia�, w kt�rym znajduje si� dokument (nale�y pami�ta� o sytuacji, gdy jeden u�ytkownik
 * znajduje si� w kilku dzia�ach, w�wczas ka�de zadanie z jego listy nale�y tylko do jednego dzia�u - jedno
 * pismo nie mo�e znajdywa� si� w 2 dzia�ach jednocze�nie; ponadto jak pismo dekretowane jest na dzia�, a nie
 * bezpo�rednio do uzytkownika, to taki dokument na pocz�tku znajduje si� np. u 5 u�ytkownik�w, ale do zestawienia
 * danego dzia�u oczywi�cie ten dokument liczymy nie jako 5, ale jako 1!),
 * <li> user - u�ytkownik, na kt�rego li�cie znajduje si� dokument.
 * </ul>
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class TasklistDocumentsCountReport extends AbstractDocumentsCountReport
{
    public static final String REPORT_ID = "tasklist-docs-count";
    public static final String REPORT_VERSION = "1.0";
     
    // podtypy raport�w
    public static final String TYPE_ALL = "all"; // domy�lny
    public static final String TYPE_UNREALIZED = "unrealized";
    
    public TasklistDocumentsCountReport(ReportCriteria criteria)
    {
        super(criteria);
    }
    
    public String getGeneratorId()
    {
        return REPORT_ID;
    } 
     
    public String getReportVersion()
    {
        return REPORT_VERSION;
    }
    
    /** 
     * Zwraca klas� dostarczaj�c� specyficzne informacje do wyszukiwania dokument�w na potrzeby
     * szczeg��w raportu.
     */
    public static SearchDocumentsHandler getSDHandler()
    {
        return new TasklistReportSDHandler();
    }
    
    public StringBuilder getSqlStatement(Criterion colCriterion) throws EdmException
    {
        String partitionString = getPartitionString(report);
        StringBuilder sql = new StringBuilder("select ");
        if (partitionString != null)
            sql.append(partitionString).append(", ");
        String colCriterionString = colCriterion != null ? getColumnCriterionString(report) : null;
        if (colCriterionString != null)
            sql.append(colCriterionString);
        sql.append("count(distinct dt.document_id) from ");
        sql.append(dockindCriteria.getDocumentKind().getTablename()).append(" dt,dsw_tasklist t").append(" where (dt.document_id=t.dso_document_id)");
        sql.append(" and ((t.dsw_process_processdef='obieg_reczny') or (t.external_workflow=1))"); 
        if (TYPE_UNREALIZED.equals(report.getProperty(Report.TYPE)))
            sql.append(" and (t.dsw_assignment_accepted=0)"); 
        // kryteria raportu w WHERE
        addWhereClause(sql);

        if (partitionString != null){
            sql.append(" group by ").append(partitionString);
            sql.append(" order by ").append(partitionString.split(",")[0]);
        }
        
        return sql;
    }
    
    protected String getSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected String getAliasSqlColumn(String fieldType, String fieldCn) throws EdmException
    {
        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
    }
    
    protected static String getSqlColumnStatic(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
    {
        if ("standard".equals(fieldType))
        {
            if ("date".equals(fieldCn))
                return "t.rcvDate";
            else if ("division".equals(fieldCn))
                return "t.divisionGuid";
            else if ("user".equals(fieldCn))
                return "t.dsw_resource_res_key";
            else if ("result".equals(fieldCn))
            {
                if (DSApi.getDialect() instanceof SQLServerDialect)
                    return "datediff(hour,t.dsw_activity_laststatetime,getdate())";
                else
                    throw new EdmException("Ten raport nie jest zaimplementowany dla u�ywanej bazy danych");
            }
        }
        else if ("dockind".equals(fieldType))
        {
            return "dt."+(documentKind.getFieldByCn(fieldCn).getColumn());
        }
        throw new IllegalStateException("Napotkano niepoprawn� definicj� kryterium raportu");
    }
    
    protected String resultColumnName()
    {
        return sm.getString("LiczbaDokumentow");
    }

    protected String resultValueSuffix()
    {
        return null;
    }
    
    // TODO: obecnie szczeg�y raportu mog� by� chyba lekko rozbie�ne z rzeczywisto�ci�
    //       bo jest troche inaczej distinct robiony w searchDocuments niz w getSqlStatement
    public static class TasklistReportSDHandler implements SearchDocumentsHandler 
    { 
        public String getSqlColumn(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
        {
            return getSqlColumnStatic(fieldType, fieldCn, documentKind);
        }                
        
        public String getSqlDocumentId()
        {
            return "dt.document_id";
        }
        
        public String getSqlFromClause(DocumentKind documentKind, Report report)
        {
            StringBuilder fromSql = new StringBuilder(documentKind.getTablename());
            fromSql.append(" dt,dsw_tasklist t").append(" where (dt.document_id=t.dso_document_id)");
            fromSql.append(" and ((t.dsw_process_processdef='obieg_reczny') or (t.external_workflow=1))"); 
            if (TYPE_UNREALIZED.equals(report.getProperty(Report.TYPE)))
                fromSql.append(" and (t.dsw_assignment_accepted=0)"); 
            return fromSql.toString();                        
        }
        
        public String getResultColumnNameOnDetails(Report report)
        {            
            StringManager sm = GlobalPreferences.loadPropertiesFile(AbstractDockindReport.class.getPackage().getName(),null);
            if (TYPE_UNREALIZED.equals(report.getProperty(Report.TYPE)))
                return sm.getString("NiezrealizowanyPrzez");
            else
                return sm.getString("NaLiscieZadanPrzez");
        }
        
        public String getResultValueSuffix()
        {
            return " h";
        }
        
        public boolean selectDistinct()
        {
            return true;
        }
    }
}
