package pl.compan.docusafe.service.reports.dockind;

import org.hibernate.dialect.FirebirdDialect;
import org.hibernate.dialect.SQLServerDialect;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.service.reports.dockind.AssignedDocumentsCountReport.AssignedReportSDHandler;


public class ProcesedDocumentsReport extends AbstractDocumentsCountReport
{
	    public static final String REPORT_ID = "procesed-docs";
	    public static final String REPORT_VERSION = "1.0";
	    
	    public ProcesedDocumentsReport(ReportCriteria criteria)
	    {
	        super(criteria);
	    }

	    public String getGeneratorId()
	    {
	        return REPORT_ID;
	    }

	    public String getReportVersion()
	    {
	        return REPORT_VERSION;
	    }
	    
	    /** 
	     * Zwraca klas� dostarczaj�c� specyficzne informacje do wyszukiwania dokument�w na potrzeby
	     * szczeg��w raportu.
	     */
	    public static SearchDocumentsHandler getSDHandler()
	    {
	        return new AssignedReportSDHandler();
	    }
	    
	    protected String getSqlColumn(String fieldType, String fieldCn) throws EdmException
	    {
	        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
	    }
	    
	    protected String getAliasSqlColumn(String fieldType, String fieldCn) throws EdmException
	    {
	        return getSqlColumnStatic(fieldType, fieldCn, dockindCriteria.getDocumentKind());
	    }
	    
	    protected static String getSqlColumnStatic(String fieldType, String fieldCn, DocumentKind documentKind) throws EdmException
	    {
	        if ("standard".equals(fieldType))
	        {
	        	if ("date".equals(fieldCn))
	                return "t.receiveDate";
	        	if ("user".equals(fieldCn))
	                return "t.username";
	            else if ("result".equals(fieldCn)) 
	                // dla tego raportu nie u�ywamy kolumny z wynikiem na szczeg�ach - zwracam 1 �eby co� by�o
	                return "1";
	        }
	        else if ("dockind".equals(fieldType))
	        {
	            return "dt."+(documentKind.getFieldByCn(fieldCn).getColumn());
	        }
	        throw new IllegalStateException("Napotkano niepoprawn� definicj� kryterium raportu");
	    }

	    public StringBuilder getSqlStatement(Criterion colCriterion) throws EdmException
	    {
	        String partitionString = getPartitionString(report);
	        StringBuilder sql = new StringBuilder("select ");
	        if (partitionString != null)
	            sql.append(partitionString).append(", ");
	        String colCriterionString = colCriterion != null ? getColumnCriterionString(report) : null;
	        if (colCriterionString != null)
	            sql.append(colCriterionString);
	       
	        if (DSApi.getDialect() instanceof SQLServerDialect)
	            sql.append("count(t.id) from ");
	        else if (DSApi.getDialect() instanceof FirebirdDialect)
	            throw new EdmException("Ten raport nie jest jeszcze zaimplementowany dla bazy Firebird");
	        else if (DSApi.isOracleServer())
	            throw new EdmException("Ten raport nie jest jeszcze zaimplementowany dla bazy Oracle");
	        else
	            throw new EdmException("Ten raport nie jest zaimplementowany dla u�ywanej bazy danych");
	        Integer days;
	        try{
	        	days = Integer.parseInt(dockindCriteria.getCriterium("days").toString());
	        }catch(Exception f){
	        	days = 30;
	        }
	        sql.append("dsw_task_history_entry t").append(" where (t.documentId!=0) ");
	        // kryteria raportu w WHERE
	        addWhereClause(sql);

	        if (partitionString != null)
	            sql.append(" group by ").append(partitionString);
	        
	        return sql;
	    }

	    protected String resultColumnName()
	    {
	        return sm.getString("LiczbaDokumentow");
	    }
	    
	    protected String resultValueSuffix()
	    {
	        return null;
	    }
  
	}
