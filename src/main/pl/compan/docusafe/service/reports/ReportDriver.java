package pl.compan.docusafe.service.reports;

import java.lang.reflect.Constructor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;

/* User: Administrator, Date: 2005-04-25 14:17:35 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ReportDriver.java,v 1.14 2009/12/12 14:03:41 wkuty Exp $
 */
public class ReportDriver extends ServiceDriver implements Report
{
    private ExecutorService threadPool;

  /*  private static final Map<String,String> reportGenerators = new HashMap<String, String>();
    static
    {
        reportGenerators.put(InDocumentsFineReport.REPORT_ID, InDocumentsFineReport.class.getName());
        reportGenerators.put(InDocumentsReport.REPORT_ID, InDocumentsReport.class.getName());
        reportGenerators.put(OutDocumentsReport.REPORT_ID, OutDocumentsReport.class.getName());
        reportGenerators.put(IntDocumentsReport.REPORT_ID, IntDocumentsReport.class.getName());
        reportGenerators.put(BoxContentsReport.REPORT_ID, BoxContentsReport.class.getName());
        reportGenerators.put(OverdueDocumentsReport.REPORT_ID, OverdueDocumentsReport.class.getName());
        // raporty z dockina
        reportGenerators.put(AssignedDocumentsCountReport.REPORT_ID, AssignedDocumentsCountReport.class.getName());
        reportGenerators.put(TasklistDocumentsCountReport.REPORT_ID, TasklistDocumentsCountReport.class.getName());
        reportGenerators.put(AverageTimeOnTasklistReport.REPORT_ID, AverageTimeOnTasklistReport.class.getName());        
    }*/



    protected void start() throws ServiceException
    {
        Integer n = Ints.tryParse(Strings.nullToEmpty(Docusafe.getAdditionProperty("reports.thread-pool-value")));
        if (n != null && n > 0) {
            Preconditions.checkState(threadPool == null, "threadPool exist");
            threadPool = Executors.newFixedThreadPool(n, Executors.defaultThreadFactory());
        }
    }

    protected void stop() throws ServiceException
    {
    }

    protected boolean canStop()
    {
        return false;
    }

    public ReportHandle generateReport(String reportId, ReportCriteria criteria, String username) throws ServiceException
    {
        // utworzenie wpisu w tabeli DS_REPORTS
        // zwr�cenie ReportHandle z identyfikatorem raportu
        // uruchomienie w�tku tworz�cego raport i przekazanie mu Handle
        // w�tek na koniec tworzenia robi handle.notifyAll

        // przechowywanie listy w�tk�w tworz�cych raporty, raporty jednego
        // u�ytkownika s� kolejkowane

        //String clazz = (String) reportGenerators.get(generatorId);
        Class c = ReportGenerator.getReportGenerator(reportId);
        if (c == null)
            throw new ServiceException("Nie istnieje generator raport�w typu "+reportId);

        ReportGenerator generator;
        try
        {
            //Class c = Class.forName(clazz);
            Constructor ctor = c.getConstructor(new Class[] { ReportCriteria.class } );
            generator = (ReportGenerator) ctor.newInstance(new Object[] { criteria } );
        }
        catch (Exception e)
        {
            throw new ServiceException(e.getMessage(), e);
        }

        ReportHandle handle = generator.createHandle(username);

        if (threadPool != null) {
            threadPool.execute(generator);
        } else {
            generator.start();
        }

        return handle;
    }
}
