package pl.compan.docusafe.service.reports;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.web.admin.BokAction;

/**
 * Tworzy raport szczeg�owy korespondencji wewn�trznej.
 * Dopuszczalne kryteria: <ul>
 * <li>eq property="kind", value=Integer (identyfikator typu pisma)
 * <li>eq property="journal" value=Long (identyfikator dziennika)
 * <li>eq property="delivery" value=Integer (identyfikator InOfficeDocumentDelivery)
 * <li>eq property="outgoingDelivery" value=Integer (identyfikator OutOfficeDocumentDelivery)
 * <li>eq property="author" value=String (identyfikator u�ytkownika tworz�cego pismo)
 * <li>range property="incomingDate" start=Date, end=Date
 * </ul>
 * Przyk�adowa posta� pliku xml:
 * <pre>
 * <report id="indocuments">
 *  <author username="lk">�ukasz Kowalczyk</author>
 *  <date ctime="1112867511109">07-04-2005</date>
 *  <data>
 *      <version>1.0</version>
 *      <criteria>
 *          <eq property="journal" value="0" name="Dziennik g��wny"/>
 *          <eq property="kind" value="2" name="List"/>
 *          <eq property="deliery" value="1" name="Poczta"/>
 *          <eq property="outgoingDelivery" value="3" name="Kurier"/>
 *          <eq property="author" value="lk" name="�ukasz Kowalczyk"/>
 *          <eq property="sender" value="Kowalczyk" fulltext="true"/>
 *          <range property="incomingDate" start="1112867511109" end="1112867511109"
 *              start.date="10-03-2005" end.date="11-03-2005"/>
 *      </criteria>
 *      <results>
 *          <count sequenceId="1" count="40" date="1112867511109" date.date="10-03-2004"/>
 *          <count sequenceId="2" count="12" date="1112867511109" date.date="11-03-2004"/>
 *          <totalcount count="52"/>
 *      </results>
 *  </data>
 * </report>
 * </pre>
 * @author <a href="mailto:jerzy.pirog@docusafe.pl">Jerzy Pir�g</a>
 * @version $Id: InDocumentsFineReport.java,v 1.14 2008/04/17 15:44:39 pecet4 Exp $
 */
public class IntDocumentsFineReport extends ReportGenerator
{
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(BokAction.class.getPackage().getName(),null);
    public static final String REPORT_ID = "intdocumentfine";
    public static final String REPORT_VERSION = "1.0";
    public static final String TITLE = sm.getString("RejestrSzczegolowyKorespondencjiWewnetrznej");
//    RejestrSzczegolowyKorespondencjiWewnetrznej
//    RejestrSzczegolowyKorespondencjiWychodzacej

    private ReportCriteria criteria;

    public IntDocumentsFineReport(ReportCriteria criteria)
    {
        super(criteria);
        this.criteria = criteria;
    }

    public String getGeneratorId()
    {
        return REPORT_ID;
    }

    public String getReportTitle()
    {
        return TITLE;
    }

    public String getReportVersion()
    {
        return REPORT_VERSION;
    }
    
    /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci HTML */
    public static String getHtmlXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/int-documents-fine-html";
    }
    
    /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci PDF */
    public static String getFoXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/int-documents-fine-fo";
    }

    public void createReport() throws EdmException
    {
        //PreparedStatement ps = null;
        try
        {
            DSApi.openAdmin();

            // wersja raportu
            write(DocumentHelper.createElement("version").addText(REPORT_VERSION));

            // generowanie sekcji opisuj�cej kryteria tworzenia raportu
            Element elCriteria = DocumentHelper.createElement("criteria");
            for (Iterator iter=criteria.getCriteria().iterator(); iter.hasNext(); )
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();
                Element el;
                if (criterium instanceof ReportCriteria.Eq)
                {
                    el = DocumentHelper.createElement("eq");
                    el.addAttribute("property", criterium.getProperty());
                    el.addAttribute("value", propertyValue(((ReportCriteria.Eq) criterium).getValue()));

                    if (criterium.getProperty().equals("journal"))
                    {
                        Long journalId = (Long) ((ReportCriteria.Eq) criterium).getValue();
                        Journal journal = Journal.find(journalId);
                        try
                        {
                            el.addAttribute("name", (journal.getOwnerGuid() == null ? "" :
                                    DSDivision.find(journal.getOwnerGuid()).getName()+" - ")+
                                journal.getDescription() + " o Symbolu - " +(journal.getSymbol()!=null ? journal.getSymbol(): "Brak" ));
                        }
                        catch (DivisionNotFoundException e)
                        {
                            el.addAttribute("name", journal.getDescription());
                        }
                    }
                    else if (criterium.getProperty().equals("author"))
                    {
                        el.addAttribute("name", DSUser.safeToFirstnameLastname((String) ((ReportCriteria.Eq) criterium).getValue()));
                    }
                }
                else if (criterium instanceof ReportCriteria.EqText)
                {
                    el = DocumentHelper.createElement("eq");
                    el.addAttribute("fulltext", "true");
                    el.addAttribute("property", criterium.getProperty());
                    el.addAttribute("value", propertyValue(((ReportCriteria.EqText) criterium).getValue()));
                }
                else if (criterium instanceof ReportCriteria.Range)
                {
                    el = DocumentHelper.createElement("range");
                    el.addAttribute("property", criterium.getProperty());
                    el.addAttribute("start", propertyValue(((ReportCriteria.Range) criterium).getStart()));
                    el.addAttribute("end", propertyValue(((ReportCriteria.Range) criterium).getEnd()));

                    if (criterium.getProperty().equals("incomingDate"))
                    {
                        Date start = (Date) ((ReportCriteria.Range) criterium).getStart();
                        Date end = (Date) ((ReportCriteria.Range) criterium).getEnd();
                        if (start != null)
                            el.addAttribute("start.date", DateUtils.formatCommonDate(start));
                        if (end != null)
                            el.addAttribute("end.date", DateUtils.formatCommonDate(end));
                    }
                }
                else
                {
                    throw new IllegalArgumentException("Nieznany typ kryterium: "+criterium);
                }

                elCriteria.add(el);
            }

            write(elCriteria);


            write("<results>");

            String author = null;
            Date incomingDateFrom = null;
            Date incomingDateTo = null;
            Long journalId = null;
            String sender = null;
            String description = null;

            for (Iterator iter=criteria.getCriteria().iterator(); iter.hasNext(); )
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();

                if (criterium instanceof ReportCriteria.Eq)
                {
                    if (criterium.getProperty().equals("author"))
                    {
                        author = (String) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("journal"))
                    {
                        journalId = (Long) ((ReportCriteria.Eq) criterium).getValue();
                    }
                }
                else if (criterium instanceof ReportCriteria.EqText)
                {
                    if (criterium.getProperty().equals("sender") && AvailabilityManager.isAvailable("reports.internalFineReportQuery"))
                    {
                        sender = (String) ((ReportCriteria.EqText) criterium).getValue();
                    } else if (criterium.getProperty().equals("description"))
                    {
                        description = (String) ((ReportCriteria.EqText) criterium).getValue();
                    }
                }
                else if (criterium.getProperty().equals("incomingDate") && criterium instanceof ReportCriteria.Range)
                {
                    ReportCriteria.Range range = (ReportCriteria.Range) criterium;
                    if (range.getStart() != null)
                        incomingDateFrom = ((Date) range.getStart());
                    if (range.getEnd() != null)
                        incomingDateTo = ((Date) range.getEnd());
                }
            }

            FromClause from = new FromClause();
            TableAlias docTable = from.createTable(DSApi.getTableName(OutOfficeDocument.class));
            WhereClause where = new WhereClause();
            TableAlias baseDocTable = from.createJoinedTable(DSApi.getTableName(Document.class), true, docTable, FromClause.LEFT_OUTER, "$l.id = $r.id");
            TableAlias senderTable = null;
            if(AvailabilityManager.isAvailable("reports.internalFineReportQuery"))
            {
            	 senderTable = from.createJoinedTable(DSApi.getTableName(Sender.class),true,docTable,FromClause.LEFT_OUTER,"$l.sender = $r.id");
            }
            //            TableAlias docBase = from.createJoinedTable("ds_document",true,docTable,FromClause.JOIN,"$l.id = $r.id");
//            where.add(Expression.eqAttribute(
//                docTable.attribute("sender"), senderTable.attribute("id")));
//            where.add(Expression.eq(senderTable.attribute("discriminator"), "SENDER"));
            where.add(Expression.eq(docTable.attribute("internaldocument"), true));

            if (sender != null)
            {
                String[] tokens = sender.toUpperCase().split("[\\s,.;:]+");

                if (tokens.length > 0)
                {
                    Junction OR = Expression.disjunction();
              
                    Junction JUN;
                    if (((Boolean) criteria.getAttribute("forceAnd")).booleanValue())
                        JUN = Expression.conjunction();
                    else
                        JUN = Expression.disjunction();
                    for (int i=0; i < tokens.length; i++)
                    {
                        String token = tokens[i];

                        Junction OR2 = Expression.disjunction();
                        OR2.add(Expression.like(Function.upper(senderTable.attribute("firstname")),
                            "%"+ StringUtils.substring(token, 0, 48)+"%"));
                        OR2.add(Expression.like(Function.upper(senderTable.attribute("lastname")),
                            "%"+StringUtils.substring(token, 0, 49)+"%"));
                        JUN.add(OR2);
                    }
                    OR.add(JUN);
                    
                    Junction AND = Expression.conjunction();
                    for (int i=0; i < tokens.length; i++)
                    {
                        String token = tokens[i];
                        AND.add(Expression.like(Function.upper(senderTable.attribute("organization")),
                            "%"+StringUtils.substring(token, 0, 49)+"%"));
                    }
                    OR.add(AND);

                    where.add(OR);
                }
            }

            if (author != null)
                where.add(Expression.eq(docTable.attribute("creatingUser"), author));


            if (journalId != null)
                where.add(Expression.eq(docTable.attribute("journal_id"), journalId));

            if (incomingDateFrom != null)
                where.add(Expression.ge(docTable.attribute("documentdate"), DateUtils.midnight(incomingDateFrom, 0)));

            if (incomingDateTo != null)
                where.add(Expression.le(docTable.attribute("documentdate"), DateUtils.midnight(incomingDateTo, 1)));

            // incomingdate = cal.getTime()
/*
            where.add(Expression.ge(docTable.attribute("incomingdate"),
                DateUtils.midnight(cal.getTime(), 0)));
            where.add(Expression.lt(docTable.attribute("incomingdate"),
                DateUtils.midnight(cal.getTime(), 1)));
*/ 
            SelectClause selectId = new SelectClause(true);
            SelectColumn colId = selectId.add(docTable, "id");
            SelectColumn colSummary = selectId.add(docTable, "summary");
            SelectColumn colDate = selectId.add(docTable, "documentdate");
            SelectColumn colIncomingDate = selectId.add(docTable, "documentdate");
            SelectColumn colKO = selectId.add(docTable, "officenumber");
            SelectColumn colGuid = selectId.add(docTable, "divisionguid");
            SelectColumn colCreatingUser = selectId.add(docTable, "creatingUser");


            SelectColumn colSndAnon = null;
            SelectColumn colSndTitle = null;
            SelectColumn colSndFirstname = null;
            SelectColumn colSndLastname = null;
            SelectColumn colSndOrganization = null;
            SelectColumn colSndStreet = null;
            SelectColumn colSndLocation = null;
            if(AvailabilityManager.isAvailable("reports.internalFineReportQuery"))
            {
            	colSndAnon = selectId.add(senderTable, "anonymous");
            	colSndTitle = selectId.add(senderTable, "title");
            	colSndFirstname = selectId.add(senderTable, "firstname");
            	colSndLastname = selectId.add(senderTable, "lastname");
            	colSndOrganization = selectId.add(senderTable, "organization");
            	colSndStreet = selectId.add(senderTable, "street");
            	colSndLocation = selectId.add(senderTable, "location");
            }
            SelectColumn colDescription = selectId.add(baseDocTable, "description");

            SelectQuery selectQuery = new SelectQuery(selectId, from, where, null);
            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());

            int totalCount = 0;
            while (rs.next())
            {
                long id = rs.getLong(colId.getPosition());

                Element element = DocumentHelper.createElement("document");
                element.addAttribute("lp", String.valueOf(totalCount+1));
                element.addAttribute("documentId", String.valueOf(id));
                element.addAttribute("document-link", Docusafe.getBaseUrl()+"/office/internal/summary.action?documentId="+String.valueOf(id));
                element.addAttribute("summary", rs.getString(colSummary.getPosition()));
                java.sql.Date sqlIncomingDate = rs.getDate(colIncomingDate.getPosition());
                if (sqlIncomingDate != null)
                {
                    element.addAttribute("incomingDate", String.valueOf(sqlIncomingDate.getTime()));
                    element.addAttribute("incomingDate.date", DateUtils.formatCommonDate(sqlIncomingDate));
                }
                String desc = rs.getString(colDescription.getPosition());
                if(desc != null)
                	element.addAttribute("description", desc);
                String nameOfCreatingUser = rs.getString(colCreatingUser.getPosition());
                if(nameOfCreatingUser != null){
                	DSUser creatingUser = DSUser.findByUsername(nameOfCreatingUser);
                	element.addAttribute("author", creatingUser.asFirstnameLastname());
                }
                java.sql.Date sqlDate = rs.getDate(colDate.getPosition());
                if (sqlDate != null)
                {
                    element.addAttribute("date", String.valueOf(sqlDate.getTime()));
                    element.addAttribute("date.date", DateUtils.formatCommonDate(sqlDate));
                }
                element.addAttribute("officeNumber", String.valueOf(rs.getInt(colKO.getPosition())));
                String guid = rs.getString(colGuid.getPosition());
                if (guid != null&& !guid.contains("null"))
                {
                    element.addAttribute("division", guid);
                    element.addAttribute("division.name", DSDivision.find(guid).getName());
                }
                if(AvailabilityManager.isAvailable("reports.internalFineReportQuery"))
                {
                element.addAttribute("senderSummary", Person.getSummary(
                    rs.getBoolean(colSndAnon.getPosition()),
                    rs.getString(colSndTitle.getPosition()),
                    rs.getString(colSndFirstname.getPosition()),
                    rs.getString(colSndLastname.getPosition()),
                    rs.getString(colSndOrganization.getPosition()),
                    rs.getString(colSndStreet.getPosition()),
                    rs.getString(colSndLocation.getPosition())));
                }
                totalCount++;

                write(element);
            }

            rs.close();

            Element element = DocumentHelper.createElement("totalcount");
            element.addAttribute("count", String.valueOf(totalCount));
            write(element);

            write("</results>");
        }
        catch (IOException e)
        {
            throw new EdmException(sm.getString("BladZapisuPlikuTymczasowego"), e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi._close();
        }
    }
}
