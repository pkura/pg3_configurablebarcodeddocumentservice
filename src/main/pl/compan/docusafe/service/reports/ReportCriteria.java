package pl.compan.docusafe.service.reports;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ReportCriteria.java,v 1.10 2007/12/06 16:45:55 kubaw Exp $
 */
public class ReportCriteria
{
    private Map<String,Object> criteria = new LinkedHashMap<String,Object>();
    private Map<String,Object> attributes = new LinkedHashMap<String,Object>();

    public void addEq(String property, Object value)
    {
        if (property == null)
            throw new NullPointerException("property");

        criteria.put(property, new Eq(property, value));
    }

    public void addEqText(String property, Object value)
    {
        if (property == null)
            throw new NullPointerException("property");

        criteria.put(property, new EqText(property, value));
    }

    public void addRange(String property, Object start, Object end)
    {
        if (property == null)
            throw new NullPointerException("property");
        if (start == null && end == null)
            throw new IllegalArgumentException("start == null && end == null");

        criteria.put(property, new Range(property, start, end));
    }

    public void addEnumeration(String property, Object[] values)
    {
        if (property == null)
            throw new NullPointerException("property");
        if (values == null || values.length == 0)
            throw new IllegalArgumentException("Brak listy warto�ci dla "+property);

        criteria.put(property, new ValueEnumeration(property, values));
    }

    public void setAttribute(String attribute, Object value)
    {
        if (attribute == null)
            throw new NullPointerException("attribute");
        if (value == null)
            throw new NullPointerException("value");
        
        attributes.put(attribute, value);
    }

    public Object getAttribute(String attribute)
    {
        return attributes.get(attribute); 
    }

    public Collection<Object> getCriteria()
    {
        return Collections.unmodifiableCollection(criteria.values());
    }
    
    public Object getCriterium(String property)
    {
        return criteria.get(property);
    }

    public abstract static class Criterium {
        public abstract String getProperty();
    }

    public abstract static class AbstractEq extends Criterium
    {
        String property;
        Object value;

        protected AbstractEq(String property, Object value)
        {
            this.property = property;
            this.value = value;
        }

        public String getProperty()
        {
            return property;
        }

        public Object getValue()
        {
            return value;
        }

    }

    public static class Eq extends AbstractEq
    {
        public Eq(String property, Object value)
        {
            super(property, value);
        }
    }

    public static class EqText extends AbstractEq
    {
        public EqText(String property, Object value)
        {
            super(property, value);
        }
    }

    public static class Range extends Criterium
    {
        private String property;
        private Object start;
        private Object end;

        public Range(String property, Object start, Object end)
        {
            this.property = property;
            this.start = start;
            this.end = end;
        }

        public String getProperty()
        {
            return property;
        }

        public Object getStart()
        {
            return start;
        }

        public Object getEnd()
        {
            return end;
        }
    }

    public static class ValueEnumeration extends Criterium
    {
        private String property;
        private Object[] values;

        public ValueEnumeration(String property, Object[] values)
        {
            this.property = property;
            this.values = values;
        }

        public String getProperty()
        {
            return property;
        }

        public Object[] getValues()
        {
            return values;
        }
    }
}
