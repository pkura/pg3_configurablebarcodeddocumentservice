package pl.compan.docusafe.service.reports;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

/**
 * Tworzy raport korespondencji wychodz�cej.
 * Dopuszczalne kryteria: <ul>
 * <li>eq property="kind", value=Integer (identyfikator typu pisma)
 * <li>eq property="journal" value=Long (identyfikator dziennika)
 * <li>eq property="delivery" value=Integer (identyfikator InOfficeDocumentDelivery)
 * <li>eq property="outgoingDelivery" value=Integer (identyfikator OutOfficeDocumentDelivery)
 * <li>eq property="author" value=String (identyfikator u�ytkownika tworz�cego pismo)
 * <li>range property="incomingDate" start=Date, end=Date
 * </ul>
 * Przyk�adowa posta� pliku xml:
 * <pre>
 * <report id="indocuments">
 *  <author username="lk">�ukasz Kowalczyk</author>
 *  <date ctime="1112867511109">07-04-2005</date>
 *  <data>
 *      <version>1.0</version>
 *      <criteria>
 *          <eq property="journal" value="0" name="Dziennik g��wny"/>
 *          <eq property="delivery" value="1" name="Poczta"/>
 *          <eq property="author" value="lk" name="�ukasz Kowalczyk"/>
 *          <range property="documentDate" start="1112867511109" end="1112867511109"
 *              start.date="10-03-2005" end.date="11-03-2005"/>
 *      </criteria>
 *      <results>
 *          <count sequenceId="1" count="40" date="1112867511109" date.date="10-03-2004"/>
 *          <count sequenceId="2" count="12" date="1112867511109" date.date="11-03-2004"/>
 *          <totalcount count="52"/>
 *      </results>
 *  </data>
 * </report>
 * </pre>
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: IntOutDocumentsReport.java,v 1.13 2009/02/05 12:17:31 wkuty Exp $
 */
public abstract class IntOutDocumentsReport extends ReportGenerator
{
	static final Logger log = LoggerFactory.getLogger(IntOutDocumentsReport.class);
    private ReportCriteria criteria;

    public IntOutDocumentsReport(ReportCriteria criteria)
    {
        super(criteria);
        this.criteria = criteria;
    }

    public abstract String getGeneratorId();

    public abstract String getReportTitle();

    public abstract String getReportVersion();

    protected abstract boolean isInternal();

    public void createReport() throws EdmException
    {
        try
        {
            DSApi.openAdmin();

            // wersja raportu
            write(DocumentHelper.createElement("version").addText(getReportVersion()));

            // generowanie sekcji opisuj�cej kryteria tworzenia raportu
            Element elCriteria = DocumentHelper.createElement("criteria");
            for (Iterator iter=criteria.getCriteria().iterator(); iter.hasNext(); )
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();
                Element el = null;
                if (criterium instanceof ReportCriteria.Eq)
                {
                    el = DocumentHelper.createElement("eq");
                    el.addAttribute("property", criterium.getProperty());
                    el.addAttribute("value", propertyValue(((ReportCriteria.Eq) criterium).getValue()));

/*
                    if (criterium.getProperty().equals("kind"))
                    {
                        Integer kindId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                        el.addAttribute("name", InOfficeDocumentKind.find(kindId).getName());
                    }
*/
                    /*else*/ if (criterium.getProperty().equals("journal"))
                    {
                        Long journalId = (Long) ((ReportCriteria.Eq) criterium).getValue();
                        Journal journal = Journal.find(journalId);
                        try
                        {
                            el.addAttribute("name", (journal.getOwnerGuid() == null ? "" :
                                    DSDivision.find(journal.getOwnerGuid()).getName()+" - ")+
                                journal.getDescription());
                        }
                        catch (DivisionNotFoundException e)
                        {
                            el.addAttribute("name", journal.getDescription());
                        }
                    }
                    else if (criterium.getProperty().equals("delivery"))
                    {
                        Integer deliveryId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                        el.addAttribute("name", OutOfficeDocumentDelivery.find(deliveryId).getName());
                    }
/*
                    else if (criterium.getProperty().equals("outgoingDelivery"))
                    {
                        Integer outgoingDeliveryId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                        el.addAttribute("name", OutOfficeDocumentDelivery.find(outgoingDeliveryId).getName());
                    }
*/
                    else if (criterium.getProperty().equals("author"))
                    {
                        el.addAttribute("name", DSUser.safeToFirstnameLastname((String) ((ReportCriteria.Eq) criterium).getValue()));
                    }
                }
                else if (criterium instanceof ReportCriteria.EqText)
                {
                    if (criterium.getProperty().equals("recipient"))
                    {
                        el = DocumentHelper.createElement("eq");
                        el.addAttribute("fulltext", "true");
                        el.addAttribute("property", criterium.getProperty());
                        el.addAttribute("value", propertyValue(((ReportCriteria.EqText) criterium).getValue()));
                    }
                }
                else if (criterium instanceof ReportCriteria.Range)
                {
                    el = DocumentHelper.createElement("range");
                    el.addAttribute("property", criterium.getProperty());
                    el.addAttribute("start", propertyValue(((ReportCriteria.Range) criterium).getStart()));
                    el.addAttribute("end", propertyValue(((ReportCriteria.Range) criterium).getEnd()));

                    if (criterium.getProperty().equals("ctime"))
                    {
                        Date start = (Date) ((ReportCriteria.Range) criterium).getStart();
                        Date end = (Date) ((ReportCriteria.Range) criterium).getEnd();
                        if (start != null)
                            el.addAttribute("start.date", DateUtils.formatCommonDate(start));
                        if (end != null)
                            el.addAttribute("end.date", DateUtils.formatCommonDate(end));
                    }
                }
                else
                {
                    throw new IllegalArgumentException("Nieznany typ kryterium: "+criterium);
                }

                if (el != null)
                    elCriteria.add(el);
            }

            write(elCriteria);


            write("<results>");

            String author = null;
            Date documentDateFrom = null;
            Date documentDateTo = null;
            Integer kindId = null;
            Long journalId = null;
            Integer deliveryId = null;
            String recipient = null;

            for (Iterator iter=criteria.getCriteria().iterator(); iter.hasNext(); )
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();

                if (criterium instanceof ReportCriteria.Eq)
                {
                    if (criterium.getProperty().equals("kind"))
                    {
                        kindId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("author"))
                    {
                        author = (String) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("journal"))
                    {
                        journalId = (Long) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("delivery"))
                    {
                        deliveryId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                    }
                }
                else if (criterium instanceof ReportCriteria.EqText)
                {
                    if (criterium.getProperty().equals("recipient"))
                    {
                        recipient = (String) ((ReportCriteria.EqText) criterium).getValue();
                    }
                }
                else if (criterium.getProperty().equals("ctime") && criterium instanceof ReportCriteria.Range)
                {
                    ReportCriteria.Range range = (ReportCriteria.Range) criterium;
                    if (range.getStart() != null)
                        documentDateFrom = ((Date) range.getStart());
                    if (range.getEnd() != null)
                        documentDateTo = ((Date) range.getEnd());
                }
            }


            Calendar cal = Calendar.getInstance();
            cal.setTime(documentDateFrom);
            int lp = 1;
            int totalCount = 0;
            do
            {
                FromClause from = new FromClause();
                TableAlias docTable = from.createTable(DSApi.getTableName(OutOfficeDocument.class));
                TableAlias docBase = from.createJoinedTable("ds_document",true,docTable,FromClause.JOIN,"$l.id = $r.id");

                WhereClause where = new WhereClause();

                TableAlias recipientTable = null;
                if (recipient != null)
                {
                    recipientTable = from.createTable(DSApi.getTableName(Recipient.class));

                    where.add(Expression.eqAttribute(
                        docTable.attribute("id"), recipientTable.attribute("document_id")));
                    where.add(Expression.eq(recipientTable.attribute("discriminator"), "RECIPIENT"));

                    String[] tokens = recipient.toUpperCase().split("[\\s,.;:]+");

                    if (tokens.length > 0)
                    {
                        
                        Junction JUN;
                        if (((Boolean) criteria.getAttribute("forceAnd")).booleanValue())
                            JUN = Expression.conjunction();
                        else
                            JUN = Expression.disjunction();
                        
                        for (int i=0; i < tokens.length; i++)
                        {
                            String token = tokens[i];

                            Junction OR = Expression.disjunction();
                            
                            OR.add(Expression.like(Function.upper(recipientTable.attribute("firstname")),
                                "%"+ StringUtils.substring(token, 0, 48)+"%"));
                            OR.add(Expression.like(Function.upper(recipientTable.attribute("lastname")),
                                "%"+StringUtils.substring(token, 0, 48)+"%"));
                            OR.add(Expression.like(Function.upper(recipientTable.attribute("organization")),
                                "%"+StringUtils.substring(token, 0, 48)+"%"));
                            OR.add(Expression.like(Function.upper(recipientTable.attribute("organizationdivision")),
                                "%"+StringUtils.substring(token, 0, 48)+"%"));
                            OR.add(Expression.like(Function.upper(recipientTable.attribute("street")),
                                "%"+StringUtils.substring(token, 0, 48)+"%"));
                            OR.add(Expression.like(Function.upper(recipientTable.attribute("location")),
                                "%"+StringUtils.substring(token, 0, 48)+"%"));
                            OR.add(Expression.like(Function.upper(recipientTable.attribute("zip")),
                                "%"+StringUtils.substring(token, 0, 12)+"%"));
                            
                            JUN.add(OR);
                        }

                        where.add(JUN);
                    }
                }
                else 
                	log.debug("Recipient nie wybrany");

                where.add(Expression.eq(docTable.attribute("internaldocument"), Boolean.valueOf(isInternal())));
                log.debug("internal document "+ Boolean.valueOf(isInternal())+"tratatata");

                if (author != null)
                    where.add(Expression.eq(docTable.attribute("creatingUser"), author));
                else 
                	log.debug("creating user null");

                if (kindId != null)
                    where.add(Expression.eq(docTable.attribute("kind"), kindId));
                else log.debug("kind id null tratatata");

                if (journalId != null)
                {
                	where.add(Expression.eq(docTable.attribute("journal_id"), journalId));
                	log.debug("journal id "+String.valueOf(journalId));
                }

                if (deliveryId != null)
                    where.add(Expression.eq(docTable.attribute("delivery"), deliveryId));
                else 
                	log.debug("delivery id null tratatata");

                where.add(Expression.ge(docBase.attribute("ctime"),
                    DateUtils.midnight(cal.getTime(), 0)));
                where.add(Expression.lt(docBase.attribute("ctime"),
                    DateUtils.midnight(cal.getTime(), 1)));

                SelectClause selectId = new SelectClause(true);
                SelectColumn idCol = selectId.addSql("count(*)");

                SelectQuery selectQuery = new SelectQuery(selectId, from, where, null);
                ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());

                if (rs.next())
                {
                    int count = rs.getInt(idCol.getPosition());

                    Element element = DocumentHelper.createElement("count");
                    element.addAttribute("sequenceId", String.valueOf(lp++));
                    element.addAttribute("count", String.valueOf(count));
                    element.addAttribute("date", String.valueOf(cal.getTime().getTime()));
                    element.addAttribute("date.date", DateUtils.formatCommonDate(cal.getTime()));

                    totalCount += count;

                    write(element);
                }

                rs.close();

                cal.add(Calendar.DATE, 1);
            }
            while (!cal.getTime().after(documentDateTo));

            Element element = DocumentHelper.createElement("totalcount");
            element.addAttribute("count", String.valueOf(totalCount));
            write(element);

            write("</results>");
        }
        catch (IOException e)
        {
            throw new EdmException("B��d zapisu pliku tymczasowego", e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi._close();
        }
    }
}
