package pl.compan.docusafe.service.reports;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.admin.BokAction;
import pl.compan.docusafe.web.reports.office.OutDocumentsReportAction;

/* User: Administrator, Date: 2005-08-04 14:46:46 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: IntDocumentsReport.java,v 1.7 2007/10/25 15:12:43 mmanski Exp $
 */
public class IntDocumentsReport extends IntOutDocumentsReport
{
    public static final String REPORT_ID = "intdocument";
    public static final String REPORT_VERSION = "1.0";
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(OutDocumentsReportAction.class.getPackage().getName(),null);
    public static final String TITLE = sm.getString("RejestrKorespondencjiWewnetrznej");

    public IntDocumentsReport(ReportCriteria criteria)
    {
        super(criteria);
    }

    public String getGeneratorId()
    {
        return REPORT_ID;
    }

    public String getReportTitle()
    {
        return TITLE;
    }

    public String getReportVersion()
    {
        return REPORT_VERSION;
    }

    protected boolean isInternal()
    {
        return true;
    }
    
    /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci HTML */
    public static String getHtmlXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/out-documents-html";
    }
    
    /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci PDF */
    public static String getFoXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/int-documents-fo";
    }
}
