package pl.compan.docusafe.service.reports;

import org.hibernate.HibernateException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.paa.PlanPracyMulti;
import pl.compan.docusafe.core.dockinds.paa.PlanPracyPodzad;
import pl.compan.docusafe.core.dockinds.paa.PlanPracyZad;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.admin.BokAction;

import java.io.IOException;
import java.util.List;
import java.text.SimpleDateFormat; 

public class PaaTaskReport extends ReportGenerator
{
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(BokAction.class.getPackage().getName(),null);
    public static final String REPORT_ID = "paatask";
    public static final String REPORT_VERSION = "1.0";
    public static final String TITLE = sm.getString("Raportzadan");

    private ReportCriteria criteria;

    public PaaTaskReport(ReportCriteria criteria)
    {
        super(criteria);
        this.criteria = criteria;
    }

    public String getGeneratorId()
    {
        return REPORT_ID;
    }

    public String getReportTitle()
    {
        return TITLE;
    }

    public String getReportVersion()
    {
        return REPORT_VERSION;
    }

    /** Zwraca ¶cieżkę zasobu Xsl do wygenerowania raportu w postaci HTML */
    public static String getHtmlXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/paatask-documents-html";
    }
    
    /** Zwraca ¶cieżkę zasobu Xsl do wygenerowania raportu w postaci PDF */
    public static String getFoXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/paatask-documents-fo";
    }
    
    public void createReport() throws EdmException
    {
        
        try
        {
            DSApi.openAdmin();

            // wersja raportu
            write(DocumentHelper.createElement("version").addText(REPORT_VERSION));

            // generowanie sekcji opisuj±cej kryteria tworzenia raportu
//            Element elCriteria = DocumentHelper.createElement("criteria");
//            for (Iterator iter=criteria.getCriteria().iterator(); iter.hasNext(); )
//            {
//                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();
//                Element el;
//                
//                if (criterium instanceof ReportCriteria.EqText)
//                {
//                    el = DocumentHelper.createElement("eq");
//                    el.addAttribute("fulltext", "true");
//                    el.addAttribute("property", criterium.getProperty());
//                    el.addAttribute("value", propertyValue(((ReportCriteria.EqText) criterium).getValue()));
//                }                
//                else
//                {
//                    throw new IllegalArgumentException("Nieznany typ kryterium: "+criterium);
//                }
//
//                elCriteria.add(el);
//            }
//
//            write(elCriteria);

            
            write("<results>");

//            String nazwaZadania = null;
//            
//            for (Iterator iter=criteria.getCriteria().iterator(); iter.hasNext(); )
//            {
//                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();
//
//                if (criterium instanceof ReportCriteria.EqText)
//                {
//                    if (criterium.getProperty().equals("nazwaZadania"))
//                    {
//                    	nazwaZadania = (String) ((ReportCriteria.EqText) criterium).getValue();
//                    }
//                }                
//            }

            //Long userId = new Long(0);
            //int divisionId = 0;
            //DSUser user = DSUser.findById(userId);
            //DSDivision division = DSDivision.find(divisionId);
                       
            int totalCount = 0;
            
            List<PlanPracyZad> listaZad = PlanPracyZad.findGlowne();
            
            for (PlanPracyZad planG : listaZad){

            	
            	//wy¶wietl zadanie główne

            	write(elementZadanie(planG));
            	totalCount++;
            	
            	List<PlanPracyMulti> multi = PlanPracyMulti.findPodzadania(planG.getDOCUMENT_ID());//lista ł±cz±ca zadnia do komurki wiod±cej lub podzadań
            	if (multi != null){
	            	for (PlanPracyMulti zadMulti : multi){
	            		try {
	            		PlanPracyZad zadKordynatora = PlanPracyZad.getZadanie(Long.parseLong(zadMulti.getFIELD_VAL())); 
	            		if (zadKordynatora != null){// jeżeli jest to zadanie            			            		
	            			write(elementZadanie(zadKordynatora));//wyswietl zadanie
	            			totalCount++;
	            			
	            			List<PlanPracyMulti> multiIn = PlanPracyMulti.findPodzadania(zadKordynatora.getDOCUMENT_ID());//lista ł±cz±ca zadnia do komurki wiod±cej lub podzadań
	                    	if (multiIn != null){
	        	            	for (PlanPracyMulti zadMultiIn : multiIn){
	        	            		try {
	        	            			PlanPracyPodzad podzadanieIn = PlanPracyPodzad.getPodzadanie(Long.parseLong(zadMultiIn.getFIELD_VAL()));
	        	            			write(elementPodZadanie(podzadanieIn));//wy¶wietl podzadania
	    	            				totalCount++;
	        	            		}catch (Exception e){}
	            			
	        	            	}
	                    	}
	            			
	            		} else {//wyszukaj i wy¶wietl podzadanie
	            			PlanPracyPodzad podzadanieJedno = PlanPracyPodzad.getPodzadanie(Long.parseLong(zadMulti.getFIELD_VAL()));
	            			if (podzadanieJedno != null){
	            				write(elementPodZadanie(podzadanieJedno));//wyswietl podzadanie
	            				totalCount++;
	            			}
	            		}
	            		} catch (Exception e){}       		
	            		
	            	}
            	}
            	
            	//totalCount++;
            	
            	
            }//for
            
            
            
            Element element = DocumentHelper.createElement("totalcount");
            element.addAttribute("count", String.valueOf(totalCount));
            write(element);

            write("</results>");
        }
        catch (IOException e)
        {
            throw new EdmException("Bł±d zapisu pliku tymczasowego", e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (EdmException e){
        	throw e;
        }
        finally
        {
            DSApi._close();
        }
    }
    private Element elementZadanie(PlanPracyZad zadanie){
    	Element element = DocumentHelper.createElement("count");
    	
    	String typString = " - ";
    	if (zadanie.getTYP_ZADANIA() != null){
    	int typ = zadanie.getTYP_ZADANIA().intValue();    	
    		switch (typ){
    		case 801:
    			typString = "L";
    			break;
    		case 802:
    			typString = "P";
    			break;
    		case 803:
    			typString = "C";
    			break;
    		case 804:
    			typString = "W";
    			break;
    		case 805:
    			typString = "I";
    			break;
    		}
    	}
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd.HH.mm");
    	element.addAttribute("typ", typString);
    	element.addAttribute("opis", String.valueOf(zadanie.getOPIS()));
    	element.addAttribute("dataod", String.valueOf(formatter.format(zadanie.getDATAOD())));
    	element.addAttribute("datado", String.valueOf(formatter.format(zadanie.getDATADO())));
    	element.addAttribute("stan", String.valueOf(zadanie.getSTAN()));
    	element.addAttribute("procent", String.valueOf(" - "));
    	try {
	    	int divisionId = zadanie.getKOMORKA_WIODACA().intValue();
	    	DSDivision division = DSDivision.findById(divisionId);
	    	element.addAttribute("komorka", String.valueOf(division.getName()));

    	}catch (Exception e){
    		element.addAttribute("komorka", " - ");
    	}
    	return element;
    }
    private Element elementPodZadanie(PlanPracyPodzad zadanie){
    	Element element = DocumentHelper.createElement("count");
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd.HH.mm");
    	element.addAttribute("typ", String.valueOf(" - "));
    	element.addAttribute("opis", String.valueOf(zadanie.getOPIS()));
    	element.addAttribute("dataod", String.valueOf(formatter.format(zadanie.getDATAOD())));
    	element.addAttribute("datado", String.valueOf(formatter.format(zadanie.getDATADO())));    	
    	element.addAttribute("stan", String.valueOf(zadanie.getSTAN()));
    	element.addAttribute("procent", String.valueOf(zadanie.getPROCENT()));
    	try {
	    	Long userid = zadanie.getWYKONAWCA().longValue();
	    	DSUser user = DSUser.findById(userid);
	    	element.addAttribute("komorka", String.valueOf(user.getFirstname() + " " + user.getLastname()));

    	}catch (Exception e){
    		return null;
    	}
    	return element;
    }
}
