package pl.compan.docusafe.service.reports;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.SortUtils;
import pl.compan.docusafe.util.SqlUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.admin.BokAction;

/**
 * Tworzy raport korespondencji przychodz�cej.
 * Dopuszczalne kryteria: <ul>
 * <li>eq property="kind", value=Integer (identyfikator typu pisma)
 * <li>eq property="journal" value=Long (identyfikator dziennika)
 * <li>eq property="delivery" value=Integer (identyfikator InOfficeDocumentDelivery)
 * <li>eq property="outgoingDelivery" value=Integer (identyfikator OutOfficeDocumentDelivery)
 * <li>eq property="author" value=String (identyfikator u�ytkownika tworz�cego pismo)
 * <li>range property="incomingDate" start=Date, end=Date
 * </ul>
 * Przyk�adowa posta� pliku xml:
 * <pre>
 * <report id="overduedocument">
 *  <author username="lk">�ukasz Kowalczyk</author>
 *  <date ctime="1112867511109">07-04-2005</date>
 *  <data>
 *      <version>1.0</version>
 *      <criteria>
 *          <eq property="journal" value="0" name="Dziennik g��wny"/>
 *          <eq property="kind" value="2" name="List"/>
 *          <eq property="deliery" value="1" name="Poczta"/>
 *          <eq property="outgoingDelivery" value="3" name="Kurier"/>
 *          <eq property="author" value="lk" name="�ukasz Kowalczyk"/>
 *          <range property="incomingDate" start="1112867511109" end="1112867511109"
 *              start.date="10-03-2005" end.date="11-03-2005"/>
 *      </criteria>
 *      <results>
 *          <count sequenceId="1" count="40" date="1112867511109" date.date="10-03-2004"/>
 *          <count sequenceId="2" count="12" date="1112867511109" date.date="11-03-2004"/>
 *          <totalcount count="52"/>
 *      </results>
 *  </data>
 * </report>
 * </pre>
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OverdueDocumentsReport.java,v 1.33 2009/02/05 12:17:31 wkuty Exp $
 */
public class OverdueDocumentsReport extends ReportGenerator
{
    static final Logger log = LoggerFactory.getLogger(OverdueDocumentsReport.class);
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(BokAction.class.getPackage().getName(),null);
    public static final String REPORT_ID = "overduedocument";
    public static final String REPORT_VERSION = "1.0";
    public static final String TITLE = sm.getString("RaportPismPrzeterminowanych");

    private ReportCriteria criteria;

    public OverdueDocumentsReport(ReportCriteria criteria)
    {
        super(criteria);
        this.criteria = criteria;
    }

    public String getGeneratorId()
    {
        return REPORT_ID;
    }

    public String getReportTitle()
    {
        return TITLE;
    }

    public String getReportVersion()
    {
        return REPORT_VERSION;
    }

    /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci HTML */
    public static String getHtmlXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/overdue-documents-html";
    }
    
    /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci PDF */
    public static String getFoXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/overdue-documents-fo";
    }
    
    //na podstawie guida zwraca wszystkie podlegle mu guidy 
    // (wchodzi we wszystkie galezie rekurencyjnie)
    private String[] childGuids (String guid) throws EdmException
    {
    	PreparedStatement ps = null;    	
    	log.trace("wchodzimy do childGuids");
    	try
    	{
    		ps = DSApi.context().prepareStatement("SELECT * FROM DS_DIVISION WHERE DIVISIONTYPE!='group' AND HIDDEN=?");    		    		
          ps.setBoolean(1, false);
    		// division	
            class Div implements Comparable
            {
                long id;
                String guid;
                String divisionType;
                long parentId;
				public int compareTo(Object arg) {
					Div second = (Div)arg;
					return (id<second.id)?-1:((id==second.id)?0:1);
				}
				
				private void addChildGuids(List<String> guids, Div[] divs, Long id)
				{
					for(int i=0;i<divs.length;i++)
					{
						if(divs[i].parentId==id)
						{
							guids.add(divs[i].guid);
							addChildGuids(guids,divs,divs[i].id);
						}
					}
				}
            }
    		
    		
            ResultSet rs = ps.executeQuery();
            List<Div> results = new ArrayList<Div>();
            List<String> guids = new ArrayList<String>();
            
            while (rs.next())
            {
                Div div = new Div();
                div.id = rs.getLong(1);
                div.guid = rs.getString(2);
                div.divisionType = rs.getString(5);
                div.parentId= rs.getLong(6);

                results.add(div);
            }
            rs.close();
            DSApi.context().closeStatement(ps);
            
            //przy results.toArray jest classcastexception - ?
            Div[] divs = new Div[results.size()];
            for(int i=0;i<results.size();i++)
            	divs[i] = results.get(i);
            Arrays.sort(divs);
            
            Long id=-1l;
            
            for(int i=0;i<divs.length;i++)
            {
            	log.trace(divs[i].guid);
            	if(divs[i].guid.equals(guid))
            		{
            			id = divs[i].id;
            			break;
            		}
            }
            
            
            (new Div()).addChildGuids(guids,divs,id);
            
            String[] values = new String[guids.size()];
            for(int i=0;i<guids.size();i++)
            	values[i] = guids.get(i);
            
            return values;
    	}
    	catch (SQLException e) 
    	{
    		log.error(" ",e);
			throw new EdmSQLException(e); 
		}
    	finally
    	{
    		DSApi.context().closeStatement(ps);
    	}
    }
    
    
    public void createReport() throws EdmException
    {
        //PreparedStatement ps = null;
        try
        {
            DSApi.openAdmin();
            boolean hasGuid = false ;
            // wersja raportu
            write(DocumentHelper.createElement("version").addText(REPORT_VERSION));

            // generowanie sekcji opisuj�cej kryteria tworzenia raportu
            Element elCriteria = DocumentHelper.createElement("criteria");
            
            
            for (Iterator iter=criteria.getCriteria().iterator(); iter.hasNext(); )
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();
                Element el = null;
                if (criterium instanceof ReportCriteria.Eq)
                {
                    el = DocumentHelper.createElement("eq");
                    el.addAttribute("property", criterium.getProperty());
                    el.addAttribute("value", propertyValue(((ReportCriteria.Eq) criterium).getValue()));

                    if (criterium.getProperty().equals("delivery"))
                    {
                        Integer deliveryId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                        el.addAttribute("name", InOfficeDocumentDelivery.find(deliveryId).getName());
                    }
                    else if (criterium.getProperty().equals("outgoingDelivery"))
                    {
                        Integer outgoingDeliveryId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                        el.addAttribute("name", OutOfficeDocumentDelivery.find(outgoingDeliveryId).getName());
                    }
                    else if (criterium.getProperty().equals("author"))
                    {
                        el.addAttribute("name", DSUser.safeToFirstnameLastname((String) ((ReportCriteria.Eq) criterium).getValue()));
                    }
                    else if (criterium.getProperty().equals("journalId"))
                    {
                        Journal journal =
                            Journal.find((Long) ((ReportCriteria.Eq) criterium).getValue());
                        el.addAttribute("name",
                            journal.getOwnerGuid() != null ?
                                DSDivision.find(journal.getOwnerGuid()).getName() :
                                "Dziennik g��wny");
                    }
                    else if (criterium.getProperty().equals("divisionGuid"))
                    {
                        el.addAttribute("name",
                            DSDivision.find((String) ((ReportCriteria.Eq) criterium).getValue()).getName());
                        hasGuid=true;
                    }
                }
                else if (criterium instanceof ReportCriteria.EqText)
                {
                    if (criterium.getProperty().equals("sender") ||
                        criterium.getProperty().equals("recipient"))
                    {
                        el = DocumentHelper.createElement("eq");
                        el.addAttribute("fulltext", "true");
                        el.addAttribute("property", criterium.getProperty());
                        el.addAttribute("value", propertyValue(((ReportCriteria.EqText) criterium).getValue()));
                    }
                }
                else if (criterium instanceof ReportCriteria.Range)
                {
                    el = DocumentHelper.createElement("range");
                    el.addAttribute("property", criterium.getProperty());
                    el.addAttribute("start", propertyValue(((ReportCriteria.Range) criterium).getStart()));
                    el.addAttribute("end", propertyValue(((ReportCriteria.Range) criterium).getEnd()));

                    if (criterium.getProperty().equals("incomingDate"))
                    {
                        Date start = (Date) ((ReportCriteria.Range) criterium).getStart();
                        Date end = (Date) ((ReportCriteria.Range) criterium).getEnd();
                        if (start != null)
                            el.addAttribute("start.date", DateUtils.formatCommonDate(start));
                        if (end != null)
                            el.addAttribute("end.date", DateUtils.formatCommonDate(end));
                    }
                }
                else if (criterium instanceof ReportCriteria.ValueEnumeration)
                {
                    el = DocumentHelper.createElement("enumeration");
                    el.addAttribute("property", criterium.getProperty());

                    Integer[] values = (Integer[]) ((ReportCriteria.ValueEnumeration) criterium).getValues();
                    for (int i=0; i < values.length; i++)
                    {
                        Element val = DocumentHelper.createElement("value");
                        val.setText(propertyValue(values[i]));
                        val.addAttribute("name", InOfficeDocumentKind.find(values[i]).getName());
                        el.add(val);
                    }
                }
                else
                {
                    throw new IllegalArgumentException("Nieznany typ kryterium: "+criterium.getProperty());
                }
                
                if (!hasGuid && AvailabilityManager.isAvailable("overudeDocumentsReportDefaultRootdivision"))
    			{
    				el.addAttribute("name", DSDivision.find("rootdivision").getName());
    			}

                if (el != null)
                    elCriteria.add(el);
                
               
            }
           


			
            write(elCriteria);

            write("<results>");

            String author = null;
            Date incomingDateFrom = null;
            Date incomingDateTo = null;
            Integer[] kindIds = null;
            Integer deliveryId = null;
            Integer outgoingDeliveryId = null;
            String[] senderTokens = null;
            String[] recipientTokens = null;
            Long journalId = null;
            String divisionGuid = null;

            for (Iterator iter=criteria.getCriteria().iterator(); iter.hasNext(); )
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();

                if (criterium instanceof ReportCriteria.Eq)
                {
                    if (criterium.getProperty().equals("author"))
                    {
                        author = (String) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("journal"))
                    {
                        journalId = (Long) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("delivery"))
                    {
                        deliveryId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("outgoingDelivery"))
                    {
                        outgoingDeliveryId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("journalId"))
                    {
                        journalId = (Long) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("divisionGuid"))
                    {
                        divisionGuid = (String) ((ReportCriteria.Eq) criterium).getValue();
                    }
                }
                else if (criterium instanceof ReportCriteria.EqText)
                {
                    if (criterium.getProperty().equals("sender"))
                    {
                        senderTokens = ((String) ((ReportCriteria.EqText) criterium).getValue()).toUpperCase().split("[\\s,.;:]+");
                    }
                    else if (criterium.getProperty().equals("recipient"))
                    {
                        recipientTokens = ((String) ((ReportCriteria.EqText) criterium).getValue()).toUpperCase().split("[\\s,.;:]+");
                    }
                }
                else if (criterium instanceof ReportCriteria.ValueEnumeration)
                {
                    if (criterium.getProperty().equals("kinds"))
                    {
                        kindIds = (Integer[]) ((ReportCriteria.ValueEnumeration) criterium).getValues();
                    }
                }
                else if (criterium.getProperty().equals("incomingDate") && criterium instanceof ReportCriteria.Range)
                {
                    ReportCriteria.Range range = (ReportCriteria.Range) criterium;
                    if (range.getStart() != null)
                        incomingDateFrom = ((Date) range.getStart());
                    if (range.getEnd() != null)
                        incomingDateTo = ((Date) range.getEnd());
                }
            }
            
            if (divisionGuid==null && AvailabilityManager.isAvailable("overudeDocumentsReportDefaultRootdivision"))
			{
            	divisionGuid ="rootdivision";
			}

            // domy�lnie brak ostrze�e� dla pism, kt�rych termin jeszcze
            // nie up�yn��
            int warnDays = 0;

            if (criteria.getAttribute("warnDays") != null)
                warnDays = ((Integer) criteria.getAttribute("warnDays")).intValue();

            if (warnDays < 0)
                warnDays = 0;

            // overdue document
            class OD
            {
                long id;
                int daysOverdue;
                boolean hasCase;

                String summary;
                String caseId;
                Date incomingDate;
                int officeNumber;
                String currentUser;
                String author;
                String division;
                String sender;
            }

            // dokumenty bez odpowiedzi
            StringBuilder sql = new StringBuilder();
            if(!AvailabilityManager.isAvailable("itwl"))
            	sql.append(
	                "select DISTINCT doc.id, doc.incomingdate, kind.days, doc.summary, " +
	                "doc.casedocumentid, doc.officenumber, doc.currentAssignmentUsername, " +
	                "doc.creatinguser, doc.divisionguid, " +
	                "sender.firstname, sender.lastname, sender.organization " +
	                "from dso_in_document doc left outer join dso_out_document outdoc on " +
	                "doc.id=outdoc.indocument inner join dso_in_document_kind kind on doc.kind=kind.id ");
            else
            	sql.append(
    	                "select DISTINCT doc.id, doc.incomingdate, jbpm.deadline_time , doc.summary, doc.casedocumentid, doc.officenumber, "+
            			"doc.currentAssignmentUsername, doc.creatinguser, doc.divisionguid, sender.firstname, " +
            			"sender.lastname, sender.organization  "+
            			"from dso_in_document doc left outer join dso_out_document outdoc on " +
            			"doc.id=outdoc.indocument inner join dso_in_document_kind kind on doc.kind=kind.id  "+ 
            			"inner join DSW_JBPM_TASKLIST jbpm on doc.ID = jbpm.document_id ");

            sql.append(" inner join dso_person sender on doc.sender = sender.id ");

            if (recipientTokens != null && recipientTokens.length > 0)
            {
                // je�eli tutaj jest INNER JOIN, dostaj� komunikat
                // "no current record for fetch operation"
                sql.append(" left outer join dso_person recipient on doc.id = recipient.document_id ");
            }
            
            if(DSApi.isPostgresServer()){
            	sql.append("where outdoc.id is null and doc.case_id is null and " +
		                "kind.days > 0 and " +
		                "(doc.incomingdate + CAST(kind.days || ' days' AS INTERVAL)) < ? ");
            	sql.append("and doc.replyunnecessary = FALSE ");
            }
            else{
	            if(!AvailabilityManager.isAvailable("itwl")){
		            sql.append("where outdoc.id is null and doc.case_id is null and " +
		                "kind.days > 0 and " +
		                "(doc.incomingdate + kind.days) < ? ");
	            }
	            else{
	            	sql.append("where outdoc.id is null and doc.case_id is null and " +
	    	                "jbpm.deadline_time is not null and " +
	    	                "jbpm.deadline_time < ? ");
	            }
	            sql.append("and doc.replyunnecessary = 0 ");
            }

            if (author != null)
                sql.append("and doc.creatinguser = ? ");

            if (kindIds != null && kindIds.length > 0)
            {
                sql.append("and (");
                for (int i=0; i < kindIds.length; i++)
                {
                    sql.append("doc.kind = ? ");
                    if (i < kindIds.length-1)
                        sql.append("OR ");
                }
                sql.append(") ");
            }

            if (deliveryId != null)
                sql.append("and doc.delivery = ? ");

            if (outgoingDeliveryId != null)
                sql.append("and doc.outgoingdelivery = ? ");

            sql.append("and doc.incomingdate >= ? ");
            sql.append("and doc.incomingdate <= ? ");

            
            if (senderTokens != null && senderTokens.length > 0)
            {
                sql.append("and sender.discriminator='SENDER' ");
                sql.append("and ((");

                for (int i=0; i < senderTokens.length; i++)
                {
                    
                    sql.append("upper(sender.firstname) LIKE '%"+ SqlUtils.escapeSql(senderTokens[i])+"%'");
                    sql.append(" OR ");
                    sql.append("upper(sender.lastname) LIKE '%"+ SqlUtils.escapeSql(senderTokens[i])+"%'");
                    
/*
                    sql.append(" OR ");
                    sql.append("sender.organization LIKE '%"+ SqlUtils.escapeSql(senderTokens[i])+"%'");
                    sql.append(" OR ");
                    sql.append("sender.organizationdivision LIKE '%"+ SqlUtils.escapeSql(senderTokens[i])+"%'");
                    sql.append(" OR ");
                    sql.append("sender.street LIKE '%"+ SqlUtils.escapeSql(senderTokens[i])+"%'");
                    sql.append(" OR ");
                    sql.append("sender.location LIKE '%"+ SqlUtils.escapeSql(senderTokens[i])+"%'");
                    sql.append(" OR ");
                    sql.append("sender.zip LIKE '%"+ SqlUtils.escapeSql(senderTokens[i])+"%'");
*/
                    if (i < senderTokens.length-1)
                        sql.append(" OR ");
                }
/*
                sql.append(FullLikeSearch.fts("sender",
                    new String[] { "firstname", "lastname", "organization",
                        "street", "location" },
                    senderTokens));
*/
                sql.append(") OR (");
  
                for (int i=0; i < senderTokens.length; i++)
                {
                
                    sql.append("upper(sender.organization) LIKE '%"+SqlUtils.escapeSql(senderTokens[i])+"%'");
                    if (i < senderTokens.length-1)
                        sql.append(" and ");
                }
                sql.append(")) ");
            }
         
            if (recipientTokens != null && recipientTokens.length > 0)
            {
                sql.append("and recipient.discriminator='RECIPIENT' ");
                sql.append("and (");
                for (int i=0; i < recipientTokens.length; i++)
                {
                    sql.append("recipient.firstname LIKE '%"+ SqlUtils.escapeSql(recipientTokens[i])+"%'");
                    sql.append(" OR ");
                    sql.append("recipient.lastname LIKE '%"+ SqlUtils.escapeSql(recipientTokens[i])+"%'");
                    sql.append(" OR ");
                    sql.append("recipient.organization LIKE '%"+ SqlUtils.escapeSql(recipientTokens[i])+"%'");
                    sql.append(" OR ");
                    sql.append("recipient.organizationdivision LIKE '%"+ SqlUtils.escapeSql(recipientTokens[i])+"%'");
                    sql.append(" OR ");
                    sql.append("recipient.street LIKE '%"+ SqlUtils.escapeSql(recipientTokens[i])+"%'");
                    sql.append(" OR ");
                    sql.append("recipient.location LIKE '%"+ SqlUtils.escapeSql(recipientTokens[i])+"%'");
                    sql.append(" OR ");
                    sql.append("recipient.zip LIKE '%"+ SqlUtils.escapeSql(recipientTokens[i])+"%'");
                    if (i < recipientTokens.length-1)
                        sql.append(" OR ");
                }
/*
                sql.append(FullLikeSearch.fts("recipient",
                    new String[] { "firstname", "lastname", "organization",
                        "street", "location" },
                    recipientTokens));
*/
                sql.append(") ");
            }

            if (journalId != null)
            {
                Journal journal = Journal.find(journalId);
                if (journal.getOwnerGuid() == null)
                {
                    sql.append("and doc.assignedDivision = '"+DSDivision.ROOT_GUID+"' ");
                }
                else
                {
                    sql.append("and doc.assignedDivision = '"+SqlUtils.escapeSql(journal.getOwnerGuid())+"' ");
                }
            }
            
            
            if (divisionGuid != null)
            {
            	String[] childGuids = childGuids(divisionGuid);
                sql.append("and (doc.divisionGuid = '"+SqlUtils.escapeSql(divisionGuid)+"' ");
                for(int i=0;i<childGuids.length;i++)
                	sql.append("or doc.divisionGuid = '"+SqlUtils.escapeSql(childGuids[i])+"'");
                sql.append(")");
                
            }

            log.trace(sql.toString());
            System.out.println(sql.toString());
            /**
             * @TODO - to nie jest prawdziwy PS
             */
            PreparedStatement ps = DSApi.context().prepareStatement(sql.toString());

            final long NOW = System.currentTimeMillis();

            int ppos = 1;
            //ps.setTimestamp(ppos++, new java.sql.Timestamp(NOW)); // now
            
            
            
            Date tmp =DateUtils.plusDays(DateUtils.getCurrentTime(),warnDays);
            ps.setDate(ppos++,new java.sql.Date(tmp.getTime()));
            if (author != null)
            {
                ps.setString(ppos++, author);
            }
            if (kindIds != null && kindIds.length > 0)
            {
                for (int i=0; i < kindIds.length; i++)
                {
                    ps.setInt(ppos++, kindIds[i].intValue());
                }
            }
            if (deliveryId != null)
            {
                ps.setInt(ppos++, deliveryId.intValue());
            }
            if (outgoingDeliveryId != null)
            {
                ps.setInt(ppos++, outgoingDeliveryId.intValue());
            }
            ps.setDate(ppos++, new java.sql.Date(DateUtils.midnight(incomingDateFrom, 0).getTime()));
            ps.setDate(ppos++, new java.sql.Date(DateUtils.midnight(incomingDateTo, 1).getTime()));
            
            ResultSet rs = ps.executeQuery();
            List<OD> results = new ArrayList<OD>(64);
            while (rs.next())
            {
                OD od = new OD();
                od.id = rs.getLong(1);
                od.incomingDate = new Date(rs.getTimestamp(2).getTime());
				if(!AvailabilityManager.isAvailable("itwl")){
	                int days = rs.getInt(3);
	                long due = od.incomingDate.getTime() + days * DateUtils.DAY;
	
	                od.daysOverdue = (int) Math.ceil( (due-NOW) / DateUtils.DAY );
				}else{
					Date deadline = rs.getDate(3);
	                long due = deadline.getTime();
	
	                od.daysOverdue = (int) Math.ceil( (due-NOW) / DateUtils.DAY );
				}
                od.summary = rs.getString(4);
                od.caseId = rs.getString(5);
                od.officeNumber = rs.getInt(6);

                String username = rs.getString(7);
                if (username != null)
                {
                    try
                    {
                        od.currentUser = DSUser.findByUsername(username).asLastnameFirstname();
                    }
                    catch (EdmException e)
                    {
                        od.currentUser = username;
                    }
                }

                username = rs.getString(8);
                
                try
                {
                    od.author = DSUser.findByUsername(username).asLastnameFirstname();
                }
                catch (EdmException e)
                {
                    od.author = username;
                }

                String guid = rs.getString(9);
                if (guid != null)
                {
                    try
                    {
                        DSDivision div = DSDivision.find(guid);
                        if (div.isPosition())
                            div = div.getParent();
                        od.division = div.getName();
                    }
                    catch (DivisionNotFoundException e)
                    {
                    }
                }

                od.sender = Person.getShortSummary(
                    rs.getString(10), rs.getString(11), rs.getString(12));

                results.add(od);
            }

            rs.close();
            DSApi.context().closeStatement(ps);

            // drugie zapytanie - sprawy

            sql = new StringBuilder();
            
            if(DSApi.isPostgresServer()){
            	sql.append(
	                "select distinct doc.id, c.finishdate, doc.incomingdate, doc.summary, " +
	                "doc.casedocumentid, doc.officenumber, doc.currentAssignmentUsername, " +
	                "doc.creatinguser, doc.divisionguid, "+
	                "sender.firstname, sender.lastname, sender.organization "+
	                "from dso_in_document doc inner join dso_container c on doc.case_id=c.id " +
	                "inner join dso_person sender on doc.sender = sender.id "+
	                "where c.closed = FALSE and c.discriminator='CASE' and c.finishdate is not null ");
            }
            else{
	            sql.append(
	                "select distinct doc.id, c.finishdate, doc.incomingdate, doc.summary, " +
	                "doc.casedocumentid, doc.officenumber, doc.currentAssignmentUsername, " +
	                "doc.creatinguser, doc.divisionguid, "+
	                "sender.firstname, sender.lastname, sender.organization "+
	                "from dso_in_document doc inner join dso_container c on doc.case_id=c.id " +
	                "inner join dso_person sender on doc.sender = sender.id "+
	                "where c.closed = 0 and c.discriminator='CASE' and c.finishdate is not null ");
            }
            
            sql.append("and c.finishdate-cast(current_timestamp as ");
            sql.append(DSApi.getDialect().getTypeName(Types.DATE));
            sql.append(") < ? ");

            if(DSApi.isPostgresServer()){
            	sql.append("and doc.replyunnecessary = FALSE ");
            }
            else{
            	sql.append("and doc.replyunnecessary = 0 ");
            }

            if (author != null)
                sql.append("and doc.creatinguser = ? ");

            if (kindIds != null && kindIds.length > 0)
            {
                sql.append("and (");
                for (int i=0; i < kindIds.length; i++)
                {
                    sql.append("doc.kind = ? ");
                    if (i < kindIds.length-1)
                        sql.append("OR ");
                }
                sql.append(") ");
            }

            if (deliveryId != null)
                sql.append("and doc.delivery = ? ");

            if (outgoingDeliveryId != null)
                sql.append("and doc.outgoingdelivery = ? ");

            sql.append("and doc.incomingdate >= ? ");
            sql.append("and doc.incomingdate <= ? ");

            if (journalId != null)
            {
                sql.append("and doc.assignedDivision = '"+SqlUtils.escapeSql(Journal.find(journalId).getOwnerGuid())+"' ");
            }

            if (divisionGuid != null)
            {
                //sql.append("and doc.divisionGuid = '"+SqlUtils.escapeSql(divisionGuid)+"' ");
                String[] childGuids = childGuids(divisionGuid);
                sql.append("and (doc.divisionGuid = '"+SqlUtils.escapeSql(divisionGuid)+"' ");
                for(int i=0;i<childGuids.length;i++)
                	sql.append("or doc.divisionGuid = '"+SqlUtils.escapeSql(childGuids[i])+"'");
                sql.append(")");
            }

            System.out.println(sql.toString());
            ps = DSApi.context().prepareStatement(sql.toString());

            ppos = 1;
            ps.setInt(ppos++,warnDays);

            if (author != null)
            {
                ps.setString(ppos++, author);

            }
            if (kindIds != null && kindIds.length > 0)
            {
                for (int i=0; i < kindIds.length; i++)
                {
                    ps.setInt(ppos++, kindIds[i].intValue());

                }
            }
            if (deliveryId != null)
            {
                ps.setInt(ppos++, deliveryId.intValue());

            }
            if (outgoingDeliveryId != null)
            {
                ps.setInt(ppos++, outgoingDeliveryId.intValue());

            }
            ps.setDate(ppos++, new java.sql.Date(incomingDateFrom.getTime()));
            ps.setDate(ppos++, new java.sql.Date(incomingDateTo.getTime()));
            rs = ps.executeQuery();

            while (rs.next())
            {
                OD od = new OD();
                od.hasCase = true;
                od.id = rs.getLong(1);

                long due = rs.getDate(2).getTime();

                od.daysOverdue = (int) Math.ceil( (due-NOW) / (1000.0 * 3600 * 24) );

                od.incomingDate = new Date(rs.getTimestamp(3).getTime());
                od.summary = rs.getString(4);
                od.caseId = rs.getString(5);
                od.officeNumber = rs.getInt(6);

                String username = rs.getString(7);
                if (username != null)
                {
                    try
                    {
                        od.currentUser = DSUser.findByUsername(username).asLastnameFirstname();
                    }
                    catch (EdmException e)
                    {
                        od.currentUser = username;
                    }
                }

                username = rs.getString(8);
                try
                {
                    od.author = DSUser.findByUsername(username).asLastnameFirstname();
                }
                catch (EdmException e)
                {
                    od.author = username;
                }

                String guid = rs.getString(9);
                if (guid != null)
                {
                    try
                    {
                        DSDivision div = DSDivision.find(guid);
                        if (div.isPosition())
                            div = div.getParent();
                        od.division = div.getName();
                    }
                    catch (DivisionNotFoundException e)
                    {
                    }
                }

                od.sender = Person.getShortSummary(
                    rs.getString(10), rs.getString(11), rs.getString(12));

                results.add(od);
            }

            rs.close();
            DSApi.context().closeStatement(ps);

            if (!((Boolean) criteria.getAttribute("showOverdues")).booleanValue())
            {
                for (Iterator iter=results.iterator(); iter.hasNext(); )
                {
                    if (((OD) iter.next()).daysOverdue < 0)
                        iter.remove();
                }
            }

            final String sortField = (String) criteria.getAttribute("sortField");
            // sortowanie dokument�w poczynaj�c od najbardziej op�nionych
            Collections.sort(results, new Comparator()
            {
                public int compare(Object o1, Object o2)
                {
                    OD a = (OD) o1;
                    OD b = (OD) o2;
                    int result = 0;
                    if ("officeNumber".equals(sortField))
                    {
                        result = a.officeNumber - b.officeNumber;
                    }
                    else if ("incomingDate".equals(sortField))
                    {
                        result = a.incomingDate.compareTo(b.incomingDate);
                    }
                    else if ("summary".equals(sortField))
                    {
                        result = a.summary.compareTo(b.summary);
                    }
                    else if ("caseId".equals(sortField))
                    {
                        result = SortUtils.compare(a.caseId, b.caseId);
                    }
                    else if ("currentUser".equals(sortField))
                    {
                        result = SortUtils.compare(a.currentUser, b.currentUser);
                    }
                    else if ("author".equals(sortField))
                    {
                        result = SortUtils.compare(a.author, b.author);
                    }
                    else if ("division".equals(sortField))
                    {
                        result = SortUtils.compare(a.division, b.division);
                    }

                    // zar�wno sortowanie domy�lne jak i fallback
                    if (result == 0)
                        result = ((OD) o1).daysOverdue - ((OD) o2).daysOverdue;

                    return result;
                }
            });

            for (Iterator iter=results.iterator(); iter.hasNext(); )
            {
                OD od = (OD) iter.next();

                Element element = DocumentHelper.createElement("document");
                element.addAttribute("documentId", String.valueOf(od.id));
                element.addAttribute("officeId", String.valueOf(od.officeNumber));
                element.addAttribute("date.incomingDate", DateUtils.formatCommonDate(od.incomingDate));
                element.addAttribute("summary", od.summary);
                element.addAttribute("caseId", od.caseId);
                element.addAttribute("days", String.valueOf(od.daysOverdue));
                if (od.currentUser != null)
                    element.addAttribute("currentUser", od.currentUser);
                element.addAttribute("author", od.author);
                element.addAttribute("division", od.division);
                element.addAttribute("senderSummary", od.sender);

                write(element);
            }
            write("</results>");
        }
        catch (IOException e)
        {
            throw new EdmException("B��d zapisu pliku tymczasowego", e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi._close();
        }
    }
}
