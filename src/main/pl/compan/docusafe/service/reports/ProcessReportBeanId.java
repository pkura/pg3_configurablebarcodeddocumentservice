package pl.compan.docusafe.service.reports;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
@SuppressWarnings("unused")
public class ProcessReportBeanId implements Serializable {

    private Long documentId;
    private String processName;
    private String statusCn;
    private Date start;
//    private Date end;
//    private Long duration;
//    private Long divisionId;
//    private String division;
//    private Long userId;
//    private String user;

    public ProcessReportBeanId() {
    }

    public ProcessReportBeanId(Long documentId, String processName, String statusCn, Date start, Date end, Long duration, Long divisionId, String division, Long userId, String user) {
        this.documentId = documentId;
        this.processName = processName;
        this.statusCn = statusCn;
        this.start = start;
//        this.end = end;
//        this.duration = duration;
//        this.divisionId = divisionId;
//        this.division = division;
//        this.userId = userId;
//        this.user = user;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getStatusCn() {
        return statusCn;
    }

    public void setStatusCn(String statusCn) {
        this.statusCn = statusCn;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

//    public Date getEnd() {
//        return end;
//    }
//
//    public void setEnd(Date end) {
//        this.end = end;
//    }
//
//    public Long getDuration() {
//        return duration;
//    }
//
//    public void setDuration(Long duration) {
//        this.duration = duration;
//    }
//
//    public Long getDivisionId() {
//        return divisionId;
//    }
//
//    public void setDivisionId(Long divisionId) {
//        this.divisionId = divisionId;
//    }
//
//    public String getDivision() {
//        return division;
//    }
//
//    public void setDivision(String division) {
//        this.division = division;
//    }
//
//    public Long getUserId() {
//        return userId;
//    }
//
//    public void setUserId(Long userId) {
//        this.userId = userId;
//    }
//
//    public String getUser() {
//        return user;
//    }
//
//    public void setUser(String user) {
//        this.user = user;
//    }

    @Override
    public int hashCode() {
        int result = documentId != null ? documentId.hashCode() : 0;
        result = 31 * result + (processName != null ? processName.hashCode() : 0);
        result = 31 * result + (statusCn != null ? statusCn.hashCode() : 0);
        result = 31 * result + (start != null ? start.hashCode() : 0);
//        result = 31 * result + (end != null ? end.hashCode() : 0);
//        result = 31 * result + (duration != null ? duration.hashCode() : 0);
//        result = 31 * result + (divisionId != null ? divisionId.hashCode() : 0);
//        result = 31 * result + (division != null ? division.hashCode() : 0);
//        result = 31 * result + (userId != null ? userId.hashCode() : 0);
//        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProcessReportBeanId that = (ProcessReportBeanId) o;

        if (documentId != null ? !documentId.equals(that.documentId) : that.documentId != null) return false;
        if (statusCn != null ? !statusCn.equals(that.statusCn) : that.statusCn != null) return false;
        if (start != null ? !start.equals(that.start) : that.start != null) return false;
        if (processName != null ? !processName.equals(that.processName) : that.processName != null) return false;
//        if (division != null ? !division.equals(that.division) : that.division != null) return false;
//        if (divisionId != null ? !divisionId.equals(that.divisionId) : that.divisionId != null) return false;
//        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
//        if (end != null ? !end.equals(that.end) : that.end != null) return false;
//        if (user != null ? !user.equals(that.user) : that.user != null) return false;
//        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        return true;
    }

    @Override
    public String toString() {
        return StringUtils.join(new Object[]{documentId, processName, statusCn, start/*, end, duration, divisionId, division, userId, user*/}, ' ');
    }
}
