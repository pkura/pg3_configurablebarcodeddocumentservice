package pl.compan.docusafe.service.reports;

import pl.compan.docusafe.core.users.DSUser;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ReportHandle.java,v 1.5 2005/04/25 15:34:02 lk Exp $
 */
public class ReportHandle
{
    private Long id; // identyfikator obiektu Report
    private DSUser author;
    private boolean ready;
    private Exception exception;

    public ReportHandle(Long id, DSUser author)
    {
        this.id = id;
        this.author = author;
    }

    public Long getId()
    {
        return id;
    }

    public DSUser getAuthor()
    {
        return author;
    }

    public synchronized boolean isReady()
    {
        return ready;
    }

    synchronized void setReady(boolean ready)
    {
        this.ready = ready;
    }

    public synchronized Exception getException()
    {
        return exception;
    }

    synchronized void setException(Exception exception)
    {
        this.exception = exception;
    }

    public synchronized boolean failed()
    {
        return this.exception != null;
    }
}
