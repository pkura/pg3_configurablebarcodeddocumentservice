package pl.compan.docusafe.service.reports;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.web.admin.BokAction;

/**
 * Tworzy raport korespondencji przychodz�cej.
 * Dopuszczalne kryteria: <ul>
 * <li>eq property="kind", value=Integer (identyfikator typu pisma)
 * <li>eq property="journal" value=Long (identyfikator dziennika)
 * <li>eq property="delivery" value=Integer (identyfikator InOfficeDocumentDelivery)
 * <li>eq property="outgoingDelivery" value=Integer (identyfikator OutOfficeDocumentDelivery)
 * <li>eq property="author" value=String (identyfikator u�ytkownika tworz�cego pismo)
 * <li>range property="incomingDate" start=Date, end=Date
 * </ul>
 * Przyk�adowa posta� pliku xml:
 * <pre>
 * <report id="indocuments">
 *  <author username="lk">�ukasz Kowalczyk</author>
 *  <date ctime="1112867511109">07-04-2005</date>
 *  <data>
 *      <version>1.0</version>
 *      <criteria>
 *          <eq property="journal" value="0" name="Dziennik g��wny"/>
 *          <eq property="kind" value="2" name="List"/>
 *          <eq property="deliery" value="1" name="Poczta"/>
 *          <eq property="outgoingDelivery" value="3" name="Kurier"/>
 *          <eq property="author" value="lk" name="�ukasz Kowalczyk"/>
 *          <eq property="sender" value="Kowalczyk" fulltext="true"/>
 *          <range property="incomingDate" start="1112867511109" end="1112867511109"
 *              start.date="10-03-2005" end.date="11-03-2005"/>
 *      </criteria>
 *      <results>
 *          <count sequenceId="1" count="40" date="1112867511109" date.date="10-03-2004"/>
 *          <count sequenceId="2" count="12" date="1112867511109" date.date="11-03-2004"/>
 *          <totalcount count="52"/>
 *      </results>
 *  </data>
 * </report>
 * </pre>
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InDocumentsFineReport.java,v 1.14 2008/04/17 15:44:39 pecet4 Exp $
 */
public class InDocumentsFineReport extends ReportGenerator
{
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(BokAction.class.getPackage().getName(),null);
    public static final String REPORT_ID = "indocumentfine";
    public static final String REPORT_VERSION = "1.0";
    public static final String TITLE = sm.getString("RejestrSzczegolowyKorespondencjiPrzychodzacej");

    private ReportCriteria criteria;

    public InDocumentsFineReport(ReportCriteria criteria)
    {
        super(criteria);
        this.criteria = criteria;
    }

    public String getGeneratorId()
    {
        return REPORT_ID;
    }

    public String getReportTitle()
    {
        return TITLE;
    }

    public String getReportVersion()
    {
        return REPORT_VERSION;
    }
    
    /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci HTML */
    public static String getHtmlXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/in-documents-fine-html";
    }
    
    /** Zwraca �cie�k� zasobu Xsl do wygenerowania raportu w postaci PDF */
    public static String getFoXslResource()
    {
        return "pl/compan/docusafe/web/reports/office/in-documents-fine-fo";
    }

    public void createReport() throws EdmException
    {
        //PreparedStatement ps = null;
    	ResultSet rs = null;
        try
        {
            DSApi.openAdmin();

            // wersja raportu
            write(DocumentHelper.createElement("version").addText(REPORT_VERSION));

            // generowanie sekcji opisuj�cej kryteria tworzenia raportu
            Element elCriteria = DocumentHelper.createElement("criteria");
            for (Iterator iter=criteria.getCriteria().iterator(); iter.hasNext(); )
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();
                Element el;
                if (criterium instanceof ReportCriteria.Eq)
                {
                    el = DocumentHelper.createElement("eq");
                    el.addAttribute("property", criterium.getProperty());
                    el.addAttribute("value", propertyValue(((ReportCriteria.Eq) criterium).getValue()));

                    if (criterium.getProperty().equals("kind"))
                    {
                        Integer kindId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                        el.addAttribute("name", InOfficeDocumentKind.find(kindId).getName());
                    }
                    else if (criterium.getProperty().equals("journal"))
                    {
                        Long journalId = (Long) ((ReportCriteria.Eq) criterium).getValue();
                        Journal journal = Journal.find(journalId);
                        try
                        {
                            el.addAttribute("name", (journal.getOwnerGuid() == null ? "" :
                                    DSDivision.find(journal.getOwnerGuid()).getName()+" - ")+
                                journal.getDescription());
                        }
                        catch (DivisionNotFoundException e)
                        {
                            el.addAttribute("name", journal.getDescription());
                        }
                    }
                    else if (criterium.getProperty().equals("delivery"))
                    {
                        Integer deliveryId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                        el.addAttribute("name", InOfficeDocumentDelivery.find(deliveryId).getName());
                    }
                    else if (criterium.getProperty().equals("outgoingDelivery"))
                    {
                        Integer outgoingDeliveryId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                        el.addAttribute("name", OutOfficeDocumentDelivery.find(outgoingDeliveryId).getName());
                    }
                    else if (criterium.getProperty().equals("author"))
                    {
                        el.addAttribute("name", DSUser.safeToFirstnameLastname((String) ((ReportCriteria.Eq) criterium).getValue()));
                    }
                }
                else if (criterium instanceof ReportCriteria.EqText)
                {
                    el = DocumentHelper.createElement("eq");
                    el.addAttribute("fulltext", "true");
                    el.addAttribute("property", criterium.getProperty());
                    el.addAttribute("value", propertyValue(((ReportCriteria.EqText) criterium).getValue()));
                }
                else if (criterium instanceof ReportCriteria.Range)
                {
                    el = DocumentHelper.createElement("range");
                    el.addAttribute("property", criterium.getProperty());
                    el.addAttribute("start", propertyValue(((ReportCriteria.Range) criterium).getStart()));
                    el.addAttribute("end", propertyValue(((ReportCriteria.Range) criterium).getEnd()));

                    if (criterium.getProperty().equals("incomingDate"))
                    {
                        Date start = (Date) ((ReportCriteria.Range) criterium).getStart();
                        Date end = (Date) ((ReportCriteria.Range) criterium).getEnd();
                        if (start != null)
                            el.addAttribute("start.date", DateUtils.formatCommonDate(start));
                        if (end != null)
                            el.addAttribute("end.date", DateUtils.formatCommonDate(end));
                    }
                }
                else
                {
                    throw new IllegalArgumentException("Nieznany typ kryterium: "+criterium);
                }

                elCriteria.add(el);
            }

            write(elCriteria);


            write("<results>");

            String author = null;
            Date incomingDateFrom = null;
            Date incomingDateTo = null;
            Integer kindId = null;
            Long journalId = null;
            Integer deliveryId = null;
            Integer outgoingDeliveryId = null;
            String sender = null;

            for (Iterator iter=criteria.getCriteria().iterator(); iter.hasNext(); )
            {
                ReportCriteria.Criterium criterium = (ReportCriteria.Criterium) iter.next();

                if (criterium instanceof ReportCriteria.Eq)
                {
                    if (criterium.getProperty().equals("kind"))
                    {
                        kindId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("author"))
                    {
                        author = (String) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("journal"))
                    {
                        journalId = (Long) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("delivery"))
                    {
                        deliveryId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                    }
                    else if (criterium.getProperty().equals("outgoingDelivery"))
                    {
                        outgoingDeliveryId = (Integer) ((ReportCriteria.Eq) criterium).getValue();
                    }
                }
                else if (criterium instanceof ReportCriteria.EqText)
                {
                    if (criterium.getProperty().equals("sender"))
                    {
                        sender = (String) ((ReportCriteria.EqText) criterium).getValue();
                    }
                }
                else if (criterium.getProperty().equals("incomingDate") && criterium instanceof ReportCriteria.Range)
                {
                    ReportCriteria.Range range = (ReportCriteria.Range) criterium;
                    if (range.getStart() != null)
                        incomingDateFrom = ((Date) range.getStart());
                    if (range.getEnd() != null)
                        incomingDateTo = ((Date) range.getEnd());
                }
            }
 
            FromClause from = new FromClause();
            TableAlias docTable = from.createTable(DSApi.getTableName(InOfficeDocument.class));
            WhereClause where = new WhereClause();

            TableAlias senderTable = from.createTable(DSApi.getTableName(Sender.class));
            where.add(Expression.eqAttribute(
                docTable.attribute("sender"), senderTable.attribute("id")));
            where.add(Expression.eq(senderTable.attribute("discriminator"), "SENDER"));

            if (sender != null)
            {
                String[] tokens = sender.toUpperCase().split("[\\s,.;:]+");

                if (tokens.length > 0)
                {
                    Junction OR = Expression.disjunction();
              
                    Junction JUN;
                    if (((Boolean) criteria.getAttribute("forceAnd")).booleanValue())
                        JUN = Expression.conjunction();
                    else
                        JUN = Expression.disjunction();
                    for (int i=0; i < tokens.length; i++)
                    {
                        String token = tokens[i];

                        Junction OR2 = Expression.disjunction();
                        OR2.add(Expression.like(Function.upper(senderTable.attribute("firstname")),
                            "%"+ StringUtils.substring(token, 0, 48)+"%"));
                        OR2.add(Expression.like(Function.upper(senderTable.attribute("lastname")),
                            "%"+StringUtils.substring(token, 0, 49)+"%"));
                        JUN.add(OR2);
                        /*
                        OR.add(Expression.like(Function.upper(senderTable.attribute("organization")),
                            "%"+StringUtils.substring(token, 0, 49)+"%"));
                        OR.add(Expression.like(Function.upper(senderTable.attribute("organizationdivision")),
                            "%"+StringUtils.substring(token, 0, 49)+"%"));
                        OR.add(Expression.like(Function.upper(senderTable.attribute("street")),
                            "%"+StringUtils.substring(token, 0, 49)+"%"));
                        OR.add(Expression.like(Function.upper(senderTable.attribute("location")),
                            "%"+StringUtils.substring(token, 0, 49)+"%"));
                        OR.add(Expression.like(Function.upper(senderTable.attribute("zip")),
                            "%"+StringUtils.substring(token, 0, 13)+"%"));
*/
                    }
                    OR.add(JUN);
                    
                    Junction AND = Expression.conjunction();
                    for (int i=0; i < tokens.length; i++)
                    {
                        String token = tokens[i];
                        AND.add(Expression.like(Function.upper(senderTable.attribute("organization")),
                            "%"+StringUtils.substring(token, 0, 49)+"%"));
                    }
                    OR.add(AND);

                    where.add(OR);
                }
            }

            if (author != null)
                where.add(Expression.eq(docTable.attribute("creatingUser"), author));

            if (kindId != null)
                where.add(Expression.eq(docTable.attribute("kind"), kindId));

            if (journalId != null)
                where.add(Expression.eq(docTable.attribute("journal_id"), journalId));

            if (deliveryId != null)
                where.add(Expression.eq(docTable.attribute("delivery"), deliveryId));

            if (outgoingDeliveryId != null)
                where.add(Expression.eq(docTable.attribute("outgoingdelivery"), outgoingDeliveryId));

            if (incomingDateFrom != null)
               where.add(Expression.ge(docTable.attribute("incomingdate"), DateUtils.midnight(incomingDateFrom, 0)));

            if (incomingDateTo != null)
               where.add(Expression.le(docTable.attribute("incomingdate"), DateUtils.midnight(incomingDateTo, 1)));

            
 
            // incomingdate = cal.getTime()
/*
            where.add(Expression.ge(docTable.attribute("incomingdate"),
                DateUtils.midnight(cal.getTime(), 0)));
            where.add(Expression.lt(docTable.attribute("incomingdate"),
                DateUtils.midnight(cal.getTime(), 1)));
*/

            SelectClause select = new SelectClause(true);
            SelectColumn colId = select.add(docTable, "id");
            SelectColumn colSummary = select.add(docTable, "summary");
            SelectColumn colDate = select.add(docTable, "documentdate");
            SelectColumn colIncomingDate = select.add(docTable, "incomingdate");
            SelectColumn colKO = select.add(docTable, "officenumber");
            SelectColumn colGuid = select.add(docTable, "divisionguid");
           
            SelectColumn colBarcode = null;
            if(AvailabilityManager.isAvailable("add.barcode.on.inDocumentFineReport"))
             colBarcode = select.add(docTable, "barcode");
            
            SelectColumn colpostalregnumber = null;
            if(AvailabilityManager.isAvailable("add.postalregnumber.on.inDocumentFineReport"))
            	colpostalregnumber = select.add(docTable, "POSTALREGNUMBER");
               
            
            SelectColumn colSndAnon = select.add(senderTable, "anonymous");
            SelectColumn colSndTitle = select.add(senderTable, "title");
            SelectColumn colSndFirstname = select.add(senderTable, "firstname");
            SelectColumn colSndLastname = select.add(senderTable, "lastname");
            SelectColumn colSndOrganization = select.add(senderTable, "organization");
            SelectColumn colSndStreet = select.add(senderTable, "street");
            SelectColumn colSndLocation = select.add(senderTable, "location");

            SelectQuery selectQuery = new SelectQuery(select, from, where, null);
            rs = selectQuery.resultSet(DSApi.context().session().connection());

            int totalCount = 0;
            while (rs.next())
            {
                long id = rs.getLong(colId.getPosition());

                Element element = DocumentHelper.createElement("document");
                element.addAttribute("lp", String.valueOf(totalCount+1));
                element.addAttribute("documentId", String.valueOf(id));
                element.addAttribute("summary", rs.getString(colSummary.getPosition()));
                java.sql.Date sqlIncomingDate = rs.getDate(colIncomingDate.getPosition());
                if (sqlIncomingDate != null)
                {
                    element.addAttribute("incomingDate", String.valueOf(sqlIncomingDate.getTime()));
                    element.addAttribute("incomingDate.date", DateUtils.formatCommonDate(sqlIncomingDate));
                }
                java.sql.Date sqlDate = rs.getDate(colDate.getPosition());
                if (sqlDate != null)
                {
                    element.addAttribute("date", String.valueOf(sqlDate.getTime()));
                    element.addAttribute("date.date", DateUtils.formatCommonDate(sqlDate));
                }
                element.addAttribute("officeNumber", String.valueOf(rs.getInt(colKO.getPosition())));
                if(AvailabilityManager.isAvailable("add.barcode.on.inDocumentFineReport"))
                    element.addAttribute("barcode", rs.getString(colBarcode.getPosition()));
                   
                if(AvailabilityManager.isAvailable("add.postalregnumber.on.inDocumentFineReport"))
                	     element.addAttribute("postalregnumber", rs.getString(colpostalregnumber.getPosition()));
              
                String guid = rs.getString(colGuid.getPosition());
                if (guid != null && !guid.contains("null"))
                {
                    element.addAttribute("division", guid);
                    element.addAttribute("division.name", DSDivision.find(guid).getName());
                }
                element.addAttribute("senderSummary", Person.getSummary(       
                	rs.getBoolean(colSndAnon.getPosition()),           
                    rs.getString(colSndTitle.getPosition()),
                    rs.getString(colSndFirstname.getPosition()),
                    rs.getString(colSndLastname.getPosition()),
                    rs.getString(colSndOrganization.getPosition()),
                    rs.getString(colSndStreet.getPosition()),
                    rs.getString(colSndLocation.getPosition())));

                totalCount++;

                write(element);
            }

            rs.close();

            Element element = DocumentHelper.createElement("totalcount");
            element.addAttribute("count", String.valueOf(totalCount));
            write(element);

            write("</results>");
        }
        catch (IOException e)
        {
            throw new EdmException(sm.getString("BladZapisuPlikuTymczasowego"), e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi._close();
        }
    }
}
