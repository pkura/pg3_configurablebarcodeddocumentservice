package pl.compan.docusafe.service.reports.dp;

import java.io.File;
import java.sql.PreparedStatement;
import java.util.Date;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.PDFDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class ContractManagmentReport extends Report{

    UniversalTableDumper dumper = new UniversalTableDumper();
    private Date dateFrom;
    private Date dateTo;


    private void initValues() throws Exception {
        for (ReportParameter param : getParams()) {
            if (param.getType().equals("break-line")) {
                continue;
            }

            if (param.getFieldCn().equals("dateFrom") && param.getValue() != null) {
                dateFrom = DateUtils.jsDateFormat.parse((String) param.getValue());
            }

            if (param.getFieldCn().equals("dateTo") && param.getValue() != null) {
                dateTo = DateUtils.jsDateFormat.parse((String) param.getValue());
            }

        }
    }

    private String getSQL() {
        StringBuilder sql = new StringBuilder();

        sql.append("SELECT usr.FIRSTNAME as imie, usr.LASTNAME as nazwisko, usr.NAME as nazwausera, cm.contract_date as datazamowienia, dpinst.name as kontrahent, dp.DATA_UMOWY as dataumowy, ");
        sql.append("cm.return_date as datazwrotu, cm.note as uwaga ");
        sql.append("FROM dso_contract_managment as cm, DSG_PRAWNY as dp, DS_USER as usr, dp_inst as dpinst ");
        sql.append("WHERE cm.document_id = dp.DOCUMENT_ID AND cm.user_id = usr.ID AND dpinst.id = dp.INST ");
        
        if(dateFrom != null)
            sql.append("AND CONVERT(DATETIME, dp.DATA_UMOWY, 111)  >= CONVERT(DATETIME, ?, 111) ");
        if(dateTo != null)
            sql.append("AND CONVERT(DATETIME, dp.DATA_UMOWY, 111)  <= CONVERT(DATETIME, ?, 111) ");
        
        sql.append("AND (dp.orginal_attachment = 1)");

        return sql.toString();
    }

    @Override
    public void doReport() throws Exception {
        sm = StringManager.getManager(this.getClass().getPackage().getName());
        LoggerFactory.getLogger("lukaszl").debug("ContractManagmentReport");
        
        try {
            this.initValues();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /** DUMPER **/
//        XlsPoiDumper poiDumper = new XlsPoiDumper();
        CsvDumper csvDumper = new CsvDumper();
        PDFDumper pdfDumper = new PDFDumper();

//        poiDumper.openFile(new File(this.getDestination(), "raport.xls"));
//        dumper.addDumper(poiDumper);

        csvDumper.openFile(new File(this.getDestination(), "raport.csv"));
        dumper.addDumper(csvDumper);

        pdfDumper.openFile(new File(this.getDestination(), "raport.pdf"));
        dumper.addDumper(pdfDumper);

        //Naglowek
        setHeaders();

        PreparedStatement ps = null;

        try {
            int count = 0;
            ps = DSApi.context().prepareStatement(getSQL());
            
            if(dateFrom != null)
                ps.setDate(++count, new java.sql.Date(dateFrom.getTime()));
            if(dateTo != null)
                ps.setDate(++count, new java.sql.Date(dateTo.getTime()));

            ResultSet rs = ps.executeQuery();
            int counter = 0;
            while (rs.next()) 
               addRecordLine(rs, ++counter);
            

            rs.close();

        } catch (Exception e) {
            throw new EdmException(e);
            //e.printStackTrace();
        } finally {
            DSApi.context().closeStatement(ps);
        }

        dumper.closeFileQuietly();
    }

    private void setHeaders() throws IOException {
        dumper.newLine();
        dumper.addText(sm.getString("Lp."));
        dumper.addText(sm.getString("Zamawiajacy"));
        dumper.addText(sm.getString("DataZamowienia"));
        dumper.addText(sm.getString("Kontrahent"));
        dumper.addText(sm.getString("DataUmowy"));
        dumper.addText(sm.getString("DataZwrotu"));
        dumper.addText(sm.getString("Uwagi"));
        dumper.dumpLine();
    }

    private void addRecordLine(ResultSet rs, int counter) throws SQLException, IOException {
        dumper.newLine();
        dumper.addInteger(counter);
        
        String zamawiajacy = rs.getString("imie") + " " + rs.getString("nazwisko") + " ("+ rs.getString("nazwausera")+")";
        Date dataZamowienia = rs.getDate("datazamowienia");
        String kontrahent = rs.getString("kontrahent");
        Date dataUmowy = rs.getDate("dataumowy");
        Date dataZwrotu = rs.getDate("datazwrotu");
        String uwaga = rs.getString("uwaga");

        dumper.addText(zamawiajacy);
        dumper.addDate(dataZamowienia, "dd-MM-yyyy");
        dumper.addText(kontrahent);
        dumper.addDate(dataUmowy, "dd-MM-yyyy");
        dumper.addDate(dataZwrotu, "dd-MM-yyyy");
        dumper.addText(uwaga);
        dumper.dumpLine();
    }
}
