package pl.compan.docusafe.service.reports.dp;

import java.io.File;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.PDFDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.SqlUtils;

/**
 * Klasa dla raport�w CRU
 * @
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class ContractItemReport extends Report {

    UniversalTableDumper dumper = new UniversalTableDumper();
    private Date dateFrom;
    private Date dateTo;
    private Set<String> contractItems = new HashSet<String>();

    private void initValues() throws Exception {
        for (ReportParameter param : getParams()) {
            if (param.getType().equals("break-line")) {
                continue;
            }

            if (param.getFieldCn().equals("dateFrom") && param.getValue() != null) {
                dateFrom = DateUtils.jsDateFormat.parse((String) param.getValue());
            }

            if (param.getFieldCn().equals("dateTo") && param.getValue() != null) {
                dateTo = DateUtils.jsDateFormat.parse((String) param.getValue());
            }

            if (param.getFieldCn().equals("contractItems") && param.getValue() != null) {
                contractItems.addAll(param.valuesAsList());
            }

        }
    }

    public String getSQL() {
        StringBuilder sql = new StringBuilder();

        sql.append("SELECT dp.DATA_UMOWY as dataumowy, pu.title as przedmiotumowy ,dpinst.name as kontrahent ");
        sql.append("FROM DSG_PRAWNY as dp left join dsg_przedmiot_umowy as pu on dp.przedmiot_umowy = pu.id,  dp_inst as dpinst ");
        sql.append("WHERE DP.INST = dpinst.id ");
        
        if(dateFrom != null)
            sql.append("AND CONVERT(DATETIME, dp.DATA_UMOWY, 111)  >= CONVERT(DATETIME, ?, 111) ");
        if(dateTo != null)
            sql.append("AND CONVERT(DATETIME, dp.DATA_UMOWY, 111)  <= CONVERT(DATETIME, ?, 111) ");
        
        sql.append("AND (dp.KLASA = 10 ) ");
        sql.append("AND (dp.orginal_attachment = 1) ");
        
        log.error("wielkosc" + contractItems.size());
        
        if(!contractItems.isEmpty())
            sql.append(String.format("AND (dp.przedmiot_umowy IN (%s))", SqlUtils.questionMarks(contractItems.size())));
        else
            sql.append("AND (dp.przedmiot_umowy is NULL ) ");
            
        return sql.toString();
    }

    @Override
    public void doReport() throws Exception {
        LoggerFactory.getLogger("lukaszl").debug("ContractManagmentReport");

        try {
            this.initValues();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        initializeDumpers();
        //Naglowek
        setHeaders();
        PreparedStatement ps = null;

        try {
            int count = 0;
            ps = DSApi.context().prepareStatement(getSQL());
            if(dateFrom != null)
                ps.setDate(++count, new java.sql.Date(dateFrom.getTime()));
            if(dateTo != null)
                ps.setDate(++count, new java.sql.Date(dateTo.getTime()));
            
            if(!contractItems.isEmpty())
                for(String contractItem : contractItems)
                    ps.setInt(++count, Integer.valueOf(contractItem));
            
            ResultSet rs = ps.executeQuery();
            int counter = 0;
            while (rs.next()) {
                addRecordLine(rs, ++counter);
            }

            rs.close();

        } catch (Exception e) {
            throw new EdmException(e);
            //e.printStackTrace();
        } finally {
            DSApi.context().closeStatement(ps);
        }

        dumper.closeFileQuietly();
    }

    public void initializeDumpers() throws IOException{
        /** DUMPER **/
//        XlsPoiDumper poiDumper = new XlsPoiDumper();
        CsvDumper csvDumper = new CsvDumper();
        PDFDumper pdfDumper = new PDFDumper();

//        poiDumper.openFile(new File(this.getDestination(), "raport.xls"));
//        dumper.addDumper(poiDumper);

        csvDumper.openFile(new File(this.getDestination(), "raport.csv"));
        dumper.addDumper(csvDumper);

        pdfDumper.openFile(new File(this.getDestination(), "raport.pdf"));
        dumper.addDumper(pdfDumper);
    }

    public void setHeaders() throws IOException {
        dumper.newLine();
//        dumper.addText("Lp.");
        dumper.addText(sm.getString("DataZawarciaRozwi�zaniaUmowy"));
        dumper.addText(sm.getString("PrzedmiotUmowy"));
        dumper.addText(sm.getString("Kontrahent"));
        dumper.dumpLine();
    }

    public void addRecordLine(ResultSet rs, int counter) throws IOException, SQLException {
        dumper.newLine();
//        dumper.addInteger(counter);
        dumper.addDate(rs.getDate("dataumowy"), "dd-MM-yyyy");
        dumper.addText(rs.getString("przedmiotumowy"));
        dumper.addText(rs.getString("kontrahent"));
        dumper.dumpLine();
    }
}
