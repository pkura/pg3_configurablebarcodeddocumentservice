package pl.compan.docusafe.service.reports;

import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceException;

/* User: Administrator, Date: 2005-04-25 14:17:42 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Report.java,v 1.2 2006/02/20 15:42:29 lk Exp $
 */
public interface Report extends Service
{
    public static final String NAME = "report";

    public ReportHandle generateReport(String generatorId, ReportCriteria criteria,
                                       String username)
        throws ServiceException;

}
