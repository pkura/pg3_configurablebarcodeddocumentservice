package pl.compan.docusafe.service.reports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.reports.Report;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.reports.dockind.*;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.XmlUtils;

import java.io.*;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ReportGenerator.java,v 1.28 2010/06/25 11:59:01 pecet1 Exp $
 */
public abstract class ReportGenerator extends Thread
{
    private static final Log log = LogFactory.getLog(ReportGenerator.class);
	private static Map<String,Class> reportGenerators = new LinkedHashMap<String, Class>();
    
    private OutputStream output;
    private ReportHandle reportHandle;


	private ReportCriteria criteria;

    protected ReportGenerator(ReportCriteria criteria)
    {
        super("report");
        this.criteria = criteria;
    }

    protected ReportCriteria getReportCriteria()
    {
        return criteria;
    }

    public abstract String getGeneratorId();
    public abstract String getReportTitle();
    public abstract String getReportVersion();
    
    /**
     * Metoda generuj�ca xml zawieraj�cy tre�� raportu.
     * @throws pl.compan.docusafe.core.EdmException
     */
    public abstract void createReport() throws EdmException;

    /**
     * Tworzy raport do pliku csv i xml zawirajacy tylko kryteria
     */
    public void createCSVReport() throws EdmException {
   	 throw new EdmException("CSV Report not supported for this document kind");
    }
    
    /**
     * Uruchamia oddzielny w�tek, w kt�rym tworzony jest obiekt
     * Report. Zwracany jest obiekt ReportHandle, kt�ry mo�e
     * by� u�yty do obserwowania statusu tworzenia raportu.
     * @return
     */

    
    public ReportHandle createHandle(String username) throws ServiceException
    {
        CreateHandleThread createHandleThread = new CreateHandleThread(username);
        createHandleThread.start();
        try
        {
            createHandleThread.join();
        }
        catch (InterruptedException e)
        {
            if (createHandleThread.getReportHandle() == null)
                throw new ServiceException("Przerwano dzia�anie w�tku", e);
        }

        reportHandle = createHandleThread.getReportHandle();

        if (reportHandle == null)
        {
            if (createHandleThread.getException() != null)
                throw new ServiceException("B��d podczas tworzenia raportu", createHandleThread.getException());
            else
                throw new ServiceException("B��d podczas tworzenia raportu");
        }

        return reportHandle;
    }

    private class CreateHandleThread extends Thread
    {
        private ReportHandle reportHandle;
        private String username;
        private Throwable exception;

        public CreateHandleThread(String username)
        {
            this.username = username;
        }

        public void run()
        {
            try
            {
                DSApi.openAdmin();
                DSApi.context().begin();

                log.info("Tworzenie obiektu Report ("+getGeneratorId()+")");

                DSUser user = DSUser.findByUsername(username);

                Report report = new Report(getGeneratorId(), getReportVersion(), user.getName(), getReportTitle());
                report.setStatus(Report.STATUS_PENDING);
                report.create();

                DSApi.context().commit();

                reportHandle = new ReportHandle(report.getId(), user);
            }
            catch (Throwable e)
            {
                DSApi.context().setRollbackOnly();
                this.exception = e;
            }
            finally
            {
                DSApi._close();
            }
        }

        public synchronized ReportHandle getReportHandle()
        {
            if (isAlive())
                throw new IllegalStateException("Przed pobraniem obiektu ReportHandle " +
                    "nale�y poczeka� na zako�czenie pracy w�tku");

            return reportHandle;
        }

        public Throwable getException()
        {
            return exception;
        }
    }

    public void run()
    {
        if (reportHandle == null)
            throw new IllegalStateException("Nie wywo�ano metody createHandle() lub metoda " +
                "ta zako�czy�a si� b��dem.");

        Exception exception = null;
        File file = null;
        try
        {
            file = File.createTempFile("docusafe_report_", ".tmp");
            output = new BufferedOutputStream(new FileOutputStream(file));

            writeXMLProlog();

            log.info("Tworzenie raportu");
            
            
           
            if ((this instanceof AbstractDockindReport)&&((AbstractDockindReport)this).getReport().getType().equals(pl.compan.docusafe.core.dockinds.Report.CSV_ONLY)) {
            	LogFactory.getLog("kuba").debug("robimy csv");
            	createCSVReport();
            } else {
            	LogFactory.getLog("kuba").debug("robimy zwykly");
            	createReport();
            }
            

            writeXMLEpilog();
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            exception = e;
/*
            reportHandle.setException(e);
            synchronized (reportHandle)
            {
                reportHandle.setReady(true);
                reportHandle.notifyAll();
            }
            return;
*/
        }

        try
        {
            if (output != null) output.close();
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }

        PreparedStatement ps = null;
        InputStream is = null;
        try
        {
            DSApi.openAdmin();
            DSApi.context().begin();

            if (exception == null)
            {
                is = new FileInputStream(file);

                ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(Report.class)+
                    " set contentdata = ?, status = ? where id = ?");
                ps.setBinaryStream(1, is, (int) file.length());
                ps.setString(2, Report.STATUS_DONE);
                ps.setLong(3, reportHandle.getId().longValue());

                int rows = ps.executeUpdate();

                if (rows != 1)
                    throw new EdmException("Nie znaleziono raportu nr "+reportHandle.getId()+
                        ", nie mo�na zapisa� danych");
            }
            else
            {
                byte[] xml = getErrorReport(exception);
                is = new ByteArrayInputStream(xml);

                ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(Report.class)+
                    " set contentdata = ?, status = ? where id = ?");
                ps.setBinaryStream(1, is, xml.length);
                ps.setString(2, Report.STATUS_FAILED);
                ps.setLong(3, reportHandle.getId().longValue());

                int rows = ps.executeUpdate();

                if (rows != 1)
                    throw new EdmException("Nie znaleziono raportu nr "+reportHandle.getId()+
                        ", nie mo�na zapisa� danych");
            }

            DSApi.context().commit();
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            DSApi.context().setRollbackOnly();
        }
        finally
        {
            DSApi.context().closeStatement(ps);
            org.apache.commons.io.IOUtils.closeQuietly(is);
            DSApi._close();
        }

        synchronized (reportHandle)
        {
            reportHandle.setReady(true);
            reportHandle.notifyAll();
        }

        log.info("Utworzono raport");
    }

    protected void writeXMLProlog() throws IOException
    {
/*
        output.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n".getBytes("iso-8859-1"));
        output.write(("<report id=\""+getGeneratorId()+"\">").getBytes("utf-8"));

        Date now = new Date();

        output.write(("<author username=\""+reportHandle.getAuthor().getName()+"\">" +
            reportHandle.getAuthor().asFirstnameLastname()+"</author>").getBytes("utf-8"));
        output.write(("<date time=\""+now.getTime()+"\">"+DateUtils.formatCommonDate(now)+"</date>").getBytes("utf-8"));

        // tutaj mo�na doda� og�lne dane generowane dla ka�dego raportu

        output.write("<data>".getBytes("utf-8"));
*/
        getXmlProlog(getGeneratorId(), reportHandle.getAuthor().getName(),
            reportHandle.getAuthor().asFirstnameLastname(), new Date(), output);
    }

    protected void writeXMLEpilog() throws IOException
    {
//        output.write("</data></report>".getBytes("utf-8"));
        getXmlEpilog(output);
    }

    protected void getXmlProlog(String reportId, String authorUsername,
                                  String author, Date ctime, OutputStream stream)
        throws IOException
    {
        stream.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n".getBytes("iso-8859-1"));
        stream.write(("<report id=\""+reportId+"\" name=\""+getReportTitle()+"\">").getBytes("utf-8"));
        stream.write(("<author username=\""+authorUsername+"\">"+author+"</author>").getBytes("utf-8"));
        stream.write(("<date time=\""+ctime.getTime()+"\">"+DateUtils.formatCommonDate(ctime)+"</date>").getBytes("utf-8"));
        // tutaj mo�na doda� og�lne dane generowane dla ka�dego raportu
        stream.write("<data>".getBytes("utf-8"));
    }

    protected void getXmlEpilog(OutputStream stream) throws IOException
    {
        stream.write("</data></report>".getBytes("utf-8"));
    }

    protected byte[] getErrorReport(Exception exception) throws IOException
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream(2048);
        getXmlProlog(getGeneratorId(), reportHandle.getAuthor().getName(),
            reportHandle.getAuthor().asFirstnameLastname(), new Date(), os);
        os.write(("<message>"+XmlUtils.escapeXml(exception.getMessage())+"</message>").getBytes("utf-8"));
        StackTraceElement[] trace = exception.getStackTrace();
        for (int i=0; i < trace.length; i++)
        {
            os.write(("<trace>"+XmlUtils.escapeXml(trace[i].toString())+"</trace>").getBytes("utf-8"));
        }
        getXmlEpilog(os);
        return os.toByteArray();
    }

    protected static String propertyValue(Object value)
    {
        if (value == null)
            return "";
        else if (value instanceof Date)
            return String.valueOf(((Date) value).getTime());
        else
            return value.toString();
    }

    protected void write(org.dom4j.Element element) throws IOException
    {
        output.write(element.asXML().getBytes("utf-8"));
    }

    protected void write(String string) throws IOException
    {
        if (string == null) return;
        output.write(string.getBytes("utf-8"));
    }
    
    /**
     * Dla danego identyfikatora raportu, zwraca klas� implementujac� generator tego typu raport�w.
     */
    public static Class getReportGenerator(String reportId)
    {
        return reportGenerators.get(reportId);
    }
    
    /**
     * Dla danego identyfikatora raportu i w�a�ciwo�ci tego raportu, funkcja ta warto�� tej w�a�ciwo�ci dla tego raportu
     * - poprzez wykonanie statycznej metody na generatorze tego typu raport�w, kt�ra zwraca warto��
     * tej w�a�ciwo�ci.
     * Obecnie dost�pne w�a�ciwo�ci to: "htmlXslResource", "foXslResource".
     */
    public static String getReportProperty(String reportId, String property)
    {
        Class c = reportGenerators.get(reportId);
        String methodName = "get"+(property.substring(0,1).toUpperCase())+property.substring(1);
        try
        {
            Method method = c.getMethod(methodName, new Class[]{});
            Object result = method.invoke(null, new Object[]{});
            if (result == null)
                return null;
            if (!(result instanceof String))
                throw new IllegalStateException("Wynik metody '"+methodName+"' w klasie: "+c.getName()+" nie jest typu String");
            return (String) result;
        }
        catch (NoSuchMethodException e)
        {
            // gdy nie istnieje odpowiednia metoda to zak�adam, �e warto�� danej w�a�ciwo�ci r�wna null
            return null;
        }
        catch (Exception e)
        {
            throw new IllegalStateException("B��d podczas wo�ania metody '"+methodName+"' na klasie: "+c.getName(), e);
        }
    }
        
    // REJESTRACJA RAPORT�W
    static 
    {
        // raporty og�lne
        reportGenerators.put(InDocumentsFineReport.REPORT_ID, InDocumentsFineReport.class);
        reportGenerators.put(InDocumentsFineReportUTP.REPORT_ID, InDocumentsFineReportUTP.class);
        reportGenerators.put(IntDocumentsFineReport.REPORT_ID, IntDocumentsFineReport.class);
        reportGenerators.put(OutDocumentsFineReport.REPORT_ID, OutDocumentsFineReport.class);
        reportGenerators.put(InDocumentsReport.REPORT_ID, InDocumentsReport.class);
        reportGenerators.put(PaaTaskReport.REPORT_ID, PaaTaskReport.class);
        reportGenerators.put(OutDocumentsReport.REPORT_ID, OutDocumentsReport.class);
        reportGenerators.put(IntDocumentsReport.REPORT_ID, IntDocumentsReport.class);
        reportGenerators.put(OverdueDocumentsReport.REPORT_ID, OverdueDocumentsReport.class);
        reportGenerators.put(MultiChangeReport.REPORT_ID, MultiChangeReport.class);
        reportGenerators.put(ProcessReport.REPORT_ID, ProcessReport.class);
        //raporty dla obslugi umow
//        reportGenerators.put(ContractManagmentReport.REPORT_ID, ContractManagmentReport.class);
        // raporty dla mechanizmu dockinda
        reportGenerators.put(AssignedDocumentsCountReport.REPORT_ID, AssignedDocumentsCountReport.class);
        reportGenerators.put(TasklistDocumentsCountReport.REPORT_ID, TasklistDocumentsCountReport.class);
        reportGenerators.put(AverageTimeOnTasklistReport.REPORT_ID, AverageTimeOnTasklistReport.class);
        reportGenerators.put(TasklistDocumentsReport.REPORT_ID, TasklistDocumentsReport.class);
        reportGenerators.put(ProcesedDocumentsReport.REPORT_ID, ProcesedDocumentsReport.class);
        reportGenerators.put(DocumentsReport.REPORT_ID, DocumentsReport.class);
        reportGenerators.put(UserAcceptedInvoicesIC.REPORT_ID, UserAcceptedInvoicesIC.class);
        reportGenerators.put(DiscardedInvoicesIC.REPORT_ID, DiscardedInvoicesIC.class);
        reportGenerators.put(FakturyKosztoweNW.REPORT_ID, FakturyKosztoweNW.class);
        reportGenerators.put(RockwellReport.REPORT_ID, RockwellReport.class);
    }
    
    
    public ReportHandle getReportHandle() {
		return reportHandle;
	}
    
}
