package pl.compan.docusafe.service.ocr;

import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.FullTextSearchField;
import pl.compan.docusafe.core.ocr.OcrUtils;
import pl.compan.docusafe.lucene.IndexStatus;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.util.*;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Serwis do ocrowania za��cznik�w przez zewn�trzny system.
 *
 * Klasa oczekuje na zocrowane za��czniki w wybranych katalogach.
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class OcrFileService extends ServiceDriver implements Service {
    private final static Logger LOG = LoggerFactory.getLogger(OcrFileService.class);
    private final static String NAME = "ocr-file-service";

    private final static Set<String> SUPPORTED_EXTENSIONS = new HashSet<String>(Arrays.asList("doc", "pdf", "txt"));

    private final static FileFilter OCR_SUPPORTED_FILES = new FileFilter() {
        public boolean accept(File file) {
            return file.isFile()
                && SUPPORTED_EXTENSIONS.contains(OcrUtils.fileExtension(file));
        }
    };

    private Timer timer;
    /**
     * Zbi�r obserwowanych katalog�w
     */
    private Set<File> ocrDirs = new HashSet<File>();

    //====================
    //  Service Methods
    //====================

    public static OcrFileService getService() throws ServiceException {
        return (OcrFileService) ServiceManager.getService(NAME);
    }

    /**
     * Dodaje katalog z ocrowanymi plikami do obserwowanych przez ten serwis
     * @param directory
     */
    public synchronized void addWatchedDirectory(File directory){
        checkNotNull(directory, "directory cannot be null");
        checkArgument(directory.isDirectory(), "directory must be existing directory");
        LOG.info("directory submitted : {}", directory);

        ocrDirs.add(directory);
    }

    //====================
    //  Service Driver
    //====================

    /**
     * Zwraca kopie zbioru obserwowanych katalog�w
     * @return
     */
    private synchronized List<File> cloneWatchedDirectories(){
        return new ArrayList<File>(ocrDirs);
    }

    private final TimerTask SERVICE_TASK = new TimerTask() {
        @Override
        public void run() {
            List<File> dirs = cloneWatchedDirectories();
            LOG.debug("checking dirs : {}", dirs);
            for(File dir: dirs){
                //dla ka�dego zocrowanego pliku
                for(File file : dir.listFiles(OCR_SUPPORTED_FILES)){
                    long attachmentRevId = revisionFromFilename(file.getName());
                    LOG.error("ocred file found : '{}' for attachmentRevision {}", file, attachmentRevId);
                    addOcrAttachment(attachmentRevId, file);
                }
            }
        }
    };

    private void addOcrAttachment(long attachmentRevId, File file) {
        try {
            DSApi.openAdmin();
            DSApi.context().begin();
            AttachmentRevision nonOcrrev = AttachmentRevision.find(attachmentRevId);
            Attachment nonOcrAtt = nonOcrrev.getAttachment();
            if(nonOcrrev.getIndexStatus() == IndexStatus.SEND_TO_OCR)
            {
	            AttachmentRevision.find(attachmentRevId).getAttachment().getDocument().createAttachment(nonOcrAtt);
	            AttachmentRevision ocrRev = nonOcrAtt.createRevision(file, true);
	            for (Field field : nonOcrAtt.getDocument().getDocumentKind().getDockindInfo().getDockindFields())
	            {
					if(Field.FULL_TEXT_SEARCH.equals(field.getType()))
					{
						FullTextSearchField f = (FullTextSearchField) field;
						updateIndex(f.getIndexColumn(), nonOcrAtt.getDocument(), collectTextFromAttachment(ocrRev));
					}
				}
	            ocrRev.setIndexStatus(IndexStatus.RETURNED_FROM_OCR);
	            boolean fileDeleted = file.delete();
	            if(!fileDeleted){
	                throw new IllegalStateException("Nie uda�o si� skasowa� pliku : " + file.getAbsolutePath());
	            }
            }
            else
            {
	            Attachment att = new Attachment(nonOcrAtt.getTitle() + "[OCR]");
	            AttachmentRevision.find(attachmentRevId).getAttachment().getDocument().createAttachment(att);
	            att.createRevision(file, true);
	            boolean fileDeleted = file.delete();
	            if(!fileDeleted){
	                throw new IllegalStateException("Nie uda�o si� skasowa� pliku : " + file.getAbsolutePath());
	            }
            }
            DSApi.context().commit();
        } catch (Exception ex) {
            DSApi.context().setRollbackOnly();
            LOG.error(ex.getMessage(), ex);
        } finally {
            DSApi._close();
        }
    }
    
    private static CharSequence collectTextFromAttachment(AttachmentRevision ar) throws EdmException {
        LOG.info("collectTextFromAllAttachments");
        StringBuilder sb = new StringBuilder();
        Tika tika = new Tika();            
        if(ar.getIndexStatus() == IndexStatus.INDEXING_NOT_SUPPORTED){
           return sb;
        }
        try {
            sb.append(IOUtils.toString(tika.parse(ar.getBinaryStream())));
            ar.setIndexStatus(IndexStatus.INDEXED);
        } catch (IOException ex) {
            LOG.error(ex.getMessage(), ex);
            ar.setIndexStatus(IndexStatus.INDEXING_NOT_SUPPORTED);
        }
        
        return sb;
    }
    
    private void updateIndex(String column, Document document, Object sb) throws EdmException
    {
        PreparedStatement ps = null;
        try {
            String sql = "update " + document.getDocumentKind().getTablename() + " set " + column + " = ? where document_id = ? ";
            LOG.info("sql = \n'{}'\ncollected text = \n'{}'", sql, sb);
            ps = DSApi.context().prepareStatement(sql);
            ps.setString(1, sb.toString());
            ps.setLong(2, document.getId());
            int updated = ps.executeUpdate();
            assert updated > 0;
        } catch(Exception ex) {
            throw new EdmException(ex); 
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }

    private long revisionFromFilename(String fileName) {
        return Long.valueOf(fileName.substring(0,fileName.indexOf('_')));
    }

    @Override
    protected void start() throws ServiceException {
        timer = new Timer("ocr-file-service-thread", true);
        timer.scheduleAtFixedRate(SERVICE_TASK, 0, 1 * DateUtils.MINUTE);
    }

    @Override
    protected void stop() throws ServiceException {
        timer.cancel();
    }

    @Override
    protected boolean canStop() {
        return false;
    }
}
