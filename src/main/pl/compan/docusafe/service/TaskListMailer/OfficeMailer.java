package pl.compan.docusafe.service.TaskListMailer;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListCounterManager;
import pl.compan.docusafe.core.office.workflow.snapshot.UserCounter;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.p4.P4Helper;
import pl.compan.docusafe.service.*;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import std.fun;

import java.util.*;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 *
 */
public class OfficeMailer extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(OfficeMailer.class);

	private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);
	private Property[] properties;
	private String mailTimes;
	private String[] times;
	
    class MailTimesProperty extends Property
    {
        public MailTimesProperty()
        {
            super(SIMPLE, PERSISTENT, OfficeMailer.this, "mailTimes", "Godziny powiadomie� (np:13:00,16:00)", String.class);
        }

        protected Object getValueSpi()
        {
            return mailTimes;
        } 

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (OfficeMailer.this)
            {
            	mailTimes = (String) object;
            	if(mailTimes != null)
            		times = mailTimes.split(",");
                else
                    times = new String[] {"10:00", "14:00"};
            }
        }
    }


	public OfficeMailer()
	{
		properties = new Property[] { new MailTimesProperty() };
	}

	protected boolean canStop()
	{
		return true;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi officeMailer");
		timer = new Timer(true);
		timer.schedule(new MailSender(), (long) 2*1000 ,(long) 5*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("SystemPowiadamianiaWystartowal"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}

	class MailSender extends TimerTask
	{
		TaskList taskList;
		
		private void sendMail(DSUser user) throws EdmException
		{
			DSApi.context().begin();
			if(user.getEmail() == null || user.getEmail().length() < 1)
			{
				console(Console.ERROR, "Brak adresu email dla u�ytkownika "+user.getName());
			}
				
			log.debug("wysylka maila do uzytkownika {}",user.getName());
			UserCounter uc = null;
			try
			{
				TaskListCounterManager mgr = new TaskListCounterManager(false);
				uc = mgr.getUserCounter(user.getName(),DSApi.context().systemPreferences().node("taskListMailer").getLong(user.getName(), 0));
			}
			catch(EdmException e)
			{
				OfficeMailer.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
            if(uc.getAllTask() > 0)
            {
                log.trace("!!!! uc.getAllTask(): {} dla: {}", uc.getAllTask(), user);
                StringBuilder msg = new StringBuilder();
                List<TaskSnapshot> incomingTasks = getUserTasksByType(user, InOfficeDocument.TYPE);
                List<TaskSnapshot> outTasks = getUserTasksByType(user, OutOfficeDocument.TYPE);
                List<TaskSnapshot> internalTasks = getUserTasksByType(user, OutOfficeDocument.INTERNAL_TYPE);
                int acceptedTasks = 0;
                int overduedTasks = 0;
                for (TaskSnapshot oneTask : incomingTasks)
                {
                    log.trace("TASK.documentSummary: {}, TASK.documentId: {}", oneTask.getDocumentSummary(), oneTask.getDocumentId());
                    log.trace("TASK.getDivisionGuid: {}, TASK.getAssigned_user: {}", oneTask.getDivisionGuid(), oneTask.getAssigned_user());
                    msg.append(oneTask.getDocumentSummary()+"\n");
                    if(oneTask.getDeadlineTime()!=null && oneTask.getDeadlineTime().compareTo(new Date())<0)
                        overduedTasks++;
                    if(oneTask.isAccepted())
                        acceptedTasks++;
                }

                for (TaskSnapshot oneTask : outTasks)
                {
                    log.trace("TASK.documentSummary: {}, TASK.documentId: {}", oneTask.getDocumentSummary(), oneTask.getDocumentId());
                    log.trace("TASK.getDivisionGuid: {}, TASK.getAssigned_user: {}", oneTask.getDivisionGuid(), oneTask.getAssigned_user());
                    msg.append(oneTask.getDocumentSummary()+"\n");
                    if(oneTask.getDeadlineTime()!=null && oneTask.getDeadlineTime().compareTo(new Date())<0)
                        overduedTasks++;
                    if(oneTask.isAccepted())
                        acceptedTasks++;
                }

                for (TaskSnapshot oneTask : internalTasks)
                {
                    log.trace("TASK.documentSummary: {}, TASK.documentId: {}", oneTask.getDocumentSummary(), oneTask.getDocumentId());
                    log.trace("TASK.getDivisionGuid: {}, TASK.getAssigned_user: {}", oneTask.getDivisionGuid(), oneTask.getAssigned_user());
                    msg.append(oneTask.getDocumentSummary()+"\n");
                    if(oneTask.getDeadlineTime()!=null && oneTask.getDeadlineTime().compareTo(new Date())<0)
                        overduedTasks++;
                    if(oneTask.isAccepted())
                        acceptedTasks++;
                }


                Map<String, Object> ctext = new HashMap<String, Object>();
                ctext.put("imie", user.getFirstname());
                ctext.put("nazwisko", user.getLastname());
                ctext.put("n",uc.getAllTask());
                ctext.put("incomingTasksCount",incomingTasks.size());
                ctext.put("outTasksCount",outTasks.size());
                ctext.put("internalTasksCount",internalTasks.size());
                ctext.put("msg", msg.toString());
                ctext.put("czas", DateUtils.formatCommonTime(new Date()));
                ctext.put("zadaniaOverdue", overduedTasks);
                ctext.put("zadaniaNew", uc.getAllTask()-acceptedTasks);
                ctext.put("zadaniaOld", acceptedTasks);
                ctext.put("link", Docusafe.getBaseUrl()+"/office/tasklist/user-task-list.action");
                try
                {
                    OfficeMailer.log.error(msg.toString());
                    ((Mailer) ServiceManager.getService(Mailer.NAME)).
                            send(user.getEmail(), user.getEmail(), null, Configuration.getMail(Mail.OFFICE_MAILER), ctext,false);
                    console(Console.INFO, "Wysylam mail do "+user.getName()+" na email "+user.getEmail()+" czas "+new Date());
                    //	console(Console.INFO, msg.toString());
                }
                catch(Exception e)
                {
                    OfficeMailer.log.error("",e);
                    console(Console.ERROR, "B��d [ "+e+" ]");
                }
                if(!AvailabilityManager.isAvailable("itwl")){
                    WorkflowFactory wff = WorkflowFactory.getInstance();
                    for (Task oneTask : incomingTasks)
                    {
                        WorkflowFactory.getInstance().manualFinish(wff.keyToId(oneTask.getActivityKey()), false);
                        TaskSnapshot.updateByDocumentId(oneTask.getDocumentId(), "anyType");
                    }
                }
                uc.setLastSent(new Date());
			}
			else
			{
				log.debug("Brak korespondencji dla " + user.getName());
			}
			log.debug("KONIEC WYSYLKI DLA "+user.getName());
			DSApi.context().commit();
		}

        /**
         * Metoda sprawdza
         * @param user
         * @return
         */
        private boolean checkIfSendOptionIsNotAvailableForThisUser(DSUser user)
        {
            if (AvailabilityManager.isAvailable("officeMailer.checkPermisionForUser"))
            {
                try
                {
                    return !P4Helper.journaGuidOwner(user).equals(P4Helper.TASMOWA_GUID);
                    //return !DSApi.context().hasPermission(user, DSPermission.OFFICE_MAILER_POWIADOMIENIA);
                }
                catch (EdmException e)
                {
                    log.error(e.getMessage(), e);
                    return false;
                }
            }
            else
                return false;
        }

        /**
         * Metoda sprawdz czy dla danego typu dokumentu {@link pl.compan.docusafe.core.dockinds.DocumentKind},
         * kt�rego dotyczy zadanie (oneTask) jest zablokwana opisa powiadomien o nowej pozycje na li�cie zada�.
         * List� dockind�w dla kt�rych nie jest mo�liwe wys�anie powiadomienie, okre�la si� w pliku adds.properites,
         * poprzez parameter notAvailableDockinds (cn dockind�w przedziolone ','), np.: parcel_registery,normal_out
         * @param oneTask
         * @since
         * @return
         * @throws EdmException
        */
        private boolean checkIfSendOptionIsNotAvailableForThisDockind(TaskSnapshot oneTask) throws EdmException
        {
            DocumentKind kind = Document.find(oneTask.getDocumentId()).getDocumentKind();
            if (kind != null)
            {
                String notAvailableDockinds = Docusafe.getAdditionProperty("officeMailer.notAvailableDockinds");
                String [] notAvailableDockindsTable = notAvailableDockinds.split(",");
                List<String> notAvailableDockindsList = Arrays.asList(notAvailableDockindsTable) ;
                if (notAvailableDockinds!= null && notAvailableDockindsList.contains(kind.getCn()))
                {
                    return true;
                }
            }
            return false;
        }

        private List<TaskSnapshot> getUserTasksByType(DSUser user, String officeDocumentType) throws EdmException
        {
            TaskSnapshot.Query query;
            query = new TaskSnapshot.Query();
            List<Task> tasks = null;
            query.setUser(user);
            if (officeDocumentType != null)
                query.setDocumentType(officeDocumentType);
            SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
            return fun.list(results);
        }
		
//		private Long getUserOverdueTasksCount(String user) throws EdmException 
//		{
//			StringBuilder sql = new StringBuilder();
//			sql.append("SELECT COUNT(*) " +
//					" FROM DSW_JBPM_TASKLIST " + 
//					" where assigned_resource = ? " + 
//					" and deadline_time < ? ");
//  
//			PreparedStatement ps = null;
//			ResultSet rs = null;
//			try {
//				ps = DSApi.context().prepareStatement(sql.toString());
//				int ppos = 1;
//				ps.setString(ppos++, user);
//	            Date tmp = DateUtils.getCurrentTime();
//	            ps.setDate(ppos++,new java.sql.Date(tmp.getTime()));
//	            rs = ps.executeQuery();
//	            while (rs.next())
//	            {          	
//	            	return rs.getLong(1);
//	            }
//			} catch (SQLException e) {
//				log.error("B��d podczas pobrania ilo�ci zada� przeterminowanych do maila " + e.getStackTrace());
//			} finally{
//				 
//		         DSApi.context().closeStatement(ps);
//		         try {
//					rs.close();
//				} catch (SQLException e) {
//					log.error("B��d podczas pobrania ilo�ci zada� przeterminowanych do maila " + e.getStackTrace());
//				}
//			}
//	        return null;
//		}

		/**
		 * Czy nalezy wyslac maila
		 * @return
		 */
		private boolean shouldSend()
		{
			Date now = new Date();
			Long lastActivityTime = DSApi.context().systemPreferences().node("officeMailer").getLong("lastActivityTime", 0);
			Calendar lastActivityDate = new GregorianCalendar();
			lastActivityDate.setTime(new Date(lastActivityTime));
			ArrayList<Calendar> sendTimes = new ArrayList<Calendar>();
            if(times == null || times.length==0){
                console(Console.ERROR, "Brak ustawionej godziny powiadomie�");
                log.error("Brak ustawionej godziny powiadomie�");
                return false;
            }
			for (String time : times)
			{
				String[] timeTab = time.split(":");
				int hour = Integer.parseInt(timeTab[0]);
				int minut = Integer.parseInt(timeTab[1]);
				Calendar cal = Calendar.getInstance();
				cal.set(GregorianCalendar.HOUR_OF_DAY,hour);
				cal.set(GregorianCalendar.MINUTE,minut);
				sendTimes.add(cal);
			}
			for (Calendar calendar : sendTimes) 
			{
				if(calendar.getTime().before(now) && lastActivityDate.getTime().before(calendar.getTime()))
				{ 
					console(Console.INFO, "WYSYLAM");
					return true;
				}
			}
			return false;
		}

		public void run()
		{
			try
			{
				console(Console.INFO, "Sprawdzam czy wyslac");
				DSApi.openAdmin();
				if(shouldSend())
				{
					DSApi.context().systemPreferences().node("officeMailer").putLong("lastActivityTime", new Date().getTime());
					taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
					for (DSUser user : DSUser.list(DSUser.SORT_NAME))
					{
						sendMail(user);
					} 
				}
                else
                {
                    console(Console.INFO, "Nie wysy�am");
                }
			}
			catch(Exception e)
			{
				OfficeMailer.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			finally
			{
				log.trace("Petla powiadomienia STOP");
				DSApi._close();
			}
		}
	}

	public Property[] getProperties() {
		return properties;
	}

	public void setProperties(Property[] properties) {
		this.properties = properties;
	}
}
