package pl.compan.docusafe.service.TaskListMailer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;

public class ScheduleTaskListMailDriver extends ServiceDriver implements Service
{
	private Timer timer;
	private final static StringManager sm = 
		GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);
	private List<DSUser> DSUsers = new ArrayList<DSUser>();
	private Boolean trueMail;
	private Boolean label;
	
	private static final Log log = LogFactory.getLog(ScheduleTaskListMailDriver.class);
	
	public ScheduleTaskListMailDriver() 
	{
	}
	
	protected boolean canStop() {
		return false;
	}

	protected void start() throws ServiceException {

		String timeFromProperty = Docusafe.getAdditionProperty("mailTime");
		if ("false".equalsIgnoreCase(timeFromProperty)
				|| timeFromProperty == null)
			return;

		label = "true".equals(Docusafe.getAdditionProperty("scheduleTaskList.label"));

		timer = new Timer(true);

		String timesFromProperties[] = timeFromProperty.split(";");
		for (String s : timesFromProperties) {
			try {
				Date sendDateTime = new Date();

				sendDateTime.setHours(Integer.parseInt(s.trim().split(":")[0]));
				sendDateTime.setMinutes(Integer.parseInt(s.trim().split(":")[1]));

				if (sendDateTime.before(new Date())) {
					sendDateTime.setDate(sendDateTime.getDate() + 1);
				}

				if (label) {
					timer.schedule(new SendTaskListLabelState(), sendDateTime, (long) 24 * DateUtils.HOUR);
				} else {
					timer.schedule(new SendTaskListState(), sendDateTime, (long) 24 * DateUtils.HOUR);
				}
			} catch (Exception e) {
				log.error("", e);
				// log.debug(new EdmException(e));
				return;
			}
		}

	}

	protected void stop() throws ServiceException 
	{
	}

	public boolean hasConsole()
	{
		return true;
	}
}
