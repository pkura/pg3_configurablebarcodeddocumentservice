package pl.compan.docusafe.service.TaskListMailer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;

class SendTaskListState extends TimerTask
{
	//private static final Log log = LogFactory.getLog(SendTaskListState.class);
	private static final Logger log = LoggerFactory.getLogger(SendTaskListState.class);
	
	private Boolean sendMail(String username) throws Exception
	{
		DSUser user = DSUser.findByUsername(username);
		
		int allTasks = 0;
		int newTasks = 0;
		
		TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);				
		for(Task task : taskList.getTasks(user))
		{
			if(!task.isAccepted())
			{
				newTasks++;
			}
			allTasks++;
		}			
		
		if("true".equals(Docusafe.getAdditionProperty("scheduleTaskList.sendAlways")))
		{
			if(allTasks<1)
				return false;
		}
		else if(newTasks < 1)
			return false;
		
		Map<String, Object> ctext = new HashMap<String, Object>();
		ctext.put("imie", user.getFirstname());
		ctext.put("nazwisko", user.getLastname());
		ctext.put("czas", DateUtils.formatCommonTime(new Date()));
		ctext.put("zadaniaAll", allTasks);
		ctext.put("zadaniaNew", newTasks);
		ctext.put("zadaniaOld", (int)(allTasks-newTasks));
		ctext.put("link", Docusafe.getBaseUrl());
		
		log.trace("Wysylam mail do uzytkowniaka "+user.getName()+" na e-mail "+user.getEmail());
		
		((Mailer) ServiceManager.getService(Mailer.NAME)).send(user.getEmail(), user.getEmail(), null, Configuration.getMail(Mail.TASK_LIST_MAILER), ctext);
		

		return true;
	}
	
	private List<String> getUsers()
	{
		List<String> returnList = new ArrayList<String>();
		PreparedStatement ps = null;		
		try {
			ps = DSApi.context().prepareStatement("select name from ds_user with (nolock) where DELETED = 0 and systemuser = '0' and EMAIL is not null;");
			ResultSet rs =  ps.executeQuery();
			
			while(rs.next())
			{
				returnList.add(rs.getString("name"));
			}
			
			rs.close();
		}
		catch(Exception e)
		{
			log.error(e.getMessage(),e);			
		}
		finally
		{
			DSApi.context().closeStatement(ps);			
		}
		
		
		return returnList;
	}
	
	public void run() 
	{
		
		GregorianCalendar gc = new GregorianCalendar();
		if(gc.get(GregorianCalendar.DAY_OF_WEEK) == 1 || gc.get(GregorianCalendar.DAY_OF_WEEK) == 7)			
			return;
			
		try
		{
			DSApi.openAdmin();
			
			
			for(String username : getUsers())					
			{
				try
				{
					/*if(user.getEmail() != null)
					{
						log.trace("Wysylam mail do uzytkowniaka "+user.getName()+" na e-mail "+user.getEmail());
						sendMail(user);
					}
					else
					{
						log.trace("Nie wysylam maila do uzytkownika "+user.getName()+" brak adresu e-mail");
					}*/
					sendMail(username);
				}
				catch(Exception e)
				{
					log.error(e.getMessage(),e);
				}
			}			
		} 
		catch(Exception e)
		{
			log.error("",e);			
		}
		finally
		{
			DSApi._close();
		}
	}
}
