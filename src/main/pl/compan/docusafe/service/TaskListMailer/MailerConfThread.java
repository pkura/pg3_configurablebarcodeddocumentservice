package pl.compan.docusafe.service.TaskListMailer;

import java.security.Principal;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.security.auth.Subject;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.RolePrincipal;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class MailerConfThread implements Runnable
{
	private Logger log = LoggerFactory.getLogger(MailerConfThread.class);
	private Boolean useTaskListMailer;
	private Integer timeMail;
	private List<DSUser> users;
    private Integer termKind;
    private Integer day;
    private String time;
    private Integer dayOfMonth;

	public MailerConfThread(Boolean useTaskListMailer ,Integer timeMail,Integer termKind,Integer day,String time,Integer dayOfMonth)
	{
		this.useTaskListMailer = useTaskListMailer;
		this.timeMail = timeMail;
		this.termKind = termKind;
		this.day = day;
		this.time = time;
		this.dayOfMonth = dayOfMonth;
		try
		{
			this.users = DSUser.list(0);
		}
		catch (Exception e)
		{
			this.users = Collections.EMPTY_LIST;
		}
	}

	public MailerConfThread(Boolean useTaskListMailer ,Integer timeMail, List<DSUser> dsusers,Integer termKind,Integer day,String time,Integer dayOfMonth)
	{
		this.useTaskListMailer = useTaskListMailer;
		this.timeMail = timeMail;
		this.users = dsusers;
		this.termKind = termKind;
		this.day = day;
		this.time = time;
		this.dayOfMonth = dayOfMonth;
	}

	public void run()
	{
		try
		{
			for(DSUser user : users)
			{
				Set<Principal> principals = new HashSet<Principal>();
		        principals.add(new UserPrincipal(user.getName()));
		        principals.add(new RolePrincipal("admin"));
		        Subject subject = new Subject(true, principals, Collections.emptySet(), Collections.emptySet());
		        DSContext ctx = DSApi.open(subject);

				if(user.getEmail() != null && !user.isDeleted())
				{
					try
					{
						ctx.begin();
						ctx.userPreferences().node("taskListMailer").putBoolean("use", useTaskListMailer);
						ctx.userPreferences().node("taskListMailer").putInt("timeMail", timeMail);
						ctx.userPreferences().node("taskListMailer").putInt("termKind", termKind);
						ctx.userPreferences().node("taskListMailer").putInt("dayOfWeek", day);
						ctx.userPreferences().node("taskListMailer").put("time", time);
						ctx.userPreferences().node("taskListMailer").putInt("dayOfMonth", dayOfMonth);
						ctx.userPreferences().node("taskListMailer").flush();
						if(useTaskListMailer)
		                {
							ctx.systemPreferences().node("taskListMailer").putLong(user.getName(), 0);
		                }
						else
		                {
		                	ctx.systemPreferences().node("taskListMailer").remove(user.getName());
		                	ctx.systemPreferences().node("taskListMailer").remove(user.getExternalName());
		                }
						ctx.commit();

					}
					catch (EdmException e)
					{
						log.error("",e);
						DSApi.context().setRollbackOnly();
					}
					DSApi.close();
				}
			}
		}
		catch (Exception e)
		{
			log.error("",e);
		}
	}
}
