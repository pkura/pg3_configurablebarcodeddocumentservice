package pl.compan.docusafe.service.TaskListMailer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;

class SendTaskListLabelState extends TimerTask
{
	private static final Log log = LogFactory.getLog(SendTaskListLabelState.class);
	
	@SuppressWarnings("unchecked")
	private Boolean sendMail(DSUser user) throws Exception
	{
		LoggerFactory.getLogger("mariusz").debug("Wysy�a info o etykietach do "+user.getName());
		List<MsgBean> msgBeans = new ArrayList<MsgBean>();
		TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
		List<Label> allLabels = LabelsManager.getSystemLabels(Label.LABEL_TYPE);
		for (Label label : allLabels) 
		{
			String contractorNames = null;
			List<Label> labels = new ArrayList<Label>();
			labels.add(label);
			Map map = taskList.getDocumentTasks(null, user, InOfficeDocument.TYPE, null, "documentId", true, 0, 5,null,null,false,labels,true,null);
			Integer tasksSize = ((Integer) map.get("size")).intValue();
			//if(label.getId().intValue() == 3)
		//	{
				contractorNames = getContractorNames(user.getName(),label.getId().intValue());
		//	}
			
			if(tasksSize > 0)
				msgBeans.add(new MsgBean(tasksSize,label.getName(),tasksSize,contractorNames));
		}
		if(msgBeans.size() < 1)
			return false;
		String msg = getMsg(msgBeans);	
		LoggerFactory.getLogger("mariusz").debug("Wysy�a info o etykietach msg =  "+msg);
		Map<String, Object> ctext = new HashMap<String, Object>();
		ctext.put("imie", user.getFirstname());
		ctext.put("nazwisko", user.getLastname());
		ctext.put("czas", DateUtils.formatCommonTime(new Date()));
		ctext.put("msg", msg);
		ctext.put("link", Docusafe.getBaseUrl());	
		((Mailer) ServiceManager.getService(Mailer.NAME)).send(user.getEmail(), user.getEmail(), null, Configuration.getMail(Mail.TASK_LIST_MAILER_LABEL), ctext);
		return true;
	}
	
	private String getContractorNames(String username,Integer labelId)
    {
		String contractorNames = "";
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement("select task.dockindbusinessatr2 from dsw_tasklist task, dso_document_to_label label where " +
    				" task.dso_document_id = label.document_id and task.dsw_resource_res_key = ? and task.dockindname = 'Zadanie Marketingowe' " +
    				" and label.label_id = ? and rownum < 51 ");
    		ps.setString(1, username);
    		ps.setInt(2, labelId);
    		rs = ps.executeQuery();
    		while (rs.next()) 
    		{
				contractorNames = contractorNames + rs.getString(1) + "\n";
			}
    	}
    	catch (Exception e) 
    	{
			log.error(e.getMessage(),e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DbUtils.closeQuietly(ps);
    	}
    	return contractorNames;
    }
	
	private String getMsg(List<MsgBean> beans)
	{
		String tmp = "";
		for (MsgBean bean : beans)
		{
			if(bean.getAllTasks() > 0)
				tmp += bean.getAllTasks() +" zada� z etykiet� "+bean.getLable()+" \n";
				if(bean.getContractorNames() != null && 
						! "false".equals(Docusafe.getAdditionProperty("column.dockindBusinessAtr2")))
				{
					//tmp = tmp + "Opisanych parametrem " + Docusafe.getAdditionProperty("column.dockindBusinessAtr2") + ":";
					tmp += bean.getContractorNames();
				}
		}
		log.debug("SendTaskListLabelState - getMsg: Tre�� wiadomo�ci: " + tmp );
		return tmp;
	}
	
	class MsgBean
	{
		public int allTasks = 0;
		public int newTasks = 0;
		public String label;
		public String contractorNames;
		
		public MsgBean(int allTasks, String lable, int newTasks, String contractorNames) 
		{
			
			super();
			this.setAllTasks(allTasks);
			this.setLable(lable); 
			this.setNewTasks(newTasks);
			this.setContractorNames(contractorNames);
		}
		public void setAllTasks(int allTasks) {
			this.allTasks = allTasks;
		}
		public int getAllTasks() {
			return allTasks;
		}
		public void setNewTasks(int newTasks) {
			this.newTasks = newTasks;
		}
		public int getNewTasks() {
			return newTasks;
		}
		public void setLable(String lable) {
			this.label = lable;
		}
		public String getLable() {
			return label;
		}
		public int getOldTask()
		{
			return allTasks - newTasks;
		}
		public void setContractorNames(String contractorNames) {
			this.contractorNames = contractorNames;
		}
		public String getContractorNames() {
			return contractorNames;
		}
	}
	
	public void run() 
	{
		
		GregorianCalendar gc = new GregorianCalendar();
		if(gc.get(GregorianCalendar.DAY_OF_WEEK) == 1 || gc.get(GregorianCalendar.DAY_OF_WEEK) == 7)			
			return;
			
		try
		{
			DSApi.openAdmin();
			
			
			for(DSUser user : DSUser.list(0))					
			{
				if(user.getEmail() != null )
				{
					sendMail(user);
				}
			}
			DSApi.context().commit();
		} 
		catch(Exception e)
		{
			log.debug(new EdmException(e));
		}
		finally
		{
			DSApi._close();
		}
	}
}
