package pl.compan.docusafe.service.TaskListMailer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListCounterManager;
import pl.compan.docusafe.core.office.workflow.snapshot.UserCounter;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:tomasz.lipka@com-pan.pl">Tomasz Lipka</a>
 *
 */
public class TaskListMailer extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(TaskListMailer.class);

	public static Integer TERM_KIND_CO_ILE_MINUT = 1;
	public static Integer TERM_KIND_CO_ILE_GODZIN = 2;
	public static Integer TERM_KIND_RAZ_DZIENNIE = 3;
	public static Integer TERM_KIND_RAZ_W_TYGODNIU = 4;
	public static Integer TERM_KIND_RAZ_W_MIESIACU = 5;


	private Timer timer;
	private final static StringManager sm =
		GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);
	private String[] users;
	private List<DSUser> DSUsers = new ArrayList<DSUser>();


	public TaskListMailer()
	{
	}

	protected boolean canStop()
	{
		return false;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi mailera");
		String run = Docusafe.getAdditionProperty("taskListMailer");
		if("false".equalsIgnoreCase(run))
			return;


		timer = new Timer(true);
		timer.schedule(new MailSender(), (long) 2*1000 ,(long) 1*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("SystemPowiadamianiaWystartowal"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi");
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}

	class MailSender extends TimerTask
	{

		/**
		 * Lista uzytkownikow do wysylki
		 * @return
		 */
		private List<DSUser> prepUsers()
		{
			log.trace("propUsers START");
			List<DSUser> users = new ArrayList<DSUser>();
			String[] usersFromProperties;

			try
			{
				usersFromProperties = DSApi.context().systemPreferences().node("taskListMailer").keys();
				for(int i=0;i<usersFromProperties.length;i++)
				{
					log.trace("ADD user "+ usersFromProperties[i]);
					if(usersFromProperties[i] != null && usersFromProperties[i].length() > 0)
					{
						DSUser user = null;
						try
						{
							user = DSUser.findByUsername(usersFromProperties[i]);
						}catch(UserNotFoundException e){
							user = DSUser.findByExternalName(usersFromProperties[i]);
							log.trace("wyszukuje po external user name" + usersFromProperties[i]);
						}
						
						if(user.getEmail() != null)
							users.add(user);
						else
							log.trace("User getEmail = NULL");
					}
				}
			}
			catch(UserNotFoundException e)
			{
				TaskListMailer.log.error("",e.getMessage());
				console(Console.ERROR, "Nie znaleziono u�ytkownika [ "+e+" ]");
			}
			catch(EdmException e)
			{
				TaskListMailer.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			catch(Throwable e)
			{
				TaskListMailer.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}

			log.trace("propUsers STOP");
			return users;
		}

		private void sendMail(DSUser user)
		{
				log.trace("wysylka maila do uzytkownika {}",user.getName());
				StringBuilder msg = new StringBuilder();
				msg.append("Witaj "+user.getFirstname()+" "+user.getLastname()+"\n\n");
	
				msg.append("Masz nowe dokumenty na li�cie zada�\n");
				UserCounter uc = null;
				try
				{
					TaskListCounterManager mgr = new TaskListCounterManager(false);
					uc = mgr.getUserCounter(user.getName(),DSApi.context().systemPreferences().node("taskListMailer").getLong(user.getName(), 0));
				}
				catch(EdmException e)
				{
					TaskListMailer.log.error("B��d",e);
					console(Console.ERROR, "B��d [ "+e+" ]");
				}
	
				msg.append("O godzinie "+DateUtils.formatCommonTime(new Date())+" na twojej li�cie zada� znajdowa�o si� "+uc.getAllTask()+" zada�.\n\n");
	
				msg.append("Nowe zadania z kt�rymi si� jeszcze nie zapozna�e� - "+uc.getNewNotAcceptedTask()+". \n");
				msg.append("Zadania z kt�rymi nie zako�czy�e� jeszcze pracy - "+(int)(uc.getNewAcceptedTask())+".\n\n");
				//msg.append("Liczba zada� "+counter+"\n");
				msg.append("Aby zalogowa� si� do systemu i zaj�� si� zadaniami kliknij na link - "+Docusafe.getBaseUrl());
				msg.append("\n\n Administrator Systemu DocuSafe");
	
				Map<String, Object> ctext = new HashMap<String, Object>();
				ctext.put("imie", user.getFirstname());
				ctext.put("nazwisko", user.getLastname());
				ctext.put("czas", DateUtils.formatCommonTime(new Date()));
				ctext.put("zadaniaAll", uc.getAllTask());
				ctext.put("zadaniaNew", uc.getNewNotAcceptedTask());
				ctext.put("zadaniaOld", uc.getNewAcceptedTask());
				ctext.put("link", Docusafe.getBaseUrl());
	
				if(uc.getNewTask() > 0)
				{
					try
					{
					((Mailer) ServiceManager.getService(Mailer.NAME)).
		        		send(user.getEmail(), user.getEmail(), null, Configuration.getMail(Mail.TASK_LIST_MAILER), ctext);
						console(Console.INFO, "Wysylam mail do "+user.getName()+" na email "+user.getEmail()+" czas "+new Date());
						TaskListMailer.log.info("Wysylam mail do "+user.getName()+" na email "+user.getEmail()+" czas "+new Date());
					}
					catch(Exception e)
					{
						TaskListMailer.log.error("B��d wys�ania maila o onowym zadaniu do " + user.getName(),e);
						console(Console.ERROR, "B��d [ "+e+" ]");
					}
				}
				else
				{
					String kom = "Nie wysylam mail do "+user.getName()+" na email "+user.getEmail()+" czas "+new Date()+" - brak nowych zadan";
					console(Console.INFO, kom);
					log.trace(kom);
				}
				uc.setLastSent(new Date());
				DSApi.context().systemPreferences().node("taskListMailer").putLong(user.getName(), new Date().getTime());
		}

		/**
		 * Czy nalezy wyslac maila
		 * @param user
		 * @return
		 */
		private boolean shouldSend(DSUser user)
		{
			if("admin".equals(user.getName()))
				log.debug("");
			log.trace("shouldSend START");
			/**Ostatnia akywnosc*/
			Long lastActivityTime = DSApi.context().systemPreferences().node("taskListMailer").getLong(user.getName(), 0);
			/**Rodzaj okresu*/
			Integer termKind = DSApi.context().userPreferences(user.getName()).node("taskListMailer").getInt("termKind", TERM_KIND_CO_ILE_MINUT);
			Integer coIle = DSApi.context().userPreferences(user.getName()).node("taskListMailer").getInt("timeMail", 10);
			/**Domyslnie poniedzialek*/
			Integer dayOfWeek =  DSApi.context().userPreferences(user.getName()).node("taskListMailer").getInt("dayOfWeek", 2);
			/**O ktorej godzinie*/
			String time =  DSApi.context().userPreferences(user.getName()).node("taskListMailer").get("time", "9:00");
			if(log.isTraceEnabled())
				log.trace("shouldSend lastActivityTime = {}, termKind = {}, coIle = {}, dayOfWeek = {} ,time = {}",
						lastActivityTime,termKind,coIle,dayOfWeek,time);
			String[] timeTab = time.split(":");
			Integer hour = Integer.parseInt(timeTab[0]);
			Integer minut = null;
			if (timeTab.length>1)
				minut = Integer.parseInt("00");
			Date lastActivityDate = null;
			if(!lastActivityTime.equals(0L))
			{
				lastActivityDate = new Date(lastActivityTime);
			}
			else
				return true;
			
			log.trace("shouldSend lastActivity = {}",lastActivityDate);

			Calendar newActivity = Calendar.getInstance();
			newActivity.setTime(lastActivityDate);
			if(TERM_KIND_CO_ILE_MINUT.equals(termKind))
			{
				newActivity.add(Calendar.MINUTE,coIle);
			}
			else if(TERM_KIND_CO_ILE_GODZIN.equals(termKind))
			{
				newActivity.add(Calendar.HOUR_OF_DAY,coIle);
			}
			else if(TERM_KIND_RAZ_DZIENNIE.equals(termKind))
			{
				newActivity.add(Calendar.DAY_OF_YEAR,1);
				newActivity.set(Calendar.HOUR_OF_DAY, hour);
				newActivity.set(Calendar.MINUTE, minut);
			}
			else if(TERM_KIND_RAZ_W_TYGODNIU.equals(termKind))
			{
				newActivity.add(Calendar.WEEK_OF_YEAR,1);
				newActivity.set(Calendar.HOUR_OF_DAY, hour);
				newActivity.set(Calendar.MINUTE, minut);
			}
			else if(TERM_KIND_RAZ_W_MIESIACU.equals(termKind))
			{
				newActivity.add(Calendar.MONTH,1);
				newActivity.set(Calendar.DAY_OF_WEEK, dayOfWeek);
				newActivity.set(Calendar.HOUR_OF_DAY, hour);
				newActivity.set(Calendar.MINUTE, minut);
			}
			log.trace("shouldSend newActivity = {}",DateUtils.formatJsDateTime(newActivity.getTime()));
			if(newActivity.getTime().before(new Date()))
			{
				log.trace("shouldSend STOP shouldSend = true");
				return true;
			}
			else
			{
				log.trace("shouldSend STOP shouldSend = false");
				return false;
			}
		}

		public void run()
		{
			try
			{
				log.trace("Petla powiadomienia START");
				DSApi.openAdmin();
				for(DSUser user : prepUsers())
				{
					try
					{
						DSApi.context().begin();
						if(shouldSend(user))
							sendMail(user);
						DSApi.context().commit();
					}
					catch (Throwable e) 
					{
						TaskListMailer.log.error("",e);
						console(Console.ERROR, "B��d [ "+e+" ]");
						DSApi.context()._rollback();
					}
				}
			}
			catch(EdmException e)
			{
				TaskListMailer.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			catch(Throwable e)
			{
				TaskListMailer.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			finally
			{
				log.trace("Petla powiadomienia STOP");
				DSApi._close();
			}
		}
	}
}
