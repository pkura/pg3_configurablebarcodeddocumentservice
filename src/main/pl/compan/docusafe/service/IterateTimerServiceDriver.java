package pl.compan.docusafe.service;

import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.DateUtils;

/**
 * <p>Interruptable ServiceDriver</p>
 *
 * @author Jan �wi�cki <a href="mailto:jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public abstract class IterateTimerServiceDriver extends ServiceDriver implements Service
{
    private static final Logger log = LoggerFactory.getLogger(IterateTimerServiceDriver.class);

    protected boolean cancelled;

    /**
     * Task, kt�ry b�dzie uruchomiony w timerze.
     */
    protected IterateTimerTask task;

    /**
     * Czas op�nienie startu timera w sekundach.
     * Pobierany z {@code service.delay.SERVICE_ID} z {@code HOME/adds.properties}
     */
    protected Integer delay;

    /**
     * Czas pomi�dzy wywo�aniami timera w sekundach.
     * Pobierany z {@code service.period.SERVICE_ID} z {@code HOME/adds.properties}
     */
    protected Integer period;

    protected Timer timer;

    public IterateTimerServiceDriver(IterateTimerTask task)
    {
        this.task = task;
    }

    public IterateTimerServiceDriver() {}

    public void start()
    {
        log.debug("[start] begin");

        if(task == null) {
            log.error("[start] TASK IS NULL, STOPPING");
            return;
        }

        delay = Integer.valueOf(Docusafe.getAdditionPropertyOrDefault("service.delay."+getServiceId(), "60"));
        period = Integer.valueOf(Docusafe.getAdditionPropertyOrDefault("service.period."+getServiceId(), "300"));

        log.debug("[start] serviceId = {}, delay = {}, period = {}", getServiceId(), delay, period);

        timer = new Timer(true);
        log.info("[start] created timer {}", timer);

        timer.schedule(task, delay * DateUtils.SECOND, period * DateUtils.SECOND);
        log.debug("[start] end");
    }

    public void stop()
    {
        log.debug("[stop] begin");
        if(timer != null)
        {
            log.debug("[stop] cancelling task and timer");
            task.stop();
            timer.cancel();
        }
        log.debug("[stop] end");
    }

    /**
     * Je�li ta metoda zwr�ci true to service powinien si� zatrzyma�.
     *
     * @author Jan �wi�cki <a href="mailto:jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     */
    boolean isCancelled()
    {
        return cancelled;
    }

    protected boolean canStop()
    {
        return true;
    }

    /**
     * Wywo�ywane po uko�czonej iteracji
     */
    public void onComplete() {

    }

    public Timer getTimer()
    {
        return timer;
    }

    public void setTimer(Timer timer)
    {
        this.timer = timer;
    }

    public void setTask(IterateTimerTask task) {
        this.task = task;
    }

    public IterateTimerTask getTask() {
        return task;
    }
}
