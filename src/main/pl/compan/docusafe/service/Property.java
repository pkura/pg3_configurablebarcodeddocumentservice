package pl.compan.docusafe.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collections;
import java.util.Map;
import java.util.prefs.Preferences;

/* User: Administrator, Date: 2005-04-20 13:55:31 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Property.java,v 1.9 2008/12/09 13:59:17 mariuszk Exp $
 */
public abstract class Property
{
    private static final Log log = LogFactory.getLog(Property.class);

    static class Type {
        private int type;

        private Type(int type)
        {
            this.type = type;
        }

        public boolean equals(Object o)
        {
            return (o instanceof Type) && ((Type) o).type == type;
        }

        public int hashCode()
        {
            return type;
        }
    }

    static class Holdability {
        private int flavour;

        private Holdability(int flavour)
        {
            this.flavour = flavour;
        }

        public boolean equals(Object o)
        {
            return (o instanceof Holdability) && ((Holdability) o).flavour == flavour;
        }

        public int hashCode()
        {
            return flavour;
        }
    }

    static class DocusafeType
    {
        private int type;

        private DocusafeType(int type)
        {
            this.type = type;
        }

        public boolean equals(Object o)
        {
            return o instanceof DocusafeType && ((DocusafeType) o).type == type;
        }

        public int hashCode()
        {
            return type;
        }
    }

    protected static final Type SIMPLE = new Type(1);
    protected static final Type ENUMERATED = new Type(2);

    protected static final Holdability RUNTIME = new Holdability(1);
    protected static final Holdability PERSISTENT = new Holdability(2);

    protected static final DocusafeType GUID = new DocusafeType(1);
    protected static final DocusafeType IN_DOCUMENT_DELIVERY = new DocusafeType(2);
    protected static final DocusafeType DATE = new DocusafeType(3);
    protected static final DocusafeType REPORT = new DocusafeType(4);
    //protected static final DocusafeType USERNAME = new DocusafeType(2);

    private Type propertyType;
    private Holdability holdability;
    private ServiceDriver driver;
    private String id;
    private String name;
    private Class type;
    private DocusafeType docusafeType;

    protected Property(Type propertyType, Holdability holdability, ServiceDriver driver,
                       String id, String name, Class type, DocusafeType docusafeType)
    {
        if ((docusafeType == GUID /*|| docusafeType == USERNAME*/) &&
            (type != String.class))
            throw new IllegalArgumentException("W�asno�ci wymagaj�ce docusafeType " +
                "= GUID | USERNAME musz� mie� typ java.lang.String");
        this.propertyType = propertyType;
        this.holdability = holdability;
        this.driver = driver;
        this.id = id;
        this.name = name;
        this.type = type;
        this.docusafeType = docusafeType;
    }

    protected Property(Type propertyType, Holdability holdability, ServiceDriver driver,
                       String id, String name, Class type)
    {
        this(propertyType, holdability, driver, id, name, type, null);
    }

    public final String getId()
    {
        return id;
    }

    public final String getName()
    {
        return name;
    }

    public final Class getType()
    {
        return type;
    }

    protected Object getValueSpi()
    {
        return null;
    }

    protected void setValueSpi(Object object) throws ServiceException
    {
    	LogFactory.getLog("tomekl").debug("setvalue");
    }

    public Map getValueConstraints() throws ServiceException
    {
        if (!propertyType.equals(ENUMERATED))
            throw new IllegalStateException("List� dozwolonych warto�ci mo�na pobra�" +
                " tylko dla w�asno�ci typu ENUMERATED");
        return Collections.EMPTY_MAP;
    }

    public boolean canModify()
    {
        return true;
    }

    public final void setValue(Object object) throws ServiceException
    {
        if (object != null && !(type.isInstance(object)))
            throw new IllegalArgumentException("Spodziewano si� obiektu typu "+type);

        if (!canModify())
            throw new ServiceException("Warto�ci nie mo�na w tym momencie zmodyfikowa�");

        setValueSpi(object);

        // je�eli warto�� by�a prawid�owa, jest zapisywana w Preferences
        if (holdability.equals(PERSISTENT))
        {
            if (object != null)
            {
                driver.getPreferences().put(getId(), object.toString());
            }
            else
            {
                driver.getPreferences().remove(getId());
            }
        }
    }

    public final Object getValue()
    {
        if (!propertyType.equals(SIMPLE) && !propertyType.equals(ENUMERATED))
            throw new IllegalStateException("Warto�� mo�na pobra� tylko z w�asno�ci" +
                " typu SIMPLE lub ENUMERATED");

        return getValueSpi();
    }

    public final boolean isEnumerated()
    {
        return propertyType.equals(ENUMERATED);
    }

    public final boolean wantsGuid()
    {
        return docusafeType == GUID;
    }

    public final boolean wantsInDocumentDelivery()
    {
        return docusafeType == IN_DOCUMENT_DELIVERY;
    }

    public final boolean wantsDate()
    {
    	return docusafeType == DATE;
    }
    
    public final boolean wantsReport()
    {
    	return docusafeType == REPORT;
    }
/*
    public final boolean wantsUsername()
    {
        return docusafeType == USERNAME;
    }
*/

    protected final void init()
    {
        if (!holdability.equals(PERSISTENT))
            return;

        Preferences prefs = driver.getPreferences();

        if (prefs.get(getId(), null) == null)
            return;

        try
        {
            if (getType() == String.class)
            {
                setValueSpi(prefs.get(getId(), null));
            }
            else if (getType() == Boolean.class)
            {
                setValueSpi(Boolean.valueOf(prefs.getBoolean(getId(), false)));
            }
            else if (getType() == Integer.class)
            {
                setValueSpi(new Integer(prefs.getInt(getId(), 0)));
            }
            else if(getType() == Long.class)
            {
            	setValueSpi(new Long(prefs.getLong(getId(), 0)));
            }
            else
            {
                throw new IllegalArgumentException("Nieznany typ parametru: "+getType());
            }
        }
        catch (ServiceException e)
        {
            log.error(e.getMessage(), e);
        }
    }
}
