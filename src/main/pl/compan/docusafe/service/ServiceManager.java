package pl.compan.docusafe.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import com.google.common.base.Preconditions;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.service.bip.Bip;
import pl.compan.docusafe.tools.messenger.server.ServerCore;
import pl.compan.docusafe.web.CssService;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ServiceManager.java,v 1.47 2010/03/29 10:36:06 pecet1 Exp $
 */
public class ServiceManager
{
    private static final Logger log = LoggerFactory.getLogger(ServiceManager.class);

    public static String AVAILABLE_DISABLE_PREFIX = "service.disable.";
    /**
     * serviceId (String) -> ServiceInfo
     */
    private static Map serviceInfos;

    /**
     * serviceId (String) -> ServiceDriver
     */
    private static Map activeDrivers = new HashMap();
    private static List activeDriversList;
    /**
     * Zwraca obiekt implementuj�cy us�ug�.
     * @param serviceId
     * @return
     * @throws ServiceNotFoundException
     * @throws ServiceDriverNotSelectedException
     */
    public static Service getService(String serviceId) throws ServiceNotFoundException, ServiceDriverNotSelectedException
    {
        if (serviceId == null)
            throw new NullPointerException("serviceId");

        if (serviceInfos.get(serviceId) == null)
            throw new ServiceNotFoundException(serviceId); //debug.log

        ServiceDriver driver = (ServiceDriver) activeDrivers.get(serviceId);
        if (driver == null)
            throw new ServiceDriverNotSelectedException(serviceId);

        return (Service) driver;
    }

    public static <T extends Service> T getService(Class<T> clazz, String serviceId)  throws ServiceNotFoundException, ServiceDriverNotSelectedException 
    {
        Object ret =  getService(serviceId);
        Preconditions.checkState(clazz.isInstance(ret),
            "service with id " + serviceId
                + " is not instance of " + clazz.getSimpleName()
                + " it is " + ret.getClass().getSimpleName());
        return (T)ret;
    }
    
    public static List<ServiceDriver> getActiveDrivers()
    {
    	if(activeDriversList == null)
    	{
    		activeDriversList = new ArrayList<ServiceDriver>();
    		Set<String> keys = activeDrivers.keySet();
    		for (String key : keys)
    		{
    			activeDriversList.add(activeDrivers.get(key));
			}
    	}
    	return activeDriversList;
    }
    
    public static ServiceDriver getServiceDriver(String serviceId)
    	throws ServiceNotFoundException, ServiceDriverNotSelectedException
    {
	    if (serviceId == null)
	        throw new NullPointerException("serviceId");
	
	    if (serviceInfos.get(serviceId) == null)
	        throw new ServiceNotFoundException(serviceId);
	
	    ServiceDriver driver = (ServiceDriver) activeDrivers.get(serviceId);
	    if (driver == null)
	        throw new ServiceDriverNotSelectedException(serviceId);
	    return  driver;
    }

    /**
     * Zwraca tablic� parametr�w us�ugi niezale�nych od wybranego
     * sterownika.
     * @param serviceId
     * @return Tablica parametr�w us�ugi, mo�e mie� d�ugo�� 0.
     */
    public static Property[] getServiceProperties(String serviceId)
        throws ServiceNotFoundException
    {
        throw new UnsupportedOperationException();
    }

/*
    public static ServiceDriverInfo[] getDriverInfos(String serviceId)
        throws ServiceNotFoundException
    {
        ServiceInfo serviceInfo = (ServiceInfo) serviceInfos.get(serviceId);
        if (serviceInfo == null)
            throw new ServiceNotFoundException(serviceId);

        return serviceInfo.getDriverInfos();
    }
*/

    public static ServiceInfo getServiceInfo(String serviceId)
        throws ServiceNotFoundException
    {
        ServiceInfo serviceInfo = (ServiceInfo) serviceInfos.get(serviceId);
        if (serviceInfo == null)
            throw new ServiceNotFoundException(serviceId);

        return serviceInfo;
    }

    /**
     * Zwraca true, je�eli w danym momencie mo�na zmieni� sterownik
     * us�ugi na inny.
     */
    public static boolean canSetDriver(String serviceId)
    {
        if (serviceId == null)
            throw new NullPointerException("serviceId");

        ServiceInfo info = (ServiceInfo) serviceInfos.get(serviceId);
        if (info.isFixed())
            return false;
        
        ServiceDriver driver = (ServiceDriver) activeDrivers.get(serviceId);
        return driver == null || driver.canStop();
    }

    /**
     * W��cza nowy sterownik us�ugi. Metoda powinna by� wywo�ywana
     * w ramach otwartej transakcji, poniewa� zapisuje identyfikator
     * nowego sterownika w Preferences.
     */ 
    public static void setDriver(String serviceId, String driverId)
        throws ServiceNotFoundException, ServiceException
    {
        if (serviceId == null)
            throw new NullPointerException("serviceId");

        ServiceInfo info = (ServiceInfo) serviceInfos.get(serviceId);
        if (info.isFixed())
            throw new ServiceException("Usluga nie pozwala na zmiane sterownika");

        ServiceDriver driver = (ServiceDriver) activeDrivers.get(serviceId);

        if (driver != null && !driver.canStop())
            throw new ServiceException("Nie mozna w tej chwili zatrzymac uslugi "+serviceId);

        try
        {
            if (driver != null)
            {
                if (driverId != null && driver.getDriverId().equals(driverId))
                    return;

                driver.stop();
                log.info("Zatrzymano usluge "+serviceId+"/"+driver.getDriverId() +
                    " ("+driver.getClass()+")");
                activeDrivers.remove(serviceId);
            }

            if (driverId == null)
            {
                DSApi.context().systemPreferences().node("services/"+serviceId).
                    remove("driverId");
                return;
            }

            driver = instantiateDriver(info.getDriverInfo(driverId));

            DSApi.context().systemPreferences().node("services/"+serviceId).
                put("driverId", driverId);

            driver.init();
            driver.start();
            log.info("Uruchomiono usluge "+serviceId+"/"+driverId +
                " ("+driver.getClass()+")");
            activeDrivers.put(serviceId, driver);
        }
        catch (ServiceException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * Zwraca identyfikator aktualnie wybranego sterownika us�ugi.
     * @param serviceId
     * @return
     * @throws ServiceNotFoundException
     */
    public static String getDriver(String serviceId)
        throws ServiceNotFoundException
    {
        ServiceInfo serviceInfo = (ServiceInfo) serviceInfos.get(serviceId);
        if (serviceInfo == null)
            throw new ServiceNotFoundException(serviceId);

        ServiceDriver driver = (ServiceDriver) activeDrivers.get(serviceId);

        if (driver != null) return driver.getDriverId();
        return null;
    }

    public static void init() throws ServiceException
    {
        readIndividualProperties();
        //FIXME - decydujmy co robic z tym serwisem RMI - na razie do stopuje
        if (false) { ServerCore.runCommunication(); }

        try
        {
        	File homeServices = new File(Docusafe.getHome(), "services.xml");
        	if(homeServices.exists())
        	{
        		log.info("Laduje konfiguracje servicow z katalogu home");
        		new ConfigurationReader(new InputSource(new FileInputStream(homeServices)));
        	}
        	else
        		new ConfigurationReader(new InputSource(
        				ServiceManager.class.getResourceAsStream("services.xml")));
        }
        catch (Exception e)
        {
            throw new ServiceException("Blad podczas odczytu konfiguracji uslug", e);
        }

        try
        {
            DSApi.openAdmin();

            for (Iterator iter=serviceInfos.values().iterator(); iter.hasNext(); )
            {
                ServiceInfo serviceInfo = (ServiceInfo) iter.next();
                String driverId = DSApi.context().systemPreferences().
                    node("services/"+serviceInfo.getServiceId()).get("driverId", null);

                log.debug("Instantiating service with driverId = {}", driverId);

                if (driverId == null)
                    driverId = serviceInfo.getDefaultDriverId();

                if (driverId != null)
                {
                    ServiceDriver driver = instantiateDriver(serviceInfo.getDriverInfo(driverId));
                    driver.init();
                    driver.start();
                    log.info("Uruchomiono usluge "+serviceInfo.getServiceId()+"/"+driverId +
                        " ("+driver.getClass()+")");
                    activeDrivers.put(serviceInfo.getServiceId(), driver);
                }
            }
        }
        catch (ServiceException e)
        {
            log.error(e.getMessage(), e);
            throw e;
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            throw new ServiceException(e.getMessage(), e);
        }
        finally
        {
            DSApi._close();
        }
    }

    public static void destroy() throws ServiceException
    {
        for (Iterator iter=activeDrivers.values().iterator(); iter.hasNext(); )
        {
            ((ServiceDriver) iter.next()).stop();
        }
    }

    private static ServiceDriver instantiateDriver(ServiceDriverInfo driverInfo) throws Exception
    {
        ServiceDriver driver =
            (ServiceDriver) Class.forName(driverInfo.getClazz()).newInstance();

        if (!(driver instanceof Service))
            throw new ServiceException("Sterownik uslugi musi byc instancja "+
                Service.class.getName()+" ("+driverInfo.getClazz()+")");

        driver.setDriverInfo(driverInfo);

        return driver;
    }

    public static void main(String[] args) throws Exception
    {
        init();
        for (Iterator iter=serviceInfos.values().iterator(); iter.hasNext(); )
        {
            ServiceInfo info = (ServiceInfo) iter.next();
            ServiceDriverInfo[] di = info.getDriverInfos();
            for (int i=0; i < di.length; i++)
            {
            }
        }

        Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
    }

    private static class ConfigurationReader extends DefaultHandler
    {
        private ServiceInfo serviceInfo;
        private Boolean disabled = false;

        private Set serviceIds = new HashSet();
        private Set driverIds = new HashSet();

        private Map services = new LinkedHashMap();

        public ConfigurationReader(InputSource inputSource)
            throws ParserConfigurationException, SAXException, IOException
        {
            SAXParserFactory.newInstance().newSAXParser().parse(inputSource, this);
            serviceInfos = services;
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
        {
            if ("service".equals(qName))
            {
                if (serviceInfo != null)
                    throw new SAXException("Zagniezdzanie elementow w service jest niedozwolone");

                String id = attributes.getValue("id");
                if (id == null || id.length() == 0)
                    throw new SAXException("Brak identyfikatora uslugi");
                if (serviceIds.contains(id))
                    throw new SAXException("Powtornie uzyty identyfikator uslugi: "+serviceInfo.getServiceId());
                String name = attributes.getValue("name");
                if(AvailabilityManager.isAvailable(ServiceManager.AVAILABLE_DISABLE_PREFIX + name)) 
                {
                	log.info("Pomijanie servicu {}",name);
                	disabled = true;
                	serviceInfo = null;
                	return;
                }
                if (name == null || name.length() == 0)
                    throw new SAXException("Brak nazwy uslugi "+id);
                String def = attributes.getValue("default");
                boolean fixed = Boolean.valueOf(attributes.getValue("fixed")).booleanValue();

                serviceInfo = new ServiceInfo(id, name, def, fixed);
                services.put(id, serviceInfo);

                serviceIds.add(id);
                driverIds.clear();
            }
            else if ("driver".equals(qName))
            {
            	if(disabled) return;
                if (serviceInfo == null)
                    throw new SAXException("Element driver musi znajdowac sie w elemencie service");

                ServiceDriverInfo driverInfo = new ServiceDriverInfo(
                    serviceInfo.getServiceId(),
                    attributes.getValue("id"),
                    attributes.getValue("name"),
                    attributes.getValue("class"));

                if (driverIds.contains(driverInfo.getDriverId()))
                    throw new SAXException("Powtornie uzyty identyfikator sterownika " +
                        "uslugi "+serviceInfo.getServiceId()+": "+driverInfo.getDriverId());

                driverIds.add(driverInfo.getDriverId());

                serviceInfo.addDriverInfo(driverInfo);
            }
        }

        public void endElement(String uri, String localName, String qName) throws SAXException
        {
            if ("service".equals(qName))
            {
            	if(disabled) 
            	{
            		serviceInfo = null;
            		disabled = false;
            		return;
            	}
                if (serviceInfo.getDefaultDriverId() != null &&
                    !driverIds.contains(serviceInfo.getDefaultDriverId()))
                    throw new SAXException("Nie istnieje sterownik uslugi "+serviceInfo.getServiceId()+
                        " okreslony jako domyslny: "+serviceInfo.getDefaultDriverId());

                serviceInfo = null;
            }
        }
    }

    static void console(ServiceDriver driver, int logLevel, String message)
    {
        console(driver, logLevel, message, null);
    }

    static void console(ServiceDriver driver, int logLevel, String message, Throwable t)
    {
        Console.getInstance(driver.getServiceId()).log(driver.getDriverId(), logLevel, message, t);
    }

    public static Console.Message[] getConsoleMessages(String serviceId)
    {
        Console console = Console.getInstance(serviceId);
        return console.getMessages();
    }

    
    /** Ustawia domyslne wartosci dla niekt�rych indywidualnych 
     * 
     */
    private static void setDeafultIndividualProperties()
    {
    	Docusafe.setProperty("copyright.notice.url", "http://www.docusafe.pl");
    	Docusafe.setProperty("copyright.notice", "Copyright (c) COM-PAN");
    	Docusafe.setProperty("copyright.notice.url.2", "");
    	Docusafe.setProperty("copyright.notice.2", "");
    }
    /**
     *  Czyta indywidualne ustawienia zawarte w pliku HOME/individual_properties.xml
     *  (te ustawienia s� "extra" tzn. ten plik wcale nie musi istniec.)
     *  @todo - te ustawienia nie sa teraz elegancko, ale zanim sie do tego wezmiemy trzeba przemyslec temat
     */
    private static void readIndividualProperties() 
    {
        

    	setDeafultIndividualProperties();
    	CssService.initCss();
        String path = Docusafe.getHome().getAbsolutePath();
        path = path+File.separator+"individual_properties.xml";
        File props = new File(path);
        
        if(!props.exists())
        {
            return;
        }        
        SAXReader reader = new SAXReader();
        Document doc;
        try 
        {
            doc = reader.read(props);
        }
        catch (Exception ex) 
        {
            log.error("",ex);
            return;
        }
        
        Element root = doc.getRootElement();
        
        //czytanie preferencji do CssService
        Properties classic = CssService.layouts.get("classic");
        
        for ( Iterator i = root.elementIterator("CssService"); i.hasNext(); )
        {
            Element tmp = (Element) i.next();
            for ( Iterator j = tmp.elementIterator(); j.hasNext(); ) 
            {
                Element element = (Element) j.next();
                Docusafe.setProperty(element.getName(), element.getText());
                classic.setProperty(element.getName(), element.getText());
                CssService.layouts.get("classic").put(element.getName(), element.getText());
            }
        }
        //czytanie preferencji, ktore nadpisuja te w docusafe.properties
        for ( Iterator i = root.elementIterator("Overwriting"); i.hasNext(); ) 
        {
            Element tmp = (Element) i.next();
            for ( Iterator j = tmp.elementIterator(); j.hasNext(); ) 
            {
                Element element = (Element) j.next();
                classic.setProperty(element.getName(), element.getText());
                Docusafe.setProperty(element.getName(), element.getText());                
            }
        }
        
        //czytanie layoutow
        for (Iterator i = root.elementIterator("Layout"); i.hasNext();) 
        {
            Element tmp = (Element) i.next();
            if(tmp.attribute("name") == null)
                continue;
            
            Properties prop = new Properties(classic);
            for (Iterator j = tmp.elementIterator(); j.hasNext(); ) 
            {
                Element element = (Element) j.next();
                prop.put(element.getName(), element.getText());
            }            
            CssService.layouts.put(tmp.attributeValue("name"), prop);
        }
    }
}
