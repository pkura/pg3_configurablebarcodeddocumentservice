package pl.compan.docusafe.service.agent;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;

/**
 * Klasa abstrakcyjna odpowiedzialna za obsluge agenta
 * Agent jest to specyficzny serwis uruchamiany co jakis czas
 * udajacy uzytkownika w systemie
 * @author wkutyla
 *
 */
public abstract class Agent extends TimerTask
{	
	protected Timer timer = null; 
	protected Integer time;
	/** 
	 * Propertiesy przekazane do bazy danych
	 */
	protected Properties configuration = null;
	
	/**
	 * Nazwa uzytkownika bedacego agentem
	 */
	protected String agentName = null;
	/**
	 * Czy agent jest wlaczony?
	 */
	protected boolean agentOn = false;
	
	
	/** Metoda inicjuje dzialanie agenta **/
	
	public void initAgent(Properties props)
	{
		configuration = props;
		if (configuration.getProperty(getPropertyPrefix()+"enabled").equalsIgnoreCase("true"))
		{
			agentOn = true;
			agentName = configuration.getProperty(getPropertyPrefix()+"agentName");
            time = Integer.parseInt(configuration.getProperty(getPropertyPrefix()+"agentTime"));
			/* Ustaw specyficzne ustawienia */
			setupAgent();
		}
		
		
	}
	
	protected void start()
    {        
        if (agentOn && agentAllowed())
        {        	
            if (time == null || time <= 0)
                time = 10;
            AgentManager.log.info(getAgentCode()+ "Uruchomilem agenta  z częstotliwością co "+ time +" minut");
            timer = new Timer(true);
            timer.schedule(this, 1000, time*DateUtils.MINUTE);
            
        }
    }
	
	protected void stop()
    {		
        if (timer != null) 
        {
        	AgentManager.log.info("Stopuje agenta " + getAgentCode());
        	timer.cancel();
       	}
    }
	
	/**  
	 * W tej metodzie inicjujemy zmienne specyficzne dla agenta 
	 * Metoda korzysta z obiektu configuration
	 */
	protected abstract void setupAgent();
	
	/** 
	 * @return Kod agenta 
	 */
	public abstract String getAgentCode();

	/**
	 * Kod zwraca nam prefix przed odpowiednim property
	 * Prefix musi zawierac znak .
	 */
	public abstract String getPropertyPrefix();
	
	/**
	 * Metoda okresla dodatkowe warunki jakie powinny byc spelnione do uruchomienia agenta
	 * @return
	 */
	public abstract boolean agentAllowed();
	
	public Properties getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Properties configuration) {
		this.configuration = configuration;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	
}
