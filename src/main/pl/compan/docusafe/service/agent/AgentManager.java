package pl.compan.docusafe.service.agent;

import java.io.*;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.parametrization.aegon.AbsenceAgent;
import pl.compan.docusafe.parametrization.aegon.CrmDriver;
import pl.compan.docusafe.parametrization.aegon.ICRDriver;
import pl.compan.docusafe.util.LoggerFactory;
/** 
 * Klasa odpowiedzialna jest za zarzadzanie serwisami typu AGENT
 * @author wkutyla
 *
 */

public class AgentManager 
{
	public static final Log log = LogFactory.getLog("agent_log");
	protected static final Map<String,Agent> agents = new HashMap<String,Agent>();
	private static Properties configuration = null; 
		
	/**
	 * 
	 * @param homeDirectory
	 * @throws Exception
	 */
	public static void initAgentFactory(File homeDirectory)
	{
		try
		{
			//Ten kawalek przrzucaty do Agenta
        	File config = new File(homeDirectory, "agent.config");
        
        	if(config.exists())
        	{
        		
        		log.debug("Uruchamiam konfiguracje agenta");
        		configuration = new Properties();
        		configuration.load(Docusafe.class.getClassLoader().getResourceAsStream("agent.properties"));
        		configuration.load(new FileInputStream(config));
        		/**
        		 * @todo - to trzeba zrobic przez jakis plik XML
        		 */
        		initAgent(new CrmDriver());
        		initAgent(new ICRDriver());
        		initAgent(new AbsenceAgent());
        	}
        	
		}
		catch (Exception e)
		{
			log.error("Blad w czasie inicjowania agentow", e);
		}
	}
	
	/**
	 * 
	 * Metoda zatrzymuje wszystkich agentow
	 */
	public static void destroy()
	{	
		Iterator<Agent> it= agents.values().iterator();
		while (it.hasNext())
		{
			Agent ag = (Agent) it.next();
			ag.stop();
		}
	}
	/**
	 * @param agent
	 */
	
	private static void initAgent(Agent agent)
	{
		try
		{
			agent.initAgent(configuration);
			agents.put(agent.getAgentCode(), agent);
			agent.start();
		}
		catch (Exception e)
		{
			log.error("Nie zainicjowano agenta " + agent.getAgentCode(),e);
		}
	}
}
