package pl.compan.docusafe.service;

import org.apache.commons.collections.iterators.ArrayIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;

import java.io.File;
import java.util.Arrays;

import static com.google.common.base.Preconditions.checkNotNull;

abstract public class FileTimerTask extends IterateTimerTask<File> {

    private static final Logger log = LoggerFactory.getLogger(FileTimerTask.class);

    private String folderPath;
    private final boolean sort;

    public static final String LOCK_FILENAME = "docusafe.lock";

    public FileTimerTask(String folderPath) {
        this(folderPath, true);
    }

    public FileTimerTask(String folderPath, boolean sort) {
        checkNotNull(folderPath);
        this.folderPath = folderPath;
        this.sort = sort;
    }

    @Override
    public void beforeRun() throws Exception {
        File folder = new File(folderPath);

        if(! folder.isDirectory()) {
            throw new Exception("folder "+folderPath+" is not directory");
        }

        if(isLocked(folder)) {
            log.info("[beforeRun] skipping interation, folder is locked, folderPath = {}", folderPath);
            stop = true;
            return;
        } else {
            log.info("[beforeRun] doing iteration, folderPath = {}", folderPath);
            stop = false;
        }

        File[] list = folder.listFiles();

        if(list == null) {
            throw new Exception("file list is null, not directory or IO error");
        }

        if(sort) {
            Arrays.sort(list);
        }

        iterator = new ArrayIterator(list);
    }

    @Override
    protected void afterRun() {

    }

    public boolean isLocked(File folder) {
        File file = new File(folder, LOCK_FILENAME);
        return file.exists();
    }

    @Override
    protected abstract void forEach(File element, int index) throws Exception;
}
