package pl.compan.docusafe.service.fax;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/* User: Administrator, Date: 2005-08-08 13:32:12 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: RelayXmlHandler.java,v 1.8 2010/07/15 09:07:42 kamilj Exp $
 */
public class RelayXmlHandler extends DefaultHandler
{
	static final Logger log = LoggerFactory.getLogger(RelayXmlHandler.class);
    private File imageFile;
    private int pages;
    private String port;
    private String rfaxid;
    private Calendar calTime = Calendar.getInstance();
    private String relayID;
    private String path;
    private String did;
    private Date sendDate;
    private Date sendTime;
    private int state;

    private boolean error;
    private String errorMessage;

    private static final int STATE_NONE = 0;
    private static final int STATE_IMAGE = 1;
    private static final int STATE_PAGES = 2;
    private static final int STATE_PORT = 3;
    private static final int STATE_RFAXID = 4;
    private static final int STATE_TIME = 5;
    private static final int STATE_DATE = 6;
    private static final int STATE_ID = 7;
    private static final int STATE_ERROR = 8;
    private static final int STATE_DID = 9;
    private static final int STATE_REMOTE_ID = 10;
    private static final int STATE_SEND_DATE = 11;
    private static final int STATE_SEND_TIME = 12;

    private static final DateFormat dformat = new SimpleDateFormat("yyyy/MM/dd");
    private static final DateFormat tformat = new SimpleDateFormat("HH:mm:ss");

    public RelayXmlHandler(File f)
    {
    	this.path = f.getAbsolutePath();
    }
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        if ("ImageFilePath".equals(qName))
            state = STATE_IMAGE;
        else if ("Port".equals(qName))
            state = STATE_PORT;
        else if ("RemoteID".equals(qName))
            state = STATE_RFAXID;
        else if ("SubmitDate".equals(qName))
            state = STATE_DATE;
        else if ("SubmitTime".equals(qName))
            state = STATE_TIME;
        else if ("TotalPages".equals(qName))
            state = STATE_PAGES;
        else if ("ID".equals(qName))
            state = STATE_ID;
        else if ("Error".equals(qName))
        {
            state = STATE_ERROR;
            error = true;
        }
        else if ("DID".equals(qName))
        	state = STATE_DID;
        else if ("RemoteID".equals(qName))
            state = STATE_REMOTE_ID;
        else if ("SendDate".equals(qName))
            state = STATE_SEND_DATE;
        else if ("SendTime".equals(qName))
            state = STATE_SEND_TIME;
        else
            state = 0;
    }


    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        state = STATE_NONE;
    }

    public void characters(char ch[], int start, int length) throws SAXException
    {
        switch (state)
        {
            case STATE_IMAGE:
            	if(path.startsWith("\\\\"))
            	{
            		try
            		{
	            		String fileName = new String(ch, start, length);
	            		String [] tmp = fileName.split("\\\\");	
	            		String filePath = path + "\\" + tmp[tmp.length-1];
	            		imageFile = new File(filePath);
            		}
            		catch (Exception e) 
            		{
						log.error("",e);
					}
            	}
            	else
            	{
            		imageFile = new File(path + File.separator + new String(ch, start, length));
            	}
                
                break;
            case STATE_PORT:
                port = new String(ch, start, length);
                break;
            case STATE_RFAXID:
                rfaxid = new String(ch, start, length);
                break;
            case STATE_DATE:
                synchronized (dformat)
                {
                    try
                    {
                        Calendar tmp = Calendar.getInstance();
                        tmp.setTime(dformat.parse(new String(ch, start, length)));
                        calTime.set(Calendar.YEAR, tmp.get(Calendar.YEAR));
                        calTime.set(Calendar.MONTH, tmp.get(Calendar.MONTH));
                        calTime.set(Calendar.DAY_OF_MONTH, tmp.get(Calendar.DAY_OF_MONTH));
                    }
                    catch (ParseException e)
                    {
                    	log.debug("",e);
                    }
                }
                break;
            case STATE_TIME:
                synchronized (tformat)
                {
                    try
                    {
                        Calendar tmp = Calendar.getInstance();
                        tmp.setTime(tformat.parse(new String(ch, start, length)));
                        calTime.set(Calendar.HOUR_OF_DAY, tmp.get(Calendar.HOUR_OF_DAY));
                        calTime.set(Calendar.MINUTE, tmp.get(Calendar.MINUTE));
                        calTime.set(Calendar.SECOND, tmp.get(Calendar.SECOND));
                    }
                    catch (ParseException e)
                    {
                    	log.debug("",e);
                    }
                }
                break;
            case STATE_PAGES:
                try
                {
                    pages = Integer.parseInt(new String(ch, start, length));
                }
                catch (NumberFormatException e)
                {
                	log.debug("",e);
                }
                break;
            case STATE_ID:
                relayID = new String(ch, start, length);
                break;
            case STATE_ERROR:
                errorMessage = new String(ch, start, length);
                break;
            case STATE_DID:
            	did = new String(ch, start, length);
            case STATE_SEND_DATE:
                synchronized (dformat)
                {
                    try
                    {
                        Calendar tmp = Calendar.getInstance();
                        tmp.setTime(dformat.parse(new String(ch, start, length)));
                        calTime.set(Calendar.YEAR, tmp.get(Calendar.YEAR));
                        calTime.set(Calendar.MONTH, tmp.get(Calendar.MONTH));
                        calTime.set(Calendar.DAY_OF_MONTH, tmp.get(Calendar.DAY_OF_MONTH));
                        this.sendDate = tmp.getTime();
                    }
                    catch (ParseException e)
                    {
                        log.debug("",e);
                    }
                }
                break;
            case STATE_SEND_TIME:
                synchronized (tformat)
                {
                    try
                    {
                        Calendar tmp = Calendar.getInstance();
                        tmp.setTime(tformat.parse(new String(ch, start, length)));
                        calTime.set(Calendar.HOUR_OF_DAY, tmp.get(Calendar.HOUR_OF_DAY));
                        calTime.set(Calendar.MINUTE, tmp.get(Calendar.MINUTE));
                        calTime.set(Calendar.SECOND, tmp.get(Calendar.SECOND));
                        this.sendTime = tmp.getTime();
                    }
                    catch (ParseException e)
                    {
                        log.debug("",e);
                    }
                }
                break;
        }
    }

    public File getImageFile()
    {
        return imageFile;
    }

    public int getPages()
    {
        return pages;
    }

    public String getPort()
    {
        return port;
    }

    public String getRfaxid()
    {
        return rfaxid;
    }

    public Date getTime()
    {
        return calTime.getTime();
    }

    public String getRelayID()
    {
        return relayID;
    }

    public boolean isError()
    {
        return error;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }
    
    public String getDid() 
    {
		return did;
	}

    public Date getSendDate() {
        return sendDate;
    }

    public Date getSendTime() {
        return sendTime;
    }

//    else if ("RemoteID".equals(qName))
//    state = STATE_REMOTE_ID;
//    else if ("SendDate".equals(qName))
//    state = STATE_SEND_DATE;
//    else if ("SendTime".equals(qName))
//    state = STATE_SEND_TIME;
}
