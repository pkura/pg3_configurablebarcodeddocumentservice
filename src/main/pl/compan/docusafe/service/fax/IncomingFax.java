package pl.compan.docusafe.service.fax;

import pl.compan.docusafe.service.ServiceException;

import java.io.File;
import java.util.Date;

/* User: Administrator, Date: 2005-06-23 15:01:30 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: IncomingFax.java,v 1.10 2010/07/15 09:07:42 kamilj Exp $
 */
public abstract class IncomingFax
{
    public static String FAX_ATTACHMENT_CN = "fax";
    protected IncomingFax()
    {
    }

    public abstract void purge() throws ServiceException;

    /**
     * ID faksu, nie musi by� dost�pne.
     * @return ID lub null.
     */
    public abstract String getFaxID();

    public abstract String getSubject();

    public abstract String getFromline();

    public abstract String getBanner();

    public abstract Date getTime();

    public abstract String getRfaxid();

    public abstract int getPages();

    public abstract String getPort();
    
    public abstract File getSource();

    /**
     * Lista plik�w stanowi�cych obraz faksu.  Jest wa�ne, by pliki
     * mia�y rozszerzenia odpowiadaj�ce ich formatowi, poniewa� nazwy
     * plik�w s� u�ywane podczas tworzenia dokument�w Docusafe.
     */ 
    public abstract File[] getFiles();

    public abstract boolean isError();

    public abstract String getErrorMessage();
    
    public abstract String getDID();

    public abstract Date getSendDate();

    public abstract Date getSendTime();
}
