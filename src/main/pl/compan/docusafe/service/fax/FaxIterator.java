package pl.compan.docusafe.service.fax;

import pl.compan.docusafe.service.ServiceException;

/* User: Administrator, Date: 2005-06-23 15:19:01 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: FaxIterator.java,v 1.4 2006/02/20 15:42:28 lk Exp $
 */
public class FaxIterator
{
    private IncomingFax[] faxes;
    private int offset;
    private int maxOffset;
    private int index;

    public FaxIterator(IncomingFax[] faxes, int offset, int count)
    {
        if (faxes == null)
            throw new NullPointerException("faxes");
        if (offset < 0 || (faxes.length > 0 && offset > faxes.length-1))
            throw new IllegalArgumentException("length="+faxes.length+" offset="+offset);
        if (offset + count > faxes.length)
            throw new IllegalArgumentException("length="+faxes.length+" offset="+offset+
                " count="+count);
        if (count < 0)
            throw new IllegalArgumentException("count="+count);
        this.faxes = faxes;
        this.offset = offset; // pierwszy element tablicy
        this.maxOffset = offset + count - 1;
        this.index = offset - 1;
    }

    public void remove() throws ServiceException
    {
        if (index < offset)
            throw new IllegalStateException("Nie wywo�ano next()");
        if (faxes[index] == null)
            throw new IllegalStateException("Ponowne wywo�anie remove() dla tego samego elementu");
        faxes[index].purge();
        faxes[index] = null;
    }

    public boolean hasNext()
    {
        return index < maxOffset;
    }

    public IncomingFax next()
    {
        if (!hasNext())
            throw new IllegalStateException("Brak kolejnych element�w do zwr�cenia");
        return faxes[++index];
    }
}
