package pl.compan.docusafe.service.fax;

import pl.compan.docusafe.service.ServiceException;

/* User: Administrator, Date: 2005-08-09 09:34:52 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: InvalidFaxException.java,v 1.3 2008/11/08 13:59:35 wkuty Exp $
 */
public class InvalidFaxException extends ServiceException
{
    public InvalidFaxException(String message)
    {
        super(message);
    }

    public InvalidFaxException(String message, Throwable cause)
    {
        super(message, cause);
    }    
}
