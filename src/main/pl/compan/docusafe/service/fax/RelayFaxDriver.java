package pl.compan.docusafe.service.fax;

import org.bouncycastle.crypto.DSA;
import org.xml.sax.SAXException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.FaxCache;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;

/* User: Administrator, Date: 2005-06-23 15:06:48 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: RelayFaxDriver.java,v 1.21 2010/07/16 14:30:20 kamilj Exp $
 */
public class RelayFaxDriver extends AbstractFaxDriver implements ActionListener
{
    private File cacheFile;
    private Map<String,Long> cache;
    //public static final Long MIN_FILE_TIME = 1 * DateUtils.MINUTE;
    public static final Long MIN_FILE_TIME = Long.parseLong(Docusafe.getAdditionProperty("FaxFileTime")) * DateUtils.MINUTE;

    public RelayFaxDriver()
    {
    }

//    public void actionPerformed(ActionEvent e)
//    {
//        if (log.isDebugEnabled())
//            log.debug("actionPerformed "+e);
//        // IncomingFax.purge()
//        RelayIncomingFax fax = (RelayIncomingFax) e.getSource();
//        cache.add(fax.getSource().getAbsolutePath());
//    }

    protected void startImport()
    {
    }

    protected void endImport()
    {
    	try {
	        saveCache(cacheFile, cache);
		} catch (Exception e) { 
			log.error(e.getMessage(),e);
			console(Console.ERROR, e.getMessage());
		}

    }

    /**
     * Zwraca Iterator ze znalezionymi faksami.
     * @return FaxIterator lub null.
     */
    protected FaxIterator iterator(File directory, String spool) throws ServiceException
    {
        if (directory == null)
            throw new ServiceException("Nie skonfigurowano katalogu na faksy przychodz�ce");
        if (spool == null)
            throw new ServiceException("Nie podano rodzaju katalogu (spool)");

        // pobieram z wskazanego katalogu pierwsze 128 plik�w znajduj�cych
        // si� w podkatalogach i zwracam je w iteratorze

        List<IncomingFax> list = null;


        try
        {
            DSApi.openAdmin();
            DSApi.context().begin();
            //TODO:Wywalic zostalo zeby zapisac plik do bazy
            loadCache(directory);
		
		    if (SPOOL_EXPORT.equals(spool))
		    {
		    	try
		    	{
		    		list = loadFromCsv(directory);
		    	}
		    	catch (Exception e) 
		    	{
					log.error(e.getMessage(),e);
					new ServiceException(e.getMessage());
				}
		    }
		    else
		    {
		        list = loadFromXml(directory);
		    }
	        DSApi.context().commit();
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            throw new ServiceException(e.getMessage());
        }
        finally
        {
            try { DSApi.close(); } catch (Exception e) { }
        }

        return new FaxIterator(list.toArray(new IncomingFax[list.size()]), 0, list.size());
    }

    private List<IncomingFax> loadFromCsv(File directory) throws EdmException
    {
        // szukam podkatalog�w, w kt�rych mog� znajdowa� si� faksy
        File[] subdirs = directory.listFiles(new FileFilter()
        {
            public boolean accept(File file)
            {
                return file.isDirectory();
            }
        });

        if (subdirs == null || subdirs.length == 0)
        {
            console(Console.INFO, "Nie znaleziono faks�w w podkatalogach katalogu "+
                directory.getAbsolutePath());
            return null;
        }

        List<IncomingFax> list = new LinkedList<IncomingFax>();
        //int count = 0;

        outer:
        for (int i=0; i < subdirs.length; i++)
        {
            File[] csvFiles = subdirs[i].listFiles(new FilenameFilter()
            {
                public boolean accept(File dir, String name)
                {
                    return name.toLowerCase().endsWith(".csv");
                }
            });

            for (int j=0; j < csvFiles.length; j++)
            {
            	List fc = FaxCache.findByFileName(csvFiles[i].getAbsolutePath());
               // if (cache.contains(csvFiles[i].getAbsolutePath()))
                if(fc != null && fc.size() > 0)
                {
                    log.info("Pomijanie pliku "+csvFiles[i]+" (by� w cache)");
                    continue;
                }

                IncomingFax fax;
                try
                {
                    BufferedReader reader = new BufferedReader(new FileReader(csvFiles[j]));
                    String line;
                    while ((line = reader.readLine()) != null)
                    {
                        fax = new RelayIncomingFax(csvFiles[i], line, subdirs[i], isSplitTiffs());
                        //count++;
                        list.add(fax);

                        ((RelayIncomingFax) fax).addActionListener(this);
                    }
                    reader.close();

                    // csvFiles[j].renameTo(new File(csvFiles[j].getAbsolutePath()+".done"));

                }
                catch (ServiceException e)
                {
                    console(Console.WARN, "Nie znaleziono pliku graficznego zwi�zanego z " +
                        "faksem "+csvFiles[i]);
                    csvFiles[i].renameTo(new File(csvFiles[i].getAbsolutePath()+".invalid"));
                }
                catch (Exception e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }

        return list;
    }

    private List<IncomingFax> loadFromXml(File directory)
        throws ServiceException
    {
        // lista plik�w xml, kt�re s� starsze ni� dwie minuty
        File[] files = directory.listFiles(new FileFilter()
        {
            public boolean accept(File file)
            {
                if (!file.isFile())
                {
                    console(Console.INFO, "Pomijanie "+file+": nie jest plikiem");
                    if (log.isDebugEnabled())
                        log.debug("Pomijanie "+file+": nie jest plikiem");
                    return false;
                }
                if (!file.getName().toLowerCase().endsWith(".xml"))
                {
                    if (log.isDebugEnabled())
                        log.debug("Pomijanie "+file+": nazwa nie ma postaci *.xml");
                    return false;
                }
                if ((System.currentTimeMillis() - file.lastModified()) < MIN_FILE_TIME)
                {
                    console(Console.INFO, "Pomijanie "+file+": nowszy ni� "+MIN_FILE_TIME+" sekund (mtime="+new Date(file.lastModified()));
                    if (log.isDebugEnabled())
                        log.debug("Pomijanie "+file+": nowszy ni� "+MIN_FILE_TIME+" sekund (mtime="+new Date(file.lastModified()));
                    return false;
                }

                return true;
            }
        });

        try
        {
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();

            List<IncomingFax> list = new LinkedList<IncomingFax>();

            for (int i=0; i < files.length; i++)
            {
            	try
            	{
            		List fc = FaxCache.findByFileName(files[i].getAbsolutePath());
                    if(fc != null && fc.size() > 0)
            //		if (cache.contains(files[i].getAbsolutePath()))
		            {
                    	//System.out.println("Pomijanie pliku "+files[i]+" (jest w cache)");
		                log.info("Pomijanie pliku "+files[i]+" (jest w cache)");
		                continue;
		            }
		
		            RelayXmlHandler handler = new RelayXmlHandler(directory);
		            InputStream is = new FileInputStream(files[i]);
		            parser.parse(is, handler);
		            org.apache.commons.io.IOUtils.closeQuietly(is);
		
		/*
		                if (handler.isError())
		                {
		                    console(Console.ERROR, "Pomijanie b��dnego faksu "+files[i]+" ("+handler.getErrorMessage()+")");
		                    log.info("Pomijanie b��dnego faksu "+files[i]+" ("+handler.getErrorMessage()+")");
		                    cache.add(files[i].getAbsolutePath());
		                    continue;
		                }
		*/
		
		            try
		            {
		                IncomingFax fax = new RelayIncomingFax(files[i], handler, isSplitTiffs());
		
		                ((RelayIncomingFax) fax).addActionListener(this);
		                list.add(fax);
		            }
		            catch (InvalidFaxException e)
		            {
		                console(Console.WARN, e.getMessage());
		                log.error(e.getMessage(),e);
		            }
                }
                catch (Exception e)
                {
                	console(Console.ERROR, "Pomijanie b��dnego faksu "+files[i]+" ("+e.getMessage()+")");
                    log.info("Pomijanie b��dnego faksu "+files[i]+" ("+e.getMessage()+")",e);
                }
            }
            return list;
        }
        catch (Exception e)
        {
            throw new ServiceException("B��d odczytu faksu"+e.getMessage(), e);
        }
    }

    protected boolean isStartWorkflowProcess()
    {
        return true;
    }

    private static final String CACHE_FILE = "docusafe.db";
    private static final int MAX_FILES_IN_CACHE = 100;
    private static final long CACHE_CLEANING_INTERVAL = DateUtils.HOUR;
    private long lastCacheCleaning = System.currentTimeMillis();


    protected void loadCache(File directory)
    {
        cacheFile = new File(directory, CACHE_FILE);
        if (!cacheFile.exists())
        {
            return;
        }
        else
        {
		    try
		    {
		        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(cacheFile));
		        Object object = ois.readObject();
		        org.apache.commons.io.IOUtils.closeQuietly(ois);
		        if (!(object instanceof Set))
		        {
		            console(Console.ERROR, "B��dny format pliku set "+cacheFile);
		            cacheFile.delete();
		            return;
		        }
		
		        Set<String> set = (Set<String>) object;
		
		        if (log.isDebugEnabled())
		            log.debug("Odczytano set z "+cacheFile+", "+set.size()+" element�w");
		
		        if (set.size() > MAX_FILES_IN_CACHE &&
		            (System.currentTimeMillis() - lastCacheCleaning) > CACHE_CLEANING_INTERVAL)
		        {
		            lastCacheCleaning = System.currentTimeMillis();
		
		            Set<String> newCache = new HashSet<String>();
		            //for (Iterator iter=set.iterator(); iter.hasNext(); )
		            for (String s : set)
		            {
		                File f = new File(s);
		                if (f.exists())
		                    newCache.add(s);
		            }
		            int delta = set.size() - newCache.size();
		
		            if (delta > 0)
		                set = newCache;
		
		            console(Console.INFO, "Czyszczenie pliku set "+cacheFile+", usuni�to "+delta+" element�w");
		        }
		        
		     //   cache = set;
			    for (String fileName : set)
			    {
					FaxCache faxCache = new FaxCache();
					faxCache.setFileName(fileName);
					faxCache.create();
				}
			    cacheFile.renameTo(new File(directory, CACHE_FILE+"old"));
		    }
		    catch (Exception e)
		    {
		        log.error(e.getMessage(), e);
		        console(Console.WARN, "B��d odczytu pliku cache "+cacheFile+": "+e.getMessage());
		    }
        }
        return;
    }

    protected void saveCache(File cacheFile, Map<String,Long> cache)
    {
        try
        {
//            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(cacheFile));
//            oos.writeObject(cache);
//            oos.close();
//            if (log.isDebugEnabled())
//                log.debug("Zapisano cache "+cacheFile+", "+cache.size()+" element�w");
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            console(Console.ERROR, "B��d zapisu pliku cache "+cacheFile);
        }
    }

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
