package pl.compan.docusafe.service.fax;

import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;

import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.ParameterBlock;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/* User: Administrator, Date: 2005-06-23 15:14:02 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: RelayIncomingFax.java,v 1.20 2010/07/15 09:07:42 kamilj Exp $
 */
public class RelayIncomingFax extends IncomingFax
{
	
    private static final Log log = LogFactory.getLog(RelayIncomingFax.class);

    /**
     * Czas, jaki musi up�yn�� od ostatniej modyfikacji pliku graficznego,
     * aby zosta� uznany za poprawny.
     */
    public static final long MIN_TIF_LIFETIME = 1 * DateUtils.MINUTE;

    private ActionListener listener;

    private File source;

    /**
     * Oryginalny plik tif zwi�zany z faksem; klasa tworzy w�asn�
     * kopi� tego pliku.
     */
    private File tifFile;

    String subject;
    String fromline;
    String banner;
    Date time;
    String rfaxid;
    int pages;
    String port;
    File[] files;
    /**
     * ID faksu, dost�pne tylko przy imporcie z plik�w XML.
     */
    String faxID;
    boolean error;
    String errorMessage;
    String did;

    private boolean splitTiffs;
    private Date sendDate;
    private Date sendTime;


    public void addActionListener(ActionListener l)
    {
        listener = AWTEventMulticaster.add(listener, l);
    }

    /**
     * @param source
     * @param handler
     * @throws InvalidFaxException Je�eli plik graficzny nie istnieje lub jest
     *  m�odszy ni� {@link #MIN_TIF_LIFETIME} milisekund.
     */
    public RelayIncomingFax(File source, RelayXmlHandler handler, boolean splitTiffs)
        throws InvalidFaxException
    {
        this.source = source;
        this.splitTiffs = splitTiffs;

        tifFile = handler.getImageFile();
/*
        if (tifFile == null || !tifFile.exists() || !tifFile.isFile() || tifFile.length() == 0)
            throw new InvalidFaxException("Nie znaleziono pliku zwi�zanego z faksem ("+tifFile+
                "), jest to katalog lub plik ma zerow� d�ugo�� ("+source+")");
*/

        if ((System.currentTimeMillis() - tifFile.lastModified()) < MIN_TIF_LIFETIME)
            throw new InvalidFaxException("Plik "+tifFile+" zosta� ostatnio zmodyfikowany nie "+
                "wi�cej ni� "+MIN_TIF_LIFETIME+" sekund temu, pomijanie ("+source+")");

        faxID = handler.getRelayID();

        rfaxid = handler.getRfaxid();
        pages = handler.getPages();

        
        if (pages == 0)
            throw new InvalidFaxException("Faks ma zero stron ("+source+")"); //debug.log
		

        port = handler.getPort();
        time = handler.getTime();
        error = handler.isError();
        errorMessage = handler.getErrorMessage();
        did = handler.getDid();
        sendDate = handler.getSendDate();
        sendTime = handler.getSendTime();
        try
        {
            this.files = getFiles(tifFile);
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
            throw new InvalidFaxException("B��d odczytu lub zapisu pliku graficznego, by� mo�e na dysku "+
                "sko�czy�o si� miejsce");
        }
    }

    /**
     * Dostaje wiersz z pliku csv opisuj�cy jeden faks.  Tworzy w�asn�
     * kopi� pliku tif zwi�zanego z faksem.
     * @param csvLine
     * @param directory
     * @throws IOException
     * @throws ServiceException
     */
    public RelayIncomingFax(File source, String csvLine, File directory, boolean splitTiffs)
        throws IOException, ServiceException
    {
        if (csvLine == null)
            throw new NullPointerException("csvLine");

        this.source = source;
        this.splitTiffs = splitTiffs;

        String[] fields = csvLine.split(",");

        if (fields.length < 54)
            throw new ServiceException("Niepoprawny format pliku csv (niedostateczna " +
                "liczba p�l: "+fields.length+"), <"+csvLine+">");

        // TNUMBER
        int tnumber = readInt(fields[0], -1);
        if (tnumber < 0)
            throw new ServiceException("Brak obrazu zwi�zanego z plikiem csv <"+csvLine+">");

        tifFile = new File(directory, tnumber+".tif");
        if (!tifFile.exists() || !tifFile.isFile() || tifFile.length() == 0)
            throw new ServiceException("Nie znaleziono pliku zwi�zanego z faksem ("+tifFile+
                "), jest to katalog lub plik ma zerow� d�ugo�� <"+csvLine+">:");

        // FSTATUS
        if (readInt(fields[1], -1) != 16)
            throw new ServiceException("Spodziewano si� faksu przychodz�cego <"+csvLine+">");

        // SUBJECT
        subject = readString(fields[5]);

        // FROMLINE
        fromline = readString(fields[6]);

        // BANNER
        banner = readString(fields[7]);

        // RFAXID
        rfaxid = readString(fields[30]);

        // PAGESSENT
        pages = readInt(fields[34], 0);

        if (pages == 0)
            throw new ServiceException("Faks ma zero stron <"+csvLine+">"); //debug.log

        // PORT
        port = readString(fields[36]);

        int year = readInt(fields[23], 0);
        int month = readInt(fields[24], 0);
        int date = readInt(fields[25], 0);
        int hour = readInt(fields[26], 0);
        int minute = readInt(fields[27], 0);
        int second = readInt(fields[28], 0);

        if (year < 1980 || month < 1 || date < 1)
        {
            time = new Date();
        }
        else
        {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month-1);
            cal.set(Calendar.DATE, date);
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, minute);
            cal.set(Calendar.SECOND, second);
            time = cal.getTime();
        }

        this.files = getFiles(tifFile);
    }

    /**
     * Dzieli na strony przekazany plik TIFF.  Je�eli w pliku jest tylko
     * jedna strona, plik jest pozostawiany bez zmian, w przeciwnym razie
     * jest dzielony na pliki PNG.
     */
    File[] getFiles(File tif) throws IOException, InvalidFaxException
    {
        if (tif == null || !tif.exists() || !tif.isFile())
        {
            log.info("Nie znaleziono pliku zwi�zanego z faksem "+source+": "+tif);
            return new File[0];
        }

        File[] result = null;

        final String format = "png";

        // dzielenie tifa na strony
        SeekableStream s = null;
        try
        {
            s = new FileSeekableStream(tif);
            ImageDecoder dec = ImageCodec.createImageDecoder("tiff", s, null);

            if (dec.getNumPages() == 0)
                throw new InvalidFaxException("W pliku "+tif+" brakuje stron ("+source+")");

            if (splitTiffs && dec.getNumPages() > 1)
            {
                List imageFiles = new ArrayList(dec.getNumPages());

                for (int i=0; i < dec.getNumPages(); i++)
                {
                    File tmp = File.createTempFile("docusafe_fax", "."+format);

                    RenderedImage im = dec.decodeAsRenderedImage(i);

                    if (log.isDebugEnabled())
                        log.debug("width="+im.getWidth()+", height="+im.getHeight());

                    // faksy mog� przychodzi� w rozdzielczo�ci 204x196 dpi lub 204x98 dpi
                    // w tym drugim wypadku nale�y faks przeskalowa� o czynnik 2.0 w pionie
                    if (im.getWidth() > im.getHeight())
                    {
                        if (log.isDebugEnabled())
                            log.debug("skalowanie 1.0x2.0");
                        ParameterBlock pb = new ParameterBlock();
                        pb.addSource(im);
                        pb.add(new Float(1.0));
                        pb.add(new Float(2.0));
                        pb.add(new Float(0.0));
                        pb.add(new Float(0.0));
                        im = JAI.create("scale", pb);
                    }

                    if (log.isDebugEnabled())
                        log.debug("Tworzenie pliku strumieniem");
                    // stream
                    FileOutputStream os = new FileOutputStream(tmp);
                    ParameterBlock pb = new ParameterBlock();
                    pb.addSource(im);
                    pb.add(os);
                    pb.add(format);
                    RenderedOp op = JAI.create("encode", pb);
                    forceLoad(op);
                    op.dispose();
                    os.close();

                    log.info("Utworzono plik "+tmp+" o rozmiarze "+tmp.length());

                    imageFiles.add(tmp);
                    //result = (File[]) imageFiles.toArray(new File[imageFiles.size()]);
                }
                result = (File[]) imageFiles.toArray(new File[imageFiles.size()]);
            }
        }
        catch (Exception e)
        {
            //throw new InvalidFaxException("B��d analizy faksu: "+e.toString()+" ("+source+")", e);
            log.error("TO JEST TYLKO POWIADOMIENIE "+e.getMessage(), e);
        }
        finally
        {
            if (s != null) s.close();
        }

        if (result == null)
        {
            File image = File.createTempFile("docusafe_fax", ".tif");
            FileChannel chin = new FileInputStream(tif).getChannel();
            FileChannel chout = new FileOutputStream(image).getChannel();
            chout.transferFrom(chin, 0, chin.size());

            chin.close();
            chout.close();

            result = new File[] { image };
        }

        return result;
    }

    private static void forceLoad(RenderedImage op)
    {
        // wymuszanie zako�czenia operacji
        // http://java.sun.com/products/java-media/jai/forDevelopers/jaifaq.html#deferred

        if (op instanceof RenderedOp)
        {
            ((RenderedOp) op).getRendering();
        }

        int minX = op.getMinTileX();
        int minY = op.getMinTileY();
        int maxX = minX + op.getNumXTiles();
        int maxY = minY + op.getNumYTiles();

        for (int y = minY; y < maxY; y++)
        {
            for (int x = minX; x < maxX; x++)
            {
                op.getTile(x, y);
            }
        }
    }

/*
    public RelayIncomingFax(File file) throws IOException, ServiceException
    {
        if (file == null)
            throw new NullPointerException("csvFile");
        this.csvFile = file;

        BufferedReader reader = new BufferedReader(new FileReader(csvFile));
        String line = reader.readLine();
        reader.close();

        if (line == null)
            throw new ServiceException("Niepoprawny formatu pliku csv: "+csvFile);

        StringTokenizer st = new StringTokenizer(line, ",");

        String[] fields = line.split(",");
        if (fields.length < 54)
            throw new ServiceException("Niepoprawny format pliku csv (niedostateczna " +
                "liczba p�l: "+st.countTokens()+"), <"+line+">: "+csvFile);

        // TNUMBER
        int tnumber = readInt(fields[0], -1);
        if (tnumber < 0)
            throw new ServiceException("Brak obrazu zwi�zanego z plikiem csv <"+line+">: "+csvFile);

        tifFile = new File(csvFile.getParentFile(), tnumber+".tif");
        if (!tifFile.exists() || !tifFile.isFile() || tifFile.length() == 0)
            throw new ServiceException("Nie znaleziono pliku zwi�zanego z faksem ("+tifFile+
                "), jest to katalog lub plik ma zerow� d�ugo�� <"+line+">: "+csvFile);

        // FSTATUS
        if (readInt(fields[1], -1) != 16)
            throw new ServiceException("Spodziewano si� faksu przychodz�cego <"+line+">: "+csvFile);

        // SUBJECT
        subject = readString(fields[5]);

        // FROMLINE
        fromline = readString(fields[6]);

        // BANNER
        banner = readString(fields[7]);

        // RFAXID
        rfaxid = readString(fields[30]);

        // PAGESSENT
        pages = readInt(fields[34], 0);

        if (pages == 0)
            throw new ServiceException("Faks ma zero stron <"+line+">: "+csvFile);

        int year = readInt(fields[23], 0);
        int month = readInt(fields[24], 0);
        int date = readInt(fields[25], 0);
        int hour = readInt(fields[26], 0);
        int minute = readInt(fields[27], 0);
        int second = readInt(fields[28], 0);

        if (year < 1980 || month < 1 || date < 1)
        {
            time = new Date();
        }
        else
        {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month-1);
            cal.set(Calendar.DATE, date);
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, minute);
            cal.set(Calendar.SECOND, second);
            time = cal.getTime();
        }

        // tworzenie w�asnej kopii pliku z obrazem faksu
        File image = File.createTempFile("docusafe_fax", ".tif");
        FileChannel chin = new FileInputStream(tifFile).getChannel();
        FileChannel chout = new FileOutputStream(image).getChannel();
        chout.transferFrom(chin, 0, chin.size());

        chin.close();
        chout.close();

        files = new File[] { image };
    }
*/

    /**
     * Odczytuje zawarto�� pola jako napis (jego tre�� mo�e, ale nie musi,
     * by� zawarta w cudzys�owach).
     * @param field
     * @return
     */
    private String readString(String field)
    {
        if (field == null)
            return null;
        if (field.trim().length() == 0)
            return null;
        if (field.length() == 1)
            return field;
        if (field.charAt(0) == '"' && field.charAt(field.length()-1) == '"')
            return field.substring(1, field.length()-1);
        return field.trim();
    }

    private int readInt(String field, int def)
    {
        if (field == null)
            return def;
        try
        {
            return Integer.parseInt(field);
        }
        catch (NumberFormatException e)
        {
            return def;
        }
    }

    public File getSource()
    {
        return source;
    }

    public void purge() throws ServiceException
    {
//        if (tifFile.exists() && !tifFile.delete())
//            throw new ServiceException("Nie mo�na usun�� pliku "+tifFile);

//        tifFile.delete();

        if (files != null && files.length > 0)
        {
            for (int i=0; i < files.length; i++)
            {
                if (files[i].exists()) files[i].delete();
            }
        }

        if (listener != null)
            listener.actionPerformed(new ActionEvent(this, 0, "purge"));
    }

    public String getSubject()
    {
        return subject;
    }

    public String getFromline()
    {
        return fromline;
    }

    public String getBanner()
    {
        return banner;
    }

    public Date getTime()
    {
        return time;
    }

    public String getRfaxid()
    {
        return rfaxid;
    }

    public int getPages()
    {
        return pages;
    }

    public String getPort()
    {
        return port;
    }

    public File[] getFiles()
    {
        return files;
    }

    public String getFaxID()
    {
        return faxID;
    }

    public boolean isError()
    {
        return error;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }
    
    public String getDID()
    {
    	return did;
    }

    public String toString()
    {
        return getClass().getName()+"[source="+source+"]";
    }

    public Date getSendDate() {
        return sendDate;
    }

    public Date getSendTime() {
        return sendTime;
    }
}
