package pl.compan.docusafe.service.fax;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AccessLog;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.FaxCache;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.*;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.parametrization.ilpol.ServicesUtils;
import pl.compan.docusafe.parametrization.ilpoldwr.CokUtils;
import pl.compan.docusafe.parametrization.ilpoldwr.IlpolCokLogic;
import pl.compan.docusafe.service.*;
import pl.compan.docusafe.service.stagingarea.RetainedObject;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.StringManager;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

import com.sdicons.json.validator.impl.predicates.Bool;

/* User: Administrator, Date: 2005-06-23 14:54:16 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: AbstractFaxDriver.java,v 1.46 2010/08/04 10:37:57 kamilj Exp $
 */
public abstract class AbstractFaxDriver extends ServiceDriver implements Service
{
    private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(AbstractFaxDriver.class.getPackage().getName(),null);
    
    public static final int PERIOD = 60;

    public static final String SPOOL_EXPORT = "export";
    public static final String SPOOL_INTERNAL = "internal";

    private Property[] properties;

    private File directory;
    private String userName;
    private String password;
    private String guid;
    private String spoolType;
    private Boolean splitTiffs;
    private Boolean retainFaxes;
    private Boolean onlyArchive;

    private Integer com1delivery;
    private Integer com2delivery;
    private Integer com3delivery;
    private Integer com4delivery;

    private Timer timer;
    private final FaxReceiver faxReceiver = new FaxReceiver();

    protected abstract FaxIterator iterator(File directory, String spool) throws ServiceException;
    protected abstract void startImport();
    protected abstract void endImport();

    protected AbstractFaxDriver()
    {
        properties = new Property[]
        {
        	new UsernameProperty(),
            new PasswordProperty(),
            new DirectoryProperty(),
            new GuidProperty(),
            new SpoolTypeProperty(),
            new SplitTiffsProperty(),
            new OnlyArchiveProperty(),
            new RetainFaxesProperty(),
            new Com1DeliveryProperty(),
            new Com2DeliveryProperty(),
            new Com3DeliveryProperty(),
            new Com4DeliveryProperty()
        };
    }

    public final Property[] getProperties()
    {
        return properties;
    }

    class FaxReceiver extends TimerTask
    {
        private boolean running;
        private volatile boolean iSaidIt;

        public void run()
        {

            synchronized (this)
            {
                if (running)
                {
                    console(Console.WARN, sm.getString("PoprzedniImportNieZostalZakonczonyPomijanie"));
                    return;
                }
            }

            if (directory == null || !directory.exists())
            {
                if(!iSaidIt){
                    console(Console.ERROR, sm.getString("NieOkreslonoKataloguWktorymZnajdujaSieFaksy"));
                    iSaidIt = true;
                }
                return;
            }

            if (spoolType == null ||
                !SPOOL_EXPORT.equals(spoolType) && !SPOOL_INTERNAL.equals(spoolType))
            {
                console(Console.ERROR, sm.getString("NieOkreslonoRodzajuKolejki"));
                return;
            }
            if(guid == null)
            {
            	guid = DSDivision.ROOT_GUID;
            }
            if (isStartWorkflowProcess() && guid == null)
            {
                console(Console.ERROR, sm.getString("NieOkreslonoDzialuDoDekretacjiFaksow"));
                return;
            }

            synchronized (this)
            {
                running = true;
            }

            try
            {
                startImport();

                FaxIterator iter = iterator(directory, spoolType);

                if (iter == null)
                    return;

                List<String> faxValidForCok = CokUtils.prepareValidFaxNumbersForCok();

                while (iter.hasNext())
                {
                    IncomingFax fax = iter.next();

                    try
                    {
                        if (retainFaxes != null && retainFaxes)
                        {
                            retainDocument(fax);
                            if(!fax.isError())
                                console(Console.INFO, sm.getString("DoPoczekalniPrzyjetoFaksPort",fax,fax.getPort()));
                            else
                                console(Console.INFO, sm.getString("DoPoczekalniPrzyjetoFaksPortBlad",fax,fax.getPort(),fax.getErrorMessage()));
                        }
                        else
                        {
                            try
                            {
                                DSApi.openAdmin();
                                DSApi.context().begin();

                                if(faxValidForCok.contains(fax.getDID())){
                                    InOfficeDocument document = new InOfficeDocument();
                                    CokUtils.createDocumentFromFax(document, DocumentKind.findByCn(IlpolCokLogic.DOCKIND_NAME), DSApi.context().getDSUser(), null, null, "Dokument utworzony z faksu", "Zg�oszenie COK", "Dokument utworzony z faksu", fax);
                                    console(Console.INFO, sm.getString("UtworzonoZadanieZfaksu", fax, fax.getPort()));
                                }
                                //  czy jesli tworzymy taska z faksu to mamy dodatkowo tworzyc jeszcze jeden dokument do tego faksu?
                                importDocument(fax);

                                if(!fax.isError())
                                    console(Console.INFO, sm.getString("PrzyjetoFaksPort",fax ,fax.getPort()));
                                else
                                    console(Console.INFO, sm.getString("PrzyjetoFaksPortBlad",fax ,fax.getPort(),fax.getErrorMessage()));

                                DSApi.context().commit();
                            }
                            catch (EdmException e)
                            {
                                DSApi.context().setRollbackOnly();
                                throw e;
                            }
                            finally
                            {
                                try { DSApi.close(); } catch (Exception e) { }
                            }
                        }
                        iter.remove();
                    }
                    catch (Exception e)
                    {
                        console(Console.ERROR, sm.getString("BladPodczasPrzyjmowaniaFaksuZnumeru",fax.getRfaxid(),e.getMessage()), e);
                    }
                }
            }
            catch (Exception e)
            {
                console(Console.ERROR, sm.getString("BladPodczasPrzyjmowaniaFaksow",e.getMessage()), e);
            }
            finally
            {
                synchronized (this)
                {
                    running = false;
                }
                endImport();
            }
        }

        private void retainDocument(IncomingFax fax) throws EdmException, ServiceException
        {
            String title;
            if (fax.getSubject() != null && fax.getSubject().length() > 0)
            {
                title = (sm.getString("Faks")+" ("+fax.getSubject()+")");
            }
            else
            {
                title = (sm.getString("Faks") + (fax.getFaxID() != null ? " ("+fax.getFaxID()+")" : ""));
            }

            try
            {
                DSApi.openAdmin();
                DSApi.context().begin();
                Map<String, String> properties = new HashMap<String, String>();
                properties.put("banner", fax.getBanner());
                properties.put("faxID", fax.getFaxID());
                properties.put("subject", fax.getSubject());
                properties.put("fromline", fax.getFromline());
                properties.put("rfaxid", fax.getRfaxid());
                properties.put("port", fax.getPort());
                RetainedObject.create(title, "fax", properties, fax.getFiles());
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                throw e;
            }
            finally
            {
                DSApi._close();
            }
        }

        private void importDocument(IncomingFax fax) throws EdmException, ServiceException
        {
            File[] faxFiles = getFaxFiles(fax);

            InOfficeDocument doc = new InOfficeDocument();
            doc.setSource("fax");
            setDocumentSender(doc, fax);

            doc.setPackageRemarks(getPackageRemarks(fax));

            doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
            doc.setCurrentAssignmentAccepted(Boolean.FALSE);
            //doc.setDivisionGuid(DSDivision.ROOT_GUID);

            // data, z jak� zostanie utworzony wpis w dzienniku
            Date entryDate;
            Long journalId;
            Integer sequenceId;
            // sprawdzam, czy w bazie istnieje ju� dokument o danym referenceId
            Calendar currentDay = Calendar.getInstance();
            currentDay.setTime(GlobalPreferences.getCurrentDay());
            entryDate = new Date(currentDay.getTime().getTime());

            Journal journal = Journal.getMainIncoming();
            journalId = journal.getId();

            doc.setCreatingUser(DSApi.context().getPrincipalName());
            doc.setDocumentDate(fax.getTime());

            setupCurrentDay(currentDay);
            doc.setIncomingDate(currentDay.getTime());

            doc.setAssignedDivision(DSDivision.ROOT_GUID);

            // referent nie jest na razie przydzielany
            if (fax.getSubject() != null && fax.getSubject().length() > 0)
            {
                if (onlyArchive == null || onlyArchive.equals(Boolean.FALSE))
                    doc.setSummary(sm.getString("Faks") + " (" + fax.getSubject() + ")");
                else
                    doc.setSummary("Faks z numeru " + fax.getRfaxid() + " (" + fax.getSubject() + ")");
            }
            else
            {
                if (onlyArchive == null || onlyArchive.equals(Boolean.FALSE))
                    doc.setSummary(sm.getString("Faks") + (fax.getFaxID() != null ? " (" + fax.getFaxID() + ")" : ""));
                else
                    doc.setSummary("Faks z numeru " + fax.getRfaxid() + (fax.getFaxID() != null ? " (" + fax.getFaxID() + ")" : ""));
            }

            setKind(doc);

//                Calendar cal = Calendar.getInstance();
//                cal.add(Calendar.DATE, DateUtils.getBusinessDays(kind.getDays()));
//                doc.setStatus(InOfficeDocumentStatus.find(statusId));
//                doc.setDelivery(InOfficeDocumentDelivery.find(deliveryId));
//                doc.setOutgoingDelivery(OutOfficeDocumentDelivery.find(outgoingDeliveryId));

            setDelivery(fax, doc);
            doc.setDocumentKind(DocumentKind.findByCn(DocumentKind.NORMAL_KIND));
            doc.create();

            doc.getDocumentKind().set(doc.getId(), new HashMap<String,Object>());
            setupDocumentPermissions(doc, fax);

            if (onlyArchive == null || onlyArchive.equals(Boolean.FALSE))
                doc.getDocumentKind().logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
            else
                archive(doc, fax);

            // za��czniki mo�na tworzy� dopiero po zapisaniu dokumentu,
            // inaczej w Document.onSave wyst�puje b��d zapisywania dokumentu
            // nie zwi�zanego z folderem (createAttachment zapisuje dokument
            // w bazie).
            /*
            if (kind.getRequiredAttachments() != null && kind.getRequiredAttachments().size() > 0)
            {
                for (Iterator iter=kind.getRequiredAttachments().iterator(); iter.hasNext(); )
                {
                    InOfficeDocumentKindRequiredAttachment req = (InOfficeDocumentKindRequiredAttachment) iter.next();
                    doc.createAttachment(new Attachment(req.getTitle()));
                }
            }
            */
            setAttachments(fax, doc);
            // tworzenie wpisu w dzienniku


            try
            {
                sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
                log.info("Przyj�to faks pod numerem KO: "+sequenceId);
            }
            catch (EdmException e)
            {
                throw e;
            }

            cacheFax(fax, doc);
            log.info("Przyj�to faks jako dokument " + doc.getId());


            
            if (onlyArchive == null || onlyArchive.equals(Boolean.FALSE))
            	createProcess(doc, journalId, sequenceId);
        }
    }

    protected void cacheFax(IncomingFax fax, Document document) throws EdmException {
        String faxFileName = fax.getSource().getAbsolutePath();
        Long documentId = document.getId();
        if(FaxCache.findByFileNameAndDocId(faxFileName, documentId).size() == 0){
            FaxCache faxCache = new FaxCache();
            faxCache.setFileName(faxFileName);
            faxCache.setDocumentId(documentId);
            if(AvailabilityManager.isAvailable("faxSetTime"))
                faxCache.setCtime(fax.getTime());
            faxCache.create();
            LoggerFactory.getLogger("krzysiekm").debug("Stworzono faks o nazwie " + faxFileName + " oraz o id dokumentu " + documentId);
        } else{
            LoggerFactory.getLogger("krzysiekm").debug("Juz istnieje faks o nazwie " + faxFileName + " oraz dokumencie z id " + documentId);
        }
    }

    protected File[] getFaxFiles(IncomingFax fax) throws ServiceException
    {
    	File[] faxFiles = fax.getFiles();
        if (faxFiles != null)
        {
            for (int i=0; i < faxFiles.length; i++)
            {
                if (!faxFiles[i].exists() || !faxFiles[i].isFile())
                    throw new ServiceException(sm.getString("NieIstniejePlikZwiazanyZfaksem")+
                        faxFiles[i]);
            }
        }
        
        return faxFiles;
    }
    
    protected void setDocumentSender(InOfficeDocument doc, IncomingFax fax)
    {
    	Sender sender = new Sender();
//      sender.setTitle(TextUtils.trimmedStringOrNull(senderTitle, 30));
//      sender.setFirstname(TextUtils.trimmedStringOrNull(senderFirstname, 50));
//      sender.setLastname(TextUtils.trimmedStringOrNull(senderLastname, 50));
//      sender.setOrganization(TextUtils.trimmedStringOrNull(senderOrganization, 50));
    	sender.setOrganization(fax.getFromline());
    	sender.setFax(fax.getRfaxid());
    	doc.setSender(sender);
    }
    
    protected void setupCurrentDay(Calendar currentDay)
    {
    	Date breakOfDay = DateUtils.endOfDay(currentDay.getTime());

        // je�eli faktyczna data jest p�niejsza ni� otwarty
        // dzie�, pozostawiam dat� i ustawiam godzin� 23:59:59.999
        if (new Date().after(breakOfDay))
        {
            currentDay.setTime(breakOfDay);
        }
        else
        {
            Calendar now = Calendar.getInstance();
            currentDay.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
            currentDay.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
            currentDay.set(Calendar.SECOND, now.get(Calendar.SECOND));
        }
    }
    
    protected String getPackageRemarks(IncomingFax fax)
    {
    	StringBuilder remarks = new StringBuilder();
    	remarks.append(sm.getString("FaksZnumeru")).append(" ").append(fax.getRfaxid());
    	if (fax.isError())
    	{
    		remarks.append("\n").append(sm.getString("BladTransmisji")).append(" \"")
    			.append(fax.getErrorMessage()).append("\"");
    	}
    	if (fax.getFiles() == null || fax.getFiles().length == 0)
    		remarks.append("\n").append("FaksNiePosiadalPlikuGraficznego");
    	
    	return remarks.toString();
    }
    
    protected void setKind(InOfficeDocument doc) throws EdmException
    {
    	InOfficeDocumentKind kind = null;
        List kinds = InOfficeDocumentKind.list();
        if (kinds.size() == 0)
            throw new EdmException(sm.getString("BrakZdefiniowanychTypowDokumentow"));
        for (Iterator iter=kinds.iterator(); iter.hasNext(); )
        {
            InOfficeDocumentKind k = (InOfficeDocumentKind) iter.next();
            if (k.getName().indexOf("Faks") >= 0 ||
                k.getName().indexOf("Fax") >= 0 ||
                k.getName().indexOf("faks") >= 0 ||
                k.getName().indexOf("fax") >= 0)
            {
                kind = k;
                break;
            }
        }

        if (kind != null)
            doc.setKind(kind);
        else
        {
            kind = (InOfficeDocumentKind) kinds.iterator().next();
            doc.setKind(kind);
        }

        if (log.isDebugEnabled())
            log.debug(sm.getString("WybranyRodzajPisma")+doc.getKind().getName());
    }
    
    protected void setupDocumentPermissions(InOfficeDocument document, IncomingFax fax) throws EdmException
    {
    	if (AvailabilityManager.isAvailable("archive.fax.permission"))
		{
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
			String faxNumber = StringUtils.isNotEmpty(fax.getDID()) ? fax.getDID() : "NO";
			perms.add(new PermissionBean(ObjectPermission.READ,"FAX_READ_" + faxNumber, ObjectPermission.GROUP, "Faksy z numeru "  + faxNumber + " - odczyt"));
	   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "FAX_READ_ATT_READ_" + faxNumber, ObjectPermission.GROUP, "Zalaczniki faksu z numeru "  + faxNumber + " - odczyt"));
	   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"FAX_READ_MODIFY_" + faxNumber,ObjectPermission.GROUP, "Faksy z numeru "  + faxNumber + " - modyfikacja"));
	   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"FAX_READ_ATT_MODIFY_" + faxNumber, ObjectPermission.GROUP, "Zalczniki faksow z numeru "  + faxNumber + " - modyfikacja"));
	   	 	perms.add(new PermissionBean(ObjectPermission.DELETE,"FAX_READ_DELETE_" + faxNumber, ObjectPermission.GROUP, "Faksy z numeru "  + faxNumber + " - usuwanie"));
	   	 	
			for (PermissionBean perm : perms)    		
	    	{
	    		if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
	    		{
	    			String groupName = perm.getGroupName();
	    			if (groupName == null)
	    			{
	    				groupName = perm.getSubject();
	    			}
	    			createOrFind(document, groupName,perm.getSubject());
	    		}
	    		DSApi.context().grant(document, perm);
	    	}
		}
    	else
    	{
    		document.getDocumentKind().logic().documentPermissions(document);
    	}
    }
    
    protected DSDivision createOrFind(Document document,String name, String guid) throws EdmException
    {        
        try
        {
            return DSDivision.find(guid);
        }
        catch (Exception e)
        {
            DSDivision parent = DSDivision.find(Docusafe.getAdditionProperty("baseGuid"));
            return parent.createGroup(name, guid); 
        }
        
    }
    
    protected void setDelivery(IncomingFax fax, InOfficeDocument doc) throws EdmException
    {
    	if (fax.getPort() != null &&
                fax.getPort().length() == 4 &&
                fax.getPort().toUpperCase().startsWith("COM"))
        {
            try
            {
                int port = Integer.parseInt(fax.getPort().substring(3));
                if (port >= 1 && port <= 4)
                {
                    Integer deliveryId = getComDelivery(port);
                    if (deliveryId != null)
                    {
                        doc.setDelivery(InOfficeDocumentDelivery.find(deliveryId));
                        if (log.isDebugEnabled())
                            log.debug(sm.getString("WybranyRodzajDostarczenia")+doc.getDelivery().getName());
                    }
                }
            }
            catch (NumberFormatException e)
            {
            }
            // InOfficeDocumentDelivery
            catch (EntityNotFoundException e)
            {
            }
        }
    }
    
    protected void setAttachments(IncomingFax fax, Document doc) throws EdmException
    {
    	File[] files = fax.getFiles();
        for (int i=0; files != null && i < files.length; i++)
        {
            if (files[i].exists())
            {
                Attachment attachment = new Attachment(sm.getString("Skan")+(i+1));
                doc.createAttachment(attachment);
                String filename = files[i].getName();
                String ext;
                if (filename.indexOf('.') > 0)
                    ext = filename.substring(filename.indexOf('.')+1);
                else
                    ext = "tif";
                log.info("Tworzenie za��cznika z pliku "+files[i]+" o rozmiarze "+files[i].length());
                attachment.createRevision(files[i]).setOriginalFilename("skan."+ext);
                //System.out.println(filename +" "+ DateUtils.formatJsDateTime(fax.getTime()));
            }
        }
    }

    protected void archive(InOfficeDocument doc, IncomingFax fax) throws EdmException
    {
    	FolderInserter.root()
    		.subfolder("Faksy")
    		.subfolder(StringUtils.isEmpty(fax.getDID()) ? "Brak numeru" : fax.getDID())
    		.insert(doc);
    }
    
    protected void createProcess(InOfficeDocument doc, Long journalId, Integer sequenceId)
    	throws EdmException
    {
    	try
        {
            DSApi.openAdmin();

            DSApi.context().begin();

            // powi�zanie dokumentu z bie��c� sesj�
            doc = InOfficeDocument.findInOfficeDocument(doc.getId());
            doc.bindToJournal(journalId, sequenceId);

            // tworzenie i uruchamianie wewn�trznego procesu workflow
           // LogFactory.getLog("mariusz").debug(guid);
            WorkflowFactory.createNewProcess(doc, false, "Domy�lnie", "ds_guid_"+guid, guid, null);
            //LogFactory.getLog("mariusz").debug("DOC ID "+ doc.getId());
            doc.setCurrentAssignmentGuid(guid);
            doc.setDivisionGuid(guid);

            TaskSnapshot.updateByDocumentId(doc.getId(), doc.getStringType());
            DSApi.context().commit();

            AccessLog.logPersonalDataInput(DSApi.context().getPrincipalName(),
                doc.getId(),
                Arrays.asList(new Sender[] { doc.getSender() }));
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            throw e;
        }
        finally
        {
            try { DSApi.close(); } catch (Exception e) { }
        }
    }

    protected boolean isStartWorkflowProcess()
    {
        return true;
    }

    protected boolean isSplitTiffs()
    {
        return splitTiffs != null && splitTiffs.booleanValue();
    }

    /* ServiceDriver */

    protected void start() throws ServiceException
    {
        long period = PERIOD * DateUtils.SECOND;

        timer = new Timer(true);
        timer.schedule(faxReceiver, 60*1000, period);
        console(Console.INFO, sm.getString("UruchamianieSterownikaOdbiorFaksowCoSekund",PERIOD));
    }

    protected void stop() throws ServiceException
    {
        timer.cancel();
        timer = null;
        console(Console.INFO, sm.getString("WylaczanieSterownika"));
        // faxReceiver.join() ?
    }

    protected boolean canStop()
    {
        return true;
    }

    public boolean hasConsole()
    {
        return true;
    }


    protected Integer getComDelivery(int com)
    {
        switch (com)
        {
            case 1:
                return com1delivery;
            case 2:
                return com2delivery;
            case 3:
                return com3delivery;
            case 4:
                return com4delivery;
            default:
                throw new RuntimeException(sm.getString("NieznanyNumerPortuCOM",com));
        }
    }

    protected void setComDelivery(int com, Integer deliveryId)
    {
        switch (com)
        {
            case 1:
                com1delivery = deliveryId;
                break;
            case 2:
                com2delivery = deliveryId;
                break;
            case 3:
                com3delivery = deliveryId;
                break;
            case 4:
                com4delivery = deliveryId;
                break;
            default:
                throw new RuntimeException(sm.getString("NieznanyNumerPortuCOM",com));
        }
    }

    class DirectoryProperty extends Property
    {
        public DirectoryProperty()
        {
            super(SIMPLE, PERSISTENT, AbstractFaxDriver.this, "directory", sm.getString("KatalogZfaksami"), String.class);
        }

        protected Object getValueSpi()
        {
        	if (directory != null)
        		return directory.getAbsolutePath();
        	
            return null;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null)
                return;
            
            String path = (String) object;
            File tmp = new File(path);
            if (!tmp.exists() && StringUtils.isNotEmpty(userName) && StringUtils.isNotEmpty(password)
            		&& tmp.getAbsolutePath().startsWith("\\\\"))
            {
            	try {
                    // Uruchomienie uslugi NET USE
                    String command = "cmd /c net use " + tmp.getAbsolutePath() + " /USER:" + userName + " " + password;
                    Process process = Runtime.getRuntime().exec(command);
                    
                } catch (Exception e) {
                	log.error("",e);
                	throw new ServiceException(sm.getString("nieIstniejeLubNieJestKatalogiem", tmp));
                }
            }
            else if (!tmp.isDirectory())
                throw new ServiceException(sm.getString("nieIstniejeLubNieJestKatalogiem", tmp));
            else if (!tmp.canWrite())
                throw new ServiceException(sm.getString("BrakPrawZapisuWkatalogu", tmp));
            
            directory = tmp;
        }
    }
    
    class UsernameProperty extends Property
    {
    	public UsernameProperty() 
    	{
    		super(SIMPLE, PERSISTENT, AbstractFaxDriver.this, "username", sm.getString("NazwaUzytkownika"), String.class);
		}
    	
    	protected Object getValueSpi()
        {
            return userName;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null)
                return;
            if (!(object instanceof String))
            	throw new ServiceException("W�a�ciwo�� NazwaUzytkownika nie jest ciagiem znakowym!");
            userName = (String) object;
        }
    }
    
    class PasswordProperty extends Property
    {
    	public PasswordProperty() 
    	{
    		super(SIMPLE, PERSISTENT, AbstractFaxDriver.this, "password", sm.getString("Haslo"), String.class);
		}
    	
    	protected Object getValueSpi()
        {
            return password;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null)
                return;
            if (!(object instanceof String))
            	throw new ServiceException("W�a�ciwo�� Haslo nie jest ciagiem znakowym!");
            password = (String) object;
        }
    }

    class GuidProperty extends Property
    {
        public GuidProperty()
        {
            super(SIMPLE, PERSISTENT, AbstractFaxDriver.this, "guid", sm.getString("DzialDoDekretacji"), String.class, GUID);
        }

        protected Object getValueSpi()
        {
            return guid;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null)
                return;
            String divisionGuid = (String) object;
            try
            {
                DSDivision division = DSDivision.find(divisionGuid);
                if (!division.isDivision())
                    throw new ServiceException(sm.getString("DekretacjaMusiOdbywacSieNaDzialNieStanowiskoLubGrupe"));
                guid = division.getGuid();
            }
            catch (DivisionNotFoundException e)
            {
                throw new ServiceException(sm.getString("NieZnalezionoWskazanegoDzialu"));
            }
            catch (EdmException e)
            {
                throw new ServiceException(e.getMessage(), e);
            }
        }
    }

    private static final Map<String, String> spoolTypeConstraints = new LinkedHashMap<String, String>();

    static
    {
        spoolTypeConstraints.put(SPOOL_EXPORT, sm.getString("KatalogZplikamiCSV"));
        spoolTypeConstraints.put(SPOOL_INTERNAL, sm.getString("KatalogZplikamiXML"));
    }

    class SpoolTypeProperty extends Property
    {

        public SpoolTypeProperty()
        {
            super(ENUMERATED, PERSISTENT, AbstractFaxDriver.this, "spoolType", sm.getString("RodzajKolejki"), String.class);
        }

        protected Object getValueSpi()
        {
            return spoolType;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            spoolType = (String) object;
        }

        public Map getValueConstraints()
        {
            return spoolTypeConstraints;
        }
    }
    
    class OnlyArchiveProperty extends Property
    {
        public OnlyArchiveProperty()
        {
            super(SIMPLE, PERSISTENT, AbstractFaxDriver.this, "onlyArchive", sm.getString("TylkoDoArchiwum"), Boolean.class);
        }

        protected Object getValueSpi()
        {
            return onlyArchive;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            onlyArchive = (Boolean) object;
        }
    }

    class RetainFaxesProperty extends Property
    {
        public RetainFaxesProperty()
        {
            super(SIMPLE, PERSISTENT, AbstractFaxDriver.this, "retainFaxes", sm.getString("PrzekazywanieDoPoczekalni"), Boolean.class);
        }

        protected Object getValueSpi()
        {
            return retainFaxes;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            retainFaxes = (Boolean) object;
        }
    }

    class SplitTiffsProperty extends Property
    {
        public SplitTiffsProperty()
        {
            super(SIMPLE, PERSISTENT, AbstractFaxDriver.this, "splitTiffs", sm.getString("PodzialPlikowTIFF"), Boolean.class);
        }

        protected Object getValueSpi()
        {
            return splitTiffs;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            splitTiffs = (Boolean) object;
        }
    }

    /**
     * W�asno�� okre�laj�ca identyfikator obiektu InOfficeDocumentDelivery,
     * jaki nale�y przypisa� tworzonemu dokumentowi dla faksu przychodz�cego
     * ze wskazanego portu COM.
     */
    abstract class ComDeliveryProperty extends Property
    {
        private int com;

        protected ComDeliveryProperty(int com)
        {
            super(SIMPLE, PERSISTENT, AbstractFaxDriver.this, "com"+com+"delivery", sm.getString("SposobPrzyjeciaDlaPortuCOM")+com, Integer.class, IN_DOCUMENT_DELIVERY);
            this.com = com;
        }

        protected Object getValueSpi()
        {
            return getComDelivery(com);
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null)
            {
                setComDelivery(com, null);
                return;
            }

            Integer deliveryId = (Integer) object;

            try
            {
                InOfficeDocumentDelivery.find(deliveryId);
                setComDelivery(com, deliveryId);
            }
            catch (EntityNotFoundException e)
            {
                throw new ServiceException(sm.getString("NieZnalezionoWskazanegoSposobuPrzyjecia"));
            }
            catch (EdmException e)
            {
                throw new ServiceException(sm.getString("NieZnalezionoWskazanegoSposobuPrzyjecia")+" ("+e.getMessage()+")", e);
            }
        }
    }

    class Com1DeliveryProperty extends ComDeliveryProperty
    {
        public Com1DeliveryProperty()
        {
            super(1);
        }
    }

    class Com2DeliveryProperty extends ComDeliveryProperty
    {
        public Com2DeliveryProperty()
        {
            super(2);
        }
    }
    class Com3DeliveryProperty extends ComDeliveryProperty
    {
        public Com3DeliveryProperty()
        {
            super(3);
        }
    }
    class Com4DeliveryProperty extends ComDeliveryProperty
    {
        public Com4DeliveryProperty()
        {
            super(4);
        }
    }
}
