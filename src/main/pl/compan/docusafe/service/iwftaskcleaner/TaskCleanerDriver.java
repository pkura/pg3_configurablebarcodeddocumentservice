package pl.compan.docusafe.service.iwftaskcleaner;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;

public class TaskCleanerDriver extends ServiceDriver implements Service {
	private int daysValid;
	private Timer timer;
	
	public TaskCleanerDriver() {
		try {
			daysValid = Integer.parseInt(Docusafe.getAdditionProperty("task_cleaner_interval"));
		} catch (Exception e) {
			daysValid = 0;
		}
	}
	
	protected void start() throws ServiceException
    {
		if (daysValid == 0) {
			return;
		}
		
        timer = new Timer(true);
        timer.schedule(new CleanTasks(), 2*1000, 1*DateUtils.HOUR);
    }
	
	protected void stop() throws ServiceException
    {
        if (timer != null) timer.cancel();
    }

    protected boolean canStop()
    {
        return false;
    }
    
    private boolean nightHour() {
    	Calendar cal = Calendar.getInstance();
        return (cal.get(Calendar.HOUR_OF_DAY) > 20 || cal.get(Calendar.HOUR_OF_DAY) < 7); 
    }
    
	private class CleanTasks extends TimerTask {
		 public void run() {
			Calendar cal = Calendar.getInstance();
            if(cal.get(Calendar.HOUR_OF_DAY) < 20 && cal.get(Calendar.HOUR_OF_DAY) > 7) return; 
			closeOpened();
			deleteClosed();
		 }
		 
		 private void closeOpened() {
             PreparedStatement pst = null;
             java.util.Date now = new java.util.Date();
             Date date = new Date(now.getTime() - daysValid*24*3600*1000);
             WorkflowFactory wff = WorkflowFactory.getInstance();
             try
             {
                DSApi.openAdmin();
                DSApi.context().session().flush();
                java.sql.Connection con = DSApi.context().session().connection();        
                pst = con.prepareStatement("select count(*) from dsw_activity where state='open.running' and laststatetime < ?");
                pst.setDate(1, date);
                ResultSet rs = pst.executeQuery();
                rs.next();
                LogFactory.getLog("workflow_log").debug("task cleaner service : znaleziono " + rs.getInt(1) + " zadan do zamkniecia");
                rs.close();
                pst.close();
                
                pst = con.prepareStatement("select activity_key from dsw_activity where state='open.running' and laststatetime < ?");
                pst.setDate(1, date);
                rs = pst.executeQuery();
                int i = 0;
                while (rs.next())
                {
                	//if (i % 5000 == 0 && !nightHour()) {
                	//	break;
                	//}
                	
                	DSApi.context().begin();
                	String key = rs.getString("activity_key");
                	wff.deprFinish(wff.keyToId(key));
                	LogFactory.getLog("workflow_log").debug("task cleaner service : zakonczono zadanie o kluczu: " + key);
                	DSApi.context().commit();
                }
                rs.close();
                pst.close(); 
             } catch (Exception e) {
            	 LogFactory.getLog("workflow_log").error(e.getMessage(),e);
             } finally {
                 if (DSApi.context().isTransactionOpen()==true)
                 {
                 	try { DSApi.context().rollback(); } catch (Exception e1) { }
                 }
             }
		 }
		 
		 private void deleteClosed() {
			 
		 }
		
	}
}
