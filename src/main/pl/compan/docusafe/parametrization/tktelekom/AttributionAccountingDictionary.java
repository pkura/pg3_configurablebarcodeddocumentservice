package pl.compan.docusafe.parametrization.tktelekom;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: jtarasiuk
 * Date: 13.08.13
 * Time: 22:14
 * To change this template use File | Settings | File Templates.
 */
public class AttributionAccountingDictionary extends pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase
{
    @Override
    public void filterAfterGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dicitonaryEnrtyId, Map<String, Object> fieldsValues, Map<String, Object> documentValues) {
        super.filterAfterGetValues(dictionaryFilteredFieldsValues, dicitonaryEnrtyId, fieldsValues, documentValues);
        if (dicitonaryEnrtyId != null && !dicitonaryEnrtyId.equals(""))
        {
            Object podstawa = dictionaryFilteredFieldsValues.get("DEKRETACJA_KSIEG_GROUPID");
            if (podstawa == null || !podstawa.equals(1))
            {
                try {
                    HashMap<String, String> mapa = new HashMap<String, String>();
                    mapa.put("", "--wybierz--");
                    mapa.put("0", "NIE DOTYCZY");
                    ArrayList<String> sel = new ArrayList<String>();
                    sel.add("0");
                    pl.compan.docusafe.core.dockinds.dwr.EnumValues ev = new pl.compan.docusafe.core.dockinds.dwr.EnumValues(mapa, sel);
                    dictionaryFilteredFieldsValues.put("DEKRETACJA_KSIEG_ANALYTICS_ID", ev);
                } catch (Exception e) {

                }
            }
        }
       /* boolean podstawa = values.get("DEKRETACJA_KSIEG_GROUPID").getBooleanData();
        if (podstawa) {

        } else {
            HashMap<String, String> mapa = new HashMap<String, String>();
            mapa.put("", "--wybierz--");
            mapa.put("0", "NIE DOTYCZY");
            ArrayList<String> sel = new ArrayList<String>();
            sel.add("0");
            pl.compan.docusafe.core.dockinds.dwr.EnumValues ev = new pl.compan.docusafe.core.dockinds.dwr.EnumValues(mapa, sel);
            result.put("DEKRETACJA_KSIEG_ANALYTICS_ID", ev);
        }  */
    }


}
