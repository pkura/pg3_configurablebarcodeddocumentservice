package pl.compan.docusafe.parametrization.tktelekom;

import org.hibernate.HibernateException;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.api.ActivityInfo;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4ActivityInfo;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static pl.compan.docusafe.core.dockinds.dwr.DwrUtils.setRefValueEnumOptions;

public class FakturaZakupuLogic extends AbstractDocumentLogic {

    public static final String KWOTA_NETTO_CN = "KWOTA_NETTO";
    public static final String KWOTA_BRUTTO_CN = "KWOTA_BRUTTO";
    public static final String WYBRANY_DZIAL_CN = "WYBRANY_DZIAL";
    public static final String WYBRANY_DZIAL_MANAGER_CN = "WYBRANY_DZIAL_MANAGER";
    public static final String MPK_CN = "MPK";
    public static final String MPK_DISABLED_CN = "MPK_DISABLED";
    public static final String PODSTAWA_ZAKUPU_CN = "PODSTAWA_ZAKUPU";
    public static final String NUMER_IWT_CN = "NR_IWT";
    public static final String NR_MAGAZYNU_CN = "NR_MAGAZYNU";
    public static final String STAWKI_VAT_CN = "STAWKI_VAT";
    public static final String DWR_STAWKI_VAT_CN = "DWR_STAWKI_VAT";
    public static final String DEKRETACJA_KSIEG_CN = "DEKRETACJA_KSIEG";
    public static final String DWR_DEKRETACJA_KSIEG_CN = "DWR_DEKRETACJA_KSIEG";
    public static final String OPIS_KSIEGOWY_CN = "OPIS_KSIEGOWY";
    public static final String DWR_KWOTA_VAT = "DWR_KWOTA_VAT";
    public static final String DWR_KWOTA_BRUTTO = "DWR_KWOTA_BRUTTO";
    public static final String DWR_KWOTA_NETTO = "DWR_KWOTA_NETTO";
    public static final String DWR_NALICZENIE_PODATKU = "DWR_NALICZENIE_PODATKU";
    public static final String DWR_MPK = "DWR_MPK";
    public static final String DWR_MPK_DISABLED = "DWR_MPK_DISABLED";
    public static final String STATUS = "STATUS";
    public static final String DWR_KONTRAHENT_NR_KONTA_SET = "DWR_KONTRAHENT_NR_KONTA_SET";
    public static final String SENDER = "SENDER";
    public static final String STATUS_KSIEGOWOSC_SENDER = "ksiegowosc_sender";
    public static final String STATUS_BOOK_KEEPERS = "book-keepers";
    public static final String STATUS_OPIS_MERYTORYCZNY = "opis-merytoryczny";
    public static final String ROZL_NAKLADU_CN = "ROZL_NAKLADU";
    public static final String DSG_MPK_KONTO = "dsg_mpk_konto";
    public static final String PREFIX_KONTO = "908";
    public static final String BRAK_KONT_908_XXXXX = "Brak Kont '908XXXXX'";
    public static final String WARNING_SUMA_DEKRETACJI = "Suma kwot z kolumny 'warto��' w Rozliczeniu nak�adu nie mo�e by� wy�sza ni� kwota wskazana w dekretacji ksi�gowej poniesionego nak�adu w pozycji linii zawieraj�cej 'KONTO' wype�nione warto�ci� '908XXXXX'";
    public static final String SUMA_KWOT_WN_MUSI_BY�_R�WNA_SUMIE_KWOT_MA = "Suma kwot WN musi by� r�wna sumie kwot MA";
    public static final String DSG_ACCOUNTS_VIEW = "dsg_accounts_view";
    public static final String TABLE_DSG_KOD_VAT = "dsg_kod_vat";
    public static final String TABLE_DSG_MPK_KOD_PODATKU = "dsg_mpk_kod_podatku";
    public static final String KONTRAHENT_NR_KONTA_STRING_CN = "KONTRAHENT_NR_KONTA_STRING";
    public static final String AKCEPTACJA_MERYTORYCZNA_CN = "akceptacja-merytoryczna";
    public static final String PROSZE_WYBRAC_POZYCJE_DO_ZAAKCEPTOWANIA = "Prosz� wybra� pozycj� do zaakceptowania";
    public static final String SUMA_MPK_CN = "SUMA_MPK";

    public static final String OS_MERYTORYCZNE_ACCEPTATION_CN = "akceptacja-merytoryczna";
    public static final String WYBRANY_DZIAL_ACCEPTATION_CN = "wybrany-dzial";
    public static final String WYBRANY_DZIAL_MANAGER_ACCEPTATION_CN = "wybrany-dzial-manager";

    public static final String DYNAMIC_FORK_VARIABLE_NAME = "dictionaryId";
    public static final String FAKTURA_ZAKUPU_TABLENAME = "dsg_faktura_zakupu";
    public static final String FAKTURA_ZAKUPU_MULTIPLE_TABLENAME = "dsg_faktura_zakupu_multiple";
    public static final String RODZAJ_ZAKUPU_TABLENAME = "dsg_rodzaj_zakupu";
    public static final String WYBRANY_DZIAL_TABLENAME = "dsg_wybrany_dzial";
    public static final String WYBRANY_DZIAL_MANAGER_TABLENAME = "dsg_wybrany_dzial_kierownik";
    public static final String RODZAJ_ZAKUPU_CN = "RODZAJ_ZAKUPU";
    public static final String RODZAJ_ZAKUPU_INWESTYCYJNA = "INW";
    public static final String RODZAJ_ZAKUPU_MAGAZYNOWA = "MAG";
    public static final String RODZAJ_ZAKUPU_USLUGOWA = "USL";
    private static final String DWR_SUMA_MPK_CN = "DWR_SUMA_MPK";
    private static final long serialVersionUID = 1L;
    public static final String RODZAJ_ZAKUPU_INVEST = "RODZAJ_ZAKUPU_INVEST";
    public static final String RODZAJ_ZAKUPU_MAG = "RODZAJ_ZAKUPU_MAG";
    public static final String DWR_OS_MERYTORYCZNE = "DWR_OS_MERYTORYCZNE";
    public static final String DWR_KOMORKA_OSOBY = "DWR_KOMORKA_OSOBY";
    public static final String DSG_KOMORKA_OSOBY = "dsg_komorka_osoby";

    private Logger log = LoggerFactory.getLogger(FakturaZakupuLogic.class);

    @Override
    public void documentPermissions(Document document) throws EdmException {
        Set<PermissionBean> perms = new HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, documentKindName + " zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName + " - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, documentKindName + " zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName + " - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Faktura zakupowa");

        folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));

        document.setFolder(folder);
    }

    private void checkCase(Document document) throws EdmException {

        if (((OfficeDocument) document).getContainingCase() == null)
            throw new EdmException("Faktura nie powi�zana ze spraw�");
    }

    public boolean canChangeDockind(Document document) throws EdmException {
        return false;
    }

    private void setFlag(FieldsManager fm, String kindCn, String flagFieldCn) {
        try {
            boolean whatSet = checkIs(fm, kindCn);
            setFieldInDB(fm, flagFieldCn, whatSet);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void setFieldInDB(FieldsManager fm, String flagFieldCn, boolean whatSet) {
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement("update dsg_faktura_zakupu set " + flagFieldCn + " = ? where document_id = ?");
            ps.setBoolean(1, whatSet);
            ps.setLong(2, fm.getDocumentId());
            ps.executeUpdate();
            ps.close();

            DSApi.context().closeStatement(ps);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }

    /**
     * Suma kwot Wn i Ma musi byc sobie r�wna
     *
     * @param fm
     * @throws EdmException
     */
    private void checkDecratetionWnMa(FieldsManager fm) throws EdmException {
        // ile jest kierownikow lfr
        List<Long> decreations = (List<Long>) fm.getKey("KIEROWNICY_LFR");

        // proste zapytanie, wyci?gni�cie wbranych kolumn z warunkiem where
        NativeCriteria nc = DSApi.context().createNativeCriteria("DS_DOCUMENT_ACCEPTANCE", "d");
        nc.setProjection(NativeExps.projection()
                // ustalenie kolumn
                .addProjection("d.document_id")
                .addProjection("d.acceptanceCn"))
                // warunek where
                .add(NativeExps.eq("d.document_id", fm.getDocumentId()))
                .add(NativeExps.eq("d.acceptanceCn", "book-keepers"));

        // pobranie i wy?wietlenie wynik�w
        CriteriaResult cr = nc.criteriaResult();

        int i = 0;
        while (cr.next()) {
            i++;
        }

        // czy jest to ostania dekretacja ksiegowa
        if (decreations.size() <= i) {
            // WN
            BigDecimal sumWn = BigDecimal.ZERO;
            // MA
            BigDecimal sumMa = BigDecimal.ZERO;

            List<Long> decretationIds = (List<Long>) fm.getKey(DEKRETACJA_KSIEG_CN);
            for (Long decId : decretationIds) {
                CentrumKosztowDlaFaktury dekret = CentrumKosztowDlaFaktury.getInstance().find(decId);
                sumWn = sumWn.add(dekret.getRealAmount());
                sumMa = sumMa.add(dekret.getAmountUsed());
            }

            if (fm.getKey("STAWKI_VAT") != null)
            {
	            decretationIds = (List<Long>) fm.getKey("STAWKI_VAT");
	            for (Long decId : decretationIds) {
	                CentrumKosztowDlaFaktury dekret = CentrumKosztowDlaFaktury.getInstance().find(decId);
	                sumWn = sumWn.add(dekret.getRealAmount());
	                sumMa = sumMa.add(dekret.getAmountUsed());
	            }
            }
            if (!(sumWn.compareTo(sumMa) == 0))
                throw new EdmException(SUMA_KWOT_WN_MUSI_BY�_R�WNA_SUMIE_KWOT_MA);
        }
    }

    /**
     * Sprawdz czy jednym z wybranych rodzai zakupu s� zakupy inwestycyjne
     *
     * @param fm
     * @return
     * @throws EdmException
     */
    private boolean checkIs(FieldsManager fm, String fieldCn) throws EdmException {
        List<Integer> rodzajIDS = (List<Integer>) fm.getKey(RODZAJ_ZAKUPU_CN);
        for (Integer id : rodzajIDS) {
            String cn = DataBaseEnumField.getEnumItemForTable(RODZAJ_ZAKUPU_TABLENAME, id).getCn();
            if (cn != null && cn.equalsIgnoreCase(fieldCn)) {
                return true;
            }
        }
        return false;
    }

    /**
     * W celu sprawdzenia rozliczenia faktury w ca�o�ci winno by� pole
     * sprawdzaj�ce warto�� netto w zakresie dekretacji poniesionego nak�adu z
     * warto�ci� nak�adu rozliczonego, czyli suma kwot z kolumny ?warto��? nie
     * mo�e by� wy�sza ni� kwota wskazana w dekretacji ksi�gowej poniesionego
     * nak�adu w pozycji linii zawieraj�cej ?KONTO? wype�nione warto�ci� ?
     * 908XXXXX?, .
     *
     * @param fm
     */
    private void checkSettlementOfInvoices(FieldsManager fm) throws EdmException {
        BigDecimal sumaDekretacji = sumaDekretacji(fm);

        BigDecimal sumaRozliczenie = sumaRozliczenia(fm);

		/*
         * if (sumaDekretacji.equals(BigDecimal.ZERO)) throw new
		 * EdmException(BRAK_KONT_908_XXXXX);
		 */
        if (sumaRozliczenie.compareTo(sumaDekretacji) == 1)
            throw new EdmException(WARNING_SUMA_DEKRETACJI);
    }

    /**
     * Sumuje wartosci ze slownika rozliczenia nakladu
     *
     * @param fm
     * @return sumaRozliczenie
     * @throws EdmException
     */
    private BigDecimal sumaRozliczenia(FieldsManager fm) throws EdmException {
        BigDecimal sumaRozliczenie = BigDecimal.ZERO;
        List<Long> rozliczeniaIds = (List<Long>) fm.getKey(ROZL_NAKLADU_CN);
        for (Long rozId : rozliczeniaIds) {
            CentrumKosztowDlaFaktury rozNakladu = CentrumKosztowDlaFaktury.getInstance().find(rozId);
            sumaRozliczenie = sumaRozliczenie.add(rozNakladu.getRealAmount());
        }
        return sumaRozliczenie;
    }

    /**
     * Sumuje wartosci NA i WIN ze slownika Dekertacji ksiegowej
     *
     * @param fm
     * @return sumaDekretacji
     * @throws EdmException
     */
    private BigDecimal sumaDekretacji(FieldsManager fm) throws EdmException {
        BigDecimal sumaDekretacji = BigDecimal.ZERO;
        List<Long> dekretacjeIDs = (List<Long>) fm.getKey(DEKRETACJA_KSIEG_CN);
        for (Long dekretacjaId : dekretacjeIDs) {
            CentrumKosztowDlaFaktury dekretacjaKsiegowa = CentrumKosztowDlaFaktury.getInstance().find(dekretacjaId);
            // if (isAccountStartSWith908(dekretacjaKsiegowa)) {
            sumaDekretacji = sumaDekretacji.add(dekretacjaKsiegowa.getRealAmount());
            sumaDekretacji = sumaDekretacji.add(dekretacjaKsiegowa.getAmountUsed());
            // }
        }
        return sumaDekretacji;
    }

    /**
     * Sprawdza czy kontoa zaczyna sie na 908
     *
     * @param dekretacjaKsiegowa
     * @return
     * @throws EdmException
     */
    private boolean isAccountStartSWith908(CentrumKosztowDlaFaktury dekretacjaKsiegowa) throws EdmException {
        Integer kontoId = dekretacjaKsiegowa.getConnectedTypeId();
        EnumItem enumItem = DataBaseEnumField.getEnumItemForTable(DSG_MPK_KONTO, kontoId);
        if (enumItem.getCn().startsWith(PREFIX_KONTO))
            return true;
        return false;
    }

    @Override
    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
        if (doc != null) {
            FieldsManager fm = doc.getFieldsManager();
            EnumItem statusItem = fm.getEnumItem(STATUS);

            // je�eli "ksiegowosc_sender".equalsIgnoreCase(statusItem.getCn())
            insertContractorAccountToDataBaseEnum(doc, fm, statusItem);

            // ustawienie falgi rodzaj zakupy typu inwestycjnego
            if (fm != null) {
                setFlag(fm, RODZAJ_ZAKUPU_INWESTYCYJNA, RODZAJ_ZAKUPU_INVEST);
                setFlag(fm, RODZAJ_ZAKUPU_MAGAZYNOWA, RODZAJ_ZAKUPU_MAG);
                if (fm.getKey("PODSTAWA_ZAKUPU") != null)
                    setFieldInDB(fm, "HIDE_BOOL", true);
                else
                    setFieldInDB(fm, "HIDE_BOOL", false);
            }


            // sprawdzenie
            // if (statusItem != null && STATUS_BOOK_KEEPERS.equalsIgnoreCase(statusItem.getCn()) && checkIs(fm, RODZAJ_ZAKUPU_INWESTYCYJNA))
            // checkSettlementOfInvoices(fm);

            if (STATUS_BOOK_KEEPERS.equalsIgnoreCase(acceptationCn))
                checkDecratetionWnMa(fm);

            if (STATUS_OPIS_MERYTORYCZNY.equalsIgnoreCase(acceptationCn)) {
                checkNettoWithMpksSum(fm);
                checkCase(doc);
            }
        }
    }






    private void checkNettoWithMpksSum(FieldsManager fm) throws EdmException {
        StringBuilder msgBuilder = new StringBuilder();
        if (fm.getKey(KWOTA_NETTO_CN) != null && fm.getKey(KWOTA_BRUTTO_CN) != null && fm.getKey(SUMA_MPK_CN) != null) {

            BigDecimal netto = (BigDecimal) fm.getKey(KWOTA_NETTO_CN);
            BigDecimal brutto = (BigDecimal) fm.getKey(KWOTA_BRUTTO_CN);
            BigDecimal suma = (BigDecimal) fm.getKey(SUMA_MPK_CN);

            if (netto.abs().compareTo(suma.abs()) > 0 || brutto.abs().compareTo(suma.abs()) < 0) {
                if (msgBuilder.length() == 0)
                    msgBuilder.append("Suma kwot pozycji bud�etowych musi by� r�wna lub wi�ksza od kwoty netto faktury i nie wi�ksza ni� kwota brutto faktury.");
                else
                    msgBuilder.append(";  Suma kwot pozycji bud�etowych musi by� r�wna lub wi�ksza od kwoty netto i nie wi�ksza ni� kwota brutto faktury.");
            }
        }
        if (msgBuilder.length() > 0)
            throw new EdmException(msgBuilder.toString());
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        boolean assigned = false;
        try {
            Long dicId = (Long) openExecution.getVariable(DYNAMIC_FORK_VARIABLE_NAME);
            FieldsManager fm = doc.getFieldsManager();
            // akceptacja merytoryczna
            if (OS_MERYTORYCZNE_ACCEPTATION_CN.equals(acceptationCn)) {
                EnumItem userToAccept = DataBaseEnumField.getEnumItemForTable("dsg_komorka_osoby", dicId.intValue());
                if (userToAccept == null || userToAccept.getCn() == null)
                    throw  new EdmException("Brak os. akceptujacej pozycje");
            	String userName = DSUser.findByUsername(userToAccept.getCn()).getName();
                assignable.addCandidateUser(userName);
                assigned = true;
                AssigneeHandler.addToHistory(openExecution, doc, userName, null);
            } else if (WYBRANY_DZIAL_ACCEPTATION_CN.equals(acceptationCn)) {
                Integer id = (Integer) fm.getKey(WYBRANY_DZIAL_CN);
                if (id == 13) {
                    String code = DataBaseEnumField.getEnumItemForTable(WYBRANY_DZIAL_TABLENAME, id).getCn();
                    String guid = DSDivision.findByCode(code).getGuid();
                    assignable.addCandidateGroup(guid);
                    assigned = true;
                    AssigneeHandler.addToHistory(openExecution, doc, null, guid);
                } else {
                    String userName = DataBaseEnumField.getEnumItemForTable(WYBRANY_DZIAL_TABLENAME, id).getRefValue();
                    assignable.addCandidateUser(userName);
                    assigned = true;
                    AssigneeHandler.addToHistory(openExecution, doc, userName, null);
                }
            } else if (WYBRANY_DZIAL_MANAGER_ACCEPTATION_CN.equals(acceptationCn)) {
                Integer id = (Integer) fm.getKey(WYBRANY_DZIAL_CN);
                String userName = DataBaseEnumField.getEnumItemForTable(WYBRANY_DZIAL_MANAGER_TABLENAME, id).getRefValue();
                assignable.addCandidateUser(userName);
                assigned = true;
                AssigneeHandler.addToHistory(openExecution, doc, userName, null);
            } else if ("kier_lfr".equals(acceptationCn) || "kier-ksieg-lfr".equals(acceptationCn)) {
                PreparedStatement ps;
                ResultSet rs;
                ps = DSApi.context().prepareStatement("SELECT KIEROWNIK_LFR FROM dsg_kierownicy_lfr WHERE id = ?");

                ps.setLong(1, dicId);
                rs = ps.executeQuery();
                rs.next();
                Long entryId = rs.getLong(1);

                String lfrCn = DataBaseEnumField.getEnumItemForTable("dsg_kierownik_lfr", entryId.intValue()).getCn();
                List<String> users = new ArrayList<String>();
                List<String> divisions = new ArrayList<String>();
                List<AcceptanceCondition> acceptances = AcceptanceCondition.find(lfrCn, null);
                if (acceptances.size() == 0) {
                    throw new EdmException("Nie uda�o si� przej�� do kolejnego etapu procesu. Podana kom�rka (" + lfrCn + ") nie posiada kierownika");
                }
                for (AcceptanceCondition accept : acceptances) {
                    if (accept.getUsername() != null && !users.contains(accept.getUsername()))
                        users.add(accept.getUsername());
                    if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
                        divisions.add(accept.getDivisionGuid());
                }
                for (String user : users) {
                    assignable.addCandidateUser(user);
                    AssigneeHandler.addToHistory(openExecution, doc, user, null);
                    assigned = true;
                }
                for (String division : divisions) {
                    assignable.addCandidateGroup(division);
                    AssigneeHandler.addToHistory(openExecution, doc, null, division);
                    assigned = true;
                }
            } else if ("weryfikacja-dekretacji".equals(acceptationCn)) {
                /*PreparedStatement ps;
                ResultSet rs;
                ps = DSApi.context().prepareStatement("SELECT KIEROWNIK_LFR FROM dsg_kierownicy_lfr WHERE id = ?");

                ps.setLong(1, dicId);
                rs = ps.executeQuery();
                rs.next();
                Long entryId = rs.getLong(1);*/
                PreparedStatement ps;
                ResultSet rs;
                ps = DSApi.context().prepareStatement("SELECT KIEROWNIK_LFR FROM dsg_kierownicy_lfr WHERE id = ?");

                ps.setLong(1, dicId);
                rs = ps.executeQuery();
                rs.next();
                Long entryId = rs.getLong(1);
                String lfrCn = DataBaseEnumField.getEnumItemForTable("dsg_kierownik_lfr", entryId.intValue()).getCn();
                List<String> users = new ArrayList<String>();
                List<String> divisions = new ArrayList<String>();
                // akceptacja kierownik + zastepca lfrCn + "K_Z"
                List<AcceptanceCondition> acceptances = AcceptanceCondition.find(lfrCn + "_K_Z", null);
                if (acceptances.size() == 0) {
                    throw new EdmException("Nie uda�o si� przej�� do kolejnego etapu procesu. Podana kom�rka (" + lfrCn + ") nie posiada kierownika");
                }
                for (AcceptanceCondition accept : acceptances) {
                    if (accept.getUsername() != null && !users.contains(accept.getUsername()))
                        users.add(accept.getUsername());
                    if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
                        divisions.add(accept.getDivisionGuid());
                }
                for (String user : users) {
                    assignable.addCandidateUser(user);
                    AssigneeHandler.addToHistory(openExecution, doc, user, null);
                    assigned = true;
                }
                for (String division : divisions) {
                    assignable.addCandidateGroup(division);
                    AssigneeHandler.addToHistory(openExecution, doc, null, division);
                    assigned = true;
                }
            } else if ("book-keepers".equals(acceptationCn)) {
                PreparedStatement ps;
                ResultSet rs;
                ps = DSApi.context().prepareStatement("SELECT pracownik_LFR FROM dsg_kierownicy_lfr WHERE id = ?");

                ps.setLong(1, dicId);
                rs = ps.executeQuery();
                rs.next();
                Long entryId = rs.getLong(1);

                String flrUserName = DataBaseEnumField.getEnumItemForTable("dsg_lfr_users_view", entryId.intValue()).getCn();

                assignable.addCandidateUser(flrUserName);
                AssigneeHandler.addToHistory(openExecution, doc, flrUserName, null);

                assigned = true;
            }

            return assigned;
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return assigned;
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            return assigned;
        }
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (dictionaryName.equals(STAWKI_VAT_CN)) {
         if (values.get("STAWKI_VAT_ID") != null && values.get("STAWKI_VAT_ID").getData() != null)
         {
            try
            {
                Integer mpkId = values.get("STAWKI_VAT_ID").getIntegerData();
                CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId.longValue());

                // connected_type_id
                EnumValues connectedType = values.get("STAWKI_VAT_CONNECTED_TYPE_ID").getEnumValuesData();
                String connectedTypeId = connectedType.getSelectedOptions().get(0);
                if (connectedTypeId != null && !connectedTypeId.equals(""))
                    mpk.setConnectedTypeId(Integer.valueOf(connectedTypeId));
                else
                    mpk.setConnectedTypeId(null);

                // centrumid enum
                EnumValues centrumIdEnum = values.get("STAWKI_VAT_CENTRUMID").getEnumValuesData();
                String centrumId = centrumIdEnum.getSelectedOptions().get(0);
                if (centrumId != null && !centrumId.equals(""))
                    mpk.setCentrumId(Integer.valueOf(centrumId));
                else
                    mpk.setCentrumId(null);

                // accountnumber enum
                EnumValues accountNumber = values.get("STAWKI_VAT_ACCOUNTNUMBER").getEnumValuesData();
                String accountNumberId = accountNumber.getSelectedOptions().get(0);
                if (accountNumberId != null && !accountNumberId.equals(""))
                    mpk.setAccountNumber(accountNumberId);
                else
                    mpk.setAccountNumber(null);

                // acceptingcentrumid enum
                EnumValues acceptingCentrum = values.get("STAWKI_VAT_ACCEPTINGCENTRUMID").getEnumValuesData();
                String acceptingCentrumId = acceptingCentrum.getSelectedOptions().get(0);
                if (acceptingCentrumId != null && !acceptingCentrumId.equals(""))
                    mpk.setAcceptingCentrumId(Integer.valueOf(acceptingCentrumId));
                else
                    mpk.setAcceptingCentrumId(null);

                // lokalizacjia enum
                EnumValues location = values.get("STAWKI_VAT_LOKALIZACJA").getEnumValuesData();
                String locationId = location.getSelectedOptions().get(0);
                if (locationId != null && !locationId.equals(""))
                    mpk.setLocationId(Integer.valueOf(locationId));
                else
                    mpk.setLocationId(null);

                // itemno enum
                EnumValues itemno = values.get("STAWKI_VAT_ITEMNO").getEnumValuesData();
                String itemnoId = itemno.getSelectedOptions().get(0);
                if (itemnoId != null && !itemnoId.equals(""))
                    mpk.setItemNo(Integer.valueOf(itemnoId));
                else
                    mpk.setItemNo(null);

                //class_type_id
                EnumValues classType = values.get("STAWKI_VAT_CLASS_TYPE_ID").getEnumValuesData();
                String classTypeId = classType.getSelectedOptions().get(0);
                if (classTypeId != null && !classTypeId.equals(""))
                    mpk.setClassTypeId(Integer.valueOf(classTypeId));
                else
                    mpk.setClassTypeId(null);

                // subgroupid
                EnumValues subgroup = values.get("STAWKI_VAT_SUBGROUPID").getEnumValuesData();
                String subgroupId = subgroup.getSelectedOptions().get(0);
                if (subgroupId != null && !subgroupId.equals(""))
                    mpk.setSubgroupId(Integer.valueOf(subgroupId));
                else
                    mpk.setSubgroupId(null);

                // customagencyid
                Integer customAgenct = values.get("STAWKI_VAT_CUSTOMSAGENCYID").getIntegerData();
                    mpk.setCustomsAgencyId(customAgenct);

                // amount money
                if (values.get("STAWKI_VAT_AMOUNT").getData() != null)
                    mpk.setAmount(values.get("STAWKI_VAT_AMOUNT").getMoneyData());
                // amountused money
                if (values.get("STAWKI_VAT_AMOUNTUSED").getData() != null)
                    mpk.setAmountUsed(values.get("STAWKI_VAT_AMOUNTUSED").getMoneyData());

                // analytics_id
                EnumValues analytics_id = values.get("STAWKI_VAT_ANALYTICS_ID").getEnumValuesData();
                String analytics_idId = analytics_id.getSelectedOptions().get(0);
                if (analytics_idId != null && !analytics_idId.equals(""))
                    mpk.setAnalyticsId(Integer.valueOf(analytics_idId));
                else
                    mpk.setAnalyticsId(null);


            }
            catch (Exception e)
            {
                log.warn(e.getMessage(), e);
            }
         }
        } else if (dictionaryName.equals(MPK_CN)) {
            result.put(MPK_CN + "_CUSTOMSAGENCYID", 0);
            if (values.get(MPK_CN + "_AMOUNT").getMoneyData() == null)
                result.put(MPK_CN + "_AMOUNT", BigDecimal.ZERO.setScale(2));

            if (DwrUtils.isNotNull(values, "MPK_ANALYTICS_ID")) {
                try {
                    Long osobaMerytorycznaID = DSApi.context().getDSUser().getId();
                    if (osobaMerytorycznaID != null) {
                        Long osobaAkceptujacaID = Long.valueOf(values.get("MPK_ANALYTICS_ID").getEnumValuesData().getSelectedOptions().get(0));
                        if (osobaMerytorycznaID.equals(osobaAkceptujacaID)) {
                            List<String> selected = new LinkedList<String>();
                            selected.add("");
                            DataBaseEnumField d = DataBaseEnumField.getEnumFiledForTable(DSG_KOMORKA_OSOBY);
                            EnumValues val = d.getEnumItemsForDwr(fieldsValues);
                            val.setSelectedOptions(selected);
                            result.put("MPK_ANALYTICS_ID", val);

                        }
                    }
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }

        } else if (dictionaryName.equals("DEKRETACJA_KSIEG")) {
            if (values.get("DEKRETACJA_KSIEG_CENTRUMID").getEnumValuesData().getSelectedId().equals(""))
            {
                set0000(values, fieldsValues, result, "dsg_mpk_konto", "DEKRETACJA_KSIEG_CONNECTED_TYPE_ID");
                set0000(values, fieldsValues, result, "dsg_mpk_podmiot", "DEKRETACJA_KSIEG_CENTRUMID");
                set0000(values, fieldsValues, result, "dsg_mpk_cpk", "DEKRETACJA_KSIEG_ACCOUNTNUMBER");
                set0000(values, fieldsValues, result, "dsg_mpk_proces", "DEKRETACJA_KSIEG_ACCEPTINGCENTRUMID");
                set0000(values, fieldsValues, result, "dsg_mpk_obiekt", "DEKRETACJA_KSIEG_LOKALIZACJA");
                set0000(values, fieldsValues, result, "dsg_mpk_usluga", "DEKRETACJA_KSIEG_ITEMNO");
                set0000(values, fieldsValues, result, "dsg_mpk_projekt", "DEKRETACJA_KSIEG_CLASS_TYPE_ID");
                set0000(values, fieldsValues, result, "dsg_mpk_rpk", "DEKRETACJA_KSIEG_SUBGROUPID");
            }
            boolean podstawa = values.get("DEKRETACJA_KSIEG_GROUPID").getBooleanData();
            if (podstawa) {
                try {
                    DataBaseEnumField en = DataBaseEnumField.getEnumFiledForTable("dsg_mpk_kod_podatku");
                    String selectedId = values.get(DEKRETACJA_KSIEG_CN+"_ANALYTICS_ID").getEnumValuesData().getSelectedId();
                    EnumValues x = en.getEnumValuesForForcedPush(values, "_1");
                    ArrayList<String> sel = new ArrayList<String>();
                    if (selectedId.equals("0"))
                        selectedId = "";
                    sel.add(selectedId);
                    x.setSelectedOptions(sel);
                    result.put("DEKRETACJA_KSIEG_ANALYTICS_ID", x);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            } else {
                HashMap<String, String> mapa = new HashMap<String, String>();
                mapa.put("", "--wybierz--");
                mapa.put("0", "NIE DOTYCZY");
                ArrayList<String> sel = new ArrayList<String>();
                sel.add("0");
                EnumValues ev = new EnumValues(mapa, sel);
                result.put("DEKRETACJA_KSIEG_ANALYTICS_ID", ev);
            }
            result.put("DEKRETACJA_KSIEG_CUSTOMSAGENCYID", 0);
            if (values.get("DEKRETACJA_KSIEG_AMOUNT").getMoneyData() == null)
                result.put("DEKRETACJA_KSIEG_AMOUNT", BigDecimal.ZERO.setScale(2));
            if (values.get("DEKRETACJA_KSIEG_AMOUNTUSED").getMoneyData() == null)
                result.put("DEKRETACJA_KSIEG_AMOUNTUSED", BigDecimal.ZERO.setScale(2));
        } else if (dictionaryName.equals("KIEROWNICY_LFR")) {
            setRefValueEnumOptions(values, result, "KIEROWNIK_LFR", "KIEROWNICY_LFR_", "PRACOWNIK_LFR", "dsg_lfr_users_view");
        }
        return result;
    }

    private void set0000(Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> result, String tableName, String fieldCn) {
        ArrayList<String> sel1 = new ArrayList<String>();
        if (tableName.equalsIgnoreCase("dsg_mpk_podmiot")) {
            sel1.add("308");
        } 
        if (tableName.equalsIgnoreCase("dsg_mpk_usluga"))
        {
            sel1.add("32");
        }
        if (tableName.equalsIgnoreCase("dsg_mpk_konto"))
        {
            sel1.add("2267");
        }
        else
            sel1.add("2");
        
        try {
            if (values.get(fieldCn).getEnumValuesData().getSelectedId().equals("")) {
                for (DataBaseEnumField d : DataBaseEnumField.tableToField.get(tableName)) {
                    EnumValues val = d.getEnumItemsForDwr(fieldsValues);
                    val.setSelectedOptions(sel1);
                    result.put(fieldCn, val);
                    break;
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        Field msgField = null;
        StringBuilder msgBuilder = new StringBuilder();

        if (DwrUtils.isNotNull(values, DWR_KOMORKA_OSOBY, DWR_MPK)) {
            /**
             *  Osoba merytoryczna nie mo�e by� jednocze�nie osob� akceptuj�c� merytorycznie
             */
            int osobaMerytorycznaID = Long.valueOf(values.get(DWR_KOMORKA_OSOBY).getEnumValuesData().getSelectedOptions().get(0)).intValue();
            Map<String, FieldData> dwrOsMerytoryczne = values.get(DWR_MPK).getDictionaryData();
            for (String key : dwrOsMerytoryczne.keySet()) {
                if (key.contains("ANALYTICS_ID") && dwrOsMerytoryczne.get(key) != null && dwrOsMerytoryczne.get(key).getData() != null) {
                    int osobaAkceptujacaID = Long.valueOf(dwrOsMerytoryczne.get(key).getEnumValuesData().getSelectedOptions().get(0)).intValue();
                    if (osobaMerytorycznaID == osobaAkceptujacaID) {
                        List<String> selected = new LinkedList<String>();
                        selected.add("");
                        values.get(DWR_MPK).getDictionaryData().get(key).getEnumValuesData().setSelectedOptions(selected);
                        return new Field("messageField", "Osoba merytoryczna nie mo�e by� jednocze�nie osob� akceptuj�c� merytorycznie", Field.Type.BOOLEAN);
                    }
                }
            }
        }


        if(DwrUtils.isNotNull(values, "DWR_DEKRETACJA_KSIEG"))
        {
            List<Object> czyPodatekList = DwrUtils.getValueListFromDictionary(values, DEKRETACJA_KSIEG_CN, true, "GROUPID");
        	if(czyPodatekList.contains(true))
        	{
        		values.get("DWR_HIDE_BOOL4").setBooleanData(Boolean.TRUE);
        	}
        	else
        	{
        		values.get("DWR_HIDE_BOOL4").setBooleanData(Boolean.FALSE);
        	}

        }
        if (DwrUtils.isNotNull(values, DWR_KWOTA_NETTO, DWR_KWOTA_BRUTTO) && (values.get(DWR_KWOTA_NETTO).isSender() || values.get(DWR_KWOTA_BRUTTO).isSender())) {
            BigDecimal netto = values.get(DWR_KWOTA_NETTO).getMoneyData().setScale(2, RoundingMode.HALF_UP);
            BigDecimal brutto = values.get(DWR_KWOTA_BRUTTO).getMoneyData().setScale(2, RoundingMode.HALF_UP);
            BigDecimal vat = brutto.subtract(netto);
            values.get(DWR_KWOTA_VAT).setMoneyData(vat);
        }
        if (fm.getEnumItemCn(STATUS).equals(STATUS_KSIEGOWOSC_SENDER) && DwrUtils.extractIdFromDictonaryField(values, SENDER).size() > 0) {
            DataBaseEnumField enumItem = DataBaseEnumField.getEnumFiledForTable(DSG_ACCOUNTS_VIEW);
            if (enumItem.getRefEnumItemsMap().containsKey(DwrUtils.extractIdFromDictonaryField(values, SENDER).get(0).toString())) {
                values.get(DWR_KONTRAHENT_NR_KONTA_SET).setBooleanData(Boolean.TRUE);
            } else
                values.get(DWR_KONTRAHENT_NR_KONTA_SET).setBooleanData(Boolean.FALSE);
        } else
            values.get(DWR_KONTRAHENT_NR_KONTA_SET).setBooleanData(Boolean.FALSE);
        if (fm.getEnumItemCn(STATUS).equalsIgnoreCase(STATUS_OPIS_MERYTORYCZNY)) {
            List<String> rodzajeIds = values.get(DwrUtils.getCnWithPrefix(RODZAJ_ZAKUPU_CN)).getEnumValuesData().getSelectedOptions();
            //2 magagazyna, 3 inwestycyjna
            if (rodzajeIds.contains("3"))
                values.get(DwrUtils.getCnWithPrefix(RODZAJ_ZAKUPU_INVEST)).setBooleanData(Boolean.TRUE);
            else
                values.get(DwrUtils.getCnWithPrefix(RODZAJ_ZAKUPU_INVEST)).setBooleanData(Boolean.FALSE);
            if (rodzajeIds.contains("2"))
                values.get(DwrUtils.getCnWithPrefix(RODZAJ_ZAKUPU_MAG)).setBooleanData(Boolean.TRUE);
            else
                values.get(DwrUtils.getCnWithPrefix(RODZAJ_ZAKUPU_MAG)).setBooleanData(Boolean.FALSE);
        }
        if (values.get("DWR_FAKT_ROZL") != null && values.get("DWR_FAKT_ROZL").getEnumValuesData().getSelectedId().equals("1"))
        {
            FieldData rozliczenie = values.get("DWR_ROZL_NAKLADU");
            Map<String, FieldData> rozliczenieMap = rozliczenie.getDictionaryData();
            BigDecimal sumaRozl = BigDecimal.ZERO;
            List<Object> rozliczenieNakladuAmount = DwrUtils.getValueListFromDictionary(values, "ROZL_NAKLADU", true, "AMOUNT");
            for (Object amount : rozliczenieNakladuAmount)
            {
                sumaRozl = sumaRozl.add((BigDecimal)amount);
            }

            FieldData rozliczenieDeklaracja = values.get("DWR_DEKRETACJA_KSIEG");
            Map<String, FieldData> rozliczenieDekl = rozliczenieDeklaracja.getDictionaryData();
//            BigDecimal sumaDeklWinien = BigDecimal.ZERO;
            BigDecimal sumaDeklMa = BigDecimal.ZERO;
            for (String ss : rozliczenieDekl.keySet()) {
                String index = ss.substring(ss.lastIndexOf("_") + 1);
                String klucz = "DEKRETACJA_KSIEG_CONNECTED_TYPE_ID_" + index;
                FieldData fieldData = rozliczenieDekl.get(klucz);
                if(fieldData.getEnumValuesData().getSelectedId()!=null && fieldData.getEnumValuesData().getSelectedId().length()>2)
                {
                	Integer idik = Integer.parseInt(fieldData.getEnumValuesData().getSelectedId());
                	EnumItem eItem = DataBaseEnumField.getEnumItemForTable("dsg_mpk_konto", idik);
                	if (eItem.getCn().startsWith("908")) {
                		if (ss.contains("AMOUNTUSED_")) {
                			if (rozliczenieDekl.get(ss).getMoneyData() != null) {
                				sumaDeklMa = sumaDeklMa.add(rozliczenieDekl.get(ss).getMoneyData());
                			}
                		}
                	}
                }
            }

            
            if (sumaDeklMa.doubleValue() != sumaRozl.doubleValue() || sumaRozl.doubleValue()==0.0 || sumaDeklMa.doubleValue()==0.0)
            {
            	List<String> sel1 = new ArrayList<String>();
                sel1.add("");
                values.get("DWR_FAKT_ROZL").getEnumValuesData().setSelectedOptions(sel1);
                msgBuilder.append("Suma kwot Rozliczenia nak�adu musi by� r�wna sumie kwot dekretacji ksi�gowej kont 908xxx. Warto�� pola \"faktura rozliczona\" zosta�a zmieniona na warto�� \"NIE\"");
            }
        }

        /**
         * Ukrywanie pola LOG_ZAM, je�li uzupe�niona jest PODSTAWA_ZAKUPU
         */
        if ((Integer) fm.getKey(STATUS) == 100 || (Integer) fm.getKey("STATUS") == 120) {

            boolean hasPodstawa = false;
            if (values.get("DWR_" + PODSTAWA_ZAKUPU_CN) != null) {
                String podstawa = values.get("DWR_" + PODSTAWA_ZAKUPU_CN).getStringData();
                if (podstawa != null && !podstawa.equals("")) {
                    hasPodstawa = true;
                }
                if (hasPodstawa) {
                    values.get("DWR_HIDE_BOOL").setBooleanData(Boolean.TRUE);
                } else {
                    values.get("DWR_HIDE_BOOL").setBooleanData(Boolean.FALSE);
                }
            } else {
                values.get("DWR_HIDE_BOOL").setBooleanData(Boolean.FALSE);
            }
        }

        /**
         * Sumowanie kwot pozycji bud�etowych i wstawienie tej sumy do pola SUM_MPK
         */
        if (DwrUtils.isNotNull(values, DWR_MPK, DWR_MPK_DISABLED)) {
            sumaMPK(values);
        }
        /**
         * Naliczenie podatku VAT na podstawie dekretacji ksi�gowych
         */
        if (values.get(DWR_NALICZENIE_PODATKU) != null && values.get(DWR_NALICZENIE_PODATKU).isSender()) {

            naliczPodatekVAT(values, fm, msgBuilder);
        }


        if (DwrUtils.isNotNull(values, "DWR_NR_FAKTURY") && values.get("DWR_NR_FAKTURY").isSender())
        {
            boolean isOpened = true;
            List<Long> ids = new LinkedList<Long>();
            try
            {
                if (!DSApi.isContextOpen())
                {
                    isOpened = false;
                    DSApi.openAdmin();
                }

                // proste zapytanie, wyci�gni�cie wbranych kolumn z warunkiem where
                NativeCriteria nc = DSApi.context().createNativeCriteria("dsg_faktura_zakupu", "d");
                nc.setProjection(NativeExps.projection()
                        // ustalenie kolumn
                        .addProjection("d.DOCUMENT_ID").addProjection("d.nr_faktury"))
                        // warunek where
                        .add(NativeExps.eq("d.nr_faktury", values.get("DWR_NR_FAKTURY").getData()));

                // pobranie i wy�wietlenie wynik�w
                CriteriaResult cr = nc.criteriaResult();

                while (cr.next())
                {
                    ids.add(cr.getLong(0, null));
                }
            }catch (EdmException e)
            {
                log.error(e.getMessage(), e);
            }
            finally
            {
                if (DSApi.isContextOpen() && !isOpened)
                {
                    DSApi._close();
                }
            }
            if (ids.size() > 0)
            {
                StringBuilder msgString = new StringBuilder("Faktura o numerze '").append(values.get("DWR_NR_FAKTURY").getData()).append("' ju� istnieje w systemie. ID dokument�w z takim numerem: ");
                for (Long id : ids)
                {
                    msgString.append(id);
                    if (ids.indexOf(id) < ids.size()-1)
                        msgString.append(", ");
                }
                if (msgBuilder.length() > 0)
                {
                    msgBuilder.append(". ").append(msgString);
                }
                else
                    msgBuilder.append(msgString);
            }
        }
        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Type.BOOLEAN);
        }

        return null;
    }

    /**
     * Sumuje kwoty ze slownika "Pozycje bud�etowe" i wstawia do pola "SUMA_MPK"
     *
     * @param values
     * @param fm
     * @throws EdmException
     */

    private void sumaMPK(Map<String, FieldData> values) throws EdmException
    {
        BigDecimal sumaMPK = BigDecimal.ZERO;
        sumaMPK = sumaMPK.add(DwrUtils.sumDictionaryMoneyFields(values, "MPK", "AMOUNT"));
        sumaMPK = sumaMPK.add(DwrUtils.sumDictionaryMoneyFields(values, "MPK_DISABLED", "AMOUNT"));
        values.get(DWR_SUMA_MPK_CN).setMoneyData(sumaMPK.setScale(2, RoundingMode.HALF_DOWN));
    }

    private void naliczPodatekVAT(Map<String, FieldData> values, FieldsManager fm, StringBuilder msgBuilder) throws EdmException {
        Map<String, BigDecimal> vat = new HashMap<String, BigDecimal>();

        FieldData dkFields = values.get(DWR_DEKRETACJA_KSIEG_CN);
        Map<String, FieldData> dkValues = dkFields.getDictionaryData();
        Integer kontoID = null;
        for (String key : dkValues.keySet()) {
            if (key != null && key.contains("ANALYTICS")) {
                String suffix = key.substring(key.lastIndexOf("_"));
                String podstawaCN = DEKRETACJA_KSIEG_CN + "_GROUPID".concat(suffix);
                String amountCN = DEKRETACJA_KSIEG_CN + "_AMOUNT".concat(suffix);
                String kontoCN = DEKRETACJA_KSIEG_CN + "_CONNECTED_TYPE_ID".concat(suffix);
                if (dkValues.get(key) == null || dkValues.get(key).getEnumValuesData() == null || dkValues.get(key).getEnumValuesData().getSelectedId() == null || dkValues.get(key).getEnumValuesData().getSelectedId().equals("")) {
                    if (msgBuilder.length() == 0)
                        msgBuilder.append("Nie wybrano kodu podatku we wszystkich pozycjach dekretacji.");
                    else
                        msgBuilder.append(";  Nie wybrano kodu podatku we wszystkich pozycjach dekretacji.");
                } else if (dkValues.get(podstawaCN) == null || dkValues.get(podstawaCN).getData() == null || !dkValues.get(podstawaCN).getBooleanData()) {
                    continue;
                } else {
                    EnumValues enumValuesData = dkValues.get(key).getEnumValuesData();
                    String selectedId = enumValuesData.getSelectedId();
                    EnumItem enumItemForTable = DataBaseEnumField.getEnumItemForTable(TABLE_DSG_MPK_KOD_PODATKU, Integer.parseInt(selectedId));
                    String refValue = enumItemForTable.getRefValue();
                    Integer stawka = Integer.valueOf(refValue);
                    BigDecimal kwota = dkValues.get(amountCN).getMoneyData().setScale(2, RoundingMode.HALF_DOWN);

                    kontoID = DataBaseEnumField.getEnumItemForTable(DSG_MPK_KONTO, Integer.parseInt(dkValues.get(kontoCN).getEnumValuesData().getSelectedId())).getId();
                    String keyForMap = enumItemForTable.getCn() + "_" + kontoID;

                    switch (stawka) {
                        case 3:
                            kwota = kwota.multiply(new BigDecimal(0.03)).setScale(2, RoundingMode.HALF_DOWN);
                            break;
                        case 5:
                            kwota = kwota.multiply(new BigDecimal(0.05)).setScale(2, RoundingMode.HALF_DOWN);
                            break;
                        case 7:
                            kwota = kwota.multiply(new BigDecimal(0.07)).setScale(2, RoundingMode.HALF_DOWN);
                            break;
                        case 8:
                            kwota = kwota.multiply(new BigDecimal(0.08)).setScale(2, RoundingMode.HALF_DOWN);
                            break;
                        case 22:
                            kwota = kwota.multiply(new BigDecimal(0.22)).setScale(2, RoundingMode.HALF_DOWN);
                            break;
                        case 23:
                            kwota = kwota.multiply(new BigDecimal(0.23)).setScale(2, RoundingMode.HALF_DOWN);
                            break;
                    }
                    if (vat.containsKey(keyForMap)) {
                        vat.put(keyForMap, vat.get(keyForMap).add(kwota));
                    } else {
                        vat.put(keyForMap, kwota);
                    }
                }
            }
        }

        int counter = 1;
        FieldData vatFields = values.get(DWR_STAWKI_VAT_CN);
        Map<String, FieldData> vatValues = new HashMap<String, FieldData>();
        for (String key : vat.keySet()) {
            boolean isOpened = true;
            try {
                if (!DSApi.isContextOpen()) {
                    isOpened = false;
                    DSApi.openAdmin();
                }
                CentrumKosztowDlaFaktury to = new CentrumKosztowDlaFaktury();

                to.setConnectedTypeId(DataBaseEnumField.getEnumFiledForTable(DSG_MPK_KONTO).getEnumItem(key.substring(key.indexOf("_") + 1)).getId());
                to.setCentrumId(308);
                to.setAccountNumber("2");
                to.setAcceptingCentrumId(2);
                to.setLocationId(2);
                to.setItemNo(32);
                to.setClassTypeId(2);
                to.setSubgroupId(2);
                to.setCustomsAgencyId(0);
                to.setRealAmount(vat.get(key).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                to.setAmountUsed(BigDecimal.ZERO);
                String cnPodatku = key.subSequence(0, key.indexOf("_")).toString();
                to.setAnalyticsId(DataBaseEnumField.getEnumFiledForTable(TABLE_DSG_MPK_KOD_PODATKU).getEnumItemByCn(cnPodatku).getId());

                DSApi.context().begin();
                DSApi.context().session().save(to);
                DSApi.context().commit();
                vatValues.put(STAWKI_VAT_CN + "_ID_" + counter, new FieldData(Type.LONG, to.getId()));
                counter++;
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                DSApi.context().rollback();
            } catch (HibernateException e) {
                log.error(e.getMessage(), e);
                DSApi.context().rollback();
            } finally {
                if (DSApi.isContextOpen() && !isOpened) {
                    DSApi._close();
                }
            }
        }
        vatFields.setDictionaryData(vatValues);
    }

    @Override
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {

    }

    private boolean assignDependingOnTypeOfPurchase(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String rodzajZakupu) throws EdmException, UserNotFoundException {
        boolean assigned;
        assignable.addCandidateGroup(rodzajZakupu);
        assigned = true;
        AssigneeHandler.addToHistory(openExecution, doc, null, rodzajZakupu);
        return assigned;
    }

    @Override
    public OcrSupport getOcrSupport() {
        return TKTelekomOcrSupport.get();

    }

    /**
     * Przepisuje nr konta kontrahenta podanego przeza ksiegowosc do
     * odpowiedniej tabeli dbEnum
     *
     * @param document
     * @param fm
     * @param statusItem
     * @throws EdmException
     */
    private void insertContractorAccountToDataBaseEnum(Document document, FieldsManager fm, EnumItem statusItem) throws EdmException {
        String insertIntoDsgContractorAccountSql = "insert into dsg_contractor_account (id_kontrahenta, nr_rachunku, nazwa_banku) values(?,?,?)";
        String updateDsgFakturaZakupu = "update dsg_faktura_zakupu set KONTRAHENT_NR_KONTA_STRING = null, KONTRAHENT_NR_KONTA_SET=? where document_id=?";

        Long dostawcaId = fm.getLongKey(SENDER);
        if (statusItem != null && STATUS_KSIEGOWOSC_SENDER.equalsIgnoreCase(statusItem.getCn()) && dostawcaId != null) {
            String nrKontaString = fm.getStringKey(KONTRAHENT_NR_KONTA_STRING_CN);
            Person dostawca = Person.find(dostawcaId.longValue());
            dostawca.setWparam(dostawcaId);
            if (nrKontaString != null && !"".equalsIgnoreCase(nrKontaString)) {
                PreparedStatement ps = null;
                ResultSet rs = null;
                try {
                    ps = DSApi.context().prepareStatement(insertIntoDsgContractorAccountSql);

                    ps.setLong(1, dostawcaId);
                    ps.setString(2, nrKontaString);
                    ps.setString(3, "xxx");
                    ps.execute();
                    ps.close();

                    DSApi.context().closeStatement(ps);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                } finally {
                    DSApi.context().closeStatement(ps);
                }
                ps = null;
                try {
                    ps = DSApi.context().prepareStatement(updateDsgFakturaZakupu);
                    ps.setBoolean(1, true);
                    ps.setLong(2, document.getId());
                    ps.executeUpdate();
                    ps.close();

                    DSApi.context().closeStatement(ps);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                } finally {
                    DSApi.context().closeStatement(ps);
                }
            }
        }
    }
    
    @Override
    public void beforeSetWithHistory(Document document, Map<String, Object> values, String activity) throws EdmException
    {
    	Long docId = document.getId();
    	PreparedStatement ps = null;
    	try
    	{
    		ActivityInfo activityInfo = new Jbpm4ActivityInfo(activity);
    		String taskName = (String) activityInfo.get("task_name");

    		if(document.getFieldsManager().getEnumItemCn("STATUS").equals("akceptacja-merytoryczna"))
    		{
    			Map mDict = (Map) values.get("M_DICT_VALUES");
    			
        		ps = DSApi.context().prepareStatement("select field_val from dsg_faktura_zakupu_multiple where field_cn='MPK' and document_id= ? order by field_val desc");
        		ps.setLong(1, docId);
        		ResultSet rs = ps.executeQuery();
        		ArrayList<Long> mpkIds = new ArrayList<Long>();
        		int index = 0;
        		HashMap<String, Object> newMultiDict = new HashMap<String, Object>();
        		while(rs.next())
        		{
        			index++;
        			Long id = rs.getLong(1);
        			String kluczMpk = "MPK_"+index;
        			
        			Map mpk = (Map) mDict.get(kluczMpk);
        			
        			
        			CentrumKosztowDlaFaktury centrum = Finder.find(CentrumKosztowDlaFaktury.class, id);
        			HashMap<String, Object> temp = new HashMap<String, Object>();
        			FieldData fdId = new FieldData(Type.INTEGER, centrum.getId());
        			DSApi.context().session().saveOrUpdate(centrum);
        			FieldData fdAnalytics = new FieldData(Type.ENUM, String.valueOf(centrum.getAnalyticsId()));
        			
        			temp.put("ID", fdId);
        			temp.put("ANALYTICS_ID", fdAnalytics);
        			mDict.put(kluczMpk, temp);
        		}
        		values.put("M_DICT_VALUES", mDict);
        		document.getFieldsManager().reloadValues(values);
    		}
    		if(taskName.equals("kier_lfr"))
    		{
    			Map mDict = (Map) values.get("M_DICT_VALUES");
    			
        		ps = DSApi.context().prepareStatement("select field_val from dsg_faktura_zakupu_multiple where field_cn='KIEROWNICY_LFR' and document_id= ? order by field_val desc");
        		ps.setLong(1, docId);
        		ResultSet rs = ps.executeQuery();
        		int index = 0;
        		HashMap<String, Object> newMultiDict = new HashMap<String, Object>();
        		while(rs.next())
        		{
        			index++;
        			Long id = rs.getLong(1);
        			String kluczMpk = "KIEROWNICY_LFR_"+index;
        			
        			FieldData fdPracownik = new FieldData(Type.INTEGER, null);
        			
        			Set<String> klucze = mDict.keySet();
        			for(String klucz: klucze)
        			{
        				Map<String, FieldData> value = (Map<String, FieldData>) mDict.get(klucz);
        				FieldData fdId = value.get("ID");
        				Long idLong = fdId.getLongData();
        				if(idLong.longValue()==id.longValue())
        				{
        					fdPracownik.setLongData(value.get("PRACOWNIK_LFR").getLongData());
        				}
        			}
        			
        			
        			HashMap<String, Object> temp = new HashMap<String, Object>();
        			FieldData fdId = new FieldData(Type.INTEGER, id);
        			Map testt = (Map) mDict.get(kluczMpk);
        			
        			temp.put("ID", fdId);
        			temp.put("PRACOWNIK_LFR", fdPracownik);
        			newMultiDict.put(kluczMpk, temp);
        		}
        		values.put("M_DICT_VALUES", newMultiDict);
        		document.getFieldsManager().reloadValues(values);
    		}
    		
    	}
    	catch(Exception e)
    	{
    		log.error(e.getMessage(), e);
    	}
    }

    public TaskListParams getTaskListParams(DocumentKind dockind, long id, TaskSnapshot task) throws EdmException {
        TaskListParams params = super.getTaskListParams(dockind, id, task);
        FieldsManager fm = dockind.getFieldsManager(id);

        // termin p�atno�ci z umowy - 1
        if (fm.getKey("TERMIN_PLAT_Z_UMOWY") != null) {
            String terminPlatZUmowy = DateUtils.formatCommonDate(((Date) fm.getKey("TERMIN_PLAT_Z_UMOWY")));
            params.setAttribute(terminPlatZUmowy, 1);
        }
        else
            params.setAttribute("Nie okre�lona", 1);

        // faktura rozliczona - 2
        if (fm.getKey("FAKT_ROZL") != null) {
            params.setAttribute(fm.getEnumItem("FAKT_ROZL").getTitle(), 2);
        }
        else params.setAttribute("NIE", 2);
        
        // rodzaj zakupu - 3
        if (fm.getKey("RODZAJ_ZAKUPU") != null) {
        	ArrayList<String> lista = (ArrayList<String>) fm.getValue("RODZAJ_ZAKUPU");
        	String content = "";
        	for(String rodzajZakupu: lista){
        		content += rodzajZakupu + ",";
        	}
        	
            params.setAttribute(content.substring(0, content.length()-1), 3);
        }
        return params;
    }

    public void printLabel(Document document, int type, Map<String, Object> dockindKeys) throws EdmException {
        TKTelekomUtils.printLabel(document, type, dockindKeys);
    }

    public void setAdditionalValues(FieldsManager fieldsManager, Integer valueOf, String activity) throws EdmException {
    	try
    	{
    		ActivityInfo activityInfo = new Jbpm4ActivityInfo(activity);
    		String taskName = (String) activityInfo.get("task_name");
    		
    		List<Long> mpkIds = new ArrayList<Long>();
    		Object mpk = fieldsManager.getFieldValues().get("MPK");
    		if (mpk != null)
    		{
    			if (mpk instanceof List<?>) 
    			{
    				mpkIds = (List<Long>) fieldsManager.getFieldValues().get("MPK");
    			} 
    			else if (mpk instanceof Long) 
    			{
    				mpkIds.add((Long)mpk);
    			}
    		}
    		String statusCn = fieldsManager.getEnumItem("STATUS").getCn();
    		if(statusCn.equals("akceptacja-merytoryczna"))
    		{
    			List<Long> mpkIdsToShow = new ArrayList<Long>();
				DSUser currUser = DSApi.context().getDSUser();
				for(Long mpkId: mpkIds)
				{
					CentrumKosztowDlaFaktury centrum = Finder.find(CentrumKosztowDlaFaktury.class, mpkId);
					if(centrum.getAnalyticsId().intValue()==currUser.getId().intValue())
					{
						mpkIdsToShow.add(centrum.getId());
					}
				}
				
				if (mpkIdsToShow.size() > 0) 
				{
					if (mpk instanceof List<?>) 
					{
						((List<Long>) mpk).clear();
						((List<Long>) mpk).addAll(mpkIdsToShow);
					} 
					else if (mpk instanceof Long) 
					{
						if (mpkIdsToShow.size() > 0) 
						{
							mpk = mpkIdsToShow.get(0);
						}
					}
				}
    		}
            if (taskName != null && taskName.equals(STATUS_BOOK_KEEPERS))
            {
                Map<String, Object> toReload = new HashMap<String, Object>();
                pl.compan.docusafe.core.dockinds.field.Field metodaPlatnosciField =  fieldsManager.getField("METODA_PLATNOSCI");
                if(metodaPlatnosciField.getDefaultValue() != null){
                    toReload.put(metodaPlatnosciField.getCn(), metodaPlatnosciField.simpleCoerce(metodaPlatnosciField.getDefaultValue()));
                }
                pl.compan.docusafe.core.dockinds.field.Field grupaPlatnosciField =  fieldsManager.getField("GRUPA_PLATNOSCI");
                if(grupaPlatnosciField.getDefaultValue() != null){
                    toReload.put(grupaPlatnosciField.getCn(), grupaPlatnosciField.simpleCoerce(grupaPlatnosciField.getDefaultValue()));
                }

                fieldsManager.reloadValues(toReload);
            }
    		if(taskName!=null && taskName.equals("kier_lfr"))
    		{
    			LinkedList<Map> kierownicy = (LinkedList<Map>) fieldsManager.getValue("KIEROWNICY_LFR");
    			DSUser currUser = DSApi.context().getDSUser();
    			PreparedStatement ps = null;
    			ps = DSApi.context().prepareStatement("select refValue from dsg_lfr_users_view where cn=?");
        		ps.setString(1, currUser.getName());
        		ResultSet rs = ps.executeQuery();
        		int refVal = -1;
        		while(rs.next())
        		{
        			 refVal = rs.getInt(1);
        		}
        		if(refVal>0)
        		{
        			List<Long> lfrIdsToShow = new ArrayList<Long>();
        			Object lfr = fieldsManager.getFieldValues().get("KIEROWNICY_LFR");
        			for(Map kierownik: kierownicy)
        			{
        				
        				EnumValues ev =  (EnumValues) kierownik.get("KIEROWNICY_LFR_KIEROWNIK_LFR");
        				String selected = ev.getSelectedValue();
        				String selectedId = ev.getSelectedId();
        				long kierownikId = Long.parseLong(selectedId);
        				BigDecimal kierId = (BigDecimal) kierownik.get("KIEROWNICY_LFR_ID");
        				
        				String klasaid = kierId.getClass().getName();
        				if(kierownikId==refVal) lfrIdsToShow.add(kierId.longValue());
        				
        			}
        			((List<Long>) lfr).clear();
					((List<Long>) lfr).addAll(lfrIdsToShow);
        		}
    		}
    		if((statusCn.equals("rejected") || statusCn.equals("accepted")))
    		{
    			Object obj = fieldsManager.getValue("HIDE_BOOL3");
    			String  klasa = obj.getClass().getName();
    			String xxx="xxx";
    			HashMap<String, Object> toReload = new HashMap<String, Object>();
    			if(isLfr()) toReload.put("HIDE_BOOL3", true);
    			else toReload.put("HIDE_BOOL3", false);
    			fieldsManager.reloadValues(toReload);
    			obj = fieldsManager.getValue("HIDE_BOOL3");
    			klasa = obj.getClass().getName();
    		}
    	}
    	catch(Exception e)
    	{
    		log.error(e.getMessage(), e);
    	}
       
    }

    private boolean isLfr()
    {
    	try
        {
            DSUser logged = DSApi.context().getDSUser();

            for(DSDivision divi : logged.getDivisions())
            {
                String cn = divi.getCode();
                if(cn.equals("LFR1") || cn.equals("LFR2") || cn.equals("LFR3") || cn.equals("LFR4") || cn.equals("LFR"))
                {
                    return true;
                }
            }
    	}
    	catch(Exception e)
    	{
    		log.error(e.getMessage(), e);
    	}
    	return false;
    }
    

}
