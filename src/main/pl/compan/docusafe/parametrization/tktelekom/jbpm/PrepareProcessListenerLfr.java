package pl.compan.docusafe.parametrization.tktelekom.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PrepareProcessListenerLfr implements ExternalActivityBehaviour {

	private static final long serialVersionUID = 1L;
	private String lfr;
	private Logger log = LoggerFactory.getLogger(PrepareProcessListener.class);

	// private String enumIdField;
	// public static final List<String> guids = new LinkedList<String>();

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		try{
            execution.removeVariable("correction");
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		final List<Long> argSets = new LinkedList<Long>();

		ArrayList<Long> lfrIds = new ArrayList<Long>();
		

		Map<String, Object> mapa = doc.getFieldsManager().getFieldValues();
		Set xxx = mapa.keySet();
		Statement st = DSApi.context().createStatement();
		int test = st.executeUpdate("INSERT INTO dsg_kierownicy_lfr(kierownik_lfr) values (" + lfr+ ")");
		if(test==1)
		{
			ResultSet resultId = st.getGeneratedKeys();
//			resultId.beforeFirst();
			while(resultId.next())
			{
				Long id = resultId.getLong(1);
				lfrIds.add(id);
			}
				
		}
		
		Map<String, Object> toReaload = new HashMap<String, Object>();
//		toReaload.put("KIEROWNICY_LFR", lfrIds.get(0));
		toReaload.put("KIEROWNICY_LFR", 28L);
		
		doc.getFieldsManager().reloadValues(toReaload);
		
		argSets.addAll(lfrIds);

		execution.setVariable("ids", argSets);
		execution.setVariable("count", argSets.size());
		execution.setVariable("count2", 2);
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
