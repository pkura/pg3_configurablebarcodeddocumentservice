package pl.compan.docusafe.parametrization.tktelekom.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PrepareProcessListenerMpk implements ExternalActivityBehaviour {

	private static final long serialVersionUID = 1L;
	private String dictionaryField;

	@Override
	public void execute(ActivityExecution execution) throws Exception {
        execution.removeVariable("correction");
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		List<Long> argSets = new LinkedList<Long>();

		LinkedList<Map> obje = (LinkedList<Map>) doc.getFieldsManager().getValue(dictionaryField.toUpperCase());
		for (Map mpk : obje) 
		{
			CentrumKosztowDlaFaktury centrum = Finder.find(CentrumKosztowDlaFaktury.class,((Integer) mpk.get("MPK_ID")).longValue());
            long osAkcpetujacaMerytorycznie = centrum.getAnalyticsId().longValue();
            if (!argSets.contains(osAkcpetujacaMerytorycznie))
                argSets.add(osAkcpetujacaMerytorycznie);
		}

		execution.setVariable("ids", argSets);
		execution.setVariable("count", argSets.size());
		execution.setVariable("count2", 2);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
