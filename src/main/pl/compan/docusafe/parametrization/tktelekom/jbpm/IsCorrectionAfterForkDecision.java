package pl.compan.docusafe.parametrization.tktelekom.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

public class IsCorrectionAfterForkDecision implements DecisionHandler {

    private static final long serialVersionUID = 1L;

    @Override
    public String decide(OpenExecution openExecution) {
        Integer cor = (Integer) openExecution.getVariable("correction");
        Boolean wasSingle = (Boolean) openExecution.getVariable("singlePath");
        openExecution.removeVariable("singlePath");

        if (cor != null && cor == 11) {
            openExecution.removeVariable("correction");
            return "correction";
        } else if (cor != null && cor == 12) {
            openExecution.removeVariable("correction");
            return "rejection";
        } else if (wasSingle != null && wasSingle) {
            return "startFork";
        } else{
            return "normal";
        }
    }

}
