package pl.compan.docusafe.parametrization.tktelekom.jbpm;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.tktelekom.FakturaZakupuLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AkceptacjaMerytoryczna implements DecisionHandler {

    private final static Logger LOG = LoggerFactory.getLogger(AkceptacjaMerytoryczna.class);
    private static final long serialVersionUID = 1L;

    /**
     * Przy wyborze rodzaju zakupowego innego niz magazynowa oraz inwestycyjna, 
     * pomijany jest krok LOTS2/Magazyn oraz Akceptacja kierownika LOTS2/magazynowa
     */
    @Override
    public String decide(OpenExecution openExecution) {
    	try {
    	Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
	    OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        List<Integer> rodzajIDS = (List<Integer>) fm.getKey(FakturaZakupuLogic.RODZAJ_ZAKUPU_CN);
        for (Integer id : rodzajIDS) {
            String cn = DataBaseEnumField.getEnumItemForTable(FakturaZakupuLogic.RODZAJ_ZAKUPU_TABLENAME, id).getCn();
            if (cn != null && (cn.equals(FakturaZakupuLogic.RODZAJ_ZAKUPU_INWESTYCYJNA) || cn.equals(FakturaZakupuLogic.RODZAJ_ZAKUPU_MAGAZYNOWA))) {
                return "tak";
            }
        }
        return "nie";
    	} catch (EdmException ex) {
    	    LOG.error(ex.getMessage(), ex);
    	    return "nie";
    	}
    }
}