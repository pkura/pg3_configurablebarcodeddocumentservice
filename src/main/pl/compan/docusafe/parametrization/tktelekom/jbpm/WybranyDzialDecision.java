package pl.compan.docusafe.parametrization.tktelekom.jbpm;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.tktelekom.FakturaZakupuLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WybranyDzialDecision implements DecisionHandler {

    private static final long serialVersionUID = 1L;

    private final static Logger LOG = LoggerFactory.getLogger(WybranyDzialDecision.class);

    private String field;

    @Override
    public String decide(OpenExecution openExecution) {
	try {
	    Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
	    OfficeDocument doc = OfficeDocument.find(docId);
	    FieldsManager fm = doc.getFieldsManager();
	    @SuppressWarnings("unchecked")
	    List<Integer> rodzajeIDs = (List<Integer>) fm.getKey(field);
	    for (Integer id : rodzajeIDs) {
		EnumItem ei = DataBaseEnumField.getEnumItemForTable(FakturaZakupuLogic.RODZAJ_ZAKUPU_TABLENAME, id);
		if (ei.getCn().equalsIgnoreCase("INW")) {
		    return "LOT3";
		}
	    }
	    return "magazyn";
	} catch (EdmException ex) {
	    LOG.error(ex.getMessage(), ex);
	    return "magazyn";
	}
    }
}
