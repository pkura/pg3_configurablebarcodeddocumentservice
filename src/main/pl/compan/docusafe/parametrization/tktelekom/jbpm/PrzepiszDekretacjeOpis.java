package pl.compan.docusafe.parametrization.tktelekom.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.tktelekom.FakturaZakupuLogic;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PrzepiszDekretacjeOpis implements ExternalActivityBehaviour {

    private static final long serialVersionUID = 1L;

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        final List<Long> dekretacjeIDs = new LinkedList<Long>();
        @SuppressWarnings("unchecked")
        List<Long> mpkIDs = (List<Long>) doc.getFieldsManager().getKey(FakturaZakupuLogic.MPK_CN);

        przepiszDekretacje(docId, dekretacjeIDs, mpkIDs);

        przepiszOpis(docId, doc, fm);
    }

    private void przepiszOpis(Long docId, OfficeDocument doc, FieldsManager fm) throws EdmException, SQLException {
        StringBuilder sb = new StringBuilder("");
        /*List<Integer> rodzajIDS = (List<Integer>) doc.getFieldsManager().getKey(FakturaZakupuLogic.RODZAJ_ZAKUPU_CN);
        sb.append("Rodzaj zakupu: ");
        for (int i = 0; i < rodzajIDS.size(); i++) {
            String title = DataBaseEnumField.getEnumItemForTable(FakturaZakupuLogic.RODZAJ_ZAKUPU_TABLENAME, rodzajIDS.get(i)).getTitle();
            if (title != null) {
                if (i == rodzajIDS.size() - 1) {
                    sb.append(title + ";\n");
                } else {
                    sb.append(title + ", ");
                }
            }
        }*/
        if (fm.getKey("RODZ_ZAK_INWEST") != null)
        {
          sb.append(fm.getEnumItem("RODZ_ZAK_INWEST").getTitle() + ";\n");
        }
        String numerIWT = fm.getStringValue(FakturaZakupuLogic.NUMER_IWT_CN);
        if (numerIWT != null) {
            sb.append(numerIWT + ";\n");
        }
        String numerMagazynu = fm.getStringValue(FakturaZakupuLogic.NR_MAGAZYNU_CN);
        if (numerMagazynu != null) {
            sb.append(numerMagazynu + ";\n");
        }
        PreparedStatement ps;
        ps = DSApi.context().prepareStatement("update " + FakturaZakupuLogic.FAKTURA_ZAKUPU_TABLENAME + " set OPIS_KSIEGOWY = ? where DOCUMENT_ID = ?");
        ps.setString(1, sb.toString());
        ps.setLong(2, docId);
        ps.executeUpdate();
        ps.close();
    }

    private void przepiszDekretacje(Long docId, List<Long> dekretacjeIDs, List<Long> mpkIDs) throws EdmException, SQLException {

        for (Long mpkID : mpkIDs) {
            CentrumKosztowDlaFaktury from = CentrumKosztowDlaFaktury.getInstance().find(mpkID);
            CentrumKosztowDlaFaktury to = new CentrumKosztowDlaFaktury();
            to.setCentrumId(from.getCentrumId());
            to.setAmount(from.getRealAmount());
            to.setAmountUsed(BigDecimal.ZERO);
            to.setAccountNumber(from.getAccountNumber());
            to.setAcceptingCentrumId(from.getAcceptingCentrumId());
            to.setLocationId(from.getLocationId());
            to.setItemNo(from.getItemNo());
            to.setClassTypeId(from.getClassTypeId());
            to.setSubgroupId(from.getSubgroupId());
            to.setCustomsAgencyId(from.getCustomsAgencyId());
            DSApi.context().session().save(to);
            dekretacjeIDs.add(to.getId());
        }

        PreparedStatement ps;
        if (dekretacjeIDs.size() > 0) {
            ps = DSApi.context().prepareStatement("delete from " + FakturaZakupuLogic.FAKTURA_ZAKUPU_MULTIPLE_TABLENAME + " where DOCUMENT_ID = ? AND FIELD_CN = ?");
            ps.setLong(1, docId);
            ps.setString(2, FakturaZakupuLogic.DEKRETACJA_KSIEG_CN);
            ps.executeUpdate();
            ps.close();
        }
        for (Long dekrID : dekretacjeIDs) {
            ps = DSApi.context().prepareStatement("insert into " + FakturaZakupuLogic.FAKTURA_ZAKUPU_MULTIPLE_TABLENAME + " (DOCUMENT_ID, FIELD_CN, FIELD_VAL) values (?, ?, ?)");
            ps.setLong(1, docId);
            ps.setString(2, FakturaZakupuLogic.DEKRETACJA_KSIEG_CN);
            ps.setLong(3, dekrID);
            ps.executeUpdate();
            ps.close();
        }
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
        // TODO Auto-generated method stub

    }

}
