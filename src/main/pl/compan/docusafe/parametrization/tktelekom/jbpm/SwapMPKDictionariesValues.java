package pl.compan.docusafe.parametrization.tktelekom.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.tktelekom.FakturaZakupuLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.Map;

public class SwapMPKDictionariesValues implements ExternalActivityBehaviour {

    private static final long serialVersionUID = 1L;
    private Logger log = LoggerFactory.getLogger(SwapMPKDictionariesValues.class);

    private String dictionaryFrom;
    private String dictionaryTo;

    @Override
    public void execute(ActivityExecution execution) throws Exception {

        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();

        @SuppressWarnings("unchecked")
        List<Long> mpkIDs = (List<Long>) fm.getKey(dictionaryFrom);
        if (mpkIDs != null && mpkIDs.size() > 0) {
            PreparedStatement ps = null;
            ps = DSApi.context().prepareStatement("UPDATE " + FakturaZakupuLogic.FAKTURA_ZAKUPU_MULTIPLE_TABLENAME + "  set FIELD_CN = ? WHERE DOCUMENT_ID = ? AND FIELD_CN = ?");
            ps.setString(1, dictionaryTo);
            ps.setLong(2, docId);
            ps.setString(3, dictionaryFrom);
            ps.executeUpdate();
        }

    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
        // TODO Auto-generated method stub

    }

}
