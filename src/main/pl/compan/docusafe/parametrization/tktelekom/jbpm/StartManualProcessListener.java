package pl.compan.docusafe.parametrization.tktelekom.jbpm;

import com.google.common.collect.Maps;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;

import java.util.List;
import java.util.Map;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 * Created with IntelliJ IDEA.
 * User: jtarasiuk
 * Date: 01.07.13
 * Time: 13:38
 * To change this template use File | Settings | File Templates.
 */
public class StartManualProcessListener implements ExternalActivityBehaviour
{
    @Override
    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception
    {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception
    {
        OfficeDocument doc = Jbpm4Utils.getDocument(activityExecution);

        List<String> addRecipients = (List<String>) doc.getFieldsManager().getKey("ADD_RECIPIENT_HERE");
        for (String id: addRecipients)
        {
        	Long idLong = Long.parseLong(id);
            Recipient rec = (Recipient) Recipient.find(idLong.longValue());
            String userDivision = rec.getLparam();
            String selectedUserName = null, selectedDivisionGUID=null;
            if (userDivision != null)
            {
                String[] ids = ((String) userDivision).split(";");
                if (ids[0].contains("u:"))
                    selectedUserName = ids[0].split(":")[1];
                if (ids[1].contains("d:"))
                    selectedDivisionGUID = ids[1].split(":")[1];

                Map<String, Object> map = Maps.newHashMap();
                map.put(ASSIGN_USER_PARAM, selectedUserName);
                map.put(ASSIGN_DIVISION_GUID_PARAM, selectedDivisionGUID);
                doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess("manual-with-autostatus").getLogic().startProcess(doc, map);
                JBPMTaskSnapshot.updateByDocument(doc);
            }
        }
        activityExecution.takeDefaultTransition();
    }
}
