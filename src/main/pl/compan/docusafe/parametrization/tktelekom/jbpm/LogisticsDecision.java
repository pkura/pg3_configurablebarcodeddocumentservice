package pl.compan.docusafe.parametrization.tktelekom.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class LogisticsDecision implements DecisionHandler {

    private static final long serialVersionUID = 1L;

    private final static Logger LOG = LoggerFactory.getLogger(LogisticsDecision.class);

    private String field;

    public String decide(OpenExecution openExecution) {
        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            LOG.info("documentid = " + docId + " field = " + field);
            String s = fm.getStringValue(field);
            if (s != null && !s.equals("")) {
                return "opis-merytoryczny";
            } else {
                return "logistics";
            }
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
    }
}