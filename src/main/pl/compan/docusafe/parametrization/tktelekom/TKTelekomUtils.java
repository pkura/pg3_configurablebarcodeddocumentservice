package pl.compan.docusafe.parametrization.tktelekom;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.service.zebra.ZebraPrinterManager;
import pl.compan.docusafe.util.BarcodeUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @todo - uzupelnic JAVADOCA
 * @author user
 *
 */
public class TKTelekomUtils 
{
	
	private final static Logger log = LoggerFactory.getLogger(TKTelekomUtils.class);
	
	 public static void printLabel(Document document, int type, Map<String, Object> dockindKeys) throws EdmException
	    {
	    	if(!AvailabilityManager.isAvailable("print.barcode.create", document.getDocumentKind().getCn()))
	    		return;
	    	String barcode = null;
	    	if(document instanceof InOfficeDocument)
	    	{
	    		barcode = ((OfficeDocument)document).getBarcode();
	    		 if(StringUtils.isEmpty(barcode))
	    		 {
	    			 ((OfficeDocument)document).setBarcode(BarcodeUtils.getKoBarcode((OfficeDocument)document));
	    			 barcode = ((OfficeDocument)document).getBarcode();
	    		 }
	    	}
	    	else
	    	{
	    		return;
	    	}
	    	Map<String,String> fields = new LinkedHashMap<String,String>();
	    	ZebraPrinterManager zm = new ZebraPrinterManager();
			zm.printLabel(barcode, fields);
            PreparedStatement ps = null;
            try
            {
                ps = DSApi.context().prepareStatement("update dsg_normal_dockind set BARCODE = ? where document_id = ?");
                ps.setString(1, barcode);
                ps.setLong(2, document.getId());
                ps.executeUpdate();
            } 
            catch (Exception e) 
            {
            	//Exception musi byc logowany
            	log.error("",e);
            }
            finally
            {
                DSApi.context().closeStatement(ps);
            }
	    }

}
