package pl.compan.docusafe.parametrization.tktelekom;

import com.google.common.collect.Sets;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.ocr.OcrUtils;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: jtarasiuk
 * Date: 10.06.13
 * Time: 13:33
 * To change this template use File | Settings | File Templates.
 */
public class TKTelekomOcrSupport
{
    public final static OcrSupport OCR_SUPPORT = OcrUtils.getFileOcrSupport(
            new File(Docusafe.getHome(), "ocr-put"),
            new File(Docusafe.getHome(), "ocr-get"),
            Sets.newHashSet("tiff", "tif", "gif", "jpg", "pdf")
    );

    public static OcrSupport get(){
        return OCR_SUPPORT;
    }
}
