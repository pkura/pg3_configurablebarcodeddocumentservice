package pl.compan.docusafe.parametrization.imgwwaw;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.ProfileNotAssignedException;
import pl.compan.docusafe.core.ValueNotFoundException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.AttachmentInfo;
import pl.compan.docusafe.core.exports.BudgetItem;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.FundSource;
import pl.compan.docusafe.core.exports.ReservationDocument;
import pl.compan.docusafe.core.jbpm4.AssignUtils;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.Source;
import pl.compan.docusafe.parametrization.imgwwaw.jbpm.PurchaseDocumentTypeDecision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class WniosekZakupowyLogic extends ImgwDocumentLogic
{
    private static final long serialVersionUID = 1L;
    public static final String ERP_STATUS = "ERP_STATUS";
    private static WniosekZakupowyLogic instance;
	protected static Logger log = LoggerFactory.getLogger(WniosekZakupowyLogic.class);
	private static final String WNIOSEK_ZAKUPOWY = "wniosek_zakupowy";
	
	//Pola na dokumencie
	public static final String INICJATOR_CN = "INICJATOR";
	public static final String NUMER_WNIOSKU_CN = "NUMER_WNIOSKU";
	public static final String SYMBOL_KOMORKI_CN = "SYMBOL_KOMORKI";
	public static final String RODZAJ_ZAMOWIENIA_CN = "RODZAJ_ZAMOWIENIA";
	public static final String RODZAJ_PRZEDMIOTU_CN = "RODZAJ_PRZEDMIOTU";
	public static final String OSOBA_ODPOWIEDZIALNA_CN = "OSOBA_ODPOWIEDZIALNA";
	public static final String PRZEDMIOT_ZAMOWIENIA_CN = "PRZEDMIOT_ZAMOWIENIA";
	public static final String ZRODLO_FINANSOWANIA_CN = "ZRODLO_FINANSOWANIA";
	public static final String REALIZATOR_CN = "RECIPIENT_HERE";
	public static final String STATUS_CN = "STATUS";
	
	public static final String PRZEDMIOT_ZAMOWIENIA = "PRZEDMIOT_ZAMOWIENIA";
	public static final String DWR_PRZEDMIOT_ZAMOWIENIA = "DWR_PRZEDMIOT_ZAMOWIENIA";
	public static final String PRZEDMIOT_ZAMOWIENIA_CENA_JEDN_NETTO = "PRZEDMIOT_ZAMOWIENIA_CENA_JEDN_NETTO";
	public static final String PRZEDMIOT_ZAMOWIENIA_ILOSC = "PRZEDMIOT_ZAMOWIENIA_ILOSC";
	public static final String PRZEDMIOT_ZAMOWIENIA_WARTOSC_NETTO = "PRZEDMIOT_ZAMOWIENIA_WARTOSC_NETTO";
	
	//Przedroski dla akceptacji
	public static final String KIEROWNIK_PREFF = "KIER_";
    public static final String DYREKTOR_PREFF = "DYR_";
    public static final String PROJEKT_PREFF = "PRO_";
    
    //task names
    public static final String CELL_MAN_ACCEPT = "cell_menager_accept";
    public static final String VERTICAL_DYR_ACCEPT = "vertical_director_accept";
    public static final String PROJ_MAN_ACCEPT = "project_manager_accept";
	
	//Kody akceptacji z jpdl acceptationCn
    public static final String CREATION = "creation_by_worker";
    public static final String PROCUREMENT = "procurement_accept";
    public static final String CHOOSEN_PERSON = "choosen_person";
	public static final String CELL_MANAGERS = "managers_accept";
	public static final String DIRECTOR  = "directors_accept";
	public static final String PROJECT = "project_manager";
	public static final String SUBJECT = "subject_accept";
	public static final String FIN_DIRECTOR = "finance_director";
	public static final String REALIZATOR = "REALIZATOR";
	public static final String AKCEPTACJA_OZP_ACCEPTANCE = "akceptacja_ozp";
	
	public static final String SUFFIX = "WZ";
	
	protected static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String, String>(), new ArrayList<String>());
	
	//Zmienna instancyjna foreach - ids przedmiotow zamowien
	public static final String FINANCE_SOURCE_ID = "financeSourceId";
    private static final String ERP_DOCUMENT_IDM = "ERP_DOCUMENT_IDM";
    
    public static final String OZP_K_ACCEPTANCE = Docusafe.getAdditionProperty("ozp_k_acceptance") == null ? "ozp_k_acceptance" : Docusafe.getAdditionProperty("ozp_k_acceptance");
    public static final String OZP_G_ACCEPTANCE = Docusafe.getAdditionProperty("ozp_g_acceptance") == null ? "ozp_g_acceptance" : Docusafe.getAdditionProperty("ozp_g_acceptance");
    public static final String OZP_W_ACCEPTANCE = Docusafe.getAdditionProperty("ozp_w_acceptance") == null ? "ozp_w_acceptance" : Docusafe.getAdditionProperty("ozp_w_acceptance");
    public static final String OZP_O_ACCEPTANCE = Docusafe.getAdditionProperty("ozp_o_acceptance") == null ? "ozp_o_acceptance" : Docusafe.getAdditionProperty("ozp_o_acceptance");
    public static final String OZP_T_ACCEPTANCE = Docusafe.getAdditionProperty("ozp_t_acceptance") == null ? "ozp_t_acceptance" : Docusafe.getAdditionProperty("ozp_t_acceptance");


    public static WniosekZakupowyLogic getInstance()
	{
		if (instance == null)
			instance = new WniosekZakupowyLogic();
		return instance;
	}

	@Override
	public void documentPermissions(Document document) throws EdmException{
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event){
		log.info("--- Wniosek Zakupowy : START PROCESS !!! ---- {}", event);
		try
		{   
			 Map<String, Object> map = Maps.newHashMap();
	         map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
	         map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
	         document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
	         
	         java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

	         DSUser author = DSApi.context().getDSUser();
	         String user = author.getName();
	         String fullName = author.asFirstnameLastname();

	         perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

	         Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
	         perms.addAll(documentPermissions);
	         this.setUpPermission(document, perms);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	@Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException{
		if(values.get("DWR_PRZEDMIOT_ZAMOWIENIA").getDictionaryData() != null){
			Map<String, FieldData> przedmiotZamowienia = values.get("DWR_PRZEDMIOT_ZAMOWIENIA").getDictionaryData();
			BigDecimal sumaWartosci = BigDecimal.ZERO;
			String suffix = "";
			for(String cn : przedmiotZamowienia.keySet()){
				suffix = cn.substring(cn.lastIndexOf("_"));
				if(cn.contains(PRZEDMIOT_ZAMOWIENIA_WARTOSC_NETTO + suffix) && przedmiotZamowienia.get(cn).getData() != null){
					sumaWartosci = sumaWartosci.add(przedmiotZamowienia.get(cn).getMoneyData());
				} 
			}
			values.get("DWR_WARTOSC_NETTO").setMoneyData(sumaWartosci);
		}
		return null;
	}
	
	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
    {
		Map<String, Object> results = new HashMap<String, Object>();
        try {
            if (dictionaryName.equals("ZRODLO_FINANSOWANIA")) {
                setBudgetItemsRefValues(dictionaryName, values, results);
                }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

		if(dictionaryName.equals(PRZEDMIOT_ZAMOWIENIA)){
			Map<String, Object> result = new HashMap<String, Object>();
			if((values.get(PRZEDMIOT_ZAMOWIENIA_CENA_JEDN_NETTO).getData() != null) && (values.get(PRZEDMIOT_ZAMOWIENIA_ILOSC).getData() != null)){
				BigDecimal cenaJednNetto = values.get(PRZEDMIOT_ZAMOWIENIA_CENA_JEDN_NETTO).getMoneyData();
				Integer ilosc = values.get(PRZEDMIOT_ZAMOWIENIA_ILOSC).getIntegerData();
				result.put(PRZEDMIOT_ZAMOWIENIA_WARTOSC_NETTO, cenaJednNetto.multiply(BigDecimal.valueOf(ilosc.longValue())));
				return result;
			}
		}
		return results;
    }
	
	private String setRequestNumber(String divisionCode) {
		boolean isOpened = false;
		Long lp = 0l;
		try{
			Date currentDate = new Date();	
			SimpleDateFormat sdf = new SimpleDateFormat("yy");
			String year = sdf.format(currentDate);
			
			StringBuilder sb = new StringBuilder();
			sb.append(divisionCode)
				.append("/");
			
			String sql = "select count(1) from dsg_imgw_wniosek_zakupowy";
			PreparedStatement stmt = DSApi.context().prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				lp = rs.getLong(1);
			}
			sb.append(lp)
				.append("/")
				.append(year)
				.append("/")
				.append(SUFFIX);
			
			return sb.toString();
		
		} catch ( EdmException e ) {
			log.error(e.getMessage(), e);
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		} finally {
			if (isOpened) {
				DSApi.closeContextIfNeeded(isOpened);
			}
		}
		
		return "";
	}
	
	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		// TODO Auto-generated method stub
		
	}
	
	
	 @Override
	 public void setInitialValues(FieldsManager fm, int type) throws EdmException {
	    	Map<String, Object> toReload = Maps.newHashMap();
	    	Long inicjatorId = DSApi.context().getDSUser().getId();
			if (WNIOSEK_ZAKUPOWY.equalsIgnoreCase(fm.getDocumentKind().getCn())) {
				DSDivision[] userDivs = DSUser.findById(inicjatorId).getDivisionsWithoutGroup();
				String symbolKomorki = "";
				if(userDivs.length > 0){
					symbolKomorki = userDivs[0].getCode().trim();
				}
				toReload.put(SYMBOL_KOMORKI_CN, symbolKomorki);
				toReload.put(INICJATOR_CN, inicjatorId);
				toReload.put(NUMER_WNIOSKU_CN, setRequestNumber(userDivs[0].getCode()));
			}
			fm.reloadValues(toReload);
	    }
	
	
    @Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) throws EdmException {
		boolean assigned = false;
		Long docId = doc.getId();
		FieldsManager fm = new FieldsManager(doc.getId(), doc.getDocumentKind());
		/**
	     * Akceptacje dla Wniosku zakupowego
	     */
        try {
        	if (KIEROWNIK_PROJEKTU_ACCEPTANCE.equals(acceptationCn)) {
                Long dictionaryId = (Long) openExecution.getVariable(DICTIONARY_ID_VARIABLE);
                String typFaktury = fm.getEnumItemCn(FakturaZakupowaLogic.TYP_FAKTURY_CN);
                Boolean zwrot = fm.getBoolean(FakturaZakupowaLogic.ZWROT_KOSZTOW_CN);
                String divGuid = null;
                if (PurchaseDocumentTypeDecision.FZS.equals(typFaktury) || (zwrot != null && zwrot)) {
                    divGuid = DSDivision.findById(dictionaryId).getGuid();
                } else {
                    DSUser user = DSUser.findById(dictionaryId);
                    if (user != null) {
                        assignable.addCandidateUser(user.getName());
                        assigned = true;
                        AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
                        return assigned;
                    }
                }
                if (divGuid == null) {
                    throw new DivisionNotFoundException("Nie znaleziono poprawnej kom�rki do przypisania");
                }
                List<String> userNames = Lists.newArrayList();
                Integer koordynatorKomorkiProfileId = 3;
                if (Docusafe.getAdditionProperty("profile.id.kierownik.projektu") != null) {
                    try {
                        koordynatorKomorkiProfileId = Integer.valueOf(Docusafe.getAdditionProperty("profile.id.kierownik.projektu"));
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
                assigned = assignToUsersFromProfile(doc, assignable, openExecution, divGuid, userNames, koordynatorKomorkiProfileId, false, false, 1);
            }
    		//Akceptacja w zaleznosci od rodzaju przedmiotu
        	else if(SUBJECT.equals(acceptationCn)){
            	String refValue = fm.getEnumItem(RODZAJ_PRZEDMIOTU_CN).getRefValue();
                DSUser user = DSUser.findById(Long.valueOf(refValue));
                assignable.addCandidateUser(user.getName());
                assigned = true;
                AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
                return assigned;
    		//Przekazanie do osoby odpowiedzialnej za utworzenie wniosku zakupowego
    		} else if(REALIZATOR.equals(acceptationCn)){
    			Long id = (Long) fm.getKey(REALIZATOR_CN);
    			Person sender = Person.find(id.longValue());
    			if(id != null && sender != null){
    				assigned = assignUser(doc, assignable, openExecution,  DSUser.findByFirstnameLastname(sender.getFirstname(), sender.getLastname()).getId());
    			}
    		} else  if (AKCEPTACJA_OZP_ACCEPTANCE.equals(acceptationCn)) {
                String author = doc.getAuthor();
                DSUser userAuthor = DSUser.findByUsername(author);
                DSDivision[] usersDivisions = userAuthor.getDivisionsWithoutGroup();
                char c = usersDivisions[0].getCode().charAt(0);

                List<String> users = new ArrayList<String>();
                List<String> divisions = new ArrayList<String>();
                String usedAcceptanceCn;
                switch (c) {
                    case 'K':
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_K_ACCEPTANCE);
                        usedAcceptanceCn = OZP_K_ACCEPTANCE;
                        break;
                    case 'G':
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_G_ACCEPTANCE);
                        usedAcceptanceCn = OZP_G_ACCEPTANCE;
                        break;
                    case 'W':
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_W_ACCEPTANCE);
                        usedAcceptanceCn = OZP_W_ACCEPTANCE;
                        break;
                    case 'O':
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_O_ACCEPTANCE);
                        usedAcceptanceCn = OZP_O_ACCEPTANCE;
                        break;
                    case 'T':
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_T_ACCEPTANCE);
                        usedAcceptanceCn = OZP_T_ACCEPTANCE;
                        break;
                    default:
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_O_ACCEPTANCE);
                        usedAcceptanceCn = OZP_O_ACCEPTANCE;
                }

                for (String user : users)
                {
                    assignable.addCandidateUser(user);
                    AssigneeHandler.addToHistory(openExecution, doc, user, null);
                    assigned = true;
                }
                for (String division : divisions)
                {
                    assignable.addCandidateGroup(division);
                    AssigneeHandler.addToHistory(openExecution, doc, null, division);
                    assigned = true;
                }
                if (!assigned) {
                    throw new EdmException("Nie znaleziono nikogo do przypisania na drzewie akceptacji " +
                            "dla akceptacji: '" + usedAcceptanceCn + "'");
                }
            }
        	
        	// Jezeli nie znalazlo tu odpowiedniej akceptacji to szukam w klasie powyzej
        	if (!assigned) {
        		assigned = super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
        	}
            return assigned;
        } catch (EdmException e) {
            if (e instanceof ProfileNotAssignedException || e instanceof ValueNotFoundException || e instanceof DivisionNotFoundException) {
                throw e;
            }
            throw e;
        } catch (SQLException e) {
            throw new EdmException(e);
        }
	}
		
		/**
	     * @param doc
	     * @param assignable
	     * @param openExecution
	     * @param dicId         identyfikator wpisu w s�owniku, kt�rego nazw� okre�la
	     *                      fieldCnValue
	     * @return 'true' je�li znaleziono u�ytkownika
	     * @throws EdmException
	     * @throws UserNotFoundException
	     */
	    private boolean assignUser(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Long userId)
	            throws EdmException, UserNotFoundException
	    {
	        boolean assigned = false;
	        String userName = DSUser.findById(userId).getName();
	        assignable.addCandidateUser(userName);
	        assigned = true;
	        AssigneeHandler.addToHistory(openExecution, doc, userName, null);
	        return assigned;
	    }
	
	    
    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        FieldsManager fm = doc.getFieldsManager();

        ReservationDocument exported = new ReservationDocument();

        exported.setReservationStateFromId(1);

        //exported.setReservationNumber(fm.getStringKey(ERP_DOCUMENT_IDM));

        //exported.getHeaderBudget().setBudgetDirectCode(Docusafe.getAdditionPropertyOrDefault("export.dokument_rezerwacyjny.idm_budzetu", "IMGW-2014\\00001")/* IDM BUD?ETU, OKRESU BUD?ETOWEGO, USTAWI? NA STA?E WG SERWIS?W ERPOWYCH. DOCELOWO TRZEBA WYRZUCI? DO NAG??WKA*/);
        exported.getHeaderBudget().setBudgetDirectId(Long.valueOf(Docusafe.getAdditionPropertyOrDefault("export.dokument_rezerwacyjny.id_budzetu", "10")));

        exported.setBudgetReservationTypeCode(Docusafe.getAdditionPropertyOrDefault("export.dokument_rezerwacyjny.typ","WMR"));
        exported.setReservationMethod(ReservationDocument.ReservationMethod.AUTO);


        List<BudgetItem> positionItems = Lists.newArrayList();
        exported.setPostionItems(positionItems);

        List<Map<String, Object>> positions = (List) fm.getValue(PRZEDMIOT_ZAMOWIENIA);

        BudgetItem bItem;
        int nrp = 1;
        for (Map<String, Object> position : positions) {
            positionItems.add(bItem = new BudgetItem());

            bItem.setItemNo(nrp);
            bItem.setQuantity(Objects.firstNonNull((BigDecimal) position.get(PRZEDMIOT_ZAMOWIENIA + "_ILOSC"), BigDecimal.ONE));
            bItem.setNetAmount((BigDecimal) position.get(PRZEDMIOT_ZAMOWIENIA_CENA_JEDN_NETTO));

            bItem.setName(ObjectUtils.toString(position.get(PRZEDMIOT_ZAMOWIENIA + "_" + "DOSTAWA")));
            bItem.setCostKindName(ObjectUtils.toString(position.get(PRZEDMIOT_ZAMOWIENIA + "_" + "NAZWA")));

            bItem.setBudgetDirectId(exported.getHeaderBudget().getBudgetDirectId());

            bItem.setBudgetKoId(position.get(PRZEDMIOT_ZAMOWIENIA + "_" + "BUDGET") != null && !Strings.isNullOrEmpty(((EnumValues) position.get(PRZEDMIOT_ZAMOWIENIA + "_" + "BUDGET")).getSelectedId()) ? Long.valueOf(((EnumValues) position.get(PRZEDMIOT_ZAMOWIENIA + "_" + "BUDGET")).getSelectedId()) : null);
            bItem.setBudgetKindId(position.get(PRZEDMIOT_ZAMOWIENIA + "_" + "POSITION") != null && !Strings.isNullOrEmpty(((EnumValues) position.get(PRZEDMIOT_ZAMOWIENIA + "_" + "POSITION")).getSelectedId()) ? Long.valueOf(((EnumValues) position.get(PRZEDMIOT_ZAMOWIENIA + "_" + "POSITION")).getSelectedId()) : null);

            if (exported.getHeaderBudget().getBudgetKoId() == null) {
                exported.getHeaderBudget().setBudgetKoId(bItem.getBudgetKoId());
            }

            EnumValues fsEnum = (EnumValues) position.get(PRZEDMIOT_ZAMOWIENIA + "_" + "SOURCE");

            if (fsEnum != null && !Strings.isNullOrEmpty(fsEnum.getSelectedId())) {
                FundSource fItem = new FundSource();
                fItem.setFundPercent(new BigDecimal(100));
                fItem.setCode(Source.find(Long.valueOf(fsEnum.getSelectedId())).getZrodloIdn());
                bItem.setFundSources(Lists.newArrayList(fItem));
            }

            nrp++;
        }

        exported.setDescription(ImgwDocumentLogic.buildAdditionDescription(getAdditionFieldsDescription(doc, fm)));

        String systemPath;

        if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu") != null) {
            systemPath = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
        } else {
            systemPath = Docusafe.getBaseUrl();
        }

        for (Attachment zal : doc.getAttachments()) {
            AttachmentInfo info = new AttachmentInfo();

            String path = systemPath + "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();

            info.setName(zal.getTitle());
            info.setUrl(path);

            exported.addAttachementInfo(info);
        }
        return exported;
    }

    private Map<String,String> getAdditionFieldsDescription(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String,String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO",doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Rodzaj zam�wienia", fm.getStringValue("RODZAJ_ZAMOWIENIA"));
        additionFields.put("Rodzaj przedmiotu", fm.getStringValue("RODZAJ_PRZEDMIOTU"));
        additionFields.put("Uzasadnienie zakupu", fm.getStringValue("UZASADNIENIE_ZAKUPU"));
        additionFields.put("Symbol kom�rki", fm.getStringValue("SYMBOL_KOMORKI"));
        additionFields.put("Numer wniosku", fm.getStringValue("NUMER_WNIOSKU"));

        return additionFields;
    }
	    protected void setBudgetItemsRefValues(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results) throws EntityNotFoundException, EdmException {
	        String dic = dictionaryName + "_";
	        DwrUtils.setRefValueEnumOptions(values, results, "BUDGET", dic, "POSITION", BUDGET_POSITIONS_TABLENAME, null, EMPTY_ENUM_VALUE);
	        DwrUtils.setRefValueEnumOptions(values, results, "POSITION", dic, "SOURCE", SOURCES_TABLENAME, null, EMPTY_ENUM_VALUE);

	    }
}
