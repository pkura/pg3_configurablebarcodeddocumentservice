package pl.compan.docusafe.parametrization.imgwwaw.exports;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.exports.Checker;
import pl.compan.docusafe.core.exports.RequestResult;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.util.axis.AxisClientUtils;
import pl.compan.docusafe.ws.imgw.RequestServiceStub;

public class SimpleWSChecker implements Checker {
	private RequestServiceStub stub;
	private RequestResult result;

	public RequestServiceStub getStub() throws EdmException {
		if (stub == null) {
			initChecker();
		}
		return stub;
	}

	public void setStub(RequestServiceStub stub) {
		this.stub = stub;
	}

	@Override
	public void initChecker() throws EdmException {
		try {
			AxisClientConfigurator conf = new AxisClientConfigurator();
			setStub(new RequestServiceStub(conf.getAxisConfigurationContext()));
			conf.setUpHttpParameters(stub, "/services/RequestService");
            conf.setHttpClient(AxisClientUtils.createHttpClient(18, 18));
        } catch (Exception e) {
			throw new EdmException(e.getMessage(),e);
		}
	}

	@Override
	public void check(String guid) throws EdmException {
		try {
			RequestServiceStub.GetStatusForGuid param = new RequestServiceStub.GetStatusForGuid();
			param.setGuid(guid);

			RequestServiceStub.GetStatusForGuidResponse result = getStub().getStatusForGuid(param);
			
			this.result = new RequestResult();
			
			if (result != null && result.get_return() != null) {
				this.result.setStateId(result.get_return().getState());
				this.result.setStateName(result.get_return().getStateName());
				this.result.setResult(result.get_return().getResult());
			}
		} catch (Exception e) {
			throw new EdmException(e);
		}
	}

	@Override
	public void finalizeChecker() {
		// TODO Auto-generated method stub

	}

	@Override
	public RequestResult getResult() {
		return result;
	}

}
