package pl.compan.docusafe.parametrization.imgwwaw;

/**
 * Created by brs on 18.12.13.
 */
public class CostAcceptance {
    private String budzet;
    private String pozycja;
    private String zrodlo;

    private String kwota;

    private String kierownikKomorki;
    private String kierownikKomorkiData;
    private String dyrektorPionu;
    private String dyrektorPionuData;
    private String kierownikProjektu;

    public String getKierownikProjektuData() {
        return kierownikProjektuData;
    }

    public void setKierownikProjektuData(String kierownikProjektuData) {
        this.kierownikProjektuData = kierownikProjektuData;
    }

    public String getKierownikProjektu() {
        return kierownikProjektu;
    }

    public void setKierownikProjektu(String kierownikProjektu) {
        this.kierownikProjektu = kierownikProjektu;
    }

    public String getDyrektorPionuData() {
        return dyrektorPionuData;
    }

    public void setDyrektorPionuData(String dyrektorPionuData) {
        this.dyrektorPionuData = dyrektorPionuData;
    }

    public String getDyrektorPionu() {
        return dyrektorPionu;
    }

    public void setDyrektorPionu(String dyrektorPionu) {
        this.dyrektorPionu = dyrektorPionu;
    }

    public String getKierownikKomorkiData() {
        return kierownikKomorkiData;
    }

    public void setKierownikKomorkiData(String kierownikKomorkiData) {
        this.kierownikKomorkiData = kierownikKomorkiData;
    }

    public String getKierownikKomorki() {
        return kierownikKomorki;
    }

    public void setKierownikKomorki(String kierownikKomorki) {
        this.kierownikKomorki = kierownikKomorki;
    }

    public String getKwota() {
        return kwota;
    }

    public void setKwota(String kwota) {
        this.kwota = kwota;
    }

    public String getZrodlo() {
        return zrodlo;
    }

    public void setZrodlo(String zrodlo) {
        this.zrodlo = zrodlo;
    }

    public String getPozycja() {
        return pozycja;
    }

    public void setPozycja(String pozycja) {
        this.pozycja = pozycja;
    }

    public String getBudzet() {
        return budzet;
    }

    public void setBudzet(String budzet) {
        this.budzet = budzet;
    }

    private String kierownikProjektuData;

}
